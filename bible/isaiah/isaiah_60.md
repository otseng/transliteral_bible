# [Isaiah 60](https://www.blueletterbible.org/kjv/isa/60/1/s_739001)

<a name="isaiah_60_1"></a>Isaiah 60:1

[quwm](../../strongs/h/h6965.md), ['owr](../../strongs/h/h215.md); for thy ['owr](../../strongs/h/h216.md) is [bow'](../../strongs/h/h935.md), and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) is [zāraḥ](../../strongs/h/h2224.md) upon thee.

<a name="isaiah_60_2"></a>Isaiah 60:2

For, behold, the [choshek](../../strongs/h/h2822.md) shall [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md), and ['araphel](../../strongs/h/h6205.md) the [lĕom](../../strongs/h/h3816.md): but [Yĕhovah](../../strongs/h/h3068.md) shall [zāraḥ](../../strongs/h/h2224.md) upon thee, and his [kabowd](../../strongs/h/h3519.md) shall be [ra'ah](../../strongs/h/h7200.md) upon thee.

<a name="isaiah_60_3"></a>Isaiah 60:3

And the [gowy](../../strongs/h/h1471.md) shall [halak](../../strongs/h/h1980.md) to thy ['owr](../../strongs/h/h216.md), and [melek](../../strongs/h/h4428.md) to the [nogahh](../../strongs/h/h5051.md) of thy [Zeraḥ](../../strongs/h/h2225.md).

<a name="isaiah_60_4"></a>Isaiah 60:4

[nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) round about, and [ra'ah](../../strongs/h/h7200.md): all they [qāḇaṣ](../../strongs/h/h6908.md) themselves, they [bow'](../../strongs/h/h935.md) to thee: thy [ben](../../strongs/h/h1121.md) shall [bow'](../../strongs/h/h935.md) from [rachowq](../../strongs/h/h7350.md), and thy [bath](../../strongs/h/h1323.md) shall be ['aman](../../strongs/h/h539.md) at thy [ṣaḏ](../../strongs/h/h6654.md).

<a name="isaiah_60_5"></a>Isaiah 60:5

Then thou shalt [ra'ah](../../strongs/h/h7200.md) [yare'](../../strongs/h/h3372.md), and [nāhar](../../strongs/h/h5102.md), and thine [lebab](../../strongs/h/h3824.md) shall [pachad](../../strongs/h/h6342.md), and be [rāḥaḇ](../../strongs/h/h7337.md); because the [hāmôn](../../strongs/h/h1995.md) of the [yam](../../strongs/h/h3220.md) shall be [hāp̄aḵ](../../strongs/h/h2015.md) unto thee, the [ḥayil](../../strongs/h/h2428.md) of the [gowy](../../strongs/h/h1471.md) shall [bow'](../../strongs/h/h935.md) unto thee.

<a name="isaiah_60_6"></a>Isaiah 60:6

The [šip̄ʿâ](../../strongs/h/h8229.md) of [gāmāl](../../strongs/h/h1581.md) shall [kāsâ](../../strongs/h/h3680.md) thee, the [bēḵer](../../strongs/h/h1070.md) of [Miḏyān](../../strongs/h/h4080.md) and [ʿÊp̄â](../../strongs/h/h5891.md); all they from [Šᵊḇā'](../../strongs/h/h7614.md) shall come: they shall bring [zāhāḇ](../../strongs/h/h2091.md) and [lᵊḇônâ](../../strongs/h/h3828.md); and they shall [bāśar](../../strongs/h/h1319.md) the [tehillah](../../strongs/h/h8416.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_60_7"></a>Isaiah 60:7

All the [tso'n](../../strongs/h/h6629.md) of [Qēḏār](../../strongs/h/h6938.md) shall be [qāḇaṣ](../../strongs/h/h6908.md) unto thee, the ['ayil](../../strongs/h/h352.md) of [Nᵊḇāyôṯ](../../strongs/h/h5032.md) shall [sharath](../../strongs/h/h8334.md) unto thee: they shall come up with [ratsown](../../strongs/h/h7522.md) on mine [mizbeach](../../strongs/h/h4196.md), and I will [pā'ar](../../strongs/h/h6286.md) the [bayith](../../strongs/h/h1004.md) of my [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="isaiah_60_8"></a>Isaiah 60:8

Who are these that ['uwph](../../strongs/h/h5774.md) as an ['ab](../../strongs/h/h5645.md), and as the [yônâ](../../strongs/h/h3123.md) to their ['ărubâ](../../strongs/h/h699.md)?

<a name="isaiah_60_9"></a>Isaiah 60:9

Surely the ['î](../../strongs/h/h339.md) shall [qāvâ](../../strongs/h/h6960.md) for me, and the ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md) first, to [bow'](../../strongs/h/h935.md) thy [ben](../../strongs/h/h1121.md) from far, their [keceph](../../strongs/h/h3701.md) and their [zāhāḇ](../../strongs/h/h2091.md) with them, unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and to the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), because he hath [pā'ar](../../strongs/h/h6286.md) thee.

<a name="isaiah_60_10"></a>Isaiah 60:10

And the [ben](../../strongs/h/h1121.md) of [nēḵār](../../strongs/h/h5236.md) shall [bānâ](../../strongs/h/h1129.md) thy [ḥômâ](../../strongs/h/h2346.md), and their [melek](../../strongs/h/h4428.md) shall [sharath](../../strongs/h/h8334.md) unto thee: for in my [qeṣep̄](../../strongs/h/h7110.md) I [nakah](../../strongs/h/h5221.md) thee, but in my [ratsown](../../strongs/h/h7522.md) have I had [racham](../../strongs/h/h7355.md) on thee.

<a name="isaiah_60_11"></a>Isaiah 60:11

Therefore thy [sha'ar](../../strongs/h/h8179.md) shall be [pāṯaḥ](../../strongs/h/h6605.md) [tāmîḏ](../../strongs/h/h8548.md); they shall not be [cagar](../../strongs/h/h5462.md) [yômām](../../strongs/h/h3119.md) nor [layil](../../strongs/h/h3915.md); that men may [bow'](../../strongs/h/h935.md) unto thee the [ḥayil](../../strongs/h/h2428.md) of the [gowy](../../strongs/h/h1471.md), and that their [melek](../../strongs/h/h4428.md) may be [nāhaḡ](../../strongs/h/h5090.md).

<a name="isaiah_60_12"></a>Isaiah 60:12

For the [gowy](../../strongs/h/h1471.md) and [mamlāḵâ](../../strongs/h/h4467.md) that will not ['abad](../../strongs/h/h5647.md) thee shall ['abad](../../strongs/h/h6.md); yea, those [gowy](../../strongs/h/h1471.md) shall be [ḥāraḇ](../../strongs/h/h2717.md) [ḥāraḇ](../../strongs/h/h2717.md).

<a name="isaiah_60_13"></a>Isaiah 60:13

The [kabowd](../../strongs/h/h3519.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) shall [bow'](../../strongs/h/h935.md) unto thee, the [bᵊrôš](../../strongs/h/h1265.md), the [tiḏhār](../../strongs/h/h8410.md), and the [tᵊ'aššûr](../../strongs/h/h8391.md) together, to [pā'ar](../../strongs/h/h6286.md) the [maqowm](../../strongs/h/h4725.md) of my [miqdash](../../strongs/h/h4720.md); and I will make the [maqowm](../../strongs/h/h4725.md) of my [regel](../../strongs/h/h7272.md) [kabad](../../strongs/h/h3513.md).

<a name="isaiah_60_14"></a>Isaiah 60:14

The [ben](../../strongs/h/h1121.md) also of them that [ʿānâ](../../strongs/h/h6031.md) thee shall come [shachach](../../strongs/h/h7817.md) unto thee; and all they that [na'ats](../../strongs/h/h5006.md) thee shall [shachah](../../strongs/h/h7812.md) at the [kaph](../../strongs/h/h3709.md) of thy [regel](../../strongs/h/h7272.md); and they shall call thee; The [ʿîr](../../strongs/h/h5892.md) of [Yĕhovah](../../strongs/h/h3068.md), The [Tsiyown](../../strongs/h/h6726.md) of the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_60_15"></a>Isaiah 60:15

Whereas thou has been ['azab](../../strongs/h/h5800.md) and [sane'](../../strongs/h/h8130.md), so that no man ['abar](../../strongs/h/h5674.md) thee, I will [śûm](../../strongs/h/h7760.md) thee an ['owlam](../../strongs/h/h5769.md) [gā'ôn](../../strongs/h/h1347.md), a [māśôś](../../strongs/h/h4885.md) of many [dôr](../../strongs/h/h1755.md).

<a name="isaiah_60_16"></a>Isaiah 60:16

Thou shalt also [yānaq](../../strongs/h/h3243.md) the [chalab](../../strongs/h/h2461.md) of the [gowy](../../strongs/h/h1471.md), and shalt [yānaq](../../strongs/h/h3243.md) the [šaḏ](../../strongs/h/h7699.md) of [melek](../../strongs/h/h4428.md): and thou shalt [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) am thy [yasha'](../../strongs/h/h3467.md) and thy [gā'al](../../strongs/h/h1350.md), the ['abiyr](../../strongs/h/h46.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="isaiah_60_17"></a>Isaiah 60:17

For [nᵊḥšeṯ](../../strongs/h/h5178.md) I will [bow'](../../strongs/h/h935.md) [zāhāḇ](../../strongs/h/h2091.md), and for [barzel](../../strongs/h/h1270.md) I will [bow'](../../strongs/h/h935.md) [keceph](../../strongs/h/h3701.md), and for ['ets](../../strongs/h/h6086.md) [nᵊḥšeṯ](../../strongs/h/h5178.md), and for ['eben](../../strongs/h/h68.md) [barzel](../../strongs/h/h1270.md): I will also make thy [pᵊqudâ](../../strongs/h/h6486.md) [shalowm](../../strongs/h/h7965.md), and thine [nāḡaś](../../strongs/h/h5065.md) [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_60_18"></a>Isaiah 60:18

[chamac](../../strongs/h/h2555.md) shall no more be [shama'](../../strongs/h/h8085.md) in thy ['erets](../../strongs/h/h776.md), [shod](../../strongs/h/h7701.md) nor [šeḇar](../../strongs/h/h7667.md) within thy [gᵊḇûl](../../strongs/h/h1366.md); but thou shalt [qara'](../../strongs/h/h7121.md) thy [ḥômâ](../../strongs/h/h2346.md) [yĕshuw'ah](../../strongs/h/h3444.md), and thy [sha'ar](../../strongs/h/h8179.md) [tehillah](../../strongs/h/h8416.md).

<a name="isaiah_60_19"></a>Isaiah 60:19

The [šemeš](../../strongs/h/h8121.md) shall be no more thy ['owr](../../strongs/h/h216.md) by [yômām](../../strongs/h/h3119.md); neither for [nogahh](../../strongs/h/h5051.md) shall the [yareach](../../strongs/h/h3394.md) give ['owr](../../strongs/h/h216.md) unto thee: but [Yĕhovah](../../strongs/h/h3068.md) shall be unto thee an ['owlam](../../strongs/h/h5769.md) ['owr](../../strongs/h/h215.md), and thy ['Elohiym](../../strongs/h/h430.md) thy [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="isaiah_60_20"></a>Isaiah 60:20

Thy [šemeš](../../strongs/h/h8121.md) shall no more [bow'](../../strongs/h/h935.md); neither shall thy [yeraḥ](../../strongs/h/h3391.md) ['āsap̄](../../strongs/h/h622.md) itself: for [Yĕhovah](../../strongs/h/h3068.md) shall be thine ['owlam](../../strongs/h/h5769.md) ['owr](../../strongs/h/h216.md), and the [yowm](../../strongs/h/h3117.md) of thy ['ēḇel](../../strongs/h/h60.md) shall be [shalam](../../strongs/h/h7999.md).

<a name="isaiah_60_21"></a>Isaiah 60:21

Thy ['am](../../strongs/h/h5971.md) also shall be all [tsaddiyq](../../strongs/h/h6662.md): they shall [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) ['owlam](../../strongs/h/h5769.md), the [nēṣer](../../strongs/h/h5342.md) of my [maṭṭāʿ](../../strongs/h/h4302.md), the [ma'aseh](../../strongs/h/h4639.md) of my [yad](../../strongs/h/h3027.md), that I may be [pā'ar](../../strongs/h/h6286.md).

<a name="isaiah_60_22"></a>Isaiah 60:22

A [qāṭān](../../strongs/h/h6996.md) shall become an ['elep̄](../../strongs/h/h505.md), and a [ṣāʿîr](../../strongs/h/h6810.md) an ['atsuwm](../../strongs/h/h6099.md) [gowy](../../strongs/h/h1471.md): I [Yĕhovah](../../strongs/h/h3068.md) will [ḥûš](../../strongs/h/h2363.md) it in his [ʿēṯ](../../strongs/h/h6256.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 59](isaiah_59.md) - [Isaiah 61](isaiah_61.md)