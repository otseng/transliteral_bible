# [Isaiah 5](https://www.blueletterbible.org/kjv/isa/5/1/s_684001)

<a name="isaiah_5_1"></a>Isaiah 5:1

Now will I [shiyr](../../strongs/h/h7891.md) to my [yāḏîḏ](../../strongs/h/h3039.md) a [šîr](../../strongs/h/h7892.md) of my [dôḏ](../../strongs/h/h1730.md) touching his [kerem](../../strongs/h/h3754.md). My [yāḏîḏ](../../strongs/h/h3039.md) hath a [kerem](../../strongs/h/h3754.md) in a [ben](../../strongs/h/h1121.md) [šemen](../../strongs/h/h8081.md) [qeren](../../strongs/h/h7161.md):

<a name="isaiah_5_2"></a>Isaiah 5:2

And he [ʿāzaq](../../strongs/h/h5823.md) it, and gathered out the [sāqal](../../strongs/h/h5619.md) thereof, and [nāṭaʿ](../../strongs/h/h5193.md) it with the [śrēq](../../strongs/h/h8321.md), and [bānâ](../../strongs/h/h1129.md) a [miḡdāl](../../strongs/h/h4026.md) in the [tavek](../../strongs/h/h8432.md) of it, and also made a [yeqeḇ](../../strongs/h/h3342.md) therein: and he [qāvâ](../../strongs/h/h6960.md) that it should ['asah](../../strongs/h/h6213.md) [ʿēnāḇ](../../strongs/h/h6025.md), and it ['asah](../../strongs/h/h6213.md) [bᵊ'ušîm](../../strongs/h/h891.md).

<a name="isaiah_5_3"></a>Isaiah 5:3

And now, O [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and men of [Yehuwdah](../../strongs/h/h3063.md), [shaphat](../../strongs/h/h8199.md), I pray you, betwixt me and my [kerem](../../strongs/h/h3754.md).

<a name="isaiah_5_4"></a>Isaiah 5:4

What could have been ['asah](../../strongs/h/h6213.md) more to my [kerem](../../strongs/h/h3754.md), that I have not ['asah](../../strongs/h/h6213.md) in it? wherefore, when I [qāvâ](../../strongs/h/h6960.md) that it should ['asah](../../strongs/h/h6213.md) [ʿēnāḇ](../../strongs/h/h6025.md), ['asah](../../strongs/h/h6213.md) it [bᵊ'ušîm](../../strongs/h/h891.md)?

<a name="isaiah_5_5"></a>Isaiah 5:5

And now go to; I will [yada'](../../strongs/h/h3045.md) you what I will ['asah](../../strongs/h/h6213.md) to my [kerem](../../strongs/h/h3754.md): I will [cuwr](../../strongs/h/h5493.md) the [mᵊśûḵâ](../../strongs/h/h4881.md) thereof, and it shall be [bāʿar](../../strongs/h/h1197.md); and [pāraṣ](../../strongs/h/h6555.md) the [gāḏēr](../../strongs/h/h1447.md) thereof, and it shall be [mirmās](../../strongs/h/h4823.md):

<a name="isaiah_5_6"></a>Isaiah 5:6

And I will [shiyth](../../strongs/h/h7896.md) it [bāṯâ](../../strongs/h/h1326.md): it shall not be [zāmar](../../strongs/h/h2168.md), nor [ʿāḏar](../../strongs/h/h5737.md); but there shall come up [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md): I will also [tsavah](../../strongs/h/h6680.md) the ['ab](../../strongs/h/h5645.md) that they [matar](../../strongs/h/h4305.md) no [māṭār](../../strongs/h/h4306.md) upon it.

<a name="isaiah_5_7"></a>Isaiah 5:7

For the [kerem](../../strongs/h/h3754.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) his [šaʿăšûʿîm](../../strongs/h/h8191.md) [neṭaʿ](../../strongs/h/h5194.md): and he [qāvâ](../../strongs/h/h6960.md) for [mishpat](../../strongs/h/h4941.md), but behold [miśpāḥ](../../strongs/h/h4939.md); for [ṣĕdāqāh](../../strongs/h/h6666.md), but behold a [tsa'aqah](../../strongs/h/h6818.md).

<a name="isaiah_5_8"></a>Isaiah 5:8

[hôy](../../strongs/h/h1945.md) unto them that [naga'](../../strongs/h/h5060.md) [bayith](../../strongs/h/h1004.md) to [bayith](../../strongs/h/h1004.md), that [qāraḇ](../../strongs/h/h7126.md) [sadeh](../../strongs/h/h7704.md) to [sadeh](../../strongs/h/h7704.md), till there be ['ep̄es](../../strongs/h/h657.md) [maqowm](../../strongs/h/h4725.md), that they may be [yashab](../../strongs/h/h3427.md) alone in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md)!

<a name="isaiah_5_9"></a>Isaiah 5:9

In mine ['ozen](../../strongs/h/h241.md) said [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), [lō'](../../strongs/h/h3808.md) many [bayith](../../strongs/h/h1004.md) shall be [šammâ](../../strongs/h/h8047.md), even [gadowl](../../strongs/h/h1419.md) and [towb](../../strongs/h/h2896.md), without [yashab](../../strongs/h/h3427.md).

<a name="isaiah_5_10"></a>Isaiah 5:10

[kî](../../strongs/h/h3588.md), ten [ṣemeḏ](../../strongs/h/h6776.md) of [kerem](../../strongs/h/h3754.md) shall ['asah](../../strongs/h/h6213.md) one [baṯ](../../strongs/h/h1324.md), and the [zera'](../../strongs/h/h2233.md) of an [ḥōmer](../../strongs/h/h2563.md) shall ['asah](../../strongs/h/h6213.md) an ['êp̄â](../../strongs/h/h374.md).

<a name="isaiah_5_11"></a>Isaiah 5:11

[hôy](../../strongs/h/h1945.md) unto them that [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), that they may follow [šēḵār](../../strongs/h/h7941.md); that ['āḥar](../../strongs/h/h309.md) until [nešep̄](../../strongs/h/h5399.md), till [yayin](../../strongs/h/h3196.md) [dalaq](../../strongs/h/h1814.md) them!

<a name="isaiah_5_12"></a>Isaiah 5:12

And the [kinnôr](../../strongs/h/h3658.md), and the [neḇel](../../strongs/h/h5035.md), the [tōp̄](../../strongs/h/h8596.md), and [ḥālîl](../../strongs/h/h2485.md), and [yayin](../../strongs/h/h3196.md), are in their [mištê](../../strongs/h/h4960.md): but they [nabat](../../strongs/h/h5027.md) not the [pōʿal](../../strongs/h/h6467.md) of [Yĕhovah](../../strongs/h/h3068.md), neither [ra'ah](../../strongs/h/h7200.md) the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md).

<a name="isaiah_5_13"></a>Isaiah 5:13

Therefore my ['am](../../strongs/h/h5971.md) are [gālâ](../../strongs/h/h1540.md), because they have no [da'ath](../../strongs/h/h1847.md): and their [kabowd](../../strongs/h/h3519.md) [math](../../strongs/h/h4962.md) are [rāʿāḇ](../../strongs/h/h7458.md), and their [hāmôn](../../strongs/h/h1995.md) [ṣiḥê](../../strongs/h/h6704.md) with [ṣāmā'](../../strongs/h/h6772.md).

<a name="isaiah_5_14"></a>Isaiah 5:14

Therefore [shĕ'owl](../../strongs/h/h7585.md) hath [rāḥaḇ](../../strongs/h/h7337.md) herself, and [pāʿar](../../strongs/h/h6473.md) her [peh](../../strongs/h/h6310.md) without [choq](../../strongs/h/h2706.md): and their [hadar](../../strongs/h/h1926.md), and their [hāmôn](../../strongs/h/h1995.md), and their [שā'ôn](../../strongs/h/h7588.md), and he that [ʿālēz](../../strongs/h/h5938.md), shall [yarad](../../strongs/h/h3381.md) into it.

<a name="isaiah_5_15"></a>Isaiah 5:15

And the ['āḏām](../../strongs/h/h120.md) shall be [shachach](../../strongs/h/h7817.md), and the ['iysh](../../strongs/h/h376.md) shall be [šāp̄ēl](../../strongs/h/h8213.md), and the ['ayin](../../strongs/h/h5869.md) of the [gāḇōha](../../strongs/h/h1364.md) shall be [šāp̄ēl](../../strongs/h/h8213.md):

<a name="isaiah_5_16"></a>Isaiah 5:16

But [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall be [gāḇah](../../strongs/h/h1361.md) in [mishpat](../../strongs/h/h4941.md), and ['el](../../strongs/h/h410.md) that is [qadowsh](../../strongs/h/h6918.md) shall be [qadash](../../strongs/h/h6942.md) in [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_5_17"></a>Isaiah 5:17

Then shall the [keḇeś](../../strongs/h/h3532.md) [ra'ah](../../strongs/h/h7462.md) after their [dibēr](../../strongs/h/h1699.md), and the [chorbah](../../strongs/h/h2723.md) of the [mēaḥ](../../strongs/h/h4220.md) shall [guwr](../../strongs/h/h1481.md) ['akal](../../strongs/h/h398.md).

<a name="isaiah_5_18"></a>Isaiah 5:18

[hôy](../../strongs/h/h1945.md) unto them that [mashak](../../strongs/h/h4900.md) ['avon](../../strongs/h/h5771.md) with [chebel](../../strongs/h/h2256.md) of [shav'](../../strongs/h/h7723.md), and [chatta'ath](../../strongs/h/h2403.md) as it were with a [ʿăḡālâ](../../strongs/h/h5699.md) [ʿăḇōṯ](../../strongs/h/h5688.md):

<a name="isaiah_5_19"></a>Isaiah 5:19

That ['āmar](../../strongs/h/h559.md), Let him make [māhar](../../strongs/h/h4116.md), and [ḥûš](../../strongs/h/h2363.md) his [ma'aseh](../../strongs/h/h4639.md), that we may [ra'ah](../../strongs/h/h7200.md) it: and let the ['etsah](../../strongs/h/h6098.md) of the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) [qāraḇ](../../strongs/h/h7126.md) and [bow'](../../strongs/h/h935.md), that we may [yada'](../../strongs/h/h3045.md) it!

<a name="isaiah_5_20"></a>Isaiah 5:20

[hôy](../../strongs/h/h1945.md) unto them that ['āmar](../../strongs/h/h559.md) [ra'](../../strongs/h/h7451.md) [towb](../../strongs/h/h2896.md), and [towb](../../strongs/h/h2896.md) [ra'](../../strongs/h/h7451.md); that put [choshek](../../strongs/h/h2822.md) for ['owr](../../strongs/h/h216.md), and ['owr](../../strongs/h/h216.md) for [choshek](../../strongs/h/h2822.md); that put [mar](../../strongs/h/h4751.md) for [māṯôq](../../strongs/h/h4966.md), and [māṯôq](../../strongs/h/h4966.md) for [mar](../../strongs/h/h4751.md)!

<a name="isaiah_5_21"></a>Isaiah 5:21

[hôy](../../strongs/h/h1945.md) unto them that are [ḥāḵām](../../strongs/h/h2450.md) in their own ['ayin](../../strongs/h/h5869.md), and [bîn](../../strongs/h/h995.md) in their own [paniym](../../strongs/h/h6440.md)!

<a name="isaiah_5_22"></a>Isaiah 5:22

[hôy](../../strongs/h/h1945.md) unto them that are [gibôr](../../strongs/h/h1368.md) to [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md), and ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md) to [māsaḵ](../../strongs/h/h4537.md) [šēḵār](../../strongs/h/h7941.md):

<a name="isaiah_5_23"></a>Isaiah 5:23

Which [ṣāḏaq](../../strongs/h/h6663.md) the [rasha'](../../strongs/h/h7563.md) for [shachad](../../strongs/h/h7810.md), and take away the [ṣĕdāqāh](../../strongs/h/h6666.md) of the [tsaddiyq](../../strongs/h/h6662.md) from him!

<a name="isaiah_5_24"></a>Isaiah 5:24

Therefore as the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) the [qaš](../../strongs/h/h7179.md), and the [lashown](../../strongs/h/h3956.md) [lehāḇâ](../../strongs/h/h3852.md) [rāp̄â](../../strongs/h/h7503.md) the [ḥăšaš](../../strongs/h/h2842.md), so their [šereš](../../strongs/h/h8328.md) shall be as [maq](../../strongs/h/h4716.md), and their [peraḥ](../../strongs/h/h6525.md) shall go up as ['āḇāq](../../strongs/h/h80.md): because they have [mā'as](../../strongs/h/h3988.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and [na'ats](../../strongs/h/h5006.md) the ['imrah](../../strongs/h/h565.md) of the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_5_25"></a>Isaiah 5:25

Therefore is the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥārâ](../../strongs/h/h2734.md) against his ['am](../../strongs/h/h5971.md), and he hath [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) against them, and hath [nakah](../../strongs/h/h5221.md) them: and the [har](../../strongs/h/h2022.md) did [ragaz](../../strongs/h/h7264.md), and their [nᵊḇēlâ](../../strongs/h/h5038.md) were [sûḥâ](../../strongs/h/h5478.md) in the midst of the [ḥûṣ](../../strongs/h/h2351.md). For all this his ['aph](../../strongs/h/h639.md) is not turned away, but his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md) still.

<a name="isaiah_5_26"></a>Isaiah 5:26

And he will [nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md) to the [gowy](../../strongs/h/h1471.md) from far, and will [šāraq](../../strongs/h/h8319.md) unto them from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md): and, behold, they shall come with [mᵊhērâ](../../strongs/h/h4120.md) [qal](../../strongs/h/h7031.md):

<a name="isaiah_5_27"></a>Isaiah 5:27

None shall be [ʿāyēp̄](../../strongs/h/h5889.md) nor [kashal](../../strongs/h/h3782.md) among them; none shall [nûm](../../strongs/h/h5123.md) nor [yashen](../../strongs/h/h3462.md); neither shall the ['ēzôr](../../strongs/h/h232.md) of their [ḥālāṣ](../../strongs/h/h2504.md) be [pāṯaḥ](../../strongs/h/h6605.md), nor the [śᵊrôḵ](../../strongs/h/h8288.md) of their [naʿal](../../strongs/h/h5275.md) be [nathaq](../../strongs/h/h5423.md):

<a name="isaiah_5_28"></a>Isaiah 5:28

Whose [chets](../../strongs/h/h2671.md) are [šānan](../../strongs/h/h8150.md), and all their [qesheth](../../strongs/h/h7198.md) [dāraḵ](../../strongs/h/h1869.md), their [sûs](../../strongs/h/h5483.md) hoofs shall be [chashab](../../strongs/h/h2803.md) like [tsar](../../strongs/h/h6862.md), and their [galgal](../../strongs/h/h1534.md) like a [sûp̄â](../../strongs/h/h5492.md):

<a name="isaiah_5_29"></a>Isaiah 5:29

Their [šᵊ'āḡâ](../../strongs/h/h7581.md) shall be like a [lāḇî'](../../strongs/h/h3833.md), they shall [šā'aḡ](../../strongs/h/h7580.md) like [kephiyr](../../strongs/h/h3715.md): yea, they shall [nāham](../../strongs/h/h5098.md), and lay hold of the [ṭerep̄](../../strongs/h/h2964.md), and shall [palat](../../strongs/h/h6403.md), and none shall [natsal](../../strongs/h/h5337.md) it.

<a name="isaiah_5_30"></a>Isaiah 5:30

And in that [yowm](../../strongs/h/h3117.md) they shall [nāham](../../strongs/h/h5098.md) against them like the [nᵊhāmâ](../../strongs/h/h5100.md) of the [yam](../../strongs/h/h3220.md): and if one look unto the ['erets](../../strongs/h/h776.md), behold [choshek](../../strongs/h/h2822.md) and [tsar](../../strongs/h/h6862.md), and the ['owr](../../strongs/h/h216.md) is [ḥāšaḵ](../../strongs/h/h2821.md) in the [ʿărîp̄îm](../../strongs/h/h6183.md) thereof.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 4](isaiah_4.md) - [Isaiah 6](isaiah_6.md)