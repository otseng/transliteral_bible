# [Isaiah 48](https://www.blueletterbible.org/kjv/isa/48/1/s_727001)

<a name="isaiah_48_1"></a>Isaiah 48:1

[shama'](../../strongs/h/h8085.md) ye this, O [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), which are [qara'](../../strongs/h/h7121.md) by the [shem](../../strongs/h/h8034.md) of [Yisra'el](../../strongs/h/h3478.md), and are [yāṣā'](../../strongs/h/h3318.md) out of the [mayim](../../strongs/h/h4325.md) of [Yehuwdah](../../strongs/h/h3063.md), which [shaba'](../../strongs/h/h7650.md) by the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), and [zakar](../../strongs/h/h2142.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), but not in ['emeth](../../strongs/h/h571.md), nor in [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_48_2"></a>Isaiah 48:2

For they [qara'](../../strongs/h/h7121.md) themselves of the [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md), and [camak](../../strongs/h/h5564.md) themselves upon the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md).

<a name="isaiah_48_3"></a>Isaiah 48:3

I have [nāḡaḏ](../../strongs/h/h5046.md) the [ri'šôn](../../strongs/h/h7223.md) from the ['āz](../../strongs/h/h227.md); and they [yāṣā'](../../strongs/h/h3318.md) out of my [peh](../../strongs/h/h6310.md), and I [shama'](../../strongs/h/h8085.md) them; I did them [piṯ'ōm](../../strongs/h/h6597.md), and they [bow'](../../strongs/h/h935.md).

<a name="isaiah_48_4"></a>Isaiah 48:4

Because I [da'ath](../../strongs/h/h1847.md) that thou art [qāšê](../../strongs/h/h7186.md), and thy [ʿōrep̄](../../strongs/h/h6203.md) is a [barzel](../../strongs/h/h1270.md) [gîḏ](../../strongs/h/h1517.md), and thy [mēṣaḥ](../../strongs/h/h4696.md) [nᵊḥûšâ](../../strongs/h/h5154.md);

<a name="isaiah_48_5"></a>Isaiah 48:5

I have even from the ['āz](../../strongs/h/h227.md) [nāḡaḏ](../../strongs/h/h5046.md) it to thee; before it [bow'](../../strongs/h/h935.md) I [shama'](../../strongs/h/h8085.md) it thee: lest thou shouldest ['āmar](../../strongs/h/h559.md), Mine [ʿōṣeḇ](../../strongs/h/h6090.md) hath ['asah](../../strongs/h/h6213.md) them, and my [pecel](../../strongs/h/h6459.md), and my [necek](../../strongs/h/h5262.md), hath [tsavah](../../strongs/h/h6680.md) them.

<a name="isaiah_48_6"></a>Isaiah 48:6

Thou hast [shama'](../../strongs/h/h8085.md), [chazah](../../strongs/h/h2372.md) all this; and will not ye [nāḡaḏ](../../strongs/h/h5046.md) it? I have [shama'](../../strongs/h/h8085.md) thee [ḥāḏāš](../../strongs/h/h2319.md) from [ʿatâ](../../strongs/h/h6258.md), even [nāṣar](../../strongs/h/h5341.md), and thou didst not [yada'](../../strongs/h/h3045.md) them.

<a name="isaiah_48_7"></a>Isaiah 48:7

They are [bara'](../../strongs/h/h1254.md) now, and not from the beginning; even [paniym](../../strongs/h/h6440.md) the [yowm](../../strongs/h/h3117.md) when thou [shama'](../../strongs/h/h8085.md) them not; lest thou shouldest ['āmar](../../strongs/h/h559.md), Behold, I [yada'](../../strongs/h/h3045.md) them.

<a name="isaiah_48_8"></a>Isaiah 48:8

Yea, thou [shama'](../../strongs/h/h8085.md) not; yea, thou [yada'](../../strongs/h/h3045.md) not; yea, from that time that thine ['ozen](../../strongs/h/h241.md) was not [pāṯaḥ](../../strongs/h/h6605.md): for I [yada'](../../strongs/h/h3045.md) that thou wouldest deal [bāḡaḏ](../../strongs/h/h898.md) [bāḡaḏ](../../strongs/h/h898.md), and wast [qara'](../../strongs/h/h7121.md) a [pāšaʿ](../../strongs/h/h6586.md) from the [beten](../../strongs/h/h990.md).

<a name="isaiah_48_9"></a>Isaiah 48:9

For my [shem](../../strongs/h/h8034.md) sake will I ['arak](../../strongs/h/h748.md) mine ['aph](../../strongs/h/h639.md), and for my [tehillah](../../strongs/h/h8416.md) will I [ḥāṭam](../../strongs/h/h2413.md) for thee, that I not [karath](../../strongs/h/h3772.md) thee.

<a name="isaiah_48_10"></a>Isaiah 48:10

Behold, I have [tsaraph](../../strongs/h/h6884.md) thee, but not with [keceph](../../strongs/h/h3701.md); I have [bāḥar](../../strongs/h/h977.md) thee in the [kûr](../../strongs/h/h3564.md) of ['oniy](../../strongs/h/h6040.md).

<a name="isaiah_48_11"></a>Isaiah 48:11

For mine own sake, even for mine own sake, will I ['asah](../../strongs/h/h6213.md) it: for how should be [ḥālal](../../strongs/h/h2490.md)? and I will not [nathan](../../strongs/h/h5414.md) my [kabowd](../../strongs/h/h3519.md) unto another.

<a name="isaiah_48_12"></a>Isaiah 48:12

[shama'](../../strongs/h/h8085.md) unto me, O [Ya'aqob](../../strongs/h/h3290.md) and [Yisra'el](../../strongs/h/h3478.md), my [qara'](../../strongs/h/h7121.md); I am he; I am the [ri'šôn](../../strongs/h/h7223.md), I also am the ['aḥărôn](../../strongs/h/h314.md).

<a name="isaiah_48_13"></a>Isaiah 48:13

Mine [yad](../../strongs/h/h3027.md) also hath laid the [yacad](../../strongs/h/h3245.md) of the ['erets](../../strongs/h/h776.md), and my [yamiyn](../../strongs/h/h3225.md) hath [ṭāp̄aḥ](../../strongs/h/h2946.md) the [shamayim](../../strongs/h/h8064.md): when I [qara'](../../strongs/h/h7121.md) unto them, they ['amad](../../strongs/h/h5975.md) together.

<a name="isaiah_48_14"></a>Isaiah 48:14

All ye, [qāḇaṣ](../../strongs/h/h6908.md) yourselves, and [shama'](../../strongs/h/h8085.md); which among them hath [nāḡaḏ](../../strongs/h/h5046.md) these things? [Yĕhovah](../../strongs/h/h3068.md) hath ['ahab](../../strongs/h/h157.md) him: he will do his [chephets](../../strongs/h/h2656.md) on [Bāḇel](../../strongs/h/h894.md), and his [zerowa'](../../strongs/h/h2220.md) shall be on the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="isaiah_48_15"></a>Isaiah 48:15

I, even I, have [dabar](../../strongs/h/h1696.md); yea, I have [qara'](../../strongs/h/h7121.md) him: I have [bow'](../../strongs/h/h935.md) him, and he shall make his [derek](../../strongs/h/h1870.md) [tsalach](../../strongs/h/h6743.md).

<a name="isaiah_48_16"></a>Isaiah 48:16

[qāraḇ](../../strongs/h/h7126.md) ye near unto me, [shama'](../../strongs/h/h8085.md) ye this; I have not [dabar](../../strongs/h/h1696.md) in [cether](../../strongs/h/h5643.md) from the [ro'sh](../../strongs/h/h7218.md); from the time that it was, there am I: and now ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), and his [ruwach](../../strongs/h/h7307.md), hath [shalach](../../strongs/h/h7971.md) me.

<a name="isaiah_48_17"></a>Isaiah 48:17

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), thy [gā'al](../../strongs/h/h1350.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md); I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) which [lamad](../../strongs/h/h3925.md) thee to [yāʿal](../../strongs/h/h3276.md), which [dāraḵ](../../strongs/h/h1869.md) thee by the [derek](../../strongs/h/h1870.md) that thou shouldest [yālaḵ](../../strongs/h/h3212.md).

<a name="isaiah_48_18"></a>Isaiah 48:18

O that thou hadst [qashab](../../strongs/h/h7181.md) to my [mitsvah](../../strongs/h/h4687.md)! then had thy [shalowm](../../strongs/h/h7965.md) been as a [nāhār](../../strongs/h/h5104.md), and thy [ṣĕdāqāh](../../strongs/h/h6666.md) as the [gal](../../strongs/h/h1530.md) of the [yam](../../strongs/h/h3220.md):

<a name="isaiah_48_19"></a>Isaiah 48:19

Thy [zera'](../../strongs/h/h2233.md) also had been as the [ḥôl](../../strongs/h/h2344.md), and the [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) of thy [me'ah](../../strongs/h/h4578.md) like the [māʿâ](../../strongs/h/h4579.md) thereof; his [shem](../../strongs/h/h8034.md) should not have been [karath](../../strongs/h/h3772.md) nor [šāmaḏ](../../strongs/h/h8045.md) from before me.

<a name="isaiah_48_20"></a>Isaiah 48:20

[yāṣā'](../../strongs/h/h3318.md) ye of [Bāḇel](../../strongs/h/h894.md), [bāraḥ](../../strongs/h/h1272.md) ye from the [Kaśdîmâ](../../strongs/h/h3778.md), with a [qowl](../../strongs/h/h6963.md) of [rinnah](../../strongs/h/h7440.md) [nāḡaḏ](../../strongs/h/h5046.md) ye, tell this, [yāṣā'](../../strongs/h/h3318.md) it even to the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md); say ye, [Yĕhovah](../../strongs/h/h3068.md) hath [gā'al](../../strongs/h/h1350.md) his ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md).

<a name="isaiah_48_21"></a>Isaiah 48:21

And they [ṣāmē'](../../strongs/h/h6770.md) not when he [yālaḵ](../../strongs/h/h3212.md) them through the [chorbah](../../strongs/h/h2723.md): he caused the [mayim](../../strongs/h/h4325.md) to [nāzal](../../strongs/h/h5140.md) out of the [tsuwr](../../strongs/h/h6697.md) for them: he [bāqaʿ](../../strongs/h/h1234.md) the [tsuwr](../../strongs/h/h6697.md) also, and the [mayim](../../strongs/h/h4325.md) [zûḇ](../../strongs/h/h2100.md).

<a name="isaiah_48_22"></a>Isaiah 48:22

There is no [shalowm](../../strongs/h/h7965.md), saith [Yĕhovah](../../strongs/h/h3068.md), unto the [rasha'](../../strongs/h/h7563.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 47](isaiah_47.md) - [Isaiah 49](isaiah_49.md)