# [Isaiah 16](https://www.blueletterbible.org/kjv/isa/16/1/s_695001)

<a name="isaiah_16_1"></a>Isaiah 16:1

[shalach](../../strongs/h/h7971.md) ye the [kar](../../strongs/h/h3733.md) to the [mashal](../../strongs/h/h4910.md) of the ['erets](../../strongs/h/h776.md) from [selaʿ](../../strongs/h/h5554.md) to the [midbar](../../strongs/h/h4057.md), unto the [har](../../strongs/h/h2022.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_16_2"></a>Isaiah 16:2

For it shall be, that, as a [nāḏaḏ](../../strongs/h/h5074.md) [ʿôp̄](../../strongs/h/h5775.md) [shalach](../../strongs/h/h7971.md) of the [qēn](../../strongs/h/h7064.md), so the [bath](../../strongs/h/h1323.md) of [Mô'āḇ](../../strongs/h/h4124.md) shall be at the [maʿăḇār](../../strongs/h/h4569.md) of ['Arnôn](../../strongs/h/h769.md).

<a name="isaiah_16_3"></a>Isaiah 16:3

Take ['etsah](../../strongs/h/h6098.md), ['asah](../../strongs/h/h6213.md) [pᵊlîlâ](../../strongs/h/h6415.md); [shiyth](../../strongs/h/h7896.md) thy [ṣēl](../../strongs/h/h6738.md) as the [layil](../../strongs/h/h3915.md) in the [tavek](../../strongs/h/h8432.md) of the [ṣōhar](../../strongs/h/h6672.md); [cathar](../../strongs/h/h5641.md) the [nāḏaḥ](../../strongs/h/h5080.md); [gālâ](../../strongs/h/h1540.md) not him that [nāḏaḏ](../../strongs/h/h5074.md).

<a name="isaiah_16_4"></a>Isaiah 16:4

Let mine [nāḏaḥ](../../strongs/h/h5080.md) [guwr](../../strongs/h/h1481.md) with thee, [Mô'āḇ](../../strongs/h/h4124.md); be thou a [cether](../../strongs/h/h5643.md) to them from the [paniym](../../strongs/h/h6440.md) of the [shadad](../../strongs/h/h7703.md): for the [mûṣ](../../strongs/h/h4160.md) is at an ['āp̄as](../../strongs/h/h656.md), the [shod](../../strongs/h/h7701.md) [kalah](../../strongs/h/h3615.md), the [rāmas](../../strongs/h/h7429.md) are [tamam](../../strongs/h/h8552.md) out of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_16_5"></a>Isaiah 16:5

And in [checed](../../strongs/h/h2617.md) shall the [kicce'](../../strongs/h/h3678.md) be [kuwn](../../strongs/h/h3559.md): and he shall [yashab](../../strongs/h/h3427.md) upon it in ['emeth](../../strongs/h/h571.md) in the ['ohel](../../strongs/h/h168.md) of [Dāviḏ](../../strongs/h/h1732.md), [shaphat](../../strongs/h/h8199.md), and [darash](../../strongs/h/h1875.md) [mishpat](../../strongs/h/h4941.md), and [māhîr](../../strongs/h/h4106.md) [tsedeq](../../strongs/h/h6664.md).

<a name="isaiah_16_6"></a>Isaiah 16:6

We have [shama'](../../strongs/h/h8085.md) of the [gā'ôn](../../strongs/h/h1347.md) of [Mô'āḇ](../../strongs/h/h4124.md); he is very [gē'](../../strongs/h/h1341.md): even of his [ga'avah](../../strongs/h/h1346.md), and his [gā'ôn](../../strongs/h/h1347.md), and his ['ebrah](../../strongs/h/h5678.md): but his [baḏ](../../strongs/h/h907.md) shall not be so.

<a name="isaiah_16_7"></a>Isaiah 16:7

Therefore shall [Mô'āḇ](../../strongs/h/h4124.md) [yālal](../../strongs/h/h3213.md) for [Mô'āḇ](../../strongs/h/h4124.md), every one shall [yālal](../../strongs/h/h3213.md): for the ['ăšîš](../../strongs/h/h808.md) of [Qîr-ḥereś](../../strongs/h/h7025.md) shall ye [hagah](../../strongs/h/h1897.md); surely they are [nāḵā'](../../strongs/h/h5218.md).

<a name="isaiah_16_8"></a>Isaiah 16:8

For the [šᵊḏēmâ](../../strongs/h/h7709.md) of [Hešbôn](../../strongs/h/h2809.md) ['āmal](../../strongs/h/h535.md), and the [gep̄en](../../strongs/h/h1612.md) of [Śᵊḇām](../../strongs/h/h7643.md): the [baʿal](../../strongs/h/h1167.md) of the [gowy](../../strongs/h/h1471.md) have [hālam](../../strongs/h/h1986.md) the [śārōq](../../strongs/h/h8291.md) thereof, they are [naga'](../../strongs/h/h5060.md) even unto [Yaʿzêr](../../strongs/h/h3270.md), they [tāʿâ](../../strongs/h/h8582.md) through the [midbar](../../strongs/h/h4057.md): her [šᵊluḥôṯ](../../strongs/h/h7976.md) are [nāṭaš](../../strongs/h/h5203.md), they are ['abar](../../strongs/h/h5674.md) the [yam](../../strongs/h/h3220.md).

<a name="isaiah_16_9"></a>Isaiah 16:9

Therefore I will [bāḵâ](../../strongs/h/h1058.md) with the [bĕkiy](../../strongs/h/h1065.md) of [Yaʿzêr](../../strongs/h/h3270.md) the [gep̄en](../../strongs/h/h1612.md) of [Śᵊḇām](../../strongs/h/h7643.md): I will [rāvâ](../../strongs/h/h7301.md) thee with my [dim'ah](../../strongs/h/h1832.md), O [Hešbôn](../../strongs/h/h2809.md), and ['Elʿālē'](../../strongs/h/h500.md): for the [hêḏāḏ](../../strongs/h/h1959.md) for thy [qayiṣ](../../strongs/h/h7019.md) and for thy [qāṣîr](../../strongs/h/h7105.md) is [naphal](../../strongs/h/h5307.md).

<a name="isaiah_16_10"></a>Isaiah 16:10

And [simchah](../../strongs/h/h8057.md) is ['āsap̄](../../strongs/h/h622.md), and [gîl](../../strongs/h/h1524.md) out of the [karmel](../../strongs/h/h3759.md); and in the [kerem](../../strongs/h/h3754.md) there shall be no [ranan](../../strongs/h/h7442.md), neither shall there be [rûaʿ](../../strongs/h/h7321.md): the [dāraḵ](../../strongs/h/h1869.md) shall [dāraḵ](../../strongs/h/h1869.md) no [yayin](../../strongs/h/h3196.md) in their [yeqeḇ](../../strongs/h/h3342.md); I have made their [hêḏāḏ](../../strongs/h/h1959.md) to [shabath](../../strongs/h/h7673.md).

<a name="isaiah_16_11"></a>Isaiah 16:11

Wherefore my [me'ah](../../strongs/h/h4578.md) shall [hāmâ](../../strongs/h/h1993.md) like a [kinnôr](../../strongs/h/h3658.md) for [Mô'āḇ](../../strongs/h/h4124.md), and mine [qereḇ](../../strongs/h/h7130.md) for [Qîr-ḥereś](../../strongs/h/h7025.md).

<a name="isaiah_16_12"></a>Isaiah 16:12

And it shall come to pass, when it is [ra'ah](../../strongs/h/h7200.md) that [Mô'āḇ](../../strongs/h/h4124.md) is [lā'â](../../strongs/h/h3811.md) on the [bāmâ](../../strongs/h/h1116.md), that he shall [bow'](../../strongs/h/h935.md) to his [miqdash](../../strongs/h/h4720.md) to [palal](../../strongs/h/h6419.md); but he shall not [yakol](../../strongs/h/h3201.md).

<a name="isaiah_16_13"></a>Isaiah 16:13

This is the [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) concerning [Mô'āḇ](../../strongs/h/h4124.md) since that ['āz](../../strongs/h/h227.md).

<a name="isaiah_16_14"></a>Isaiah 16:14

But now [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), Within three [šānâ](../../strongs/h/h8141.md), as the [šānâ](../../strongs/h/h8141.md) of a [śāḵîr](../../strongs/h/h7916.md), and the [kabowd](../../strongs/h/h3519.md) of [Mô'āḇ](../../strongs/h/h4124.md) shall be [qālâ](../../strongs/h/h7034.md), with all that [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md); and the [šᵊ'ār](../../strongs/h/h7605.md) shall be very [mizʿār](../../strongs/h/h4213.md) and [lō'](../../strongs/h/h3808.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 15](isaiah_15.md) - [Isaiah 17](isaiah_17.md)