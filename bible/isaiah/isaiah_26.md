# [Isaiah 26](https://www.blueletterbible.org/kjv/isa/26/1/s_705001)

<a name="isaiah_26_1"></a>Isaiah 26:1

In that [yowm](../../strongs/h/h3117.md) shall this [šîr](../../strongs/h/h7892.md) be [shiyr](../../strongs/h/h7891.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md); We have a ['oz](../../strongs/h/h5797.md) [ʿîr](../../strongs/h/h5892.md); [yĕshuw'ah](../../strongs/h/h3444.md) will God [shiyth](../../strongs/h/h7896.md) for [ḥômâ](../../strongs/h/h2346.md) and [cheyl](../../strongs/h/h2426.md).

<a name="isaiah_26_2"></a>Isaiah 26:2

[pāṯaḥ](../../strongs/h/h6605.md) ye the [sha'ar](../../strongs/h/h8179.md), that the [tsaddiyq](../../strongs/h/h6662.md) [gowy](../../strongs/h/h1471.md) which [shamar](../../strongs/h/h8104.md) the ['ēmûn](../../strongs/h/h529.md) may [bow'](../../strongs/h/h935.md).

<a name="isaiah_26_3"></a>Isaiah 26:3

Thou wilt [nāṣar](../../strongs/h/h5341.md) him in [shalowm](../../strongs/h/h7965.md) [shalowm](../../strongs/h/h7965.md), whose [yetser](../../strongs/h/h3336.md) is [camak](../../strongs/h/h5564.md) on thee: because he [batach](../../strongs/h/h982.md) in thee.

<a name="isaiah_26_4"></a>Isaiah 26:4

[batach](../../strongs/h/h982.md) ye in [Yĕhovah](../../strongs/h/h3068.md) for ever: for in [Yahh](../../strongs/h/h3050.md) [Yĕhovah](../../strongs/h/h3068.md) is ['owlam](../../strongs/h/h5769.md) [tsuwr](../../strongs/h/h6697.md):

<a name="isaiah_26_5"></a>Isaiah 26:5

For he [shachach](../../strongs/h/h7817.md) them that [yashab](../../strongs/h/h3427.md) on [marowm](../../strongs/h/h4791.md); the [śāḡaḇ](../../strongs/h/h7682.md) [qiryâ](../../strongs/h/h7151.md), he [šāp̄ēl](../../strongs/h/h8213.md); he [šāp̄ēl](../../strongs/h/h8213.md), even to the ['erets](../../strongs/h/h776.md); he [naga'](../../strongs/h/h5060.md) it even to the ['aphar](../../strongs/h/h6083.md).

<a name="isaiah_26_6"></a>Isaiah 26:6

The [regel](../../strongs/h/h7272.md) shall [rāmas](../../strongs/h/h7429.md), even the [regel](../../strongs/h/h7272.md) of the ['aniy](../../strongs/h/h6041.md), and the [pa'am](../../strongs/h/h6471.md) of the [dal](../../strongs/h/h1800.md).

<a name="isaiah_26_7"></a>Isaiah 26:7

The ['orach](../../strongs/h/h734.md) of the [tsaddiyq](../../strongs/h/h6662.md) is [meyshar](../../strongs/h/h4339.md): thou, most [yashar](../../strongs/h/h3477.md), dost [pālas](../../strongs/h/h6424.md) the [ma'gal](../../strongs/h/h4570.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="isaiah_26_8"></a>Isaiah 26:8

Yea, in the ['orach](../../strongs/h/h734.md) of thy [mishpat](../../strongs/h/h4941.md), [Yĕhovah](../../strongs/h/h3068.md), have we [qāvâ](../../strongs/h/h6960.md) for thee; the [ta'avah](../../strongs/h/h8378.md) of our [nephesh](../../strongs/h/h5315.md) is to thy [shem](../../strongs/h/h8034.md), and to the [zeker](../../strongs/h/h2143.md) of thee.

<a name="isaiah_26_9"></a>Isaiah 26:9

With my [nephesh](../../strongs/h/h5315.md) have I ['āvâ](../../strongs/h/h183.md) thee in the [layil](../../strongs/h/h3915.md); yea, with my [ruwach](../../strongs/h/h7307.md) within me will I [šāḥar](../../strongs/h/h7836.md): for when thy [mishpat](../../strongs/h/h4941.md) are in the ['erets](../../strongs/h/h776.md), the [yashab](../../strongs/h/h3427.md) of the [tebel](../../strongs/h/h8398.md) will learn [tsedeq](../../strongs/h/h6664.md).

<a name="isaiah_26_10"></a>Isaiah 26:10

Let [chanan](../../strongs/h/h2603.md) to the [rasha'](../../strongs/h/h7563.md), yet will he not [lamad](../../strongs/h/h3925.md) [tsedeq](../../strongs/h/h6664.md): in the ['erets](../../strongs/h/h776.md) of [nᵊḵōḥâ](../../strongs/h/h5229.md) will he [ʿāval](../../strongs/h/h5765.md), and will not [ra'ah](../../strongs/h/h7200.md) the [ge'uwth](../../strongs/h/h1348.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_26_11"></a>Isaiah 26:11

[Yĕhovah](../../strongs/h/h3068.md), when thy [yad](../../strongs/h/h3027.md) is [ruwm](../../strongs/h/h7311.md), they will not [chazah](../../strongs/h/h2372.md): but they shall [chazah](../../strongs/h/h2372.md), and be [buwsh](../../strongs/h/h954.md) for their [qin'â](../../strongs/h/h7068.md) at the ['am](../../strongs/h/h5971.md); yea, the ['esh](../../strongs/h/h784.md) of thine [tsar](../../strongs/h/h6862.md) shall ['akal](../../strongs/h/h398.md) them.

<a name="isaiah_26_12"></a>Isaiah 26:12

[Yĕhovah](../../strongs/h/h3068.md), thou wilt [šāp̄aṯ](../../strongs/h/h8239.md) [shalowm](../../strongs/h/h7965.md) for us: for thou also hast [pa'al](../../strongs/h/h6466.md) all our [ma'aseh](../../strongs/h/h4639.md) in us.

<a name="isaiah_26_13"></a>Isaiah 26:13

[Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), other ['adown](../../strongs/h/h113.md) beside thee have had [bāʿal](../../strongs/h/h1166.md) over us: but by thee only will we [zakar](../../strongs/h/h2142.md) of thy [shem](../../strongs/h/h8034.md).

<a name="isaiah_26_14"></a>Isaiah 26:14

They are [muwth](../../strongs/h/h4191.md), they shall not [ḥāyâ](../../strongs/h/h2421.md); they are [rᵊp̄ā'îm](../../strongs/h/h7496.md), they shall not [quwm](../../strongs/h/h6965.md): therefore hast thou [paqad](../../strongs/h/h6485.md) and [šāmaḏ](../../strongs/h/h8045.md) them, and made all their [zeker](../../strongs/h/h2143.md) to ['abad](../../strongs/h/h6.md).

<a name="isaiah_26_15"></a>Isaiah 26:15

Thou hast [yāsap̄](../../strongs/h/h3254.md) the [gowy](../../strongs/h/h1471.md), [Yĕhovah](../../strongs/h/h3068.md), thou hast [yāsap̄](../../strongs/h/h3254.md) the [gowy](../../strongs/h/h1471.md): thou art [kabad](../../strongs/h/h3513.md): thou hadst [rachaq](../../strongs/h/h7368.md) it far unto all the [qeṣev](../../strongs/h/h7099.md) of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_26_16"></a>Isaiah 26:16

[Yĕhovah](../../strongs/h/h3068.md), in [tsar](../../strongs/h/h6862.md) have they [paqad](../../strongs/h/h6485.md) thee, they [ṣûq](../../strongs/h/h6694.md) a [laḥaš](../../strongs/h/h3908.md) when thy [mûsār](../../strongs/h/h4148.md) was upon them.

<a name="isaiah_26_17"></a>Isaiah 26:17

Like as a [hārê](../../strongs/h/h2030.md), that [qāraḇ](../../strongs/h/h7126.md) the [yalad](../../strongs/h/h3205.md), is in [chuwl](../../strongs/h/h2342.md), and [zāʿaq](../../strongs/h/h2199.md) in her [chebel](../../strongs/h/h2256.md); so have we been in thy [paniym](../../strongs/h/h6440.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_26_18"></a>Isaiah 26:18

We have been [harah](../../strongs/h/h2029.md), we have been in [chuwl](../../strongs/h/h2342.md), we have as it were [yalad](../../strongs/h/h3205.md) [ruwach](../../strongs/h/h7307.md); we have not ['asah](../../strongs/h/h6213.md) any [yĕshuw'ah](../../strongs/h/h3444.md) in the ['erets](../../strongs/h/h776.md); neither have the [yashab](../../strongs/h/h3427.md) of the [tebel](../../strongs/h/h8398.md) [naphal](../../strongs/h/h5307.md).

<a name="isaiah_26_19"></a>Isaiah 26:19

Thy [muwth](../../strongs/h/h4191.md) men shall [ḥāyâ](../../strongs/h/h2421.md), together with my [nᵊḇēlâ](../../strongs/h/h5038.md) shall they [quwm](../../strongs/h/h6965.md). [quwts](../../strongs/h/h6974.md) and [ranan](../../strongs/h/h7442.md), ye that [shakan](../../strongs/h/h7931.md) in ['aphar](../../strongs/h/h6083.md): for thy [ṭal](../../strongs/h/h2919.md) is as the [ṭal](../../strongs/h/h2919.md) of ['ôrâ](../../strongs/h/h219.md), and the ['erets](../../strongs/h/h776.md) shall [naphal](../../strongs/h/h5307.md) the [rᵊp̄ā'îm](../../strongs/h/h7496.md).

<a name="isaiah_26_20"></a>Isaiah 26:20

[yālaḵ](../../strongs/h/h3212.md), my ['am](../../strongs/h/h5971.md), enter thou into thy [ḥeḏer](../../strongs/h/h2315.md), and [cagar](../../strongs/h/h5462.md) thy [deleṯ](../../strongs/h/h1817.md) about thee: [ḥāḇâ](../../strongs/h/h2247.md) thyself as it were for a little [reḡaʿ](../../strongs/h/h7281.md), until the [zaʿam](../../strongs/h/h2195.md) be overpast.

<a name="isaiah_26_21"></a>Isaiah 26:21

For, behold, [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) of his [maqowm](../../strongs/h/h4725.md) to [paqad](../../strongs/h/h6485.md) the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) for their ['avon](../../strongs/h/h5771.md): the ['erets](../../strongs/h/h776.md) also shall [gālâ](../../strongs/h/h1540.md) her [dam](../../strongs/h/h1818.md), and shall no more [kāsâ](../../strongs/h/h3680.md) her [harag](../../strongs/h/h2026.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 25](isaiah_25.md) - [Isaiah 27](isaiah_27.md)