# [Isaiah 64](https://www.blueletterbible.org/kjv/isa/64/1/s_743001)

<a name="isaiah_64_1"></a>Isaiah 64:1

Oh that thou wouldest [qāraʿ](../../strongs/h/h7167.md) the [shamayim](../../strongs/h/h8064.md), that thou wouldest [yarad](../../strongs/h/h3381.md), that the [har](../../strongs/h/h2022.md) might [zālal](../../strongs/h/h2151.md) at thy [paniym](../../strongs/h/h6440.md),

<a name="isaiah_64_2"></a>Isaiah 64:2

As when the [hemes](../../strongs/h/h2003.md) ['esh](../../strongs/h/h784.md) [qāḏaḥ](../../strongs/h/h6919.md), the ['esh](../../strongs/h/h784.md) causeth the [mayim](../../strongs/h/h4325.md) to [bāʿâ](../../strongs/h/h1158.md), to make thy [shem](../../strongs/h/h8034.md) [yada'](../../strongs/h/h3045.md) to thine [tsar](../../strongs/h/h6862.md), that the [gowy](../../strongs/h/h1471.md) may [ragaz](../../strongs/h/h7264.md) at thy [paniym](../../strongs/h/h6440.md)!

<a name="isaiah_64_3"></a>Isaiah 64:3

When thou didst [yare'](../../strongs/h/h3372.md) which we [qāvâ](../../strongs/h/h6960.md) not for, thou [yarad](../../strongs/h/h3381.md), the [har](../../strongs/h/h2022.md) [zālal](../../strongs/h/h2151.md) at thy [paniym](../../strongs/h/h6440.md).

<a name="isaiah_64_4"></a>Isaiah 64:4

For since ['owlam](../../strongs/h/h5769.md) men have not [shama'](../../strongs/h/h8085.md), nor ['azan](../../strongs/h/h238.md), neither hath the ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md), O ['Elohiym](../../strongs/h/h430.md), beside thee, what he hath ['asah](../../strongs/h/h6213.md) for him that [ḥāḵâ](../../strongs/h/h2442.md) for him.

<a name="isaiah_64_5"></a>Isaiah 64:5

Thou [pāḡaʿ](../../strongs/h/h6293.md) him that [śûś](../../strongs/h/h7797.md) and ['asah](../../strongs/h/h6213.md) [tsedeq](../../strongs/h/h6664.md), those that [zakar](../../strongs/h/h2142.md) thee in thy ways: behold, thou art [qāṣap̄](../../strongs/h/h7107.md); for we have [chata'](../../strongs/h/h2398.md): in those is ['owlam](../../strongs/h/h5769.md), and we shall be [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_64_6"></a>Isaiah 64:6

But we are all as a [tame'](../../strongs/h/h2931.md) thing, and all our [tsedaqah](../../strongs/h/h6666.md) are as [ʿidâ](../../strongs/h/h5708.md) [beḡeḏ](../../strongs/h/h899.md); and we all do [nabel](../../strongs/h/h5034.md) [bālal](../../strongs/h/h1101.md) as an ['aleh](../../strongs/h/h5929.md); and our ['avon](../../strongs/h/h5771.md), like the [ruwach](../../strongs/h/h7307.md), have [nasa'](../../strongs/h/h5375.md).

<a name="isaiah_64_7"></a>Isaiah 64:7

And there is none that [qara'](../../strongs/h/h7121.md) upon thy [shem](../../strongs/h/h8034.md), that [ʿûr](../../strongs/h/h5782.md) himself to [ḥāzaq](../../strongs/h/h2388.md) of thee: for thou hast [cathar](../../strongs/h/h5641.md) thy [paniym](../../strongs/h/h6440.md) from us, and hast [mûḡ](../../strongs/h/h4127.md) us, because of our ['avon](../../strongs/h/h5771.md).

<a name="isaiah_64_8"></a>Isaiah 64:8

But now, [Yĕhovah](../../strongs/h/h3068.md), thou art our ['ab](../../strongs/h/h1.md); we are the [ḥōmer](../../strongs/h/h2563.md), and thou our [yāṣar](../../strongs/h/h3335.md); and we all are the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md).

<a name="isaiah_64_9"></a>Isaiah 64:9

Be not [qāṣap̄](../../strongs/h/h7107.md) [me'od](../../strongs/h/h3966.md), [Yĕhovah](../../strongs/h/h3068.md), neither [zakar](../../strongs/h/h2142.md) ['avon](../../strongs/h/h5771.md) [ʿaḏ](../../strongs/h/h5703.md): behold, see, we beseech thee, we are all thy ['am](../../strongs/h/h5971.md).

<a name="isaiah_64_10"></a>Isaiah 64:10

Thy [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md) are a [midbar](../../strongs/h/h4057.md), [Tsiyown](../../strongs/h/h6726.md) is a [midbar](../../strongs/h/h4057.md), [Yĕruwshalaim](../../strongs/h/h3389.md) a [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="isaiah_64_11"></a>Isaiah 64:11

Our [qodesh](../../strongs/h/h6944.md) and our [tip̄'ārâ](../../strongs/h/h8597.md) [bayith](../../strongs/h/h1004.md), where our ['ab](../../strongs/h/h1.md) [halal](../../strongs/h/h1984.md) thee, is [śᵊrēp̄â](../../strongs/h/h8316.md) with ['esh](../../strongs/h/h784.md): and all our [maḥmāḏ](../../strongs/h/h4261.md) are [chorbah](../../strongs/h/h2723.md).

<a name="isaiah_64_12"></a>Isaiah 64:12

Wilt thou ['āp̄aq](../../strongs/h/h662.md) thyself for these things, [Yĕhovah](../../strongs/h/h3068.md)? wilt thou [ḥāšâ](../../strongs/h/h2814.md), and [ʿānâ](../../strongs/h/h6031.md) us [me'od](../../strongs/h/h3966.md)?

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 63](isaiah_63.md) - [Isaiah 65](isaiah_65.md)