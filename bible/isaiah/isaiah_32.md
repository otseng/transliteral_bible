# [Isaiah 32](https://www.blueletterbible.org/kjv/isa/32/1/s_711001)

<a name="isaiah_32_1"></a>Isaiah 32:1

Behold, a [melek](../../strongs/h/h4428.md) shall [mālaḵ](../../strongs/h/h4427.md) in [tsedeq](../../strongs/h/h6664.md), and [śar](../../strongs/h/h8269.md) shall [śārar](../../strongs/h/h8323.md) in [mishpat](../../strongs/h/h4941.md).

<a name="isaiah_32_2"></a>Isaiah 32:2

And an ['iysh](../../strongs/h/h376.md) shall be as a [maḥăḇē'](../../strongs/h/h4224.md) from the [ruwach](../../strongs/h/h7307.md), and a [cether](../../strongs/h/h5643.md) from the [zerem](../../strongs/h/h2230.md); as [peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md) in a [ṣāyôn](../../strongs/h/h6724.md), as the [ṣēl](../../strongs/h/h6738.md) of a [kāḇēḏ](../../strongs/h/h3515.md) [cela'](../../strongs/h/h5553.md) in a [ʿāyēp̄](../../strongs/h/h5889.md) ['erets](../../strongs/h/h776.md).

<a name="isaiah_32_3"></a>Isaiah 32:3

And the ['ayin](../../strongs/h/h5869.md) of them that [ra'ah](../../strongs/h/h7200.md) shall not [šāʿâ](../../strongs/h/h8159.md), and the ['ozen](../../strongs/h/h241.md) of them that [shama'](../../strongs/h/h8085.md) shall [qashab](../../strongs/h/h7181.md).

<a name="isaiah_32_4"></a>Isaiah 32:4

The [lebab](../../strongs/h/h3824.md) also of the [māhar](../../strongs/h/h4116.md) shall [bîn](../../strongs/h/h995.md) [da'ath](../../strongs/h/h1847.md), and the [lashown](../../strongs/h/h3956.md) of the [ʿillēḡ](../../strongs/h/h5926.md) shall [māhar](../../strongs/h/h4116.md)to [dabar](../../strongs/h/h1696.md) [ṣaḥ](../../strongs/h/h6703.md).

<a name="isaiah_32_5"></a>Isaiah 32:5

The [nabal](../../strongs/h/h5036.md) shall be no more [qara'](../../strongs/h/h7121.md) [nāḏîḇ](../../strongs/h/h5081.md), nor the [kîlay](../../strongs/h/h3596.md) ['āmar](../../strongs/h/h559.md) to be [šôaʿ](../../strongs/h/h7771.md).

<a name="isaiah_32_6"></a>Isaiah 32:6

For the [nabal](../../strongs/h/h5036.md) will [dabar](../../strongs/h/h1696.md) [nᵊḇālâ](../../strongs/h/h5039.md), and his [leb](../../strongs/h/h3820.md) will ['asah](../../strongs/h/h6213.md) ['aven](../../strongs/h/h205.md), to ['asah](../../strongs/h/h6213.md) [ḥōnep̄](../../strongs/h/h2612.md), and to [dabar](../../strongs/h/h1696.md) [tôʿâ](../../strongs/h/h8442.md) against [Yĕhovah](../../strongs/h/h3068.md), to [rîq](../../strongs/h/h7324.md) the [nephesh](../../strongs/h/h5315.md) of the [rāʿēḇ](../../strongs/h/h7457.md), and he will cause the [mašqê](../../strongs/h/h4945.md) of the [ṣāmē'](../../strongs/h/h6771.md) to [ḥāsēr](../../strongs/h/h2637.md).

<a name="isaiah_32_7"></a>Isaiah 32:7

The [kĕliy](../../strongs/h/h3627.md) also of the [kîlay](../../strongs/h/h3596.md) are [ra'](../../strongs/h/h7451.md): he [ya'ats](../../strongs/h/h3289.md) [zimmâ](../../strongs/h/h2154.md) to [chabal](../../strongs/h/h2254.md) the ['aniy](../../strongs/h/h6041.md) ['anav](../../strongs/h/h6035.md) with [sheqer](../../strongs/h/h8267.md) ['emer](../../strongs/h/h561.md), even when the ['ebyown](../../strongs/h/h34.md) [dabar](../../strongs/h/h1696.md) [mishpat](../../strongs/h/h4941.md).

<a name="isaiah_32_8"></a>Isaiah 32:8

But the [nāḏîḇ](../../strongs/h/h5081.md) [ya'ats](../../strongs/h/h3289.md) [nāḏîḇ](../../strongs/h/h5081.md); and by [nāḏîḇ](../../strongs/h/h5081.md) shall he [quwm](../../strongs/h/h6965.md).

<a name="isaiah_32_9"></a>Isaiah 32:9

[quwm](../../strongs/h/h6965.md), ye ['ishshah](../../strongs/h/h802.md) that are at [ša'ănān](../../strongs/h/h7600.md); [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), ye [batach](../../strongs/h/h982.md) [bath](../../strongs/h/h1323.md); ['azan](../../strongs/h/h238.md) unto my ['imrah](../../strongs/h/h565.md).

<a name="isaiah_32_10"></a>Isaiah 32:10

Many [yowm](../../strongs/h/h3117.md) and [šānâ](../../strongs/h/h8141.md) shall ye be [ragaz](../../strongs/h/h7264.md), ye [batach](../../strongs/h/h982.md): for the [bāṣîr](../../strongs/h/h1210.md) shall [kalah](../../strongs/h/h3615.md), the ['ōsep̄](../../strongs/h/h625.md) shall not come.

<a name="isaiah_32_11"></a>Isaiah 32:11

[ḥārēḏ](../../strongs/h/h2729.md), ye [ša'ănān](../../strongs/h/h7600.md); be [ragaz](../../strongs/h/h7264.md), ye [batach](../../strongs/h/h982.md): [pāšaṭ](../../strongs/h/h6584.md) you, and [ʿārar](../../strongs/h/h6209.md), and [chagowr](../../strongs/h/h2290.md) sackcloth upon your [ḥālāṣ](../../strongs/h/h2504.md).

<a name="isaiah_32_12"></a>Isaiah 32:12

They shall [sāp̄aḏ](../../strongs/h/h5594.md) for the [šaḏ](../../strongs/h/h7699.md), for the [ḥemeḏ](../../strongs/h/h2531.md) [sadeh](../../strongs/h/h7704.md), for the [parah](../../strongs/h/h6509.md) [gep̄en](../../strongs/h/h1612.md).

<a name="isaiah_32_13"></a>Isaiah 32:13

Upon the ['ăḏāmâ](../../strongs/h/h127.md) of my ['am](../../strongs/h/h5971.md) shall [ʿālâ](../../strongs/h/h5927.md) [qowts](../../strongs/h/h6975.md) and [šāmîr](../../strongs/h/h8068.md); yea, upon all the [bayith](../../strongs/h/h1004.md) of [māśôś](../../strongs/h/h4885.md) in the [ʿallîz](../../strongs/h/h5947.md) [qiryâ](../../strongs/h/h7151.md):

<a name="isaiah_32_14"></a>Isaiah 32:14

Because the ['armôn](../../strongs/h/h759.md) shall be [nāṭaš](../../strongs/h/h5203.md); the [hāmôn](../../strongs/h/h1995.md) of the [ʿîr](../../strongs/h/h5892.md) shall be ['azab](../../strongs/h/h5800.md); the [ʿōp̄el](../../strongs/h/h6076.md) and [baḥan](../../strongs/h/h975.md) shall be for [mᵊʿārâ](../../strongs/h/h4631.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md), a [māśôś](../../strongs/h/h4885.md) of [pere'](../../strongs/h/h6501.md), a [mirʿê](../../strongs/h/h4829.md) of [ʿēḏer](../../strongs/h/h5739.md);

<a name="isaiah_32_15"></a>Isaiah 32:15

Until the [ruwach](../../strongs/h/h7307.md) be [ʿārâ](../../strongs/h/h6168.md) upon us from [marowm](../../strongs/h/h4791.md), and the [midbar](../../strongs/h/h4057.md) be a [karmel](../../strongs/h/h3759.md), and the [karmel](../../strongs/h/h3759.md) be [chashab](../../strongs/h/h2803.md) for a [yaʿar](../../strongs/h/h3293.md).

<a name="isaiah_32_16"></a>Isaiah 32:16

Then [mishpat](../../strongs/h/h4941.md) shall [shakan](../../strongs/h/h7931.md) in the [midbar](../../strongs/h/h4057.md), and [ṣĕdāqāh](../../strongs/h/h6666.md) [yashab](../../strongs/h/h3427.md) in the [karmel](../../strongs/h/h3759.md).

<a name="isaiah_32_17"></a>Isaiah 32:17

And the [ma'aseh](../../strongs/h/h4639.md) of [ṣĕdāqāh](../../strongs/h/h6666.md) shall be [shalowm](../../strongs/h/h7965.md); and the effect of [ṣĕdāqāh](../../strongs/h/h6666.md) [šāqaṭ](../../strongs/h/h8252.md) and [betach](../../strongs/h/h983.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="isaiah_32_18"></a>Isaiah 32:18

And my ['am](../../strongs/h/h5971.md) shall [yashab](../../strongs/h/h3427.md) in a [shalowm](../../strongs/h/h7965.md) [nāvê](../../strongs/h/h5116.md), and in [miḇṭāḥ](../../strongs/h/h4009.md) [miškān](../../strongs/h/h4908.md), and in [ša'ănān](../../strongs/h/h7600.md) [mᵊnûḥâ](../../strongs/h/h4496.md);

<a name="isaiah_32_19"></a>Isaiah 32:19

When it shall [bāraḏ](../../strongs/h/h1258.md), [yarad](../../strongs/h/h3381.md) on the [yaʿar](../../strongs/h/h3293.md); and the [ʿîr](../../strongs/h/h5892.md) shall be [šāp̄ēl](../../strongs/h/h8213.md) in a [šip̄lâ](../../strongs/h/h8218.md).

<a name="isaiah_32_20"></a>Isaiah 32:20

['esher](../../strongs/h/h835.md) are ye that [zāraʿ](../../strongs/h/h2232.md) beside all [mayim](../../strongs/h/h4325.md), that [shalach](../../strongs/h/h7971.md) thither the [regel](../../strongs/h/h7272.md) of the [showr](../../strongs/h/h7794.md) and the [chamowr](../../strongs/h/h2543.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 31](isaiah_31.md) - [Isaiah 33](isaiah_33.md)