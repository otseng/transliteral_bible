# [Isaiah 54](https://www.blueletterbible.org/kjv/isa/54/1/s_733001)

<a name="isaiah_54_1"></a>Isaiah 54:1

[ranan](../../strongs/h/h7442.md), O [ʿāqār](../../strongs/h/h6135.md), thou that didst not [yalad](../../strongs/h/h3205.md); [pāṣaḥ](../../strongs/h/h6476.md) into [rinnah](../../strongs/h/h7440.md), and [ṣāhal](../../strongs/h/h6670.md), thou that didst not [chuwl](../../strongs/h/h2342.md): for more are the [ben](../../strongs/h/h1121.md) of the [šāmēm](../../strongs/h/h8074.md) than the [ben](../../strongs/h/h1121.md) of the [bāʿal](../../strongs/h/h1166.md), saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_54_2"></a>Isaiah 54:2

[rāḥaḇ](../../strongs/h/h7337.md) the [maqowm](../../strongs/h/h4725.md) of thy ['ohel](../../strongs/h/h168.md), and let them [natah](../../strongs/h/h5186.md) the [yᵊrîʿâ](../../strongs/h/h3407.md) of thine [miškān](../../strongs/h/h4908.md): [ḥāśaḵ](../../strongs/h/h2820.md) not, ['arak](../../strongs/h/h748.md) thy [mêṯār](../../strongs/h/h4340.md), and [ḥāzaq](../../strongs/h/h2388.md) thy [yāṯēḏ](../../strongs/h/h3489.md);

<a name="isaiah_54_3"></a>Isaiah 54:3

For thou shalt [pāraṣ](../../strongs/h/h6555.md) on the [yamiyn](../../strongs/h/h3225.md) and on the [śᵊmō'l](../../strongs/h/h8040.md); and thy [zera'](../../strongs/h/h2233.md) shall [yarash](../../strongs/h/h3423.md) the [gowy](../../strongs/h/h1471.md), and make the [šāmēm](../../strongs/h/h8074.md) [ʿîr](../../strongs/h/h5892.md) to be [yashab](../../strongs/h/h3427.md).

<a name="isaiah_54_4"></a>Isaiah 54:4

[yare'](../../strongs/h/h3372.md) not; for thou shalt not be [buwsh](../../strongs/h/h954.md): neither be thou [kālam](../../strongs/h/h3637.md); for thou shalt not be put to [ḥāp̄ēr](../../strongs/h/h2659.md): for thou shalt [shakach](../../strongs/h/h7911.md) the [bšeṯ](../../strongs/h/h1322.md) of thy [ʿălûmîm](../../strongs/h/h5934.md), and shalt not [zakar](../../strongs/h/h2142.md) the [cherpah](../../strongs/h/h2781.md) of thy ['almānûṯ](../../strongs/h/h491.md) any more.

<a name="isaiah_54_5"></a>Isaiah 54:5

For thy ['asah](../../strongs/h/h6213.md) is thine [bāʿal](../../strongs/h/h1166.md); [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md); and thy [gā'al](../../strongs/h/h1350.md) the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md); The ['Elohiym](../../strongs/h/h430.md) of the ['erets](../../strongs/h/h776.md) shall he be [qara'](../../strongs/h/h7121.md).

<a name="isaiah_54_6"></a>Isaiah 54:6

For [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) thee as an ['ishshah](../../strongs/h/h802.md) ['azab](../../strongs/h/h5800.md) and [ʿāṣaḇ](../../strongs/h/h6087.md) in [ruwach](../../strongs/h/h7307.md), and an ['ishshah](../../strongs/h/h802.md) of [nāʿur](../../strongs/h/h5271.md), when thou wast [mā'as](../../strongs/h/h3988.md), saith thy ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_54_7"></a>Isaiah 54:7

For a [qāṭān](../../strongs/h/h6996.md) [reḡaʿ](../../strongs/h/h7281.md) have I ['azab](../../strongs/h/h5800.md) thee; but with [gadowl](../../strongs/h/h1419.md) [raḥam](../../strongs/h/h7356.md) will I gather thee.

<a name="isaiah_54_8"></a>Isaiah 54:8

In a [šeṣep̄](../../strongs/h/h8241.md) [qeṣep̄](../../strongs/h/h7110.md) I [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) from thee for a [reḡaʿ](../../strongs/h/h7281.md); but with ['owlam](../../strongs/h/h5769.md) [checed](../../strongs/h/h2617.md) will I have [racham](../../strongs/h/h7355.md) on thee, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) thy [gā'al](../../strongs/h/h1350.md).

<a name="isaiah_54_9"></a>Isaiah 54:9

For this is as the [mayim](../../strongs/h/h4325.md) of [Nōaḥ](../../strongs/h/h5146.md) unto me: for as I have [shaba'](../../strongs/h/h7650.md) that the [mayim](../../strongs/h/h4325.md) of [Nōaḥ](../../strongs/h/h5146.md) should no more ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md); so have I [shaba'](../../strongs/h/h7650.md) that I would not be [qāṣap̄](../../strongs/h/h7107.md) with thee, nor [gāʿar](../../strongs/h/h1605.md) thee.

<a name="isaiah_54_10"></a>Isaiah 54:10

For the [har](../../strongs/h/h2022.md) shall [mûš](../../strongs/h/h4185.md), and the [giḇʿâ](../../strongs/h/h1389.md) be [mowt](../../strongs/h/h4131.md); but my [checed](../../strongs/h/h2617.md) shall not [mûš](../../strongs/h/h4185.md) from thee, neither shall the [bĕriyth](../../strongs/h/h1285.md) of my [shalowm](../../strongs/h/h7965.md) be [mowt](../../strongs/h/h4131.md), saith [Yĕhovah](../../strongs/h/h3068.md) that hath [racham](../../strongs/h/h7355.md) on thee.

<a name="isaiah_54_11"></a>Isaiah 54:11

O thou ['aniy](../../strongs/h/h6041.md), [sāʿar](../../strongs/h/h5590.md), and not [nacham](../../strongs/h/h5162.md), behold, I will [rāḇaṣ](../../strongs/h/h7257.md) thy ['eben](../../strongs/h/h68.md) with [pûḵ](../../strongs/h/h6320.md), and lay thy [yacad](../../strongs/h/h3245.md) with [sapîr](../../strongs/h/h5601.md).

<a name="isaiah_54_12"></a>Isaiah 54:12

And I will [śûm](../../strongs/h/h7760.md) thy [šemeš](../../strongs/h/h8121.md) of [kaḏkōḏ](../../strongs/h/h3539.md), and thy [sha'ar](../../strongs/h/h8179.md) of ['eben](../../strongs/h/h68.md) ['eqdāḥ](../../strongs/h/h688.md), and all thy [gᵊḇûl](../../strongs/h/h1366.md) of [chephets](../../strongs/h/h2656.md) ['eben](../../strongs/h/h68.md).

<a name="isaiah_54_13"></a>Isaiah 54:13

And all thy [ben](../../strongs/h/h1121.md) shall be [limmûḏ](../../strongs/h/h3928.md) of [Yĕhovah](../../strongs/h/h3068.md); and [rab](../../strongs/h/h7227.md) shall be the [shalowm](../../strongs/h/h7965.md) of thy [ben](../../strongs/h/h1121.md).

<a name="isaiah_54_14"></a>Isaiah 54:14

In [ṣĕdāqāh](../../strongs/h/h6666.md) shalt thou be [kuwn](../../strongs/h/h3559.md): thou shalt be [rachaq](../../strongs/h/h7368.md) from [ʿōšeq](../../strongs/h/h6233.md); for thou shalt not [yare'](../../strongs/h/h3372.md): and from [mᵊḥitâ](../../strongs/h/h4288.md); for it shall not come near thee.

<a name="isaiah_54_15"></a>Isaiah 54:15

Behold, they shall [guwr](../../strongs/h/h1481.md) [guwr](../../strongs/h/h1481.md), but not by me: whosoever shall [guwr](../../strongs/h/h1481.md) against thee shall [naphal](../../strongs/h/h5307.md) for thy sake.

<a name="isaiah_54_16"></a>Isaiah 54:16

Behold, I have [bara'](../../strongs/h/h1254.md) the [ḥārāš](../../strongs/h/h2796.md) that [nāp̄aḥ](../../strongs/h/h5301.md) the [peḥām](../../strongs/h/h6352.md) in the ['esh](../../strongs/h/h784.md), and that bringeth forth a [kĕliy](../../strongs/h/h3627.md) for his [ma'aseh](../../strongs/h/h4639.md); and I have [bara'](../../strongs/h/h1254.md) the [shachath](../../strongs/h/h7843.md) to [chabal](../../strongs/h/h2254.md).

<a name="isaiah_54_17"></a>Isaiah 54:17

No [kĕliy](../../strongs/h/h3627.md) that is [yāṣar](../../strongs/h/h3335.md) against thee shall [tsalach](../../strongs/h/h6743.md); and every [lashown](../../strongs/h/h3956.md) that shall [quwm](../../strongs/h/h6965.md) against thee in [mishpat](../../strongs/h/h4941.md) thou shalt [rāšaʿ](../../strongs/h/h7561.md). This is the [nachalah](../../strongs/h/h5159.md) of the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), and their [ṣĕdāqāh](../../strongs/h/h6666.md) is of me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 53](isaiah_53.md) - [Isaiah 55](isaiah_55.md)