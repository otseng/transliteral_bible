# [Isaiah 20](https://www.blueletterbible.org/kjv/isa/20/1/s_699001)

<a name="isaiah_20_1"></a>Isaiah 20:1

In the [šānâ](../../strongs/h/h8141.md) that [tartān](../../strongs/h/h8661.md) [bow'](../../strongs/h/h935.md) unto ['Ašdôḏ](../../strongs/h/h795.md), (when [sargôn](../../strongs/h/h5623.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [shalach](../../strongs/h/h7971.md) him,) and fought against ['Ašdôḏ](../../strongs/h/h795.md), and [lāḵaḏ](../../strongs/h/h3920.md) it;

<a name="isaiah_20_2"></a>Isaiah 20:2

At the same [ʿēṯ](../../strongs/h/h6256.md) [dabar](../../strongs/h/h1696.md) [Yĕhovah](../../strongs/h/h3068.md) by [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [pāṯaḥ](../../strongs/h/h6605.md) the [śaq](../../strongs/h/h8242.md) from off thy [māṯnayim](../../strongs/h/h4975.md), and [chalats](../../strongs/h/h2502.md) thy [naʿal](../../strongs/h/h5275.md) from thy [regel](../../strongs/h/h7272.md). And he ['asah](../../strongs/h/h6213.md) so, [halak](../../strongs/h/h1980.md) ['arowm](../../strongs/h/h6174.md) and [yāḥēp̄](../../strongs/h/h3182.md).

<a name="isaiah_20_3"></a>Isaiah 20:3

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Like as my ['ebed](../../strongs/h/h5650.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) hath [halak](../../strongs/h/h1980.md) ['arowm](../../strongs/h/h6174.md) and [yāḥēp̄](../../strongs/h/h3182.md) three [šānâ](../../strongs/h/h8141.md) for an ['ôṯ](../../strongs/h/h226.md) and [môp̄ēṯ](../../strongs/h/h4159.md) upon [Mitsrayim](../../strongs/h/h4714.md) and upon [Kûš](../../strongs/h/h3568.md);

<a name="isaiah_20_4"></a>Isaiah 20:4

So shall the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [nāhaḡ](../../strongs/h/h5090.md) the [Mitsrayim](../../strongs/h/h4714.md) [šᵊḇî](../../strongs/h/h7628.md), and the [Kûš](../../strongs/h/h3568.md) [gālûṯ](../../strongs/h/h1546.md), [naʿar](../../strongs/h/h5288.md) and [zāqēn](../../strongs/h/h2205.md), ['arowm](../../strongs/h/h6174.md) and [yāḥēp̄](../../strongs/h/h3182.md), even with their [šēṯ](../../strongs/h/h8357.md) uncovered, to the [ʿervâ](../../strongs/h/h6172.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="isaiah_20_5"></a>Isaiah 20:5

And they shall be [ḥāṯaṯ](../../strongs/h/h2865.md) and [buwsh](../../strongs/h/h954.md) of [Kûš](../../strongs/h/h3568.md) their [mabāṭ](../../strongs/h/h4007.md), and of [Mitsrayim](../../strongs/h/h4714.md) their [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="isaiah_20_6"></a>Isaiah 20:6

And the [yashab](../../strongs/h/h3427.md) of this ['î](../../strongs/h/h339.md) shall ['āmar](../../strongs/h/h559.md) in that [yowm](../../strongs/h/h3117.md), Behold, such is our [mabāṭ](../../strongs/h/h4007.md), whither we [nûs](../../strongs/h/h5127.md) for [ʿezrâ](../../strongs/h/h5833.md) to be [natsal](../../strongs/h/h5337.md) from the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md): and how shall we [mālaṭ](../../strongs/h/h4422.md)?

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 19](isaiah_19.md) - [Isaiah 21](isaiah_21.md)