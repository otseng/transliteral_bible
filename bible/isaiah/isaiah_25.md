# [Isaiah 25](https://www.blueletterbible.org/kjv/isa/25/1/s_704001)

<a name="isaiah_25_1"></a>Isaiah 25:1

[Yĕhovah](../../strongs/h/h3068.md), thou art my ['Elohiym](../../strongs/h/h430.md); I will [ruwm](../../strongs/h/h7311.md) thee, I will [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md); for thou hast ['asah](../../strongs/h/h6213.md) [pele'](../../strongs/h/h6382.md) things; thy ['etsah](../../strongs/h/h6098.md) of [rachowq](../../strongs/h/h7350.md) are ['ĕmûnâ](../../strongs/h/h530.md) and ['ōmen](../../strongs/h/h544.md).

<a name="isaiah_25_2"></a>Isaiah 25:2

For thou hast [śûm](../../strongs/h/h7760.md) of an [ʿîr](../../strongs/h/h5892.md) a [gal](../../strongs/h/h1530.md); of a [bāṣar](../../strongs/h/h1219.md) [qiryâ](../../strongs/h/h7151.md) a [mapālâ](../../strongs/h/h4654.md): an ['armôn](../../strongs/h/h759.md) of [zûr](../../strongs/h/h2114.md) to be no [ʿîr](../../strongs/h/h5892.md); it shall never be [bānâ](../../strongs/h/h1129.md).

<a name="isaiah_25_3"></a>Isaiah 25:3

Therefore shall the ['az](../../strongs/h/h5794.md) ['am](../../strongs/h/h5971.md) [kabad](../../strongs/h/h3513.md) thee, the [qiryâ](../../strongs/h/h7151.md) of the [ʿārîṣ](../../strongs/h/h6184.md) [gowy](../../strongs/h/h1471.md) shall [yare'](../../strongs/h/h3372.md) thee.

<a name="isaiah_25_4"></a>Isaiah 25:4

For thou hast been a [māʿôz](../../strongs/h/h4581.md) to the [dal](../../strongs/h/h1800.md), a [māʿôz](../../strongs/h/h4581.md) to the ['ebyown](../../strongs/h/h34.md) in his [tsar](../../strongs/h/h6862.md), a [machaceh](../../strongs/h/h4268.md) from the [zerem](../../strongs/h/h2230.md), a [ṣēl](../../strongs/h/h6738.md) from the [ḥōreḇ](../../strongs/h/h2721.md), when the [ruwach](../../strongs/h/h7307.md) of the [ʿārîṣ](../../strongs/h/h6184.md) is as a [zerem](../../strongs/h/h2230.md) against the [qîr](../../strongs/h/h7023.md).

<a name="isaiah_25_5"></a>Isaiah 25:5

Thou shalt [kānaʿ](../../strongs/h/h3665.md) the [shā'ôn](../../strongs/h/h7588.md) of [zûr](../../strongs/h/h2114.md), as the [ḥōreḇ](../../strongs/h/h2721.md) in a [ṣāyôn](../../strongs/h/h6724.md); even the [ḥōreḇ](../../strongs/h/h2721.md) with the [ṣēl](../../strongs/h/h6738.md) of an ['ab](../../strongs/h/h5645.md): the [zāmîr](../../strongs/h/h2159.md) of the [ʿārîṣ](../../strongs/h/h6184.md) shall be ['anah](../../strongs/h/h6030.md).

<a name="isaiah_25_6"></a>Isaiah 25:6

And in this [har](../../strongs/h/h2022.md) shall [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) make unto all ['am](../../strongs/h/h5971.md) a [mištê](../../strongs/h/h4960.md) of [šemen](../../strongs/h/h8081.md), a [mištê](../../strongs/h/h4960.md) of [šemer](../../strongs/h/h8105.md), of [šemen](../../strongs/h/h8081.md) full of [māḥâ](../../strongs/h/h4229.md), of [šemer](../../strongs/h/h8105.md) [zaqaq](../../strongs/h/h2212.md).

<a name="isaiah_25_7"></a>Isaiah 25:7

And he will [bālaʿ](../../strongs/h/h1104.md) in this [har](../../strongs/h/h2022.md) the [paniym](../../strongs/h/h6440.md) of the [Lôṭ](../../strongs/h/h3875.md) [lûṭ](../../strongs/h/h3874.md) all ['am](../../strongs/h/h5971.md), and the [massēḵâ](../../strongs/h/h4541.md) that is [nāsaḵ](../../strongs/h/h5259.md) over all [gowy](../../strongs/h/h1471.md).

<a name="isaiah_25_8"></a>Isaiah 25:8

He will [bālaʿ](../../strongs/h/h1104.md) [maveth](../../strongs/h/h4194.md) in [netsach](../../strongs/h/h5331.md); and ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will [māḥâ](../../strongs/h/h4229.md) [dim'ah](../../strongs/h/h1832.md) from off all [paniym](../../strongs/h/h6440.md); and the [cherpah](../../strongs/h/h2781.md) of his ['am](../../strongs/h/h5971.md) shall he take away from off all the ['erets](../../strongs/h/h776.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="isaiah_25_9"></a>Isaiah 25:9

And it shall be ['āmar](../../strongs/h/h559.md) in that [yowm](../../strongs/h/h3117.md), Lo, this is our ['Elohiym](../../strongs/h/h430.md); we have [qāvâ](../../strongs/h/h6960.md) for him, and he will [yasha'](../../strongs/h/h3467.md) us: this is [Yĕhovah](../../strongs/h/h3068.md); we have [qāvâ](../../strongs/h/h6960.md) for him, we will be [giyl](../../strongs/h/h1523.md) and [samach](../../strongs/h/h8055.md) in his [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="isaiah_25_10"></a>Isaiah 25:10

For in this [har](../../strongs/h/h2022.md) shall the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) [nuwach](../../strongs/h/h5117.md), and [Mô'āḇ](../../strongs/h/h4124.md) shall be [dûš](../../strongs/h/h1758.md) under him, even as [maṯbēn](../../strongs/h/h4963.md) is [dûš](../../strongs/h/h1758.md) for the [maḏmēnâ](../../strongs/h/h4087.md) [mayim](../../strongs/h/h4325.md).

<a name="isaiah_25_11"></a>Isaiah 25:11

And he shall [pāraś](../../strongs/h/h6566.md) his [yad](../../strongs/h/h3027.md) in the midst of them, as he that [sachah](../../strongs/h/h7811.md) [pāraś](../../strongs/h/h6566.md) his hands to [sachah](../../strongs/h/h7811.md): and he shall [šāp̄ēl](../../strongs/h/h8213.md) their [ga'avah](../../strongs/h/h1346.md) together with the ['ārbâ](../../strongs/h/h698.md) of their [yad](../../strongs/h/h3027.md).

<a name="isaiah_25_12"></a>Isaiah 25:12

And the [miḇṣār](../../strongs/h/h4013.md) of the [misgab](../../strongs/h/h4869.md) of thy [ḥômâ](../../strongs/h/h2346.md) shall he [shachach](../../strongs/h/h7817.md), [šāp̄ēl](../../strongs/h/h8213.md), and bring to the ['erets](../../strongs/h/h776.md), even to the ['aphar](../../strongs/h/h6083.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 24](isaiah_24.md) - [Isaiah 26](isaiah_26.md)