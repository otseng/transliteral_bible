# [Isaiah 1](https://www.blueletterbible.org/kjv/isa/1/1/s_680001)

<a name="isaiah_1_1"></a>Isaiah 1:1

The [ḥāzôn](../../strongs/h/h2377.md) of [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md), which he [chazah](../../strongs/h/h2372.md) concerning [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) in the [yowm](../../strongs/h/h3117.md) of ['Uzziyah](../../strongs/h/h5818.md), [Yôṯām](../../strongs/h/h3147.md), ['Āḥāz](../../strongs/h/h271.md), and [Yᵊḥizqîyâ](../../strongs/h/h3169.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="isaiah_1_2"></a>Isaiah 1:2

[shama'](../../strongs/h/h8085.md), [shamayim](../../strongs/h/h8064.md), and ['azan](../../strongs/h/h238.md), ['erets](../../strongs/h/h776.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md), I have [gāḏal](../../strongs/h/h1431.md) and [ruwm](../../strongs/h/h7311.md) [ben](../../strongs/h/h1121.md), and they have [pāšaʿ](../../strongs/h/h6586.md) against me.

<a name="isaiah_1_3"></a>Isaiah 1:3

The [showr](../../strongs/h/h7794.md) [yada'](../../strongs/h/h3045.md) his [qānâ](../../strongs/h/h7069.md), and the [chamowr](../../strongs/h/h2543.md) his [baʿal](../../strongs/h/h1167.md) ['ēḇûs](../../strongs/h/h18.md): but [Yisra'el](../../strongs/h/h3478.md) doth not [yada'](../../strongs/h/h3045.md), my ['am](../../strongs/h/h5971.md) doth not [bîn](../../strongs/h/h995.md).

<a name="isaiah_1_4"></a>Isaiah 1:4

[hôy](../../strongs/h/h1945.md) [chata'](../../strongs/h/h2398.md) [gowy](../../strongs/h/h1471.md), ['am](../../strongs/h/h5971.md) [kāḇēḏ](../../strongs/h/h3515.md) with ['avon](../../strongs/h/h5771.md), a [zera'](../../strongs/h/h2233.md) of [ra'a'](../../strongs/h/h7489.md), [ben](../../strongs/h/h1121.md) that are [shachath](../../strongs/h/h7843.md): they have ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), they have [na'ats](../../strongs/h/h5006.md) the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) unto [na'ats](../../strongs/h/h5006.md), they are [zûr](../../strongs/h/h2114.md) ['āḥôr](../../strongs/h/h268.md).

<a name="isaiah_1_5"></a>Isaiah 1:5

Why should ye be [nakah](../../strongs/h/h5221.md) any more? ye will [sārâ](../../strongs/h/h5627.md) [yāsap̄](../../strongs/h/h3254.md): the [ro'sh](../../strongs/h/h7218.md) is [ḥŏlî](../../strongs/h/h2483.md), and the [lebab](../../strongs/h/h3824.md) [daûāy](../../strongs/h/h1742.md).

<a name="isaiah_1_6"></a>Isaiah 1:6

From the [kaph](../../strongs/h/h3709.md) of the [regel](../../strongs/h/h7272.md) even unto the [ro'sh](../../strongs/h/h7218.md) there is no [mᵊṯōm](../../strongs/h/h4974.md) in it; but [peṣaʿ](../../strongs/h/h6482.md), and [ḥabûrâ](../../strongs/h/h2250.md), and [ṭārî](../../strongs/h/h2961.md) [makâ](../../strongs/h/h4347.md): they have not been [zûr](../../strongs/h/h2115.md), neither [ḥāḇaš](../../strongs/h/h2280.md), neither [rāḵaḵ](../../strongs/h/h7401.md) with [šemen](../../strongs/h/h8081.md).

<a name="isaiah_1_7"></a>Isaiah 1:7

Your ['erets](../../strongs/h/h776.md) is [šᵊmāmâ](../../strongs/h/h8077.md), your [ʿîr](../../strongs/h/h5892.md) are [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md): your ['ăḏāmâ](../../strongs/h/h127.md), [zûr](../../strongs/h/h2114.md) ['akal](../../strongs/h/h398.md) it in your presence, and it is [šᵊmāmâ](../../strongs/h/h8077.md), as [mahpēḵâ](../../strongs/h/h4114.md) by [zûr](../../strongs/h/h2114.md).

<a name="isaiah_1_8"></a>Isaiah 1:8

And the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) is left as a [cukkah](../../strongs/h/h5521.md) in a [kerem](../../strongs/h/h3754.md), as a [mᵊlûnâ](../../strongs/h/h4412.md) in a [miqšâ](../../strongs/h/h4750.md), as a [nāṣar](../../strongs/h/h5341.md) [ʿîr](../../strongs/h/h5892.md).

<a name="isaiah_1_9"></a>Isaiah 1:9

Except [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) had [yāṯar](../../strongs/h/h3498.md) unto us a very [mᵊʿaṭ](../../strongs/h/h4592.md) [śārîḏ](../../strongs/h/h8300.md), we should have been as [Sᵊḏōm](../../strongs/h/h5467.md), and we should have been like unto [ʿĂmōrâ](../../strongs/h/h6017.md).

<a name="isaiah_1_10"></a>Isaiah 1:10

[shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye [qāṣîn](../../strongs/h/h7101.md) of [Sᵊḏōm](../../strongs/h/h5467.md); give ear unto the [towrah](../../strongs/h/h8451.md) of our ['Elohiym](../../strongs/h/h430.md), ye ['am](../../strongs/h/h5971.md) of [ʿĂmōrâ](../../strongs/h/h6017.md).

<a name="isaiah_1_11"></a>Isaiah 1:11

To what purpose is the [rōḇ](../../strongs/h/h7230.md) of your [zebach](../../strongs/h/h2077.md) unto me? saith [Yĕhovah](../../strongs/h/h3068.md): I am [sāׂbaʿ](../../strongs/h/h7646.md) of the [ʿōlâ](../../strongs/h/h5930.md) of ['ayil](../../strongs/h/h352.md), and the [cheleb](../../strongs/h/h2459.md) of [mᵊrî'](../../strongs/h/h4806.md); and I [ḥāp̄ēṣ](../../strongs/h/h2654.md) not in the [dam](../../strongs/h/h1818.md) of [par](../../strongs/h/h6499.md), or of [keḇeś](../../strongs/h/h3532.md), or of he [ʿatûḏ](../../strongs/h/h6260.md).

<a name="isaiah_1_12"></a>Isaiah 1:12

When ye [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) me, who hath [bāqaš](../../strongs/h/h1245.md) this at your [yad](../../strongs/h/h3027.md), to [rāmas](../../strongs/h/h7429.md) my [ḥāṣēr](../../strongs/h/h2691.md)?

<a name="isaiah_1_13"></a>Isaiah 1:13

[bow'](../../strongs/h/h935.md) no more [shav'](../../strongs/h/h7723.md) [minchah](../../strongs/h/h4503.md); [qᵊṭōreṯ](../../strongs/h/h7004.md) is a [tôʿēḇâ](../../strongs/h/h8441.md) unto me; the [ḥōḏeš](../../strongs/h/h2320.md) and [shabbath](../../strongs/h/h7676.md), the [qara'](../../strongs/h/h7121.md) of [miqrā'](../../strongs/h/h4744.md), I cannot [yakol](../../strongs/h/h3201.md); it is ['aven](../../strongs/h/h205.md), even the [ʿăṣārâ](../../strongs/h/h6116.md).

<a name="isaiah_1_14"></a>Isaiah 1:14

Your [ḥōḏeš](../../strongs/h/h2320.md) and your [môʿēḏ](../../strongs/h/h4150.md) my [nephesh](../../strongs/h/h5315.md) [sane'](../../strongs/h/h8130.md): they are a [ṭōraḥ](../../strongs/h/h2960.md) unto me; I am [lā'â](../../strongs/h/h3811.md) to [nasa'](../../strongs/h/h5375.md) them.

<a name="isaiah_1_15"></a>Isaiah 1:15

And when ye [pāraś](../../strongs/h/h6566.md) your [kaph](../../strongs/h/h3709.md), I will ['alam](../../strongs/h/h5956.md) mine ['ayin](../../strongs/h/h5869.md) from you: yea, when ye [rabah](../../strongs/h/h7235.md) [tĕphillah](../../strongs/h/h8605.md), I will not [shama'](../../strongs/h/h8085.md): your [yad](../../strongs/h/h3027.md) are [mālā'](../../strongs/h/h4390.md)l of [dam](../../strongs/h/h1818.md).

<a name="isaiah_1_16"></a>Isaiah 1:16

[rāḥaṣ](../../strongs/h/h7364.md) you, make you [zāḵâ](../../strongs/h/h2135.md); [cuwr](../../strongs/h/h5493.md) the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md) from before mine ['ayin](../../strongs/h/h5869.md); [ḥāḏal](../../strongs/h/h2308.md) to do [ra'a'](../../strongs/h/h7489.md);

<a name="isaiah_1_17"></a>Isaiah 1:17

[lamad](../../strongs/h/h3925.md) to do [yatab](../../strongs/h/h3190.md); [darash](../../strongs/h/h1875.md) [mishpat](../../strongs/h/h4941.md), ['āšar](../../strongs/h/h833.md) the [ḥāmôṣ](../../strongs/h/h2541.md), [shaphat](../../strongs/h/h8199.md) the [yathowm](../../strongs/h/h3490.md), [riyb](../../strongs/h/h7378.md) for the ['almānâ](../../strongs/h/h490.md).

<a name="isaiah_1_18"></a>Isaiah 1:18

[yālaḵ](../../strongs/h/h3212.md), and let us [yakach](../../strongs/h/h3198.md) together, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md): though your [ḥēṭĕ'](../../strongs/h/h2399.md) be as [šānî](../../strongs/h/h8144.md), they shall be as [lāḇan](../../strongs/h/h3835.md) as [šeleḡ](../../strongs/h/h7950.md); though they be ['āḏam](../../strongs/h/h119.md) like [tôlāʿ](../../strongs/h/h8438.md), they shall be as [ṣemer](../../strongs/h/h6785.md).

<a name="isaiah_1_19"></a>Isaiah 1:19

If ye be ['āḇâ](../../strongs/h/h14.md) and [shama'](../../strongs/h/h8085.md), ye shall ['akal](../../strongs/h/h398.md) the [ṭûḇ](../../strongs/h/h2898.md) of the ['erets](../../strongs/h/h776.md):

<a name="isaiah_1_20"></a>Isaiah 1:20

But if ye [mā'ēn](../../strongs/h/h3985.md) and [marah](../../strongs/h/h4784.md), ye shall be ['akal](../../strongs/h/h398.md) with the [chereb](../../strongs/h/h2719.md): for the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="isaiah_1_21"></a>Isaiah 1:21

How is the ['aman](../../strongs/h/h539.md) [qiryâ](../../strongs/h/h7151.md) become a [zānâ](../../strongs/h/h2181.md)! it was [mālē'](../../strongs/h/h4392.md) of [mishpat](../../strongs/h/h4941.md); [tsedeq](../../strongs/h/h6664.md) [lûn](../../strongs/h/h3885.md) in it; but now [ratsach](../../strongs/h/h7523.md).

<a name="isaiah_1_22"></a>Isaiah 1:22

Thy [keceph](../../strongs/h/h3701.md) is become [sîḡ](../../strongs/h/h5509.md), thy [sōḇe'](../../strongs/h/h5435.md) mixed with [mayim](../../strongs/h/h4325.md):

<a name="isaiah_1_23"></a>Isaiah 1:23

Thy [śar](../../strongs/h/h8269.md) are [sārar](../../strongs/h/h5637.md), and [ḥāḇēr](../../strongs/h/h2270.md) of [gannāḇ](../../strongs/h/h1590.md): every one ['ahab](../../strongs/h/h157.md) [shachad](../../strongs/h/h7810.md), and [radaph](../../strongs/h/h7291.md) after [šalmōnîm](../../strongs/h/h8021.md): they [shaphat](../../strongs/h/h8199.md) not the [yathowm](../../strongs/h/h3490.md), neither doth the [rîḇ](../../strongs/h/h7379.md) of the ['almānâ](../../strongs/h/h490.md) come unto them.

<a name="isaiah_1_24"></a>Isaiah 1:24

Therefore [nᵊ'um](../../strongs/h/h5002.md) ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['abiyr](../../strongs/h/h46.md) of [Yisra'el](../../strongs/h/h3478.md), [hôy](../../strongs/h/h1945.md), I will [nacham](../../strongs/h/h5162.md) me of mine [tsar](../../strongs/h/h6862.md), and [naqam](../../strongs/h/h5358.md) me of mine ['oyeb](../../strongs/h/h341.md):

<a name="isaiah_1_25"></a>Isaiah 1:25

And I will [shuwb](../../strongs/h/h7725.md) my [yad](../../strongs/h/h3027.md) upon thee, and purely [tsaraph](../../strongs/h/h6884.md) thy [sîḡ](../../strongs/h/h5509.md), and [cuwr](../../strongs/h/h5493.md) all thy [bᵊḏîl](../../strongs/h/h913.md):

<a name="isaiah_1_26"></a>Isaiah 1:26

And I will [shuwb](../../strongs/h/h7725.md) thy [shaphat](../../strongs/h/h8199.md) as at the [ri'šôn](../../strongs/h/h7223.md), and thy [ya'ats](../../strongs/h/h3289.md) as at the [tᵊḥillâ](../../strongs/h/h8462.md): afterward thou shalt be [qara'](../../strongs/h/h7121.md), The [ʿîr](../../strongs/h/h5892.md) of [tsedeq](../../strongs/h/h6664.md), the ['aman](../../strongs/h/h539.md) [qiryâ](../../strongs/h/h7151.md).

<a name="isaiah_1_27"></a>Isaiah 1:27

[Tsiyown](../../strongs/h/h6726.md) shall be [pāḏâ](../../strongs/h/h6299.md) with [mishpat](../../strongs/h/h4941.md), and her [shuwb](../../strongs/h/h7725.md) with [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_1_28"></a>Isaiah 1:28

And the [šeḇar](../../strongs/h/h7667.md) of the [pāšaʿ](../../strongs/h/h6586.md) and of the [chatta'](../../strongs/h/h2400.md) shall be [yaḥaḏ](../../strongs/h/h3162.md), and they that ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) shall be [kalah](../../strongs/h/h3615.md).

<a name="isaiah_1_29"></a>Isaiah 1:29

For they shall be [buwsh](../../strongs/h/h954.md) of the ['ayil](../../strongs/h/h352.md) which ye have [chamad](../../strongs/h/h2530.md), and ye shall be [ḥāp̄ēr](../../strongs/h/h2659.md) for the [gannâ](../../strongs/h/h1593.md) that ye have [bāḥar](../../strongs/h/h977.md).

<a name="isaiah_1_30"></a>Isaiah 1:30

For ye shall be as an ['ēlâ](../../strongs/h/h424.md) whose ['aleh](../../strongs/h/h5929.md) [nabel](../../strongs/h/h5034.md), and as a [gannâ](../../strongs/h/h1593.md) that hath no [mayim](../../strongs/h/h4325.md).

<a name="isaiah_1_31"></a>Isaiah 1:31

And the [ḥāsōn](../../strongs/h/h2634.md) shall be as [nᵊʿōreṯ](../../strongs/h/h5296.md), and the [pōʿal](../../strongs/h/h6467.md) of it as a [nîṣôṣ](../../strongs/h/h5213.md), and they shall both [bāʿar](../../strongs/h/h1197.md) together, and none shall [kāḇâ](../../strongs/h/h3518.md) them.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 2](isaiah_2.md)