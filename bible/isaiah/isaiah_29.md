# [Isaiah 29](https://www.blueletterbible.org/kjv/isa/29/1/s_708001)

<a name="isaiah_29_1"></a>Isaiah 29:1

[hôy](../../strongs/h/h1945.md) to ['ărî'ēl](../../strongs/h/h740.md), to ['ărî'ēl](../../strongs/h/h740.md), the [qiryâ](../../strongs/h/h7151.md) where [Dāviḏ](../../strongs/h/h1732.md) [ḥānâ](../../strongs/h/h2583.md)! [sāp̄â](../../strongs/h/h5595.md) ye [šānâ](../../strongs/h/h8141.md) to [šānâ](../../strongs/h/h8141.md); let them [naqaph](../../strongs/h/h5362.md) [ḥāḡ](../../strongs/h/h2282.md).

<a name="isaiah_29_2"></a>Isaiah 29:2

Yet I will [ṣûq](../../strongs/h/h6693.md) ['ărî'ēl](../../strongs/h/h740.md), and there shall be [ta'ănîyâ](../../strongs/h/h8386.md) and ['ănîâ](../../strongs/h/h592.md): and it shall be unto me as ['ărî'ēl](../../strongs/h/h740.md).

<a name="isaiah_29_3"></a>Isaiah 29:3

And I will [ḥānâ](../../strongs/h/h2583.md) against thee [dûr](../../strongs/h/h1754.md), and will [ṣûr](../../strongs/h/h6696.md) against thee with a [muṣṣāḇ](../../strongs/h/h4674.md), and I will [quwm](../../strongs/h/h6965.md) [mᵊṣûrâ](../../strongs/h/h4694.md) against thee.

<a name="isaiah_29_4"></a>Isaiah 29:4

And thou shalt be [šāp̄ēl](../../strongs/h/h8213.md), and shalt [dabar](../../strongs/h/h1696.md) out of the ['erets](../../strongs/h/h776.md), and thy ['imrah](../../strongs/h/h565.md) shall [shachach](../../strongs/h/h7817.md) out of the ['aphar](../../strongs/h/h6083.md), and thy [qowl](../../strongs/h/h6963.md) shall be, as of one that hath a ['ôḇ](../../strongs/h/h178.md), out of the ['erets](../../strongs/h/h776.md), and thy ['imrah](../../strongs/h/h565.md) shall [ṣāp̄ap̄](../../strongs/h/h6850.md) out of the ['aphar](../../strongs/h/h6083.md).

<a name="isaiah_29_5"></a>Isaiah 29:5

Moreover the [hāmôn](../../strongs/h/h1995.md) of thy [zûr](../../strongs/h/h2114.md) shall be like [daq](../../strongs/h/h1851.md) ['āḇāq](../../strongs/h/h80.md), and the [hāmôn](../../strongs/h/h1995.md) of the [ʿārîṣ](../../strongs/h/h6184.md) shall be as [mots](../../strongs/h/h4671.md) that ['abar](../../strongs/h/h5674.md): yea, it shall be at a [peṯaʿ](../../strongs/h/h6621.md) suddenly.

<a name="isaiah_29_6"></a>Isaiah 29:6

Thou shalt be [paqad](../../strongs/h/h6485.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) with [raʿam](../../strongs/h/h7482.md), and with [raʿaš](../../strongs/h/h7494.md), and [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), with [sûp̄â](../../strongs/h/h5492.md) and [saʿar](../../strongs/h/h5591.md), and the [lahaḇ](../../strongs/h/h3851.md) of ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md).

<a name="isaiah_29_7"></a>Isaiah 29:7

And the [hāmôn](../../strongs/h/h1995.md) of all the [gowy](../../strongs/h/h1471.md) that [ṣᵊḇā'](../../strongs/h/h6633.md) against ['ărî'ēl](../../strongs/h/h740.md), even all that [ṣāḇâ](../../strongs/h/h6638.md) against her and her [māṣôḏ](../../strongs/h/h4685.md), and that [ṣûq](../../strongs/h/h6693.md) her, shall be as a [ḥălôm](../../strongs/h/h2472.md) of a [layil](../../strongs/h/h3915.md) [ḥāzôn](../../strongs/h/h2377.md).

<a name="isaiah_29_8"></a>Isaiah 29:8

It shall even be as when a [rāʿēḇ](../../strongs/h/h7457.md)[ḥālam](../../strongs/h/h2492.md), and, behold, he ['akal](../../strongs/h/h398.md); but he [quwts](../../strongs/h/h6974.md), and his [nephesh](../../strongs/h/h5315.md) is [reyq](../../strongs/h/h7386.md): or as when a [ṣāmē'](../../strongs/h/h6771.md) [ḥālam](../../strongs/h/h2492.md), and, behold, he [šāṯâ](../../strongs/h/h8354.md); but he [quwts](../../strongs/h/h6974.md), and, behold, he is [ʿāyēp̄](../../strongs/h/h5889.md), and his [nephesh](../../strongs/h/h5315.md) hath [šāqaq](../../strongs/h/h8264.md): so shall the [hāmôn](../../strongs/h/h1995.md) of all the [gowy](../../strongs/h/h1471.md) be, that [ṣᵊḇā'](../../strongs/h/h6633.md) against [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_29_9"></a>Isaiah 29:9

[māhah](../../strongs/h/h4102.md) yourselves, and [tāmah](../../strongs/h/h8539.md); [šāʿaʿ](../../strongs/h/h8173.md), and [šāʿaʿ](../../strongs/h/h8173.md): they are [šāḵar](../../strongs/h/h7937.md), but not with [yayin](../../strongs/h/h3196.md); they [nûaʿ](../../strongs/h/h5128.md), but not with [šēḵār](../../strongs/h/h7941.md).

<a name="isaiah_29_10"></a>Isaiah 29:10

For [Yĕhovah](../../strongs/h/h3068.md) hath [nacak](../../strongs/h/h5258.md) upon you the [ruwach](../../strongs/h/h7307.md) of [tardēmâ](../../strongs/h/h8639.md), and hath [ʿāṣam](../../strongs/h/h6105.md) your ['ayin](../../strongs/h/h5869.md): the [nāḇî'](../../strongs/h/h5030.md) and your [ro'sh](../../strongs/h/h7218.md), the [ḥōzê](../../strongs/h/h2374.md) hath he [kāsâ](../../strongs/h/h3680.md).

<a name="isaiah_29_11"></a>Isaiah 29:11

And the [ḥāzûṯ](../../strongs/h/h2380.md) of all is become unto you as the [dabar](../../strongs/h/h1697.md) of a [sēp̄er](../../strongs/h/h5612.md) that is [ḥāṯam](../../strongs/h/h2856.md), which men [nathan](../../strongs/h/h5414.md) to one that is [yada'](../../strongs/h/h3045.md), saying, [qara'](../../strongs/h/h7121.md) this, I pray thee: and he saith, I cannot; for it is [ḥāṯam](../../strongs/h/h2856.md):

<a name="isaiah_29_12"></a>Isaiah 29:12

And the [sēp̄er](../../strongs/h/h5612.md) is [nathan](../../strongs/h/h5414.md) to him that is not [yada'](../../strongs/h/h3045.md), saying, [qara'](../../strongs/h/h7121.md) this, I pray thee: and he ['āmar](../../strongs/h/h559.md), I am not [yada'](../../strongs/h/h3045.md).

<a name="isaiah_29_13"></a>Isaiah 29:13

Wherefore ['adonay](../../strongs/h/h136.md) ['āmar](../../strongs/h/h559.md), Forasmuch as this ['am](../../strongs/h/h5971.md) [nāḡaš](../../strongs/h/h5066.md) me with their [peh](../../strongs/h/h6310.md), and with their [saphah](../../strongs/h/h8193.md) do [kabad](../../strongs/h/h3513.md) me, but have [rachaq](../../strongs/h/h7368.md) their [leb](../../strongs/h/h3820.md) far from me, and their [yir'ah](../../strongs/h/h3374.md) toward me is [lamad](../../strongs/h/h3925.md) by the [mitsvah](../../strongs/h/h4687.md) of ['enowsh](../../strongs/h/h582.md):

<a name="isaiah_29_14"></a>Isaiah 29:14

Therefore, behold, I will [yāsap̄](../../strongs/h/h3254.md) to do a [pala'](../../strongs/h/h6381.md) among this ['am](../../strongs/h/h5971.md), even a [pala'](../../strongs/h/h6381.md) and a [pele'](../../strongs/h/h6382.md): for the [ḥāḵmâ](../../strongs/h/h2451.md) of their [ḥāḵām](../../strongs/h/h2450.md) shall ['abad](../../strongs/h/h6.md), and the [bînâ](../../strongs/h/h998.md) of their [bîn](../../strongs/h/h995.md) men shall be [cathar](../../strongs/h/h5641.md).

<a name="isaiah_29_15"></a>Isaiah 29:15

[hôy](../../strongs/h/h1945.md) unto them that [ʿāmaq](../../strongs/h/h6009.md) to [cathar](../../strongs/h/h5641.md) their ['etsah](../../strongs/h/h6098.md) from [Yĕhovah](../../strongs/h/h3068.md), and their [ma'aseh](../../strongs/h/h4639.md) are in the [maḥšāḵ](../../strongs/h/h4285.md), and they ['āmar](../../strongs/h/h559.md), Who [ra'ah](../../strongs/h/h7200.md) us? and who [yada'](../../strongs/h/h3045.md) us?

<a name="isaiah_29_16"></a>Isaiah 29:16

Surely your turning of things [hōp̄eḵ](../../strongs/h/h2017.md) shall be [chashab](../../strongs/h/h2803.md) as the [yāṣar](../../strongs/h/h3335.md) [ḥōmer](../../strongs/h/h2563.md): for shall the [ma'aseh](../../strongs/h/h4639.md) say of him that ['asah](../../strongs/h/h6213.md) it, He ['asah](../../strongs/h/h6213.md) me not? or shall the thing [yetser](../../strongs/h/h3336.md) say of him that [yāṣar](../../strongs/h/h3335.md) it, He had no [bîn](../../strongs/h/h995.md)?

<a name="isaiah_29_17"></a>Isaiah 29:17

Is it not yet a [mizʿār](../../strongs/h/h4213.md) [mᵊʿaṭ](../../strongs/h/h4592.md), and [Lᵊḇānôn](../../strongs/h/h3844.md) shall be [shuwb](../../strongs/h/h7725.md) into a [karmel](../../strongs/h/h3759.md), and the [karmel](../../strongs/h/h3759.md) shall be [chashab](../../strongs/h/h2803.md) as a [yaʿar](../../strongs/h/h3293.md)?

<a name="isaiah_29_18"></a>Isaiah 29:18

And in that [yowm](../../strongs/h/h3117.md) shall the [ḥērēš](../../strongs/h/h2795.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md), and the ['ayin](../../strongs/h/h5869.md) of the [ʿiûēr](../../strongs/h/h5787.md) shall [ra'ah](../../strongs/h/h7200.md) out of ['ōp̄el](../../strongs/h/h652.md), and out of [choshek](../../strongs/h/h2822.md).

<a name="isaiah_29_19"></a>Isaiah 29:19

The ['anav](../../strongs/h/h6035.md) also shall [yāsap̄](../../strongs/h/h3254.md) their [simchah](../../strongs/h/h8057.md) in [Yĕhovah](../../strongs/h/h3068.md), and the ['ebyown](../../strongs/h/h34.md) among ['adam](../../strongs/h/h120.md) shall [giyl](../../strongs/h/h1523.md) in the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_29_20"></a>Isaiah 29:20

For the [ʿārîṣ](../../strongs/h/h6184.md) is ['āp̄as](../../strongs/h/h656.md), and the [luwts](../../strongs/h/h3887.md) is [kalah](../../strongs/h/h3615.md), and all that [šāqaḏ](../../strongs/h/h8245.md) for ['aven](../../strongs/h/h205.md) are [karath](../../strongs/h/h3772.md):

<a name="isaiah_29_21"></a>Isaiah 29:21

That make an ['adam](../../strongs/h/h120.md) a [chata'](../../strongs/h/h2398.md) for a [dabar](../../strongs/h/h1697.md), and [qôš](../../strongs/h/h6983.md) for him that [yakach](../../strongs/h/h3198.md) in the [sha'ar](../../strongs/h/h8179.md), and [natah](../../strongs/h/h5186.md) the [tsaddiyq](../../strongs/h/h6662.md) for a [tohuw](../../strongs/h/h8414.md).

<a name="isaiah_29_22"></a>Isaiah 29:22

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), who [pāḏâ](../../strongs/h/h6299.md) ['Abraham](../../strongs/h/h85.md), concerning the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), [Ya'aqob](../../strongs/h/h3290.md) shall not now be [buwsh](../../strongs/h/h954.md), neither shall his [paniym](../../strongs/h/h6440.md) now [ḥāvar](../../strongs/h/h2357.md).

<a name="isaiah_29_23"></a>Isaiah 29:23

But when he [ra'ah](../../strongs/h/h7200.md) his [yeleḏ](../../strongs/h/h3206.md), the [ma'aseh](../../strongs/h/h4639.md) of mine [yad](../../strongs/h/h3027.md), in the midst of him, they shall [qadash](../../strongs/h/h6942.md) my [shem](../../strongs/h/h8034.md), and [qadash](../../strongs/h/h6942.md) the [qadowsh](../../strongs/h/h6918.md) of [Ya'aqob](../../strongs/h/h3290.md), and shall [ʿāraṣ](../../strongs/h/h6206.md) the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_29_24"></a>Isaiah 29:24

They also that [tāʿâ](../../strongs/h/h8582.md) in [ruwach](../../strongs/h/h7307.md) shall [yada'](../../strongs/h/h3045.md) to [bînâ](../../strongs/h/h998.md), and they that [rāḡan](../../strongs/h/h7279.md) shall [lamad](../../strongs/h/h3925.md) [leqaḥ](../../strongs/h/h3948.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 28](isaiah_28.md) - [Isaiah 30](isaiah_30.md)