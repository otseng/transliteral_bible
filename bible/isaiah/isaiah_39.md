# [Isaiah 39](https://www.blueletterbible.org/kjv/isa/39/1/s_718001)

<a name="isaiah_39_1"></a>Isaiah 39:1

At that time [mᵊrō'ḏaḵ-bal'ăḏān](../../strongs/h/h4757.md), the [ben](../../strongs/h/h1121.md) of [Bal'Ăḏān](../../strongs/h/h1081.md), [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) and a [minchah](../../strongs/h/h4503.md) to [Ḥizqîyâ](../../strongs/h/h2396.md): for he had [shama'](../../strongs/h/h8085.md) that he had been [ḥālâ](../../strongs/h/h2470.md), and was [ḥāzaq](../../strongs/h/h2388.md).

<a name="isaiah_39_2"></a>Isaiah 39:2

And [Ḥizqîyâ](../../strongs/h/h2396.md) was [samach](../../strongs/h/h8055.md) of them, and [ra'ah](../../strongs/h/h7200.md) them the [bayith](../../strongs/h/h1004.md) of his [nᵊḵōṯ](../../strongs/h/h5238.md), the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [beśem](../../strongs/h/h1314.md), and the [towb](../../strongs/h/h2896.md) [šemen](../../strongs/h/h8081.md), and all the [bayith](../../strongs/h/h1004.md) of his [kĕliy](../../strongs/h/h3627.md), and all that was [māṣā'](../../strongs/h/h4672.md) in his ['ôṣār](../../strongs/h/h214.md): there was nothing in his [bayith](../../strongs/h/h1004.md), nor in all his [memshalah](../../strongs/h/h4475.md), that [Ḥizqîyâ](../../strongs/h/h2396.md) shewed them not.

<a name="isaiah_39_3"></a>Isaiah 39:3

Then [bow'](../../strongs/h/h935.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md) unto [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md), and ['āmar](../../strongs/h/h559.md) unto him, What ['āmar](../../strongs/h/h559.md) these ['enowsh](../../strongs/h/h582.md)? and from whence [bow'](../../strongs/h/h935.md) they unto thee? And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md), They are [bow'](../../strongs/h/h935.md) from a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md) unto me, even from [Bāḇel](../../strongs/h/h894.md).

<a name="isaiah_39_4"></a>Isaiah 39:4

Then ['āmar](../../strongs/h/h559.md) he, What have they [ra'ah](../../strongs/h/h7200.md) in thine [bayith](../../strongs/h/h1004.md)? And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md), All that is in mine [bayith](../../strongs/h/h1004.md) have they [ra'ah](../../strongs/h/h7200.md): there is nothing among my ['ôṣār](../../strongs/h/h214.md) that I have not [ra'ah](../../strongs/h/h7200.md) them.

<a name="isaiah_39_5"></a>Isaiah 39:5

Then ['āmar](../../strongs/h/h559.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) to [Ḥizqîyâ](../../strongs/h/h2396.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md):

<a name="isaiah_39_6"></a>Isaiah 39:6

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), that all that is in thine [bayith](../../strongs/h/h1004.md), and that which thy ['ab](../../strongs/h/h1.md) have laid up in ['āṣar](../../strongs/h/h686.md) until this [yowm](../../strongs/h/h3117.md), shall be [nasa'](../../strongs/h/h5375.md) to [Bāḇel](../../strongs/h/h894.md): nothing shall be left, saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_39_7"></a>Isaiah 39:7

And of thy [ben](../../strongs/h/h1121.md) that shall [yāṣā'](../../strongs/h/h3318.md) from thee, which thou shalt [yalad](../../strongs/h/h3205.md), shall they [laqach](../../strongs/h/h3947.md); and they shall be [sārîs](../../strongs/h/h5631.md) in the [heykal](../../strongs/h/h1964.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="isaiah_39_8"></a>Isaiah 39:8

Then ['āmar](../../strongs/h/h559.md) [Ḥizqîyâ](../../strongs/h/h2396.md) to [Yᵊšaʿyâ](../../strongs/h/h3470.md), [towb](../../strongs/h/h2896.md) is the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which thou hast [dabar](../../strongs/h/h1696.md). He ['āmar](../../strongs/h/h559.md) moreover, For there shall be [shalowm](../../strongs/h/h7965.md) and ['emeth](../../strongs/h/h571.md) in my [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 38](isaiah_38.md) - [Isaiah 40](isaiah_40.md)