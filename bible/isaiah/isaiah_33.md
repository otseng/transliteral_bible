# [Isaiah 33](https://www.blueletterbible.org/kjv/isa/33/1/s_712001)

<a name="isaiah_33_1"></a>Isaiah 33:1

[hôy](../../strongs/h/h1945.md) to thee that [shadad](../../strongs/h/h7703.md), and thou wast not [shadad](../../strongs/h/h7703.md); and [bāḡaḏ](../../strongs/h/h898.md), and they dealt not [bāḡaḏ](../../strongs/h/h898.md) with thee! when thou shalt  [tamam](../../strongs/h/h8552.md) to [shadad](../../strongs/h/h7703.md), thou shalt be [shadad](../../strongs/h/h7703.md); and when thou shalt [nālâ](../../strongs/h/h5239.md) to [bāḡaḏ](../../strongs/h/h898.md), they shall [bāḡaḏ](../../strongs/h/h898.md) with thee.

<a name="isaiah_33_2"></a>Isaiah 33:2

[Yĕhovah](../../strongs/h/h3068.md), be [chanan](../../strongs/h/h2603.md) unto us; we have [qāvâ](../../strongs/h/h6960.md) for thee: be thou their [zerowa'](../../strongs/h/h2220.md) every [boqer](../../strongs/h/h1242.md), our [yĕshuw'ah](../../strongs/h/h3444.md) also in the [ʿēṯ](../../strongs/h/h6256.md) of [tsarah](../../strongs/h/h6869.md).

<a name="isaiah_33_3"></a>Isaiah 33:3

At the [qowl](../../strongs/h/h6963.md) of the [hāmôn](../../strongs/h/h1995.md) the ['am](../../strongs/h/h5971.md) [nāḏaḏ](../../strongs/h/h5074.md); at the [rvmmṯ](../../strongs/h/h7427.md) of thyself the [gowy](../../strongs/h/h1471.md) were [naphats](../../strongs/h/h5310.md).

<a name="isaiah_33_4"></a>Isaiah 33:4

And your [šālāl](../../strongs/h/h7998.md) shall be ['āsap̄](../../strongs/h/h622.md) like the ['ōsep̄](../../strongs/h/h625.md) of the [ḥāsîl](../../strongs/h/h2625.md): as the [maššāq](../../strongs/h/h4944.md) of [gēḇ](../../strongs/h/h1357.md) shall he [šāqaq](../../strongs/h/h8264.md) upon them.

<a name="isaiah_33_5"></a>Isaiah 33:5

[Yĕhovah](../../strongs/h/h3068.md) is [śāḡaḇ](../../strongs/h/h7682.md); for he [shakan](../../strongs/h/h7931.md) on [marowm](../../strongs/h/h4791.md): he hath filled [Tsiyown](../../strongs/h/h6726.md) with [mishpat](../../strongs/h/h4941.md) and [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_33_6"></a>Isaiah 33:6

And [ḥāḵmâ](../../strongs/h/h2451.md) and [da'ath](../../strongs/h/h1847.md) shall be the ['ĕmûnâ](../../strongs/h/h530.md) of thy [ʿēṯ](../../strongs/h/h6256.md), and [ḥōsen](../../strongs/h/h2633.md) of [yĕshuw'ah](../../strongs/h/h3444.md): the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md) is his ['ôṣār](../../strongs/h/h214.md).

<a name="isaiah_33_7"></a>Isaiah 33:7

Behold, their ['er'ēl](../../strongs/h/h691.md) shall [ṣāʿaq](../../strongs/h/h6817.md) without: the [mal'ak](../../strongs/h/h4397.md) of [shalowm](../../strongs/h/h7965.md) shall [bāḵâ](../../strongs/h/h1058.md) [mar](../../strongs/h/h4751.md).

<a name="isaiah_33_8"></a>Isaiah 33:8

The [mĕcillah](../../strongs/h/h4546.md) [šāmēm](../../strongs/h/h8074.md), the ['abar](../../strongs/h/h5674.md) ['orach](../../strongs/h/h734.md) [shabath](../../strongs/h/h7673.md): he hath [pārar](../../strongs/h/h6565.md) the [bĕriyth](../../strongs/h/h1285.md), he hath [mā'as](../../strongs/h/h3988.md) the [ʿîr](../../strongs/h/h5892.md), he [chashab](../../strongs/h/h2803.md) no ['enowsh](../../strongs/h/h582.md).

<a name="isaiah_33_9"></a>Isaiah 33:9

The ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md) and ['āmal](../../strongs/h/h535.md): [Lᵊḇānôn](../../strongs/h/h3844.md) is [ḥāp̄ēr](../../strongs/h/h2659.md) and [qāmal](../../strongs/h/h7060.md): [Šārôn](../../strongs/h/h8289.md) is like an ['arabah](../../strongs/h/h6160.md); and [Bāšān](../../strongs/h/h1316.md) and [karmel](../../strongs/h/h3760.md) [nāʿar](../../strongs/h/h5287.md) their fruits.

<a name="isaiah_33_10"></a>Isaiah 33:10

Now will I [quwm](../../strongs/h/h6965.md), saith [Yĕhovah](../../strongs/h/h3068.md); now will I be [rāmam](../../strongs/h/h7426.md); now will I [nasa'](../../strongs/h/h5375.md) myself.

<a name="isaiah_33_11"></a>Isaiah 33:11

Ye shall [harah](../../strongs/h/h2029.md) [ḥăšaš](../../strongs/h/h2842.md), ye shall bring forth [qaš](../../strongs/h/h7179.md): your [ruwach](../../strongs/h/h7307.md), as ['esh](../../strongs/h/h784.md), shall ['akal](../../strongs/h/h398.md) you.

<a name="isaiah_33_12"></a>Isaiah 33:12

And the ['am](../../strongs/h/h5971.md) shall be as the [miśrāp̄ôṯ](../../strongs/h/h4955.md) of [śîḏ](../../strongs/h/h7875.md): as [qowts](../../strongs/h/h6975.md) [kāsaḥ](../../strongs/h/h3683.md) shall they be [yāṣaṯ](../../strongs/h/h3341.md) in the ['esh](../../strongs/h/h784.md).

<a name="isaiah_33_13"></a>Isaiah 33:13

[shama'](../../strongs/h/h8085.md), ye that are [rachowq](../../strongs/h/h7350.md), what I have ['asah](../../strongs/h/h6213.md); and, ye that are [qarowb](../../strongs/h/h7138.md), [yada'](../../strongs/h/h3045.md) my [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="isaiah_33_14"></a>Isaiah 33:14

The [chatta'](../../strongs/h/h2400.md) in [Tsiyown](../../strongs/h/h6726.md) are [pachad](../../strongs/h/h6342.md); [ra'ad](../../strongs/h/h7461.md) hath ['āḥaz](../../strongs/h/h270.md) the [ḥānēp̄](../../strongs/h/h2611.md). Who among us shall [guwr](../../strongs/h/h1481.md) with the ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md)? who among us shall [guwr](../../strongs/h/h1481.md) with ['owlam](../../strongs/h/h5769.md) [môqēḏ](../../strongs/h/h4168.md)?

<a name="isaiah_33_15"></a>Isaiah 33:15

He that [halak](../../strongs/h/h1980.md) [tsedaqah](../../strongs/h/h6666.md), and [dabar](../../strongs/h/h1696.md) [meyshar](../../strongs/h/h4339.md); he that [mā'as](../../strongs/h/h3988.md) the [beṣaʿ](../../strongs/h/h1215.md) of [maʿăšaqqâ](../../strongs/h/h4642.md), that [nāʿar](../../strongs/h/h5287.md) his [kaph](../../strongs/h/h3709.md) from [tamak](../../strongs/h/h8551.md) of [shachad](../../strongs/h/h7810.md), that ['āṭam](../../strongs/h/h331.md) his ['ozen](../../strongs/h/h241.md) from [shama'](../../strongs/h/h8085.md) of [dam](../../strongs/h/h1818.md), and [ʿāṣam](../../strongs/h/h6105.md) his ['ayin](../../strongs/h/h5869.md) from [ra'ah](../../strongs/h/h7200.md) [ra'](../../strongs/h/h7451.md);

<a name="isaiah_33_16"></a>Isaiah 33:16

He shall [shakan](../../strongs/h/h7931.md) on [marowm](../../strongs/h/h4791.md): his [misgab](../../strongs/h/h4869.md) shall be the [mᵊṣāḏ](../../strongs/h/h4679.md) of [cela'](../../strongs/h/h5553.md): [lechem](../../strongs/h/h3899.md) shall be [nathan](../../strongs/h/h5414.md) him; his [mayim](../../strongs/h/h4325.md) shall be ['aman](../../strongs/h/h539.md).

<a name="isaiah_33_17"></a>Isaiah 33:17

Thine ['ayin](../../strongs/h/h5869.md) shall [chazah](../../strongs/h/h2372.md) the [melek](../../strongs/h/h4428.md) in his [yᵊp̄î](../../strongs/h/h3308.md): they shall [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) that is very [merḥāq](../../strongs/h/h4801.md).

<a name="isaiah_33_18"></a>Isaiah 33:18

Thine [leb](../../strongs/h/h3820.md) shall [hagah](../../strongs/h/h1897.md) ['êmâ](../../strongs/h/h367.md). Where is the [sāp̄ar](../../strongs/h/h5608.md)? where is the [šāqal](../../strongs/h/h8254.md)? where is he that [sāp̄ar](../../strongs/h/h5608.md) the [miḡdāl](../../strongs/h/h4026.md)?

<a name="isaiah_33_19"></a>Isaiah 33:19

Thou shalt not see a [yāʿaz](../../strongs/h/h3267.md) ['am](../../strongs/h/h5971.md), an ['am](../../strongs/h/h5971.md) of a [ʿāmēq](../../strongs/h/h6012.md) [saphah](../../strongs/h/h8193.md) than thou canst [shama'](../../strongs/h/h8085.md); of a [lāʿaḡ](../../strongs/h/h3932.md) [lashown](../../strongs/h/h3956.md), that thou canst not [bînâ](../../strongs/h/h998.md).

<a name="isaiah_33_20"></a>Isaiah 33:20

[chazah](../../strongs/h/h2372.md) upon [Tsiyown](../../strongs/h/h6726.md), the [qiryâ](../../strongs/h/h7151.md) of our [môʿēḏ](../../strongs/h/h4150.md): thine ['ayin](../../strongs/h/h5869.md) shall see [Yĕruwshalaim](../../strongs/h/h3389.md) a [ša'ănān](../../strongs/h/h7600.md) [nāvê](../../strongs/h/h5116.md), a ['ohel](../../strongs/h/h168.md) that shall not be [ṣāʿan](../../strongs/h/h6813.md); not one of the [yāṯēḏ](../../strongs/h/h3489.md) thereof shall ever be [nāsaʿ](../../strongs/h/h5265.md), neither shall any of the [chebel](../../strongs/h/h2256.md) thereof be [nathaq](../../strongs/h/h5423.md).

<a name="isaiah_33_21"></a>Isaiah 33:21

But there the ['addiyr](../../strongs/h/h117.md) [Yĕhovah](../../strongs/h/h3068.md) will be unto us a [maqowm](../../strongs/h/h4725.md) of [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md) [nāhār](../../strongs/h/h5104.md) and [yᵊ'ōr](../../strongs/h/h2975.md); wherein shall go no ['ŏnî](../../strongs/h/h590.md) with [šayiṭ](../../strongs/h/h7885.md), neither shall ['addiyr](../../strongs/h/h117.md) [ṣî](../../strongs/h/h6716.md) pass thereby.

<a name="isaiah_33_22"></a>Isaiah 33:22

For [Yĕhovah](../../strongs/h/h3068.md) is our [shaphat](../../strongs/h/h8199.md), [Yĕhovah](../../strongs/h/h3068.md) is our [ḥāqaq](../../strongs/h/h2710.md), [Yĕhovah](../../strongs/h/h3068.md) is our [melek](../../strongs/h/h4428.md); he will [yasha'](../../strongs/h/h3467.md) us.

<a name="isaiah_33_23"></a>Isaiah 33:23

Thy [chebel](../../strongs/h/h2256.md) are [nāṭaš](../../strongs/h/h5203.md); they could not well [ḥāzaq](../../strongs/h/h2388.md) their [tōren](../../strongs/h/h8650.md), they could not [pāraś](../../strongs/h/h6566.md) the [nēs](../../strongs/h/h5251.md): then is the [ʿaḏ](../../strongs/h/h5706.md) of a [marbê](../../strongs/h/h4766.md) [šālāl](../../strongs/h/h7998.md) [chalaq](../../strongs/h/h2505.md); the [pissēaḥ](../../strongs/h/h6455.md) take the [baz](../../strongs/h/h957.md)d.

<a name="isaiah_33_24"></a>Isaiah 33:24

And the [šāḵēn](../../strongs/h/h7934.md) shall not ['āmar](../../strongs/h/h559.md), I am [ḥālâ](../../strongs/h/h2470.md): the ['am](../../strongs/h/h5971.md) that [yashab](../../strongs/h/h3427.md) therein shall be [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 32](isaiah_32.md) - [Isaiah 34](isaiah_34.md)