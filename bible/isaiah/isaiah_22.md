# [Isaiah 22](https://www.blueletterbible.org/kjv/isa/22/1/s_701001)

<a name="isaiah_22_1"></a>Isaiah 22:1

The [maśśā'](../../strongs/h/h4853.md) of the [gay'](../../strongs/h/h1516.md) of [ḥizzāyôn](../../strongs/h/h2384.md). What aileth thee now, that thou art [ʿālâ](../../strongs/h/h5927.md) to the [gāḡ](../../strongs/h/h1406.md)?

<a name="isaiah_22_2"></a>Isaiah 22:2

Thou that art [mālē'](../../strongs/h/h4392.md) of [tᵊšu'â](../../strongs/h/h8663.md), a [hāmâ](../../strongs/h/h1993.md) [ʿîr](../../strongs/h/h5892.md), [ʿallîz](../../strongs/h/h5947.md) [qiryâ](../../strongs/h/h7151.md): thy [ḥālāl](../../strongs/h/h2491.md) men are not [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md), nor [muwth](../../strongs/h/h4191.md) in [milḥāmâ](../../strongs/h/h4421.md).

<a name="isaiah_22_3"></a>Isaiah 22:3

All thy [qāṣîn](../../strongs/h/h7101.md) are [nāḏaḏ](../../strongs/h/h5074.md) [yaḥaḏ](../../strongs/h/h3162.md), they are ['āsar](../../strongs/h/h631.md) by the [qesheth](../../strongs/h/h7198.md): all that are [māṣā'](../../strongs/h/h4672.md) in thee are ['āsar](../../strongs/h/h631.md) together, which have [bāraḥ](../../strongs/h/h1272.md) from [rachowq](../../strongs/h/h7350.md).

<a name="isaiah_22_4"></a>Isaiah 22:4

Therefore ['āmar](../../strongs/h/h559.md) I, [šāʿâ](../../strongs/h/h8159.md) from me; I will [bĕkiy](../../strongs/h/h1065.md) [mārar](../../strongs/h/h4843.md), ['ûṣ](../../strongs/h/h213.md) not to [nacham](../../strongs/h/h5162.md) me, because of the [shod](../../strongs/h/h7701.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md).

<a name="isaiah_22_5"></a>Isaiah 22:5

For it is a [yowm](../../strongs/h/h3117.md) of [mᵊhûmâ](../../strongs/h/h4103.md), and of [mᵊḇûsâ](../../strongs/h/h4001.md), and of [mᵊḇûḵâ](../../strongs/h/h3998.md) by ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) in the [gay'](../../strongs/h/h1516.md) of [ḥizzāyôn](../../strongs/h/h2384.md), [qûr](../../strongs/h/h6979.md) the walls, and of [šôaʿ](../../strongs/h/h7771.md) to the [har](../../strongs/h/h2022.md).

<a name="isaiah_22_6"></a>Isaiah 22:6

And [ʿÊlām](../../strongs/h/h5867.md) [nasa'](../../strongs/h/h5375.md) the ['ašpâ](../../strongs/h/h827.md) with [reḵeḇ](../../strongs/h/h7393.md) of ['adam](../../strongs/h/h120.md) and [pārāš](../../strongs/h/h6571.md), and [qîr](../../strongs/h/h7024.md) [ʿārâ](../../strongs/h/h6168.md) the [magen](../../strongs/h/h4043.md).

<a name="isaiah_22_7"></a>Isaiah 22:7

And it shall come to pass, that thy [miḇḥār](../../strongs/h/h4005.md) [ʿēmeq](../../strongs/h/h6010.md) shall be [mālā'](../../strongs/h/h4390.md) of [reḵeḇ](../../strongs/h/h7393.md), and the [pārāš](../../strongs/h/h6571.md) shall [shiyth](../../strongs/h/h7896.md) themselves in array at the [sha'ar](../../strongs/h/h8179.md).

<a name="isaiah_22_8"></a>Isaiah 22:8

And he [gālâ](../../strongs/h/h1540.md) the [māsāḵ](../../strongs/h/h4539.md) of [Yehuwdah](../../strongs/h/h3063.md), and thou didst [nabat](../../strongs/h/h5027.md) in that [yowm](../../strongs/h/h3117.md) to the [nešeq](../../strongs/h/h5402.md) of the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md).

<a name="isaiah_22_9"></a>Isaiah 22:9

Ye have [ra'ah](../../strongs/h/h7200.md) also the [bᵊqîaʿ](../../strongs/h/h1233.md) of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), that they are [rabab](../../strongs/h/h7231.md): and ye [qāḇaṣ](../../strongs/h/h6908.md) the [mayim](../../strongs/h/h4325.md) of the [taḥtôn](../../strongs/h/h8481.md) [bᵊrēḵâ](../../strongs/h/h1295.md).

<a name="isaiah_22_10"></a>Isaiah 22:10

And ye have [sāp̄ar](../../strongs/h/h5608.md) the [bayith](../../strongs/h/h1004.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and the [bayith](../../strongs/h/h1004.md) have ye [nāṯaṣ](../../strongs/h/h5422.md) to [bāṣar](../../strongs/h/h1219.md) the [ḥômâ](../../strongs/h/h2346.md).

<a name="isaiah_22_11"></a>Isaiah 22:11

Ye ['asah](../../strongs/h/h6213.md) also a [miqvâ](../../strongs/h/h4724.md) between the two [ḥômâ](../../strongs/h/h2346.md) for the [mayim](../../strongs/h/h4325.md) of the [yāšān](../../strongs/h/h3465.md) [bᵊrēḵâ](../../strongs/h/h1295.md): but ye have not [nabat](../../strongs/h/h5027.md) unto the ['asah](../../strongs/h/h6213.md) thereof, neither had [ra'ah](../../strongs/h/h7200.md) unto him that [yāṣar](../../strongs/h/h3335.md) it long ago.

<a name="isaiah_22_12"></a>Isaiah 22:12

And in that [yowm](../../strongs/h/h3117.md) did ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) [qara'](../../strongs/h/h7121.md) to [bĕkiy](../../strongs/h/h1065.md), and to [mispēḏ](../../strongs/h/h4553.md), and to [qrḥ](../../strongs/h/h7144.md), and to [ḥāḡar](../../strongs/h/h2296.md) with [śaq](../../strongs/h/h8242.md):

<a name="isaiah_22_13"></a>Isaiah 22:13

And behold [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md), [harag](../../strongs/h/h2026.md) [bāqār](../../strongs/h/h1241.md), and [šāḥaṭ](../../strongs/h/h7819.md) [tso'n](../../strongs/h/h6629.md), ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md), and [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md): let us ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md); for [māḥār](../../strongs/h/h4279.md) we shall [muwth](../../strongs/h/h4191.md).

<a name="isaiah_22_14"></a>Isaiah 22:14

And it was [gālâ](../../strongs/h/h1540.md) in mine ['ozen](../../strongs/h/h241.md) by [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), Surely this ['avon](../../strongs/h/h5771.md) shall not be [kāp̄ar](../../strongs/h/h3722.md) from you till ye [muwth](../../strongs/h/h4191.md), saith ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_22_15"></a>Isaiah 22:15

Thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md), [yālaḵ](../../strongs/h/h3212.md), [bow'](../../strongs/h/h935.md) thee unto this [sāḵan](../../strongs/h/h5532.md), even unto [Šeḇnā'](../../strongs/h/h7644.md), which is over the [bayith](../../strongs/h/h1004.md),

<a name="isaiah_22_16"></a>Isaiah 22:16

What hast thou here? and whom hast thou here, that thou hast [ḥāṣaḇ](../../strongs/h/h2672.md) thee out a [qeber](../../strongs/h/h6913.md) here, as he that [ḥāṣaḇ](../../strongs/h/h2672.md) him out a [qeber](../../strongs/h/h6913.md) on [marowm](../../strongs/h/h4791.md), and that [ḥāqaq](../../strongs/h/h2710.md) a [miškān](../../strongs/h/h4908.md) for himself in a [cela'](../../strongs/h/h5553.md)?

<a name="isaiah_22_17"></a>Isaiah 22:17

Behold, [Yĕhovah](../../strongs/h/h3068.md) will [ṭûl](../../strongs/h/h2904.md) with a [geḇer](../../strongs/h/h1397.md) [ṭalṭēlâ](../../strongs/h/h2925.md), and will surely [ʿāṭâ](../../strongs/h/h5844.md) thee.

<a name="isaiah_22_18"></a>Isaiah 22:18

He will [ṣānap̄](../../strongs/h/h6801.md) [ṣānap̄](../../strongs/h/h6801.md) and [ṣᵊnēp̄â](../../strongs/h/h6802.md) thee like a [dûr](../../strongs/h/h1754.md) into a [rāḥāḇ](../../strongs/h/h7342.md) [yad](../../strongs/h/h3027.md) ['erets](../../strongs/h/h776.md): there shalt thou [muwth](../../strongs/h/h4191.md), and there the [merkāḇâ](../../strongs/h/h4818.md) of thy [kabowd](../../strongs/h/h3519.md) shall be the [qālôn](../../strongs/h/h7036.md) of thy ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md).

<a name="isaiah_22_19"></a>Isaiah 22:19

And I will [hāḏap̄](../../strongs/h/h1920.md) thee from thy [maṣṣāḇ](../../strongs/h/h4673.md), and from thy [maʿămāḏ](../../strongs/h/h4612.md) shall he [harac](../../strongs/h/h2040.md).

<a name="isaiah_22_20"></a>Isaiah 22:20

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that I will [qara'](../../strongs/h/h7121.md) my ['ebed](../../strongs/h/h5650.md) ['Elyāqîm](../../strongs/h/h471.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md):

<a name="isaiah_22_21"></a>Isaiah 22:21

And I will [labash](../../strongs/h/h3847.md) him with thy [kĕthoneth](../../strongs/h/h3801.md), and [ḥāzaq](../../strongs/h/h2388.md) him with thy ['aḇnēṭ](../../strongs/h/h73.md), and I will [nathan](../../strongs/h/h5414.md) thy [memshalah](../../strongs/h/h4475.md) into his [yad](../../strongs/h/h3027.md): and he shall be an ['ab](../../strongs/h/h1.md) to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="isaiah_22_22"></a>Isaiah 22:22

And the [map̄tēaḥ](../../strongs/h/h4668.md) of the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) will I lay upon his [šᵊḵem](../../strongs/h/h7926.md); so he shall [pāṯaḥ](../../strongs/h/h6605.md), and none shall [cagar](../../strongs/h/h5462.md); and he shall [cagar](../../strongs/h/h5462.md), and none shall [pāṯaḥ](../../strongs/h/h6605.md).

<a name="isaiah_22_23"></a>Isaiah 22:23

And I will [tāqaʿ](../../strongs/h/h8628.md) him as a [yāṯēḏ](../../strongs/h/h3489.md) in an ['aman](../../strongs/h/h539.md) [maqowm](../../strongs/h/h4725.md); and he shall be for a [kabowd](../../strongs/h/h3519.md) [kicce'](../../strongs/h/h3678.md) to his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="isaiah_22_24"></a>Isaiah 22:24

And they shall [tālâ](../../strongs/h/h8518.md) upon him all the [kabowd](../../strongs/h/h3519.md) of his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), the [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) and the [ṣᵊp̄îʿâ](../../strongs/h/h6849.md), all [kĕliy](../../strongs/h/h3627.md) of [qāṭān](../../strongs/h/h6996.md) quantity, from the [kĕliy](../../strongs/h/h3627.md) of ['agān](../../strongs/h/h101.md), even to all the [kĕliy](../../strongs/h/h3627.md) of [neḇel](../../strongs/h/h5035.md).

<a name="isaiah_22_25"></a>Isaiah 22:25

In that [yowm](../../strongs/h/h3117.md), saith [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), shall the [yāṯēḏ](../../strongs/h/h3489.md) that is [tāqaʿ](../../strongs/h/h8628.md) in the ['aman](../../strongs/h/h539.md) [maqowm](../../strongs/h/h4725.md) be [mûš](../../strongs/h/h4185.md), and be [gāḏaʿ](../../strongs/h/h1438.md), and [naphal](../../strongs/h/h5307.md); and the [maśśā'](../../strongs/h/h4853.md) that was upon it shall be [karath](../../strongs/h/h3772.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 21](isaiah_21.md) - [Isaiah 23](isaiah_23.md)