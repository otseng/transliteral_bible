# [Isaiah 24](https://www.blueletterbible.org/kjv/isa/24/1/s_703001)

<a name="isaiah_24_1"></a>Isaiah 24:1

Behold, [Yĕhovah](../../strongs/h/h3068.md) maketh the ['erets](../../strongs/h/h776.md) [bāqaq](../../strongs/h/h1238.md), and maketh it [bālaq](../../strongs/h/h1110.md), and [ʿāvâ](../../strongs/h/h5753.md) it [paniym](../../strongs/h/h6440.md), and [puwts](../../strongs/h/h6327.md) the [yashab](../../strongs/h/h3427.md) thereof.

<a name="isaiah_24_2"></a>Isaiah 24:2

And it shall be, as with the ['am](../../strongs/h/h5971.md), so with the [kōhēn](../../strongs/h/h3548.md); as with the ['ebed](../../strongs/h/h5650.md), so with his ['adown](../../strongs/h/h113.md); as with the [šip̄ḥâ](../../strongs/h/h8198.md), so with her [gᵊḇereṯ](../../strongs/h/h1404.md); as with the [qānâ](../../strongs/h/h7069.md), so with the [māḵar](../../strongs/h/h4376.md); as with the [lāvâ](../../strongs/h/h3867.md), so with the [lāvâ](../../strongs/h/h3867.md); as with the [nāšâ](../../strongs/h/h5383.md), so with the [nāšā'](../../strongs/h/h5378.md) to him.

<a name="isaiah_24_3"></a>Isaiah 24:3

The ['erets](../../strongs/h/h776.md) shall be [bāqaq](../../strongs/h/h1238.md) [bāqaq](../../strongs/h/h1238.md), and [bāzaz](../../strongs/h/h962.md) [bāzaz](../../strongs/h/h962.md): for [Yĕhovah](../../strongs/h/h3068.md) hath spoken this [dabar](../../strongs/h/h1697.md).

<a name="isaiah_24_4"></a>Isaiah 24:4

The ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md) and [nabel](../../strongs/h/h5034.md), the [tebel](../../strongs/h/h8398.md) ['āmal](../../strongs/h/h535.md) and [nabel](../../strongs/h/h5034.md), the [marowm](../../strongs/h/h4791.md) ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) do ['āmal](../../strongs/h/h535.md).

<a name="isaiah_24_5"></a>Isaiah 24:5

The ['erets](../../strongs/h/h776.md) also is [ḥānēp̄](../../strongs/h/h2610.md) under the [yashab](../../strongs/h/h3427.md) thereof; because they have ['abar](../../strongs/h/h5674.md) the [towrah](../../strongs/h/h8451.md), [ḥālap̄](../../strongs/h/h2498.md) the [choq](../../strongs/h/h2706.md), [pārar](../../strongs/h/h6565.md) the ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="isaiah_24_6"></a>Isaiah 24:6

Therefore hath the ['alah](../../strongs/h/h423.md) ['akal](../../strongs/h/h398.md) the ['erets](../../strongs/h/h776.md), and they that [yashab](../../strongs/h/h3427.md) therein are ['asham](../../strongs/h/h816.md): therefore the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) are [ḥārar](../../strongs/h/h2787.md), and [mizʿār](../../strongs/h/h4213.md) ['enowsh](../../strongs/h/h582.md) left.

<a name="isaiah_24_7"></a>Isaiah 24:7

The [tiyrowsh](../../strongs/h/h8492.md) ['āḇal](../../strongs/h/h56.md), the [gep̄en](../../strongs/h/h1612.md) ['āmal](../../strongs/h/h535.md), all the [śāmēaḥ](../../strongs/h/h8056.md) [leb](../../strongs/h/h3820.md) do ['ānaḥ](../../strongs/h/h584.md).

<a name="isaiah_24_8"></a>Isaiah 24:8

The [māśôś](../../strongs/h/h4885.md) of [tōp̄](../../strongs/h/h8596.md) [shabath](../../strongs/h/h7673.md), the [shā'ôn](../../strongs/h/h7588.md) of them that [ʿallîz](../../strongs/h/h5947.md) [ḥāḏal](../../strongs/h/h2308.md), the [māśôś](../../strongs/h/h4885.md) of the [kinnôr](../../strongs/h/h3658.md) [shabath](../../strongs/h/h7673.md).

<a name="isaiah_24_9"></a>Isaiah 24:9

They shall not [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) with a [šîr](../../strongs/h/h7892.md); [šēḵār](../../strongs/h/h7941.md) shall be [mārar](../../strongs/h/h4843.md) to them that [šāṯâ](../../strongs/h/h8354.md) it.

<a name="isaiah_24_10"></a>Isaiah 24:10

The [qiryâ](../../strongs/h/h7151.md) of [tohuw](../../strongs/h/h8414.md) is [shabar](../../strongs/h/h7665.md): every [bayith](../../strongs/h/h1004.md) is [cagar](../../strongs/h/h5462.md), that no man may come in.

<a name="isaiah_24_11"></a>Isaiah 24:11

There is a [ṣᵊvāḥâ](../../strongs/h/h6682.md) for [yayin](../../strongs/h/h3196.md) in the [ḥûṣ](../../strongs/h/h2351.md); all [simchah](../../strongs/h/h8057.md) is [ʿāraḇ](../../strongs/h/h6150.md), the [māśôś](../../strongs/h/h4885.md) of the ['erets](../../strongs/h/h776.md) is [gālâ](../../strongs/h/h1540.md).

<a name="isaiah_24_12"></a>Isaiah 24:12

In the [ʿîr](../../strongs/h/h5892.md) is [šā'ar](../../strongs/h/h7604.md) [šammâ](../../strongs/h/h8047.md), and the [sha'ar](../../strongs/h/h8179.md) is [kāṯaṯ](../../strongs/h/h3807.md) with [šᵊ'îyâ](../../strongs/h/h7591.md).

<a name="isaiah_24_13"></a>Isaiah 24:13

When thus it shall be in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md) among the ['am](../../strongs/h/h5971.md), there shall be as the [nōqep̄](../../strongs/h/h5363.md) of a [zayiṯ](../../strongs/h/h2132.md), and as the [ʿōlēlôṯ](../../strongs/h/h5955.md) when the [bāṣîr](../../strongs/h/h1210.md) is done.

<a name="isaiah_24_14"></a>Isaiah 24:14

They shall [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), they shall [ranan](../../strongs/h/h7442.md) for the [gā'ôn](../../strongs/h/h1347.md) of [Yĕhovah](../../strongs/h/h3068.md), they shall [ṣāhal](../../strongs/h/h6670.md) from the [yam](../../strongs/h/h3220.md).

<a name="isaiah_24_15"></a>Isaiah 24:15

Wherefore [kabad](../../strongs/h/h3513.md) ye [Yĕhovah](../../strongs/h/h3068.md) in the ['ûr](../../strongs/h/h217.md), even the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['î](../../strongs/h/h339.md) of the [yam](../../strongs/h/h3220.md).

<a name="isaiah_24_16"></a>Isaiah 24:16

From the [kanaph](../../strongs/h/h3671.md) of the ['erets](../../strongs/h/h776.md) have we [shama'](../../strongs/h/h8085.md) [zᵊmîr](../../strongs/h/h2158.md), even [ṣᵊḇî](../../strongs/h/h6643.md) to the [tsaddiyq](../../strongs/h/h6662.md). But I ['āmar](../../strongs/h/h559.md), My [rāzî](../../strongs/h/h7334.md), my [rāzî](../../strongs/h/h7334.md), ['owy](../../strongs/h/h188.md) unto me! the [bāḡaḏ](../../strongs/h/h898.md) have [bāḡaḏ](../../strongs/h/h898.md); yea, the [bāḡaḏ](../../strongs/h/h898.md) have dealt very [bāḡaḏ](../../strongs/h/h898.md).

<a name="isaiah_24_17"></a>Isaiah 24:17

[paḥaḏ](../../strongs/h/h6343.md), and the [paḥaṯ](../../strongs/h/h6354.md), and the [paḥ](../../strongs/h/h6341.md), are upon thee, O [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_24_18"></a>Isaiah 24:18

And it shall come to pass, that he who [nûs](../../strongs/h/h5127.md) from the [qowl](../../strongs/h/h6963.md) of the [paḥaḏ](../../strongs/h/h6343.md) shall [naphal](../../strongs/h/h5307.md) into the [paḥaṯ](../../strongs/h/h6354.md); and he that [ʿālâ](../../strongs/h/h5927.md) out of the midst of the [paḥaṯ](../../strongs/h/h6354.md) shall be [lāḵaḏ](../../strongs/h/h3920.md) in the [paḥ](../../strongs/h/h6341.md): for the ['ărubâ](../../strongs/h/h699.md) from on [marowm](../../strongs/h/h4791.md) are [pāṯaḥ](../../strongs/h/h6605.md), and the [mowcadah](../../strongs/h/h4146.md) of the ['erets](../../strongs/h/h776.md) do [rāʿaš](../../strongs/h/h7493.md).

<a name="isaiah_24_19"></a>Isaiah 24:19

The ['erets](../../strongs/h/h776.md) is [ra'a'](../../strongs/h/h7489.md) [ra'a'](../../strongs/h/h7489.md), the ['erets](../../strongs/h/h776.md) is [pārar](../../strongs/h/h6565.md) [pārar](../../strongs/h/h6565.md), the ['erets](../../strongs/h/h776.md) is [mowt](../../strongs/h/h4131.md) [mowt](../../strongs/h/h4131.md).

<a name="isaiah_24_20"></a>Isaiah 24:20

The ['erets](../../strongs/h/h776.md) shall [nûaʿ](../../strongs/h/h5128.md) [nûaʿ](../../strongs/h/h5128.md) like a [šikôr](../../strongs/h/h7910.md), and shall be [nuwd](../../strongs/h/h5110.md) like a [mᵊlûnâ](../../strongs/h/h4412.md); and the [pesha'](../../strongs/h/h6588.md) thereof shall be [kabad](../../strongs/h/h3513.md) upon it; and it shall [naphal](../../strongs/h/h5307.md), and not [quwm](../../strongs/h/h6965.md) again.

<a name="isaiah_24_21"></a>Isaiah 24:21

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) shall [paqad](../../strongs/h/h6485.md) the [tsaba'](../../strongs/h/h6635.md) of the [marowm](../../strongs/h/h4791.md) that are on [marowm](../../strongs/h/h4791.md), and the [melek](../../strongs/h/h4428.md) of the ['ăḏāmâ](../../strongs/h/h127.md) upon the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="isaiah_24_22"></a>Isaiah 24:22

And they shall be ['āsap̄](../../strongs/h/h622.md), as ['assîr](../../strongs/h/h616.md) are ['ăsēp̄â](../../strongs/h/h626.md) in the [bowr](../../strongs/h/h953.md), and shall be [cagar](../../strongs/h/h5462.md) in the [masgēr](../../strongs/h/h4525.md), and after many [yowm](../../strongs/h/h3117.md) shall they be [paqad](../../strongs/h/h6485.md).

<a name="isaiah_24_23"></a>Isaiah 24:23

Then the [lᵊḇānâ](../../strongs/h/h3842.md) shall be [ḥāp̄ēr](../../strongs/h/h2659.md), and the [ḥammâ](../../strongs/h/h2535.md) [buwsh](../../strongs/h/h954.md), when [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall [mālaḵ](../../strongs/h/h4427.md) in [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md), and before his [zāqēn](../../strongs/h/h2205.md) [kabowd](../../strongs/h/h3519.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 23](isaiah_23.md) - [Isaiah 25](isaiah_25.md)