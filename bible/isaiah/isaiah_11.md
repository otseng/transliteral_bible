# [Isaiah 11](https://www.blueletterbible.org/kjv/isa/11/1/s_690001)

<a name="isaiah_11_1"></a>Isaiah 11:1

And there shall [yāṣā'](../../strongs/h/h3318.md) a [ḥōṭer](../../strongs/h/h2415.md) out of the [gezaʿ](../../strongs/h/h1503.md) of [Yišay](../../strongs/h/h3448.md), and a [nēṣer](../../strongs/h/h5342.md) shall [parah](../../strongs/h/h6509.md) of his [šereš](../../strongs/h/h8328.md):

<a name="isaiah_11_2"></a>Isaiah 11:2

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [nuwach](../../strongs/h/h5117.md) upon him, the [ruwach](../../strongs/h/h7307.md) of [ḥāḵmâ](../../strongs/h/h2451.md) and [bînâ](../../strongs/h/h998.md), the [ruwach](../../strongs/h/h7307.md) of ['etsah](../../strongs/h/h6098.md) and [gᵊḇûrâ](../../strongs/h/h1369.md), the [ruwach](../../strongs/h/h7307.md) of [da'ath](../../strongs/h/h1847.md) and of the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="isaiah_11_3"></a>Isaiah 11:3

And shall make him of [rîaḥ](../../strongs/h/h7306.md) in the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md): and he shall not [shaphat](../../strongs/h/h8199.md) after the [mar'ê](../../strongs/h/h4758.md) of his ['ayin](../../strongs/h/h5869.md), neither [yakach](../../strongs/h/h3198.md) after the [Mišmāʿ](../../strongs/h/h4926.md) of his ['ozen](../../strongs/h/h241.md):

<a name="isaiah_11_4"></a>Isaiah 11:4

But with [tsedeq](../../strongs/h/h6664.md) shall he [shaphat](../../strongs/h/h8199.md) the [dal](../../strongs/h/h1800.md), and [yakach](../../strongs/h/h3198.md) with [mîšôr](../../strongs/h/h4334.md) for the ['anav](../../strongs/h/h6035.md) of the ['erets](../../strongs/h/h776.md): and he shall [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md): with the [shebet](../../strongs/h/h7626.md) of his [peh](../../strongs/h/h6310.md), and with the [ruwach](../../strongs/h/h7307.md) of his [saphah](../../strongs/h/h8193.md) shall he [muwth](../../strongs/h/h4191.md) the [rasha'](../../strongs/h/h7563.md).

<a name="isaiah_11_5"></a>Isaiah 11:5

And [tsedeq](../../strongs/h/h6664.md) shall be the ['ēzôr](../../strongs/h/h232.md) of his [māṯnayim](../../strongs/h/h4975.md), and ['ĕmûnâ](../../strongs/h/h530.md) the ['ēzôr](../../strongs/h/h232.md) of his [ḥālāṣ](../../strongs/h/h2504.md).

<a name="isaiah_11_6"></a>Isaiah 11:6

The [zᵊ'ēḇ](../../strongs/h/h2061.md) also shall [guwr](../../strongs/h/h1481.md) with the [keḇeś](../../strongs/h/h3532.md), and the [nāmēr](../../strongs/h/h5246.md) shall [rāḇaṣ](../../strongs/h/h7257.md) with the [gᵊḏî](../../strongs/h/h1423.md); and the [ʿēḡel](../../strongs/h/h5695.md) and the [kephiyr](../../strongs/h/h3715.md) and the [mᵊrî'](../../strongs/h/h4806.md) together; and a [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md) shall [nāhaḡ](../../strongs/h/h5090.md) them.

<a name="isaiah_11_7"></a>Isaiah 11:7

And the [pārâ](../../strongs/h/h6510.md) and the [dōḇ](../../strongs/h/h1677.md) shall [ra'ah](../../strongs/h/h7462.md); their [yeleḏ](../../strongs/h/h3206.md) shall [rāḇaṣ](../../strongs/h/h7257.md) together: and the ['ariy](../../strongs/h/h738.md) shall ['akal](../../strongs/h/h398.md) [teḇen](../../strongs/h/h8401.md) like the [bāqār](../../strongs/h/h1241.md).

<a name="isaiah_11_8"></a>Isaiah 11:8

And the [yānaq](../../strongs/h/h3243.md) shall [šāʿaʿ](../../strongs/h/h8173.md) on the [Ḥûr](../../strongs/h/h2352.md) of the [peṯen](../../strongs/h/h6620.md), and the [gamal](../../strongs/h/h1580.md) shall put his [yad](../../strongs/h/h3027.md) on the [ṣep̄aʿ](../../strongs/h/h6848.md) [mᵊ'ûrâ](../../strongs/h/h3975.md).

<a name="isaiah_11_9"></a>Isaiah 11:9

They shall not [ra'a'](../../strongs/h/h7489.md) nor [shachath](../../strongs/h/h7843.md) in all my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md): for the ['erets](../../strongs/h/h776.md) shall be [mālā'](../../strongs/h/h4390.md) of the [dēʿâ](../../strongs/h/h1844.md) of [Yĕhovah](../../strongs/h/h3068.md), as the [mayim](../../strongs/h/h4325.md) [kāsâ](../../strongs/h/h3680.md) the [yam](../../strongs/h/h3220.md).

<a name="isaiah_11_10"></a>Isaiah 11:10

And in that [yowm](../../strongs/h/h3117.md) there shall be a [šereš](../../strongs/h/h8328.md) of [Yišay](../../strongs/h/h3448.md), which shall stand for a [nēs](../../strongs/h/h5251.md) of the ['am](../../strongs/h/h5971.md); to it shall the [gowy](../../strongs/h/h1471.md) [darash](../../strongs/h/h1875.md): and his [mᵊnûḥâ](../../strongs/h/h4496.md) shall be [kabowd](../../strongs/h/h3519.md).

<a name="isaiah_11_11"></a>Isaiah 11:11

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that ['adonay](../../strongs/h/h136.md) shall set his [yad](../../strongs/h/h3027.md) again the second time to [qānâ](../../strongs/h/h7069.md) the [šᵊ'ār](../../strongs/h/h7605.md) of his ['am](../../strongs/h/h5971.md), which shall be [šā'ar](../../strongs/h/h7604.md), from ['Aššûr](../../strongs/h/h804.md), and from [Mitsrayim](../../strongs/h/h4714.md), and from [Paṯrôs](../../strongs/h/h6624.md), and from [Kûš](../../strongs/h/h3568.md), and from [ʿÊlām](../../strongs/h/h5867.md), and from [Šinʿār](../../strongs/h/h8152.md), and from [Ḥămāṯ](../../strongs/h/h2574.md), and from the ['î](../../strongs/h/h339.md) of the [yam](../../strongs/h/h3220.md).

<a name="isaiah_11_12"></a>Isaiah 11:12

And he shall [nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md) for the [gowy](../../strongs/h/h1471.md), and shall ['āsap̄](../../strongs/h/h622.md) the outcasts of [Yisra'el](../../strongs/h/h3478.md), and [qāḇaṣ](../../strongs/h/h6908.md) the [naphats](../../strongs/h/h5310.md) of [Yehuwdah](../../strongs/h/h3063.md) from the [kanaph](../../strongs/h/h3671.md) of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_11_13"></a>Isaiah 11:13

The [qin'â](../../strongs/h/h7068.md) also of ['Ep̄rayim](../../strongs/h/h669.md) shall [cuwr](../../strongs/h/h5493.md), and the [tsarar](../../strongs/h/h6887.md) of [Yehuwdah](../../strongs/h/h3063.md) shall be [karath](../../strongs/h/h3772.md): ['Ep̄rayim](../../strongs/h/h669.md) shall not envy [Yehuwdah](../../strongs/h/h3063.md), and [Yehuwdah](../../strongs/h/h3063.md) shall not [tsarar](../../strongs/h/h6887.md) ['Ep̄rayim](../../strongs/h/h669.md).

<a name="isaiah_11_14"></a>Isaiah 11:14

But they shall ['uwph](../../strongs/h/h5774.md) upon the [kāṯēp̄](../../strongs/h/h3802.md) of the [Pᵊlištî](../../strongs/h/h6430.md) toward the [yam](../../strongs/h/h3220.md); they shall [bāzaz](../../strongs/h/h962.md) them of the [qeḏem](../../strongs/h/h6924.md) together: they shall [mišlôaḥ](../../strongs/h/h4916.md) their [yad](../../strongs/h/h3027.md) upon ['Ĕḏōm](../../strongs/h/h123.md) and [Mô'āḇ](../../strongs/h/h4124.md); and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) shall [mišmaʿaṯ](../../strongs/h/h4928.md) them.

<a name="isaiah_11_15"></a>Isaiah 11:15

And [Yĕhovah](../../strongs/h/h3068.md) shall [ḥāram](../../strongs/h/h2763.md) the [lashown](../../strongs/h/h3956.md) of the [Mitsrayim](../../strongs/h/h4714.md) [yam](../../strongs/h/h3220.md); and with his [ʿăyām](../../strongs/h/h5868.md) [ruwach](../../strongs/h/h7307.md) shall he [nûp̄](../../strongs/h/h5130.md) his [yad](../../strongs/h/h3027.md) over the [nāhār](../../strongs/h/h5104.md), and shall [nakah](../../strongs/h/h5221.md) it in the [šeḇaʿ](../../strongs/h/h7651.md) [nachal](../../strongs/h/h5158.md), and make men [dāraḵ](../../strongs/h/h1869.md) [naʿal](../../strongs/h/h5275.md).

<a name="isaiah_11_16"></a>Isaiah 11:16

And there shall be a [mĕcillah](../../strongs/h/h4546.md) for the [šᵊ'ār](../../strongs/h/h7605.md) of his ['am](../../strongs/h/h5971.md), which shall be [šā'ar](../../strongs/h/h7604.md), from ['Aššûr](../../strongs/h/h804.md); like as it was to [Yisra'el](../../strongs/h/h3478.md) in the [yowm](../../strongs/h/h3117.md) that he [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 10](isaiah_10.md) - [Isaiah 12](isaiah_12.md)