# [Isaiah 42](https://www.blueletterbible.org/kjv/isa/42/1/s_721001)

<a name="isaiah_42_1"></a>Isaiah 42:1

Behold my ['ebed](../../strongs/h/h5650.md), whom I [tamak](../../strongs/h/h8551.md); mine [bāḥîr](../../strongs/h/h972.md), in whom my [nephesh](../../strongs/h/h5315.md) [ratsah](../../strongs/h/h7521.md); I have [nathan](../../strongs/h/h5414.md) my [ruwach](../../strongs/h/h7307.md) upon him: he shall [yāṣā'](../../strongs/h/h3318.md) [mishpat](../../strongs/h/h4941.md) to the [gowy](../../strongs/h/h1471.md).

<a name="isaiah_42_2"></a>Isaiah 42:2

He shall not [ṣāʿaq](../../strongs/h/h6817.md), nor [nasa'](../../strongs/h/h5375.md), nor cause his [qowl](../../strongs/h/h6963.md) to be [shama'](../../strongs/h/h8085.md) in the [ḥûṣ](../../strongs/h/h2351.md).

<a name="isaiah_42_3"></a>Isaiah 42:3

A [rāṣaṣ](../../strongs/h/h7533.md) [qānê](../../strongs/h/h7070.md) shall he not [shabar](../../strongs/h/h7665.md), and the [kēhê](../../strongs/h/h3544.md) [pištê](../../strongs/h/h6594.md) shall he not [kāḇâ](../../strongs/h/h3518.md): he shall [yāṣā'](../../strongs/h/h3318.md) [mishpat](../../strongs/h/h4941.md) unto ['emeth](../../strongs/h/h571.md).

<a name="isaiah_42_4"></a>Isaiah 42:4

He shall not [kāhâ](../../strongs/h/h3543.md) nor be [rāṣaṣ](../../strongs/h/h7533.md), till he have set [mishpat](../../strongs/h/h4941.md) in the ['erets](../../strongs/h/h776.md): and the ['î](../../strongs/h/h339.md) shall [yāḥal](../../strongs/h/h3176.md) for his [towrah](../../strongs/h/h8451.md).

<a name="isaiah_42_5"></a>Isaiah 42:5

Thus saith ['el](../../strongs/h/h410.md) [Yĕhovah](../../strongs/h/h3068.md), he that [bara'](../../strongs/h/h1254.md) the [shamayim](../../strongs/h/h8064.md), and [natah](../../strongs/h/h5186.md) them; he that [rāqaʿ](../../strongs/h/h7554.md) the ['erets](../../strongs/h/h776.md), and that which [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) of it; he that [nathan](../../strongs/h/h5414.md) [neshamah](../../strongs/h/h5397.md) unto the ['am](../../strongs/h/h5971.md) upon it, and [ruwach](../../strongs/h/h7307.md) to them that [halak](../../strongs/h/h1980.md) therein:

<a name="isaiah_42_6"></a>Isaiah 42:6

I [Yĕhovah](../../strongs/h/h3068.md) have [qara'](../../strongs/h/h7121.md) thee in [tsedeq](../../strongs/h/h6664.md), and will [ḥāzaq](../../strongs/h/h2388.md) thine [yad](../../strongs/h/h3027.md), and will [nāṣar](../../strongs/h/h5341.md) thee, and [nathan](../../strongs/h/h5414.md) thee for a [bĕriyth](../../strongs/h/h1285.md) of the ['am](../../strongs/h/h5971.md), for a ['owr](../../strongs/h/h216.md) of the [gowy](../../strongs/h/h1471.md);

<a name="isaiah_42_7"></a>Isaiah 42:7

To [paqach](../../strongs/h/h6491.md) the [ʿiûēr](../../strongs/h/h5787.md) ['ayin](../../strongs/h/h5869.md), to [yāṣā'](../../strongs/h/h3318.md) the ['assîr](../../strongs/h/h616.md) from the [masgēr](../../strongs/h/h4525.md), and them that [yashab](../../strongs/h/h3427.md) in [choshek](../../strongs/h/h2822.md) out of the [kele'](../../strongs/h/h3608.md) [bayith](../../strongs/h/h1004.md).

<a name="isaiah_42_8"></a>Isaiah 42:8

I am [Yĕhovah](../../strongs/h/h3068.md): that is my [shem](../../strongs/h/h8034.md): and my [kabowd](../../strongs/h/h3519.md) will I not [nathan](../../strongs/h/h5414.md) to another, neither my [tehillah](../../strongs/h/h8416.md) to [pāsîl](../../strongs/h/h6456.md).

<a name="isaiah_42_9"></a>Isaiah 42:9

Behold, the [ri'šôn](../../strongs/h/h7223.md) are [bow'](../../strongs/h/h935.md), and [ḥāḏāš](../../strongs/h/h2319.md) do I [nāḡaḏ](../../strongs/h/h5046.md): before they [ṣāmaḥ](../../strongs/h/h6779.md) I [shama'](../../strongs/h/h8085.md) you of them.

<a name="isaiah_42_10"></a>Isaiah 42:10

[shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [ḥāḏāš](../../strongs/h/h2319.md) [šîr](../../strongs/h/h7892.md), and his [tehillah](../../strongs/h/h8416.md) from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md), ye that [yarad](../../strongs/h/h3381.md) to the [yam](../../strongs/h/h3220.md), and all that is therein; the ['î](../../strongs/h/h339.md), and the [yashab](../../strongs/h/h3427.md) thereof.

<a name="isaiah_42_11"></a>Isaiah 42:11

Let the [midbar](../../strongs/h/h4057.md) and the [ʿîr](../../strongs/h/h5892.md) thereof [nasa'](../../strongs/h/h5375.md) their voice, the [ḥāṣēr](../../strongs/h/h2691.md) that [Qēḏār](../../strongs/h/h6938.md) doth [yashab](../../strongs/h/h3427.md): let the [yashab](../../strongs/h/h3427.md) of the [cela'](../../strongs/h/h5553.md) [ranan](../../strongs/h/h7442.md), let them [ṣāvaḥ](../../strongs/h/h6681.md) from the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md).

<a name="isaiah_42_12"></a>Isaiah 42:12

Let them [śûm](../../strongs/h/h7760.md) [kabowd](../../strongs/h/h3519.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [nāḡaḏ](../../strongs/h/h5046.md) his [tehillah](../../strongs/h/h8416.md) in the ['î](../../strongs/h/h339.md).

<a name="isaiah_42_13"></a>Isaiah 42:13

[Yĕhovah](../../strongs/h/h3068.md) shall [yāṣā'](../../strongs/h/h3318.md) as a [gibôr](../../strongs/h/h1368.md), he shall [ʿûr](../../strongs/h/h5782.md) [qin'â](../../strongs/h/h7068.md) like an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md): he shall [rûaʿ](../../strongs/h/h7321.md), yea, [ṣāraḥ](../../strongs/h/h6873.md); he shall [gabar](../../strongs/h/h1396.md) against his ['oyeb](../../strongs/h/h341.md).

<a name="isaiah_42_14"></a>Isaiah 42:14

I have ['owlam](../../strongs/h/h5769.md) [ḥāšâ](../../strongs/h/h2814.md); I have been [ḥāraš](../../strongs/h/h2790.md), and ['āp̄aq](../../strongs/h/h662.md) myself: now will I [pāʿâ](../../strongs/h/h6463.md) like a [yalad](../../strongs/h/h3205.md); I will [nāšam](../../strongs/h/h5395.md) [šāmēm](../../strongs/h/h8074.md) and [šā'ap̄](../../strongs/h/h7602.md) at once.

<a name="isaiah_42_15"></a>Isaiah 42:15

I will [ḥāraḇ](../../strongs/h/h2717.md) [har](../../strongs/h/h2022.md) and [giḇʿâ](../../strongs/h/h1389.md), and [yāḇēš](../../strongs/h/h3001.md) all their ['eseb](../../strongs/h/h6212.md); and I will [śûm](../../strongs/h/h7760.md) the [nāhār](../../strongs/h/h5104.md) ['î](../../strongs/h/h339.md), and I will [yāḇēš](../../strongs/h/h3001.md) the ['ăḡam](../../strongs/h/h98.md).

<a name="isaiah_42_16"></a>Isaiah 42:16

And I will [yālaḵ](../../strongs/h/h3212.md) the [ʿiûēr](../../strongs/h/h5787.md) by a [derek](../../strongs/h/h1870.md) that they [yada'](../../strongs/h/h3045.md) not; I will [dāraḵ](../../strongs/h/h1869.md) them in [nāṯîḇ](../../strongs/h/h5410.md) that they have not [yada'](../../strongs/h/h3045.md): I will [śûm](../../strongs/h/h7760.md) [maḥšāḵ](../../strongs/h/h4285.md) ['owr](../../strongs/h/h216.md) before them, and [maʿăqaššîm](../../strongs/h/h4625.md) [mîšôr](../../strongs/h/h4334.md). These things will I ['asah](../../strongs/h/h6213.md) unto them, and not ['azab](../../strongs/h/h5800.md) them.

<a name="isaiah_42_17"></a>Isaiah 42:17

They shall be [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md), they shall be [bšeṯ](../../strongs/h/h1322.md) [buwsh](../../strongs/h/h954.md), that [batach](../../strongs/h/h982.md) in [pecel](../../strongs/h/h6459.md), that ['āmar](../../strongs/h/h559.md) to the [massēḵâ](../../strongs/h/h4541.md), Ye are our ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_42_18"></a>Isaiah 42:18

[shama'](../../strongs/h/h8085.md), ye [ḥērēš](../../strongs/h/h2795.md); and [nabat](../../strongs/h/h5027.md), ye [ʿiûēr](../../strongs/h/h5787.md), that ye may [ra'ah](../../strongs/h/h7200.md).

<a name="isaiah_42_19"></a>Isaiah 42:19

Who is [ʿiûēr](../../strongs/h/h5787.md), but my ['ebed](../../strongs/h/h5650.md)? or [ḥērēš](../../strongs/h/h2795.md), as my [mal'ak](../../strongs/h/h4397.md) that I [shalach](../../strongs/h/h7971.md)?  who is [ʿiûēr](../../strongs/h/h5787.md) as he that is [shalam](../../strongs/h/h7999.md), and [ʿiûēr](../../strongs/h/h5787.md) as [Yĕhovah](../../strongs/h/h3068.md) ['ebed](../../strongs/h/h5650.md)?

<a name="isaiah_42_20"></a>Isaiah 42:20

[ra'ah](../../strongs/h/h7200.md) [ra'ah](../../strongs/h/h7200.md) many things, but thou [shamar](../../strongs/h/h8104.md) not; [paqach](../../strongs/h/h6491.md) the ['ozen](../../strongs/h/h241.md), but he [shama'](../../strongs/h/h8085.md) not.

<a name="isaiah_42_21"></a>Isaiah 42:21

[Yĕhovah](../../strongs/h/h3068.md) is [ḥāp̄ēṣ](../../strongs/h/h2654.md) for his [tsedeq](../../strongs/h/h6664.md) sake; he will [gāḏal](../../strongs/h/h1431.md) the [towrah](../../strongs/h/h8451.md), and ['āḏar](../../strongs/h/h142.md).

<a name="isaiah_42_22"></a>Isaiah 42:22

But this is an ['am](../../strongs/h/h5971.md) [bāzaz](../../strongs/h/h962.md) and [šāsâ](../../strongs/h/h8154.md); they are all of them [pāḥaḥ](../../strongs/h/h6351.md) in [Ḥûr](../../strongs/h/h2352.md) [bāḥûr](../../strongs/h/h970.md), and they are [chaba'](../../strongs/h/h2244.md) in [kele'](../../strongs/h/h3608.md) [bayith](../../strongs/h/h1004.md): they are for a [baz](../../strongs/h/h957.md), and none [natsal](../../strongs/h/h5337.md); for a [mᵊšissâ](../../strongs/h/h4933.md), and none ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md).

<a name="isaiah_42_23"></a>Isaiah 42:23

Who among you will ['azan](../../strongs/h/h238.md) to this? who will [qashab](../../strongs/h/h7181.md) and [shama'](../../strongs/h/h8085.md) for the ['āḥôr](../../strongs/h/h268.md)?

<a name="isaiah_42_24"></a>Isaiah 42:24

Who [nathan](../../strongs/h/h5414.md) [Ya'aqob](../../strongs/h/h3290.md) for a [mᵊšissâ](../../strongs/h/h4933.md) [mᵊšûssâ](../../strongs/h/h4882.md), and [Yisra'el](../../strongs/h/h3478.md) to the [bāzaz](../../strongs/h/h962.md)? did not [Yĕhovah](../../strongs/h/h3068.md), he against whom we have [chata'](../../strongs/h/h2398.md)? for they would not [halak](../../strongs/h/h1980.md) in his ways, neither were they [shama'](../../strongs/h/h8085.md) unto his [towrah](../../strongs/h/h8451.md).

<a name="isaiah_42_25"></a>Isaiah 42:25

Therefore he hath [šāp̄aḵ](../../strongs/h/h8210.md) upon him the [chemah](../../strongs/h/h2534.md) of his ['aph](../../strongs/h/h639.md), and the [ʿĕzûz](../../strongs/h/h5807.md) of [milḥāmâ](../../strongs/h/h4421.md): and it hath [lāhaṭ](../../strongs/h/h3857.md) [cabiyb](../../strongs/h/h5439.md), yet he [yada'](../../strongs/h/h3045.md) not; and it [bāʿar](../../strongs/h/h1197.md) him, yet he [śûm](../../strongs/h/h7760.md) it not to [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 41](isaiah_41.md) - [Isaiah 43](isaiah_43.md)