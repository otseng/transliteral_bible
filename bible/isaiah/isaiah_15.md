# [Isaiah 15](https://www.blueletterbible.org/kjv/isa/15/1/s_694001)

<a name="isaiah_15_1"></a>Isaiah 15:1

The [maśśā'](../../strongs/h/h4853.md) of [Mô'āḇ](../../strongs/h/h4124.md). Because in the [layil](../../strongs/h/h3915.md) [ʿĀr](../../strongs/h/h6144.md) of [Mô'āḇ](../../strongs/h/h4124.md) is [shadad](../../strongs/h/h7703.md), and [damah](../../strongs/h/h1820.md); because in the [layil](../../strongs/h/h3915.md) [qîr](../../strongs/h/h7024.md) of [Mô'āḇ](../../strongs/h/h4124.md) is [shadad](../../strongs/h/h7703.md), and [damah](../../strongs/h/h1820.md);

<a name="isaiah_15_2"></a>Isaiah 15:2

He is [ʿālâ](../../strongs/h/h5927.md) to [Bayiṯ](../../strongs/h/h1006.md), and to [Dîḇōvn](../../strongs/h/h1769.md), the [bāmâ](../../strongs/h/h1116.md), to [bĕkiy](../../strongs/h/h1065.md): [Mô'āḇ](../../strongs/h/h4124.md) shall [yālal](../../strongs/h/h3213.md) over [Nᵊḇô](../../strongs/h/h5015.md), and over [Mêḏḇā'](../../strongs/h/h4311.md): on all their [ro'sh](../../strongs/h/h7218.md) shall be [qrḥ](../../strongs/h/h7144.md), and every [zāqān](../../strongs/h/h2206.md) [gāḏaʿ](../../strongs/h/h1438.md).

<a name="isaiah_15_3"></a>Isaiah 15:3

In their [ḥûṣ](../../strongs/h/h2351.md) they shall [ḥāḡar](../../strongs/h/h2296.md) themselves with [śaq](../../strongs/h/h8242.md): on the [gāḡ](../../strongs/h/h1406.md), and in their [rᵊḥōḇ](../../strongs/h/h7339.md), every one shall [yālal](../../strongs/h/h3213.md), [Bĕkiy](../../strongs/h/h1065.md) [yarad](../../strongs/h/h3381.md).

<a name="isaiah_15_4"></a>Isaiah 15:4

And [Hešbôn](../../strongs/h/h2809.md) shall [zāʿaq](../../strongs/h/h2199.md), and ['Elʿālē'](../../strongs/h/h500.md): their [qowl](../../strongs/h/h6963.md) shall be [shama'](../../strongs/h/h8085.md) even unto [Yahaṣ](../../strongs/h/h3096.md): therefore the [chalats](../../strongs/h/h2502.md) of [Mô'āḇ](../../strongs/h/h4124.md) shall [rûaʿ](../../strongs/h/h7321.md); his [nephesh](../../strongs/h/h5315.md) shall be [yāraʿ](../../strongs/h/h3415.md) unto him.

<a name="isaiah_15_5"></a>Isaiah 15:5

My [leb](../../strongs/h/h3820.md) shall cry out for [Mô'āḇ](../../strongs/h/h4124.md); his [bᵊrîaḥ](../../strongs/h/h1280.md) shall flee unto [Ṣōʿar](../../strongs/h/h6820.md), an [ʿeḡlâ](../../strongs/h/h5697.md) of three years old: for by the [maʿălê](../../strongs/h/h4608.md) of [lûḥîṯ](../../strongs/h/h3872.md) with [bĕkiy](../../strongs/h/h1065.md) shall they [ʿālâ](../../strongs/h/h5927.md) it; for in the [derek](../../strongs/h/h1870.md) of [ḥōrōnayim](../../strongs/h/h2773.md) they shall [ʿûr](../../strongs/h/h5782.md) a [zaʿaq](../../strongs/h/h2201.md) of [šeḇar](../../strongs/h/h7667.md).

<a name="isaiah_15_6"></a>Isaiah 15:6

For the [mayim](../../strongs/h/h4325.md) of [nimrîm](../../strongs/h/h5249.md) shall be [mᵊšammâ](../../strongs/h/h4923.md): for the [chatsiyr](../../strongs/h/h2682.md) is [yāḇēš](../../strongs/h/h3001.md), the [deše'](../../strongs/h/h1877.md) [kalah](../../strongs/h/h3615.md), there is no [yereq](../../strongs/h/h3418.md).

<a name="isaiah_15_7"></a>Isaiah 15:7

Therefore the [yiṯrâ](../../strongs/h/h3502.md) they have ['asah](../../strongs/h/h6213.md), and that which they have [pᵊqudâ](../../strongs/h/h6486.md), shall they [nasa'](../../strongs/h/h5375.md) to the [nachal](../../strongs/h/h5158.md) of the [ʿărāḇâ](../../strongs/h/h6155.md).

<a name="isaiah_15_8"></a>Isaiah 15:8

For the [zaʿaq](../../strongs/h/h2201.md) is gone [naqaph](../../strongs/h/h5362.md) the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md); the [yᵊlālâ](../../strongs/h/h3215.md) thereof unto ['eḡlayim](../../strongs/h/h97.md), and the [yᵊlālâ](../../strongs/h/h3215.md) thereof unto [bᵊ'ēr 'ēlîm](../../strongs/h/h879.md).

<a name="isaiah_15_9"></a>Isaiah 15:9

For the [mayim](../../strongs/h/h4325.md) of [dîmôn](../../strongs/h/h1775.md) shall be [mālā'](../../strongs/h/h4390.md) of [dam](../../strongs/h/h1818.md): for I will [shiyth](../../strongs/h/h7896.md) more upon [dîmôn](../../strongs/h/h1775.md), ['ariy](../../strongs/h/h738.md) upon him that [pᵊlêṭâ](../../strongs/h/h6413.md) of [Mô'āḇ](../../strongs/h/h4124.md), and upon the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 14](isaiah_14.md) - [Isaiah 16](isaiah_16.md)