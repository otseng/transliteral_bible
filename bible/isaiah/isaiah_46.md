# [Isaiah 46](https://www.blueletterbible.org/kjv/isa/46/1/s_725001)

<a name="isaiah_46_1"></a>Isaiah 46:1

[bēl](../../strongs/h/h1078.md) [kara'](../../strongs/h/h3766.md), [Nᵊḇô](../../strongs/h/h5015.md) [qāras](../../strongs/h/h7164.md), their [ʿāṣāḇ](../../strongs/h/h6091.md) were upon the [chay](../../strongs/h/h2416.md), and upon the [bĕhemah](../../strongs/h/h929.md): your [nᵊśû'â](../../strongs/h/h5385.md) were [ʿāmas](../../strongs/h/h6006.md); they are a [maśśā'](../../strongs/h/h4853.md) to the [ʿāyēp̄](../../strongs/h/h5889.md).

<a name="isaiah_46_2"></a>Isaiah 46:2

They [qāras](../../strongs/h/h7164.md), they [kara'](../../strongs/h/h3766.md) together; they could not [mālaṭ](../../strongs/h/h4422.md) the [maśśā'](../../strongs/h/h4853.md), but themselves are [halak](../../strongs/h/h1980.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="isaiah_46_3"></a>Isaiah 46:3

[shama'](../../strongs/h/h8085.md) unto me, O [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), which are [ʿāmas](../../strongs/h/h6006.md) by me from the [beten](../../strongs/h/h990.md), which are [nasa'](../../strongs/h/h5375.md) from the [raḥam](../../strongs/h/h7356.md):

<a name="isaiah_46_4"></a>Isaiah 46:4

And even to your [ziqnâ](../../strongs/h/h2209.md) I am he; and even to [śêḇâ](../../strongs/h/h7872.md) will I [sāḇal](../../strongs/h/h5445.md) you: I have ['asah](../../strongs/h/h6213.md), and I will [nasa'](../../strongs/h/h5375.md); even I will [sāḇal](../../strongs/h/h5445.md), and will [mālaṭ](../../strongs/h/h4422.md) you.

<a name="isaiah_46_5"></a>Isaiah 46:5

To whom will ye [dāmâ](../../strongs/h/h1819.md) me, and make me [šāvâ](../../strongs/h/h7737.md), and [māšal](../../strongs/h/h4911.md) me, that we may be [dāmâ](../../strongs/h/h1819.md)?

<a name="isaiah_46_6"></a>Isaiah 46:6

They [zûl](../../strongs/h/h2107.md) [zāhāḇ](../../strongs/h/h2091.md) out of the [kîs](../../strongs/h/h3599.md), and [šāqal](../../strongs/h/h8254.md) [keceph](../../strongs/h/h3701.md) in the [qānê](../../strongs/h/h7070.md), and [śāḵar](../../strongs/h/h7936.md) a [tsaraph](../../strongs/h/h6884.md); and he ['asah](../../strongs/h/h6213.md) it an ['el](../../strongs/h/h410.md): they [sāḡaḏ](../../strongs/h/h5456.md), yea, they [shachah](../../strongs/h/h7812.md).

<a name="isaiah_46_7"></a>Isaiah 46:7

They [nasa'](../../strongs/h/h5375.md) him upon the [kāṯēp̄](../../strongs/h/h3802.md), they [sāḇal](../../strongs/h/h5445.md) him, and set him in his [yānaḥ](../../strongs/h/h3240.md), and he ['amad](../../strongs/h/h5975.md); from his [maqowm](../../strongs/h/h4725.md) shall he not [mûš](../../strongs/h/h4185.md): yea, one shall [ṣāʿaq](../../strongs/h/h6817.md) unto him, yet can he not ['anah](../../strongs/h/h6030.md), nor [yasha'](../../strongs/h/h3467.md) him out of his [tsarah](../../strongs/h/h6869.md).

<a name="isaiah_46_8"></a>Isaiah 46:8

[zakar](../../strongs/h/h2142.md) this, and ['îš](../../strongs/h/h377.md): [shuwb](../../strongs/h/h7725.md) to [leb](../../strongs/h/h3820.md), O ye [pāšaʿ](../../strongs/h/h6586.md).

<a name="isaiah_46_9"></a>Isaiah 46:9

[zakar](../../strongs/h/h2142.md) the [ri'šôn](../../strongs/h/h7223.md) of ['owlam](../../strongs/h/h5769.md): for I am ['el](../../strongs/h/h410.md), and there is none else; I am ['Elohiym](../../strongs/h/h430.md), and there is none [kᵊmô](../../strongs/h/h3644.md) me,

<a name="isaiah_46_10"></a>Isaiah 46:10

[nāḡaḏ](../../strongs/h/h5046.md) the ['aḥărîṯ](../../strongs/h/h319.md) from the [re'shiyth](../../strongs/h/h7225.md), and from [qeḏem](../../strongs/h/h6924.md) the things that are not yet ['asah](../../strongs/h/h6213.md), ['āmar](../../strongs/h/h559.md), My ['etsah](../../strongs/h/h6098.md) shall [quwm](../../strongs/h/h6965.md), and I will ['asah](../../strongs/h/h6213.md) all my [chephets](../../strongs/h/h2656.md):

<a name="isaiah_46_11"></a>Isaiah 46:11

[qara'](../../strongs/h/h7121.md) a [ʿayiṭ](../../strongs/h/h5861.md) from the [mizrach](../../strongs/h/h4217.md), the ['iysh](../../strongs/h/h376.md) that executeth my ['etsah](../../strongs/h/h6098.md) from a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md): yea, I have [dabar](../../strongs/h/h1696.md) it, I will also [bow'](../../strongs/h/h935.md) it to pass; I have [yāṣar](../../strongs/h/h3335.md) it, I will also ['asah](../../strongs/h/h6213.md) it.

<a name="isaiah_46_12"></a>Isaiah 46:12

[shama'](../../strongs/h/h8085.md) unto me, ye ['abîr](../../strongs/h/h47.md) [leb](../../strongs/h/h3820.md), that are [rachowq](../../strongs/h/h7350.md) from [ṣĕdāqāh](../../strongs/h/h6666.md):

<a name="isaiah_46_13"></a>Isaiah 46:13

I [qāraḇ](../../strongs/h/h7126.md) my [ṣĕdāqāh](../../strongs/h/h6666.md); it shall not be [rachaq](../../strongs/h/h7368.md), and my [tᵊšûʿâ](../../strongs/h/h8668.md) shall not ['āḥar](../../strongs/h/h309.md): and I will [nathan](../../strongs/h/h5414.md) [tᵊšûʿâ](../../strongs/h/h8668.md) in [Tsiyown](../../strongs/h/h6726.md) for [Yisra'el](../../strongs/h/h3478.md) my [tip̄'ārâ](../../strongs/h/h8597.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 45](isaiah_45.md) - [Isaiah 47](isaiah_47.md)