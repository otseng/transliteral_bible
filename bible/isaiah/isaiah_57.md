# [Isaiah 57](https://www.blueletterbible.org/kjv/isa/57/1/s_736001)

<a name="isaiah_57_1"></a>Isaiah 57:1

The [tsaddiyq](../../strongs/h/h6662.md) ['abad](../../strongs/h/h6.md), and no ['iysh](../../strongs/h/h376.md) [śûm](../../strongs/h/h7760.md) it to [leb](../../strongs/h/h3820.md): and [checed](../../strongs/h/h2617.md) ['enowsh](../../strongs/h/h582.md) are ['āsap̄](../../strongs/h/h622.md), none considering that the [tsaddiyq](../../strongs/h/h6662.md) is ['āsap̄](../../strongs/h/h622.md) from the [ra'](../../strongs/h/h7451.md) to come.

<a name="isaiah_57_2"></a>Isaiah 57:2

He shall [bow'](../../strongs/h/h935.md) into [shalowm](../../strongs/h/h7965.md): they shall [nuwach](../../strongs/h/h5117.md) in their [miškāḇ](../../strongs/h/h4904.md), each one [halak](../../strongs/h/h1980.md) in his [nāḵōaḥ](../../strongs/h/h5228.md).

<a name="isaiah_57_3"></a>Isaiah 57:3

But [qāraḇ](../../strongs/h/h7126.md) hither, ye [ben](../../strongs/h/h1121.md) of the [ʿānan](../../strongs/h/h6049.md), the [zera'](../../strongs/h/h2233.md) of the [na'aph](../../strongs/h/h5003.md) and the [zānâ](../../strongs/h/h2181.md).

<a name="isaiah_57_4"></a>Isaiah 57:4

Against whom do ye [ʿānaḡ](../../strongs/h/h6026.md) yourselves? against whom make ye a [rāḥaḇ](../../strongs/h/h7337.md) [peh](../../strongs/h/h6310.md), and ['arak](../../strongs/h/h748.md) the [lashown](../../strongs/h/h3956.md)? are ye not [yeleḏ](../../strongs/h/h3206.md) of [pesha'](../../strongs/h/h6588.md), a [zera'](../../strongs/h/h2233.md) of [sheqer](../../strongs/h/h8267.md).

<a name="isaiah_57_5"></a>Isaiah 57:5

[ḥāmam](../../strongs/h/h2552.md) yourselves with ['el](../../strongs/h/h410.md) under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md), [šāḥaṭ](../../strongs/h/h7819.md) the [yeleḏ](../../strongs/h/h3206.md) in the [nachal](../../strongs/h/h5158.md) under the [sᵊʿîp̄](../../strongs/h/h5585.md) of the [cela'](../../strongs/h/h5553.md)?

<a name="isaiah_57_6"></a>Isaiah 57:6

Among the [ḥālāq](../../strongs/h/h2511.md) of the [nachal](../../strongs/h/h5158.md) is thy [cheleq](../../strongs/h/h2506.md); they, they are thy [gôrāl](../../strongs/h/h1486.md): even to them hast thou [šāp̄aḵ](../../strongs/h/h8210.md) a [necek](../../strongs/h/h5262.md), thou hast [ʿālâ](../../strongs/h/h5927.md) a [minchah](../../strongs/h/h4503.md). Should I receive [nacham](../../strongs/h/h5162.md) in these?

<a name="isaiah_57_7"></a>Isaiah 57:7

Upon a [gāḇōha](../../strongs/h/h1364.md) and [nasa'](../../strongs/h/h5375.md) [har](../../strongs/h/h2022.md) hast thou [śûm](../../strongs/h/h7760.md) thy [miškāḇ](../../strongs/h/h4904.md): even thither wentest thou up to [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md).

<a name="isaiah_57_8"></a>Isaiah 57:8

['aḥar](../../strongs/h/h310.md) the [deleṯ](../../strongs/h/h1817.md) also and the [mᵊzûzâ](../../strongs/h/h4201.md) hast thou [śûm](../../strongs/h/h7760.md) thy [zikārôn](../../strongs/h/h2146.md): for thou hast [gālâ](../../strongs/h/h1540.md) thyself to another than me, and art [ʿālâ](../../strongs/h/h5927.md); thou hast [rāḥaḇ](../../strongs/h/h7337.md) thy [miškāḇ](../../strongs/h/h4904.md), and [karath](../../strongs/h/h3772.md) thee with them; thou ['ahab](../../strongs/h/h157.md) their [miškāḇ](../../strongs/h/h4904.md) where thou [chazah](../../strongs/h/h2372.md) it.

<a name="isaiah_57_9"></a>Isaiah 57:9

And thou wentest to the [melek](../../strongs/h/h4428.md) with [šemen](../../strongs/h/h8081.md), and didst [rabah](../../strongs/h/h7235.md) thy [rqḥ](../../strongs/h/h7547.md), and didst send thy [ṣîr](../../strongs/h/h6735.md) [rachowq](../../strongs/h/h7350.md), and didst [šāp̄ēl](../../strongs/h/h8213.md) unto [shĕ'owl](../../strongs/h/h7585.md).

<a name="isaiah_57_10"></a>Isaiah 57:10

Thou art [yaga'](../../strongs/h/h3021.md) in the [rōḇ](../../strongs/h/h7230.md) of thy [derek](../../strongs/h/h1870.md); yet ['āmar](../../strongs/h/h559.md) thou not, There is [yā'aš](../../strongs/h/h2976.md): thou hast [māṣā'](../../strongs/h/h4672.md) the [chay](../../strongs/h/h2416.md) of thine [yad](../../strongs/h/h3027.md); therefore thou wast not [ḥālâ](../../strongs/h/h2470.md).

<a name="isaiah_57_11"></a>Isaiah 57:11

And of whom hast thou been [dā'aḡ](../../strongs/h/h1672.md) or [yare'](../../strongs/h/h3372.md), that thou hast [kāzaḇ](../../strongs/h/h3576.md), and hast not [zakar](../../strongs/h/h2142.md) me, nor [śûm](../../strongs/h/h7760.md) it to thy [leb](../../strongs/h/h3820.md)? have not I [ḥāšâ](../../strongs/h/h2814.md) even of ['owlam](../../strongs/h/h5769.md), and thou [yare'](../../strongs/h/h3372.md) me not?

<a name="isaiah_57_12"></a>Isaiah 57:12

I will [nāḡaḏ](../../strongs/h/h5046.md) thy [ṣĕdāqāh](../../strongs/h/h6666.md), and thy [ma'aseh](../../strongs/h/h4639.md); for they shall not [yāʿal](../../strongs/h/h3276.md) thee.

<a name="isaiah_57_13"></a>Isaiah 57:13

When thou [zāʿaq](../../strongs/h/h2199.md), let thy [qibûṣ](../../strongs/h/h6899.md) [natsal](../../strongs/h/h5337.md) thee; but the [ruwach](../../strongs/h/h7307.md) shall [nasa'](../../strongs/h/h5375.md) them; [heḇel](../../strongs/h/h1892.md) shall [laqach](../../strongs/h/h3947.md) them: but he that [chacah](../../strongs/h/h2620.md) in me shall [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md), and shall [yarash](../../strongs/h/h3423.md) my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md);

<a name="isaiah_57_14"></a>Isaiah 57:14

And shall ['āmar](../../strongs/h/h559.md), [sālal](../../strongs/h/h5549.md), [sālal](../../strongs/h/h5549.md), [panah](../../strongs/h/h6437.md) the [derek](../../strongs/h/h1870.md), [ruwm](../../strongs/h/h7311.md) the [miḵšôl](../../strongs/h/h4383.md) out of the [derek](../../strongs/h/h1870.md) of my ['am](../../strongs/h/h5971.md).

<a name="isaiah_57_15"></a>Isaiah 57:15

For thus ['āmar](../../strongs/h/h559.md) the [ruwm](../../strongs/h/h7311.md) and [nasa'](../../strongs/h/h5375.md) that [shakan](../../strongs/h/h7931.md) [ʿaḏ](../../strongs/h/h5703.md), whose [shem](../../strongs/h/h8034.md) is [qadowsh](../../strongs/h/h6918.md); I [shakan](../../strongs/h/h7931.md) in the [marowm](../../strongs/h/h4791.md) and [qadowsh](../../strongs/h/h6918.md) place, with him also that is of a [dakā'](../../strongs/h/h1793.md) and [šāp̄āl](../../strongs/h/h8217.md) [ruwach](../../strongs/h/h7307.md), to [ḥāyâ](../../strongs/h/h2421.md) the [ruwach](../../strongs/h/h7307.md) of the [šāp̄āl](../../strongs/h/h8217.md), and to [ḥāyâ](../../strongs/h/h2421.md) the [leb](../../strongs/h/h3820.md) of the [dāḵā'](../../strongs/h/h1792.md).

<a name="isaiah_57_16"></a>Isaiah 57:16

For I will not [riyb](../../strongs/h/h7378.md) ['owlam](../../strongs/h/h5769.md), neither will I be [netsach](../../strongs/h/h5331.md) [qāṣap̄](../../strongs/h/h7107.md): for the [ruwach](../../strongs/h/h7307.md) should [ʿāṭap̄](../../strongs/h/h5848.md) before me, and the [neshamah](../../strongs/h/h5397.md) which I have ['asah](../../strongs/h/h6213.md).

<a name="isaiah_57_17"></a>Isaiah 57:17

For the ['avon](../../strongs/h/h5771.md) of his [beṣaʿ](../../strongs/h/h1215.md) was I [qāṣap̄](../../strongs/h/h7107.md), and [nakah](../../strongs/h/h5221.md) him: I [cathar](../../strongs/h/h5641.md) me, and was [qāṣap̄](../../strongs/h/h7107.md), and he [yālaḵ](../../strongs/h/h3212.md) [šôḇāḇ](../../strongs/h/h7726.md) in the [derek](../../strongs/h/h1870.md) of his [leb](../../strongs/h/h3820.md).

<a name="isaiah_57_18"></a>Isaiah 57:18

I have [ra'ah](../../strongs/h/h7200.md) his [derek](../../strongs/h/h1870.md), and will [rapha'](../../strongs/h/h7495.md) him: I will [nachah](../../strongs/h/h5148.md) him also, and [shalam](../../strongs/h/h7999.md) [niḥum](../../strongs/h/h5150.md) unto him and to his ['āḇēl](../../strongs/h/h57.md).

<a name="isaiah_57_19"></a>Isaiah 57:19

I [bara'](../../strongs/h/h1254.md) the [nîḇ](../../strongs/h/h5108.md) of the [saphah](../../strongs/h/h8193.md); [shalowm](../../strongs/h/h7965.md), [shalowm](../../strongs/h/h7965.md) to him that is [rachowq](../../strongs/h/h7350.md), and to him that is [qarowb](../../strongs/h/h7138.md), saith [Yĕhovah](../../strongs/h/h3068.md); and I will [rapha'](../../strongs/h/h7495.md) him.

<a name="isaiah_57_20"></a>Isaiah 57:20

But the [rasha'](../../strongs/h/h7563.md) are like the [gāraš](../../strongs/h/h1644.md) [yam](../../strongs/h/h3220.md), when it cannot [šāqaṭ](../../strongs/h/h8252.md), whose [mayim](../../strongs/h/h4325.md) [gāraš](../../strongs/h/h1644.md) [rep̄eš](../../strongs/h/h7516.md) and [ṭîṭ](../../strongs/h/h2916.md).

<a name="isaiah_57_21"></a>Isaiah 57:21

There is no [shalowm](../../strongs/h/h7965.md), ['āmar](../../strongs/h/h559.md) my ['Elohiym](../../strongs/h/h430.md), to the [rasha'](../../strongs/h/h7563.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 56](isaiah_56.md) - [Isaiah 58](isaiah_58.md)