# [Isaiah 9](https://www.blueletterbible.org/kjv/isa/9/1/s_688001)

<a name="isaiah_9_1"></a>Isaiah 9:1

Nevertheless the [mûʿāp̄](../../strongs/h/h4155.md) shall not be such as was in her [mûṣaq](../../strongs/h/h4164.md), when at the first he [qālal](../../strongs/h/h7043.md) the ['erets](../../strongs/h/h776.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md) and the ['erets](../../strongs/h/h776.md) of [Nap̄tālî](../../strongs/h/h5321.md), and afterward did more [kabad](../../strongs/h/h3513.md) her by the [derek](../../strongs/h/h1870.md) of the [yam](../../strongs/h/h3220.md), beyond [Yardēn](../../strongs/h/h3383.md), in [Gālîl](../../strongs/h/h1551.md) of the [gowy](../../strongs/h/h1471.md).

<a name="isaiah_9_2"></a>Isaiah 9:2

The ['am](../../strongs/h/h5971.md) that [halak](../../strongs/h/h1980.md) in [choshek](../../strongs/h/h2822.md) have [ra'ah](../../strongs/h/h7200.md) a [gadowl](../../strongs/h/h1419.md) ['owr](../../strongs/h/h216.md): they that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of the [ṣalmāveṯ](../../strongs/h/h6757.md), upon them hath the ['owr](../../strongs/h/h216.md) [nāḡahh](../../strongs/h/h5050.md).

<a name="isaiah_9_3"></a>Isaiah 9:3

Thou hast [rabah](../../strongs/h/h7235.md) the [gowy](../../strongs/h/h1471.md), and not [gāḏal](../../strongs/h/h1431.md) the [simchah](../../strongs/h/h8057.md): they [samach](../../strongs/h/h8055.md) before thee according to the [simchah](../../strongs/h/h8057.md) in [qāṣîr](../../strongs/h/h7105.md), and as men [giyl](../../strongs/h/h1523.md) when they [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md).

<a name="isaiah_9_4"></a>Isaiah 9:4

For thou hast [ḥāṯaṯ](../../strongs/h/h2865.md) the [ʿōl](../../strongs/h/h5923.md) of his [sōḇel](../../strongs/h/h5448.md), and the [maṭṭê](../../strongs/h/h4294.md) of his [šᵊḵem](../../strongs/h/h7926.md), the [shebet](../../strongs/h/h7626.md) of his [nāḡaś](../../strongs/h/h5065.md), as in the [yowm](../../strongs/h/h3117.md) of [Miḏyān](../../strongs/h/h4080.md).

<a name="isaiah_9_5"></a>Isaiah 9:5

For every [sᵊ'ôn](../../strongs/h/h5430.md) of the [sā'an](../../strongs/h/h5431.md) is with [raʿaš](../../strongs/h/h7494.md), and [śimlâ](../../strongs/h/h8071.md) [gālal](../../strongs/h/h1556.md) in [dam](../../strongs/h/h1818.md); but this shall be with [śᵊrēp̄â](../../strongs/h/h8316.md) and [ma'ăḵōleṯ](../../strongs/h/h3980.md) of ['esh](../../strongs/h/h784.md).

<a name="isaiah_9_6"></a>Isaiah 9:6

For unto us a [yeleḏ](../../strongs/h/h3206.md) is [yalad](../../strongs/h/h3205.md), unto us a [ben](../../strongs/h/h1121.md) is [nathan](../../strongs/h/h5414.md): and the [miśrâ](../../strongs/h/h4951.md) shall be upon his [šᵊḵem](../../strongs/h/h7926.md): and his [shem](../../strongs/h/h8034.md) shall be [qara'](../../strongs/h/h7121.md) [pele'](../../strongs/h/h6382.md), [ya'ats](../../strongs/h/h3289.md), The [gibôr](../../strongs/h/h1368.md) ['el](../../strongs/h/h410.md), The [ʿaḏ](../../strongs/h/h5703.md) ['ab](../../strongs/h/h1.md), The [śar](../../strongs/h/h8269.md) of [shalowm](../../strongs/h/h7965.md).

<a name="isaiah_9_7"></a>Isaiah 9:7

Of the [marbê](../../strongs/h/h4766.md) of his [miśrâ](../../strongs/h/h4951.md) and [shalowm](../../strongs/h/h7965.md) there shall be no [qēṣ](../../strongs/h/h7093.md), upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), and upon his [mamlāḵâ](../../strongs/h/h4467.md), to [kuwn](../../strongs/h/h3559.md) it, and to [sāʿaḏ](../../strongs/h/h5582.md) it with [mishpat](../../strongs/h/h4941.md) and with [tsedaqah](../../strongs/h/h6666.md) from henceforth even for ever. The [qin'â](../../strongs/h/h7068.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) will ['asah](../../strongs/h/h6213.md) this.

<a name="isaiah_9_8"></a>Isaiah 9:8

['adonay](../../strongs/h/h136.md) sent a [dabar](../../strongs/h/h1697.md) into [Ya'aqob](../../strongs/h/h3290.md), and it hath [naphal](../../strongs/h/h5307.md) upon [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_9_9"></a>Isaiah 9:9

And all the ['am](../../strongs/h/h5971.md) shall [yada'](../../strongs/h/h3045.md), even ['Ep̄rayim](../../strongs/h/h669.md) and the inhabitant of [Šōmrôn](../../strongs/h/h8111.md), that ['āmar](../../strongs/h/h559.md) in the [ga'avah](../../strongs/h/h1346.md) and [gōḏel](../../strongs/h/h1433.md) of [lebab](../../strongs/h/h3824.md),

<a name="isaiah_9_10"></a>Isaiah 9:10

The [lᵊḇēnâ](../../strongs/h/h3843.md) are [naphal](../../strongs/h/h5307.md), but we will [bānâ](../../strongs/h/h1129.md) with [gāzîṯ](../../strongs/h/h1496.md): the [šiqmâ](../../strongs/h/h8256.md) are [gāḏaʿ](../../strongs/h/h1438.md), but we will [ḥālap̄](../../strongs/h/h2498.md) them into ['erez](../../strongs/h/h730.md).

<a name="isaiah_9_11"></a>Isaiah 9:11

Therefore [Yĕhovah](../../strongs/h/h3068.md) shall [śāḡaḇ](../../strongs/h/h7682.md) the [tsar](../../strongs/h/h6862.md) of [Rᵊṣîn](../../strongs/h/h7526.md) against him, and [cakak](../../strongs/h/h5526.md) his ['oyeb](../../strongs/h/h341.md) together;

<a name="isaiah_9_12"></a>Isaiah 9:12

The ['Ărām](../../strongs/h/h758.md) before, and the [Pᵊlištî](../../strongs/h/h6430.md) behind; and they shall ['akal](../../strongs/h/h398.md) [Yisra'el](../../strongs/h/h3478.md) with [peh](../../strongs/h/h6310.md). For all this his ['aph](../../strongs/h/h639.md) is not [shuwb](../../strongs/h/h7725.md), but his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md).

<a name="isaiah_9_13"></a>Isaiah 9:13

For the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) not unto him that [nakah](../../strongs/h/h5221.md) them, neither do they [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_9_14"></a>Isaiah 9:14

Therefore [Yĕhovah](../../strongs/h/h3068.md) will [karath](../../strongs/h/h3772.md) from [Yisra'el](../../strongs/h/h3478.md) [ro'sh](../../strongs/h/h7218.md) and [zānāḇ](../../strongs/h/h2180.md), [kipâ](../../strongs/h/h3712.md) and ['aḡmôn](../../strongs/h/h100.md), in ['echad](../../strongs/h/h259.md) [yowm](../../strongs/h/h3117.md).

<a name="isaiah_9_15"></a>Isaiah 9:15

The [zāqēn](../../strongs/h/h2205.md) and [paniym](../../strongs/h/h6440.md) [nasa'](../../strongs/h/h5375.md), he is the [ro'sh](../../strongs/h/h7218.md); and the [nāḇî'](../../strongs/h/h5030.md) that [yārâ](../../strongs/h/h3384.md) [sheqer](../../strongs/h/h8267.md), he is the [zānāḇ](../../strongs/h/h2180.md).

<a name="isaiah_9_16"></a>Isaiah 9:16

For the ['āšar](../../strongs/h/h833.md) of this ['am](../../strongs/h/h5971.md) cause them to [tāʿâ](../../strongs/h/h8582.md); and they that are ['āšar](../../strongs/h/h833.md) of them are [bālaʿ](../../strongs/h/h1104.md).

<a name="isaiah_9_17"></a>Isaiah 9:17

Therefore ['adonay](../../strongs/h/h136.md) shall have no [samach](../../strongs/h/h8055.md) in their [bāḥûr](../../strongs/h/h970.md), neither shall have [racham](../../strongs/h/h7355.md) on their [yathowm](../../strongs/h/h3490.md) and ['almānâ](../../strongs/h/h490.md): for every one is an [ḥānēp̄](../../strongs/h/h2611.md) and a [ra'a'](../../strongs/h/h7489.md), and every [peh](../../strongs/h/h6310.md) speaketh [nᵊḇālâ](../../strongs/h/h5039.md). For all this his ['aph](../../strongs/h/h639.md) is not [shuwb](../../strongs/h/h7725.md), but his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md).

<a name="isaiah_9_18"></a>Isaiah 9:18

For [rišʿâ](../../strongs/h/h7564.md) [bāʿar](../../strongs/h/h1197.md) as the ['esh](../../strongs/h/h784.md): it shall ['akal](../../strongs/h/h398.md) the [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md), and shall [yāṣaṯ](../../strongs/h/h3341.md) in the [sᵊḇāḵ](../../strongs/h/h5442.md) of the [yaʿar](../../strongs/h/h3293.md), and they shall ['āḇaḵ](../../strongs/h/h55.md) like the [ge'uwth](../../strongs/h/h1348.md) of ['ashan](../../strongs/h/h6227.md).

<a name="isaiah_9_19"></a>Isaiah 9:19

Through the ['ebrah](../../strongs/h/h5678.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is the ['erets](../../strongs/h/h776.md) [ʿāṯam](../../strongs/h/h6272.md), and the ['am](../../strongs/h/h5971.md) shall be as the [ma'ăḵōleṯ](../../strongs/h/h3980.md) of the ['esh](../../strongs/h/h784.md): no ['iysh](../../strongs/h/h376.md) shall [ḥāmal](../../strongs/h/h2550.md) his ['ach](../../strongs/h/h251.md).

<a name="isaiah_9_20"></a>Isaiah 9:20

And he shall [gāzar](../../strongs/h/h1504.md) on the [yamiyn](../../strongs/h/h3225.md), and be [rāʿēḇ](../../strongs/h/h7457.md); and he shall ['akal](../../strongs/h/h398.md) on the [śᵊmō'l](../../strongs/h/h8040.md), and they shall not be [sāׂbaʿ](../../strongs/h/h7646.md): they shall ['akal](../../strongs/h/h398.md) every ['iysh](../../strongs/h/h376.md) the [basar](../../strongs/h/h1320.md) of his own [zerowa'](../../strongs/h/h2220.md):

<a name="isaiah_9_21"></a>Isaiah 9:21

[Mᵊnaššê](../../strongs/h/h4519.md), ['Ep̄rayim](../../strongs/h/h669.md); and ['Ep̄rayim](../../strongs/h/h669.md), [Mᵊnaššê](../../strongs/h/h4519.md): and they together shall be against [Yehuwdah](../../strongs/h/h3063.md). For all this his ['aph](../../strongs/h/h639.md) is not [shuwb](../../strongs/h/h7725.md), but his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 8](isaiah_8.md) - [Isaiah 10](isaiah_10.md)