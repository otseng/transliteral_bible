# [Isaiah 23](https://www.blueletterbible.org/kjv/isa/23/1/s_702001)

<a name="isaiah_23_1"></a>Isaiah 23:1

The [maśśā'](../../strongs/h/h4853.md) of [Ṣōr](../../strongs/h/h6865.md). [yālal](../../strongs/h/h3213.md), ye ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md); for it is [shadad](../../strongs/h/h7703.md), so that there is no [bayith](../../strongs/h/h1004.md), no [bow'](../../strongs/h/h935.md): from the ['erets](../../strongs/h/h776.md) of [Kitîm](../../strongs/h/h3794.md) it is [gālâ](../../strongs/h/h1540.md) to them.

<a name="isaiah_23_2"></a>Isaiah 23:2

[damam](../../strongs/h/h1826.md), ye [yashab](../../strongs/h/h3427.md) of the ['î](../../strongs/h/h339.md); thou whom the [sāḥar](../../strongs/h/h5503.md) of [Ṣîḏôn](../../strongs/h/h6721.md), that ['abar](../../strongs/h/h5674.md) the [yam](../../strongs/h/h3220.md), have [mālā'](../../strongs/h/h4390.md).

<a name="isaiah_23_3"></a>Isaiah 23:3

And by [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) the [zera'](../../strongs/h/h2233.md) of [šîḥôr](../../strongs/h/h7883.md), the [qāṣîr](../../strongs/h/h7105.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), is her [tᵊḇû'â](../../strongs/h/h8393.md); and she is a [sāḥār](../../strongs/h/h5505.md) of [gowy](../../strongs/h/h1471.md).

<a name="isaiah_23_4"></a>Isaiah 23:4

Be thou [buwsh](../../strongs/h/h954.md), O [Ṣîḏôn](../../strongs/h/h6721.md): for the [yam](../../strongs/h/h3220.md) hath ['āmar](../../strongs/h/h559.md), even the [māʿôz](../../strongs/h/h4581.md) of the [yam](../../strongs/h/h3220.md), saying, I [chuwl](../../strongs/h/h2342.md) not, nor [yalad](../../strongs/h/h3205.md), neither do I [gāḏal](../../strongs/h/h1431.md) [bāḥûr](../../strongs/h/h970.md), nor bring up [bᵊṯûlâ](../../strongs/h/h1330.md).

<a name="isaiah_23_5"></a>Isaiah 23:5

As at the [šēmaʿ](../../strongs/h/h8088.md) concerning [Mitsrayim](../../strongs/h/h4714.md), so shall they be [chuwl](../../strongs/h/h2342.md) at the [šēmaʿ](../../strongs/h/h8088.md) of [Ṣōr](../../strongs/h/h6865.md).

<a name="isaiah_23_6"></a>Isaiah 23:6

['abar](../../strongs/h/h5674.md) to [Taršîš](../../strongs/h/h8659.md); [yālal](../../strongs/h/h3213.md), ye [yashab](../../strongs/h/h3427.md) of the ['î](../../strongs/h/h339.md).

<a name="isaiah_23_7"></a>Isaiah 23:7

Is this your [ʿallîz](../../strongs/h/h5947.md) city, whose [qaḏmâ](../../strongs/h/h6927.md) is of [qeḏem](../../strongs/h/h6924.md) [yowm](../../strongs/h/h3117.md)? her own [regel](../../strongs/h/h7272.md) shall [yāḇal](../../strongs/h/h2986.md) her [rachowq](../../strongs/h/h7350.md) to [guwr](../../strongs/h/h1481.md).

<a name="isaiah_23_8"></a>Isaiah 23:8

Who hath taken this [ya'ats](../../strongs/h/h3289.md) against [Ṣōr](../../strongs/h/h6865.md), the ['atar](../../strongs/h/h5849.md) city, whose [sāḥar](../../strongs/h/h5503.md) are [śar](../../strongs/h/h8269.md), whose [kĕna'an](../../strongs/h/h3667.md) are the [kabad](../../strongs/h/h3513.md) of the ['erets](../../strongs/h/h776.md)?

<a name="isaiah_23_9"></a>Isaiah 23:9

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [ya'ats](../../strongs/h/h3289.md) it, to [ḥālal](../../strongs/h/h2490.md) the [gā'ôn](../../strongs/h/h1347.md) of all [ṣᵊḇî](../../strongs/h/h6643.md), and to bring into [qālal](../../strongs/h/h7043.md) all the [kabad](../../strongs/h/h3513.md) of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_23_10"></a>Isaiah 23:10

['abar](../../strongs/h/h5674.md) through thy ['erets](../../strongs/h/h776.md) as a [yᵊ'ōr](../../strongs/h/h2975.md), O [bath](../../strongs/h/h1323.md) of [Taršîš](../../strongs/h/h8659.md): there is no more [mᵊzîaḥ](../../strongs/h/h4206.md).

<a name="isaiah_23_11"></a>Isaiah 23:11

He [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md) over the [yam](../../strongs/h/h3220.md), he [ragaz](../../strongs/h/h7264.md) the [mamlāḵâ](../../strongs/h/h4467.md): [Yĕhovah](../../strongs/h/h3068.md) hath given a [tsavah](../../strongs/h/h6680.md) against the [kĕna'an](../../strongs/h/h3667.md) city, to [šāmaḏ](../../strongs/h/h8045.md) the [māʿôz](../../strongs/h/h4581.md) [māʿôz](../../strongs/h/h4581.md) thereof.

<a name="isaiah_23_12"></a>Isaiah 23:12

And he ['āmar](../../strongs/h/h559.md), Thou shalt no more [ʿālaz](../../strongs/h/h5937.md), O thou [ʿāšaq](../../strongs/h/h6231.md) [bᵊṯûlâ](../../strongs/h/h1330.md), [bath](../../strongs/h/h1323.md) of [Ṣîḏôn](../../strongs/h/h6721.md): [quwm](../../strongs/h/h6965.md), ['abar](../../strongs/h/h5674.md) to [Kitîm](../../strongs/h/h3794.md); there also shalt thou have no [nuwach](../../strongs/h/h5117.md).

<a name="isaiah_23_13"></a>Isaiah 23:13

Behold the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md); this ['am](../../strongs/h/h5971.md) was not, till the ['Aššûr](../../strongs/h/h804.md) [yacad](../../strongs/h/h3245.md) it for them that dwell in the [ṣîyî](../../strongs/h/h6728.md): they [quwm](../../strongs/h/h6965.md) the [baḥîn](../../strongs/h/h971.md) thereof, they [ʿārar](../../strongs/h/h6209.md) the ['armôn](../../strongs/h/h759.md) thereof; and he [śûm](../../strongs/h/h7760.md) it to [mapālâ](../../strongs/h/h4654.md).

<a name="isaiah_23_14"></a>Isaiah 23:14

[yālal](../../strongs/h/h3213.md), ye ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md): for your [māʿôz](../../strongs/h/h4581.md) is [shadad](../../strongs/h/h7703.md).

<a name="isaiah_23_15"></a>Isaiah 23:15

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that [Ṣōr](../../strongs/h/h6865.md) shall be [shakach](../../strongs/h/h7911.md) [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md), according to the [yowm](../../strongs/h/h3117.md) of ['echad](../../strongs/h/h259.md) [melek](../../strongs/h/h4428.md): after the [qēṣ](../../strongs/h/h7093.md) of [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md) shall [šîr](../../strongs/h/h7892.md) as a [zānâ](../../strongs/h/h2181.md).

<a name="isaiah_23_16"></a>Isaiah 23:16

[laqach](../../strongs/h/h3947.md) a [kinnôr](../../strongs/h/h3658.md), go about the [ʿîr](../../strongs/h/h5892.md), thou [zānâ](../../strongs/h/h2181.md) that hast been [shakach](../../strongs/h/h7911.md); make [yatab](../../strongs/h/h3190.md) [nāḡan](../../strongs/h/h5059.md), sing many [šîr](../../strongs/h/h7892.md), that thou mayest be [zakar](../../strongs/h/h2142.md).

<a name="isaiah_23_17"></a>Isaiah 23:17

And it shall come to pass after the [qēṣ](../../strongs/h/h7093.md) of [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md), that [Yĕhovah](../../strongs/h/h3068.md) will visit [Ṣōr](../../strongs/h/h6865.md), and she shall turn to her ['eṯnan](../../strongs/h/h868.md), and shall [zānâ](../../strongs/h/h2181.md) with all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="isaiah_23_18"></a>Isaiah 23:18

And her [saḥar](../../strongs/h/h5504.md) and her ['eṯnan](../../strongs/h/h868.md) shall be [qodesh](../../strongs/h/h6944.md) to [Yĕhovah](../../strongs/h/h3068.md): it shall not be ['āṣar](../../strongs/h/h686.md) nor [ḥāsan](../../strongs/h/h2630.md); for her [saḥar](../../strongs/h/h5504.md) shall be for them that [yashab](../../strongs/h/h3427.md) before [Yĕhovah](../../strongs/h/h3068.md), to ['akal](../../strongs/h/h398.md) [śāḇʿâ](../../strongs/h/h7654.md), and for [ʿāṯîq](../../strongs/h/h6266.md) [mᵊḵassê](../../strongs/h/h4374.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 22](isaiah_22.md) - [Isaiah 24](isaiah_24.md)