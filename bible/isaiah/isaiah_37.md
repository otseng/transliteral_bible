# [Isaiah 37](https://www.blueletterbible.org/kjv/isa/37/1/s_716001)

<a name="isaiah_37_1"></a>Isaiah 37:1

And it came to pass, when [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md) [shama'](../../strongs/h/h8085.md) it, that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and [kāsâ](../../strongs/h/h3680.md) himself with [śaq](../../strongs/h/h8242.md), and went into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_37_2"></a>Isaiah 37:2

And he [shalach](../../strongs/h/h7971.md) ['Elyāqîm](../../strongs/h/h471.md), who was over the [bayith](../../strongs/h/h1004.md), and [Šeḇnā'](../../strongs/h/h7644.md) the [sāp̄ar](../../strongs/h/h5608.md), and the [zāqēn](../../strongs/h/h2205.md) of the [kōhēn](../../strongs/h/h3548.md) [kāsâ](../../strongs/h/h3680.md) with [śaq](../../strongs/h/h8242.md), unto [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md).

<a name="isaiah_37_3"></a>Isaiah 37:3

And they ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Ḥizqîyâ](../../strongs/h/h2396.md), This [yowm](../../strongs/h/h3117.md) is a [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md), and of [tôḵēḥâ](../../strongs/h/h8433.md), and of [ne'āṣâ](../../strongs/h/h5007.md): for the [ben](../../strongs/h/h1121.md) are [bow'](../../strongs/h/h935.md) to the [mašbēr](../../strongs/h/h4866.md), and there is not [koach](../../strongs/h/h3581.md) to [yalad](../../strongs/h/h3205.md).

<a name="isaiah_37_4"></a>Isaiah 37:4

It may be [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Raḇ-šāqê](../../strongs/h/h7262.md), whom the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) his ['adown](../../strongs/h/h113.md) hath [shalach](../../strongs/h/h7971.md) to [ḥārap̄](../../strongs/h/h2778.md) the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md), and will [yakach](../../strongs/h/h3198.md) the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [shama'](../../strongs/h/h8085.md): wherefore [nasa'](../../strongs/h/h5375.md) thy [tĕphillah](../../strongs/h/h8605.md) for the [šᵊ'ērîṯ](../../strongs/h/h7611.md) that is [māṣā'](../../strongs/h/h4672.md).

<a name="isaiah_37_5"></a>Isaiah 37:5

So the ['ebed](../../strongs/h/h5650.md) of [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md) [bow'](../../strongs/h/h935.md) to [Yᵊšaʿyâ](../../strongs/h/h3470.md).

<a name="isaiah_37_6"></a>Isaiah 37:6

And [Yᵊšaʿyâ](../../strongs/h/h3470.md) ['āmar](../../strongs/h/h559.md) unto them, Thus shall ye ['āmar](../../strongs/h/h559.md) unto your ['adown](../../strongs/h/h113.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Be not [yare'](../../strongs/h/h3372.md) of the [dabar](../../strongs/h/h1697.md) that thou hast [shama'](../../strongs/h/h8085.md), wherewith the [naʿar](../../strongs/h/h5288.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) have [gāḏap̄](../../strongs/h/h1442.md) me.

<a name="isaiah_37_7"></a>Isaiah 37:7

Behold, I will [nathan](../../strongs/h/h5414.md) a [ruwach](../../strongs/h/h7307.md) upon him, and he shall [shama'](../../strongs/h/h8085.md) a [šᵊmûʿâ](../../strongs/h/h8052.md), and [shuwb](../../strongs/h/h7725.md) to his own ['erets](../../strongs/h/h776.md); and I will cause him to [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md) in his own ['erets](../../strongs/h/h776.md).

<a name="isaiah_37_8"></a>Isaiah 37:8

So [Raḇ-šāqê](../../strongs/h/h7262.md) [shuwb](../../strongs/h/h7725.md), and [māṣā'](../../strongs/h/h4672.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [lāḥam](../../strongs/h/h3898.md) against [Liḇnâ](../../strongs/h/h3841.md): for he had [shama'](../../strongs/h/h8085.md) that he was [nāsaʿ](../../strongs/h/h5265.md) from [Lāḵîš](../../strongs/h/h3923.md).

<a name="isaiah_37_9"></a>Isaiah 37:9

And he [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md) concerning [Tirhăqâ](../../strongs/h/h8640.md) [melek](../../strongs/h/h4428.md) of [Kûš](../../strongs/h/h3568.md), He is [yāṣā'](../../strongs/h/h3318.md) to [lāḥam](../../strongs/h/h3898.md) with thee. And when he [shama'](../../strongs/h/h8085.md) it, he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Ḥizqîyâ](../../strongs/h/h2396.md), ['āmar](../../strongs/h/h559.md),

<a name="isaiah_37_10"></a>Isaiah 37:10

Thus shall ye speak to [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), saying, Let not thy ['Elohiym](../../strongs/h/h430.md), in whom thou [batach](../../strongs/h/h982.md), [nasha'](../../strongs/h/h5377.md) thee, saying, [Yĕruwshalaim](../../strongs/h/h3389.md) shall not be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_37_11"></a>Isaiah 37:11

Behold, thou hast [shama'](../../strongs/h/h8085.md) what the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) have done to all ['erets](../../strongs/h/h776.md) by [ḥāram](../../strongs/h/h2763.md) them; and shalt thou be [natsal](../../strongs/h/h5337.md)?

<a name="isaiah_37_12"></a>Isaiah 37:12

Have the ['Elohiym](../../strongs/h/h430.md) of the [gowy](../../strongs/h/h1471.md) [natsal](../../strongs/h/h5337.md) them which my ['ab](../../strongs/h/h1.md) have [shachath](../../strongs/h/h7843.md), as [Gôzān](../../strongs/h/h1470.md), and [Ḥārān](../../strongs/h/h2771.md), and [reṣep̄](../../strongs/h/h7530.md), and the [ben](../../strongs/h/h1121.md) of [ʿEḏen](../../strongs/h/h5729.md) which were in [Tᵊla'śśār](../../strongs/h/h8515.md)?

<a name="isaiah_37_13"></a>Isaiah 37:13

Where is the [melek](../../strongs/h/h4428.md) of [Ḥămāṯ](../../strongs/h/h2574.md), and the [melek](../../strongs/h/h4428.md) of ['Arpāḏ](../../strongs/h/h774.md), and the [melek](../../strongs/h/h4428.md) of the [ʿîr](../../strongs/h/h5892.md) of [Sᵊp̄arvayim](../../strongs/h/h5617.md), [Hēnaʿ](../../strongs/h/h2012.md), and [ʿIûâ](../../strongs/h/h5755.md)?

<a name="isaiah_37_14"></a>Isaiah 37:14

And [Ḥizqîyâ](../../strongs/h/h2396.md) [laqach](../../strongs/h/h3947.md) the [sēp̄er](../../strongs/h/h5612.md) from the [yad](../../strongs/h/h3027.md) of the [mal'ak](../../strongs/h/h4397.md), and [qara'](../../strongs/h/h7121.md) it: and [Ḥizqîyâ](../../strongs/h/h2396.md) [ʿālâ](../../strongs/h/h5927.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [pāraś](../../strongs/h/h6566.md) it before [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_37_15"></a>Isaiah 37:15

And [Ḥizqîyâ](../../strongs/h/h2396.md) [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="isaiah_37_16"></a>Isaiah 37:16

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), that [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md), thou art the ['Elohiym](../../strongs/h/h430.md), even thou alone, of all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md): thou hast made [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md).

<a name="isaiah_37_17"></a>Isaiah 37:17

[natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md), [Yĕhovah](../../strongs/h/h3068.md), and [shama'](../../strongs/h/h8085.md); [paqach](../../strongs/h/h6491.md) thine ['ayin](../../strongs/h/h5869.md), [Yĕhovah](../../strongs/h/h3068.md), and [ra'ah](../../strongs/h/h7200.md): and [shama'](../../strongs/h/h8085.md) all the [dabar](../../strongs/h/h1697.md) of [Sanḥērîḇ](../../strongs/h/h5576.md), which hath [shalach](../../strongs/h/h7971.md) to [ḥārap̄](../../strongs/h/h2778.md) the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_37_18"></a>Isaiah 37:18

['āmnām](../../strongs/h/h551.md), [Yĕhovah](../../strongs/h/h3068.md), the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) have [ḥāraḇ](../../strongs/h/h2717.md) all the ['erets](../../strongs/h/h776.md), and their ['erets](../../strongs/h/h776.md),

<a name="isaiah_37_19"></a>Isaiah 37:19

And have [nathan](../../strongs/h/h5414.md) their ['Elohiym](../../strongs/h/h430.md) into the ['esh](../../strongs/h/h784.md): for they were no ['Elohiym](../../strongs/h/h430.md), but the [ma'aseh](../../strongs/h/h4639.md) of ['adam](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md), ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md): therefore they have ['abad](../../strongs/h/h6.md) them.

<a name="isaiah_37_20"></a>Isaiah 37:20

Now therefore, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), [yasha'](../../strongs/h/h3467.md) us from his [yad](../../strongs/h/h3027.md), that all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) may [yada'](../../strongs/h/h3045.md) that thou art [Yĕhovah](../../strongs/h/h3068.md), even thou only.

<a name="isaiah_37_21"></a>Isaiah 37:21

Then [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md) [shalach](../../strongs/h/h7971.md) unto [Ḥizqîyâ](../../strongs/h/h2396.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Whereas thou hast [palal](../../strongs/h/h6419.md) to me against [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md):

<a name="isaiah_37_22"></a>Isaiah 37:22

This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) concerning him; The [bᵊṯûlâ](../../strongs/h/h1330.md), the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), hath [bazah](../../strongs/h/h959.md) thee, and [lāʿaḡ](../../strongs/h/h3932.md); the [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) hath [nûaʿ](../../strongs/h/h5128.md) her [ro'sh](../../strongs/h/h7218.md) at thee.

<a name="isaiah_37_23"></a>Isaiah 37:23

Whom hast thou [ḥārap̄](../../strongs/h/h2778.md) and [gāḏap̄](../../strongs/h/h1442.md)? and against whom hast thou [ruwm](../../strongs/h/h7311.md) thy [qowl](../../strongs/h/h6963.md), and [nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) on high? even against the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_37_24"></a>Isaiah 37:24

By thy ['ebed](../../strongs/h/h5650.md) hast thou [ḥārap̄](../../strongs/h/h2778.md) ['adonay](../../strongs/h/h136.md), and hast ['āmar](../../strongs/h/h559.md), By the [rōḇ](../../strongs/h/h7230.md) of my [reḵeḇ](../../strongs/h/h7393.md) am I [ʿālâ](../../strongs/h/h5927.md) to the [marowm](../../strongs/h/h4791.md) of the [har](../../strongs/h/h2022.md), to the [yᵊrēḵâ](../../strongs/h/h3411.md) of [Lᵊḇānôn](../../strongs/h/h3844.md); and I will [karath](../../strongs/h/h3772.md) the [qômâ](../../strongs/h/h6967.md) ['erez](../../strongs/h/h730.md) thereof, and the [miḇḥār](../../strongs/h/h4005.md) [bᵊrôš](../../strongs/h/h1265.md) thereof: and I will [bow'](../../strongs/h/h935.md) into the [marowm](../../strongs/h/h4791.md) of his [qēṣ](../../strongs/h/h7093.md), and the [yaʿar](../../strongs/h/h3293.md) of his [karmel](../../strongs/h/h3760.md).

<a name="isaiah_37_25"></a>Isaiah 37:25

I have [qûr](../../strongs/h/h6979.md), and [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md); and with the [kaph](../../strongs/h/h3709.md) of my [pa'am](../../strongs/h/h6471.md) have I [ḥāraḇ](../../strongs/h/h2717.md) all the [yᵊ'ōr](../../strongs/h/h2975.md) of the [māṣôr](../../strongs/h/h4693.md).

<a name="isaiah_37_26"></a>Isaiah 37:26

Hast thou not [shama'](../../strongs/h/h8085.md) long ago, how I have ['asah](../../strongs/h/h6213.md) it; and of [qeḏem](../../strongs/h/h6924.md) [yowm](../../strongs/h/h3117.md), that I have [yāṣar](../../strongs/h/h3335.md) it? now have I [bow'](../../strongs/h/h935.md) it to pass, that thou shouldest be to [šā'â](../../strongs/h/h7582.md) [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) into [nāṣâ](../../strongs/h/h5327.md) [gal](../../strongs/h/h1530.md).

<a name="isaiah_37_27"></a>Isaiah 37:27

Therefore their [yashab](../../strongs/h/h3427.md) were of [qāṣēr](../../strongs/h/h7116.md) [yad](../../strongs/h/h3027.md), they were [ḥāṯaṯ](../../strongs/h/h2865.md) and [buwsh](../../strongs/h/h954.md): they were as the ['eseb](../../strongs/h/h6212.md) of the [sadeh](../../strongs/h/h7704.md), and as the [yārāq](../../strongs/h/h3419.md) [deše'](../../strongs/h/h1877.md), as the [chatsiyr](../../strongs/h/h2682.md) on the [gāḡ](../../strongs/h/h1406.md), and as [šᵊḏēmâ](../../strongs/h/h7709.md) before it be [qāmâ](../../strongs/h/h7054.md).

<a name="isaiah_37_28"></a>Isaiah 37:28

But I [yada'](../../strongs/h/h3045.md) thy [yashab](../../strongs/h/h3427.md), and thy [yāṣā'](../../strongs/h/h3318.md), and thy [bow'](../../strongs/h/h935.md), and thy [ragaz](../../strongs/h/h7264.md) against me.

<a name="isaiah_37_29"></a>Isaiah 37:29

Because thy [ragaz](../../strongs/h/h7264.md) against me, and thy [ša'ănān](../../strongs/h/h7600.md), is [ʿālâ](../../strongs/h/h5927.md) into mine ['ozen](../../strongs/h/h241.md), therefore will I put my [ḥāḥ](../../strongs/h/h2397.md) in thy ['aph](../../strongs/h/h639.md), and my [meṯeḡ](../../strongs/h/h4964.md) in thy [saphah](../../strongs/h/h8193.md), and I will [shuwb](../../strongs/h/h7725.md) by the [derek](../../strongs/h/h1870.md) by which thou [bow'](../../strongs/h/h935.md).

<a name="isaiah_37_30"></a>Isaiah 37:30

And this shall be an ['ôṯ](../../strongs/h/h226.md) unto thee, Ye shall ['akal](../../strongs/h/h398.md) this [šānâ](../../strongs/h/h8141.md) such as [sāp̄îaḥ](../../strongs/h/h5599.md); and the second [šānâ](../../strongs/h/h8141.md) that which [šāḥîs](../../strongs/h/h7823.md) of the same: and in the third [šānâ](../../strongs/h/h8141.md) [zāraʿ](../../strongs/h/h2232.md) ye, and [qāṣar](../../strongs/h/h7114.md), and [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) thereof.

<a name="isaiah_37_31"></a>Isaiah 37:31

And the [šā'ar](../../strongs/h/h7604.md) that is [pᵊlêṭâ](../../strongs/h/h6413.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) shall again [šereš](../../strongs/h/h8328.md) [maṭṭâ](../../strongs/h/h4295.md), and ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md) upward:

<a name="isaiah_37_32"></a>Isaiah 37:32

For out of [Yĕruwshalaim](../../strongs/h/h3389.md) shall [yāṣā'](../../strongs/h/h3318.md) a [šᵊ'ērîṯ](../../strongs/h/h7611.md), and they that [pᵊlêṭâ](../../strongs/h/h6413.md) out of [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md): the [qin'â](../../strongs/h/h7068.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall ['asah](../../strongs/h/h6213.md) this.

<a name="isaiah_37_33"></a>Isaiah 37:33

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), He shall not [bow'](../../strongs/h/h935.md) into this [ʿîr](../../strongs/h/h5892.md), nor [yārâ](../../strongs/h/h3384.md) a [chets](../../strongs/h/h2671.md) there, nor [qadam](../../strongs/h/h6923.md) it with [magen](../../strongs/h/h4043.md), nor [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md) against it.

<a name="isaiah_37_34"></a>Isaiah 37:34

By the [derek](../../strongs/h/h1870.md) that he [bow'](../../strongs/h/h935.md), by the same shall he [shuwb](../../strongs/h/h7725.md), and shall not [bow'](../../strongs/h/h935.md) into this [ʿîr](../../strongs/h/h5892.md), saith [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_37_35"></a>Isaiah 37:35

For I will [gānan](../../strongs/h/h1598.md) this [ʿîr](../../strongs/h/h5892.md) to [yasha'](../../strongs/h/h3467.md) it for mine own sake, and for my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) sake.

<a name="isaiah_37_36"></a>Isaiah 37:36

Then the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md), and [nakah](../../strongs/h/h5221.md) in the [maḥănê](../../strongs/h/h4264.md) of the ['Aššûr](../../strongs/h/h804.md) a hundred and fourscore and five thousand: and when they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), behold, they were all [muwth](../../strongs/h/h4191.md) [peḡer](../../strongs/h/h6297.md).

<a name="isaiah_37_37"></a>Isaiah 37:37

So [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [nāsaʿ](../../strongs/h/h5265.md), and [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md), and [yashab](../../strongs/h/h3427.md) at [Nînvê](../../strongs/h/h5210.md).

<a name="isaiah_37_38"></a>Isaiah 37:38

And it came to pass, as he was [shachah](../../strongs/h/h7812.md) in the [bayith](../../strongs/h/h1004.md) of [Nisrōḵ](../../strongs/h/h5268.md) his ['Elohiym](../../strongs/h/h430.md), that ['Aḏrammeleḵ](../../strongs/h/h152.md) and [Šar'eṣer](../../strongs/h/h8272.md) his sons [nakah](../../strongs/h/h5221.md) him with the [chereb](../../strongs/h/h2719.md); and they [mālaṭ](../../strongs/h/h4422.md) into the ['erets](../../strongs/h/h776.md) of ['Ărārāṭ](../../strongs/h/h780.md): and ['Ēsar-ḥadôn](../../strongs/h/h634.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 36](isaiah_36.md) - [Isaiah 38](isaiah_38.md)