# [Isaiah 52](https://www.blueletterbible.org/kjv/isa/52/1/s_731001)

<a name="isaiah_52_1"></a>Isaiah 52:1

[ʿûr](../../strongs/h/h5782.md), [ʿûr](../../strongs/h/h5782.md); put on thy ['oz](../../strongs/h/h5797.md), O [Tsiyown](../../strongs/h/h6726.md); put on thy [tip̄'ārâ](../../strongs/h/h8597.md) [beḡeḏ](../../strongs/h/h899.md), O [Yĕruwshalaim](../../strongs/h/h3389.md), the [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md): for henceforth there shall no more come into thee the [ʿārēl](../../strongs/h/h6189.md) and the [tame'](../../strongs/h/h2931.md).

<a name="isaiah_52_2"></a>Isaiah 52:2

[nāʿar](../../strongs/h/h5287.md) thyself from the ['aphar](../../strongs/h/h6083.md); [quwm](../../strongs/h/h6965.md), and [yashab](../../strongs/h/h3427.md), O [Yĕruwshalaim](../../strongs/h/h3389.md): [pāṯaḥ](../../strongs/h/h6605.md) thyself from the [mowcer](../../strongs/h/h4147.md) of thy [ṣaûā'r](../../strongs/h/h6677.md), O [šᵊḇî](../../strongs/h/h7628.md) [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_52_3"></a>Isaiah 52:3

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Ye have [māḵar](../../strongs/h/h4376.md) yourselves for [ḥinnām](../../strongs/h/h2600.md); and ye shall be [gā'al](../../strongs/h/h1350.md) without [keceph](../../strongs/h/h3701.md).

<a name="isaiah_52_4"></a>Isaiah 52:4

For thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), My ['am](../../strongs/h/h5971.md) went down aforetime into [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there; and the ['Aššûr](../../strongs/h/h804.md) [ʿāšaq](../../strongs/h/h6231.md) them ['ep̄es](../../strongs/h/h657.md).

<a name="isaiah_52_5"></a>Isaiah 52:5

Now therefore, what have I here, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that my ['am](../../strongs/h/h5971.md) is taken away for [ḥinnām](../../strongs/h/h2600.md)? they that [mashal](../../strongs/h/h4910.md) over them make them to [yālal](../../strongs/h/h3213.md), saith [Yĕhovah](../../strongs/h/h3068.md); and my [shem](../../strongs/h/h8034.md) [tāmîḏ](../../strongs/h/h8548.md) every [yowm](../../strongs/h/h3117.md) is [na'ats](../../strongs/h/h5006.md).

<a name="isaiah_52_6"></a>Isaiah 52:6

Therefore my ['am](../../strongs/h/h5971.md) shall [yada'](../../strongs/h/h3045.md) my [shem](../../strongs/h/h8034.md): therefore they shall know in that [yowm](../../strongs/h/h3117.md) that I am he that doth [dabar](../../strongs/h/h1696.md): behold, it is I.

<a name="isaiah_52_7"></a>Isaiah 52:7

How [nā'â](../../strongs/h/h4998.md) upon the [har](../../strongs/h/h2022.md) are the [regel](../../strongs/h/h7272.md) of him that [bāśar](../../strongs/h/h1319.md), that [shama'](../../strongs/h/h8085.md) [shalowm](../../strongs/h/h7965.md); that [bāśar](../../strongs/h/h1319.md) of [towb](../../strongs/h/h2896.md), that [shama'](../../strongs/h/h8085.md) [yĕshuw'ah](../../strongs/h/h3444.md); that ['āmar](../../strongs/h/h559.md) unto [Tsiyown](../../strongs/h/h6726.md), Thy ['Elohiym](../../strongs/h/h430.md) [mālaḵ](../../strongs/h/h4427.md)!

<a name="isaiah_52_8"></a>Isaiah 52:8

Thy [tsaphah](../../strongs/h/h6822.md) shall [nasa'](../../strongs/h/h5375.md) the [qowl](../../strongs/h/h6963.md); with the [qowl](../../strongs/h/h6963.md) together shall they [ranan](../../strongs/h/h7442.md): for they shall [ra'ah](../../strongs/h/h7200.md) ['ayin](../../strongs/h/h5869.md) to ['ayin](../../strongs/h/h5869.md), when [Yĕhovah](../../strongs/h/h3068.md) shall bring again [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_52_9"></a>Isaiah 52:9

[pāṣaḥ](../../strongs/h/h6476.md), [ranan](../../strongs/h/h7442.md) together, ye [chorbah](../../strongs/h/h2723.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [nacham](../../strongs/h/h5162.md) his ['am](../../strongs/h/h5971.md), he hath [gā'al](../../strongs/h/h1350.md) [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_52_10"></a>Isaiah 52:10

[Yĕhovah](../../strongs/h/h3068.md) hath [ḥāśap̄](../../strongs/h/h2834.md) his [qodesh](../../strongs/h/h6944.md) [zerowa'](../../strongs/h/h2220.md) in the ['ayin](../../strongs/h/h5869.md) of all the [gowy](../../strongs/h/h1471.md); and all the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md) shall [ra'ah](../../strongs/h/h7200.md) the [yĕshuw'ah](../../strongs/h/h3444.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_52_11"></a>Isaiah 52:11

[cuwr](../../strongs/h/h5493.md) ye, [cuwr](../../strongs/h/h5493.md) ye, [yāṣā'](../../strongs/h/h3318.md) ye from thence, [naga'](../../strongs/h/h5060.md) no [tame'](../../strongs/h/h2931.md) thing; [yāṣā'](../../strongs/h/h3318.md) ye of the [tavek](../../strongs/h/h8432.md) of her; be ye [bārar](../../strongs/h/h1305.md), that [nasa'](../../strongs/h/h5375.md) the [kĕliy](../../strongs/h/h3627.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_52_12"></a>Isaiah 52:12

For ye shall not [yāṣā'](../../strongs/h/h3318.md) with [ḥipāzôn](../../strongs/h/h2649.md), nor go by [mᵊnûsâ](../../strongs/h/h4499.md): for [Yĕhovah](../../strongs/h/h3068.md) will [halak](../../strongs/h/h1980.md) before you; and the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) will be your ['āsap̄](../../strongs/h/h622.md).

<a name="isaiah_52_13"></a>Isaiah 52:13

Behold, my ['ebed](../../strongs/h/h5650.md) shall [sakal](../../strongs/h/h7919.md), he shall be [ruwm](../../strongs/h/h7311.md) and [nasa'](../../strongs/h/h5375.md), and be [me'od](../../strongs/h/h3966.md) [gāḇah](../../strongs/h/h1361.md).

<a name="isaiah_52_14"></a>Isaiah 52:14

As many were [šāmēm](../../strongs/h/h8074.md) at thee; his [mar'ê](../../strongs/h/h4758.md) was so [mišḥāṯ](../../strongs/h/h4893.md) more than any ['iysh](../../strongs/h/h376.md), and his [tō'ar](../../strongs/h/h8389.md) more than the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md):

<a name="isaiah_52_15"></a>Isaiah 52:15

So shall he [nāzâ](../../strongs/h/h5137.md) many [gowy](../../strongs/h/h1471.md); the [melek](../../strongs/h/h4428.md) shall [qāp̄aṣ](../../strongs/h/h7092.md) their [peh](../../strongs/h/h6310.md) at him: for that which had not been [sāp̄ar](../../strongs/h/h5608.md) them shall they [ra'ah](../../strongs/h/h7200.md); and that which they had not [shama'](../../strongs/h/h8085.md) shall they [bîn](../../strongs/h/h995.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 51](isaiah_51.md) - [Isaiah 53](isaiah_53.md)