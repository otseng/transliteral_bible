# [Isaiah 49](https://www.blueletterbible.org/kjv/isa/49/1/s_728001)

<a name="isaiah_49_1"></a>Isaiah 49:1

[shama'](../../strongs/h/h8085.md), O ['î](../../strongs/h/h339.md), unto me; and [qashab](../../strongs/h/h7181.md), ye [lĕom](../../strongs/h/h3816.md), from [rachowq](../../strongs/h/h7350.md); [Yĕhovah](../../strongs/h/h3068.md) hath [qara'](../../strongs/h/h7121.md) me from the [beten](../../strongs/h/h990.md); from the [me'ah](../../strongs/h/h4578.md) of my ['em](../../strongs/h/h517.md) hath he [zakar](../../strongs/h/h2142.md) of my [shem](../../strongs/h/h8034.md).

<a name="isaiah_49_2"></a>Isaiah 49:2

And he hath [śûm](../../strongs/h/h7760.md) my [peh](../../strongs/h/h6310.md) like a [ḥaḏ](../../strongs/h/h2299.md) [chereb](../../strongs/h/h2719.md); in the [ṣēl](../../strongs/h/h6738.md) of his [yad](../../strongs/h/h3027.md) hath he [chaba'](../../strongs/h/h2244.md) me, and [śûm](../../strongs/h/h7760.md) me a [bārar](../../strongs/h/h1305.md) [chets](../../strongs/h/h2671.md); in his ['ašpâ](../../strongs/h/h827.md) hath he [cathar](../../strongs/h/h5641.md) me;

<a name="isaiah_49_3"></a>Isaiah 49:3

And ['āmar](../../strongs/h/h559.md) unto me, Thou art my ['ebed](../../strongs/h/h5650.md), [Yisra'el](../../strongs/h/h3478.md), in whom I will be [pā'ar](../../strongs/h/h6286.md).

<a name="isaiah_49_4"></a>Isaiah 49:4

Then I ['āmar](../../strongs/h/h559.md), I have [yaga'](../../strongs/h/h3021.md) in [riyq](../../strongs/h/h7385.md), I have [kalah](../../strongs/h/h3615.md) my [koach](../../strongs/h/h3581.md) for [tohuw](../../strongs/h/h8414.md), and in [heḇel](../../strongs/h/h1892.md): yet surely my [mishpat](../../strongs/h/h4941.md) is with [Yĕhovah](../../strongs/h/h3068.md), and my [pe'ullah](../../strongs/h/h6468.md) with my ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_49_5"></a>Isaiah 49:5

And now, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) that [yāṣar](../../strongs/h/h3335.md) me from the [beten](../../strongs/h/h990.md) to be his ['ebed](../../strongs/h/h5650.md), to bring [Ya'aqob](../../strongs/h/h3290.md) again to him, Though [Yisra'el](../../strongs/h/h3478.md) be not ['āsap̄](../../strongs/h/h622.md), yet shall I be [kabad](../../strongs/h/h3513.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and my ['Elohiym](../../strongs/h/h430.md) shall be my ['oz](../../strongs/h/h5797.md).

<a name="isaiah_49_6"></a>Isaiah 49:6

And he ['āmar](../../strongs/h/h559.md), It is a [qālal](../../strongs/h/h7043.md) that thou shouldest be my ['ebed](../../strongs/h/h5650.md) to [quwm](../../strongs/h/h6965.md) the [shebet](../../strongs/h/h7626.md) of [Ya'aqob](../../strongs/h/h3290.md), and to [shuwb](../../strongs/h/h7725.md) the [nāṣar](../../strongs/h/h5341.md) [nāṣîr](../../strongs/h/h5336.md) of [Yisra'el](../../strongs/h/h3478.md): I will also [nathan](../../strongs/h/h5414.md) thee for a ['owr](../../strongs/h/h216.md) to the [gowy](../../strongs/h/h1471.md), that thou mayest be my [yĕshuw'ah](../../strongs/h/h3444.md) unto the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md).

<a name="isaiah_49_7"></a>Isaiah 49:7

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the [gā'al](../../strongs/h/h1350.md) of [Yisra'el](../../strongs/h/h3478.md), and his [qadowsh](../../strongs/h/h6918.md), to him whom [nephesh](../../strongs/h/h5315.md) [bāzô](../../strongs/h/h960.md), to him whom the [gowy](../../strongs/h/h1471.md) [ta'ab](../../strongs/h/h8581.md), to an ['ebed](../../strongs/h/h5650.md) of [mashal](../../strongs/h/h4910.md), [melek](../../strongs/h/h4428.md) shall [ra'ah](../../strongs/h/h7200.md) and [quwm](../../strongs/h/h6965.md), [śar](../../strongs/h/h8269.md) also shall [shachah](../../strongs/h/h7812.md), because of [Yĕhovah](../../strongs/h/h3068.md) that is ['aman](../../strongs/h/h539.md), and the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), and he shall [bāḥar](../../strongs/h/h977.md) thee.

<a name="isaiah_49_8"></a>Isaiah 49:8

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), In a [ratsown](../../strongs/h/h7522.md) [ʿēṯ](../../strongs/h/h6256.md) have I ['anah](../../strongs/h/h6030.md) thee, and in a day of [yĕshuw'ah](../../strongs/h/h3444.md) have I [ʿāzar](../../strongs/h/h5826.md) thee: and I will [nāṣar](../../strongs/h/h5341.md) thee, and [nathan](../../strongs/h/h5414.md) thee for a [bĕriyth](../../strongs/h/h1285.md) of the ['am](../../strongs/h/h5971.md), to [quwm](../../strongs/h/h6965.md) the ['erets](../../strongs/h/h776.md), to cause to [nāḥal](../../strongs/h/h5157.md) the [šāmēm](../../strongs/h/h8074.md) [nachalah](../../strongs/h/h5159.md);

<a name="isaiah_49_9"></a>Isaiah 49:9

That thou mayest ['āmar](../../strongs/h/h559.md) to the ['āsar](../../strongs/h/h631.md), [yāṣā'](../../strongs/h/h3318.md); to them that are in [choshek](../../strongs/h/h2822.md), [gālâ](../../strongs/h/h1540.md) yourselves. They shall [ra'ah](../../strongs/h/h7462.md) in the ways, and their [marʿîṯ](../../strongs/h/h4830.md) shall be in all [šᵊp̄î](../../strongs/h/h8205.md).

<a name="isaiah_49_10"></a>Isaiah 49:10

They shall not [rāʿēḇ](../../strongs/h/h7456.md) nor [ṣāmē'](../../strongs/h/h6770.md); neither shall the [šārāḇ](../../strongs/h/h8273.md) nor [šemeš](../../strongs/h/h8121.md) [nakah](../../strongs/h/h5221.md) them: for he that hath [racham](../../strongs/h/h7355.md) on them shall [nāhaḡ](../../strongs/h/h5090.md) them, even by the [mabûaʿ](../../strongs/h/h4002.md) of [mayim](../../strongs/h/h4325.md) shall he [nāhal](../../strongs/h/h5095.md) them.

<a name="isaiah_49_11"></a>Isaiah 49:11

And I will [śûm](../../strongs/h/h7760.md) all my [har](../../strongs/h/h2022.md) a [derek](../../strongs/h/h1870.md), and my [mĕcillah](../../strongs/h/h4546.md) shall be [ruwm](../../strongs/h/h7311.md).

<a name="isaiah_49_12"></a>Isaiah 49:12

Behold, these shall [bow'](../../strongs/h/h935.md) from [rachowq](../../strongs/h/h7350.md): and, lo, these from the [ṣāp̄ôn](../../strongs/h/h6828.md) and from the [yam](../../strongs/h/h3220.md); and these from the ['erets](../../strongs/h/h776.md) of [sînîm](../../strongs/h/h5515.md).

<a name="isaiah_49_13"></a>Isaiah 49:13

[ranan](../../strongs/h/h7442.md), [shamayim](../../strongs/h/h8064.md); and be [giyl](../../strongs/h/h1523.md), ['erets](../../strongs/h/h776.md); and [pāṣaḥ](../../strongs/h/h6476.md) into [rinnah](../../strongs/h/h7440.md), O [har](../../strongs/h/h2022.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [nacham](../../strongs/h/h5162.md) his ['am](../../strongs/h/h5971.md), and will have [racham](../../strongs/h/h7355.md) upon his ['aniy](../../strongs/h/h6041.md).

<a name="isaiah_49_14"></a>Isaiah 49:14

But [Tsiyown](../../strongs/h/h6726.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath ['azab](../../strongs/h/h5800.md) me, and my['adonay](../../strongs/h/h136.md) hath [shakach](../../strongs/h/h7911.md) me.

<a name="isaiah_49_15"></a>Isaiah 49:15

Can an ['ishshah](../../strongs/h/h802.md) [shakach](../../strongs/h/h7911.md) her [ʿûl](../../strongs/h/h5764.md), that she should not have [racham](../../strongs/h/h7355.md) on the [ben](../../strongs/h/h1121.md) of her [beten](../../strongs/h/h990.md)? yea, they may [shakach](../../strongs/h/h7911.md), yet will I not [shakach](../../strongs/h/h7911.md) thee.

<a name="isaiah_49_16"></a>Isaiah 49:16

Behold, I have [ḥāqaq](../../strongs/h/h2710.md) thee upon the palms of my [kaph](../../strongs/h/h3709.md); thy [ḥômâ](../../strongs/h/h2346.md) are [tāmîḏ](../../strongs/h/h8548.md) before me.

<a name="isaiah_49_17"></a>Isaiah 49:17

Thy [ben](../../strongs/h/h1121.md) shall make [māhar](../../strongs/h/h4116.md); thy [harac](../../strongs/h/h2040.md) and they that made thee [ḥāraḇ](../../strongs/h/h2717.md) shall [yāṣā'](../../strongs/h/h3318.md) of thee.

<a name="isaiah_49_18"></a>Isaiah 49:18

[nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) [cabiyb](../../strongs/h/h5439.md), and [ra'ah](../../strongs/h/h7200.md): all these [qāḇaṣ](../../strongs/h/h6908.md), and [bow'](../../strongs/h/h935.md) to thee. As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), thou shalt surely [labash](../../strongs/h/h3847.md) thee with them all, as with an [ʿădiy](../../strongs/h/h5716.md), and [qāšar](../../strongs/h/h7194.md) them on thee, as a [kallâ](../../strongs/h/h3618.md).

<a name="isaiah_49_19"></a>Isaiah 49:19

For thy [chorbah](../../strongs/h/h2723.md) and thy [šāmēm](../../strongs/h/h8074.md), and the ['erets](../../strongs/h/h776.md) of thy [hărîsûṯ](../../strongs/h/h2035.md), shall even now be too [yāṣar](../../strongs/h/h3334.md) by reason of the [yashab](../../strongs/h/h3427.md), and they that [bālaʿ](../../strongs/h/h1104.md) thee shall be [rachaq](../../strongs/h/h7368.md).

<a name="isaiah_49_20"></a>Isaiah 49:20

The [ben](../../strongs/h/h1121.md) which thou shalt have, after thou hast [šikulîm](../../strongs/h/h7923.md) the other, shall ['āmar](../../strongs/h/h559.md) again in thine ['ozen](../../strongs/h/h241.md), The [maqowm](../../strongs/h/h4725.md) is too [tsar](../../strongs/h/h6862.md) for me: [nāḡaš](../../strongs/h/h5066.md) [maqowm](../../strongs/h/h4725.md) to me that I may [yashab](../../strongs/h/h3427.md).

<a name="isaiah_49_21"></a>Isaiah 49:21

Then shalt thou ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), Who hath [yalad](../../strongs/h/h3205.md) me these, seeing I am [šāḵōl](../../strongs/h/h7921.md), and am [galmûḏ](../../strongs/h/h1565.md), a [gālâ](../../strongs/h/h1540.md), and [cuwr](../../strongs/h/h5493.md) to and fro? and who hath [gāḏal](../../strongs/h/h1431.md) these? Behold, I was [šā'ar](../../strongs/h/h7604.md) alone; these, where had they been?

<a name="isaiah_49_22"></a>Isaiah 49:22

Thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), Behold, I will [nasa'](../../strongs/h/h5375.md) mine [yad](../../strongs/h/h3027.md) to the [gowy](../../strongs/h/h1471.md), and set up my [nēs](../../strongs/h/h5251.md) to the ['am](../../strongs/h/h5971.md): and they shall [bow'](../../strongs/h/h935.md) thy [ben](../../strongs/h/h1121.md) in their [ḥēṣen](../../strongs/h/h2684.md), and thy [bath](../../strongs/h/h1323.md) shall be [nasa'](../../strongs/h/h5375.md) their [kāṯēp̄](../../strongs/h/h3802.md).

<a name="isaiah_49_23"></a>Isaiah 49:23

And [melek](../../strongs/h/h4428.md) shall be thy ['aman](../../strongs/h/h539.md), and their [Śārâ](../../strongs/h/h8282.md) thy [yānaq](../../strongs/h/h3243.md): they shall [shachah](../../strongs/h/h7812.md) to thee with their ['aph](../../strongs/h/h639.md) toward the ['erets](../../strongs/h/h776.md), and [lāḥaḵ](../../strongs/h/h3897.md) the ['aphar](../../strongs/h/h6083.md) of thy [regel](../../strongs/h/h7272.md); and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md): for they shall not be [buwsh](../../strongs/h/h954.md) that [qāvâ](../../strongs/h/h6960.md) for me.

<a name="isaiah_49_24"></a>Isaiah 49:24

Shall the [malqôaḥ](../../strongs/h/h4455.md) be [laqach](../../strongs/h/h3947.md) from the [gibôr](../../strongs/h/h1368.md), or the [tsaddiyq](../../strongs/h/h6662.md) [šᵊḇî](../../strongs/h/h7628.md) [mālaṭ](../../strongs/h/h4422.md)?

<a name="isaiah_49_25"></a>Isaiah 49:25

But thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Even the [šᵊḇî](../../strongs/h/h7628.md) of the [gibôr](../../strongs/h/h1368.md) shall be [laqach](../../strongs/h/h3947.md), and the [malqôaḥ](../../strongs/h/h4455.md) of the [ʿārîṣ](../../strongs/h/h6184.md) shall be [mālaṭ](../../strongs/h/h4422.md): for I will [riyb](../../strongs/h/h7378.md) with him that [yārîḇ](../../strongs/h/h3401.md) with thee, and I will [yasha'](../../strongs/h/h3467.md) thy [ben](../../strongs/h/h1121.md).

<a name="isaiah_49_26"></a>Isaiah 49:26

And I will ['akal](../../strongs/h/h398.md) them that [yānâ](../../strongs/h/h3238.md) thee with their own [basar](../../strongs/h/h1320.md); and they shall be [šāḵar](../../strongs/h/h7937.md) with their own [dam](../../strongs/h/h1818.md), as with [ʿāsîs](../../strongs/h/h6071.md): and all [basar](../../strongs/h/h1320.md) shall [yada'](../../strongs/h/h3045.md) that I [Yĕhovah](../../strongs/h/h3068.md) am thy [yasha'](../../strongs/h/h3467.md) and thy [gā'al](../../strongs/h/h1350.md), the ['abiyr](../../strongs/h/h46.md) of [Ya'aqob](../../strongs/h/h3290.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 48](isaiah_48.md) - [Isaiah 50](isaiah_50.md)