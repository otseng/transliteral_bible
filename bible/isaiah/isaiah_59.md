# [Isaiah 59](https://www.blueletterbible.org/kjv/isa/59/1/s_738001)

<a name="isaiah_59_1"></a>Isaiah 59:1

[hen](../../strongs/h/h2005.md), [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) is not [qāṣar](../../strongs/h/h7114.md), that it cannot [yasha'](../../strongs/h/h3467.md); neither his ['ozen](../../strongs/h/h241.md) [kabad](../../strongs/h/h3513.md), that it cannot [shama'](../../strongs/h/h8085.md):

<a name="isaiah_59_2"></a>Isaiah 59:2

But your ['avon](../../strongs/h/h5771.md) have [bāḏal](../../strongs/h/h914.md) between you and your ['Elohiym](../../strongs/h/h430.md), and your [chatta'ath](../../strongs/h/h2403.md) have [cathar](../../strongs/h/h5641.md) his [paniym](../../strongs/h/h6440.md) from you, that he will not [shama'](../../strongs/h/h8085.md).

<a name="isaiah_59_3"></a>Isaiah 59:3

For your [kaph](../../strongs/h/h3709.md) are [gā'al](../../strongs/h/h1351.md) with [dam](../../strongs/h/h1818.md), and your ['etsba'](../../strongs/h/h676.md) with ['avon](../../strongs/h/h5771.md); your [saphah](../../strongs/h/h8193.md) have [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md), your [lashown](../../strongs/h/h3956.md) hath muttered ['evel](../../strongs/h/h5766.md).

<a name="isaiah_59_4"></a>Isaiah 59:4

None [qara'](../../strongs/h/h7121.md) for [tsedeq](../../strongs/h/h6664.md), nor any [shaphat](../../strongs/h/h8199.md) for ['ĕmûnâ](../../strongs/h/h530.md): they [batach](../../strongs/h/h982.md) in [tohuw](../../strongs/h/h8414.md), and [dabar](../../strongs/h/h1696.md) [shav'](../../strongs/h/h7723.md); they [harah](../../strongs/h/h2029.md) ['amal](../../strongs/h/h5999.md), and [yalad](../../strongs/h/h3205.md) ['aven](../../strongs/h/h205.md).

<a name="isaiah_59_5"></a>Isaiah 59:5

They [bāqaʿ](../../strongs/h/h1234.md) [ṣep̄aʿ](../../strongs/h/h6848.md) [bêṣâ](../../strongs/h/h1000.md), and ['āraḡ](../../strongs/h/h707.md) the [ʿakāḇîš](../../strongs/h/h5908.md) [qûr](../../strongs/h/h6980.md): he that ['akal](../../strongs/h/h398.md) of their [bêṣâ](../../strongs/h/h1000.md) [muwth](../../strongs/h/h4191.md), and that which is [zûrê](../../strongs/h/h2116.md) [bāqaʿ](../../strongs/h/h1234.md) into an ['ep̄ʿê](../../strongs/h/h660.md).

<a name="isaiah_59_6"></a>Isaiah 59:6

Their [qûr](../../strongs/h/h6980.md) shall not become [beḡeḏ](../../strongs/h/h899.md), neither shall they [kāsâ](../../strongs/h/h3680.md) themselves with their [ma'aseh](../../strongs/h/h4639.md): their [ma'aseh](../../strongs/h/h4639.md) are [ma'aseh](../../strongs/h/h4639.md) of ['aven](../../strongs/h/h205.md), and the [pōʿal](../../strongs/h/h6467.md) of [chamac](../../strongs/h/h2555.md) is in their [kaph](../../strongs/h/h3709.md).

<a name="isaiah_59_7"></a>Isaiah 59:7

Their [regel](../../strongs/h/h7272.md) [rûṣ](../../strongs/h/h7323.md) to [ra'](../../strongs/h/h7451.md), and they make [māhar](../../strongs/h/h4116.md) to [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md): their [maḥăšāḇâ](../../strongs/h/h4284.md) are [maḥăšāḇâ](../../strongs/h/h4284.md) of ['aven](../../strongs/h/h205.md); [shod](../../strongs/h/h7701.md) and [šeḇar](../../strongs/h/h7667.md) are in their [mĕcillah](../../strongs/h/h4546.md).

<a name="isaiah_59_8"></a>Isaiah 59:8

The [derek](../../strongs/h/h1870.md) of [shalowm](../../strongs/h/h7965.md) they [yada'](../../strongs/h/h3045.md) not; and there is no [mishpat](../../strongs/h/h4941.md) in their [ma'gal](../../strongs/h/h4570.md): they have made them [ʿāqaš](../../strongs/h/h6140.md) [nāṯîḇ](../../strongs/h/h5410.md): whosoever [dāraḵ](../../strongs/h/h1869.md) therein shall not know [shalowm](../../strongs/h/h7965.md).

<a name="isaiah_59_9"></a>Isaiah 59:9

Therefore is [mishpat](../../strongs/h/h4941.md) [rachaq](../../strongs/h/h7368.md) from us, neither doth [tsedaqah](../../strongs/h/h6666.md) [nāśaḡ](../../strongs/h/h5381.md) us: we [qāvâ](../../strongs/h/h6960.md) for ['owr](../../strongs/h/h216.md), but behold [choshek](../../strongs/h/h2822.md); for [nᵊḡôâ](../../strongs/h/h5054.md), but we [halak](../../strongs/h/h1980.md) in ['ăp̄ēlâ](../../strongs/h/h653.md).

<a name="isaiah_59_10"></a>Isaiah 59:10

We [gāšaš](../../strongs/h/h1659.md) for the [qîr](../../strongs/h/h7023.md) like the [ʿiûēr](../../strongs/h/h5787.md), and we [gāšaš](../../strongs/h/h1659.md) as if we had no ['ayin](../../strongs/h/h5869.md): we [kashal](../../strongs/h/h3782.md) at [ṣōhar](../../strongs/h/h6672.md) as in the [nešep̄](../../strongs/h/h5399.md); we are in ['ašmannîm](../../strongs/h/h820.md) as [muwth](../../strongs/h/h4191.md).

<a name="isaiah_59_11"></a>Isaiah 59:11

We [hāmâ](../../strongs/h/h1993.md) all like [dōḇ](../../strongs/h/h1677.md), and [hagah](../../strongs/h/h1897.md) [hagah](../../strongs/h/h1897.md) like [yônâ](../../strongs/h/h3123.md): we [qāvâ](../../strongs/h/h6960.md) for [mishpat](../../strongs/h/h4941.md), but there is none; for [yĕshuw'ah](../../strongs/h/h3444.md), but it is [rachaq](../../strongs/h/h7368.md) from us.

<a name="isaiah_59_12"></a>Isaiah 59:12

For our [pesha'](../../strongs/h/h6588.md) are [rabab](../../strongs/h/h7231.md) before thee, and our [chatta'ath](../../strongs/h/h2403.md) ['anah](../../strongs/h/h6030.md) against us: for our [pesha'](../../strongs/h/h6588.md) are with us; and as for our ['avon](../../strongs/h/h5771.md), we [yada'](../../strongs/h/h3045.md) them;

<a name="isaiah_59_13"></a>Isaiah 59:13

In [pāšaʿ](../../strongs/h/h6586.md) and [kāḥaš](../../strongs/h/h3584.md) against [Yĕhovah](../../strongs/h/h3068.md), and [nāsaḡ](../../strongs/h/h5253.md) from our ['Elohiym](../../strongs/h/h430.md), speaking [ʿōšeq](../../strongs/h/h6233.md) and [sārâ](../../strongs/h/h5627.md), [harah](../../strongs/h/h2029.md) and [hagah](../../strongs/h/h1897.md) from the [leb](../../strongs/h/h3820.md) [dabar](../../strongs/h/h1697.md) of [sheqer](../../strongs/h/h8267.md).

<a name="isaiah_59_14"></a>Isaiah 59:14

And [mishpat](../../strongs/h/h4941.md) is [nāsaḡ](../../strongs/h/h5253.md) ['āḥôr](../../strongs/h/h268.md), and [tsedaqah](../../strongs/h/h6666.md) ['amad](../../strongs/h/h5975.md) afar off: for ['emeth](../../strongs/h/h571.md) is [kashal](../../strongs/h/h3782.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md), and [nᵊḵōḥâ](../../strongs/h/h5229.md) cannot [bow'](../../strongs/h/h935.md).

<a name="isaiah_59_15"></a>Isaiah 59:15

Yea, ['emeth](../../strongs/h/h571.md) [ʿāḏar](../../strongs/h/h5737.md); and he that departeth from [ra'](../../strongs/h/h7451.md) maketh himself a [šālal](../../strongs/h/h7997.md): and [Yĕhovah](../../strongs/h/h3068.md) saw it, and it [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) him that there was no [mishpat](../../strongs/h/h4941.md).

<a name="isaiah_59_16"></a>Isaiah 59:16

And he [ra'ah](../../strongs/h/h7200.md) that there was no ['iysh](../../strongs/h/h376.md), and [šāmēm](../../strongs/h/h8074.md) that there was no [pāḡaʿ](../../strongs/h/h6293.md): therefore his [zerowa'](../../strongs/h/h2220.md) brought [yasha'](../../strongs/h/h3467.md) unto him; and his [ṣĕdāqāh](../../strongs/h/h6666.md), it [camak](../../strongs/h/h5564.md) him.

<a name="isaiah_59_17"></a>Isaiah 59:17

For he [labash](../../strongs/h/h3847.md) [ṣĕdāqāh](../../strongs/h/h6666.md) as a [širyôn](../../strongs/h/h8302.md), and a [kôḇaʿ](../../strongs/h/h3553.md) of [yĕshuw'ah](../../strongs/h/h3444.md) upon his [ro'sh](../../strongs/h/h7218.md); and he [labash](../../strongs/h/h3847.md) the [beḡeḏ](../../strongs/h/h899.md) of [nāqām](../../strongs/h/h5359.md) for [tilbōšeṯ](../../strongs/h/h8516.md), and was [ʿāṭâ](../../strongs/h/h5844.md) with [qin'â](../../strongs/h/h7068.md) as a [mᵊʿîl](../../strongs/h/h4598.md).

<a name="isaiah_59_18"></a>Isaiah 59:18

According to their [gᵊmûlâ](../../strongs/h/h1578.md), accordingly he will [shalam](../../strongs/h/h7999.md), [chemah](../../strongs/h/h2534.md) to his [tsar](../../strongs/h/h6862.md), [gĕmwl](../../strongs/h/h1576.md) to his ['oyeb](../../strongs/h/h341.md); to the ['î](../../strongs/h/h339.md) he will repay [gĕmwl](../../strongs/h/h1576.md).

<a name="isaiah_59_19"></a>Isaiah 59:19

So shall they [yare'](../../strongs/h/h3372.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) from the [ma'arab](../../strongs/h/h4628.md), and his [kabowd](../../strongs/h/h3519.md) from the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md). When the [tsar](../../strongs/h/h6862.md) shall come in like a [nāhār](../../strongs/h/h5104.md), the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [nûs](../../strongs/h/h5127.md) against him.

<a name="isaiah_59_20"></a>Isaiah 59:20

And the [gā'al](../../strongs/h/h1350.md) shall [bow'](../../strongs/h/h935.md) to [Tsiyown](../../strongs/h/h6726.md), and unto them that [shuwb](../../strongs/h/h7725.md) from [pesha'](../../strongs/h/h6588.md) in [Ya'aqob](../../strongs/h/h3290.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_59_21"></a>Isaiah 59:21

As for me, this is my [bĕriyth](../../strongs/h/h1285.md) with them, saith [Yĕhovah](../../strongs/h/h3068.md); My [ruwach](../../strongs/h/h7307.md) that is upon thee, and my [dabar](../../strongs/h/h1697.md) which I have [śûm](../../strongs/h/h7760.md) in thy [peh](../../strongs/h/h6310.md), shall not [mûš](../../strongs/h/h4185.md) out of thy [peh](../../strongs/h/h6310.md), nor out of the [peh](../../strongs/h/h6310.md) of thy [zera'](../../strongs/h/h2233.md), nor out of the [peh](../../strongs/h/h6310.md) of thy [zera'](../../strongs/h/h2233.md) [zera'](../../strongs/h/h2233.md), saith [Yĕhovah](../../strongs/h/h3068.md), from henceforth and [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 58](isaiah_58.md) - [Isaiah 60](isaiah_60.md)