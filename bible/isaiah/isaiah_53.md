# [Isaiah 53](https://www.blueletterbible.org/kjv/isa/53/1/s_732001)

<a name="isaiah_53_1"></a>Isaiah 53:1

Who hath ['aman](../../strongs/h/h539.md) our [šᵊmûʿâ](../../strongs/h/h8052.md)? and to whom is the [zerowa'](../../strongs/h/h2220.md) of [Yĕhovah](../../strongs/h/h3068.md) [gālâ](../../strongs/h/h1540.md)?

<a name="isaiah_53_2"></a>Isaiah 53:2

For he shall [ʿālâ](../../strongs/h/h5927.md) before him as a [yônēq](../../strongs/h/h3126.md), and as a [šereš](../../strongs/h/h8328.md) out of a [ṣîyâ](../../strongs/h/h6723.md) ['erets](../../strongs/h/h776.md): he hath no [tō'ar](../../strongs/h/h8389.md) nor [hadar](../../strongs/h/h1926.md); and when we shall [ra'ah](../../strongs/h/h7200.md) him, there is no [mar'ê](../../strongs/h/h4758.md) that we should [chamad](../../strongs/h/h2530.md) him.

<a name="isaiah_53_3"></a>Isaiah 53:3

He is [bazah](../../strongs/h/h959.md) and [ḥāḏēl](../../strongs/h/h2310.md) of ['iysh](../../strongs/h/h376.md); an ['iysh](../../strongs/h/h376.md) of [maḵ'ōḇ](../../strongs/h/h4341.md), and [yada'](../../strongs/h/h3045.md) with [ḥŏlî](../../strongs/h/h2483.md): and we [mastēr](../../strongs/h/h4564.md) our [paniym](../../strongs/h/h6440.md) from him; he was [bazah](../../strongs/h/h959.md), and we [chashab](../../strongs/h/h2803.md) him not.

<a name="isaiah_53_4"></a>Isaiah 53:4

['āḵēn](../../strongs/h/h403.md) he hath [nasa'](../../strongs/h/h5375.md) our [ḥŏlî](../../strongs/h/h2483.md), and [sāḇal](../../strongs/h/h5445.md) our [maḵ'ōḇ](../../strongs/h/h4341.md): yet we did [chashab](../../strongs/h/h2803.md) him [naga'](../../strongs/h/h5060.md), [nakah](../../strongs/h/h5221.md) of ['Elohiym](../../strongs/h/h430.md), and afflicted.

<a name="isaiah_53_5"></a>Isaiah 53:5

But he was [ḥālal](../../strongs/h/h2490.md) for our [pesha'](../../strongs/h/h6588.md), he was [dāḵā'](../../strongs/h/h1792.md) for our ['avon](../../strongs/h/h5771.md): the [mûsār](../../strongs/h/h4148.md) of our [shalowm](../../strongs/h/h7965.md) was upon him; and with his [ḥabûrâ](../../strongs/h/h2250.md) we are [rapha'](../../strongs/h/h7495.md).

<a name="isaiah_53_6"></a>Isaiah 53:6

All we like [tso'n](../../strongs/h/h6629.md) have [tāʿâ](../../strongs/h/h8582.md); we have [panah](../../strongs/h/h6437.md) every one to his own [derek](../../strongs/h/h1870.md); and [Yĕhovah](../../strongs/h/h3068.md) hath [pāḡaʿ](../../strongs/h/h6293.md) on him the ['avon](../../strongs/h/h5771.md) of us all.

<a name="isaiah_53_7"></a>Isaiah 53:7

He was [nāḡaś](../../strongs/h/h5065.md), and he was [ʿānâ](../../strongs/h/h6031.md), yet he [pāṯaḥ](../../strongs/h/h6605.md) not his [peh](../../strongs/h/h6310.md): he is [yāḇal](../../strongs/h/h2986.md) as a [śê](../../strongs/h/h7716.md) to the [ṭeḇaḥ](../../strongs/h/h2874.md), and as a [rāḥēl](../../strongs/h/h7353.md) before her [gāzaz](../../strongs/h/h1494.md) is ['ālam](../../strongs/h/h481.md), so he [pāṯaḥ](../../strongs/h/h6605.md) not his [peh](../../strongs/h/h6310.md).

<a name="isaiah_53_8"></a>Isaiah 53:8

He was [laqach](../../strongs/h/h3947.md) from [ʿōṣer](../../strongs/h/h6115.md) and from [mishpat](../../strongs/h/h4941.md): and who shall [śîaḥ](../../strongs/h/h7878.md) his [dôr](../../strongs/h/h1755.md)? for he was [gāzar](../../strongs/h/h1504.md) out of the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md): for the [pesha'](../../strongs/h/h6588.md) of my ['am](../../strongs/h/h5971.md) was he [neḡaʿ](../../strongs/h/h5061.md).

<a name="isaiah_53_9"></a>Isaiah 53:9

And he [nathan](../../strongs/h/h5414.md) his [qeber](../../strongs/h/h6913.md) with the [rasha'](../../strongs/h/h7563.md), and with the [ʿāšîr](../../strongs/h/h6223.md) in his [maveth](../../strongs/h/h4194.md); because he had ['asah](../../strongs/h/h6213.md) no [chamac](../../strongs/h/h2555.md), neither was any [mirmah](../../strongs/h/h4820.md) in his [peh](../../strongs/h/h6310.md).

<a name="isaiah_53_10"></a>Isaiah 53:10

Yet it [ḥāp̄ēṣ](../../strongs/h/h2654.md) [Yĕhovah](../../strongs/h/h3068.md) to [dāḵā'](../../strongs/h/h1792.md) him; he hath put him to [ḥālâ](../../strongs/h/h2470.md): when thou shalt [śûm](../../strongs/h/h7760.md) his [nephesh](../../strongs/h/h5315.md) an ['āšām](../../strongs/h/h817.md), he shall [ra'ah](../../strongs/h/h7200.md) his [zera'](../../strongs/h/h2233.md), he shall ['arak](../../strongs/h/h748.md) his [yowm](../../strongs/h/h3117.md), and the [chephets](../../strongs/h/h2656.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [tsalach](../../strongs/h/h6743.md) in his [yad](../../strongs/h/h3027.md).

<a name="isaiah_53_11"></a>Isaiah 53:11

He shall [ra'ah](../../strongs/h/h7200.md) of the ['amal](../../strongs/h/h5999.md) of his [nephesh](../../strongs/h/h5315.md), and shall be [sāׂbaʿ](../../strongs/h/h7646.md): by his [da'ath](../../strongs/h/h1847.md) shall my [tsaddiyq](../../strongs/h/h6662.md) ['ebed](../../strongs/h/h5650.md) [ṣāḏaq](../../strongs/h/h6663.md) many; for he shall [sāḇal](../../strongs/h/h5445.md) their ['avon](../../strongs/h/h5771.md).

<a name="isaiah_53_12"></a>Isaiah 53:12

Therefore will I [chalaq](../../strongs/h/h2505.md) him a portion with the [rab](../../strongs/h/h7227.md), and he shall [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md) with the ['atsuwm](../../strongs/h/h6099.md); because he hath [ʿārâ](../../strongs/h/h6168.md) his [nephesh](../../strongs/h/h5315.md) unto [maveth](../../strongs/h/h4194.md): and he was [mānâ](../../strongs/h/h4487.md) with the [pāšaʿ](../../strongs/h/h6586.md); and he [nasa'](../../strongs/h/h5375.md) the [ḥēṭĕ'](../../strongs/h/h2399.md) of many, and [pāḡaʿ](../../strongs/h/h6293.md) for the [pāšaʿ](../../strongs/h/h6586.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 52](isaiah_52.md) - [Isaiah 54](isaiah_54.md)