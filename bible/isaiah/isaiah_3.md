# [Isaiah 3](https://www.blueletterbible.org/kjv/isa/3/1/s_682001)

<a name="isaiah_3_1"></a>Isaiah 3:1

For ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), doth [cuwr](../../strongs/h/h5493.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) and from [Yehuwdah](../../strongs/h/h3063.md) the [mašʿēn](../../strongs/h/h4937.md) and the [mašʿēnâ](../../strongs/h/h4938.md), the [mašʿēn](../../strongs/h/h4937.md) of [lechem](../../strongs/h/h3899.md), and the whole [mašʿēn](../../strongs/h/h4937.md) of [mayim](../../strongs/h/h4325.md).

<a name="isaiah_3_2"></a>Isaiah 3:2

The [gibôr](../../strongs/h/h1368.md), and the ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), the [shaphat](../../strongs/h/h8199.md), and the [nāḇî'](../../strongs/h/h5030.md), and the [qāsam](../../strongs/h/h7080.md), and the [zāqēn](../../strongs/h/h2205.md),

<a name="isaiah_3_3"></a>Isaiah 3:3

The [śar](../../strongs/h/h8269.md) of fifty, and the [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md), and the [ya'ats](../../strongs/h/h3289.md), and the [ḥāḵām](../../strongs/h/h2450.md) [ḥereš](../../strongs/h/h2791.md), and the [bîn](../../strongs/h/h995.md) [laḥaš](../../strongs/h/h3908.md).

<a name="isaiah_3_4"></a>Isaiah 3:4

And I will [nathan](../../strongs/h/h5414.md) [naʿar](../../strongs/h/h5288.md) to be their [śar](../../strongs/h/h8269.md), and [taʿălûlîm](../../strongs/h/h8586.md) shall [mashal](../../strongs/h/h4910.md) over them.

<a name="isaiah_3_5"></a>Isaiah 3:5

And the ['am](../../strongs/h/h5971.md) shall be [nāḡaś](../../strongs/h/h5065.md), every one by another, and every one by his [rea'](../../strongs/h/h7453.md): the [naʿar](../../strongs/h/h5288.md) shall behave [rāhaḇ](../../strongs/h/h7292.md) against the [zāqēn](../../strongs/h/h2205.md), and the [qālâ](../../strongs/h/h7034.md) against the [kabad](../../strongs/h/h3513.md).

<a name="isaiah_3_6"></a>Isaiah 3:6

When an ['iysh](../../strongs/h/h376.md) shall [tāp̄aś](../../strongs/h/h8610.md) of his ['ach](../../strongs/h/h251.md) of the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md), saying, Thou hast [śimlâ](../../strongs/h/h8071.md), be thou our [qāṣîn](../../strongs/h/h7101.md), and let this [maḵšēlâ](../../strongs/h/h4384.md) be under thy [yad](../../strongs/h/h3027.md):

<a name="isaiah_3_7"></a>Isaiah 3:7

In that [yowm](../../strongs/h/h3117.md) shall he [nasa'](../../strongs/h/h5375.md), saying, I will not be an [ḥāḇaš](../../strongs/h/h2280.md); for in my [bayith](../../strongs/h/h1004.md) is neither [lechem](../../strongs/h/h3899.md) nor [śimlâ](../../strongs/h/h8071.md): make me not a [qāṣîn](../../strongs/h/h7101.md) of the ['am](../../strongs/h/h5971.md).

<a name="isaiah_3_8"></a>Isaiah 3:8

For [Yĕruwshalaim](../../strongs/h/h3389.md) is [kashal](../../strongs/h/h3782.md), and [Yehuwdah](../../strongs/h/h3063.md) is [naphal](../../strongs/h/h5307.md): because their [lashown](../../strongs/h/h3956.md) and their [maʿălāl](../../strongs/h/h4611.md) are against [Yĕhovah](../../strongs/h/h3068.md), to [marah](../../strongs/h/h4784.md) the ['ayin](../../strongs/h/h5869.md) of his [kabowd](../../strongs/h/h3519.md).

<a name="isaiah_3_9"></a>Isaiah 3:9

The [hakārâ](../../strongs/h/h1971.md) of their [paniym](../../strongs/h/h6440.md) doth ['anah](../../strongs/h/h6030.md) against them; and they declare their [chatta'ath](../../strongs/h/h2403.md) as [Sᵊḏōm](../../strongs/h/h5467.md), they [kāḥaḏ](../../strongs/h/h3582.md) it not. ['owy](../../strongs/h/h188.md) unto their [nephesh](../../strongs/h/h5315.md)! for they have [gamal](../../strongs/h/h1580.md) [ra'](../../strongs/h/h7451.md) unto themselves.

<a name="isaiah_3_10"></a>Isaiah 3:10

['āmar](../../strongs/h/h559.md) ye to the [tsaddiyq](../../strongs/h/h6662.md), that it shall be [towb](../../strongs/h/h2896.md) with him: for they shall ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of their [maʿălāl](../../strongs/h/h4611.md).

<a name="isaiah_3_11"></a>Isaiah 3:11

['owy](../../strongs/h/h188.md) unto the [rasha'](../../strongs/h/h7563.md)! it shall be [ra'](../../strongs/h/h7451.md) with him: for the [gĕmwl](../../strongs/h/h1576.md) of his [yad](../../strongs/h/h3027.md) shall be given him.

<a name="isaiah_3_12"></a>Isaiah 3:12

As for my ['am](../../strongs/h/h5971.md), [ʿālal](../../strongs/h/h5953.md) are their [nāḡaś](../../strongs/h/h5065.md), and ['ishshah](../../strongs/h/h802.md) [mashal](../../strongs/h/h4910.md) over them. O my ['am](../../strongs/h/h5971.md), they which lead thee cause thee to [tāʿâ](../../strongs/h/h8582.md), and [bālaʿ](../../strongs/h/h1104.md) the [derek](../../strongs/h/h1870.md) of thy ['orach](../../strongs/h/h734.md).

<a name="isaiah_3_13"></a>Isaiah 3:13

[Yĕhovah](../../strongs/h/h3068.md) [nāṣaḇ](../../strongs/h/h5324.md) up to [riyb](../../strongs/h/h7378.md), and ['amad](../../strongs/h/h5975.md) to [diyn](../../strongs/h/h1777.md) the ['am](../../strongs/h/h5971.md).

<a name="isaiah_3_14"></a>Isaiah 3:14

[Yĕhovah](../../strongs/h/h3068.md) will [bow'](../../strongs/h/h935.md) into [mishpat](../../strongs/h/h4941.md) with the [zāqēn](../../strongs/h/h2205.md) of his ['am](../../strongs/h/h5971.md), and the [śar](../../strongs/h/h8269.md) thereof: for ye have [bāʿar](../../strongs/h/h1197.md) the [kerem](../../strongs/h/h3754.md); the [gᵊzēlâ](../../strongs/h/h1500.md) of the ['aniy](../../strongs/h/h6041.md) is in your [bayith](../../strongs/h/h1004.md).

<a name="isaiah_3_15"></a>Isaiah 3:15

What mean ye that ye [dāḵā'](../../strongs/h/h1792.md) my ['am](../../strongs/h/h5971.md) [dāḵā'](../../strongs/h/h1792.md), and [ṭāḥan](../../strongs/h/h2912.md) the [paniym](../../strongs/h/h6440.md) of the ['aniy](../../strongs/h/h6041.md)? [nᵊ'um](../../strongs/h/h5002.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of hosts.

<a name="isaiah_3_16"></a>Isaiah 3:16

Moreover [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Because the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) are [gāḇah](../../strongs/h/h1361.md), and [yālaḵ](../../strongs/h/h3212.md) with [natah](../../strongs/h/h5186.md) [garown](../../strongs/h/h1627.md) and [śāqar](../../strongs/h/h8265.md) ['ayin](../../strongs/h/h5869.md), [halak](../../strongs/h/h1980.md) and [ṭāp̄ap̄](../../strongs/h/h2952.md) as they [yālaḵ](../../strongs/h/h3212.md), and [ʿāḵas](../../strongs/h/h5913.md) with their [regel](../../strongs/h/h7272.md):

<a name="isaiah_3_17"></a>Isaiah 3:17

Therefore ['adonay](../../strongs/h/h136.md) will [sāp̄aḥ](../../strongs/h/h5596.md) the [qodqod](../../strongs/h/h6936.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), and [Yĕhovah](../../strongs/h/h3068.md) will [ʿārâ](../../strongs/h/h6168.md) their [pōṯ](../../strongs/h/h6596.md).

<a name="isaiah_3_18"></a>Isaiah 3:18

In that [yowm](../../strongs/h/h3117.md) ['adonay](../../strongs/h/h136.md) will take away the [tip̄'ārâ](../../strongs/h/h8597.md) of their [ʿeḵes](../../strongs/h/h5914.md) about their feet, and their [šᵊḇîs](../../strongs/h/h7636.md), and their [śahărōnîm](../../strongs/h/h7720.md),

<a name="isaiah_3_19"></a>Isaiah 3:19

The [nᵊṭîp̄â](../../strongs/h/h5188.md), and the [šērâ](../../strongs/h/h8285.md), and the [rᵊʿālâ](../../strongs/h/h7479.md),

<a name="isaiah_3_20"></a>Isaiah 3:20

The [pᵊ'ēr](../../strongs/h/h6287.md), and the [ṣᵊʿāḏâ](../../strongs/h/h6807.md), and the [qiššurîm](../../strongs/h/h7196.md), and the [nephesh](../../strongs/h/h5315.md) [bayith](../../strongs/h/h1004.md), and the [laḥaš](../../strongs/h/h3908.md),

<a name="isaiah_3_21"></a>Isaiah 3:21

The [ṭabaʿaṯ](../../strongs/h/h2885.md), and ['aph](../../strongs/h/h639.md) [nezem](../../strongs/h/h5141.md),

<a name="isaiah_3_22"></a>Isaiah 3:22

The [maḥălāṣôṯ](../../strongs/h/h4254.md), and the [maʿăṭāp̄â](../../strongs/h/h4595.md), and the [miṭpaḥaṯ](../../strongs/h/h4304.md), and the [ḥārîṭ](../../strongs/h/h2754.md),

<a name="isaiah_3_23"></a>Isaiah 3:23

The [gillāyôn](../../strongs/h/h1549.md), and the [sāḏîn](../../strongs/h/h5466.md), and the [ṣānîp̄](../../strongs/h/h6797.md), and the [rāḏîḏ](../../strongs/h/h7289.md).

<a name="isaiah_3_24"></a>Isaiah 3:24

And it shall come to pass, that instead of [beśem](../../strongs/h/h1314.md) there shall be [maq](../../strongs/h/h4716.md); and instead of a [chagowr](../../strongs/h/h2290.md) a [niqpâ](../../strongs/h/h5364.md); and instead of [ma'aseh](../../strongs/h/h4639.md) [miqšê](../../strongs/h/h4748.md) [qrḥ](../../strongs/h/h7144.md); and instead of a [pᵊṯîḡîl](../../strongs/h/h6614.md) a [maḥăḡōreṯ](../../strongs/h/h4228.md) of [śaq](../../strongs/h/h8242.md); and [kî](../../strongs/h/h3587.md) instead of [yᵊp̄î](../../strongs/h/h3308.md).

<a name="isaiah_3_25"></a>Isaiah 3:25

Thy [math](../../strongs/h/h4962.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), and thy [gᵊḇûrâ](../../strongs/h/h1369.md) in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="isaiah_3_26"></a>Isaiah 3:26

And her [peṯaḥ](../../strongs/h/h6607.md) shall ['ānâ](../../strongs/h/h578.md) and ['āḇal](../../strongs/h/h56.md); and she being [naqah](../../strongs/h/h5352.md) shall [yashab](../../strongs/h/h3427.md) upon the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 2](isaiah_2.md) - [Isaiah 4](isaiah_4.md)