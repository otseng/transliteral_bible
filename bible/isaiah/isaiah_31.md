# [Isaiah 31](https://www.blueletterbible.org/kjv/isa/31/1/s_710001)

<a name="isaiah_31_1"></a>Isaiah 31:1

[hôy](../../strongs/h/h1945.md) to them that go down to [Mitsrayim](../../strongs/h/h4714.md) for [ʿezrâ](../../strongs/h/h5833.md); and [šāʿan](../../strongs/h/h8172.md) on [sûs](../../strongs/h/h5483.md), and [batach](../../strongs/h/h982.md) in [reḵeḇ](../../strongs/h/h7393.md), because they are [rab](../../strongs/h/h7227.md); and in [pārāš](../../strongs/h/h6571.md), because they are very [ʿāṣam](../../strongs/h/h6105.md); but they [šāʿâ](../../strongs/h/h8159.md) not unto the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), neither [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md)!

<a name="isaiah_31_2"></a>Isaiah 31:2

Yet he also is [ḥāḵām](../../strongs/h/h2450.md), and will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md), and will not [cuwr](../../strongs/h/h5493.md) his [dabar](../../strongs/h/h1697.md): but will [quwm](../../strongs/h/h6965.md) against the [bayith](../../strongs/h/h1004.md) of the [ra'a'](../../strongs/h/h7489.md), and against the [ʿezrâ](../../strongs/h/h5833.md) of them that [pa'al](../../strongs/h/h6466.md) ['aven](../../strongs/h/h205.md).

<a name="isaiah_31_3"></a>Isaiah 31:3

Now the [Mitsrayim](../../strongs/h/h4714.md) are ['adam](../../strongs/h/h120.md), and not ['el](../../strongs/h/h410.md); and their [sûs](../../strongs/h/h5483.md) [basar](../../strongs/h/h1320.md), and not [ruwach](../../strongs/h/h7307.md). When [Yĕhovah](../../strongs/h/h3068.md) shall [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md), both he that [ʿāzar](../../strongs/h/h5826.md) shall [kashal](../../strongs/h/h3782.md), and he that is [ʿāzar](../../strongs/h/h5826.md) shall [naphal](../../strongs/h/h5307.md), and they all shall [kalah](../../strongs/h/h3615.md) together.

<a name="isaiah_31_4"></a>Isaiah 31:4

For thus hath [Yĕhovah](../../strongs/h/h3068.md) spoken unto me, Like as the ['ariy](../../strongs/h/h738.md) and the [kephiyr](../../strongs/h/h3715.md) [hagah](../../strongs/h/h1897.md) on his [ṭerep̄](../../strongs/h/h2964.md), when a [mᵊlō'](../../strongs/h/h4393.md) of [ra'ah](../../strongs/h/h7462.md) is [qara'](../../strongs/h/h7121.md) against him, he will not be [ḥāṯaṯ](../../strongs/h/h2865.md) of their [qowl](../../strongs/h/h6963.md), nor [ʿānâ](../../strongs/h/h6031.md) himself for the [hāmôn](../../strongs/h/h1995.md) of them: so shall [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) come down to [ṣᵊḇā'](../../strongs/h/h6633.md) for [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), and for the [giḇʿâ](../../strongs/h/h1389.md) thereof.

<a name="isaiah_31_5"></a>Isaiah 31:5

As [tsippowr](../../strongs/h/h6833.md) ['uwph](../../strongs/h/h5774.md), so will [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) [gānan](../../strongs/h/h1598.md) [Yĕruwshalaim](../../strongs/h/h3389.md); [gānan](../../strongs/h/h1598.md) also he will [natsal](../../strongs/h/h5337.md) it; and [pāsaḥ](../../strongs/h/h6452.md) he will [mālaṭ](../../strongs/h/h4422.md) it.

<a name="isaiah_31_6"></a>Isaiah 31:6

[shuwb](../../strongs/h/h7725.md) ye unto him from whom the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have [ʿāmaq](../../strongs/h/h6009.md) [sārâ](../../strongs/h/h5627.md).

<a name="isaiah_31_7"></a>Isaiah 31:7

For in that [yowm](../../strongs/h/h3117.md) every ['iysh](../../strongs/h/h376.md) shall [mā'as](../../strongs/h/h3988.md) his ['ĕlîl](../../strongs/h/h457.md) of [keceph](../../strongs/h/h3701.md), and his ['ĕlîl](../../strongs/h/h457.md) of [zāhāḇ](../../strongs/h/h2091.md), which your own [yad](../../strongs/h/h3027.md) have ['asah](../../strongs/h/h6213.md) unto you for a [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="isaiah_31_8"></a>Isaiah 31:8

Then shall the ['Aššûr](../../strongs/h/h804.md) [naphal](../../strongs/h/h5307.md) with the [chereb](../../strongs/h/h2719.md), not of an ['iysh](../../strongs/h/h376.md); and the [chereb](../../strongs/h/h2719.md), not of an ['āḏām](../../strongs/h/h120.md), shall ['akal](../../strongs/h/h398.md) him: but he shall [nûs](../../strongs/h/h5127.md) from the [chereb](../../strongs/h/h2719.md), and his [bāḥûr](../../strongs/h/h970.md) shall be [mas](../../strongs/h/h4522.md).

<a name="isaiah_31_9"></a>Isaiah 31:9

And he shall ['abar](../../strongs/h/h5674.md) to his [cela'](../../strongs/h/h5553.md) for [māḡôr](../../strongs/h/h4032.md), and his [śar](../../strongs/h/h8269.md) shall be [ḥāṯaṯ](../../strongs/h/h2865.md) of the [nēs](../../strongs/h/h5251.md), saith [Yĕhovah](../../strongs/h/h3068.md), whose ['ûr](../../strongs/h/h217.md) is in [Tsiyown](../../strongs/h/h6726.md), and his [tannûr](../../strongs/h/h8574.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 30](isaiah_30.md) - [Isaiah 32](isaiah_32.md)