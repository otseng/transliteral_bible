# [Isaiah 2](https://www.blueletterbible.org/kjv/isa/2/1/s_681001)

<a name="isaiah_2_1"></a>Isaiah 2:1

The [dabar](../../strongs/h/h1697.md) that [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md) [chazah](../../strongs/h/h2372.md) concerning [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_2_2"></a>Isaiah 2:2

And it shall come to pass in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md), that the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) shall be [kuwn](../../strongs/h/h3559.md) in the top of the [har](../../strongs/h/h2022.md), and shall be [nasa'](../../strongs/h/h5375.md) above the [giḇʿâ](../../strongs/h/h1389.md); and all [gowy](../../strongs/h/h1471.md) shall [nāhar](../../strongs/h/h5102.md) unto it.

<a name="isaiah_2_3"></a>Isaiah 2:3

And many ['am](../../strongs/h/h5971.md) shall [halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) ye, and let us [ʿālâ](../../strongs/h/h5927.md) to the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md), to the [bayith](../../strongs/h/h1004.md) of the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md); and he will [yārâ](../../strongs/h/h3384.md) us of his [derek](../../strongs/h/h1870.md), and we will [yālaḵ](../../strongs/h/h3212.md) in his ['orach](../../strongs/h/h734.md): for out of [Tsiyown](../../strongs/h/h6726.md) shall go forth the [towrah](../../strongs/h/h8451.md), and the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) from [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_2_4"></a>Isaiah 2:4

And he shall [shaphat](../../strongs/h/h8199.md) among the [gowy](../../strongs/h/h1471.md), and shall [yakach](../../strongs/h/h3198.md) many ['am](../../strongs/h/h5971.md): and they shall [kāṯaṯ](../../strongs/h/h3807.md) their [chereb](../../strongs/h/h2719.md) into ['ēṯ](../../strongs/h/h855.md), and their [ḥănîṯ](../../strongs/h/h2595.md) into [mazmērâ](../../strongs/h/h4211.md): [gowy](../../strongs/h/h1471.md) shall not [nasa'](../../strongs/h/h5375.md) [chereb](../../strongs/h/h2719.md) against [gowy](../../strongs/h/h1471.md), neither shall they [lamad](../../strongs/h/h3925.md) [milḥāmâ](../../strongs/h/h4421.md) any more. [^1]

<a name="isaiah_2_5"></a>Isaiah 2:5

O [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), [yālaḵ](../../strongs/h/h3212.md) ye, and let us [yālaḵ](../../strongs/h/h3212.md) in the ['owr](../../strongs/h/h216.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_2_6"></a>Isaiah 2:6

Therefore thou hast [nāṭaš](../../strongs/h/h5203.md) thy ['am](../../strongs/h/h5971.md) the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), because they be [mālā'](../../strongs/h/h4390.md) from the [qeḏem](../../strongs/h/h6924.md), and are [ʿānan](../../strongs/h/h6049.md) like the [Pᵊlištî](../../strongs/h/h6430.md), and they [sāp̄aq](../../strongs/h/h5606.md) themselves in the [yeleḏ](../../strongs/h/h3206.md) of [nāḵrî](../../strongs/h/h5237.md).

<a name="isaiah_2_7"></a>Isaiah 2:7

Their ['erets](../../strongs/h/h776.md) also is [mālā'](../../strongs/h/h4390.md) of [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), neither is there any end of their ['ôṣār](../../strongs/h/h214.md); their ['erets](../../strongs/h/h776.md) is also full of [sûs](../../strongs/h/h5483.md), neither is there any [qāṣê](../../strongs/h/h7097.md) of their [merkāḇâ](../../strongs/h/h4818.md):

<a name="isaiah_2_8"></a>Isaiah 2:8

Their ['erets](../../strongs/h/h776.md) also is [mālā'](../../strongs/h/h4390.md) of ['ĕlîl](../../strongs/h/h457.md); they [shachah](../../strongs/h/h7812.md) the [ma'aseh](../../strongs/h/h4639.md) of their own [yad](../../strongs/h/h3027.md), that which their own ['etsba'](../../strongs/h/h676.md) have ['asah](../../strongs/h/h6213.md):

<a name="isaiah_2_9"></a>Isaiah 2:9

And the ['adam](../../strongs/h/h120.md) [shachach](../../strongs/h/h7817.md), and the ['iysh](../../strongs/h/h376.md) [šāp̄ēl](../../strongs/h/h8213.md) himself: therefore [nasa'](../../strongs/h/h5375.md) them not.

<a name="isaiah_2_10"></a>Isaiah 2:10

[bow'](../../strongs/h/h935.md) into the [tsuwr](../../strongs/h/h6697.md), and [taman](../../strongs/h/h2934.md) thee in the ['aphar](../../strongs/h/h6083.md), for [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [hadar](../../strongs/h/h1926.md) of his [gā'ôn](../../strongs/h/h1347.md).

<a name="isaiah_2_11"></a>Isaiah 2:11

The [gaḇhûṯ](../../strongs/h/h1365.md) ['ayin](../../strongs/h/h5869.md) of ['adam](../../strongs/h/h120.md) shall be [šāp̄ēl](../../strongs/h/h8213.md), and the [rûm](../../strongs/h/h7312.md) of ['enowsh](../../strongs/h/h582.md) shall be [shachach](../../strongs/h/h7817.md), and [Yĕhovah](../../strongs/h/h3068.md) alone shall be [śāḡaḇ](../../strongs/h/h7682.md) in that [yowm](../../strongs/h/h3117.md).

<a name="isaiah_2_12"></a>Isaiah 2:12

For the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall be upon every one that is [gē'ê](../../strongs/h/h1343.md) and [ruwm](../../strongs/h/h7311.md), and upon every one that is [nasa'](../../strongs/h/h5375.md); and he shall be [šāp̄ēl](../../strongs/h/h8213.md):

<a name="isaiah_2_13"></a>Isaiah 2:13

And upon all the ['erez](../../strongs/h/h730.md) of [Lᵊḇānôn](../../strongs/h/h3844.md), that are [ruwm](../../strongs/h/h7311.md) and [nasa'](../../strongs/h/h5375.md), and upon all the ['allôn](../../strongs/h/h437.md) of [Bāšān](../../strongs/h/h1316.md),

<a name="isaiah_2_14"></a>Isaiah 2:14

And upon all the [ruwm](../../strongs/h/h7311.md) [har](../../strongs/h/h2022.md), and upon all the [giḇʿâ](../../strongs/h/h1389.md) that are [nasa'](../../strongs/h/h5375.md),

<a name="isaiah_2_15"></a>Isaiah 2:15

And upon every [gāḇōha](../../strongs/h/h1364.md) [miḡdāl](../../strongs/h/h4026.md), and upon every [bāṣar](../../strongs/h/h1219.md) [ḥômâ](../../strongs/h/h2346.md),

<a name="isaiah_2_16"></a>Isaiah 2:16

And upon all the ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md), and upon all [ḥemdâ](../../strongs/h/h2532.md) [śᵊḵîyâ](../../strongs/h/h7914.md).

<a name="isaiah_2_17"></a>Isaiah 2:17

And the [gaḇhûṯ](../../strongs/h/h1365.md) of ['adam](../../strongs/h/h120.md) shall be [shachach](../../strongs/h/h7817.md), and the [rûm](../../strongs/h/h7312.md) of ['enowsh](../../strongs/h/h582.md) shall be [šāp̄ēl](../../strongs/h/h8213.md): and [Yĕhovah](../../strongs/h/h3068.md) alone shall be [śāḡaḇ](../../strongs/h/h7682.md) in that day.

<a name="isaiah_2_18"></a>Isaiah 2:18

And the ['ĕlîl](../../strongs/h/h457.md) he shall [kālîl](../../strongs/h/h3632.md) [ḥālap̄](../../strongs/h/h2498.md).

<a name="isaiah_2_19"></a>Isaiah 2:19

And they shall [bow'](../../strongs/h/h935.md) into the [mᵊʿārâ](../../strongs/h/h4631.md) of the [tsuwr](../../strongs/h/h6697.md), and into the [mᵊḥillâ](../../strongs/h/h4247.md) of the ['aphar](../../strongs/h/h6083.md), for [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [hadar](../../strongs/h/h1926.md) of his [gā'ôn](../../strongs/h/h1347.md), when he [quwm](../../strongs/h/h6965.md) to [ʿāraṣ](../../strongs/h/h6206.md) the ['erets](../../strongs/h/h776.md).

<a name="isaiah_2_20"></a>Isaiah 2:20

In that [yowm](../../strongs/h/h3117.md) an ['adam](../../strongs/h/h120.md) shall [shalak](../../strongs/h/h7993.md) his ['ĕlîl](../../strongs/h/h457.md) of [keceph](../../strongs/h/h3701.md), and his ['ĕlîl](../../strongs/h/h457.md) of [zāhāḇ](../../strongs/h/h2091.md), which they made for himself to [shachah](../../strongs/h/h7812.md), to the [ḥăp̄arpērâ](../../strongs/h/h2661.md) [pērâ](../../strongs/h/h6512.md) and to the [ʿăṭallēp̄](../../strongs/h/h5847.md);

<a name="isaiah_2_21"></a>Isaiah 2:21

To [bow'](../../strongs/h/h935.md) into the [nᵊqārâ](../../strongs/h/h5366.md) of the [tsuwr](../../strongs/h/h6697.md), and into the [sᵊʿîp̄](../../strongs/h/h5585.md) of the [cela'](../../strongs/h/h5553.md), for [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [hadar](../../strongs/h/h1926.md) of his [gā'ôn](../../strongs/h/h1347.md), when he ariseth to [ʿāraṣ](../../strongs/h/h6206.md) the ['erets](../../strongs/h/h776.md).

<a name="isaiah_2_22"></a>Isaiah 2:22

[ḥāḏal](../../strongs/h/h2308.md) ye from ['adam](../../strongs/h/h120.md), whose [neshamah](../../strongs/h/h5397.md) is in his ['aph](../../strongs/h/h639.md): for wherein is he to be [chashab](../../strongs/h/h2803.md)?

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 1](isaiah_1.md) - [Isaiah 3](isaiah_3.md)

---

[^1]: [Isaiah 2:4 Commentary](../../commentary/isaiah/isaiah_2_commentary.md#isaiah_2_4)
