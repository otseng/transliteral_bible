# [Isaiah 51](https://www.blueletterbible.org/kjv/isa/51/1/s_730001)

<a name="isaiah_51_1"></a>Isaiah 51:1

[shama'](../../strongs/h/h8085.md) to me, ye that [radaph](../../strongs/h/h7291.md) [tsedeq](../../strongs/h/h6664.md), ye that [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md): look unto the [tsuwr](../../strongs/h/h6697.md) whence ye are [ḥāṣaḇ](../../strongs/h/h2672.md), and to the [maqqeḇeṯ](../../strongs/h/h4718.md) of the [bowr](../../strongs/h/h953.md) whence ye are [nāqar](../../strongs/h/h5365.md).

<a name="isaiah_51_2"></a>Isaiah 51:2

[nabat](../../strongs/h/h5027.md) unto ['Abraham](../../strongs/h/h85.md) your ['ab](../../strongs/h/h1.md), and unto [Śārâ](../../strongs/h/h8283.md) that [chuwl](../../strongs/h/h2342.md) you: for I [qara'](../../strongs/h/h7121.md) him ['echad](../../strongs/h/h259.md), and [barak](../../strongs/h/h1288.md) him, and [rabah](../../strongs/h/h7235.md) him.

<a name="isaiah_51_3"></a>Isaiah 51:3

For [Yĕhovah](../../strongs/h/h3068.md) shall [nacham](../../strongs/h/h5162.md) [Tsiyown](../../strongs/h/h6726.md): he will [nacham](../../strongs/h/h5162.md) all her [chorbah](../../strongs/h/h2723.md); and he will [śûm](../../strongs/h/h7760.md) her [midbar](../../strongs/h/h4057.md) like ['Eden](../../strongs/h/h5731.md), and her ['arabah](../../strongs/h/h6160.md) like the [gan](../../strongs/h/h1588.md) of [Yĕhovah](../../strongs/h/h3068.md); [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md) shall be [māṣā'](../../strongs/h/h4672.md) therein, [tôḏâ](../../strongs/h/h8426.md), and the [qowl](../../strongs/h/h6963.md) of [zimrâ](../../strongs/h/h2172.md).

<a name="isaiah_51_4"></a>Isaiah 51:4

[qashab](../../strongs/h/h7181.md) unto me, my ['am](../../strongs/h/h5971.md); and ['azan](../../strongs/h/h238.md) unto me, O my [lĕom](../../strongs/h/h3816.md): for a [towrah](../../strongs/h/h8451.md) shall [yāṣā'](../../strongs/h/h3318.md) from me, and I will make my [mishpat](../../strongs/h/h4941.md) to [rāḡaʿ](../../strongs/h/h7280.md) for a ['owr](../../strongs/h/h216.md) of the ['am](../../strongs/h/h5971.md).

<a name="isaiah_51_5"></a>Isaiah 51:5

My [tsedeq](../../strongs/h/h6664.md) is [qarowb](../../strongs/h/h7138.md); my [yesha'](../../strongs/h/h3468.md) is [yāṣā'](../../strongs/h/h3318.md), and mine [zerowa'](../../strongs/h/h2220.md) shall [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md); the ['î](../../strongs/h/h339.md) shall [qāvâ](../../strongs/h/h6960.md) upon me, and on mine [zerowa'](../../strongs/h/h2220.md) shall they [yāḥal](../../strongs/h/h3176.md).

<a name="isaiah_51_6"></a>Isaiah 51:6

[nasa'](../../strongs/h/h5375.md) your ['ayin](../../strongs/h/h5869.md) to the [shamayim](../../strongs/h/h8064.md), and [nabat](../../strongs/h/h5027.md) upon the ['erets](../../strongs/h/h776.md) beneath: for the [shamayim](../../strongs/h/h8064.md) shall [mālaḥ](../../strongs/h/h4414.md) like ['ashan](../../strongs/h/h6227.md), and the ['erets](../../strongs/h/h776.md) shall [bālâ](../../strongs/h/h1086.md) like a [beḡeḏ](../../strongs/h/h899.md), and they that [yashab](../../strongs/h/h3427.md) therein shall [muwth](../../strongs/h/h4191.md) in like manner: but my [yĕshuw'ah](../../strongs/h/h3444.md) shall be ['owlam](../../strongs/h/h5769.md), and my [tsedaqah](../../strongs/h/h6666.md) shall not be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="isaiah_51_7"></a>Isaiah 51:7

[shama'](../../strongs/h/h8085.md) unto me, ye that [yada'](../../strongs/h/h3045.md) [tsedeq](../../strongs/h/h6664.md), the ['am](../../strongs/h/h5971.md) in whose [leb](../../strongs/h/h3820.md) is my [towrah](../../strongs/h/h8451.md); [yare'](../../strongs/h/h3372.md) ye not the [cherpah](../../strongs/h/h2781.md) of ['enowsh](../../strongs/h/h582.md), neither be ye [ḥāṯaṯ](../../strongs/h/h2865.md) of their [gidûp̄](../../strongs/h/h1421.md).

<a name="isaiah_51_8"></a>Isaiah 51:8

For the [ʿāš](../../strongs/h/h6211.md) shall ['akal](../../strongs/h/h398.md) them up like a [beḡeḏ](../../strongs/h/h899.md), and the [sās](../../strongs/h/h5580.md) shall ['akal](../../strongs/h/h398.md) them like [ṣemer](../../strongs/h/h6785.md): but my [ṣĕdāqāh](../../strongs/h/h6666.md) shall be ['owlam](../../strongs/h/h5769.md), and my [yĕshuw'ah](../../strongs/h/h3444.md) from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md).

<a name="isaiah_51_9"></a>Isaiah 51:9

[ʿûr](../../strongs/h/h5782.md), [ʿûr](../../strongs/h/h5782.md), put on ['oz](../../strongs/h/h5797.md), O [zerowa'](../../strongs/h/h2220.md) of [Yĕhovah](../../strongs/h/h3068.md); [ʿûr](../../strongs/h/h5782.md), as in the [qeḏem](../../strongs/h/h6924.md) [yowm](../../strongs/h/h3117.md), in the [dôr](../../strongs/h/h1755.md) of ['owlam](../../strongs/h/h5769.md). Art thou not it that hath cut [rahaḇ](../../strongs/h/h7294.md), and [ḥālal](../../strongs/h/h2490.md) the [tannîn](../../strongs/h/h8577.md)?

<a name="isaiah_51_10"></a>Isaiah 51:10

Art thou not it which hath [ḥāraḇ](../../strongs/h/h2717.md) the [yam](../../strongs/h/h3220.md), the [mayim](../../strongs/h/h4325.md) of the great [tĕhowm](../../strongs/h/h8415.md); that hath made the [maʿămaqqîm](../../strongs/h/h4615.md) of the [yam](../../strongs/h/h3220.md) a [derek](../../strongs/h/h1870.md) for the [gā'al](../../strongs/h/h1350.md) to ['abar](../../strongs/h/h5674.md)?

<a name="isaiah_51_11"></a>Isaiah 51:11

Therefore the [pāḏâ](../../strongs/h/h6299.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [shuwb](../../strongs/h/h7725.md), and come with [rinnah](../../strongs/h/h7440.md) unto [Tsiyown](../../strongs/h/h6726.md); and ['owlam](../../strongs/h/h5769.md) [simchah](../../strongs/h/h8057.md) shall be upon their [ro'sh](../../strongs/h/h7218.md): they shall [nāśaḡ](../../strongs/h/h5381.md) [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md); and [yagown](../../strongs/h/h3015.md) and ['anachah](../../strongs/h/h585.md) shall [nûs](../../strongs/h/h5127.md).

<a name="isaiah_51_12"></a>Isaiah 51:12

I, even I, am he that [nacham](../../strongs/h/h5162.md) you: who art thou, that thou shouldest be [yare'](../../strongs/h/h3372.md) of an ['enowsh](../../strongs/h/h582.md) that shall [muwth](../../strongs/h/h4191.md), and of the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md) which shall be [nathan](../../strongs/h/h5414.md) as [chatsiyr](../../strongs/h/h2682.md);

<a name="isaiah_51_13"></a>Isaiah 51:13

And [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['asah](../../strongs/h/h6213.md), that hath [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md), and laid the [yacad](../../strongs/h/h3245.md) of the ['erets](../../strongs/h/h776.md); and hast [pachad](../../strongs/h/h6342.md) [tāmîḏ](../../strongs/h/h8548.md) every [yowm](../../strongs/h/h3117.md) because of the [chemah](../../strongs/h/h2534.md) of the [ṣûq](../../strongs/h/h6693.md), as if he were [kuwn](../../strongs/h/h3559.md) to [shachath](../../strongs/h/h7843.md)? and where is the [chemah](../../strongs/h/h2534.md) of the [ṣûq](../../strongs/h/h6693.md)?

<a name="isaiah_51_14"></a>Isaiah 51:14

The [ṣāʿâ](../../strongs/h/h6808.md) [māhar](../../strongs/h/h4116.md) that he may be [pāṯaḥ](../../strongs/h/h6605.md), and that he should not [muwth](../../strongs/h/h4191.md) in the [shachath](../../strongs/h/h7845.md), nor that his [lechem](../../strongs/h/h3899.md) should [ḥāsēr](../../strongs/h/h2637.md).

<a name="isaiah_51_15"></a>Isaiah 51:15

But I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), that [rāḡaʿ](../../strongs/h/h7280.md) the [yam](../../strongs/h/h3220.md), whose [gal](../../strongs/h/h1530.md) [hāmâ](../../strongs/h/h1993.md): [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md).

<a name="isaiah_51_16"></a>Isaiah 51:16

And I have [śûm](../../strongs/h/h7760.md) my [dabar](../../strongs/h/h1697.md) in thy [peh](../../strongs/h/h6310.md), and I have [kāsâ](../../strongs/h/h3680.md) thee in the [ṣēl](../../strongs/h/h6738.md) of mine [yad](../../strongs/h/h3027.md), that I may [nāṭaʿ](../../strongs/h/h5193.md) the [shamayim](../../strongs/h/h8064.md), and lay the [yacad](../../strongs/h/h3245.md) of the ['erets](../../strongs/h/h776.md), and say unto [Tsiyown](../../strongs/h/h6726.md), Thou art my ['am](../../strongs/h/h5971.md).

<a name="isaiah_51_17"></a>Isaiah 51:17

[ʿûr](../../strongs/h/h5782.md), [ʿûr](../../strongs/h/h5782.md), [quwm](../../strongs/h/h6965.md), O [Yĕruwshalaim](../../strongs/h/h3389.md), which hast [šāṯâ](../../strongs/h/h8354.md) at the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) the [kowc](../../strongs/h/h3563.md) of his [chemah](../../strongs/h/h2534.md); thou hast [šāṯâ](../../strongs/h/h8354.md) the [qubaʿaṯ](../../strongs/h/h6907.md) of the [kowc](../../strongs/h/h3563.md) of [tarʿēlâ](../../strongs/h/h8653.md), and [māṣâ](../../strongs/h/h4680.md) them.

<a name="isaiah_51_18"></a>Isaiah 51:18

There is none to [nāhal](../../strongs/h/h5095.md) her among all the [ben](../../strongs/h/h1121.md) whom she hath [yalad](../../strongs/h/h3205.md); neither is there any that [ḥāzaq](../../strongs/h/h2388.md) her by the [yad](../../strongs/h/h3027.md) of all the [ben](../../strongs/h/h1121.md) that she hath [gāḏal](../../strongs/h/h1431.md).

<a name="isaiah_51_19"></a>Isaiah 51:19

These [šᵊnayim](../../strongs/h/h8147.md) are [qārā'](../../strongs/h/h7122.md) unto thee; who shall be [nuwd](../../strongs/h/h5110.md) for thee?  [shod](../../strongs/h/h7701.md), and [šeḇar](../../strongs/h/h7667.md), and the [rāʿāḇ](../../strongs/h/h7458.md), and the [chereb](../../strongs/h/h2719.md): by whom shall I [nacham](../../strongs/h/h5162.md) thee?

<a name="isaiah_51_20"></a>Isaiah 51:20

Thy [ben](../../strongs/h/h1121.md) have [ʿālap̄](../../strongs/h/h5968.md), they [shakab](../../strongs/h/h7901.md) at the [ro'sh](../../strongs/h/h7218.md) of all the [ḥûṣ](../../strongs/h/h2351.md), as a [tᵊ'ô](../../strongs/h/h8377.md) in a [miḵmār](../../strongs/h/h4364.md): they are [mālē'](../../strongs/h/h4392.md) of the [chemah](../../strongs/h/h2534.md) of [Yĕhovah](../../strongs/h/h3068.md), the [ge'arah](../../strongs/h/h1606.md) of thy ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_51_21"></a>Isaiah 51:21

Therefore [shama'](../../strongs/h/h8085.md) now this, thou ['aniy](../../strongs/h/h6041.md), and [šāḵar](../../strongs/h/h7937.md), but not with [yayin](../../strongs/h/h3196.md):

<a name="isaiah_51_22"></a>Isaiah 51:22

Thus ['āmar](../../strongs/h/h559.md) thy ['adown](../../strongs/h/h113.md) [Yĕhovah](../../strongs/h/h3068.md), and thy ['Elohiym](../../strongs/h/h430.md) that [riyb](../../strongs/h/h7378.md) the cause of his ['am](../../strongs/h/h5971.md), Behold, I have [laqach](../../strongs/h/h3947.md) of thine [yad](../../strongs/h/h3027.md) the [kowc](../../strongs/h/h3563.md) of [tarʿēlâ](../../strongs/h/h8653.md), even the [qubaʿaṯ](../../strongs/h/h6907.md) of the [kowc](../../strongs/h/h3563.md) of my [chemah](../../strongs/h/h2534.md); thou shalt no more [šāṯâ](../../strongs/h/h8354.md) it again:

<a name="isaiah_51_23"></a>Isaiah 51:23

But I will [śûm](../../strongs/h/h7760.md) it into the [yad](../../strongs/h/h3027.md) of them that [yāḡâ](../../strongs/h/h3013.md) thee; which have ['āmar](../../strongs/h/h559.md) to thy [nephesh](../../strongs/h/h5315.md), [shachah](../../strongs/h/h7812.md), that we may ['abar](../../strongs/h/h5674.md): and thou hast [śûm](../../strongs/h/h7760.md) thy [gēv](../../strongs/h/h1460.md) as the ['erets](../../strongs/h/h776.md), and as the [ḥûṣ](../../strongs/h/h2351.md), to them that ['abar](../../strongs/h/h5674.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 50](isaiah_50.md) - [Isaiah 52](isaiah_52.md)