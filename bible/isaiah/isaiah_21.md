# [Isaiah 21](https://www.blueletterbible.org/kjv/isa/21/1/s_700001)

<a name="isaiah_21_1"></a>Isaiah 21:1

The [maśśā'](../../strongs/h/h4853.md) of the [midbar](../../strongs/h/h4057.md) of the [yam](../../strongs/h/h3220.md). As [sûp̄â](../../strongs/h/h5492.md) in the [neḡeḇ](../../strongs/h/h5045.md) [ḥālap̄](../../strongs/h/h2498.md) through; so it [bow'](../../strongs/h/h935.md) from the [midbar](../../strongs/h/h4057.md), from a [yare'](../../strongs/h/h3372.md) ['erets](../../strongs/h/h776.md).

<a name="isaiah_21_2"></a>Isaiah 21:2

A [qāšê](../../strongs/h/h7186.md) [ḥāzûṯ](../../strongs/h/h2380.md) is [nāḡaḏ](../../strongs/h/h5046.md) unto me; the [bāḡaḏ](../../strongs/h/h898.md) [bāḡaḏ](../../strongs/h/h898.md), and the [shadad](../../strongs/h/h7703.md) [shadad](../../strongs/h/h7703.md). Go up, O [ʿÊlām](../../strongs/h/h5867.md): [ṣûr](../../strongs/h/h6696.md), O [Māḏay](../../strongs/h/h4074.md); all the ['anachah](../../strongs/h/h585.md) thereof have I made to [shabath](../../strongs/h/h7673.md).

<a name="isaiah_21_3"></a>Isaiah 21:3

Therefore are my [māṯnayim](../../strongs/h/h4975.md) [mālā'](../../strongs/h/h4390.md) with [ḥalḥālâ](../../strongs/h/h2479.md): [ṣîr](../../strongs/h/h6735.md) have ['āḥaz](../../strongs/h/h270.md) upon me, as the [ṣîr](../../strongs/h/h6735.md) of a [yalad](../../strongs/h/h3205.md): I was [ʿāvâ](../../strongs/h/h5753.md) at the [shama'](../../strongs/h/h8085.md) of it; I was [bahal](../../strongs/h/h926.md) at the [ra'ah](../../strongs/h/h7200.md) of it.

<a name="isaiah_21_4"></a>Isaiah 21:4

My [lebab](../../strongs/h/h3824.md) [tāʿâ](../../strongs/h/h8582.md), [pallāṣûṯ](../../strongs/h/h6427.md) [ba'ath](../../strongs/h/h1204.md) me: the [nešep̄](../../strongs/h/h5399.md) of my [ḥēšeq](../../strongs/h/h2837.md) hath he [śûm](../../strongs/h/h7760.md) into [ḥărāḏâ](../../strongs/h/h2731.md) unto me.

<a name="isaiah_21_5"></a>Isaiah 21:5

['arak](../../strongs/h/h6186.md) the [šulḥān](../../strongs/h/h7979.md), [tsaphah](../../strongs/h/h6822.md) in the [ṣāp̄îṯ](../../strongs/h/h6844.md), ['akal](../../strongs/h/h398.md), [šāṯâ](../../strongs/h/h8354.md): [quwm](../../strongs/h/h6965.md), ye [śar](../../strongs/h/h8269.md), and [māšaḥ](../../strongs/h/h4886.md) the [magen](../../strongs/h/h4043.md).

<a name="isaiah_21_6"></a>Isaiah 21:6

For thus hath ['adonay](../../strongs/h/h136.md) said unto me, [yālaḵ](../../strongs/h/h3212.md), ['amad](../../strongs/h/h5975.md) a [tsaphah](../../strongs/h/h6822.md), let him [nāḡaḏ](../../strongs/h/h5046.md) what he [ra'ah](../../strongs/h/h7200.md).

<a name="isaiah_21_7"></a>Isaiah 21:7

And he [ra'ah](../../strongs/h/h7200.md) a [reḵeḇ](../../strongs/h/h7393.md) with a couple of [pārāš](../../strongs/h/h6571.md), a [reḵeḇ](../../strongs/h/h7393.md) of [chamowr](../../strongs/h/h2543.md), and a [reḵeḇ](../../strongs/h/h7393.md) of [gāmāl](../../strongs/h/h1581.md); and he [qashab](../../strongs/h/h7181.md) [qešeḇ](../../strongs/h/h7182.md) with much heed:

<a name="isaiah_21_8"></a>Isaiah 21:8

And he [qara'](../../strongs/h/h7121.md), An ['ariy](../../strongs/h/h738.md): My ['adonay](../../strongs/h/h136.md), I ['amad](../../strongs/h/h5975.md) [tāmîḏ](../../strongs/h/h8548.md) upon the [miṣpê](../../strongs/h/h4707.md) in the [yômām](../../strongs/h/h3119.md), and I am [nāṣaḇ](../../strongs/h/h5324.md) in my [mišmereṯ](../../strongs/h/h4931.md) [layil](../../strongs/h/h3915.md):

<a name="isaiah_21_9"></a>Isaiah 21:9

And, behold, here [bow'](../../strongs/h/h935.md) a [reḵeḇ](../../strongs/h/h7393.md) of ['iysh](../../strongs/h/h376.md), with a couple of [pārāš](../../strongs/h/h6571.md). And he ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [Bāḇel](../../strongs/h/h894.md) is [naphal](../../strongs/h/h5307.md), is [naphal](../../strongs/h/h5307.md); and all the [pāsîl](../../strongs/h/h6456.md) of her ['Elohiym](../../strongs/h/h430.md) he hath [shabar](../../strongs/h/h7665.md) unto the ['erets](../../strongs/h/h776.md).

<a name="isaiah_21_10"></a>Isaiah 21:10

O my [mᵊḏušâ](../../strongs/h/h4098.md), and the [ben](../../strongs/h/h1121.md) of my [gōren](../../strongs/h/h1637.md): that which I have [shama'](../../strongs/h/h8085.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), have I [nāḡaḏ](../../strongs/h/h5046.md) unto you.

<a name="isaiah_21_11"></a>Isaiah 21:11

The [maśśā'](../../strongs/h/h4853.md) of [Dûmâ](../../strongs/h/h1746.md). He [qara'](../../strongs/h/h7121.md) to me out of [Śēʿîr](../../strongs/h/h8165.md), [shamar](../../strongs/h/h8104.md), what of the [layil](../../strongs/h/h3915.md)? [shamar](../../strongs/h/h8104.md), what of the [layil](../../strongs/h/h3915.md)?

<a name="isaiah_21_12"></a>Isaiah 21:12

The [shamar](../../strongs/h/h8104.md) ['āmar](../../strongs/h/h559.md), The [boqer](../../strongs/h/h1242.md) ['āṯâ](../../strongs/h/h857.md), and also the [layil](../../strongs/h/h3915.md): if ye will [bāʿâ](../../strongs/h/h1158.md), [bāʿâ](../../strongs/h/h1158.md) ye: [shuwb](../../strongs/h/h7725.md), ['āṯâ](../../strongs/h/h857.md).

<a name="isaiah_21_13"></a>Isaiah 21:13

The [maśśā'](../../strongs/h/h4853.md) upon [ʿĂrāḇ](../../strongs/h/h6152.md). In the [yaʿar](../../strongs/h/h3293.md) in [ʿĂrāḇ](../../strongs/h/h6152.md) shall ye [lûn](../../strongs/h/h3885.md), O ye ['ōrḥâ](../../strongs/h/h736.md) of [dᵊḏānî](../../strongs/h/h1720.md).

<a name="isaiah_21_14"></a>Isaiah 21:14

The [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) of [Têmā'](../../strongs/h/h8485.md) ['āṯâ](../../strongs/h/h857.md) [mayim](../../strongs/h/h4325.md) to him that was [ṣāmē'](../../strongs/h/h6771.md), they [qadam](../../strongs/h/h6923.md) with their [lechem](../../strongs/h/h3899.md) him that [nāḏaḏ](../../strongs/h/h5074.md).

<a name="isaiah_21_15"></a>Isaiah 21:15

For they [nāḏaḏ](../../strongs/h/h5074.md) from the [chereb](../../strongs/h/h2719.md), from the [nāṭaš](../../strongs/h/h5203.md) [chereb](../../strongs/h/h2719.md), and from the [dāraḵ](../../strongs/h/h1869.md) [qesheth](../../strongs/h/h7198.md), and from the [kōḇeḏ](../../strongs/h/h3514.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="isaiah_21_16"></a>Isaiah 21:16

For thus hath ['adonay](../../strongs/h/h136.md) ['āmar](../../strongs/h/h559.md) unto me, Within a [šānâ](../../strongs/h/h8141.md), according to the [šānâ](../../strongs/h/h8141.md) of an [śāḵîr](../../strongs/h/h7916.md), and all the [kabowd](../../strongs/h/h3519.md) of [Qēḏār](../../strongs/h/h6938.md) shall [kalah](../../strongs/h/h3615.md):

<a name="isaiah_21_17"></a>Isaiah 21:17

And the [šᵊ'ār](../../strongs/h/h7605.md) of the [mispār](../../strongs/h/h4557.md) of [qesheth](../../strongs/h/h7198.md), the [gibôr](../../strongs/h/h1368.md) of the [ben](../../strongs/h/h1121.md) of [Qēḏār](../../strongs/h/h6938.md), shall be [māʿaṭ](../../strongs/h/h4591.md): for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) hath [dabar](../../strongs/h/h1696.md) it.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 20](isaiah_20.md) - [Isaiah 22](isaiah_22.md)