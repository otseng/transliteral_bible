# [Isaiah 40](https://www.blueletterbible.org/kjv/isa/40/1/s_719001)

<a name="isaiah_40_1"></a>Isaiah 40:1

[nacham](../../strongs/h/h5162.md) ye, [nacham](../../strongs/h/h5162.md) ye my ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_40_2"></a>Isaiah 40:2

[dabar](../../strongs/h/h1696.md) ye [leb](../../strongs/h/h3820.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and [qara'](../../strongs/h/h7121.md) unto her, that her [tsaba'](../../strongs/h/h6635.md) is [mālā'](../../strongs/h/h4390.md), that her ['avon](../../strongs/h/h5771.md) is [ratsah](../../strongs/h/h7521.md): for she hath [laqach](../../strongs/h/h3947.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) double for all her [chatta'ath](../../strongs/h/h2403.md).

<a name="isaiah_40_3"></a>Isaiah 40:3

The [qowl](../../strongs/h/h6963.md) of him that [qara'](../../strongs/h/h7121.md) in the [midbar](../../strongs/h/h4057.md), [panah](../../strongs/h/h6437.md) ye the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), make [yashar](../../strongs/h/h3474.md) in the ['arabah](../../strongs/h/h6160.md) a [mĕcillah](../../strongs/h/h4546.md) for our ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_40_4"></a>Isaiah 40:4

Every [gay'](../../strongs/h/h1516.md) shall be [nasa'](../../strongs/h/h5375.md), and every [har](../../strongs/h/h2022.md) and [giḇʿâ](../../strongs/h/h1389.md) shall be made [šāp̄ēl](../../strongs/h/h8213.md): and the ['aqob](../../strongs/h/h6121.md) shall be made [mîšôr](../../strongs/h/h4334.md), and the [reḵes](../../strongs/h/h7406.md) [biqʿâ](../../strongs/h/h1237.md):

<a name="isaiah_40_5"></a>Isaiah 40:5

And the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be [gālâ](../../strongs/h/h1540.md), and all [basar](../../strongs/h/h1320.md) shall [ra'ah](../../strongs/h/h7200.md) it together: for the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="isaiah_40_6"></a>Isaiah 40:6

The [qowl](../../strongs/h/h6963.md) said, [qara'](../../strongs/h/h7121.md). And he ['āmar](../../strongs/h/h559.md), What shall I [qara'](../../strongs/h/h7121.md)? All [basar](../../strongs/h/h1320.md) is [chatsiyr](../../strongs/h/h2682.md), and all the [checed](../../strongs/h/h2617.md) thereof is as the [tsiyts](../../strongs/h/h6731.md) of the [sadeh](../../strongs/h/h7704.md):

<a name="isaiah_40_7"></a>Isaiah 40:7

The [chatsiyr](../../strongs/h/h2682.md) [yāḇēš](../../strongs/h/h3001.md), the [tsiyts](../../strongs/h/h6731.md) [nabel](../../strongs/h/h5034.md): because the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāšaḇ](../../strongs/h/h5380.md) upon it: surely the ['am](../../strongs/h/h5971.md) is [chatsiyr](../../strongs/h/h2682.md).

<a name="isaiah_40_8"></a>Isaiah 40:8

The [chatsiyr](../../strongs/h/h2682.md) [yāḇēš](../../strongs/h/h3001.md), the [tsiyts](../../strongs/h/h6731.md) [nabel](../../strongs/h/h5034.md): but the [dabar](../../strongs/h/h1697.md) of our ['Elohiym](../../strongs/h/h430.md) shall [quwm](../../strongs/h/h6965.md) ['owlam](../../strongs/h/h5769.md).

<a name="isaiah_40_9"></a>Isaiah 40:9

O [Tsiyown](../../strongs/h/h6726.md), that [bāśar](../../strongs/h/h1319.md), [ʿālâ](../../strongs/h/h5927.md) thee into the [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md); O [Yĕruwshalaim](../../strongs/h/h3389.md), that [bāśar](../../strongs/h/h1319.md), [ruwm](../../strongs/h/h7311.md) thy [qowl](../../strongs/h/h6963.md) with [koach](../../strongs/h/h3581.md); [ruwm](../../strongs/h/h7311.md), be not [yare'](../../strongs/h/h3372.md); ['āmar](../../strongs/h/h559.md) unto the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), Behold your ['Elohiym](../../strongs/h/h430.md)!

<a name="isaiah_40_10"></a>Isaiah 40:10

Behold, ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will [bow'](../../strongs/h/h935.md) with [ḥāzāq](../../strongs/h/h2389.md), and his [zerowa'](../../strongs/h/h2220.md) shall [mashal](../../strongs/h/h4910.md) for him: behold, his [śāḵār](../../strongs/h/h7939.md) is with him, and his [pe'ullah](../../strongs/h/h6468.md) [paniym](../../strongs/h/h6440.md) him.

<a name="isaiah_40_11"></a>Isaiah 40:11

He shall [ra'ah](../../strongs/h/h7462.md) his [ʿēḏer](../../strongs/h/h5739.md) like a [ra'ah](../../strongs/h/h7462.md): he shall [qāḇaṣ](../../strongs/h/h6908.md) the [ṭᵊlî](../../strongs/h/h2922.md) with his [zerowa'](../../strongs/h/h2220.md), and [nasa'](../../strongs/h/h5375.md) them in his [ḥêq](../../strongs/h/h2436.md), and shall gently [nāhal](../../strongs/h/h5095.md) those that are with [ʿûl](../../strongs/h/h5763.md).

<a name="isaiah_40_12"></a>Isaiah 40:12

Who hath [māḏaḏ](../../strongs/h/h4058.md) the [mayim](../../strongs/h/h4325.md) in the [šōʿal](../../strongs/h/h8168.md), and [tāḵan](../../strongs/h/h8505.md) [shamayim](../../strongs/h/h8064.md) with the [zereṯ](../../strongs/h/h2239.md), and [kûl](../../strongs/h/h3557.md) the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md) in a [šālîš](../../strongs/h/h7991.md), and [šāqal](../../strongs/h/h8254.md) the [har](../../strongs/h/h2022.md) in [peles](../../strongs/h/h6425.md), and the [giḇʿâ](../../strongs/h/h1389.md) in a [mō'znayim](../../strongs/h/h3976.md)?

<a name="isaiah_40_13"></a>Isaiah 40:13

Who hath [tāḵan](../../strongs/h/h8505.md) the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md), or being his ['iysh](../../strongs/h/h376.md) ['etsah](../../strongs/h/h6098.md) hath [yada'](../../strongs/h/h3045.md) him?

<a name="isaiah_40_14"></a>Isaiah 40:14

With whom took he [ya'ats](../../strongs/h/h3289.md), and who [bîn](../../strongs/h/h995.md) him, and [lamad](../../strongs/h/h3925.md) him in the ['orach](../../strongs/h/h734.md) of [mishpat](../../strongs/h/h4941.md), and [lamad](../../strongs/h/h3925.md) him [da'ath](../../strongs/h/h1847.md), and shewed to him the [derek](../../strongs/h/h1870.md) of [tāḇûn](../../strongs/h/h8394.md)?

<a name="isaiah_40_15"></a>Isaiah 40:15

Behold, the [gowy](../../strongs/h/h1471.md) are as a [mar](../../strongs/h/h4752.md) of a [dᵊlî](../../strongs/h/h1805.md), and are [chashab](../../strongs/h/h2803.md) as the [shachaq](../../strongs/h/h7834.md) of the [mō'znayim](../../strongs/h/h3976.md): behold, he [nāṭal](../../strongs/h/h5190.md) the ['î](../../strongs/h/h339.md) as a [daq](../../strongs/h/h1851.md).

<a name="isaiah_40_16"></a>Isaiah 40:16

And [Lᵊḇānôn](../../strongs/h/h3844.md) is not [day](../../strongs/h/h1767.md) to [bāʿar](../../strongs/h/h1197.md), nor the [chay](../../strongs/h/h2416.md) thereof [day](../../strongs/h/h1767.md) for an [ʿōlâ](../../strongs/h/h5930.md).

<a name="isaiah_40_17"></a>Isaiah 40:17

All [gowy](../../strongs/h/h1471.md) before him are as nothing; and they are [chashab](../../strongs/h/h2803.md) to him less than ['ep̄es](../../strongs/h/h657.md), and [tohuw](../../strongs/h/h8414.md).

<a name="isaiah_40_18"></a>Isaiah 40:18

To whom then will ye liken ['el](../../strongs/h/h410.md)? or what [dĕmuwth](../../strongs/h/h1823.md) will ye ['arak](../../strongs/h/h6186.md) unto him?

<a name="isaiah_40_19"></a>Isaiah 40:19

The [ḥārāš](../../strongs/h/h2796.md) [nacak](../../strongs/h/h5258.md) a [pecel](../../strongs/h/h6459.md), and the [tsaraph](../../strongs/h/h6884.md) spreadeth it over with [zāhāḇ](../../strongs/h/h2091.md), and [tsaraph](../../strongs/h/h6884.md) [keceph](../../strongs/h/h3701.md) [rᵊṯuqôṯ](../../strongs/h/h7577.md).

<a name="isaiah_40_20"></a>Isaiah 40:20

He that is so [sāḵan](../../strongs/h/h5533.md) [sāḵar](../../strongs/h/h5534.md) that he hath no [tᵊrûmâ](../../strongs/h/h8641.md) [bāḥar](../../strongs/h/h977.md) an ['ets](../../strongs/h/h6086.md) that will not [rāqaḇ](../../strongs/h/h7537.md); he seeketh unto him a [ḥāḵām](../../strongs/h/h2450.md) [ḥārāš](../../strongs/h/h2796.md) to prepare a [pecel](../../strongs/h/h6459.md), that shall not be [mowt](../../strongs/h/h4131.md).

<a name="isaiah_40_21"></a>Isaiah 40:21

Have ye not [yada'](../../strongs/h/h3045.md)? have ye not [shama'](../../strongs/h/h8085.md)? hath it not been [nāḡaḏ](../../strongs/h/h5046.md) you from the [ro'sh](../../strongs/h/h7218.md)? have ye not [bîn](../../strongs/h/h995.md) from the [mowcadah](../../strongs/h/h4146.md) of the ['erets](../../strongs/h/h776.md)?

<a name="isaiah_40_22"></a>Isaiah 40:22

It is he that [yashab](../../strongs/h/h3427.md) upon the [ḥûḡ](../../strongs/h/h2329.md) of the ['erets](../../strongs/h/h776.md), and the [yashab](../../strongs/h/h3427.md) thereof are as [ḥāḡāḇ](../../strongs/h/h2284.md); that [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) as a [dōq](../../strongs/h/h1852.md), and [māṯaḥ](../../strongs/h/h4969.md) as a ['ohel](../../strongs/h/h168.md) to [yashab](../../strongs/h/h3427.md) in:

<a name="isaiah_40_23"></a>Isaiah 40:23

That [nathan](../../strongs/h/h5414.md) the [razan](../../strongs/h/h7336.md) to nothing; he ['asah](../../strongs/h/h6213.md) the [shaphat](../../strongs/h/h8199.md) of the ['erets](../../strongs/h/h776.md) as [tohuw](../../strongs/h/h8414.md).

<a name="isaiah_40_24"></a>Isaiah 40:24

Yea, they shall not be [nāṭaʿ](../../strongs/h/h5193.md); yea, they shall not be [zāraʿ](../../strongs/h/h2232.md): yea, their [gezaʿ](../../strongs/h/h1503.md) shall not [šērēš](../../strongs/h/h8327.md) in the ['erets](../../strongs/h/h776.md): and he shall also [nāšap̄](../../strongs/h/h5398.md) upon them, and they shall [yāḇēš](../../strongs/h/h3001.md), and the [saʿar](../../strongs/h/h5591.md) shall [nasa'](../../strongs/h/h5375.md) them away as [qaš](../../strongs/h/h7179.md).

<a name="isaiah_40_25"></a>Isaiah 40:25

To whom then will ye [dāmâ](../../strongs/h/h1819.md) me, or shall I be [šāvâ](../../strongs/h/h7737.md)? saith the [qadowsh](../../strongs/h/h6918.md).

<a name="isaiah_40_26"></a>Isaiah 40:26

[nasa'](../../strongs/h/h5375.md) your ['ayin](../../strongs/h/h5869.md) on [marowm](../../strongs/h/h4791.md), and [ra'ah](../../strongs/h/h7200.md) who hath [bara'](../../strongs/h/h1254.md) these things, that bringeth out their [tsaba'](../../strongs/h/h6635.md) by [mispār](../../strongs/h/h4557.md): he [qara'](../../strongs/h/h7121.md) them all by [shem](../../strongs/h/h8034.md) by the [rōḇ](../../strongs/h/h7230.md) of his ['ôn](../../strongs/h/h202.md), for that he is ['ammîṣ](../../strongs/h/h533.md) in [koach](../../strongs/h/h3581.md); not one [ʿāḏar](../../strongs/h/h5737.md).

<a name="isaiah_40_27"></a>Isaiah 40:27

Why ['āmar](../../strongs/h/h559.md) thou, O [Ya'aqob](../../strongs/h/h3290.md), and [dabar](../../strongs/h/h1696.md), [Yisra'el](../../strongs/h/h3478.md), My [derek](../../strongs/h/h1870.md) is [cathar](../../strongs/h/h5641.md) from [Yĕhovah](../../strongs/h/h3068.md), and my [mishpat](../../strongs/h/h4941.md) is ['abar](../../strongs/h/h5674.md) from my ['Elohiym](../../strongs/h/h430.md)?

<a name="isaiah_40_28"></a>Isaiah 40:28

Hast thou not [yada'](../../strongs/h/h3045.md)? hast thou not [shama'](../../strongs/h/h8085.md), that the ['owlam](../../strongs/h/h5769.md) ['Elohiym](../../strongs/h/h430.md), [Yĕhovah](../../strongs/h/h3068.md), the [bara'](../../strongs/h/h1254.md) of the [qāṣâ](../../strongs/h/h7098.md) of the ['erets](../../strongs/h/h776.md), [yāʿap̄](../../strongs/h/h3286.md) not, neither is [yaga'](../../strongs/h/h3021.md)? there is no [ḥēqer](../../strongs/h/h2714.md) of his [tāḇûn](../../strongs/h/h8394.md).

<a name="isaiah_40_29"></a>Isaiah 40:29

He [nathan](../../strongs/h/h5414.md) [koach](../../strongs/h/h3581.md) to the [yāʿēp̄](../../strongs/h/h3287.md); and to them that have no ['ôn](../../strongs/h/h202.md) he [rabah](../../strongs/h/h7235.md) [ʿāṣmâ](../../strongs/h/h6109.md).

<a name="isaiah_40_30"></a>Isaiah 40:30

Even the [naʿar](../../strongs/h/h5288.md) shall [yāʿap̄](../../strongs/h/h3286.md) and be [yaga'](../../strongs/h/h3021.md), and the [bāḥûr](../../strongs/h/h970.md) shall [kashal](../../strongs/h/h3782.md) [kashal](../../strongs/h/h3782.md):

<a name="isaiah_40_31"></a>Isaiah 40:31

But they that [qāvâ](../../strongs/h/h6960.md) upon [Yĕhovah](../../strongs/h/h3068.md) shall [ḥālap̄](../../strongs/h/h2498.md) their [koach](../../strongs/h/h3581.md); they shall [ʿālâ](../../strongs/h/h5927.md) with ['ēḇer](../../strongs/h/h83.md) as [nesheׁr](../../strongs/h/h5404.md); they shall [rûṣ](../../strongs/h/h7323.md), and not be [yaga'](../../strongs/h/h3021.md); and they shall [yālaḵ](../../strongs/h/h3212.md), and not [yāʿap̄](../../strongs/h/h3286.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 39](isaiah_39.md) - [Isaiah 41](isaiah_41.md)