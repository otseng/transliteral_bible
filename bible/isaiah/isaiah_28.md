# [Isaiah 28](https://www.blueletterbible.org/kjv/isa/28/1/s_707001)

<a name="isaiah_28_1"></a>Isaiah 28:1

[hôy](../../strongs/h/h1945.md) to the [ʿăṭārâ](../../strongs/h/h5850.md) of [ge'uwth](../../strongs/h/h1348.md), to the [šikôr](../../strongs/h/h7910.md) of ['Ep̄rayim](../../strongs/h/h669.md), whose [ṣᵊḇî](../../strongs/h/h6643.md) [tip̄'ārâ](../../strongs/h/h8597.md) is a [nabel](../../strongs/h/h5034.md) [tsiyts](../../strongs/h/h6731.md), which are on the [ro'sh](../../strongs/h/h7218.md) of the [šemen](../../strongs/h/h8081.md) [gay'](../../strongs/h/h1516.md) of them that are [hālam](../../strongs/h/h1986.md) with [yayin](../../strongs/h/h3196.md)!

<a name="isaiah_28_2"></a>Isaiah 28:2

Behold, ['adonay](../../strongs/h/h136.md) hath a [ḥāzāq](../../strongs/h/h2389.md) and ['ammîṣ](../../strongs/h/h533.md), which as a [zerem](../../strongs/h/h2230.md) of [barad](../../strongs/h/h1259.md) and a [qeṭeḇ](../../strongs/h/h6986.md) [śaʿar](../../strongs/h/h8178.md), as a [zerem](../../strongs/h/h2230.md) of [kabîr](../../strongs/h/h3524.md) [mayim](../../strongs/h/h4325.md) [šāṭap̄](../../strongs/h/h7857.md), shall [yānaḥ](../../strongs/h/h3240.md) to the ['erets](../../strongs/h/h776.md) with the [yad](../../strongs/h/h3027.md).

<a name="isaiah_28_3"></a>Isaiah 28:3

The [ʿăṭārâ](../../strongs/h/h5850.md) of [ge'uwth](../../strongs/h/h1348.md), the [šikôr](../../strongs/h/h7910.md) of ['Ep̄rayim](../../strongs/h/h669.md), shall be [rāmas](../../strongs/h/h7429.md) under [regel](../../strongs/h/h7272.md):

<a name="isaiah_28_4"></a>Isaiah 28:4

And the [ṣᵊḇî](../../strongs/h/h6643.md) [tip̄'ārâ](../../strongs/h/h8597.md), which is on the [ro'sh](../../strongs/h/h7218.md) of the [šemen](../../strongs/h/h8081.md) [gay'](../../strongs/h/h1516.md), shall be a [nabel](../../strongs/h/h5034.md) [ṣîṣâ](../../strongs/h/h6733.md), and as the [bikûr](../../strongs/h/h1061.md) before the [qayiṣ](../../strongs/h/h7019.md); which when he that [ra'ah](../../strongs/h/h7200.md) upon it [ra'ah](../../strongs/h/h7200.md), while it is yet in his [kaph](../../strongs/h/h3709.md) he [bālaʿ](../../strongs/h/h1104.md).

<a name="isaiah_28_5"></a>Isaiah 28:5

In that [yowm](../../strongs/h/h3117.md) shall [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) be for a [ʿăṭārâ](../../strongs/h/h5850.md) of [ṣᵊḇî](../../strongs/h/h6643.md), and for a [ṣᵊp̄îrâ](../../strongs/h/h6843.md) of [tip̄'ārâ](../../strongs/h/h8597.md), unto the [šᵊ'ār](../../strongs/h/h7605.md) of his ['am](../../strongs/h/h5971.md),

<a name="isaiah_28_6"></a>Isaiah 28:6

And for a [ruwach](../../strongs/h/h7307.md) of [mishpat](../../strongs/h/h4941.md) to him that [yashab](../../strongs/h/h3427.md) in [mishpat](../../strongs/h/h4941.md), and for [gᵊḇûrâ](../../strongs/h/h1369.md) to them that [shuwb](../../strongs/h/h7725.md) the [milḥāmâ](../../strongs/h/h4421.md) to the [sha'ar](../../strongs/h/h8179.md).

<a name="isaiah_28_7"></a>Isaiah 28:7

But they also have [šāḡâ](../../strongs/h/h7686.md) through [yayin](../../strongs/h/h3196.md), and through [šēḵār](../../strongs/h/h7941.md) are [tāʿâ](../../strongs/h/h8582.md); the [kōhēn](../../strongs/h/h3548.md) and the [nāḇî'](../../strongs/h/h5030.md) have [šāḡâ](../../strongs/h/h7686.md) through [šēḵār](../../strongs/h/h7941.md), they are [bālaʿ](../../strongs/h/h1104.md) of [yayin](../../strongs/h/h3196.md), they are [tāʿâ](../../strongs/h/h8582.md) through [šēḵār](../../strongs/h/h7941.md); they [šāḡâ](../../strongs/h/h7686.md) in [rō'ê](../../strongs/h/h7203.md), they [pûq](../../strongs/h/h6328.md) in [pᵊlîlîyâ](../../strongs/h/h6417.md).

<a name="isaiah_28_8"></a>Isaiah 28:8

For all [šulḥān](../../strongs/h/h7979.md) are [mālā'](../../strongs/h/h4390.md) of [qē'](../../strongs/h/h6892.md) and [ṣ'h](../../strongs/h/h6675.md), so that there is no [maqowm](../../strongs/h/h4725.md) clean.

<a name="isaiah_28_9"></a>Isaiah 28:9

Whom shall he [yārâ](../../strongs/h/h3384.md) [dēʿâ](../../strongs/h/h1844.md)? and whom shall he make to [bîn](../../strongs/h/h995.md) [šᵊmûʿâ](../../strongs/h/h8052.md)? them that are [gamal](../../strongs/h/h1580.md) from the [chalab](../../strongs/h/h2461.md), and [ʿatîq](../../strongs/h/h6267.md) from the [šaḏ](../../strongs/h/h7699.md).

<a name="isaiah_28_10"></a>Isaiah 28:10

For [ṣav](../../strongs/h/h6673.md) must be upon [ṣav](../../strongs/h/h6673.md), [ṣav](../../strongs/h/h6673.md) upon [ṣav](../../strongs/h/h6673.md); [qāv](../../strongs/h/h6957.md) upon [qāv](../../strongs/h/h6957.md), [qāv](../../strongs/h/h6957.md) upon [qāv](../../strongs/h/h6957.md); here a [zᵊʿêr](../../strongs/h/h2191.md), and there a [zᵊʿêr](../../strongs/h/h2191.md):

<a name="isaiah_28_11"></a>Isaiah 28:11

For with [lāʿēḡ](../../strongs/h/h3934.md) [saphah](../../strongs/h/h8193.md) and another [lashown](../../strongs/h/h3956.md) will he [dabar](../../strongs/h/h1696.md) to this ['am](../../strongs/h/h5971.md).

<a name="isaiah_28_12"></a>Isaiah 28:12

To whom he ['āmar](../../strongs/h/h559.md), This is the [mᵊnûḥâ](../../strongs/h/h4496.md) wherewith ye may cause the [ʿāyēp̄](../../strongs/h/h5889.md) to [nuwach](../../strongs/h/h5117.md); and this is the [margēʿâ](../../strongs/h/h4774.md): yet they would not [shama'](../../strongs/h/h8085.md).

<a name="isaiah_28_13"></a>Isaiah 28:13

But the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) was unto them [ṣav](../../strongs/h/h6673.md) upon [ṣav](../../strongs/h/h6673.md), [ṣav](../../strongs/h/h6673.md) upon [ṣav](../../strongs/h/h6673.md); [qāv](../../strongs/h/h6957.md) upon [qāv](../../strongs/h/h6957.md), [qāv](../../strongs/h/h6957.md) upon [qāv](../../strongs/h/h6957.md); here a [zᵊʿêr](../../strongs/h/h2191.md), and there a [zᵊʿêr](../../strongs/h/h2191.md); that they might go, and [kashal](../../strongs/h/h3782.md) ['āḥôr](../../strongs/h/h268.md), and be [shabar](../../strongs/h/h7665.md), and [yāqōš](../../strongs/h/h3369.md), and [lāḵaḏ](../../strongs/h/h3920.md).

<a name="isaiah_28_14"></a>Isaiah 28:14

Wherefore [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye [lāṣôn](../../strongs/h/h3944.md) ['enowsh](../../strongs/h/h582.md), that [mashal](../../strongs/h/h4910.md) this ['am](../../strongs/h/h5971.md) which is in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_28_15"></a>Isaiah 28:15

Because ye have ['āmar](../../strongs/h/h559.md), We have [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with [maveth](../../strongs/h/h4194.md), and with [shĕ'owl](../../strongs/h/h7585.md) are we at [ḥōzê](../../strongs/h/h2374.md); when the [šāṭap̄](../../strongs/h/h7857.md) [šôṭ](../../strongs/h/h7752.md) [šayiṭ](../../strongs/h/h7885.md) shall pass through, it shall not [bow'](../../strongs/h/h935.md) unto us: for we have [śûm](../../strongs/h/h7760.md) [kazab](../../strongs/h/h3577.md) our [machaceh](../../strongs/h/h4268.md), and under [sheqer](../../strongs/h/h8267.md) have we [cathar](../../strongs/h/h5641.md) ourselves:

<a name="isaiah_28_16"></a>Isaiah 28:16

Therefore thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), Behold, I [yacad](../../strongs/h/h3245.md) in [Tsiyown](../../strongs/h/h6726.md) for an ['eben](../../strongs/h/h68.md), a [bōḥan](../../strongs/h/h976.md) ['eben](../../strongs/h/h68.md), a [yāqār](../../strongs/h/h3368.md) [pinnâ](../../strongs/h/h6438.md), a [yacad](../../strongs/h/h3245.md) [mûsāḏ](../../strongs/h/h4143.md): he that ['aman](../../strongs/h/h539.md) shall not make [ḥûš](../../strongs/h/h2363.md).

<a name="isaiah_28_17"></a>Isaiah 28:17

[mishpat](../../strongs/h/h4941.md) also will I [śûm](../../strongs/h/h7760.md) to the [qāv](../../strongs/h/h6957.md), and [ṣĕdāqāh](../../strongs/h/h6666.md) to the [mišqeleṯ](../../strongs/h/h4949.md): and the [barad](../../strongs/h/h1259.md) shall [yāʿâ](../../strongs/h/h3261.md) the [machaceh](../../strongs/h/h4268.md) of [kazab](../../strongs/h/h3577.md), and the [mayim](../../strongs/h/h4325.md) shall [šāṭap̄](../../strongs/h/h7857.md) the [cether](../../strongs/h/h5643.md).

<a name="isaiah_28_18"></a>Isaiah 28:18

And your [bĕriyth](../../strongs/h/h1285.md) with [maveth](../../strongs/h/h4194.md) shall be [kāp̄ar](../../strongs/h/h3722.md), and your [ḥāzûṯ](../../strongs/h/h2380.md) with [shĕ'owl](../../strongs/h/h7585.md) shall not [quwm](../../strongs/h/h6965.md); when the [šāṭap̄](../../strongs/h/h7857.md) [šôṭ](../../strongs/h/h7752.md) shall ['abar](../../strongs/h/h5674.md), then ye shall be [mirmās](../../strongs/h/h4823.md) by it.

<a name="isaiah_28_19"></a>Isaiah 28:19

From the [day](../../strongs/h/h1767.md) that it ['abar](../../strongs/h/h5674.md) it shall [laqach](../../strongs/h/h3947.md) you: for [boqer](../../strongs/h/h1242.md) by [boqer](../../strongs/h/h1242.md) shall it ['abar](../../strongs/h/h5674.md), by [yowm](../../strongs/h/h3117.md) and by [layil](../../strongs/h/h3915.md): and it shall be a [zᵊvāʿâ](../../strongs/h/h2113.md) only to [bîn](../../strongs/h/h995.md) the [šᵊmûʿâ](../../strongs/h/h8052.md).

<a name="isaiah_28_20"></a>Isaiah 28:20

For the [maṣṣāʿ](../../strongs/h/h4702.md) is [qāṣar](../../strongs/h/h7114.md) than that a man can [śāraʿ](../../strongs/h/h8311.md) himself on it: and the [massēḵâ](../../strongs/h/h4541.md) [tsarar](../../strongs/h/h6887.md) than that he can [kānas](../../strongs/h/h3664.md) himself in it.

<a name="isaiah_28_21"></a>Isaiah 28:21

For [Yĕhovah](../../strongs/h/h3068.md) shall [quwm](../../strongs/h/h6965.md) as in [har](../../strongs/h/h2022.md) [pᵊrāṣîm](../../strongs/h/h6559.md), he shall be [ragaz](../../strongs/h/h7264.md) as in the [ʿēmeq](../../strongs/h/h6010.md) of [Giḇʿôn](../../strongs/h/h1391.md), that he may ['asah](../../strongs/h/h6213.md) his [ma'aseh](../../strongs/h/h4639.md), his [zûr](../../strongs/h/h2114.md) [ma'aseh](../../strongs/h/h4639.md); and ['abad](../../strongs/h/h5647.md) his [ʿăḇōḏâ](../../strongs/h/h5656.md), his [nāḵrî](../../strongs/h/h5237.md) [ʿăḇōḏâ](../../strongs/h/h5656.md).

<a name="isaiah_28_22"></a>Isaiah 28:22

Now therefore be ye not [luwts](../../strongs/h/h3887.md), lest your [mowcer](../../strongs/h/h4147.md) be made [ḥāzaq](../../strongs/h/h2388.md): for I have [shama'](../../strongs/h/h8085.md) from ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) a [kālâ](../../strongs/h/h3617.md), even [ḥāraṣ](../../strongs/h/h2782.md) upon the ['erets](../../strongs/h/h776.md).

<a name="isaiah_28_23"></a>Isaiah 28:23

['azan](../../strongs/h/h238.md), and [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md); [qashab](../../strongs/h/h7181.md), and [shama'](../../strongs/h/h8085.md) my ['imrah](../../strongs/h/h565.md).

<a name="isaiah_28_24"></a>Isaiah 28:24

Doth the [ḥāraš](../../strongs/h/h2790.md) [ḥāraš](../../strongs/h/h2790.md) all [yowm](../../strongs/h/h3117.md) to [zāraʿ](../../strongs/h/h2232.md)? doth he [pāṯaḥ](../../strongs/h/h6605.md) and [śāḏaḏ](../../strongs/h/h7702.md) of his ['ăḏāmâ](../../strongs/h/h127.md)?

<a name="isaiah_28_25"></a>Isaiah 28:25

When he hath [šāvâ](../../strongs/h/h7737.md) the [paniym](../../strongs/h/h6440.md) thereof, doth he not [puwts](../../strongs/h/h6327.md) the [qeṣaḥ](../../strongs/h/h7100.md), and [zāraq](../../strongs/h/h2236.md) the [kammōn](../../strongs/h/h3646.md), and [śûm](../../strongs/h/h7760.md) in the [śôrâ](../../strongs/h/h7795.md) [ḥiṭṭâ](../../strongs/h/h2406.md) and the [sāman](../../strongs/h/h5567.md) [śᵊʿōrâ](../../strongs/h/h8184.md) and the [kussemeṯ](../../strongs/h/h3698.md) in their [gᵊḇûlâ](../../strongs/h/h1367.md)?

<a name="isaiah_28_26"></a>Isaiah 28:26

For his ['Elohiym](../../strongs/h/h430.md) doth [yacar](../../strongs/h/h3256.md) him to [mishpat](../../strongs/h/h4941.md), and doth [yārâ](../../strongs/h/h3384.md) him.

<a name="isaiah_28_27"></a>Isaiah 28:27

For the [qeṣaḥ](../../strongs/h/h7100.md) are not [dûš](../../strongs/h/h1758.md) with a [ḥārûṣ](../../strongs/h/h2742.md), neither is a [ʿăḡālâ](../../strongs/h/h5699.md) ['ôp̄ān](../../strongs/h/h212.md) [cabab](../../strongs/h/h5437.md) upon the [kammōn](../../strongs/h/h3646.md); but the [qeṣaḥ](../../strongs/h/h7100.md) are [ḥāḇaṭ](../../strongs/h/h2251.md) with a [maṭṭê](../../strongs/h/h4294.md), and the [kammōn](../../strongs/h/h3646.md) with a [shebet](../../strongs/h/h7626.md).

<a name="isaiah_28_28"></a>Isaiah 28:28

[lechem](../../strongs/h/h3899.md) is [dāqaq](../../strongs/h/h1854.md); because he will not ever be ['āḏaš](../../strongs/h/h156.md) [dûš](../../strongs/h/h1758.md) it, nor [hāmam](../../strongs/h/h2000.md) it with the [Gilgāl](../../strongs/h/h1536.md) of his [ʿăḡālâ](../../strongs/h/h5699.md), nor [dāqaq](../../strongs/h/h1854.md) it with his [pārāš](../../strongs/h/h6571.md).

<a name="isaiah_28_29"></a>Isaiah 28:29

This also [yāṣā'](../../strongs/h/h3318.md) from [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), which is [pala'](../../strongs/h/h6381.md) in ['etsah](../../strongs/h/h6098.md), and [gāḏal](../../strongs/h/h1431.md) in [tûšîyâ](../../strongs/h/h8454.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 27](isaiah_27.md) - [Isaiah 29](isaiah_29.md)