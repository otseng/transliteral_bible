# [Isaiah 61](https://www.blueletterbible.org/kjv/isa/61/1/s_740001)

<a name="isaiah_61_1"></a>Isaiah 61:1

The [ruwach](../../strongs/h/h7307.md) of ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) is upon me; because [Yĕhovah](../../strongs/h/h3068.md) hath [māšaḥ](../../strongs/h/h4886.md) me to [bāśar](../../strongs/h/h1319.md) unto the ['anav](../../strongs/h/h6035.md); he hath [shalach](../../strongs/h/h7971.md) me to [ḥāḇaš](../../strongs/h/h2280.md) the [shabar](../../strongs/h/h7665.md) [leb](../../strongs/h/h3820.md), to [qara'](../../strongs/h/h7121.md) [dᵊrôr](../../strongs/h/h1865.md) to the [šāḇâ](../../strongs/h/h7617.md), and the [pᵊqaḥ-qôaḥ](../../strongs/h/h6495.md) to them that are ['āsar](../../strongs/h/h631.md);

<a name="isaiah_61_2"></a>Isaiah 61:2

To [qara'](../../strongs/h/h7121.md) the [ratsown](../../strongs/h/h7522.md) [šānâ](../../strongs/h/h8141.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [yowm](../../strongs/h/h3117.md) of [nāqām](../../strongs/h/h5359.md) of our ['Elohiym](../../strongs/h/h430.md); to [nacham](../../strongs/h/h5162.md) all that ['āḇēl](../../strongs/h/h57.md);

<a name="isaiah_61_3"></a>Isaiah 61:3

To [śûm](../../strongs/h/h7760.md) unto them that ['āḇēl](../../strongs/h/h57.md) in [Tsiyown](../../strongs/h/h6726.md), to give unto them [pᵊ'ēr](../../strongs/h/h6287.md) for ['ēp̄er](../../strongs/h/h665.md), the [šemen](../../strongs/h/h8081.md) of [śāśôn](../../strongs/h/h8342.md) for ['ēḇel](../../strongs/h/h60.md), the [maʿăṭê](../../strongs/h/h4594.md) of [tehillah](../../strongs/h/h8416.md) for the [ruwach](../../strongs/h/h7307.md) of [kēhê](../../strongs/h/h3544.md); that they might be called ['ayil](../../strongs/h/h352.md) of [tsedeq](../../strongs/h/h6664.md), the [maṭṭāʿ](../../strongs/h/h4302.md) of [Yĕhovah](../../strongs/h/h3068.md), that he might be [pā'ar](../../strongs/h/h6286.md).

<a name="isaiah_61_4"></a>Isaiah 61:4

And they shall [bānâ](../../strongs/h/h1129.md) the ['owlam](../../strongs/h/h5769.md) [chorbah](../../strongs/h/h2723.md), they shall [quwm](../../strongs/h/h6965.md) the [ri'šôn](../../strongs/h/h7223.md) [šāmēm](../../strongs/h/h8074.md), and they shall [ḥādaš](../../strongs/h/h2318.md) the [ḥōreḇ](../../strongs/h/h2721.md) [ʿîr](../../strongs/h/h5892.md), the [šāmēm](../../strongs/h/h8074.md) of many [dôr](../../strongs/h/h1755.md).

<a name="isaiah_61_5"></a>Isaiah 61:5

And [zûr](../../strongs/h/h2114.md) shall ['amad](../../strongs/h/h5975.md) and [ra'ah](../../strongs/h/h7462.md) your [tso'n](../../strongs/h/h6629.md), and the [ben](../../strongs/h/h1121.md) of the [nēḵār](../../strongs/h/h5236.md) shall be your ['ikār](../../strongs/h/h406.md) and your [kōrēm](../../strongs/h/h3755.md).

<a name="isaiah_61_6"></a>Isaiah 61:6

But ye shall be [qara'](../../strongs/h/h7121.md) the [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md): men shall ['āmar](../../strongs/h/h559.md) you the [sharath](../../strongs/h/h8334.md) of our ['Elohiym](../../strongs/h/h430.md): ye shall ['akal](../../strongs/h/h398.md) the [ḥayil](../../strongs/h/h2428.md) of the [gowy](../../strongs/h/h1471.md), and in their [kabowd](../../strongs/h/h3519.md) shall ye [yāmar](../../strongs/h/h3235.md) yourselves.

<a name="isaiah_61_7"></a>Isaiah 61:7

For your [bšeṯ](../../strongs/h/h1322.md) ye shall have [mišnê](../../strongs/h/h4932.md); and for [kĕlimmah](../../strongs/h/h3639.md) they shall [ranan](../../strongs/h/h7442.md) in their [cheleq](../../strongs/h/h2506.md): therefore in their ['erets](../../strongs/h/h776.md) they shall [yarash](../../strongs/h/h3423.md) the [mišnê](../../strongs/h/h4932.md): ['owlam](../../strongs/h/h5769.md) [simchah](../../strongs/h/h8057.md) shall be unto them.

<a name="isaiah_61_8"></a>Isaiah 61:8

For I [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) [mishpat](../../strongs/h/h4941.md), I [sane'](../../strongs/h/h8130.md) [gāzēl](../../strongs/h/h1498.md) for [ʿōlâ](../../strongs/h/h5930.md); and I will [nathan](../../strongs/h/h5414.md) their [pe'ullah](../../strongs/h/h6468.md) in ['emeth](../../strongs/h/h571.md), and I will make an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) with them.

<a name="isaiah_61_9"></a>Isaiah 61:9

And their [zera'](../../strongs/h/h2233.md) shall be [yada'](../../strongs/h/h3045.md) among the [gowy](../../strongs/h/h1471.md), and their [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) among the ['am](../../strongs/h/h5971.md): all that [ra'ah](../../strongs/h/h7200.md) them shall [nāḵar](../../strongs/h/h5234.md) them, that they are the [zera'](../../strongs/h/h2233.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md).

<a name="isaiah_61_10"></a>Isaiah 61:10

I will [śûś](../../strongs/h/h7797.md) [śûś](../../strongs/h/h7797.md) in [Yĕhovah](../../strongs/h/h3068.md), my [nephesh](../../strongs/h/h5315.md) shall [giyl](../../strongs/h/h1523.md) in my ['Elohiym](../../strongs/h/h430.md); for he hath [labash](../../strongs/h/h3847.md) me with the [beḡeḏ](../../strongs/h/h899.md) of [yesha'](../../strongs/h/h3468.md), he hath [yāʿaṭ](../../strongs/h/h3271.md) me with the [mᵊʿîl](../../strongs/h/h4598.md) of [ṣĕdāqāh](../../strongs/h/h6666.md), as a [ḥāṯān](../../strongs/h/h2860.md) [kāhan](../../strongs/h/h3547.md) himself with [pᵊ'ēr](../../strongs/h/h6287.md), and as a [kallâ](../../strongs/h/h3618.md) [ʿāḏâ](../../strongs/h/h5710.md) herself with her [kĕliy](../../strongs/h/h3627.md).

<a name="isaiah_61_11"></a>Isaiah 61:11

For as the ['erets](../../strongs/h/h776.md) [yāṣā'](../../strongs/h/h3318.md) her [ṣemaḥ](../../strongs/h/h6780.md), and as the [gannâ](../../strongs/h/h1593.md) causeth the things that are [zērûaʿ](../../strongs/h/h2221.md) in it to [ṣāmaḥ](../../strongs/h/h6779.md); so ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will cause [ṣĕdāqāh](../../strongs/h/h6666.md) and [tehillah](../../strongs/h/h8416.md) to [ṣāmaḥ](../../strongs/h/h6779.md) before all the [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 60](isaiah_60.md) - [Isaiah 62](isaiah_62.md)