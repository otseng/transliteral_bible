# [Isaiah 18](https://www.blueletterbible.org/kjv/isa/18/1/s_697001)

<a name="isaiah_18_1"></a>Isaiah 18:1

[hôy](../../strongs/h/h1945.md) to the ['erets](../../strongs/h/h776.md) shadowing with [kanaph](../../strongs/h/h3671.md), which is beyond the [nāhār](../../strongs/h/h5104.md) of [Kûš](../../strongs/h/h3568.md):

<a name="isaiah_18_2"></a>Isaiah 18:2

That [shalach](../../strongs/h/h7971.md) [ṣîr](../../strongs/h/h6735.md) by the [yam](../../strongs/h/h3220.md), even in [kĕliy](../../strongs/h/h3627.md) of [gōme'](../../strongs/h/h1573.md) upon the [mayim](../../strongs/h/h4325.md), saying, [yālaḵ](../../strongs/h/h3212.md), ye [qal](../../strongs/h/h7031.md) [mal'ak](../../strongs/h/h4397.md), to a [gowy](../../strongs/h/h1471.md) [mashak](../../strongs/h/h4900.md) and [môrāṭ](../../strongs/h/h4178.md), to a people [yare'](../../strongs/h/h3372.md) from their [hāl'â](../../strongs/h/h1973.md); a [gowy](../../strongs/h/h1471.md) [qv qv](../../strongs/h/h6978.md) and [mᵊḇûsâ](../../strongs/h/h4001.md), whose ['erets](../../strongs/h/h776.md) the [nāhār](../../strongs/h/h5104.md) have [bāzā'](../../strongs/h/h958.md)!

<a name="isaiah_18_3"></a>Isaiah 18:3

All ye [yashab](../../strongs/h/h3427.md) of the [tebel](../../strongs/h/h8398.md), and [shakan](../../strongs/h/h7931.md) on the ['erets](../../strongs/h/h776.md), [ra'ah](../../strongs/h/h7200.md) ye, when he [nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md) on the [har](../../strongs/h/h2022.md); and when he [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md), [shama'](../../strongs/h/h8085.md) ye.

<a name="isaiah_18_4"></a>Isaiah 18:4

For so [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, I will take my [šāqaṭ](../../strongs/h/h8252.md), and I will [nabat](../../strongs/h/h5027.md) in my [māḵôn](../../strongs/h/h4349.md) like a [ṣaḥ](../../strongs/h/h6703.md) [ḥōm](../../strongs/h/h2527.md) upon ['owr](../../strongs/h/h216.md), and like an ['ab](../../strongs/h/h5645.md) of [ṭal](../../strongs/h/h2919.md) in the [ḥōm](../../strongs/h/h2527.md) of [qāṣîr](../../strongs/h/h7105.md).

<a name="isaiah_18_5"></a>Isaiah 18:5

For afore the [qāṣîr](../../strongs/h/h7105.md), when the [peraḥ](../../strongs/h/h6525.md) is [tamam](../../strongs/h/h8552.md), and the [bōser](../../strongs/h/h1155.md) is [gamal](../../strongs/h/h1580.md) in the [niṣṣâ](../../strongs/h/h5328.md), he shall both [karath](../../strongs/h/h3772.md) the [zalzal](../../strongs/h/h2150.md) with [mazmērâ](../../strongs/h/h4211.md), and take away and [tāzaz](../../strongs/h/h8456.md) the [nᵊṭîšâ](../../strongs/h/h5189.md).

<a name="isaiah_18_6"></a>Isaiah 18:6

They shall be ['azab](../../strongs/h/h5800.md) [yaḥaḏ](../../strongs/h/h3162.md) unto the [ʿayiṭ](../../strongs/h/h5861.md) of the [har](../../strongs/h/h2022.md), and to the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md): and the [ʿayiṭ](../../strongs/h/h5861.md) shall [qûṣ](../../strongs/h/h6972.md) upon them, and all the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md) shall [ḥārap̄](../../strongs/h/h2778.md) upon them.

<a name="isaiah_18_7"></a>Isaiah 18:7

In that [ʿēṯ](../../strongs/h/h6256.md) shall the [šay](../../strongs/h/h7862.md) be [yāḇal](../../strongs/h/h2986.md) unto [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) of an ['am](../../strongs/h/h5971.md) [mashak](../../strongs/h/h4900.md) and [môrāṭ](../../strongs/h/h4178.md), and from an ['am](../../strongs/h/h5971.md) [yare'](../../strongs/h/h3372.md) from their [hāl'â](../../strongs/h/h1973.md); a [gowy](../../strongs/h/h1471.md) [qv qv](../../strongs/h/h6978.md) and [mᵊḇûsâ](../../strongs/h/h4001.md), whose ['erets](../../strongs/h/h776.md) the [nāhār](../../strongs/h/h5104.md) have [bāzā'](../../strongs/h/h958.md), to the [maqowm](../../strongs/h/h4725.md) of the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 17](isaiah_17.md) - [Isaiah 19](isaiah_19.md)