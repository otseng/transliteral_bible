# [Isaiah 12](https://www.blueletterbible.org/kjv/isa/12/1/s_691001)

<a name="isaiah_12_1"></a>Isaiah 12:1

And in that [yowm](../../strongs/h/h3117.md) thou shalt say, [Yĕhovah](../../strongs/h/h3068.md), I will [yadah](../../strongs/h/h3034.md) thee: though thou wast ['anaph](../../strongs/h/h599.md) with me, thine ['aph](../../strongs/h/h639.md) is [shuwb](../../strongs/h/h7725.md), and thou [nacham](../../strongs/h/h5162.md) me.

<a name="isaiah_12_2"></a>Isaiah 12:2

Behold, ['el](../../strongs/h/h410.md) is my [yĕshuw'ah](../../strongs/h/h3444.md); I will [batach](../../strongs/h/h982.md), and not be [pachad](../../strongs/h/h6342.md): for [Yahh](../../strongs/h/h3050.md) [Yĕhovah](../../strongs/h/h3068.md) is my ['oz](../../strongs/h/h5797.md) and my [zimrāṯ](../../strongs/h/h2176.md); he also is become my [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="isaiah_12_3"></a>Isaiah 12:3

Therefore with [śāśôn](../../strongs/h/h8342.md) shall ye [šā'aḇ](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md) out of the [maʿyān](../../strongs/h/h4599.md) of [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="isaiah_12_4"></a>Isaiah 12:4

And in that [yowm](../../strongs/h/h3117.md) shall ye ['āmar](../../strongs/h/h559.md), [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md), [qara'](../../strongs/h/h7121.md) upon his [shem](../../strongs/h/h8034.md), [yada'](../../strongs/h/h3045.md) his ['aliylah](../../strongs/h/h5949.md) among the ['am](../../strongs/h/h5971.md), [zakar](../../strongs/h/h2142.md) that his [shem](../../strongs/h/h8034.md) is [śāḡaḇ](../../strongs/h/h7682.md).

<a name="isaiah_12_5"></a>Isaiah 12:5

[zamar](../../strongs/h/h2167.md) unto [Yĕhovah](../../strongs/h/h3068.md); for he hath ['asah](../../strongs/h/h6213.md) [ge'uwth](../../strongs/h/h1348.md): this is [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) in all the ['erets](../../strongs/h/h776.md).

<a name="isaiah_12_6"></a>Isaiah 12:6

[ṣāhal](../../strongs/h/h6670.md) and [ranan](../../strongs/h/h7442.md), thou [yashab](../../strongs/h/h3427.md) of [Tsiyown](../../strongs/h/h6726.md): for [gadowl](../../strongs/h/h1419.md) is the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) in the [qereḇ](../../strongs/h/h7130.md) of thee.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 11](isaiah_11.md) - [Isaiah 13](isaiah_13.md)