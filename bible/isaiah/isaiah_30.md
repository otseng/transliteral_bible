# [Isaiah 30](https://www.blueletterbible.org/kjv/isa/30/1/s_709001)

<a name="isaiah_30_1"></a>Isaiah 30:1

[hôy](../../strongs/h/h1945.md) to the [sārar](../../strongs/h/h5637.md) [ben](../../strongs/h/h1121.md), saith [Yĕhovah](../../strongs/h/h3068.md), that take ['etsah](../../strongs/h/h6098.md), but not of me; and that [nacak](../../strongs/h/h5258.md) with a [massēḵâ](../../strongs/h/h4541.md), but not of my [ruwach](../../strongs/h/h7307.md), that they may add [chatta'ath](../../strongs/h/h2403.md) to [chatta'ath](../../strongs/h/h2403.md):

<a name="isaiah_30_2"></a>Isaiah 30:2

That [halak](../../strongs/h/h1980.md) to [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md), and have not asked at my [peh](../../strongs/h/h6310.md); to ['azaz](../../strongs/h/h5810.md) themselves in the [māʿôz](../../strongs/h/h4581.md) of [Parʿô](../../strongs/h/h6547.md), and to [chacah](../../strongs/h/h2620.md) in the [ṣēl](../../strongs/h/h6738.md) of [Mitsrayim](../../strongs/h/h4714.md)!

<a name="isaiah_30_3"></a>Isaiah 30:3

Therefore shall the [māʿôz](../../strongs/h/h4581.md) of [Parʿô](../../strongs/h/h6547.md) be your [bšeṯ](../../strongs/h/h1322.md), and the [ḥāsûṯ](../../strongs/h/h2622.md) in the [ṣēl](../../strongs/h/h6738.md) of [Mitsrayim](../../strongs/h/h4714.md) your [kĕlimmah](../../strongs/h/h3639.md).

<a name="isaiah_30_4"></a>Isaiah 30:4

For his [śar](../../strongs/h/h8269.md) were at [Ṣōʿan](../../strongs/h/h6814.md), and his [mal'ak](../../strongs/h/h4397.md) came to [ḥānēs](../../strongs/h/h2609.md).

<a name="isaiah_30_5"></a>Isaiah 30:5

They were all [yāḇēš](../../strongs/h/h3001.md) of an ['am](../../strongs/h/h5971.md) that could not [yāʿal](../../strongs/h/h3276.md) them, nor be an ['ezer](../../strongs/h/h5828.md) nor [yāʿal](../../strongs/h/h3276.md), but a [bšeṯ](../../strongs/h/h1322.md), and also a [cherpah](../../strongs/h/h2781.md).

<a name="isaiah_30_6"></a>Isaiah 30:6

The [maśśā'](../../strongs/h/h4853.md) of the [bĕhemah](../../strongs/h/h929.md) of the [neḡeḇ](../../strongs/h/h5045.md): into the ['erets](../../strongs/h/h776.md) of [tsarah](../../strongs/h/h6869.md) and [ṣôq](../../strongs/h/h6695.md), from whence come the [lāḇî'](../../strongs/h/h3833.md) and [layiš](../../strongs/h/h3918.md), the ['ep̄ʿê](../../strongs/h/h660.md) and [saraph](../../strongs/h/h8314.md) ['uwph](../../strongs/h/h5774.md), they will [nasa'](../../strongs/h/h5375.md) their [ḥayil](../../strongs/h/h2428.md) upon the [kāṯēp̄](../../strongs/h/h3802.md) of [ʿayir](../../strongs/h/h5895.md), and their ['ôṣār](../../strongs/h/h214.md) upon the [dabešeṯ](../../strongs/h/h1707.md) of [gāmāl](../../strongs/h/h1581.md), to an ['am](../../strongs/h/h5971.md) that shall not [yāʿal](../../strongs/h/h3276.md) them.

<a name="isaiah_30_7"></a>Isaiah 30:7

For the [Mitsrayim](../../strongs/h/h4714.md) shall [ʿāzar](../../strongs/h/h5826.md) in [heḇel](../../strongs/h/h1892.md), and to no [riyq](../../strongs/h/h7385.md): therefore have I [qara'](../../strongs/h/h7121.md) concerning this, Their [rahaḇ](../../strongs/h/h7293.md) is to [šeḇeṯ](../../strongs/h/h7674.md).

<a name="isaiah_30_8"></a>Isaiah 30:8

Now [bow'](../../strongs/h/h935.md), [kāṯaḇ](../../strongs/h/h3789.md) it before them in a [lûaḥ](../../strongs/h/h3871.md), and [ḥāqaq](../../strongs/h/h2710.md) it in a [sēp̄er](../../strongs/h/h5612.md), that it may be for the [yowm](../../strongs/h/h3117.md) to ['aḥărôn](../../strongs/h/h314.md) for [ʿaḏ](../../strongs/h/h5703.md) and ['owlam](../../strongs/h/h5769.md):

<a name="isaiah_30_9"></a>Isaiah 30:9

That this is a [mᵊrî](../../strongs/h/h4805.md) ['am](../../strongs/h/h5971.md), [keḥāš](../../strongs/h/h3586.md) [ben](../../strongs/h/h1121.md), [ben](../../strongs/h/h1121.md) that will not [shama'](../../strongs/h/h8085.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="isaiah_30_10"></a>Isaiah 30:10

Which say to the [ra'ah](../../strongs/h/h7200.md), [ra'ah](../../strongs/h/h7200.md) not; and to the [ḥōzê](../../strongs/h/h2374.md), [chazah](../../strongs/h/h2372.md) not unto us [nᵊḵōḥâ](../../strongs/h/h5229.md), [dabar](../../strongs/h/h1696.md) unto us [ḥelqâ](../../strongs/h/h2513.md), [chazah](../../strongs/h/h2372.md) [mahăṯalâ](../../strongs/h/h4123.md):

<a name="isaiah_30_11"></a>Isaiah 30:11

[cuwr](../../strongs/h/h5493.md) you of the [derek](../../strongs/h/h1870.md), [natah](../../strongs/h/h5186.md) out of the ['orach](../../strongs/h/h734.md), cause the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) to [shabath](../../strongs/h/h7673.md) from before us.

<a name="isaiah_30_12"></a>Isaiah 30:12

Wherefore thus ['āmar](../../strongs/h/h559.md) the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), Because ye [mā'as](../../strongs/h/h3988.md) this [dabar](../../strongs/h/h1697.md), and [batach](../../strongs/h/h982.md) in [ʿōšeq](../../strongs/h/h6233.md) and [Lûz](../../strongs/h/h3868.md), and [šāʿan](../../strongs/h/h8172.md) thereon:

<a name="isaiah_30_13"></a>Isaiah 30:13

Therefore this ['avon](../../strongs/h/h5771.md) shall be to you as a [pereṣ](../../strongs/h/h6556.md) ready to [naphal](../../strongs/h/h5307.md), [bāʿâ](../../strongs/h/h1158.md) in a [śāḡaḇ](../../strongs/h/h7682.md) [ḥômâ](../../strongs/h/h2346.md), whose [šeḇar](../../strongs/h/h7667.md) cometh suddenly at a [peṯaʿ](../../strongs/h/h6621.md).

<a name="isaiah_30_14"></a>Isaiah 30:14

And he shall [shabar](../../strongs/h/h7665.md) it as the [šeḇar](../../strongs/h/h7667.md) of the [yāṣar](../../strongs/h/h3335.md) [neḇel](../../strongs/h/h5035.md) that is [kāṯaṯ](../../strongs/h/h3807.md); he shall not [ḥāmal](../../strongs/h/h2550.md): so that there shall not be [māṣā'](../../strongs/h/h4672.md) in the [mᵊḵitâ](../../strongs/h/h4386.md) of it a [ḥereś](../../strongs/h/h2789.md) to [ḥāṯâ](../../strongs/h/h2846.md) ['esh](../../strongs/h/h784.md) from the [yāqaḏ](../../strongs/h/h3344.md), or to [ḥāśap̄](../../strongs/h/h2834.md) [mayim](../../strongs/h/h4325.md) withal out of the [geḇe'](../../strongs/h/h1360.md).

<a name="isaiah_30_15"></a>Isaiah 30:15

For thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md); In [šûḇâ](../../strongs/h/h7729.md) and [naḥaṯ](../../strongs/h/h5183.md) shall ye be [yasha'](../../strongs/h/h3467.md); in [šāqaṭ](../../strongs/h/h8252.md) and in [biṭḥâ](../../strongs/h/h985.md) shall be your [gᵊḇûrâ](../../strongs/h/h1369.md): and ye would not.

<a name="isaiah_30_16"></a>Isaiah 30:16

But ye ['āmar](../../strongs/h/h559.md), No; for we will [nûs](../../strongs/h/h5127.md) upon [sûs](../../strongs/h/h5483.md); therefore shall ye [nûs](../../strongs/h/h5127.md): and, We will [rāḵaḇ](../../strongs/h/h7392.md) upon the [qal](../../strongs/h/h7031.md); therefore shall they that [radaph](../../strongs/h/h7291.md) you be [qālal](../../strongs/h/h7043.md).

<a name="isaiah_30_17"></a>Isaiah 30:17

One thousand shall flee at the [ge'arah](../../strongs/h/h1606.md) of one; at the [ge'arah](../../strongs/h/h1606.md) of five shall ye [nûs](../../strongs/h/h5127.md): till ye [yāṯar](../../strongs/h/h3498.md) as a [tōren](../../strongs/h/h8650.md) upon the [ro'sh](../../strongs/h/h7218.md) of a [har](../../strongs/h/h2022.md), and as a [nēs](../../strongs/h/h5251.md) on a [giḇʿâ](../../strongs/h/h1389.md).

<a name="isaiah_30_18"></a>Isaiah 30:18

And therefore will [Yĕhovah](../../strongs/h/h3068.md) [ḥāḵâ](../../strongs/h/h2442.md), that he may be [chanan](../../strongs/h/h2603.md) unto you, and therefore will he be [ruwm](../../strongs/h/h7311.md), that he may have [racham](../../strongs/h/h7355.md) upon you: for [Yĕhovah](../../strongs/h/h3068.md) is an ['Elohiym](../../strongs/h/h430.md) of [mishpat](../../strongs/h/h4941.md): ['esher](../../strongs/h/h835.md) are all they that [ḥāḵâ](../../strongs/h/h2442.md) for him.

<a name="isaiah_30_19"></a>Isaiah 30:19

For the ['am](../../strongs/h/h5971.md) shall [yashab](../../strongs/h/h3427.md) in [Tsiyown](../../strongs/h/h6726.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): thou shalt [bāḵâ](../../strongs/h/h1058.md) no more: he will be [chanan](../../strongs/h/h2603.md) [chanan](../../strongs/h/h2603.md) unto thee at the [qowl](../../strongs/h/h6963.md) of thy [zāʿaq](../../strongs/h/h2199.md); when he shall [shama'](../../strongs/h/h8085.md) it, he will ['anah](../../strongs/h/h6030.md) thee.

<a name="isaiah_30_20"></a>Isaiah 30:20

And though ['adonay](../../strongs/h/h136.md) [nathan](../../strongs/h/h5414.md) you the [lechem](../../strongs/h/h3899.md) of [tsar](../../strongs/h/h6862.md), and the [mayim](../../strongs/h/h4325.md) of [laḥaṣ](../../strongs/h/h3906.md), yet shall not thy [yārâ](../../strongs/h/h3384.md) be [kānap̄](../../strongs/h/h3670.md) any more, but thine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) thy [yārâ](../../strongs/h/h3384.md):

<a name="isaiah_30_21"></a>Isaiah 30:21

And thine ['ozen](../../strongs/h/h241.md) shall [shama'](../../strongs/h/h8085.md) a [dabar](../../strongs/h/h1697.md) ['aḥar](../../strongs/h/h310.md) thee, ['āmar](../../strongs/h/h559.md), This is the [derek](../../strongs/h/h1870.md), [yālaḵ](../../strongs/h/h3212.md) ye in it, when ye ['āman](../../strongs/h/h541.md), and when ye [śam'al](../../strongs/h/h8041.md).

<a name="isaiah_30_22"></a>Isaiah 30:22

Ye shall [ṭāmē'](../../strongs/h/h2930.md) also the [ṣipûy](../../strongs/h/h6826.md) of thy [pāsîl](../../strongs/h/h6456.md) of [keceph](../../strongs/h/h3701.md), and the ['ăp̄udâ](../../strongs/h/h642.md) of thy [massēḵâ](../../strongs/h/h4541.md) of [zāhāḇ](../../strongs/h/h2091.md): thou shalt [zārâ](../../strongs/h/h2219.md) as a [dāvê](../../strongs/h/h1739.md); thou shalt ['āmar](../../strongs/h/h559.md) unto it, [yāṣā'](../../strongs/h/h3318.md).

<a name="isaiah_30_23"></a>Isaiah 30:23

Then shall he [nathan](../../strongs/h/h5414.md) the [māṭār](../../strongs/h/h4306.md) of thy [zera'](../../strongs/h/h2233.md), that thou shalt [zāraʿ](../../strongs/h/h2232.md) the ['adamah](../../strongs/h/h127.md) withal; and [lechem](../../strongs/h/h3899.md) of the [tᵊḇû'â](../../strongs/h/h8393.md) of the ['ăḏāmâ](../../strongs/h/h127.md), and it shall be [dāšēn](../../strongs/h/h1879.md) and [šāmēn](../../strongs/h/h8082.md): in that [yowm](../../strongs/h/h3117.md) shall thy [miqnê](../../strongs/h/h4735.md) [ra'ah](../../strongs/h/h7462.md) in [rāḥaḇ](../../strongs/h/h7337.md) [kar](../../strongs/h/h3733.md).

<a name="isaiah_30_24"></a>Isaiah 30:24

The ['eleph](../../strongs/h/h504.md) likewise and the [ʿayir](../../strongs/h/h5895.md) that ['abad](../../strongs/h/h5647.md) the ['adamah](../../strongs/h/h127.md) shall ['akal](../../strongs/h/h398.md) [ḥāmîṣ](../../strongs/h/h2548.md) [bᵊlîl](../../strongs/h/h1098.md), which hath been [zārâ](../../strongs/h/h2219.md) with the [raḥaṯ](../../strongs/h/h7371.md) and with the [mizrê](../../strongs/h/h4214.md).

<a name="isaiah_30_25"></a>Isaiah 30:25

And there shall be upon every [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md), and upon every [nasa'](../../strongs/h/h5375.md) [giḇʿâ](../../strongs/h/h1389.md), [peleḡ](../../strongs/h/h6388.md) and [Yāḇāl](../../strongs/h/h2988.md) of [mayim](../../strongs/h/h4325.md) in the [yowm](../../strongs/h/h3117.md) of the [rab](../../strongs/h/h7227.md) [hereḡ](../../strongs/h/h2027.md), when the [miḡdāl](../../strongs/h/h4026.md) [naphal](../../strongs/h/h5307.md).

<a name="isaiah_30_26"></a>Isaiah 30:26

Moreover the ['owr](../../strongs/h/h216.md) of the [lᵊḇānâ](../../strongs/h/h3842.md) shall be as the ['owr](../../strongs/h/h216.md) of the [ḥammâ](../../strongs/h/h2535.md), and the ['owr](../../strongs/h/h216.md) of the [ḥammâ](../../strongs/h/h2535.md) shall be [šiḇʿāṯayim](../../strongs/h/h7659.md), as the ['owr](../../strongs/h/h216.md) of [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md), in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [ḥāḇaš](../../strongs/h/h2280.md) the [šeḇar](../../strongs/h/h7667.md) of his ['am](../../strongs/h/h5971.md), and [rapha'](../../strongs/h/h7495.md) the [maḥaṣ](../../strongs/h/h4273.md) of their [makâ](../../strongs/h/h4347.md).

<a name="isaiah_30_27"></a>Isaiah 30:27

Behold, the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) from [merḥāq](../../strongs/h/h4801.md), [bāʿar](../../strongs/h/h1197.md) with his ['aph](../../strongs/h/h639.md), and the [maśśā'â](../../strongs/h/h4858.md) thereof is [kōḇeḏ](../../strongs/h/h3514.md): his [saphah](../../strongs/h/h8193.md) are full of [zaʿam](../../strongs/h/h2195.md), and his [lashown](../../strongs/h/h3956.md) as an ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md):

<a name="isaiah_30_28"></a>Isaiah 30:28

And his [ruwach](../../strongs/h/h7307.md), as a [šāṭap̄](../../strongs/h/h7857.md) [nachal](../../strongs/h/h5158.md), shall reach to the midst of the [ṣaûā'r](../../strongs/h/h6677.md), to [nûp̄](../../strongs/h/h5130.md) the [gowy](../../strongs/h/h1471.md) with the [nāp̄â](../../strongs/h/h5299.md) of [shav'](../../strongs/h/h7723.md): and there shall be a [resen](../../strongs/h/h7448.md) in the jaws of the ['am](../../strongs/h/h5971.md), causing them to [tāʿâ](../../strongs/h/h8582.md).

<a name="isaiah_30_29"></a>Isaiah 30:29

Ye shall have a [šîr](../../strongs/h/h7892.md), as in the [layil](../../strongs/h/h3915.md) when a [qadash](../../strongs/h/h6942.md) [ḥāḡ](../../strongs/h/h2282.md) is [qadash](../../strongs/h/h6942.md); and [simchah](../../strongs/h/h8057.md) of [lebab](../../strongs/h/h3824.md), as when one goeth with a [ḥālîl](../../strongs/h/h2485.md) to come into the [har](../../strongs/h/h2022.md) of [Yĕhovah](../../strongs/h/h3068.md), to the [tsuwr](../../strongs/h/h6697.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_30_30"></a>Isaiah 30:30

And [Yĕhovah](../../strongs/h/h3068.md) shall cause his [howd](../../strongs/h/h1935.md) [qowl](../../strongs/h/h6963.md) to be [shama'](../../strongs/h/h8085.md), and shall shew the [naḥaṯ](../../strongs/h/h5183.md) of his [zerowa'](../../strongs/h/h2220.md), with the [zaʿap̄](../../strongs/h/h2197.md) of his ['aph](../../strongs/h/h639.md), and with the [lahaḇ](../../strongs/h/h3851.md) of an ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md), with [nep̄eṣ](../../strongs/h/h5311.md), and [zerem](../../strongs/h/h2230.md), and ['eben](../../strongs/h/h68.md) [barad](../../strongs/h/h1259.md).

<a name="isaiah_30_31"></a>Isaiah 30:31

For through the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) shall the ['Aššûr](../../strongs/h/h804.md) be [ḥāṯaṯ](../../strongs/h/h2865.md), which [nakah](../../strongs/h/h5221.md) with a [shebet](../../strongs/h/h7626.md).

<a name="isaiah_30_32"></a>Isaiah 30:32

And in [kōl](../../strongs/h/h3605.md) where the [mûsāḏâ](../../strongs/h/h4145.md) staff shall [maʿăḇār](../../strongs/h/h4569.md), which [Yĕhovah](../../strongs/h/h3068.md) shall [nuwach](../../strongs/h/h5117.md) upon him, it shall be with [tōp̄](../../strongs/h/h8596.md) and [kinnôr](../../strongs/h/h3658.md): and in [milḥāmâ](../../strongs/h/h4421.md) of [tᵊnûp̄â](../../strongs/h/h8573.md) will he [lāḥam](../../strongs/h/h3898.md) with it.

<a name="isaiah_30_33"></a>Isaiah 30:33

For [tāp̄tê](../../strongs/h/h8613.md) is ['arak](../../strongs/h/h6186.md) of ['eṯmôl](../../strongs/h/h865.md); yea, for the [melek](../../strongs/h/h4428.md) it is [kuwn](../../strongs/h/h3559.md); he hath made it [ʿāmaq](../../strongs/h/h6009.md) and [rāḥaḇ](../../strongs/h/h7337.md): the [mᵊḏûrâ](../../strongs/h/h4071.md) thereof is ['esh](../../strongs/h/h784.md) and much ['ets](../../strongs/h/h6086.md); the [neshamah](../../strongs/h/h5397.md) of [Yĕhovah](../../strongs/h/h3068.md), like a [nachal](../../strongs/h/h5158.md) of [gophriyth](../../strongs/h/h1614.md), doth [bāʿar](../../strongs/h/h1197.md) it.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 29](isaiah_29.md) - [Isaiah 31](isaiah_31.md)