# [Isaiah 4](https://www.blueletterbible.org/kjv/isa/4/1/s_683001)

<a name="isaiah_4_1"></a>Isaiah 4:1

And in that [yowm](../../strongs/h/h3117.md) seven ['ishshah](../../strongs/h/h802.md) shall [ḥāzaq](../../strongs/h/h2388.md) of one ['iysh](../../strongs/h/h376.md), saying, We will ['akal](../../strongs/h/h398.md) our own [lechem](../../strongs/h/h3899.md), and [labash](../../strongs/h/h3847.md) our own [śimlâ](../../strongs/h/h8071.md): only let us be [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md), to ['āsap̄](../../strongs/h/h622.md) our [cherpah](../../strongs/h/h2781.md).

<a name="isaiah_4_2"></a>Isaiah 4:2

In that [yowm](../../strongs/h/h3117.md) shall the [ṣemaḥ](../../strongs/h/h6780.md) of [Yĕhovah](../../strongs/h/h3068.md) be [ṣᵊḇî](../../strongs/h/h6643.md) and [kabowd](../../strongs/h/h3519.md), and the [pĕriy](../../strongs/h/h6529.md) of the ['erets](../../strongs/h/h776.md) shall be [gā'ôn](../../strongs/h/h1347.md) and [tip̄'ārâ](../../strongs/h/h8597.md) for them that are [pᵊlêṭâ](../../strongs/h/h6413.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_4_3"></a>Isaiah 4:3

And it shall come to pass, that he that is [šā'ar](../../strongs/h/h7604.md) in [Tsiyown](../../strongs/h/h6726.md), and he that [yāṯar](../../strongs/h/h3498.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), shall be ['āmar](../../strongs/h/h559.md) [qadowsh](../../strongs/h/h6918.md), even every one that is [kāṯaḇ](../../strongs/h/h3789.md) among the [chay](../../strongs/h/h2416.md) in [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="isaiah_4_4"></a>Isaiah 4:4

When ['adonay](../../strongs/h/h136.md) shall have [rāḥaṣ](../../strongs/h/h7364.md) the [ṣ'h](../../strongs/h/h6675.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), and shall have [dûaḥ](../../strongs/h/h1740.md) the [dam](../../strongs/h/h1818.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) from the [qereḇ](../../strongs/h/h7130.md) thereof by the [ruwach](../../strongs/h/h7307.md) of [mishpat](../../strongs/h/h4941.md), and by the [ruwach](../../strongs/h/h7307.md) of [bāʿar](../../strongs/h/h1197.md).

<a name="isaiah_4_5"></a>Isaiah 4:5

And [Yĕhovah](../../strongs/h/h3068.md) will [bara'](../../strongs/h/h1254.md) upon every [māḵôn](../../strongs/h/h4349.md) of [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md), and upon her [miqrā'](../../strongs/h/h4744.md), a [ʿānān](../../strongs/h/h6051.md) and ['ashan](../../strongs/h/h6227.md) by day, and the [nogahh](../../strongs/h/h5051.md) of a [lehāḇâ](../../strongs/h/h3852.md) ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md): for upon all the [kabowd](../../strongs/h/h3519.md) shall be a [ḥupâ](../../strongs/h/h2646.md).

<a name="isaiah_4_6"></a>Isaiah 4:6

And there shall be a [cukkah](../../strongs/h/h5521.md) for a [ṣēl](../../strongs/h/h6738.md) in the [yômām](../../strongs/h/h3119.md) from the [ḥōreḇ](../../strongs/h/h2721.md), and for a place of [machaceh](../../strongs/h/h4268.md), and for a [mistôr](../../strongs/h/h4563.md) from [zerem](../../strongs/h/h2230.md) and from [māṭār](../../strongs/h/h4306.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 3](isaiah_3.md) - [Isaiah 5](isaiah_5.md)