# [Isaiah 36](https://www.blueletterbible.org/kjv/isa/36/1/s_715001)

<a name="isaiah_36_1"></a>Isaiah 36:1

Now it came to pass in the fourteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md), that [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [ʿālâ](../../strongs/h/h5927.md) against all the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [tāp̄aś](../../strongs/h/h8610.md) them.

<a name="isaiah_36_2"></a>Isaiah 36:2

And the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) sent [Raḇ-šāqê](../../strongs/h/h7262.md) from [Lāḵîš](../../strongs/h/h3923.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) unto [melek](../../strongs/h/h4428.md) [Ḥizqîyâ](../../strongs/h/h2396.md) with a [kāḇēḏ](../../strongs/h/h3515.md) [cheyl](../../strongs/h/h2426.md). And he ['amad](../../strongs/h/h5975.md) by the [tᵊʿālâ](../../strongs/h/h8585.md) of the ['elyown](../../strongs/h/h5945.md) [bᵊrēḵâ](../../strongs/h/h1295.md) in the [mĕcillah](../../strongs/h/h4546.md) of the [kāḇas](../../strongs/h/h3526.md) [sadeh](../../strongs/h/h7704.md).

<a name="isaiah_36_3"></a>Isaiah 36:3

Then [yāṣā'](../../strongs/h/h3318.md) unto him ['Elyāqîm](../../strongs/h/h471.md), [Ḥilqîyâ](../../strongs/h/h2518.md) [ben](../../strongs/h/h1121.md), which was over the [bayith](../../strongs/h/h1004.md), and [Šeḇnā'](../../strongs/h/h7644.md) the [sāp̄ar](../../strongs/h/h5608.md), and [Yô'āḥ](../../strongs/h/h3098.md), ['Āsāp̄](../../strongs/h/h623.md) [ben](../../strongs/h/h1121.md), the [zakar](../../strongs/h/h2142.md).

<a name="isaiah_36_4"></a>Isaiah 36:4

And [Raḇ-šāqê](../../strongs/h/h7262.md) ['āmar](../../strongs/h/h559.md) unto them, ['āmar](../../strongs/h/h559.md) ye now to [Ḥizqîyâ](../../strongs/h/h2396.md), Thus ['āmar](../../strongs/h/h559.md) the [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md), the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), What [biṭṭāḥôn](../../strongs/h/h986.md) is this wherein thou [batach](../../strongs/h/h982.md)?

<a name="isaiah_36_5"></a>Isaiah 36:5

I ['āmar](../../strongs/h/h559.md), sayest thou, (but they are but [saphah](../../strongs/h/h8193.md) [dabar](../../strongs/h/h1697.md)) I have ['etsah](../../strongs/h/h6098.md) and [gᵊḇûrâ](../../strongs/h/h1369.md) for [milḥāmâ](../../strongs/h/h4421.md): now on whom dost thou [batach](../../strongs/h/h982.md), that thou [māraḏ](../../strongs/h/h4775.md) against me?

<a name="isaiah_36_6"></a>Isaiah 36:6

Lo, thou [batach](../../strongs/h/h982.md) in the [mašʿēnâ](../../strongs/h/h4938.md) of this [rāṣaṣ](../../strongs/h/h7533.md) [qānê](../../strongs/h/h7070.md), on [Mitsrayim](../../strongs/h/h4714.md); whereon if an ['iysh](../../strongs/h/h376.md) [camak](../../strongs/h/h5564.md), it will [bow'](../../strongs/h/h935.md) into his [kaph](../../strongs/h/h3709.md), and [nāqaḇ](../../strongs/h/h5344.md) it: so is [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) to all that [batach](../../strongs/h/h982.md) in him.

<a name="isaiah_36_7"></a>Isaiah 36:7

But if thou ['āmar](../../strongs/h/h559.md) to me, We [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): is it not he, whose [bāmâ](../../strongs/h/h1116.md) and whose [mizbeach](../../strongs/h/h4196.md) [Ḥizqîyâ](../../strongs/h/h2396.md) hath [cuwr](../../strongs/h/h5493.md), and ['āmar](../../strongs/h/h559.md) to [Yehuwdah](../../strongs/h/h3063.md) and to [Yĕruwshalaim](../../strongs/h/h3389.md), Ye shall [shachah](../../strongs/h/h7812.md) before this [mizbeach](../../strongs/h/h4196.md)?

<a name="isaiah_36_8"></a>Isaiah 36:8

Now therefore [ʿāraḇ](../../strongs/h/h6148.md), I pray thee, to my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and I will [nathan](../../strongs/h/h5414.md) thee two ['elep̄](../../strongs/h/h505.md) [sûs](../../strongs/h/h5483.md), if thou be [yakol](../../strongs/h/h3201.md) on thy part to [nathan](../../strongs/h/h5414.md) [rāḵaḇ](../../strongs/h/h7392.md) upon them.

<a name="isaiah_36_9"></a>Isaiah 36:9

How then wilt thou [shuwb](../../strongs/h/h7725.md) the [paniym](../../strongs/h/h6440.md) of ['echad](../../strongs/h/h259.md) [peḥâ](../../strongs/h/h6346.md) of the [qāṭān](../../strongs/h/h6996.md) of my ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md), and put thy [batach](../../strongs/h/h982.md) on [Mitsrayim](../../strongs/h/h4714.md) for [reḵeḇ](../../strongs/h/h7393.md) and for [pārāš](../../strongs/h/h6571.md)?

<a name="isaiah_36_10"></a>Isaiah 36:10

And am I now [ʿālâ](../../strongs/h/h5927.md) without [Yĕhovah](../../strongs/h/h3068.md) against this ['erets](../../strongs/h/h776.md) to [shachath](../../strongs/h/h7843.md) it? [Yĕhovah](../../strongs/h/h3068.md) said unto me, [ʿālâ](../../strongs/h/h5927.md) against this ['erets](../../strongs/h/h776.md), and [shachath](../../strongs/h/h7843.md) it.

<a name="isaiah_36_11"></a>Isaiah 36:11

Then ['āmar](../../strongs/h/h559.md) ['Elyāqîm](../../strongs/h/h471.md) and [Šeḇnā'](../../strongs/h/h7644.md) and [Yô'āḥ](../../strongs/h/h3098.md) unto [Raḇ-šāqê](../../strongs/h/h7262.md), [dabar](../../strongs/h/h1696.md), I pray thee, unto thy ['ebed](../../strongs/h/h5650.md) in ['ărāmy](../../strongs/h/h762.md); for we [shama'](../../strongs/h/h8085.md) it: and [dabar](../../strongs/h/h1696.md) not to us in [yᵊhûḏîṯ](../../strongs/h/h3066.md), in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md) that are on the [ḥômâ](../../strongs/h/h2346.md).

<a name="isaiah_36_12"></a>Isaiah 36:12

But [Raḇ-šāqê](../../strongs/h/h7262.md) ['āmar](../../strongs/h/h559.md), Hath my ['adown](../../strongs/h/h113.md) [shalach](../../strongs/h/h7971.md) me to thy ['adown](../../strongs/h/h113.md) and to thee to [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md)? hath he not sent me to the ['enowsh](../../strongs/h/h582.md) that [yashab](../../strongs/h/h3427.md) upon the [ḥômâ](../../strongs/h/h2346.md), that they may ['akal](../../strongs/h/h398.md) their own [ḥărā'îm](../../strongs/h/h2716.md) [ṣ'h](../../strongs/h/h6675.md), and [šāṯâ](../../strongs/h/h8354.md) their own [mayim](../../strongs/h/h4325.md) [regel](../../strongs/h/h7272.md) [šayin](../../strongs/h/h7890.md) with you?

<a name="isaiah_36_13"></a>Isaiah 36:13

Then [Raḇ-šāqê](../../strongs/h/h7262.md) ['amad](../../strongs/h/h5975.md), and [qara'](../../strongs/h/h7121.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) in [yᵊhûḏîṯ](../../strongs/h/h3066.md), and ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of the [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md), the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_36_14"></a>Isaiah 36:14

Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), Let not [Ḥizqîyâ](../../strongs/h/h2396.md) [nasha'](../../strongs/h/h5377.md) you: for he shall not be able to [natsal](../../strongs/h/h5337.md) you.

<a name="isaiah_36_15"></a>Isaiah 36:15

Neither let [Ḥizqîyâ](../../strongs/h/h2396.md) make you [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), saying, [Yĕhovah](../../strongs/h/h3068.md) will surely [natsal](../../strongs/h/h5337.md) us: this [ʿîr](../../strongs/h/h5892.md) shall not be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_36_16"></a>Isaiah 36:16

[shama'](../../strongs/h/h8085.md) not to [Ḥizqîyâ](../../strongs/h/h2396.md): for thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), Make a [bĕrakah](../../strongs/h/h1293.md), and [yāṣā'](../../strongs/h/h3318.md) to me: and ['akal](../../strongs/h/h398.md) ye every one of his [gep̄en](../../strongs/h/h1612.md), and every one of his [tĕ'en](../../strongs/h/h8384.md), and [šāṯâ](../../strongs/h/h8354.md) ye every one the [mayim](../../strongs/h/h4325.md) of his own [bowr](../../strongs/h/h953.md);

<a name="isaiah_36_17"></a>Isaiah 36:17

Until I [bow'](../../strongs/h/h935.md) and [laqach](../../strongs/h/h3947.md) to an ['erets](../../strongs/h/h776.md) like your own ['erets](../../strongs/h/h776.md), an ['erets](../../strongs/h/h776.md) of [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md), an ['erets](../../strongs/h/h776.md) of [lechem](../../strongs/h/h3899.md) and [kerem](../../strongs/h/h3754.md).

<a name="isaiah_36_18"></a>Isaiah 36:18

Beware lest [Ḥizqîyâ](../../strongs/h/h2396.md) [sûṯ](../../strongs/h/h5496.md) you, saying, [Yĕhovah](../../strongs/h/h3068.md) will [natsal](../../strongs/h/h5337.md) us. Hath any of the ['Elohiym](../../strongs/h/h430.md) of the [gowy](../../strongs/h/h1471.md) [natsal](../../strongs/h/h5337.md) his ['erets](../../strongs/h/h776.md) out of the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md)?

<a name="isaiah_36_19"></a>Isaiah 36:19

Where are the ['Elohiym](../../strongs/h/h430.md) of [Ḥămāṯ](../../strongs/h/h2574.md) and ['Arpāḏ](../../strongs/h/h774.md)? where are the ['Elohiym](../../strongs/h/h430.md) of [Sᵊp̄arvayim](../../strongs/h/h5617.md)? and have they [natsal](../../strongs/h/h5337.md) [Šōmrôn](../../strongs/h/h8111.md) out of my [yad](../../strongs/h/h3027.md)?

<a name="isaiah_36_20"></a>Isaiah 36:20

Who are they among all the ['Elohiym](../../strongs/h/h430.md) of these ['erets](../../strongs/h/h776.md), that have [natsal](../../strongs/h/h5337.md) their ['erets](../../strongs/h/h776.md) out of my [yad](../../strongs/h/h3027.md), that [Yĕhovah](../../strongs/h/h3068.md) should [natsal](../../strongs/h/h5337.md) [Yĕruwshalaim](../../strongs/h/h3389.md) out of my [yad](../../strongs/h/h3027.md)?

<a name="isaiah_36_21"></a>Isaiah 36:21

But they [ḥāraš](../../strongs/h/h2790.md), and ['anah](../../strongs/h/h6030.md) him not a [dabar](../../strongs/h/h1697.md): for the [melek](../../strongs/h/h4428.md) [mitsvah](../../strongs/h/h4687.md) was, saying, ['anah](../../strongs/h/h6030.md) him not.

<a name="isaiah_36_22"></a>Isaiah 36:22

Then [bow'](../../strongs/h/h935.md) ['Elyāqîm](../../strongs/h/h471.md), the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), that was over the [bayith](../../strongs/h/h1004.md), and [Šeḇnā'](../../strongs/h/h7644.md) the [sāp̄ar](../../strongs/h/h5608.md), and [Yô'āḥ](../../strongs/h/h3098.md), the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), the [zakar](../../strongs/h/h2142.md), to [Ḥizqîyâ](../../strongs/h/h2396.md) with their [beḡeḏ](../../strongs/h/h899.md) [qāraʿ](../../strongs/h/h7167.md), and told him the [dabar](../../strongs/h/h1697.md) of [Raḇ-šāqê](../../strongs/h/h7262.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 35](isaiah_35.md) - [Isaiah 37](isaiah_37.md)