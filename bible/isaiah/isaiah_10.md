# [Isaiah 10](https://www.blueletterbible.org/kjv/isa/10/1/s_689001)

<a name="isaiah_10_1"></a>Isaiah 10:1

[hôy](../../strongs/h/h1945.md) unto them that [ḥāqaq](../../strongs/h/h2710.md) ['aven](../../strongs/h/h205.md) [ḥēqeq](../../strongs/h/h2711.md), and that [kāṯaḇ](../../strongs/h/h3789.md) ['amal](../../strongs/h/h5999.md) which they have [kāṯaḇ](../../strongs/h/h3789.md);

<a name="isaiah_10_2"></a>Isaiah 10:2

To [natah](../../strongs/h/h5186.md) the [dal](../../strongs/h/h1800.md) from [diyn](../../strongs/h/h1779.md), and to take away the [mishpat](../../strongs/h/h4941.md) from the ['aniy](../../strongs/h/h6041.md) of my ['am](../../strongs/h/h5971.md), that ['almānâ](../../strongs/h/h490.md) may be their [šālāl](../../strongs/h/h7998.md), and that they may [bāzaz](../../strongs/h/h962.md) the [yathowm](../../strongs/h/h3490.md)!

<a name="isaiah_10_3"></a>Isaiah 10:3

And what will ye ['asah](../../strongs/h/h6213.md) in the [yowm](../../strongs/h/h3117.md) of [pᵊqudâ](../../strongs/h/h6486.md), and in the [šô'](../../strongs/h/h7722.md) which shall come from [merḥāq](../../strongs/h/h4801.md)? to whom will ye [nûs](../../strongs/h/h5127.md) for [ʿezrâ](../../strongs/h/h5833.md)? and where will ye ['azab](../../strongs/h/h5800.md) your [kabowd](../../strongs/h/h3519.md)?

<a name="isaiah_10_4"></a>Isaiah 10:4

Without me they shall [kara'](../../strongs/h/h3766.md) under the ['assîr](../../strongs/h/h616.md), and they shall [naphal](../../strongs/h/h5307.md) under the [harag](../../strongs/h/h2026.md). For all this his ['aph](../../strongs/h/h639.md) is not [shuwb](../../strongs/h/h7725.md), but his [yad](../../strongs/h/h3027.md) is [natah](../../strongs/h/h5186.md).

<a name="isaiah_10_5"></a>Isaiah 10:5

[hôy](../../strongs/h/h1945.md) ['Aššûr](../../strongs/h/h804.md), the [shebet](../../strongs/h/h7626.md) of mine ['aph](../../strongs/h/h639.md), and the [maṭṭê](../../strongs/h/h4294.md) in their [yad](../../strongs/h/h3027.md) is mine [zaʿam](../../strongs/h/h2195.md).

<a name="isaiah_10_6"></a>Isaiah 10:6

I will [shalach](../../strongs/h/h7971.md) him against an [ḥānēp̄](../../strongs/h/h2611.md) [gowy](../../strongs/h/h1471.md), and against the ['am](../../strongs/h/h5971.md) of my ['ebrah](../../strongs/h/h5678.md) will I give him a [tsavah](../../strongs/h/h6680.md), to take the [šālāl](../../strongs/h/h7998.md), and to [bāzaz](../../strongs/h/h962.md) the [baz](../../strongs/h/h957.md), and to [śûm](../../strongs/h/h7760.md) [mirmās](../../strongs/h/h4823.md) like the [ḥōmer](../../strongs/h/h2563.md) of the [ḥûṣ](../../strongs/h/h2351.md).

<a name="isaiah_10_7"></a>Isaiah 10:7

Howbeit he [dāmâ](../../strongs/h/h1819.md) not so, neither doth his [lebab](../../strongs/h/h3824.md) [chashab](../../strongs/h/h2803.md) so; but it is in his [lebab](../../strongs/h/h3824.md) to [šāmaḏ](../../strongs/h/h8045.md) and [karath](../../strongs/h/h3772.md) [gowy](../../strongs/h/h1471.md) not a few.

<a name="isaiah_10_8"></a>Isaiah 10:8

For he ['āmar](../../strongs/h/h559.md), Are not my [śar](../../strongs/h/h8269.md) altogether [melek](../../strongs/h/h4428.md)?

<a name="isaiah_10_9"></a>Isaiah 10:9

Is not [kalnê](../../strongs/h/h3641.md) as [karkᵊmîš](../../strongs/h/h3751.md)? is not [Ḥămāṯ](../../strongs/h/h2574.md) as ['Arpāḏ](../../strongs/h/h774.md)? is not [Šōmrôn](../../strongs/h/h8111.md) as [Dammeśeq](../../strongs/h/h1834.md)?

<a name="isaiah_10_10"></a>Isaiah 10:10

As my [yad](../../strongs/h/h3027.md) hath [māṣā'](../../strongs/h/h4672.md) the [mamlāḵâ](../../strongs/h/h4467.md) of the ['ĕlîl](../../strongs/h/h457.md), and whose [pāsîl](../../strongs/h/h6456.md) did excel them of [Yĕruwshalaim](../../strongs/h/h3389.md) and of [Šōmrôn](../../strongs/h/h8111.md);

<a name="isaiah_10_11"></a>Isaiah 10:11

Shall I not, as I have ['asah](../../strongs/h/h6213.md) unto [Šōmrôn](../../strongs/h/h8111.md) and her ['ĕlîl](../../strongs/h/h457.md), so ['asah](../../strongs/h/h6213.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) and her [ʿāṣāḇ](../../strongs/h/h6091.md)?

<a name="isaiah_10_12"></a>Isaiah 10:12

Wherefore it shall come to pass, that when ['adonay](../../strongs/h/h136.md) hath [batsa'](../../strongs/h/h1214.md) his whole [ma'aseh](../../strongs/h/h4639.md) upon [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) and on [Yĕruwshalaim](../../strongs/h/h3389.md), I will [paqad](../../strongs/h/h6485.md) the [pĕriy](../../strongs/h/h6529.md) of the [gōḏel](../../strongs/h/h1433.md) [lebab](../../strongs/h/h3824.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and the [tip̄'ārâ](../../strongs/h/h8597.md) of his [rûm](../../strongs/h/h7312.md) ['ayin](../../strongs/h/h5869.md).

<a name="isaiah_10_13"></a>Isaiah 10:13

For he ['āmar](../../strongs/h/h559.md), By the [koach](../../strongs/h/h3581.md) of my [yad](../../strongs/h/h3027.md) I have done it, and by my [ḥāḵmâ](../../strongs/h/h2451.md); for I am [bîn](../../strongs/h/h995.md): and I have [cuwr](../../strongs/h/h5493.md) the [gᵊḇûlâ](../../strongs/h/h1367.md) of the ['am](../../strongs/h/h5971.md), and have [šāsâ](../../strongs/h/h8154.md) their [ʿāṯûḏ](../../strongs/h/h6259.md) [ʿāṯîḏ](../../strongs/h/h6264.md), and I have [yarad](../../strongs/h/h3381.md) the [yashab](../../strongs/h/h3427.md) like an ['abîr](../../strongs/h/h47.md) [kabîr](../../strongs/h/h3524.md):

<a name="isaiah_10_14"></a>Isaiah 10:14

And my [yad](../../strongs/h/h3027.md) hath [māṣā'](../../strongs/h/h4672.md) as a [qēn](../../strongs/h/h7064.md) the [ḥayil](../../strongs/h/h2428.md) of the ['am](../../strongs/h/h5971.md): and as one ['āsap̄](../../strongs/h/h622.md) [bêṣâ](../../strongs/h/h1000.md) that are left, have I ['āsap̄](../../strongs/h/h622.md) all the ['erets](../../strongs/h/h776.md); and there was none that [nāḏaḏ](../../strongs/h/h5074.md) the [kanaph](../../strongs/h/h3671.md), or [pāṣâ](../../strongs/h/h6475.md) the [peh](../../strongs/h/h6310.md), or [ṣāp̄ap̄](../../strongs/h/h6850.md).

<a name="isaiah_10_15"></a>Isaiah 10:15

Shall the [garzen](../../strongs/h/h1631.md) [pā'ar](../../strongs/h/h6286.md) itself against him that [ḥāṣaḇ](../../strongs/h/h2672.md) therewith? or shall the [maśśôr](../../strongs/h/h4883.md) [gāḏal](../../strongs/h/h1431.md) itself against him that [nûp̄](../../strongs/h/h5130.md) it? as if the [shebet](../../strongs/h/h7626.md) should [nûp̄](../../strongs/h/h5130.md) itself against them that [ruwm](../../strongs/h/h7311.md), or as if the [maṭṭê](../../strongs/h/h4294.md) should [ruwm](../../strongs/h/h7311.md) itself, as if it were no ['ets](../../strongs/h/h6086.md).

<a name="isaiah_10_16"></a>Isaiah 10:16

Therefore shall ['adown](../../strongs/h/h113.md), ['adonay](../../strongs/h/h136.md) of [tsaba'](../../strongs/h/h6635.md), [shalach](../../strongs/h/h7971.md) among his [mišmān](../../strongs/h/h4924.md) [rāzôn](../../strongs/h/h7332.md); and under his [kabowd](../../strongs/h/h3519.md) he shall [yāqaḏ](../../strongs/h/h3344.md) a [yᵊqôḏ](../../strongs/h/h3350.md) like the ['esh](../../strongs/h/h784.md).

<a name="isaiah_10_17"></a>Isaiah 10:17

And the ['owr](../../strongs/h/h216.md) of [Yisra'el](../../strongs/h/h3478.md) shall be for an ['esh](../../strongs/h/h784.md), and his [qadowsh](../../strongs/h/h6918.md) for a [lehāḇâ](../../strongs/h/h3852.md): and it shall [bāʿar](../../strongs/h/h1197.md) and ['akal](../../strongs/h/h398.md) his [šayiṯ](../../strongs/h/h7898.md) and his [šāmîr](../../strongs/h/h8068.md) in one day;

<a name="isaiah_10_18"></a>Isaiah 10:18

And shall [kalah](../../strongs/h/h3615.md) the [kabowd](../../strongs/h/h3519.md) of his [yaʿar](../../strongs/h/h3293.md), and of his [karmel](../../strongs/h/h3759.md), both [nephesh](../../strongs/h/h5315.md) and [basar](../../strongs/h/h1320.md): and they shall be as when a [nāsas](../../strongs/h/h5263.md) [māsas](../../strongs/h/h4549.md).

<a name="isaiah_10_19"></a>Isaiah 10:19

And the [šᵊ'ār](../../strongs/h/h7605.md) of the ['ets](../../strongs/h/h6086.md) of his [yaʿar](../../strongs/h/h3293.md) shall be [mispār](../../strongs/h/h4557.md), that a [naʿar](../../strongs/h/h5288.md) may [kāṯaḇ](../../strongs/h/h3789.md) them.

<a name="isaiah_10_20"></a>Isaiah 10:20

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that the [šᵊ'ār](../../strongs/h/h7605.md) of [Yisra'el](../../strongs/h/h3478.md), and such as are [pᵊlêṭâ](../../strongs/h/h6413.md) of the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), shall no more again [šāʿan](../../strongs/h/h8172.md) upon him that [nakah](../../strongs/h/h5221.md) them; but shall [šāʿan](../../strongs/h/h8172.md) upon [Yĕhovah](../../strongs/h/h3068.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), in ['emeth](../../strongs/h/h571.md).

<a name="isaiah_10_21"></a>Isaiah 10:21

The [šᵊ'ār](../../strongs/h/h7605.md) shall [shuwb](../../strongs/h/h7725.md), even the [šᵊ'ār](../../strongs/h/h7605.md) of [Ya'aqob](../../strongs/h/h3290.md), unto the [gibôr](../../strongs/h/h1368.md) ['el](../../strongs/h/h410.md).

<a name="isaiah_10_22"></a>Isaiah 10:22

For though thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) be as the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md), yet a [šᵊ'ār](../../strongs/h/h7605.md) of them shall [shuwb](../../strongs/h/h7725.md): the [killāyôn](../../strongs/h/h3631.md) [ḥāraṣ](../../strongs/h/h2782.md) shall [šāṭap̄](../../strongs/h/h7857.md) with [ṣĕdāqāh](../../strongs/h/h6666.md).

<a name="isaiah_10_23"></a>Isaiah 10:23

For ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) shall ['asah](../../strongs/h/h6213.md) a [kālâ](../../strongs/h/h3617.md), even [ḥāraṣ](../../strongs/h/h2782.md), in the [qereḇ](../../strongs/h/h7130.md) of all the ['erets](../../strongs/h/h776.md).

<a name="isaiah_10_24"></a>Isaiah 10:24

Therefore thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md), O my people that [yashab](../../strongs/h/h3427.md) in [Tsiyown](../../strongs/h/h6726.md), be not [yare'](../../strongs/h/h3372.md) of the ['Aššûr](../../strongs/h/h804.md): he shall [nakah](../../strongs/h/h5221.md) thee with a [shebet](../../strongs/h/h7626.md), and shall [nasa'](../../strongs/h/h5375.md) his [maṭṭê](../../strongs/h/h4294.md) against thee, after the [derek](../../strongs/h/h1870.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="isaiah_10_25"></a>Isaiah 10:25

For yet a [mizʿār](../../strongs/h/h4213.md) [mᵊʿaṭ](../../strongs/h/h4592.md), and the [zaʿam](../../strongs/h/h2195.md) shall [kalah](../../strongs/h/h3615.md), and mine ['aph](../../strongs/h/h639.md) in their [taḇlîṯ](../../strongs/h/h8399.md).

<a name="isaiah_10_26"></a>Isaiah 10:26

And [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall [ʿûr](../../strongs/h/h5782.md) a [šôṭ](../../strongs/h/h7752.md) for him according to the [makâ](../../strongs/h/h4347.md) of [Miḏyān](../../strongs/h/h4080.md) at the [tsuwr](../../strongs/h/h6697.md) of [ʿōrēḇ](../../strongs/h/h6159.md): and as his [maṭṭê](../../strongs/h/h4294.md) was upon the [yam](../../strongs/h/h3220.md), so shall he [nasa'](../../strongs/h/h5375.md) after the [derek](../../strongs/h/h1870.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="isaiah_10_27"></a>Isaiah 10:27

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that his [sōḇel](../../strongs/h/h5448.md) shall be [cuwr](../../strongs/h/h5493.md) from off thy [šᵊḵem](../../strongs/h/h7926.md), and his [ʿōl](../../strongs/h/h5923.md) from off thy [ṣaûā'r](../../strongs/h/h6677.md), and the [ʿōl](../../strongs/h/h5923.md) shall be [chabal](../../strongs/h/h2254.md) because of the [šemen](../../strongs/h/h8081.md).

<a name="isaiah_10_28"></a>Isaiah 10:28

He is [bow'](../../strongs/h/h935.md) to [ʿAy](../../strongs/h/h5857.md), he is ['abar](../../strongs/h/h5674.md) to [miḡrôn](../../strongs/h/h4051.md); at [Miḵmās](../../strongs/h/h4363.md) he hath [paqad](../../strongs/h/h6485.md) his [kĕliy](../../strongs/h/h3627.md):

<a name="isaiah_10_29"></a>Isaiah 10:29

They are ['abar](../../strongs/h/h5674.md) the [maʿăḇār](../../strongs/h/h4569.md): they have taken up their [mālôn](../../strongs/h/h4411.md) at [Geḇaʿ](../../strongs/h/h1387.md); [rāmâ](../../strongs/h/h7414.md) is [ḥārēḏ](../../strongs/h/h2729.md); [giḇʿâ](../../strongs/h/h1390.md) of [Šā'ûl](../../strongs/h/h7586.md) is [nûs](../../strongs/h/h5127.md).

<a name="isaiah_10_30"></a>Isaiah 10:30

[ṣāhal](../../strongs/h/h6670.md) thy [qowl](../../strongs/h/h6963.md), O [bath](../../strongs/h/h1323.md) of [gallîm](../../strongs/h/h1554.md): cause it to be heard unto [layiš](../../strongs/h/h3919.md), O ['aniy](../../strongs/h/h6041.md) [ʿĂnāṯôṯ](../../strongs/h/h6068.md).

<a name="isaiah_10_31"></a>Isaiah 10:31

[maḏmēnâ](../../strongs/h/h4088.md) is [nāḏaḏ](../../strongs/h/h5074.md); the [yashab](../../strongs/h/h3427.md) of [gēḇîm](../../strongs/h/h1374.md) [ʿûz](../../strongs/h/h5756.md).

<a name="isaiah_10_32"></a>Isaiah 10:32

As yet shall he ['amad](../../strongs/h/h5975.md) at [nōḇ](../../strongs/h/h5011.md) that day: he shall [nûp̄](../../strongs/h/h5130.md) his [yad](../../strongs/h/h3027.md) against the [har](../../strongs/h/h2022.md) of the [bath](../../strongs/h/h1323.md) [bayith](../../strongs/h/h1004.md) of [Tsiyown](../../strongs/h/h6726.md), the [giḇʿâ](../../strongs/h/h1389.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="isaiah_10_33"></a>Isaiah 10:33

Behold, ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), shall [sāʿap̄](../../strongs/h/h5586.md) the [pō'râ](../../strongs/h/h6288.md) with [maʿărāṣâ](../../strongs/h/h4637.md): and the [ruwm](../../strongs/h/h7311.md) of [qômâ](../../strongs/h/h6967.md) shall be [gāḏaʿ](../../strongs/h/h1438.md), and the [gāḇōha](../../strongs/h/h1364.md) shall be [šāp̄ēl](../../strongs/h/h8213.md).

<a name="isaiah_10_34"></a>Isaiah 10:34

And he shall [naqaph](../../strongs/h/h5362.md) the [sᵊḇāḵ](../../strongs/h/h5442.md) of the [yaʿar](../../strongs/h/h3293.md) with [barzel](../../strongs/h/h1270.md), and [Lᵊḇānôn](../../strongs/h/h3844.md) shall [naphal](../../strongs/h/h5307.md) by an ['addiyr](../../strongs/h/h117.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 9](isaiah_9.md) - [Isaiah 11](isaiah_11.md)