# [Isaiah 50]()

<a name="isaiah_50_1"></a>Isaiah 50:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Where is the [sēp̄er](../../strongs/h/h5612.md) of your ['em](../../strongs/h/h517.md) [kᵊrîṯûṯ](../../strongs/h/h3748.md), whom I have [shalach](../../strongs/h/h7971.md)? or which of my [nāšâ](../../strongs/h/h5383.md) is it to whom I have [māḵar](../../strongs/h/h4376.md) you?  Behold, for your ['avon](../../strongs/h/h5771.md) have ye [māḵar](../../strongs/h/h4376.md) yourselves, and for your [pesha'](../../strongs/h/h6588.md) is your ['em](../../strongs/h/h517.md) [shalach](../../strongs/h/h7971.md).

<a name="isaiah_50_2"></a>Isaiah 50:2

Wherefore, when I [bow'](../../strongs/h/h935.md), was there no ['iysh](../../strongs/h/h376.md)? when I [qara'](../../strongs/h/h7121.md), was there none to ['anah](../../strongs/h/h6030.md)? Is my [yad](../../strongs/h/h3027.md) [qāṣar](../../strongs/h/h7114.md) at all, that it cannot [pᵊḏûṯ](../../strongs/h/h6304.md)? or have I no [koach](../../strongs/h/h3581.md) to [natsal](../../strongs/h/h5337.md)? behold, at my [ge'arah](../../strongs/h/h1606.md) I [ḥāraḇ](../../strongs/h/h2717.md) the [yam](../../strongs/h/h3220.md), I [śûm](../../strongs/h/h7760.md) the [nāhār](../../strongs/h/h5104.md) a [midbar](../../strongs/h/h4057.md): their [dāḡâ](../../strongs/h/h1710.md) [bā'aš](../../strongs/h/h887.md), because there is no [mayim](../../strongs/h/h4325.md), and [muwth](../../strongs/h/h4191.md) for [ṣāmā'](../../strongs/h/h6772.md).

<a name="isaiah_50_3"></a>Isaiah 50:3

I [labash](../../strongs/h/h3847.md) the [shamayim](../../strongs/h/h8064.md) with [qaḏrûṯ](../../strongs/h/h6940.md), and I [śûm](../../strongs/h/h7760.md) [śaq](../../strongs/h/h8242.md) their [kᵊsûṯ](../../strongs/h/h3682.md).

<a name="isaiah_50_4"></a>Isaiah 50:4

['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) hath [nathan](../../strongs/h/h5414.md) me the [lashown](../../strongs/h/h3956.md) of the [limmûḏ](../../strongs/h/h3928.md), that I should [yada'](../../strongs/h/h3045.md) how to [ʿûṯ](../../strongs/h/h5790.md) a [dabar](../../strongs/h/h1697.md) in season to him that is [yāʿēp̄](../../strongs/h/h3287.md): he [ʿûr](../../strongs/h/h5782.md) [boqer](../../strongs/h/h1242.md) by [boqer](../../strongs/h/h1242.md), he [ʿûr](../../strongs/h/h5782.md) mine ['ozen](../../strongs/h/h241.md) to [shama'](../../strongs/h/h8085.md) as the [limmûḏ](../../strongs/h/h3928.md).

<a name="isaiah_50_5"></a>Isaiah 50:5

['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) hath [pāṯaḥ](../../strongs/h/h6605.md) mine ['ozen](../../strongs/h/h241.md), and I was not [marah](../../strongs/h/h4784.md), neither [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md).

<a name="isaiah_50_6"></a>Isaiah 50:6

I [nathan](../../strongs/h/h5414.md) my [gēv](../../strongs/h/h1460.md) the [nakah](../../strongs/h/h5221.md), and my [lᵊḥî](../../strongs/h/h3895.md) to them that [māraṭ](../../strongs/h/h4803.md): I [cathar](../../strongs/h/h5641.md) not my [paniym](../../strongs/h/h6440.md) from [kĕlimmah](../../strongs/h/h3639.md) and [rōq](../../strongs/h/h7536.md).

<a name="isaiah_50_7"></a>Isaiah 50:7

For ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will [ʿāzar](../../strongs/h/h5826.md) me; therefore shall I not be [kālam](../../strongs/h/h3637.md): therefore have I [śûm](../../strongs/h/h7760.md) my [paniym](../../strongs/h/h6440.md) like a [ḥallāmîš](../../strongs/h/h2496.md), and I [yada'](../../strongs/h/h3045.md) that I shall not be [buwsh](../../strongs/h/h954.md).

<a name="isaiah_50_8"></a>Isaiah 50:8

He is [qarowb](../../strongs/h/h7138.md) that [ṣāḏaq](../../strongs/h/h6663.md) me; who will [riyb](../../strongs/h/h7378.md) with me? let us ['amad](../../strongs/h/h5975.md) [yaḥaḏ](../../strongs/h/h3162.md): who is mine [baʿal](../../strongs/h/h1167.md) [mishpat](../../strongs/h/h4941.md)? let him [nāḡaš](../../strongs/h/h5066.md) to me.

<a name="isaiah_50_9"></a>Isaiah 50:9

Behold, ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will [ʿāzar](../../strongs/h/h5826.md) me; who is he that shall [rāšaʿ](../../strongs/h/h7561.md) me?  lo, they all shall [bālâ](../../strongs/h/h1086.md) as a [beḡeḏ](../../strongs/h/h899.md); the [ʿāš](../../strongs/h/h6211.md) shall ['akal](../../strongs/h/h398.md) them up.

<a name="isaiah_50_10"></a>Isaiah 50:10

Who is among you that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), that [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of his ['ebed](../../strongs/h/h5650.md), that [halak](../../strongs/h/h1980.md) in [ḥăšēḵâ](../../strongs/h/h2825.md), and hath no [nogahh](../../strongs/h/h5051.md)? let him [batach](../../strongs/h/h982.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), and [šāʿan](../../strongs/h/h8172.md) upon his ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_50_11"></a>Isaiah 50:11

Behold, all ye that [qāḏaḥ](../../strongs/h/h6919.md) an ['esh](../../strongs/h/h784.md), that ['āzar](../../strongs/h/h247.md) with [zîqôṯ](../../strongs/h/h2131.md): [yālaḵ](../../strongs/h/h3212.md) in the ['ûr](../../strongs/h/h217.md) of your ['esh](../../strongs/h/h784.md), and in the [zîqôṯ](../../strongs/h/h2131.md) that ye have [bāʿar](../../strongs/h/h1197.md). This shall ye have of mine [yad](../../strongs/h/h3027.md); ye shall [shakab](../../strongs/h/h7901.md) in [maʿăṣēḇâ](../../strongs/h/h4620.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 49](isaiah_49.md) - [Isaiah 51](isaiah_51.md)