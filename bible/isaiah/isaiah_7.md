# [Isaiah 7](https://www.blueletterbible.org/kjv/isa/7/1/s_686001)

<a name="isaiah_7_1"></a>Isaiah 7:1

And it came to pass in the days of ['Āḥāz](../../strongs/h/h271.md) the [ben](../../strongs/h/h1121.md) of [Yôṯām](../../strongs/h/h3147.md), the [ben](../../strongs/h/h1121.md) of ['Uzziyah](../../strongs/h/h5818.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), that [Rᵊṣîn](../../strongs/h/h7526.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), and [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md), [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [ʿālâ](../../strongs/h/h5927.md) toward [Yĕruwshalaim](../../strongs/h/h3389.md) to [milḥāmâ](../../strongs/h/h4421.md) against it, but could not [lāḥam](../../strongs/h/h3898.md) against it.

<a name="isaiah_7_2"></a>Isaiah 7:2

And it was [nāḡaḏ](../../strongs/h/h5046.md) the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), ['Ărām](../../strongs/h/h758.md) is [nuwach](../../strongs/h/h5117.md) with ['Ep̄rayim](../../strongs/h/h669.md). And his [lebab](../../strongs/h/h3824.md) was [nûaʿ](../../strongs/h/h5128.md), and the [lebab](../../strongs/h/h3824.md) of his ['am](../../strongs/h/h5971.md), as the ['ets](../../strongs/h/h6086.md) of the [yaʿar](../../strongs/h/h3293.md) are [nûaʿ](../../strongs/h/h5128.md) with the [ruwach](../../strongs/h/h7307.md).

<a name="isaiah_7_3"></a>Isaiah 7:3

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto [Yᵊšaʿyâ](../../strongs/h/h3470.md), [yāṣā'](../../strongs/h/h3318.md) now to [qārā'](../../strongs/h/h7125.md) ['Āḥāz](../../strongs/h/h271.md), thou, and [Šᵊ'ār yāšûḇ](../../strongs/h/h7610.md) thy [ben](../../strongs/h/h1121.md), at the [qāṣê](../../strongs/h/h7097.md) of the [tᵊʿālâ](../../strongs/h/h8585.md) of the ['elyown](../../strongs/h/h5945.md) [bᵊrēḵâ](../../strongs/h/h1295.md) in the [mĕcillah](../../strongs/h/h4546.md) of the [kāḇas](../../strongs/h/h3526.md) [sadeh](../../strongs/h/h7704.md);

<a name="isaiah_7_4"></a>Isaiah 7:4

And ['āmar](../../strongs/h/h559.md) unto him, [shamar](../../strongs/h/h8104.md), and [šāqaṭ](../../strongs/h/h8252.md); [yare'](../../strongs/h/h3372.md) not, neither be [rāḵaḵ](../../strongs/h/h7401.md) [lebab](../../strongs/h/h3824.md) for the two [zānāḇ](../../strongs/h/h2180.md) of these [ʿāšēn](../../strongs/h/h6226.md) ['ûḏ](../../strongs/h/h181.md), for the [ḥŏrî](../../strongs/h/h2750.md) ['aph](../../strongs/h/h639.md) of [Rᵊṣîn](../../strongs/h/h7526.md) with ['Ărām](../../strongs/h/h758.md), and of the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md).

<a name="isaiah_7_5"></a>Isaiah 7:5

Because ['Ărām](../../strongs/h/h758.md), ['Ep̄rayim](../../strongs/h/h669.md), and the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md), have taken [ra'](../../strongs/h/h7451.md) [ya'ats](../../strongs/h/h3289.md) against thee, ['āmar](../../strongs/h/h559.md),

<a name="isaiah_7_6"></a>Isaiah 7:6

Let us [ʿālâ](../../strongs/h/h5927.md) against [Yehuwdah](../../strongs/h/h3063.md), and [qûṣ](../../strongs/h/h6973.md) it, and let us make a [bāqaʿ](../../strongs/h/h1234.md) therein for us, and set a [melek](../../strongs/h/h4428.md) in the [tavek](../../strongs/h/h8432.md) of it, even the [ben](../../strongs/h/h1121.md) of [ṭāḇ'ēl](../../strongs/h/h2870.md):

<a name="isaiah_7_7"></a>Isaiah 7:7

Thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), It shall not [quwm](../../strongs/h/h6965.md), neither shall it come to pass.

<a name="isaiah_7_8"></a>Isaiah 7:8

For the [ro'sh](../../strongs/h/h7218.md) of ['Ărām](../../strongs/h/h758.md) is [Dammeśeq](../../strong; s/h/h1834.md), and the [ro'sh](../../strongs/h/h7218.md) of [Dammeśeq](../../strongs/h/h1834.md) is [Rᵊṣîn](../../strongs/h/h7526.md) and within threescore and five years shall ['Ep̄rayim](../../strongs/h/h669.md) be [ḥāṯaṯ](../../strongs/h/h2865.md), that it be not an ['am](../../strongs/h/h5971.md).

<a name="isaiah_7_9"></a>Isaiah 7:9

And the [ro'sh](../../strongs/h/h7218.md) of ['Ep̄rayim](../../strongs/h/h669.md) is [Šōmrôn](../../strongs/h/h8111.md), and the [ro'sh](../../strongs/h/h7218.md) of [Šōmrôn](../../strongs/h/h8111.md) is [Rᵊmalyâû](../../strongs/h/h7425.md) [ben](../../strongs/h/h1121.md). If ye will not ['aman](../../strongs/h/h539.md), surely ye shall not be ['aman](../../strongs/h/h539.md).

<a name="isaiah_7_10"></a>Isaiah 7:10

Moreover [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) [yāsap̄](../../strongs/h/h3254.md) unto ['Āḥāz](../../strongs/h/h271.md), ['āmar](../../strongs/h/h559.md),

<a name="isaiah_7_11"></a>Isaiah 7:11

[sha'al](../../strongs/h/h7592.md) thee an ['ôṯ](../../strongs/h/h226.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md); [sha'al](../../strongs/h/h7592.md) it either in the [ʿāmaq](../../strongs/h/h6009.md), or in the [gāḇah](../../strongs/h/h1361.md) [maʿal](../../strongs/h/h4605.md).

<a name="isaiah_7_12"></a>Isaiah 7:12

But ['Āḥāz](../../strongs/h/h271.md) ['āmar](../../strongs/h/h559.md), I will not [sha'al](../../strongs/h/h7592.md), neither will I [nāsâ](../../strongs/h/h5254.md) ['ēṯ](../../strongs/h/h853.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_7_13"></a>Isaiah 7:13

And he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) ye now, O [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md); Is it a [mᵊʿaṭ](../../strongs/h/h4592.md) for you to [lā'â](../../strongs/h/h3811.md) ['enowsh](../../strongs/h/h582.md), but will ye [lā'â](../../strongs/h/h3811.md) my ['Elohiym](../../strongs/h/h430.md) also?

<a name="isaiah_7_14"></a>Isaiah 7:14

Therefore ['adonay](../../strongs/h/h136.md) himself shall [nathan](../../strongs/h/h5414.md) you an ['ôṯ](../../strongs/h/h226.md); Behold, an [ʿalmâ](../../strongs/h/h5959.md) shall [hārê](../../strongs/h/h2030.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and shall [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) ['el](../../strongs/h/h410.md) [ʿimmānû'ēl](../../strongs/h/h6005.md). [^1]

<a name="isaiah_7_15"></a>Isaiah 7:15

[ḥem'â](../../strongs/h/h2529.md) and [dĕbash](../../strongs/h/h1706.md) shall he ['akal](../../strongs/h/h398.md), that he may [yada'](../../strongs/h/h3045.md) to [mā'as](../../strongs/h/h3988.md) the [ra'](../../strongs/h/h7451.md), and [bāḥar](../../strongs/h/h977.md) the [towb](../../strongs/h/h2896.md).

<a name="isaiah_7_16"></a>Isaiah 7:16

For before the [naʿar](../../strongs/h/h5288.md) shall [yada'](../../strongs/h/h3045.md) to [mā'as](../../strongs/h/h3988.md) the [ra'](../../strongs/h/h7451.md), and [bāḥar](../../strongs/h/h977.md) the [towb](../../strongs/h/h2896.md), the ['ăḏāmâ](../../strongs/h/h127.md) that thou [qûṣ](../../strongs/h/h6973.md) shall be ['azab](../../strongs/h/h5800.md) of [šᵊnayim](../../strongs/h/h8147.md) her [melek](../../strongs/h/h4428.md).

<a name="isaiah_7_17"></a>Isaiah 7:17

[Yĕhovah](../../strongs/h/h3068.md) shall [bow'](../../strongs/h/h935.md) upon thee, and upon thy ['am](../../strongs/h/h5971.md), and upon thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), [yowm](../../strongs/h/h3117.md) that have not [bow'](../../strongs/h/h935.md), from the [yowm](../../strongs/h/h3117.md) that ['Ep̄rayim](../../strongs/h/h669.md) [cuwr](../../strongs/h/h5493.md) from [Yehuwdah](../../strongs/h/h3063.md); even the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_7_18"></a>Isaiah 7:18

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) shall [šāraq](../../strongs/h/h8319.md) for the [zᵊḇûḇ](../../strongs/h/h2070.md) that is in the [qāṣê](../../strongs/h/h7097.md) of the [yᵊ'ōr](../../strongs/h/h2975.md) of [Mitsrayim](../../strongs/h/h4714.md), and for the [dᵊḇôrâ](../../strongs/h/h1682.md) that is in the ['erets](../../strongs/h/h776.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_7_19"></a>Isaiah 7:19

And they shall come, and shall [nuwach](../../strongs/h/h5117.md) all of them in the [batâ](../../strongs/h/h1327.md) [nachal](../../strongs/h/h5158.md), and in the [nāqîq](../../strongs/h/h5357.md) of the [cela'](../../strongs/h/h5553.md), and upon all [naʿăṣûṣ](../../strongs/h/h5285.md), and upon all [nahălōl](../../strongs/h/h5097.md).

<a name="isaiah_7_20"></a>Isaiah 7:20

In the same [yowm](../../strongs/h/h3117.md) shall ['adonay](../../strongs/h/h136.md) [gālaḥ](../../strongs/h/h1548.md) with a [taʿar](../../strongs/h/h8593.md) that is [śᵊḵîrâ](../../strongs/h/h7917.md), namely, by them beyond the [nāhār](../../strongs/h/h5104.md), by the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), the [ro'sh](../../strongs/h/h7218.md), and the [śēʿār](../../strongs/h/h8181.md) of the [regel](../../strongs/h/h7272.md): and it shall also [sāp̄â](../../strongs/h/h5595.md) the [zēloō](../../strongs/g/g2206.md).

<a name="isaiah_7_21"></a>Isaiah 7:21

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that an ['iysh](../../strongs/h/h376.md) shall [ḥāyâ](../../strongs/h/h2421.md) a [bāqār](../../strongs/h/h1241.md) [ʿeḡlâ](../../strongs/h/h5697.md), and two [tso'n](../../strongs/h/h6629.md);

<a name="isaiah_7_22"></a>Isaiah 7:22

And it shall come to pass, for the [rōḇ](../../strongs/h/h7230.md) of [chalab](../../strongs/h/h2461.md) that they shall ['asah](../../strongs/h/h6213.md) he shall ['akal](../../strongs/h/h398.md) [ḥem'â](../../strongs/h/h2529.md): for [ḥem'â](../../strongs/h/h2529.md) and [dĕbash](../../strongs/h/h1706.md) shall every one ['akal](../../strongs/h/h398.md) that is [yāṯar](../../strongs/h/h3498.md) in the ['erets](../../strongs/h/h776.md).

<a name="isaiah_7_23"></a>Isaiah 7:23

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that every [maqowm](../../strongs/h/h4725.md) shall be, where there were a thousand [gep̄en](../../strongs/h/h1612.md) at a thousand [keceph](../../strongs/h/h3701.md), it shall even be for [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md).

<a name="isaiah_7_24"></a>Isaiah 7:24

With [chets](../../strongs/h/h2671.md) and with [qesheth](../../strongs/h/h7198.md) shall men [bow'](../../strongs/h/h935.md) thither; because all the ['erets](../../strongs/h/h776.md) shall become [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md).

<a name="isaiah_7_25"></a>Isaiah 7:25

And on all [har](../../strongs/h/h2022.md) that shall be [ʿāḏar](../../strongs/h/h5737.md) with the [maʿdēr](../../strongs/h/h4576.md), there shall not [bow'](../../strongs/h/h935.md) the [yir'ah](../../strongs/h/h3374.md) of [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md): but it shall be for the [mišlôaḥ](../../strongs/h/h4916.md) of [showr](../../strongs/h/h7794.md), and for the [mirmās](../../strongs/h/h4823.md) of [śê](../../strongs/h/h7716.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 6](isaiah_6.md) - [Isaiah 8](isaiah_8.md)

---

[^1]: [Isaiah 7:14 Commentary](../../commentary/isaiah/isaiah_7_commentary.md#isaiah_7_14)
