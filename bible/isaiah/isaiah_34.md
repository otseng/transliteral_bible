# [Isaiah 34](https://www.blueletterbible.org/kjv/isa/34/1/s_713001)

<a name="isaiah_34_1"></a>Isaiah 34:1

[qāraḇ](../../strongs/h/h7126.md), ye [gowy](../../strongs/h/h1471.md), to [shama'](../../strongs/h/h8085.md); and [qashab](../../strongs/h/h7181.md), ye [lĕom](../../strongs/h/h3816.md): let the ['erets](../../strongs/h/h776.md) [shama'](../../strongs/h/h8085.md), and all that is therein; the [tebel](../../strongs/h/h8398.md), and all things that come forth of it.

<a name="isaiah_34_2"></a>Isaiah 34:2

For the [qeṣep̄](../../strongs/h/h7110.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon all [gowy](../../strongs/h/h1471.md), and his [chemah](../../strongs/h/h2534.md) upon all their [tsaba'](../../strongs/h/h6635.md): he hath utterly [ḥāram](../../strongs/h/h2763.md) them, he hath [nathan](../../strongs/h/h5414.md) them to the [ṭeḇaḥ](../../strongs/h/h2874.md).

<a name="isaiah_34_3"></a>Isaiah 34:3

Their [ḥālāl](../../strongs/h/h2491.md) also shall be [shalak](../../strongs/h/h7993.md), and their [bᵊ'š](../../strongs/h/h889.md) shall [ʿālâ](../../strongs/h/h5927.md) out of their [peḡer](../../strongs/h/h6297.md), and the [har](../../strongs/h/h2022.md) shall be [māsas](../../strongs/h/h4549.md) with their [dam](../../strongs/h/h1818.md).

<a name="isaiah_34_4"></a>Isaiah 34:4

And all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) shall be [māqaq](../../strongs/h/h4743.md), and the [shamayim](../../strongs/h/h8064.md) shall be [gālal](../../strongs/h/h1556.md) as a [sēp̄er](../../strongs/h/h5612.md): and all their [tsaba'](../../strongs/h/h6635.md) shall [nabel](../../strongs/h/h5034.md), as the ['aleh](../../strongs/h/h5929.md) [nabel](../../strongs/h/h5034.md) from the [gep̄en](../../strongs/h/h1612.md), and as a [nabel](../../strongs/h/h5034.md) fig from the [tĕ'en](../../strongs/h/h8384.md).

<a name="isaiah_34_5"></a>Isaiah 34:5

For my [chereb](../../strongs/h/h2719.md) shall be [rāvâ](../../strongs/h/h7301.md) in [shamayim](../../strongs/h/h8064.md): behold, it shall come down upon ['Ĕḏōm](../../strongs/h/h123.md), and upon the ['am](../../strongs/h/h5971.md) of my [ḥērem](../../strongs/h/h2764.md), to [mishpat](../../strongs/h/h4941.md).

<a name="isaiah_34_6"></a>Isaiah 34:6

The [chereb](../../strongs/h/h2719.md) of [Yĕhovah](../../strongs/h/h3068.md) is [mālā'](../../strongs/h/h4390.md) with [dam](../../strongs/h/h1818.md), it is [dāšēn](../../strongs/h/h1878.md) with [cheleb](../../strongs/h/h2459.md), and with the [dam](../../strongs/h/h1818.md) of [kar](../../strongs/h/h3733.md) and [ʿatûḏ](../../strongs/h/h6260.md), with the [cheleb](../../strongs/h/h2459.md) of the [kilyah](../../strongs/h/h3629.md) of ['ayil](../../strongs/h/h352.md): for [Yĕhovah](../../strongs/h/h3068.md) hath a [zebach](../../strongs/h/h2077.md) in [Bāṣrâ](../../strongs/h/h1224.md), and a great [ṭeḇaḥ](../../strongs/h/h2874.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="isaiah_34_7"></a>Isaiah 34:7

And the [rᵊ'ēm](../../strongs/h/h7214.md) shall [yarad](../../strongs/h/h3381.md) with them, and the [par](../../strongs/h/h6499.md) with the ['abîr](../../strongs/h/h47.md); and their ['erets](../../strongs/h/h776.md) shall be [rāvâ](../../strongs/h/h7301.md) with [dam](../../strongs/h/h1818.md), and their ['aphar](../../strongs/h/h6083.md) [dāšēn](../../strongs/h/h1878.md) with [cheleb](../../strongs/h/h2459.md).

<a name="isaiah_34_8"></a>Isaiah 34:8

For it is the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāqām](../../strongs/h/h5359.md), and the [šānâ](../../strongs/h/h8141.md) of [šillûm](../../strongs/h/h7966.md) for the [rîḇ](../../strongs/h/h7379.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="isaiah_34_9"></a>Isaiah 34:9

And the [nachal](../../strongs/h/h5158.md) thereof shall be [hāp̄aḵ](../../strongs/h/h2015.md) into [zep̄eṯ](../../strongs/h/h2203.md), and the ['aphar](../../strongs/h/h6083.md) thereof into [gophriyth](../../strongs/h/h1614.md), and the ['erets](../../strongs/h/h776.md) thereof shall become [bāʿar](../../strongs/h/h1197.md) [zep̄eṯ](../../strongs/h/h2203.md).

<a name="isaiah_34_10"></a>Isaiah 34:10

It shall not be [kāḇâ](../../strongs/h/h3518.md) [layil](../../strongs/h/h3915.md) nor [yômām](../../strongs/h/h3119.md); the ['ashan](../../strongs/h/h6227.md) thereof shall [ʿālâ](../../strongs/h/h5927.md) ['owlam](../../strongs/h/h5769.md): from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md) it shall [ḥāraḇ](../../strongs/h/h2717.md); none shall ['abar](../../strongs/h/h5674.md) it [netsach](../../strongs/h/h5331.md) and [netsach](../../strongs/h/h5331.md).

<a name="isaiah_34_11"></a>Isaiah 34:11

But the [qā'aṯ](../../strongs/h/h6893.md) and the [qipōḏ](../../strongs/h/h7090.md) shall [yarash](../../strongs/h/h3423.md) it; the [yanšûp̄](../../strongs/h/h3244.md) also and the [ʿōrēḇ](../../strongs/h/h6158.md) shall [shakan](../../strongs/h/h7931.md) in it: and he shall [natah](../../strongs/h/h5186.md) upon it the [qāv](../../strongs/h/h6957.md) of [tohuw](../../strongs/h/h8414.md), and the ['eben](../../strongs/h/h68.md) of [bohuw](../../strongs/h/h922.md).

<a name="isaiah_34_12"></a>Isaiah 34:12

They shall [qara'](../../strongs/h/h7121.md) the [ḥōr](../../strongs/h/h2715.md) thereof to the [mᵊlûḵâ](../../strongs/h/h4410.md), but none shall be there, and all her [śar](../../strongs/h/h8269.md) shall be ['ep̄es](../../strongs/h/h657.md).

<a name="isaiah_34_13"></a>Isaiah 34:13

And [sîr](../../strongs/h/h5518.md) shall come up in her ['armôn](../../strongs/h/h759.md), [qimmôš](../../strongs/h/h7057.md) and [ḥôaḥ](../../strongs/h/h2336.md) in the [miḇṣār](../../strongs/h/h4013.md) thereof: and it shall be a [nāvê](../../strongs/h/h5116.md) of [tannîn](../../strongs/h/h8577.md), and a [ḥāṣîr](../../strongs/h/h2681.md) for [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md).

<a name="isaiah_34_14"></a>Isaiah 34:14

The wild beasts of the [ṣîyî](../../strongs/h/h6728.md) shall also meet with the wild beasts of the ['î](../../strongs/h/h338.md), and the [śāʿîr](../../strongs/h/h8163.md) shall [qara'](../../strongs/h/h7121.md) to his [rea'](../../strongs/h/h7453.md); the [lîlîṯ](../../strongs/h/h3917.md) also shall [rāḡaʿ](../../strongs/h/h7280.md) there, and find for herself a place of [mānôaḥ](../../strongs/h/h4494.md).

<a name="isaiah_34_15"></a>Isaiah 34:15

There shall the [qipôz](../../strongs/h/h7091.md) [qānan](../../strongs/h/h7077.md), and [mālaṭ](../../strongs/h/h4422.md), and [bāqaʿ](../../strongs/h/h1234.md), and [dāḡar](../../strongs/h/h1716.md) under her [ṣēl](../../strongs/h/h6738.md): there shall the [dayyâ](../../strongs/h/h1772.md) also be [qāḇaṣ](../../strongs/h/h6908.md), every one with her [rᵊʿûṯ](../../strongs/h/h7468.md).

<a name="isaiah_34_16"></a>Isaiah 34:16

[darash](../../strongs/h/h1875.md) ye of the [sēp̄er](../../strongs/h/h5612.md) of [Yĕhovah](../../strongs/h/h3068.md), and [qara'](../../strongs/h/h7121.md): no one of these shall [ʿāḏar](../../strongs/h/h5737.md), none shall want her [rᵊʿûṯ](../../strongs/h/h7468.md): for my [peh](../../strongs/h/h6310.md) it hath [tsavah](../../strongs/h/h6680.md), and his [ruwach](../../strongs/h/h7307.md) it hath [qāḇaṣ](../../strongs/h/h6908.md) them.

<a name="isaiah_34_17"></a>Isaiah 34:17

And he hath [naphal](../../strongs/h/h5307.md) the [gôrāl](../../strongs/h/h1486.md) for them, and his [yad](../../strongs/h/h3027.md) hath [chalaq](../../strongs/h/h2505.md) it unto them by [qāv](../../strongs/h/h6957.md): they shall [yarash](../../strongs/h/h3423.md) it for ['owlam](../../strongs/h/h5769.md), from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md) shall they [shakan](../../strongs/h/h7931.md) therein.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 33](isaiah_33.md) - [Isaiah 35](isaiah_35.md)