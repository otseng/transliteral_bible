# [Isaiah 27](https://www.blueletterbible.org/kjv/isa/27/1/s_706001)

<a name="isaiah_27_1"></a>Isaiah 27:1

In that [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) with his [qāšê](../../strongs/h/h7186.md) and [gadowl](../../strongs/h/h1419.md) and [ḥāzāq](../../strongs/h/h2389.md) [chereb](../../strongs/h/h2719.md) shall [paqad](../../strongs/h/h6485.md) [livyāṯān](../../strongs/h/h3882.md) the [bāriaḥ](../../strongs/h/h1281.md) [nachash](../../strongs/h/h5175.md), even [livyāṯān](../../strongs/h/h3882.md) that [ʿăqallāṯôn](../../strongs/h/h6129.md) [nachash](../../strongs/h/h5175.md); and he shall [harag](../../strongs/h/h2026.md) the [tannîn](../../strongs/h/h8577.md) that is in the [yam](../../strongs/h/h3220.md).

<a name="isaiah_27_2"></a>Isaiah 27:2

In that [yowm](../../strongs/h/h3117.md) [ʿānâ](../../strongs/h/h6031.md) ye unto her, A [kerem](../../strongs/h/h3754.md) of [ḥemer](../../strongs/h/h2561.md) [ḥemeḏ](../../strongs/h/h2531.md).

<a name="isaiah_27_3"></a>Isaiah 27:3

I [Yĕhovah](../../strongs/h/h3068.md) do [nāṣar](../../strongs/h/h5341.md) it; I will [šāqâ](../../strongs/h/h8248.md) it every moment: lest any [paqad](../../strongs/h/h6485.md) it, I will [nāṣar](../../strongs/h/h5341.md) it [layil](../../strongs/h/h3915.md) and [yowm](../../strongs/h/h3117.md).

<a name="isaiah_27_4"></a>Isaiah 27:4

[chemah](../../strongs/h/h2534.md) is not in me: who would [nathan](../../strongs/h/h5414.md) the [šāmîr](../../strongs/h/h8068.md) and [šayiṯ](../../strongs/h/h7898.md) against me in [milḥāmâ](../../strongs/h/h4421.md)? I would [pāśaʿ](../../strongs/h/h6585.md) through them, I would [ṣûṯ](../../strongs/h/h6702.md) them together.

<a name="isaiah_27_5"></a>Isaiah 27:5

Or let him [ḥāzaq](../../strongs/h/h2388.md) of my [māʿôz](../../strongs/h/h4581.md), that he may ['asah](../../strongs/h/h6213.md) [shalowm](../../strongs/h/h7965.md) with me; and he shall ['asah](../../strongs/h/h6213.md) [shalowm](../../strongs/h/h7965.md) with me.

<a name="isaiah_27_6"></a>Isaiah 27:6

He shall cause them that [bow'](../../strongs/h/h935.md) of [Ya'aqob](../../strongs/h/h3290.md) to [šērēš](../../strongs/h/h8327.md): [Yisra'el](../../strongs/h/h3478.md) shall [tsuwts](../../strongs/h/h6692.md) and [pāraḥ](../../strongs/h/h6524.md), and [mālā'](../../strongs/h/h4390.md) the [paniym](../../strongs/h/h6440.md) of the [tebel](../../strongs/h/h8398.md) with [tᵊnûḇâ](../../strongs/h/h8570.md).

<a name="isaiah_27_7"></a>Isaiah 27:7

Hath he [nakah](../../strongs/h/h5221.md) him, as he [makâ](../../strongs/h/h4347.md) those that [nakah](../../strongs/h/h5221.md) him? or is he [harag](../../strongs/h/h2026.md) according to the [hereḡ](../../strongs/h/h2027.md) of them that are [harag](../../strongs/h/h2026.md) by him?

<a name="isaiah_27_8"></a>Isaiah 27:8

In [sa'ssᵊ'â](../../strongs/h/h5432.md), when it [shalach](../../strongs/h/h7971.md), thou wilt [riyb](../../strongs/h/h7378.md) with it: he [hāḡâ](../../strongs/h/h1898.md) his [qāšê](../../strongs/h/h7186.md) [ruwach](../../strongs/h/h7307.md) in the [yowm](../../strongs/h/h3117.md) of the [qāḏîm](../../strongs/h/h6921.md).

<a name="isaiah_27_9"></a>Isaiah 27:9

By this therefore shall the ['avon](../../strongs/h/h5771.md) of [Ya'aqob](../../strongs/h/h3290.md) be [kāp̄ar](../../strongs/h/h3722.md); and this is all the [pĕriy](../../strongs/h/h6529.md) to [cuwr](../../strongs/h/h5493.md) his [chatta'ath](../../strongs/h/h2403.md); when he maketh all the ['eben](../../strongs/h/h68.md) of the [mizbeach](../../strongs/h/h4196.md) as [gir](../../strongs/h/h1615.md) that are [naphats](../../strongs/h/h5310.md), the ['ăšērâ](../../strongs/h/h842.md) and [ḥammān](../../strongs/h/h2553.md) shall not [quwm](../../strongs/h/h6965.md).

<a name="isaiah_27_10"></a>Isaiah 27:10

Yet the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) shall be [bāḏāḏ](../../strongs/h/h910.md), and the [nāvê](../../strongs/h/h5116.md) [shalach](../../strongs/h/h7971.md), and ['azab](../../strongs/h/h5800.md) like a [midbar](../../strongs/h/h4057.md): there shall the [ʿēḡel](../../strongs/h/h5695.md) [ra'ah](../../strongs/h/h7462.md), and there shall he [rāḇaṣ](../../strongs/h/h7257.md), and [kalah](../../strongs/h/h3615.md) the [sᵊʿîp̄](../../strongs/h/h5585.md) thereof.

<a name="isaiah_27_11"></a>Isaiah 27:11

When the [qāṣîr](../../strongs/h/h7105.md) thereof are [yāḇēš](../../strongs/h/h3001.md), they shall be [shabar](../../strongs/h/h7665.md): the ['ishshah](../../strongs/h/h802.md) come, and set them on ['owr](../../strongs/h/h215.md): for it is an ['am](../../strongs/h/h5971.md) of no [bînâ](../../strongs/h/h998.md): therefore he that ['asah](../../strongs/h/h6213.md) them will not have [racham](../../strongs/h/h7355.md) on them, and he that [yāṣar](../../strongs/h/h3335.md) them will shew them no [chanan](../../strongs/h/h2603.md).

<a name="isaiah_27_12"></a>Isaiah 27:12

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) shall [ḥāḇaṭ](../../strongs/h/h2251.md) from the [šibōleṯ](../../strongs/h/h7641.md) of the [nāhār](../../strongs/h/h5104.md) unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md), and ye shall be [lāqaṭ](../../strongs/h/h3950.md) one by one, O ye [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_27_13"></a>Isaiah 27:13

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that the [gadowl](../../strongs/h/h1419.md) [šôp̄ār](../../strongs/h/h7782.md) shall be [tāqaʿ](../../strongs/h/h8628.md), and they shall [bow'](../../strongs/h/h935.md) which were ready to ['abad](../../strongs/h/h6.md) in the ['erets](../../strongs/h/h776.md) of ['Aššûr](../../strongs/h/h804.md), and the [nāḏaḥ](../../strongs/h/h5080.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and shall [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md) in the [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 26](isaiah_26.md) - [Isaiah 28](isaiah_28.md)