# [Isaiah 17](https://www.blueletterbible.org/kjv/isa/17/1/s_696001)

<a name="isaiah_17_1"></a>Isaiah 17:1

The [maśśā'](../../strongs/h/h4853.md) of [Dammeśeq](../../strongs/h/h1834.md). Behold, [Dammeśeq](../../strongs/h/h1834.md) is [cuwr](../../strongs/h/h5493.md) from being an [ʿîr](../../strongs/h/h5892.md), and it shall be a [mapālâ](../../strongs/h/h4654.md) [mᵊʿî](../../strongs/h/h4596.md).

<a name="isaiah_17_2"></a>Isaiah 17:2

The [ʿîr](../../strongs/h/h5892.md) of [ʿĂrôʿēr](../../strongs/h/h6177.md) are ['azab](../../strongs/h/h5800.md): they shall be for [ʿēḏer](../../strongs/h/h5739.md), which shall [rāḇaṣ](../../strongs/h/h7257.md), and none shall make them [ḥārēḏ](../../strongs/h/h2729.md).

<a name="isaiah_17_3"></a>Isaiah 17:3

The [miḇṣār](../../strongs/h/h4013.md) also shall [shabath](../../strongs/h/h7673.md) from ['Ep̄rayim](../../strongs/h/h669.md), and the [mamlāḵâ](../../strongs/h/h4467.md) from [Dammeśeq](../../strongs/h/h1834.md), and the [šᵊ'ār](../../strongs/h/h7605.md) of ['Ărām](../../strongs/h/h758.md): they shall be as the [kabowd](../../strongs/h/h3519.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), saith [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_17_4"></a>Isaiah 17:4

And in that [yowm](../../strongs/h/h3117.md) it shall come to pass, that the [kabowd](../../strongs/h/h3519.md) of [Ya'aqob](../../strongs/h/h3290.md) shall be made [dālal](../../strongs/h/h1809.md), and the [mišmān](../../strongs/h/h4924.md) of his [basar](../../strongs/h/h1320.md) shall [rāzâ](../../strongs/h/h7329.md).

<a name="isaiah_17_5"></a>Isaiah 17:5

And it shall be as when the [qāṣîr](../../strongs/h/h7105.md) gathereth the [qāmâ](../../strongs/h/h7054.md), and [qāṣar](../../strongs/h/h7114.md) the [šibōleṯ](../../strongs/h/h7641.md) with his [zerowa'](../../strongs/h/h2220.md); and it shall be as he that [lāqaṭ](../../strongs/h/h3950.md) [šibōleṯ](../../strongs/h/h7641.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="isaiah_17_6"></a>Isaiah 17:6

Yet [ʿōlēlôṯ](../../strongs/h/h5955.md) shall be [šā'ar](../../strongs/h/h7604.md) in it, as the [nōqep̄](../../strongs/h/h5363.md) of a [zayiṯ](../../strongs/h/h2132.md), two or three [gargar](../../strongs/h/h1620.md) in the [ro'sh](../../strongs/h/h7218.md) of the ['āmîr](../../strongs/h/h534.md), four or five in the [parah](../../strongs/h/h6509.md) [sᵊʿîp̄](../../strongs/h/h5585.md) thereof, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_17_7"></a>Isaiah 17:7

At that [yowm](../../strongs/h/h3117.md) shall an ['adam](../../strongs/h/h120.md) look to his ['asah](../../strongs/h/h6213.md), and his ['ayin](../../strongs/h/h5869.md) shall have [ra'ah](../../strongs/h/h7200.md) to the [qadowsh](../../strongs/h/h6918.md) One of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_17_8"></a>Isaiah 17:8

And he shall not [šāʿâ](../../strongs/h/h8159.md) to the [mizbeach](../../strongs/h/h4196.md), the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md), neither shall [ra'ah](../../strongs/h/h7200.md) that which his ['etsba'](../../strongs/h/h676.md) have ['asah](../../strongs/h/h6213.md), either the ['ăšērâ](../../strongs/h/h842.md), or the [ḥammān](../../strongs/h/h2553.md).

<a name="isaiah_17_9"></a>Isaiah 17:9

In that [yowm](../../strongs/h/h3117.md) shall his [māʿôz](../../strongs/h/h4581.md) [ʿîr](../../strongs/h/h5892.md) be as an ['azab](../../strongs/h/h5800.md) [ḥōreš](../../strongs/h/h2793.md), and an ['āmîr](../../strongs/h/h534.md), which they ['azab](../../strongs/h/h5800.md) because of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and there shall be [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="isaiah_17_10"></a>Isaiah 17:10

Because thou hast [shakach](../../strongs/h/h7911.md) the ['Elohiym](../../strongs/h/h430.md) of thy [yesha'](../../strongs/h/h3468.md), and hast not been [zakar](../../strongs/h/h2142.md) of the [tsuwr](../../strongs/h/h6697.md) of thy [māʿôz](../../strongs/h/h4581.md), therefore shalt thou [nāṭaʿ](../../strongs/h/h5193.md) [naʿămān](../../strongs/h/h5282.md) [neṭaʿ](../../strongs/h/h5194.md), and shalt set it with [zûr](../../strongs/h/h2114.md) [zᵊmôrâ](../../strongs/h/h2156.md):

<a name="isaiah_17_11"></a>Isaiah 17:11

In the [yowm](../../strongs/h/h3117.md) shalt thou make thy [neṭaʿ](../../strongs/h/h5194.md) to [śûḡ](../../strongs/h/h7735.md), and in the [boqer](../../strongs/h/h1242.md) shalt thou make thy [zera'](../../strongs/h/h2233.md) to [pāraḥ](../../strongs/h/h6524.md): but the [qāṣîr](../../strongs/h/h7105.md) shall be a [nēḏ](../../strongs/h/h5067.md) in the [yowm](../../strongs/h/h3117.md) of [ḥālâ](../../strongs/h/h2470.md) and of ['anash](../../strongs/h/h605.md) [kᵊ'ēḇ](../../strongs/h/h3511.md).

<a name="isaiah_17_12"></a>Isaiah 17:12

[hôy](../../strongs/h/h1945.md) to the [hāmôn](../../strongs/h/h1995.md) of many ['am](../../strongs/h/h5971.md), which make a [hāmâ](../../strongs/h/h1993.md) like the [hāmâ](../../strongs/h/h1993.md) of the [yam](../../strongs/h/h3220.md); and to the [shā'ôn](../../strongs/h/h7588.md) of [lĕom](../../strongs/h/h3816.md), that make a [šā'â](../../strongs/h/h7582.md) of mighty [mayim](../../strongs/h/h4325.md)!

<a name="isaiah_17_13"></a>Isaiah 17:13

The [lĕom](../../strongs/h/h3816.md) shall [šā'â](../../strongs/h/h7582.md) like the [shā'ôn](../../strongs/h/h7588.md) of many [mayim](../../strongs/h/h4325.md): but God shall [gāʿar](../../strongs/h/h1605.md) them, and they shall [nûs](../../strongs/h/h5127.md) far off, and shall be [radaph](../../strongs/h/h7291.md) as the [mots](../../strongs/h/h4671.md) of the [har](../../strongs/h/h2022.md) before the [ruwach](../../strongs/h/h7307.md), and like a [galgal](../../strongs/h/h1534.md) before the [sûp̄â](../../strongs/h/h5492.md).

<a name="isaiah_17_14"></a>Isaiah 17:14

And behold at [ʿēṯ](../../strongs/h/h6256.md) ['ereb](../../strongs/h/h6153.md) [ballāhâ](../../strongs/h/h1091.md); and before the [boqer](../../strongs/h/h1242.md) he is not. This is the [cheleq](../../strongs/h/h2506.md) of them that [šāsâ](../../strongs/h/h8154.md) us, and the [gôrāl](../../strongs/h/h1486.md) of them that [bāzaz](../../strongs/h/h962.md) us.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 16](isaiah_16.md) - [Isaiah 18](isaiah_18.md)