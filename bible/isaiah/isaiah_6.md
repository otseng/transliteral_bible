# [Isaiah 6](https://www.blueletterbible.org/kjv/isa/6/1/s_685001)

<a name="Isaiah_6_1"></a>Isaiah 6:1

In the [šānâ](../../strongs/h/h8141.md) that [melek](../../strongs/h/h4428.md) ['Uzziyah](../../strongs/h/h5818.md) [maveth](../../strongs/h/h4194.md) I [ra'ah](../../strongs/h/h7200.md) also ['adonay](../../strongs/h/h136.md) sitting upon a [kicce'](../../strongs/h/h3678.md), [ruwm](../../strongs/h/h7311.md) and [nasa'](../../strongs/h/h5375.md), and his [shuwl](../../strongs/h/h7757.md) filled the [heykal](../../strongs/h/h1964.md).

<a name="Isaiah_6_2"></a>Isaiah 6:2

[maʿal](../../strongs/h/h4605.md) it ['amad](../../strongs/h/h5975.md) the [saraph](../../strongs/h/h8314.md): each one had six [kanaph](../../strongs/h/h3671.md); with twain he [kāsâ](../../strongs/h/h3680.md) his [paniym](../../strongs/h/h6440.md), and with twain he [kāsâ](../../strongs/h/h3680.md) his [regel](../../strongs/h/h7272.md), and with twain he did ['uwph](../../strongs/h/h5774.md).

<a name="Isaiah_6_3"></a>Isaiah 6:3

And one [qara'](../../strongs/h/h7121.md) unto another, and ['āmar](../../strongs/h/h559.md), [qadowsh](../../strongs/h/h6918.md), [qadowsh](../../strongs/h/h6918.md), [qadowsh](../../strongs/h/h6918.md), is [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): the ['erets](../../strongs/h/h776.md) is [mᵊlō'](../../strongs/h/h4393.md) of his [kabowd](../../strongs/h/h3519.md).

<a name="Isaiah_6_4"></a>Isaiah 6:4

And the ['ammâ](../../strongs/h/h520.md) of the [caph](../../strongs/h/h5592.md) [nûaʿ](../../strongs/h/h5128.md) at the [qowl](../../strongs/h/h6963.md) of him that [qara'](../../strongs/h/h7121.md), and the [bayith](../../strongs/h/h1004.md) was [mālā'](../../strongs/h/h4390.md) with ['ashan](../../strongs/h/h6227.md).

<a name="Isaiah_6_5"></a>Isaiah 6:5

Then ['āmar](../../strongs/h/h559.md) I, ['owy](../../strongs/h/h188.md) [damah](../../strongs/h/h1820.md); because I am an ['iysh](../../strongs/h/h376.md) of [tame'](../../strongs/h/h2931.md) [saphah](../../strongs/h/h8193.md), and I [yashab](../../strongs/h/h3427.md) in the [tavek](../../strongs/h/h8432.md) of an ['am](../../strongs/h/h5971.md) of [tame'](../../strongs/h/h2931.md) [saphah](../../strongs/h/h8193.md): for mine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) the [melek](../../strongs/h/h4428.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="Isaiah_6_6"></a>Isaiah 6:6

Then ['uwph](../../strongs/h/h5774.md) one of the [saraph](../../strongs/h/h8314.md) unto me, having a [ritspah](../../strongs/h/h7531.md) in his [yad](../../strongs/h/h3027.md), which he had [laqach](../../strongs/h/h3947.md) with the [malqāḥayim](../../strongs/h/h4457.md) from off the [mizbeach](../../strongs/h/h4196.md):

<a name="Isaiah_6_7"></a>Isaiah 6:7

And he [naga'](../../strongs/h/h5060.md) it upon my [peh](../../strongs/h/h6310.md), and ['āmar](../../strongs/h/h559.md), Lo, this hath [naga'](../../strongs/h/h5060.md) thy [saphah](../../strongs/h/h8193.md); and thine ['avon](../../strongs/h/h5771.md) is [cuwr](../../strongs/h/h5493.md), and thy [chatta'ath](../../strongs/h/h2403.md) [kāp̄ar](../../strongs/h/h3722.md).

<a name="Isaiah_6_8"></a>Isaiah 6:8

Also I [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of ['adonay](../../strongs/h/h136.md), ['āmar](../../strongs/h/h559.md), Whom shall I [shalach](../../strongs/h/h7971.md), and who will [yālaḵ](../../strongs/h/h3212.md) for us? Then ['āmar](../../strongs/h/h559.md) I, Here am I; [shalach](../../strongs/h/h7971.md) me.

<a name="Isaiah_6_9"></a>Isaiah 6:9

And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), and ['āmar](../../strongs/h/h559.md) this ['am](../../strongs/h/h5971.md), [shama'](../../strongs/h/h8085.md) ye indeed, but [bîn](../../strongs/h/h995.md) not; and [ra'ah](../../strongs/h/h7200.md) ye indeed, but [yada'](../../strongs/h/h3045.md) not.

<a name="Isaiah_6_10"></a>Isaiah 6:10

Make the [leb](../../strongs/h/h3820.md) of this ['am](../../strongs/h/h5971.md) [šāman](../../strongs/h/h8080.md), and make their ['ozen](../../strongs/h/h241.md) [kabad](../../strongs/h/h3513.md), and [šāʿaʿ](../../strongs/h/h8173.md) their ['ayin](../../strongs/h/h5869.md); lest they [ra'ah](../../strongs/h/h7200.md) with their ['ayin](../../strongs/h/h5869.md), and [shama'](../../strongs/h/h8085.md) with their ['ozen](../../strongs/h/h241.md), and [bîn](../../strongs/h/h995.md) with their [lebab](../../strongs/h/h3824.md), and [shuwb](../../strongs/h/h7725.md), and be [rapha'](../../strongs/h/h7495.md).

<a name="Isaiah_6_11"></a>Isaiah 6:11

Then ['āmar](../../strongs/h/h559.md) I, ['adonay](../../strongs/h/h136.md), how long? And he ['āmar](../../strongs/h/h559.md), Until the [ʿîr](../../strongs/h/h5892.md) be [šā'â](../../strongs/h/h7582.md) without [yashab](../../strongs/h/h3427.md), and the [bayith](../../strongs/h/h1004.md) without ['adam](../../strongs/h/h120.md), and the ['ăḏāmâ](../../strongs/h/h127.md) be [šᵊmāmâ](../../strongs/h/h8077.md) [šā'â](../../strongs/h/h7582.md),

<a name="Isaiah_6_12"></a>Isaiah 6:12

And [Yĕhovah](../../strongs/h/h3068.md) have [rachaq](../../strongs/h/h7368.md) ['adam](../../strongs/h/h120.md), and there be a [rab](../../strongs/h/h7227.md) [ʿăzûḇâ](../../strongs/h/h5805.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md).

<a name="Isaiah_6_13"></a>Isaiah 6:13

But yet in it shall be a [ʿăśîrî](../../strongs/h/h6224.md), and it shall [shuwb](../../strongs/h/h7725.md), and shall be [bāʿar](../../strongs/h/h1197.md): as an ['ēlâ](../../strongs/h/h424.md), and as an ['allôn](../../strongs/h/h437.md), whose [maṣṣeḇeṯ](../../strongs/h/h4678.md) is in them, when they [šalleḵeṯ](../../strongs/h/h7995.md) their leaves: so the [qodesh](../../strongs/h/h6944.md) [zera'](../../strongs/h/h2233.md) shall be the [maṣṣeḇeṯ](../../strongs/h/h4678.md) thereof.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 5](isaiah_5.md) - [Isaiah 7](isaiah_7.md)