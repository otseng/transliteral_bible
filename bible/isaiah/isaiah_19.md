# [Isaiah 19](https://www.blueletterbible.org/kjv/isa/19/1/s_698001)

<a name="isaiah_19_1"></a>Isaiah 19:1

The [maśśā'](../../strongs/h/h4853.md) of [Mitsrayim](../../strongs/h/h4714.md). Behold, [Yĕhovah](../../strongs/h/h3068.md) [rāḵaḇ](../../strongs/h/h7392.md) upon a [qal](../../strongs/h/h7031.md) ['ab](../../strongs/h/h5645.md), and shall come into [Mitsrayim](../../strongs/h/h4714.md): and the ['ĕlîl](../../strongs/h/h457.md) of [Mitsrayim](../../strongs/h/h4714.md) shall be [nûaʿ](../../strongs/h/h5128.md) at his [paniym](../../strongs/h/h6440.md), and the [lebab](../../strongs/h/h3824.md) of [Mitsrayim](../../strongs/h/h4714.md) shall [māsas](../../strongs/h/h4549.md) in the midst of it.

<a name="isaiah_19_2"></a>Isaiah 19:2

And I will set the [Mitsrayim](../../strongs/h/h4714.md) against the [Mitsrayim](../../strongs/h/h4714.md): and they shall [lāḥam](../../strongs/h/h3898.md) every one against his ['ach](../../strongs/h/h251.md), and every one against his [rea'](../../strongs/h/h7453.md); [ʿîr](../../strongs/h/h5892.md) against [ʿîr](../../strongs/h/h5892.md), and [mamlāḵâ](../../strongs/h/h4467.md) against [mamlāḵâ](../../strongs/h/h4467.md).

<a name="isaiah_19_3"></a>Isaiah 19:3

And the [ruwach](../../strongs/h/h7307.md) of [Mitsrayim](../../strongs/h/h4714.md) shall [bāqaq](../../strongs/h/h1238.md) in the midst thereof; and I will [bālaʿ](../../strongs/h/h1104.md) the ['etsah](../../strongs/h/h6098.md) thereof: and they shall [darash](../../strongs/h/h1875.md) to the ['ĕlîl](../../strongs/h/h457.md), and to the ['aṭ](../../strongs/h/h328.md), and to them that have ['ôḇ](../../strongs/h/h178.md), and to the [yiḏʿōnî](../../strongs/h/h3049.md).

<a name="isaiah_19_4"></a>Isaiah 19:4

And the [Mitsrayim](../../strongs/h/h4714.md) will I [sāḵar](../../strongs/h/h5534.md) into the [yad](../../strongs/h/h3027.md) of a [qāšê](../../strongs/h/h7186.md) ['adown](../../strongs/h/h113.md); and an ['az](../../strongs/h/h5794.md) [melek](../../strongs/h/h4428.md) shall [mashal](../../strongs/h/h4910.md) over them, [nᵊ'um](../../strongs/h/h5002.md) ['adown](../../strongs/h/h113.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_19_5"></a>Isaiah 19:5

And the [mayim](../../strongs/h/h4325.md) shall [nāšaṯ](../../strongs/h/h5405.md) from the [yam](../../strongs/h/h3220.md), and the [nāhār](../../strongs/h/h5104.md) shall be [ḥāraḇ](../../strongs/h/h2717.md) and [yāḇēš](../../strongs/h/h3001.md).

<a name="isaiah_19_6"></a>Isaiah 19:6

And they shall [zānaḥ](../../strongs/h/h2186.md) the [nāhār](../../strongs/h/h5104.md) [zānaḥ](../../strongs/h/h2186.md); and the [yᵊ'ōr](../../strongs/h/h2975.md) of [māṣôr](../../strongs/h/h4693.md) shall be [dālal](../../strongs/h/h1809.md) and [ḥāraḇ](../../strongs/h/h2717.md): the [qānê](../../strongs/h/h7070.md) and [sûp̄](../../strongs/h/h5488.md) shall [qāmal](../../strongs/h/h7060.md).

<a name="isaiah_19_7"></a>Isaiah 19:7

The [ʿārâ](../../strongs/h/h6169.md) by the [yᵊ'ōr](../../strongs/h/h2975.md), by the [peh](../../strongs/h/h6310.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), and every [mizrāʿ](../../strongs/h/h4218.md) by the [yᵊ'ōr](../../strongs/h/h2975.md), shall [yāḇēš](../../strongs/h/h3001.md), be [nāḏap̄](../../strongs/h/h5086.md), and be no more.

<a name="isaiah_19_8"></a>Isaiah 19:8

The [dayyāḡ](../../strongs/h/h1771.md) also shall ['ānâ](../../strongs/h/h578.md), and all they that [shalak](../../strongs/h/h7993.md) [ḥakâ](../../strongs/h/h2443.md) into the [yᵊ'ōr](../../strongs/h/h2975.md) shall ['āḇal](../../strongs/h/h56.md), and they that [pāraś](../../strongs/h/h6566.md) [miḵmōreṯ](../../strongs/h/h4365.md) upon the [mayim](../../strongs/h/h4325.md) shall ['āmal](../../strongs/h/h535.md).

<a name="isaiah_19_9"></a>Isaiah 19:9

Moreover they that ['abad](../../strongs/h/h5647.md) in [śᵊrîqâ](../../strongs/h/h8305.md) [pēšeṯ](../../strongs/h/h6593.md), and they that weave [ḥôr](../../strongs/h/h2355.md), shall be [buwsh](../../strongs/h/h954.md).

<a name="isaiah_19_10"></a>Isaiah 19:10

And they shall be [dāḵā'](../../strongs/h/h1792.md) in the [shathah](../../strongs/h/h8356.md) thereof, all that make [śeḵer](../../strongs/h/h7938.md) and ['āḡēm](../../strongs/h/h99.md) for [nephesh](../../strongs/h/h5315.md).

<a name="isaiah_19_11"></a>Isaiah 19:11

Surely the [śar](../../strongs/h/h8269.md) of [Ṣōʿan](../../strongs/h/h6814.md) are ['ĕvîl](../../strongs/h/h191.md), the ['etsah](../../strongs/h/h6098.md) of the [ḥāḵām](../../strongs/h/h2450.md) [ya'ats](../../strongs/h/h3289.md) of [Parʿô](../../strongs/h/h6547.md) is become [bāʿar](../../strongs/h/h1197.md): how ['āmar](../../strongs/h/h559.md) ye unto [Parʿô](../../strongs/h/h6547.md), I am the [ben](../../strongs/h/h1121.md) of the [ḥāḵām](../../strongs/h/h2450.md), the [ben](../../strongs/h/h1121.md) of [qeḏem](../../strongs/h/h6924.md) [melek](../../strongs/h/h4428.md)?

<a name="isaiah_19_12"></a>Isaiah 19:12

Where are they? where are thy [ḥāḵām](../../strongs/h/h2450.md) men? and let them [nāḡaḏ](../../strongs/h/h5046.md) thee now, and let them [yada'](../../strongs/h/h3045.md) what [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [ya'ats](../../strongs/h/h3289.md) upon [Mitsrayim](../../strongs/h/h4714.md).

<a name="isaiah_19_13"></a>Isaiah 19:13

The [śar](../../strongs/h/h8269.md) of [Ṣōʿan](../../strongs/h/h6814.md) are become [yā'al](../../strongs/h/h2973.md), the [śar](../../strongs/h/h8269.md) of [Nōp̄](../../strongs/h/h5297.md) are [nasha'](../../strongs/h/h5377.md); they have also [tāʿâ](../../strongs/h/h8582.md) [Mitsrayim](../../strongs/h/h4714.md), even they that are the [pinnâ](../../strongs/h/h6438.md) of the [shebet](../../strongs/h/h7626.md) thereof.

<a name="isaiah_19_14"></a>Isaiah 19:14

[Yĕhovah](../../strongs/h/h3068.md) hath [māsaḵ](../../strongs/h/h4537.md) a [ʿivʿîm](../../strongs/h/h5773.md) [ruwach](../../strongs/h/h7307.md) in the [qereḇ](../../strongs/h/h7130.md) thereof: and they have caused [Mitsrayim](../../strongs/h/h4714.md) to [tāʿâ](../../strongs/h/h8582.md) in every [ma'aseh](../../strongs/h/h4639.md) thereof, as a [šikôr](../../strongs/h/h7910.md) man [tāʿâ](../../strongs/h/h8582.md) in his [qē'](../../strongs/h/h6892.md).

<a name="isaiah_19_15"></a>Isaiah 19:15

Neither shall there be any [ma'aseh](../../strongs/h/h4639.md) for [Mitsrayim](../../strongs/h/h4714.md), which the [ro'sh](../../strongs/h/h7218.md) or [zānāḇ](../../strongs/h/h2180.md), [kipâ](../../strongs/h/h3712.md) or ['aḡmôn](../../strongs/h/h100.md), may do.

<a name="isaiah_19_16"></a>Isaiah 19:16

In that [yowm](../../strongs/h/h3117.md) shall [Mitsrayim](../../strongs/h/h4714.md) be like unto ['ishshah](../../strongs/h/h802.md): and it shall be [ḥārēḏ](../../strongs/h/h2729.md) and [pachad](../../strongs/h/h6342.md) because of the [tᵊnûp̄â](../../strongs/h/h8573.md) of the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), which he [nûp̄](../../strongs/h/h5130.md) over it.

<a name="isaiah_19_17"></a>Isaiah 19:17

And the ['ăḏāmâ](../../strongs/h/h127.md) of [Yehuwdah](../../strongs/h/h3063.md) shall be a [ḥāgā'](../../strongs/h/h2283.md) unto [Mitsrayim](../../strongs/h/h4714.md), every one that [zakar](../../strongs/h/h2142.md) thereof shall be [pachad](../../strongs/h/h6342.md) in himself, because of the ['etsah](../../strongs/h/h6098.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), which he hath [ya'ats](../../strongs/h/h3289.md) against it.

<a name="isaiah_19_18"></a>Isaiah 19:18

In that [yowm](../../strongs/h/h3117.md) shall five [ʿîr](../../strongs/h/h5892.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) [dabar](../../strongs/h/h1696.md) the [saphah](../../strongs/h/h8193.md) of [Kĕna'an](../../strongs/h/h3667.md), and [shaba'](../../strongs/h/h7650.md) to [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); one shall be ['āmar](../../strongs/h/h559.md), The [ʿîr](../../strongs/h/h5892.md) of [heres](../../strongs/h/h2041.md).

<a name="isaiah_19_19"></a>Isaiah 19:19

In that [yowm](../../strongs/h/h3117.md) shall there be a [mizbeach](../../strongs/h/h4196.md) to [Yĕhovah](../../strongs/h/h3068.md) in the [tavek](../../strongs/h/h8432.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and a [maṣṣēḇâ](../../strongs/h/h4676.md) at the [gᵊḇûl](../../strongs/h/h1366.md) thereof to [Yĕhovah](../../strongs/h/h3068.md).

<a name="isaiah_19_20"></a>Isaiah 19:20

And it shall be for an ['ôṯ](../../strongs/h/h226.md) and for an ['ed](../../strongs/h/h5707.md) unto [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): for they shall [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md) because of the [lāḥaṣ](../../strongs/h/h3905.md), and he shall [shalach](../../strongs/h/h7971.md) them a [yasha'](../../strongs/h/h3467.md), and a [rab](../../strongs/h/h7227.md), and he shall [natsal](../../strongs/h/h5337.md) them.

<a name="isaiah_19_21"></a>Isaiah 19:21

And [Yĕhovah](../../strongs/h/h3068.md) shall be [yada'](../../strongs/h/h3045.md) to [Mitsrayim](../../strongs/h/h4714.md), and the [Mitsrayim](../../strongs/h/h4714.md) shall [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md) in that day, and shall do [zebach](../../strongs/h/h2077.md) and [minchah](../../strongs/h/h4503.md); yea, they shall [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [shalam](../../strongs/h/h7999.md) it.

<a name="isaiah_19_22"></a>Isaiah 19:22

And [Yĕhovah](../../strongs/h/h3068.md) shall [nāḡap̄](../../strongs/h/h5062.md) [Mitsrayim](../../strongs/h/h4714.md): he shall [nāḡap̄](../../strongs/h/h5062.md) and [rapha'](../../strongs/h/h7495.md) it: and they shall [shuwb](../../strongs/h/h7725.md) even to [Yĕhovah](../../strongs/h/h3068.md), and he shall be [ʿāṯar](../../strongs/h/h6279.md) of them, and shall [rapha'](../../strongs/h/h7495.md) them.

<a name="isaiah_19_23"></a>Isaiah 19:23

In that [yowm](../../strongs/h/h3117.md) shall there be a [mĕcillah](../../strongs/h/h4546.md) out of [Mitsrayim](../../strongs/h/h4714.md) to ['Aššûr](../../strongs/h/h804.md), and the ['Aššûr](../../strongs/h/h804.md) shall come into [Mitsrayim](../../strongs/h/h4714.md), and the [Mitsrayim](../../strongs/h/h4714.md) into ['Aššûr](../../strongs/h/h804.md), and the [Mitsrayim](../../strongs/h/h4714.md) shall ['abad](../../strongs/h/h5647.md) with the ['Aššûr](../../strongs/h/h804.md).

<a name="isaiah_19_24"></a>Isaiah 19:24

In that [yowm](../../strongs/h/h3117.md) shall [Yisra'el](../../strongs/h/h3478.md) be the third with [Mitsrayim](../../strongs/h/h4714.md) and with ['Aššûr](../../strongs/h/h804.md), even a [bĕrakah](../../strongs/h/h1293.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['erets](../../strongs/h/h776.md):

<a name="isaiah_19_25"></a>Isaiah 19:25

Whom [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) shall [barak](../../strongs/h/h1288.md), ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Mitsrayim](../../strongs/h/h4714.md) my ['am](../../strongs/h/h5971.md), and ['Aššûr](../../strongs/h/h804.md) the [ma'aseh](../../strongs/h/h4639.md) of my [yad](../../strongs/h/h3027.md), and [Yisra'el](../../strongs/h/h3478.md) mine [nachalah](../../strongs/h/h5159.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 18](isaiah_18.md) - [Isaiah 20](isaiah_20.md)