# [Isaiah 56](https://www.blueletterbible.org/kjv/isa/56/1/s_735001)

<a name="isaiah_56_1"></a>Isaiah 56:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [shamar](../../strongs/h/h8104.md) ye [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) [tsedaqah](../../strongs/h/h6666.md): for my [yĕshuw'ah](../../strongs/h/h3444.md) is [qarowb](../../strongs/h/h7138.md) to [bow'](../../strongs/h/h935.md), and my [ṣĕdāqāh](../../strongs/h/h6666.md) to be [gālâ](../../strongs/h/h1540.md).

<a name="isaiah_56_2"></a>Isaiah 56:2

['esher](../../strongs/h/h835.md) is the ['enowsh](../../strongs/h/h582.md) that ['asah](../../strongs/h/h6213.md) this, and the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md) that [ḥāzaq](../../strongs/h/h2388.md) on it; that [shamar](../../strongs/h/h8104.md) the [shabbath](../../strongs/h/h7676.md) from [ḥālal](../../strongs/h/h2490.md) it, and [shamar](../../strongs/h/h8104.md) his [yad](../../strongs/h/h3027.md) from doing any [ra'](../../strongs/h/h7451.md).

<a name="isaiah_56_3"></a>Isaiah 56:3

Neither let the [ben](../../strongs/h/h1121.md) of the [nēḵār](../../strongs/h/h5236.md), that hath joined himself to [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath [bāḏal](../../strongs/h/h914.md) [bāḏal](../../strongs/h/h914.md) me from his ['am](../../strongs/h/h5971.md): neither let the [sārîs](../../strongs/h/h5631.md) ['āmar](../../strongs/h/h559.md), Behold, I am a [yāḇēš](../../strongs/h/h3002.md) ['ets](../../strongs/h/h6086.md).

<a name="isaiah_56_4"></a>Isaiah 56:4

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto the [sārîs](../../strongs/h/h5631.md) that [shamar](../../strongs/h/h8104.md) my [shabbath](../../strongs/h/h7676.md), and [bāḥar](../../strongs/h/h977.md) the things that [ḥāp̄ēṣ](../../strongs/h/h2654.md) me, and [ḥāzaq](../../strongs/h/h2388.md) of my [bĕriyth](../../strongs/h/h1285.md);

<a name="isaiah_56_5"></a>Isaiah 56:5

Even unto them will I [nathan](../../strongs/h/h5414.md) in mine [bayith](../../strongs/h/h1004.md) and within my [ḥômâ](../../strongs/h/h2346.md) a [yad](../../strongs/h/h3027.md) and a [shem](../../strongs/h/h8034.md) better than of [ben](../../strongs/h/h1121.md) and of [bath](../../strongs/h/h1323.md): I will [nathan](../../strongs/h/h5414.md) them an ['owlam](../../strongs/h/h5769.md) [shem](../../strongs/h/h8034.md), that shall not be [karath](../../strongs/h/h3772.md).

<a name="isaiah_56_6"></a>Isaiah 56:6

Also the [ben](../../strongs/h/h1121.md) of the [nēḵār](../../strongs/h/h5236.md), that [lāvâ](../../strongs/h/h3867.md) themselves to [Yĕhovah](../../strongs/h/h3068.md), to [sharath](../../strongs/h/h8334.md) him, and to ['ahab](../../strongs/h/h157.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), to be his ['ebed](../../strongs/h/h5650.md), every one that [shamar](../../strongs/h/h8104.md) the [shabbath](../../strongs/h/h7676.md) from [ḥālal](../../strongs/h/h2490.md) it, and [ḥāzaq](../../strongs/h/h2388.md) of my [bĕriyth](../../strongs/h/h1285.md);

<a name="isaiah_56_7"></a>Isaiah 56:7

Even them will I [bow'](../../strongs/h/h935.md) to my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), and make them [samach](../../strongs/h/h8055.md) in my [bayith](../../strongs/h/h1004.md) of [tĕphillah](../../strongs/h/h8605.md): their [ʿōlâ](../../strongs/h/h5930.md) and their [zebach](../../strongs/h/h2077.md) shall be [ratsown](../../strongs/h/h7522.md) upon mine [mizbeach](../../strongs/h/h4196.md); for mine [bayith](../../strongs/h/h1004.md) shall be called a [bayith](../../strongs/h/h1004.md) of [tĕphillah](../../strongs/h/h8605.md) for all ['am](../../strongs/h/h5971.md).

<a name="isaiah_56_8"></a>Isaiah 56:8

['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), which [qāḇaṣ](../../strongs/h/h6908.md) the [dāḥâ](../../strongs/h/h1760.md) of [Yisra'el](../../strongs/h/h3478.md) [nᵊ'um](../../strongs/h/h5002.md), Yet will I [qāḇaṣ](../../strongs/h/h6908.md) others to him, beside those that are [qāḇaṣ](../../strongs/h/h6908.md) unto him.

<a name="isaiah_56_9"></a>Isaiah 56:9

All ye [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), ['āṯâ](../../strongs/h/h857.md) to ['akal](../../strongs/h/h398.md), yea, all ye [chay](../../strongs/h/h2416.md) in the [yaʿar](../../strongs/h/h3293.md).

<a name="isaiah_56_10"></a>Isaiah 56:10

His [tsaphah](../../strongs/h/h6822.md) are [ʿiûēr](../../strongs/h/h5787.md): they are all [yada'](../../strongs/h/h3045.md), they are all ['illēm](../../strongs/h/h483.md) [keleḇ](../../strongs/h/h3611.md), they cannot [nāḇaḥ](../../strongs/h/h5024.md); [hāzâ](../../strongs/h/h1957.md), [shakab](../../strongs/h/h7901.md), ['ahab](../../strongs/h/h157.md) to [nûm](../../strongs/h/h5123.md).

<a name="isaiah_56_11"></a>Isaiah 56:11

Yea, they are ['az](../../strongs/h/h5794.md) [nephesh](../../strongs/h/h5315.md) [keleḇ](../../strongs/h/h3611.md) which can never have enough, and they are [ra'ah](../../strongs/h/h7462.md) that cannot [bîn](../../strongs/h/h995.md): they all [panah](../../strongs/h/h6437.md) to their own [derek](../../strongs/h/h1870.md), every one for his [beṣaʿ](../../strongs/h/h1215.md), from his [qāṣê](../../strongs/h/h7097.md).

<a name="isaiah_56_12"></a>Isaiah 56:12

['āṯâ](../../strongs/h/h857.md) ye, I will [laqach](../../strongs/h/h3947.md) [yayin](../../strongs/h/h3196.md), and we will [sāḇā'](../../strongs/h/h5433.md) ourselves with [šēḵār](../../strongs/h/h7941.md); and [māḥār](../../strongs/h/h4279.md) shall be as this [yowm](../../strongs/h/h3117.md), and [me'od](../../strongs/h/h3966.md) [yeṯer](../../strongs/h/h3499.md) [gadowl](../../strongs/h/h1419.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 55](isaiah_55.md) - [Isaiah 57](isaiah_57.md)