# [Isaiah 13](https://www.blueletterbible.org/kjv/isa/13/1/s_692001)

<a name="isaiah_13_1"></a>Isaiah 13:1

The [maśśā'](../../strongs/h/h4853.md) of [Bāḇel](../../strongs/h/h894.md), which [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md) did see.

<a name="isaiah_13_2"></a>Isaiah 13:2

[nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md) upon the [šāp̄â](../../strongs/h/h8192.md) [har](../../strongs/h/h2022.md), [ruwm](../../strongs/h/h7311.md) the [qowl](../../strongs/h/h6963.md) unto them, [nûp̄](../../strongs/h/h5130.md) the [yad](../../strongs/h/h3027.md), that they may [bow'](../../strongs/h/h935.md) into the [peṯaḥ](../../strongs/h/h6607.md) of the [nāḏîḇ](../../strongs/h/h5081.md).

<a name="isaiah_13_3"></a>Isaiah 13:3

I have [tsavah](../../strongs/h/h6680.md) my [qadash](../../strongs/h/h6942.md), I have also [qara'](../../strongs/h/h7121.md) my [gibôr](../../strongs/h/h1368.md) for mine ['aph](../../strongs/h/h639.md), even them that [ʿallîz](../../strongs/h/h5947.md) in my [ga'avah](../../strongs/h/h1346.md).

<a name="isaiah_13_4"></a>Isaiah 13:4

The [qowl](../../strongs/h/h6963.md) of a [hāmôn](../../strongs/h/h1995.md) in the [har](../../strongs/h/h2022.md), like as of a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md); a [שā'ôn](../../strongs/h/h7588.md) [qowl](../../strongs/h/h6963.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of [gowy](../../strongs/h/h1471.md) ['āsap̄](../../strongs/h/h622.md): [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) [paqad](../../strongs/h/h6485.md) the [tsaba'](../../strongs/h/h6635.md) of the [milḥāmâ](../../strongs/h/h4421.md).

<a name="isaiah_13_5"></a>Isaiah 13:5

They [bow'](../../strongs/h/h935.md) from a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md), from the [qāṣê](../../strongs/h/h7097.md) of [shamayim](../../strongs/h/h8064.md), even [Yĕhovah](../../strongs/h/h3068.md), and the [kĕliy](../../strongs/h/h3627.md) of his [zaʿam](../../strongs/h/h2195.md), to [chabal](../../strongs/h/h2254.md) the ['erets](../../strongs/h/h776.md).

<a name="isaiah_13_6"></a>Isaiah 13:6

[yālal](../../strongs/h/h3213.md) ye; for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md); it shall [bow'](../../strongs/h/h935.md) as a [shod](../../strongs/h/h7701.md) from [Šaday](../../strongs/h/h7706.md).

<a name="isaiah_13_7"></a>Isaiah 13:7

Therefore shall all [yad](../../strongs/h/h3027.md) be [rāp̄â](../../strongs/h/h7503.md), and every ['enowsh](../../strongs/h/h582.md) [lebab](../../strongs/h/h3824.md) shall [māsas](../../strongs/h/h4549.md):

<a name="isaiah_13_8"></a>Isaiah 13:8

And they shall be [bahal](../../strongs/h/h926.md): [ṣîr](../../strongs/h/h6735.md) and [chebel](../../strongs/h/h2256.md) shall ['āḥaz](../../strongs/h/h270.md) of them; they shall be in [chuwl](../../strongs/h/h2342.md) as a [yalad](../../strongs/h/h3205.md): they shall be [tāmah](../../strongs/h/h8539.md) one at another; their [paniym](../../strongs/h/h6440.md) shall be as [lahaḇ](../../strongs/h/h3851.md).

<a name="isaiah_13_9"></a>Isaiah 13:9

Behold, the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md), ['aḵzārî](../../strongs/h/h394.md) both with ['ebrah](../../strongs/h/h5678.md) and [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md), to [śûm](../../strongs/h/h7760.md) the ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md): and he shall [šāmaḏ](../../strongs/h/h8045.md) the [chatta'](../../strongs/h/h2400.md) thereof out of it.

<a name="isaiah_13_10"></a>Isaiah 13:10

For the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md) and the [kᵊsîl](../../strongs/h/h3685.md) thereof shall not [halal](../../strongs/h/h1984.md) their ['owr](../../strongs/h/h216.md): the [šemeš](../../strongs/h/h8121.md) shall be [ḥāšaḵ](../../strongs/h/h2821.md) in his [yāṣā'](../../strongs/h/h3318.md), and the [yareach](../../strongs/h/h3394.md) shall not cause her ['owr](../../strongs/h/h216.md) to [nāḡahh](../../strongs/h/h5050.md).

<a name="isaiah_13_11"></a>Isaiah 13:11

And I will [paqad](../../strongs/h/h6485.md) the [tebel](../../strongs/h/h8398.md) for their [ra'](../../strongs/h/h7451.md), and the [rasha'](../../strongs/h/h7563.md) for their ['avon](../../strongs/h/h5771.md); and I will cause the [gā'ôn](../../strongs/h/h1347.md) of the [zed](../../strongs/h/h2086.md) to [shabath](../../strongs/h/h7673.md), and will [šāp̄ēl](../../strongs/h/h8213.md) the [ga'avah](../../strongs/h/h1346.md) of the [ʿārîṣ](../../strongs/h/h6184.md).

<a name="isaiah_13_12"></a>Isaiah 13:12

I will make an ['enowsh](../../strongs/h/h582.md) more [yāqar](../../strongs/h/h3365.md) than [pāz](../../strongs/h/h6337.md); even an ['adam](../../strongs/h/h120.md) than the [keṯem](../../strongs/h/h3800.md) of ['Ôp̄îr](../../strongs/h/h211.md).

<a name="isaiah_13_13"></a>Isaiah 13:13

Therefore I will [ragaz](../../strongs/h/h7264.md) the [shamayim](../../strongs/h/h8064.md), and the ['erets](../../strongs/h/h776.md) shall [rāʿaš](../../strongs/h/h7493.md) out of her [maqowm](../../strongs/h/h4725.md), in the ['ebrah](../../strongs/h/h5678.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and in the [yowm](../../strongs/h/h3117.md) of his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md).

<a name="isaiah_13_14"></a>Isaiah 13:14

And it shall be as the [nāḏaḥ](../../strongs/h/h5080.md) [ṣᵊḇî](../../strongs/h/h6643.md), and as a [tso'n](../../strongs/h/h6629.md) that no man [qāḇaṣ](../../strongs/h/h6908.md): they shall every ['iysh](../../strongs/h/h376.md) [panah](../../strongs/h/h6437.md) to his own ['am](../../strongs/h/h5971.md), and [nûs](../../strongs/h/h5127.md) every one into his own ['erets](../../strongs/h/h776.md).

<a name="isaiah_13_15"></a>Isaiah 13:15

Every one that is [māṣā'](../../strongs/h/h4672.md) shall be [dāqar](../../strongs/h/h1856.md); and every one that is [sāp̄â](../../strongs/h/h5595.md) unto them shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md).

<a name="isaiah_13_16"></a>Isaiah 13:16

Their ['owlel](../../strongs/h/h5768.md) also shall be [rāṭaš](../../strongs/h/h7376.md) before their ['ayin](../../strongs/h/h5869.md); their [bayith](../../strongs/h/h1004.md) shall be [šāsas](../../strongs/h/h8155.md), and their ['ishshah](../../strongs/h/h802.md) [shakab](../../strongs/h/h7901.md) [šāḡal](../../strongs/h/h7693.md).

<a name="isaiah_13_17"></a>Isaiah 13:17

Behold, I will [ʿûr](../../strongs/h/h5782.md) the [Māḏay](../../strongs/h/h4074.md) against them, which shall not regard [keceph](../../strongs/h/h3701.md); and as for [zāhāḇ](../../strongs/h/h2091.md), they shall not [ḥāp̄ēṣ](../../strongs/h/h2654.md) in it.

<a name="isaiah_13_18"></a>Isaiah 13:18

Their [qesheth](../../strongs/h/h7198.md) also shall [rāṭaš](../../strongs/h/h7376.md) the [naʿar](../../strongs/h/h5288.md); and they shall have no [racham](../../strongs/h/h7355.md) on the [pĕriy](../../strongs/h/h6529.md) of the [beten](../../strongs/h/h990.md); their ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md) [ben](../../strongs/h/h1121.md).

<a name="isaiah_13_19"></a>Isaiah 13:19

And [Bāḇel](../../strongs/h/h894.md), the [ṣᵊḇî](../../strongs/h/h6643.md) of [mamlāḵâ](../../strongs/h/h4467.md), the [tip̄'ārâ](../../strongs/h/h8597.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) [gā'ôn](../../strongs/h/h1347.md), shall be as when ['Elohiym](../../strongs/h/h430.md) [mahpēḵâ](../../strongs/h/h4114.md) [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md).

<a name="isaiah_13_20"></a>Isaiah 13:20

It shall [netsach](../../strongs/h/h5331.md) be [yashab](../../strongs/h/h3427.md), neither shall it be [shakan](../../strongs/h/h7931.md) in from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md): neither shall the [ʿĂrāḇî](../../strongs/h/h6163.md) ['ōhel](../../strongs/h/h167.md) there; neither shall the [ra'ah](../../strongs/h/h7462.md) [rāḇaṣ](../../strongs/h/h7257.md) there.

<a name="isaiah_13_21"></a>Isaiah 13:21

But [ṣîyî](../../strongs/h/h6728.md) shall [rāḇaṣ](../../strongs/h/h7257.md) there; and their [bayith](../../strongs/h/h1004.md) shall be [mālā'](../../strongs/h/h4390.md) of ['ōaḥ](../../strongs/h/h255.md); and [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md) shall [shakan](../../strongs/h/h7931.md) there, and [śāʿîr](../../strongs/h/h8163.md) shall [rāqaḏ](../../strongs/h/h7540.md) there.

<a name="isaiah_13_22"></a>Isaiah 13:22

And the ['î](../../strongs/h/h338.md) shall ['anah](../../strongs/h/h6030.md) in their ['almānâ](../../strongs/h/h490.md), and [tannîn](../../strongs/h/h8577.md) in their [ʿōneḡ](../../strongs/h/h6027.md) [heykal](../../strongs/h/h1964.md): and her [ʿēṯ](../../strongs/h/h6256.md) is [qarowb](../../strongs/h/h7138.md) to [bow'](../../strongs/h/h935.md), and her [yowm](../../strongs/h/h3117.md) shall not be [mashak](../../strongs/h/h4900.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 12](isaiah_12.md) - [Isaiah 14](isaiah_14.md)