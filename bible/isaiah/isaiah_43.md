# [Isaiah 43](https://www.blueletterbible.org/kjv/isa/43/1/s_722001)

<a name="isaiah_43_1"></a>Isaiah 43:1

But now thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) that [bara'](../../strongs/h/h1254.md) thee, O [Ya'aqob](../../strongs/h/h3290.md), and he that [yāṣar](../../strongs/h/h3335.md) thee, [Yisra'el](../../strongs/h/h3478.md), [yare'](../../strongs/h/h3372.md) not: for I have [gā'al](../../strongs/h/h1350.md) thee, I have [qara'](../../strongs/h/h7121.md) thee by thy [shem](../../strongs/h/h8034.md); thou art mine.

<a name="isaiah_43_2"></a>Isaiah 43:2

When thou ['abar](../../strongs/h/h5674.md) the [mayim](../../strongs/h/h4325.md), I will be with thee; and through the [nāhār](../../strongs/h/h5104.md), they shall not [šāṭap̄](../../strongs/h/h7857.md) thee: when thou [yālaḵ](../../strongs/h/h3212.md) through the ['esh](../../strongs/h/h784.md), thou shalt not be [kāvâ](../../strongs/h/h3554.md); neither shall the [lehāḇâ](../../strongs/h/h3852.md) [bāʿar](../../strongs/h/h1197.md) upon thee.

<a name="isaiah_43_3"></a>Isaiah 43:3

For I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), thy [yasha'](../../strongs/h/h3467.md): I [nathan](../../strongs/h/h5414.md) [Mitsrayim](../../strongs/h/h4714.md) for thy [kōp̄er](../../strongs/h/h3724.md), [Kûš](../../strongs/h/h3568.md) and [Sᵊḇā'](../../strongs/h/h5434.md) for thee.

<a name="isaiah_43_4"></a>Isaiah 43:4

Since thou wast [yāqar](../../strongs/h/h3365.md) in my ['ayin](../../strongs/h/h5869.md), thou hast been [kabad](../../strongs/h/h3513.md), and I have ['ahab](../../strongs/h/h157.md) thee: therefore will I [nathan](../../strongs/h/h5414.md) ['adam](../../strongs/h/h120.md) for thee, and [lĕom](../../strongs/h/h3816.md) for thy [nephesh](../../strongs/h/h5315.md).

<a name="isaiah_43_5"></a>Isaiah 43:5

[yare'](../../strongs/h/h3372.md) not: for I am with thee: I will [bow'](../../strongs/h/h935.md) thy [zera'](../../strongs/h/h2233.md) from the [mizrach](../../strongs/h/h4217.md), and [qāḇaṣ](../../strongs/h/h6908.md) thee from the [ma'arab](../../strongs/h/h4628.md);

<a name="isaiah_43_6"></a>Isaiah 43:6

I will ['āmar](../../strongs/h/h559.md) to the [ṣāp̄ôn](../../strongs/h/h6828.md), [nathan](../../strongs/h/h5414.md); and to the [têmān](../../strongs/h/h8486.md), [kālā'](../../strongs/h/h3607.md) not: bring my [ben](../../strongs/h/h1121.md) from [rachowq](../../strongs/h/h7350.md), and my [bath](../../strongs/h/h1323.md) from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md);

<a name="isaiah_43_7"></a>Isaiah 43:7

Even [kōl](../../strongs/h/h3605.md) that is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md): for I have [bara'](../../strongs/h/h1254.md) him for my [kabowd](../../strongs/h/h3519.md), I have [yāṣar](../../strongs/h/h3335.md) him; yea, I have ['asah](../../strongs/h/h6213.md) him.

<a name="isaiah_43_8"></a>Isaiah 43:8

[yāṣā'](../../strongs/h/h3318.md) the [ʿiûēr](../../strongs/h/h5787.md) ['am](../../strongs/h/h5971.md) that have ['ayin](../../strongs/h/h5869.md), and the [ḥērēš](../../strongs/h/h2795.md) that have ['ozen](../../strongs/h/h241.md).

<a name="isaiah_43_9"></a>Isaiah 43:9

Let all the [gowy](../../strongs/h/h1471.md) be [qāḇaṣ](../../strongs/h/h6908.md) [yaḥaḏ](../../strongs/h/h3162.md), and let the [lĕom](../../strongs/h/h3816.md) be ['āsap̄](../../strongs/h/h622.md): who among them can [nāḡaḏ](../../strongs/h/h5046.md) this, and [shama'](../../strongs/h/h8085.md) us [ri'šôn](../../strongs/h/h7223.md)? let them [nathan](../../strongs/h/h5414.md) their ['ed](../../strongs/h/h5707.md), that they may be [ṣāḏaq](../../strongs/h/h6663.md): or let them [shama'](../../strongs/h/h8085.md), and ['āmar](../../strongs/h/h559.md), It is ['emeth](../../strongs/h/h571.md).

<a name="isaiah_43_10"></a>Isaiah 43:10

Ye are my ['ed](../../strongs/h/h5707.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and my ['ebed](../../strongs/h/h5650.md) whom I have [bāḥar](../../strongs/h/h977.md): that ye may [yada'](../../strongs/h/h3045.md) and ['aman](../../strongs/h/h539.md) me, and [bîn](../../strongs/h/h995.md) that I am he: [paniym](../../strongs/h/h6440.md) me there was no ['el](../../strongs/h/h410.md) [yāṣar](../../strongs/h/h3335.md), neither shall there be ['aḥar](../../strongs/h/h310.md) me.

<a name="isaiah_43_11"></a>Isaiah 43:11

I, even I, am [Yĕhovah](../../strongs/h/h3068.md); and beside me there is no [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_43_12"></a>Isaiah 43:12

I have [nāḡaḏ](../../strongs/h/h5046.md), and have [yasha'](../../strongs/h/h3467.md), and I have [shama'](../../strongs/h/h8085.md), when there was no [zûr](../../strongs/h/h2114.md) god among you: therefore ye are my ['ed](../../strongs/h/h5707.md), saith [Yĕhovah](../../strongs/h/h3068.md), that I am ['el](../../strongs/h/h410.md).

<a name="isaiah_43_13"></a>Isaiah 43:13

Yea, before the [yowm](../../strongs/h/h3117.md) was I am he; and there is none that can [natsal](../../strongs/h/h5337.md) out of my [yad](../../strongs/h/h3027.md): I will [pa'al](../../strongs/h/h6466.md), and who shall let it?

<a name="isaiah_43_14"></a>Isaiah 43:14

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), your [gā'al](../../strongs/h/h1350.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md); For your sake I have sent to [Bāḇel](../../strongs/h/h894.md), and have [yarad](../../strongs/h/h3381.md) all their [bāriaḥ](../../strongs/h/h1281.md), and the [Kaśdîmâ](../../strongs/h/h3778.md), whose [rinnah](../../strongs/h/h7440.md) is in the ['ŏnîyâ](../../strongs/h/h591.md).

<a name="isaiah_43_15"></a>Isaiah 43:15

I am [Yĕhovah](../../strongs/h/h3068.md), your [qadowsh](../../strongs/h/h6918.md), the [bara'](../../strongs/h/h1254.md) of [Yisra'el](../../strongs/h/h3478.md), your [melek](../../strongs/h/h4428.md).

<a name="isaiah_43_16"></a>Isaiah 43:16

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), which [nathan](../../strongs/h/h5414.md) a [derek](../../strongs/h/h1870.md) in the [yam](../../strongs/h/h3220.md), and a [nāṯîḇ](../../strongs/h/h5410.md) in the ['az](../../strongs/h/h5794.md) [mayim](../../strongs/h/h4325.md);

<a name="isaiah_43_17"></a>Isaiah 43:17

Which [yāṣā'](../../strongs/h/h3318.md) the [reḵeḇ](../../strongs/h/h7393.md) and [sûs](../../strongs/h/h5483.md), the [ḥayil](../../strongs/h/h2428.md) and the [ʿizzûz](../../strongs/h/h5808.md); they shall [shakab](../../strongs/h/h7901.md) together, they shall not [quwm](../../strongs/h/h6965.md): they are [dāʿaḵ](../../strongs/h/h1846.md), they are [kāḇâ](../../strongs/h/h3518.md) as [pištê](../../strongs/h/h6594.md).

<a name="isaiah_43_18"></a>Isaiah 43:18

[zakar](../../strongs/h/h2142.md) ye not the [ri'šôn](../../strongs/h/h7223.md), neither [bîn](../../strongs/h/h995.md) the [qaḏmōnî](../../strongs/h/h6931.md).

<a name="isaiah_43_19"></a>Isaiah 43:19

Behold, I will ['asah](../../strongs/h/h6213.md) a [ḥāḏāš](../../strongs/h/h2319.md); now it shall [ṣāmaḥ](../../strongs/h/h6779.md); shall ye not [yada'](../../strongs/h/h3045.md) it? I will even [śûm](../../strongs/h/h7760.md) a [derek](../../strongs/h/h1870.md) in the [midbar](../../strongs/h/h4057.md), and [nāhār](../../strongs/h/h5104.md) in the [yᵊšîmôn](../../strongs/h/h3452.md).

<a name="isaiah_43_20"></a>Isaiah 43:20

The [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) shall [kabad](../../strongs/h/h3513.md) me, the [tannîn](../../strongs/h/h8577.md) and the [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md): because I [nathan](../../strongs/h/h5414.md) [mayim](../../strongs/h/h4325.md) in the [midbar](../../strongs/h/h4057.md), and [nāhār](../../strongs/h/h5104.md) in the [yᵊšîmôn](../../strongs/h/h3452.md), to [šāqâ](../../strongs/h/h8248.md) to my ['am](../../strongs/h/h5971.md), my [bāḥîr](../../strongs/h/h972.md).

<a name="isaiah_43_21"></a>Isaiah 43:21

This ['am](../../strongs/h/h5971.md) have I [yāṣar](../../strongs/h/h3335.md) for myself; they shall [sāp̄ar](../../strongs/h/h5608.md) my [tehillah](../../strongs/h/h8416.md).

<a name="isaiah_43_22"></a>Isaiah 43:22

But thou hast not [qara'](../../strongs/h/h7121.md) upon me, O [Ya'aqob](../../strongs/h/h3290.md); but thou hast been [yaga'](../../strongs/h/h3021.md) of me, [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_43_23"></a>Isaiah 43:23

Thou hast not [bow'](../../strongs/h/h935.md) me the [śê](../../strongs/h/h7716.md) of thy [ʿōlâ](../../strongs/h/h5930.md); neither hast thou [kabad](../../strongs/h/h3513.md) me with thy [zebach](../../strongs/h/h2077.md). I have not caused thee to ['abad](../../strongs/h/h5647.md) with a [minchah](../../strongs/h/h4503.md), nor [yaga'](../../strongs/h/h3021.md) thee with [lᵊḇônâ](../../strongs/h/h3828.md).

<a name="isaiah_43_24"></a>Isaiah 43:24

Thou hast [qānâ](../../strongs/h/h7069.md) me no [qānê](../../strongs/h/h7070.md) with [keceph](../../strongs/h/h3701.md), neither hast thou [rāvâ](../../strongs/h/h7301.md) me with the [cheleb](../../strongs/h/h2459.md) of thy [zebach](../../strongs/h/h2077.md): but thou hast made me to ['abad](../../strongs/h/h5647.md) with thy [chatta'ath](../../strongs/h/h2403.md), thou hast [yaga'](../../strongs/h/h3021.md) me with thine ['avon](../../strongs/h/h5771.md).

<a name="isaiah_43_25"></a>Isaiah 43:25

I, even I, am he that [māḥâ](../../strongs/h/h4229.md) out thy [pesha'](../../strongs/h/h6588.md) for mine own sake, and will not [zakar](../../strongs/h/h2142.md) thy [chatta'ath](../../strongs/h/h2403.md).

<a name="isaiah_43_26"></a>Isaiah 43:26

Put me in [zakar](../../strongs/h/h2142.md): let us [shaphat](../../strongs/h/h8199.md) together: declare thou, that thou mayest be [ṣāḏaq](../../strongs/h/h6663.md).

<a name="isaiah_43_27"></a>Isaiah 43:27

Thy first ['ab](../../strongs/h/h1.md) hath [chata'](../../strongs/h/h2398.md), and thy [luwts](../../strongs/h/h3887.md) have [pāšaʿ](../../strongs/h/h6586.md) against me.

<a name="isaiah_43_28"></a>Isaiah 43:28

Therefore I have [ḥālal](../../strongs/h/h2490.md) the [śar](../../strongs/h/h8269.md) of the [qodesh](../../strongs/h/h6944.md), and have [nathan](../../strongs/h/h5414.md) [Ya'aqob](../../strongs/h/h3290.md) to the [ḥērem](../../strongs/h/h2764.md), and [Yisra'el](../../strongs/h/h3478.md) to reproaches.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 42](isaiah_42.md) - [Isaiah 44](isaiah_44.md)