# [Isaiah 65](https://www.blueletterbible.org/kjv/isa/65/1/s_744001)

<a name="isaiah_65_1"></a>Isaiah 65:1

I am [darash](../../strongs/h/h1875.md) of them that [sha'al](../../strongs/h/h7592.md) not for me; I am [māṣā'](../../strongs/h/h4672.md) of them that [bāqaš](../../strongs/h/h1245.md) me not: I ['āmar](../../strongs/h/h559.md), Behold me, behold me, unto a [gowy](../../strongs/h/h1471.md) that was not [qara'](../../strongs/h/h7121.md) by my name.

<a name="isaiah_65_2"></a>Isaiah 65:2

I have [pāraś](../../strongs/h/h6566.md) my [yad](../../strongs/h/h3027.md) all the [yowm](../../strongs/h/h3117.md) unto a [sārar](../../strongs/h/h5637.md) ['am](../../strongs/h/h5971.md), which [halak](../../strongs/h/h1980.md) in a [derek](../../strongs/h/h1870.md) that was not [towb](../../strongs/h/h2896.md), after their own [maḥăšāḇâ](../../strongs/h/h4284.md);

<a name="isaiah_65_3"></a>Isaiah 65:3

An ['am](../../strongs/h/h5971.md) that provoketh me to [kāʿas](../../strongs/h/h3707.md) [tāmîḏ](../../strongs/h/h8548.md) to my [paniym](../../strongs/h/h6440.md); that [zabach](../../strongs/h/h2076.md) in [gannâ](../../strongs/h/h1593.md), and [qāṭar](../../strongs/h/h6999.md) upon [lᵊḇēnâ](../../strongs/h/h3843.md);

<a name="isaiah_65_4"></a>Isaiah 65:4

Which [yashab](../../strongs/h/h3427.md) among the [qeber](../../strongs/h/h6913.md), and [lûn](../../strongs/h/h3885.md) in the [nāṣar](../../strongs/h/h5341.md), which ['akal](../../strongs/h/h398.md) [ḥăzîr](../../strongs/h/h2386.md) [basar](../../strongs/h/h1320.md), and [iama](../../strongs/g/g2386.md) [pārāq](../../strongs/h/h6564.md) of [pigûl](../../strongs/h/h6292.md) things is in their [kĕliy](../../strongs/h/h3627.md);

<a name="isaiah_65_5"></a>Isaiah 65:5

Which ['āmar](../../strongs/h/h559.md), [qāraḇ](../../strongs/h/h7126.md) by thyself, [nāḡaš](../../strongs/h/h5066.md) not to me; for I am [qadash](../../strongs/h/h6942.md) than thou. These are an ['ashan](../../strongs/h/h6227.md) in my ['aph](../../strongs/h/h639.md), an ['esh](../../strongs/h/h784.md) that [yāqaḏ](../../strongs/h/h3344.md) all the [yowm](../../strongs/h/h3117.md).

<a name="isaiah_65_6"></a>Isaiah 65:6

Behold, it is [kāṯaḇ](../../strongs/h/h3789.md) before me: I will not [ḥāšâ](../../strongs/h/h2814.md), but will [shalam](../../strongs/h/h7999.md), even [shalam](../../strongs/h/h7999.md) into their [ḥêq](../../strongs/h/h2436.md),

<a name="isaiah_65_7"></a>Isaiah 65:7

Your ['avon](../../strongs/h/h5771.md), and the ['avon](../../strongs/h/h5771.md) of your ['ab](../../strongs/h/h1.md) together, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), which have [qāṭar](../../strongs/h/h6999.md) upon the [har](../../strongs/h/h2022.md), and [ḥārap̄](../../strongs/h/h2778.md) me upon the [giḇʿâ](../../strongs/h/h1389.md): therefore will I [māḏaḏ](../../strongs/h/h4058.md) their [ri'šôn](../../strongs/h/h7223.md) [pe'ullah](../../strongs/h/h6468.md) into their [ḥêq](../../strongs/h/h2436.md).

<a name="isaiah_65_8"></a>Isaiah 65:8

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), As the [tiyrowsh](../../strongs/h/h8492.md) is [māṣā'](../../strongs/h/h4672.md) in the ['eškōôl](../../strongs/h/h811.md), and one saith, [shachath](../../strongs/h/h7843.md) it not; for a [bĕrakah](../../strongs/h/h1293.md) is in it: so will I do for my ['ebed](../../strongs/h/h5650.md) sakes, that I may not [shachath](../../strongs/h/h7843.md) them all.

<a name="isaiah_65_9"></a>Isaiah 65:9

And I will [yāṣā'](../../strongs/h/h3318.md) a [zera'](../../strongs/h/h2233.md) out of [Ya'aqob](../../strongs/h/h3290.md), and out of [Yehuwdah](../../strongs/h/h3063.md) a [yarash](../../strongs/h/h3423.md) of my [har](../../strongs/h/h2022.md): and mine [bāḥîr](../../strongs/h/h972.md) shall [yarash](../../strongs/h/h3423.md) it, and my ['ebed](../../strongs/h/h5650.md) shall [shakan](../../strongs/h/h7931.md) there.

<a name="isaiah_65_10"></a>Isaiah 65:10

And [Šārôn](../../strongs/h/h8289.md) shall be a [nāvê](../../strongs/h/h5116.md) of [tso'n](../../strongs/h/h6629.md), and the [ʿēmeq](../../strongs/h/h6010.md) of [ʿāḵôr](../../strongs/h/h5911.md) a place for the [bāqār](../../strongs/h/h1241.md) to [rēḇeṣ](../../strongs/h/h7258.md), for my ['am](../../strongs/h/h5971.md) that have [darash](../../strongs/h/h1875.md) me.

<a name="isaiah_65_11"></a>Isaiah 65:11

But ye are they that ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), that [shakeach](../../strongs/h/h7913.md) my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), that ['arak](../../strongs/h/h6186.md) a [šulḥān](../../strongs/h/h7979.md) for that [Gāḏ](../../strongs/h/h1409.md) [gaḏ](../../strongs/h/h1408.md), and that [mālā'](../../strongs/h/h4390.md) the [mimsāḵ](../../strongs/h/h4469.md) unto that [mᵊnî](../../strongs/h/h4507.md).

<a name="isaiah_65_12"></a>Isaiah 65:12

Therefore will I [mānâ](../../strongs/h/h4487.md) you to the [chereb](../../strongs/h/h2719.md), and ye shall all [kara'](../../strongs/h/h3766.md) to the [ṭeḇaḥ](../../strongs/h/h2874.md): because when I [qara'](../../strongs/h/h7121.md), ye did not ['anah](../../strongs/h/h6030.md); when I [dabar](../../strongs/h/h1696.md), ye did not [shama'](../../strongs/h/h8085.md); but did [ra'](../../strongs/h/h7451.md) before mine ['ayin](../../strongs/h/h5869.md), and did [bāḥar](../../strongs/h/h977.md) that wherein I [ḥāp̄ēṣ](../../strongs/h/h2654.md) not.

<a name="isaiah_65_13"></a>Isaiah 65:13

Therefore thus ['āmar](../../strongs/h/h559.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), Behold, my ['ebed](../../strongs/h/h5650.md) shall ['akal](../../strongs/h/h398.md), but ye shall be [rāʿēḇ](../../strongs/h/h7456.md): behold, my ['ebed](../../strongs/h/h5650.md) shall [šāṯâ](../../strongs/h/h8354.md), but ye shall be [ṣāmē'](../../strongs/h/h6770.md): behold, my ['ebed](../../strongs/h/h5650.md) shall [samach](../../strongs/h/h8055.md), but ye shall be [buwsh](../../strongs/h/h954.md):

<a name="isaiah_65_14"></a>Isaiah 65:14

Behold, my ['ebed](../../strongs/h/h5650.md) shall [ranan](../../strongs/h/h7442.md) for [ṭûḇ](../../strongs/h/h2898.md) of [leb](../../strongs/h/h3820.md), but ye shall [ṣāʿaq](../../strongs/h/h6817.md) for [kᵊ'ēḇ](../../strongs/h/h3511.md) of [leb](../../strongs/h/h3820.md), and shall [yālal](../../strongs/h/h3213.md) for [šeḇar](../../strongs/h/h7667.md) of [ruwach](../../strongs/h/h7307.md).

<a name="isaiah_65_15"></a>Isaiah 65:15

And ye shall [yānaḥ](../../strongs/h/h3240.md) your [shem](../../strongs/h/h8034.md) for a [šᵊḇûʿâ](../../strongs/h/h7621.md) unto my [bāḥîr](../../strongs/h/h972.md): for ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) shall [muwth](../../strongs/h/h4191.md) thee, and [qara'](../../strongs/h/h7121.md) his ['ebed](../../strongs/h/h5650.md) by another [shem](../../strongs/h/h8034.md):

<a name="isaiah_65_16"></a>Isaiah 65:16

That he who [barak](../../strongs/h/h1288.md) himself in the ['erets](../../strongs/h/h776.md) shall [barak](../../strongs/h/h1288.md) himself in the ['Elohiym](../../strongs/h/h430.md) of ['amen](../../strongs/h/h543.md); and he that [shaba'](../../strongs/h/h7650.md) in the ['erets](../../strongs/h/h776.md) shall [shaba'](../../strongs/h/h7650.md) by the ['Elohiym](../../strongs/h/h430.md) of ['amen](../../strongs/h/h543.md); because the former [tsarah](../../strongs/h/h6869.md) are [shakach](../../strongs/h/h7911.md), and because they are [cathar](../../strongs/h/h5641.md) from mine ['ayin](../../strongs/h/h5869.md).

<a name="isaiah_65_17"></a>Isaiah 65:17

For, behold, I [bara'](../../strongs/h/h1254.md) [ḥāḏāš](../../strongs/h/h2319.md) [shamayim](../../strongs/h/h8064.md) and a [ḥāḏāš](../../strongs/h/h2319.md) ['erets](../../strongs/h/h776.md): and the [ri'šôn](../../strongs/h/h7223.md) shall not be [zakar](../../strongs/h/h2142.md), nor [ʿālâ](../../strongs/h/h5927.md) into [leb](../../strongs/h/h3820.md).

<a name="isaiah_65_18"></a>Isaiah 65:18

But be ye [śûś](../../strongs/h/h7797.md) and [giyl](../../strongs/h/h1523.md) [ʿaḏ](../../strongs/h/h5703.md) in that which I [bara'](../../strongs/h/h1254.md): for, behold, I [bara'](../../strongs/h/h1254.md) [Yĕruwshalaim](../../strongs/h/h3389.md) a [gîlâ](../../strongs/h/h1525.md), and her ['am](../../strongs/h/h5971.md) a [māśôś](../../strongs/h/h4885.md).

<a name="isaiah_65_19"></a>Isaiah 65:19

And I will [giyl](../../strongs/h/h1523.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and [śûś](../../strongs/h/h7797.md) in my ['am](../../strongs/h/h5971.md): and the [qowl](../../strongs/h/h6963.md) of [bĕkiy](../../strongs/h/h1065.md) shall be no more [shama'](../../strongs/h/h8085.md) in her, nor the [qowl](../../strongs/h/h6963.md) of [zaʿaq](../../strongs/h/h2201.md).

<a name="isaiah_65_20"></a>Isaiah 65:20

There shall be no more thence an [ʿûl](../../strongs/h/h5764.md) of [yowm](../../strongs/h/h3117.md), nor a [zāqēn](../../strongs/h/h2205.md) that hath not [mālā'](../../strongs/h/h4390.md) his [yowm](../../strongs/h/h3117.md): for the [naʿar](../../strongs/h/h5288.md) shall [muwth](../../strongs/h/h4191.md) an hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md); but the [chata'](../../strongs/h/h2398.md) being an hundred [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) shall be [qālal](../../strongs/h/h7043.md).

<a name="isaiah_65_21"></a>Isaiah 65:21

And they shall [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md), and [yashab](../../strongs/h/h3427.md) them; and they shall [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of them.

<a name="isaiah_65_22"></a>Isaiah 65:22

They shall not [bānâ](../../strongs/h/h1129.md), and another [yashab](../../strongs/h/h3427.md); they shall not [nāṭaʿ](../../strongs/h/h5193.md), and another ['akal](../../strongs/h/h398.md): for as the [yowm](../../strongs/h/h3117.md) of an ['ets](../../strongs/h/h6086.md) are the [yowm](../../strongs/h/h3117.md) of my ['am](../../strongs/h/h5971.md), and mine [bāḥîr](../../strongs/h/h972.md) shall long [bālâ](../../strongs/h/h1086.md) the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md).

<a name="isaiah_65_23"></a>Isaiah 65:23

They shall not [yaga'](../../strongs/h/h3021.md) in [riyq](../../strongs/h/h7385.md), nor [yalad](../../strongs/h/h3205.md) for [behālâ](../../strongs/h/h928.md); for they are the [zera'](../../strongs/h/h2233.md) of the [barak](../../strongs/h/h1288.md) of [Yĕhovah](../../strongs/h/h3068.md), and their [ṣe'ĕṣā'îm](../../strongs/h/h6631.md) with them.

<a name="isaiah_65_24"></a>Isaiah 65:24

And it shall come to pass, that before they [qara'](../../strongs/h/h7121.md), I will ['anah](../../strongs/h/h6030.md); and while they are yet [dabar](../../strongs/h/h1696.md), I will [shama'](../../strongs/h/h8085.md).

<a name="isaiah_65_25"></a>Isaiah 65:25

The [zᵊ'ēḇ](../../strongs/h/h2061.md) and the [ṭālê](../../strongs/h/h2924.md) shall [ra'ah](../../strongs/h/h7462.md) together, and the ['ariy](../../strongs/h/h738.md) shall ['akal](../../strongs/h/h398.md) [teḇen](../../strongs/h/h8401.md) like the [bāqār](../../strongs/h/h1241.md): and ['aphar](../../strongs/h/h6083.md) shall be the [nachash](../../strongs/h/h5175.md) [lechem](../../strongs/h/h3899.md). They shall not [ra'a'](../../strongs/h/h7489.md) nor [shachath](../../strongs/h/h7843.md) in all my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md), saith [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 64](isaiah_64.md) - [Isaiah 66](isaiah_66.md)