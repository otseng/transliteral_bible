# [Isaiah 45](https://www.blueletterbible.org/kjv/isa/45/1/s_724001)

<a name="isaiah_45_1"></a>Isaiah 45:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) to his [mashiyach](../../strongs/h/h4899.md), to [Kôreš](../../strongs/h/h3566.md), whose [yamiyn](../../strongs/h/h3225.md) I have [ḥāzaq](../../strongs/h/h2388.md), to [rāḏaḏ](../../strongs/h/h7286.md) [gowy](../../strongs/h/h1471.md) before him; and I will [pāṯaḥ](../../strongs/h/h6605.md) the [māṯnayim](../../strongs/h/h4975.md) of [melek](../../strongs/h/h4428.md), to [pāṯaḥ](../../strongs/h/h6605.md) before him the [deleṯ](../../strongs/h/h1817.md); and the [sha'ar](../../strongs/h/h8179.md) shall not be [cagar](../../strongs/h/h5462.md);

<a name="isaiah_45_2"></a>Isaiah 45:2

I will [yālaḵ](../../strongs/h/h3212.md) before thee, and [yashar](../../strongs/h/h3474.md) the [hāḏar](../../strongs/h/h1921.md) [yashar](../../strongs/h/h3474.md): I will [shabar](../../strongs/h/h7665.md) the [deleṯ](../../strongs/h/h1817.md) of [nᵊḥûšâ](../../strongs/h/h5154.md), and [gāḏaʿ](../../strongs/h/h1438.md) the [bᵊrîaḥ](../../strongs/h/h1280.md) of [barzel](../../strongs/h/h1270.md):

<a name="isaiah_45_3"></a>Isaiah 45:3

And I will [nathan](../../strongs/h/h5414.md) thee the ['ôṣār](../../strongs/h/h214.md) of [choshek](../../strongs/h/h2822.md), and [maṭmôn](../../strongs/h/h4301.md) of [mictar](../../strongs/h/h4565.md), that thou mayest [yada'](../../strongs/h/h3045.md) that I, [Yĕhovah](../../strongs/h/h3068.md), which [qara'](../../strongs/h/h7121.md) thee by thy [shem](../../strongs/h/h8034.md), am the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_45_4"></a>Isaiah 45:4

For [Ya'aqob](../../strongs/h/h3290.md) my ['ebed](../../strongs/h/h5650.md) sake, and [Yisra'el](../../strongs/h/h3478.md) mine [bāḥîr](../../strongs/h/h972.md), I have even [qara'](../../strongs/h/h7121.md) thee by thy [shem](../../strongs/h/h8034.md): I have [kānâ](../../strongs/h/h3655.md) thee, though thou hast not [yada'](../../strongs/h/h3045.md) me.

<a name="isaiah_45_5"></a>Isaiah 45:5

I am [Yĕhovah](../../strongs/h/h3068.md), and there is none else, there is no ['Elohiym](../../strongs/h/h430.md) beside me: I ['āzar](../../strongs/h/h247.md) thee, though thou hast not [yada'](../../strongs/h/h3045.md) me:

<a name="isaiah_45_6"></a>Isaiah 45:6

That they may [yada'](../../strongs/h/h3045.md) from the [alpha](../../strongs/g/g1.md) of the [šemeš](../../strongs/h/h8121.md), and from the [ma'arab](../../strongs/h/h4628.md), that there is ['ep̄es](../../strongs/h/h657.md) beside me. I am [Yĕhovah](../../strongs/h/h3068.md), and there is none else.

<a name="isaiah_45_7"></a>Isaiah 45:7

I [yāṣar](../../strongs/h/h3335.md) the ['owr](../../strongs/h/h216.md), and [bara'](../../strongs/h/h1254.md) [choshek](../../strongs/h/h2822.md): I ['asah](../../strongs/h/h6213.md) [shalowm](../../strongs/h/h7965.md), and [bara'](../../strongs/h/h1254.md) [ra'](../../strongs/h/h7451.md): I [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) all these things.

<a name="isaiah_45_8"></a>Isaiah 45:8

[rāʿap̄](../../strongs/h/h7491.md), ye [shamayim](../../strongs/h/h8064.md), from above, and let the [shachaq](../../strongs/h/h7834.md) pour down [tsedeq](../../strongs/h/h6664.md): let the ['erets](../../strongs/h/h776.md) open, and let them bring forth [yesha'](../../strongs/h/h3468.md), and let [ṣĕdāqāh](../../strongs/h/h6666.md) [ṣāmaḥ](../../strongs/h/h6779.md) together; I [Yĕhovah](../../strongs/h/h3068.md) have [bara'](../../strongs/h/h1254.md) it.

<a name="isaiah_45_9"></a>Isaiah 45:9

[hôy](../../strongs/h/h1945.md) unto him that [riyb](../../strongs/h/h7378.md) with his [yāṣar](../../strongs/h/h3335.md)! Let the [ḥereś](../../strongs/h/h2789.md) strive with the [ḥereś](../../strongs/h/h2789.md) of the ['ăḏāmâ](../../strongs/h/h127.md). Shall the [ḥōmer](../../strongs/h/h2563.md) ['āmar](../../strongs/h/h559.md) to him that [yāṣar](../../strongs/h/h3335.md) it, What ['asah](../../strongs/h/h6213.md) thou? or thy [pōʿal](../../strongs/h/h6467.md), He hath no [yad](../../strongs/h/h3027.md)?

<a name="isaiah_45_10"></a>Isaiah 45:10

[hôy](../../strongs/h/h1945.md) unto him that ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), What [yalad](../../strongs/h/h3205.md) thou? or to the ['ishshah](../../strongs/h/h802.md), What hast thou [chuwl](../../strongs/h/h2342.md)?

<a name="isaiah_45_11"></a>Isaiah 45:11

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md), and his [yāṣar](../../strongs/h/h3335.md), [sha'al](../../strongs/h/h7592.md) me of things to come concerning my [ben](../../strongs/h/h1121.md), and concerning the [pōʿal](../../strongs/h/h6467.md) of my [yad](../../strongs/h/h3027.md) [tsavah](../../strongs/h/h6680.md) ye me.

<a name="isaiah_45_12"></a>Isaiah 45:12

I have ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md), and [bara'](../../strongs/h/h1254.md) ['adam](../../strongs/h/h120.md) upon it: I, even my [yad](../../strongs/h/h3027.md), have [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md), and all their [tsaba'](../../strongs/h/h6635.md) have I [tsavah](../../strongs/h/h6680.md).

<a name="isaiah_45_13"></a>Isaiah 45:13

I have [ʿûr](../../strongs/h/h5782.md) him in [tsedeq](../../strongs/h/h6664.md), and I will [yashar](../../strongs/h/h3474.md) all his ways: he shall [bānâ](../../strongs/h/h1129.md) my [ʿîr](../../strongs/h/h5892.md), and he shall [shalach](../../strongs/h/h7971.md) my [gālûṯ](../../strongs/h/h1546.md), not for [mᵊḥîr](../../strongs/h/h4242.md) nor [shachad](../../strongs/h/h7810.md), saith [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="isaiah_45_14"></a>Isaiah 45:14

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), The [yᵊḡîaʿ](../../strongs/h/h3018.md) of [Mitsrayim](../../strongs/h/h4714.md), and [sāḥār](../../strongs/h/h5505.md) of [Kûš](../../strongs/h/h3568.md) and of the [sᵊḇā'î](../../strongs/h/h5436.md), ['enowsh](../../strongs/h/h582.md) of [midâ](../../strongs/h/h4060.md), shall ['abar](../../strongs/h/h5674.md) unto thee, and they shall be thine: they shall [yālaḵ](../../strongs/h/h3212.md) after thee; in [zîqôṯ](../../strongs/h/h2131.md) they shall ['abar](../../strongs/h/h5674.md), and they shall [shachah](../../strongs/h/h7812.md) unto thee, they shall [palal](../../strongs/h/h6419.md) unto thee, saying, Surely ['el](../../strongs/h/h410.md) is in thee; and there is none else, there is no ['Elohiym](../../strongs/h/h430.md).

<a name="isaiah_45_15"></a>Isaiah 45:15

['āḵēn](../../strongs/h/h403.md) thou art an ['el](../../strongs/h/h410.md) that [cathar](../../strongs/h/h5641.md) thyself, O ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), the [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_45_16"></a>Isaiah 45:16

They shall be [buwsh](../../strongs/h/h954.md), and also [kālam](../../strongs/h/h3637.md), all of them: they shall [halak](../../strongs/h/h1980.md) to [kĕlimmah](../../strongs/h/h3639.md) together that are [ḥārāš](../../strongs/h/h2796.md) of [ṣîr](../../strongs/h/h6736.md).

<a name="isaiah_45_17"></a>Isaiah 45:17

But [Yisra'el](../../strongs/h/h3478.md) shall be [yasha'](../../strongs/h/h3467.md) in [Yĕhovah](../../strongs/h/h3068.md) with an ['owlam](../../strongs/h/h5769.md) [tᵊšûʿâ](../../strongs/h/h8668.md): ye shall not be [buwsh](../../strongs/h/h954.md) nor [kālam](../../strongs/h/h3637.md) ['owlam](../../strongs/h/h5769.md) [ʿaḏ](../../strongs/h/h5703.md).

<a name="isaiah_45_18"></a>Isaiah 45:18

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) that [bara'](../../strongs/h/h1254.md) the [shamayim](../../strongs/h/h8064.md); ['Elohiym](../../strongs/h/h430.md) himself that [yāṣar](../../strongs/h/h3335.md) the ['erets](../../strongs/h/h776.md) and ['asah](../../strongs/h/h6213.md) it; he hath [kuwn](../../strongs/h/h3559.md) it, he [bara'](../../strongs/h/h1254.md) it not in [tohuw](../../strongs/h/h8414.md), he [yāṣar](../../strongs/h/h3335.md) it to be [yashab](../../strongs/h/h3427.md): I am [Yĕhovah](../../strongs/h/h3068.md); and there is none else.

<a name="isaiah_45_19"></a>Isaiah 45:19

I have not [dabar](../../strongs/h/h1696.md) in [cether](../../strongs/h/h5643.md), in a [choshek](../../strongs/h/h2822.md) [maqowm](../../strongs/h/h4725.md) of the ['erets](../../strongs/h/h776.md): I said not unto the [zera'](../../strongs/h/h2233.md) of [Ya'aqob](../../strongs/h/h3290.md), [bāqaš](../../strongs/h/h1245.md) ye me in [tohuw](../../strongs/h/h8414.md): I [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) [tsedeq](../../strongs/h/h6664.md), I [nāḡaḏ](../../strongs/h/h5046.md) things that are [meyshar](../../strongs/h/h4339.md).

<a name="isaiah_45_20"></a>Isaiah 45:20

[qāḇaṣ](../../strongs/h/h6908.md) yourselves and [bow'](../../strongs/h/h935.md); [nāḡaš](../../strongs/h/h5066.md) together, ye that are [pālîṭ](../../strongs/h/h6412.md) of the [gowy](../../strongs/h/h1471.md): they have no [yada'](../../strongs/h/h3045.md) that set up the ['ets](../../strongs/h/h6086.md) of their [pecel](../../strongs/h/h6459.md), and [palal](../../strongs/h/h6419.md) unto an ['el](../../strongs/h/h410.md) that cannot [yasha'](../../strongs/h/h3467.md).

<a name="isaiah_45_21"></a>Isaiah 45:21

[nāḡaḏ](../../strongs/h/h5046.md) ye, and [nāḡaš](../../strongs/h/h5066.md) them; yea, let them take [ya'ats](../../strongs/h/h3289.md) together: who hath [shama'](../../strongs/h/h8085.md) this from [qeḏem](../../strongs/h/h6924.md)? who hath [nāḡaḏ](../../strongs/h/h5046.md) it from that time?  have not I [Yĕhovah](../../strongs/h/h3068.md)? and there is no ['Elohiym](../../strongs/h/h430.md) else beside me; a [tsaddiyq](../../strongs/h/h6662.md) ['el](../../strongs/h/h410.md) and a [yasha'](../../strongs/h/h3467.md); there is none beside me.

<a name="isaiah_45_22"></a>Isaiah 45:22

[panah](../../strongs/h/h6437.md) unto me, and be ye [yasha'](../../strongs/h/h3467.md), all the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md): for I am ['el](../../strongs/h/h410.md), and there is none else.

<a name="isaiah_45_23"></a>Isaiah 45:23

I have [shaba'](../../strongs/h/h7650.md) by myself, the [dabar](../../strongs/h/h1697.md) is gone out of my [peh](../../strongs/h/h6310.md) in [ṣĕdāqāh](../../strongs/h/h6666.md), and shall not [shuwb](../../strongs/h/h7725.md), That unto me every [bereḵ](../../strongs/h/h1290.md) shall [kara'](../../strongs/h/h3766.md), every [lashown](../../strongs/h/h3956.md) shall [shaba'](../../strongs/h/h7650.md).

<a name="isaiah_45_24"></a>Isaiah 45:24

Surely, shall one ['āmar](../../strongs/h/h559.md), in [Yĕhovah](../../strongs/h/h3068.md) have I [ṣĕdāqāh](../../strongs/h/h6666.md) and ['oz](../../strongs/h/h5797.md): even to him shall men [bow'](../../strongs/h/h935.md); and all that are [ḥārâ](../../strongs/h/h2734.md) against him shall be [buwsh](../../strongs/h/h954.md).

<a name="isaiah_45_25"></a>Isaiah 45:25

In [Yĕhovah](../../strongs/h/h3068.md) shall all the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md) be [ṣāḏaq](../../strongs/h/h6663.md), and shall [halal](../../strongs/h/h1984.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 44](isaiah_44.md) - [Isaiah 46](isaiah_46.md)