# [Isaiah 47](https://www.blueletterbible.org/kjv/isa/47/1/s_726001)

<a name="isaiah_47_1"></a>Isaiah 47:1

[yarad](../../strongs/h/h3381.md), and [yashab](../../strongs/h/h3427.md) in the ['aphar](../../strongs/h/h6083.md), O [bᵊṯûlâ](../../strongs/h/h1330.md) [bath](../../strongs/h/h1323.md) of [Bāḇel](../../strongs/h/h894.md), [yashab](../../strongs/h/h3427.md) on the ['erets](../../strongs/h/h776.md): there is no [kicce'](../../strongs/h/h3678.md), O [bath](../../strongs/h/h1323.md) of the [Kaśdîmâ](../../strongs/h/h3778.md): for thou shalt no more be [qara'](../../strongs/h/h7121.md) [raḵ](../../strongs/h/h7390.md) and [ʿānōḡ](../../strongs/h/h6028.md).

<a name="isaiah_47_2"></a>Isaiah 47:2

[laqach](../../strongs/h/h3947.md) the [rēḥayim](../../strongs/h/h7347.md), and [ṭāḥan](../../strongs/h/h2912.md) [qemaḥ](../../strongs/h/h7058.md): [gālâ](../../strongs/h/h1540.md) thy [ṣammâ](../../strongs/h/h6777.md), [ḥāśap̄](../../strongs/h/h2834.md) the [šōḇel](../../strongs/h/h7640.md), [gālâ](../../strongs/h/h1540.md) the [šôq](../../strongs/h/h7785.md), ['abar](../../strongs/h/h5674.md) the [nāhār](../../strongs/h/h5104.md).

<a name="isaiah_47_3"></a>Isaiah 47:3

Thy [ʿervâ](../../strongs/h/h6172.md) shall be [gālâ](../../strongs/h/h1540.md), yea, thy [cherpah](../../strongs/h/h2781.md) shall be [ra'ah](../../strongs/h/h7200.md): I will take [nāqām](../../strongs/h/h5359.md), and I will not [pāḡaʿ](../../strongs/h/h6293.md) thee as an ['adam](../../strongs/h/h120.md).

<a name="isaiah_47_4"></a>Isaiah 47:4

As for our [gā'al](../../strongs/h/h1350.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_47_5"></a>Isaiah 47:5

[yashab](../../strongs/h/h3427.md) thou [dûmām](../../strongs/h/h1748.md), and get thee into [choshek](../../strongs/h/h2822.md), O [bath](../../strongs/h/h1323.md) of the [Kaśdîmâ](../../strongs/h/h3778.md): for thou shalt no more be [qara'](../../strongs/h/h7121.md), The [gᵊḇereṯ](../../strongs/h/h1404.md) of [mamlāḵâ](../../strongs/h/h4467.md).

<a name="isaiah_47_6"></a>Isaiah 47:6

I was [qāṣap̄](../../strongs/h/h7107.md) with my ['am](../../strongs/h/h5971.md), I have [ḥālal](../../strongs/h/h2490.md) mine [nachalah](../../strongs/h/h5159.md), and [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md): thou didst [śûm](../../strongs/h/h7760.md) them no [raḥam](../../strongs/h/h7356.md); upon the [zāqēn](../../strongs/h/h2205.md) hast thou very [kabad](../../strongs/h/h3513.md) laid thy [ʿōl](../../strongs/h/h5923.md).

<a name="isaiah_47_7"></a>Isaiah 47:7

And thou ['āmar](../../strongs/h/h559.md), I shall be a [gᵊḇereṯ](../../strongs/h/h1404.md) for ever: so that thou didst not [śûm](../../strongs/h/h7760.md) these to thy [leb](../../strongs/h/h3820.md), neither didst [zakar](../../strongs/h/h2142.md) the ['aḥărîṯ](../../strongs/h/h319.md) of it.

<a name="isaiah_47_8"></a>Isaiah 47:8

Therefore [shama'](../../strongs/h/h8085.md) now this, thou that art given to [ʿāḏîn](../../strongs/h/h5719.md), that [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), that ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), I am, and none else beside me; I shall not [yashab](../../strongs/h/h3427.md) as an ['almānâ](../../strongs/h/h490.md), neither shall I [yada'](../../strongs/h/h3045.md) [šᵊḵôl](../../strongs/h/h7908.md):

<a name="isaiah_47_9"></a>Isaiah 47:9

But these two things shall [bow'](../../strongs/h/h935.md) to thee in a [reḡaʿ](../../strongs/h/h7281.md) in ['echad](../../strongs/h/h259.md) [yowm](../../strongs/h/h3117.md), [šᵊḵôl](../../strongs/h/h7908.md), and ['almōn](../../strongs/h/h489.md): they shall [bow'](../../strongs/h/h935.md) upon thee in their [tom](../../strongs/h/h8537.md) for the [rōḇ](../../strongs/h/h7230.md) of thy [kešep̄](../../strongs/h/h3785.md), and for the [me'od](../../strongs/h/h3966.md) [ʿāṣmâ](../../strongs/h/h6109.md) of thine [Ḥeḇer](../../strongs/h/h2267.md).

<a name="isaiah_47_10"></a>Isaiah 47:10

For thou hast [batach](../../strongs/h/h982.md) in thy [ra'](../../strongs/h/h7451.md): thou hast ['āmar](../../strongs/h/h559.md), None [ra'ah](../../strongs/h/h7200.md) me. Thy [ḥāḵmâ](../../strongs/h/h2451.md) and thy [da'ath](../../strongs/h/h1847.md), it hath [shuwb](../../strongs/h/h7725.md) thee; and thou hast ['āmar](../../strongs/h/h559.md) in thine [leb](../../strongs/h/h3820.md), I am, and ['ep̄es](../../strongs/h/h657.md) beside me.

<a name="isaiah_47_11"></a>Isaiah 47:11

Therefore shall [ra'](../../strongs/h/h7451.md) [bow'](../../strongs/h/h935.md) upon thee; thou shalt not [yada'](../../strongs/h/h3045.md) from whence it [šaḥar](../../strongs/h/h7837.md): and [hvô](../../strongs/h/h1943.md) shall [naphal](../../strongs/h/h5307.md) upon thee; thou shalt not be able to [kāp̄ar](../../strongs/h/h3722.md): and [šô'](../../strongs/h/h7722.md) shall [bow'](../../strongs/h/h935.md) upon thee [piṯ'ōm](../../strongs/h/h6597.md), which thou shalt not [yada'](../../strongs/h/h3045.md).

<a name="isaiah_47_12"></a>Isaiah 47:12

['amad](../../strongs/h/h5975.md) now with thine [Ḥeḇer](../../strongs/h/h2267.md), and with the [rōḇ](../../strongs/h/h7230.md) of thy [kešep̄](../../strongs/h/h3785.md), wherein thou hast [yaga'](../../strongs/h/h3021.md) from thy [nāʿur](../../strongs/h/h5271.md); if so be thou shalt be able to [yāʿal](../../strongs/h/h3276.md), if so be thou mayest [ʿāraṣ](../../strongs/h/h6206.md).

<a name="isaiah_47_13"></a>Isaiah 47:13

Thou art [lā'â](../../strongs/h/h3811.md) in the [rōḇ](../../strongs/h/h7230.md) of thy ['etsah](../../strongs/h/h6098.md). Let now the [hāḇar](../../strongs/h/h1895.md) [shamayim](../../strongs/h/h8064.md), the [ḥōzê](../../strongs/h/h2374.md) [kowkab](../../strongs/h/h3556.md), the [ḥōḏeš](../../strongs/h/h2320.md) [yada'](../../strongs/h/h3045.md), ['amad](../../strongs/h/h5975.md), and [yasha'](../../strongs/h/h3467.md) thee from these things that shall [bow'](../../strongs/h/h935.md) upon thee.

<a name="isaiah_47_14"></a>Isaiah 47:14

Behold, they shall be as [qaš](../../strongs/h/h7179.md); the ['esh](../../strongs/h/h784.md) shall [śārap̄](../../strongs/h/h8313.md) them; they shall not [natsal](../../strongs/h/h5337.md) themselves from the [yad](../../strongs/h/h3027.md) of the [lehāḇâ](../../strongs/h/h3852.md): there shall not be a [gechel](../../strongs/h/h1513.md) to [ḥāmam](../../strongs/h/h2552.md) at, nor ['ûr](../../strongs/h/h217.md) to [yashab](../../strongs/h/h3427.md) before it.

<a name="isaiah_47_15"></a>Isaiah 47:15

Thus shall they be unto thee with whom thou hast [yaga'](../../strongs/h/h3021.md), even thy [sāḥar](../../strongs/h/h5503.md), from thy [nāʿur](../../strongs/h/h5271.md): they shall [tāʿâ](../../strongs/h/h8582.md) every one to his [ʿēḇer](../../strongs/h/h5676.md); none shall [yasha'](../../strongs/h/h3467.md) thee.

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 46](isaiah_46.md) - [Isaiah 48](isaiah_48.md)