# [Isaiah 41](https://www.blueletterbible.org/kjv/isa/41/1/s_720001)

<a name="isaiah_41_1"></a>Isaiah 41:1

[ḥāraš](../../strongs/h/h2790.md) before me, O ['î](../../strongs/h/h339.md); and let the [lĕom](../../strongs/h/h3816.md) [ḥālap̄](../../strongs/h/h2498.md) their [koach](../../strongs/h/h3581.md): let them [nāḡaš](../../strongs/h/h5066.md); then let them [dabar](../../strongs/h/h1696.md): let us [qāraḇ](../../strongs/h/h7126.md) [yaḥaḏ](../../strongs/h/h3162.md) to [mishpat](../../strongs/h/h4941.md).

<a name="isaiah_41_2"></a>Isaiah 41:2

Who [ʿûr](../../strongs/h/h5782.md) the [tsedeq](../../strongs/h/h6664.md) man from the [mizrach](../../strongs/h/h4217.md), [qara'](../../strongs/h/h7121.md) him to his [regel](../../strongs/h/h7272.md), gave the [gowy](../../strongs/h/h1471.md) before him, and made him [radah](../../strongs/h/h7287.md) over [melek](../../strongs/h/h4428.md)? he [nathan](../../strongs/h/h5414.md) them as the ['aphar](../../strongs/h/h6083.md) to his [chereb](../../strongs/h/h2719.md), and as [nāḏap̄](../../strongs/h/h5086.md) [qaš](../../strongs/h/h7179.md) to his [qesheth](../../strongs/h/h7198.md).

<a name="isaiah_41_3"></a>Isaiah 41:3

He [radaph](../../strongs/h/h7291.md) them, and ['abar](../../strongs/h/h5674.md) [shalowm](../../strongs/h/h7965.md); even by the ['orach](../../strongs/h/h734.md) that he had not [bow'](../../strongs/h/h935.md) with his [regel](../../strongs/h/h7272.md).

<a name="isaiah_41_4"></a>Isaiah 41:4

Who hath [pa'al](../../strongs/h/h6466.md) and ['asah](../../strongs/h/h6213.md) it, [qara'](../../strongs/h/h7121.md) the [dôr](../../strongs/h/h1755.md) from the [ro'sh](../../strongs/h/h7218.md)? I [Yĕhovah](../../strongs/h/h3068.md), the [ri'šôn](../../strongs/h/h7223.md), and with the ['aḥărôn](../../strongs/h/h314.md); I am he.

<a name="isaiah_41_5"></a>Isaiah 41:5

The ['î](../../strongs/h/h339.md) [ra'ah](../../strongs/h/h7200.md) it, and [yare'](../../strongs/h/h3372.md); the [qāṣâ](../../strongs/h/h7098.md) of the ['erets](../../strongs/h/h776.md) were [ḥārēḏ](../../strongs/h/h2729.md), [qāraḇ](../../strongs/h/h7126.md), and ['āṯâ](../../strongs/h/h857.md).

<a name="isaiah_41_6"></a>Isaiah 41:6

They [ʿāzar](../../strongs/h/h5826.md) every one his [rea'](../../strongs/h/h7453.md); and every one ['āmar](../../strongs/h/h559.md) to his ['ach](../../strongs/h/h251.md), Be [ḥāzaq](../../strongs/h/h2388.md).

<a name="isaiah_41_7"></a>Isaiah 41:7

So the [ḥārāš](../../strongs/h/h2796.md) [ḥāzaq](../../strongs/h/h2388.md) the [tsaraph](../../strongs/h/h6884.md), and he that [chalaq](../../strongs/h/h2505.md) with the [paṭṭîš](../../strongs/h/h6360.md) him that [hālam](../../strongs/h/h1986.md) the [pa'am](../../strongs/h/h6471.md), saying, It is [towb](../../strongs/h/h2896.md) for the [deḇeq](../../strongs/h/h1694.md): and he [ḥāzaq](../../strongs/h/h2388.md) it with [masmēr](../../strongs/h/h4548.md), that it should not be [mowt](../../strongs/h/h4131.md).

<a name="isaiah_41_8"></a>Isaiah 41:8

But thou, [Yisra'el](../../strongs/h/h3478.md), art my ['ebed](../../strongs/h/h5650.md), [Ya'aqob](../../strongs/h/h3290.md) whom I have [bāḥar](../../strongs/h/h977.md), the [zera'](../../strongs/h/h2233.md) of ['Abraham](../../strongs/h/h85.md) my ['ahab](../../strongs/h/h157.md).

<a name="isaiah_41_9"></a>Isaiah 41:9

Thou whom I have [ḥāzaq](../../strongs/h/h2388.md) from the [qāṣâ](../../strongs/h/h7098.md) of the ['erets](../../strongs/h/h776.md), and [qara'](../../strongs/h/h7121.md) thee from the ['āṣîl](../../strongs/h/h678.md) thereof, and ['āmar](../../strongs/h/h559.md) unto thee, Thou art my ['ebed](../../strongs/h/h5650.md); I have [bāḥar](../../strongs/h/h977.md) thee, and not [mā'as](../../strongs/h/h3988.md).

<a name="isaiah_41_10"></a>Isaiah 41:10

[yare'](../../strongs/h/h3372.md) thou not; for I am with thee: be not [šāʿâ](../../strongs/h/h8159.md); for I am thy ['Elohiym](../../strongs/h/h430.md): I will ['amats](../../strongs/h/h553.md) thee; yea, I will [ʿāzar](../../strongs/h/h5826.md) thee; yea, I will [tamak](../../strongs/h/h8551.md) thee with the [yamiyn](../../strongs/h/h3225.md) of my [tsedeq](../../strongs/h/h6664.md).

<a name="isaiah_41_11"></a>Isaiah 41:11

Behold, all they that were [ḥārâ](../../strongs/h/h2734.md) against thee shall be [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md): they shall be as nothing; and they that [rîḇ](../../strongs/h/h7379.md) with thee shall ['abad](../../strongs/h/h6.md).

<a name="isaiah_41_12"></a>Isaiah 41:12

Thou shalt [bāqaš](../../strongs/h/h1245.md) them, and shalt not [māṣā'](../../strongs/h/h4672.md) them, even them that [maṣṣûṯ](../../strongs/h/h4695.md) with thee: they that [milḥāmâ](../../strongs/h/h4421.md) against thee shall be as nothing, and as a thing of ['ep̄es](../../strongs/h/h657.md).

<a name="isaiah_41_13"></a>Isaiah 41:13

For I [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [ḥāzaq](../../strongs/h/h2388.md) thy [yamiyn](../../strongs/h/h3225.md), ['āmar](../../strongs/h/h559.md) unto thee, [yare'](../../strongs/h/h3372.md) not; I will [ʿāzar](../../strongs/h/h5826.md) thee.

<a name="isaiah_41_14"></a>Isaiah 41:14

[yare'](../../strongs/h/h3372.md) not, thou [tôlāʿ](../../strongs/h/h8438.md) [Ya'aqob](../../strongs/h/h3290.md), and ye [math](../../strongs/h/h4962.md) of [Yisra'el](../../strongs/h/h3478.md); I will [ʿāzar](../../strongs/h/h5826.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and thy [gā'al](../../strongs/h/h1350.md), the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_41_15"></a>Isaiah 41:15

Behold, I will [śûm](../../strongs/h/h7760.md) thee a [ḥāḏāš](../../strongs/h/h2319.md) [ḥārûṣ](../../strongs/h/h2742.md) [môraḡ](../../strongs/h/h4173.md) having [pîp̄îyôṯ](../../strongs/h/h6374.md): thou shalt [dûš](../../strongs/h/h1758.md) the [har](../../strongs/h/h2022.md), and [dāqaq](../../strongs/h/h1854.md), and shalt [śûm](../../strongs/h/h7760.md) the [giḇʿâ](../../strongs/h/h1389.md) as [mots](../../strongs/h/h4671.md).

<a name="isaiah_41_16"></a>Isaiah 41:16

Thou shalt [zārâ](../../strongs/h/h2219.md) them, and the [ruwach](../../strongs/h/h7307.md) shall [nasa'](../../strongs/h/h5375.md), and the [saʿar](../../strongs/h/h5591.md) shall [puwts](../../strongs/h/h6327.md) them: and thou shalt [giyl](../../strongs/h/h1523.md) in [Yĕhovah](../../strongs/h/h3068.md), and shalt [halal](../../strongs/h/h1984.md) in the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="isaiah_41_17"></a>Isaiah 41:17

When the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md) [bāqaš](../../strongs/h/h1245.md) [mayim](../../strongs/h/h4325.md), and there is none, and their [lashown](../../strongs/h/h3956.md) [nāšaṯ](../../strongs/h/h5405.md) for [ṣāmā'](../../strongs/h/h6772.md), I [Yĕhovah](../../strongs/h/h3068.md) will ['anah](../../strongs/h/h6030.md) them, I the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) will not ['azab](../../strongs/h/h5800.md) them.

<a name="isaiah_41_18"></a>Isaiah 41:18

I will [pāṯaḥ](../../strongs/h/h6605.md) [nāhār](../../strongs/h/h5104.md) in [šᵊp̄î](../../strongs/h/h8205.md), and [maʿyān](../../strongs/h/h4599.md) in the [tavek](../../strongs/h/h8432.md) of the [biqʿâ](../../strongs/h/h1237.md): I will make the [midbar](../../strongs/h/h4057.md) a ['ăḡam](../../strongs/h/h98.md) of [mayim](../../strongs/h/h4325.md), and the [ṣîyâ](../../strongs/h/h6723.md) ['erets](../../strongs/h/h776.md) [môṣā'](../../strongs/h/h4161.md) of [mayim](../../strongs/h/h4325.md).

<a name="isaiah_41_19"></a>Isaiah 41:19

I will [nathan](../../strongs/h/h5414.md) in the [midbar](../../strongs/h/h4057.md) the ['erez](../../strongs/h/h730.md), the [šiṭṭâ](../../strongs/h/h7848.md), and the [hăḏas](../../strongs/h/h1918.md), and the [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md); I will set in the ['arabah](../../strongs/h/h6160.md) the [bᵊrôš](../../strongs/h/h1265.md), and the [tiḏhār](../../strongs/h/h8410.md), and the [tᵊ'aššûr](../../strongs/h/h8391.md) together:

<a name="isaiah_41_20"></a>Isaiah 41:20

That they may [ra'ah](../../strongs/h/h7200.md), and [yada'](../../strongs/h/h3045.md), and [śûm](../../strongs/h/h7760.md), and [sakal](../../strongs/h/h7919.md) [yaḥaḏ](../../strongs/h/h3162.md), that the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) hath done this, and the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md) hath [bara'](../../strongs/h/h1254.md) it.

<a name="isaiah_41_21"></a>Isaiah 41:21

[qāraḇ](../../strongs/h/h7126.md) your [rîḇ](../../strongs/h/h7379.md), saith [Yĕhovah](../../strongs/h/h3068.md); [nāḡaš](../../strongs/h/h5066.md) your [ʿăṣumâ](../../strongs/h/h6110.md), saith the [melek](../../strongs/h/h4428.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="isaiah_41_22"></a>Isaiah 41:22

Let them [nāḡaš](../../strongs/h/h5066.md), and [nāḡaḏ](../../strongs/h/h5046.md) us what shall [qārâ](../../strongs/h/h7136.md): let them [nāḡaḏ](../../strongs/h/h5046.md) the [ri'šôn](../../strongs/h/h7223.md), what they be, that we may [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) them, and [yada'](../../strongs/h/h3045.md) the ['aḥărîṯ](../../strongs/h/h319.md) of them; or [shama'](../../strongs/h/h8085.md) us things for to [bow'](../../strongs/h/h935.md).

<a name="isaiah_41_23"></a>Isaiah 41:23

[nāḡaḏ](../../strongs/h/h5046.md) the things that are to ['āṯâ](../../strongs/h/h857.md) hereafter, that we may [yada'](../../strongs/h/h3045.md) that ye are ['Elohiym](../../strongs/h/h430.md): yea, do [yatab](../../strongs/h/h3190.md), or do [ra'a'](../../strongs/h/h7489.md), that we may be [šāʿâ](../../strongs/h/h8159.md), and [ra'ah](../../strongs/h/h7200.md) it together.

<a name="isaiah_41_24"></a>Isaiah 41:24

Behold, ye are of ['în](../../strongs/h/h369.md), and your [pōʿal](../../strongs/h/h6467.md) of ['ep̄aʿ](../../strongs/h/h659.md): a [tôʿēḇâ](../../strongs/h/h8441.md) is he that [bāḥar](../../strongs/h/h977.md) you.

<a name="isaiah_41_25"></a>Isaiah 41:25

I have [ʿûr](../../strongs/h/h5782.md) one from the [ṣāp̄ôn](../../strongs/h/h6828.md), and he shall ['āṯâ](../../strongs/h/h857.md): from the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md) shall he [qara'](../../strongs/h/h7121.md) upon my [shem](../../strongs/h/h8034.md): and he shall [bow'](../../strongs/h/h935.md) upon [sāḡān](../../strongs/h/h5461.md) as upon [ḥōmer](../../strongs/h/h2563.md), and as the [yāṣar](../../strongs/h/h3335.md) [rāmas](../../strongs/h/h7429.md) [ṭîṭ](../../strongs/h/h2916.md).

<a name="isaiah_41_26"></a>Isaiah 41:26

Who hath [nāḡaḏ](../../strongs/h/h5046.md) from the [ro'sh](../../strongs/h/h7218.md), that we may [yada'](../../strongs/h/h3045.md)? and [paniym](../../strongs/h/h6440.md), that we may ['āmar](../../strongs/h/h559.md), He is [tsaddiyq](../../strongs/h/h6662.md)? yea, there is none that [nāḡaḏ](../../strongs/h/h5046.md), yea, there is none that [shama'](../../strongs/h/h8085.md), yea, there is none that [shama'](../../strongs/h/h8085.md) your ['emer](../../strongs/h/h561.md).

<a name="isaiah_41_27"></a>Isaiah 41:27

The [ri'šôn](../../strongs/h/h7223.md) shall say to [Tsiyown](../../strongs/h/h6726.md), [hinneh](../../strongs/h/h2009.md), [hinneh](../../strongs/h/h2009.md) them: and I will [nathan](../../strongs/h/h5414.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) one that bringeth [bāśar](../../strongs/h/h1319.md).

<a name="isaiah_41_28"></a>Isaiah 41:28

For I [ra'ah](../../strongs/h/h7200.md), and there was no ['iysh](../../strongs/h/h376.md); even among them, and there was no [ya'ats](../../strongs/h/h3289.md), that, when I asked of them, could [shuwb](../../strongs/h/h7725.md) a [dabar](../../strongs/h/h1697.md).

<a name="isaiah_41_29"></a>Isaiah 41:29

Behold, they are all ['aven](../../strongs/h/h205.md); their [ma'aseh](../../strongs/h/h4639.md) are ['ep̄es](../../strongs/h/h657.md): their [necek](../../strongs/h/h5262.md) are [ruwach](../../strongs/h/h7307.md) and [tohuw](../../strongs/h/h8414.md).

---

[Transliteral Bible](../bible.md)

[Isaiah](isaiah.md)

[Isaiah 40](isaiah_40.md) - [Isaiah 42](isaiah_42.md)