# [Hosea 7](https://www.blueletterbible.org/kjv/hosea/7)

<a name="hosea_7_1"></a>Hosea 7:1

When I would have [rapha'](../../strongs/h/h7495.md) [Yisra'el](../../strongs/h/h3478.md), then the ['avon](../../strongs/h/h5771.md) of ['Ep̄rayim](../../strongs/h/h669.md) was [gālâ](../../strongs/h/h1540.md), and the [ra'](../../strongs/h/h7451.md) of [Šōmrôn](../../strongs/h/h8111.md): for they [pa'al](../../strongs/h/h6466.md) [sheqer](../../strongs/h/h8267.md); and the [gannāḇ](../../strongs/h/h1590.md) [bow'](../../strongs/h/h935.md), and the [gᵊḏûḏ](../../strongs/h/h1416.md) [pāšaṭ](../../strongs/h/h6584.md) [ḥûṣ](../../strongs/h/h2351.md).

<a name="hosea_7_2"></a>Hosea 7:2

And they ['āmar](../../strongs/h/h559.md) not in their [lebab](../../strongs/h/h3824.md) that I [zakar](../../strongs/h/h2142.md) all their [ra'](../../strongs/h/h7451.md): now their own [maʿălāl](../../strongs/h/h4611.md) have beset them [cabab](../../strongs/h/h5437.md); they are before my [paniym](../../strongs/h/h6440.md).

<a name="hosea_7_3"></a>Hosea 7:3

They make the [melek](../../strongs/h/h4428.md) [samach](../../strongs/h/h8055.md) with their [ra'](../../strongs/h/h7451.md), and the [śar](../../strongs/h/h8269.md) with their [kaḥaš](../../strongs/h/h3585.md).

<a name="hosea_7_4"></a>Hosea 7:4

They are all [na'aph](../../strongs/h/h5003.md), as a [tannûr](../../strongs/h/h8574.md) [bāʿar](../../strongs/h/h1197.md) by the ['āp̄â](../../strongs/h/h644.md), who [shabath](../../strongs/h/h7673.md) from [ʿûr](../../strongs/h/h5782.md) after he hath [lûš](../../strongs/h/h3888.md) the [bāṣēq](../../strongs/h/h1217.md), until it be [ḥāmēṣ](../../strongs/h/h2556.md).

<a name="hosea_7_5"></a>Hosea 7:5

In the [yowm](../../strongs/h/h3117.md) of our [melek](../../strongs/h/h4428.md) the [śar](../../strongs/h/h8269.md) have made him [ḥālâ](../../strongs/h/h2470.md) with [chemah](../../strongs/h/h2534.md) of [yayin](../../strongs/h/h3196.md); he [mashak](../../strongs/h/h4900.md) his [yad](../../strongs/h/h3027.md) with [lāṣaṣ](../../strongs/h/h3945.md).

<a name="hosea_7_6"></a>Hosea 7:6

For they have made [qāraḇ](../../strongs/h/h7126.md) their [leb](../../strongs/h/h3820.md) like a [tannûr](../../strongs/h/h8574.md), whiles they ['arab](../../strongs/h/h693.md): their ['āp̄â](../../strongs/h/h644.md) [yāšēn](../../strongs/h/h3463.md) all the [layil](../../strongs/h/h3915.md); in the [boqer](../../strongs/h/h1242.md) it [bāʿar](../../strongs/h/h1197.md) as a [lehāḇâ](../../strongs/h/h3852.md) ['esh](../../strongs/h/h784.md).

<a name="hosea_7_7"></a>Hosea 7:7

They are all [ḥāmam](../../strongs/h/h2552.md) as a [tannûr](../../strongs/h/h8574.md), and have ['akal](../../strongs/h/h398.md) their [shaphat](../../strongs/h/h8199.md); all their [melek](../../strongs/h/h4428.md) are [naphal](../../strongs/h/h5307.md): there is none among them that [qara'](../../strongs/h/h7121.md) unto me.

<a name="hosea_7_8"></a>Hosea 7:8

['Ep̄rayim](../../strongs/h/h669.md), he hath [bālal](../../strongs/h/h1101.md) himself among the ['am](../../strongs/h/h5971.md); ['Ep̄rayim](../../strongs/h/h669.md) is a [ʿugâ](../../strongs/h/h5692.md) not [hāp̄aḵ](../../strongs/h/h2015.md).

<a name="hosea_7_9"></a>Hosea 7:9

[zûr](../../strongs/h/h2114.md) have ['akal](../../strongs/h/h398.md) his [koach](../../strongs/h/h3581.md), and he [yada'](../../strongs/h/h3045.md) it not: yea, gray [śêḇâ](../../strongs/h/h7872.md) are here and [zāraq](../../strongs/h/h2236.md) upon him, yet he [yada'](../../strongs/h/h3045.md) not.

<a name="hosea_7_10"></a>Hosea 7:10

And the [gā'ôn](../../strongs/h/h1347.md) of [Yisra'el](../../strongs/h/h3478.md) ['anah](../../strongs/h/h6030.md) to his [paniym](../../strongs/h/h6440.md): and they do not [shuwb](../../strongs/h/h7725.md) to [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), nor [bāqaš](../../strongs/h/h1245.md) him for all this.

<a name="hosea_7_11"></a>Hosea 7:11

['Ep̄rayim](../../strongs/h/h669.md) also is like a [pāṯâ](../../strongs/h/h6601.md) [yônâ](../../strongs/h/h3123.md) without [leb](../../strongs/h/h3820.md): they [qara'](../../strongs/h/h7121.md) to [Mitsrayim](../../strongs/h/h4714.md), they [halak](../../strongs/h/h1980.md) to ['Aššûr](../../strongs/h/h804.md).

<a name="hosea_7_12"></a>Hosea 7:12

When they shall [yālaḵ](../../strongs/h/h3212.md), I will [pāraś](../../strongs/h/h6566.md) my [rešeṯ](../../strongs/h/h7568.md) upon them; I will bring them [yarad](../../strongs/h/h3381.md) as the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md); I will [yacar](../../strongs/h/h3256.md) them, as their ['edah](../../strongs/h/h5712.md) hath [šēmaʿ](../../strongs/h/h8088.md).

<a name="hosea_7_13"></a>Hosea 7:13

['owy](../../strongs/h/h188.md) unto them! for they have [nāḏaḏ](../../strongs/h/h5074.md) from me: [shod](../../strongs/h/h7701.md) unto them! because they have [pāšaʿ](../../strongs/h/h6586.md) against me: though I have [pāḏâ](../../strongs/h/h6299.md) them, yet they have [dabar](../../strongs/h/h1696.md) [kazab](../../strongs/h/h3577.md) against me.

<a name="hosea_7_14"></a>Hosea 7:14

And they have not [zāʿaq](../../strongs/h/h2199.md) unto me with their [leb](../../strongs/h/h3820.md), when they [yālal](../../strongs/h/h3213.md) upon their [miškāḇ](../../strongs/h/h4904.md): they [guwr](../../strongs/h/h1481.md) themselves for [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md), and they [cuwr](../../strongs/h/h5493.md) against me.

<a name="hosea_7_15"></a>Hosea 7:15

Though I have [yacar](../../strongs/h/h3256.md) and [ḥāzaq](../../strongs/h/h2388.md) their [zerowa'](../../strongs/h/h2220.md), yet do they [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) against me.

<a name="hosea_7_16"></a>Hosea 7:16

They [shuwb](../../strongs/h/h7725.md), but not to the [ʿal](../../strongs/h/h5920.md): they are like a [rᵊmîyâ](../../strongs/h/h7423.md) [qesheth](../../strongs/h/h7198.md): their [śar](../../strongs/h/h8269.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md) for the [zaʿam](../../strongs/h/h2195.md) of their [lashown](../../strongs/h/h3956.md): [zô](../../strongs/h/h2097.md) shall be their [laʿaḡ](../../strongs/h/h3933.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 6](hosea_6.md) - [Hosea 8](hosea_8.md)