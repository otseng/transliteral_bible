# [Hosea 4](https://www.blueletterbible.org/kjv/hosea/4)

<a name="hosea_4_1"></a>Hosea 4:1

[shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): for [Yĕhovah](../../strongs/h/h3068.md) hath a [rîḇ](../../strongs/h/h7379.md) with the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), because there is no ['emeth](../../strongs/h/h571.md), nor [checed](../../strongs/h/h2617.md), nor [da'ath](../../strongs/h/h1847.md) of ['Elohiym](../../strongs/h/h430.md) in the ['erets](../../strongs/h/h776.md).

<a name="hosea_4_2"></a>Hosea 4:2

By ['ālâ](../../strongs/h/h422.md), and [kāḥaš](../../strongs/h/h3584.md), and [ratsach](../../strongs/h/h7523.md), and [ganab](../../strongs/h/h1589.md), and [na'aph](../../strongs/h/h5003.md), they [pāraṣ](../../strongs/h/h6555.md), and [dam](../../strongs/h/h1818.md) [naga'](../../strongs/h/h5060.md) [dam](../../strongs/h/h1818.md).

<a name="hosea_4_3"></a>Hosea 4:3

Therefore shall the ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md), and every one that [yashab](../../strongs/h/h3427.md) therein shall ['āmal](../../strongs/h/h535.md), with the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), and with the [ʿôp̄](../../strongs/h/h5775.md) of [shamayim](../../strongs/h/h8064.md); yea, the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md) also shall be taken ['āsap̄](../../strongs/h/h622.md).

<a name="hosea_4_4"></a>Hosea 4:4

Yet let no ['iysh](../../strongs/h/h376.md) [riyb](../../strongs/h/h7378.md), nor [yakach](../../strongs/h/h3198.md) ['iysh](../../strongs/h/h376.md): for thy ['am](../../strongs/h/h5971.md) are as they that [riyb](../../strongs/h/h7378.md) with the [kōhēn](../../strongs/h/h3548.md).

<a name="hosea_4_5"></a>Hosea 4:5

Therefore shalt thou [kashal](../../strongs/h/h3782.md) in the [yowm](../../strongs/h/h3117.md), and the [nāḇî'](../../strongs/h/h5030.md) also shall [kashal](../../strongs/h/h3782.md) with thee in the [layil](../../strongs/h/h3915.md), and I will [damah](../../strongs/h/h1820.md) thy ['em](../../strongs/h/h517.md).

<a name="hosea_4_6"></a>Hosea 4:6

My ['am](../../strongs/h/h5971.md) are [damah](../../strongs/h/h1820.md) for lack of [da'ath](../../strongs/h/h1847.md): because thou hast [mā'as](../../strongs/h/h3988.md) [da'ath](../../strongs/h/h1847.md), I will also [mā'as](../../strongs/h/h3988.md) thee, that thou shalt be no [kāhan](../../strongs/h/h3547.md) to me: seeing thou hast [shakach](../../strongs/h/h7911.md) the [towrah](../../strongs/h/h8451.md) of thy ['Elohiym](../../strongs/h/h430.md), I will also [shakach](../../strongs/h/h7911.md) thy [ben](../../strongs/h/h1121.md).

<a name="hosea_4_7"></a>Hosea 4:7

As they were [rōḇ](../../strongs/h/h7230.md), so they [chata'](../../strongs/h/h2398.md) against me: therefore will I [mûr](../../strongs/h/h4171.md) their [kabowd](../../strongs/h/h3519.md) into [qālôn](../../strongs/h/h7036.md).

<a name="hosea_4_8"></a>Hosea 4:8

They ['akal](../../strongs/h/h398.md) the [chatta'ath](../../strongs/h/h2403.md) of my ['am](../../strongs/h/h5971.md), and they [nasa'](../../strongs/h/h5375.md) their [nephesh](../../strongs/h/h5315.md) on their ['avon](../../strongs/h/h5771.md).

<a name="hosea_4_9"></a>Hosea 4:9

And there shall be, like ['am](../../strongs/h/h5971.md), like [kōhēn](../../strongs/h/h3548.md): and I will [paqad](../../strongs/h/h6485.md) them for their [derek](../../strongs/h/h1870.md), and [shuwb](../../strongs/h/h7725.md) them their [maʿălāl](../../strongs/h/h4611.md).

<a name="hosea_4_10"></a>Hosea 4:10

For they shall ['akal](../../strongs/h/h398.md), and not have [sāׂbaʿ](../../strongs/h/h7646.md): they shall [zānâ](../../strongs/h/h2181.md), and shall not [pāraṣ](../../strongs/h/h6555.md): because they have left ['azab](../../strongs/h/h5800.md) to take [shamar](../../strongs/h/h8104.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_4_11"></a>Hosea 4:11

[zᵊnûṯ](../../strongs/h/h2184.md) and [yayin](../../strongs/h/h3196.md) and [tiyrowsh](../../strongs/h/h8492.md) [laqach](../../strongs/h/h3947.md) the [leb](../../strongs/h/h3820.md).

<a name="hosea_4_12"></a>Hosea 4:12

My ['am](../../strongs/h/h5971.md) [sha'al](../../strongs/h/h7592.md) at their ['ets](../../strongs/h/h6086.md), and their [maqqēl](../../strongs/h/h4731.md) [nāḡaḏ](../../strongs/h/h5046.md) unto them: for the [ruwach](../../strongs/h/h7307.md) of [zᵊnûnîm](../../strongs/h/h2183.md) hath caused them to [tāʿâ](../../strongs/h/h8582.md), and they have [zānâ](../../strongs/h/h2181.md) from under their ['Elohiym](../../strongs/h/h430.md).

<a name="hosea_4_13"></a>Hosea 4:13

They [zabach](../../strongs/h/h2076.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md), and [qāṭar](../../strongs/h/h6999.md) upon the [giḇʿâ](../../strongs/h/h1389.md), under ['allôn](../../strongs/h/h437.md) and [liḇnê](../../strongs/h/h3839.md) and ['ēlâ](../../strongs/h/h424.md), because the [ṣēl](../../strongs/h/h6738.md) thereof is [towb](../../strongs/h/h2896.md): therefore your [bath](../../strongs/h/h1323.md) shall [zānâ](../../strongs/h/h2181.md), and your [kallâ](../../strongs/h/h3618.md) shall [na'aph](../../strongs/h/h5003.md).

<a name="hosea_4_14"></a>Hosea 4:14

I will not [paqad](../../strongs/h/h6485.md) your [bath](../../strongs/h/h1323.md) when they [zānâ](../../strongs/h/h2181.md), nor your [kallâ](../../strongs/h/h3618.md) when they [na'aph](../../strongs/h/h5003.md): for themselves are [pāraḏ](../../strongs/h/h6504.md) with [zānâ](../../strongs/h/h2181.md), and they [zabach](../../strongs/h/h2076.md) with [qᵊḏēšâ](../../strongs/h/h6948.md): therefore the ['am](../../strongs/h/h5971.md) that doth not [bîn](../../strongs/h/h995.md) shall [lāḇaṭ](../../strongs/h/h3832.md).

<a name="hosea_4_15"></a>Hosea 4:15

Though thou, [Yisra'el](../../strongs/h/h3478.md), [zānâ](../../strongs/h/h2181.md), yet let not [Yehuwdah](../../strongs/h/h3063.md) ['asham](../../strongs/h/h816.md); and [bow'](../../strongs/h/h935.md) not ye unto [Gilgāl](../../strongs/h/h1537.md), neither go ye [ʿālâ](../../strongs/h/h5927.md) to [Bêṯ 'Āven](../../strongs/h/h1007.md), nor [shaba'](../../strongs/h/h7650.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md).

<a name="hosea_4_16"></a>Hosea 4:16

For [Yisra'el](../../strongs/h/h3478.md) [sārar](../../strongs/h/h5637.md) as a [sārar](../../strongs/h/h5637.md) [pārâ](../../strongs/h/h6510.md): now [Yĕhovah](../../strongs/h/h3068.md) will [ra'ah](../../strongs/h/h7462.md) them as a [keḇeś](../../strongs/h/h3532.md) in a [merḥāḇ](../../strongs/h/h4800.md).

<a name="hosea_4_17"></a>Hosea 4:17

['Ep̄rayim](../../strongs/h/h669.md) is [ḥāḇar](../../strongs/h/h2266.md) to [ʿāṣāḇ](../../strongs/h/h6091.md): let him [yānaḥ](../../strongs/h/h3240.md).

<a name="hosea_4_18"></a>Hosea 4:18

Their [sōḇe'](../../strongs/h/h5435.md) is [cuwr](../../strongs/h/h5493.md): they have [zānâ](../../strongs/h/h2181.md) [zānâ](../../strongs/h/h2181.md): her [magen](../../strongs/h/h4043.md) with [qālôn](../../strongs/h/h7036.md) do ['ahab](../../strongs/h/h157.md), [yāhaḇ](../../strongs/h/h3051.md) ye.

<a name="hosea_4_19"></a>Hosea 4:19

The [ruwach](../../strongs/h/h7307.md) hath [tsarar](../../strongs/h/h6887.md) her in her [kanaph](../../strongs/h/h3671.md), and they shall be [buwsh](../../strongs/h/h954.md) because of their [zebach](../../strongs/h/h2077.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 3](hosea_3.md) - [Hosea 5](hosea_5.md)