# [Hosea 1](https://www.blueletterbible.org/kjv/hosea/1)

<a name="hosea_1_1"></a>Hosea 1:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came unto [Hôšēaʿ](../../strongs/h/h1954.md), the [ben](../../strongs/h/h1121.md) of [Bᵊ'ērî](../../strongs/h/h882.md), in the [yowm](../../strongs/h/h3117.md) of ['Uzziyah](../../strongs/h/h5818.md), [Yôṯām](../../strongs/h/h3147.md), ['Āḥāz](../../strongs/h/h271.md), and [Yᵊḥizqîyâ](../../strongs/h/h3169.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [yowm](../../strongs/h/h3117.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md), [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="hosea_1_2"></a>Hosea 1:2

The [tᵊḥillâ](../../strongs/h/h8462.md) of the [dabar](../../strongs/h/h1696.md) of [Yĕhovah](../../strongs/h/h3068.md) by [Hôšēaʿ](../../strongs/h/h1954.md). And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to [Hôšēaʿ](../../strongs/h/h1954.md), [yālaḵ](../../strongs/h/h3212.md), [laqach](../../strongs/h/h3947.md) unto thee an ['ishshah](../../strongs/h/h802.md) of [zᵊnûnîm](../../strongs/h/h2183.md) and [yeleḏ](../../strongs/h/h3206.md) of [zᵊnûnîm](../../strongs/h/h2183.md): for the ['erets](../../strongs/h/h776.md) hath committed [zānâ](../../strongs/h/h2181.md) [zānâ](../../strongs/h/h2181.md), departing ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_1_3"></a>Hosea 1:3

So he [yālaḵ](../../strongs/h/h3212.md) and [laqach](../../strongs/h/h3947.md) [Gōmer](../../strongs/h/h1586.md) the [bath](../../strongs/h/h1323.md) of [Diḇlayim](../../strongs/h/h1691.md); which [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) him a [ben](../../strongs/h/h1121.md).

<a name="hosea_1_4"></a>Hosea 1:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [YizrᵊʿE'L](../../strongs/h/h3157.md); for yet a [mᵊʿaṭ](../../strongs/h/h4592.md) while, and I will [paqad](../../strongs/h/h6485.md) the [dam](../../strongs/h/h1818.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md) upon the [bayith](../../strongs/h/h1004.md) of [Yêû'](../../strongs/h/h3058.md), and will cause to [shabath](../../strongs/h/h7673.md) the [mamlāḵûṯ](../../strongs/h/h4468.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="hosea_1_5"></a>Hosea 1:5

And it shall come to pass at that [yowm](../../strongs/h/h3117.md), that I will [shabar](../../strongs/h/h7665.md) the [qesheth](../../strongs/h/h7198.md) of [Yisra'el](../../strongs/h/h3478.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="hosea_1_6"></a>Hosea 1:6

And she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [bath](../../strongs/h/h1323.md). And God ['āmar](../../strongs/h/h559.md) unto him, [qara'](../../strongs/h/h7121.md) her [shem](../../strongs/h/h8034.md) [Lō' Ruḥāmâ](../../strongs/h/h3819.md): for I will no more have [racham](../../strongs/h/h7355.md) upon the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); but I will [nasa'](../../strongs/h/h5375.md) take them [nasa'](../../strongs/h/h5375.md).

<a name="hosea_1_7"></a>Hosea 1:7

But I will have [racham](../../strongs/h/h7355.md) upon the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), and will [yasha'](../../strongs/h/h3467.md) them by [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and will not [yasha'](../../strongs/h/h3467.md) them by [qesheth](../../strongs/h/h7198.md), nor by [chereb](../../strongs/h/h2719.md), nor by [milḥāmâ](../../strongs/h/h4421.md), by [sûs](../../strongs/h/h5483.md), nor by [pārāš](../../strongs/h/h6571.md).

<a name="hosea_1_8"></a>Hosea 1:8

Now when she had [gamal](../../strongs/h/h1580.md) [Lō' Ruḥāmâ](../../strongs/h/h3819.md), she [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md).

<a name="hosea_1_9"></a>Hosea 1:9

Then ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Lō' ʿAmmî](../../strongs/h/h3818.md): for ye are not my ['am](../../strongs/h/h5971.md), and I will not be your.

<a name="hosea_1_10"></a>Hosea 1:10

Yet the [mispār](../../strongs/h/h4557.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall be as the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md), which cannot be [māḏaḏ](../../strongs/h/h4058.md) nor [sāp̄ar](../../strongs/h/h5608.md); and it shall come to pass, that in the [maqowm](../../strongs/h/h4725.md) where it was ['āmar](../../strongs/h/h559.md) unto them, Ye are not my ['am](../../strongs/h/h5971.md), there it shall be ['āmar](../../strongs/h/h559.md) unto them, Ye are the [ben](../../strongs/h/h1121.md) of the [chay](../../strongs/h/h2416.md) ['el](../../strongs/h/h410.md).

<a name="hosea_1_11"></a>Hosea 1:11

Then shall the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) be [qāḇaṣ](../../strongs/h/h6908.md) [yaḥaḏ](../../strongs/h/h3162.md), and [śûm](../../strongs/h/h7760.md) themselves one [ro'sh](../../strongs/h/h7218.md), and they shall [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md): for [gadowl](../../strongs/h/h1419.md) shall be the [yowm](../../strongs/h/h3117.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 2](hosea_2.md)