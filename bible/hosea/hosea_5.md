# [Hosea 5](https://www.blueletterbible.org/kjv/hosea/5)

<a name="hosea_5_1"></a>Hosea 5:1

[shama'](../../strongs/h/h8085.md) ye this, O [kōhēn](../../strongs/h/h3548.md); and [qashab](../../strongs/h/h7181.md), ye [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); and ['azan](../../strongs/h/h238.md), O [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md); for [mishpat](../../strongs/h/h4941.md) is toward you, because ye have been a [paḥ](../../strongs/h/h6341.md) on [Miṣpâ](../../strongs/h/h4709.md), and a [rešeṯ](../../strongs/h/h7568.md) [pāraś](../../strongs/h/h6566.md) upon [Tāḇôr](../../strongs/h/h8396.md).

<a name="hosea_5_2"></a>Hosea 5:2

And the [śēṭ](../../strongs/h/h7846.md) are [ʿāmaq](../../strongs/h/h6009.md) to make [šāḥaṭ](../../strongs/h/h7819.md), though I have been a [mûsār](../../strongs/h/h4148.md) of them all.

<a name="hosea_5_3"></a>Hosea 5:3

I [yada'](../../strongs/h/h3045.md) ['Ep̄rayim](../../strongs/h/h669.md), and [Yisra'el](../../strongs/h/h3478.md) is not [kāḥaḏ](../../strongs/h/h3582.md) from me: for now, O ['Ep̄rayim](../../strongs/h/h669.md), thou committest [zānâ](../../strongs/h/h2181.md), and [Yisra'el](../../strongs/h/h3478.md) is [ṭāmē'](../../strongs/h/h2930.md).

<a name="hosea_5_4"></a>Hosea 5:4

They will not [nathan](../../strongs/h/h5414.md) their [maʿălāl](../../strongs/h/h4611.md) to [shuwb](../../strongs/h/h7725.md) unto their ['Elohiym](../../strongs/h/h430.md): for the [ruwach](../../strongs/h/h7307.md) of [zᵊnûnîm](../../strongs/h/h2183.md) is in the [qereḇ](../../strongs/h/h7130.md) of them, and they have not [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_5_5"></a>Hosea 5:5

And the [gā'ôn](../../strongs/h/h1347.md) of [Yisra'el](../../strongs/h/h3478.md) doth ['anah](../../strongs/h/h6030.md) to his [paniym](../../strongs/h/h6440.md): therefore shall [Yisra'el](../../strongs/h/h3478.md) and ['Ep̄rayim](../../strongs/h/h669.md) [kashal](../../strongs/h/h3782.md) in their ['avon](../../strongs/h/h5771.md); [Yehuwdah](../../strongs/h/h3063.md) also shall [kashal](../../strongs/h/h3782.md) with them.

<a name="hosea_5_6"></a>Hosea 5:6

They shall [yālaḵ](../../strongs/h/h3212.md) with their [tso'n](../../strongs/h/h6629.md) and with their [bāqār](../../strongs/h/h1241.md) to [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md); but they shall not [māṣā'](../../strongs/h/h4672.md) him; he hath [chalats](../../strongs/h/h2502.md) himself from them.

<a name="hosea_5_7"></a>Hosea 5:7

They have [bāḡaḏ](../../strongs/h/h898.md) against [Yĕhovah](../../strongs/h/h3068.md): for they have [yalad](../../strongs/h/h3205.md) [zûr](../../strongs/h/h2114.md) [ben](../../strongs/h/h1121.md): now shall a [ḥōḏeš](../../strongs/h/h2320.md) ['akal](../../strongs/h/h398.md) them with their [cheleq](../../strongs/h/h2506.md).

<a name="hosea_5_8"></a>Hosea 5:8

[tāqaʿ](../../strongs/h/h8628.md) ye the [šôp̄ār](../../strongs/h/h7782.md) in [giḇʿâ](../../strongs/h/h1390.md), and the [ḥăṣōṣrâ](../../strongs/h/h2689.md) in [rāmâ](../../strongs/h/h7414.md): [rûaʿ](../../strongs/h/h7321.md) at [Bêṯ 'Āven](../../strongs/h/h1007.md), ['aḥar](../../strongs/h/h310.md) thee, O [Binyāmîn](../../strongs/h/h1144.md).

<a name="hosea_5_9"></a>Hosea 5:9

['Ep̄rayim](../../strongs/h/h669.md) shall be [šammâ](../../strongs/h/h8047.md) in the [yowm](../../strongs/h/h3117.md) of [tôḵēḥâ](../../strongs/h/h8433.md): among the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) have I made [yada'](../../strongs/h/h3045.md) that which shall surely ['aman](../../strongs/h/h539.md).

<a name="hosea_5_10"></a>Hosea 5:10

The [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) were like them that [nāsaḡ](../../strongs/h/h5253.md) the [gᵊḇûl](../../strongs/h/h1366.md): therefore I will [šāp̄aḵ](../../strongs/h/h8210.md) my ['ebrah](../../strongs/h/h5678.md) upon them like [mayim](../../strongs/h/h4325.md).

<a name="hosea_5_11"></a>Hosea 5:11

['Ep̄rayim](../../strongs/h/h669.md) is [ʿāšaq](../../strongs/h/h6231.md) and [rāṣaṣ](../../strongs/h/h7533.md) in [mishpat](../../strongs/h/h4941.md), because he [yā'al](../../strongs/h/h2974.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) the [ṣav](../../strongs/h/h6673.md).

<a name="hosea_5_12"></a>Hosea 5:12

Therefore will I be unto ['Ep̄rayim](../../strongs/h/h669.md) as a [ʿāš](../../strongs/h/h6211.md), and to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) as [rāqāḇ](../../strongs/h/h7538.md).

<a name="hosea_5_13"></a>Hosea 5:13

When ['Ep̄rayim](../../strongs/h/h669.md) [ra'ah](../../strongs/h/h7200.md) his [ḥŏlî](../../strongs/h/h2483.md), and [Yehuwdah](../../strongs/h/h3063.md) saw his [māzôr](../../strongs/h/h4205.md), then [yālaḵ](../../strongs/h/h3212.md) ['Ep̄rayim](../../strongs/h/h669.md) to the ['Aššûr](../../strongs/h/h804.md), and [shalach](../../strongs/h/h7971.md) to [melek](../../strongs/h/h4428.md) [yārēḇ](../../strongs/h/h3377.md) [riyb](../../strongs/h/h7378.md): yet [yakol](../../strongs/h/h3201.md) he not [rapha'](../../strongs/h/h7495.md) you, nor [gāhâ](../../strongs/h/h1455.md) you of your [māzôr](../../strongs/h/h4205.md).

<a name="hosea_5_14"></a>Hosea 5:14

For I will be unto ['Ep̄rayim](../../strongs/h/h669.md) as a [šāḥal](../../strongs/h/h7826.md), and as a [kephiyr](../../strongs/h/h3715.md) to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md): I, even I, will [taraph](../../strongs/h/h2963.md) and go [yālaḵ](../../strongs/h/h3212.md); I will [nasa'](../../strongs/h/h5375.md), and none shall [natsal](../../strongs/h/h5337.md) him.

<a name="hosea_5_15"></a>Hosea 5:15

I will [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) to my [maqowm](../../strongs/h/h4725.md), till they acknowledge their ['asham](../../strongs/h/h816.md), and [bāqaš](../../strongs/h/h1245.md) my [paniym](../../strongs/h/h6440.md): in their [tsar](../../strongs/h/h6862.md) they will [šāḥar](../../strongs/h/h7836.md) me.

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 4](hosea_4.md) - [Hosea 6](hosea_6.md)