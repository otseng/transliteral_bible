# [Hosea 9](https://www.blueletterbible.org/kjv/hosea/9)

<a name="hosea_9_1"></a>Hosea 9:1

[samach](../../strongs/h/h8055.md) not, O [Yisra'el](../../strongs/h/h3478.md), for [gîl](../../strongs/h/h1524.md), as other ['am](../../strongs/h/h5971.md): for thou hast [zānâ](../../strongs/h/h2181.md) from thy ['Elohiym](../../strongs/h/h430.md), thou hast ['ahab](../../strongs/h/h157.md) an ['eṯnan](../../strongs/h/h868.md) upon every [gōren](../../strongs/h/h1637.md) [dagan](../../strongs/h/h1715.md).

<a name="hosea_9_2"></a>Hosea 9:2

The [gōren](../../strongs/h/h1637.md) and the [yeqeḇ](../../strongs/h/h3342.md) shall not [ra'ah](../../strongs/h/h7462.md) them, and the [tiyrowsh](../../strongs/h/h8492.md) shall [kāḥaš](../../strongs/h/h3584.md) in her.

<a name="hosea_9_3"></a>Hosea 9:3

They shall not [yashab](../../strongs/h/h3427.md) in [Yĕhovah](../../strongs/h/h3068.md) ['erets](../../strongs/h/h776.md); but ['Ep̄rayim](../../strongs/h/h669.md) shall [shuwb](../../strongs/h/h7725.md) to [Mitsrayim](../../strongs/h/h4714.md), and they shall ['akal](../../strongs/h/h398.md) [tame'](../../strongs/h/h2931.md) things in ['Aššûr](../../strongs/h/h804.md).

<a name="hosea_9_4"></a>Hosea 9:4

They shall not [nacak](../../strongs/h/h5258.md) [yayin](../../strongs/h/h3196.md) to [Yĕhovah](../../strongs/h/h3068.md), neither shall they be [ʿāraḇ](../../strongs/h/h6149.md) unto him: their [zebach](../../strongs/h/h2077.md) shall be unto them as the [lechem](../../strongs/h/h3899.md) of ['aven](../../strongs/h/h205.md); all that ['akal](../../strongs/h/h398.md) thereof shall be [ṭāmē'](../../strongs/h/h2930.md): for their [lechem](../../strongs/h/h3899.md) for their [nephesh](../../strongs/h/h5315.md) shall not [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_9_5"></a>Hosea 9:5

What will ye ['asah](../../strongs/h/h6213.md) in the [môʿēḏ](../../strongs/h/h4150.md) [yowm](../../strongs/h/h3117.md), and in the [yowm](../../strongs/h/h3117.md) of the [ḥāḡ](../../strongs/h/h2282.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="hosea_9_6"></a>Hosea 9:6

For, lo, they are [halak](../../strongs/h/h1980.md) because of [shod](../../strongs/h/h7701.md): [Mitsrayim](../../strongs/h/h4714.md) shall [qāḇaṣ](../../strongs/h/h6908.md) them, [Mōp̄](../../strongs/h/h4644.md) shall [qāḇar](../../strongs/h/h6912.md) them: the [maḥmāḏ](../../strongs/h/h4261.md) for their [keceph](../../strongs/h/h3701.md), [qimmôš](../../strongs/h/h7057.md) shall [yarash](../../strongs/h/h3423.md) them: [ḥôaḥ](../../strongs/h/h2336.md) shall be in their ['ohel](../../strongs/h/h168.md).

<a name="hosea_9_7"></a>Hosea 9:7

The [yowm](../../strongs/h/h3117.md) of [pᵊqudâ](../../strongs/h/h6486.md) are [bow'](../../strongs/h/h935.md), the [yowm](../../strongs/h/h3117.md) of [šillûm](../../strongs/h/h7966.md) are [bow'](../../strongs/h/h935.md); [Yisra'el](../../strongs/h/h3478.md) shall [yada'](../../strongs/h/h3045.md) it: the [nāḇî'](../../strongs/h/h5030.md) is an ['ĕvîl](../../strongs/h/h191.md), the [ruwach](../../strongs/h/h7307.md) ['iysh](../../strongs/h/h376.md) is [šāḡaʿ](../../strongs/h/h7696.md), for the [rōḇ](../../strongs/h/h7230.md) of thine ['avon](../../strongs/h/h5771.md), and the [rab](../../strongs/h/h7227.md) [maśṭēmâ](../../strongs/h/h4895.md).

<a name="hosea_9_8"></a>Hosea 9:8

The [tsaphah](../../strongs/h/h6822.md) of ['Ep̄rayim](../../strongs/h/h669.md) was with my ['Elohiym](../../strongs/h/h430.md): but the [nāḇî'](../../strongs/h/h5030.md) is a [paḥ](../../strongs/h/h6341.md) of a [yāqôš](../../strongs/h/h3352.md) in all his [derek](../../strongs/h/h1870.md), and [maśṭēmâ](../../strongs/h/h4895.md) in the [bayith](../../strongs/h/h1004.md) of his ['Elohiym](../../strongs/h/h430.md).

<a name="hosea_9_9"></a>Hosea 9:9

They have [ʿāmaq](../../strongs/h/h6009.md) [shachath](../../strongs/h/h7843.md) themselves, as in the [yowm](../../strongs/h/h3117.md) of [giḇʿâ](../../strongs/h/h1390.md): therefore he will [zakar](../../strongs/h/h2142.md) their ['avon](../../strongs/h/h5771.md), he will [paqad](../../strongs/h/h6485.md) their [chatta'ath](../../strongs/h/h2403.md).

<a name="hosea_9_10"></a>Hosea 9:10

I [māṣā'](../../strongs/h/h4672.md) [Yisra'el](../../strongs/h/h3478.md) like [ʿēnāḇ](../../strongs/h/h6025.md) in the [midbar](../../strongs/h/h4057.md); I [ra'ah](../../strongs/h/h7200.md) your ['ab](../../strongs/h/h1.md) as the [bikûrâ](../../strongs/h/h1063.md) in the [tĕ'en](../../strongs/h/h8384.md) at her [re'shiyth](../../strongs/h/h7225.md): but they [bow'](../../strongs/h/h935.md) to [Baʿal pᵊʿôr](../../strongs/h/h1187.md), and [nāzar](../../strongs/h/h5144.md) themselves unto that [bšeṯ](../../strongs/h/h1322.md); and their [šiqqûṣ](../../strongs/h/h8251.md) were according as they ['ahab](../../strongs/h/h157.md).

<a name="hosea_9_11"></a>Hosea 9:11

As for ['Ep̄rayim](../../strongs/h/h669.md), their [kabowd](../../strongs/h/h3519.md) shall ['uwph](../../strongs/h/h5774.md) like a [ʿôp̄](../../strongs/h/h5775.md), from the [yalad](../../strongs/h/h3205.md), and from the [beten](../../strongs/h/h990.md), and from the [herown](../../strongs/h/h2032.md).

<a name="hosea_9_12"></a>Hosea 9:12

Though they [gāḏal](../../strongs/h/h1431.md) their [ben](../../strongs/h/h1121.md), yet will I [šāḵōl](../../strongs/h/h7921.md) them, that there shall not be an ['āḏām](../../strongs/h/h120.md) left: yea, ['owy](../../strongs/h/h188.md) also to them when I [cuwr](../../strongs/h/h5493.md) from them!

<a name="hosea_9_13"></a>Hosea 9:13

['Ep̄rayim](../../strongs/h/h669.md), as I [ra'ah](../../strongs/h/h7200.md) [Ṣōr](../../strongs/h/h6865.md), is [šāṯal](../../strongs/h/h8362.md) in a [nāvê](../../strongs/h/h5116.md): but ['Ep̄rayim](../../strongs/h/h669.md) shall [yāṣā'](../../strongs/h/h3318.md) his [ben](../../strongs/h/h1121.md) to the [harag](../../strongs/h/h2026.md).

<a name="hosea_9_14"></a>Hosea 9:14

[nathan](../../strongs/h/h5414.md) them, [Yĕhovah](../../strongs/h/h3068.md): what wilt thou [nathan](../../strongs/h/h5414.md)? [nathan](../../strongs/h/h5414.md) them a [šāḵōl](../../strongs/h/h7921.md) [reḥem](../../strongs/h/h7358.md) and [ṣāmaq](../../strongs/h/h6784.md) [šaḏ](../../strongs/h/h7699.md).

<a name="hosea_9_15"></a>Hosea 9:15

All their [ra'](../../strongs/h/h7451.md) is in [Gilgāl](../../strongs/h/h1537.md): for there I [sane'](../../strongs/h/h8130.md) them: for the [rōaʿ](../../strongs/h/h7455.md) of their [maʿălāl](../../strongs/h/h4611.md) I will [gāraš](../../strongs/h/h1644.md) them of mine [bayith](../../strongs/h/h1004.md), I will ['ahăḇâ](../../strongs/h/h160.md) them no more: all their [śar](../../strongs/h/h8269.md) are [sārar](../../strongs/h/h5637.md).

<a name="hosea_9_16"></a>Hosea 9:16

['Ep̄rayim](../../strongs/h/h669.md) is [nakah](../../strongs/h/h5221.md), their [šereš](../../strongs/h/h8328.md) is [yāḇēš](../../strongs/h/h3001.md), they shall ['asah](../../strongs/h/h6213.md) no [pĕriy](../../strongs/h/h6529.md): yea, though they [yalad](../../strongs/h/h3205.md), yet will I [muwth](../../strongs/h/h4191.md) even the [maḥmāḏ](../../strongs/h/h4261.md) of their [beten](../../strongs/h/h990.md).

<a name="hosea_9_17"></a>Hosea 9:17

My ['Elohiym](../../strongs/h/h430.md) will [mā'as](../../strongs/h/h3988.md) them, because they did not [shama'](../../strongs/h/h8085.md) unto him: and they shall be [nāḏaḏ](../../strongs/h/h5074.md) among the [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 8](hosea_8.md) - [Hosea 10](hosea_10.md)