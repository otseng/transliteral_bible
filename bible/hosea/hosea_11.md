# [Hosea 11](https://www.blueletterbible.org/kjv/hosea/11)

<a name="hosea_11_1"></a>Hosea 11:1

When [Yisra'el](../../strongs/h/h3478.md) was a [naʿar](../../strongs/h/h5288.md), then I ['ahab](../../strongs/h/h157.md) him, and [qara'](../../strongs/h/h7121.md) my [ben](../../strongs/h/h1121.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="hosea_11_2"></a>Hosea 11:2

As they [qara'](../../strongs/h/h7121.md) them, so they [halak](../../strongs/h/h1980.md) from [paniym](../../strongs/h/h6440.md): they [zabach](../../strongs/h/h2076.md) unto [BaʿAl](../../strongs/h/h1168.md), and [qāṭar](../../strongs/h/h6999.md) to [pāsîl](../../strongs/h/h6456.md).

<a name="hosea_11_3"></a>Hosea 11:3

I [tirgēl](../../strongs/h/h8637.md) ['Ep̄rayim](../../strongs/h/h669.md) also to [tirgēl](../../strongs/h/h8637.md), [laqach](../../strongs/h/h3947.md) them by their [zerowa'](../../strongs/h/h2220.md); but they [yada'](../../strongs/h/h3045.md) not that I [rapha'](../../strongs/h/h7495.md) them.

<a name="hosea_11_4"></a>Hosea 11:4

I [mashak](../../strongs/h/h4900.md) them with [chebel](../../strongs/h/h2256.md) of an ['āḏām](../../strongs/h/h120.md), with [ʿăḇōṯ](../../strongs/h/h5688.md) of ['ahăḇâ](../../strongs/h/h160.md): and I was to them as they that [ruwm](../../strongs/h/h7311.md) the [ʿōl](../../strongs/h/h5923.md) on their [lᵊḥî](../../strongs/h/h3895.md), and I [natah](../../strongs/h/h5186.md) ['akal](../../strongs/h/h398.md) unto them.

<a name="hosea_11_5"></a>Hosea 11:5

He shall not [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), but the ['Aššûr](../../strongs/h/h804.md) shall be his [melek](../../strongs/h/h4428.md), because they [mā'ēn](../../strongs/h/h3985.md) to [shuwb](../../strongs/h/h7725.md).

<a name="hosea_11_6"></a>Hosea 11:6

And the [chereb](../../strongs/h/h2719.md) shall [chuwl](../../strongs/h/h2342.md) on his [ʿîr](../../strongs/h/h5892.md), and shall [kalah](../../strongs/h/h3615.md) his [baḏ](../../strongs/h/h905.md), and ['akal](../../strongs/h/h398.md) them, because of their own [mow'etsah](../../strongs/h/h4156.md).

<a name="hosea_11_7"></a>Hosea 11:7

And my ['am](../../strongs/h/h5971.md) are [tālā'](../../strongs/h/h8511.md) to [mᵊšûḇâ](../../strongs/h/h4878.md) from me: though they [qara'](../../strongs/h/h7121.md) them to the most [ʿal](../../strongs/h/h5920.md), none at [yaḥaḏ](../../strongs/h/h3162.md) would [ruwm](../../strongs/h/h7311.md) him.

<a name="hosea_11_8"></a>Hosea 11:8

How shall I [nathan](../../strongs/h/h5414.md) thee, ['Ep̄rayim](../../strongs/h/h669.md)? how shall I [māḡan](../../strongs/h/h4042.md) thee, [Yisra'el](../../strongs/h/h3478.md)? how shall I [nathan](../../strongs/h/h5414.md) thee as ['Aḏmâ](../../strongs/h/h126.md)? how shall I [śûm](../../strongs/h/h7760.md) thee as [Ṣᵊḇā'îm](../../strongs/h/h6636.md)? mine [leb](../../strongs/h/h3820.md) is [hāp̄aḵ](../../strongs/h/h2015.md) within me, my [niḥum](../../strongs/h/h5150.md) are [kāmar](../../strongs/h/h3648.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="hosea_11_9"></a>Hosea 11:9

I will not ['asah](../../strongs/h/h6213.md) the [charown](../../strongs/h/h2740.md) of mine ['aph](../../strongs/h/h639.md), I will not [shuwb](../../strongs/h/h7725.md) to [shachath](../../strongs/h/h7843.md) ['Ep̄rayim](../../strongs/h/h669.md): for I am ['el](../../strongs/h/h410.md), and not ['iysh](../../strongs/h/h376.md); the [qadowsh](../../strongs/h/h6918.md) in the [qereḇ](../../strongs/h/h7130.md) of thee: and I will not [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md).

<a name="hosea_11_10"></a>Hosea 11:10

They shall [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md): he shall [šā'aḡ](../../strongs/h/h7580.md) like an ['ariy](../../strongs/h/h738.md): when he shall [šā'aḡ](../../strongs/h/h7580.md), then the [ben](../../strongs/h/h1121.md) shall [ḥārēḏ](../../strongs/h/h2729.md) from the [yam](../../strongs/h/h3220.md).

<a name="hosea_11_11"></a>Hosea 11:11

They shall [ḥārēḏ](../../strongs/h/h2729.md) as a [tsippowr](../../strongs/h/h6833.md) out of [Mitsrayim](../../strongs/h/h4714.md), and as a [yônâ](../../strongs/h/h3123.md) out of the ['erets](../../strongs/h/h776.md) of ['Aššûr](../../strongs/h/h804.md): and I will [yashab](../../strongs/h/h3427.md) them in their [bayith](../../strongs/h/h1004.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_11_12"></a>Hosea 11:12

['Ep̄rayim](../../strongs/h/h669.md) [cabab](../../strongs/h/h5437.md) me about with [kaḥaš](../../strongs/h/h3585.md), and the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) with [mirmah](../../strongs/h/h4820.md): but [Yehuwdah](../../strongs/h/h3063.md) yet [rûḏ](../../strongs/h/h7300.md) with ['el](../../strongs/h/h410.md), and is ['aman](../../strongs/h/h539.md) with the [qadowsh](../../strongs/h/h6918.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 10](hosea_10.md) - [Hosea 12](hosea_12.md)