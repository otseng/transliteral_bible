# [Hosea 14](https://www.blueletterbible.org/kjv/hosea/14)

<a name="hosea_14_1"></a>Hosea 14:1

O [Yisra'el](../../strongs/h/h3478.md), [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md); for thou hast [kashal](../../strongs/h/h3782.md) by thine ['avon](../../strongs/h/h5771.md).

<a name="hosea_14_2"></a>Hosea 14:2

[laqach](../../strongs/h/h3947.md) with you [dabar](../../strongs/h/h1697.md), and [shuwb](../../strongs/h/h7725.md) to [Yĕhovah](../../strongs/h/h3068.md): ['āmar](../../strongs/h/h559.md) unto him, [nasa'](../../strongs/h/h5375.md) all ['avon](../../strongs/h/h5771.md), and [laqach](../../strongs/h/h3947.md) us [towb](../../strongs/h/h2896.md): so will we [shalam](../../strongs/h/h7999.md) the [par](../../strongs/h/h6499.md) of our [saphah](../../strongs/h/h8193.md).

<a name="hosea_14_3"></a>Hosea 14:3

['Aššûr](../../strongs/h/h804.md) shall not [yasha'](../../strongs/h/h3467.md) us; we will not [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md): neither will we ['āmar](../../strongs/h/h559.md) any more to the [ma'aseh](../../strongs/h/h4639.md) of our [yad](../../strongs/h/h3027.md), Ye are our ['Elohiym](../../strongs/h/h430.md): for in thee the [yathowm](../../strongs/h/h3490.md) findeth [racham](../../strongs/h/h7355.md).

<a name="hosea_14_4"></a>Hosea 14:4

I will [rapha'](../../strongs/h/h7495.md) their [mᵊšûḇâ](../../strongs/h/h4878.md), I will ['ahab](../../strongs/h/h157.md) them [nᵊḏāḇâ](../../strongs/h/h5071.md): for mine ['aph](../../strongs/h/h639.md) is [shuwb](../../strongs/h/h7725.md) from him.

<a name="hosea_14_5"></a>Hosea 14:5

I will be as the [ṭal](../../strongs/h/h2919.md) unto [Yisra'el](../../strongs/h/h3478.md): he shall [pāraḥ](../../strongs/h/h6524.md) as the [šûšan](../../strongs/h/h7799.md), and [nakah](../../strongs/h/h5221.md) his [šereš](../../strongs/h/h8328.md) as [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="hosea_14_6"></a>Hosea 14:6

His [yôneqeṯ](../../strongs/h/h3127.md) shall [yālaḵ](../../strongs/h/h3212.md), and his [howd](../../strongs/h/h1935.md) shall be as the [zayiṯ](../../strongs/h/h2132.md), and his [rêaḥ](../../strongs/h/h7381.md) as [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="hosea_14_7"></a>Hosea 14:7

They that [yashab](../../strongs/h/h3427.md) under his [ṣēl](../../strongs/h/h6738.md) shall [shuwb](../../strongs/h/h7725.md); they shall [ḥāyâ](../../strongs/h/h2421.md) as the [dagan](../../strongs/h/h1715.md), and [pāraḥ](../../strongs/h/h6524.md) as the [gep̄en](../../strongs/h/h1612.md): the [zeker](../../strongs/h/h2143.md) thereof shall be as the [yayin](../../strongs/h/h3196.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="hosea_14_8"></a>Hosea 14:8

['Ep̄rayim](../../strongs/h/h669.md) shall say, What have I to do any more with [ʿāṣāḇ](../../strongs/h/h6091.md)? I have ['anah](../../strongs/h/h6030.md) him, and [šûr](../../strongs/h/h7789.md) him: I am like a [raʿănān](../../strongs/h/h7488.md) fir [bᵊrôš](../../strongs/h/h1265.md). From me is thy [pĕriy](../../strongs/h/h6529.md) [māṣā'](../../strongs/h/h4672.md).

<a name="hosea_14_9"></a>Hosea 14:9

Who is [ḥāḵām](../../strongs/h/h2450.md), and he shall [bîn](../../strongs/h/h995.md) these things? [bîn](../../strongs/h/h995.md), and he shall [yada'](../../strongs/h/h3045.md) them? for the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md) are [yashar](../../strongs/h/h3477.md), and the [tsaddiyq](../../strongs/h/h6662.md) shall [yālaḵ](../../strongs/h/h3212.md) in them: but the [pāšaʿ](../../strongs/h/h6586.md) shall [kashal](../../strongs/h/h3782.md) therein.

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 13](hosea_13.md)