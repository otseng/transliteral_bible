# [Hosea 12](https://www.blueletterbible.org/kjv/hosea/12)

<a name="hosea_12_1"></a>Hosea 12:1

['Ep̄rayim](../../strongs/h/h669.md) [ra'ah](../../strongs/h/h7462.md) on [ruwach](../../strongs/h/h7307.md), and [radaph](../../strongs/h/h7291.md) after the [qāḏîm](../../strongs/h/h6921.md): he [yowm](../../strongs/h/h3117.md) [rabah](../../strongs/h/h7235.md) [kazab](../../strongs/h/h3577.md) and [shod](../../strongs/h/h7701.md); and they do [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with the ['Aššûr](../../strongs/h/h804.md), and [šemen](../../strongs/h/h8081.md) is [yāḇal](../../strongs/h/h2986.md) into [Mitsrayim](../../strongs/h/h4714.md).

<a name="hosea_12_2"></a>Hosea 12:2

[Yĕhovah](../../strongs/h/h3068.md) hath also a [rîḇ](../../strongs/h/h7379.md) with [Yehuwdah](../../strongs/h/h3063.md), and will [paqad](../../strongs/h/h6485.md) [Ya'aqob](../../strongs/h/h3290.md) according to his [derek](../../strongs/h/h1870.md); according to his [maʿălāl](../../strongs/h/h4611.md) will he [shuwb](../../strongs/h/h7725.md) him.

<a name="hosea_12_3"></a>Hosea 12:3

He took his ['ach](../../strongs/h/h251.md) by the [ʿāqaḇ](../../strongs/h/h6117.md) in the [beten](../../strongs/h/h990.md), and by his ['ôn](../../strongs/h/h202.md) he had [śārâ](../../strongs/h/h8280.md) with ['Elohiym](../../strongs/h/h430.md):

<a name="hosea_12_4"></a>Hosea 12:4

Yea, he had [śûr](../../strongs/h/h7786.md) over the [mal'ak](../../strongs/h/h4397.md), and [yakol](../../strongs/h/h3201.md): he [bāḵâ](../../strongs/h/h1058.md), and made [chanan](../../strongs/h/h2603.md) unto him: he [māṣā'](../../strongs/h/h4672.md) him in [Bêṯ-'ēl](../../strongs/h/h1008.md), and there he [dabar](../../strongs/h/h1696.md) with us;

<a name="hosea_12_5"></a>Hosea 12:5

Even [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md); [Yĕhovah](../../strongs/h/h3068.md) is his [zeker](../../strongs/h/h2143.md).

<a name="hosea_12_6"></a>Hosea 12:6

Therefore [shuwb](../../strongs/h/h7725.md) thou to thy ['Elohiym](../../strongs/h/h430.md): [shamar](../../strongs/h/h8104.md) [checed](../../strongs/h/h2617.md) and [mishpat](../../strongs/h/h4941.md), and [qāvâ](../../strongs/h/h6960.md) on thy ['Elohiym](../../strongs/h/h430.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="hosea_12_7"></a>Hosea 12:7

He is a [kĕna'an](../../strongs/h/h3667.md), the [mō'znayim](../../strongs/h/h3976.md) of [mirmah](../../strongs/h/h4820.md) are in his [yad](../../strongs/h/h3027.md): he ['ahab](../../strongs/h/h157.md) to [ʿāšaq](../../strongs/h/h6231.md).

<a name="hosea_12_8"></a>Hosea 12:8

And ['Ep̄rayim](../../strongs/h/h669.md) ['āmar](../../strongs/h/h559.md), Yet I am become [ʿāšar](../../strongs/h/h6238.md), I have [māṣā'](../../strongs/h/h4672.md) me ['ôn](../../strongs/h/h202.md): in all my [yᵊḡîaʿ](../../strongs/h/h3018.md) they shall [māṣā'](../../strongs/h/h4672.md) none ['avon](../../strongs/h/h5771.md) in me that were [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="hosea_12_9"></a>Hosea 12:9

And I that am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) will yet make thee to [yashab](../../strongs/h/h3427.md) in ['ohel](../../strongs/h/h168.md), as in the [yowm](../../strongs/h/h3117.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="hosea_12_10"></a>Hosea 12:10

I have also [dabar](../../strongs/h/h1696.md) by the [nāḇî'](../../strongs/h/h5030.md), and I have [rabah](../../strongs/h/h7235.md) [ḥāzôn](../../strongs/h/h2377.md), and used [dāmâ](../../strongs/h/h1819.md), by the [yad](../../strongs/h/h3027.md) of the [nāḇî'](../../strongs/h/h5030.md).

<a name="hosea_12_11"></a>Hosea 12:11

Is there ['aven](../../strongs/h/h205.md) in [Gilʿāḏ](../../strongs/h/h1568.md)? surely they are [shav'](../../strongs/h/h7723.md): they [zabach](../../strongs/h/h2076.md) [showr](../../strongs/h/h7794.md) in [Gilgāl](../../strongs/h/h1537.md); yea, their [mizbeach](../../strongs/h/h4196.md) are as [gal](../../strongs/h/h1530.md) in the [telem](../../strongs/h/h8525.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="hosea_12_12"></a>Hosea 12:12

And [Ya'aqob](../../strongs/h/h3290.md) [bāraḥ](../../strongs/h/h1272.md) into the [sadeh](../../strongs/h/h7704.md) of ['Ărām](../../strongs/h/h758.md), and [Yisra'el](../../strongs/h/h3478.md) ['abad](../../strongs/h/h5647.md) for an ['ishshah](../../strongs/h/h802.md), and for an ['ishshah](../../strongs/h/h802.md) he [shamar](../../strongs/h/h8104.md).

<a name="hosea_12_13"></a>Hosea 12:13

And by a [nāḇî'](../../strongs/h/h5030.md) [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md), and by a [nāḇî'](../../strongs/h/h5030.md) was he [shamar](../../strongs/h/h8104.md).

<a name="hosea_12_14"></a>Hosea 12:14

['Ep̄rayim](../../strongs/h/h669.md) [kāʿas](../../strongs/h/h3707.md) him [tamrûrîm](../../strongs/h/h8563.md): therefore shall he [nāṭaš](../../strongs/h/h5203.md) his [dam](../../strongs/h/h1818.md) upon him, and his [cherpah](../../strongs/h/h2781.md) shall his ['adown](../../strongs/h/h113.md) [shuwb](../../strongs/h/h7725.md) unto him.

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 11](hosea_11.md) - [Hosea 13](hosea_13.md)