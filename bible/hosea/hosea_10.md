# [Hosea 10](https://www.blueletterbible.org/kjv/hosea/10)

<a name="hosea_10_1"></a>Hosea 10:1

[Yisra'el](../../strongs/h/h3478.md) is a [bāqaq](../../strongs/h/h1238.md) [gep̄en](../../strongs/h/h1612.md), he [šāvâ](../../strongs/h/h7737.md) [pĕriy](../../strongs/h/h6529.md) unto himself: according to the [rōḇ](../../strongs/h/h7230.md) of his [pĕriy](../../strongs/h/h6529.md) he hath [rabah](../../strongs/h/h7235.md) the [mizbeach](../../strongs/h/h4196.md); according to the [towb](../../strongs/h/h2896.md) of his ['erets](../../strongs/h/h776.md) they have made [ṭôḇ](../../strongs/h/h2895.md) [maṣṣēḇâ](../../strongs/h/h4676.md).

<a name="hosea_10_2"></a>Hosea 10:2

Their [leb](../../strongs/h/h3820.md) is [chalaq](../../strongs/h/h2505.md); now shall they be ['asham](../../strongs/h/h816.md): he shall [ʿārap̄](../../strongs/h/h6202.md) their [mizbeach](../../strongs/h/h4196.md), he shall [shadad](../../strongs/h/h7703.md) their [maṣṣēḇâ](../../strongs/h/h4676.md).

<a name="hosea_10_3"></a>Hosea 10:3

For now they shall ['āmar](../../strongs/h/h559.md), We have no [melek](../../strongs/h/h4428.md), because we [yare'](../../strongs/h/h3372.md) not [Yĕhovah](../../strongs/h/h3068.md); what then should a [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) to us?

<a name="hosea_10_4"></a>Hosea 10:4

They have [dabar](../../strongs/h/h1696.md) [dabar](../../strongs/h/h1697.md), ['ālâ](../../strongs/h/h422.md) [shav'](../../strongs/h/h7723.md) in [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md): thus [mishpat](../../strongs/h/h4941.md) [pāraḥ](../../strongs/h/h6524.md) as [rō'š](../../strongs/h/h7219.md) in the [telem](../../strongs/h/h8525.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="hosea_10_5"></a>Hosea 10:5

The [šāḵēn](../../strongs/h/h7934.md) of [Šōmrôn](../../strongs/h/h8111.md) shall [guwr](../../strongs/h/h1481.md) because of the [ʿeḡlâ](../../strongs/h/h5697.md) of [Bêṯ 'Āven](../../strongs/h/h1007.md): for the ['am](../../strongs/h/h5971.md) thereof shall ['āḇal](../../strongs/h/h56.md) over it, and the [kōmer](../../strongs/h/h3649.md) thereof that [giyl](../../strongs/h/h1523.md) on it, for the [kabowd](../../strongs/h/h3519.md) thereof, because it is [gālâ](../../strongs/h/h1540.md) from it.

<a name="hosea_10_6"></a>Hosea 10:6

It shall be also [yāḇal](../../strongs/h/h2986.md) unto ['Aššûr](../../strongs/h/h804.md) for a [minchah](../../strongs/h/h4503.md) to [melek](../../strongs/h/h4428.md) [yārēḇ](../../strongs/h/h3377.md): ['Ep̄rayim](../../strongs/h/h669.md) shall [laqach](../../strongs/h/h3947.md) [bāšnâ](../../strongs/h/h1317.md), and [Yisra'el](../../strongs/h/h3478.md) shall be [buwsh](../../strongs/h/h954.md) of his own ['etsah](../../strongs/h/h6098.md).

<a name="hosea_10_7"></a>Hosea 10:7

As for [Šōmrôn](../../strongs/h/h8111.md), her [melek](../../strongs/h/h4428.md) is [damah](../../strongs/h/h1820.md) as the [qeṣep̄](../../strongs/h/h7110.md) [paniym](../../strongs/h/h6440.md) the [mayim](../../strongs/h/h4325.md).

<a name="hosea_10_8"></a>Hosea 10:8

The [bāmâ](../../strongs/h/h1116.md) also of ['Āven](../../strongs/h/h206.md), the [chatta'ath](../../strongs/h/h2403.md) of [Yisra'el](../../strongs/h/h3478.md), shall be [šāmaḏ](../../strongs/h/h8045.md): the [qowts](../../strongs/h/h6975.md) and the [dardar](../../strongs/h/h1863.md) shall [ʿālâ](../../strongs/h/h5927.md) on their [mizbeach](../../strongs/h/h4196.md); and they shall ['āmar](../../strongs/h/h559.md) to the [har](../../strongs/h/h2022.md), [kāsâ](../../strongs/h/h3680.md) us; and to the [giḇʿâ](../../strongs/h/h1389.md), [naphal](../../strongs/h/h5307.md) on us.

<a name="hosea_10_9"></a>Hosea 10:9

O [Yisra'el](../../strongs/h/h3478.md), thou hast [chata'](../../strongs/h/h2398.md) from the [yowm](../../strongs/h/h3117.md) of [giḇʿâ](../../strongs/h/h1390.md): there they ['amad](../../strongs/h/h5975.md): the [milḥāmâ](../../strongs/h/h4421.md) in [giḇʿâ](../../strongs/h/h1390.md) against the [ben](../../strongs/h/h1121.md) of [ʿAlvâ](../../strongs/h/h5932.md) did not [nāśaḡ](../../strongs/h/h5381.md) them.

<a name="hosea_10_10"></a>Hosea 10:10

It is in my ['aûâ](../../strongs/h/h185.md) that I should [yacar](../../strongs/h/h3256.md) them; and the ['am](../../strongs/h/h5971.md) shall be ['āsap̄](../../strongs/h/h622.md) against them, when they shall ['āsar](../../strongs/h/h631.md) themselves in their two ['ayin](../../strongs/h/h5869.md).

<a name="hosea_10_11"></a>Hosea 10:11

And ['Ep̄rayim](../../strongs/h/h669.md) is as an [ʿeḡlâ](../../strongs/h/h5697.md) that is [lamad](../../strongs/h/h3925.md), and ['ahab](../../strongs/h/h157.md) to [dûš](../../strongs/h/h1758.md); but I ['abar](../../strongs/h/h5674.md) upon her [ṭûḇ](../../strongs/h/h2898.md) [ṣaûā'r](../../strongs/h/h6677.md): I will make ['Ep̄rayim](../../strongs/h/h669.md) to [rāḵaḇ](../../strongs/h/h7392.md); [Yehuwdah](../../strongs/h/h3063.md) shall [ḥāraš](../../strongs/h/h2790.md), and [Ya'aqob](../../strongs/h/h3290.md) shall [śāḏaḏ](../../strongs/h/h7702.md).

<a name="hosea_10_12"></a>Hosea 10:12

[zāraʿ](../../strongs/h/h2232.md) to yourselves in [tsedaqah](../../strongs/h/h6666.md), [qāṣar](../../strongs/h/h7114.md) [peh](../../strongs/h/h6310.md) [checed](../../strongs/h/h2617.md); [nîr](../../strongs/h/h5214.md) your [nîr](../../strongs/h/h5215.md): for it is [ʿēṯ](../../strongs/h/h6256.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), till he [bow'](../../strongs/h/h935.md) and [yārâ](../../strongs/h/h3384.md) [tsedeq](../../strongs/h/h6664.md) upon you.

<a name="hosea_10_13"></a>Hosea 10:13

Ye have [ḥāraš](../../strongs/h/h2790.md) [resha'](../../strongs/h/h7562.md), ye have [qāṣar](../../strongs/h/h7114.md) ['evel](../../strongs/h/h5766.md); ye have ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of [kaḥaš](../../strongs/h/h3585.md): because thou didst [batach](../../strongs/h/h982.md) in thy [derek](../../strongs/h/h1870.md), in the [rōḇ](../../strongs/h/h7230.md) of thy [gibôr](../../strongs/h/h1368.md).

<a name="hosea_10_14"></a>Hosea 10:14

Therefore shall a [shā'ôn](../../strongs/h/h7588.md) [quwm](../../strongs/h/h6965.md) among thy ['am](../../strongs/h/h5971.md), and all thy [miḇṣār](../../strongs/h/h4013.md) shall be [shadad](../../strongs/h/h7703.md), as [Šalman](../../strongs/h/h8020.md) [shod](../../strongs/h/h7701.md) [Bêṯ 'Arbᵊ'Ēl](../../strongs/h/h1009.md) in the [yowm](../../strongs/h/h3117.md) of [milḥāmâ](../../strongs/h/h4421.md): the ['em](../../strongs/h/h517.md) was [rāṭaš](../../strongs/h/h7376.md) upon her [ben](../../strongs/h/h1121.md).

<a name="hosea_10_15"></a>Hosea 10:15

So shall [Bêṯ-'ēl](../../strongs/h/h1008.md) ['asah](../../strongs/h/h6213.md) unto you [paniym](../../strongs/h/h6440.md) of your [ra'](../../strongs/h/h7451.md) [ra'](../../strongs/h/h7451.md): in a [šaḥar](../../strongs/h/h7837.md) shall the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [damah](../../strongs/h/h1820.md) be cut [damah](../../strongs/h/h1820.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 9](hosea_9.md) - [Hosea 11](hosea_11.md)