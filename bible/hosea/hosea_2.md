# [Hosea 2](https://www.blueletterbible.org/kjv/hosea/2)

<a name="hosea_2_1"></a>Hosea 2:1

['āmar](../../strongs/h/h559.md) ye unto your ['ach](../../strongs/h/h251.md), ['am](../../strongs/h/h5971.md); and to your ['āḥôṯ](../../strongs/h/h269.md), [racham](../../strongs/h/h7355.md).

<a name="hosea_2_2"></a>Hosea 2:2

[riyb](../../strongs/h/h7378.md) with your ['em](../../strongs/h/h517.md), [riyb](../../strongs/h/h7378.md): for she is not my ['ishshah](../../strongs/h/h802.md), neither am I her ['iysh](../../strongs/h/h376.md): let her therefore [cuwr](../../strongs/h/h5493.md) her [zᵊnûnîm](../../strongs/h/h2183.md) out of her [paniym](../../strongs/h/h6440.md), and her [na'ăp̄ûp̄îm](../../strongs/h/h5005.md) from between her [šaḏ](../../strongs/h/h7699.md);

<a name="hosea_2_3"></a>Hosea 2:3

Lest I [pāšaṭ](../../strongs/h/h6584.md) her ['arowm](../../strongs/h/h6174.md), and [yāṣaḡ](../../strongs/h/h3322.md) her as in the [yowm](../../strongs/h/h3117.md) that she was [yalad](../../strongs/h/h3205.md), and [śûm](../../strongs/h/h7760.md) her as a [midbar](../../strongs/h/h4057.md), and [shiyth](../../strongs/h/h7896.md) her like a [ṣîyâ](../../strongs/h/h6723.md) ['erets](../../strongs/h/h776.md), and [muwth](../../strongs/h/h4191.md) her with [ṣāmā'](../../strongs/h/h6772.md).

<a name="hosea_2_4"></a>Hosea 2:4

And I will not have [racham](../../strongs/h/h7355.md) upon her [ben](../../strongs/h/h1121.md); for they be the [ben](../../strongs/h/h1121.md) of [zᵊnûnîm](../../strongs/h/h2183.md).

<a name="hosea_2_5"></a>Hosea 2:5

For their ['em](../../strongs/h/h517.md) hath [zānâ](../../strongs/h/h2181.md): she that [harah](../../strongs/h/h2029.md) them hath done [yāḇēš](../../strongs/h/h3001.md): for she ['āmar](../../strongs/h/h559.md), I will [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) my ['ahab](../../strongs/h/h157.md), that [nathan](../../strongs/h/h5414.md) me my [lechem](../../strongs/h/h3899.md) and my [mayim](../../strongs/h/h4325.md), my [ṣemer](../../strongs/h/h6785.md) and my [pēšeṯ](../../strongs/h/h6593.md), mine [šemen](../../strongs/h/h8081.md) and my [šiqqûy](../../strongs/h/h8250.md).

<a name="hosea_2_6"></a>Hosea 2:6

Therefore, behold, I will [śûḵ](../../strongs/h/h7753.md) up thy [derek](../../strongs/h/h1870.md) with [sîr](../../strongs/h/h5518.md), and [gāḏar](../../strongs/h/h1443.md) a [gāḏēr](../../strongs/h/h1447.md), that she shall not [māṣā'](../../strongs/h/h4672.md) her [nāṯîḇ](../../strongs/h/h5410.md).

<a name="hosea_2_7"></a>Hosea 2:7

And she shall [radaph](../../strongs/h/h7291.md) after her ['ahab](../../strongs/h/h157.md), but she shall not [nāśaḡ](../../strongs/h/h5381.md) them; and she shall [bāqaš](../../strongs/h/h1245.md) them, but shall not [māṣā'](../../strongs/h/h4672.md) them: then shall she ['āmar](../../strongs/h/h559.md), I will [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) to my [ri'šôn](../../strongs/h/h7223.md) ['iysh](../../strongs/h/h376.md); for then was it [towb](../../strongs/h/h2896.md) with me than now.

<a name="hosea_2_8"></a>Hosea 2:8

For she did not [yada'](../../strongs/h/h3045.md) that I [nathan](../../strongs/h/h5414.md) her [dagan](../../strongs/h/h1715.md), and [tiyrowsh](../../strongs/h/h8492.md), and [yiṣhār](../../strongs/h/h3323.md), and [rabah](../../strongs/h/h7235.md) her [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), which they ['asah](../../strongs/h/h6213.md) for [BaʿAl](../../strongs/h/h1168.md).

<a name="hosea_2_9"></a>Hosea 2:9

Therefore will I [shuwb](../../strongs/h/h7725.md), and [laqach](../../strongs/h/h3947.md) my [dagan](../../strongs/h/h1715.md) in the [ʿēṯ](../../strongs/h/h6256.md) thereof, and my [tiyrowsh](../../strongs/h/h8492.md) in the [môʿēḏ](../../strongs/h/h4150.md) thereof, and will [natsal](../../strongs/h/h5337.md) my [ṣemer](../../strongs/h/h6785.md) and my [pēšeṯ](../../strongs/h/h6593.md) given to [kāsâ](../../strongs/h/h3680.md) her [ʿervâ](../../strongs/h/h6172.md).

<a name="hosea_2_10"></a>Hosea 2:10

And now will I [gālâ](../../strongs/h/h1540.md) her [naḇlûṯ](../../strongs/h/h5040.md) in the ['ayin](../../strongs/h/h5869.md) of her ['ahab](../../strongs/h/h157.md), and ['iysh](../../strongs/h/h376.md) shall [natsal](../../strongs/h/h5337.md) her out of mine [yad](../../strongs/h/h3027.md).

<a name="hosea_2_11"></a>Hosea 2:11

I will also cause all her [māśôś](../../strongs/h/h4885.md) to [shabath](../../strongs/h/h7673.md), her [ḥāḡ](../../strongs/h/h2282.md), her [ḥōḏeš](../../strongs/h/h2320.md), and her [shabbath](../../strongs/h/h7676.md), and all her [môʿēḏ](../../strongs/h/h4150.md).

<a name="hosea_2_12"></a>Hosea 2:12

And I will [šāmēm](../../strongs/h/h8074.md) her [gep̄en](../../strongs/h/h1612.md) and her [tĕ'en](../../strongs/h/h8384.md), whereof she hath ['āmar](../../strongs/h/h559.md), These are my ['eṯnâ](../../strongs/h/h866.md) that my ['ahab](../../strongs/h/h157.md) have [nathan](../../strongs/h/h5414.md) me: and I will [śûm](../../strongs/h/h7760.md) them a [yaʿar](../../strongs/h/h3293.md), and the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) shall ['akal](../../strongs/h/h398.md) them.

<a name="hosea_2_13"></a>Hosea 2:13

And I will [paqad](../../strongs/h/h6485.md) upon her the [yowm](../../strongs/h/h3117.md) of [BaʿAl](../../strongs/h/h1168.md), wherein she [qāṭar](../../strongs/h/h6999.md) to them, and she [ʿāḏâ](../../strongs/h/h5710.md) herself with her [nezem](../../strongs/h/h5141.md) and her [ḥelyâ](../../strongs/h/h2484.md), and she [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) her ['ahab](../../strongs/h/h157.md), and [shakach](../../strongs/h/h7911.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_2_14"></a>Hosea 2:14

Therefore, behold, I will [pāṯâ](../../strongs/h/h6601.md) her, and [yālaḵ](../../strongs/h/h3212.md) her into the [midbar](../../strongs/h/h4057.md), and [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto her.

<a name="hosea_2_15"></a>Hosea 2:15

And I will [nathan](../../strongs/h/h5414.md) her her [kerem](../../strongs/h/h3754.md) from thence, and the [ʿēmeq](../../strongs/h/h6010.md) of [ʿāḵôr](../../strongs/h/h5911.md) for a [peṯaḥ](../../strongs/h/h6607.md) of [tiqvâ](../../strongs/h/h8615.md): and she shall ['anah](../../strongs/h/h6030.md) there, as in the [yowm](../../strongs/h/h3117.md) of her [nāʿur](../../strongs/h/h5271.md), and as in the [yowm](../../strongs/h/h3117.md) when she [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="hosea_2_16"></a>Hosea 2:16

And it shall be at that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that thou shalt [qara'](../../strongs/h/h7121.md) me ['iysh](../../strongs/h/h376.md); and shalt [qara'](../../strongs/h/h7121.md) me no more [baʿllî](../../strongs/h/h1180.md).

<a name="hosea_2_17"></a>Hosea 2:17

For I will [cuwr](../../strongs/h/h5493.md) the [shem](../../strongs/h/h8034.md) of [BaʿAl](../../strongs/h/h1168.md) out of her [peh](../../strongs/h/h6310.md), and they shall no more be [zakar](../../strongs/h/h2142.md) by their [shem](../../strongs/h/h8034.md).

<a name="hosea_2_18"></a>Hosea 2:18

And in that [yowm](../../strongs/h/h3117.md) will I [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) for them with the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), and with the [ʿôp̄](../../strongs/h/h5775.md) of [shamayim](../../strongs/h/h8064.md), and with the [remeś](../../strongs/h/h7431.md) of the ['ăḏāmâ](../../strongs/h/h127.md): and I will [shabar](../../strongs/h/h7665.md) the [qesheth](../../strongs/h/h7198.md) and the [chereb](../../strongs/h/h2719.md) and the [milḥāmâ](../../strongs/h/h4421.md) out of the ['erets](../../strongs/h/h776.md), and will make them to [shakab](../../strongs/h/h7901.md) [betach](../../strongs/h/h983.md).

<a name="hosea_2_19"></a>Hosea 2:19

And I will ['āraś](../../strongs/h/h781.md) thee unto me ['owlam](../../strongs/h/h5769.md); yea, I will ['āraś](../../strongs/h/h781.md) thee unto me in [tsedeq](../../strongs/h/h6664.md), and in [mishpat](../../strongs/h/h4941.md), and in [checed](../../strongs/h/h2617.md), and in [raḥam](../../strongs/h/h7356.md).

<a name="hosea_2_20"></a>Hosea 2:20

I will even ['āraś](../../strongs/h/h781.md) thee unto me in ['ĕmûnâ](../../strongs/h/h530.md): and thou shalt [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="hosea_2_21"></a>Hosea 2:21

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), I will ['anah](../../strongs/h/h6030.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), I will ['anah](../../strongs/h/h6030.md) the [shamayim](../../strongs/h/h8064.md), and they shall ['anah](../../strongs/h/h6030.md) the ['erets](../../strongs/h/h776.md);

<a name="hosea_2_22"></a>Hosea 2:22

And the ['erets](../../strongs/h/h776.md) shall ['anah](../../strongs/h/h6030.md) the [dagan](../../strongs/h/h1715.md), and the [tiyrowsh](../../strongs/h/h8492.md), and the [yiṣhār](../../strongs/h/h3323.md); and they shall ['anah](../../strongs/h/h6030.md) [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="hosea_2_23"></a>Hosea 2:23

And I will [zāraʿ](../../strongs/h/h2232.md) her unto me in the ['erets](../../strongs/h/h776.md); and I will have [racham](../../strongs/h/h7355.md) upon her that had not obtained [racham](../../strongs/h/h7355.md) [Lō' ʿAmmî](../../strongs/h/h3818.md); and I will ['āmar](../../strongs/h/h559.md) to them which were not my ['am](../../strongs/h/h5971.md), Thou art my ['am](../../strongs/h/h5971.md); and they shall ['āmar](../../strongs/h/h559.md), Thou art my ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 1](hosea_1.md) - [Hosea 3](hosea_3.md)