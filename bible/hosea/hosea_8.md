# [Hosea 8](https://www.blueletterbible.org/kjv/hosea/8)

<a name="hosea_8_1"></a>Hosea 8:1

the [šôp̄ār](../../strongs/h/h7782.md) to thy [ḥēḵ](../../strongs/h/h2441.md). as a [nesheׁr](../../strongs/h/h5404.md) against the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), because they have ['abar](../../strongs/h/h5674.md) my [bĕriyth](../../strongs/h/h1285.md), and [pāšaʿ](../../strongs/h/h6586.md) against my [towrah](../../strongs/h/h8451.md).

<a name="hosea_8_2"></a>Hosea 8:2

[Yisra'el](../../strongs/h/h3478.md) shall [zāʿaq](../../strongs/h/h2199.md) unto me, My ['Elohiym](../../strongs/h/h430.md), we [yada'](../../strongs/h/h3045.md) thee.

<a name="hosea_8_3"></a>Hosea 8:3

[Yisra'el](../../strongs/h/h3478.md) hath [zānaḥ](../../strongs/h/h2186.md) [towb](../../strongs/h/h2896.md): the ['oyeb](../../strongs/h/h341.md) shall [radaph](../../strongs/h/h7291.md) him.

<a name="hosea_8_4"></a>Hosea 8:4

They have set up [mālaḵ](../../strongs/h/h4427.md), but not by me: they have made [śûr](../../strongs/h/h7786.md), and I [yada'](../../strongs/h/h3045.md) it not: of their [keceph](../../strongs/h/h3701.md) and their [zāhāḇ](../../strongs/h/h2091.md) have they ['asah](../../strongs/h/h6213.md) them [ʿāṣāḇ](../../strongs/h/h6091.md), that they may be [karath](../../strongs/h/h3772.md).

<a name="hosea_8_5"></a>Hosea 8:5

Thy [ʿēḡel](../../strongs/h/h5695.md), O [Šōmrôn](../../strongs/h/h8111.md), hath [zānaḥ](../../strongs/h/h2186.md) thee off; mine ['aph](../../strongs/h/h639.md) is [ḥārâ](../../strongs/h/h2734.md) against them: how long will it be ere they [yakol](../../strongs/h/h3201.md) to [niqqāyôn](../../strongs/h/h5356.md)?

<a name="hosea_8_6"></a>Hosea 8:6

For from [Yisra'el](../../strongs/h/h3478.md) was it also: the [ḥārāš](../../strongs/h/h2796.md) ['asah](../../strongs/h/h6213.md) it; therefore it is not ['Elohiym](../../strongs/h/h430.md): but the [ʿēḡel](../../strongs/h/h5695.md) of [Šōmrôn](../../strongs/h/h8111.md) shall be [šᵊḇāḇîm](../../strongs/h/h7616.md).

<a name="hosea_8_7"></a>Hosea 8:7

For they have [zāraʿ](../../strongs/h/h2232.md) the [ruwach](../../strongs/h/h7307.md), and they shall [qāṣar](../../strongs/h/h7114.md) the [sûp̄â](../../strongs/h/h5492.md): it hath no [qāmâ](../../strongs/h/h7054.md): the [ṣemaḥ](../../strongs/h/h6780.md) shall ['asah](../../strongs/h/h6213.md) no [qemaḥ](../../strongs/h/h7058.md): if so be it ['asah](../../strongs/h/h6213.md), the [zûr](../../strongs/h/h2114.md) shall [bālaʿ](../../strongs/h/h1104.md) it.

<a name="hosea_8_8"></a>Hosea 8:8

[Yisra'el](../../strongs/h/h3478.md) is [bālaʿ](../../strongs/h/h1104.md): now shall they be among the [gowy](../../strongs/h/h1471.md) as a [kĕliy](../../strongs/h/h3627.md) wherein is no [chephets](../../strongs/h/h2656.md).

<a name="hosea_8_9"></a>Hosea 8:9

For they are [ʿālâ](../../strongs/h/h5927.md) to ['Aššûr](../../strongs/h/h804.md), a [pere'](../../strongs/h/h6501.md) [bāḏaḏ](../../strongs/h/h909.md) by himself: ['Ep̄rayim](../../strongs/h/h669.md) hath [tānâ](../../strongs/h/h8566.md) ['ahaḇ](../../strongs/h/h158.md).

<a name="hosea_8_10"></a>Hosea 8:10

Yea, though they have [tānâ](../../strongs/h/h8566.md) among the [gowy](../../strongs/h/h1471.md), now will I [qāḇaṣ](../../strongs/h/h6908.md) them, and they shall [ḥālal](../../strongs/h/h2490.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) for the [maśśā'](../../strongs/h/h4853.md) of the [melek](../../strongs/h/h4428.md) of [śar](../../strongs/h/h8269.md).

<a name="hosea_8_11"></a>Hosea 8:11

Because ['Ep̄rayim](../../strongs/h/h669.md) hath made [rabah](../../strongs/h/h7235.md) [mizbeach](../../strongs/h/h4196.md) to [chata'](../../strongs/h/h2398.md), [mizbeach](../../strongs/h/h4196.md) shall be unto him to [chata'](../../strongs/h/h2398.md).

<a name="hosea_8_12"></a>Hosea 8:12

I have [kāṯaḇ](../../strongs/h/h3789.md) to him the [rōḇ](../../strongs/h/h7230.md) [ribô'](../../strongs/h/h7239.md) of my [towrah](../../strongs/h/h8451.md), but they were [chashab](../../strongs/h/h2803.md) as a [zûr](../../strongs/h/h2114.md).

<a name="hosea_8_13"></a>Hosea 8:13

They [zabach](../../strongs/h/h2076.md) [basar](../../strongs/h/h1320.md) for the [zebach](../../strongs/h/h2077.md) of mine [haḇhāḇ](../../strongs/h/h1890.md), and ['akal](../../strongs/h/h398.md) it; but [Yĕhovah](../../strongs/h/h3068.md) [ratsah](../../strongs/h/h7521.md) them not; now will he [zakar](../../strongs/h/h2142.md) their ['avon](../../strongs/h/h5771.md), and [paqad](../../strongs/h/h6485.md) their [chatta'ath](../../strongs/h/h2403.md): they shall [shuwb](../../strongs/h/h7725.md) to [Mitsrayim](../../strongs/h/h4714.md).

<a name="hosea_8_14"></a>Hosea 8:14

For [Yisra'el](../../strongs/h/h3478.md) hath [shakach](../../strongs/h/h7911.md) his ['asah](../../strongs/h/h6213.md), and [bānâ](../../strongs/h/h1129.md) [heykal](../../strongs/h/h1964.md); and [Yehuwdah](../../strongs/h/h3063.md) hath [rabah](../../strongs/h/h7235.md) [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md): but I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) upon his [ʿîr](../../strongs/h/h5892.md), and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) thereof.

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 7](hosea_7.md) - [Hosea 9](hosea_9.md)