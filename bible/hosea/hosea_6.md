# [Hosea 6](https://www.blueletterbible.org/kjv/hosea/6)

<a name="hosea_6_1"></a>Hosea 6:1

[yālaḵ](../../strongs/h/h3212.md), and let us [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md): for he hath [taraph](../../strongs/h/h2963.md), and he will [rapha'](../../strongs/h/h7495.md) us; he hath [nakah](../../strongs/h/h5221.md), and he will [ḥāḇaš](../../strongs/h/h2280.md) us.

<a name="hosea_6_2"></a>Hosea 6:2

After two [yowm](../../strongs/h/h3117.md) will he [ḥāyâ](../../strongs/h/h2421.md) us: in the third [yowm](../../strongs/h/h3117.md) he will [quwm](../../strongs/h/h6965.md) us, and we shall [ḥāyâ](../../strongs/h/h2421.md) in his [paniym](../../strongs/h/h6440.md).

<a name="hosea_6_3"></a>Hosea 6:3

Then shall we [yada'](../../strongs/h/h3045.md), if we [radaph](../../strongs/h/h7291.md) on to [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md): his [môṣā'](../../strongs/h/h4161.md) is [kuwn](../../strongs/h/h3559.md) as the [šaḥar](../../strongs/h/h7837.md); and he shall [bow'](../../strongs/h/h935.md) unto us as the [gešem](../../strongs/h/h1653.md), as the [malqôš](../../strongs/h/h4456.md) and former [yārâ](../../strongs/h/h3384.md) unto the ['erets](../../strongs/h/h776.md).

<a name="hosea_6_4"></a>Hosea 6:4

O ['Ep̄rayim](../../strongs/h/h669.md), what shall I ['asah](../../strongs/h/h6213.md) unto thee? O [Yehuwdah](../../strongs/h/h3063.md), what shall I ['asah](../../strongs/h/h6213.md) unto thee? for your [checed](../../strongs/h/h2617.md) is as a [boqer](../../strongs/h/h1242.md) [ʿānān](../../strongs/h/h6051.md), and as the [šāḵam](../../strongs/h/h7925.md) [ṭal](../../strongs/h/h2919.md) it goeth [halak](../../strongs/h/h1980.md).

<a name="hosea_6_5"></a>Hosea 6:5

Therefore have I [ḥāṣaḇ](../../strongs/h/h2672.md) them by the [nāḇî'](../../strongs/h/h5030.md); I have [harag](../../strongs/h/h2026.md) them by the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md): and thy [mishpat](../../strongs/h/h4941.md) are as the ['owr](../../strongs/h/h216.md) that [yāṣā'](../../strongs/h/h3318.md).

<a name="hosea_6_6"></a>Hosea 6:6

For I [ḥāp̄ēṣ](../../strongs/h/h2654.md) [checed](../../strongs/h/h2617.md), and not [zebach](../../strongs/h/h2077.md); and the [da'ath](../../strongs/h/h1847.md) of ['Elohiym](../../strongs/h/h430.md) more than [ʿōlâ](../../strongs/h/h5930.md).

<a name="hosea_6_7"></a>Hosea 6:7

But they like ['āḏām](../../strongs/h/h120.md) have ['abar](../../strongs/h/h5674.md) the [bĕriyth](../../strongs/h/h1285.md): there have they [bāḡaḏ](../../strongs/h/h898.md) against me.

<a name="hosea_6_8"></a>Hosea 6:8

[Gilʿāḏ](../../strongs/h/h1568.md) is a [qiryâ](../../strongs/h/h7151.md) of them that [pa'al](../../strongs/h/h6466.md) ['aven](../../strongs/h/h205.md), and is ['aqob](../../strongs/h/h6121.md) with [dam](../../strongs/h/h1818.md).

<a name="hosea_6_9"></a>Hosea 6:9

And as [gᵊḏûḏ](../../strongs/h/h1416.md) [ḥāḵâ](../../strongs/h/h2442.md) for an ['iysh](../../strongs/h/h376.md), so the [Ḥeḇer](../../strongs/h/h2267.md) of [kōhēn](../../strongs/h/h3548.md) [ratsach](../../strongs/h/h7523.md) in the [derek](../../strongs/h/h1870.md) by [šᵊḵem](../../strongs/h/h7926.md) [Šᵊḵem](../../strongs/h/h7927.md): for they ['asah](../../strongs/h/h6213.md) [zimmâ](../../strongs/h/h2154.md).

<a name="hosea_6_10"></a>Hosea 6:10

I have [ra'ah](../../strongs/h/h7200.md) a [šaʿărûr](../../strongs/h/h8186.md) in the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): there is the [zᵊnûṯ](../../strongs/h/h2184.md) of ['Ep̄rayim](../../strongs/h/h669.md), [Yisra'el](../../strongs/h/h3478.md) is [ṭāmē'](../../strongs/h/h2930.md).

<a name="hosea_6_11"></a>Hosea 6:11

Also, O [Yehuwdah](../../strongs/h/h3063.md), he hath [shiyth](../../strongs/h/h7896.md) a [qāṣîr](../../strongs/h/h7105.md) for thee, when I [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of my ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 5](hosea_5.md) - [Hosea 7](hosea_7.md)