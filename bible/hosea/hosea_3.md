# [Hosea 3](https://www.blueletterbible.org/kjv/hosea/3)

<a name="hosea_3_1"></a>Hosea 3:1

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, [yālaḵ](../../strongs/h/h3212.md) yet, ['ahab](../../strongs/h/h157.md) an ['ishshah](../../strongs/h/h802.md) ['ahab](../../strongs/h/h157.md) of her [rea'](../../strongs/h/h7453.md), yet a [na'aph](../../strongs/h/h5003.md), according to the ['ahăḇâ](../../strongs/h/h160.md) of [Yĕhovah](../../strongs/h/h3068.md) toward the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), who [panah](../../strongs/h/h6437.md) to ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and ['ahab](../../strongs/h/h157.md) ['ăšîšâ](../../strongs/h/h809.md) of [ʿēnāḇ](../../strongs/h/h6025.md).

<a name="hosea_3_2"></a>Hosea 3:2

So I [kārâ](../../strongs/h/h3739.md) her to me for fifteen pieces of [keceph](../../strongs/h/h3701.md), and for an [ḥōmer](../../strongs/h/h2563.md) of [śᵊʿōrâ](../../strongs/h/h8184.md), and an half [leṯeḵ](../../strongs/h/h3963.md) of [śᵊʿōrâ](../../strongs/h/h8184.md):

<a name="hosea_3_3"></a>Hosea 3:3

And I ['āmar](../../strongs/h/h559.md) unto her, Thou shalt [yashab](../../strongs/h/h3427.md) for me [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md); thou shalt not [zānâ](../../strongs/h/h2181.md), and thou shalt not [hayah](../../strongs/h/h1961.md) for another ['iysh](../../strongs/h/h376.md): so will I also be for thee.

<a name="hosea_3_4"></a>Hosea 3:4

For the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [yashab](../../strongs/h/h3427.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) without a [melek](../../strongs/h/h4428.md), and without a [śar](../../strongs/h/h8269.md), and without a [zebach](../../strongs/h/h2077.md), and without a [maṣṣēḇâ](../../strongs/h/h4676.md), and without an ['ēp̄ôḏ](../../strongs/h/h646.md), and [tᵊrāp̄îm](../../strongs/h/h8655.md):

<a name="hosea_3_5"></a>Hosea 3:5

['aḥar](../../strongs/h/h310.md) shall the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md), and [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and [Dāviḏ](../../strongs/h/h1732.md) their [melek](../../strongs/h/h4428.md); and shall [pachad](../../strongs/h/h6342.md) [Yĕhovah](../../strongs/h/h3068.md) and his [ṭûḇ](../../strongs/h/h2898.md) in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 2](hosea_2.md) - [Hosea 4](hosea_4.md)