# [Hosea 13](https://www.blueletterbible.org/kjv/hosea/13)

<a name="hosea_13_1"></a>Hosea 13:1

When ['Ep̄rayim](../../strongs/h/h669.md) [dabar](../../strongs/h/h1696.md) [rᵊṯēṯ](../../strongs/h/h7578.md), he [nasa'](../../strongs/h/h5375.md) himself in [Yisra'el](../../strongs/h/h3478.md); but when he ['asham](../../strongs/h/h816.md) in [BaʿAl](../../strongs/h/h1168.md), he [muwth](../../strongs/h/h4191.md).

<a name="hosea_13_2"></a>Hosea 13:2

And now they [chata'](../../strongs/h/h2398.md) more and more, and have ['asah](../../strongs/h/h6213.md) them [massēḵâ](../../strongs/h/h4541.md) of their [keceph](../../strongs/h/h3701.md), and [ʿāṣāḇ](../../strongs/h/h6091.md) according to their own [tāḇûn](../../strongs/h/h8394.md), all of it the [ma'aseh](../../strongs/h/h4639.md) of the [ḥārāš](../../strongs/h/h2796.md): they ['āmar](../../strongs/h/h559.md) of them, Let the ['āḏām](../../strongs/h/h120.md) that [zabach](../../strongs/h/h2076.md) [nashaq](../../strongs/h/h5401.md) the [ʿēḡel](../../strongs/h/h5695.md).

<a name="hosea_13_3"></a>Hosea 13:3

Therefore they shall be as the [boqer](../../strongs/h/h1242.md) [ʿānān](../../strongs/h/h6051.md), and as the [šāḵam](../../strongs/h/h7925.md) [ṭal](../../strongs/h/h2919.md) that [halak](../../strongs/h/h1980.md), as the [mots](../../strongs/h/h4671.md) that is driven with the [sāʿar](../../strongs/h/h5590.md) out of the [gōren](../../strongs/h/h1637.md), and as the ['ashan](../../strongs/h/h6227.md) out of the ['ărubâ](../../strongs/h/h699.md).

<a name="hosea_13_4"></a>Hosea 13:4

Yet I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and thou shalt [yada'](../../strongs/h/h3045.md) no ['Elohiym](../../strongs/h/h430.md) but [zûlâ](../../strongs/h/h2108.md): for there is no [yasha'](../../strongs/h/h3467.md) beside me.

<a name="hosea_13_5"></a>Hosea 13:5

I did [yada'](../../strongs/h/h3045.md) thee in the [midbar](../../strongs/h/h4057.md), in the ['erets](../../strongs/h/h776.md) of [tal'ûḇāṯ](../../strongs/h/h8514.md).

<a name="hosea_13_6"></a>Hosea 13:6

According to their [marʿîṯ](../../strongs/h/h4830.md), so were they [sāׂbaʿ](../../strongs/h/h7646.md); they were [sāׂbaʿ](../../strongs/h/h7646.md), and their [leb](../../strongs/h/h3820.md) was [ruwm](../../strongs/h/h7311.md); therefore have they [shakach](../../strongs/h/h7911.md) me.

<a name="hosea_13_7"></a>Hosea 13:7

Therefore I will be unto them as a [šāḥal](../../strongs/h/h7826.md): as a [nāmēr](../../strongs/h/h5246.md) by the [derek](../../strongs/h/h1870.md) will I [šûr](../../strongs/h/h7789.md) them:

<a name="hosea_13_8"></a>Hosea 13:8

I will [pāḡaš](../../strongs/h/h6298.md) them as a [dōḇ](../../strongs/h/h1677.md) that is [šakûl](../../strongs/h/h7909.md), and will [qāraʿ](../../strongs/h/h7167.md) the [sᵊḡôr](../../strongs/h/h5458.md) of their [leb](../../strongs/h/h3820.md), and there will I ['akal](../../strongs/h/h398.md) them like a [lāḇî'](../../strongs/h/h3833.md): the [sadeh](../../strongs/h/h7704.md) [chay](../../strongs/h/h2416.md) shall [bāqaʿ](../../strongs/h/h1234.md) them.

<a name="hosea_13_9"></a>Hosea 13:9

O [Yisra'el](../../strongs/h/h3478.md), thou hast [shachath](../../strongs/h/h7843.md) thyself; but in me is thine ['ezer](../../strongs/h/h5828.md).

<a name="hosea_13_10"></a>Hosea 13:10

I ['ĕhî](../../strongs/h/h165.md) be thy [melek](../../strongs/h/h4428.md): where that may [yasha'](../../strongs/h/h3467.md) thee in all thy [ʿîr](../../strongs/h/h5892.md)? and thy [shaphat](../../strongs/h/h8199.md) of whom thou ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) me a [melek](../../strongs/h/h4428.md) and [śar](../../strongs/h/h8269.md)?

<a name="hosea_13_11"></a>Hosea 13:11

I [nathan](../../strongs/h/h5414.md) thee a [melek](../../strongs/h/h4428.md) in mine ['aph](../../strongs/h/h639.md), and [laqach](../../strongs/h/h3947.md) him away in my ['ebrah](../../strongs/h/h5678.md).

<a name="hosea_13_12"></a>Hosea 13:12

The ['avon](../../strongs/h/h5771.md) of ['Ep̄rayim](../../strongs/h/h669.md) is [tsarar](../../strongs/h/h6887.md); his [chatta'ath](../../strongs/h/h2403.md) is [tsaphan](../../strongs/h/h6845.md).

<a name="hosea_13_13"></a>Hosea 13:13

The [chebel](../../strongs/h/h2256.md) of a [yalad](../../strongs/h/h3205.md) shall [bow'](../../strongs/h/h935.md) upon him: he is an [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md); for he should not ['amad](../../strongs/h/h5975.md) [ʿēṯ](../../strongs/h/h6256.md) in the [mašbēr](../../strongs/h/h4866.md) of [ben](../../strongs/h/h1121.md).

<a name="hosea_13_14"></a>Hosea 13:14

I will [pāḏâ](../../strongs/h/h6299.md) them from the [yad](../../strongs/h/h3027.md) of the [shĕ'owl](../../strongs/h/h7585.md); I will [gā'al](../../strongs/h/h1350.md) them from [maveth](../../strongs/h/h4194.md): O [maveth](../../strongs/h/h4194.md), I ['ĕhî](../../strongs/h/h165.md) be thy [deḇer](../../strongs/h/h1698.md); O [shĕ'owl](../../strongs/h/h7585.md), I ['ĕhî](../../strongs/h/h165.md) be thy [qōṭeḇ](../../strongs/h/h6987.md): [nōḥam](../../strongs/h/h5164.md) shall be [cathar](../../strongs/h/h5641.md) from mine ['ayin](../../strongs/h/h5869.md).

<a name="hosea_13_15"></a>Hosea 13:15

Though he be [pārā'](../../strongs/h/h6500.md) among his ['ach](../../strongs/h/h251.md), a [qāḏîm](../../strongs/h/h6921.md) shall [bow'](../../strongs/h/h935.md), the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [ʿālâ](../../strongs/h/h5927.md) from the [midbar](../../strongs/h/h4057.md), and his [māqôr](../../strongs/h/h4726.md) shall become [buwsh](../../strongs/h/h954.md), and his [maʿyān](../../strongs/h/h4599.md) shall be dried [ḥāraḇ](../../strongs/h/h2717.md): he shall [šāsâ](../../strongs/h/h8154.md) the ['ôṣār](../../strongs/h/h214.md) of all [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md).

<a name="hosea_13_16"></a>Hosea 13:16

[Šōmrôn](../../strongs/h/h8111.md) shall become ['asham](../../strongs/h/h816.md); for she hath [marah](../../strongs/h/h4784.md) against her ['Elohiym](../../strongs/h/h430.md): they shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md): their ['owlel](../../strongs/h/h5768.md) shall be [rāṭaš](../../strongs/h/h7376.md), and their [hārê](../../strongs/h/h2030.md) shall be [bāqaʿ](../../strongs/h/h1234.md).

---

[Transliteral Bible](../bible.md)

[Hosea](hosea.md)

[Hosea 12](hosea_12.md) - [Hosea 14](hosea_14.md)