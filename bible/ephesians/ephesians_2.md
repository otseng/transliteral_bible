# [Ephesians 2](https://www.blueletterbible.org/kjv/eph/2/1/s_1099001)

<a name="ephesians_2_1"></a>Ephesians 2:1

And you, who were [nekros](../../strongs/g/g3498.md) in [paraptōma](../../strongs/g/g3900.md) and [hamartia](../../strongs/g/g266.md);

<a name="ephesians_2_2"></a>Ephesians 2:2

Wherein in [pote](../../strongs/g/g4218.md) ye [peripateō](../../strongs/g/g4043.md) according to the [aiōn](../../strongs/g/g165.md) of this [kosmos](../../strongs/g/g2889.md), according to the [archōn](../../strongs/g/g758.md) of the [exousia](../../strongs/g/g1849.md) of the [aēr](../../strongs/g/g109.md), the [pneuma](../../strongs/g/g4151.md) that now [energeō](../../strongs/g/g1754.md) in the [huios](../../strongs/g/g5207.md) of [apeitheia](../../strongs/g/g543.md):

<a name="ephesians_2_3"></a>Ephesians 2:3

Among whom also we all had our [anastrephō](../../strongs/g/g390.md) in [pote](../../strongs/g/g4218.md) in the [epithymia](../../strongs/g/g1939.md) of our [sarx](../../strongs/g/g4561.md), [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of the [sarx](../../strongs/g/g4561.md) and of the [dianoia](../../strongs/g/g1271.md); and were by [physis](../../strongs/g/g5449.md) the [teknon](../../strongs/g/g5043.md) of [orgē](../../strongs/g/g3709.md), even as [loipos](../../strongs/g/g3062.md).

<a name="ephesians_2_4"></a>Ephesians 2:4

But [theos](../../strongs/g/g2316.md), who is [plousios](../../strongs/g/g4145.md) in [eleos](../../strongs/g/g1656.md), for his [polys](../../strongs/g/g4183.md) [agapē](../../strongs/g/g26.md) wherewith he [agapaō](../../strongs/g/g25.md) us,

<a name="ephesians_2_5"></a>Ephesians 2:5

Even when we were [nekros](../../strongs/g/g3498.md) in [paraptōma](../../strongs/g/g3900.md), hath [syzōopoieō](../../strongs/g/g4806.md) with [Christos](../../strongs/g/g5547.md), (by [charis](../../strongs/g/g5485.md) ye are [sōzō](../../strongs/g/g4982.md);)

<a name="ephesians_2_6"></a>Ephesians 2:6

And hath [synegeirō](../../strongs/g/g4891.md), and [sygkathizō](../../strongs/g/g4776.md) in [epouranios](../../strongs/g/g2032.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="ephesians_2_7"></a>Ephesians 2:7

That in the [aiōn](../../strongs/g/g165.md) to [eperchomai](../../strongs/g/g1904.md) he might [endeiknymi](../../strongs/g/g1731.md) the [hyperballō](../../strongs/g/g5235.md) [ploutos](../../strongs/g/g4149.md) of his [charis](../../strongs/g/g5485.md) in his [chrēstotēs](../../strongs/g/g5544.md) toward us through [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="ephesians_2_8"></a>Ephesians 2:8

For by [charis](../../strongs/g/g5485.md) are ye [sōzō](../../strongs/g/g4982.md) through [pistis](../../strongs/g/g4102.md); and that not of [hymōn](../../strongs/g/g5216.md): the [dōron](../../strongs/g/g1435.md) of [theos](../../strongs/g/g2316.md):

<a name="ephesians_2_9"></a>Ephesians 2:9

Not of [ergon](../../strongs/g/g2041.md), lest [tis](../../strongs/g/g5100.md) should [kauchaomai](../../strongs/g/g2744.md).

<a name="ephesians_2_10"></a>Ephesians 2:10

For we are his [poiēma](../../strongs/g/g4161.md), [ktizō](../../strongs/g/g2936.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) unto [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md), which [theos](../../strongs/g/g2316.md) hath [proetoimazō](../../strongs/g/g4282.md) that we should [peripateō](../../strongs/g/g4043.md) in them.

<a name="ephesians_2_11"></a>Ephesians 2:11

Wherefore [mnēmoneuō](../../strongs/g/g3421.md), that ye being in time past [ethnos](../../strongs/g/g1484.md) in the [sarx](../../strongs/g/g4561.md), who are [legō](../../strongs/g/g3004.md) [akrobystia](../../strongs/g/g203.md) by that which is [legō](../../strongs/g/g3004.md) the [peritomē](../../strongs/g/g4061.md) in the [sarx](../../strongs/g/g4561.md) [cheiropoiētos](../../strongs/g/g5499.md);

<a name="ephesians_2_12"></a>Ephesians 2:12

That at that [kairos](../../strongs/g/g2540.md) ye were without [Christos](../../strongs/g/g5547.md), being [apallotrioō](../../strongs/g/g526.md) from the [politeia](../../strongs/g/g4174.md) of [Israēl](../../strongs/g/g2474.md), and [xenos](../../strongs/g/g3581.md) from the [diathēkē](../../strongs/g/g1242.md) of [epaggelia](../../strongs/g/g1860.md), having no [elpis](../../strongs/g/g1680.md), and [atheos](../../strongs/g/g112.md) in the [kosmos](../../strongs/g/g2889.md):

<a name="ephesians_2_13"></a>Ephesians 2:13

But [nyni](../../strongs/g/g3570.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) ye who sometimes were [makran](../../strongs/g/g3112.md) are [ginomai](../../strongs/g/g1096.md) [eggys](../../strongs/g/g1451.md) by the [haima](../../strongs/g/g129.md) of [Christos](../../strongs/g/g5547.md).

<a name="ephesians_2_14"></a>Ephesians 2:14

For he is our [eirēnē](../../strongs/g/g1515.md), who hath [poieō](../../strongs/g/g4160.md) both one, and hath [lyō](../../strongs/g/g3089.md) the [mesotoichon](../../strongs/g/g3320.md) of [phragmos](../../strongs/g/g5418.md);

<a name="ephesians_2_15"></a>Ephesians 2:15

Having [katargeō](../../strongs/g/g2673.md) in his [sarx](../../strongs/g/g4561.md) the [echthra](../../strongs/g/g2189.md), the [nomos](../../strongs/g/g3551.md) of [entolē](../../strongs/g/g1785.md) in [dogma](../../strongs/g/g1378.md); for to [ktizō](../../strongs/g/g2936.md) in himself of twain one [kainos](../../strongs/g/g2537.md) [anthrōpos](../../strongs/g/g444.md), so [poieō](../../strongs/g/g4160.md) [eirēnē](../../strongs/g/g1515.md);

<a name="ephesians_2_16"></a>Ephesians 2:16

And that he might [apokatallassō](../../strongs/g/g604.md) both unto [theos](../../strongs/g/g2316.md) in one [sōma](../../strongs/g/g4983.md) by the [stauros](../../strongs/g/g4716.md), having [apokteinō](../../strongs/g/g615.md) the [echthra](../../strongs/g/g2189.md) thereby:

<a name="ephesians_2_17"></a>Ephesians 2:17

And [erchomai](../../strongs/g/g2064.md) and [euaggelizō](../../strongs/g/g2097.md) [eirēnē](../../strongs/g/g1515.md) to you which were [makran](../../strongs/g/g3112.md), and to them that were [eggys](../../strongs/g/g1451.md).

<a name="ephesians_2_18"></a>Ephesians 2:18

For through him we both have [prosagōgē](../../strongs/g/g4318.md) by one [pneuma](../../strongs/g/g4151.md) unto the [patēr](../../strongs/g/g3962.md).

<a name="ephesians_2_19"></a>Ephesians 2:19

Now therefore ye are no more [xenos](../../strongs/g/g3581.md) and [paroikos](../../strongs/g/g3941.md), but [sympolitēs](../../strongs/g/g4847.md) with the [hagios](../../strongs/g/g40.md), and of the [oikeios](../../strongs/g/g3609.md) of [theos](../../strongs/g/g2316.md);

<a name="ephesians_2_20"></a>Ephesians 2:20

And are [epoikodomeō](../../strongs/g/g2026.md) upon the [themelios](../../strongs/g/g2310.md) of the [apostolos](../../strongs/g/g652.md) and [prophētēs](../../strongs/g/g4396.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) himself being the [akrogōniaios](../../strongs/g/g204.md);

<a name="ephesians_2_21"></a>Ephesians 2:21

In whom all the [oikodomē](../../strongs/g/g3619.md) [synarmologeō](../../strongs/g/g4883.md) [auxanō](../../strongs/g/g837.md) unto an [hagios](../../strongs/g/g40.md) [naos](../../strongs/g/g3485.md) in the [kyrios](../../strongs/g/g2962.md):

<a name="ephesians_2_22"></a>Ephesians 2:22

In whom ye also are [synoikodomeō](../../strongs/g/g4925.md) for a [katoikētērion](../../strongs/g/g2732.md) of [theos](../../strongs/g/g2316.md) through the [pneuma](../../strongs/g/g4151.md).

---

[Transliteral Bible](../bible.md)

[Ephesians](ephesians.md)

[Ephesians 1](ephesians_1.md) - [Ephesians 3](ephesians_3.md)