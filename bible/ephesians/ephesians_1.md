# [Ephesians 1](https://www.blueletterbible.org/kjv/eph/1/1/s_1098001)

<a name="ephesians_1_1"></a>Ephesians 1:1

[Paulos](../../strongs/g/g3972.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), to the [hagios](../../strongs/g/g40.md) which are at [Ephesos](../../strongs/g/g2181.md), and to the [pistos](../../strongs/g/g4103.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="ephesians_1_2"></a>Ephesians 1:2

[charis](../../strongs/g/g5485.md) be to you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="ephesians_1_3"></a>Ephesians 1:3

[eulogētos](../../strongs/g/g2128.md) the [theos](../../strongs/g/g2316.md) and [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), who hath [eulogeō](../../strongs/g/g2127.md) us with all [pneumatikos](../../strongs/g/g4152.md) [eulogia](../../strongs/g/g2129.md) in [epouranios](../../strongs/g/g2032.md) in [Christos](../../strongs/g/g5547.md): [^1]

<a name="ephesians_1_4"></a>Ephesians 1:4

According as he hath [eklegomai](../../strongs/g/g1586.md) us in him [katenōpion](../../strongs/g/g2714.md) the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md), that we should be [hagios](../../strongs/g/g40.md) and [amōmos](../../strongs/g/g299.md) before him in [agapē](../../strongs/g/g26.md):

<a name="ephesians_1_5"></a>Ephesians 1:5

Having [proorizō](../../strongs/g/g4309.md) us unto the [huiothesia](../../strongs/g/g5206.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) to himself, according to the [eudokia](../../strongs/g/g2107.md) of his [thelēma](../../strongs/g/g2307.md),

<a name="ephesians_1_6"></a>Ephesians 1:6

To the [epainos](../../strongs/g/g1868.md) of the [doxa](../../strongs/g/g1391.md) of his [charis](../../strongs/g/g5485.md), wherein he hath made us [charitoō](../../strongs/g/g5487.md) in the [agapa](../../strongs/g/g25.md).

<a name="ephesians_1_7"></a>Ephesians 1:7

In whom we have [apolytrōis](../../strongs/g/g629.md) through his [haima](../../strongs/g/g129.md), the [aphesis](../../strongs/g/g859.md) of [paraptōma](../../strongs/g/g3900.md), according to the [ploutos](../../strongs/g/g4149.md) of his [charis](../../strongs/g/g5485.md);

<a name="ephesians_1_8"></a>Ephesians 1:8

Wherein he hath [perisseuō](../../strongs/g/g4052.md) toward us in all [sophia](../../strongs/g/g4678.md) and [phronēsis](../../strongs/g/g5428.md);

<a name="ephesians_1_9"></a>Ephesians 1:9

Having [gnōrizō](../../strongs/g/g1107.md) unto us the [mystērion](../../strongs/g/g3466.md) of his [thelēma](../../strongs/g/g2307.md), according to his [eudokia](../../strongs/g/g2107.md) which he hath [protithēmi](../../strongs/g/g4388.md) in himself:

<a name="ephesians_1_10"></a>Ephesians 1:10

That in the [oikonomia](../../strongs/g/g3622.md) of the [plērōma](../../strongs/g/g4138.md) of [kairos](../../strongs/g/g2540.md) he might [anakephalaioō](../../strongs/g/g346.md) all things in [Christos](../../strongs/g/g5547.md), both which are in [ouranos](../../strongs/g/g3772.md), and which are on [gē](../../strongs/g/g1093.md); even in him:

<a name="ephesians_1_11"></a>Ephesians 1:11

In whom also we have [klēroō](../../strongs/g/g2820.md), being [proorizō](../../strongs/g/g4309.md) according to the [prothesis](../../strongs/g/g4286.md) of him who [energeō](../../strongs/g/g1754.md) all things after the [boulē](../../strongs/g/g1012.md) of his own [thelēma](../../strongs/g/g2307.md):

<a name="ephesians_1_12"></a>Ephesians 1:12

That we should be to the [epainos](../../strongs/g/g1868.md) of his [doxa](../../strongs/g/g1391.md), who [proelpizō](../../strongs/g/g4276.md) in [Christos](../../strongs/g/g5547.md).

<a name="ephesians_1_13"></a>Ephesians 1:13

In whom ye also, after that ye [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [alētheia](../../strongs/g/g225.md), the [euaggelion](../../strongs/g/g2098.md) of your [sōtēria](../../strongs/g/g4991.md): in whom also after that ye [pisteuō](../../strongs/g/g4100.md), ye were [sphrgizō](../../strongs/g/g4972.md) with that [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) of [epaggelia](../../strongs/g/g1860.md),

<a name="ephesians_1_14"></a>Ephesians 1:14

Which is the [arrabn](../../strongs/g/g728.md) of our [klēronomia](../../strongs/g/g2817.md) until the [apolytrōsis](../../strongs/g/g629.md) of the [peripoiēsis](../../strongs/g/g4047.md), unto the [epainos](../../strongs/g/g1868.md) of his [doxa](../../strongs/g/g1391.md).

<a name="ephesians_1_15"></a>Ephesians 1:15

Wherefore I also, after I [akouō](../../strongs/g/g191.md) of your [pistis](../../strongs/g/g4102.md) in the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and [agapē](../../strongs/g/g26.md) unto all the [hagios](../../strongs/g/g40.md),

<a name="ephesians_1_16"></a>Ephesians 1:16

[pauō](../../strongs/g/g3973.md) not to [eucharisteō](../../strongs/g/g2168.md) for you, [poieō](../../strongs/g/g4160.md) [mneia](../../strongs/g/g3417.md) of you in my [proseuchē](../../strongs/g/g4335.md);

<a name="ephesians_1_17"></a>Ephesians 1:17

That the [theos](../../strongs/g/g2316.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [patēr](../../strongs/g/g3962.md) of [doxa](../../strongs/g/g1391.md), may [didōmi](../../strongs/g/g1325.md) unto you the [pneuma](../../strongs/g/g4151.md) of [sophia](../../strongs/g/g4678.md) and [apokalypsis](../../strongs/g/g602.md) in the [epignōsis](../../strongs/g/g1922.md) of him:

<a name="ephesians_1_18"></a>Ephesians 1:18

The [ophthalmos](../../strongs/g/g3788.md) of your [dianoia](../../strongs/g/g1271.md) being [phōtizō](../../strongs/g/g5461.md); that ye may [eidō](../../strongs/g/g1492.md) what is the [elpis](../../strongs/g/g1680.md) of his [klēsi](../../strongs/g/g2821.md), and what the [ploutos](../../strongs/g/g4149.md) of the [doxa](../../strongs/g/g1391.md) of his [klēronomia](../../strongs/g/g2817.md) in the [hagios](../../strongs/g/g40.md),

<a name="ephesians_1_19"></a>Ephesians 1:19

And what [hyperballō](../../strongs/g/g5235.md) [megethos](../../strongs/g/g3174.md) of his [dynamis](../../strongs/g/g1411.md) to us-ward who [pisteuō](../../strongs/g/g4100.md), according to the [energeia](../../strongs/g/g1753.md) of his [ischys](../../strongs/g/g2479.md) [kratos](../../strongs/g/g2904.md),

<a name="ephesians_1_20"></a>Ephesians 1:20

Which he [energeō](../../strongs/g/g1754.md) in [Christos](../../strongs/g/g5547.md), when he [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md), and [kathizō](../../strongs/g/g2523.md) him at his own [dexios](../../strongs/g/g1188.md) in the [epouranios](../../strongs/g/g2032.md),

<a name="ephesians_1_21"></a>Ephesians 1:21

[hyperanō](../../strongs/g/g5231.md) all [archē](../../strongs/g/g746.md), and [exousia](../../strongs/g/g1849.md), and [dynamis](../../strongs/g/g1411.md), and [kyriotēs](../../strongs/g/g2963.md), and every [onoma](../../strongs/g/g3686.md) that is [onomazō](../../strongs/g/g3687.md), not only in this [aiōn](../../strongs/g/g165.md), but also in that which is to come:

<a name="ephesians_1_22"></a>Ephesians 1:22

And hath [hypotassō](../../strongs/g/g5293.md) all things under his [pous](../../strongs/g/g4228.md), and [didōmi](../../strongs/g/g1325.md) him to be the [kephalē](../../strongs/g/g2776.md) over all things to the [ekklēsia](../../strongs/g/g1577.md),

<a name="ephesians_1_23"></a>Ephesians 1:23

Which is his [sōma](../../strongs/g/g4983.md), the [plērōma](../../strongs/g/g4138.md) of him that [plēroō](../../strongs/g/g4137.md) all in all.

---

[Transliteral Bible](../bible.md)

[Ephesians](ephesians.md)

[Ephesians 2](ephesians_2.md)

---

[^1]: [Ephesians 1:3 Commentary](../../commentary/ephesians/ephesians_1_commentary.md#ephesians_1_3)
