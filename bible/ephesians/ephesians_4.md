# [Ephesians 4](https://www.blueletterbible.org/kjv/eph/4/1/s_1101001)

<a name="ephesians_4_1"></a>Ephesians 4:1

I therefore, the [desmios](../../strongs/g/g1198.md) of the [kyrios](../../strongs/g/g2962.md), [parakaleō](../../strongs/g/g3870.md) you that ye [peripateō](../../strongs/g/g4043.md) [axiōs](../../strongs/g/g516.md) of the [klēsis](../../strongs/g/g2821.md) wherewith ye are [kaleō](../../strongs/g/g2564.md),

<a name="ephesians_4_2"></a>Ephesians 4:2

With all [tapeinophrosynē](../../strongs/g/g5012.md) and [praotēs](../../strongs/g/g4236.md), with [makrothymia](../../strongs/g/g3115.md), [anechō](../../strongs/g/g430.md) [allēlōn](../../strongs/g/g240.md) in [agapē](../../strongs/g/g26.md);

<a name="ephesians_4_3"></a>Ephesians 4:3

[spoudazō](../../strongs/g/g4704.md) to [tēreō](../../strongs/g/g5083.md) the [henotēs](../../strongs/g/g1775.md) of the [pneuma](../../strongs/g/g4151.md) in the [syndesmos](../../strongs/g/g4886.md) of [eirēnē](../../strongs/g/g1515.md).

<a name="ephesians_4_4"></a>Ephesians 4:4

[heis](../../strongs/g/g1520.md) [sōma](../../strongs/g/g4983.md), and [heis](../../strongs/g/g1520.md) [pneuma](../../strongs/g/g4151.md), even as ye are [kaleō](../../strongs/g/g2564.md) in one [elpis](../../strongs/g/g1680.md) of your [klēsis](../../strongs/g/g2821.md);

<a name="ephesians_4_5"></a>Ephesians 4:5

[heis](../../strongs/g/g1520.md) [kyrios](../../strongs/g/g2962.md), [heis](../../strongs/g/g1520.md) [pistis](../../strongs/g/g4102.md), [mia](../../strongs/g/g3391.md) [baptisma](../../strongs/g/g908.md),

<a name="ephesians_4_6"></a>Ephesians 4:6

One [theos](../../strongs/g/g2316.md) and [patēr](../../strongs/g/g3962.md) of all, who above all, and through all, and in you all.

<a name="ephesians_4_7"></a>Ephesians 4:7

But unto every one of us is [didōmi](../../strongs/g/g1325.md) [charis](../../strongs/g/g5485.md) according to the [metron](../../strongs/g/g3358.md) of the [dōrea](../../strongs/g/g1431.md) of [Christos](../../strongs/g/g5547.md).

<a name="ephesians_4_8"></a>Ephesians 4:8

Wherefore he [legō](../../strongs/g/g3004.md), When he [anabainō](../../strongs/g/g305.md) up on [hypsos](../../strongs/g/g5311.md), he [aichmalōteuō](../../strongs/g/g162.md) [aichmalōsia](../../strongs/g/g161.md), and [didōmi](../../strongs/g/g1325.md) [doma](../../strongs/g/g1390.md) unto [anthrōpos](../../strongs/g/g444.md).

<a name="ephesians_4_9"></a>Ephesians 4:9

(Now that he [anabainō](../../strongs/g/g305.md), what is it but that he also [katabainō](../../strongs/g/g2597.md) first into the [katōteros](../../strongs/g/g2737.md) [meros](../../strongs/g/g3313.md) of [gē](../../strongs/g/g1093.md)?

<a name="ephesians_4_10"></a>Ephesians 4:10

He that [katabainō](../../strongs/g/g2597.md) is the same also that [anabainō](../../strongs/g/g305.md) [hyperanō](../../strongs/g/g5231.md) all [ouranos](../../strongs/g/g3772.md), that he might [plēroō](../../strongs/g/g4137.md) all things.)

<a name="ephesians_4_11"></a>Ephesians 4:11

And he [didōmi](../../strongs/g/g1325.md) some, [apostolos](../../strongs/g/g652.md); and some, [prophētēs](../../strongs/g/g4396.md); and some, [euaggelistēs](../../strongs/g/g2099.md); and some, [poimēn](../../strongs/g/g4166.md) and [didaskalos](../../strongs/g/g1320.md);

<a name="ephesians_4_12"></a>Ephesians 4:12

For the [katartismos](../../strongs/g/g2677.md) of the [hagios](../../strongs/g/g40.md), for the [ergon](../../strongs/g/g2041.md) of the [diakonia](../../strongs/g/g1248.md), for the [oikodomē](../../strongs/g/g3619.md) of the [sōma](../../strongs/g/g4983.md) of [Christos](../../strongs/g/g5547.md):

<a name="ephesians_4_13"></a>Ephesians 4:13

Till we all [katantaō](../../strongs/g/g2658.md) in the [henotēs](../../strongs/g/g1775.md) of the [pistis](../../strongs/g/g4102.md), and of the [epignōsis](../../strongs/g/g1922.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), unto a [teleios](../../strongs/g/g5046.md) [anēr](../../strongs/g/g435.md), unto the [metron](../../strongs/g/g3358.md) of the [hēlikia](../../strongs/g/g2244.md) of the [plērōma](../../strongs/g/g4138.md) of [Christos](../../strongs/g/g5547.md):

<a name="ephesians_4_14"></a>Ephesians 4:14

That we be no more [nēpios](../../strongs/g/g3516.md), [klydōnizomai](../../strongs/g/g2831.md), and [peripherō](../../strongs/g/g4064.md) with every [anemos](../../strongs/g/g417.md) of [didaskalia](../../strongs/g/g1319.md), by the [kybeia](../../strongs/g/g2940.md) of [anthrōpos](../../strongs/g/g444.md), and [panourgia](../../strongs/g/g3834.md), whereby they [methodeia](../../strongs/g/g3180.md) to [planē](../../strongs/g/g4106.md);

<a name="ephesians_4_15"></a>Ephesians 4:15

But [alētheuō](../../strongs/g/g226.md) in [agapē](../../strongs/g/g26.md), may [auxanō](../../strongs/g/g837.md) into him in all things, which is the [kephalē](../../strongs/g/g2776.md), [Christos](../../strongs/g/g5547.md):

<a name="ephesians_4_16"></a>Ephesians 4:16

From whom the whole [sōma](../../strongs/g/g4983.md) [synarmologeō](../../strongs/g/g4883.md) and [symbibazō](../../strongs/g/g4822.md) by that which every [haphē](../../strongs/g/g860.md) [epichorēgia](../../strongs/g/g2024.md), according to the [energeia](../../strongs/g/g1753.md) in the [metron](../../strongs/g/g3358.md) of every [meros](../../strongs/g/g3313.md), [poieō](../../strongs/g/g4160.md) [auxēsis](../../strongs/g/g838.md) of the [sōma](../../strongs/g/g4983.md) unto the [oikodomē](../../strongs/g/g3619.md) of itself in [agapē](../../strongs/g/g26.md).

<a name="ephesians_4_17"></a>Ephesians 4:17

This I [legō](../../strongs/g/g3004.md) therefore, and [martyromai](../../strongs/g/g3143.md) in the [kyrios](../../strongs/g/g2962.md), that ye henceforth [peripateō](../../strongs/g/g4043.md) not as [loipos](../../strongs/g/g3062.md) [ethnos](../../strongs/g/g1484.md) [peripateō](../../strongs/g/g4043.md), in the [mataiotēs](../../strongs/g/g3153.md) of their [nous](../../strongs/g/g3563.md),

<a name="ephesians_4_18"></a>Ephesians 4:18

Having the [dianoia](../../strongs/g/g1271.md) [skotizō](../../strongs/g/g4654.md), being [apallotrioō](../../strongs/g/g526.md) from the [zōē](../../strongs/g/g2222.md) of [theos](../../strongs/g/g2316.md) through the [agnoia](../../strongs/g/g52.md) that is in them, because of the [pōrōsis](../../strongs/g/g4457.md) of their [kardia](../../strongs/g/g2588.md):

<a name="ephesians_4_19"></a>Ephesians 4:19

Who being [apalgeō](../../strongs/g/g524.md) have [paradidōmi](../../strongs/g/g3860.md) themselves over unto [aselgeia](../../strongs/g/g766.md), to [ergasia](../../strongs/g/g2039.md) all [akatharsia](../../strongs/g/g167.md) with [pleonexia](../../strongs/g/g4124.md).

<a name="ephesians_4_20"></a>Ephesians 4:20

But ye have not so [manthanō](../../strongs/g/g3129.md) [Christos](../../strongs/g/g5547.md);

<a name="ephesians_4_21"></a>Ephesians 4:21

If so be that ye have [akouō](../../strongs/g/g191.md) him, and have been [didaskō](../../strongs/g/g1321.md) by him, as the [alētheia](../../strongs/g/g225.md) is in [Iēsous](../../strongs/g/g2424.md):

<a name="ephesians_4_22"></a>Ephesians 4:22

That ye [apotithēmi](../../strongs/g/g659.md) concerning the [proteros](../../strongs/g/g4387.md) [anastrophē](../../strongs/g/g391.md) the [palaios](../../strongs/g/g3820.md) [anthrōpos](../../strongs/g/g444.md), which is [phtheirō](../../strongs/g/g5351.md) according to the [apatē](../../strongs/g/g539.md) [epithymia](../../strongs/g/g1939.md);

<a name="ephesians_4_23"></a>Ephesians 4:23

And be [ananeoō](../../strongs/g/g365.md) in the [pneuma](../../strongs/g/g4151.md) of your [nous](../../strongs/g/g3563.md);

<a name="ephesians_4_24"></a>Ephesians 4:24

And that ye [endyō](../../strongs/g/g1746.md) the [kainos](../../strongs/g/g2537.md) [anthrōpos](../../strongs/g/g444.md), which after [theos](../../strongs/g/g2316.md) is [ktizō](../../strongs/g/g2936.md) in [dikaiosynē](../../strongs/g/g1343.md) and [alētheia](../../strongs/g/g225.md) [hosiotēs](../../strongs/g/g3742.md).

<a name="ephesians_4_25"></a>Ephesians 4:25

Wherefore [apotithēmi](../../strongs/g/g659.md) [pseudos](../../strongs/g/g5579.md), [laleō](../../strongs/g/g2980.md) every man [alētheia](../../strongs/g/g225.md) with his [plēsion](../../strongs/g/g4139.md): for we are [melos](../../strongs/g/g3196.md) [allēlōn](../../strongs/g/g240.md).

<a name="ephesians_4_26"></a>Ephesians 4:26

Be ye [orgizō](../../strongs/g/g3710.md), and [hamartanō](../../strongs/g/g264.md) not: let not the [hēlios](../../strongs/g/g2246.md) [epidyō](../../strongs/g/g1931.md) upon your [parorgismos](../../strongs/g/g3950.md):

<a name="ephesians_4_27"></a>Ephesians 4:27

Neither [didōmi](../../strongs/g/g1325.md) [topos](../../strongs/g/g5117.md) to the [diabolos](../../strongs/g/g1228.md).

<a name="ephesians_4_28"></a>Ephesians 4:28

Let him that [kleptō](../../strongs/g/g2813.md) [kleptō](../../strongs/g/g2813.md) no more: but rather let him [kopiaō](../../strongs/g/g2872.md), [ergazomai](../../strongs/g/g2038.md) with his [cheir](../../strongs/g/g5495.md) the thing which is [agathos](../../strongs/g/g18.md), that he may have to [metadidōmi](../../strongs/g/g3330.md) to [chreia](../../strongs/g/g5532.md).

<a name="ephesians_4_29"></a>Ephesians 4:29

Let no [sapros](../../strongs/g/g4550.md) [logos](../../strongs/g/g3056.md) [ekporeuomai](../../strongs/g/g1607.md) out of your [stoma](../../strongs/g/g4750.md), but that which is [agathos](../../strongs/g/g18.md) to the [chreia](../../strongs/g/g5532.md) of [oikodomē](../../strongs/g/g3619.md), that it may [didōmi](../../strongs/g/g1325.md) [charis](../../strongs/g/g5485.md) unto the [akouō](../../strongs/g/g191.md).

<a name="ephesians_4_30"></a>Ephesians 4:30

And [lypeō](../../strongs/g/g3076.md) not the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md), whereby ye are [sphragizō](../../strongs/g/g4972.md) unto the day of [apolytrōsis](../../strongs/g/g629.md).

<a name="ephesians_4_31"></a>Ephesians 4:31

Let all [pikria](../../strongs/g/g4088.md), and [thymos](../../strongs/g/g2372.md), and [orgē](../../strongs/g/g3709.md), and [kraugē](../../strongs/g/g2906.md), and [blasphēmia](../../strongs/g/g988.md), be [airō](../../strongs/g/g142.md) from you, with all [kakia](../../strongs/g/g2549.md):

<a name="ephesians_4_32"></a>Ephesians 4:32

And be ye [chrēstos](../../strongs/g/g5543.md) to [allēlōn](../../strongs/g/g240.md), [eusplagchnos](../../strongs/g/g2155.md), [charizomai](../../strongs/g/g5483.md) one another, even as [theos](../../strongs/g/g2316.md) for [Christos](../../strongs/g/g5547.md) sake hath [charizomai](../../strongs/g/g5483.md) you.

---

[Transliteral Bible](../bible.md)

[Ephesians 3](ephesians_3.md) - [Ephesians 5](ephesians_5.md)