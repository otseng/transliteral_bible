# Ephesians

[Ephesians Overview](../../commentary/ephesians/ephesians_overview.md)

[Ephesians 1](ephesians_1.md)

[Ephesians 2](ephesians_2.md)

[Ephesians 3](ephesians_3.md)

[Ephesians 4](ephesians_4.md)

[Ephesians 5](ephesians_5.md)

[Ephesians 6](ephesians_6.md)

---

[Transliteral Bible](../index.md)
