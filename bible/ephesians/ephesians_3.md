# [Ephesians 3](https://www.blueletterbible.org/kjv/eph/3/1/s_1100001)

<a name="ephesians_3_1"></a>Ephesians 3:1

For this cause I [Paulos](../../strongs/g/g3972.md), the [desmios](../../strongs/g/g1198.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) for you [ethnos](../../strongs/g/g1484.md),

<a name="ephesians_3_2"></a>Ephesians 3:2

If ye have [akouō](../../strongs/g/g191.md) of the [oikonomia](../../strongs/g/g3622.md) of the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) which is [didōmi](../../strongs/g/g1325.md) me to you-ward:

<a name="ephesians_3_3"></a>Ephesians 3:3

How that by [apokalypsis](../../strongs/g/g602.md) he [gnōrizō](../../strongs/g/g1107.md) unto me the [mystērion](../../strongs/g/g3466.md); (as I [prographō](../../strongs/g/g4270.md) in [oligos](../../strongs/g/g3641.md),

<a name="ephesians_3_4"></a>Ephesians 3:4

Whereby, when ye [anaginōskō](../../strongs/g/g314.md), ye may [noeō](../../strongs/g/g3539.md) my [synesis](../../strongs/g/g4907.md) in the [mystērion](../../strongs/g/g3466.md) of [Christos](../../strongs/g/g5547.md))

<a name="ephesians_3_5"></a>Ephesians 3:5

Which in other [genea](../../strongs/g/g1074.md) was not [gnōrizō](../../strongs/g/g1107.md) unto the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), as it is now [apokalyptō](../../strongs/g/g601.md) unto his [hagios](../../strongs/g/g40.md) [apostolos](../../strongs/g/g652.md) and [prophētēs](../../strongs/g/g4396.md) by the [pneuma](../../strongs/g/g4151.md);

<a name="ephesians_3_6"></a>Ephesians 3:6

That the [ethnos](../../strongs/g/g1484.md) should be [sygklēronomos](../../strongs/g/g4789.md), and of [syssōmos](../../strongs/g/g4954.md), and [symmetochos](../../strongs/g/g4830.md) of his [epaggelia](../../strongs/g/g1860.md) in [Christos](../../strongs/g/g5547.md) by the [euaggelion](../../strongs/g/g2098.md):

<a name="ephesians_3_7"></a>Ephesians 3:7

Whereof I was [ginomai](../../strongs/g/g1096.md) a [diakonos](../../strongs/g/g1249.md), according to the [dōrea](../../strongs/g/g1431.md) of the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) unto me by the [energeia](../../strongs/g/g1753.md) of his [dynamis](../../strongs/g/g1411.md).

<a name="ephesians_3_8"></a>Ephesians 3:8

Unto me, who am [elachistoteros](../../strongs/g/g1647.md) of all [hagios](../../strongs/g/g40.md), is this [charis](../../strongs/g/g5485.md) [didōmi](../../strongs/g/g1325.md), that I should [euaggelizō](../../strongs/g/g2097.md) among the [ethnos](../../strongs/g/g1484.md) the [anexichniastos](../../strongs/g/g421.md) [ploutos](../../strongs/g/g4149.md) of [Christos](../../strongs/g/g5547.md);

<a name="ephesians_3_9"></a>Ephesians 3:9

And to [phōtizō](../../strongs/g/g5461.md) all what is the [koinōnia](../../strongs/g/g2842.md) of the [mystērion](../../strongs/g/g3466.md), which from [aiōn](../../strongs/g/g165.md) hath been [apokryptō](../../strongs/g/g613.md) in [theos](../../strongs/g/g2316.md), who [ktizō](../../strongs/g/g2936.md) all things by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="ephesians_3_10"></a>Ephesians 3:10

To the intent that now unto the [archē](../../strongs/g/g746.md) and [exousia](../../strongs/g/g1849.md) in [epouranios](../../strongs/g/g2032.md) might be [gnōrizō](../../strongs/g/g1107.md) by the [ekklēsia](../../strongs/g/g1577.md) the [polypoikilos](../../strongs/g/g4182.md) [sophia](../../strongs/g/g4678.md) of [theos](../../strongs/g/g2316.md),

<a name="ephesians_3_11"></a>Ephesians 3:11

According to the [aiōn](../../strongs/g/g165.md) [prothesis](../../strongs/g/g4286.md) which he [poieō](../../strongs/g/g4160.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md):

<a name="ephesians_3_12"></a>Ephesians 3:12

In whom we have [parrēsia](../../strongs/g/g3954.md) and [prosagōgē](../../strongs/g/g4318.md) with [pepoithēsis](../../strongs/g/g4006.md) by the [pistis](../../strongs/g/g4102.md) of him.

<a name="ephesians_3_13"></a>Ephesians 3:13

Wherefore I [aiteō](../../strongs/g/g154.md) that ye [ekkakeō](../../strongs/g/g1573.md) not at my [thlipsis](../../strongs/g/g2347.md) for you, which is your [doxa](../../strongs/g/g1391.md).

<a name="ephesians_3_14"></a>Ephesians 3:14

For this cause I [kamptō](../../strongs/g/g2578.md) my [gony](../../strongs/g/g1119.md) unto the [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md),

<a name="ephesians_3_15"></a>Ephesians 3:15

Of whom the whole [patria](../../strongs/g/g3965.md) in [ouranos](../../strongs/g/g3772.md) and [epi](../../strongs/g/g1909.md) [gē](../../strongs/g/g1093.md) is [onomazō](../../strongs/g/g3687.md),

<a name="ephesians_3_16"></a>Ephesians 3:16

That he would [didōmi](../../strongs/g/g1325.md) you, according to the [ploutos](../../strongs/g/g4149.md) of his [doxa](../../strongs/g/g1391.md), to be [krataioō](../../strongs/g/g2901.md) with [dynamis](../../strongs/g/g1411.md) by his [pneuma](../../strongs/g/g4151.md) in the [esō](../../strongs/g/g2080.md) [anthrōpos](../../strongs/g/g444.md);

<a name="ephesians_3_17"></a>Ephesians 3:17

That [Christos](../../strongs/g/g5547.md) may [katoikeō](../../strongs/g/g2730.md) in your [kardia](../../strongs/g/g2588.md) by [pistis](../../strongs/g/g4102.md); that ye, being [rhizoō](../../strongs/g/g4492.md) and [themelioō](../../strongs/g/g2311.md) in [agapē](../../strongs/g/g26.md),

<a name="ephesians_3_18"></a>Ephesians 3:18

May [exischyō](../../strongs/g/g1840.md) to [katalambanō](../../strongs/g/g2638.md) with all [hagios](../../strongs/g/g40.md) what is the [platos](../../strongs/g/g4114.md), and [mēkos](../../strongs/g/g3372.md), and [bathos](../../strongs/g/g899.md), and [hypsos](../../strongs/g/g5311.md);

<a name="ephesians_3_19"></a>Ephesians 3:19

And to [ginōskō](../../strongs/g/g1097.md) the [agapē](../../strongs/g/g26.md) of [Christos](../../strongs/g/g5547.md), which [hyperballō](../../strongs/g/g5235.md) [gnōsis](../../strongs/g/g1108.md), that ye might be [plēroō](../../strongs/g/g4137.md) with all the [plērōma](../../strongs/g/g4138.md) of [theos](../../strongs/g/g2316.md).

<a name="ephesians_3_20"></a>Ephesians 3:20

Now unto him that is able to [poieō](../../strongs/g/g4160.md) [hyper](../../strongs/g/g5228.md) [perissos](../../strongs/g/g4053.md) [hyper](../../strongs/g/g5228.md) all that we [aiteō](../../strongs/g/g154.md) or [noeō](../../strongs/g/g3539.md), according to the [dynamis](../../strongs/g/g1411.md) that [energeō](../../strongs/g/g1754.md) in us,

<a name="ephesians_3_21"></a>Ephesians 3:21

Unto him [doxa](../../strongs/g/g1391.md) in the [ekklēsia](../../strongs/g/g1577.md) by [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) throughout all [genea](../../strongs/g/g1074.md), [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Ephesians](ephesians.md)

[Ephesians 2](ephesians_2.md) - [Ephesians 4](ephesians_4.md)