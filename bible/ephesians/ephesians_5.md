# [Ephesians 5](https://www.blueletterbible.org/kjv/eph/5/15/s_1102015)

<a name="ephesians_5_1"></a>Ephesians 5:1

Be ye therefore [mimētēs](../../strongs/g/g3402.md) of [theos](../../strongs/g/g2316.md), as [agapētos](../../strongs/g/g27.md) [teknon](../../strongs/g/g5043.md);

<a name="ephesians_5_2"></a>Ephesians 5:2

And [peripateō](../../strongs/g/g4043.md) in [agapē](../../strongs/g/g26.md), as [Christos](../../strongs/g/g5547.md) also hath [agapaō](../../strongs/g/g25.md) us, and hath [paradidōmi](../../strongs/g/g3860.md) himself for us a [prosphora](../../strongs/g/g4376.md) and a [thysia](../../strongs/g/g2378.md) to [theos](../../strongs/g/g2316.md) for an [euōdia](../../strongs/g/g2175.md) [osmē](../../strongs/g/g3744.md).

<a name="ephesians_5_3"></a>Ephesians 5:3

But [porneia](../../strongs/g/g4202.md), and all [akatharsia](../../strongs/g/g167.md), or [pleonexia](../../strongs/g/g4124.md), let it not be once [onomazō](../../strongs/g/g3687.md) among you, as becometh [hagios](../../strongs/g/g40.md);

<a name="ephesians_5_4"></a>Ephesians 5:4

Neither [aischrotēs](../../strongs/g/g151.md), nor [mōrologia](../../strongs/g/g3473.md), nor [eutrapelia](../../strongs/g/g2160.md), which are not [anēkō](../../strongs/g/g433.md): but rather [eucharistia](../../strongs/g/g2169.md).

<a name="ephesians_5_5"></a>Ephesians 5:5

For this ye [ginōskō](../../strongs/g/g1097.md), that no [pornos](../../strongs/g/g4205.md), nor [akathartos](../../strongs/g/g169.md), nor [pleonektēs](../../strongs/g/g4123.md), who is an [eidōlolatrēs](../../strongs/g/g1496.md), hath any [klēronomia](../../strongs/g/g2817.md) in the [basileia](../../strongs/g/g932.md) of [Christos](../../strongs/g/g5547.md) and of [theos](../../strongs/g/g2316.md).

<a name="ephesians_5_6"></a>Ephesians 5:6

Let [mēdeis](../../strongs/g/g3367.md) [apataō](../../strongs/g/g538.md) you with [kenos](../../strongs/g/g2756.md) [logos](../../strongs/g/g3056.md): for because of these things [erchomai](../../strongs/g/g2064.md) the [orgē](../../strongs/g/g3709.md) of [theos](../../strongs/g/g2316.md) upon the [huios](../../strongs/g/g5207.md) of [apeitheia](../../strongs/g/g543.md).

<a name="ephesians_5_7"></a>Ephesians 5:7

Be not ye therefore [symmetochos](../../strongs/g/g4830.md) with them.

<a name="ephesians_5_8"></a>Ephesians 5:8

For ye were sometimes [skotos](../../strongs/g/g4655.md), but now are ye [phōs](../../strongs/g/g5457.md) in the [kyrios](../../strongs/g/g2962.md): [peripateō](../../strongs/g/g4043.md) as [teknon](../../strongs/g/g5043.md) of [phōs](../../strongs/g/g5457.md):

<a name="ephesians_5_9"></a>Ephesians 5:9

(For the [karpos](../../strongs/g/g2590.md) of the [pneuma](../../strongs/g/g4151.md) is in all [agathōsynē](../../strongs/g/g19.md) and [dikaiosynē](../../strongs/g/g1343.md) and [alētheia](../../strongs/g/g225.md);)

<a name="ephesians_5_10"></a>Ephesians 5:10

[dokimazō](../../strongs/g/g1381.md) what is [euarestos](../../strongs/g/g2101.md) unto the [kyrios](../../strongs/g/g2962.md).

<a name="ephesians_5_11"></a>Ephesians 5:11

And have no [sygkoinōneō](../../strongs/g/g4790.md) with the [akarpos](../../strongs/g/g175.md) [ergon](../../strongs/g/g2041.md) of [skotos](../../strongs/g/g4655.md), but rather [elegchō](../../strongs/g/g1651.md) them.

<a name="ephesians_5_12"></a>Ephesians 5:12

For it is an [aischron](../../strongs/g/g149.md) even to [legō](../../strongs/g/g3004.md) of those things which are [ginomai](../../strongs/g/g1096.md) of them in [kryphē](../../strongs/g/g2931.md).

<a name="ephesians_5_13"></a>Ephesians 5:13

But all things that are [elegchō](../../strongs/g/g1651.md) are [phaneroō](../../strongs/g/g5319.md) by the [phōs](../../strongs/g/g5457.md): for whatsoever doth [phaneroō](../../strongs/g/g5319.md) is [phōs](../../strongs/g/g5457.md).

<a name="ephesians_5_14"></a>Ephesians 5:14

Wherefore he [legō](../../strongs/g/g3004.md), [egeirō](../../strongs/g/g1453.md) thou that [katheudō](../../strongs/g/g2518.md), and [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md), and [Christos](../../strongs/g/g5547.md) shall [epiphauskō](../../strongs/g/g2017.md) thee.

<a name="ephesians_5_15"></a>Ephesians 5:15

[blepō](../../strongs/g/g991.md) then that ye [peripateō](../../strongs/g/g4043.md) [akribōs](../../strongs/g/g199.md), not as [asophos](../../strongs/g/g781.md), but as [sophos](../../strongs/g/g4680.md),

<a name="ephesians_5_16"></a>Ephesians 5:16

[exagorazō](../../strongs/g/g1805.md) the [kairos](../../strongs/g/g2540.md), because the [hēmera](../../strongs/g/g2250.md) are [ponēros](../../strongs/g/g4190.md).

<a name="ephesians_5_17"></a>Ephesians 5:17

Wherefore be ye not [aphrōn](../../strongs/g/g878.md), but [syniēmi](../../strongs/g/g4920.md) what the [thelēma](../../strongs/g/g2307.md) of the [kyrios](../../strongs/g/g2962.md) is.

<a name="ephesians_5_18"></a>Ephesians 5:18

And be not [methyskō](../../strongs/g/g3182.md) with [oinos](../../strongs/g/g3631.md), wherein is [asōtia](../../strongs/g/g810.md); but be [plēroō](../../strongs/g/g4137.md) with the [pneuma](../../strongs/g/g4151.md);

<a name="ephesians_5_19"></a>Ephesians 5:19

[laleō](../../strongs/g/g2980.md) to yourselves in [psalmos](../../strongs/g/g5568.md) and [hymnos](../../strongs/g/g5215.md) and [pneumatikos](../../strongs/g/g4152.md) [ōdē](../../strongs/g/g5603.md), [adō](../../strongs/g/g103.md) and [psallō](../../strongs/g/g5567.md) in your [kardia](../../strongs/g/g2588.md) to the [kyrios](../../strongs/g/g2962.md);

<a name="ephesians_5_20"></a>Ephesians 5:20

[eucharisteō](../../strongs/g/g2168.md) [pantote](../../strongs/g/g3842.md) for all things unto [theos](../../strongs/g/g2316.md) and the [patēr](../../strongs/g/g3962.md) in the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md);

<a name="ephesians_5_21"></a>Ephesians 5:21

[hypotassō](../../strongs/g/g5293.md) to [allēlōn](../../strongs/g/g240.md) in the [phobos](../../strongs/g/g5401.md) of [theos](../../strongs/g/g2316.md).

<a name="ephesians_5_22"></a>Ephesians 5:22

[gynē](../../strongs/g/g1135.md), [hypotassō](../../strongs/g/g5293.md) unto your own [anēr](../../strongs/g/g435.md), as unto the [kyrios](../../strongs/g/g2962.md).

<a name="ephesians_5_23"></a>Ephesians 5:23

For the [anēr](../../strongs/g/g435.md) is the [kephalē](../../strongs/g/g2776.md) of the [gynē](../../strongs/g/g1135.md), even as [Christos](../../strongs/g/g5547.md) is the [kephalē](../../strongs/g/g2776.md) of the [ekklēsia](../../strongs/g/g1577.md): and he is the [sōtēr](../../strongs/g/g4990.md) of the [sōma](../../strongs/g/g4983.md).

<a name="ephesians_5_24"></a>Ephesians 5:24

Therefore as the [ekklēsia](../../strongs/g/g1577.md) [hypotassō](../../strongs/g/g5293.md) unto [Christos](../../strongs/g/g5547.md), so let the [gynē](../../strongs/g/g1135.md) be to their own [anēr](../../strongs/g/g435.md) in every thing.

<a name="ephesians_5_25"></a>Ephesians 5:25

[anēr](../../strongs/g/g435.md), [agapaō](../../strongs/g/g25.md) your [gynē](../../strongs/g/g1135.md), even as [Christos](../../strongs/g/g5547.md) also [agapaō](../../strongs/g/g25.md) the [ekklēsia](../../strongs/g/g1577.md), and [paradidōmi](../../strongs/g/g3860.md) himself for it;

<a name="ephesians_5_26"></a>Ephesians 5:26

That he might [hagiazō](../../strongs/g/g37.md) and [katharizō](../../strongs/g/g2511.md) with the [loutron](../../strongs/g/g3067.md) of [hydōr](../../strongs/g/g5204.md) by the [rhēma](../../strongs/g/g4487.md),

<a name="ephesians_5_27"></a>Ephesians 5:27

That he might [paristēmi](../../strongs/g/g3936.md) it to himself [endoxos](../../strongs/g/g1741.md) [ekklēsia](../../strongs/g/g1577.md), not having [spilos](../../strongs/g/g4696.md), or [rhytis](../../strongs/g/g4512.md), or any such thing; but that it should be [hagios](../../strongs/g/g40.md) and [amōmos](../../strongs/g/g299.md).

<a name="ephesians_5_28"></a>Ephesians 5:28

So [opheilō](../../strongs/g/g3784.md) [anēr](../../strongs/g/g435.md) to [agapaō](../../strongs/g/g25.md) their [gynē](../../strongs/g/g1135.md) as their own [sōma](../../strongs/g/g4983.md). He that [agapaō](../../strongs/g/g25.md) his wife [agapaō](../../strongs/g/g25.md) himself.

<a name="ephesians_5_29"></a>Ephesians 5:29

For [oudeis](../../strongs/g/g3762.md) ever yet [miseō](../../strongs/g/g3404.md) his own [sarx](../../strongs/g/g4561.md); but [ektrephō](../../strongs/g/g1625.md) and [thalpō](../../strongs/g/g2282.md) it, even as the [kyrios](../../strongs/g/g2962.md) the [ekklēsia](../../strongs/g/g1577.md):

<a name="ephesians_5_30"></a>Ephesians 5:30

For we are [melos](../../strongs/g/g3196.md) of his [sōma](../../strongs/g/g4983.md), of his [sarx](../../strongs/g/g4561.md), and of his [osteon](../../strongs/g/g3747.md). [^1]

<a name="ephesians_5_31"></a>Ephesians 5:31

For this cause shall [anthrōpos](../../strongs/g/g444.md) [kataleipō](../../strongs/g/g2641.md) his [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md), and shall be [proskollaō](../../strongs/g/g4347.md) unto his [gynē](../../strongs/g/g1135.md), and they two shall be one [sarx](../../strongs/g/g4561.md).

<a name="ephesians_5_32"></a>Ephesians 5:32

This is a [megas](../../strongs/g/g3173.md) [mystērion](../../strongs/g/g3466.md): but I [legō](../../strongs/g/g3004.md) concerning [Christos](../../strongs/g/g5547.md) and the [ekklēsia](../../strongs/g/g1577.md).

<a name="ephesians_5_33"></a>Ephesians 5:33

Nevertheless let every one of you in particular so [agapaō](../../strongs/g/g25.md) his [gynē](../../strongs/g/g1135.md) even as himself; and the [gynē](../../strongs/g/g1135.md) that she [phobeō](../../strongs/g/g5399.md) her [anēr](../../strongs/g/g435.md).

---

[Transliteral Bible](../bible.md)

[Ephesians](ephesians.md)

[Ephesians 4](ephesians_4.md) - [Ephesians 6](ephesians_6.md)

---

[^1]: [Ephesians 5:30 Commentary](../../commentary/ephesians/ephesians_5_commentary.md#ephesians_5_30)
