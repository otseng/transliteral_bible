# [Ephesians 6](https://www.blueletterbible.org/kjv/eph/6/1/s_1103001)

<a name="ephesians_6_1"></a>Ephesians 6:1

[teknon](../../strongs/g/g5043.md), [hypakouō](../../strongs/g/g5219.md) your [goneus](../../strongs/g/g1118.md) in the [kyrios](../../strongs/g/g2962.md): for this is [dikaios](../../strongs/g/g1342.md).

<a name="ephesians_6_2"></a>Ephesians 6:2

[timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md); which is the first [entolē](../../strongs/g/g1785.md) with [epaggelia](../../strongs/g/g1860.md);

<a name="ephesians_6_3"></a>Ephesians 6:3

That it may be [eu](../../strongs/g/g2095.md) with thee, and thou mayest [makrochronios](../../strongs/g/g3118.md) on [gē](../../strongs/g/g1093.md).

<a name="ephesians_6_4"></a>Ephesians 6:4

And, ye [patēr](../../strongs/g/g3962.md), [parorgizō](../../strongs/g/g3949.md) not your [teknon](../../strongs/g/g5043.md): but [ektrephō](../../strongs/g/g1625.md) them in the [paideia](../../strongs/g/g3809.md) and [nouthesia](../../strongs/g/g3559.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="ephesians_6_5"></a>Ephesians 6:5

[doulos](../../strongs/g/g1401.md), [hypakouō](../../strongs/g/g5219.md) [kyrios](../../strongs/g/g2962.md) according to the [sarx](../../strongs/g/g4561.md), with [phobos](../../strongs/g/g5401.md) and [tromos](../../strongs/g/g5156.md), in [haplotēs](../../strongs/g/g572.md) of your [kardia](../../strongs/g/g2588.md), as unto [Christos](../../strongs/g/g5547.md);

<a name="ephesians_6_6"></a>Ephesians 6:6

Not with [ophthalmodoulia](../../strongs/g/g3787.md), as [anthrōpareskos](../../strongs/g/g441.md); but as the [doulos](../../strongs/g/g1401.md) of [Christos](../../strongs/g/g5547.md), [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) from the [psychē](../../strongs/g/g5590.md);

<a name="ephesians_6_7"></a>Ephesians 6:7

With [eunoia](../../strongs/g/g2133.md) [douleuō](../../strongs/g/g1398.md), as to the [kyrios](../../strongs/g/g2962.md), and not to [anthrōpos](../../strongs/g/g444.md):

<a name="ephesians_6_8"></a>Ephesians 6:8

[eidō](../../strongs/g/g1492.md) that whatsoever [agathos](../../strongs/g/g18.md) [hekastos](../../strongs/g/g1538.md) [poieō](../../strongs/g/g4160.md), the same shall he [komizō](../../strongs/g/g2865.md) of the [kyrios](../../strongs/g/g2962.md), whether he be [doulos](../../strongs/g/g1401.md) or [eleutheros](../../strongs/g/g1658.md).

<a name="ephesians_6_9"></a>Ephesians 6:9

And, ye [kyrios](../../strongs/g/g2962.md), [poieō](../../strongs/g/g4160.md) the same things unto them, [aniēmi](../../strongs/g/g447.md) [apeilē](../../strongs/g/g547.md): [eidō](../../strongs/g/g1492.md) that your [kyrios](../../strongs/g/g2962.md) also is in [ouranos](../../strongs/g/g3772.md); neither is there [prosōpolēmpsia](../../strongs/g/g4382.md) with him.

<a name="ephesians_6_10"></a>Ephesians 6:10

[loipon](../../strongs/g/g3063.md), my [adelphos](../../strongs/g/g80.md), be [endynamoō](../../strongs/g/g1743.md) in the [kyrios](../../strongs/g/g2962.md), and in the [kratos](../../strongs/g/g2904.md) of his [ischys](../../strongs/g/g2479.md).

<a name="ephesians_6_11"></a>Ephesians 6:11

[endyō](../../strongs/g/g1746.md) the [panoplia](../../strongs/g/g3833.md) of [theos](../../strongs/g/g2316.md), that ye may be able to [histēmi](../../strongs/g/g2476.md) against the [methodeia](../../strongs/g/g3180.md) of the [diabolos](../../strongs/g/g1228.md).

<a name="ephesians_6_12"></a>Ephesians 6:12

For we [palē](../../strongs/g/g3823.md) not against [sarx](../../strongs/g/g4561.md) and [haima](../../strongs/g/g129.md), but against [archē](../../strongs/g/g746.md), against [exousia](../../strongs/g/g1849.md), against the [kosmokratōr](../../strongs/g/g2888.md) of the [skotos](../../strongs/g/g4655.md) of this [aiōn](../../strongs/g/g165.md), against [pneumatikos](../../strongs/g/g4152.md) [ponēria](../../strongs/g/g4189.md) in [epouranios](../../strongs/g/g2032.md).

<a name="ephesians_6_13"></a>Ephesians 6:13

Wherefore [analambanō](../../strongs/g/g353.md) unto you the [panoplia](../../strongs/g/g3833.md) of [theos](../../strongs/g/g2316.md), that ye may be able to [anthistēmi](../../strongs/g/g436.md) in the [ponēros](../../strongs/g/g4190.md) [hēmera](../../strongs/g/g2250.md), and having [katergazomai](../../strongs/g/g2716.md) all, [histēmi](../../strongs/g/g2476.md).

<a name="ephesians_6_14"></a>Ephesians 6:14

[histēmi](../../strongs/g/g2476.md) therefore, having your [osphys](../../strongs/g/g3751.md) [perizōnnymi](../../strongs/g/g4024.md) with [alētheia](../../strongs/g/g225.md), and [endyō](../../strongs/g/g1746.md) the [thōrax](../../strongs/g/g2382.md) of [dikaiosynē](../../strongs/g/g1343.md); [^1]

<a name="ephesians_6_15"></a>Ephesians 6:15

And your [pous](../../strongs/g/g4228.md) [hypodeō](../../strongs/g/g5265.md) with the [hetoimasia](../../strongs/g/g2091.md) of the [euaggelion](../../strongs/g/g2098.md) of [eirēnē](../../strongs/g/g1515.md);

<a name="ephesians_6_16"></a>Ephesians 6:16

Above all, [analambanō](../../strongs/g/g353.md) the [thyreos](../../strongs/g/g2375.md) of [pistis](../../strongs/g/g4102.md), wherewith ye shall be able to [sbennymi](../../strongs/g/g4570.md) all the [pyroō](../../strongs/g/g4448.md) [belos](../../strongs/g/g956.md) of the [ponēros](../../strongs/g/g4190.md).

<a name="ephesians_6_17"></a>Ephesians 6:17

And [dechomai](../../strongs/g/g1209.md) the [perikephalaia](../../strongs/g/g4030.md) of [sōtērios](../../strongs/g/g4992.md), and the [machaira](../../strongs/g/g3162.md) of the [pneuma](../../strongs/g/g4151.md), which is the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md):

<a name="ephesians_6_18"></a>Ephesians 6:18

[proseuchomai](../../strongs/g/g4336.md) [en](../../strongs/g/g1722.md) [kairos](../../strongs/g/g2540.md) [pas](../../strongs/g/g3956.md) with all [proseuchē](../../strongs/g/g4335.md) and [deēsis](../../strongs/g/g1162.md) in the [pneuma](../../strongs/g/g4151.md), and [agrypneō](../../strongs/g/g69.md) thereunto with all [proskarterēsis](../../strongs/g/g4343.md) and [deēsis](../../strongs/g/g1162.md) for all [hagios](../../strongs/g/g40.md);

<a name="ephesians_6_19"></a>Ephesians 6:19

And for me, that [logos](../../strongs/g/g3056.md) may be [didōmi](../../strongs/g/g1325.md) unto me, that I may [en](../../strongs/g/g1722.md) [anoixis](../../strongs/g/g457.md) my [stoma](../../strongs/g/g4750.md) [en](../../strongs/g/g1722.md) [parrēsia](../../strongs/g/g3954.md), to [gnōrizō](../../strongs/g/g1107.md) the [mystērion](../../strongs/g/g3466.md) of the [euaggelion](../../strongs/g/g2098.md),

<a name="ephesians_6_20"></a>Ephesians 6:20

For which I am a [presbeuō](../../strongs/g/g4243.md) in [halysis](../../strongs/g/g254.md): that therein I may [parrēsiazomai](../../strongs/g/g3955.md), as I ought to [laleō](../../strongs/g/g2980.md).

<a name="ephesians_6_21"></a>Ephesians 6:21

But that ye also may [eidō](../../strongs/g/g1492.md) my affairs, and how I [prassō](../../strongs/g/g4238.md), [Tychikos](../../strongs/g/g5190.md), an [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md) and [pistos](../../strongs/g/g4103.md) [diakonos](../../strongs/g/g1249.md) in the [kyrios](../../strongs/g/g2962.md), shall [gnōrizō](../../strongs/g/g1107.md) to you all things:

<a name="ephesians_6_22"></a>Ephesians 6:22

Whom I have [pempō](../../strongs/g/g3992.md) unto you for the same [touto](../../strongs/g/g5124.md), that ye might [ginōskō](../../strongs/g/g1097.md) our [peri](../../strongs/g/g4012.md), and that he might [parakaleō](../../strongs/g/g3870.md) your [kardia](../../strongs/g/g2588.md).

<a name="ephesians_6_23"></a>Ephesians 6:23

[eirēnē](../../strongs/g/g1515.md) be to the [adelphos](../../strongs/g/g80.md), and [agapē](../../strongs/g/g26.md) with [pistis](../../strongs/g/g4102.md), from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="ephesians_6_24"></a>Ephesians 6:24

[charis](../../strongs/g/g5485.md) with all them that [agapaō](../../strongs/g/g25.md) our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) in [aphtharsia](../../strongs/g/g861.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Ephesians](ephesians.md)

[Ephesians 5](ephesians_5.md)

[^1]: [Isaiah 59:17](../isaiah/isaiah_59.md#isaiah_59_17), [Isaiah 59:17 LXX](../isaiah/isaiah_lxx_59.md#isaiah_59_17)
