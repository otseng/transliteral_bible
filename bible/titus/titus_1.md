# [Titus 1](https://www.blueletterbible.org/kjv/tit/1/1/s_1130001)

<a name="titus_1_1"></a>Titus 1:1

[Paulos](../../strongs/g/g3972.md), a [doulos](../../strongs/g/g1401.md) of [theos](../../strongs/g/g2316.md), and an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), according to the [pistis](../../strongs/g/g4102.md) of [theos](../../strongs/g/g2316.md) [eklektos](../../strongs/g/g1588.md), and the [epignōsis](../../strongs/g/g1922.md) of the [alētheia](../../strongs/g/g225.md) which is after [eusebeia](../../strongs/g/g2150.md);

<a name="titus_1_2"></a>Titus 1:2

In [elpis](../../strongs/g/g1680.md) of [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), which [theos](../../strongs/g/g2316.md), [apseudēs](../../strongs/g/g893.md), [epaggellomai](../../strongs/g/g1861.md) before the [aiōnios](../../strongs/g/g166.md) [chronos](../../strongs/g/g5550.md);

<a name="titus_1_3"></a>Titus 1:3

But hath in due [kairos](../../strongs/g/g2540.md) [phaneroō](../../strongs/g/g5319.md) his [logos](../../strongs/g/g3056.md) through [kērygma](../../strongs/g/g2782.md), which is [pisteuō](../../strongs/g/g4100.md) unto me according to the [epitagē](../../strongs/g/g2003.md) of [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md);

<a name="titus_1_4"></a>Titus 1:4

To [titos](../../strongs/g/g5103.md), [gnēsios](../../strongs/g/g1103.md) [teknon](../../strongs/g/g5043.md) after the [koinos](../../strongs/g/g2839.md) [pistis](../../strongs/g/g4102.md): [charis](../../strongs/g/g5485.md), [eleos](../../strongs/g/g1656.md), [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [sōtēr](../../strongs/g/g4990.md).

<a name="titus_1_5"></a>Titus 1:5

For this [charin](../../strongs/g/g5484.md) [kataleipō](../../strongs/g/g2641.md) I thee in [Krētē](../../strongs/g/g2914.md), that thou shouldest [epidiorthoō](../../strongs/g/g1930.md) the things that are [leipō](../../strongs/g/g3007.md), and [kathistēmi](../../strongs/g/g2525.md) [presbyteros](../../strongs/g/g4245.md) in every [polis](../../strongs/g/g4172.md), as I had [diatassō](../../strongs/g/g1299.md) thee:

<a name="titus_1_6"></a>Titus 1:6

If any be [anegklētos](../../strongs/g/g410.md), the [anēr](../../strongs/g/g435.md) of one [gynē](../../strongs/g/g1135.md), having [pistos](../../strongs/g/g4103.md) [teknon](../../strongs/g/g5043.md) not [katēgoria](../../strongs/g/g2724.md) of [asōtia](../../strongs/g/g810.md) or [anypotaktos](../../strongs/g/g506.md).

<a name="titus_1_7"></a>Titus 1:7

For an [episkopos](../../strongs/g/g1985.md) must be [anegklētos](../../strongs/g/g410.md), as the [oikonomos](../../strongs/g/g3623.md) of [theos](../../strongs/g/g2316.md); not [authadēs](../../strongs/g/g829.md), not [orgilos](../../strongs/g/g3711.md), not [paroinos](../../strongs/g/g3943.md), not [plēktēs](../../strongs/g/g4131.md), not [aischrokerdēs](../../strongs/g/g146.md);

<a name="titus_1_8"></a>Titus 1:8

But a [philoxenos](../../strongs/g/g5382.md), a [philagathos](../../strongs/g/g5358.md), [sōphrōn](../../strongs/g/g4998.md), [dikaios](../../strongs/g/g1342.md), [hosios](../../strongs/g/g3741.md), [egkratēs](../../strongs/g/g1468.md);

<a name="titus_1_9"></a>Titus 1:9

[antechō](../../strongs/g/g472.md) the [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md) as he hath been [didachē](../../strongs/g/g1322.md), that he may be [dynatos](../../strongs/g/g1415.md) by [hygiainō](../../strongs/g/g5198.md) [didaskalia](../../strongs/g/g1319.md) both to [parakaleō](../../strongs/g/g3870.md) and to [elegchō](../../strongs/g/g1651.md) the [antilegō](../../strongs/g/g483.md).

<a name="titus_1_10"></a>Titus 1:10

For there are [polys](../../strongs/g/g4183.md) [anypotaktos](../../strongs/g/g506.md) and [mataiologos](../../strongs/g/g3151.md) and [phrenapatēs](../../strongs/g/g5423.md), [malista](../../strongs/g/g3122.md) they of the [peritomē](../../strongs/g/g4061.md):

<a name="titus_1_11"></a>Titus 1:11

Whose must [epistomizō](../../strongs/g/g1993.md), who [anatrepō](../../strongs/g/g396.md) [holos](../../strongs/g/g3650.md) [oikos](../../strongs/g/g3624.md), [didaskō](../../strongs/g/g1321.md) things which they ought not, for [aischros](../../strongs/g/g150.md) [kerdos](../../strongs/g/g2771.md).

<a name="titus_1_12"></a>Titus 1:12

One of themselves, a [prophētēs](../../strongs/g/g4396.md) of their own, [eipon](../../strongs/g/g2036.md), The [Krēs](../../strongs/g/g2912.md) alway [pseustēs](../../strongs/g/g5583.md), [kakos](../../strongs/g/g2556.md) [thērion](../../strongs/g/g2342.md), [argos](../../strongs/g/g692.md) [gastēr](../../strongs/g/g1064.md).

<a name="titus_1_13"></a>Titus 1:13

This [martyria](../../strongs/g/g3141.md) is [alēthēs](../../strongs/g/g227.md). Wherefore [elegchō](../../strongs/g/g1651.md) them [apotomōs](../../strongs/g/g664.md), that they may be [hygiainō](../../strongs/g/g5198.md) in the [pistis](../../strongs/g/g4102.md);

<a name="titus_1_14"></a>Titus 1:14

Not [prosechō](../../strongs/g/g4337.md) to [ioudaikos](../../strongs/g/g2451.md) [mythos](../../strongs/g/g3454.md), and [entolē](../../strongs/g/g1785.md) of [anthrōpos](../../strongs/g/g444.md), that [apostrephō](../../strongs/g/g654.md) the [alētheia](../../strongs/g/g225.md).

<a name="titus_1_15"></a>Titus 1:15

Unto the [katharos](../../strongs/g/g2513.md) all things [katharos](../../strongs/g/g2513.md): but unto them that are [miainō](../../strongs/g/g3392.md) and [apistos](../../strongs/g/g571.md) nothing [katharos](../../strongs/g/g2513.md); but even their [nous](../../strongs/g/g3563.md) and [syneidēsis](../../strongs/g/g4893.md) is [miainō](../../strongs/g/g3392.md).

<a name="titus_1_16"></a>Titus 1:16

They [homologeō](../../strongs/g/g3670.md) that they [eidō](../../strongs/g/g1492.md) [theos](../../strongs/g/g2316.md); but in [ergon](../../strongs/g/g2041.md) they [arneomai](../../strongs/g/g720.md), being [bdelyktos](../../strongs/g/g947.md), and [apeithēs](../../strongs/g/g545.md), and unto every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md) [adokimos](../../strongs/g/g96.md).

---

[Transliteral Bible](../bible.md)

[Titus](titus.md)

[Titus 2](titus_2.md)