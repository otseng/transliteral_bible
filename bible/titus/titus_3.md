# [Titus 3](https://www.blueletterbible.org/kjv/tit/3/1/s_1132001)

<a name="titus_3_1"></a>Titus 3:1

[hypomimnēskō](../../strongs/g/g5279.md) them to be [hypotassō](../../strongs/g/g5293.md) to [archē](../../strongs/g/g746.md) and [exousia](../../strongs/g/g1849.md), to [peitharcheō](../../strongs/g/g3980.md), to be [hetoimos](../../strongs/g/g2092.md) to every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md),

<a name="titus_3_2"></a>Titus 3:2

To [blasphēmeō](../../strongs/g/g987.md) of [mēdeis](../../strongs/g/g3367.md), to be [amachos](../../strongs/g/g269.md), [epieikēs](../../strongs/g/g1933.md), [endeiknymi](../../strongs/g/g1731.md) all [praotēs](../../strongs/g/g4236.md) unto all [anthrōpos](../../strongs/g/g444.md).

<a name="titus_3_3"></a>Titus 3:3

For we ourselves also were sometimes [anoētos](../../strongs/g/g453.md), [apeithēs](../../strongs/g/g545.md), [planaō](../../strongs/g/g4105.md), [douleuō](../../strongs/g/g1398.md) [poikilos](../../strongs/g/g4164.md) [epithymia](../../strongs/g/g1939.md) and [hēdonē](../../strongs/g/g2237.md), [diagō](../../strongs/g/g1236.md) in [kakia](../../strongs/g/g2549.md) and [phthonos](../../strongs/g/g5355.md), [stygētos](../../strongs/g/g4767.md), [miseō](../../strongs/g/g3404.md) [allēlōn](../../strongs/g/g240.md).

<a name="titus_3_4"></a>Titus 3:4

But after that the [chrēstotēs](../../strongs/g/g5544.md) and [philanthrōpia](../../strongs/g/g5363.md) of [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md) [epiphainō](../../strongs/g/g2014.md),

<a name="titus_3_5"></a>Titus 3:5

Not by [ergon](../../strongs/g/g2041.md) of [dikaiosynē](../../strongs/g/g1343.md) which we have [poieō](../../strongs/g/g4160.md), but according to his [eleos](../../strongs/g/g1656.md) he [sōzō](../../strongs/g/g4982.md) us, by the [loutron](../../strongs/g/g3067.md) of [paliggenesia](../../strongs/g/g3824.md), and [anakainōsis](../../strongs/g/g342.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md);

<a name="titus_3_6"></a>Titus 3:6

Which he [ekcheō](../../strongs/g/g1632.md) on us [plousiōs](../../strongs/g/g4146.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [sōtēr](../../strongs/g/g4990.md);

<a name="titus_3_7"></a>Titus 3:7

That being [dikaioō](../../strongs/g/g1344.md) by his [charis](../../strongs/g/g5485.md), we should be made [klēronomos](../../strongs/g/g2818.md) according to the [elpis](../../strongs/g/g1680.md) of [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="titus_3_8"></a>Titus 3:8

a [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md), and these things I [boulomai](../../strongs/g/g1014.md) that thou [diabebaioomai](../../strongs/g/g1226.md), that they which have [pisteuō](../../strongs/g/g4100.md) in [theos](../../strongs/g/g2316.md) might be [phrontizō](../../strongs/g/g5431.md) to [proistēmi](../../strongs/g/g4291.md) [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md). These things are [kalos](../../strongs/g/g2570.md) and [ōphelimos](../../strongs/g/g5624.md) unto [anthrōpos](../../strongs/g/g444.md).

<a name="titus_3_9"></a>Titus 3:9

But [periistēmi](../../strongs/g/g4026.md) [mōros](../../strongs/g/g3474.md) [zētēsis](../../strongs/g/g2214.md), and [genealogia](../../strongs/g/g1076.md), and [eris](../../strongs/g/g2054.md), and [machē](../../strongs/g/g3163.md) about the [nomikos](../../strongs/g/g3544.md); for they are [anōphelēs](../../strongs/g/g512.md) and [mataios](../../strongs/g/g3152.md).

<a name="titus_3_10"></a>Titus 3:10

an [anthrōpos](../../strongs/g/g444.md) that is an [hairetikos](../../strongs/g/g141.md) after the first and second [nouthesia](../../strongs/g/g3559.md) [paraiteomai](../../strongs/g/g3868.md);

<a name="titus_3_11"></a>Titus 3:11

[eidō](../../strongs/g/g1492.md) that he that is such is [ekstrephō](../../strongs/g/g1612.md), and [hamartanō](../../strongs/g/g264.md), being [autokatakritos](../../strongs/g/g843.md).

<a name="titus_3_12"></a>Titus 3:12

When I shall [pempō](../../strongs/g/g3992.md) [artemas](../../strongs/g/g734.md) unto thee, or [Tychikos](../../strongs/g/g5190.md), [spoudazō](../../strongs/g/g4704.md) to [erchomai](../../strongs/g/g2064.md) unto me to [nikopolis](../../strongs/g/g3533.md): for I have [krinō](../../strongs/g/g2919.md) there to [paracheimazō](../../strongs/g/g3914.md).

<a name="titus_3_13"></a>Titus 3:13

[propempō](../../strongs/g/g4311.md) [zēnas](../../strongs/g/g2211.md) the [nomikos](../../strongs/g/g3544.md) and [Apollōs](../../strongs/g/g625.md) on their [propempō](../../strongs/g/g4311.md) [spoudaiōs](../../strongs/g/g4709.md), that [mēdeis](../../strongs/g/g3367.md) be [leipō](../../strongs/g/g3007.md) unto them.

<a name="titus_3_14"></a>Titus 3:14

And let our's also [manthanō](../../strongs/g/g3129.md) to [proistēmi](../../strongs/g/g4291.md) [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) for [anagkaios](../../strongs/g/g316.md) [chreia](../../strongs/g/g5532.md), that they be not [akarpos](../../strongs/g/g175.md).

<a name="titus_3_15"></a>Titus 3:15

All that are with me [aspazomai](../../strongs/g/g782.md) thee. [aspazomai](../../strongs/g/g782.md) them that [phileō](../../strongs/g/g5368.md) us in the [pistis](../../strongs/g/g4102.md). [charis](../../strongs/g/g5485.md) with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Titus](titus.md)

[Titus 2](titus_2.md)