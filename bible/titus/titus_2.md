# [Titus 2](https://www.blueletterbible.org/kjv/tit/2/11/s_1131001)

<a name="titus_2_1"></a>Titus 2:1

But [laleō](../../strongs/g/g2980.md) thou the things which become [hygiainō](../../strongs/g/g5198.md) [didaskalia](../../strongs/g/g1319.md):

<a name="titus_2_2"></a>Titus 2:2

That the [presbytēs](../../strongs/g/g4246.md) be [nēphalios](../../strongs/g/g3524.md), [semnos](../../strongs/g/g4586.md), [sōphrōn](../../strongs/g/g4998.md), [hygiainō](../../strongs/g/g5198.md) in [pistis](../../strongs/g/g4102.md), in [agapē](../../strongs/g/g26.md), in [hypomonē](../../strongs/g/g5281.md).

<a name="titus_2_3"></a>Titus 2:3

The [presbytis](../../strongs/g/g4247.md) [hōsautōs](../../strongs/g/g5615.md), that in [katastēma](../../strongs/g/g2688.md) as [hieroprepēs](../../strongs/g/g2412.md), not [diabolos](../../strongs/g/g1228.md), not [douloō](../../strongs/g/g1402.md) to [polys](../../strongs/g/g4183.md) [oinos](../../strongs/g/g3631.md), [kalodidaskalos](../../strongs/g/g2567.md);

<a name="titus_2_4"></a>Titus 2:4

That they may [sōphronizō](../../strongs/g/g4994.md) the [neos](../../strongs/g/g3501.md), to [philandros](../../strongs/g/g5362.md), to [philoteknos](../../strongs/g/g5388.md),

<a name="titus_2_5"></a>Titus 2:5

[sōphrōn](../../strongs/g/g4998.md), [hagnos](../../strongs/g/g53.md), [oikourgos](../../strongs/g/g3626.md), [agathos](../../strongs/g/g18.md), [hypotassō](../../strongs/g/g5293.md) to their own [anēr](../../strongs/g/g435.md), that the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) be not [blasphēmeō](../../strongs/g/g987.md).

<a name="titus_2_6"></a>Titus 2:6

[neos](../../strongs/g/g3501.md) [hōsautōs](../../strongs/g/g5615.md) [parakaleō](../../strongs/g/g3870.md) to be [sōphroneō](../../strongs/g/g4993.md).

<a name="titus_2_7"></a>Titus 2:7

In all things [parechō](../../strongs/g/g3930.md) thyself a [typos](../../strongs/g/g5179.md) of [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md): in [didaskalia](../../strongs/g/g1319.md) [adiaphthoria](../../strongs/g/g90.md), [semnotēs](../../strongs/g/g4587.md), [aphtharsia](../../strongs/g/g861.md),

<a name="titus_2_8"></a>Titus 2:8

[hygiēs](../../strongs/g/g5199.md) [logos](../../strongs/g/g3056.md), [akatagnōstos](../../strongs/g/g176.md); that he that is of the [enantios](../../strongs/g/g1727.md) may be [entrepō](../../strongs/g/g1788.md), having [mēdeis](../../strongs/g/g3367.md) [phaulos](../../strongs/g/g5337.md) to [legō](../../strongs/g/g3004.md) of you.

<a name="titus_2_9"></a>Titus 2:9

[doulos](../../strongs/g/g1401.md) to be [hypotassō](../../strongs/g/g5293.md) unto their own [despotēs](../../strongs/g/g1203.md), to [euarestos](../../strongs/g/g2101.md) in all; not [antilegō](../../strongs/g/g483.md);

<a name="titus_2_10"></a>Titus 2:10

Not [nosphizō](../../strongs/g/g3557.md), but [endeiknymi](../../strongs/g/g1731.md) all [agathos](../../strongs/g/g18.md) [pistis](../../strongs/g/g4102.md); that they may [kosmeō](../../strongs/g/g2885.md) the [didaskalia](../../strongs/g/g1319.md) of [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md) in all things.

<a name="titus_2_11"></a>Titus 2:11

For the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) [sōtērios](../../strongs/g/g4992.md) hath [epiphainō](../../strongs/g/g2014.md) to all [anthrōpos](../../strongs/g/g444.md),

<a name="titus_2_12"></a>Titus 2:12

[paideuō](../../strongs/g/g3811.md) us that, [arneomai](../../strongs/g/g720.md) [asebeia](../../strongs/g/g763.md) and [kosmikos](../../strongs/g/g2886.md) [epithymia](../../strongs/g/g1939.md), we should [zaō](../../strongs/g/g2198.md) [sōphronōs](../../strongs/g/g4996.md), [dikaiōs](../../strongs/g/g1346.md), and [eusebōs](../../strongs/g/g2153.md), in this present [aiōn](../../strongs/g/g165.md);

<a name="titus_2_13"></a>Titus 2:13

[prosdechomai](../../strongs/g/g4327.md) [makarios](../../strongs/g/g3107.md) [elpis](../../strongs/g/g1680.md), and the [doxa](../../strongs/g/g1391.md) [epiphaneia](../../strongs/g/g2015.md) of the [megas](../../strongs/g/g3173.md) [theos](../../strongs/g/g2316.md) and our [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md);

<a name="titus_2_14"></a>Titus 2:14

Who [didōmi](../../strongs/g/g1325.md) [heautou](../../strongs/g/g1438.md) for us, that he might [lytroō](../../strongs/g/g3084.md) us from all [anomia](../../strongs/g/g458.md), and [katharizō](../../strongs/g/g2511.md) unto himself a [periousios](../../strongs/g/g4041.md) [laos](../../strongs/g/g2992.md), [zēlōtēs](../../strongs/g/g2207.md) of [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md).

<a name="titus_2_15"></a>Titus 2:15

These things [laleō](../../strongs/g/g2980.md), and [parakaleō](../../strongs/g/g3870.md), and [elegchō](../../strongs/g/g1651.md) with all [epitagē](../../strongs/g/g2003.md). Let [mēdeis](../../strongs/g/g3367.md) [periphroneō](../../strongs/g/g4065.md) thee.

---

[Transliteral Bible](../bible.md)

[Titus](titus.md)

[Titus 1](titus_1.md) - [Titus 3](titus_3.md)