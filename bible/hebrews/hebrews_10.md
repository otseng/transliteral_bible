# [Hebrews 10](https://www.blueletterbible.org/kjv/heb/10/1/s_1143001)

<a name="hebrews_10_1"></a>Hebrews 10:1

For the [nomos](../../strongs/g/g3551.md) having a [skia](../../strongs/g/g4639.md) of [agathos](../../strongs/g/g18.md) to [mellō](../../strongs/g/g3195.md), not the very [eikōn](../../strongs/g/g1504.md) of the [pragma](../../strongs/g/g4229.md), can never with those [thysia](../../strongs/g/g2378.md) which they [prospherō](../../strongs/g/g4374.md) [kata](../../strongs/g/g2596.md) [eniautos](../../strongs/g/g1763.md) [eis](../../strongs/g/g1519.md) [diēnekēs](../../strongs/g/g1336.md) make the [proserchomai](../../strongs/g/g4334.md) thereunto [teleioō](../../strongs/g/g5048.md).

<a name="hebrews_10_2"></a>Hebrews 10:2

For then would they not have [pauō](../../strongs/g/g3973.md) to be [prospherō](../../strongs/g/g4374.md)? because that the [latreuō](../../strongs/g/g3000.md) [hapax](../../strongs/g/g530.md) [kathairō](../../strongs/g/g2508.md) should have had [mēdeis](../../strongs/g/g3367.md) more [syneidēsis](../../strongs/g/g4893.md) of [hamartia](../../strongs/g/g266.md).

<a name="hebrews_10_3"></a>Hebrews 10:3

But in those an [anamnēsis](../../strongs/g/g364.md) of [hamartia](../../strongs/g/g266.md) every [eniautos](../../strongs/g/g1763.md).

<a name="hebrews_10_4"></a>Hebrews 10:4

For [adynatos](../../strongs/g/g102.md) that the [haima](../../strongs/g/g129.md) of [tauros](../../strongs/g/g5022.md) and of [tragos](../../strongs/g/g5131.md) should [aphaireō](../../strongs/g/g851.md) [hamartia](../../strongs/g/g266.md).

<a name="hebrews_10_5"></a>Hebrews 10:5

Wherefore when he [eiserchomai](../../strongs/g/g1525.md) into the [kosmos](../../strongs/g/g2889.md), he [legō](../../strongs/g/g3004.md), [thysia](../../strongs/g/g2378.md) and [prosphora](../../strongs/g/g4376.md) thou wouldest not, but a [sōma](../../strongs/g/g4983.md) hast thou [katartizō](../../strongs/g/g2675.md) me:

<a name="hebrews_10_6"></a>Hebrews 10:6

In [holokautōma](../../strongs/g/g3646.md) and for [hamartia](../../strongs/g/g266.md) thou hast had no [eudokeō](../../strongs/g/g2106.md).

<a name="hebrews_10_7"></a>Hebrews 10:7

Then [eipon](../../strongs/g/g2036.md) I, [idou](../../strongs/g/g2400.md), I [hēkō](../../strongs/g/g2240.md) (in the [kephalis](../../strongs/g/g2777.md) of the [biblion](../../strongs/g/g975.md) it is [graphō](../../strongs/g/g1125.md) of me,) to [poieō](../../strongs/g/g4160.md) thy [thelēma](../../strongs/g/g2307.md), O [theos](../../strongs/g/g2316.md).

<a name="hebrews_10_8"></a>Hebrews 10:8

[anōteros](../../strongs/g/g511.md) when he [legō](../../strongs/g/g3004.md), [thysia](../../strongs/g/g2378.md) and [prosphora](../../strongs/g/g4376.md) and [holokautōma](../../strongs/g/g3646.md) and for [hamartia](../../strongs/g/g266.md) thou wouldest not, neither hadst [eudokeō](../../strongs/g/g2106.md); which are [prospherō](../../strongs/g/g4374.md) by the [nomos](../../strongs/g/g3551.md);

<a name="hebrews_10_9"></a>Hebrews 10:9

Then [eipon](../../strongs/g/g2046.md) he, [idou](../../strongs/g/g2400.md), I [hēkō](../../strongs/g/g2240.md) to [poieō](../../strongs/g/g4160.md) thy [thelēma](../../strongs/g/g2307.md), O [theos](../../strongs/g/g2316.md). He [anaireō](../../strongs/g/g337.md) the first, that he may [histēmi](../../strongs/g/g2476.md) the second.

<a name="hebrews_10_10"></a>Hebrews 10:10

By the which [thelēma](../../strongs/g/g2307.md) we are [hagiazō](../../strongs/g/g37.md) through the [prosphora](../../strongs/g/g4376.md) of the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) [ephapax](../../strongs/g/g2178.md) for all.

<a name="hebrews_10_11"></a>Hebrews 10:11

And every [hiereus](../../strongs/g/g2409.md) [histēmi](../../strongs/g/g2476.md) [kata](../../strongs/g/g2596.md) [hēmera](../../strongs/g/g2250.md) [leitourgeō](../../strongs/g/g3008.md) and [prospherō](../../strongs/g/g4374.md) oftentimes the same [thysia](../../strongs/g/g2378.md), which can never [periaireō](../../strongs/g/g4014.md) [hamartia](../../strongs/g/g266.md):

<a name="hebrews_10_12"></a>Hebrews 10:12

But [autos](../../strongs/g/g846.md), after he had [prospherō](../../strongs/g/g4374.md) one [thysia](../../strongs/g/g2378.md) for [hamartia](../../strongs/g/g266.md) [eis](../../strongs/g/g1519.md) [diēnekēs](../../strongs/g/g1336.md), [kathizō](../../strongs/g/g2523.md) on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md);

<a name="hebrews_10_13"></a>Hebrews 10:13

From henceforth [ekdechomai](../../strongs/g/g1551.md) till his [echthros](../../strongs/g/g2190.md) be [tithēmi](../../strongs/g/g5087.md) his [hypopodion](../../strongs/g/g5286.md) [pous](../../strongs/g/g4228.md).

<a name="hebrews_10_14"></a>Hebrews 10:14

For by one [prosphora](../../strongs/g/g4376.md) he hath [teleioō](../../strongs/g/g5048.md) [eis](../../strongs/g/g1519.md) [diēnekēs](../../strongs/g/g1336.md) them that are [hagiazō](../../strongs/g/g37.md).

<a name="hebrews_10_15"></a>Hebrews 10:15

the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) also is a [martyreō](../../strongs/g/g3140.md) to us: for after that he had [proereō](../../strongs/g/g4280.md),

<a name="hebrews_10_16"></a>Hebrews 10:16

This the [diathēkē](../../strongs/g/g1242.md) that I will [diatithēmi](../../strongs/g/g1303.md) with them after those [hēmera](../../strongs/g/g2250.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), I will [didōmi](../../strongs/g/g1325.md) my [nomos](../../strongs/g/g3551.md) into their [kardia](../../strongs/g/g2588.md), and in their [dianoia](../../strongs/g/g1271.md) will I [epigraphō](../../strongs/g/g1924.md) them;

<a name="hebrews_10_17"></a>Hebrews 10:17

And their [hamartia](../../strongs/g/g266.md) and [anomia](../../strongs/g/g458.md) will I [mnaomai](../../strongs/g/g3415.md) no more.

<a name="hebrews_10_18"></a>Hebrews 10:18

Now where [aphesis](../../strongs/g/g859.md) of these, no more [prosphora](../../strongs/g/g4376.md) for [hamartia](../../strongs/g/g266.md).

<a name="hebrews_10_19"></a>Hebrews 10:19

Having therefore, [adelphos](../../strongs/g/g80.md), [parrēsia](../../strongs/g/g3954.md) to [eisodos](../../strongs/g/g1529.md) into the [hagion](../../strongs/g/g39.md) by the [haima](../../strongs/g/g129.md) of [Iēsous](../../strongs/g/g2424.md),

<a name="hebrews_10_20"></a>Hebrews 10:20

By a [prosphatos](../../strongs/g/g4372.md) and [zaō](../../strongs/g/g2198.md) [hodos](../../strongs/g/g3598.md), which he hath [egkainizō](../../strongs/g/g1457.md) for us, through the [katapetasma](../../strongs/g/g2665.md), [tout᾿ estin](../../strongs/g/g5123.md), his [sarx](../../strongs/g/g4561.md);

<a name="hebrews_10_21"></a>Hebrews 10:21

And a [megas](../../strongs/g/g3173.md) [hiereus](../../strongs/g/g2409.md) over the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md);

<a name="hebrews_10_22"></a>Hebrews 10:22

Let us [proserchomai](../../strongs/g/g4334.md) with an [alēthinos](../../strongs/g/g228.md) [kardia](../../strongs/g/g2588.md) in [plērophoria](../../strongs/g/g4136.md) of [pistis](../../strongs/g/g4102.md), having our [kardia](../../strongs/g/g2588.md) [rhantizō](../../strongs/g/g4472.md) from a [ponēros](../../strongs/g/g4190.md) [syneidēsis](../../strongs/g/g4893.md), and our [sōma](../../strongs/g/g4983.md) [louō](../../strongs/g/g3068.md) with [katharos](../../strongs/g/g2513.md) [hydōr](../../strongs/g/g5204.md).

<a name="hebrews_10_23"></a>Hebrews 10:23

Let us [katechō](../../strongs/g/g2722.md) the [homologia](../../strongs/g/g3671.md) of [elpis](../../strongs/g/g1680.md) [aklinēs](../../strongs/g/g186.md); (for he [pistos](../../strongs/g/g4103.md) that [epaggellomai](../../strongs/g/g1861.md);)

<a name="hebrews_10_24"></a>Hebrews 10:24

And let us [katanoeō](../../strongs/g/g2657.md) [allēlōn](../../strongs/g/g240.md) to [paroxysmos](../../strongs/g/g3948.md) unto [agapē](../../strongs/g/g26.md) and to [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md):

<a name="hebrews_10_25"></a>Hebrews 10:25

Not [egkataleipō](../../strongs/g/g1459.md) the [episynagōgē](../../strongs/g/g1997.md) of ourselves, as the [ethos](../../strongs/g/g1485.md) of some is; but [parakaleō](../../strongs/g/g3870.md): and [tosoutos](../../strongs/g/g5118.md) the [mallon](../../strongs/g/g3123.md), as ye [blepō](../../strongs/g/g991.md) the [hēmera](../../strongs/g/g2250.md) [eggizō](../../strongs/g/g1448.md).

<a name="hebrews_10_26"></a>Hebrews 10:26

For if we [hamartanō](../../strongs/g/g264.md) [hekousiōs](../../strongs/g/g1596.md) after that we have [lambanō](../../strongs/g/g2983.md) the [epignōsis](../../strongs/g/g1922.md) of the [alētheia](../../strongs/g/g225.md), there [apoleipō](../../strongs/g/g620.md) no more [thysia](../../strongs/g/g2378.md) for [hamartia](../../strongs/g/g266.md),

<a name="hebrews_10_27"></a>Hebrews 10:27

But a certain [phoberos](../../strongs/g/g5398.md) [ekdochē](../../strongs/g/g1561.md) of [krisis](../../strongs/g/g2920.md) and [pyr](../../strongs/g/g4442.md) [zēlos](../../strongs/g/g2205.md), which shall [esthiō](../../strongs/g/g2068.md) the [hypenantios](../../strongs/g/g5227.md).

<a name="hebrews_10_28"></a>Hebrews 10:28

He that [atheteō](../../strongs/g/g114.md) [Mōÿsēs](../../strongs/g/g3475.md) [nomos](../../strongs/g/g3551.md) [apothnēskō](../../strongs/g/g599.md) without [oiktirmos](../../strongs/g/g3628.md) under two or three [martys](../../strongs/g/g3144.md):

<a name="hebrews_10_29"></a>Hebrews 10:29

Of how much [cheirōn](../../strongs/g/g5501.md) [timōria](../../strongs/g/g5098.md), [dokeō](../../strongs/g/g1380.md) ye, shall he be [axioō](../../strongs/g/g515.md), who hath [katapateō](../../strongs/g/g2662.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), and hath [hēgeomai](../../strongs/g/g2233.md) the [haima](../../strongs/g/g129.md) of the [diathēkē](../../strongs/g/g1242.md), wherewith he was [hagiazō](../../strongs/g/g37.md), a [koinos](../../strongs/g/g2839.md), and hath [enybrizō](../../strongs/g/g1796.md) the [pneuma](../../strongs/g/g4151.md) of [charis](../../strongs/g/g5485.md)?

<a name="hebrews_10_30"></a>Hebrews 10:30

For we [eidō](../../strongs/g/g1492.md) him that hath [eipon](../../strongs/g/g2036.md), [ekdikēsis](../../strongs/g/g1557.md) unto me, I will [antapodidōmi](../../strongs/g/g467.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md). And again, The [kyrios](../../strongs/g/g2962.md) shall [krinō](../../strongs/g/g2919.md) his [laos](../../strongs/g/g2992.md).

<a name="hebrews_10_31"></a>Hebrews 10:31

a [phoberos](../../strongs/g/g5398.md) to [empiptō](../../strongs/g/g1706.md) into the [cheir](../../strongs/g/g5495.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md).

<a name="hebrews_10_32"></a>Hebrews 10:32

But [anamimnēskō](../../strongs/g/g363.md) the [proteros](../../strongs/g/g4386.md) [hēmera](../../strongs/g/g2250.md), in which, after ye were [phōtizō](../../strongs/g/g5461.md), ye [hypomenō](../../strongs/g/g5278.md) a [polys](../../strongs/g/g4183.md) [athlēsis](../../strongs/g/g119.md) of [pathēma](../../strongs/g/g3804.md);

<a name="hebrews_10_33"></a>Hebrews 10:33

Partly, whilst ye were [theatrizō](../../strongs/g/g2301.md) both by [oneidismos](../../strongs/g/g3680.md) and [thlipsis](../../strongs/g/g2347.md); and partly, whilst ye became [koinōnos](../../strongs/g/g2844.md) of them that [anastrephō](../../strongs/g/g390.md) so.

<a name="hebrews_10_34"></a>Hebrews 10:34

For ye had [sympatheō](../../strongs/g/g4834.md) of me in my [desmos](../../strongs/g/g1199.md), and [prosdechomai](../../strongs/g/g4327.md) [meta](../../strongs/g/g3326.md) [chara](../../strongs/g/g5479.md) the [harpagē](../../strongs/g/g724.md) of your [hyparchonta](../../strongs/g/g5224.md), [ginōskō](../../strongs/g/g1097.md) in yourselves that ye have in [ouranos](../../strongs/g/g3772.md) a [kreittōn](../../strongs/g/g2909.md) and a [menō](../../strongs/g/g3306.md) [hyparxis](../../strongs/g/g5223.md). [^1]

<a name="hebrews_10_35"></a>Hebrews 10:35

[apoballō](../../strongs/g/g577.md) not away therefore your [parrēsia](../../strongs/g/g3954.md), which hath [megas](../../strongs/g/g3173.md) [misthapodosia](../../strongs/g/g3405.md).

<a name="hebrews_10_36"></a>Hebrews 10:36

For ye have [chreia](../../strongs/g/g5532.md) of [hypomonē](../../strongs/g/g5281.md), that, after ye have [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), ye might [komizō](../../strongs/g/g2865.md) the [epaggelia](../../strongs/g/g1860.md).

<a name="hebrews_10_37"></a>Hebrews 10:37

For yet a [mikron](../../strongs/g/g3397.md) while, and he that shall [erchomai](../../strongs/g/g2064.md) will [hēkō](../../strongs/g/g2240.md), and will not [chronizō](../../strongs/g/g5549.md).

<a name="hebrews_10_38"></a>Hebrews 10:38

Now the [dikaios](../../strongs/g/g1342.md) shall [zaō](../../strongs/g/g2198.md) by [pistis](../../strongs/g/g4102.md): but if [hypostellō](../../strongs/g/g5288.md), my [psychē](../../strongs/g/g5590.md) shall have no [eudokeō](../../strongs/g/g2106.md) in him.

<a name="hebrews_10_39"></a>Hebrews 10:39

But we are not of them who [hypostolē](../../strongs/g/g5289.md) unto [apōleia](../../strongs/g/g684.md); but of them that [pistis](../../strongs/g/g4102.md) to the [peripoiēsis](../../strongs/g/g4047.md) of the [psychē](../../strongs/g/g5590.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 9](hebrews_9.md) - [Hebrews 11](hebrews_11.md)

---

[^1]: [Hebrews 10:34 Commentary](../../commentary/hebrews/hebrews_10_commentary.md#hebrews_10_34)
