# [Hebrews 5](https://www.blueletterbible.org/kjv/heb/5)

<a name="hebrews_5_1"></a>Hebrews 5:1

For every [archiereus](../../strongs/g/g749.md) [lambanō](../../strongs/g/g2983.md) from among [anthrōpos](../../strongs/g/g444.md) is [kathistēmi](../../strongs/g/g2525.md) for [anthrōpos](../../strongs/g/g444.md) in things to [theos](../../strongs/g/g2316.md), that he may [prospherō](../../strongs/g/g4374.md) both [dōron](../../strongs/g/g1435.md) and [thysia](../../strongs/g/g2378.md) for [hamartia](../../strongs/g/g266.md):

<a name="hebrews_5_2"></a>Hebrews 5:2

Who can have [metriopatheō](../../strongs/g/g3356.md) on the [agnoeō](../../strongs/g/g50.md), and on [planaō](../../strongs/g/g4105.md); for that he himself also is [perikeimai](../../strongs/g/g4029.md) with [astheneia](../../strongs/g/g769.md).

<a name="hebrews_5_3"></a>Hebrews 5:3

And by reason hereof he [opheilō](../../strongs/g/g3784.md), as for the [laos](../../strongs/g/g2992.md), so also for himself, to [prospherō](../../strongs/g/g4374.md) for [hamartia](../../strongs/g/g266.md).

<a name="hebrews_5_4"></a>Hebrews 5:4

And no [tis](../../strongs/g/g5100.md) [lambanō](../../strongs/g/g2983.md) this [timē](../../strongs/g/g5092.md) unto himself, but he that is [kaleō](../../strongs/g/g2564.md) of [theos](../../strongs/g/g2316.md), as was [Aarōn](../../strongs/g/g2.md).

<a name="hebrews_5_5"></a>Hebrews 5:5

So also [Christos](../../strongs/g/g5547.md) [doxazō](../../strongs/g/g1392.md) not himself to be made an [archiereus](../../strongs/g/g749.md); but he that [laleō](../../strongs/g/g2980.md) unto him, Thou art my [huios](../../strongs/g/g5207.md), [sēmeron](../../strongs/g/g4594.md) have I [gennaō](../../strongs/g/g1080.md) thee.

<a name="hebrews_5_6"></a>Hebrews 5:6

As he [legō](../../strongs/g/g3004.md) also in another place, Thou a [hiereus](../../strongs/g/g2409.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md).

<a name="hebrews_5_7"></a>Hebrews 5:7

Who in the [hēmera](../../strongs/g/g2250.md) of his [sarx](../../strongs/g/g4561.md), when he had [prospherō](../../strongs/g/g4374.md) [deēsis](../../strongs/g/g1162.md) and [hiketēria](../../strongs/g/g2428.md) with [ischyros](../../strongs/g/g2478.md) [kraugē](../../strongs/g/g2906.md) and [dakry](../../strongs/g/g1144.md) unto him that was able to [sōzō](../../strongs/g/g4982.md) him from [thanatos](../../strongs/g/g2288.md), and was [eisakouō](../../strongs/g/g1522.md) in that he [eulabeia](../../strongs/g/g2124.md);

<a name="hebrews_5_8"></a>Hebrews 5:8

[kaiper](../../strongs/g/g2539.md) he were a [huios](../../strongs/g/g5207.md), yet [manthanō](../../strongs/g/g3129.md) he [hypakoē](../../strongs/g/g5218.md) by the things which he [paschō](../../strongs/g/g3958.md);

<a name="hebrews_5_9"></a>Hebrews 5:9

And being [teleioō](../../strongs/g/g5048.md), he became the [aitios](../../strongs/g/g159.md) of [aiōnios](../../strongs/g/g166.md) [sōtēria](../../strongs/g/g4991.md) unto all them that [hypakouō](../../strongs/g/g5219.md) him;

<a name="hebrews_5_10"></a>Hebrews 5:10

[prosagoreuō](../../strongs/g/g4316.md) of [theos](../../strongs/g/g2316.md) an [archiereus](../../strongs/g/g749.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md).

<a name="hebrews_5_11"></a>Hebrews 5:11

Of whom we have [polys](../../strongs/g/g4183.md) to [logos](../../strongs/g/g3056.md), and [dysermēneutos](../../strongs/g/g1421.md) to be [legō](../../strongs/g/g3004.md), seeing ye are [nōthros](../../strongs/g/g3576.md) of [akoē](../../strongs/g/g189.md).

<a name="hebrews_5_12"></a>Hebrews 5:12

For when for the [chronos](../../strongs/g/g5550.md) ye [opheilō](../../strongs/g/g3784.md) to be [didaskalos](../../strongs/g/g1320.md), ye have [chreia](../../strongs/g/g5532.md) that one [didaskō](../../strongs/g/g1321.md) you again which be the [archē](../../strongs/g/g746.md) [stoicheion](../../strongs/g/g4747.md) of the [logion](../../strongs/g/g3051.md) of [theos](../../strongs/g/g2316.md); and are become such as have [chreia](../../strongs/g/g5532.md) of [gala](../../strongs/g/g1051.md), and not of [stereos](../../strongs/g/g4731.md) [trophē](../../strongs/g/g5160.md).

<a name="hebrews_5_13"></a>Hebrews 5:13

For every one that [metechō](../../strongs/g/g3348.md) [gala](../../strongs/g/g1051.md) [apeiros](../../strongs/g/g552.md) in the [logos](../../strongs/g/g3056.md) of [dikaiosynē](../../strongs/g/g1343.md): for he is a [nēpios](../../strongs/g/g3516.md).

<a name="hebrews_5_14"></a>Hebrews 5:14

But [stereos](../../strongs/g/g4731.md) [trophē](../../strongs/g/g5160.md) belongeth to them that are of [teleios](../../strongs/g/g5046.md), those who by reason of [hexis](../../strongs/g/g1838.md) have their [aisthētērion](../../strongs/g/g145.md) [gymnazō](../../strongs/g/g1128.md) to [diakrisis](../../strongs/g/g1253.md) both [kalos](../../strongs/g/g2570.md) and [kakos](../../strongs/g/g2556.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 4](hebrews_4.md) - [Hebrews 6](hebrews_6.md)