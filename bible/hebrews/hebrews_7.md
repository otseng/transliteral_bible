# [Hebrews 7](https://www.blueletterbible.org/kjv/heb/7/1/s_1140001)

<a name="hebrews_7_1"></a>Hebrews 7:1

For this [Melchisedek](../../strongs/g/g3198.md), [basileus](../../strongs/g/g935.md) of [salēm](../../strongs/g/g4532.md), [hiereus](../../strongs/g/g2409.md) of the [hypsistos](../../strongs/g/g5310.md) [theos](../../strongs/g/g2316.md), who [synantaō](../../strongs/g/g4876.md) [Abraam](../../strongs/g/g11.md) [hypostrephō](../../strongs/g/g5290.md) from the [kopē](../../strongs/g/g2871.md) of the [basileus](../../strongs/g/g935.md), and [eulogeō](../../strongs/g/g2127.md) him;

<a name="hebrews_7_2"></a>Hebrews 7:2

To whom also [Abraam](../../strongs/g/g11.md) [merizō](../../strongs/g/g3307.md) a [dekatē](../../strongs/g/g1181.md) of all; first being by [hermēneuō](../../strongs/g/g2059.md) [basileus](../../strongs/g/g935.md) of [dikaiosynē](../../strongs/g/g1343.md), and after that also [basileus](../../strongs/g/g935.md) of [salēm](../../strongs/g/g4532.md), which is, [basileus](../../strongs/g/g935.md) of [eirēnē](../../strongs/g/g1515.md);

<a name="hebrews_7_3"></a>Hebrews 7:3

[apatōr](../../strongs/g/g540.md), [amētōr](../../strongs/g/g282.md), [agenealogētos](../../strongs/g/g35.md), having neither [archē](../../strongs/g/g746.md) of [hēmera](../../strongs/g/g2250.md), nor [telos](../../strongs/g/g5056.md) of [zōē](../../strongs/g/g2222.md); but [aphomoioō](../../strongs/g/g871.md) unto the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md); [menō](../../strongs/g/g3306.md) a [hiereus](../../strongs/g/g2409.md) [eis](../../strongs/g/g1519.md) [diēnekēs](../../strongs/g/g1336.md).

<a name="hebrews_7_4"></a>Hebrews 7:4

Now [theōreō](../../strongs/g/g2334.md) [pēlikos](../../strongs/g/g4080.md) [houtos](../../strongs/g/g3778.md), unto whom even the [patriarchēs](../../strongs/g/g3966.md) [Abraam](../../strongs/g/g11.md) [didōmi](../../strongs/g/g1325.md) the [dekatē](../../strongs/g/g1181.md) of the [akrothinion](../../strongs/g/g205.md).

<a name="hebrews_7_5"></a>Hebrews 7:5

And [men](../../strongs/g/g3303.md) they that are of the [huios](../../strongs/g/g5207.md) of [Leui](../../strongs/g/g3017.md), who [lambanō](../../strongs/g/g2983.md) the [hierateia](../../strongs/g/g2405.md), have an [entolē](../../strongs/g/g1785.md) to take [apodekatoō](../../strongs/g/g586.md) of the [laos](../../strongs/g/g2992.md) according to the [nomos](../../strongs/g/g3551.md), that is, of their [adelphos](../../strongs/g/g80.md), though they [exerchomai](../../strongs/g/g1831.md) out of the [osphys](../../strongs/g/g3751.md) of [Abraam](../../strongs/g/g11.md):

<a name="hebrews_7_6"></a>Hebrews 7:6

But he not [genealogeō](../../strongs/g/g1075.md) from them [dekatoō](../../strongs/g/g1183.md) of [Abraam](../../strongs/g/g11.md), and [eulogeō](../../strongs/g/g2127.md) him that had the [epaggelia](../../strongs/g/g1860.md).

<a name="hebrews_7_7"></a>Hebrews 7:7

And without all [antilogia](../../strongs/g/g485.md) the [elassōn](../../strongs/g/g1640.md) is [eulogeō](../../strongs/g/g2127.md) of the [kreittōn](../../strongs/g/g2909.md).

<a name="hebrews_7_8"></a>Hebrews 7:8

And here [anthrōpos](../../strongs/g/g444.md) that [apothnēskō](../../strongs/g/g599.md) [lambanō](../../strongs/g/g2983.md) [dekatē](../../strongs/g/g1181.md); but there he, of whom it is [martyreō](../../strongs/g/g3140.md) that he [zaō](../../strongs/g/g2198.md).

<a name="hebrews_7_9"></a>Hebrews 7:9

And as I may so [epos](../../strongs/g/g2031.md) [eipon](../../strongs/g/g2036.md), [Leui](../../strongs/g/g3017.md) also, who [lambanō](../../strongs/g/g2983.md) [dekatē](../../strongs/g/g1181.md), [dekatoō](../../strongs/g/g1183.md) in [Abraam](../../strongs/g/g11.md).

<a name="hebrews_7_10"></a>Hebrews 7:10

For he was yet in the [osphys](../../strongs/g/g3751.md) of his [patēr](../../strongs/g/g3962.md), when [Melchisedek](../../strongs/g/g3198.md) [synantaō](../../strongs/g/g4876.md) him.

<a name="hebrews_7_11"></a>Hebrews 7:11

If therefore [teleiōsis](../../strongs/g/g5050.md) were by the [leuitikos](../../strongs/g/g3020.md) [hierōsynē](../../strongs/g/g2420.md), (for under it the [laos](../../strongs/g/g2992.md) [nomotheteō](../../strongs/g/g3549.md),) what further [chreia](../../strongs/g/g5532.md) that another [hiereus](../../strongs/g/g2409.md) should [anistēmi](../../strongs/g/g450.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md), and not be [legō](../../strongs/g/g3004.md) after the [taxis](../../strongs/g/g5010.md) of [Aarōn](../../strongs/g/g2.md)?

<a name="hebrews_7_12"></a>Hebrews 7:12

For the [hierōsynē](../../strongs/g/g2420.md) being [metatithēmi](../../strongs/g/g3346.md), there is made of [anagkē](../../strongs/g/g318.md) a [metathesis](../../strongs/g/g3331.md) also of the [nomos](../../strongs/g/g3551.md).

<a name="hebrews_7_13"></a>Hebrews 7:13

For he of whom these things are [legō](../../strongs/g/g3004.md) [metechō](../../strongs/g/g3348.md) to another [phylē](../../strongs/g/g5443.md), of which [oudeis](../../strongs/g/g3762.md) [prosechō](../../strongs/g/g4337.md) at the [thysiastērion](../../strongs/g/g2379.md).

<a name="hebrews_7_14"></a>Hebrews 7:14

For [prodēlos](../../strongs/g/g4271.md) that our [kyrios](../../strongs/g/g2962.md) [anatellō](../../strongs/g/g393.md) out of [Ioudas](../../strongs/g/g2455.md); of which [phylē](../../strongs/g/g5443.md) [Mōÿsēs](../../strongs/g/g3475.md) [laleō](../../strongs/g/g2980.md) [oudeis](../../strongs/g/g3762.md) concerning [hierōsynē](../../strongs/g/g2420.md).

<a name="hebrews_7_15"></a>Hebrews 7:15

And it is yet [perissoteron](../../strongs/g/g4054.md) [katadēlos](../../strongs/g/g2612.md): for that after the [homoiotēs](../../strongs/g/g3665.md) of [Melchisedek](../../strongs/g/g3198.md) there [anistēmi](../../strongs/g/g450.md) another [hiereus](../../strongs/g/g2409.md),

<a name="hebrews_7_16"></a>Hebrews 7:16

Who is [ginomai](../../strongs/g/g1096.md), not after the [nomos](../../strongs/g/g3551.md) of a [sarkikos](../../strongs/g/g4559.md) [entolē](../../strongs/g/g1785.md), but after the [dynamis](../../strongs/g/g1411.md) of an [akatalytos](../../strongs/g/g179.md) [zōē](../../strongs/g/g2222.md).

<a name="hebrews_7_17"></a>Hebrews 7:17

For he [martyreō](../../strongs/g/g3140.md), Thou a [hiereus](../../strongs/g/g2409.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md).

<a name="hebrews_7_18"></a>Hebrews 7:18

For there is [men](../../strongs/g/g3303.md) an [athetēsis](../../strongs/g/g115.md) of the [entolē](../../strongs/g/g1785.md) [proagō](../../strongs/g/g4254.md) for the [asthenēs](../../strongs/g/g772.md) and [anōphelēs](../../strongs/g/g512.md) thereof.

<a name="hebrews_7_19"></a>Hebrews 7:19

For the [nomos](../../strongs/g/g3551.md) made nothing [teleioō](../../strongs/g/g5048.md), but the [epeisagōgē](../../strongs/g/g1898.md) of a [kreittōn](../../strongs/g/g2909.md) [elpis](../../strongs/g/g1680.md) did; by the which we [eggizō](../../strongs/g/g1448.md) unto [theos](../../strongs/g/g2316.md).

<a name="hebrews_7_20"></a>Hebrews 7:20

And inasmuch as not without an [horkōmosia](../../strongs/g/g3728.md):

<a name="hebrews_7_21"></a>Hebrews 7:21

(For those [hiereus](../../strongs/g/g2409.md) were [ginomai](../../strongs/g/g1096.md) without an [horkōmosia](../../strongs/g/g3728.md); but this with an [horkōmosia](../../strongs/g/g3728.md) by him that [legō](../../strongs/g/g3004.md) unto him, The [kyrios](../../strongs/g/g2962.md) [omnyō](../../strongs/g/g3660.md) and will not [metamelomai](../../strongs/g/g3338.md), Thou art a [hiereus](../../strongs/g/g2409.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md):)

<a name="hebrews_7_22"></a>Hebrews 7:22

By so [tosoutos](../../strongs/g/g5118.md) was [Iēsous](../../strongs/g/g2424.md) [ginomai](../../strongs/g/g1096.md) an [engyos](../../strongs/g/g1450.md) of a [kreittōn](../../strongs/g/g2909.md) [diathēkē](../../strongs/g/g1242.md).

<a name="hebrews_7_23"></a>Hebrews 7:23

And they [men](../../strongs/g/g3303.md) were many [hiereus](../../strongs/g/g2409.md), because they were not [kōlyō](../../strongs/g/g2967.md) to [paramenō](../../strongs/g/g3887.md) by reason of [thanatos](../../strongs/g/g2288.md):

<a name="hebrews_7_24"></a>Hebrews 7:24

But this, because he [menō](../../strongs/g/g3306.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md), hath an [aparabatos](../../strongs/g/g531.md) [hierōsynē](../../strongs/g/g2420.md).

<a name="hebrews_7_25"></a>Hebrews 7:25

Wherefore he is able also to [sōzō](../../strongs/g/g4982.md) them to the [pantelēs](../../strongs/g/g3838.md) that [proserchomai](../../strongs/g/g4334.md) unto [theos](../../strongs/g/g2316.md) by him, seeing he [pantote](../../strongs/g/g3842.md) [zaō](../../strongs/g/g2198.md) to [entygchanō](../../strongs/g/g1793.md) for them.

<a name="hebrews_7_26"></a>Hebrews 7:26

For such an [archiereus](../../strongs/g/g749.md) became us, [hosios](../../strongs/g/g3741.md), [akakos](../../strongs/g/g172.md), [amiantos](../../strongs/g/g283.md), [chōrizō](../../strongs/g/g5563.md) from [hamartōlos](../../strongs/g/g268.md), and [ginomai](../../strongs/g/g1096.md) [hypsēlos](../../strongs/g/g5308.md) the [ouranos](../../strongs/g/g3772.md);

<a name="hebrews_7_27"></a>Hebrews 7:27

Who [anagkē](../../strongs/g/g318.md) not [hēmera](../../strongs/g/g2250.md), as those [archiereus](../../strongs/g/g749.md), to [anapherō](../../strongs/g/g399.md) [thysia](../../strongs/g/g2378.md), first for his own [hamartia](../../strongs/g/g266.md), and then for the [laos](../../strongs/g/g2992.md): for this he [poieō](../../strongs/g/g4160.md) [ephapax](../../strongs/g/g2178.md), when he [anapherō](../../strongs/g/g399.md) himself.

<a name="hebrews_7_28"></a>Hebrews 7:28

For the [nomos](../../strongs/g/g3551.md) [kathistēmi](../../strongs/g/g2525.md) [anthrōpos](../../strongs/g/g444.md) [archiereus](../../strongs/g/g749.md) which have [astheneia](../../strongs/g/g769.md); but the [logos](../../strongs/g/g3056.md) of the [horkōmosia](../../strongs/g/g3728.md), which was since the [nomos](../../strongs/g/g3551.md), maketh the [huios](../../strongs/g/g5207.md), who is [teleioō](../../strongs/g/g5048.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 6](hebrews_6.md) - [Hebrews 8](hebrews_8.md)