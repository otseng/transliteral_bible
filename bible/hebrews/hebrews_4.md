# [Hebrews 4](https://www.blueletterbible.org/kjv/heb/4)

<a name="hebrews_4_1"></a>Hebrews 4:1

Let us therefore [phobeō](../../strongs/g/g5399.md), lest, an [epaggelia](../../strongs/g/g1860.md) being [kataleipō](../../strongs/g/g2641.md) us of [eiserchomai](../../strongs/g/g1525.md) into his [katapausis](../../strongs/g/g2663.md), any of you should [dokeō](../../strongs/g/g1380.md) to [hystereō](../../strongs/g/g5302.md) of it.

<a name="hebrews_4_2"></a>Hebrews 4:2

For unto us was [euaggelizō](../../strongs/g/g2097.md), as well as unto them: but the [logos](../../strongs/g/g3056.md) [akoē](../../strongs/g/g189.md) did not [ōpheleō](../../strongs/g/g5623.md) them, not being [sygkerannymi](../../strongs/g/g4786.md) with [pistis](../../strongs/g/g4102.md) in them that [akouō](../../strongs/g/g191.md).

<a name="hebrews_4_3"></a>Hebrews 4:3

For we which have [pisteuō](../../strongs/g/g4100.md) do [eiserchomai](../../strongs/g/g1525.md) into [katapausis](../../strongs/g/g2663.md), as he [eipon](../../strongs/g/g2046.md), As I have [omnyō](../../strongs/g/g3660.md) in my [orgē](../../strongs/g/g3709.md), if they shall [eiserchomai](../../strongs/g/g1525.md) into my [katapausis](../../strongs/g/g2663.md): although the [ergon](../../strongs/g/g2041.md) were [ginomai](../../strongs/g/g1096.md) from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="hebrews_4_4"></a>Hebrews 4:4

For he [eipon](../../strongs/g/g2046.md) in a certain place of the seventh on this wise, And [theos](../../strongs/g/g2316.md) did [katapauō](../../strongs/g/g2664.md) the seventh [hēmera](../../strongs/g/g2250.md) from all his [ergon](../../strongs/g/g2041.md).

<a name="hebrews_4_5"></a>Hebrews 4:5

And in this again, If they shall [eiserchomai](../../strongs/g/g1525.md) into my [katapausis](../../strongs/g/g2663.md).

<a name="hebrews_4_6"></a>Hebrews 4:6

Seeing therefore it [apoleipō](../../strongs/g/g620.md) that some must [eiserchomai](../../strongs/g/g1525.md) therein, and they to whom it was first [euaggelizō](../../strongs/g/g2097.md) [eiserchomai](../../strongs/g/g1525.md) not in because of [apeitheia](../../strongs/g/g543.md):

<a name="hebrews_4_7"></a>Hebrews 4:7

Again, he [horizō](../../strongs/g/g3724.md) a certain [hēmera](../../strongs/g/g2250.md), [legō](../../strongs/g/g3004.md) in [Dabid](../../strongs/g/g1138.md), [sēmeron](../../strongs/g/g4594.md), after [tosoutos](../../strongs/g/g5118.md) a [chronos](../../strongs/g/g5550.md); as it is [eipon](../../strongs/g/g2046.md), [sēmeron](../../strongs/g/g4594.md) if ye will [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md), [sklērynō](../../strongs/g/g4645.md) not your [kardia](../../strongs/g/g2588.md).

<a name="hebrews_4_8"></a>Hebrews 4:8

For if [Iēsous](../../strongs/g/g2424.md) had [katapauō](../../strongs/g/g2664.md) them, then would he not afterward have [laleō](../../strongs/g/g2980.md) of another [hēmera](../../strongs/g/g2250.md).

<a name="hebrews_4_9"></a>Hebrews 4:9

There [apoleipō](../../strongs/g/g620.md) therefore a [sabbatismos](../../strongs/g/g4520.md) to the [laos](../../strongs/g/g2992.md) of [theos](../../strongs/g/g2316.md).

<a name="hebrews_4_10"></a>Hebrews 4:10

For he that is [eiserchomai](../../strongs/g/g1525.md) into his [katapausis](../../strongs/g/g2663.md), he also hath [katapauō](../../strongs/g/g2664.md) from his own [ergon](../../strongs/g/g2041.md), as [theos](../../strongs/g/g2316.md) from his.

<a name="hebrews_4_11"></a>Hebrews 4:11

Let us [spoudazō](../../strongs/g/g4704.md) therefore to [eiserchomai](../../strongs/g/g1525.md) into that [katapausis](../../strongs/g/g2663.md), lest any man [piptō](../../strongs/g/g4098.md) after the same [hypodeigma](../../strongs/g/g5262.md) of [apeitheia](../../strongs/g/g543.md).

<a name="hebrews_4_12"></a>Hebrews 4:12

For the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) is [zaō](../../strongs/g/g2198.md), and [energēs](../../strongs/g/g1756.md), and [tomos](../../strongs/g/g5114.md) than any [distomos](../../strongs/g/g1366.md) [machaira](../../strongs/g/g3162.md), [diïkneomai](../../strongs/g/g1338.md) even to the [merismos](../../strongs/g/g3311.md) of [psychē](../../strongs/g/g5590.md) and [pneuma](../../strongs/g/g4151.md), and of the [harmos](../../strongs/g/g719.md) and [myelos](../../strongs/g/g3452.md), and is a [kritikos](../../strongs/g/g2924.md) of the [enthymēsis](../../strongs/g/g1761.md) and [ennoia](../../strongs/g/g1771.md) of the [kardia](../../strongs/g/g2588.md).

<a name="hebrews_4_13"></a>Hebrews 4:13

Neither is there any [ktisis](../../strongs/g/g2937.md) that is not [aphanēs](../../strongs/g/g852.md) in his [enōpion](../../strongs/g/g1799.md): but all things are [gymnos](../../strongs/g/g1131.md) and [trachēlizō](../../strongs/g/g5136.md) unto the [ophthalmos](../../strongs/g/g3788.md) of him with whom we have [logos](../../strongs/g/g3056.md).

<a name="hebrews_4_14"></a>Hebrews 4:14

Seeing then that we have a [megas](../../strongs/g/g3173.md) [archiereus](../../strongs/g/g749.md), that is [dierchomai](../../strongs/g/g1330.md) the [ouranos](../../strongs/g/g3772.md), [Iēsous](../../strongs/g/g2424.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), let us [krateō](../../strongs/g/g2902.md) our [homologia](../../strongs/g/g3671.md).

<a name="hebrews_4_15"></a>Hebrews 4:15

For we have not an [archiereus](../../strongs/g/g749.md) which cannot be [sympatheō](../../strongs/g/g4834.md) of our [astheneia](../../strongs/g/g769.md); but was in all points [peirazō](../../strongs/g/g3985.md) [peiraō](../../strongs/g/g3987.md) [homoiotēs](../../strongs/g/g3665.md) we are, yet without [hamartia](../../strongs/g/g266.md).

<a name="hebrews_4_16"></a>Hebrews 4:16

Let us therefore [proserchomai](../../strongs/g/g4334.md) [parrēsia](../../strongs/g/g3954.md) unto the [thronos](../../strongs/g/g2362.md) of [charis](../../strongs/g/g5485.md), that we may [lambanō](../../strongs/g/g2983.md) [eleos](../../strongs/g/g1656.md), and [heuriskō](../../strongs/g/g2147.md) [charis](../../strongs/g/g5485.md) to [boētheia](../../strongs/g/g996.md) in [eukairos](../../strongs/g/g2121.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 3](hebrews_3.md) - [Hebrews 5](hebrews_5.md)