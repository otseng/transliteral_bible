# [Hebrews 3](https://www.blueletterbible.org/kjv/heb/3/1/s_1136001)

<a name="hebrews_3_1"></a>Hebrews 3:1

Wherefore, [hagios](../../strongs/g/g40.md) [adelphos](../../strongs/g/g80.md), [metochos](../../strongs/g/g3353.md) of the [epouranios](../../strongs/g/g2032.md) [klēsis](../../strongs/g/g2821.md), [katanoeō](../../strongs/g/g2657.md) the [apostolos](../../strongs/g/g652.md) and [archiereus](../../strongs/g/g749.md) of our [homologia](../../strongs/g/g3671.md), [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md);

<a name="hebrews_3_2"></a>Hebrews 3:2

Who was [pistos](../../strongs/g/g4103.md) to him that [poieō](../../strongs/g/g4160.md) him, as also [Mōÿsēs](../../strongs/g/g3475.md) in all his [oikos](../../strongs/g/g3624.md).

<a name="hebrews_3_3"></a>Hebrews 3:3

For this was [axioō](../../strongs/g/g515.md) of more [doxa](../../strongs/g/g1391.md) than [Mōÿsēs](../../strongs/g/g3475.md), inasmuch as he who hath [kataskeuazō](../../strongs/g/g2680.md) the [oikos](../../strongs/g/g3624.md) hath more [timē](../../strongs/g/g5092.md) than [autos](../../strongs/g/g846.md).

<a name="hebrews_3_4"></a>Hebrews 3:4

For every [oikos](../../strongs/g/g3624.md) is [kataskeuazō](../../strongs/g/g2680.md) by some; but he that [kataskeuazō](../../strongs/g/g2680.md) all things [theos](../../strongs/g/g2316.md).

<a name="hebrews_3_5"></a>Hebrews 3:5

And [Mōÿsēs](../../strongs/g/g3475.md) [men](../../strongs/g/g3303.md) was [pistos](../../strongs/g/g4103.md) in all his [oikos](../../strongs/g/g3624.md), as a [therapōn](../../strongs/g/g2324.md), for a [martyrion](../../strongs/g/g3142.md) of those things which were to be [laleō](../../strongs/g/g2980.md) after;

<a name="hebrews_3_6"></a>Hebrews 3:6

But [Christos](../../strongs/g/g5547.md) as a [huios](../../strongs/g/g5207.md) over his own [oikos](../../strongs/g/g3624.md); whose [oikos](../../strongs/g/g3624.md) are we, if we [katechō](../../strongs/g/g2722.md) the [parrēsia](../../strongs/g/g3954.md) and the [kauchēma](../../strongs/g/g2745.md) of the [elpis](../../strongs/g/g1680.md) [bebaios](../../strongs/g/g949.md) unto the [telos](../../strongs/g/g5056.md).

<a name="hebrews_3_7"></a>Hebrews 3:7

Wherefore (as the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md), [sēmeron](../../strongs/g/g4594.md) if ye will [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md),

<a name="hebrews_3_8"></a>Hebrews 3:8

[sklērynō](../../strongs/g/g4645.md) not your [kardia](../../strongs/g/g2588.md), as in the [parapikrasmos](../../strongs/g/g3894.md), in the day of [peirasmos](../../strongs/g/g3986.md) in the [erēmos](../../strongs/g/g2048.md):

<a name="hebrews_3_9"></a>Hebrews 3:9

When your [patēr](../../strongs/g/g3962.md) [peirazō](../../strongs/g/g3985.md) me, [dokimazō](../../strongs/g/g1381.md) me, and [eidō](../../strongs/g/g1492.md) my [ergon](../../strongs/g/g2041.md) forty [etos](../../strongs/g/g2094.md).

<a name="hebrews_3_10"></a>Hebrews 3:10

Wherefore I was [prosochthizō](../../strongs/g/g4360.md) with that [genea](../../strongs/g/g1074.md), and [eipon](../../strongs/g/g2036.md), They do alway [planaō](../../strongs/g/g4105.md) in [kardia](../../strongs/g/g2588.md); and they have not [ginōskō](../../strongs/g/g1097.md) my [hodos](../../strongs/g/g3598.md).

<a name="hebrews_3_11"></a>Hebrews 3:11

So I [omnyō](../../strongs/g/g3660.md) in my [orgē](../../strongs/g/g3709.md), They shall not [eiserchomai](../../strongs/g/g1525.md) into my [katapausis](../../strongs/g/g2663.md).)

<a name="hebrews_3_12"></a>Hebrews 3:12

[blepō](../../strongs/g/g991.md), [adelphos](../../strongs/g/g80.md), lest there be in any of you a [ponēros](../../strongs/g/g4190.md) [kardia](../../strongs/g/g2588.md) of [apistia](../../strongs/g/g570.md), in [aphistēmi](../../strongs/g/g868.md) from the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md).

<a name="hebrews_3_13"></a>Hebrews 3:13

But [parakaleō](../../strongs/g/g3870.md) [heautou](../../strongs/g/g1438.md) [kata](../../strongs/g/g2596.md) [hekastos](../../strongs/g/g1538.md) [hēmera](../../strongs/g/g2250.md), while it is [kaleō](../../strongs/g/g2564.md) [sēmeron](../../strongs/g/g4594.md); lest any of you be [sklērynō](../../strongs/g/g4645.md) through the [apatē](../../strongs/g/g539.md) of [hamartia](../../strongs/g/g266.md).

<a name="hebrews_3_14"></a>Hebrews 3:14

For we are [ginomai](../../strongs/g/g1096.md) [metochos](../../strongs/g/g3353.md) of [Christos](../../strongs/g/g5547.md), if we [katechō](../../strongs/g/g2722.md) the [archē](../../strongs/g/g746.md) of our [hypostasis](../../strongs/g/g5287.md) [bebaios](../../strongs/g/g949.md) unto the [telos](../../strongs/g/g5056.md);

<a name="hebrews_3_15"></a>Hebrews 3:15

While it is [legō](../../strongs/g/g3004.md), [sēmeron](../../strongs/g/g4594.md) if ye will [akouō](../../strongs/g/g191.md) his [phōnē](../../strongs/g/g5456.md), [sklērynō](../../strongs/g/g4645.md) not your [kardia](../../strongs/g/g2588.md), as in the [parapikrasmos](../../strongs/g/g3894.md).

<a name="hebrews_3_16"></a>Hebrews 3:16

For some, when they had [akouō](../../strongs/g/g191.md), did [parapikrainō](../../strongs/g/g3893.md): howbeit not all that [exerchomai](../../strongs/g/g1831.md) of [Aigyptos](../../strongs/g/g125.md) by [Mōÿsēs](../../strongs/g/g3475.md).

<a name="hebrews_3_17"></a>Hebrews 3:17

But with whom was he [prosochthizō](../../strongs/g/g4360.md) forty [etos](../../strongs/g/g2094.md)? was it not with them that had [hamartanō](../../strongs/g/g264.md), whose [kōlon](../../strongs/g/g2966.md) [piptō](../../strongs/g/g4098.md) in the [erēmos](../../strongs/g/g2048.md)?

<a name="hebrews_3_18"></a>Hebrews 3:18

And to whom [omnyō](../../strongs/g/g3660.md) he that they should not [eiserchomai](../../strongs/g/g1525.md) into his [katapausis](../../strongs/g/g2663.md), but to them that [apeitheō](../../strongs/g/g544.md)?

<a name="hebrews_3_19"></a>Hebrews 3:19

So we [blepō](../../strongs/g/g991.md) that they could not [eiserchomai](../../strongs/g/g1525.md) because of [apistia](../../strongs/g/g570.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 2](hebrews_2.md) - [Hebrews 4](hebrews_4.md)