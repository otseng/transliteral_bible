# [Hebrews 1](https://www.blueletterbible.org/kjv/heb/1/1/s_1134001)

<a name="hebrews_1_1"></a>Hebrews 1:1

[theos](../../strongs/g/g2316.md), who at [polymerōs](../../strongs/g/g4181.md) and in [polytropōs](../../strongs/g/g4187.md) [laleō](../../strongs/g/g2980.md) in [palai](../../strongs/g/g3819.md) unto the [patēr](../../strongs/g/g3962.md) by the [prophētēs](../../strongs/g/g4396.md),

<a name="hebrews_1_2"></a>Hebrews 1:2

Hath in these [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md) [laleō](../../strongs/g/g2980.md) unto us by [huios](../../strongs/g/g5207.md), whom he hath [tithēmi](../../strongs/g/g5087.md) [klēronomos](../../strongs/g/g2818.md) of all things, by whom also he [poieō](../../strongs/g/g4160.md) the [aiōn](../../strongs/g/g165.md);

<a name="hebrews_1_3"></a>Hebrews 1:3

Who being the [apaugasma](../../strongs/g/g541.md) of [doxa](../../strongs/g/g1391.md), and the [charaktēr](../../strongs/g/g5481.md) of his [hypostasis](../../strongs/g/g5287.md), and [pherō](../../strongs/g/g5342.md) all things by the [rhēma](../../strongs/g/g4487.md) of his [dynamis](../../strongs/g/g1411.md), when he [poieō](../../strongs/g/g4160.md) by himself [katharismos](../../strongs/g/g2512.md) our [hamartia](../../strongs/g/g266.md), [kathizō](../../strongs/g/g2523.md) on the [dexios](../../strongs/g/g1188.md) of the [megalōsynē](../../strongs/g/g3172.md) on [hypsēlos](../../strongs/g/g5308.md):

<a name="hebrews_1_4"></a>Hebrews 1:4

[ginomai](../../strongs/g/g1096.md) [tosoutos](../../strongs/g/g5118.md) [kreittōn](../../strongs/g/g2909.md) than the [aggelos](../../strongs/g/g32.md), as he hath by [klēronomeō](../../strongs/g/g2816.md) a [diaphoros](../../strongs/g/g1313.md) [onoma](../../strongs/g/g3686.md) than they.

<a name="hebrews_1_5"></a>Hebrews 1:5

For unto which of the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) he at any time, Thou art my [huios](../../strongs/g/g5207.md), [sēmeron](../../strongs/g/g4594.md) have I [gennaō](../../strongs/g/g1080.md) thee? And again, I will be to him a [patēr](../../strongs/g/g3962.md), and he shall be to me a [huios](../../strongs/g/g5207.md)?

<a name="hebrews_1_6"></a>Hebrews 1:6

And again, when he [eisagō](../../strongs/g/g1521.md) the [prōtotokos](../../strongs/g/g4416.md) into the [oikoumenē](../../strongs/g/g3625.md), he [legō](../../strongs/g/g3004.md), And let all the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) [proskyneō](../../strongs/g/g4352.md) him.

<a name="hebrews_1_7"></a>Hebrews 1:7

And of the [aggelos](../../strongs/g/g32.md) he [legō](../../strongs/g/g3004.md), Who [poieō](../../strongs/g/g4160.md) his [aggelos](../../strongs/g/g32.md) [pneuma](../../strongs/g/g4151.md), and his [leitourgos](../../strongs/g/g3011.md) a [phlox](../../strongs/g/g5395.md) of [pyr](../../strongs/g/g4442.md).

<a name="hebrews_1_8"></a>Hebrews 1:8

But unto the [huios](../../strongs/g/g5207.md), Thy [thronos](../../strongs/g/g2362.md), [theos](../../strongs/g/g2316.md), [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md): a [rhabdos](../../strongs/g/g4464.md) of [euthytēs](../../strongs/g/g2118.md) the [rhabdos](../../strongs/g/g4464.md) of thy [basileia](../../strongs/g/g932.md).

<a name="hebrews_1_9"></a>Hebrews 1:9

Thou hast [agapaō](../../strongs/g/g25.md) [dikaiosynē](../../strongs/g/g1343.md), and [miseō](../../strongs/g/g3404.md) [anomia](../../strongs/g/g458.md); therefore [theos](../../strongs/g/g2316.md), thy [theos](../../strongs/g/g2316.md), hath [chriō](../../strongs/g/g5548.md) thee with the [elaion](../../strongs/g/g1637.md) of [agalliasis](../../strongs/g/g20.md) above thy [metochos](../../strongs/g/g3353.md).

<a name="hebrews_1_10"></a>Hebrews 1:10

And, Thou, [kyrios](../../strongs/g/g2962.md), in the [archē](../../strongs/g/g746.md) hast [themelioō](../../strongs/g/g2311.md) of [gē](../../strongs/g/g1093.md); and the [ouranos](../../strongs/g/g3772.md) are the [ergon](../../strongs/g/g2041.md) of thine [cheir](../../strongs/g/g5495.md):

<a name="hebrews_1_11"></a>Hebrews 1:11

They shall [apollymi](../../strongs/g/g622.md); but thou [diamenō](../../strongs/g/g1265.md); and they all shall [palaioō](../../strongs/g/g3822.md) as doth a [himation](../../strongs/g/g2440.md);

<a name="hebrews_1_12"></a>Hebrews 1:12

And as a [peribolaion](../../strongs/g/g4018.md) shalt thou [helissō](../../strongs/g/g1667.md), and they shall be [allassō](../../strongs/g/g236.md): but thou art the same, and thy [etos](../../strongs/g/g2094.md) shall not [ekleipō](../../strongs/g/g1587.md).

<a name="hebrews_1_13"></a>Hebrews 1:13

But to which of the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2046.md) he at any time, [kathēmai](../../strongs/g/g2521.md) on my [dexios](../../strongs/g/g1188.md), until I [tithēmi](../../strongs/g/g5087.md) thine [echthros](../../strongs/g/g2190.md) thy [hypopodion](../../strongs/g/g5286.md) [pous](../../strongs/g/g4228.md)?

<a name="hebrews_1_14"></a>Hebrews 1:14

Are they not all [leitourgikos](../../strongs/g/g3010.md) [pneuma](../../strongs/g/g4151.md), [apostellō](../../strongs/g/g649.md) to [diakonia](../../strongs/g/g1248.md) for them who shall [klēronomeō](../../strongs/g/g2816.md) [sōtēria](../../strongs/g/g4991.md)?

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 2](hebrews_2.md)