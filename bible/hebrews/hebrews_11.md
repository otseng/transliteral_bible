# [Hebrews 11](https://www.blueletterbible.org/kjv/heb/11/1/s_1144001)

<a name="hebrews_11_1"></a>Hebrews 11:1

Now [pistis](../../strongs/g/g4102.md) is the [hypostasis](../../strongs/g/g5287.md) of [elpizō](../../strongs/g/g1679.md) for, the [elegchos](../../strongs/g/g1650.md) of [pragma](../../strongs/g/g4229.md) not [blepō](../../strongs/g/g991.md).

<a name="hebrews_11_2"></a>Hebrews 11:2

For by it the [presbyteros](../../strongs/g/g4245.md) [martyreō](../../strongs/g/g3140.md).

<a name="hebrews_11_3"></a>Hebrews 11:3

Through [pistis](../../strongs/g/g4102.md) we [noeō](../../strongs/g/g3539.md) that the [aiōn](../../strongs/g/g165.md) were [katartizō](../../strongs/g/g2675.md) by the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md), so that things which are [blepō](../../strongs/g/g991.md) were not [ginomai](../../strongs/g/g1096.md) of things which do [phainō](../../strongs/g/g5316.md).

<a name="hebrews_11_4"></a>Hebrews 11:4

By [pistis](../../strongs/g/g4102.md) [Abel](../../strongs/g/g6.md) [prospherō](../../strongs/g/g4374.md) unto [theos](../../strongs/g/g2316.md) a [pleiōn](../../strongs/g/g4119.md) [thysia](../../strongs/g/g2378.md) than [kain](../../strongs/g/g2535.md), by which he [martyreō](../../strongs/g/g3140.md) that he was [dikaios](../../strongs/g/g1342.md), [theos](../../strongs/g/g2316.md) [martyreō](../../strongs/g/g3140.md) of his [dōron](../../strongs/g/g1435.md): and by it he being [apothnēskō](../../strongs/g/g599.md) yet [laleō](../../strongs/g/g2980.md).

<a name="hebrews_11_5"></a>Hebrews 11:5

By [pistis](../../strongs/g/g4102.md) [Henōch](../../strongs/g/g1802.md) was [metatithēmi](../../strongs/g/g3346.md) that he should not [eidō](../../strongs/g/g1492.md) [thanatos](../../strongs/g/g2288.md); and was not [heuriskō](../../strongs/g/g2147.md), because [theos](../../strongs/g/g2316.md) had [metatithēmi](../../strongs/g/g3346.md) him: for before his [metathesis](../../strongs/g/g3331.md) he had this [martyreō](../../strongs/g/g3140.md), that he [euaresteō](../../strongs/g/g2100.md) [theos](../../strongs/g/g2316.md).

<a name="hebrews_11_6"></a>Hebrews 11:6

But without [pistis](../../strongs/g/g4102.md) [adynatos](../../strongs/g/g102.md) to [euaresteō](../../strongs/g/g2100.md): for he that [proserchomai](../../strongs/g/g4334.md) to [theos](../../strongs/g/g2316.md) must [pisteuō](../../strongs/g/g4100.md) that he is, and he is a [misthapodotēs](../../strongs/g/g3406.md) of them that [ekzēteō](../../strongs/g/g1567.md) him.

<a name="hebrews_11_7"></a>Hebrews 11:7

By [pistis](../../strongs/g/g4102.md) [Nōe](../../strongs/g/g3575.md), being [chrēmatizō](../../strongs/g/g5537.md) of things [mēdepō](../../strongs/g/g3369.md) [blepō](../../strongs/g/g991.md), [eulabeomai](../../strongs/g/g2125.md), [kataskeuazō](../../strongs/g/g2680.md) a [kibōtos](../../strongs/g/g2787.md) to the [sōtēria](../../strongs/g/g4991.md) of his [oikos](../../strongs/g/g3624.md); by the which he [katakrinō](../../strongs/g/g2632.md) the [kosmos](../../strongs/g/g2889.md), and became [klēronomos](../../strongs/g/g2818.md) of the [dikaiosynē](../../strongs/g/g1343.md) which is by [pistis](../../strongs/g/g4102.md).

<a name="hebrews_11_8"></a>Hebrews 11:8

By [pistis](../../strongs/g/g4102.md) [Abraam](../../strongs/g/g11.md), when he was [kaleō](../../strongs/g/g2564.md) to [exerchomai](../../strongs/g/g1831.md) into a [topos](../../strongs/g/g5117.md) which he [mellō](../../strongs/g/g3195.md) [lambanō](../../strongs/g/g2983.md) for a [klēronomia](../../strongs/g/g2817.md), [hypakouō](../../strongs/g/g5219.md); and he [exerchomai](../../strongs/g/g1831.md), not [epistamai](../../strongs/g/g1987.md) whither he [erchomai](../../strongs/g/g2064.md).

<a name="hebrews_11_9"></a>Hebrews 11:9

By [pistis](../../strongs/g/g4102.md) he [paroikeō](../../strongs/g/g3939.md) in the [gē](../../strongs/g/g1093.md) of [epaggelia](../../strongs/g/g1860.md), as an [allotrios](../../strongs/g/g245.md), [katoikeō](../../strongs/g/g2730.md) in [skēnē](../../strongs/g/g4633.md) with [Isaak](../../strongs/g/g2464.md) and [Iakōb](../../strongs/g/g2384.md), the [sygklēronomos](../../strongs/g/g4789.md) of the same [epaggelia](../../strongs/g/g1860.md):

<a name="hebrews_11_10"></a>Hebrews 11:10

For he [ekdechomai](../../strongs/g/g1551.md) for a [polis](../../strongs/g/g4172.md) which hath [themelios](../../strongs/g/g2310.md), whose [technitēs](../../strongs/g/g5079.md) and [dēmiourgos](../../strongs/g/g1217.md) [theos](../../strongs/g/g2316.md).

<a name="hebrews_11_11"></a>Hebrews 11:11

Through [pistis](../../strongs/g/g4102.md) also [Sarra](../../strongs/g/g4564.md) herself [lambanō](../../strongs/g/g2983.md) [dynamis](../../strongs/g/g1411.md) to [katabolē](../../strongs/g/g2602.md) [sperma](../../strongs/g/g4690.md), and [tiktō](../../strongs/g/g5088.md) when she was past [kairos](../../strongs/g/g2540.md) [hēlikia](../../strongs/g/g2244.md), because she [hēgeomai](../../strongs/g/g2233.md) him [pistos](../../strongs/g/g4103.md) who had [epaggellomai](../../strongs/g/g1861.md).

<a name="hebrews_11_12"></a>Hebrews 11:12

Therefore [gennaō](../../strongs/g/g1080.md) there even of one, and him as [nekroō](../../strongs/g/g3499.md), as the [astron](../../strongs/g/g798.md) of the [ouranos](../../strongs/g/g3772.md) in [plēthos](../../strongs/g/g4128.md), and as the [ammos](../../strongs/g/g285.md) which is by the [thalassa](../../strongs/g/g2281.md) [cheilos](../../strongs/g/g5491.md) [anarithmētos](../../strongs/g/g382.md).

<a name="hebrews_11_13"></a>Hebrews 11:13

These all [apothnēskō](../../strongs/g/g599.md) in [pistis](../../strongs/g/g4102.md), not having [lambanō](../../strongs/g/g2983.md) the [epaggelia](../../strongs/g/g1860.md), but having [eidō](../../strongs/g/g1492.md) them [porrōthen](../../strongs/g/g4207.md), and were [peithō](../../strongs/g/g3982.md), and [aspazomai](../../strongs/g/g782.md), and [homologeō](../../strongs/g/g3670.md) that they were [xenos](../../strongs/g/g3581.md) and [parepidēmos](../../strongs/g/g3927.md) on the [gē](../../strongs/g/g1093.md).

<a name="hebrews_11_14"></a>Hebrews 11:14

For they that [legō](../../strongs/g/g3004.md) such things [emphanizō](../../strongs/g/g1718.md) that they [epizēteō](../../strongs/g/g1934.md) a [patris](../../strongs/g/g3968.md).

<a name="hebrews_11_15"></a>Hebrews 11:15

And [men](../../strongs/g/g3303.md), if they had been [mnēmoneuō](../../strongs/g/g3421.md) of that from whence they [exerchomai](../../strongs/g/g1831.md), they might have had [kairos](../../strongs/g/g2540.md) to have [anakamptō](../../strongs/g/g344.md).

<a name="hebrews_11_16"></a>Hebrews 11:16

But [nyni](../../strongs/g/g3570.md) they [oregō](../../strongs/g/g3713.md) a [kreittōn](../../strongs/g/g2909.md), that is, an [epouranios](../../strongs/g/g2032.md): wherefore [theos](../../strongs/g/g2316.md) is not [epaischynomai](../../strongs/g/g1870.md) to be [epikaleō](../../strongs/g/g1941.md) their [theos](../../strongs/g/g2316.md): for he hath [hetoimazō](../../strongs/g/g2090.md) for them a [polis](../../strongs/g/g4172.md).

<a name="hebrews_11_17"></a>Hebrews 11:17

By [pistis](../../strongs/g/g4102.md) [Abraam](../../strongs/g/g11.md), when he was [peirazō](../../strongs/g/g3985.md), [prospherō](../../strongs/g/g4374.md) [Isaak](../../strongs/g/g2464.md): and he that had [anadechomai](../../strongs/g/g324.md) the [epaggelia](../../strongs/g/g1860.md) [prospherō](../../strongs/g/g4374.md) his [monogenēs](../../strongs/g/g3439.md),

<a name="hebrews_11_18"></a>Hebrews 11:18

Of whom it was [laleō](../../strongs/g/g2980.md), That in [Isaak](../../strongs/g/g2464.md) shall thy [sperma](../../strongs/g/g4690.md) be [kaleō](../../strongs/g/g2564.md):

<a name="hebrews_11_19"></a>Hebrews 11:19

[logizomai](../../strongs/g/g3049.md) that [theos](../../strongs/g/g2316.md) was [dynatos](../../strongs/g/g1415.md) to [egeirō](../../strongs/g/g1453.md), even from the [nekros](../../strongs/g/g3498.md); from whence also he [komizō](../../strongs/g/g2865.md) him in a [parabolē](../../strongs/g/g3850.md).

<a name="hebrews_11_20"></a>Hebrews 11:20

By [pistis](../../strongs/g/g4102.md) [Isaak](../../strongs/g/g2464.md) [eulogeō](../../strongs/g/g2127.md) [Iakōb](../../strongs/g/g2384.md) and [Ēsau](../../strongs/g/g2269.md) concerning things [mellō](../../strongs/g/g3195.md).

<a name="hebrews_11_21"></a>Hebrews 11:21

By [pistis](../../strongs/g/g4102.md) [Iakōb](../../strongs/g/g2384.md), when he was an [apothnēskō](../../strongs/g/g599.md), [eulogeō](../../strongs/g/g2127.md) both the [huios](../../strongs/g/g5207.md) of [Iōsēph](../../strongs/g/g2501.md); and [proskyneō](../../strongs/g/g4352.md), [epi](../../strongs/g/g1909.md) upon the [akron](../../strongs/g/g206.md) of his [rhabdos](../../strongs/g/g4464.md).

<a name="hebrews_11_22"></a>Hebrews 11:22

By [pistis](../../strongs/g/g4102.md) [Iōsēph](../../strongs/g/g2501.md), when he [teleutaō](../../strongs/g/g5053.md), [mnēmoneuō](../../strongs/g/g3421.md) of the [exodos](../../strongs/g/g1841.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md); and [entellō](../../strongs/g/g1781.md) concerning his [osteon](../../strongs/g/g3747.md).

<a name="hebrews_11_23"></a>Hebrews 11:23

By [pistis](../../strongs/g/g4102.md) [Mōÿsēs](../../strongs/g/g3475.md), when he was [gennaō](../../strongs/g/g1080.md), was [kryptō](../../strongs/g/g2928.md) [trimēnos](../../strongs/g/g5150.md) of his [patēr](../../strongs/g/g3962.md), because they [eidō](../../strongs/g/g1492.md) an [asteios](../../strongs/g/g791.md) [paidion](../../strongs/g/g3813.md); and they were not [phobeō](../../strongs/g/g5399.md) of the [basileus](../../strongs/g/g935.md) [diatagma](../../strongs/g/g1297.md).

<a name="hebrews_11_24"></a>Hebrews 11:24

By [pistis](../../strongs/g/g4102.md) [Mōÿsēs](../../strongs/g/g3475.md), when he was [ginomai](../../strongs/g/g1096.md) to [megas](../../strongs/g/g3173.md), [arneomai](../../strongs/g/g720.md) to be [legō](../../strongs/g/g3004.md) the [huios](../../strongs/g/g5207.md) of [Pharaō](../../strongs/g/g5328.md) [thygatēr](../../strongs/g/g2364.md);

<a name="hebrews_11_25"></a>Hebrews 11:25

[haireō](../../strongs/g/g138.md) rather to [sygkakoucheomai](../../strongs/g/g4778.md) with the [laos](../../strongs/g/g2992.md) of [theos](../../strongs/g/g2316.md), than to [apolausis](../../strongs/g/g619.md) of [hamartia](../../strongs/g/g266.md) [proskairos](../../strongs/g/g4340.md);

<a name="hebrews_11_26"></a>Hebrews 11:26

[hēgeomai](../../strongs/g/g2233.md) the [oneidismos](../../strongs/g/g3680.md) of [Christos](../../strongs/g/g5547.md) [meizōn](../../strongs/g/g3187.md) [ploutos](../../strongs/g/g4149.md) than the [thēsauros](../../strongs/g/g2344.md) in [Aigyptos](../../strongs/g/g125.md): for he had [apoblepō](../../strongs/g/g578.md) unto the [misthapodosia](../../strongs/g/g3405.md).

<a name="hebrews_11_27"></a>Hebrews 11:27

By [pistis](../../strongs/g/g4102.md) he [kataleipō](../../strongs/g/g2641.md) [Aigyptos](../../strongs/g/g125.md), not [phobeō](../../strongs/g/g5399.md) the [thymos](../../strongs/g/g2372.md) of the [basileus](../../strongs/g/g935.md): for he [kartereō](../../strongs/g/g2594.md), as [horaō](../../strongs/g/g3708.md) him who is [aoratos](../../strongs/g/g517.md).

<a name="hebrews_11_28"></a>Hebrews 11:28

Through [pistis](../../strongs/g/g4102.md) he [poieō](../../strongs/g/g4160.md) the [pascha](../../strongs/g/g3957.md), and the [proschysis](../../strongs/g/g4378.md) of [haima](../../strongs/g/g129.md), lest he that [olothreuō](../../strongs/g/g3645.md) the [prōtotokos](../../strongs/g/g4416.md) should [thinganō](../../strongs/g/g2345.md) them.

<a name="hebrews_11_29"></a>Hebrews 11:29

By [pistis](../../strongs/g/g4102.md) they [diabainō](../../strongs/g/g1224.md) the [erythros](../../strongs/g/g2063.md) [thalassa](../../strongs/g/g2281.md) as by [xēros](../../strongs/g/g3584.md): which the [Aigyptios](../../strongs/g/g124.md) [peira](../../strongs/g/g3984.md) to [lambanō](../../strongs/g/g2983.md) were [katapinō](../../strongs/g/g2666.md).

<a name="hebrews_11_30"></a>Hebrews 11:30

By [pistis](../../strongs/g/g4102.md) the [teichos](../../strongs/g/g5038.md) of [Ierichō](../../strongs/g/g2410.md) [piptō](../../strongs/g/g4098.md), after they were [kykloō](../../strongs/g/g2944.md) about seven days.

<a name="hebrews_11_31"></a>Hebrews 11:31

By [pistis](../../strongs/g/g4102.md) the [pornē](../../strongs/g/g4204.md) [rhaab](../../strongs/g/g4460.md) [synapollymi](../../strongs/g/g4881.md) not with [apeitheō](../../strongs/g/g544.md), when she had [dechomai](../../strongs/g/g1209.md) the [kataskopos](../../strongs/g/g2685.md) with [eirēnē](../../strongs/g/g1515.md).

<a name="hebrews_11_32"></a>Hebrews 11:32

And what shall I more [legō](../../strongs/g/g3004.md)? for the [chronos](../../strongs/g/g5550.md) would [epileipō](../../strongs/g/g1952.md) me to [diēgeomai](../../strongs/g/g1334.md) of [gedeōn](../../strongs/g/g1066.md), and [barak](../../strongs/g/g913.md), and [sampsōn](../../strongs/g/g4546.md), and [iephthae](../../strongs/g/g2422.md); [Dabid](../../strongs/g/g1138.md) also, and [Samouēl](../../strongs/g/g4545.md), and of the [prophētēs](../../strongs/g/g4396.md):

<a name="hebrews_11_33"></a>Hebrews 11:33

Who through [pistis](../../strongs/g/g4102.md) [katagōnizomai](../../strongs/g/g2610.md) [basileia](../../strongs/g/g932.md), [ergazomai](../../strongs/g/g2038.md) [dikaiosynē](../../strongs/g/g1343.md), [epitygchanō](../../strongs/g/g2013.md) [epaggelia](../../strongs/g/g1860.md), [phrassō](../../strongs/g/g5420.md) the [stoma](../../strongs/g/g4750.md) of [leōn](../../strongs/g/g3023.md).

<a name="hebrews_11_34"></a>Hebrews 11:34

[sbennymi](../../strongs/g/g4570.md) the [dynamis](../../strongs/g/g1411.md) of [pyr](../../strongs/g/g4442.md), [pheugō](../../strongs/g/g5343.md) the [stoma](../../strongs/g/g4750.md) of the [machaira](../../strongs/g/g3162.md), out of [astheneia](../../strongs/g/g769.md) were [endynamoō](../../strongs/g/g1743.md), waxed [ischyros](../../strongs/g/g2478.md) in [polemos](../../strongs/g/g4171.md), [klinō](../../strongs/g/g2827.md) the [parembolē](../../strongs/g/g3925.md) of [allotrios](../../strongs/g/g245.md).

<a name="hebrews_11_35"></a>Hebrews 11:35

[gynē](../../strongs/g/g1135.md) [lambanō](../../strongs/g/g2983.md) their [nekros](../../strongs/g/g3498.md) [ek](../../strongs/g/g1537.md) [anastasis](../../strongs/g/g386.md): and others were [tympanizō](../../strongs/g/g5178.md), not [prosdechomai](../../strongs/g/g4327.md) [apolytrōsis](../../strongs/g/g629.md); that they might [tygchanō](../../strongs/g/g5177.md) a [kreittōn](../../strongs/g/g2909.md) [anastasis](../../strongs/g/g386.md):

<a name="hebrews_11_36"></a>Hebrews 11:36

And others [lambanō](../../strongs/g/g2983.md) [peira](../../strongs/g/g3984.md) of [empaigmos](../../strongs/g/g1701.md) and [mastix](../../strongs/g/g3148.md), yea, moreover of [desmos](../../strongs/g/g1199.md) and [phylakē](../../strongs/g/g5438.md):

<a name="hebrews_11_37"></a>Hebrews 11:37

They were [lithazō](../../strongs/g/g3034.md), they were [prizō](../../strongs/g/g4249.md), were [peirazō](../../strongs/g/g3985.md), were [apothnēskō](../../strongs/g/g599.md) with the [phonos](../../strongs/g/g5408.md) [machaira](../../strongs/g/g3162.md): they [perierchomai](../../strongs/g/g4022.md) in [mēlōtē](../../strongs/g/g3374.md) and [aigeios](../../strongs/g/g122.md) [derma](../../strongs/g/g1192.md); being [hystereō](../../strongs/g/g5302.md), [thlibō](../../strongs/g/g2346.md), [kakoucheō](../../strongs/g/g2558.md);

<a name="hebrews_11_38"></a>Hebrews 11:38

(Of whom the [kosmos](../../strongs/g/g2889.md) was not [axios](../../strongs/g/g514.md):) they [planaō](../../strongs/g/g4105.md) in [erēmia](../../strongs/g/g2047.md), and [oros](../../strongs/g/g3735.md), and [spēlaion](../../strongs/g/g4693.md) and [opē](../../strongs/g/g3692.md) of the [gē](../../strongs/g/g1093.md).

<a name="hebrews_11_39"></a>Hebrews 11:39

And these all, having [martyreō](../../strongs/g/g3140.md) through [pistis](../../strongs/g/g4102.md), [komizō](../../strongs/g/g2865.md) not the [epaggelia](../../strongs/g/g1860.md):

<a name="hebrews_11_40"></a>Hebrews 11:40

[theos](../../strongs/g/g2316.md) having [problepō](../../strongs/g/g4265.md) some [kreittōn](../../strongs/g/g2909.md) [tis](../../strongs/g/g5100.md) for us, that they without us should not be [teleioō](../../strongs/g/g5048.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 10](hebrews_10..md) - [Hebrews 12](hebrews_12.md)