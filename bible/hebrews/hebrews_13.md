# [Hebrews 13](https://www.blueletterbible.org/kjv/heb/13/1/s_1146001)

<a name="hebrews_13_1"></a>Hebrews 13:1

Let [philadelphia](../../strongs/g/g5360.md) [menō](../../strongs/g/g3306.md).

<a name="hebrews_13_2"></a>Hebrews 13:2

Be not [epilanthanomai](../../strongs/g/g1950.md) to [philoxenia](../../strongs/g/g5381.md): for thereby some have [xenizō](../../strongs/g/g3579.md) [aggelos](../../strongs/g/g32.md) [lanthanō](../../strongs/g/g2990.md).

<a name="hebrews_13_3"></a>Hebrews 13:3

[mimnēskomai](../../strongs/g/g3403.md) [desmios](../../strongs/g/g1198.md), as [syndeō](../../strongs/g/g4887.md); and them which [kakoucheō](../../strongs/g/g2558.md), as being yourselves also in the [sōma](../../strongs/g/g4983.md).

<a name="hebrews_13_4"></a>Hebrews 13:4

[gamos](../../strongs/g/g1062.md) is [timios](../../strongs/g/g5093.md) in all, and the [koitē](../../strongs/g/g2845.md) [amiantos](../../strongs/g/g283.md): but [pornos](../../strongs/g/g4205.md) and [moichos](../../strongs/g/g3432.md) [theos](../../strongs/g/g2316.md) will [krinō](../../strongs/g/g2919.md).

<a name="hebrews_13_5"></a>Hebrews 13:5

Let your [tropos](../../strongs/g/g5158.md) be [aphilargyros](../../strongs/g/g866.md); and be [arkeō](../../strongs/g/g714.md) with such things as ye [pareimi](../../strongs/g/g3918.md): for he hath [eipon](../../strongs/g/g2046.md), I will never [aniēmi](../../strongs/g/g447.md) thee, nor [egkataleipō](../../strongs/g/g1459.md) thee.

<a name="hebrews_13_6"></a>Hebrews 13:6

So that we may [tharreō](../../strongs/g/g2292.md) [legō](../../strongs/g/g3004.md), The [kyrios](../../strongs/g/g2962.md) is my [boēthos](../../strongs/g/g998.md), and I will not [phobeō](../../strongs/g/g5399.md) what [anthrōpos](../../strongs/g/g444.md) shall [poieō](../../strongs/g/g4160.md) unto me.

<a name="hebrews_13_7"></a>Hebrews 13:7

[mnēmoneuō](../../strongs/g/g3421.md) them which [hēgeomai](../../strongs/g/g2233.md) over you, who have [laleō](../../strongs/g/g2980.md) unto you the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md): whose [pistis](../../strongs/g/g4102.md) [mimeomai](../../strongs/g/g3401.md), [anatheōreō](../../strongs/g/g333.md) the [ekbasis](../../strongs/g/g1545.md) of their [anastrophē](../../strongs/g/g391.md).

<a name="hebrews_13_8"></a>Hebrews 13:8

[Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) the same [echthes](../../strongs/g/g5504.md), and [sēmeron](../../strongs/g/g4594.md), and for [aiōn](../../strongs/g/g165.md).

<a name="hebrews_13_9"></a>Hebrews 13:9

Be not [peripherō](../../strongs/g/g4064.md) with [poikilos](../../strongs/g/g4164.md) and [xenos](../../strongs/g/g3581.md) [didachē](../../strongs/g/g1322.md). For it is a [kalos](../../strongs/g/g2570.md) that the [kardia](../../strongs/g/g2588.md) be [bebaioō](../../strongs/g/g950.md) with [charis](../../strongs/g/g5485.md); not with [brōma](../../strongs/g/g1033.md), which have not [ōpheleō](../../strongs/g/g5623.md) them that have been [peripateō](../../strongs/g/g4043.md) therein.

<a name="hebrews_13_10"></a>Hebrews 13:10

We have a [thysiastērion](../../strongs/g/g2379.md), whereof they have no [exousia](../../strongs/g/g1849.md) to [phago](../../strongs/g/g5315.md) which [latreuō](../../strongs/g/g3000.md) the [skēnē](../../strongs/g/g4633.md).

<a name="hebrews_13_11"></a>Hebrews 13:11

For the [sōma](../../strongs/g/g4983.md) of those [zōon](../../strongs/g/g2226.md), whose [haima](../../strongs/g/g129.md) is [eispherō](../../strongs/g/g1533.md) into the [hagion](../../strongs/g/g39.md) by the [archiereus](../../strongs/g/g749.md) for [hamartia](../../strongs/g/g266.md), are [katakaiō](../../strongs/g/g2618.md) without the [parembolē](../../strongs/g/g3925.md).

<a name="hebrews_13_12"></a>Hebrews 13:12

Wherefore [Iēsous](../../strongs/g/g2424.md) also, that he might [hagiazō](../../strongs/g/g37.md) the [laos](../../strongs/g/g2992.md) with his own [haima](../../strongs/g/g129.md), [paschō](../../strongs/g/g3958.md) [exō](../../strongs/g/g1854.md) the [pylē](../../strongs/g/g4439.md).

<a name="hebrews_13_13"></a>Hebrews 13:13

Let us [exerchomai](../../strongs/g/g1831.md) therefore unto him [exō](../../strongs/g/g1854.md) the [parembolē](../../strongs/g/g3925.md), [pherō](../../strongs/g/g5342.md) his [oneidismos](../../strongs/g/g3680.md).

<a name="hebrews_13_14"></a>Hebrews 13:14

For here have we no [menō](../../strongs/g/g3306.md) [polis](../../strongs/g/g4172.md), but we [epizēteō](../../strongs/g/g1934.md) to [mellō](../../strongs/g/g3195.md).

<a name="hebrews_13_15"></a>Hebrews 13:15

By him therefore let us [anapherō](../../strongs/g/g399.md) the [thysia](../../strongs/g/g2378.md) of [ainesis](../../strongs/g/g133.md) to [theos](../../strongs/g/g2316.md) [diapantos](../../strongs/g/g1275.md), that is, the [karpos](../../strongs/g/g2590.md) of our [cheilos](../../strongs/g/g5491.md) [homologeō](../../strongs/g/g3670.md) to his [onoma](../../strongs/g/g3686.md).

<a name="hebrews_13_16"></a>Hebrews 13:16

But to [eupoiia](../../strongs/g/g2140.md) and to [koinōnia](../../strongs/g/g2842.md) [epilanthanomai](../../strongs/g/g1950.md) not: for with such [thysia](../../strongs/g/g2378.md) [theos](../../strongs/g/g2316.md) is [euaresteō](../../strongs/g/g2100.md).

<a name="hebrews_13_17"></a>Hebrews 13:17

[peithō](../../strongs/g/g3982.md) them that have the [hēgeomai](../../strongs/g/g2233.md) over you, and [hypeikō](../../strongs/g/g5226.md) yourselves: for they [agrypneō](../../strongs/g/g69.md) for your [psychē](../../strongs/g/g5590.md), as they that must [apodidōmi](../../strongs/g/g591.md) [logos](../../strongs/g/g3056.md), that they may [poieō](../../strongs/g/g4160.md) it with [chara](../../strongs/g/g5479.md), and not with [stenazō](../../strongs/g/g4727.md): for that is [alysitelēs](../../strongs/g/g255.md) for you.

<a name="hebrews_13_18"></a>Hebrews 13:18

[proseuchomai](../../strongs/g/g4336.md) for us: for we [peithō](../../strongs/g/g3982.md) we have a [kalos](../../strongs/g/g2570.md) [syneidēsis](../../strongs/g/g4893.md), in all things [thelō](../../strongs/g/g2309.md) to [anastrephō](../../strongs/g/g390.md) [kalōs](../../strongs/g/g2573.md).

<a name="hebrews_13_19"></a>Hebrews 13:19

But I [parakaleō](../../strongs/g/g3870.md) you the [perissoterōs](../../strongs/g/g4056.md) to [poieō](../../strongs/g/g4160.md) this, that I may be [apokathistēmi](../../strongs/g/g600.md) to you [tachion](../../strongs/g/g5032.md).

<a name="hebrews_13_20"></a>Hebrews 13:20

Now the [theos](../../strongs/g/g2316.md) of [eirēnē](../../strongs/g/g1515.md), that [anagō](../../strongs/g/g321.md) from the [nekros](../../strongs/g/g3498.md) our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), that [megas](../../strongs/g/g3173.md) [poimēn](../../strongs/g/g4166.md) of the [probaton](../../strongs/g/g4263.md), through the [haima](../../strongs/g/g129.md) of the [aiōnios](../../strongs/g/g166.md) [diathēkē](../../strongs/g/g1242.md),

<a name="hebrews_13_21"></a>Hebrews 13:21

[katartizō](../../strongs/g/g2675.md) you in every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md) to [poieō](../../strongs/g/g4160.md) his [thelēma](../../strongs/g/g2307.md), [poieō](../../strongs/g/g4160.md) in you that which is [euarestos](../../strongs/g/g2101.md) in his [enōpion](../../strongs/g/g1799.md), through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md); to whom be [doxa](../../strongs/g/g1391.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="hebrews_13_22"></a>Hebrews 13:22

And I [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), [anechō](../../strongs/g/g430.md) the [logos](../../strongs/g/g3056.md) of [paraklēsis](../../strongs/g/g3874.md): for I have [epistellō](../../strongs/g/g1989.md) unto you in [brachys](../../strongs/g/g1024.md).

<a name="hebrews_13_23"></a>Hebrews 13:23

[ginōskō](../../strongs/g/g1097.md) ye that our [adelphos](../../strongs/g/g80.md) [Timotheos](../../strongs/g/g5095.md) is [apolyō](../../strongs/g/g630.md); with whom, if he [erchomai](../../strongs/g/g2064.md) [tachion](../../strongs/g/g5032.md), I will [optanomai](../../strongs/g/g3700.md) you.

<a name="hebrews_13_24"></a>Hebrews 13:24

[aspazomai](../../strongs/g/g782.md) all them [hēgeomai](../../strongs/g/g2233.md) over you, and all the [hagios](../../strongs/g/g40.md). They of [Italia](../../strongs/g/g2482.md) [aspazomai](../../strongs/g/g782.md) you.

<a name="hebrews_13_25"></a>Hebrews 13:25

[charis](../../strongs/g/g5485.md) be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 12](hebrews_12.md)