# [Hebrews 2](https://www.blueletterbible.org/kjv/heb/2/1/s_1135001)

<a name="hebrews_2_1"></a>Hebrews 2:1

Therefore we [dei](../../strongs/g/g1163.md) to give [perissoterōs](../../strongs/g/g4056.md) [prosechō](../../strongs/g/g4337.md) to the things which we have [akouō](../../strongs/g/g191.md), lest at any time we should let [pararreō](../../strongs/g/g3901.md).

<a name="hebrews_2_2"></a>Hebrews 2:2

For if the [logos](../../strongs/g/g3056.md) [laleō](../../strongs/g/g2980.md) by [aggelos](../../strongs/g/g32.md) was [bebaios](../../strongs/g/g949.md), and every [parabasis](../../strongs/g/g3847.md) and [parakoē](../../strongs/g/g3876.md) [lambanō](../../strongs/g/g2983.md) an [endikos](../../strongs/g/g1738.md) [misthapodosia](../../strongs/g/g3405.md);

<a name="hebrews_2_3"></a>Hebrews 2:3

How shall we [ekpheugō](../../strongs/g/g1628.md), if we [ameleō](../../strongs/g/g272.md) [tēlikoutos](../../strongs/g/g5082.md) [sōtēria](../../strongs/g/g4991.md); which at the [archē](../../strongs/g/g746.md) [lambanō](../../strongs/g/g2983.md) to be [laleō](../../strongs/g/g2980.md) by the [kyrios](../../strongs/g/g2962.md), and was [bebaioō](../../strongs/g/g950.md) unto us by them that [akouō](../../strongs/g/g191.md);

<a name="hebrews_2_4"></a>Hebrews 2:4

[theos](../../strongs/g/g2316.md) also [synepimartyreō](../../strongs/g/g4901.md), both with [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md), and with [poikilos](../../strongs/g/g4164.md) [dynamis](../../strongs/g/g1411.md), and [merismos](../../strongs/g/g3311.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), according to his own [thelēsis](../../strongs/g/g2308.md)?

<a name="hebrews_2_5"></a>Hebrews 2:5

For unto the [aggelos](../../strongs/g/g32.md) hath he not [hypotassō](../../strongs/g/g5293.md) the [oikoumenē](../../strongs/g/g3625.md) [mellō](../../strongs/g/g3195.md), whereof we [laleō](../../strongs/g/g2980.md).

<a name="hebrews_2_6"></a>Hebrews 2:6

But one in [pou](../../strongs/g/g4225.md) [diamartyromai](../../strongs/g/g1263.md), [legō](../../strongs/g/g3004.md), What is [anthrōpos](../../strongs/g/g444.md), that thou art [mimnēskomai](../../strongs/g/g3403.md) of him? or the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) that thou [episkeptomai](../../strongs/g/g1980.md) him?

<a name="hebrews_2_7"></a>Hebrews 2:7

Thou [elattoō](../../strongs/g/g1642.md) him a [brachys](../../strongs/g/g1024.md) than the [aggelos](../../strongs/g/g32.md); thou [stephanoō](../../strongs/g/g4737.md) him with [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md), and didst [kathistēmi](../../strongs/g/g2525.md) him over the [ergon](../../strongs/g/g2041.md) of thy [cheir](../../strongs/g/g5495.md): [^1]

<a name="hebrews_2_8"></a>Hebrews 2:8

Thou hast [hypotassō](../../strongs/g/g5293.md) all things [hypokatō](../../strongs/g/g5270.md) his [pous](../../strongs/g/g4228.md). For in that he [hypotassō](../../strongs/g/g5293.md) all under him, he [aphiēmi](../../strongs/g/g863.md) nothing that [hypotassō](../../strongs/g/g5293.md) [anypotaktos](../../strongs/g/g506.md) him. But now we [horaō](../../strongs/g/g3708.md) not yet all things [hypotassō](../../strongs/g/g5293.md) him.

<a name="hebrews_2_9"></a>Hebrews 2:9

But we [blepō](../../strongs/g/g991.md) [Iēsous](../../strongs/g/g2424.md), who was [elattoō](../../strongs/g/g1642.md) a [brachys](../../strongs/g/g1024.md) than the [aggelos](../../strongs/g/g32.md) for the [pathēma](../../strongs/g/g3804.md) of [thanatos](../../strongs/g/g2288.md), [stephanoō](../../strongs/g/g4737.md) with [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md); that he by the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) should [geuomai](../../strongs/g/g1089.md) [thanatos](../../strongs/g/g2288.md) for every man.

<a name="hebrews_2_10"></a>Hebrews 2:10

For it became him, for whom are all things, and by whom all things, in [agō](../../strongs/g/g71.md) [polys](../../strongs/g/g4183.md) [huios](../../strongs/g/g5207.md) unto [doxa](../../strongs/g/g1391.md), to [teleioō](../../strongs/g/g5048.md) the [archēgos](../../strongs/g/g747.md) of their [sōtēria](../../strongs/g/g4991.md) through [pathēma](../../strongs/g/g3804.md).

<a name="hebrews_2_11"></a>Hebrews 2:11

For both he that [hagiazō](../../strongs/g/g37.md) and they who are [hagiazō](../../strongs/g/g37.md) all of one: for which cause he is not [epaischynomai](../../strongs/g/g1870.md) to [kaleō](../../strongs/g/g2564.md) them [adelphos](../../strongs/g/g80.md),

<a name="hebrews_2_12"></a>Hebrews 2:12

[legō](../../strongs/g/g3004.md), I will [apaggellō](../../strongs/g/g518.md) thy [onoma](../../strongs/g/g3686.md) unto my [adelphos](../../strongs/g/g80.md), in the midst of the [ekklēsia](../../strongs/g/g1577.md) will I [hymneō](../../strongs/g/g5214.md) unto thee.

<a name="hebrews_2_13"></a>Hebrews 2:13

And again, I will [peithō](../../strongs/g/g3982.md) in him. And again, [idou](../../strongs/g/g2400.md) I and the [paidion](../../strongs/g/g3813.md) which [theos](../../strongs/g/g2316.md) hath [didōmi](../../strongs/g/g1325.md) me.

<a name="hebrews_2_14"></a>Hebrews 2:14

Forasmuch then as the [paidion](../../strongs/g/g3813.md) are [koinōneō](../../strongs/g/g2841.md) of [sarx](../../strongs/g/g4561.md) and [haima](../../strongs/g/g129.md), he also himself [paraplēsiōs](../../strongs/g/g3898.md) [metechō](../../strongs/g/g3348.md) of the same; that through [thanatos](../../strongs/g/g2288.md) he might [katargeō](../../strongs/g/g2673.md) him that had the [kratos](../../strongs/g/g2904.md) of [thanatos](../../strongs/g/g2288.md), that is, the [diabolos](../../strongs/g/g1228.md);

<a name="hebrews_2_15"></a>Hebrews 2:15

And [apallassō](../../strongs/g/g525.md) them who through [phobos](../../strongs/g/g5401.md) of [thanatos](../../strongs/g/g2288.md) were all their [zaō](../../strongs/g/g2198.md) [enochos](../../strongs/g/g1777.md) to [douleia](../../strongs/g/g1397.md).

<a name="hebrews_2_16"></a>Hebrews 2:16

For [dēpou](../../strongs/g/g1222.md) he [epilambanomai](../../strongs/g/g1949.md) not on [aggelos](../../strongs/g/g32.md); but he [epilambanomai](../../strongs/g/g1949.md) on the [sperma](../../strongs/g/g4690.md) of [Abraam](../../strongs/g/g11.md).

<a name="hebrews_2_17"></a>Hebrews 2:17

Wherefore in all things it [opheilō](../../strongs/g/g3784.md) him to be [homoioō](../../strongs/g/g3666.md) unto [adelphos](../../strongs/g/g80.md), that he might be an [eleēmōn](../../strongs/g/g1655.md) and [pistos](../../strongs/g/g4103.md) [archiereus](../../strongs/g/g749.md) in things to [theos](../../strongs/g/g2316.md), to [hilaskomai](../../strongs/g/g2433.md) for the [hamartia](../../strongs/g/g266.md) of the [laos](../../strongs/g/g2992.md).

<a name="hebrews_2_18"></a>Hebrews 2:18

For in that he himself hath [paschō](../../strongs/g/g3958.md) being [peirazō](../../strongs/g/g3985.md), he is able to [boētheō](../../strongs/g/g997.md) them that are [peirazō](../../strongs/g/g3985.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 1](hebrews_1.md) - [Hebrews 3](hebrews_3.md)

---

[^1]: [Hebrews 2:7 Commentary](../../commentary/hebrews/hebrews_2_commentary.md#hebrews_2_7)
