# [Hebrews 9](https://www.blueletterbible.org/kjv/heb/9/1/s_1142001)

<a name="hebrews_9_1"></a>Hebrews 9:1

Then [men](../../strongs/g/g3303.md) the [prōtos](../../strongs/g/g4413.md) had also [dikaiōma](../../strongs/g/g1345.md) of [latreia](../../strongs/g/g2999.md), and a [kosmikos](../../strongs/g/g2886.md) [hagion](../../strongs/g/g39.md).

<a name="hebrews_9_2"></a>Hebrews 9:2

For there [kataskeuazō](../../strongs/g/g2680.md) a [skēnē](../../strongs/g/g4633.md); the [prōtos](../../strongs/g/g4413.md), wherein was the [lychnia](../../strongs/g/g3087.md), and the [trapeza](../../strongs/g/g5132.md), and the [prothesis](../../strongs/g/g4286.md) [artos](../../strongs/g/g740.md); which is [legō](../../strongs/g/g3004.md) the [hagion](../../strongs/g/g39.md).

<a name="hebrews_9_3"></a>Hebrews 9:3

And after the second [katapetasma](../../strongs/g/g2665.md), the [skēnē](../../strongs/g/g4633.md) which is [legō](../../strongs/g/g3004.md) the [hagion](../../strongs/g/g39.md) [hagion](../../strongs/g/g39.md);

<a name="hebrews_9_4"></a>Hebrews 9:4

Which had the [chrysous](../../strongs/g/g5552.md) [thymiatērion](../../strongs/g/g2369.md), and the [kibōtos](../../strongs/g/g2787.md) of the [diathēkē](../../strongs/g/g1242.md) [perikalyptō](../../strongs/g/g4028.md) [pantothen](../../strongs/g/g3840.md) with [chrysion](../../strongs/g/g5553.md), wherein was the [chrysous](../../strongs/g/g5552.md) [stamnos](../../strongs/g/g4713.md) that had [manna](../../strongs/g/g3131.md), and [Aarōn](../../strongs/g/g2.md) [rhabdos](../../strongs/g/g4464.md) that [blastanō](../../strongs/g/g985.md), and the [plax](../../strongs/g/g4109.md) of the [diathēkē](../../strongs/g/g1242.md);

<a name="hebrews_9_5"></a>Hebrews 9:5

And [hyperanō](../../strongs/g/g5231.md) it the [cheroub](../../strongs/g/g5502.md) of [doxa](../../strongs/g/g1391.md) [kataskiazō](../../strongs/g/g2683.md) the [hilastērion](../../strongs/g/g2435.md); of which we cannot now [legō](../../strongs/g/g3004.md) [kata](../../strongs/g/g2596.md) [meros](../../strongs/g/g3313.md).

<a name="hebrews_9_6"></a>Hebrews 9:6

Now when these things were thus [kataskeuazō](../../strongs/g/g2680.md), the [hiereus](../../strongs/g/g2409.md) [eiseimi](../../strongs/g/g1524.md) [diapantos](../../strongs/g/g1275.md) into the [prōtos](../../strongs/g/g4413.md) [skēnē](../../strongs/g/g4633.md), [epiteleō](../../strongs/g/g2005.md) the [latreia](../../strongs/g/g2999.md).

<a name="hebrews_9_7"></a>Hebrews 9:7

But into the second the [archiereus](../../strongs/g/g749.md) alone [hapax](../../strongs/g/g530.md) every [eniautos](../../strongs/g/g1763.md), not without [haima](../../strongs/g/g129.md), which he [prospherō](../../strongs/g/g4374.md) for himself, and the [agnoēma](../../strongs/g/g51.md) of the [laos](../../strongs/g/g2992.md):

<a name="hebrews_9_8"></a>Hebrews 9:8

The [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) this [dēloō](../../strongs/g/g1213.md), that [hodos](../../strongs/g/g3598.md) into the [hagion](../../strongs/g/g39.md) was not yet [phaneroō](../../strongs/g/g5319.md), while as the [prōtos](../../strongs/g/g4413.md) [skēnē](../../strongs/g/g4633.md) was yet [stasis](../../strongs/g/g4714.md):

<a name="hebrews_9_9"></a>Hebrews 9:9

Which was a [parabolē](../../strongs/g/g3850.md) for the [kairos](../../strongs/g/g2540.md) then [enistēmi](../../strongs/g/g1764.md), in which were [prospherō](../../strongs/g/g4374.md) both [dōron](../../strongs/g/g1435.md) and [thysia](../../strongs/g/g2378.md), that could not make him that did the [latreuō](../../strongs/g/g3000.md) [teleioō](../../strongs/g/g5048.md), as pertaining to the [syneidēsis](../../strongs/g/g4893.md);

<a name="hebrews_9_10"></a>Hebrews 9:10

only in [brōma](../../strongs/g/g1033.md) and [poma](../../strongs/g/g4188.md), and [diaphoros](../../strongs/g/g1313.md) [baptismos](../../strongs/g/g909.md), and [sarx](../../strongs/g/g4561.md) [dikaiōma](../../strongs/g/g1345.md), [epikeimai](../../strongs/g/g1945.md) until the [kairos](../../strongs/g/g2540.md) of [diorthōsis](../../strongs/g/g1357.md).

<a name="hebrews_9_11"></a>Hebrews 9:11

But [Christos](../../strongs/g/g5547.md) [paraginomai](../../strongs/g/g3854.md) an [archiereus](../../strongs/g/g749.md) of [agathos](../../strongs/g/g18.md) to [mellō](../../strongs/g/g3195.md), by a [meizōn](../../strongs/g/g3187.md) and [teleios](../../strongs/g/g5046.md) [skēnē](../../strongs/g/g4633.md), not [cheiropoiētos](../../strongs/g/g5499.md), [tout᾿ estin](../../strongs/g/g5123.md), not of this [ktisis](../../strongs/g/g2937.md);

<a name="hebrews_9_12"></a>Hebrews 9:12

Neither by the [haima](../../strongs/g/g129.md) of [tragos](../../strongs/g/g5131.md) and [moschos](../../strongs/g/g3448.md), but by his own [haima](../../strongs/g/g129.md) he [eiserchomai](../../strongs/g/g1525.md) in [ephapax](../../strongs/g/g2178.md) into the [hagion](../../strongs/g/g39.md), [heuriskō](../../strongs/g/g2147.md) [aiōnios](../../strongs/g/g166.md) [lytrōsis](../../strongs/g/g3085.md).

<a name="hebrews_9_13"></a>Hebrews 9:13

For if the [haima](../../strongs/g/g129.md) of [tauros](../../strongs/g/g5022.md) and of [tragos](../../strongs/g/g5131.md), and the [spodos](../../strongs/g/g4700.md) of a [damalis](../../strongs/g/g1151.md) [rhantizō](../../strongs/g/g4472.md) the [koinoō](../../strongs/g/g2840.md), [hagiazō](../../strongs/g/g37.md) to the [katharotēs](../../strongs/g/g2514.md) of the [sarx](../../strongs/g/g4561.md):

<a name="hebrews_9_14"></a>Hebrews 9:14

How much more shall the [haima](../../strongs/g/g129.md) of [Christos](../../strongs/g/g5547.md), who through the [aiōnios](../../strongs/g/g166.md) [pneuma](../../strongs/g/g4151.md) [prospherō](../../strongs/g/g4374.md) himself [amōmos](../../strongs/g/g299.md) to [theos](../../strongs/g/g2316.md), [katharizō](../../strongs/g/g2511.md) your [syneidēsis](../../strongs/g/g4893.md) from [nekros](../../strongs/g/g3498.md) [ergon](../../strongs/g/g2041.md) to [latreuō](../../strongs/g/g3000.md) the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md)?

<a name="hebrews_9_15"></a>Hebrews 9:15

And for this cause he is the [mesitēs](../../strongs/g/g3316.md) of the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md), that by means of [thanatos](../../strongs/g/g2288.md), for the [apolytrōsis](../../strongs/g/g629.md) of the [parabasis](../../strongs/g/g3847.md) that were under the first [diathēkē](../../strongs/g/g1242.md), they which are [kaleō](../../strongs/g/g2564.md) might [lambanō](../../strongs/g/g2983.md) the [epaggelia](../../strongs/g/g1860.md) of [aiōnios](../../strongs/g/g166.md) [klēronomia](../../strongs/g/g2817.md).

<a name="hebrews_9_16"></a>Hebrews 9:16

For where a [diathēkē](../../strongs/g/g1242.md), there must also of [anagkē](../../strongs/g/g318.md) [pherō](../../strongs/g/g5342.md) the [thanatos](../../strongs/g/g2288.md) of the [diatithēmi](../../strongs/g/g1303.md).

<a name="hebrews_9_17"></a>Hebrews 9:17

For a [diathēkē](../../strongs/g/g1242.md) of [bebaios](../../strongs/g/g949.md) after men are [nekros](../../strongs/g/g3498.md): otherwise it is of no [ischyō](../../strongs/g/g2480.md) at all while the [diatithēmi](../../strongs/g/g1303.md) [zaō](../../strongs/g/g2198.md).

<a name="hebrews_9_18"></a>Hebrews 9:18

Whereupon neither the first was [egkainizō](../../strongs/g/g1457.md) without [haima](../../strongs/g/g129.md).

<a name="hebrews_9_19"></a>Hebrews 9:19

For when [Mōÿsēs](../../strongs/g/g3475.md) had [laleō](../../strongs/g/g2980.md) every [entolē](../../strongs/g/g1785.md) to all the [laos](../../strongs/g/g2992.md) according to the [nomos](../../strongs/g/g3551.md), he [lambanō](../../strongs/g/g2983.md) the [haima](../../strongs/g/g129.md) of [moschos](../../strongs/g/g3448.md) and of [tragos](../../strongs/g/g5131.md), with [hydōr](../../strongs/g/g5204.md), and [kokkinos](../../strongs/g/g2847.md) [erion](../../strongs/g/g2053.md), and [hyssōpos](../../strongs/g/g5301.md), and [rhantizō](../../strongs/g/g4472.md) both the [biblion](../../strongs/g/g975.md), and all the [laos](../../strongs/g/g2992.md),

<a name="hebrews_9_20"></a>Hebrews 9:20

[legō](../../strongs/g/g3004.md), This the [haima](../../strongs/g/g129.md) of the [diathēkē](../../strongs/g/g1242.md) which [theos](../../strongs/g/g2316.md) hath [entellō](../../strongs/g/g1781.md) unto you.

<a name="hebrews_9_21"></a>Hebrews 9:21

[homoiōs](../../strongs/g/g3668.md) he [rhantizō](../../strongs/g/g4472.md) with [haima](../../strongs/g/g129.md) both the [skēnē](../../strongs/g/g4633.md), and all the [skeuos](../../strongs/g/g4632.md) of the [leitourgia](../../strongs/g/g3009.md).

<a name="hebrews_9_22"></a>Hebrews 9:22

And [schedon](../../strongs/g/g4975.md) all things are by the [nomos](../../strongs/g/g3551.md) [katharizō](../../strongs/g/g2511.md) with [haima](../../strongs/g/g129.md); and without [haimatekchysia](../../strongs/g/g130.md) is no [aphesis](../../strongs/g/g859.md).

<a name="hebrews_9_23"></a>Hebrews 9:23

therefore [anagkē](../../strongs/g/g318.md) that the [hypodeigma](../../strongs/g/g5262.md) of things in the [ouranos](../../strongs/g/g3772.md) should be [katharizō](../../strongs/g/g2511.md) with these; but the [epouranios](../../strongs/g/g2032.md) themselves with [kreittōn](../../strongs/g/g2909.md) [thysia](../../strongs/g/g2378.md) than these.

<a name="hebrews_9_24"></a>Hebrews 9:24

For [Christos](../../strongs/g/g5547.md) is not [eiserchomai](../../strongs/g/g1525.md) into the [hagion](../../strongs/g/g39.md) [cheiropoiētos](../../strongs/g/g5499.md), the [antitypos](../../strongs/g/g499.md) of the [alēthinos](../../strongs/g/g228.md); but into [ouranos](../../strongs/g/g3772.md) itself, now to [emphanizō](../../strongs/g/g1718.md) in the [prosōpon](../../strongs/g/g4383.md) of [theos](../../strongs/g/g2316.md) for us:

<a name="hebrews_9_25"></a>Hebrews 9:25

Nor yet that he should [prospherō](../../strongs/g/g4374.md)himself often, as the [archiereus](../../strongs/g/g749.md) [eiserchomai](../../strongs/g/g1525.md) into the [hagion](../../strongs/g/g39.md) every [eniautos](../../strongs/g/g1763.md) with [haima](../../strongs/g/g129.md) of [allotrios](../../strongs/g/g245.md);

<a name="hebrews_9_26"></a>Hebrews 9:26

For then must he often have [paschō](../../strongs/g/g3958.md) since the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md): but now [hapax](../../strongs/g/g530.md) in the [synteleia](../../strongs/g/g4930.md) of the [aiōn](../../strongs/g/g165.md) hath he [phaneroō](../../strongs/g/g5319.md) to [athetēsis](../../strongs/g/g115.md) [hamartia](../../strongs/g/g266.md) by the [thysia](../../strongs/g/g2378.md) of himself.

<a name="hebrews_9_27"></a>Hebrews 9:27

And as it is [apokeimai](../../strongs/g/g606.md) unto [anthrōpos](../../strongs/g/g444.md) [hapax](../../strongs/g/g530.md) to [apothnēskō](../../strongs/g/g599.md), but after this the [krisis](../../strongs/g/g2920.md):

<a name="hebrews_9_28"></a>Hebrews 9:28

So [Christos](../../strongs/g/g5547.md) was [hapax](../../strongs/g/g530.md) [prospherō](../../strongs/g/g4374.md) to [anapherō](../../strongs/g/g399.md) the [hamartia](../../strongs/g/g266.md) of [polys](../../strongs/g/g4183.md); and unto them that [apekdechomai](../../strongs/g/g553.md) for him shall he [optanomai](../../strongs/g/g3700.md) the second time without [hamartia](../../strongs/g/g266.md) unto [sōtēria](../../strongs/g/g4991.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 8](hebrews_8.md) - [Hebrews 10](hebrews_10.md)