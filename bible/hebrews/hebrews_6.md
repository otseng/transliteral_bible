# [Hebrews 6](https://www.blueletterbible.org/kjv/heb/6/1/s_1139001)

<a name="hebrews_6_1"></a>Hebrews 6:1

Therefore [aphiēmi](../../strongs/g/g863.md) the [archē](../../strongs/g/g746.md) of the [logos](../../strongs/g/g3056.md) of [Christos](../../strongs/g/g5547.md), let us [pherō](../../strongs/g/g5342.md) unto [teleiotēs](../../strongs/g/g5047.md); not [kataballō](../../strongs/g/g2598.md) again the [themelios](../../strongs/g/g2310.md) of [metanoia](../../strongs/g/g3341.md) from [nekros](../../strongs/g/g3498.md) [ergon](../../strongs/g/g2041.md), and of [pistis](../../strongs/g/g4102.md) toward [theos](../../strongs/g/g2316.md),

<a name="hebrews_6_2"></a>Hebrews 6:2

Of the [didachē](../../strongs/g/g1322.md) of [baptismos](../../strongs/g/g909.md), and of [epithesis](../../strongs/g/g1936.md) of [cheir](../../strongs/g/g5495.md), and of [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md), and of [aiōnios](../../strongs/g/g166.md) [krima](../../strongs/g/g2917.md).

<a name="hebrews_6_3"></a>Hebrews 6:3

And this will we [poieō](../../strongs/g/g4160.md), if [theos](../../strongs/g/g2316.md) [epitrepō](../../strongs/g/g2010.md).

<a name="hebrews_6_4"></a>Hebrews 6:4

For [adynatos](../../strongs/g/g102.md) for those who were [hapax](../../strongs/g/g530.md) [phōtizō](../../strongs/g/g5461.md), and have [geuomai](../../strongs/g/g1089.md) of the [epouranios](../../strongs/g/g2032.md) [dōrea](../../strongs/g/g1431.md), and were [ginomai](../../strongs/g/g1096.md) [metochos](../../strongs/g/g3353.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md),

<a name="hebrews_6_5"></a>Hebrews 6:5

And have [geuomai](../../strongs/g/g1089.md) the [kalos](../../strongs/g/g2570.md) [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md), and the [dynamis](../../strongs/g/g1411.md) of the [aiōn](../../strongs/g/g165.md) to come,

<a name="hebrews_6_6"></a>Hebrews 6:6

If they shall [parapiptō](../../strongs/g/g3895.md), to [anakainizō](../../strongs/g/g340.md) again unto [metanoia](../../strongs/g/g3341.md); seeing they [anastauroō](../../strongs/g/g388.md) to themselves the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), and [paradeigmatizō](../../strongs/g/g3856.md).

<a name="hebrews_6_7"></a>Hebrews 6:7

For [gē](../../strongs/g/g1093.md) which [pinō](../../strongs/g/g4095.md) the [hyetos](../../strongs/g/g5205.md) that [erchomai](../../strongs/g/g2064.md) oft upon it, and [tiktō](../../strongs/g/g5088.md) [botanē](../../strongs/g/g1008.md) [euthetos](../../strongs/g/g2111.md) for them by whom it is [geōrgeō](../../strongs/g/g1090.md), [metalambanō](../../strongs/g/g3335.md) [eulogia](../../strongs/g/g2129.md) from [theos](../../strongs/g/g2316.md):

<a name="hebrews_6_8"></a>Hebrews 6:8

But that which [ekpherō](../../strongs/g/g1627.md) [akantha](../../strongs/g/g173.md) and [tribolos](../../strongs/g/g5146.md) [adokimos](../../strongs/g/g96.md), and [eggys](../../strongs/g/g1451.md) unto [katara](../../strongs/g/g2671.md); whose [telos](../../strongs/g/g5056.md) is to be [kausis](../../strongs/g/g2740.md).

<a name="hebrews_6_9"></a>Hebrews 6:9

But, [agapētos](../../strongs/g/g27.md), we are [peithō](../../strongs/g/g3982.md) [kreittōn](../../strongs/g/g2909.md) of you, and things that accompany [sōtēria](../../strongs/g/g4991.md), though we thus [laleō](../../strongs/g/g2980.md).

<a name="hebrews_6_10"></a>Hebrews 6:10

For [theos](../../strongs/g/g2316.md) not [adikos](../../strongs/g/g94.md) to [epilanthanomai](../../strongs/g/g1950.md) your [ergon](../../strongs/g/g2041.md) and [kopos](../../strongs/g/g2873.md) of [agapē](../../strongs/g/g26.md), which ye have [endeiknymi](../../strongs/g/g1731.md) toward his [onoma](../../strongs/g/g3686.md), in that ye have [diakoneō](../../strongs/g/g1247.md) to the [hagios](../../strongs/g/g40.md), and do [diakoneō](../../strongs/g/g1247.md).

<a name="hebrews_6_11"></a>Hebrews 6:11

And we [epithymeō](../../strongs/g/g1937.md) that every one of you do [endeiknymi](../../strongs/g/g1731.md) the same [spoudē](../../strongs/g/g4710.md) to the [plērophoria](../../strongs/g/g4136.md) of [elpis](../../strongs/g/g1680.md) unto the [telos](../../strongs/g/g5056.md):

<a name="hebrews_6_12"></a>Hebrews 6:12

That ye be not [nōthros](../../strongs/g/g3576.md), but [mimētēs](../../strongs/g/g3402.md) of them who through [pistis](../../strongs/g/g4102.md) and [makrothymia](../../strongs/g/g3115.md) [klēronomeō](../../strongs/g/g2816.md) the [epaggelia](../../strongs/g/g1860.md).

<a name="hebrews_6_13"></a>Hebrews 6:13

For when [theos](../../strongs/g/g2316.md) made [epaggellomai](../../strongs/g/g1861.md) to [Abraam](../../strongs/g/g11.md), because he could [omnyō](../../strongs/g/g3660.md) by no [meizōn](../../strongs/g/g3187.md), he [omnyō](../../strongs/g/g3660.md) by himself,

<a name="hebrews_6_14"></a>Hebrews 6:14

[legō](../../strongs/g/g3004.md), [ē](../../strongs/g/g2229.md) [mēn](../../strongs/g/g3375.md) [eulogeō](../../strongs/g/g2127.md) I will [eulogeō](../../strongs/g/g2127.md) thee, and [plēthynō](../../strongs/g/g4129.md) I will [plēthynō](../../strongs/g/g4129.md) thee.

<a name="hebrews_6_15"></a>Hebrews 6:15

And so, after he had [makrothymeō](../../strongs/g/g3114.md), he [epitygchanō](../../strongs/g/g2013.md) the [epaggelia](../../strongs/g/g1860.md).

<a name="hebrews_6_16"></a>Hebrews 6:16

For [anthrōpos](../../strongs/g/g444.md) [men](../../strongs/g/g3303.md) [omnyō](../../strongs/g/g3660.md) by the [meizōn](../../strongs/g/g3187.md): and an [horkos](../../strongs/g/g3727.md) for [bebaiōsis](../../strongs/g/g951.md) to them a [peras](../../strongs/g/g4009.md) of all [antilogia](../../strongs/g/g485.md).

<a name="hebrews_6_17"></a>Hebrews 6:17

Wherein [theos](../../strongs/g/g2316.md), [boulomai](../../strongs/g/g1014.md) [perissoteron](../../strongs/g/g4054.md) to [epideiknymi](../../strongs/g/g1925.md) unto the [klēronomos](../../strongs/g/g2818.md) of [epaggelia](../../strongs/g/g1860.md) the [ametathetos](../../strongs/g/g276.md) of his [boulē](../../strongs/g/g1012.md), [mesiteuō](../../strongs/g/g3315.md) by an [horkos](../../strongs/g/g3727.md):

<a name="hebrews_6_18"></a>Hebrews 6:18

That by two [ametathetos](../../strongs/g/g276.md) [pragma](../../strongs/g/g4229.md), in which [adynatos](../../strongs/g/g102.md) for [theos](../../strongs/g/g2316.md) to [pseudomai](../../strongs/g/g5574.md), we might have an [ischyros](../../strongs/g/g2478.md) [paraklēsis](../../strongs/g/g3874.md), who have [katapheugō](../../strongs/g/g2703.md) to [krateō](../../strongs/g/g2902.md) upon the [elpis](../../strongs/g/g1680.md) [prokeimai](../../strongs/g/g4295.md) us:

<a name="hebrews_6_19"></a>Hebrews 6:19

Which we have as an [agkyra](../../strongs/g/g45.md) of the [psychē](../../strongs/g/g5590.md), both [asphalēs](../../strongs/g/g804.md) and [bebaios](../../strongs/g/g949.md), and which [eiserchomai](../../strongs/g/g1525.md) into that [esōteros](../../strongs/g/g2082.md) the [katapetasma](../../strongs/g/g2665.md);

<a name="hebrews_6_20"></a>Hebrews 6:20

Whither the [prodromos](../../strongs/g/g4274.md) is for us [eiserchomai](../../strongs/g/g1525.md), even [Iēsous](../../strongs/g/g2424.md), made an [archiereus](../../strongs/g/g749.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) after the [taxis](../../strongs/g/g5010.md) of [Melchisedek](../../strongs/g/g3198.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 5](hebrews_5.md) - [Hebrews 7](hebrews_7.md)