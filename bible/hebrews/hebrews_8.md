# [Hebrews 8](https://www.blueletterbible.org/kjv/heb/8/1/s_1141001)

<a name="hebrews_8_1"></a>Hebrews 8:1

Now of the things which we have [legō](../../strongs/g/g3004.md) the [kephalaion](../../strongs/g/g2774.md): We have such an [archiereus](../../strongs/g/g749.md), who is [kathizō](../../strongs/g/g2523.md) on the [dexios](../../strongs/g/g1188.md) of the [thronos](../../strongs/g/g2362.md) of the [megalōsynē](../../strongs/g/g3172.md) in the [ouranos](../../strongs/g/g3772.md);

<a name="hebrews_8_2"></a>Hebrews 8:2

A [leitourgos](../../strongs/g/g3011.md) of the [hagion](../../strongs/g/g39.md), and of the [alēthinos](../../strongs/g/g228.md) [skēnē](../../strongs/g/g4633.md), which the [kyrios](../../strongs/g/g2962.md) [pēgnymi](../../strongs/g/g4078.md), and not [anthrōpos](../../strongs/g/g444.md).

<a name="hebrews_8_3"></a>Hebrews 8:3

For every [archiereus](../../strongs/g/g749.md) is [kathistēmi](../../strongs/g/g2525.md) to [prospherō](../../strongs/g/g4374.md) [dōron](../../strongs/g/g1435.md) and [thysia](../../strongs/g/g2378.md): wherefore of [anagkaios](../../strongs/g/g316.md) that [touton](../../strongs/g/g5126.md) have somewhat also to [prospherō](../../strongs/g/g4374.md).

<a name="hebrews_8_4"></a>Hebrews 8:4

For if he were on [gē](../../strongs/g/g1093.md), he should not be a [hiereus](../../strongs/g/g2409.md), seeing that there are [hiereus](../../strongs/g/g2409.md) that [prospherō](../../strongs/g/g4374.md) [dōron](../../strongs/g/g1435.md) according to the [nomos](../../strongs/g/g3551.md):

<a name="hebrews_8_5"></a>Hebrews 8:5

Who [latreuō](../../strongs/g/g3000.md) unto the [hypodeigma](../../strongs/g/g5262.md) and [skia](../../strongs/g/g4639.md) of [epouranios](../../strongs/g/g2032.md), as [Mōÿsēs](../../strongs/g/g3475.md) was [chrēmatizō](../../strongs/g/g5537.md) when he was about to [epiteleō](../../strongs/g/g2005.md) the [skēnē](../../strongs/g/g4633.md): for, [horaō](../../strongs/g/g3708.md), [phēmi](../../strongs/g/g5346.md) he, thou [poieō](../../strongs/g/g4160.md) all things according to the [typos](../../strongs/g/g5179.md) [deiknyō](../../strongs/g/g1166.md) to thee in the [oros](../../strongs/g/g3735.md).

<a name="hebrews_8_6"></a>Hebrews 8:6

But [nyni](../../strongs/g/g3570.md) hath he [tygchanō](../../strongs/g/g5177.md) a [diaphoros](../../strongs/g/g1313.md) [leitourgia](../../strongs/g/g3009.md), by how much also he is the [mesitēs](../../strongs/g/g3316.md) of a [kreittōn](../../strongs/g/g2909.md) [diathēkē](../../strongs/g/g1242.md), which was [nomotheteō](../../strongs/g/g3549.md) upon [kreittōn](../../strongs/g/g2909.md) [epaggelia](../../strongs/g/g1860.md).

<a name="hebrews_8_7"></a>Hebrews 8:7

For if that [prōtos](../../strongs/g/g4413.md) had been [amemptos](../../strongs/g/g273.md), then should no [topos](../../strongs/g/g5117.md) have been [zēteō](../../strongs/g/g2212.md) for the [deuteros](../../strongs/g/g1208.md).

<a name="hebrews_8_8"></a>Hebrews 8:8

For [memphomai](../../strongs/g/g3201.md) with them, he [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), the [hēmera](../../strongs/g/g2250.md) [erchomai](../../strongs/g/g2064.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), when I will [synteleō](../../strongs/g/g4931.md) a [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md) with the [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md) and with the [oikos](../../strongs/g/g3624.md) of [Ioudas](../../strongs/g/g2455.md):

<a name="hebrews_8_9"></a>Hebrews 8:9

Not according to the [diathēkē](../../strongs/g/g1242.md) that I [poieō](../../strongs/g/g4160.md) with their [patēr](../../strongs/g/g3962.md) in the [hēmera](../../strongs/g/g2250.md) when I [epilambanomai](../../strongs/g/g1949.md) them by the [cheir](../../strongs/g/g5495.md) to [exagō](../../strongs/g/g1806.md) them out of the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md); because they [emmenō](../../strongs/g/g1696.md) not in my [diathēkē](../../strongs/g/g1242.md), and I [ameleō](../../strongs/g/g272.md) them, [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md).

<a name="hebrews_8_10"></a>Hebrews 8:10

For this the [diathēkē](../../strongs/g/g1242.md) that I will [diatithēmi](../../strongs/g/g1303.md) with the [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md) after those [hēmera](../../strongs/g/g2250.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md); I will [didōmi](../../strongs/g/g1325.md) my [nomos](../../strongs/g/g3551.md) into their [dianoia](../../strongs/g/g1271.md), and [epigraphō](../../strongs/g/g1924.md) them in their [kardia](../../strongs/g/g2588.md): and I will be to them a [theos](../../strongs/g/g2316.md), and they shall be to me a [laos](../../strongs/g/g2992.md):

<a name="hebrews_8_11"></a>Hebrews 8:11

And they shall not [didaskō](../../strongs/g/g1321.md) [hekastos](../../strongs/g/g1538.md) his [plēsion](../../strongs/g/g4139.md), and [hekastos](../../strongs/g/g1538.md) his [adelphos](../../strongs/g/g80.md), [legō](../../strongs/g/g3004.md), [ginōskō](../../strongs/g/g1097.md) the [kyrios](../../strongs/g/g2962.md): for all shall [eidō](../../strongs/g/g1492.md) me, from the [mikros](../../strongs/g/g3398.md) to the [megas](../../strongs/g/g3173.md).

<a name="hebrews_8_12"></a>Hebrews 8:12

For I will be [hileōs](../../strongs/g/g2436.md) to their [adikia](../../strongs/g/g93.md), and their [hamartia](../../strongs/g/g266.md) and their [anomia](../../strongs/g/g458.md) will I [mnaomai](../../strongs/g/g3415.md) no more.

<a name="hebrews_8_13"></a>Hebrews 8:13

In that he [legō](../../strongs/g/g3004.md), A [kainos](../../strongs/g/g2537.md), he hath [palaioō](../../strongs/g/g3822.md) the [prōtos](../../strongs/g/g4413.md). Now that which [palaioō](../../strongs/g/g3822.md) and [gēraskō](../../strongs/g/g1095.md) [eggys](../../strongs/g/g1451.md) to [aphanismos](../../strongs/g/g854.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 7](hebrews_7.md) - [Hebrews 9](hebrews_9.md)