# [Hebrews 12](https://www.blueletterbible.org/kjv/heb/12/1/s_1145001)

<a name="hebrews_12_1"></a>Hebrews 12:1

[toigaroun](../../strongs/g/g5105.md) seeing we also are [perikeimai](../../strongs/g/g4029.md) [hēmin](../../strongs/g/g2254.md) with [tosoutos](../../strongs/g/g5118.md) a [nephos](../../strongs/g/g3509.md) of [martys](../../strongs/g/g3144.md), let us [apotithēmi](../../strongs/g/g659.md) every [ogkos](../../strongs/g/g3591.md), and the [hamartia](../../strongs/g/g266.md) which [euperistatos](../../strongs/g/g2139.md) us, and let us [trechō](../../strongs/g/g5143.md) with [hypomonē](../../strongs/g/g5281.md) the [agōn](../../strongs/g/g73.md) that is [prokeimai](../../strongs/g/g4295.md) us,

<a name="hebrews_12_2"></a>Hebrews 12:2

[aphoraō](../../strongs/g/g872.md) unto [Iēsous](../../strongs/g/g2424.md) the [archēgos](../../strongs/g/g747.md) and [teleiōtēs](../../strongs/g/g5051.md) of [pistis](../../strongs/g/g4102.md); who for the [chara](../../strongs/g/g5479.md) that was [prokeimai](../../strongs/g/g4295.md) him [hypomenō](../../strongs/g/g5278.md) the [stauros](../../strongs/g/g4716.md), [kataphroneō](../../strongs/g/g2706.md) the [aischynē](../../strongs/g/g152.md), and is [kathizō](../../strongs/g/g2523.md) at the [dexios](../../strongs/g/g1188.md) of the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md).

<a name="hebrews_12_3"></a>Hebrews 12:3

For [analogizomai](../../strongs/g/g357.md) him that [hypomenō](../../strongs/g/g5278.md) such [antilogia](../../strongs/g/g485.md) of [hamartōlos](../../strongs/g/g268.md) against himself, lest ye be [kamnō](../../strongs/g/g2577.md) and [eklyō](../../strongs/g/g1590.md) in your [psychē](../../strongs/g/g5590.md).

<a name="hebrews_12_4"></a>Hebrews 12:4

Ye have not yet [antikathistēmi](../../strongs/g/g478.md) unto [haima](../../strongs/g/g129.md), [antagōnizomai](../../strongs/g/g464.md) against [hamartia](../../strongs/g/g266.md).

<a name="hebrews_12_5"></a>Hebrews 12:5

And ye have [eklanthanomai](../../strongs/g/g1585.md) the [paraklēsis](../../strongs/g/g3874.md) which [dialegomai](../../strongs/g/g1256.md) unto you as unto [huios](../../strongs/g/g5207.md), My [huios](../../strongs/g/g5207.md), [oligōreō](../../strongs/g/g3643.md) not thou the [paideia](../../strongs/g/g3809.md) of the [kyrios](../../strongs/g/g2962.md), nor [eklyō](../../strongs/g/g1590.md) when thou art [elegchō](../../strongs/g/g1651.md) of him:

<a name="hebrews_12_6"></a>Hebrews 12:6

For whom the [kyrios](../../strongs/g/g2962.md) [agapaō](../../strongs/g/g25.md) he [paideuō](../../strongs/g/g3811.md), and [mastigoō](../../strongs/g/g3146.md) every [huios](../../strongs/g/g5207.md) whom he [paradechomai](../../strongs/g/g3858.md).

<a name="hebrews_12_7"></a>Hebrews 12:7

If ye [hypomenō](../../strongs/g/g5278.md) [paideia](../../strongs/g/g3809.md), [theos](../../strongs/g/g2316.md) [prospherō](../../strongs/g/g4374.md) with you as with [huios](../../strongs/g/g5207.md); for what [huios](../../strongs/g/g5207.md) is he whom the [patēr](../../strongs/g/g3962.md) [paideuō](../../strongs/g/g3811.md) not?

<a name="hebrews_12_8"></a>Hebrews 12:8

But if ye be without [paideia](../../strongs/g/g3809.md), whereof all are [metochos](../../strongs/g/g3353.md), then are ye [nothos](../../strongs/g/g3541.md), and not [huios](../../strongs/g/g5207.md).

<a name="hebrews_12_9"></a>Hebrews 12:9

Furthermore we have had [patēr](../../strongs/g/g3962.md) of our [sarx](../../strongs/g/g4561.md) which [paideutēs](../../strongs/g/g3810.md) us, and we gave [entrepō](../../strongs/g/g1788.md): shall we not [polys](../../strongs/g/g4183.md) [mallon](../../strongs/g/g3123.md) be in [hypotassō](../../strongs/g/g5293.md) unto the [patēr](../../strongs/g/g3962.md) of [pneuma](../../strongs/g/g4151.md), and [zaō](../../strongs/g/g2198.md)?

<a name="hebrews_12_10"></a>Hebrews 12:10

For they [men](../../strongs/g/g3303.md) for a [oligos](../../strongs/g/g3641.md) [hēmera](../../strongs/g/g2250.md) [paideuō](../../strongs/g/g3811.md) after their own [dokeō](../../strongs/g/g1380.md); but he for [sympherō](../../strongs/g/g4851.md), that we might be [metalambanō](../../strongs/g/g3335.md) of his [hagiotēs](../../strongs/g/g41.md).

<a name="hebrews_12_11"></a>Hebrews 12:11

Now no [paideia](../../strongs/g/g3809.md) for the [pareimi](../../strongs/g/g3918.md) [dokeō](../../strongs/g/g1380.md) to be [chara](../../strongs/g/g5479.md), but [lypē](../../strongs/g/g3077.md): nevertheless afterward it [apodidōmi](../../strongs/g/g591.md) the [eirēnikos](../../strongs/g/g1516.md) [karpos](../../strongs/g/g2590.md) of [dikaiosynē](../../strongs/g/g1343.md) unto them which are [gymnazō](../../strongs/g/g1128.md) thereby.

<a name="hebrews_12_12"></a>Hebrews 12:12

Wherefore [anorthoō](../../strongs/g/g461.md) the [cheir](../../strongs/g/g5495.md) which [pariēmi](../../strongs/g/g3935.md), and the [paralyō](../../strongs/g/g3886.md) [gony](../../strongs/g/g1119.md);

<a name="hebrews_12_13"></a>Hebrews 12:13

And [poieō](../../strongs/g/g4160.md) [orthos](../../strongs/g/g3717.md) [trochia](../../strongs/g/g5163.md) for your [pous](../../strongs/g/g4228.md), lest that which is [chōlos](../../strongs/g/g5560.md) be [ektrepō](../../strongs/g/g1624.md); but let it rather be [iaomai](../../strongs/g/g2390.md).

<a name="hebrews_12_14"></a>Hebrews 12:14

[diōkō](../../strongs/g/g1377.md) [eirēnē](../../strongs/g/g1515.md) with all men, and [hagiasmos](../../strongs/g/g38.md), without which [oudeis](../../strongs/g/g3762.md) shall [optanomai](../../strongs/g/g3700.md) the [kyrios](../../strongs/g/g2962.md):

<a name="hebrews_12_15"></a>Hebrews 12:15

[episkopeō](../../strongs/g/g1983.md) lest [tis](../../strongs/g/g5100.md) [hystereō](../../strongs/g/g5302.md) of the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md); lest any [rhiza](../../strongs/g/g4491.md) of [pikria](../../strongs/g/g4088.md) [phyō](../../strongs/g/g5453.md) [anō](../../strongs/g/g507.md) [enochleō](../../strongs/g/g1776.md) you, and thereby [polys](../../strongs/g/g4183.md) be [miainō](../../strongs/g/g3392.md);

<a name="hebrews_12_16"></a>Hebrews 12:16

Lest there be any [pornos](../../strongs/g/g4205.md), or [bebēlos](../../strongs/g/g952.md), as [Ēsau](../../strongs/g/g2269.md), who for one [brōsis](../../strongs/g/g1035.md) [apodidōmi](../../strongs/g/g591.md) his [prōtotokia](../../strongs/g/g4415.md).

<a name="hebrews_12_17"></a>Hebrews 12:17

For ye [isēmi](../../strongs/g/g2467.md) how that [metepeita](../../strongs/g/g3347.md), when he would have [klēronomeō](../../strongs/g/g2816.md) the [eulogia](../../strongs/g/g2129.md), he was [apodokimazō](../../strongs/g/g593.md): for he [heuriskō](../../strongs/g/g2147.md) no [topos](../../strongs/g/g5117.md) of [metanoia](../../strongs/g/g3341.md), though he [ekzēteō](../../strongs/g/g1567.md) it with [dakry](../../strongs/g/g1144.md).

<a name="hebrews_12_18"></a>Hebrews 12:18

For ye are not [proserchomai](../../strongs/g/g4334.md) unto the [oros](../../strongs/g/g3735.md) that might be [psēlaphaō](../../strongs/g/g5584.md), and that [kaiō](../../strongs/g/g2545.md) with [pyr](../../strongs/g/g4442.md), nor unto [gnophos](../../strongs/g/g1105.md), and [skotos](../../strongs/g/g4655.md), and [thyella](../../strongs/g/g2366.md),

<a name="hebrews_12_19"></a>Hebrews 12:19

And the [ēchos](../../strongs/g/g2279.md) of a [salpigx](../../strongs/g/g4536.md), and the [phōnē](../../strongs/g/g5456.md) of [rhēma](../../strongs/g/g4487.md); which [akouō](../../strongs/g/g191.md) [paraiteomai](../../strongs/g/g3868.md) that the [logos](../../strongs/g/g3056.md) should not be [prostithēmi](../../strongs/g/g4369.md) to them any more:

<a name="hebrews_12_20"></a>Hebrews 12:20

(For they could not [pherō](../../strongs/g/g5342.md) that which was [diastellō](../../strongs/g/g1291.md), And if so much as a [thērion](../../strongs/g/g2342.md) [thinganō](../../strongs/g/g2345.md) the [oros](../../strongs/g/g3735.md), it shall be [lithoboleō](../../strongs/g/g3036.md), or [katatoxeuō](../../strongs/g/g2700.md) with a [bolis](../../strongs/g/g1002.md): [^1]

<a name="hebrews_12_21"></a>Hebrews 12:21

And so [phoberos](../../strongs/g/g5398.md) was the [phantazō](../../strongs/g/g5324.md), that [Mōÿsēs](../../strongs/g/g3475.md) [eipon](../../strongs/g/g2036.md), I [ekphobos](../../strongs/g/g1630.md) and [entromos](../../strongs/g/g1790.md):)

<a name="hebrews_12_22"></a>Hebrews 12:22

But ye are [proserchomai](../../strongs/g/g4334.md) unto [oros](../../strongs/g/g3735.md) [Siōn](../../strongs/g/g4622.md), and unto the [polis](../../strongs/g/g4172.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), the [epouranios](../../strongs/g/g2032.md) [Ierousalēm](../../strongs/g/g2419.md), and to a [myrias](../../strongs/g/g3461.md) of [aggelos](../../strongs/g/g32.md),

<a name="hebrews_12_23"></a>Hebrews 12:23

To the [panēgyris](../../strongs/g/g3831.md) and [ekklēsia](../../strongs/g/g1577.md) of the [prōtotokos](../../strongs/g/g4416.md), which are [apographō](../../strongs/g/g583.md) in [ouranos](../../strongs/g/g3772.md), and to [theos](../../strongs/g/g2316.md) the [kritēs](../../strongs/g/g2923.md) of all, and to the [pneuma](../../strongs/g/g4151.md) of [dikaios](../../strongs/g/g1342.md) [teleioō](../../strongs/g/g5048.md),

<a name="hebrews_12_24"></a>Hebrews 12:24

And to [Iēsous](../../strongs/g/g2424.md) the [mesitēs](../../strongs/g/g3316.md) of the [neos](../../strongs/g/g3501.md) [diathēkē](../../strongs/g/g1242.md), and to the [haima](../../strongs/g/g129.md) of [rhantismos](../../strongs/g/g4473.md), that [laleō](../../strongs/g/g2980.md) [kreittōn](../../strongs/g/g2909.md) than [Abel](../../strongs/g/g6.md).

<a name="hebrews_12_25"></a>Hebrews 12:25

[blepō](../../strongs/g/g991.md) that ye [paraiteomai](../../strongs/g/g3868.md) not him that [laleō](../../strongs/g/g2980.md). For if they [pheugō](../../strongs/g/g5343.md) not who [paraiteomai](../../strongs/g/g3868.md) him that [chrēmatizō](../../strongs/g/g5537.md) on [gē](../../strongs/g/g1093.md), [polys](../../strongs/g/g4183.md) more we, if we [apostrephō](../../strongs/g/g654.md) from him that from [ouranos](../../strongs/g/g3772.md):

<a name="hebrews_12_26"></a>Hebrews 12:26

Whose [phōnē](../../strongs/g/g5456.md) then [saleuō](../../strongs/g/g4531.md) the [gē](../../strongs/g/g1093.md): but now he hath [epaggellomai](../../strongs/g/g1861.md), [legō](../../strongs/g/g3004.md), Yet [hapax](../../strongs/g/g530.md) I [seiō](../../strongs/g/g4579.md) not the [gē](../../strongs/g/g1093.md) only, but also [ouranos](../../strongs/g/g3772.md).

<a name="hebrews_12_27"></a>Hebrews 12:27

And this, Yet [hapax](../../strongs/g/g530.md), [dēloō](../../strongs/g/g1213.md) the [metathesis](../../strongs/g/g3331.md) of [saleuō](../../strongs/g/g4531.md), as of things that are [poieō](../../strongs/g/g4160.md), that those things which cannot be [saleuō](../../strongs/g/g4531.md) may [menō](../../strongs/g/g3306.md).

<a name="hebrews_12_28"></a>Hebrews 12:28

Wherefore we [paralambanō](../../strongs/g/g3880.md) a [basileia](../../strongs/g/g932.md) which [asaleutos](../../strongs/g/g761.md), let us have [charis](../../strongs/g/g5485.md), whereby we may [latreuō](../../strongs/g/g3000.md) [theos](../../strongs/g/g2316.md) [euarestōs](../../strongs/g/g2102.md) with [aidōs](../../strongs/g/g127.md) and [eulabeia](../../strongs/g/g2124.md):

<a name="hebrews_12_29"></a>Hebrews 12:29

For our [theos](../../strongs/g/g2316.md) a [katanaliskō](../../strongs/g/g2654.md) [pyr](../../strongs/g/g4442.md).

---

[Transliteral Bible](../bible.md)

[Hebrews](hebrews.md)

[Hebrews 11](hebrews_11.md) - [Hebrews 13](hebrews_13.md)

---

[^1]: [Hebrews 12:20 Commentary](../../commentary/hebrews/hebrews_12_commentary.md#hebrews_12_20)
