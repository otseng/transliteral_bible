# [1 Timothy 5](https://www.blueletterbible.org/kjv/1ti/5/1/s_1124001)

<a name="1timothy_5_1"></a>1 Timothy 5:1

[epiplēssō](../../strongs/g/g1969.md) not a [presbyteros](../../strongs/g/g4245.md), but [parakaleō](../../strongs/g/g3870.md) as a [patēr](../../strongs/g/g3962.md); the [neos](../../strongs/g/g3501.md) as [adelphos](../../strongs/g/g80.md);

<a name="1timothy_5_2"></a>1 Timothy 5:2

The [presbyteros](../../strongs/g/g4245.md) as [mētēr](../../strongs/g/g3384.md); the [neos](../../strongs/g/g3501.md) as [adelphē](../../strongs/g/g79.md), with all [hagneia](../../strongs/g/g47.md).

<a name="1timothy_5_3"></a>1 Timothy 5:3

[timaō](../../strongs/g/g5091.md) [chēra](../../strongs/g/g5503.md) that are [chēra](../../strongs/g/g5503.md) [ontōs](../../strongs/g/g3689.md).

<a name="1timothy_5_4"></a>1 Timothy 5:4

But if any [chēra](../../strongs/g/g5503.md) have [teknon](../../strongs/g/g5043.md) or [enkonos](../../strongs/g/g1549.md), let them [manthanō](../../strongs/g/g3129.md) first to [eusebeō](../../strongs/g/g2151.md) at [idios](../../strongs/g/g2398.md) [oikos](../../strongs/g/g3624.md), and to [amoibē](../../strongs/g/g287.md) [apodidōmi](../../strongs/g/g591.md) their [progonos](../../strongs/g/g4269.md): for that is [kalos](../../strongs/g/g2570.md) and [apodektos](../../strongs/g/g587.md) before [theos](../../strongs/g/g2316.md).

<a name="1timothy_5_5"></a>1 Timothy 5:5

Now she that is a [chēra](../../strongs/g/g5503.md) [ontōs](../../strongs/g/g3689.md), and [monoō](../../strongs/g/g3443.md), [elpizō](../../strongs/g/g1679.md) in [theos](../../strongs/g/g2316.md), and [prosmenō](../../strongs/g/g4357.md) in [deēsis](../../strongs/g/g1162.md) and [proseuchē](../../strongs/g/g4335.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md).

<a name="1timothy_5_6"></a>1 Timothy 5:6

But she that [spatalaō](../../strongs/g/g4684.md) is [thnēskō](../../strongs/g/g2348.md) while she [zaō](../../strongs/g/g2198.md).

<a name="1timothy_5_7"></a>1 Timothy 5:7

And these things [paraggellō](../../strongs/g/g3853.md), that they may be [anepilēmptos](../../strongs/g/g423.md).

<a name="1timothy_5_8"></a>1 Timothy 5:8

But if any [pronoeō](../../strongs/g/g4306.md) not for his own, and [malista](../../strongs/g/g3122.md) for those of his own [oikeios](../../strongs/g/g3609.md), he hath [arneomai](../../strongs/g/g720.md) the [pistis](../../strongs/g/g4102.md), and is [cheirōn](../../strongs/g/g5501.md) than an [apistos](../../strongs/g/g571.md).

<a name="1timothy_5_9"></a>1 Timothy 5:9

Let not a [chēra](../../strongs/g/g5503.md) be [katalegō](../../strongs/g/g2639.md) [elassōn](../../strongs/g/g1640.md) threescore [etos](../../strongs/g/g2094.md), having been the [gynē](../../strongs/g/g1135.md) of one [anēr](../../strongs/g/g435.md).

<a name="1timothy_5_10"></a>1 Timothy 5:10

[martyreō](../../strongs/g/g3140.md) of for [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md); if she have [teknotropheō](../../strongs/g/g5044.md), if she have [xenodocheō](../../strongs/g/g3580.md), if she have [niptō](../../strongs/g/g3538.md) the [hagios](../../strongs/g/g40.md) [pous](../../strongs/g/g4228.md), if she have [eparkeō](../../strongs/g/g1884.md) the [thlibō](../../strongs/g/g2346.md), if she have [epakoloutheō](../../strongs/g/g1872.md) every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md).

<a name="1timothy_5_11"></a>1 Timothy 5:11

But the [neos](../../strongs/g/g3501.md) [chēra](../../strongs/g/g5503.md) [paraiteomai](../../strongs/g/g3868.md): for when they have begun to [katastrēniaō](../../strongs/g/g2691.md) [Christos](../../strongs/g/g5547.md), they will [gameō](../../strongs/g/g1060.md);

<a name="1timothy_5_12"></a>1 Timothy 5:12

Having [krima](../../strongs/g/g2917.md), because they have [atheteō](../../strongs/g/g114.md) their [prōtos](../../strongs/g/g4413.md) [pistis](../../strongs/g/g4102.md).

<a name="1timothy_5_13"></a>1 Timothy 5:13

And withal they [manthanō](../../strongs/g/g3129.md) [argos](../../strongs/g/g692.md), [perierchomai](../../strongs/g/g4022.md) [oikia](../../strongs/g/g3614.md); and not only [argos](../../strongs/g/g692.md), but [phlyaros](../../strongs/g/g5397.md) also and [periergos](../../strongs/g/g4021.md), [laleō](../../strongs/g/g2980.md) things which they ought not.

<a name="1timothy_5_14"></a>1 Timothy 5:14

I [boulomai](../../strongs/g/g1014.md) therefore that the [neos](../../strongs/g/g3501.md) [gameō](../../strongs/g/g1060.md), [teknogoneō](../../strongs/g/g5041.md), [oikodespoteō](../../strongs/g/g3616.md), [didōmi](../../strongs/g/g1325.md) [mēdeis](../../strongs/g/g3367.md) [aphormē](../../strongs/g/g874.md) to the [antikeimai](../../strongs/g/g480.md) to [loidoria](../../strongs/g/g3059.md).

<a name="1timothy_5_15"></a>1 Timothy 5:15

For some are already [ektrepō](../../strongs/g/g1624.md) after [Satanas](../../strongs/g/g4567.md).

<a name="1timothy_5_16"></a>1 Timothy 5:16

If any [pistos](../../strongs/g/g4103.md) have [chēra](../../strongs/g/g5503.md), let them [eparkeō](../../strongs/g/g1884.md) them, and let not the [ekklēsia](../../strongs/g/g1577.md) [bareō](../../strongs/g/g916.md); that it may [eparkeō](../../strongs/g/g1884.md) them that are [chēra](../../strongs/g/g5503.md) [ontōs](../../strongs/g/g3689.md).

<a name="1timothy_5_17"></a>1 Timothy 5:17

Let the [presbyteros](../../strongs/g/g4245.md) that [proistēmi](../../strongs/g/g4291.md) [kalōs](../../strongs/g/g2573.md) be [axioō](../../strongs/g/g515.md) of [diplous](../../strongs/g/g1362.md) [timē](../../strongs/g/g5092.md), [malista](../../strongs/g/g3122.md) they who [kopiaō](../../strongs/g/g2872.md) in the [logos](../../strongs/g/g3056.md) and [didaskalia](../../strongs/g/g1319.md).

<a name="1timothy_5_18"></a>1 Timothy 5:18

For the [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md), Thou shalt not [phimoō](../../strongs/g/g5392.md) the [bous](../../strongs/g/g1016.md) that [aloaō](../../strongs/g/g248.md). And, The [ergatēs](../../strongs/g/g2040.md) is [axios](../../strongs/g/g514.md) of his [misthos](../../strongs/g/g3408.md).

<a name="1timothy_5_19"></a>1 Timothy 5:19

Against a [presbyteros](../../strongs/g/g4245.md) [paradechomai](../../strongs/g/g3858.md) not a [katēgoria](../../strongs/g/g2724.md), but before two or three [martys](../../strongs/g/g3144.md).

<a name="1timothy_5_20"></a>1 Timothy 5:20

Them that [hamartanō](../../strongs/g/g264.md) [elegchō](../../strongs/g/g1651.md) before all, that [loipos](../../strongs/g/g3062.md) also may [phobos](../../strongs/g/g5401.md).

<a name="1timothy_5_21"></a>1 Timothy 5:21

I [diamartyromai](../../strongs/g/g1263.md) before [theos](../../strongs/g/g2316.md), and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and the [eklektos](../../strongs/g/g1588.md) [aggelos](../../strongs/g/g32.md), that thou [phylassō](../../strongs/g/g5442.md) these things without [prokrima](../../strongs/g/g4299.md), [poieō](../../strongs/g/g4160.md) [mēdeis](../../strongs/g/g3367.md) by [prosklisis](../../strongs/g/g4346.md).

<a name="1timothy_5_22"></a>1 Timothy 5:22

[epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) [tacheōs](../../strongs/g/g5030.md) on [mēdeis](../../strongs/g/g3367.md), neither be [koinōneō](../../strongs/g/g2841.md) of [allotrios](../../strongs/g/g245.md) [hamartia](../../strongs/g/g266.md): [tēreō](../../strongs/g/g5083.md) thyself [hagnos](../../strongs/g/g53.md).

<a name="1timothy_5_23"></a>1 Timothy 5:23

[hydropoteō](../../strongs/g/g5202.md) no longer, but [chraomai](../../strongs/g/g5530.md) [oligos](../../strongs/g/g3641.md) [oinos](../../strongs/g/g3631.md) for thy [stomachos](../../strongs/g/g4751.md) sake and thine [pyknos](../../strongs/g/g4437.md) [astheneia](../../strongs/g/g769.md).

<a name="1timothy_5_24"></a>1 Timothy 5:24

Some [anthrōpos](../../strongs/g/g444.md) [hamartia](../../strongs/g/g266.md) are [prodēlos](../../strongs/g/g4271.md), [proagō](../../strongs/g/g4254.md) to [krisis](../../strongs/g/g2920.md); and some they [epakoloutheō](../../strongs/g/g1872.md).

<a name="1timothy_5_25"></a>1 Timothy 5:25

[hōsautōs](../../strongs/g/g5615.md) also the [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) are [prodēlos](../../strongs/g/g4271.md); and they that are [allōs](../../strongs/g/g247.md) cannot be [kryptō](../../strongs/g/g2928.md).

---

[Transliteral Bible](../bible.md)

[1 Timothy](1timothy.md)

[1 Timothy 4](1timothy_4.md) - [1 Timothy 6](1timothy_6.md)