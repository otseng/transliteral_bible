# [1 Timothy 2](https://www.blueletterbible.org/kjv/1ti/2/1/s_1121001)

<a name="1timothy_2_1"></a>1 Timothy 2:1

I [parakaleō](../../strongs/g/g3870.md) therefore, that, [prōton](../../strongs/g/g4412.md) of all, [deēsis](../../strongs/g/g1162.md), [proseuchē](../../strongs/g/g4335.md), [enteuxis](../../strongs/g/g1783.md), [eucharistia](../../strongs/g/g2169.md), be [poieō](../../strongs/g/g4160.md) for all [anthrōpos](../../strongs/g/g444.md);

<a name="1timothy_2_2"></a>1 Timothy 2:2

For [basileus](../../strongs/g/g935.md), and all that are in [hyperochē](../../strongs/g/g5247.md); that we may [diagō](../../strongs/g/g1236.md) an [ēremos](../../strongs/g/g2263.md) and [hēsychios](../../strongs/g/g2272.md) [bios](../../strongs/g/g979.md) in all [eusebeia](../../strongs/g/g2150.md) and [semnotēs](../../strongs/g/g4587.md).

<a name="1timothy_2_3"></a>1 Timothy 2:3

For this [kalos](../../strongs/g/g2570.md) and [apodektos](../../strongs/g/g587.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md);

<a name="1timothy_2_4"></a>1 Timothy 2:4

Who will have all [anthrōpos](../../strongs/g/g444.md) to be [sōzō](../../strongs/g/g4982.md), and to [erchomai](../../strongs/g/g2064.md) unto the [epignōsis](../../strongs/g/g1922.md) of the [alētheia](../../strongs/g/g225.md).

<a name="1timothy_2_5"></a>1 Timothy 2:5

For one [theos](../../strongs/g/g2316.md), and one [mesitēs](../../strongs/g/g3316.md) between [theos](../../strongs/g/g2316.md) and [anthrōpos](../../strongs/g/g444.md), the [anthrōpos](../../strongs/g/g444.md) [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md);

<a name="1timothy_2_6"></a>1 Timothy 2:6

Who [didōmi](../../strongs/g/g1325.md) himself an [antilytron](../../strongs/g/g487.md) for all, to be [martyrion](../../strongs/g/g3142.md) in [idios](../../strongs/g/g2398.md) [kairos](../../strongs/g/g2540.md).

<a name="1timothy_2_7"></a>1 Timothy 2:7

Whereunto I am [tithēmi](../../strongs/g/g5087.md) a [kēryx](../../strongs/g/g2783.md), and an [apostolos](../../strongs/g/g652.md), (I [legō](../../strongs/g/g3004.md) the [alētheia](../../strongs/g/g225.md) in [Christos](../../strongs/g/g5547.md), [pseudomai](../../strongs/g/g5574.md) not;) a [didaskalos](../../strongs/g/g1320.md) of the [ethnos](../../strongs/g/g1484.md) in [pistis](../../strongs/g/g4102.md) and [alētheia](../../strongs/g/g225.md).

<a name="1timothy_2_8"></a>1 Timothy 2:8

I [boulomai](../../strongs/g/g1014.md) therefore that [anēr](../../strongs/g/g435.md) [proseuchomai](../../strongs/g/g4336.md) every [topos](../../strongs/g/g5117.md), [epairō](../../strongs/g/g1869.md) up [hosios](../../strongs/g/g3741.md) [cheir](../../strongs/g/g5495.md), without [orgē](../../strongs/g/g3709.md) and [dialogismos](../../strongs/g/g1261.md).

<a name="1timothy_2_9"></a>1 Timothy 2:9

[hōsautōs](../../strongs/g/g5615.md) also, that [gynē](../../strongs/g/g1135.md) [kosmeō](../../strongs/g/g2885.md) themselves in [kosmios](../../strongs/g/g2887.md) [katastolē](../../strongs/g/g2689.md), with [aidōs](../../strongs/g/g127.md) and [sōphrosynē](../../strongs/g/g4997.md); not with [plegma](../../strongs/g/g4117.md), or [chrysos](../../strongs/g/g5557.md), or [margaritēs](../../strongs/g/g3135.md), or [polytelēs](../../strongs/g/g4185.md) [himatismos](../../strongs/g/g2441.md);

<a name="1timothy_2_10"></a>1 Timothy 2:10

But (which becometh [gynē](../../strongs/g/g1135.md) [epaggellomai](../../strongs/g/g1861.md) [theosebeia](../../strongs/g/g2317.md)) with [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md).

<a name="1timothy_2_11"></a>1 Timothy 2:11

Let the [gynē](../../strongs/g/g1135.md) [manthanō](../../strongs/g/g3129.md) in [hēsychia](../../strongs/g/g2271.md) with all [hypotagē](../../strongs/g/g5292.md).

<a name="1timothy_2_12"></a>1 Timothy 2:12

But I [epitrepō](../../strongs/g/g2010.md) not a [gynē](../../strongs/g/g1135.md) to [didaskō](../../strongs/g/g1321.md), nor to [authenteō](../../strongs/g/g831.md) over the [anēr](../../strongs/g/g435.md), but to be in [hēsychia](../../strongs/g/g2271.md). [^1]

<a name="1timothy_2_13"></a>1 Timothy 2:13

For [Adam](../../strongs/g/g76.md) was first [plassō](../../strongs/g/g4111.md), then [heya](../../strongs/g/g2096.md).

<a name="1timothy_2_14"></a>1 Timothy 2:14

And [Adam](../../strongs/g/g76.md) was not [apataō](../../strongs/g/g538.md), but the [gynē](../../strongs/g/g1135.md) being [apataō](../../strongs/g/g538.md) was in the [parabasis](../../strongs/g/g3847.md).

<a name="1timothy_2_15"></a>1 Timothy 2:15

Notwithstanding she shall be [sōzō](../../strongs/g/g4982.md) in [teknogonia](../../strongs/g/g5042.md), if they [menō](../../strongs/g/g3306.md) in [pistis](../../strongs/g/g4102.md) and [agapē](../../strongs/g/g26.md) and [hagiasmos](../../strongs/g/g38.md) with [sōphrosynē](../../strongs/g/g4997.md).

---

[Transliteral Bible](../bible.md)

[1 Timothy](1timothy.md)

[1 Timothy 1](1timothy_1.md) - [1 Timothy 3](1timothy_3.md)

---

[^1]: [1 Timothy 2:12 Commentary](../../commentary/1timothy/1timothy_2_commentary.md#1timothy_2_12)
