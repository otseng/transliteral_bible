# [1 Timothy 3](https://www.blueletterbible.org/kjv/1ti/3/1/s_1122001)

<a name="1timothy_3_1"></a>1 Timothy 3:1

This a [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md), [ei tis](../../strongs/g/g1536.md) [oregō](../../strongs/g/g3713.md) an [episkopē](../../strongs/g/g1984.md), he [epithymeō](../../strongs/g/g1937.md) a [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md).

<a name="1timothy_3_2"></a>1 Timothy 3:2

An [episkopos](../../strongs/g/g1985.md) then must be [anepilēmptos](../../strongs/g/g423.md), the [anēr](../../strongs/g/g435.md) of one [gynē](../../strongs/g/g1135.md), [nēphalios](../../strongs/g/g3524.md), [sōphrōn](../../strongs/g/g4998.md), of [kosmios](../../strongs/g/g2887.md), [philoxenos](../../strongs/g/g5382.md), [didaktikos](../../strongs/g/g1317.md);

<a name="1timothy_3_3"></a>1 Timothy 3:3

Not [paroinos](../../strongs/g/g3943.md), no [plēktēs](../../strongs/g/g4131.md), not [aischrokerdēs](../../strongs/g/g146.md); but [epieikēs](../../strongs/g/g1933.md), [amachos](../../strongs/g/g269.md), [aphilargyros](../../strongs/g/g866.md);

<a name="1timothy_3_4"></a>1 Timothy 3:4

One that [proistēmi](../../strongs/g/g4291.md) [kalōs](../../strongs/g/g2573.md) his own [oikos](../../strongs/g/g3624.md), having his [teknon](../../strongs/g/g5043.md) in [hypotagē](../../strongs/g/g5292.md) with all [semnotēs](../../strongs/g/g4587.md);

<a name="1timothy_3_5"></a>1 Timothy 3:5

(For [ei tis](../../strongs/g/g1536.md) [eidō](../../strongs/g/g1492.md) not how to [proistēmi](../../strongs/g/g4291.md) his own [oikos](../../strongs/g/g3624.md), how shall he [epimeleomai](../../strongs/g/g1959.md) of the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md)?)

<a name="1timothy_3_6"></a>1 Timothy 3:6

Not a [neophytos](../../strongs/g/g3504.md), lest being [typhoō](../../strongs/g/g5187.md) he [empiptō](../../strongs/g/g1706.md) into the [krima](../../strongs/g/g2917.md) of the [diabolos](../../strongs/g/g1228.md).

<a name="1timothy_3_7"></a>1 Timothy 3:7

Moreover he must have a [kalos](../../strongs/g/g2570.md) [martyria](../../strongs/g/g3141.md) of them which are [exōthen](../../strongs/g/g1855.md); lest he [empiptō](../../strongs/g/g1706.md) into [oneidismos](../../strongs/g/g3680.md) and the [pagis](../../strongs/g/g3803.md) of the [diabolos](../../strongs/g/g1228.md).

<a name="1timothy_3_8"></a>1 Timothy 3:8

[hōsautōs](../../strongs/g/g5615.md) the [diakonos](../../strongs/g/g1249.md) be [semnos](../../strongs/g/g4586.md), not [dilogos](../../strongs/g/g1351.md), not [prosechō](../../strongs/g/g4337.md) to [polys](../../strongs/g/g4183.md) [oinos](../../strongs/g/g3631.md), not [aischrokerdēs](../../strongs/g/g146.md);

<a name="1timothy_3_9"></a>1 Timothy 3:9

[echō](../../strongs/g/g2192.md) the [mystērion](../../strongs/g/g3466.md) of the [pistis](../../strongs/g/g4102.md) in a [katharos](../../strongs/g/g2513.md) [syneidēsis](../../strongs/g/g4893.md).

<a name="1timothy_3_10"></a>1 Timothy 3:10

And let these also first be [dokimazō](../../strongs/g/g1381.md); then let them [diakoneō](../../strongs/g/g1247.md), being [anegklētos](../../strongs/g/g410.md).

<a name="1timothy_3_11"></a>1 Timothy 3:11

[hōsautōs](../../strongs/g/g5615.md) [gynē](../../strongs/g/g1135.md) [semnos](../../strongs/g/g4586.md), not [diabolos](../../strongs/g/g1228.md), [nēphalios](../../strongs/g/g3524.md), [pistos](../../strongs/g/g4103.md) in all things.

<a name="1timothy_3_12"></a>1 Timothy 3:12

Let the [diakonos](../../strongs/g/g1249.md) be the [anēr](../../strongs/g/g435.md) of one [gynē](../../strongs/g/g1135.md), [proistēmi](../../strongs/g/g4291.md) their [teknon](../../strongs/g/g5043.md) and their own [oikos](../../strongs/g/g3624.md) [kalōs](../../strongs/g/g2573.md).

<a name="1timothy_3_13"></a>1 Timothy 3:13

For they that have [diakoneō](../../strongs/g/g1247.md) [kalōs](../../strongs/g/g2573.md) [peripoieō](../../strongs/g/g4046.md) to themselves a [kalos](../../strongs/g/g2570.md) [bathmos](../../strongs/g/g898.md), and [polys](../../strongs/g/g4183.md) [parrēsia](../../strongs/g/g3954.md) in the [pistis](../../strongs/g/g4102.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="1timothy_3_14"></a>1 Timothy 3:14

These things [graphō](../../strongs/g/g1125.md) I unto thee, [elpizō](../../strongs/g/g1679.md) to [erchomai](../../strongs/g/g2064.md) unto thee [tachion](../../strongs/g/g5032.md):

<a name="1timothy_3_15"></a>1 Timothy 3:15

But if I [bradynō](../../strongs/g/g1019.md) long, that thou mayest [eidō](../../strongs/g/g1492.md) how thou oughtest to [anastrephō](../../strongs/g/g390.md) thyself in the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md), which is the [ekklēsia](../../strongs/g/g1577.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), the [stylos](../../strongs/g/g4769.md) and [hedraiōma](../../strongs/g/g1477.md) of the [alētheia](../../strongs/g/g225.md).

<a name="1timothy_3_16"></a>1 Timothy 3:16

And [homologoumenōs](../../strongs/g/g3672.md) [megas](../../strongs/g/g3173.md) is the [mystērion](../../strongs/g/g3466.md) of [eusebeia](../../strongs/g/g2150.md): [theos](../../strongs/g/g2316.md) was [phaneroō](../../strongs/g/g5319.md) in the [sarx](../../strongs/g/g4561.md), [dikaioō](../../strongs/g/g1344.md) in the [pneuma](../../strongs/g/g4151.md), [optanomai](../../strongs/g/g3700.md) of [aggelos](../../strongs/g/g32.md), [kēryssō](../../strongs/g/g2784.md) unto the [ethnos](../../strongs/g/g1484.md), [pisteuō](../../strongs/g/g4100.md) on in the [kosmos](../../strongs/g/g2889.md), [analambanō](../../strongs/g/g353.md) into [doxa](../../strongs/g/g1391.md). [^1]

---

[Transliteral Bible](../bible.md)

[1 Timothy](1timothy.md)

[1 Timothy 2](1timothy_2.md) - [1 Timothy 4](1timothy_4.md)

---

[^1]: [1 Timothy 3:16 Commentary](../../commentary/1timothy/1timothy_3_commentary.md#1timothy_3_16)
