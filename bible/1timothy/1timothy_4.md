# [1 Timothy 4](https://www.blueletterbible.org/kjv/1ti/4/1/s_1123001)

<a name="1timothy_4_1"></a>1 Timothy 4:1

Now the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) [rhētōs](../../strongs/g/g4490.md), that in the [ysteros](../../strongs/g/g5306.md) [kairos](../../strongs/g/g2540.md) some shall [aphistēmi](../../strongs/g/g868.md) from the [pistis](../../strongs/g/g4102.md), [prosechō](../../strongs/g/g4337.md) to [planos](../../strongs/g/g4108.md) [pneuma](../../strongs/g/g4151.md), and [didaskalia](../../strongs/g/g1319.md) of [daimonion](../../strongs/g/g1140.md);

<a name="1timothy_4_2"></a>1 Timothy 4:2

[pseudologos](../../strongs/g/g5573.md) in [hypokrisis](../../strongs/g/g5272.md); having their [syneidēsis](../../strongs/g/g4893.md) [kaustēriazō](../../strongs/g/g2743.md);

<a name="1timothy_4_3"></a>1 Timothy 4:3

[kōlyō](../../strongs/g/g2967.md) to [gameō](../../strongs/g/g1060.md), to [apechomai](../../strongs/g/g567.md) from [brōma](../../strongs/g/g1033.md), which [theos](../../strongs/g/g2316.md) hath [ktizō](../../strongs/g/g2936.md) to be [metalēmpsis](../../strongs/g/g3336.md) with [eucharistia](../../strongs/g/g2169.md) of them which [pistos](../../strongs/g/g4103.md) and [epiginōskō](../../strongs/g/g1921.md) the [alētheia](../../strongs/g/g225.md).

<a name="1timothy_4_4"></a>1 Timothy 4:4

For every [ktisma](../../strongs/g/g2938.md) of [theos](../../strongs/g/g2316.md) [kalos](../../strongs/g/g2570.md), and [oudeis](../../strongs/g/g3762.md) to be [apoblētos](../../strongs/g/g579.md), if it be [lambanō](../../strongs/g/g2983.md) with [eucharistia](../../strongs/g/g2169.md):

<a name="1timothy_4_5"></a>1 Timothy 4:5

For it is [hagiazō](../../strongs/g/g37.md) by the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) and [enteuxis](../../strongs/g/g1783.md).

<a name="1timothy_4_6"></a>1 Timothy 4:6

If thou [hypotithēmi](../../strongs/g/g5294.md) the [adelphos](../../strongs/g/g80.md) of these things, thou shalt be a [kalos](../../strongs/g/g2570.md) [diakonos](../../strongs/g/g1249.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [entrephō](../../strongs/g/g1789.md) in the [logos](../../strongs/g/g3056.md) of [pistis](../../strongs/g/g4102.md) and of [kalos](../../strongs/g/g2570.md) [didaskalia](../../strongs/g/g1319.md), whereunto thou hast [parakoloutheō](../../strongs/g/g3877.md).

<a name="1timothy_4_7"></a>1 Timothy 4:7

But [paraiteomai](../../strongs/g/g3868.md) [bebēlos](../../strongs/g/g952.md) and [graōdēs](../../strongs/g/g1126.md) [mythos](../../strongs/g/g3454.md), and [gymnazō](../../strongs/g/g1128.md) thyself unto [eusebeia](../../strongs/g/g2150.md).

<a name="1timothy_4_8"></a>1 Timothy 4:8

For [sōmatikos](../../strongs/g/g4984.md) [gymnasia](../../strongs/g/g1129.md) [ōphelimos](../../strongs/g/g5624.md) [oligos](../../strongs/g/g3641.md): but [eusebeia](../../strongs/g/g2150.md) is [ōphelimos](../../strongs/g/g5624.md) unto all things, having [epaggelia](../../strongs/g/g1860.md) of the [zōē](../../strongs/g/g2222.md) that [nyn](../../strongs/g/g3568.md) is, and of [mellō](../../strongs/g/g3195.md).

<a name="1timothy_4_9"></a>1 Timothy 4:9

This a [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md) and [axios](../../strongs/g/g514.md) of all [apodochē](../../strongs/g/g594.md).

<a name="1timothy_4_10"></a>1 Timothy 4:10

For therefore we both [kopiaō](../../strongs/g/g2872.md) and [oneidizō](../../strongs/g/g3679.md), because we [elpizō](../../strongs/g/g1679.md) in the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), who is the [sōtēr](../../strongs/g/g4990.md) of all [anthrōpos](../../strongs/g/g444.md), [malista](../../strongs/g/g3122.md) of those that [pistos](../../strongs/g/g4103.md).

<a name="1timothy_4_11"></a>1 Timothy 4:11

These things [paraggellō](../../strongs/g/g3853.md) and [didaskō](../../strongs/g/g1321.md).

<a name="1timothy_4_12"></a>1 Timothy 4:12

Let [mēdeis](../../strongs/g/g3367.md) [kataphroneō](../../strongs/g/g2706.md) thy [neotēs](../../strongs/g/g3503.md); but be thou a [typos](../../strongs/g/g5179.md) of the [pistos](../../strongs/g/g4103.md), in [logos](../../strongs/g/g3056.md), in [anastrophē](../../strongs/g/g391.md), in [agapē](../../strongs/g/g26.md), in [pneuma](../../strongs/g/g4151.md), in [pistis](../../strongs/g/g4102.md), in [hagneia](../../strongs/g/g47.md).

<a name="1timothy_4_13"></a>1 Timothy 4:13

Till I [erchomai](../../strongs/g/g2064.md), [prosechō](../../strongs/g/g4337.md) to [anagnōsis](../../strongs/g/g320.md), to [paraklēsis](../../strongs/g/g3874.md), to [didaskalia](../../strongs/g/g1319.md).

<a name="1timothy_4_14"></a>1 Timothy 4:14

[ameleō](../../strongs/g/g272.md) not the [charisma](../../strongs/g/g5486.md) that is in thee, which was [didōmi](../../strongs/g/g1325.md) thee by [prophēteia](../../strongs/g/g4394.md), with the [epithesis](../../strongs/g/g1936.md) of the [cheir](../../strongs/g/g5495.md) of the [presbyterion](../../strongs/g/g4244.md).

<a name="1timothy_4_15"></a>1 Timothy 4:15

[meletaō](../../strongs/g/g3191.md) these things; [isthi](../../strongs/g/g2468.md) [en](../../strongs/g/g1722.md) to them; that thy [prokopē](../../strongs/g/g4297.md) may [phaneros](../../strongs/g/g5318.md) to all.

<a name="1timothy_4_16"></a>1 Timothy 4:16

[epechō](../../strongs/g/g1907.md) unto thyself, and unto the [didaskalia](../../strongs/g/g1319.md); [epimenō](../../strongs/g/g1961.md) in them: for in [poieō](../../strongs/g/g4160.md) this thou shalt both [sōzō](../../strongs/g/g4982.md) thyself, and them that [akouō](../../strongs/g/g191.md) thee.

---

[Transliteral Bible](../bible.md)

[1Timothy](1timothy.md)

[1Timothy 3](1timothy_3.md) - [1Timothy 5](1timothy_5.md)