# [1 Timothy 6](https://www.blueletterbible.org/kjv/1ti/6/1/s_1125001)

<a name="1timothy_6_1"></a>1 Timothy 6:1

Let as many [doulos](../../strongs/g/g1401.md) as are under the [zygos](../../strongs/g/g2218.md) [hēgeomai](../../strongs/g/g2233.md) their own [despotēs](../../strongs/g/g1203.md) [axios](../../strongs/g/g514.md) of all [timē](../../strongs/g/g5092.md), that the [onoma](../../strongs/g/g3686.md) of [theos](../../strongs/g/g2316.md) and [didaskalia](../../strongs/g/g1319.md) be not [blasphēmeō](../../strongs/g/g987.md).

<a name="1timothy_6_2"></a>1 Timothy 6:2

And they that have [pistos](../../strongs/g/g4103.md) [despotēs](../../strongs/g/g1203.md), let them not [kataphroneō](../../strongs/g/g2706.md), because they are [adelphos](../../strongs/g/g80.md); but rather do [douleuō](../../strongs/g/g1398.md), because they are [pistos](../../strongs/g/g4103.md) and [agapētos](../../strongs/g/g27.md), [antilambanō](../../strongs/g/g482.md) of the [euergesia](../../strongs/g/g2108.md). These things [didaskō](../../strongs/g/g1321.md) and [parakaleō](../../strongs/g/g3870.md).

<a name="1timothy_6_3"></a>1 Timothy 6:3

If any man [heterodidaskaleō](../../strongs/g/g2085.md), and [proserchomai](../../strongs/g/g4334.md) not to [hygiainō](../../strongs/g/g5198.md) [logos](../../strongs/g/g3056.md), [hēmōn](../../strongs/g/g2257.md) our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and to the [didaskalia](../../strongs/g/g1319.md) which is according to [eusebeia](../../strongs/g/g2150.md);

<a name="1timothy_6_4"></a>1 Timothy 6:4

He is [typhoō](../../strongs/g/g5187.md), [epistamai](../../strongs/g/g1987.md) [mēdeis](../../strongs/g/g3367.md), but [noseō](../../strongs/g/g3552.md) about [zētēsis](../../strongs/g/g2214.md) and [logomachia](../../strongs/g/g3055.md), whereof [ginomai](../../strongs/g/g1096.md) [phthonos](../../strongs/g/g5355.md), [eris](../../strongs/g/g2054.md), [blasphēmia](../../strongs/g/g988.md), [ponēros](../../strongs/g/g4190.md) [hyponoia](../../strongs/g/g5283.md),

<a name="1timothy_6_5"></a>1 Timothy 6:5

[diaparatribē](../../strongs/g/g3859.md) of [anthrōpos](../../strongs/g/g444.md) of [diaphtheirō](../../strongs/g/g1311.md) [nous](../../strongs/g/g3563.md), and [apostereō](../../strongs/g/g650.md) of the [alētheia](../../strongs/g/g225.md), [nomizō](../../strongs/g/g3543.md) that [porismos](../../strongs/g/g4200.md) is [eusebeia](../../strongs/g/g2150.md): from such [aphistēmi](../../strongs/g/g868.md). [^1]

<a name="1timothy_6_6"></a>1 Timothy 6:6

But [eusebeia](../../strongs/g/g2150.md) with [autarkeia](../../strongs/g/g841.md) is [megas](../../strongs/g/g3173.md) [porismos](../../strongs/g/g4200.md).

<a name="1timothy_6_7"></a>1 Timothy 6:7

For we [eispherō](../../strongs/g/g1533.md) [oudeis](../../strongs/g/g3762.md) into [kosmos](../../strongs/g/g2889.md), [dēlos](../../strongs/g/g1212.md) we can [ekpherō](../../strongs/g/g1627.md) nothing out.

<a name="1timothy_6_8"></a>1 Timothy 6:8

And having [diatrophē](../../strongs/g/g1305.md) and [skepasma](../../strongs/g/g4629.md) let us be therewith [arkeō](../../strongs/g/g714.md).

<a name="1timothy_6_9"></a>1 Timothy 6:9

But they that [boulomai](../../strongs/g/g1014.md) be [plouteō](../../strongs/g/g4147.md) [empiptō](../../strongs/g/g1706.md) into [peirasmos](../../strongs/g/g3986.md) and a [pagis](../../strongs/g/g3803.md), and [polys](../../strongs/g/g4183.md) [anoētos](../../strongs/g/g453.md) and [blaberos](../../strongs/g/g983.md) [epithymia](../../strongs/g/g1939.md), which [bythizō](../../strongs/g/g1036.md) [anthrōpos](../../strongs/g/g444.md) in [olethros](../../strongs/g/g3639.md) and [apōleia](../../strongs/g/g684.md).

<a name="1timothy_6_10"></a>1 Timothy 6:10

For [philargyria](../../strongs/g/g5365.md) is the [rhiza](../../strongs/g/g4491.md) of [pas](../../strongs/g/g3956.md) [kakos](../../strongs/g/g2556.md): which while some [oregō](../../strongs/g/g3713.md), they have [apoplanaō](../../strongs/g/g635.md) from the [pistis](../../strongs/g/g4102.md), and [peripeirō](../../strongs/g/g4044.md) themselves through with [polys](../../strongs/g/g4183.md) [odynē](../../strongs/g/g3601.md).

<a name="1timothy_6_11"></a>1 Timothy 6:11

But thou, [ō](../../strongs/g/g5599.md) [anthrōpos](../../strongs/g/g444.md) of [theos](../../strongs/g/g2316.md), [pheugō](../../strongs/g/g5343.md) these things; and [diōkō](../../strongs/g/g1377.md) after [dikaiosynē](../../strongs/g/g1343.md), [eusebeia](../../strongs/g/g2150.md), [pistis](../../strongs/g/g4102.md), [agapē](../../strongs/g/g26.md), [hypomonē](../../strongs/g/g5281.md), [praotēs](../../strongs/g/g4236.md).

<a name="1timothy_6_12"></a>1 Timothy 6:12

[agōnizomai](../../strongs/g/g75.md) the [kalos](../../strongs/g/g2570.md) [agōn](../../strongs/g/g73.md) of [pistis](../../strongs/g/g4102.md), [epilambanomai](../../strongs/g/g1949.md) on [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), whereunto thou art also [kaleō](../../strongs/g/g2564.md), and hast [homologeō](../../strongs/g/g3670.md) a [kalos](../../strongs/g/g2570.md) [homologia](../../strongs/g/g3671.md) before [polys](../../strongs/g/g4183.md) [martys](../../strongs/g/g3144.md).

<a name="1timothy_6_13"></a>1 Timothy 6:13

I [paraggellō](../../strongs/g/g3853.md) thee in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md), who [zōopoieō](../../strongs/g/g2227.md) all things, and before [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), who [Pontios](../../strongs/g/g4194.md) [Pilatos](../../strongs/g/g4091.md) [martyreō](../../strongs/g/g3140.md) a [kalos](../../strongs/g/g2570.md) [homologia](../../strongs/g/g3671.md);

<a name="1timothy_6_14"></a>1 Timothy 6:14

That thou [tēreō](../../strongs/g/g5083.md) [entolē](../../strongs/g/g1785.md) [aspilos](../../strongs/g/g784.md), [anepilēmptos](../../strongs/g/g423.md), until the [epiphaneia](../../strongs/g/g2015.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="1timothy_6_15"></a>1 Timothy 6:15

Which in his [kairos](../../strongs/g/g2540.md) he shall [deiknyō](../../strongs/g/g1166.md), the [makarios](../../strongs/g/g3107.md) and only [dynastēs](../../strongs/g/g1413.md), the [basileus](../../strongs/g/g935.md) of [basileuō](../../strongs/g/g936.md), and [kyrios](../../strongs/g/g2962.md) of [kyrieuō](../../strongs/g/g2961.md);

<a name="1timothy_6_16"></a>1 Timothy 6:16

Who only hath [athanasia](../../strongs/g/g110.md), [oikeō](../../strongs/g/g3611.md) in the [phōs](../../strongs/g/g5457.md) which [aprositos](../../strongs/g/g676.md); whom no [anthrōpos](../../strongs/g/g444.md) hath [eidō](../../strongs/g/g1492.md), nor can [eidō](../../strongs/g/g1492.md): to whom [timē](../../strongs/g/g5092.md) and [kratos](../../strongs/g/g2904.md) [aiōnios](../../strongs/g/g166.md). [amēn](../../strongs/g/g281.md).

<a name="1timothy_6_17"></a>1 Timothy 6:17

[paraggellō](../../strongs/g/g3853.md) them that are [plousios](../../strongs/g/g4145.md) in this [aiōn](../../strongs/g/g165.md), that they be not [hypsēlophroneō](../../strongs/g/g5309.md), nor [elpizō](../../strongs/g/g1679.md) in [adēlotēs](../../strongs/g/g83.md) [ploutos](../../strongs/g/g4149.md), but in the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), who [parechō](../../strongs/g/g3930.md) us [plousiōs](../../strongs/g/g4146.md) all things to [apolausis](../../strongs/g/g619.md);

<a name="1timothy_6_18"></a>1 Timothy 6:18

That they [agathoergeō](../../strongs/g/g14.md), that they be [plouteō](../../strongs/g/g4147.md) in [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md), ready to [eumetadotos](../../strongs/g/g2130.md), willing to [koinōnikos](../../strongs/g/g2843.md);

<a name="1timothy_6_19"></a>1 Timothy 6:19

[apothēsaurizō](../../strongs/g/g597.md) for themselves a [kalos](../../strongs/g/g2570.md) [themelios](../../strongs/g/g2310.md) against the [mellō](../../strongs/g/g3195.md), that they may [epilambanomai](../../strongs/g/g1949.md) on [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="1timothy_6_20"></a>1 Timothy 6:20

[ō](../../strongs/g/g5599.md) [Timotheos](../../strongs/g/g5095.md), [phylassō](../../strongs/g/g5442.md) that which is [parakatathēkē](../../strongs/g/g3872.md), [ektrepō](../../strongs/g/g1624.md) [bebēlos](../../strongs/g/g952.md) [kenophōnia](../../strongs/g/g2757.md), and [antithesis](../../strongs/g/g477.md) of [gnōsis](../../strongs/g/g1108.md) [pseudōnymos](../../strongs/g/g5581.md):

<a name="1timothy_6_21"></a>1 Timothy 6:21

Which some [epaggellomai](../../strongs/g/g1861.md) have [astocheō](../../strongs/g/g795.md) concerning the [pistis](../../strongs/g/g4102.md). [charis](../../strongs/g/g5485.md) with thee. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[1 Timothy](1timothy.md)

[1 Timothy 5](1timothy_5.md)

---

[^1]: [1 Timothy 6:5 Commentary](../../commentary/1timothy/1timothy_6_commentary.md#1timothy_6_5)
