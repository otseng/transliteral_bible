# 1 Timothy

[1 Timothy Overview](../../commentary/1timothy/1timothy_overview.md)

[1 Timothy 1](1timothy_1.md)

[1 Timothy 2](1timothy_2.md)

[1 Timothy 3](1timothy_3.md)

[1 Timothy 4](1timothy_4.md)

[1 Timothy 5](1timothy_5.md)

[1 Timothy 6](1timothy_6.md)

---

[Transliteral Bible](../index.md)
