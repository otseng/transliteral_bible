# [1 Timothy 1](https://www.blueletterbible.org/kjv/1ti/1/1/s_1120001)

<a name="1timothy_1_1"></a>1 Timothy 1:1

[Paulos](../../strongs/g/g3972.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) by the [epitagē](../../strongs/g/g2003.md) of [theos](../../strongs/g/g2316.md) our [sōtēr](../../strongs/g/g4990.md), and [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), our [elpis](../../strongs/g/g1680.md);

<a name="1timothy_1_2"></a>1 Timothy 1:2

Unto [Timotheos](../../strongs/g/g5095.md), [gnēsios](../../strongs/g/g1103.md) [teknon](../../strongs/g/g5043.md) in the [pistis](../../strongs/g/g4102.md): [charis](../../strongs/g/g5485.md), [eleos](../../strongs/g/g1656.md), [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md) and [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md).

<a name="1timothy_1_3"></a>1 Timothy 1:3

As I [parakaleō](../../strongs/g/g3870.md) thee to [prosmenō](../../strongs/g/g4357.md) still at [Ephesos](../../strongs/g/g2181.md), when I [poreuō](../../strongs/g/g4198.md) into [Makedonia](../../strongs/g/g3109.md), that thou [paraggellō](../../strongs/g/g3853.md) some that they no [heterodidaskaleō](../../strongs/g/g2085.md),

<a name="1timothy_1_4"></a>1 Timothy 1:4

Neither [prosechō](../../strongs/g/g4337.md) to [mythos](../../strongs/g/g3454.md) and [aperantos](../../strongs/g/g562.md) [genealogia](../../strongs/g/g1076.md), which [parechō](../../strongs/g/g3930.md) [zētēsis](../../strongs/g/g2214.md), rather than [theos](../../strongs/g/g2316.md) [oikodomia](../../strongs/g/g3620.md) which is in [pistis](../../strongs/g/g4102.md).

<a name="1timothy_1_5"></a>1 Timothy 1:5

Now the [telos](../../strongs/g/g5056.md) of the [paraggelia](../../strongs/g/g3852.md) is [agapē](../../strongs/g/g26.md) out of a [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md), and an [agathos](../../strongs/g/g18.md) [syneidēsis](../../strongs/g/g4893.md), and of [pistis](../../strongs/g/g4102.md) [anypokritos](../../strongs/g/g505.md):

<a name="1timothy_1_6"></a>1 Timothy 1:6

From which some having [astocheō](../../strongs/g/g795.md) have [ektrepō](../../strongs/g/g1624.md) unto [mataiologia](../../strongs/g/g3150.md);

<a name="1timothy_1_7"></a>1 Timothy 1:7

[thelō](../../strongs/g/g2309.md) to be [nomodidaskalos](../../strongs/g/g3547.md); [noeō](../../strongs/g/g3539.md) neither what they [legō](../../strongs/g/g3004.md), nor whereof they [diabebaioomai](../../strongs/g/g1226.md).

<a name="1timothy_1_8"></a>1 Timothy 1:8

But we [eidō](../../strongs/g/g1492.md) that the [nomos](../../strongs/g/g3551.md) [kalos](../../strongs/g/g2570.md), if [tis](../../strongs/g/g5100.md) [chraomai](../../strongs/g/g5530.md) it [nomimōs](../../strongs/g/g3545.md);

<a name="1timothy_1_9"></a>1 Timothy 1:9

[eidō](../../strongs/g/g1492.md) this, that the [nomos](../../strongs/g/g3551.md) is not [keimai](../../strongs/g/g2749.md) for a [dikaios](../../strongs/g/g1342.md), but for the [anomos](../../strongs/g/g459.md) and [anypotaktos](../../strongs/g/g506.md), for the [asebēs](../../strongs/g/g765.md) and for [hamartōlos](../../strongs/g/g268.md), for [anosios](../../strongs/g/g462.md) and [bebēlos](../../strongs/g/g952.md), for [patrolōas](../../strongs/g/g3964.md) and [mētrolōas](../../strongs/g/g3389.md), for [androphonos](../../strongs/g/g409.md),

<a name="1timothy_1_10"></a>1 Timothy 1:10

For [pornos](../../strongs/g/g4205.md), for them that [arsenokoitēs](../../strongs/g/g733.md), for [andrapodistēs](../../strongs/g/g405.md), for [pseustēs](../../strongs/g/g5583.md), for [epiorkos](../../strongs/g/g1965.md), and if there be any [heteros](../../strongs/g/g2087.md) that is [antikeimai](../../strongs/g/g480.md) to [hygiainō](../../strongs/g/g5198.md) [didaskalia](../../strongs/g/g1319.md);

<a name="1timothy_1_11"></a>1 Timothy 1:11

According to the [doxa](../../strongs/g/g1391.md) [euaggelion](../../strongs/g/g2098.md) of the [makarios](../../strongs/g/g3107.md) [theos](../../strongs/g/g2316.md), which was [pisteuō](../../strongs/g/g4100.md) me.

<a name="1timothy_1_12"></a>1 Timothy 1:12

And I [charis](../../strongs/g/g5485.md) [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md), who hath [endynamoō](../../strongs/g/g1743.md) me, for that he [hēgeomai](../../strongs/g/g2233.md) me [pistos](../../strongs/g/g4103.md), [tithēmi](../../strongs/g/g5087.md) me into the [diakonia](../../strongs/g/g1248.md);

<a name="1timothy_1_13"></a>1 Timothy 1:13

Who was before a [blasphēmos](../../strongs/g/g989.md), and a [diōktēs](../../strongs/g/g1376.md), and [hybristes](../../strongs/g/g5197.md): but I obtained [eleeō](../../strongs/g/g1653.md), because I [poieō](../../strongs/g/g4160.md) [agnoeō](../../strongs/g/g50.md) in [apistia](../../strongs/g/g570.md).

<a name="1timothy_1_14"></a>1 Timothy 1:14

And the [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) was [hyperpleonazō](../../strongs/g/g5250.md) with [pistis](../../strongs/g/g4102.md) and [agapē](../../strongs/g/g26.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="1timothy_1_15"></a>1 Timothy 1:15

This a [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md), and [axios](../../strongs/g/g514.md) of all [apodochē](../../strongs/g/g594.md), that [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) into the [kosmos](../../strongs/g/g2889.md) to [sōzō](../../strongs/g/g4982.md) [hamartōlos](../../strongs/g/g268.md); of whom I am [prōtos](../../strongs/g/g4413.md).

<a name="1timothy_1_16"></a>1 Timothy 1:16

Howbeit for this cause I [eleeō](../../strongs/g/g1653.md), that in me first [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) might [endeiknymi](../../strongs/g/g1731.md) all [makrothymia](../../strongs/g/g3115.md), for a [hypotypōsis](../../strongs/g/g5296.md) to them which should hereafter [pisteuō](../../strongs/g/g4100.md) on him to [zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md).

<a name="1timothy_1_17"></a>1 Timothy 1:17

Now unto the [basileus](../../strongs/g/g935.md) [aiōn](../../strongs/g/g165.md), [aphthartos](../../strongs/g/g862.md), [aoratos](../../strongs/g/g517.md), the only [sophos](../../strongs/g/g4680.md) [theos](../../strongs/g/g2316.md), [timē](../../strongs/g/g5092.md) and [doxa](../../strongs/g/g1391.md) for [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="1timothy_1_18"></a>1 Timothy 1:18

This [paraggelia](../../strongs/g/g3852.md) I [paratithēmi](../../strongs/g/g3908.md) unto thee, [teknon](../../strongs/g/g5043.md) [Timotheos](../../strongs/g/g5095.md), according to the [prophēteia](../../strongs/g/g4394.md) which [proagō](../../strongs/g/g4254.md) on thee, that thou by them mightest [strateuō](../../strongs/g/g4754.md) a [kalos](../../strongs/g/g2570.md) [strateia](../../strongs/g/g4752.md);

<a name="1timothy_1_19"></a>1 Timothy 1:19

Holding [pistis](../../strongs/g/g4102.md), and an [agathos](../../strongs/g/g18.md) [syneidēsis](../../strongs/g/g4893.md); which some having [apōtheō](../../strongs/g/g683.md) concerning [pistis](../../strongs/g/g4102.md) have [nauageō](../../strongs/g/g3489.md):

<a name="1timothy_1_20"></a>1 Timothy 1:20

Of whom is [hymenaios](../../strongs/g/g5211.md) and [Alexandros](../../strongs/g/g223.md); whom I have [paradidōmi](../../strongs/g/g3860.md) unto [Satanas](../../strongs/g/g4567.md), that they may [paideuō](../../strongs/g/g3811.md) not to [blasphēmeō](../../strongs/g/g987.md).

---

[Transliteral Bible](../bible.md)

[1 Timothy](1timothy.md)

[1 Timothy 2](1timothy_2.md)