# [Acts 5](https://www.blueletterbible.org/kjv/act/5/1/s_1023001)

<a name="acts_5_1"></a>Acts 5:1

But a [tis](../../strongs/g/g5100.md) [anēr](../../strongs/g/g435.md) [onoma](../../strongs/g/g3686.md) [Hananias](../../strongs/g/g367.md), with [Sapphira](../../strongs/g/g4551.md) his [gynē](../../strongs/g/g1135.md), [pōleō](../../strongs/g/g4453.md) a [ktēma](../../strongs/g/g2933.md),

<a name="acts_5_2"></a>Acts 5:2

And [nosphizō](../../strongs/g/g3557.md) of the [timē](../../strongs/g/g5092.md), his [gynē](../../strongs/g/g1135.md) also [synoraō](../../strongs/g/g4894.md), and [pherō](../../strongs/g/g5342.md) a certain [meros](../../strongs/g/g3313.md), and [tithēmi](../../strongs/g/g5087.md) it at the [apostolos](../../strongs/g/g652.md) [pous](../../strongs/g/g4228.md).

<a name="acts_5_3"></a>Acts 5:3

But [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [Hananias](../../strongs/g/g367.md), why hath [Satanas](../../strongs/g/g4567.md) [plēroō](../../strongs/g/g4137.md) thine [kardia](../../strongs/g/g2588.md) to [pseudomai](../../strongs/g/g5574.md) to the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and to [nosphizō](../../strongs/g/g3557.md) of the [timē](../../strongs/g/g5092.md) of the [chōrion](../../strongs/g/g5564.md)?

<a name="acts_5_4"></a>Acts 5:4

Whiles it [menō](../../strongs/g/g3306.md), was it not [soi](../../strongs/g/g4671.md) [menō](../../strongs/g/g3306.md)? and after it was [pipraskō](../../strongs/g/g4097.md), was it not in thine own [exousia](../../strongs/g/g1849.md)? why hast thou [tithēmi](../../strongs/g/g5087.md) this [pragma](../../strongs/g/g4229.md) in thine [kardia](../../strongs/g/g2588.md)?  thou hast not [pseudomai](../../strongs/g/g5574.md) unto [anthrōpos](../../strongs/g/g444.md), but unto [theos](../../strongs/g/g2316.md).

<a name="acts_5_5"></a>Acts 5:5

And [Hananias](../../strongs/g/g367.md) [akouō](../../strongs/g/g191.md) these [logos](../../strongs/g/g3056.md) [piptō](../../strongs/g/g4098.md), and [ekpsychō](../../strongs/g/g1634.md): and [megas](../../strongs/g/g3173.md) [phobos](../../strongs/g/g5401.md) [ginomai](../../strongs/g/g1096.md) on all them that [akouō](../../strongs/g/g191.md) these things.

<a name="acts_5_6"></a>Acts 5:6

And the [neos](../../strongs/g/g3501.md) [anistēmi](../../strongs/g/g450.md), [systellō](../../strongs/g/g4958.md) him, and [ekpherō](../../strongs/g/g1627.md), and [thaptō](../../strongs/g/g2290.md).

<a name="acts_5_7"></a>Acts 5:7

And it was about the [diastēma](../../strongs/g/g1292.md) of three [hōra](../../strongs/g/g5610.md) after, when his [gynē](../../strongs/g/g1135.md), not [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md), [eiserchomai](../../strongs/g/g1525.md).

<a name="acts_5_8"></a>Acts 5:8

And [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) unto her, [eipon](../../strongs/g/g2036.md) me whether ye [apodidōmi](../../strongs/g/g591.md) the [chōrion](../../strongs/g/g5564.md) [tosoutos](../../strongs/g/g5118.md)? And she [eipon](../../strongs/g/g2036.md), [nai](../../strongs/g/g3483.md), [tosoutos](../../strongs/g/g5118.md).

<a name="acts_5_9"></a>Acts 5:9

Then [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md) unto her, How is it that ye have [symphōneō](../../strongs/g/g4856.md) to [peirazō](../../strongs/g/g3985.md) the [pneuma](../../strongs/g/g4151.md) of the [kyrios](../../strongs/g/g2962.md)? [idou](../../strongs/g/g2400.md), the [pous](../../strongs/g/g4228.md) of them which have [thaptō](../../strongs/g/g2290.md) thy [anēr](../../strongs/g/g435.md) are at the [thyra](../../strongs/g/g2374.md), and shall [ekpherō](../../strongs/g/g1627.md) thee.

<a name="acts_5_10"></a>Acts 5:10

Then [piptō](../../strongs/g/g4098.md) she [parachrēma](../../strongs/g/g3916.md) at his [pous](../../strongs/g/g4228.md), and [ekpsychō](../../strongs/g/g1634.md): and the [neaniskos](../../strongs/g/g3495.md) [eiserchomai](../../strongs/g/g1525.md), and [heuriskō](../../strongs/g/g2147.md) her [nekros](../../strongs/g/g3498.md), and, [ekpherō](../../strongs/g/g1627.md), [thaptō](../../strongs/g/g2290.md) her by her [anēr](../../strongs/g/g435.md).

<a name="acts_5_11"></a>Acts 5:11

And [megas](../../strongs/g/g3173.md) [phobos](../../strongs/g/g5401.md) [ginomai](../../strongs/g/g1096.md) upon all the [ekklēsia](../../strongs/g/g1577.md), and upon [pas](../../strongs/g/g3956.md) [akouō](../../strongs/g/g191.md) these things.

<a name="acts_5_12"></a>Acts 5:12

And by the [cheir](../../strongs/g/g5495.md) of the [apostolos](../../strongs/g/g652.md) were [polys](../../strongs/g/g4183.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md) [ginomai](../../strongs/g/g1096.md) among the [laos](../../strongs/g/g2992.md); (and they were all [homothymadon](../../strongs/g/g3661.md) in [Solomōn](../../strongs/g/g4672.md) [stoa](../../strongs/g/g4745.md).

<a name="acts_5_13"></a>Acts 5:13

And of the [loipos](../../strongs/g/g3062.md) [tolmaō](../../strongs/g/g5111.md) no man [kollaō](../../strongs/g/g2853.md) to them: but the [laos](../../strongs/g/g2992.md) [megalynō](../../strongs/g/g3170.md) them.

<a name="acts_5_14"></a>Acts 5:14

And [pisteuō](../../strongs/g/g4100.md) were the more [prostithēmi](../../strongs/g/g4369.md) to the [kyrios](../../strongs/g/g2962.md), [plēthos](../../strongs/g/g4128.md) both of [anēr](../../strongs/g/g435.md) and [gynē](../../strongs/g/g1135.md).)

<a name="acts_5_15"></a>Acts 5:15

Insomuch that they [ekpherō](../../strongs/g/g1627.md) the [asthenēs](../../strongs/g/g772.md) into the [plateia](../../strongs/g/g4113.md), and [tithēmi](../../strongs/g/g5087.md) on [klinē](../../strongs/g/g2825.md) and [krabattos](../../strongs/g/g2895.md), that at the least the [skia](../../strongs/g/g4639.md) of [Petros](../../strongs/g/g4074.md) [erchomai](../../strongs/g/g2064.md) might [episkiazō](../../strongs/g/g1982.md) some of them.

<a name="acts_5_16"></a>Acts 5:16

There [synerchomai](../../strongs/g/g4905.md) also a [plēthos](../../strongs/g/g4128.md) of the [polis](../../strongs/g/g4172.md) [perix](../../strongs/g/g4038.md) unto [Ierousalēm](../../strongs/g/g2419.md), [pherō](../../strongs/g/g5342.md) [asthenēs](../../strongs/g/g772.md), and them which were [ochleō](../../strongs/g/g3791.md) with [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md): and they were [therapeuō](../../strongs/g/g2323.md) every one.

<a name="acts_5_17"></a>Acts 5:17

Then the [archiereus](../../strongs/g/g749.md) [anistēmi](../../strongs/g/g450.md), and all they that were with him, (which is the [hairesis](../../strongs/g/g139.md) of the [Saddoukaios](../../strongs/g/g4523.md),) and were [pimplēmi](../../strongs/g/g4130.md) with [zēlos](../../strongs/g/g2205.md),

<a name="acts_5_18"></a>Acts 5:18

And [epiballō](../../strongs/g/g1911.md) their [cheir](../../strongs/g/g5495.md) on the [apostolos](../../strongs/g/g652.md), and [tithēmi](../../strongs/g/g5087.md) them in the [dēmosios](../../strongs/g/g1219.md) [tērēsis](../../strongs/g/g5084.md).

<a name="acts_5_19"></a>Acts 5:19

But the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) by [nyx](../../strongs/g/g3571.md) [anoigō](../../strongs/g/g455.md) the [phylakē](../../strongs/g/g5438.md) [thyra](../../strongs/g/g2374.md), and [exagō](../../strongs/g/g1806.md) them, and [eipon](../../strongs/g/g2036.md),

<a name="acts_5_20"></a>Acts 5:20

[poreuō](../../strongs/g/g4198.md), [histēmi](../../strongs/g/g2476.md) and [laleō](../../strongs/g/g2980.md) in the [hieron](../../strongs/g/g2411.md) to the [laos](../../strongs/g/g2992.md) all the [rhēma](../../strongs/g/g4487.md) of this [zōē](../../strongs/g/g2222.md).

<a name="acts_5_21"></a>Acts 5:21

And when they [akouō](../../strongs/g/g191.md), they [eiserchomai](../../strongs/g/g1525.md) into the [hieron](../../strongs/g/g2411.md) [hypo](../../strongs/g/g5259.md) [orthros](../../strongs/g/g3722.md), and [didaskō](../../strongs/g/g1321.md). But the [archiereus](../../strongs/g/g749.md) [paraginomai](../../strongs/g/g3854.md), and they that were with him, and [sygkaleō](../../strongs/g/g4779.md) the [synedrion](../../strongs/g/g4892.md), and all the [gerousia](../../strongs/g/g1087.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md), and [apostellō](../../strongs/g/g649.md) to the [desmōtērion](../../strongs/g/g1201.md) to [agō](../../strongs/g/g71.md) them.

<a name="acts_5_22"></a>Acts 5:22

But when the [hypēretēs](../../strongs/g/g5257.md) [paraginomai](../../strongs/g/g3854.md), and [heuriskō](../../strongs/g/g2147.md) them not in the [phylakē](../../strongs/g/g5438.md), they [anastrephō](../../strongs/g/g390.md) and [apaggellō](../../strongs/g/g518.md),

<a name="acts_5_23"></a>Acts 5:23

[legō](../../strongs/g/g3004.md), the [desmōtērion](../../strongs/g/g1201.md) [men](../../strongs/g/g3303.md) [heuriskō](../../strongs/g/g2147.md) we [kleiō](../../strongs/g/g2808.md) with all [asphaleia](../../strongs/g/g803.md), and the [phylax](../../strongs/g/g5441.md) [histēmi](../../strongs/g/g2476.md) without before the [thyra](../../strongs/g/g2374.md): but when we had [anoigō](../../strongs/g/g455.md), we [heuriskō](../../strongs/g/g2147.md) [oudeis](../../strongs/g/g3762.md) within.

<a name="acts_5_24"></a>Acts 5:24

Now when the [hiereus](../../strongs/g/g2409.md) and the [stratēgos](../../strongs/g/g4755.md) of the [hieron](../../strongs/g/g2411.md) and the [archiereus](../../strongs/g/g749.md) [akouō](../../strongs/g/g191.md) these [logos](../../strongs/g/g3056.md), they [diaporeō](../../strongs/g/g1280.md) of them whereunto this would [ginomai](../../strongs/g/g1096.md).

<a name="acts_5_25"></a>Acts 5:25

Then [paraginomai](../../strongs/g/g3854.md) one and [apaggellō](../../strongs/g/g518.md) them, [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), the [anēr](../../strongs/g/g435.md) whom ye [tithēmi](../../strongs/g/g5087.md) in [phylakē](../../strongs/g/g5438.md) are [histēmi](../../strongs/g/g2476.md) in the [hieron](../../strongs/g/g2411.md), and [didaskō](../../strongs/g/g1321.md) the [laos](../../strongs/g/g2992.md).

<a name="acts_5_26"></a>Acts 5:26

Then [aperchomai](../../strongs/g/g565.md) the [stratēgos](../../strongs/g/g4755.md) with the [hypēretēs](../../strongs/g/g5257.md), and [agō](../../strongs/g/g71.md) them without [bia](../../strongs/g/g970.md): for they [phobeō](../../strongs/g/g5399.md) the [laos](../../strongs/g/g2992.md), lest they should have been [lithazō](../../strongs/g/g3034.md).

<a name="acts_5_27"></a>Acts 5:27

And when they had [agō](../../strongs/g/g71.md) them, they [histēmi](../../strongs/g/g2476.md) before the [synedrion](../../strongs/g/g4892.md): and the [archiereus](../../strongs/g/g749.md) [eperōtaō](../../strongs/g/g1905.md) them,

<a name="acts_5_28"></a>Acts 5:28

[legō](../../strongs/g/g3004.md), Did not we [paraggelia](../../strongs/g/g3852.md) [paraggellō](../../strongs/g/g3853.md) you that ye should not [didaskō](../../strongs/g/g1321.md) in this [onoma](../../strongs/g/g3686.md)? and, [idou](../../strongs/g/g2400.md), ye have [plēroō](../../strongs/g/g4137.md) [Ierousalēm](../../strongs/g/g2419.md) with your [didachē](../../strongs/g/g1322.md), and [boulomai](../../strongs/g/g1014.md) to [epagō](../../strongs/g/g1863.md) this [anthrōpos](../../strongs/g/g444.md) [haima](../../strongs/g/g129.md) upon us.

<a name="acts_5_29"></a>Acts 5:29

Then [Petros](../../strongs/g/g4074.md) and the [apostolos](../../strongs/g/g652.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), We ought to [peitharcheō](../../strongs/g/g3980.md) [theos](../../strongs/g/g2316.md) rather than [anthrōpos](../../strongs/g/g444.md).

<a name="acts_5_30"></a>Acts 5:30

The [theos](../../strongs/g/g2316.md) of our [patēr](../../strongs/g/g3962.md) [egeirō](../../strongs/g/g1453.md) [Iēsous](../../strongs/g/g2424.md), whom ye [diacheirizō](../../strongs/g/g1315.md) and [kremannymi](../../strongs/g/g2910.md) on a [xylon](../../strongs/g/g3586.md).

<a name="acts_5_31"></a>Acts 5:31

Him hath [theos](../../strongs/g/g2316.md) [hypsoō](../../strongs/g/g5312.md) with his [dexios](../../strongs/g/g1188.md) an [archēgos](../../strongs/g/g747.md) and a [sōtēr](../../strongs/g/g4990.md), for to [didōmi](../../strongs/g/g1325.md) [metanoia](../../strongs/g/g3341.md) to [Israēl](../../strongs/g/g2474.md), and [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md).

<a name="acts_5_32"></a>Acts 5:32

And we are his [martys](../../strongs/g/g3144.md) of these [rhēma](../../strongs/g/g4487.md); and also the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), whom [theos](../../strongs/g/g2316.md) hath [didōmi](../../strongs/g/g1325.md) to them that [peitharcheō](../../strongs/g/g3980.md) him.

<a name="acts_5_33"></a>Acts 5:33

When they [akouō](../../strongs/g/g191.md), they were [diapriō](../../strongs/g/g1282.md), and [bouleuō](../../strongs/g/g1011.md) to [anaireō](../../strongs/g/g337.md) them.

<a name="acts_5_34"></a>Acts 5:34

Then [anistēmi](../../strongs/g/g450.md) one in the [synedrion](../../strongs/g/g4892.md), a [Pharisaios](../../strongs/g/g5330.md), [onoma](../../strongs/g/g3686.md) [Gamaliēl](../../strongs/g/g1059.md), a [nomodidaskalos](../../strongs/g/g3547.md), [timios](../../strongs/g/g5093.md) among all the [laos](../../strongs/g/g2992.md), and [keleuō](../../strongs/g/g2753.md) to [poieō](../../strongs/g/g4160.md) the [apostolos](../../strongs/g/g652.md) forth [brachys](../../strongs/g/g1024.md);

<a name="acts_5_35"></a>Acts 5:35

And [eipon](../../strongs/g/g2036.md) unto them, Ye [anēr](../../strongs/g/g435.md) [Israēlitēs](../../strongs/g/g2475.md), [prosechō](../../strongs/g/g4337.md) to yourselves what ye [mellō](../../strongs/g/g3195.md) to [prassō](../../strongs/g/g4238.md) as [epi](../../strongs/g/g1909.md) these [anthrōpos](../../strongs/g/g444.md).

<a name="acts_5_36"></a>Acts 5:36

For before these [hēmera](../../strongs/g/g2250.md) [anistēmi](../../strongs/g/g450.md) [Theudas](../../strongs/g/g2333.md), [legō](../../strongs/g/g3004.md) himself to be somebody; to whom an [arithmos](../../strongs/g/g706.md) of [anēr](../../strongs/g/g435.md), about four hundred, [proskollaō](../../strongs/g/g4347.md): who was [anaireō](../../strongs/g/g337.md); and all, as many as [peithō](../../strongs/g/g3982.md) him, were [dialyō](../../strongs/g/g1262.md), and [ginomai](../../strongs/g/g1096.md) to [oudeis](../../strongs/g/g3762.md).

<a name="acts_5_37"></a>Acts 5:37

After [touton](../../strongs/g/g5126.md) [anistēmi](../../strongs/g/g450.md) [Ioudas](../../strongs/g/g2455.md) [Galilaios](../../strongs/g/g1057.md) in the [hēmera](../../strongs/g/g2250.md) of the [apographē](../../strongs/g/g582.md), and [aphistēmi](../../strongs/g/g868.md) [hikanos](../../strongs/g/g2425.md) [laos](../../strongs/g/g2992.md) after him: he also [apollymi](../../strongs/g/g622.md); and all, as many as [peithō](../../strongs/g/g3982.md) him, were [diaskorpizō](../../strongs/g/g1287.md).

<a name="acts_5_38"></a>Acts 5:38

And [ta nyn](../../strongs/g/g3569.md) I [legō](../../strongs/g/g3004.md) unto you, [aphistēmi](../../strongs/g/g868.md) from these [anthrōpos](../../strongs/g/g444.md), and [eaō](../../strongs/g/g1439.md) them: for if this [boulē](../../strongs/g/g1012.md) or this [ergon](../../strongs/g/g2041.md) be of [anthrōpos](../../strongs/g/g444.md), it will [katalyō](../../strongs/g/g2647.md):

<a name="acts_5_39"></a>Acts 5:39

But if it be of [theos](../../strongs/g/g2316.md), ye cannot [katalyō](../../strongs/g/g2647.md) it; [mēpote](../../strongs/g/g3379.md) ye be [heuriskō](../../strongs/g/g2147.md) even to [theomachos](../../strongs/g/g2314.md).

<a name="acts_5_40"></a>Acts 5:40

And to him they [peithō](../../strongs/g/g3982.md): and when they had [proskaleō](../../strongs/g/g4341.md) the [apostolos](../../strongs/g/g652.md), and [derō](../../strongs/g/g1194.md) them, they [paraggellō](../../strongs/g/g3853.md) that they should not [laleō](../../strongs/g/g2980.md) in the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md), and [apolyō](../../strongs/g/g630.md) them.

<a name="acts_5_41"></a>Acts 5:41

And they [poreuō](../../strongs/g/g4198.md) from the [prosōpon](../../strongs/g/g4383.md) of the [synedrion](../../strongs/g/g4892.md), [chairō](../../strongs/g/g5463.md) that they were [kataxioō](../../strongs/g/g2661.md) to [atimazō](../../strongs/g/g818.md) for his [onoma](../../strongs/g/g3686.md).

<a name="acts_5_42"></a>Acts 5:42

And [hēmera](../../strongs/g/g2250.md) [pas](../../strongs/g/g3956.md) in the [hieron](../../strongs/g/g2411.md), and in every [oikos](../../strongs/g/g3624.md), they [pauō](../../strongs/g/g3973.md) not to [didaskō](../../strongs/g/g1321.md) and [euaggelizō](../../strongs/g/g2097.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 4](acts_4.md) - [Acts 6](acts_6.md)