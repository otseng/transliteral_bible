# [Acts 20](https://www.blueletterbible.org/kjv/act/20/1/s_1038001)

<a name="acts_20_1"></a>Acts 20:1

And after the [thorybos](../../strongs/g/g2351.md) was [pauō](../../strongs/g/g3973.md), [Paulos](../../strongs/g/g3972.md) [proskaleō](../../strongs/g/g4341.md) unto him the [mathētēs](../../strongs/g/g3101.md), and [aspazomai](../../strongs/g/g782.md) them, and [exerchomai](../../strongs/g/g1831.md) for to [poreuō](../../strongs/g/g4198.md) into [Makedonia](../../strongs/g/g3109.md).

<a name="acts_20_2"></a>Acts 20:2

And when he had [dierchomai](../../strongs/g/g1330.md) those [meros](../../strongs/g/g3313.md), and had [polys](../../strongs/g/g4183.md) them [parakaleō](../../strongs/g/g3870.md) [logos](../../strongs/g/g3056.md), he [erchomai](../../strongs/g/g2064.md) into [Hellas](../../strongs/g/g1671.md),

<a name="acts_20_3"></a>Acts 20:3

And there [poieō](../../strongs/g/g4160.md) three [mēn](../../strongs/g/g3376.md). And when the [Ioudaios](../../strongs/g/g2453.md) [ginomai](../../strongs/g/g1096.md) [epiboulē](../../strongs/g/g1917.md) for him, as he was about to [anagō](../../strongs/g/g321.md) into [Syria](../../strongs/g/g4947.md), he [ginomai](../../strongs/g/g1096.md) [gnōmē](../../strongs/g/g1106.md) to [hypostrephō](../../strongs/g/g5290.md) through [Makedonia](../../strongs/g/g3109.md).

<a name="acts_20_4"></a>Acts 20:4

And there [synepomai](../../strongs/g/g4902.md) him into [Asia](../../strongs/g/g773.md) [Sōpatros](../../strongs/g/g4986.md) [Beroiaios](../../strongs/g/g961.md); and [Thessalonikeus](../../strongs/g/g2331.md), [Aristarchos](../../strongs/g/g708.md) and [Sekoundos](../../strongs/g/g4580.md); and [Gaïos](../../strongs/g/g1050.md) [Derbaios](../../strongs/g/g1190.md), and [Timotheos](../../strongs/g/g5095.md); and [Asianos](../../strongs/g/g774.md), [Tychikos](../../strongs/g/g5190.md) and [Trophimos](../../strongs/g/g5161.md).

<a name="acts_20_5"></a>Acts 20:5

These [proerchomai](../../strongs/g/g4281.md) [menō](../../strongs/g/g3306.md) for us at [Trōas](../../strongs/g/g5174.md).

<a name="acts_20_6"></a>Acts 20:6

And we [ekpleō](../../strongs/g/g1602.md) from [Philippoi](../../strongs/g/g5375.md) after the [hēmera](../../strongs/g/g2250.md) of [azymos](../../strongs/g/g106.md), and [erchomai](../../strongs/g/g2064.md) unto them to [Trōas](../../strongs/g/g5174.md) in five [hēmera](../../strongs/g/g2250.md); where we [diatribō](../../strongs/g/g1304.md) seven days.

<a name="acts_20_7"></a>Acts 20:7

And upon the first of the [sabbaton](../../strongs/g/g4521.md), when the [mathētēs](../../strongs/g/g3101.md) [synagō](../../strongs/g/g4863.md) to [klaō](../../strongs/g/g2806.md) [artos](../../strongs/g/g740.md), [Paulos](../../strongs/g/g3972.md) [dialegomai](../../strongs/g/g1256.md) unto them, [mellō](../../strongs/g/g3195.md) to [exeimi](../../strongs/g/g1826.md) on the [epaurion](../../strongs/g/g1887.md); and [parateinō](../../strongs/g/g3905.md) his [logos](../../strongs/g/g3056.md) until [mesonyktion](../../strongs/g/g3317.md).

<a name="acts_20_8"></a>Acts 20:8

And there were [hikanos](../../strongs/g/g2425.md) [lampas](../../strongs/g/g2985.md) in the [hyperōon](../../strongs/g/g5253.md), where they were [synagō](../../strongs/g/g4863.md).

<a name="acts_20_9"></a>Acts 20:9

And there [kathēmai](../../strongs/g/g2521.md) in a [thyris](../../strongs/g/g2376.md) a certain [neanias](../../strongs/g/g3494.md) [onoma](../../strongs/g/g3686.md) [eutychos](../../strongs/g/g2161.md), being [katapherō](../../strongs/g/g2702.md) into a [bathys](../../strongs/g/g901.md) [hypnos](../../strongs/g/g5258.md): and as [Paulos](../../strongs/g/g3972.md) was long [dialegomai](../../strongs/g/g1256.md), he [katapherō](../../strongs/g/g2702.md) with [hypnos](../../strongs/g/g5258.md), and [katō](../../strongs/g/g2736.md) [piptō](../../strongs/g/g4098.md) from the [tristegon](../../strongs/g/g5152.md), and was [airō](../../strongs/g/g142.md) [nekros](../../strongs/g/g3498.md).

<a name="acts_20_10"></a>Acts 20:10

And [Paulos](../../strongs/g/g3972.md) [katabainō](../../strongs/g/g2597.md), and [epipiptō](../../strongs/g/g1968.md) on him, and [symperilambanō](../../strongs/g/g4843.md) him [eipon](../../strongs/g/g2036.md), [thorybeō](../../strongs/g/g2350.md) not; for his [psychē](../../strongs/g/g5590.md) is in him.

<a name="acts_20_11"></a>Acts 20:11

When he therefore was [anabainō](../../strongs/g/g305.md), and had [klaō](../../strongs/g/g2806.md) [artos](../../strongs/g/g740.md), and [geuomai](../../strongs/g/g1089.md), and [homileō](../../strongs/g/g3656.md) a [hikanos](../../strongs/g/g2425.md), even till [augē](../../strongs/g/g827.md), so he [exerchomai](../../strongs/g/g1831.md).

<a name="acts_20_12"></a>Acts 20:12

And they [agō](../../strongs/g/g71.md) the [pais](../../strongs/g/g3816.md) [zaō](../../strongs/g/g2198.md), and were not a [metriōs](../../strongs/g/g3357.md) [parakaleō](../../strongs/g/g3870.md).

<a name="acts_20_13"></a>Acts 20:13

And we [proerchomai](../../strongs/g/g4281.md) to [ploion](../../strongs/g/g4143.md), and [anagō](../../strongs/g/g321.md) unto [Assos](../../strongs/g/g789.md), there [mellō](../../strongs/g/g3195.md) to [analambanō](../../strongs/g/g353.md) [Paulos](../../strongs/g/g3972.md): for so had he [diatassō](../../strongs/g/g1299.md), [mellō](../../strongs/g/g3195.md) himself to [pezeuō](../../strongs/g/g3978.md).

<a name="acts_20_14"></a>Acts 20:14

And when he [symballō](../../strongs/g/g4820.md) with us at [Assos](../../strongs/g/g789.md), we [analambanō](../../strongs/g/g353.md) him, and [erchomai](../../strongs/g/g2064.md) to [Mitylēnē](../../strongs/g/g3412.md).

<a name="acts_20_15"></a>Acts 20:15

And we [apopleō](../../strongs/g/g636.md) thence, and [katantaō](../../strongs/g/g2658.md) the [epeimi](../../strongs/g/g1966.md) [antikry](../../strongs/g/g481.md) [Chios](../../strongs/g/g5508.md); and the [heteros](../../strongs/g/g2087.md) we [paraballō](../../strongs/g/g3846.md) at [Samos](../../strongs/g/g4544.md), and [menō](../../strongs/g/g3306.md) at [Trōgyllion](../../strongs/g/g5175.md); and the [echō](../../strongs/g/g2192.md) we [erchomai](../../strongs/g/g2064.md) to [Milētos](../../strongs/g/g3399.md).

<a name="acts_20_16"></a>Acts 20:16

For [Paulos](../../strongs/g/g3972.md) had [krinō](../../strongs/g/g2919.md) to [parapleō](../../strongs/g/g3896.md) [Ephesos](../../strongs/g/g2181.md), because he would not [chronotribeō](../../strongs/g/g5551.md) in [Asia](../../strongs/g/g773.md): for he [speudō](../../strongs/g/g4692.md), if it were [dynatos](../../strongs/g/g1415.md) for him, to be at [Hierosolyma](../../strongs/g/g2414.md) the day of [pentēkostē](../../strongs/g/g4005.md).

<a name="acts_20_17"></a>Acts 20:17

And from [Milētos](../../strongs/g/g3399.md) he [pempō](../../strongs/g/g3992.md) to [Ephesos](../../strongs/g/g2181.md), and [metakaleō](../../strongs/g/g3333.md) the [presbyteros](../../strongs/g/g4245.md) of the [ekklēsia](../../strongs/g/g1577.md).

<a name="acts_20_18"></a>Acts 20:18

And when they were [paraginomai](../../strongs/g/g3854.md) to him, he [eipon](../../strongs/g/g2036.md) unto them, Ye [epistamai](../../strongs/g/g1987.md), from the first day that I [epibainō](../../strongs/g/g1910.md) into [Asia](../../strongs/g/g773.md), after what manner I have been with you at all [chronos](../../strongs/g/g5550.md),

<a name="acts_20_19"></a>Acts 20:19

[douleuō](../../strongs/g/g1398.md) the [kyrios](../../strongs/g/g2962.md) with all [tapeinophrosynē](../../strongs/g/g5012.md), and with [polys](../../strongs/g/g4183.md) [dakry](../../strongs/g/g1144.md), and [peirasmos](../../strongs/g/g3986.md), which [symbainō](../../strongs/g/g4819.md) me by the [epiboulē](../../strongs/g/g1917.md) of the [Ioudaios](../../strongs/g/g2453.md):

<a name="acts_20_20"></a>Acts 20:20

And how I [hypostellō](../../strongs/g/g5288.md) nothing that was [sympherō](../../strongs/g/g4851.md) unto you, but have [anaggellō](../../strongs/g/g312.md) you, and have [didaskō](../../strongs/g/g1321.md) you [dēmosios](../../strongs/g/g1219.md), and from [oikos](../../strongs/g/g3624.md),

<a name="acts_20_21"></a>Acts 20:21

[diamartyromai](../../strongs/g/g1263.md) both to the [Ioudaios](../../strongs/g/g2453.md), and also to the [Hellēn](../../strongs/g/g1672.md), [metanoia](../../strongs/g/g3341.md) toward [theos](../../strongs/g/g2316.md), and [pistis](../../strongs/g/g4102.md) toward our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="acts_20_22"></a>Acts 20:22

And now, [idou](../../strongs/g/g2400.md), I [poreuō](../../strongs/g/g4198.md) [deō](../../strongs/g/g1210.md) in the [pneuma](../../strongs/g/g4151.md) unto [Ierousalēm](../../strongs/g/g2419.md), not [eidō](../../strongs/g/g1492.md) the things that shall [synantaō](../../strongs/g/g4876.md) me there:

<a name="acts_20_23"></a>Acts 20:23

[plēn](../../strongs/g/g4133.md) that the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [diamartyromai](../../strongs/g/g1263.md) in every [polis](../../strongs/g/g4172.md), [legō](../../strongs/g/g3004.md) that [desmos](../../strongs/g/g1199.md) and [thlipsis](../../strongs/g/g2347.md) [menō](../../strongs/g/g3306.md) me.

<a name="acts_20_24"></a>Acts 20:24

But none of these [logos](../../strongs/g/g3056.md) [poieō](../../strongs/g/g4160.md) me, neither count I my [psychē](../../strongs/g/g5590.md) [timios](../../strongs/g/g5093.md) unto myself, so that I might [teleioō](../../strongs/g/g5048.md) my [dromos](../../strongs/g/g1408.md) with [chara](../../strongs/g/g5479.md), and the [diakonia](../../strongs/g/g1248.md), which I have [lambanō](../../strongs/g/g2983.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), to [diamartyromai](../../strongs/g/g1263.md) the [euaggelion](../../strongs/g/g2098.md) of the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_20_25"></a>Acts 20:25

And now, [idou](../../strongs/g/g2400.md), I [eidō](../../strongs/g/g1492.md) that ye all, among whom I have [dierchomai](../../strongs/g/g1330.md) [kēryssō](../../strongs/g/g2784.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), shall [optanomai](../../strongs/g/g3700.md) my [prosōpon](../../strongs/g/g4383.md) no more.

<a name="acts_20_26"></a>Acts 20:26

Wherefore I [martyromai](../../strongs/g/g3143.md) you [sēmeron](../../strongs/g/g4594.md) [hēmera](../../strongs/g/g2250.md), that I am [katharos](../../strongs/g/g2513.md) from the [haima](../../strongs/g/g129.md) of all.

<a name="acts_20_27"></a>Acts 20:27

For I have not [hypostellō](../../strongs/g/g5288.md) to [anaggellō](../../strongs/g/g312.md) unto you all the [boulē](../../strongs/g/g1012.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_20_28"></a>Acts 20:28

[prosechō](../../strongs/g/g4337.md) therefore unto yourselves, and to all the [poimnion](../../strongs/g/g4168.md), over the which the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) hath [tithēmi](../../strongs/g/g5087.md) you [episkopos](../../strongs/g/g1985.md), to [poimainō](../../strongs/g/g4165.md) the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md), which he hath [peripoieō](../../strongs/g/g4046.md) with his own [haima](../../strongs/g/g129.md).

<a name="acts_20_29"></a>Acts 20:29

For I [eidō](../../strongs/g/g1492.md) this, that after my [aphixis](../../strongs/g/g867.md) shall [barys](../../strongs/g/g926.md) [lykos](../../strongs/g/g3074.md) [eiserchomai](../../strongs/g/g1525.md) among you, not [pheidomai](../../strongs/g/g5339.md) the [poimnion](../../strongs/g/g4168.md).

<a name="acts_20_30"></a>Acts 20:30

Also of your own selves shall [anēr](../../strongs/g/g435.md) [anistēmi](../../strongs/g/g450.md), [laleō](../../strongs/g/g2980.md) [diastrephō](../../strongs/g/g1294.md), to [apospaō](../../strongs/g/g645.md) [mathētēs](../../strongs/g/g3101.md) after them.

<a name="acts_20_31"></a>Acts 20:31

Therefore [grēgoreō](../../strongs/g/g1127.md), and [mnēmoneuō](../../strongs/g/g3421.md), that by the space of three years I [pauō](../../strongs/g/g3973.md) not to [noutheteō](../../strongs/g/g3560.md) every one [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md) with [dakry](../../strongs/g/g1144.md).

<a name="acts_20_32"></a>Acts 20:32

And [ta nyn](../../strongs/g/g3569.md), [adelphos](../../strongs/g/g80.md), I [paratithēmi](../../strongs/g/g3908.md) you to [theos](../../strongs/g/g2316.md), and to the [logos](../../strongs/g/g3056.md) of his [charis](../../strongs/g/g5485.md), which is able to [epoikodomeō](../../strongs/g/g2026.md), and to [didōmi](../../strongs/g/g1325.md) you a [klēronomia](../../strongs/g/g2817.md) among all [hagiazō](../../strongs/g/g37.md).

<a name="acts_20_33"></a>Acts 20:33

I have [epithymeō](../../strongs/g/g1937.md) no man's [argyrion](../../strongs/g/g694.md), or [chrysion](../../strongs/g/g5553.md), or [himatismos](../../strongs/g/g2441.md).

<a name="acts_20_34"></a>Acts 20:34

Yea, ye yourselves [ginōskō](../../strongs/g/g1097.md), that these [cheir](../../strongs/g/g5495.md) have [hypēreteō](../../strongs/g/g5256.md) my [chreia](../../strongs/g/g5532.md), and to them that were with me.

<a name="acts_20_35"></a>Acts 20:35

I have [hypodeiknymi](../../strongs/g/g5263.md) you all things, how that so [kopiaō](../../strongs/g/g2872.md) ye ought to [antilambanō](../../strongs/g/g482.md) the [astheneō](../../strongs/g/g770.md), and to [mnēmoneuō](../../strongs/g/g3421.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), how he [eipon](../../strongs/g/g2036.md), It is more [makarios](../../strongs/g/g3107.md) to [didōmi](../../strongs/g/g1325.md) than to [lambanō](../../strongs/g/g2983.md).

<a name="acts_20_36"></a>Acts 20:36

And when he had thus [eipon](../../strongs/g/g2036.md), he [gony](../../strongs/g/g1119.md) [tithēmi](../../strongs/g/g5087.md), and [proseuchomai](../../strongs/g/g4336.md) with them all.

<a name="acts_20_37"></a>Acts 20:37

And they all [klauthmos](../../strongs/g/g2805.md) [ginomai](../../strongs/g/g1096.md) [hikanos](../../strongs/g/g2425.md), and [epipiptō](../../strongs/g/g1968.md) on [Paulos](../../strongs/g/g3972.md) [trachēlos](../../strongs/g/g5137.md), and [kataphileō](../../strongs/g/g2705.md) him,

<a name="acts_20_38"></a>Acts 20:38

[odynaō](../../strongs/g/g3600.md) [malista](../../strongs/g/g3122.md) for the [logos](../../strongs/g/g3056.md) which he [eipon](../../strongs/g/g2046.md), that they should [theōreō](../../strongs/g/g2334.md) his [prosōpon](../../strongs/g/g4383.md) no more. And they [propempō](../../strongs/g/g4311.md) him unto the [ploion](../../strongs/g/g4143.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 19](acts_19.md) - [Acts 21](acts_21.md)