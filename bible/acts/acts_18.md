# [Acts 18](https://www.blueletterbible.org/kjv/act/18/1/s_1036001)

<a name="acts_18_1"></a>Acts 18:1

After these things [Paulos](../../strongs/g/g3972.md) [chōrizō](../../strongs/g/g5563.md) from [Athēnai](../../strongs/g/g116.md), and [erchomai](../../strongs/g/g2064.md) to [Korinthos](../../strongs/g/g2882.md);

<a name="acts_18_2"></a>Acts 18:2

And [heuriskō](../../strongs/g/g2147.md) a certain [Ioudaios](../../strongs/g/g2453.md) [onoma](../../strongs/g/g3686.md) [Akylas](../../strongs/g/g207.md), [genos](../../strongs/g/g1085.md) in [Pontikos](../../strongs/g/g4193.md), [prosphatōs](../../strongs/g/g4373.md) [erchomai](../../strongs/g/g2064.md) from [Italia](../../strongs/g/g2482.md), with his [gynē](../../strongs/g/g1135.md) [Priskilla](../../strongs/g/g4252.md); (because that [Klaudios](../../strongs/g/g2804.md) had [diatassō](../../strongs/g/g1299.md) all [Ioudaios](../../strongs/g/g2453.md) to [chōrizō](../../strongs/g/g5563.md) from [Rhōmē](../../strongs/g/g4516.md):) and [proserchomai](../../strongs/g/g4334.md) unto them.

<a name="acts_18_3"></a>Acts 18:3

And because he was [homotechnos](../../strongs/g/g3673.md), he [menō](../../strongs/g/g3306.md) with them, and [ergazomai](../../strongs/g/g2038.md): for by their [technē](../../strongs/g/g5078.md) they were [skēnopoios](../../strongs/g/g4635.md).

<a name="acts_18_4"></a>Acts 18:4

And he [dialegomai](../../strongs/g/g1256.md) in the [synagōgē](../../strongs/g/g4864.md) every [sabbaton](../../strongs/g/g4521.md), and [peithō](../../strongs/g/g3982.md) the [Ioudaios](../../strongs/g/g2453.md) and the [Hellēn](../../strongs/g/g1672.md).

<a name="acts_18_5"></a>Acts 18:5

And when [Silas](../../strongs/g/g4609.md) and [Timotheos](../../strongs/g/g5095.md) were [katerchomai](../../strongs/g/g2718.md) from [Makedonia](../../strongs/g/g3109.md), [Paulos](../../strongs/g/g3972.md) was [synechō](../../strongs/g/g4912.md) in the [pneuma](../../strongs/g/g4151.md), and [diamartyromai](../../strongs/g/g1263.md) to the [Ioudaios](../../strongs/g/g2453.md) [Iēsous](../../strongs/g/g2424.md) was [Christos](../../strongs/g/g5547.md).

<a name="acts_18_6"></a>Acts 18:6

And when they [antitassō](../../strongs/g/g498.md) themselves, and [blasphēmeō](../../strongs/g/g987.md), he [ektinassō](../../strongs/g/g1621.md) [himation](../../strongs/g/g2440.md), and [eipon](../../strongs/g/g2036.md) unto them, Your [haima](../../strongs/g/g129.md) upon your own [kephalē](../../strongs/g/g2776.md); I [katharos](../../strongs/g/g2513.md); from henceforth I will [poreuō](../../strongs/g/g4198.md) unto the [ethnos](../../strongs/g/g1484.md).

<a name="acts_18_7"></a>Acts 18:7

And he [metabainō](../../strongs/g/g3327.md) thence, and [erchomai](../../strongs/g/g2064.md) into a certain [oikia](../../strongs/g/g3614.md), [onoma](../../strongs/g/g3686.md) [Ioustos](../../strongs/g/g2459.md), one that [sebō](../../strongs/g/g4576.md) [theos](../../strongs/g/g2316.md), whose [oikia](../../strongs/g/g3614.md) [synomoreō](../../strongs/g/g4927.md) to the [synagōgē](../../strongs/g/g4864.md).

<a name="acts_18_8"></a>Acts 18:8

And [Krispos](../../strongs/g/g2921.md), the [archisynagōgos](../../strongs/g/g752.md), [pisteuō](../../strongs/g/g4100.md) on the [kyrios](../../strongs/g/g2962.md) with all his [oikos](../../strongs/g/g3624.md); and [polys](../../strongs/g/g4183.md) of the [Korinthios](../../strongs/g/g2881.md) [akouō](../../strongs/g/g191.md) [pisteuō](../../strongs/g/g4100.md), and were [baptizō](../../strongs/g/g907.md).

<a name="acts_18_9"></a>Acts 18:9

Then [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) to [Paulos](../../strongs/g/g3972.md) in the [nyx](../../strongs/g/g3571.md) by a [horama](../../strongs/g/g3705.md), [phobeō](../../strongs/g/g5399.md) not, but [laleō](../../strongs/g/g2980.md), and [siōpaō](../../strongs/g/g4623.md) not:

<a name="acts_18_10"></a>Acts 18:10

For I am with thee, and [oudeis](../../strongs/g/g3762.md) shall [epitithēmi](../../strongs/g/g2007.md) thee to [kakoō](../../strongs/g/g2559.md) thee: for I have [polys](../../strongs/g/g4183.md) [laos](../../strongs/g/g2992.md) in this [polis](../../strongs/g/g4172.md).

<a name="acts_18_11"></a>Acts 18:11

And he [kathizō](../../strongs/g/g2523.md) there an [eniautos](../../strongs/g/g1763.md) and six [mēn](../../strongs/g/g3376.md), [didaskō](../../strongs/g/g1321.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) among them.

<a name="acts_18_12"></a>Acts 18:12

And when [Galliōn](../../strongs/g/g1058.md) was the [anthypateuō](../../strongs/g/g445.md) of [Achaïa](../../strongs/g/g882.md), the [Ioudaios](../../strongs/g/g2453.md) [katephistamai](../../strongs/g/g2721.md) with [homothymadon](../../strongs/g/g3661.md) [Paulos](../../strongs/g/g3972.md), and [agō](../../strongs/g/g71.md) him to the [bēma](../../strongs/g/g968.md),

<a name="acts_18_13"></a>Acts 18:13

[legō](../../strongs/g/g3004.md), This [anapeithō](../../strongs/g/g374.md) [anthrōpos](../../strongs/g/g444.md) to [sebō](../../strongs/g/g4576.md) [theos](../../strongs/g/g2316.md) contrary to the [nomos](../../strongs/g/g3551.md).

<a name="acts_18_14"></a>Acts 18:14

And when [Paulos](../../strongs/g/g3972.md) was now about to [anoigō](../../strongs/g/g455.md) [stoma](../../strongs/g/g4750.md), [Galliōn](../../strongs/g/g1058.md) [eipon](../../strongs/g/g2036.md) unto the [Ioudaios](../../strongs/g/g2453.md), If it were an [adikēma](../../strongs/g/g92.md) or [ponēros](../../strongs/g/g4190.md) [rhadiourgēma](../../strongs/g/g4467.md), [ō](../../strongs/g/g5599.md) ye [Ioudaios](../../strongs/g/g2453.md), [kata](../../strongs/g/g2596.md) [logos](../../strongs/g/g3056.md) would that I should [anechō](../../strongs/g/g430.md) with you:

<a name="acts_18_15"></a>Acts 18:15

But if it be a [zētēma](../../strongs/g/g2213.md) of [logos](../../strongs/g/g3056.md) and [onoma](../../strongs/g/g3686.md), and your [nomos](../../strongs/g/g3551.md), [optanomai](../../strongs/g/g3700.md) ye; for I will [boulomai](../../strongs/g/g1014.md) [einai](../../strongs/g/g1511.md) no [kritēs](../../strongs/g/g2923.md) of such.

<a name="acts_18_16"></a>Acts 18:16

And he [apelaunō](../../strongs/g/g556.md) them from the [bēma](../../strongs/g/g968.md).

<a name="acts_18_17"></a>Acts 18:17

Then all the [Hellēn](../../strongs/g/g1672.md) [epilambanomai](../../strongs/g/g1949.md) [Sōsthenēs](../../strongs/g/g4988.md), the [archisynagōgos](../../strongs/g/g752.md), and [typtō](../../strongs/g/g5180.md) him before the [bēma](../../strongs/g/g968.md). And [Galliōn](../../strongs/g/g1058.md) [melei](../../strongs/g/g3199.md) for none of those things.

<a name="acts_18_18"></a>Acts 18:18

And [Paulos](../../strongs/g/g3972.md) [prosmenō](../../strongs/g/g4357.md) yet a [hikanos](../../strongs/g/g2425.md) [hēmera](../../strongs/g/g2250.md), and then [apotassō](../../strongs/g/g657.md) of the [adelphos](../../strongs/g/g80.md), and [ekpleō](../../strongs/g/g1602.md) into [Syria](../../strongs/g/g4947.md), and with him [Priskilla](../../strongs/g/g4252.md) and [Akylas](../../strongs/g/g207.md); having [keirō](../../strongs/g/g2751.md) [kephalē](../../strongs/g/g2776.md) in [Kegchreai](../../strongs/g/g2747.md): for he had an [euchē](../../strongs/g/g2171.md).

<a name="acts_18_19"></a>Acts 18:19

And he [katantaō](../../strongs/g/g2658.md) to [Ephesos](../../strongs/g/g2181.md), and [kataleipō](../../strongs/g/g2641.md) them there: but he himself [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md), and [dialegomai](../../strongs/g/g1256.md) with the [Ioudaios](../../strongs/g/g2453.md).

<a name="acts_18_20"></a>Acts 18:20

When they [erōtaō](../../strongs/g/g2065.md) to [menō](../../strongs/g/g3306.md) longer [chronos](../../strongs/g/g5550.md) with them, he [epineuō](../../strongs/g/g1962.md) not;

<a name="acts_18_21"></a>Acts 18:21

But [apotassō](../../strongs/g/g657.md) them, [eipon](../../strongs/g/g2036.md), I must [pantōs](../../strongs/g/g3843.md) [poieō](../../strongs/g/g4160.md) this [heortē](../../strongs/g/g1859.md) that [erchomai](../../strongs/g/g2064.md) in [Hierosolyma](../../strongs/g/g2414.md): but I will [anakamptō](../../strongs/g/g344.md) again unto you, if [theos](../../strongs/g/g2316.md) will. And he [anagō](../../strongs/g/g321.md) from [Ephesos](../../strongs/g/g2181.md). [^1]

<a name="acts_18_22"></a>Acts 18:22

And when he had [katerchomai](../../strongs/g/g2718.md) at [Kaisareia](../../strongs/g/g2542.md), and [anabainō](../../strongs/g/g305.md), and [aspazomai](../../strongs/g/g782.md) the [ekklēsia](../../strongs/g/g1577.md), he [katabainō](../../strongs/g/g2597.md) to [Antiocheia](../../strongs/g/g490.md).

<a name="acts_18_23"></a>Acts 18:23

And after he had [poieō](../../strongs/g/g4160.md) some [chronos](../../strongs/g/g5550.md), he [exerchomai](../../strongs/g/g1831.md), and [dierchomai](../../strongs/g/g1330.md) the [chōra](../../strongs/g/g5561.md) of [Galatikos](../../strongs/g/g1054.md) and [Phrygia](../../strongs/g/g5435.md) in [kathexēs](../../strongs/g/g2517.md), [epistērizō](../../strongs/g/g1991.md) all the [mathētēs](../../strongs/g/g3101.md).

<a name="acts_18_24"></a>Acts 18:24

And a certain [Ioudaios](../../strongs/g/g2453.md) [onoma](../../strongs/g/g3686.md) [Apollōs](../../strongs/g/g625.md), [genos](../../strongs/g/g1085.md) [Alexandreus](../../strongs/g/g221.md), a [logios](../../strongs/g/g3052.md) [anēr](../../strongs/g/g435.md), [ōn](../../strongs/g/g5607.md) [dynatos](../../strongs/g/g1415.md) in the [graphē](../../strongs/g/g1124.md), [katantaō](../../strongs/g/g2658.md) to [Ephesos](../../strongs/g/g2181.md).

<a name="acts_18_25"></a>Acts 18:25

This man was [katēcheō](../../strongs/g/g2727.md) in [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md); and [zeō](../../strongs/g/g2204.md) in the [pneuma](../../strongs/g/g4151.md), he [laleō](../../strongs/g/g2980.md) and [didaskō](../../strongs/g/g1321.md) [akribōs](../../strongs/g/g199.md) the things of the [kyrios](../../strongs/g/g2962.md), [epistamai](../../strongs/g/g1987.md) only the [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md).

<a name="acts_18_26"></a>Acts 18:26

And he [archomai](../../strongs/g/g756.md) to [parrēsiazomai](../../strongs/g/g3955.md) in the [synagōgē](../../strongs/g/g4864.md): whom when [Akylas](../../strongs/g/g207.md) and [Priskilla](../../strongs/g/g4252.md) had [akouō](../../strongs/g/g191.md), they [proslambanō](../../strongs/g/g4355.md) him unto them, and [ektithēmi](../../strongs/g/g1620.md) unto him the [hodos](../../strongs/g/g3598.md) of [theos](../../strongs/g/g2316.md) [akribesteron](../../strongs/g/g197.md).

<a name="acts_18_27"></a>Acts 18:27

And when he was [boulomai](../../strongs/g/g1014.md) to [dierchomai](../../strongs/g/g1330.md) into [Achaïa](../../strongs/g/g882.md), the [adelphos](../../strongs/g/g80.md) [graphō](../../strongs/g/g1125.md), [protrepō](../../strongs/g/g4389.md) the [mathētēs](../../strongs/g/g3101.md) to [apodechomai](../../strongs/g/g588.md) him: who, when he was [paraginomai](../../strongs/g/g3854.md), [symballō](../../strongs/g/g4820.md) them [polys](../../strongs/g/g4183.md) which had [pisteuō](../../strongs/g/g4100.md) through [charis](../../strongs/g/g5485.md):

<a name="acts_18_28"></a>Acts 18:28

For he [eutonōs](../../strongs/g/g2159.md) [diakatelegchomai](../../strongs/g/g1246.md) the [Ioudaios](../../strongs/g/g2453.md), [dēmosios](../../strongs/g/g1219.md), [epideiknymi](../../strongs/g/g1925.md) by the [graphē](../../strongs/g/g1124.md) that [Iēsous](../../strongs/g/g2424.md) was [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 17](acts_17.md) - [Acts 19](acts_19.md)

---

[^1]: [Acts 18:21 Commentary](../../commentary/acts/acts_18_commentary.md#acts_18_21)
