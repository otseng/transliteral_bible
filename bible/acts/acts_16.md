# [Acts 16](https://www.blueletterbible.org/kjv/act/16/1/s_1034001)

<a name="acts_16_1"></a>Acts 16:1

Then [katantaō](../../strongs/g/g2658.md) he to [Derbē](../../strongs/g/g1191.md) and [Lystra](../../strongs/g/g3082.md): and, [idou](../../strongs/g/g2400.md), a certain [mathētēs](../../strongs/g/g3101.md) was there, [onoma](../../strongs/g/g3686.md) [Timotheos](../../strongs/g/g5095.md), the [huios](../../strongs/g/g5207.md) of a certain [gynē](../../strongs/g/g1135.md), which was an [Ioudaios](../../strongs/g/g2453.md), and [pistos](../../strongs/g/g4103.md); but his [patēr](../../strongs/g/g3962.md) was a [Hellēn](../../strongs/g/g1672.md):

<a name="acts_16_2"></a>Acts 16:2

Which was [martyreō](../../strongs/g/g3140.md) of by the [adelphos](../../strongs/g/g80.md) that were at [Lystra](../../strongs/g/g3082.md) and [Ikonion](../../strongs/g/g2430.md).

<a name="acts_16_3"></a>Acts 16:3

Him would [Paulos](../../strongs/g/g3972.md) have to [exerchomai](../../strongs/g/g1831.md) with him; and [lambanō](../../strongs/g/g2983.md) and [peritemnō](../../strongs/g/g4059.md) him because of the [Ioudaios](../../strongs/g/g2453.md) which were in those [topos](../../strongs/g/g5117.md): for they [eidō](../../strongs/g/g1492.md) all that his [patēr](../../strongs/g/g3962.md) was a [Hellēn](../../strongs/g/g1672.md).

<a name="acts_16_4"></a>Acts 16:4

And as they [diaporeuomai](../../strongs/g/g1279.md) the [polis](../../strongs/g/g4172.md), they [paradidōmi](../../strongs/g/g3860.md) them the [dogma](../../strongs/g/g1378.md) for to [phylassō](../../strongs/g/g5442.md), that were [krinō](../../strongs/g/g2919.md) of the [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md) which were at [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_16_5"></a>Acts 16:5

And so were the [ekklēsia](../../strongs/g/g1577.md) [stereoō](../../strongs/g/g4732.md) in the [pistis](../../strongs/g/g4102.md), and [perisseuō](../../strongs/g/g4052.md) in [arithmos](../../strongs/g/g706.md) daily.

<a name="acts_16_6"></a>Acts 16:6

Now when they had [dierchomai](../../strongs/g/g1330.md) [Phrygia](../../strongs/g/g5435.md) and the [chōra](../../strongs/g/g5561.md) of [Galatikos](../../strongs/g/g1054.md), and were [kōlyō](../../strongs/g/g2967.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) to [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) in [Asia](../../strongs/g/g773.md),

<a name="acts_16_7"></a>Acts 16:7

After they were [erchomai](../../strongs/g/g2064.md) to [Mysia](../../strongs/g/g3465.md), they [peirazō](../../strongs/g/g3985.md) to [poreuō](../../strongs/g/g4198.md) into [Bithynia](../../strongs/g/g978.md): but the [pneuma](../../strongs/g/g4151.md) [eaō](../../strongs/g/g1439.md) them not.

<a name="acts_16_8"></a>Acts 16:8

And they [parerchomai](../../strongs/g/g3928.md) [Mysia](../../strongs/g/g3465.md) [katabainō](../../strongs/g/g2597.md) to [Trōas](../../strongs/g/g5174.md).

<a name="acts_16_9"></a>Acts 16:9

And a [horama](../../strongs/g/g3705.md) [optanomai](../../strongs/g/g3700.md) to [Paulos](../../strongs/g/g3972.md) in the [nyx](../../strongs/g/g3571.md); There [histēmi](../../strongs/g/g2476.md) an [anēr](../../strongs/g/g435.md) of [Makedōn](../../strongs/g/g3110.md), and [parakaleō](../../strongs/g/g3870.md) him, [legō](../../strongs/g/g3004.md), [diabainō](../../strongs/g/g1224.md) into [Makedonia](../../strongs/g/g3109.md), and [boētheō](../../strongs/g/g997.md) us.

<a name="acts_16_10"></a>Acts 16:10

And after he had [eidō](../../strongs/g/g1492.md) the [horama](../../strongs/g/g3705.md), [eutheōs](../../strongs/g/g2112.md) we [zēteō](../../strongs/g/g2212.md) to [exerchomai](../../strongs/g/g1831.md) into [Makedonia](../../strongs/g/g3109.md), [symbibazō](../../strongs/g/g4822.md) that the [kyrios](../../strongs/g/g2962.md) had [proskaleō](../../strongs/g/g4341.md) us for to [euaggelizō](../../strongs/g/g2097.md) unto them.

<a name="acts_16_11"></a>Acts 16:11

Therefore [anagō](../../strongs/g/g321.md) from [Trōas](../../strongs/g/g5174.md), we [euthydromeō](../../strongs/g/g2113.md) to [Samothrakē](../../strongs/g/g4543.md), and the [epeimi](../../strongs/g/g1966.md) to [Nea Polis](../../strongs/g/g3496.md);

<a name="acts_16_12"></a>Acts 16:12

And from thence to [Philippoi](../../strongs/g/g5375.md), which is the [prōtos](../../strongs/g/g4413.md) [polis](../../strongs/g/g4172.md) of that [meris](../../strongs/g/g3310.md) of [Makedonia](../../strongs/g/g3109.md), and a [kolōnia](../../strongs/g/g2862.md): and we were in that [polis](../../strongs/g/g4172.md) [diatribō](../../strongs/g/g1304.md) certain [hēmera](../../strongs/g/g2250.md).

<a name="acts_16_13"></a>Acts 16:13

And on the [sabbaton](../../strongs/g/g4521.md) we [exerchomai](../../strongs/g/g1831.md) of the [polis](../../strongs/g/g4172.md) by a [potamos](../../strongs/g/g4215.md), where [proseuchē](../../strongs/g/g4335.md) was [nomizō](../../strongs/g/g3543.md) [einai](../../strongs/g/g1511.md); and we [kathizō](../../strongs/g/g2523.md), and [laleō](../../strongs/g/g2980.md) unto the [gynē](../../strongs/g/g1135.md) which [synerchomai](../../strongs/g/g4905.md).

<a name="acts_16_14"></a>Acts 16:14

And a certain [gynē](../../strongs/g/g1135.md) [onoma](../../strongs/g/g3686.md) [Lydia](../../strongs/g/g3070.md), a [porphyropōlis](../../strongs/g/g4211.md), of the [polis](../../strongs/g/g4172.md) of [Thyateira](../../strongs/g/g2363.md), which [sebō](../../strongs/g/g4576.md) [theos](../../strongs/g/g2316.md), [akouō](../../strongs/g/g191.md): whose [kardia](../../strongs/g/g2588.md) the [kyrios](../../strongs/g/g2962.md) [dianoigō](../../strongs/g/g1272.md), that she [prosechō](../../strongs/g/g4337.md) unto the things which were [laleō](../../strongs/g/g2980.md) of [Paulos](../../strongs/g/g3972.md).

<a name="acts_16_15"></a>Acts 16:15

And when she was [baptizō](../../strongs/g/g907.md), and her [oikos](../../strongs/g/g3624.md), she [parakaleō](../../strongs/g/g3870.md) us, [legō](../../strongs/g/g3004.md), If ye have [krinō](../../strongs/g/g2919.md) me to be [pistos](../../strongs/g/g4103.md) to the [kyrios](../../strongs/g/g2962.md), [eiserchomai](../../strongs/g/g1525.md) into my [oikos](../../strongs/g/g3624.md), and [menō](../../strongs/g/g3306.md) there. And she [parabiazomai](../../strongs/g/g3849.md) us.

<a name="acts_16_16"></a>Acts 16:16

And [ginomai](../../strongs/g/g1096.md), as we [poreuō](../../strongs/g/g4198.md) to [proseuchē](../../strongs/g/g4335.md), a certain [paidiskē](../../strongs/g/g3814.md) [echō](../../strongs/g/g2192.md) with a [pneuma](../../strongs/g/g4151.md) of [pythōn](../../strongs/g/g4436.md) [apantaō](../../strongs/g/g528.md) us, which [parechō](../../strongs/g/g3930.md) her [kyrios](../../strongs/g/g2962.md) [polys](../../strongs/g/g4183.md) [ergasia](../../strongs/g/g2039.md) by [manteuomai](../../strongs/g/g3132.md):

<a name="acts_16_17"></a>Acts 16:17

The same [katakoloutheō](../../strongs/g/g2628.md) [Paulos](../../strongs/g/g3972.md) and us, and [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), These [anthrōpos](../../strongs/g/g444.md) are the [doulos](../../strongs/g/g1401.md) of the [hypsistos](../../strongs/g/g5310.md) [theos](../../strongs/g/g2316.md), which [kataggellō](../../strongs/g/g2605.md) unto us the [hodos](../../strongs/g/g3598.md) of [sōtēria](../../strongs/g/g4991.md).

<a name="acts_16_18"></a>Acts 16:18

And this [poieō](../../strongs/g/g4160.md) she [polys](../../strongs/g/g4183.md) [hēmera](../../strongs/g/g2250.md). But [Paulos](../../strongs/g/g3972.md), [diaponeomai](../../strongs/g/g1278.md), [epistrephō](../../strongs/g/g1994.md) and [eipon](../../strongs/g/g2036.md) to the [pneuma](../../strongs/g/g4151.md), I [paraggellō](../../strongs/g/g3853.md) thee in the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) to [exerchomai](../../strongs/g/g1831.md) of her. And he [exerchomai](../../strongs/g/g1831.md) the same [hōra](../../strongs/g/g5610.md).

<a name="acts_16_19"></a>Acts 16:19

And when her [kyrios](../../strongs/g/g2962.md) [eidō](../../strongs/g/g1492.md) that the [elpis](../../strongs/g/g1680.md) of their [ergasia](../../strongs/g/g2039.md) was [exerchomai](../../strongs/g/g1831.md), they [epilambanomai](../../strongs/g/g1949.md) [Paulos](../../strongs/g/g3972.md) and [Silas](../../strongs/g/g4609.md), and [helkō](../../strongs/g/g1670.md) into the [agora](../../strongs/g/g58.md) unto the [archōn](../../strongs/g/g758.md),

<a name="acts_16_20"></a>Acts 16:20

And [prosagō](../../strongs/g/g4317.md) them to the [stratēgos](../../strongs/g/g4755.md), [eipon](../../strongs/g/g2036.md), These [anthrōpos](../../strongs/g/g444.md), being [Ioudaios](../../strongs/g/g2453.md), [ektarassō](../../strongs/g/g1613.md) our [polis](../../strongs/g/g4172.md),

<a name="acts_16_21"></a>Acts 16:21

And [kataggellō](../../strongs/g/g2605.md) [ethos](../../strongs/g/g1485.md), which are not [exesti](../../strongs/g/g1832.md) for us to [paradechomai](../../strongs/g/g3858.md), neither to [poieō](../../strongs/g/g4160.md), being [Rhōmaios](../../strongs/g/g4514.md).

<a name="acts_16_22"></a>Acts 16:22

And the [ochlos](../../strongs/g/g3793.md) [synephistēmi](../../strongs/g/g4911.md) against them: and the [stratēgos](../../strongs/g/g4755.md) [perirēgnymi](../../strongs/g/g4048.md) their [himation](../../strongs/g/g2440.md), and [keleuō](../../strongs/g/g2753.md) to [rhabdizō](../../strongs/g/g4463.md).

<a name="acts_16_23"></a>Acts 16:23

And when they had [epitithēmi](../../strongs/g/g2007.md) [polys](../../strongs/g/g4183.md) [plēgē](../../strongs/g/g4127.md) upon them, they [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md), [paraggellō](../../strongs/g/g3853.md) the [desmophylax](../../strongs/g/g1200.md) to [tēreō](../../strongs/g/g5083.md) them [asphalōs](../../strongs/g/g806.md):

<a name="acts_16_24"></a>Acts 16:24

Who, having [lambanō](../../strongs/g/g2983.md) such a [paraggelia](../../strongs/g/g3852.md), [ballō](../../strongs/g/g906.md) them into the [esōteros](../../strongs/g/g2082.md) [phylakē](../../strongs/g/g5438.md), and [asphalizō](../../strongs/g/g805.md) their [pous](../../strongs/g/g4228.md) [asphalizō](../../strongs/g/g805.md) in the [xylon](../../strongs/g/g3586.md).

<a name="acts_16_25"></a>Acts 16:25

And at [mesonyktion](../../strongs/g/g3317.md) [Paulos](../../strongs/g/g3972.md) and [Silas](../../strongs/g/g4609.md) [proseuchomai](../../strongs/g/g4336.md), and [hymneō](../../strongs/g/g5214.md) unto [theos](../../strongs/g/g2316.md): and the [desmios](../../strongs/g/g1198.md) [epakroaomai](../../strongs/g/g1874.md) them.

<a name="acts_16_26"></a>Acts 16:26

And [aphnō](../../strongs/g/g869.md) there was a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md), so that the [themelios](../../strongs/g/g2310.md) of the [desmōtērion](../../strongs/g/g1201.md) were [saleuō](../../strongs/g/g4531.md): and [parachrēma](../../strongs/g/g3916.md) all the [thyra](../../strongs/g/g2374.md) were [anoigō](../../strongs/g/g455.md), and every one's [desmos](../../strongs/g/g1199.md) were [aniēmi](../../strongs/g/g447.md).

<a name="acts_16_27"></a>Acts 16:27

And the [desmophylax](../../strongs/g/g1200.md) [ginomai](../../strongs/g/g1096.md) [exypnos](../../strongs/g/g1853.md), and [eidō](../../strongs/g/g1492.md) the [phylakē](../../strongs/g/g5438.md) [thyra](../../strongs/g/g2374.md) [anoigō](../../strongs/g/g455.md), he [spaō](../../strongs/g/g4685.md) his [machaira](../../strongs/g/g3162.md), and would have [anaireō](../../strongs/g/g337.md) himself, [nomizō](../../strongs/g/g3543.md) that the [desmios](../../strongs/g/g1198.md) had [ekpheugō](../../strongs/g/g1628.md).

<a name="acts_16_28"></a>Acts 16:28

But [Paulos](../../strongs/g/g3972.md) [phōneō](../../strongs/g/g5455.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), [prassō](../../strongs/g/g4238.md) thyself [mēdeis](../../strongs/g/g3367.md) [kakos](../../strongs/g/g2556.md): for we are all [enthade](../../strongs/g/g1759.md).

<a name="acts_16_29"></a>Acts 16:29

Then he [aiteō](../../strongs/g/g154.md) for a [phōs](../../strongs/g/g5457.md), and [eispēdaō](../../strongs/g/g1530.md), and [ginomai](../../strongs/g/g1096.md) [entromos](../../strongs/g/g1790.md), and [prospiptō](../../strongs/g/g4363.md) [Paulos](../../strongs/g/g3972.md) and [Silas](../../strongs/g/g4609.md),

<a name="acts_16_30"></a>Acts 16:30

And [proagō](../../strongs/g/g4254.md) them out, and [phēmi](../../strongs/g/g5346.md), [kyrios](../../strongs/g/g2962.md), what must I [poieō](../../strongs/g/g4160.md) to be [sōzō](../../strongs/g/g4982.md)?

<a name="acts_16_31"></a>Acts 16:31

And they [eipon](../../strongs/g/g2036.md), [pisteuō](../../strongs/g/g4100.md) on the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and thou shalt be [sōzō](../../strongs/g/g4982.md), and thy [oikos](../../strongs/g/g3624.md).

<a name="acts_16_32"></a>Acts 16:32

And they [laleō](../../strongs/g/g2980.md) unto him the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), and to all that were in his [oikia](../../strongs/g/g3614.md).

<a name="acts_16_33"></a>Acts 16:33

And he [paralambanō](../../strongs/g/g3880.md) them the same [hōra](../../strongs/g/g5610.md) of the [nyx](../../strongs/g/g3571.md), and [louō](../../strongs/g/g3068.md) their [plēgē](../../strongs/g/g4127.md); and was [baptizō](../../strongs/g/g907.md), he and all his, [parachrēma](../../strongs/g/g3916.md).

<a name="acts_16_34"></a>Acts 16:34

And when he had [anagō](../../strongs/g/g321.md) them into his [oikos](../../strongs/g/g3624.md), he [paratithēmi](../../strongs/g/g3908.md) [trapeza](../../strongs/g/g5132.md) before them, and [agalliaō](../../strongs/g/g21.md), [pisteuō](../../strongs/g/g4100.md) in [theos](../../strongs/g/g2316.md) with all his [panoikei](../../strongs/g/g3832.md).

<a name="acts_16_35"></a>Acts 16:35

And when it was [hēmera](../../strongs/g/g2250.md), the [stratēgos](../../strongs/g/g4755.md) [apostellō](../../strongs/g/g649.md) the [rhabdouchos](../../strongs/g/g4465.md), [legō](../../strongs/g/g3004.md), [apolyō](../../strongs/g/g630.md) those [anthrōpos](../../strongs/g/g444.md).

<a name="acts_16_36"></a>Acts 16:36

And the [desmophylax](../../strongs/g/g1200.md) [apaggellō](../../strongs/g/g518.md) this [logos](../../strongs/g/g3056.md) to [Paulos](../../strongs/g/g3972.md), The [stratēgos](../../strongs/g/g4755.md) have [apostellō](../../strongs/g/g649.md) to [apolyō](../../strongs/g/g630.md) you: now therefore [exerchomai](../../strongs/g/g1831.md), and [poreuō](../../strongs/g/g4198.md) in [eirēnē](../../strongs/g/g1515.md).

<a name="acts_16_37"></a>Acts 16:37

But [Paulos](../../strongs/g/g3972.md) [phēmi](../../strongs/g/g5346.md) unto them, They have [derō](../../strongs/g/g1194.md) us [dēmosios](../../strongs/g/g1219.md) [akatakritos](../../strongs/g/g178.md), being [Rhōmaios](../../strongs/g/g4514.md) [anthrōpos](../../strongs/g/g444.md), and have [ballō](../../strongs/g/g906.md) into [phylakē](../../strongs/g/g5438.md); and now do they [ekballō](../../strongs/g/g1544.md) us [lathra](../../strongs/g/g2977.md)? nay [gar](../../strongs/g/g1063.md); but let them [erchomai](../../strongs/g/g2064.md) themselves and [exagō](../../strongs/g/g1806.md) us.

<a name="acts_16_38"></a>Acts 16:38

And the [rhabdouchos](../../strongs/g/g4465.md) [anaggellō](../../strongs/g/g312.md) these [rhēma](../../strongs/g/g4487.md) unto the [stratēgos](../../strongs/g/g4755.md): and they [phobeō](../../strongs/g/g5399.md), when they [akouō](../../strongs/g/g191.md) that they were [Rhōmaios](../../strongs/g/g4514.md).

<a name="acts_16_39"></a>Acts 16:39

And they [erchomai](../../strongs/g/g2064.md) and [parakaleō](../../strongs/g/g3870.md) them, and [exagō](../../strongs/g/g1806.md), and [erōtaō](../../strongs/g/g2065.md) to [exerchomai](../../strongs/g/g1831.md) of the [polis](../../strongs/g/g4172.md).

<a name="acts_16_40"></a>Acts 16:40

And they [exerchomai](../../strongs/g/g1831.md) out of the [phylakē](../../strongs/g/g5438.md), and [eiserchomai](../../strongs/g/g1525.md) into of [Lydia](../../strongs/g/g3070.md): and when they had [eidō](../../strongs/g/g1492.md) the [adelphos](../../strongs/g/g80.md), they [parakaleō](../../strongs/g/g3870.md) them, and [exerchomai](../../strongs/g/g1831.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 15](acts_15.md) - [Acts 17](acts_17.md)