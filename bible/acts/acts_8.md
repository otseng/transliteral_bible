# [Acts 8](https://www.blueletterbible.org/kjv/act/8/1/s_1026001)

<a name="acts_8_1"></a>Acts 8:1

And [Saulos](../../strongs/g/g4569.md) was [syneudokeō](../../strongs/g/g4909.md) unto his [anairesis](../../strongs/g/g336.md). And at that [hēmera](../../strongs/g/g2250.md) there was a [megas](../../strongs/g/g3173.md) [diōgmos](../../strongs/g/g1375.md) against the [ekklēsia](../../strongs/g/g1577.md) which was at [Hierosolyma](../../strongs/g/g2414.md); and they were all [diaspeirō](../../strongs/g/g1289.md) throughout the [chōra](../../strongs/g/g5561.md) of [Ioudaia](../../strongs/g/g2449.md) and [Samareia](../../strongs/g/g4540.md), except the [apostolos](../../strongs/g/g652.md).

<a name="acts_8_2"></a>Acts 8:2

And [eulabēs](../../strongs/g/g2126.md) [anēr](../../strongs/g/g435.md) [sygkomizō](../../strongs/g/g4792.md) [Stephanos](../../strongs/g/g4736.md), and [poieō](../../strongs/g/g4160.md) [megas](../../strongs/g/g3173.md) [kopetos](../../strongs/g/g2870.md) over him.

<a name="acts_8_3"></a>Acts 8:3

As for [Saulos](../../strongs/g/g4569.md), he [lymainō](../../strongs/g/g3075.md) of the [ekklēsia](../../strongs/g/g1577.md), [eisporeuomai](../../strongs/g/g1531.md) every [oikos](../../strongs/g/g3624.md), and [syrō](../../strongs/g/g4951.md) [anēr](../../strongs/g/g435.md) and [gynē](../../strongs/g/g1135.md) [paradidōmi](../../strongs/g/g3860.md) them to [phylakē](../../strongs/g/g5438.md).

<a name="acts_8_4"></a>Acts 8:4

Therefore [diaspeirō](../../strongs/g/g1289.md) [dierchomai](../../strongs/g/g1330.md) [euaggelizō](../../strongs/g/g2097.md) the [logos](../../strongs/g/g3056.md).

<a name="acts_8_5"></a>Acts 8:5

Then [Philippos](../../strongs/g/g5376.md) [katerchomai](../../strongs/g/g2718.md) to the [polis](../../strongs/g/g4172.md) of [Samareia](../../strongs/g/g4540.md), and [kēryssō](../../strongs/g/g2784.md) [Christos](../../strongs/g/g5547.md) unto them.

<a name="acts_8_6"></a>Acts 8:6

And the [ochlos](../../strongs/g/g3793.md) [homothymadon](../../strongs/g/g3661.md) [prosechō](../../strongs/g/g4337.md) unto those things which [Philippos](../../strongs/g/g5376.md) [legō](../../strongs/g/g3004.md), [akouō](../../strongs/g/g191.md) and [blepō](../../strongs/g/g991.md) the [sēmeion](../../strongs/g/g4592.md) which he [poieō](../../strongs/g/g4160.md).

<a name="acts_8_7"></a>Acts 8:7

For [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), [boaō](../../strongs/g/g994.md) with [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [exerchomai](../../strongs/g/g1831.md) of [polys](../../strongs/g/g4183.md) [echō](../../strongs/g/g2192.md): and [polys](../../strongs/g/g4183.md) [paralyō](../../strongs/g/g3886.md), and that were [chōlos](../../strongs/g/g5560.md), were [therapeuō](../../strongs/g/g2323.md).

<a name="acts_8_8"></a>Acts 8:8

And there was [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md) in that [polis](../../strongs/g/g4172.md).

<a name="acts_8_9"></a>Acts 8:9

But there was a certain [anēr](../../strongs/g/g435.md), [onoma](../../strongs/g/g3686.md) [Simōn](../../strongs/g/g4613.md), which [prouparchō](../../strongs/g/g4391.md) in the same [polis](../../strongs/g/g4172.md) [mageuō](../../strongs/g/g3096.md), and [existēmi](../../strongs/g/g1839.md) the [ethnos](../../strongs/g/g1484.md) of [Samareia](../../strongs/g/g4540.md), [legō](../../strongs/g/g3004.md) that himself was some [megas](../../strongs/g/g3173.md):

<a name="acts_8_10"></a>Acts 8:10

To whom they all [prosechō](../../strongs/g/g4337.md), from the [mikros](../../strongs/g/g3398.md) to the [megas](../../strongs/g/g3173.md), [legō](../../strongs/g/g3004.md), [houtos](../../strongs/g/g3778.md) is the [megas](../../strongs/g/g3173.md) [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_8_11"></a>Acts 8:11

And to him they had [prosechō](../../strongs/g/g4337.md), because [hikanos](../../strongs/g/g2425.md) [chronos](../../strongs/g/g5550.md) he had [existēmi](../../strongs/g/g1839.md) them with [mageia](../../strongs/g/g3095.md).

<a name="acts_8_12"></a>Acts 8:12

But when they [pisteuō](../../strongs/g/g4100.md) [Philippos](../../strongs/g/g5376.md) [euaggelizō](../../strongs/g/g2097.md) the things concerning the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), they were [baptizō](../../strongs/g/g907.md), both [anēr](../../strongs/g/g435.md) and [gynē](../../strongs/g/g1135.md).

<a name="acts_8_13"></a>Acts 8:13

Then [Simōn](../../strongs/g/g4613.md) himself [pisteuō](../../strongs/g/g4100.md) also: and when he was [baptizō](../../strongs/g/g907.md), he [proskartereō](../../strongs/g/g4342.md) with [Philippos](../../strongs/g/g5376.md), and [existēmi](../../strongs/g/g1839.md), [theōreō](../../strongs/g/g2334.md) the [dynamis](../../strongs/g/g1411.md) and [sēmeion](../../strongs/g/g4592.md) [megas](../../strongs/g/g3173.md) which were [ginomai](../../strongs/g/g1096.md).

<a name="acts_8_14"></a>Acts 8:14

Now when the [apostolos](../../strongs/g/g652.md) which were at [Hierosolyma](../../strongs/g/g2414.md) [akouō](../../strongs/g/g191.md) that [Samareia](../../strongs/g/g4540.md) had [dechomai](../../strongs/g/g1209.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), they [apostellō](../../strongs/g/g649.md) unto them [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md):

<a name="acts_8_15"></a>Acts 8:15

Who, when they were [katabainō](../../strongs/g/g2597.md), [proseuchomai](../../strongs/g/g4336.md) for them, that they might [lambanō](../../strongs/g/g2983.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md):

<a name="acts_8_16"></a>Acts 8:16

(For as yet he was [epipiptō](../../strongs/g/g1968.md) upon none of them: only they were [baptizō](../../strongs/g/g907.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).)

<a name="acts_8_17"></a>Acts 8:17

Then [epitithēmi](../../strongs/g/g2007.md) they [cheir](../../strongs/g/g5495.md) on them, and they [lambanō](../../strongs/g/g2983.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_8_18"></a>Acts 8:18

And when [Simōn](../../strongs/g/g4613.md) [theaomai](../../strongs/g/g2300.md) that through [epithesis](../../strongs/g/g1936.md) of the [apostolos](../../strongs/g/g652.md) [cheir](../../strongs/g/g5495.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) was [didōmi](../../strongs/g/g1325.md), he [prospherō](../../strongs/g/g4374.md) them [chrēma](../../strongs/g/g5536.md),

<a name="acts_8_19"></a>Acts 8:19

[legō](../../strongs/g/g3004.md), [didōmi](../../strongs/g/g1325.md) me also this [exousia](../../strongs/g/g1849.md), that on whomsoever I [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md), he may [lambanō](../../strongs/g/g2983.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_8_20"></a>Acts 8:20

But [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md) unto him, Thy [argyrion](../../strongs/g/g694.md) [eiēn](../../strongs/g/g1498.md) [eis](../../strongs/g/g1519.md) [apōleia](../../strongs/g/g684.md) with thee, because thou hast [nomizō](../../strongs/g/g3543.md) that the [dōrea](../../strongs/g/g1431.md) of [theos](../../strongs/g/g2316.md) may be [ktaomai](../../strongs/g/g2932.md) with [chrēma](../../strongs/g/g5536.md).

<a name="acts_8_21"></a>Acts 8:21

Thou hast neither [meris](../../strongs/g/g3310.md) nor [klēros](../../strongs/g/g2819.md) in this [logos](../../strongs/g/g3056.md): for thy [kardia](../../strongs/g/g2588.md) is not [euthys](../../strongs/g/g2117.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_8_22"></a>Acts 8:22

[metanoeō](../../strongs/g/g3340.md) therefore of this thy [kakia](../../strongs/g/g2549.md), and [deomai](../../strongs/g/g1189.md) [theos](../../strongs/g/g2316.md), if perhaps the [epinoia](../../strongs/g/g1963.md) of thine [kardia](../../strongs/g/g2588.md) may be [aphiēmi](../../strongs/g/g863.md) thee.

<a name="acts_8_23"></a>Acts 8:23

For I [horaō](../../strongs/g/g3708.md) that thou art in the [cholē](../../strongs/g/g5521.md) of [pikria](../../strongs/g/g4088.md), and in the [syndesmos](../../strongs/g/g4886.md) of [adikia](../../strongs/g/g93.md).

<a name="acts_8_24"></a>Acts 8:24

Then [apokrinomai](../../strongs/g/g611.md) [Simōn](../../strongs/g/g4613.md), and [eipon](../../strongs/g/g2036.md), [deomai](../../strongs/g/g1189.md) ye to the [kyrios](../../strongs/g/g2962.md) for me, that [mēdeis](../../strongs/g/g3367.md) which ye have [eipon](../../strongs/g/g2046.md) [eperchomai](../../strongs/g/g1904.md) upon me.

<a name="acts_8_25"></a>Acts 8:25

And they, when they had [diamartyromai](../../strongs/g/g1263.md) and [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), [hypostrephō](../../strongs/g/g5290.md) to [Ierousalēm](../../strongs/g/g2419.md), and [euaggelizō](../../strongs/g/g2097.md) in [polys](../../strongs/g/g4183.md) [kōmē](../../strongs/g/g2968.md) of the [Samaritēs](../../strongs/g/g4541.md).

<a name="acts_8_26"></a>Acts 8:26

And the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [laleō](../../strongs/g/g2980.md) unto [Philippos](../../strongs/g/g5376.md), [legō](../../strongs/g/g3004.md), [anistēmi](../../strongs/g/g450.md), and [poreuō](../../strongs/g/g4198.md) toward [mesēmbria](../../strongs/g/g3314.md) unto [hodos](../../strongs/g/g3598.md) [katabainō](../../strongs/g/g2597.md) from [Ierousalēm](../../strongs/g/g2419.md) unto [Gaza](../../strongs/g/g1048.md), which is [erēmos](../../strongs/g/g2048.md).

<a name="acts_8_27"></a>Acts 8:27

And he [anistēmi](../../strongs/g/g450.md) and [poreuō](../../strongs/g/g4198.md): and, [idou](../../strongs/g/g2400.md), an [anēr](../../strongs/g/g435.md) of [Aithiops](../../strongs/g/g128.md), an [eunouchos](../../strongs/g/g2135.md) [dynastēs](../../strongs/g/g1413.md) under [Kandakē](../../strongs/g/g2582.md) [basilissa](../../strongs/g/g938.md) of the [Aithiops](../../strongs/g/g128.md), who had the charge of all her [gaza](../../strongs/g/g1047.md), and had [erchomai](../../strongs/g/g2064.md) to [Ierousalēm](../../strongs/g/g2419.md) [proskyneō](../../strongs/g/g4352.md),

<a name="acts_8_28"></a>Acts 8:28

Was [hypostrephō](../../strongs/g/g5290.md), and [kathēmai](../../strongs/g/g2521.md) in his [harma](../../strongs/g/g716.md) [anaginōskō](../../strongs/g/g314.md) [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md).

<a name="acts_8_29"></a>Acts 8:29

Then the [pneuma](../../strongs/g/g4151.md) [eipon](../../strongs/g/g2036.md) unto [Philippos](../../strongs/g/g5376.md), [proserchomai](../../strongs/g/g4334.md), and [kollaō](../../strongs/g/g2853.md) to this [harma](../../strongs/g/g716.md).

<a name="acts_8_30"></a>Acts 8:30

And [Philippos](../../strongs/g/g5376.md) [prostrechō](../../strongs/g/g4370.md), and [akouō](../../strongs/g/g191.md) him [anaginōskō](../../strongs/g/g314.md) the [prophētēs](../../strongs/g/g4396.md) [Ēsaïas](../../strongs/g/g2268.md), and [eipon](../../strongs/g/g2036.md), [ara](../../strongs/g/g687.md) [ge](../../strongs/g/g1065.md) [ginōskō](../../strongs/g/g1097.md) thou what thou [anaginōskō](../../strongs/g/g314.md)?

<a name="acts_8_31"></a>Acts 8:31

And he [eipon](../../strongs/g/g2036.md), How can I, except [tis](../../strongs/g/g5100.md) should [hodēgeō](../../strongs/g/g3594.md) me? And he [parakaleō](../../strongs/g/g3870.md) [Philippos](../../strongs/g/g5376.md) that he would [anabainō](../../strongs/g/g305.md) and [kathizō](../../strongs/g/g2523.md) with him.

<a name="acts_8_32"></a>Acts 8:32

The [periochē](../../strongs/g/g4042.md) of the [graphē](../../strongs/g/g1124.md) which he [anaginōskō](../../strongs/g/g314.md) was this, He was [agō](../../strongs/g/g71.md) as a [probaton](../../strongs/g/g4263.md) to the [sphagē](../../strongs/g/g4967.md); and like an [amnos](../../strongs/g/g286.md) [aphōnos](../../strongs/g/g880.md) [enantion](../../strongs/g/g1726.md) his [keirō](../../strongs/g/g2751.md), so [anoigō](../../strongs/g/g455.md) he not his [stoma](../../strongs/g/g4750.md):

<a name="acts_8_33"></a>Acts 8:33

In his [tapeinōsis](../../strongs/g/g5014.md) his [krisis](../../strongs/g/g2920.md) was [airō](../../strongs/g/g142.md): and who shall [diēgeomai](../../strongs/g/g1334.md) his [genea](../../strongs/g/g1074.md)? for his [zōē](../../strongs/g/g2222.md) is [airō](../../strongs/g/g142.md) from the [gē](../../strongs/g/g1093.md).

<a name="acts_8_34"></a>Acts 8:34

And the [eunouchos](../../strongs/g/g2135.md) [apokrinomai](../../strongs/g/g611.md) [Philippos](../../strongs/g/g5376.md), and [eipon](../../strongs/g/g2036.md), I [deomai](../../strongs/g/g1189.md) thee, of whom [legō](../../strongs/g/g3004.md) the [prophētēs](../../strongs/g/g4396.md) this? of himself, or of some other man?

<a name="acts_8_35"></a>Acts 8:35

Then [Philippos](../../strongs/g/g5376.md) [anoigō](../../strongs/g/g455.md) his [stoma](../../strongs/g/g4750.md), and [archomai](../../strongs/g/g756.md) at the same [graphē](../../strongs/g/g1124.md), and [euaggelizō](../../strongs/g/g2097.md) unto him [Iēsous](../../strongs/g/g2424.md).

<a name="acts_8_36"></a>Acts 8:36

And as they [poreuō](../../strongs/g/g4198.md) on [hodos](../../strongs/g/g3598.md), they [erchomai](../../strongs/g/g2064.md) unto a certain [hydōr](../../strongs/g/g5204.md): and the [eunouchos](../../strongs/g/g2135.md) [phēmi](../../strongs/g/g5346.md), [idou](../../strongs/g/g2400.md), here is [hydōr](../../strongs/g/g5204.md); what doth [kōlyō](../../strongs/g/g2967.md) me to be [baptizō](../../strongs/g/g907.md)?

<a name="acts_8_37"></a>Acts 8:37

And [Philippos](../../strongs/g/g5376.md) [eipon](../../strongs/g/g2036.md), If thou [pisteuō](../../strongs/g/g4100.md) with all thine [kardia](../../strongs/g/g2588.md), thou [exesti](../../strongs/g/g1832.md). And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), I [pisteuō](../../strongs/g/g4100.md) that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md). [^1]

<a name="acts_8_38"></a>Acts 8:38

And he [keleuō](../../strongs/g/g2753.md) the [harma](../../strongs/g/g716.md) to [histēmi](../../strongs/g/g2476.md): and they [katabainō](../../strongs/g/g2597.md) [amphoteroi](../../strongs/g/g297.md) into the [hydōr](../../strongs/g/g5204.md), both [Philippos](../../strongs/g/g5376.md) and the [eunouchos](../../strongs/g/g2135.md); and he [baptizō](../../strongs/g/g907.md) him.

<a name="acts_8_39"></a>Acts 8:39

And when they were [anabainō](../../strongs/g/g305.md) out of the [hydōr](../../strongs/g/g5204.md), the [pneuma](../../strongs/g/g4151.md) of the [kyrios](../../strongs/g/g2962.md) [harpazō](../../strongs/g/g726.md) [Philippos](../../strongs/g/g5376.md), that the [eunouchos](../../strongs/g/g2135.md) [eidō](../../strongs/g/g1492.md) him no more: and he [poreuō](../../strongs/g/g4198.md) on his [hodos](../../strongs/g/g3598.md) [chairō](../../strongs/g/g5463.md).

<a name="acts_8_40"></a>Acts 8:40

But [Philippos](../../strongs/g/g5376.md) was [heuriskō](../../strongs/g/g2147.md) at [Azōtos](../../strongs/g/g108.md): and [dierchomai](../../strongs/g/g1330.md) through he [euaggelizō](../../strongs/g/g2097.md) in all the [polis](../../strongs/g/g4172.md), till he [erchomai](../../strongs/g/g2064.md) to [Kaisareia](../../strongs/g/g2542.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 7](acts_7.md) - [Acts 9](acts_9.md)

---

[^1]: [Acts 8:37 Commentary](../../commentary/acts/acts_8_commentary.md#acts_8_37)
