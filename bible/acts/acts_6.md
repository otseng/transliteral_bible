# [Acts 6](https://www.blueletterbible.org/kjv/act/6/1/s_1024001)

<a name="acts_6_1"></a>Acts 6:1

And in those [hēmera](../../strongs/g/g2250.md), when the [mathētēs](../../strongs/g/g3101.md) was [plēthynō](../../strongs/g/g4129.md), there arose a [goggysmos](../../strongs/g/g1112.md) of the [Hellēnistēs](../../strongs/g/g1675.md) against the [Hebraios](../../strongs/g/g1445.md), because their [chēra](../../strongs/g/g5503.md) were [paratheōreō](../../strongs/g/g3865.md) in the [kathēmerinos](../../strongs/g/g2522.md) [diakonia](../../strongs/g/g1248.md).

<a name="acts_6_2"></a>Acts 6:2

Then the [dōdeka](../../strongs/g/g1427.md) [proskaleō](../../strongs/g/g4341.md) the [plēthos](../../strongs/g/g4128.md) of the [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md), It is not [arestos](../../strongs/g/g701.md) that we should [kataleipō](../../strongs/g/g2641.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and [diakoneō](../../strongs/g/g1247.md) [trapeza](../../strongs/g/g5132.md).

<a name="acts_6_3"></a>Acts 6:3

[oun](../../strongs/g/g3767.md), [adelphos](../../strongs/g/g80.md), [episkeptomai](../../strongs/g/g1980.md) among you seven [anēr](../../strongs/g/g435.md) [martyreō](../../strongs/g/g3140.md), [plērēs](../../strongs/g/g4134.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) and [sophia](../../strongs/g/g4678.md), whom we may [kathistēmi](../../strongs/g/g2525.md) over this [chreia](../../strongs/g/g5532.md).

<a name="acts_6_4"></a>Acts 6:4

But we will [proskartereō](../../strongs/g/g4342.md) to [proseuchē](../../strongs/g/g4335.md), and to the [diakonia](../../strongs/g/g1248.md) of the [logos](../../strongs/g/g3056.md).

<a name="acts_6_5"></a>Acts 6:5

And the [logos](../../strongs/g/g3056.md) [areskō](../../strongs/g/g700.md) [enōpion](../../strongs/g/g1799.md) [pas](../../strongs/g/g3956.md) [plēthos](../../strongs/g/g4128.md): and they [eklegomai](../../strongs/g/g1586.md) [Stephanos](../../strongs/g/g4736.md), an [anēr](../../strongs/g/g435.md) [plērēs](../../strongs/g/g4134.md) of [pistis](../../strongs/g/g4102.md) and of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and [Philippos](../../strongs/g/g5376.md), and [Prochoros](../../strongs/g/g4402.md), and [Nikanōr](../../strongs/g/g3527.md), and [Timōn](../../strongs/g/g5096.md), and [Parmenas](../../strongs/g/g3937.md), and [Nikolaos](../../strongs/g/g3532.md) a [prosēlytos](../../strongs/g/g4339.md) [Antiocheus](../../strongs/g/g491.md):

<a name="acts_6_6"></a>Acts 6:6

Whom they [histēmi](../../strongs/g/g2476.md) before the [apostolos](../../strongs/g/g652.md): and when they had [proseuchomai](../../strongs/g/g4336.md), they [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) on them.

<a name="acts_6_7"></a>Acts 6:7

And the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) [auxanō](../../strongs/g/g837.md); and the [arithmos](../../strongs/g/g706.md) of the [mathētēs](../../strongs/g/g3101.md) [plēthynō](../../strongs/g/g4129.md) in [Ierousalēm](../../strongs/g/g2419.md) [sphodra](../../strongs/g/g4970.md); and a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) of the [hiereus](../../strongs/g/g2409.md) were [hypakouō](../../strongs/g/g5219.md) to the [pistis](../../strongs/g/g4102.md).

<a name="acts_6_8"></a>Acts 6:8

And [Stephanos](../../strongs/g/g4736.md), [plērēs](../../strongs/g/g4134.md) of [pistis](../../strongs/g/g4102.md) and [dynamis](../../strongs/g/g1411.md), [poieō](../../strongs/g/g4160.md) [megas](../../strongs/g/g3173.md) [teras](../../strongs/g/g5059.md) and [sēmeion](../../strongs/g/g4592.md) among the [laos](../../strongs/g/g2992.md).

<a name="acts_6_9"></a>Acts 6:9

Then there [anistēmi](../../strongs/g/g450.md) certain of the [synagōgē](../../strongs/g/g4864.md), which is [legō](../../strongs/g/g3004.md) the [Libertinos](../../strongs/g/g3032.md), and [Kyrēnaios](../../strongs/g/g2956.md), and [Alexandreus](../../strongs/g/g221.md), and of them of [Kilikia](../../strongs/g/g2791.md) and of [Asia](../../strongs/g/g773.md), [syzēteō](../../strongs/g/g4802.md) with [Stephanos](../../strongs/g/g4736.md).

<a name="acts_6_10"></a>Acts 6:10

And they were not able to [anthistēmi](../../strongs/g/g436.md) the [sophia](../../strongs/g/g4678.md) and the [pneuma](../../strongs/g/g4151.md) by which he [laleō](../../strongs/g/g2980.md).

<a name="acts_6_11"></a>Acts 6:11

Then they [hypoballō](../../strongs/g/g5260.md) [anēr](../../strongs/g/g435.md), which [legō](../../strongs/g/g3004.md), We have [akouō](../../strongs/g/g191.md) him [laleō](../../strongs/g/g2980.md) [blasphēmos](../../strongs/g/g989.md) [rhēma](../../strongs/g/g4487.md) against [Mōÿsēs](../../strongs/g/g3475.md), and [theos](../../strongs/g/g2316.md).

<a name="acts_6_12"></a>Acts 6:12

And they [sygkineō](../../strongs/g/g4787.md) the [laos](../../strongs/g/g2992.md), and the [presbyteros](../../strongs/g/g4245.md), and the [grammateus](../../strongs/g/g1122.md), and [ephistēmi](../../strongs/g/g2186.md), and [synarpazō](../../strongs/g/g4884.md) him, and [agō](../../strongs/g/g71.md) to the [synedrion](../../strongs/g/g4892.md),

<a name="acts_6_13"></a>Acts 6:13

And [histēmi](../../strongs/g/g2476.md) [pseudēs](../../strongs/g/g5571.md) [martys](../../strongs/g/g3144.md), which [legō](../../strongs/g/g3004.md), This [anthrōpos](../../strongs/g/g444.md) [pauō](../../strongs/g/g3973.md) not to [laleō](../../strongs/g/g2980.md) [blasphēmos](../../strongs/g/g989.md) [rhēma](../../strongs/g/g4487.md) against this [hagios](../../strongs/g/g40.md) [topos](../../strongs/g/g5117.md), and the [nomos](../../strongs/g/g3551.md):

<a name="acts_6_14"></a>Acts 6:14

For we have [akouō](../../strongs/g/g191.md) him [legō](../../strongs/g/g3004.md), that this [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md) shall [katalyō](../../strongs/g/g2647.md) this [topos](../../strongs/g/g5117.md), and shall [allassō](../../strongs/g/g236.md) the [ethos](../../strongs/g/g1485.md) which [Mōÿsēs](../../strongs/g/g3475.md) [paradidōmi](../../strongs/g/g3860.md) us.

<a name="acts_6_15"></a>Acts 6:15

And all that [kathezomai](../../strongs/g/g2516.md) in the [synedrion](../../strongs/g/g4892.md), [atenizō](../../strongs/g/g816.md) on him, [eidō](../../strongs/g/g1492.md) his [prosōpon](../../strongs/g/g4383.md) as it had been the [prosōpon](../../strongs/g/g4383.md) of an [aggelos](../../strongs/g/g32.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 5](acts_5.md) - [Acts 7](acts_7.md)