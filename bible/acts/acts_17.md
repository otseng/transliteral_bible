# [Acts 17](https://www.blueletterbible.org/kjv/act/17/1/s_1035001)

<a name="acts_17_1"></a>Acts 17:1

Now when they had [diodeuō](../../strongs/g/g1353.md) [Amphipolis](../../strongs/g/g295.md) and [Apollōnia](../../strongs/g/g624.md), they [erchomai](../../strongs/g/g2064.md) to [Thessalonikē](../../strongs/g/g2332.md), where was a [synagōgē](../../strongs/g/g4864.md) of the [Ioudaios](../../strongs/g/g2453.md):

<a name="acts_17_2"></a>Acts 17:2

And [Paulos](../../strongs/g/g3972.md), as his [ethō](../../strongs/g/g1486.md), [eiserchomai](../../strongs/g/g1525.md) unto them, and three [sabbaton](../../strongs/g/g4521.md) [dialegomai](../../strongs/g/g1256.md) them out of the [graphē](../../strongs/g/g1124.md),

<a name="acts_17_3"></a>Acts 17:3

[dianoigō](../../strongs/g/g1272.md) and [paratithēmi](../../strongs/g/g3908.md), that [Christos](../../strongs/g/g5547.md) [dei](../../strongs/g/g1163.md) have [paschō](../../strongs/g/g3958.md), and [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md); and that this [Iēsous](../../strongs/g/g2424.md), whom I [kataggellō](../../strongs/g/g2605.md) unto you, is [Christos](../../strongs/g/g5547.md).

<a name="acts_17_4"></a>Acts 17:4

And some of them [peithō](../../strongs/g/g3982.md), and [prosklēroō](../../strongs/g/g4345.md) with [Paulos](../../strongs/g/g3972.md) and [Silas](../../strongs/g/g4609.md); and of the [sebō](../../strongs/g/g4576.md) [Hellēn](../../strongs/g/g1672.md) a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md), and of the [prōtos](../../strongs/g/g4413.md) [gynē](../../strongs/g/g1135.md) not a [oligos](../../strongs/g/g3641.md).

<a name="acts_17_5"></a>Acts 17:5

But the [Ioudaios](../../strongs/g/g2453.md) which [apeitheō](../../strongs/g/g544.md), [zēloō](../../strongs/g/g2206.md), [proslambanō](../../strongs/g/g4355.md) unto them certain [ponēros](../../strongs/g/g4190.md) [anēr](../../strongs/g/g435.md) of [agoraios](../../strongs/g/g60.md), and [ochlopoieō](../../strongs/g/g3792.md), and [thorybeō](../../strongs/g/g2350.md) the [polis](../../strongs/g/g4172.md), and [ephistēmi](../../strongs/g/g2186.md) the [oikia](../../strongs/g/g3614.md) of [Iasōn](../../strongs/g/g2394.md), and [zēteō](../../strongs/g/g2212.md) to [agō](../../strongs/g/g71.md) them out to the [dēmos](../../strongs/g/g1218.md).

<a name="acts_17_6"></a>Acts 17:6

And when they [heuriskō](../../strongs/g/g2147.md) them not, they [syrō](../../strongs/g/g4951.md) [Iasōn](../../strongs/g/g2394.md) and certain [adelphos](../../strongs/g/g80.md) unto the [politarchēs](../../strongs/g/g4173.md), [boaō](../../strongs/g/g994.md), These that have [anastatoō](../../strongs/g/g387.md) [oikoumenē](../../strongs/g/g3625.md) are [pareimi](../../strongs/g/g3918.md) [enthade](../../strongs/g/g1759.md) also;

<a name="acts_17_7"></a>Acts 17:7

Whom [Iasōn](../../strongs/g/g2394.md) hath [hypodechomai](../../strongs/g/g5264.md): and these all [prassō](../../strongs/g/g4238.md) [apenanti](../../strongs/g/g561.md) to the [dogma](../../strongs/g/g1378.md) of [Kaisar](../../strongs/g/g2541.md), [legō](../../strongs/g/g3004.md) that there is another [basileus](../../strongs/g/g935.md), [Iēsous](../../strongs/g/g2424.md).

<a name="acts_17_8"></a>Acts 17:8

And they [tarassō](../../strongs/g/g5015.md) the [ochlos](../../strongs/g/g3793.md) and the [politarchēs](../../strongs/g/g4173.md), when they [akouō](../../strongs/g/g191.md) these things.

<a name="acts_17_9"></a>Acts 17:9

And when they had [lambanō](../../strongs/g/g2983.md) [hikanos](../../strongs/g/g2425.md) of [Iasōn](../../strongs/g/g2394.md), and of the [loipos](../../strongs/g/g3062.md), they [apolyō](../../strongs/g/g630.md) them.

<a name="acts_17_10"></a>Acts 17:10

And the [adelphos](../../strongs/g/g80.md) [eutheōs](../../strongs/g/g2112.md) [ekpempō](../../strongs/g/g1599.md) [Paulos](../../strongs/g/g3972.md) and [Silas](../../strongs/g/g4609.md) by [nyx](../../strongs/g/g3571.md) unto [Beroia](../../strongs/g/g960.md): who [paraginomai](../../strongs/g/g3854.md) [apeimi](../../strongs/g/g549.md) into the [synagōgē](../../strongs/g/g4864.md) of the [Ioudaios](../../strongs/g/g2453.md).

<a name="acts_17_11"></a>Acts 17:11

These were [eugenēs](../../strongs/g/g2104.md) than those in [Thessalonikē](../../strongs/g/g2332.md), in that they [dechomai](../../strongs/g/g1209.md) the [logos](../../strongs/g/g3056.md) with all [prothymia](../../strongs/g/g4288.md), and [anakrinō](../../strongs/g/g350.md) the [graphē](../../strongs/g/g1124.md) daily, whether those things were so.

<a name="acts_17_12"></a>Acts 17:12

Therefore [polys](../../strongs/g/g4183.md) of them [pisteuō](../../strongs/g/g4100.md); also [euschēmōn](../../strongs/g/g2158.md) [gynē](../../strongs/g/g1135.md) which were [Hellēnis](../../strongs/g/g1674.md), and of [anēr](../../strongs/g/g435.md), not a [oligos](../../strongs/g/g3641.md).

<a name="acts_17_13"></a>Acts 17:13

But when the [Ioudaios](../../strongs/g/g2453.md) of [Thessalonikē](../../strongs/g/g2332.md) had [ginōskō](../../strongs/g/g1097.md) that the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) was [kataggellō](../../strongs/g/g2605.md) of [Paulos](../../strongs/g/g3972.md) at [Beroia](../../strongs/g/g960.md), they [erchomai](../../strongs/g/g2064.md) thither also, and [saleuō](../../strongs/g/g4531.md) the [ochlos](../../strongs/g/g3793.md).

<a name="acts_17_14"></a>Acts 17:14

And then [eutheōs](../../strongs/g/g2112.md) the [adelphos](../../strongs/g/g80.md) [exapostellō](../../strongs/g/g1821.md) [Paulos](../../strongs/g/g3972.md) to [poreuō](../../strongs/g/g4198.md) as it were to the [thalassa](../../strongs/g/g2281.md): but [Silas](../../strongs/g/g4609.md) and [Timotheos](../../strongs/g/g5095.md) [hypomenō](../../strongs/g/g5278.md) there.

<a name="acts_17_15"></a>Acts 17:15

And they that [kathistēmi](../../strongs/g/g2525.md) [Paulos](../../strongs/g/g3972.md) [agō](../../strongs/g/g71.md) him unto [Athēnai](../../strongs/g/g116.md): and [lambanō](../../strongs/g/g2983.md) an [entolē](../../strongs/g/g1785.md) unto [Silas](../../strongs/g/g4609.md) and [Timotheos](../../strongs/g/g5095.md) for to [erchomai](../../strongs/g/g2064.md) to him [tachista](../../strongs/g/g5033.md), they [exeimi](../../strongs/g/g1826.md).

<a name="acts_17_16"></a>Acts 17:16

Now while [Paulos](../../strongs/g/g3972.md) [ekdechomai](../../strongs/g/g1551.md) them at [Athēnai](../../strongs/g/g116.md), his [pneuma](../../strongs/g/g4151.md) was [paroxynō](../../strongs/g/g3947.md) in him, when he [theōreō](../../strongs/g/g2334.md) the [polis](../../strongs/g/g4172.md) [kateidōlos](../../strongs/g/g2712.md).

<a name="acts_17_17"></a>Acts 17:17

Therefore [dialegomai](../../strongs/g/g1256.md) he in the [synagōgē](../../strongs/g/g4864.md) with the [Ioudaios](../../strongs/g/g2453.md), and with the [sebō](../../strongs/g/g4576.md), and in the [agora](../../strongs/g/g58.md) daily with [paratygchanō](../../strongs/g/g3909.md).

<a name="acts_17_18"></a>Acts 17:18

Then certain [philosophos](../../strongs/g/g5386.md) of the [Epikoureios](../../strongs/g/g1946.md), and of the [Stoïkos](../../strongs/g/g4770.md), [symballō](../../strongs/g/g4820.md) him. And some [legō](../../strongs/g/g3004.md), What will this [spermologos](../../strongs/g/g4691.md) [legō](../../strongs/g/g3004.md)? other some, He [dokeō](../../strongs/g/g1380.md) to be a [kataggeleus](../../strongs/g/g2604.md) of [xenos](../../strongs/g/g3581.md) [daimonion](../../strongs/g/g1140.md): because he [euaggelizō](../../strongs/g/g2097.md) unto them [Iēsous](../../strongs/g/g2424.md), and the [anastasis](../../strongs/g/g386.md).

<a name="acts_17_19"></a>Acts 17:19

And they [epilambanomai](../../strongs/g/g1949.md) him, and [agō](../../strongs/g/g71.md) him unto [Areios pagos](../../strongs/g/g697.md), [legō](../../strongs/g/g3004.md), May we [ginōskō](../../strongs/g/g1097.md) what this [kainos](../../strongs/g/g2537.md) [didachē](../../strongs/g/g1322.md), whereof thou [laleō](../../strongs/g/g2980.md), is?

<a name="acts_17_20"></a>Acts 17:20

For thou [eispherō](../../strongs/g/g1533.md) certain [xenizō](../../strongs/g/g3579.md) to our [akoē](../../strongs/g/g189.md): we [boulomai](../../strongs/g/g1014.md) [ginōskō](../../strongs/g/g1097.md) therefore what these things [an](../../strongs/g/g302.md) [thelō](../../strongs/g/g2309.md) [einai](../../strongs/g/g1511.md).

<a name="acts_17_21"></a>Acts 17:21

(For all the [Athēnaios](../../strongs/g/g117.md) and [xenos](../../strongs/g/g3581.md) which [epidēmeō](../../strongs/g/g1927.md) [eukaireō](../../strongs/g/g2119.md) in nothing else, but either to [legō](../../strongs/g/g3004.md), or to [akouō](../../strongs/g/g191.md) some [kainos](../../strongs/g/g2537.md).)

<a name="acts_17_22"></a>Acts 17:22

Then [Paulos](../../strongs/g/g3972.md) [histēmi](../../strongs/g/g2476.md) in the midst of [Areios pagos](../../strongs/g/g697.md), and [phēmi](../../strongs/g/g5346.md), Ye [anēr](../../strongs/g/g435.md) of [Athēnaios](../../strongs/g/g117.md), I [theōreō](../../strongs/g/g2334.md) that in all things ye are [deisidaimōn](../../strongs/g/g1174.md).

<a name="acts_17_23"></a>Acts 17:23

For as I [dierchomai](../../strongs/g/g1330.md), and [anatheōreō](../../strongs/g/g333.md) your [sebasma](../../strongs/g/g4574.md), I [heuriskō](../../strongs/g/g2147.md) a [bōmos](../../strongs/g/g1041.md) with this [epigraphō](../../strongs/g/g1924.md), [agnōstos](../../strongs/g/g57.md) [theos](../../strongs/g/g2316.md). Whom therefore ye [agnoeō](../../strongs/g/g50.md) [eusebeō](../../strongs/g/g2151.md), him [kataggellō](../../strongs/g/g2605.md) I unto you.

<a name="acts_17_24"></a>Acts 17:24

[theos](../../strongs/g/g2316.md) that [poieō](../../strongs/g/g4160.md) the [kosmos](../../strongs/g/g2889.md) and all things therein, seeing that he is [kyrios](../../strongs/g/g2962.md) of [ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md), [katoikeō](../../strongs/g/g2730.md) not in [naos](../../strongs/g/g3485.md) [cheiropoiētos](../../strongs/g/g5499.md);

<a name="acts_17_25"></a>Acts 17:25

Neither is [therapeuō](../../strongs/g/g2323.md) with [anthrōpos](../../strongs/g/g444.md) [cheir](../../strongs/g/g5495.md), as though he [prosdeomai](../../strongs/g/g4326.md) any thing, seeing he [didōmi](../../strongs/g/g1325.md) to all [zōē](../../strongs/g/g2222.md), and [pnoē](../../strongs/g/g4157.md), and all things;

<a name="acts_17_26"></a>Acts 17:26

And hath [poieō](../../strongs/g/g4160.md) of one [haima](../../strongs/g/g129.md) all [ethnos](../../strongs/g/g1484.md) of [anthrōpos](../../strongs/g/g444.md) for to [katoikeō](../../strongs/g/g2730.md) on all the [prosōpon](../../strongs/g/g4383.md) of [gē](../../strongs/g/g1093.md), and hath [horizō](../../strongs/g/g3724.md) the [kairos](../../strongs/g/g2540.md) before [protassō](../../strongs/g/g4384.md), and the [horothesia](../../strongs/g/g3734.md) of their [katoikia](../../strongs/g/g2733.md);

<a name="acts_17_27"></a>Acts 17:27

That they should [zēteō](../../strongs/g/g2212.md) the [kyrios](../../strongs/g/g2962.md), if haply they might [psēlaphaō](../../strongs/g/g5584.md) after him, and [heuriskō](../../strongs/g/g2147.md) him, [kaitoige](../../strongs/g/g2544.md) he be not [makran](../../strongs/g/g3112.md) from every one of us:

<a name="acts_17_28"></a>Acts 17:28

For in him we [zaō](../../strongs/g/g2198.md), and [kineō](../../strongs/g/g2795.md), and [esmen](../../strongs/g/g2070.md); as certain also of your own [poiētēs](../../strongs/g/g4163.md) have [eipon](../../strongs/g/g2046.md), For we are also his [genos](../../strongs/g/g1085.md).

<a name="acts_17_29"></a>Acts 17:29

Forasmuch then as we are the [genos](../../strongs/g/g1085.md) of [theos](../../strongs/g/g2316.md), we [opheilō](../../strongs/g/g3784.md) not to [nomizō](../../strongs/g/g3543.md) that the [theios](../../strongs/g/g2304.md) is [homoios](../../strongs/g/g3664.md) unto [chrysos](../../strongs/g/g5557.md), or [argyros](../../strongs/g/g696.md), or [lithos](../../strongs/g/g3037.md), [charagma](../../strongs/g/g5480.md) by [technē](../../strongs/g/g5078.md) and [anthrōpos](../../strongs/g/g444.md) [enthymēsis](../../strongs/g/g1761.md).

<a name="acts_17_30"></a>Acts 17:30

And the [chronos](../../strongs/g/g5550.md) of this [agnoia](../../strongs/g/g52.md) [theos](../../strongs/g/g2316.md) [hyperoraō](../../strongs/g/g5237.md); but [ta nyn](../../strongs/g/g3569.md) [paraggellō](../../strongs/g/g3853.md) all [anthrōpos](../../strongs/g/g444.md) [pantachou](../../strongs/g/g3837.md) to [metanoeō](../../strongs/g/g3340.md):

<a name="acts_17_31"></a>Acts 17:31

Because he hath [histēmi](../../strongs/g/g2476.md) a day, in the which he will [krinō](../../strongs/g/g2919.md) the [oikoumenē](../../strongs/g/g3625.md) in [dikaiosynē](../../strongs/g/g1343.md) by that [anēr](../../strongs/g/g435.md) whom he hath [horizō](../../strongs/g/g3724.md); whereof he hath [parechō](../../strongs/g/g3930.md) [pistis](../../strongs/g/g4102.md) unto all, in that he hath [anistēmi](../../strongs/g/g450.md) him from the [nekros](../../strongs/g/g3498.md).

<a name="acts_17_32"></a>Acts 17:32

And when they [akouō](../../strongs/g/g191.md) of the [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md), some [chleuazō](../../strongs/g/g5512.md): and others [eipon](../../strongs/g/g2036.md), We will [akouō](../../strongs/g/g191.md) thee again of this.

<a name="acts_17_33"></a>Acts 17:33

So [Paulos](../../strongs/g/g3972.md) [exerchomai](../../strongs/g/g1831.md) from among them.

<a name="acts_17_34"></a>Acts 17:34

Howbeit certain [anēr](../../strongs/g/g435.md) [kollaō](../../strongs/g/g2853.md) unto him, and [pisteuō](../../strongs/g/g4100.md): among the which was [Dionysios](../../strongs/g/g1354.md) the [Areopagitēs](../../strongs/g/g698.md), and a [gynē](../../strongs/g/g1135.md) [onoma](../../strongs/g/g3686.md) [Damaris](../../strongs/g/g1152.md), and others with them.

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 16](acts_16.md) - [Acts 18](acts_18.md)