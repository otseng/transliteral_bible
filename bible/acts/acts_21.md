# [Acts 21](https://www.blueletterbible.org/kjv/act/21/1/s_1039001)

<a name="acts_21_1"></a>Acts 21:1

And [ginomai](../../strongs/g/g1096.md), that after we were [apospaō](../../strongs/g/g645.md) from them, and had [anagō](../../strongs/g/g321.md), we [erchomai](../../strongs/g/g2064.md) [euthydromeō](../../strongs/g/g2113.md) unto [Kōs](../../strongs/g/g2972.md), and the [hexēs](../../strongs/g/g1836.md) unto [Rhodos](../../strongs/g/g4499.md), and from thence unto [Patara](../../strongs/g/g3959.md):

<a name="acts_21_2"></a>Acts 21:2

And [heuriskō](../../strongs/g/g2147.md) a [ploion](../../strongs/g/g4143.md) [diaperaō](../../strongs/g/g1276.md) unto [Phoinikē](../../strongs/g/g5403.md), we [epibainō](../../strongs/g/g1910.md), and [anagō](../../strongs/g/g321.md).

<a name="acts_21_3"></a>Acts 21:3

Now when we had [anaphainō](../../strongs/g/g398.md) [Kypros](../../strongs/g/g2954.md), we [kataleipō](../../strongs/g/g2641.md) it on the [euōnymos](../../strongs/g/g2176.md), and [pleō](../../strongs/g/g4126.md) into [Syria](../../strongs/g/g4947.md), and [katagō](../../strongs/g/g2609.md) at [Tyros](../../strongs/g/g5184.md): for there the [ploion](../../strongs/g/g4143.md) was to [apophortizomai](../../strongs/g/g670.md) her [gomos](../../strongs/g/g1117.md).

<a name="acts_21_4"></a>Acts 21:4

And [aneuriskō](../../strongs/g/g429.md) [mathētēs](../../strongs/g/g3101.md), we [epimenō](../../strongs/g/g1961.md) there seven [hēmera](../../strongs/g/g2250.md): who [legō](../../strongs/g/g3004.md) to [Paulos](../../strongs/g/g3972.md) through the [pneuma](../../strongs/g/g4151.md), that he should not [anabainō](../../strongs/g/g305.md) to [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_21_5"></a>Acts 21:5

And when we had [exartizō](../../strongs/g/g1822.md) those [hēmera](../../strongs/g/g2250.md), we [exerchomai](../../strongs/g/g1831.md) and [poreuō](../../strongs/g/g4198.md); and they all [propempō](../../strongs/g/g4311.md) us, with [gynē](../../strongs/g/g1135.md) and [teknon](../../strongs/g/g5043.md), till we were out of the [polis](../../strongs/g/g4172.md): and we [tithēmi](../../strongs/g/g5087.md) [gony](../../strongs/g/g1119.md) on the [aigialos](../../strongs/g/g123.md), and [proseuchomai](../../strongs/g/g4336.md).

<a name="acts_21_6"></a>Acts 21:6

And when we had [aspazomai](../../strongs/g/g782.md) of [allēlōn](../../strongs/g/g240.md), we [epibainō](../../strongs/g/g1910.md) [eis](../../strongs/g/g1519.md) [ploion](../../strongs/g/g4143.md); and they [hypostrephō](../../strongs/g/g5290.md) [idios](../../strongs/g/g2398.md).

<a name="acts_21_7"></a>Acts 21:7

And when we had [dianyō](../../strongs/g/g1274.md) our [ploos](../../strongs/g/g4144.md) from [Tyros](../../strongs/g/g5184.md), we [katantaō](../../strongs/g/g2658.md) to [Ptolemaïs](../../strongs/g/g4424.md), and [aspazomai](../../strongs/g/g782.md) the [adelphos](../../strongs/g/g80.md), and [menō](../../strongs/g/g3306.md) with them one [hēmera](../../strongs/g/g2250.md).

<a name="acts_21_8"></a>Acts 21:8

And the [epaurion](../../strongs/g/g1887.md) we that were of [Paulos](../../strongs/g/g3972.md) company [exerchomai](../../strongs/g/g1831.md), and [erchomai](../../strongs/g/g2064.md) unto [Kaisareia](../../strongs/g/g2542.md): and we [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) of [Philippos](../../strongs/g/g5376.md) the [euaggelistēs](../../strongs/g/g2099.md), which was one of the seven; and [menō](../../strongs/g/g3306.md) with him.

<a name="acts_21_9"></a>Acts 21:9

And [toutō](../../strongs/g/g5129.md) had four [thygatēr](../../strongs/g/g2364.md), [parthenos](../../strongs/g/g3933.md), which did [prophēteuō](../../strongs/g/g4395.md).

<a name="acts_21_10"></a>Acts 21:10

And as we [epimenō](../../strongs/g/g1961.md) there many [hēmera](../../strongs/g/g2250.md), there [katerchomai](../../strongs/g/g2718.md) from [Ioudaia](../../strongs/g/g2449.md) a certain [prophētēs](../../strongs/g/g4396.md), [onoma](../../strongs/g/g3686.md) [Agabos](../../strongs/g/g13.md).

<a name="acts_21_11"></a>Acts 21:11

And when he was [erchomai](../../strongs/g/g2064.md) unto us, he [airō](../../strongs/g/g142.md) [Paulos](../../strongs/g/g3972.md) [zōnē](../../strongs/g/g2223.md), and [deō](../../strongs/g/g1210.md) his own [cheir](../../strongs/g/g5495.md) and [pous](../../strongs/g/g4228.md), and [eipon](../../strongs/g/g2036.md), Thus [legō](../../strongs/g/g3004.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), So shall the [Ioudaios](../../strongs/g/g2453.md) at [Ierousalēm](../../strongs/g/g2419.md) [deō](../../strongs/g/g1210.md) the [anēr](../../strongs/g/g435.md) that [esti](../../strongs/g/g2076.md) this [zōnē](../../strongs/g/g2223.md), and shall [paradidōmi](../../strongs/g/g3860.md) him into the [cheir](../../strongs/g/g5495.md) of the [ethnos](../../strongs/g/g1484.md).

<a name="acts_21_12"></a>Acts 21:12

And when we [akouō](../../strongs/g/g191.md) these things, both we, and they [entopios](../../strongs/g/g1786.md), [parakaleō](../../strongs/g/g3870.md) him not to [anabainō](../../strongs/g/g305.md) to [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_21_13"></a>Acts 21:13

Then [Paulos](../../strongs/g/g3972.md) [apokrinomai](../../strongs/g/g611.md), What [poieō](../../strongs/g/g4160.md) ye to [klaiō](../../strongs/g/g2799.md) and to [synthryptō](../../strongs/g/g4919.md) mine [kardia](../../strongs/g/g2588.md)?  for I am [hetoimōs](../../strongs/g/g2093.md) not to be [deō](../../strongs/g/g1210.md) only, but also to [apothnēskō](../../strongs/g/g599.md) at [Ierousalēm](../../strongs/g/g2419.md) for the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_21_14"></a>Acts 21:14

And when he would not be [peithō](../../strongs/g/g3982.md), we [hēsychazō](../../strongs/g/g2270.md), [eipon](../../strongs/g/g2036.md), The [thelēma](../../strongs/g/g2307.md) of the [kyrios](../../strongs/g/g2962.md) [ginomai](../../strongs/g/g1096.md).

<a name="acts_21_15"></a>Acts 21:15

And after those [hēmera](../../strongs/g/g2250.md) we [aposkeuazō](../../strongs/g/g643.md), and [anabainō](../../strongs/g/g305.md) to [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_21_16"></a>Acts 21:16

There [synerchomai](../../strongs/g/g4905.md) with us also of the [mathētēs](../../strongs/g/g3101.md) of [Kaisareia](../../strongs/g/g2542.md), and [agō](../../strongs/g/g71.md) with them one [Mnasōn](../../strongs/g/g3416.md) of [Kyprios](../../strongs/g/g2953.md), an [archaios](../../strongs/g/g744.md) [mathētēs](../../strongs/g/g3101.md), with whom we should [xenizō](../../strongs/g/g3579.md).

<a name="acts_21_17"></a>Acts 21:17

And when we were [ginomai](../../strongs/g/g1096.md) to [Hierosolyma](../../strongs/g/g2414.md), the [adelphos](../../strongs/g/g80.md) [dechomai](../../strongs/g/g1209.md) us [asmenōs](../../strongs/g/g780.md).

<a name="acts_21_18"></a>Acts 21:18

And the [epeimi](../../strongs/g/g1966.md) [Paulos](../../strongs/g/g3972.md) [eiseimi](../../strongs/g/g1524.md) with us unto [Iakōbos](../../strongs/g/g2385.md); and all the [presbyteros](../../strongs/g/g4245.md) were [paraginomai](../../strongs/g/g3854.md).

<a name="acts_21_19"></a>Acts 21:19

And when he had [aspazomai](../../strongs/g/g782.md) them, he [exēgeomai](../../strongs/g/g1834.md) particularly what things [theos](../../strongs/g/g2316.md) had [poieō](../../strongs/g/g4160.md) among the [ethnos](../../strongs/g/g1484.md) by his [diakonia](../../strongs/g/g1248.md).

<a name="acts_21_20"></a>Acts 21:20

And when they [akouō](../../strongs/g/g191.md), they [doxazō](../../strongs/g/g1392.md) the [kyrios](../../strongs/g/g2962.md), and [eipon](../../strongs/g/g2036.md) unto him, Thou [theōreō](../../strongs/g/g2334.md), [adelphos](../../strongs/g/g80.md), how many thousands of [Ioudaios](../../strongs/g/g2453.md) there are which [pisteuō](../../strongs/g/g4100.md); and they are all [zēlōtēs](../../strongs/g/g2207.md) of the [nomos](../../strongs/g/g3551.md):

<a name="acts_21_21"></a>Acts 21:21

And they are [katēcheō](../../strongs/g/g2727.md) of thee, that thou [didaskō](../../strongs/g/g1321.md) all the [Ioudaios](../../strongs/g/g2453.md) which are among the [ethnos](../../strongs/g/g1484.md) to [apostasia](../../strongs/g/g646.md) [Mōÿsēs](../../strongs/g/g3475.md), [legō](../../strongs/g/g3004.md) that they ought not to [peritemnō](../../strongs/g/g4059.md) [teknon](../../strongs/g/g5043.md), neither to [peripateō](../../strongs/g/g4043.md) after the [ethos](../../strongs/g/g1485.md).

<a name="acts_21_22"></a>Acts 21:22

What is it therefore? the [plēthos](../../strongs/g/g4128.md) must [pantōs](../../strongs/g/g3843.md) [synerchomai](../../strongs/g/g4905.md): for they will [akouō](../../strongs/g/g191.md) that thou art [erchomai](../../strongs/g/g2064.md).

<a name="acts_21_23"></a>Acts 21:23

[poieō](../../strongs/g/g4160.md) therefore this that we [legō](../../strongs/g/g3004.md) to thee: We have four [anēr](../../strongs/g/g435.md) which have an [euchē](../../strongs/g/g2171.md) on them;

<a name="acts_21_24"></a>Acts 21:24

Them [paralambanō](../../strongs/g/g3880.md), and [hagnizō](../../strongs/g/g48.md) with them, and [dapanaō](../../strongs/g/g1159.md) with them, that they may [xyraō](../../strongs/g/g3587.md) their [kephalē](../../strongs/g/g2776.md): and all may [ginōskō](../../strongs/g/g1097.md) that those things, whereof they were [katēcheō](../../strongs/g/g2727.md) concerning thee, are nothing; but that thou thyself also [stoicheō](../../strongs/g/g4748.md), and [phylassō](../../strongs/g/g5442.md) the [nomos](../../strongs/g/g3551.md).

<a name="acts_21_25"></a>Acts 21:25

As touching the [ethnos](../../strongs/g/g1484.md) which [pisteuō](../../strongs/g/g4100.md), we have [epistellō](../../strongs/g/g1989.md) and [krinō](../../strongs/g/g2919.md) that they [tēreō](../../strongs/g/g5083.md) [mēdeis](../../strongs/g/g3367.md) such thing, save only that they [phylassō](../../strongs/g/g5442.md) themselves from [eidōlothytos](../../strongs/g/g1494.md), and from [haima](../../strongs/g/g129.md), and from [pniktos](../../strongs/g/g4156.md), and from [porneia](../../strongs/g/g4202.md).

<a name="acts_21_26"></a>Acts 21:26

Then [Paulos](../../strongs/g/g3972.md) [paralambanō](../../strongs/g/g3880.md) the [anēr](../../strongs/g/g435.md), and the next [hēmera](../../strongs/g/g2250.md) [hagnizō](../../strongs/g/g48.md) with them [eiseimi](../../strongs/g/g1524.md) into the [hieron](../../strongs/g/g2411.md), to [diaggellō](../../strongs/g/g1229.md) the [ekplērōsis](../../strongs/g/g1604.md) of the [hēmera](../../strongs/g/g2250.md) of [hagnismos](../../strongs/g/g49.md), until that a [prosphora](../../strongs/g/g4376.md) should be [prospherō](../../strongs/g/g4374.md) for every one of them.

<a name="acts_21_27"></a>Acts 21:27

And when the seven [hēmera](../../strongs/g/g2250.md) were almost [synteleō](../../strongs/g/g4931.md), the [Ioudaios](../../strongs/g/g2453.md) which were of [Asia](../../strongs/g/g773.md), when they [theaomai](../../strongs/g/g2300.md) him in the [hieron](../../strongs/g/g2411.md), [sygcheō](../../strongs/g/g4797.md) all the [ochlos](../../strongs/g/g3793.md), and [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on him,

<a name="acts_21_28"></a>Acts 21:28

[krazō](../../strongs/g/g2896.md), [anēr](../../strongs/g/g435.md) of [Israēlitēs](../../strongs/g/g2475.md), [boētheō](../../strongs/g/g997.md): This is the [anthrōpos](../../strongs/g/g444.md), that [didaskō](../../strongs/g/g1321.md) all [pantachou](../../strongs/g/g3837.md) against the [laos](../../strongs/g/g2992.md), and the [nomos](../../strongs/g/g3551.md), and this [topos](../../strongs/g/g5117.md): and further [eisagō](../../strongs/g/g1521.md) [Hellēn](../../strongs/g/g1672.md) also into the [hieron](../../strongs/g/g2411.md), and hath [koinoō](../../strongs/g/g2840.md) this [hagios](../../strongs/g/g40.md) [topos](../../strongs/g/g5117.md).

<a name="acts_21_29"></a>Acts 21:29

(For they had [prooraō](../../strongs/g/g4308.md) with him in the [polis](../../strongs/g/g4172.md) [Trophimos](../../strongs/g/g5161.md) an [Ephesios](../../strongs/g/g2180.md), whom they [nomizō](../../strongs/g/g3543.md) that [Paulos](../../strongs/g/g3972.md) had [eisagō](../../strongs/g/g1521.md) into the [hieron](../../strongs/g/g2411.md).)

<a name="acts_21_30"></a>Acts 21:30

And all the [polis](../../strongs/g/g4172.md) was [kineō](../../strongs/g/g2795.md), and the [laos](../../strongs/g/g2992.md) [ginomai](../../strongs/g/g1096.md) [syndromē](../../strongs/g/g4890.md): and they [epilambanomai](../../strongs/g/g1949.md) [Paulos](../../strongs/g/g3972.md), and [helkō](../../strongs/g/g1670.md) him out of the [hieron](../../strongs/g/g2411.md): and [eutheōs](../../strongs/g/g2112.md) the [thyra](../../strongs/g/g2374.md) were [kleiō](../../strongs/g/g2808.md).

<a name="acts_21_31"></a>Acts 21:31

And as they [zēteō](../../strongs/g/g2212.md) to [apokteinō](../../strongs/g/g615.md) him, [phasis](../../strongs/g/g5334.md) [anabainō](../../strongs/g/g305.md) unto the [chiliarchos](../../strongs/g/g5506.md) of the [speira](../../strongs/g/g4686.md), that all [Ierousalēm](../../strongs/g/g2419.md) was in [sygcheō](../../strongs/g/g4797.md).

<a name="acts_21_32"></a>Acts 21:32

Who [exautēs](../../strongs/g/g1824.md) [paralambanō](../../strongs/g/g3880.md) [stratiōtēs](../../strongs/g/g4757.md) and [hekatontarchēs](../../strongs/g/g1543.md), and [katatrechō](../../strongs/g/g2701.md) unto them: and when they [eidō](../../strongs/g/g1492.md) the [chiliarchos](../../strongs/g/g5506.md) and the [stratiōtēs](../../strongs/g/g4757.md), they [pauō](../../strongs/g/g3973.md) [typtō](../../strongs/g/g5180.md) of [Paulos](../../strongs/g/g3972.md).

<a name="acts_21_33"></a>Acts 21:33

Then the [chiliarchos](../../strongs/g/g5506.md) [eggizō](../../strongs/g/g1448.md), and [epilambanomai](../../strongs/g/g1949.md) him, and [keleuō](../../strongs/g/g2753.md) to be [deō](../../strongs/g/g1210.md) with two [halysis](../../strongs/g/g254.md); and [pynthanomai](../../strongs/g/g4441.md) who he was, and what he had [poieō](../../strongs/g/g4160.md).

<a name="acts_21_34"></a>Acts 21:34

And some [boaō](../../strongs/g/g994.md) one thing, some another, among the [ochlos](../../strongs/g/g3793.md): and when he could not [ginōskō](../../strongs/g/g1097.md) the [asphalēs](../../strongs/g/g804.md) for the [thorybos](../../strongs/g/g2351.md), he [keleuō](../../strongs/g/g2753.md) him to be [agō](../../strongs/g/g71.md) into the [parembolē](../../strongs/g/g3925.md).

<a name="acts_21_35"></a>Acts 21:35

And when he [ginomai](../../strongs/g/g1096.md) upon the [anabathmos](../../strongs/g/g304.md), so it [symbainō](../../strongs/g/g4819.md), that he was [bastazō](../../strongs/g/g941.md) of the [stratiōtēs](../../strongs/g/g4757.md) for the [bia](../../strongs/g/g970.md) of the [ochlos](../../strongs/g/g3793.md).

<a name="acts_21_36"></a>Acts 21:36

For the [plēthos](../../strongs/g/g4128.md) of the [laos](../../strongs/g/g2992.md) [akoloutheō](../../strongs/g/g190.md), [krazō](../../strongs/g/g2896.md), [airō](../../strongs/g/g142.md) with him.

<a name="acts_21_37"></a>Acts 21:37

And as [Paulos](../../strongs/g/g3972.md) was to be [eisagō](../../strongs/g/g1521.md) into the [parembolē](../../strongs/g/g3925.md), he [legō](../../strongs/g/g3004.md) unto the [chiliarchos](../../strongs/g/g5506.md), [exesti](../../strongs/g/g1832.md) I [eipon](../../strongs/g/g2036.md) unto thee? Who [phēmi](../../strongs/g/g5346.md), Canst thou [ginōskō](../../strongs/g/g1097.md) [Hellēnisti](../../strongs/g/g1676.md)?

<a name="acts_21_38"></a>Acts 21:38

Art not thou that [Aigyptios](../../strongs/g/g124.md), which before these [hēmera](../../strongs/g/g2250.md) [anastatoō](../../strongs/g/g387.md), and [exagō](../../strongs/g/g1806.md) into the [erēmos](../../strongs/g/g2048.md) four thousand [anēr](../../strongs/g/g435.md) that were [sikarios](../../strongs/g/g4607.md)?

<a name="acts_21_39"></a>Acts 21:39

But [Paulos](../../strongs/g/g3972.md) [eipon](../../strongs/g/g2036.md), I am an [anthrōpos](../../strongs/g/g444.md) an [Ioudaios](../../strongs/g/g2453.md) of [Tarseus](../../strongs/g/g5018.md), in [Kilikia](../../strongs/g/g2791.md), a [politēs](../../strongs/g/g4177.md) of no [asēmos](../../strongs/g/g767.md) [polis](../../strongs/g/g4172.md): and, I [deomai](../../strongs/g/g1189.md) thee, [epitrepō](../../strongs/g/g2010.md) me to [laleō](../../strongs/g/g2980.md) unto the [laos](../../strongs/g/g2992.md).

<a name="acts_21_40"></a>Acts 21:40

And when he had [epitrepō](../../strongs/g/g2010.md) him, [Paulos](../../strongs/g/g3972.md) [histēmi](../../strongs/g/g2476.md) on the [anabathmos](../../strongs/g/g304.md), and [kataseiō](../../strongs/g/g2678.md) with the [cheir](../../strongs/g/g5495.md) unto the [laos](../../strongs/g/g2992.md). And when there was [ginomai](../../strongs/g/g1096.md) a [polys](../../strongs/g/g4183.md) [sigē](../../strongs/g/g4602.md), he [prosphōneō](../../strongs/g/g4377.md) unto them in the [Hebraïs](../../strongs/g/g1446.md) [dialektos](../../strongs/g/g1258.md), [legō](../../strongs/g/g3004.md),

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 20](acts_20.md) - [Acts 22](acts_22.md)