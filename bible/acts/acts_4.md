# [Acts 4](https://www.blueletterbible.org/kjv/act/4/1/s_1022001)

<a name="acts_4_1"></a>Acts 4:1

And as they [laleō](../../strongs/g/g2980.md) unto the [laos](../../strongs/g/g2992.md), the [hiereus](../../strongs/g/g2409.md), and the [stratēgos](../../strongs/g/g4755.md) of the [hieron](../../strongs/g/g2411.md), and the [Saddoukaios](../../strongs/g/g4523.md), [ephistēmi](../../strongs/g/g2186.md) them,

<a name="acts_4_2"></a>Acts 4:2

Being [diaponeomai](../../strongs/g/g1278.md) that they [didaskō](../../strongs/g/g1321.md) the [laos](../../strongs/g/g2992.md), and [kataggellō](../../strongs/g/g2605.md) through [Iēsous](../../strongs/g/g2424.md) the [anastasis](../../strongs/g/g386.md) from the [nekros](../../strongs/g/g3498.md).

<a name="acts_4_3"></a>Acts 4:3

And they [epiballō](../../strongs/g/g1911.md) [cheir](../../strongs/g/g5495.md) on them, and [tithēmi](../../strongs/g/g5087.md) them in [tērēsis](../../strongs/g/g5084.md) unto the [aurion](../../strongs/g/g839.md): for it was now [hespera](../../strongs/g/g2073.md).

<a name="acts_4_4"></a>Acts 4:4

Howbeit [polys](../../strongs/g/g4183.md) of them which [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) [pisteuō](../../strongs/g/g4100.md); and the [arithmos](../../strongs/g/g706.md) of the [anēr](../../strongs/g/g435.md) was about five [chilias](../../strongs/g/g5505.md).

<a name="acts_4_5"></a>Acts 4:5

And [ginomai](../../strongs/g/g1096.md) on the [aurion](../../strongs/g/g839.md), that their [archōn](../../strongs/g/g758.md), and [presbyteros](../../strongs/g/g4245.md), and [grammateus](../../strongs/g/g1122.md),

<a name="acts_4_6"></a>Acts 4:6

And [Annas](../../strongs/g/g452.md) the [archiereus](../../strongs/g/g749.md), and [Kaïaphas](../../strongs/g/g2533.md), and [Iōannēs](../../strongs/g/g2491.md), and [Alexandros](../../strongs/g/g223.md), and as many as were of the [genos](../../strongs/g/g1085.md) of the [archieratikos](../../strongs/g/g748.md), were [synagō](../../strongs/g/g4863.md) at [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_4_7"></a>Acts 4:7

And when they had [histēmi](../../strongs/g/g2476.md) them in the midst, they [pynthanomai](../../strongs/g/g4441.md), By what [dynamis](../../strongs/g/g1411.md), or by what [onoma](../../strongs/g/g3686.md), have ye [poieō](../../strongs/g/g4160.md) this?

<a name="acts_4_8"></a>Acts 4:8

Then [Petros](../../strongs/g/g4074.md), [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), [eipon](../../strongs/g/g2036.md) unto them, Ye [archōn](../../strongs/g/g758.md) of the [laos](../../strongs/g/g2992.md), and [presbyteros](../../strongs/g/g4245.md) of [Israēl](../../strongs/g/g2474.md),

<a name="acts_4_9"></a>Acts 4:9

If we [sēmeron](../../strongs/g/g4594.md) be [anakrinō](../../strongs/g/g350.md) of the [euergesia](../../strongs/g/g2108.md) to the [asthenēs](../../strongs/g/g772.md) [anthrōpos](../../strongs/g/g444.md), by what means he is [sōzō](../../strongs/g/g4982.md);

<a name="acts_4_10"></a>Acts 4:10

Be it [gnōstos](../../strongs/g/g1110.md) unto you all, and to all the [laos](../../strongs/g/g2992.md) of [Israēl](../../strongs/g/g2474.md), that by the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) of [Nazōraios](../../strongs/g/g3480.md), whom ye [stauroō](../../strongs/g/g4717.md), whom [theos](../../strongs/g/g2316.md) [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), even by him doth [houtos](../../strongs/g/g3778.md) [paristēmi](../../strongs/g/g3936.md) before you [hygiēs](../../strongs/g/g5199.md).

<a name="acts_4_11"></a>Acts 4:11

This is the [lithos](../../strongs/g/g3037.md) which was [exoutheneō](../../strongs/g/g1848.md) of you [oikodomeō](../../strongs/g/g3618.md), which is become the [kephalē](../../strongs/g/g2776.md) of the [gōnia](../../strongs/g/g1137.md).

<a name="acts_4_12"></a>Acts 4:12

Neither is there [sōtēria](../../strongs/g/g4991.md) in any other: for there is none other [onoma](../../strongs/g/g3686.md) under [ouranos](../../strongs/g/g3772.md) [didōmi](../../strongs/g/g1325.md) among [anthrōpos](../../strongs/g/g444.md), whereby we must be [sōzō](../../strongs/g/g4982.md).

<a name="acts_4_13"></a>Acts 4:13

Now when they [theōreō](../../strongs/g/g2334.md) the [parrēsia](../../strongs/g/g3954.md) of [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md), and [katalambanō](../../strongs/g/g2638.md) that they were [agrammatos](../../strongs/g/g62.md) and [idiōtēs](../../strongs/g/g2399.md) [anthrōpos](../../strongs/g/g444.md), they [thaumazō](../../strongs/g/g2296.md); and they [epiginōskō](../../strongs/g/g1921.md) of them, that they had been with [Iēsous](../../strongs/g/g2424.md).

<a name="acts_4_14"></a>Acts 4:14

And [blepō](../../strongs/g/g991.md) the [anthrōpos](../../strongs/g/g444.md) which was [therapeuō](../../strongs/g/g2323.md) [histēmi](../../strongs/g/g2476.md) with them, they could [anteipon](../../strongs/g/g471.md) nothing.

<a name="acts_4_15"></a>Acts 4:15

But when they had [keleuō](../../strongs/g/g2753.md) them to [aperchomai](../../strongs/g/g565.md) out of the [synedrion](../../strongs/g/g4892.md), they [symballō](../../strongs/g/g4820.md) among [allēlōn](../../strongs/g/g240.md),

<a name="acts_4_16"></a>Acts 4:16

[legō](../../strongs/g/g3004.md), What shall we [poieō](../../strongs/g/g4160.md) to these [anthrōpos](../../strongs/g/g444.md)? for that indeed a [gnōstos](../../strongs/g/g1110.md) [sēmeion](../../strongs/g/g4592.md) hath been [ginomai](../../strongs/g/g1096.md) by them [phaneros](../../strongs/g/g5318.md) to all them that [katoikeō](../../strongs/g/g2730.md) in [Ierousalēm](../../strongs/g/g2419.md); and we cannot [arneomai](../../strongs/g/g720.md).

<a name="acts_4_17"></a>Acts 4:17

But that it [dianemō](../../strongs/g/g1268.md) no further among the [laos](../../strongs/g/g2992.md), [apeilē](../../strongs/g/g547.md) [apeileō](../../strongs/g/g546.md) them, that they [laleō](../../strongs/g/g2980.md) [mēketi](../../strongs/g/g3371.md) to [mēdeis](../../strongs/g/g3367.md) [anthrōpos](../../strongs/g/g444.md) in this [onoma](../../strongs/g/g3686.md).

<a name="acts_4_18"></a>Acts 4:18

And they [kaleō](../../strongs/g/g2564.md) them, and [paraggellō](../../strongs/g/g3853.md) them not to [phtheggomai](../../strongs/g/g5350.md) [katholou](../../strongs/g/g2527.md) nor [didaskō](../../strongs/g/g1321.md) in the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="acts_4_19"></a>Acts 4:19

But [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, Whether it be [dikaios](../../strongs/g/g1342.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md) to [akouō](../../strongs/g/g191.md) unto you more than unto [theos](../../strongs/g/g2316.md), [krinō](../../strongs/g/g2919.md) ye.

<a name="acts_4_20"></a>Acts 4:20

For we cannot but [laleō](../../strongs/g/g2980.md) the things which we have [eidō](../../strongs/g/g1492.md) and [akouō](../../strongs/g/g191.md).

<a name="acts_4_21"></a>Acts 4:21

So when they had [prosapeileō](../../strongs/g/g4324.md) them, they [apolyō](../../strongs/g/g630.md) them, [heuriskō](../../strongs/g/g2147.md) [mēdeis](../../strongs/g/g3367.md) how they might [kolazō](../../strongs/g/g2849.md) them, because of the [laos](../../strongs/g/g2992.md): for all [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) for that which was done.

<a name="acts_4_22"></a>Acts 4:22

For the [anthrōpos](../../strongs/g/g444.md) was above forty [etos](../../strongs/g/g2094.md), on whom this [sēmeion](../../strongs/g/g4592.md) of [iasis](../../strongs/g/g2392.md) was shewed.

<a name="acts_4_23"></a>Acts 4:23

And being [apolyō](../../strongs/g/g630.md), they [erchomai](../../strongs/g/g2064.md) to [idios](../../strongs/g/g2398.md), and [apaggellō](../../strongs/g/g518.md) all that the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md) had [eipon](../../strongs/g/g2036.md) unto them.

<a name="acts_4_24"></a>Acts 4:24

And when they [akouō](../../strongs/g/g191.md) that, they [airō](../../strongs/g/g142.md) their [phōnē](../../strongs/g/g5456.md) to [theos](../../strongs/g/g2316.md) with [homothymadon](../../strongs/g/g3661.md), and [eipon](../../strongs/g/g2036.md), [despotēs](../../strongs/g/g1203.md), thou art [theos](../../strongs/g/g2316.md), which hast [poieō](../../strongs/g/g4160.md) [ouranos](../../strongs/g/g3772.md), and [gē](../../strongs/g/g1093.md), and the [thalassa](../../strongs/g/g2281.md), and all that in them is:

<a name="acts_4_25"></a>Acts 4:25

Who by the [stoma](../../strongs/g/g4750.md) of thy [pais](../../strongs/g/g3816.md) [Dabid](../../strongs/g/g1138.md) hast [eipon](../../strongs/g/g2036.md), Why did the [ethnos](../../strongs/g/g1484.md) [phryassō](../../strongs/g/g5433.md), and the [laos](../../strongs/g/g2992.md) [meletaō](../../strongs/g/g3191.md) [kenos](../../strongs/g/g2756.md)?

<a name="acts_4_26"></a>Acts 4:26

The [basileus](../../strongs/g/g935.md) of the [gē](../../strongs/g/g1093.md) [paristēmi](../../strongs/g/g3936.md), and the [archōn](../../strongs/g/g758.md) were [synagō](../../strongs/g/g4863.md) together against the [kyrios](../../strongs/g/g2962.md), and against his [Christos](../../strongs/g/g5547.md).

<a name="acts_4_27"></a>Acts 4:27

For of an [alētheia](../../strongs/g/g225.md) against thy [hagios](../../strongs/g/g40.md) [pais](../../strongs/g/g3816.md) [Iēsous](../../strongs/g/g2424.md), whom thou hast [chriō](../../strongs/g/g5548.md), both [Hērōdēs](../../strongs/g/g2264.md), and [Pontios](../../strongs/g/g4194.md) [Pilatos](../../strongs/g/g4091.md), with the [ethnos](../../strongs/g/g1484.md), and the [laos](../../strongs/g/g2992.md) of [Israēl](../../strongs/g/g2474.md), were [synagō](../../strongs/g/g4863.md),

<a name="acts_4_28"></a>Acts 4:28

For to [poieō](../../strongs/g/g4160.md) whatsoever thy [cheir](../../strongs/g/g5495.md) and thy [boulē](../../strongs/g/g1012.md) [proorizō](../../strongs/g/g4309.md) to be [ginomai](../../strongs/g/g1096.md).

<a name="acts_4_29"></a>Acts 4:29

And [ta nyn](../../strongs/g/g3569.md), [kyrios](../../strongs/g/g2962.md), [epeidon](../../strongs/g/g1896.md) their [apeilē](../../strongs/g/g547.md): and [didōmi](../../strongs/g/g1325.md) thy [doulos](../../strongs/g/g1401.md), that with all [parrēsia](../../strongs/g/g3954.md) they may [laleō](../../strongs/g/g2980.md) thy [logos](../../strongs/g/g3056.md),

<a name="acts_4_30"></a>Acts 4:30

By [ekteinō](../../strongs/g/g1614.md) thine [cheir](../../strongs/g/g5495.md) to [iasis](../../strongs/g/g2392.md); and that [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md) may be [ginomai](../../strongs/g/g1096.md) by the [onoma](../../strongs/g/g3686.md) of thy [hagios](../../strongs/g/g40.md) [pais](../../strongs/g/g3816.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_4_31"></a>Acts 4:31

And when they had [deomai](../../strongs/g/g1189.md), the [topos](../../strongs/g/g5117.md) was [saleuō](../../strongs/g/g4531.md) where they were [synagō](../../strongs/g/g4863.md); and they were all [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and they [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) with [parrēsia](../../strongs/g/g3954.md).

<a name="acts_4_32"></a>Acts 4:32

And the [plēthos](../../strongs/g/g4128.md) of [pisteuō](../../strongs/g/g4100.md) were of one [kardia](../../strongs/g/g2588.md) and of one [psychē](../../strongs/g/g5590.md): neither [legō](../../strongs/g/g3004.md) any of them that ought of the things which he [hyparchonta](../../strongs/g/g5224.md) was his own; but they had all things [koinos](../../strongs/g/g2839.md).

<a name="acts_4_33"></a>Acts 4:33

And with [megas](../../strongs/g/g3173.md) [dynamis](../../strongs/g/g1411.md) [apodidōmi](../../strongs/g/g591.md) the [apostolos](../../strongs/g/g652.md) [martyrion](../../strongs/g/g3142.md) of the [anastasis](../../strongs/g/g386.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md): and [megas](../../strongs/g/g3173.md) [charis](../../strongs/g/g5485.md) was upon them all.

<a name="acts_4_34"></a>Acts 4:34

Neither was there any among them that [endeēs](../../strongs/g/g1729.md): for as many as were [ktētōr](../../strongs/g/g2935.md) of [chōrion](../../strongs/g/g5564.md) or [oikia](../../strongs/g/g3614.md) [pōleō](../../strongs/g/g4453.md) them, and [pherō](../../strongs/g/g5342.md) the [timē](../../strongs/g/g5092.md) of the [pipraskō](../../strongs/g/g4097.md),

<a name="acts_4_35"></a>Acts 4:35

And [tithēmi](../../strongs/g/g5087.md) at the [apostolos](../../strongs/g/g652.md) [pous](../../strongs/g/g4228.md): and [diadidōmi](../../strongs/g/g1239.md) was made unto [hekastos](../../strongs/g/g1538.md) according as he had [chreia](../../strongs/g/g5532.md).

<a name="acts_4_36"></a>Acts 4:36

And [Iōsēs](../../strongs/g/g2500.md), who by the [apostolos](../../strongs/g/g652.md) was [epikaleō](../../strongs/g/g1941.md) [Barnabas](../../strongs/g/g921.md), (which is, [methermēneuō](../../strongs/g/g3177.md), The [huios](../../strongs/g/g5207.md) of [paraklēsis](../../strongs/g/g3874.md),) a [Leuitēs](../../strongs/g/g3019.md), and of the [genos](../../strongs/g/g1085.md) of [Kyprios](../../strongs/g/g2953.md),

<a name="acts_4_37"></a>Acts 4:37

Having [agros](../../strongs/g/g68.md), [pōleō](../../strongs/g/g4453.md), and [pherō](../../strongs/g/g5342.md) the [chrēma](../../strongs/g/g5536.md), and [tithēmi](../../strongs/g/g5087.md) it at the [apostolos](../../strongs/g/g652.md) [pous](../../strongs/g/g4228.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 3](acts_3.md) - [Acts 5](acts_5.md)