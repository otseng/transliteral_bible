# [Acts 9](https://www.blueletterbible.org/kjv/act/9/1/s_1027001)

<a name="acts_9_1"></a>Acts 9:1

And [Saulos](../../strongs/g/g4569.md), yet [empneō](../../strongs/g/g1709.md) [apeilē](../../strongs/g/g547.md) and [phonos](../../strongs/g/g5408.md) against the [mathētēs](../../strongs/g/g3101.md) of the [kyrios](../../strongs/g/g2962.md), [proserchomai](../../strongs/g/g4334.md) unto the [archiereus](../../strongs/g/g749.md),

<a name="acts_9_2"></a>Acts 9:2

And [aiteō](../../strongs/g/g154.md) of him [epistolē](../../strongs/g/g1992.md) to [Damaskos](../../strongs/g/g1154.md) to the [synagōgē](../../strongs/g/g4864.md), that if he [heuriskō](../../strongs/g/g2147.md) any of [hodos](../../strongs/g/g3598.md), whether they were [anēr](../../strongs/g/g435.md) or [gynē](../../strongs/g/g1135.md), he might [agō](../../strongs/g/g71.md) them [deō](../../strongs/g/g1210.md) unto [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_9_3"></a>Acts 9:3

And as he [poreuō](../../strongs/g/g4198.md), he [ginomai](../../strongs/g/g1096.md) [eggizō](../../strongs/g/g1448.md) [Damaskos](../../strongs/g/g1154.md): and [exaiphnēs](../../strongs/g/g1810.md) there [periastraptō](../../strongs/g/g4015.md) him a [phōs](../../strongs/g/g5457.md) from [ouranos](../../strongs/g/g3772.md):

<a name="acts_9_4"></a>Acts 9:4

And he [piptō](../../strongs/g/g4098.md) to [gē](../../strongs/g/g1093.md), and [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) [legō](../../strongs/g/g3004.md) unto him, [Saoul](../../strongs/g/g4549.md), [Saoul](../../strongs/g/g4549.md), why [diōkō](../../strongs/g/g1377.md) thou me?

<a name="acts_9_5"></a>Acts 9:5

And he [eipon](../../strongs/g/g2036.md), Who art thou, [kyrios](../../strongs/g/g2962.md)? And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md), I am [Iēsous](../../strongs/g/g2424.md) whom thou [diōkō](../../strongs/g/g1377.md): it is [sklēros](../../strongs/g/g4642.md) for thee to [laktizō](../../strongs/g/g2979.md) against the [kentron](../../strongs/g/g2759.md). [^1]

<a name="acts_9_6"></a>Acts 9:6

And he [tremō](../../strongs/g/g5141.md) and [thambeō](../../strongs/g/g2284.md) [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), what [thelō](../../strongs/g/g2309.md) me to do? And the [kyrios](../../strongs/g/g2962.md) unto him, [anistēmi](../../strongs/g/g450.md), and [eiserchomai](../../strongs/g/g1525.md) into the [polis](../../strongs/g/g4172.md), and it shall be [laleō](../../strongs/g/g2980.md) thee what thou must [poieō](../../strongs/g/g4160.md).

<a name="acts_9_7"></a>Acts 9:7

And the [anēr](../../strongs/g/g435.md) which [synodeuō](../../strongs/g/g4922.md) with him [histēmi](../../strongs/g/g2476.md) [eneos](../../strongs/g/g1769.md), [akouō](../../strongs/g/g191.md) [phōnē](../../strongs/g/g5456.md), but [theōreō](../../strongs/g/g2334.md) [mēdeis](../../strongs/g/g3367.md).

<a name="acts_9_8"></a>Acts 9:8

And [Saulos](../../strongs/g/g4569.md) [egeirō](../../strongs/g/g1453.md) from [gē](../../strongs/g/g1093.md); and when his [ophthalmos](../../strongs/g/g3788.md) were [anoigō](../../strongs/g/g455.md), he [blepō](../../strongs/g/g991.md) [oudeis](../../strongs/g/g3762.md): but they [cheiragōgeō](../../strongs/g/g5496.md) him, and [eisagō](../../strongs/g/g1521.md) into [Damaskos](../../strongs/g/g1154.md).

<a name="acts_9_9"></a>Acts 9:9

And he was three [hēmera](../../strongs/g/g2250.md) without [blepō](../../strongs/g/g991.md), and neither did [phago](../../strongs/g/g5315.md) nor [pinō](../../strongs/g/g4095.md).

<a name="acts_9_10"></a>Acts 9:10

And there was a certain [mathētēs](../../strongs/g/g3101.md) at [Damaskos](../../strongs/g/g1154.md), [onoma](../../strongs/g/g3686.md) [Hananias](../../strongs/g/g367.md); and to him [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) in a [horama](../../strongs/g/g3705.md), [Hananias](../../strongs/g/g367.md). And he [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I, [kyrios](../../strongs/g/g2962.md).

<a name="acts_9_11"></a>Acts 9:11

And the [kyrios](../../strongs/g/g2962.md) unto him, [anistēmi](../../strongs/g/g450.md), and [poreuō](../../strongs/g/g4198.md) into the [rhymē](../../strongs/g/g4505.md) which is [kaleō](../../strongs/g/g2564.md) [euthys](../../strongs/g/g2117.md), and [zēteō](../../strongs/g/g2212.md) in the [oikia](../../strongs/g/g3614.md) of [Ioudas](../../strongs/g/g2455.md) for one [onoma](../../strongs/g/g3686.md) [Saulos](../../strongs/g/g4569.md), of [Tarseus](../../strongs/g/g5018.md): for, [idou](../../strongs/g/g2400.md), he [proseuchomai](../../strongs/g/g4336.md),

<a name="acts_9_12"></a>Acts 9:12

And hath [eidō](../../strongs/g/g1492.md) in a [horama](../../strongs/g/g3705.md) an [anēr](../../strongs/g/g435.md) [onoma](../../strongs/g/g3686.md) [Hananias](../../strongs/g/g367.md) [eiserchomai](../../strongs/g/g1525.md), and [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) on him, that he might [anablepō](../../strongs/g/g308.md).

<a name="acts_9_13"></a>Acts 9:13

Then [Hananias](../../strongs/g/g367.md) [apokrinomai](../../strongs/g/g611.md), [kyrios](../../strongs/g/g2962.md), I have [akouō](../../strongs/g/g191.md) by [polys](../../strongs/g/g4183.md) of this [anēr](../../strongs/g/g435.md), how much [kakos](../../strongs/g/g2556.md) he hath [poieō](../../strongs/g/g4160.md) to thy [hagios](../../strongs/g/g40.md) at [Ierousalēm](../../strongs/g/g2419.md):

<a name="acts_9_14"></a>Acts 9:14

And here he hath [exousia](../../strongs/g/g1849.md) from the [archiereus](../../strongs/g/g749.md) to [deō](../../strongs/g/g1210.md) all [epikaleō](../../strongs/g/g1941.md) on thy [onoma](../../strongs/g/g3686.md).

<a name="acts_9_15"></a>Acts 9:15

But the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto him, [poreuō](../../strongs/g/g4198.md): for he is an [eklogē](../../strongs/g/g1589.md) [skeuos](../../strongs/g/g4632.md) unto me, to [bastazō](../../strongs/g/g941.md) my [onoma](../../strongs/g/g3686.md) before the [ethnos](../../strongs/g/g1484.md), and [basileus](../../strongs/g/g935.md), and the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md):

<a name="acts_9_16"></a>Acts 9:16

For I will [hypodeiknymi](../../strongs/g/g5263.md) him [hosos](../../strongs/g/g3745.md) he must [paschō](../../strongs/g/g3958.md) for my [onoma](../../strongs/g/g3686.md) sake.

<a name="acts_9_17"></a>Acts 9:17

And [Hananias](../../strongs/g/g367.md) [aperchomai](../../strongs/g/g565.md), and [eiserchomai](../../strongs/g/g1525.md) into the [oikia](../../strongs/g/g3614.md); and [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) on him [eipon](../../strongs/g/g2036.md), [adelphos](../../strongs/g/g80.md) [Saoul](../../strongs/g/g4549.md), the [kyrios](../../strongs/g/g2962.md), even [Iēsous](../../strongs/g/g2424.md), that [optanomai](../../strongs/g/g3700.md) unto thee in [hodos](../../strongs/g/g3598.md) as thou [erchomai](../../strongs/g/g2064.md), hath [apostellō](../../strongs/g/g649.md) me, that thou [anablepō](../../strongs/g/g308.md), and be [pimplēmi](../../strongs/g/g4130.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_9_18"></a>Acts 9:18

And [eutheōs](../../strongs/g/g2112.md) [apopiptō](../../strongs/g/g634.md) from his [ophthalmos](../../strongs/g/g3788.md) as [lepis](../../strongs/g/g3013.md): and he [anablepō](../../strongs/g/g308.md) [parachrēma](../../strongs/g/g3916.md), and [anistēmi](../../strongs/g/g450.md), and was [baptizō](../../strongs/g/g907.md).

<a name="acts_9_19"></a>Acts 9:19

And when he had [lambanō](../../strongs/g/g2983.md) [trophē](../../strongs/g/g5160.md), he was [enischyō](../../strongs/g/g1765.md). Then was [Saulos](../../strongs/g/g4569.md) certain [hēmera](../../strongs/g/g2250.md) with the [mathētēs](../../strongs/g/g3101.md) which were at [Damaskos](../../strongs/g/g1154.md).

<a name="acts_9_20"></a>Acts 9:20

And [eutheōs](../../strongs/g/g2112.md) he [kēryssō](../../strongs/g/g2784.md) [Christos](../../strongs/g/g5547.md) in the [synagōgē](../../strongs/g/g4864.md), that he is the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_9_21"></a>Acts 9:21

But all that [akouō](../../strongs/g/g191.md) were [existēmi](../../strongs/g/g1839.md), and [legō](../../strongs/g/g3004.md); Is not this he that [portheō](../../strongs/g/g4199.md) them which [epikaleō](../../strongs/g/g1941.md) this [onoma](../../strongs/g/g3686.md) in [Ierousalēm](../../strongs/g/g2419.md), and [erchomai](../../strongs/g/g2064.md) hither for that intent, that he might [agō](../../strongs/g/g71.md) them [deō](../../strongs/g/g1210.md) unto the [archiereus](../../strongs/g/g749.md)?

<a name="acts_9_22"></a>Acts 9:22

But [Saulos](../../strongs/g/g4569.md) [endynamoō](../../strongs/g/g1743.md) the more, and [sygcheō](../../strongs/g/g4797.md) the [Ioudaios](../../strongs/g/g2453.md) which [katoikeō](../../strongs/g/g2730.md) at [Damaskos](../../strongs/g/g1154.md), [symbibazō](../../strongs/g/g4822.md) that this is [Christos](../../strongs/g/g5547.md).

<a name="acts_9_23"></a>Acts 9:23

And after [hikanos](../../strongs/g/g2425.md) [hēmera](../../strongs/g/g2250.md) were [plēroō](../../strongs/g/g4137.md), the [Ioudaios](../../strongs/g/g2453.md) [symbouleuō](../../strongs/g/g4823.md) to [anaireō](../../strongs/g/g337.md) him:

<a name="acts_9_24"></a>Acts 9:24

But their [epiboulē](../../strongs/g/g1917.md) was [ginōskō](../../strongs/g/g1097.md) of [Saulos](../../strongs/g/g4569.md). And they [paratēreō](../../strongs/g/g3906.md) the [pylē](../../strongs/g/g4439.md) [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md) to [anaireō](../../strongs/g/g337.md) him.

<a name="acts_9_25"></a>Acts 9:25

Then the [mathētēs](../../strongs/g/g3101.md) [lambanō](../../strongs/g/g2983.md) him by [nyx](../../strongs/g/g3571.md), and [kathiēmi](../../strongs/g/g2524.md) [chalaō](../../strongs/g/g5465.md) by the [teichos](../../strongs/g/g5038.md) in a [spyris](../../strongs/g/g4711.md).

<a name="acts_9_26"></a>Acts 9:26

And when [Saulos](../../strongs/g/g4569.md) was [paraginomai](../../strongs/g/g3854.md) to [Ierousalēm](../../strongs/g/g2419.md), he [peiraō](../../strongs/g/g3987.md) to [kollaō](../../strongs/g/g2853.md) to the [mathētēs](../../strongs/g/g3101.md): but they were all [phobeō](../../strongs/g/g5399.md) of him, and [pisteuō](../../strongs/g/g4100.md) not that he was a [mathētēs](../../strongs/g/g3101.md).

<a name="acts_9_27"></a>Acts 9:27

But [Barnabas](../../strongs/g/g921.md) [epilambanomai](../../strongs/g/g1949.md) him, and [agō](../../strongs/g/g71.md) to the [apostolos](../../strongs/g/g652.md), and [diēgeomai](../../strongs/g/g1334.md) unto them how he had [eidō](../../strongs/g/g1492.md) the [kyrios](../../strongs/g/g2962.md) in [hodos](../../strongs/g/g3598.md), and that he had [laleō](../../strongs/g/g2980.md) to him, and how he had [parrēsiazomai](../../strongs/g/g3955.md) at [Damaskos](../../strongs/g/g1154.md) in the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="acts_9_28"></a>Acts 9:28

And he was with them [eisporeuomai](../../strongs/g/g1531.md) and [ekporeuomai](../../strongs/g/g1607.md) at [Ierousalēm](../../strongs/g/g2419.md).

<a name="acts_9_29"></a>Acts 9:29

And he [laleō](../../strongs/g/g2980.md) [parrēsiazomai](../../strongs/g/g3955.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and [syzēteō](../../strongs/g/g4802.md) against the [Hellēnistēs](../../strongs/g/g1675.md): but they [epicheireō](../../strongs/g/g2021.md) to [anaireō](../../strongs/g/g337.md) him.

<a name="acts_9_30"></a>Acts 9:30

when the [adelphos](../../strongs/g/g80.md) [epiginōskō](../../strongs/g/g1921.md), they [katagō](../../strongs/g/g2609.md) him to [Kaisareia](../../strongs/g/g2542.md), and [exapostellō](../../strongs/g/g1821.md) him to [Tarsos](../../strongs/g/g5019.md).

<a name="acts_9_31"></a>Acts 9:31

Then had the [ekklēsia](../../strongs/g/g1577.md) [eirēnē](../../strongs/g/g1515.md) throughout all [Ioudaia](../../strongs/g/g2449.md) and [Galilaia](../../strongs/g/g1056.md) and [Samareia](../../strongs/g/g4540.md), and were [oikodomeō](../../strongs/g/g3618.md); and [poreuō](../../strongs/g/g4198.md) in the [phobos](../../strongs/g/g5401.md) of the [kyrios](../../strongs/g/g2962.md), and in the [paraklēsis](../../strongs/g/g3874.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), were [plēthynō](../../strongs/g/g4129.md).

<a name="acts_9_32"></a>Acts 9:32

And [ginomai](../../strongs/g/g1096.md), as [Petros](../../strongs/g/g4074.md) [dierchomai](../../strongs/g/g1330.md) all, he [katerchomai](../../strongs/g/g2718.md) also to the [hagios](../../strongs/g/g40.md) which [katoikeō](../../strongs/g/g2730.md) at [Lydda](../../strongs/g/g3069.md).

<a name="acts_9_33"></a>Acts 9:33

And there he [heuriskō](../../strongs/g/g2147.md) a certain [anthrōpos](../../strongs/g/g444.md) [onoma](../../strongs/g/g3686.md) [Aineas](../../strongs/g/g132.md), which had [katakeimai](../../strongs/g/g2621.md) his [krabattos](../../strongs/g/g2895.md) eight [etos](../../strongs/g/g2094.md), and was [paralyō](../../strongs/g/g3886.md).

<a name="acts_9_34"></a>Acts 9:34

And [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md) unto him, [Aineas](../../strongs/g/g132.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) [iaomai](../../strongs/g/g2390.md) thee: [anistēmi](../../strongs/g/g450.md), and [strōnnyō](../../strongs/g/g4766.md) [seautou](../../strongs/g/g4572.md). And he [anistēmi](../../strongs/g/g450.md) [eutheōs](../../strongs/g/g2112.md).

<a name="acts_9_35"></a>Acts 9:35

And all that [katoikeō](../../strongs/g/g2730.md) at [Lydda](../../strongs/g/g3069.md) and [Sar(r)ōn](../../strongs/g/g4565.md) [eidō](../../strongs/g/g1492.md) him, and [epistrephō](../../strongs/g/g1994.md) to the [kyrios](../../strongs/g/g2962.md).

<a name="acts_9_36"></a>Acts 9:36

Now there was at [Ioppē](../../strongs/g/g2445.md) a certain [mathētria](../../strongs/g/g3102.md) [onoma](../../strongs/g/g3686.md) [Tabitha](../../strongs/g/g5000.md), which [diermēneuō](../../strongs/g/g1329.md) is [legō](../../strongs/g/g3004.md) [Dorkas](../../strongs/g/g1393.md): [houtos](../../strongs/g/g3778.md) [autos](../../strongs/g/g846.md) was [plērēs](../../strongs/g/g4134.md) of [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md) and [eleēmosynē](../../strongs/g/g1654.md) which she [poieō](../../strongs/g/g4160.md).

<a name="acts_9_37"></a>Acts 9:37

And [ginomai](../../strongs/g/g1096.md) in those [hēmera](../../strongs/g/g2250.md), [astheneō](../../strongs/g/g770.md), and [apothnēskō](../../strongs/g/g599.md): whom when they had [louō](../../strongs/g/g3068.md), they [tithēmi](../../strongs/g/g5087.md) in [hyperōon](../../strongs/g/g5253.md).

<a name="acts_9_38"></a>Acts 9:38

And forasmuch as [Lydda](../../strongs/g/g3069.md) was [eggys](../../strongs/g/g1451.md) to [Ioppē](../../strongs/g/g2445.md), and the [mathētēs](../../strongs/g/g3101.md) had [akouō](../../strongs/g/g191.md) that [Petros](../../strongs/g/g4074.md) was there, they [apostellō](../../strongs/g/g649.md) unto him two [anēr](../../strongs/g/g435.md), [parakaleō](../../strongs/g/g3870.md) that he would not [okneō](../../strongs/g/g3635.md) [dierchomai](../../strongs/g/g1330.md) to them.

<a name="acts_9_39"></a>Acts 9:39

Then [Petros](../../strongs/g/g4074.md) [anistēmi](../../strongs/g/g450.md) and [synerchomai](../../strongs/g/g4905.md) with them. When he was [paraginomai](../../strongs/g/g3854.md), they [anagō](../../strongs/g/g321.md) him into [hyperōon](../../strongs/g/g5253.md): and all the [chēra](../../strongs/g/g5503.md) [paristēmi](../../strongs/g/g3936.md) him [klaiō](../../strongs/g/g2799.md), and [epideiknymi](../../strongs/g/g1925.md) the [chitōn](../../strongs/g/g5509.md) and [himation](../../strongs/g/g2440.md) which [Dorkas](../../strongs/g/g1393.md) [poieō](../../strongs/g/g4160.md), while she was with them.

<a name="acts_9_40"></a>Acts 9:40

But [Petros](../../strongs/g/g4074.md) [ekballō](../../strongs/g/g1544.md) them all [exō](../../strongs/g/g1854.md), [tithēmi](../../strongs/g/g5087.md) [gony](../../strongs/g/g1119.md), and [proseuchomai](../../strongs/g/g4336.md); and [epistrephō](../../strongs/g/g1994.md) to the [sōma](../../strongs/g/g4983.md) [eipon](../../strongs/g/g2036.md), [Tabitha](../../strongs/g/g5000.md), [anistēmi](../../strongs/g/g450.md). And she [anoigō](../../strongs/g/g455.md) her [ophthalmos](../../strongs/g/g3788.md): and when she [eidō](../../strongs/g/g1492.md) [Petros](../../strongs/g/g4074.md), she [anakathizō](../../strongs/g/g339.md).

<a name="acts_9_41"></a>Acts 9:41

And he [didōmi](../../strongs/g/g1325.md) her [cheir](../../strongs/g/g5495.md), and [anistēmi](../../strongs/g/g450.md) her, and when he had [phōneō](../../strongs/g/g5455.md) the [hagios](../../strongs/g/g40.md) and [chēra](../../strongs/g/g5503.md), [paristēmi](../../strongs/g/g3936.md) her [zaō](../../strongs/g/g2198.md).

<a name="acts_9_42"></a>Acts 9:42

And it was [gnōstos](../../strongs/g/g1110.md) throughout all [Ioppē](../../strongs/g/g2445.md); and [polys](../../strongs/g/g4183.md) [pisteuō](../../strongs/g/g4100.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="acts_9_43"></a>Acts 9:43

And [ginomai](../../strongs/g/g1096.md), that he [menō](../../strongs/g/g3306.md) [hikanos](../../strongs/g/g2425.md) [hēmera](../../strongs/g/g2250.md) in [Ioppē](../../strongs/g/g2445.md) with one [Simōn](../../strongs/g/g4613.md) a [byrseus](../../strongs/g/g1038.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 8](acts_8.md) - [Acts 10](acts_10.md)

---

[^1]: [Acts 9:5 Commentary](../../commentary/acts/acts_9_commentary.md#acts_9_5)
