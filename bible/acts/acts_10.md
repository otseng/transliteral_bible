# [Acts 10](https://www.blueletterbible.org/kjv/act/10/1/s_1028001)

<a name="acts_10_1"></a>Acts 10:1

There was a certain [anēr](../../strongs/g/g435.md) in [Kaisareia](../../strongs/g/g2542.md) [onoma](../../strongs/g/g3686.md) [Kornēlios](../../strongs/g/g2883.md), a [hekatontarchēs](../../strongs/g/g1543.md) of the [speira](../../strongs/g/g4686.md) [kaleō](../../strongs/g/g2564.md) the [Italikos](../../strongs/g/g2483.md),

<a name="acts_10_2"></a>Acts 10:2

An [eusebēs](../../strongs/g/g2152.md), and one that [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md) with all his [oikos](../../strongs/g/g3624.md), which [poieō](../../strongs/g/g4160.md) [polys](../../strongs/g/g4183.md) [eleēmosynē](../../strongs/g/g1654.md) to the [laos](../../strongs/g/g2992.md), and [deomai](../../strongs/g/g1189.md) to [theos](../../strongs/g/g2316.md) [diapantos](../../strongs/g/g1275.md).

<a name="acts_10_3"></a>Acts 10:3

He [eidō](../../strongs/g/g1492.md) in a [horama](../../strongs/g/g3705.md) [phanerōs](../../strongs/g/g5320.md) about the ninth [hōra](../../strongs/g/g5610.md) of the [hēmera](../../strongs/g/g2250.md) an [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md) [eiserchomai](../../strongs/g/g1525.md) to him, and [eipon](../../strongs/g/g2036.md) unto him, [Kornēlios](../../strongs/g/g2883.md).

<a name="acts_10_4"></a>Acts 10:4

And when he [atenizō](../../strongs/g/g816.md) on him, he was [emphobos](../../strongs/g/g1719.md), and [eipon](../../strongs/g/g2036.md), What is it, [kyrios](../../strongs/g/g2962.md)?  And he [eipon](../../strongs/g/g2036.md) unto him, Thy [proseuchē](../../strongs/g/g4335.md) and thine [eleēmosynē](../../strongs/g/g1654.md) are [anabainō](../../strongs/g/g305.md) for a [mnēmosynon](../../strongs/g/g3422.md) before [theos](../../strongs/g/g2316.md).

<a name="acts_10_5"></a>Acts 10:5

And now [pempō](../../strongs/g/g3992.md) [anēr](../../strongs/g/g435.md) to [Ioppē](../../strongs/g/g2445.md), and [metapempō](../../strongs/g/g3343.md) [Simōn](../../strongs/g/g4613.md), whose [epikaleō](../../strongs/g/g1941.md) is [Petros](../../strongs/g/g4074.md):

<a name="acts_10_6"></a>Acts 10:6

He [xenizō](../../strongs/g/g3579.md) with one [Simōn](../../strongs/g/g4613.md) a [byrseus](../../strongs/g/g1038.md), whose [oikia](../../strongs/g/g3614.md) is by the [thalassa](../../strongs/g/g2281.md): he shall [laleō](../../strongs/g/g2980.md) thee what thou oughtest to [poieō](../../strongs/g/g4160.md). [^1]

<a name="acts_10_7"></a>Acts 10:7

And when the [aggelos](../../strongs/g/g32.md) which [laleō](../../strongs/g/g2980.md) unto [Kornēlios](../../strongs/g/g2883.md) was [aperchomai](../../strongs/g/g565.md), he [phōneō](../../strongs/g/g5455.md) two of his [oiketēs](../../strongs/g/g3610.md), and an [eusebēs](../../strongs/g/g2152.md) [stratiōtēs](../../strongs/g/g4757.md) [proskartereō](../../strongs/g/g4342.md) him;

<a name="acts_10_8"></a>Acts 10:8

And when he had [exēgeomai](../../strongs/g/g1834.md) all things unto them, he [apostellō](../../strongs/g/g649.md) them to [Ioppē](../../strongs/g/g2445.md).

<a name="acts_10_9"></a>Acts 10:9

On the [epaurion](../../strongs/g/g1887.md), as they [hodoiporeō](../../strongs/g/g3596.md), and [eggizō](../../strongs/g/g1448.md) unto the [polis](../../strongs/g/g4172.md), [Petros](../../strongs/g/g4074.md) [anabainō](../../strongs/g/g305.md) upon the [dōma](../../strongs/g/g1430.md) to [proseuchomai](../../strongs/g/g4336.md) about the sixth [hōra](../../strongs/g/g5610.md):

<a name="acts_10_10"></a>Acts 10:10

And he became [prospeinos](../../strongs/g/g4361.md), and would have [geuomai](../../strongs/g/g1089.md): but while they [paraskeuazō](../../strongs/g/g3903.md), he [epipiptō](../../strongs/g/g1968.md) into [ekstasis](../../strongs/g/g1611.md),

<a name="acts_10_11"></a>Acts 10:11

And [theōreō](../../strongs/g/g2334.md) [ouranos](../../strongs/g/g3772.md) [anoigō](../../strongs/g/g455.md), and a certain [skeuos](../../strongs/g/g4632.md) [katabainō](../../strongs/g/g2597.md) upon him, as it had been a [megas](../../strongs/g/g3173.md) [othonē](../../strongs/g/g3607.md) [deō](../../strongs/g/g1210.md) at the four [archē](../../strongs/g/g746.md), and [kathiēmi](../../strongs/g/g2524.md) to [gē](../../strongs/g/g1093.md):

<a name="acts_10_12"></a>Acts 10:12

Wherein were all manner of [tetrapous](../../strongs/g/g5074.md) of [gē](../../strongs/g/g1093.md), and [thērion](../../strongs/g/g2342.md), and [herpeton](../../strongs/g/g2062.md), and [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md).

<a name="acts_10_13"></a>Acts 10:13

And there [ginomai](../../strongs/g/g1096.md) a [phōnē](../../strongs/g/g5456.md) to him, [anistēmi](../../strongs/g/g450.md), [Petros](../../strongs/g/g4074.md); [thyō](../../strongs/g/g2380.md), and [phago](../../strongs/g/g5315.md).

<a name="acts_10_14"></a>Acts 10:14

But [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [mēdamōs](../../strongs/g/g3365.md), [kyrios](../../strongs/g/g2962.md); for I have never [phago](../../strongs/g/g5315.md) any thing that is [koinos](../../strongs/g/g2839.md) or [akathartos](../../strongs/g/g169.md).

<a name="acts_10_15"></a>Acts 10:15

And the [phōnē](../../strongs/g/g5456.md) unto him again the second time, What [theos](../../strongs/g/g2316.md) hath [katharizō](../../strongs/g/g2511.md), [koinoō](../../strongs/g/g2840.md) not thou.

<a name="acts_10_16"></a>Acts 10:16

This was [ginomai](../../strongs/g/g1096.md) thrice: and the [skeuos](../../strongs/g/g4632.md) was [analambanō](../../strongs/g/g353.md) again into [ouranos](../../strongs/g/g3772.md).

<a name="acts_10_17"></a>Acts 10:17

Now while [Petros](../../strongs/g/g4074.md) [diaporeō](../../strongs/g/g1280.md) in himself what this [horama](../../strongs/g/g3705.md) which he had [eidō](../../strongs/g/g1492.md) should mean, [idou](../../strongs/g/g2400.md), the [anēr](../../strongs/g/g435.md) which were [apostellō](../../strongs/g/g649.md) from [Kornēlios](../../strongs/g/g2883.md) had [dierōtaō](../../strongs/g/g1331.md) for [Simōn](../../strongs/g/g4613.md) [oikia](../../strongs/g/g3614.md), and [ephistēmi](../../strongs/g/g2186.md) before the [pylōn](../../strongs/g/g4440.md),

<a name="acts_10_18"></a>Acts 10:18

And [phōneō](../../strongs/g/g5455.md), and [pynthanomai](../../strongs/g/g4441.md) whether [Simōn](../../strongs/g/g4613.md), which was [epikaleō](../../strongs/g/g1941.md) [Petros](../../strongs/g/g4074.md), were [xenizō](../../strongs/g/g3579.md) [enthade](../../strongs/g/g1759.md).

<a name="acts_10_19"></a>Acts 10:19

While [Petros](../../strongs/g/g4074.md) [enthymeomai](../../strongs/g/g1760.md) on the [horama](../../strongs/g/g3705.md), the [pneuma](../../strongs/g/g4151.md) [eipon](../../strongs/g/g2036.md) unto him, [idou](../../strongs/g/g2400.md), three [anēr](../../strongs/g/g435.md) [zēteō](../../strongs/g/g2212.md) thee.

<a name="acts_10_20"></a>Acts 10:20

[anistēmi](../../strongs/g/g450.md) therefore, and [katabainō](../../strongs/g/g2597.md), and [poreuō](../../strongs/g/g4198.md) with them, [diakrinō](../../strongs/g/g1252.md) [mēdeis](../../strongs/g/g3367.md): for I have [apostellō](../../strongs/g/g649.md) them.

<a name="acts_10_21"></a>Acts 10:21

Then [Petros](../../strongs/g/g4074.md) [katabainō](../../strongs/g/g2597.md) to the [anēr](../../strongs/g/g435.md) which were [apostellō](../../strongs/g/g649.md) unto him from [Kornēlios](../../strongs/g/g2883.md); and [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I am he whom ye [zēteō](../../strongs/g/g2212.md): what is the [aitia](../../strongs/g/g156.md) wherefore ye are [pareimi](../../strongs/g/g3918.md)? [^2]

<a name="acts_10_22"></a>Acts 10:22

And they [eipon](../../strongs/g/g2036.md), [Kornēlios](../../strongs/g/g2883.md) the [hekatontarchēs](../../strongs/g/g1543.md), a [dikaios](../../strongs/g/g1342.md) [anēr](../../strongs/g/g435.md), and one that [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), and of [martyreō](../../strongs/g/g3140.md) among all the [ethnos](../../strongs/g/g1484.md) of the [Ioudaios](../../strongs/g/g2453.md), was [chrēmatizō](../../strongs/g/g5537.md) by an [hagios](../../strongs/g/g40.md) [aggelos](../../strongs/g/g32.md) to [metapempō](../../strongs/g/g3343.md) for thee into his [oikos](../../strongs/g/g3624.md), and to [akouō](../../strongs/g/g191.md) [rhēma](../../strongs/g/g4487.md) of thee.

<a name="acts_10_23"></a>Acts 10:23

Then [eiskaleomai](../../strongs/g/g1528.md) he them in, and [xenizō](../../strongs/g/g3579.md) them. And on the [epaurion](../../strongs/g/g1887.md) [Petros](../../strongs/g/g4074.md) [exerchomai](../../strongs/g/g1831.md) with them, and certain [adelphos](../../strongs/g/g80.md) from [Ioppē](../../strongs/g/g2445.md) [synerchomai](../../strongs/g/g4905.md) him.

<a name="acts_10_24"></a>Acts 10:24

And the [epaurion](../../strongs/g/g1887.md) after they [eiserchomai](../../strongs/g/g1525.md) into [Kaisareia](../../strongs/g/g2542.md). And [Kornēlios](../../strongs/g/g2883.md) [prosdokaō](../../strongs/g/g4328.md) for them, and he had [sygkaleō](../../strongs/g/g4779.md) his [syggenēs](../../strongs/g/g4773.md) and [anagkaios](../../strongs/g/g316.md) [philos](../../strongs/g/g5384.md).

<a name="acts_10_25"></a>Acts 10:25

And as [Petros](../../strongs/g/g4074.md) was [eiserchomai](../../strongs/g/g1525.md), [Kornēlios](../../strongs/g/g2883.md) [synantaō](../../strongs/g/g4876.md) him, and [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md), and [proskyneō](../../strongs/g/g4352.md).

<a name="acts_10_26"></a>Acts 10:26

But [Petros](../../strongs/g/g4074.md) [egeirō](../../strongs/g/g1453.md) him, [legō](../../strongs/g/g3004.md), [anistēmi](../../strongs/g/g450.md); I myself also am an [anthrōpos](../../strongs/g/g444.md).

<a name="acts_10_27"></a>Acts 10:27

And as he [synomileō](../../strongs/g/g4926.md) with him, he [eiserchomai](../../strongs/g/g1525.md), and [heuriskō](../../strongs/g/g2147.md) [polys](../../strongs/g/g4183.md) that were [synerchomai](../../strongs/g/g4905.md).

<a name="acts_10_28"></a>Acts 10:28

And he [phēmi](../../strongs/g/g5346.md) unto them, Ye [epistamai](../../strongs/g/g1987.md) how that it is [athemitos](../../strongs/g/g111.md) for an [anēr](../../strongs/g/g435.md) that is [Ioudaios](../../strongs/g/g2453.md) to [kollaō](../../strongs/g/g2853.md), or [proserchomai](../../strongs/g/g4334.md) unto [allophylos](../../strongs/g/g246.md); but [theos](../../strongs/g/g2316.md) hath [deiknyō](../../strongs/g/g1166.md) me that I should [mēdeis](../../strongs/g/g3367.md) [legō](../../strongs/g/g3004.md) any [anthrōpos](../../strongs/g/g444.md) [koinos](../../strongs/g/g2839.md) or [akathartos](../../strongs/g/g169.md).

<a name="acts_10_29"></a>Acts 10:29

Therefore [erchomai](../../strongs/g/g2064.md) I [anantirrētōs](../../strongs/g/g369.md), [metapempō](../../strongs/g/g3343.md): I [pynthanomai](../../strongs/g/g4441.md) therefore for what [logos](../../strongs/g/g3056.md) ye have [metapempō](../../strongs/g/g3343.md) for me?

<a name="acts_10_30"></a>Acts 10:30

And [Kornēlios](../../strongs/g/g2883.md) [phēmi](../../strongs/g/g5346.md), Four [hēmera](../../strongs/g/g2250.md) ago I was [nēsteuō](../../strongs/g/g3522.md) until this [hōra](../../strongs/g/g5610.md); and at the ninth [hōra](../../strongs/g/g5610.md) I [proseuchomai](../../strongs/g/g4336.md) in my [oikos](../../strongs/g/g3624.md), and, [idou](../../strongs/g/g2400.md), an [anēr](../../strongs/g/g435.md) [histēmi](../../strongs/g/g2476.md) before me in [lampros](../../strongs/g/g2986.md) [esthēs](../../strongs/g/g2066.md),

<a name="acts_10_31"></a>Acts 10:31

And [phēmi](../../strongs/g/g5346.md), [Kornēlios](../../strongs/g/g2883.md), thy [proseuchē](../../strongs/g/g4335.md) is [eisakouō](../../strongs/g/g1522.md), and thine [eleēmosynē](../../strongs/g/g1654.md) are had in [mnaomai](../../strongs/g/g3415.md) in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_10_32"></a>Acts 10:32

[pempō](../../strongs/g/g3992.md) therefore to [Ioppē](../../strongs/g/g2445.md), and [metakaleō](../../strongs/g/g3333.md) [Simōn](../../strongs/g/g4613.md), whose [epikaleō](../../strongs/g/g1941.md) is [Petros](../../strongs/g/g4074.md); he is [xenizō](../../strongs/g/g3579.md) in the [oikia](../../strongs/g/g3614.md) of [Simōn](../../strongs/g/g4613.md) [byrseus](../../strongs/g/g1038.md) by the [thalassa](../../strongs/g/g2281.md): who, when he [paraginomai](../../strongs/g/g3854.md), shall [laleō](../../strongs/g/g2980.md) unto thee.

<a name="acts_10_33"></a>Acts 10:33

[exautēs](../../strongs/g/g1824.md) therefore I [pempō](../../strongs/g/g3992.md) to thee; and thou hast [kalōs](../../strongs/g/g2573.md) [poieō](../../strongs/g/g4160.md) that thou art [paraginomai](../../strongs/g/g3854.md). Now therefore are we all here [pareimi](../../strongs/g/g3918.md) before [theos](../../strongs/g/g2316.md), to [akouō](../../strongs/g/g191.md) all things that are [prostassō](../../strongs/g/g4367.md) thee of [theos](../../strongs/g/g2316.md).

<a name="acts_10_34"></a>Acts 10:34

Then [Petros](../../strongs/g/g4074.md) [anoigō](../../strongs/g/g455.md) [stoma](../../strongs/g/g4750.md), and [eipon](../../strongs/g/g2036.md), Of an [alētheia](../../strongs/g/g225.md) I [katalambanō](../../strongs/g/g2638.md) that [theos](../../strongs/g/g2316.md) is no [prosōpolēmptēs](../../strongs/g/g4381.md):

<a name="acts_10_35"></a>Acts 10:35

But in every [ethnos](../../strongs/g/g1484.md) he that [phobeō](../../strongs/g/g5399.md) him, and [ergazomai](../../strongs/g/g2038.md) [dikaiosynē](../../strongs/g/g1343.md), is [dektos](../../strongs/g/g1184.md) with him.

<a name="acts_10_36"></a>Acts 10:36

The [logos](../../strongs/g/g3056.md) which [apostellō](../../strongs/g/g649.md) unto the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md), [euaggelizō](../../strongs/g/g2097.md) [eirēnē](../../strongs/g/g1515.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md): (he is [kyrios](../../strongs/g/g2962.md) of all:)

<a name="acts_10_37"></a>Acts 10:37

That [rhēma](../../strongs/g/g4487.md), ye [eidō](../../strongs/g/g1492.md), which was [ginomai](../../strongs/g/g1096.md) throughout all [Ioudaia](../../strongs/g/g2449.md), and [archomai](../../strongs/g/g756.md) from [Galilaia](../../strongs/g/g1056.md), after the [baptisma](../../strongs/g/g908.md) which [Iōannēs](../../strongs/g/g2491.md) [kēryssō](../../strongs/g/g2784.md);

<a name="acts_10_38"></a>Acts 10:38

How [theos](../../strongs/g/g2316.md) [chriō](../../strongs/g/g5548.md) [Iēsous](../../strongs/g/g2424.md) of [Nazara](../../strongs/g/g3478.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) and with [dynamis](../../strongs/g/g1411.md): who [dierchomai](../../strongs/g/g1330.md) [euergeteō](../../strongs/g/g2109.md), and [iaomai](../../strongs/g/g2390.md) all [katadynasteuō](../../strongs/g/g2616.md) of the [diabolos](../../strongs/g/g1228.md); for [theos](../../strongs/g/g2316.md) was with him.

<a name="acts_10_39"></a>Acts 10:39

And we are [martys](../../strongs/g/g3144.md) of all things which he [poieō](../../strongs/g/g4160.md) both in the [chōra](../../strongs/g/g5561.md) of the [Ioudaios](../../strongs/g/g2453.md), and in [Ierousalēm](../../strongs/g/g2419.md); whom they [anaireō](../../strongs/g/g337.md) and [kremannymi](../../strongs/g/g2910.md) on a [xylon](../../strongs/g/g3586.md):

<a name="acts_10_40"></a>Acts 10:40

Him [theos](../../strongs/g/g2316.md) [egeirō](../../strongs/g/g1453.md) the third [hēmera](../../strongs/g/g2250.md), and [ginomai](../../strongs/g/g1096.md) him [emphanēs](../../strongs/g/g1717.md) [didōmi](../../strongs/g/g1325.md);

<a name="acts_10_41"></a>Acts 10:41

Not to all the [laos](../../strongs/g/g2992.md), but unto [martys](../../strongs/g/g3144.md) [procheirotoneō](../../strongs/g/g4401.md) [theos](../../strongs/g/g2316.md), even to us, who did [synesthiō](../../strongs/g/g4906.md) and [sympinō](../../strongs/g/g4844.md) him after he [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md).

<a name="acts_10_42"></a>Acts 10:42

And he [paraggellō](../../strongs/g/g3853.md) us to [kēryssō](../../strongs/g/g2784.md) unto the [laos](../../strongs/g/g2992.md), and to [diamartyromai](../../strongs/g/g1263.md) that it is he which was [horizō](../../strongs/g/g3724.md) of [theos](../../strongs/g/g2316.md) to be the [kritēs](../../strongs/g/g2923.md) of [zaō](../../strongs/g/g2198.md) and [nekros](../../strongs/g/g3498.md).

<a name="acts_10_43"></a>Acts 10:43

To him [martyreō](../../strongs/g/g3140.md) all the [prophētēs](../../strongs/g/g4396.md), that through his [onoma](../../strongs/g/g3686.md) whosoever [pisteuō](../../strongs/g/g4100.md) in him shall [lambanō](../../strongs/g/g2983.md) [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md).

<a name="acts_10_44"></a>Acts 10:44

While [Petros](../../strongs/g/g4074.md) yet [laleō](../../strongs/g/g2980.md) these [rhēma](../../strongs/g/g4487.md), the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [epipiptō](../../strongs/g/g1968.md) on all them which [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md).

<a name="acts_10_45"></a>Acts 10:45

And they of the [peritomē](../../strongs/g/g4061.md) which [pistos](../../strongs/g/g4103.md) were [existēmi](../../strongs/g/g1839.md), as many as [synerchomai](../../strongs/g/g4905.md) [Petros](../../strongs/g/g4074.md), because that on the [ethnos](../../strongs/g/g1484.md) also was [ekcheō](../../strongs/g/g1632.md) the [dōrea](../../strongs/g/g1431.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_10_46"></a>Acts 10:46

For they [akouō](../../strongs/g/g191.md) them [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md), and [megalynō](../../strongs/g/g3170.md) [theos](../../strongs/g/g2316.md). Then [apokrinomai](../../strongs/g/g611.md) [Petros](../../strongs/g/g4074.md),

<a name="acts_10_47"></a>Acts 10:47

Can [tis](../../strongs/g/g5100.md) [kōlyō](../../strongs/g/g2967.md) [hydōr](../../strongs/g/g5204.md), that these should not be [baptizō](../../strongs/g/g907.md), which have [lambanō](../../strongs/g/g2983.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) as well as we?

<a name="acts_10_48"></a>Acts 10:48

And he [prostassō](../../strongs/g/g4367.md) them to be [baptizō](../../strongs/g/g907.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md). Then [erōtaō](../../strongs/g/g2065.md) they him to [epimenō](../../strongs/g/g1961.md) certain [hēmera](../../strongs/g/g2250.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 9](acts_9.md) - [Acts 11](acts_11.md)

---

[^1]: [Acts 10:6 Commentary](../../commentary/acts/acts_10_commentary.md#acts_10_6)

[^2]: [Acts 10:21 Commentary](../../commentary/acts/acts_10_commentary.md#acts_10_21)
