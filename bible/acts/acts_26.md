# [Acts 26](https://www.blueletterbible.org/kjv/act/26/1/s_1044001)

<a name="acts_26_1"></a>Acts 26:1

Then [Agrippas](../../strongs/g/g67.md) [phēmi](../../strongs/g/g5346.md) unto [Paulos](../../strongs/g/g3972.md), Thou art [epitrepō](../../strongs/g/g2010.md) to [legō](../../strongs/g/g3004.md) for thyself. Then [Paulos](../../strongs/g/g3972.md) [ekteinō](../../strongs/g/g1614.md) the [cheir](../../strongs/g/g5495.md), and [apologeomai](../../strongs/g/g626.md):

<a name="acts_26_2"></a>Acts 26:2

I [hēgeomai](../../strongs/g/g2233.md) myself [makarios](../../strongs/g/g3107.md), [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), because I shall [apologeomai](../../strongs/g/g626.md) [sēmeron](../../strongs/g/g4594.md) before thee touching all the things whereof I am [egkaleō](../../strongs/g/g1458.md) of the [Ioudaios](../../strongs/g/g2453.md):

<a name="acts_26_3"></a>Acts 26:3

[malista](../../strongs/g/g3122.md) because I [eidō](../../strongs/g/g1492.md) thee to be [gnōstēs](../../strongs/g/g1109.md) in all [ethos](../../strongs/g/g1485.md) and [zētēma](../../strongs/g/g2213.md) which are among the [Ioudaios](../../strongs/g/g2453.md): wherefore I [deomai](../../strongs/g/g1189.md) thee to [akouō](../../strongs/g/g191.md) me [makrothymōs](../../strongs/g/g3116.md).

<a name="acts_26_4"></a>Acts 26:4

My manner of [biōsis](../../strongs/g/g981.md) from my [neotēs](../../strongs/g/g3503.md), which was at the [archē](../../strongs/g/g746.md) among mine [ethnos](../../strongs/g/g1484.md) at [Hierosolyma](../../strongs/g/g2414.md), [isēmi](../../strongs/g/g2467.md) all the [Ioudaios](../../strongs/g/g2453.md);

<a name="acts_26_5"></a>Acts 26:5

Which [proginōskō](../../strongs/g/g4267.md) me from the [anōthen](../../strongs/g/g509.md), if they would [martyreō](../../strongs/g/g3140.md), that after the [akribēs](../../strongs/g/g196.md) [hairesis](../../strongs/g/g139.md) of our [thrēskeia](../../strongs/g/g2356.md) I [zaō](../../strongs/g/g2198.md) a [Pharisaios](../../strongs/g/g5330.md).

<a name="acts_26_6"></a>Acts 26:6

And now I [histēmi](../../strongs/g/g2476.md) and am [krinō](../../strongs/g/g2919.md) for the [elpis](../../strongs/g/g1680.md) of the [epaggelia](../../strongs/g/g1860.md) made of [theos](../../strongs/g/g2316.md), unto our [patēr](../../strongs/g/g3962.md):

<a name="acts_26_7"></a>Acts 26:7

Unto which our [dōdekaphylon](../../strongs/g/g1429.md), [en](../../strongs/g/g1722.md) [ekteneia](../../strongs/g/g1616.md) [latreuō](../../strongs/g/g3000.md) [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md), [elpizō](../../strongs/g/g1679.md) to [katantaō](../../strongs/g/g2658.md). For which [elpis](../../strongs/g/g1680.md), [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), I am [egkaleō](../../strongs/g/g1458.md) of the [Ioudaios](../../strongs/g/g2453.md).

<a name="acts_26_8"></a>Acts 26:8

Why [krinō](../../strongs/g/g2919.md) [apistos](../../strongs/g/g571.md) with you, that [theos](../../strongs/g/g2316.md) should [egeirō](../../strongs/g/g1453.md) the [nekros](../../strongs/g/g3498.md)?

<a name="acts_26_9"></a>Acts 26:9

I [men](../../strongs/g/g3303.md) [dokeō](../../strongs/g/g1380.md) with myself, that I [dei](../../strongs/g/g1163.md) to [prassō](../../strongs/g/g4238.md) [polys](../../strongs/g/g4183.md) [enantios](../../strongs/g/g1727.md) to the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md).

<a name="acts_26_10"></a>Acts 26:10

Which thing I also [poieō](../../strongs/g/g4160.md) in [Hierosolyma](../../strongs/g/g2414.md): and [polys](../../strongs/g/g4183.md) of the [hagios](../../strongs/g/g40.md) did I [katakleiō](../../strongs/g/g2623.md) in [phylakē](../../strongs/g/g5438.md), having [lambanō](../../strongs/g/g2983.md) [exousia](../../strongs/g/g1849.md) from the [archiereus](../../strongs/g/g749.md); and when they were [anaireō](../../strongs/g/g337.md), I [katapherō](../../strongs/g/g2702.md) my [psēphos](../../strongs/g/g5586.md) against them.

<a name="acts_26_11"></a>Acts 26:11

And I [timōreō](../../strongs/g/g5097.md) them oft in every [synagōgē](../../strongs/g/g4864.md), and [anagkazō](../../strongs/g/g315.md) to [blasphēmeō](../../strongs/g/g987.md); and being [perissōs](../../strongs/g/g4057.md) [emmainomai](../../strongs/g/g1693.md) against them, I [diōkō](../../strongs/g/g1377.md) even unto [exō](../../strongs/g/g1854.md) [polis](../../strongs/g/g4172.md).

<a name="acts_26_12"></a>Acts 26:12

Whereupon as I [poreuō](../../strongs/g/g4198.md) to [Damaskos](../../strongs/g/g1154.md) with [exousia](../../strongs/g/g1849.md) and [epitropē](../../strongs/g/g2011.md) from the [archiereus](../../strongs/g/g749.md),

<a name="acts_26_13"></a>Acts 26:13

At midday, O [basileus](../../strongs/g/g935.md), I [eidō](../../strongs/g/g1492.md) in [hodos](../../strongs/g/g3598.md) a [phōs](../../strongs/g/g5457.md) from [ouranothen](../../strongs/g/g3771.md), above the [lamprotēs](../../strongs/g/g2987.md) of the [hēlios](../../strongs/g/g2246.md), [perilampō](../../strongs/g/g4034.md) me and them which [poreuō](../../strongs/g/g4198.md) with me.

<a name="acts_26_14"></a>Acts 26:14

And when we were all [katapiptō](../../strongs/g/g2667.md) to the [gē](../../strongs/g/g1093.md), I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) [laleō](../../strongs/g/g2980.md) unto me, and [legō](../../strongs/g/g3004.md) in the [Hebraïs](../../strongs/g/g1446.md) [dialektos](../../strongs/g/g1258.md), [Saoul](../../strongs/g/g4549.md), [Saoul](../../strongs/g/g4549.md), why [diōkō](../../strongs/g/g1377.md) me? it is [sklēros](../../strongs/g/g4642.md) for thee to [laktizō](../../strongs/g/g2979.md) against the [kentron](../../strongs/g/g2759.md).

<a name="acts_26_15"></a>Acts 26:15

And I [eipon](../../strongs/g/g2036.md), Who art thou, [kyrios](../../strongs/g/g2962.md)? And he [eipon](../../strongs/g/g2036.md), I am [Iēsous](../../strongs/g/g2424.md) whom thou [diōkō](../../strongs/g/g1377.md).

<a name="acts_26_16"></a>Acts 26:16

But [anistēmi](../../strongs/g/g450.md), and [histēmi](../../strongs/g/g2476.md) upon thy [pous](../../strongs/g/g4228.md): for I have [optanomai](../../strongs/g/g3700.md) unto thee for this purpose, to [procheirizō](../../strongs/g/g4400.md) thee a [hypēretēs](../../strongs/g/g5257.md) and a [martys](../../strongs/g/g3144.md) both of these things which thou hast [eidō](../../strongs/g/g1492.md), and of those things in the which I will [optanomai](../../strongs/g/g3700.md) unto thee;

<a name="acts_26_17"></a>Acts 26:17

[exaireō](../../strongs/g/g1807.md) thee from the [laos](../../strongs/g/g2992.md), and from the [ethnos](../../strongs/g/g1484.md), unto whom now I [apostellō](../../strongs/g/g649.md) thee,

<a name="acts_26_18"></a>Acts 26:18

To [anoigō](../../strongs/g/g455.md) their [ophthalmos](../../strongs/g/g3788.md), to [epistrephō](../../strongs/g/g1994.md) from [skotos](../../strongs/g/g4655.md) to [phōs](../../strongs/g/g5457.md), and the [exousia](../../strongs/g/g1849.md) of [Satanas](../../strongs/g/g4567.md) unto [theos](../../strongs/g/g2316.md), that they may [lambanō](../../strongs/g/g2983.md) [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md), and [klēros](../../strongs/g/g2819.md) among them which are [hagiazō](../../strongs/g/g37.md) by [pistis](../../strongs/g/g4102.md) that is in me.

<a name="acts_26_19"></a>Acts 26:19

[hothen](../../strongs/g/g3606.md), O [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), I was not [apeithēs](../../strongs/g/g545.md) unto the [ouranios](../../strongs/g/g3770.md) [optasia](../../strongs/g/g3701.md):

<a name="acts_26_20"></a>Acts 26:20

But [apaggellō](../../strongs/g/g518.md) first unto them of [Damaskos](../../strongs/g/g1154.md), and at [Hierosolyma](../../strongs/g/g2414.md), and throughout all the [chōra](../../strongs/g/g5561.md) of [Ioudaia](../../strongs/g/g2449.md), and to the [ethnos](../../strongs/g/g1484.md), that they should [metanoeō](../../strongs/g/g3340.md) and [epistrephō](../../strongs/g/g1994.md) to [theos](../../strongs/g/g2316.md), and [prassō](../../strongs/g/g4238.md) [ergon](../../strongs/g/g2041.md) [axios](../../strongs/g/g514.md) for [metanoia](../../strongs/g/g3341.md).

<a name="acts_26_21"></a>Acts 26:21

For these causes the [Ioudaios](../../strongs/g/g2453.md) [syllambanō](../../strongs/g/g4815.md) me in the [hieron](../../strongs/g/g2411.md), and [peiraō](../../strongs/g/g3987.md) to [diacheirizō](../../strongs/g/g1315.md).

<a name="acts_26_22"></a>Acts 26:22

Having therefore [tygchanō](../../strongs/g/g5177.md) [epikouria](../../strongs/g/g1947.md) of [theos](../../strongs/g/g2316.md), I [histēmi](../../strongs/g/g2476.md) unto this [hēmera](../../strongs/g/g2250.md), [martyreō](../../strongs/g/g3140.md) both to [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md), [legō](../../strongs/g/g3004.md) [oudeis](../../strongs/g/g3762.md) [ektos](../../strongs/g/g1622.md) those which the [prophētēs](../../strongs/g/g4396.md) and [Mōÿsēs](../../strongs/g/g3475.md) did [laleō](../../strongs/g/g2980.md) should [ginomai](../../strongs/g/g1096.md):

<a name="acts_26_23"></a>Acts 26:23

That [Christos](../../strongs/g/g5547.md) should [pathētos](../../strongs/g/g3805.md), and that he should be the first that should [anastasis](../../strongs/g/g386.md) from the [nekros](../../strongs/g/g3498.md), and should [kataggellō](../../strongs/g/g2605.md) [phōs](../../strongs/g/g5457.md) unto the [laos](../../strongs/g/g2992.md), and to the [ethnos](../../strongs/g/g1484.md).

<a name="acts_26_24"></a>Acts 26:24

And as he thus [apologeomai](../../strongs/g/g626.md), [Phēstos](../../strongs/g/g5347.md) [phēmi](../../strongs/g/g5346.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [Paulos](../../strongs/g/g3972.md), thou art [mainomai](../../strongs/g/g3105.md); [polys](../../strongs/g/g4183.md) [gramma](../../strongs/g/g1121.md) doth [peritrepō](../../strongs/g/g4062.md) thee [mania](../../strongs/g/g3130.md).

<a name="acts_26_25"></a>Acts 26:25

But he [phēmi](../../strongs/g/g5346.md), I am not [mainomai](../../strongs/g/g3105.md), [kratistos](../../strongs/g/g2903.md) [Phēstos](../../strongs/g/g5347.md); but [apophtheggomai](../../strongs/g/g669.md) the [rhēma](../../strongs/g/g4487.md) of [alētheia](../../strongs/g/g225.md) and [sōphrosynē](../../strongs/g/g4997.md).

<a name="acts_26_26"></a>Acts 26:26

For the [basileus](../../strongs/g/g935.md) [epistamai](../../strongs/g/g1987.md) of these things, before whom also I [laleō](../../strongs/g/g2980.md) [parrēsiazomai](../../strongs/g/g3955.md): for I am [peithō](../../strongs/g/g3982.md) that none of these things are [lanthanō](../../strongs/g/g2990.md) from him; for this thing was not [prassō](../../strongs/g/g4238.md) in a [gōnia](../../strongs/g/g1137.md).

<a name="acts_26_27"></a>Acts 26:27

[basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), [pisteuō](../../strongs/g/g4100.md) thou the [prophētēs](../../strongs/g/g4396.md)? I [eidō](../../strongs/g/g1492.md) that thou [pisteuō](../../strongs/g/g4100.md).

<a name="acts_26_28"></a>Acts 26:28

Then [Agrippas](../../strongs/g/g67.md) [phēmi](../../strongs/g/g5346.md) unto [Paulos](../../strongs/g/g3972.md), [oligos](../../strongs/g/g3641.md) thou [peithō](../../strongs/g/g3982.md) me to be a [Christianos](../../strongs/g/g5546.md).

<a name="acts_26_29"></a>Acts 26:29

And [Paulos](../../strongs/g/g3972.md) [eipon](../../strongs/g/g2036.md), I [euchomai](../../strongs/g/g2172.md) to [theos](../../strongs/g/g2316.md), that not only thou, but also all that [akouō](../../strongs/g/g191.md) me [sēmeron](../../strongs/g/g4594.md), were both [en](../../strongs/g/g1722.md) [oligos](../../strongs/g/g3641.md), and [en](../../strongs/g/g1722.md) [polys](../../strongs/g/g4183.md) such [hopoios](../../strongs/g/g3697.md) I am, [parektos](../../strongs/g/g3924.md) these [desmos](../../strongs/g/g1199.md).

<a name="acts_26_30"></a>Acts 26:30

And when he had thus [eipon](../../strongs/g/g2036.md), the [basileus](../../strongs/g/g935.md) [anistēmi](../../strongs/g/g450.md), and the [hēgemōn](../../strongs/g/g2232.md), and [Bernikē](../../strongs/g/g959.md), and they that [sygkathēmai](../../strongs/g/g4775.md) with them:

<a name="acts_26_31"></a>Acts 26:31

And when they were [anachōreō](../../strongs/g/g402.md), they [laleō](../../strongs/g/g2980.md) between [allēlōn](../../strongs/g/g240.md), [legō](../../strongs/g/g3004.md), This [anthrōpos](../../strongs/g/g444.md) [prassō](../../strongs/g/g4238.md) nothing [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md) or of [desmos](../../strongs/g/g1199.md).

<a name="acts_26_32"></a>Acts 26:32

Then [phēmi](../../strongs/g/g5346.md) [Agrippas](../../strongs/g/g67.md) unto [Phēstos](../../strongs/g/g5347.md), This [anthrōpos](../../strongs/g/g444.md) might have been [apolyō](../../strongs/g/g630.md), if he had not [epikaleō](../../strongs/g/g1941.md) unto [Kaisar](../../strongs/g/g2541.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 25](acts_25.md) - [Acts 27](acts_27.md)