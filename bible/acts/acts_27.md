# [Acts 27](https://www.blueletterbible.org/kjv/act/27/1/s_1045001)

<a name="acts_27_1"></a>Acts 27:1

And when it was [krinō](../../strongs/g/g2919.md) that we should [apopleō](../../strongs/g/g636.md) into [Italia](../../strongs/g/g2482.md), they [paradidōmi](../../strongs/g/g3860.md) [Paulos](../../strongs/g/g3972.md) and certain other [desmōtēs](../../strongs/g/g1202.md) unto one [onoma](../../strongs/g/g3686.md) [Ioulios](../../strongs/g/g2457.md), a [hekatontarchēs](../../strongs/g/g1543.md) of [sebastos](../../strongs/g/g4575.md) [speira](../../strongs/g/g4686.md).

<a name="acts_27_2"></a>Acts 27:2

And [epibainō](../../strongs/g/g1910.md) into a [ploion](../../strongs/g/g4143.md) of [Adramyttēnos](../../strongs/g/g98.md), we [anagō](../../strongs/g/g321.md), [mellō](../../strongs/g/g3195.md) to [pleō](../../strongs/g/g4126.md) by the [topos](../../strongs/g/g5117.md) of [Asia](../../strongs/g/g773.md); [Aristarchos](../../strongs/g/g708.md), [Makedōn](../../strongs/g/g3110.md) of [Thessalonikeus](../../strongs/g/g2331.md), being with us.

<a name="acts_27_3"></a>Acts 27:3

And the next we [katagō](../../strongs/g/g2609.md) at [Sidōn](../../strongs/g/g4605.md). And [Ioulios](../../strongs/g/g2457.md) [philanthrōpōs](../../strongs/g/g5364.md) [chraomai](../../strongs/g/g5530.md) [Paulos](../../strongs/g/g3972.md), and [epitrepō](../../strongs/g/g2010.md) him to [poreuō](../../strongs/g/g4198.md) unto his [philos](../../strongs/g/g5384.md) to [tygchanō](../../strongs/g/g5177.md) [epimeleia](../../strongs/g/g1958.md).

<a name="acts_27_4"></a>Acts 27:4

And when we had [anagō](../../strongs/g/g321.md) from thence, we [hypopleō](../../strongs/g/g5284.md) under [Kypros](../../strongs/g/g2954.md), because the [anemos](../../strongs/g/g417.md) were [enantios](../../strongs/g/g1727.md).

<a name="acts_27_5"></a>Acts 27:5

And when we had [diapleō](../../strongs/g/g1277.md) the [pelagos](../../strongs/g/g3989.md) of [Kilikia](../../strongs/g/g2791.md) and [Pamphylia](../../strongs/g/g3828.md), we [katerchomai](../../strongs/g/g2718.md) to [Myra](../../strongs/g/g3460.md), of [Lykia](../../strongs/g/g3073.md).

<a name="acts_27_6"></a>Acts 27:6

And there the [hekatontarchēs](../../strongs/g/g1543.md) [heuriskō](../../strongs/g/g2147.md) a [ploion](../../strongs/g/g4143.md) of [Alexandrinos](../../strongs/g/g222.md) [pleō](../../strongs/g/g4126.md) into [Italia](../../strongs/g/g2482.md); and he [embibazō](../../strongs/g/g1688.md) us therein.

<a name="acts_27_7"></a>Acts 27:7

And when we had [bradyploeō](../../strongs/g/g1020.md) [hikanos](../../strongs/g/g2425.md) [hēmera](../../strongs/g/g2250.md), and [molis](../../strongs/g/g3433.md) were [ginomai](../../strongs/g/g1096.md) over against [Knidos](../../strongs/g/g2834.md), the [anemos](../../strongs/g/g417.md) not [proseaō](../../strongs/g/g4330.md) us, we [hypopleō](../../strongs/g/g5284.md) [Krētē](../../strongs/g/g2914.md), over against [Salmōnē](../../strongs/g/g4534.md);

<a name="acts_27_8"></a>Acts 27:8

And, [molis](../../strongs/g/g3433.md) [paralegomai](../../strongs/g/g3881.md) it, [erchomai](../../strongs/g/g2064.md) unto a [topos](../../strongs/g/g5117.md) which is [kaleō](../../strongs/g/g2564.md) [kalos](../../strongs/g/g2570.md) [Kaloi Limenes](../../strongs/g/g2568.md) [limēn](../../strongs/g/g3040.md); [eggys](../../strongs/g/g1451.md) whereunto was the [polis](../../strongs/g/g4172.md) of [Lasaia](../../strongs/g/g2996.md).

<a name="acts_27_9"></a>Acts 27:9

Now when [hikanos](../../strongs/g/g2425.md) [chronos](../../strongs/g/g5550.md) was [diaginomai](../../strongs/g/g1230.md), and when [ploos](../../strongs/g/g4144.md) was now [episphalēs](../../strongs/g/g2000.md), because the [nēsteia](../../strongs/g/g3521.md) was now already [parerchomai](../../strongs/g/g3928.md), [Paulos](../../strongs/g/g3972.md) [paraineō](../../strongs/g/g3867.md),

<a name="acts_27_10"></a>Acts 27:10

And [legō](../../strongs/g/g3004.md) unto them, [anēr](../../strongs/g/g435.md), I [theōreō](../../strongs/g/g2334.md) that this [ploos](../../strongs/g/g4144.md) will be with [hybris](../../strongs/g/g5196.md) and [polys](../../strongs/g/g4183.md) [zēmia](../../strongs/g/g2209.md), not only of the [phortos](../../strongs/g/g5414.md) and [ploion](../../strongs/g/g4143.md), but also of our [psychē](../../strongs/g/g5590.md).

<a name="acts_27_11"></a>Acts 27:11

Nevertheless the [hekatontarchēs](../../strongs/g/g1543.md) [peithō](../../strongs/g/g3982.md) the [kybernētēs](../../strongs/g/g2942.md) and the [nauklēros](../../strongs/g/g3490.md), more than those things which were [legō](../../strongs/g/g3004.md) by [Paulos](../../strongs/g/g3972.md).

<a name="acts_27_12"></a>Acts 27:12

And because the [limēn](../../strongs/g/g3040.md) was [aneuthetos](../../strongs/g/g428.md) to [paracheimasia](../../strongs/g/g3915.md), the [pleiōn](../../strongs/g/g4119.md) [boulē](../../strongs/g/g1012.md) [tithēmi](../../strongs/g/g5087.md) to [anagō](../../strongs/g/g321.md) thence also, [ei pōs](../../strongs/g/g1513.md) by any [pōs](../../strongs/g/g4458.md) they might [katantaō](../../strongs/g/g2658.md) to [Phoinix](../../strongs/g/g5405.md), and there [paracheimazō](../../strongs/g/g3914.md); which is a [limēn](../../strongs/g/g3040.md) of [Krētē](../../strongs/g/g2914.md), and [blepō](../../strongs/g/g991.md) toward the [lips](../../strongs/g/g3047.md) and [chōros](../../strongs/g/g5566.md).

<a name="acts_27_13"></a>Acts 27:13

And when the [notos](../../strongs/g/g3558.md) [hypopneō](../../strongs/g/g5285.md), [dokeō](../../strongs/g/g1380.md) that they had [krateō](../../strongs/g/g2902.md) their [prothesis](../../strongs/g/g4286.md), [airō](../../strongs/g/g142.md) thence, they [paralegomai](../../strongs/g/g3881.md) close by [Krētē](../../strongs/g/g2914.md).

<a name="acts_27_14"></a>Acts 27:14

But not [polys](../../strongs/g/g4183.md) after there [ballō](../../strongs/g/g906.md) against it a [typhōnikos](../../strongs/g/g5189.md) [anemos](../../strongs/g/g417.md), [kaleō](../../strongs/g/g2564.md) [euroklydōn](../../strongs/g/g2148.md).

<a name="acts_27_15"></a>Acts 27:15

And when the [ploion](../../strongs/g/g4143.md) was [synarpazō](../../strongs/g/g4884.md), and could not [antophthalmeō](../../strongs/g/g503.md) the [anemos](../../strongs/g/g417.md), we let her [epididōmi](../../strongs/g/g1929.md) [pherō](../../strongs/g/g5342.md).

<a name="acts_27_16"></a>Acts 27:16

And [hypotrechō](../../strongs/g/g5295.md) a certain [nēsion](../../strongs/g/g3519.md) which is [kaleō](../../strongs/g/g2564.md) [Kauda](../../strongs/g/g2802.md), we had [molis](../../strongs/g/g3433.md) [ischyō](../../strongs/g/g2480.md) to [perikratēs](../../strongs/g/g4031.md) [ginomai](../../strongs/g/g1096.md) the [skaphē](../../strongs/g/g4627.md):

<a name="acts_27_17"></a>Acts 27:17

Which when they had [airō](../../strongs/g/g142.md), they [chraomai](../../strongs/g/g5530.md) [boētheia](../../strongs/g/g996.md), [hypozōnnymi](../../strongs/g/g5269.md) the [ploion](../../strongs/g/g4143.md); and, [phobeō](../../strongs/g/g5399.md) lest they should [ekpiptō](../../strongs/g/g1601.md) into the [Syrtis](../../strongs/g/g4950.md), [chalaō](../../strongs/g/g5465.md) [skeuos](../../strongs/g/g4632.md), and so were [pherō](../../strongs/g/g5342.md).

<a name="acts_27_18"></a>Acts 27:18

And we being [sphodrōs](../../strongs/g/g4971.md) [cheimazō](../../strongs/g/g5492.md), the [hexēs](../../strongs/g/g1836.md) they [ekbolē](../../strongs/g/g1546.md) [poieō](../../strongs/g/g4160.md);

<a name="acts_27_19"></a>Acts 27:19

And the third we [rhiptō](../../strongs/g/g4496.md) with our [autocheir](../../strongs/g/g849.md) the [skeuē](../../strongs/g/g4631.md) of the [ploion](../../strongs/g/g4143.md).

<a name="acts_27_20"></a>Acts 27:20

And when neither [hēlios](../../strongs/g/g2246.md) nor [astron](../../strongs/g/g798.md) in many days [epiphainō](../../strongs/g/g2014.md), and no [oligos](../../strongs/g/g3641.md) [cheimōn](../../strongs/g/g5494.md) [epikeimai](../../strongs/g/g1945.md) us, all [elpis](../../strongs/g/g1680.md) that we should be [sōzō](../../strongs/g/g4982.md) was then [periaireō](../../strongs/g/g4014.md).

<a name="acts_27_21"></a>Acts 27:21

But after [polys](../../strongs/g/g4183.md) [asitia](../../strongs/g/g776.md) [tote](../../strongs/g/g5119.md) [Paulos](../../strongs/g/g3972.md) [histēmi](../../strongs/g/g2476.md) in the midst of them, and [eipon](../../strongs/g/g2036.md), [ō](../../strongs/g/g5599.md) [anēr](../../strongs/g/g435.md), ye should have [peitharcheō](../../strongs/g/g3980.md) unto me, and not have [anagō](../../strongs/g/g321.md) from [Krētē](../../strongs/g/g2914.md), and to have [kerdainō](../../strongs/g/g2770.md) this [hybris](../../strongs/g/g5196.md) and [zēmia](../../strongs/g/g2209.md).

<a name="acts_27_22"></a>Acts 27:22

And [ta nyn](../../strongs/g/g3569.md) I [paraineō](../../strongs/g/g3867.md) you to [euthymeō](../../strongs/g/g2114.md): for there shall be no [apobolē](../../strongs/g/g580.md) of [psychē](../../strongs/g/g5590.md) among you, but of the [ploion](../../strongs/g/g4143.md).

<a name="acts_27_23"></a>Acts 27:23

For there [paristēmi](../../strongs/g/g3936.md) me this [nyx](../../strongs/g/g3571.md) the [aggelos](../../strongs/g/g32.md) of [theos](../../strongs/g/g2316.md), whose I am, and whom I [latreuō](../../strongs/g/g3000.md),

<a name="acts_27_24"></a>Acts 27:24

[legō](../../strongs/g/g3004.md), [phobeō](../../strongs/g/g5399.md) not, [Paulos](../../strongs/g/g3972.md); thou must be [paristēmi](../../strongs/g/g3936.md) before [Kaisar](../../strongs/g/g2541.md): and, [idou](../../strongs/g/g2400.md), [theos](../../strongs/g/g2316.md) hath [charizomai](../../strongs/g/g5483.md) thee all them that [pleō](../../strongs/g/g4126.md) with thee.

<a name="acts_27_25"></a>Acts 27:25

Wherefore, [anēr](../../strongs/g/g435.md), [euthymeō](../../strongs/g/g2114.md): for I [pisteuō](../../strongs/g/g4100.md) [theos](../../strongs/g/g2316.md), that it shall be even as it was [laleō](../../strongs/g/g2980.md) me.

<a name="acts_27_26"></a>Acts 27:26

Howbeit we must be [ekpiptō](../../strongs/g/g1601.md) upon a certain [nēsos](../../strongs/g/g3520.md).

<a name="acts_27_27"></a>Acts 27:27

But when the fourteenth [nyx](../../strongs/g/g3571.md) was [ginomai](../../strongs/g/g1096.md), as we were [diapherō](../../strongs/g/g1308.md) in [Adrias](../../strongs/g/g99.md), about midnight the [nautēs](../../strongs/g/g3492.md) [hyponoeō](../../strongs/g/g5282.md) that they [prosagō](../../strongs/g/g4317.md) to some [chōra](../../strongs/g/g5561.md);

<a name="acts_27_28"></a>Acts 27:28

And [bolizō](../../strongs/g/g1001.md), and [heuriskō](../../strongs/g/g2147.md) twenty [orguia](../../strongs/g/g3712.md): and when they had [diïstēmi](../../strongs/g/g1339.md) a [brachys](../../strongs/g/g1024.md), they [bolizō](../../strongs/g/g1001.md) again, and [heuriskō](../../strongs/g/g2147.md) it fifteen [orguia](../../strongs/g/g3712.md).

<a name="acts_27_29"></a>Acts 27:29

Then [phobeō](../../strongs/g/g5399.md) lest we should have [ekpiptō](../../strongs/g/g1601.md) upon [topos](../../strongs/g/g5117.md) [trachys](../../strongs/g/g5138.md), they [rhiptō](../../strongs/g/g4496.md) four [agkyra](../../strongs/g/g45.md) out of the [prymna](../../strongs/g/g4403.md), and [euchomai](../../strongs/g/g2172.md) [ginomai](../../strongs/g/g1096.md)for the day.

<a name="acts_27_30"></a>Acts 27:30

And as the [nautēs](../../strongs/g/g3492.md) were [zēteō](../../strongs/g/g2212.md) [pheugō](../../strongs/g/g5343.md) out of the [ploion](../../strongs/g/g4143.md), when they had [chalaō](../../strongs/g/g5465.md) the [skaphē](../../strongs/g/g4627.md) into the [thalassa](../../strongs/g/g2281.md), [prophasis](../../strongs/g/g4392.md) as though they would have [ekteinō](../../strongs/g/g1614.md) [agkyra](../../strongs/g/g45.md) out of the [prōra](../../strongs/g/g4408.md),

<a name="acts_27_31"></a>Acts 27:31

[Paulos](../../strongs/g/g3972.md) [eipon](../../strongs/g/g2036.md) to the [hekatontarchēs](../../strongs/g/g1543.md) and to the [stratiōtēs](../../strongs/g/g4757.md), Except these [menō](../../strongs/g/g3306.md) in the [ploion](../../strongs/g/g4143.md), ye cannot be [sōzō](../../strongs/g/g4982.md).

<a name="acts_27_32"></a>Acts 27:32

Then the [stratiōtēs](../../strongs/g/g4757.md) [apokoptō](../../strongs/g/g609.md) the [schoinion](../../strongs/g/g4979.md) of the [skaphē](../../strongs/g/g4627.md), and [eaō](../../strongs/g/g1439.md) her [ekpiptō](../../strongs/g/g1601.md).

<a name="acts_27_33"></a>Acts 27:33

And while the [hēmera](../../strongs/g/g2250.md) was [ginomai](../../strongs/g/g1096.md) on, [Paulos](../../strongs/g/g3972.md) [parakaleō](../../strongs/g/g3870.md) them all to [metalambanō](../../strongs/g/g3335.md) [trophē](../../strongs/g/g5160.md), [legō](../../strongs/g/g3004.md), [sēmeron](../../strongs/g/g4594.md) is the fourteenth [hēmera](../../strongs/g/g2250.md) that ye have [prosdokaō](../../strongs/g/g4328.md) and [diateleō](../../strongs/g/g1300.md) [asitos](../../strongs/g/g777.md), having [proslambanō](../../strongs/g/g4355.md) [mēdeis](../../strongs/g/g3367.md).

<a name="acts_27_34"></a>Acts 27:34

Wherefore I [parakaleō](../../strongs/g/g3870.md) you to [proslambanō](../../strongs/g/g4355.md) some [trophē](../../strongs/g/g5160.md): for this is for your [sōtēria](../../strongs/g/g4991.md): for there shall not a [thrix](../../strongs/g/g2359.md) [piptō](../../strongs/g/g4098.md) from the [kephalē](../../strongs/g/g2776.md) of any of you.

<a name="acts_27_35"></a>Acts 27:35

And when he had thus [eipon](../../strongs/g/g2036.md), he [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and [eucharisteō](../../strongs/g/g2168.md) to [theos](../../strongs/g/g2316.md) in presence of them all: and when he had [klaō](../../strongs/g/g2806.md) it, he [archomai](../../strongs/g/g756.md) to [esthiō](../../strongs/g/g2068.md).

<a name="acts_27_36"></a>Acts 27:36

Then were they all of [euthymos](../../strongs/g/g2115.md), and they also [proslambanō](../../strongs/g/g4355.md) [trophē](../../strongs/g/g5160.md).

<a name="acts_27_37"></a>Acts 27:37

And we were in all in the [ploion](../../strongs/g/g4143.md) two hundred threescore and sixteen [psychē](../../strongs/g/g5590.md).

<a name="acts_27_38"></a>Acts 27:38

And when they had [trophē](../../strongs/g/g5160.md) [korennymi](../../strongs/g/g2880.md), they [kouphizō](../../strongs/g/g2893.md) the [ploion](../../strongs/g/g4143.md), and [ekballō](../../strongs/g/g1544.md) the [sitos](../../strongs/g/g4621.md) into the [thalassa](../../strongs/g/g2281.md).

<a name="acts_27_39"></a>Acts 27:39

And when it was [hēmera](../../strongs/g/g2250.md), they [epiginōskō](../../strongs/g/g1921.md) not the [gē](../../strongs/g/g1093.md): but they [katanoeō](../../strongs/g/g2657.md) a certain [kolpos](../../strongs/g/g2859.md) with [aigialos](../../strongs/g/g123.md), into the which they were [bouleuō](../../strongs/g/g1011.md), if it were possible, to [exōtheō](../../strongs/g/g1856.md) the [ploion](../../strongs/g/g4143.md).

<a name="acts_27_40"></a>Acts 27:40

And when they had [periaireō](../../strongs/g/g4014.md) the [agkyra](../../strongs/g/g45.md), they [eaō](../../strongs/g/g1439.md) themselves unto the [thalassa](../../strongs/g/g2281.md), and [aniēmi](../../strongs/g/g447.md) the [pēdalion](../../strongs/g/g4079.md) [zeuktēria](../../strongs/g/g2202.md), and [epairō](../../strongs/g/g1869.md) the [artemōn](../../strongs/g/g736.md) to the [pneō](../../strongs/g/g4154.md), and [katechō](../../strongs/g/g2722.md) toward [aigialos](../../strongs/g/g123.md).

<a name="acts_27_41"></a>Acts 27:41

And [peripiptō](../../strongs/g/g4045.md) into a [topos](../../strongs/g/g5117.md) [dithalassos](../../strongs/g/g1337.md), they [epikellō](../../strongs/g/g2027.md) the [naus](../../strongs/g/g3491.md); and the [prōra](../../strongs/g/g4408.md) [ereidō](../../strongs/g/g2043.md), and [menō](../../strongs/g/g3306.md) [asaleutos](../../strongs/g/g761.md), but the [prymna](../../strongs/g/g4403.md) was [lyō](../../strongs/g/g3089.md) with the [bia](../../strongs/g/g970.md) of the [kyma](../../strongs/g/g2949.md).

<a name="acts_27_42"></a>Acts 27:42

And the [stratiōtēs](../../strongs/g/g4757.md) [boulē](../../strongs/g/g1012.md) was to [apokteinō](../../strongs/g/g615.md) the [desmōtēs](../../strongs/g/g1202.md), lest any of them should [ekkolymbaō](../../strongs/g/g1579.md), and [diapheugō](../../strongs/g/g1309.md).

<a name="acts_27_43"></a>Acts 27:43

But the [hekatontarchēs](../../strongs/g/g1543.md), [boulomai](../../strongs/g/g1014.md) to [diasōzō](../../strongs/g/g1295.md) [Paulos](../../strongs/g/g3972.md), [kōlyō](../../strongs/g/g2967.md) them from [boulēma](../../strongs/g/g1013.md); and [keleuō](../../strongs/g/g2753.md) that they which could [kolymbaō](../../strongs/g/g2860.md) should [aporriptō](../../strongs/g/g641.md) first, and [exeimi](../../strongs/g/g1826.md) to [gē](../../strongs/g/g1093.md):

<a name="acts_27_44"></a>Acts 27:44

And the [loipos](../../strongs/g/g3062.md), some on [sanis](../../strongs/g/g4548.md), and some [apo](../../strongs/g/g575.md) the [ploion](../../strongs/g/g4143.md). And so it [ginomai](../../strongs/g/g1096.md), that they [diasōzō](../../strongs/g/g1295.md) all to [gē](../../strongs/g/g1093.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 26](acts_26.md) - [Acts 28](acts_28.md)