# [Acts 13](https://www.blueletterbible.org/kjv/act/13/1/s_1031001)

<a name="acts_13_1"></a>Acts 13:1

Now there were in the [ekklēsia](../../strongs/g/g1577.md) that was at [Antiocheia](../../strongs/g/g490.md) certain [prophētēs](../../strongs/g/g4396.md) and [didaskalos](../../strongs/g/g1320.md); as [Barnabas](../../strongs/g/g921.md), and [Symeōn](../../strongs/g/g4826.md) that was [kaleō](../../strongs/g/g2564.md) [Niger](../../strongs/g/g3526.md), and [Loukios](../../strongs/g/g3066.md) of [Kyrēnaios](../../strongs/g/g2956.md), and [Manaēn](../../strongs/g/g3127.md), which had been [syntrophos](../../strongs/g/g4939.md) with [Hērōdēs](../../strongs/g/g2264.md) the [tetraarchēs](../../strongs/g/g5076.md), and [Saulos](../../strongs/g/g4569.md).

<a name="acts_13_2"></a>Acts 13:2

As they [leitourgeō](../../strongs/g/g3008.md) to the [kyrios](../../strongs/g/g2962.md), and [nēsteuō](../../strongs/g/g3522.md), the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [eipon](../../strongs/g/g2036.md), [aphorizō](../../strongs/g/g873.md) me [Barnabas](../../strongs/g/g921.md) and [Saulos](../../strongs/g/g4569.md) for the [ergon](../../strongs/g/g2041.md) whereunto I have [proskaleō](../../strongs/g/g4341.md) them.

<a name="acts_13_3"></a>Acts 13:3

And when they had [nēsteuō](../../strongs/g/g3522.md) and [proseuchomai](../../strongs/g/g4336.md), and [epitithēmi](../../strongs/g/g2007.md) their [cheir](../../strongs/g/g5495.md) on them, [apolyō](../../strongs/g/g630.md).

<a name="acts_13_4"></a>Acts 13:4

So they, being [ekpempō](../../strongs/g/g1599.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), [katerchomai](../../strongs/g/g2718.md) unto [Seleukeia](../../strongs/g/g4581.md); and from thence they [apopleō](../../strongs/g/g636.md) to [Kypros](../../strongs/g/g2954.md).

<a name="acts_13_5"></a>Acts 13:5

And when they were at [Salamis](../../strongs/g/g4529.md), they [kataggellō](../../strongs/g/g2605.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) in the [synagōgē](../../strongs/g/g4864.md) of the [Ioudaios](../../strongs/g/g2453.md): and they had also [Iōannēs](../../strongs/g/g2491.md) [hypēretēs](../../strongs/g/g5257.md).

<a name="acts_13_6"></a>Acts 13:6

And when they had [dierchomai](../../strongs/g/g1330.md) the [nēsos](../../strongs/g/g3520.md) unto [Paphos](../../strongs/g/g3974.md), they [heuriskō](../../strongs/g/g2147.md) a certain [magos](../../strongs/g/g3097.md), [pseudoprophētēs](../../strongs/g/g5578.md), an [Ioudaios](../../strongs/g/g2453.md), whose [onoma](../../strongs/g/g3686.md) was [Bariēsou](../../strongs/g/g919.md):

<a name="acts_13_7"></a>Acts 13:7

Which was with the [anthypatos](../../strongs/g/g446.md), [Sergios](../../strongs/g/g4588.md) [Paulos](../../strongs/g/g3972.md), a [synetos](../../strongs/g/g4908.md) [anēr](../../strongs/g/g435.md); who [proskaleō](../../strongs/g/g4341.md) for [Barnabas](../../strongs/g/g921.md) and [Saulos](../../strongs/g/g4569.md), and [epizēteō](../../strongs/g/g1934.md) to [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_13_8"></a>Acts 13:8

But [Elymas](../../strongs/g/g1681.md) the [magos](../../strongs/g/g3097.md) (for so is his [onoma](../../strongs/g/g3686.md) by [methermēneuō](../../strongs/g/g3177.md)) [anthistēmi](../../strongs/g/g436.md) them, [zēteō](../../strongs/g/g2212.md) to [diastrephō](../../strongs/g/g1294.md) the [anthypatos](../../strongs/g/g446.md) from the [pistis](../../strongs/g/g4102.md).

<a name="acts_13_9"></a>Acts 13:9

Then [Saulos](../../strongs/g/g4569.md), (who also [Paulos](../../strongs/g/g3972.md),) [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), [atenizō](../../strongs/g/g816.md) on him.

<a name="acts_13_10"></a>Acts 13:10

And [eipon](../../strongs/g/g2036.md), [ō](../../strongs/g/g5599.md) [plērēs](../../strongs/g/g4134.md) of all [dolos](../../strongs/g/g1388.md) and all [rhadiourgia](../../strongs/g/g4468.md), [huios](../../strongs/g/g5207.md) of the [diabolos](../../strongs/g/g1228.md), [echthros](../../strongs/g/g2190.md) of all [dikaiosynē](../../strongs/g/g1343.md), wilt thou not [pauō](../../strongs/g/g3973.md) to [diastrephō](../../strongs/g/g1294.md) the [euthys](../../strongs/g/g2117.md) [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md)?

<a name="acts_13_11"></a>Acts 13:11

And now, [idou](../../strongs/g/g2400.md), the [cheir](../../strongs/g/g5495.md) of the [kyrios](../../strongs/g/g2962.md) is upon thee, and thou shalt be [typhlos](../../strongs/g/g5185.md), not [blepō](../../strongs/g/g991.md) the [hēlios](../../strongs/g/g2246.md) for a [kairos](../../strongs/g/g2540.md). And [parachrēma](../../strongs/g/g3916.md) there [epipiptō](../../strongs/g/g1968.md) on him an [achlys](../../strongs/g/g887.md) and a [skotos](../../strongs/g/g4655.md); and he [periagō](../../strongs/g/g4013.md) [zēteō](../../strongs/g/g2212.md) [cheiragōgos](../../strongs/g/g5497.md).

<a name="acts_13_12"></a>Acts 13:12

Then the [anthypatos](../../strongs/g/g446.md), when he [eidō](../../strongs/g/g1492.md) what was done, [pisteuō](../../strongs/g/g4100.md), [ekplēssō](../../strongs/g/g1605.md) at the [didachē](../../strongs/g/g1322.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="acts_13_13"></a>Acts 13:13

Now when [Paulos](../../strongs/g/g3972.md) and his [peri](../../strongs/g/g4012.md) [anagō](../../strongs/g/g321.md) from [Paphos](../../strongs/g/g3974.md), they [erchomai](../../strongs/g/g2064.md) to [Pergē](../../strongs/g/g4011.md) in [Pamphylia](../../strongs/g/g3828.md): and [Iōannēs](../../strongs/g/g2491.md) [apochōreō](../../strongs/g/g672.md) from them [hypostrephō](../../strongs/g/g5290.md) to [Hierosolyma](../../strongs/g/g2414.md).

<a name="acts_13_14"></a>Acts 13:14

But when they [dierchomai](../../strongs/g/g1330.md) from [Pergē](../../strongs/g/g4011.md), they [paraginomai](../../strongs/g/g3854.md) to [Antiocheia](../../strongs/g/g490.md) in [Pisidia](../../strongs/g/g4099.md), and [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md) on the [sabbaton](../../strongs/g/g4521.md), and [kathizō](../../strongs/g/g2523.md).

<a name="acts_13_15"></a>Acts 13:15

And after the [anagnōsis](../../strongs/g/g320.md) of the [nomos](../../strongs/g/g3551.md) and the [prophētēs](../../strongs/g/g4396.md) the [archisynagōgos](../../strongs/g/g752.md) [apostellō](../../strongs/g/g649.md) unto them, [legō](../../strongs/g/g3004.md), Ye [anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), if ye have any [logos](../../strongs/g/g3056.md) of [paraklēsis](../../strongs/g/g3874.md) for the [laos](../../strongs/g/g2992.md), [legō](../../strongs/g/g3004.md).

<a name="acts_13_16"></a>Acts 13:16

Then [Paulos](../../strongs/g/g3972.md) [anistēmi](../../strongs/g/g450.md), and [kataseiō](../../strongs/g/g2678.md) with his [cheir](../../strongs/g/g5495.md) [eipon](../../strongs/g/g2036.md), [anēr](../../strongs/g/g435.md) of [Israēlitēs](../../strongs/g/g2475.md), and ye that [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), [akouō](../../strongs/g/g191.md).

<a name="acts_13_17"></a>Acts 13:17

The [theos](../../strongs/g/g2316.md) of this [laos](../../strongs/g/g2992.md) of [Israēl](../../strongs/g/g2474.md) [eklegomai](../../strongs/g/g1586.md) our [patēr](../../strongs/g/g3962.md), and [hypsoō](../../strongs/g/g5312.md) the [laos](../../strongs/g/g2992.md) when they [paroikia](../../strongs/g/g3940.md) in the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md), and with an [hypsēlos](../../strongs/g/g5308.md) [brachiōn](../../strongs/g/g1023.md) [exagō](../../strongs/g/g1806.md) he them out of it.

<a name="acts_13_18"></a>Acts 13:18

And about the [chronos](../../strongs/g/g5550.md) of forty years [tropophoreō](../../strongs/g/g5159.md) in the [erēmos](../../strongs/g/g2048.md).

<a name="acts_13_19"></a>Acts 13:19

And when he had [kathaireō](../../strongs/g/g2507.md) seven [ethnos](../../strongs/g/g1484.md) in the [gē](../../strongs/g/g1093.md) of [Chanaan](../../strongs/g/g5477.md), he [kataklēronomeō](../../strongs/g/g2624.md) their [gē](../../strongs/g/g1093.md) to them.

<a name="acts_13_20"></a>Acts 13:20

And after that he [didōmi](../../strongs/g/g1325.md) [kritēs](../../strongs/g/g2923.md) about the space of four hundred and fifty [etos](../../strongs/g/g2094.md), until [Samouēl](../../strongs/g/g4545.md) the [prophētēs](../../strongs/g/g4396.md).

<a name="acts_13_21"></a>Acts 13:21

And [kakeithen](../../strongs/g/g2547.md) they [aiteō](../../strongs/g/g154.md) a [basileus](../../strongs/g/g935.md): and [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) unto them [Saoul](../../strongs/g/g4549.md) the [huios](../../strongs/g/g5207.md) of [Kis](../../strongs/g/g2797.md), an [anēr](../../strongs/g/g435.md) of the [phylē](../../strongs/g/g5443.md) of [Beniam(e)in](../../strongs/g/g958.md), by the space of forty [etos](../../strongs/g/g2094.md).

<a name="acts_13_22"></a>Acts 13:22

And when he had [methistēmi](../../strongs/g/g3179.md) him, he [egeirō](../../strongs/g/g1453.md) unto them [Dabid](../../strongs/g/g1138.md) to be their [basileus](../../strongs/g/g935.md); to whom also he [martyreō](../../strongs/g/g3140.md), and [eipon](../../strongs/g/g2036.md), I have [heuriskō](../../strongs/g/g2147.md) [Dabid](../../strongs/g/g1138.md) of [Iessai](../../strongs/g/g2421.md), an [anēr](../../strongs/g/g435.md) after mine own [kardia](../../strongs/g/g2588.md), which shall [poieō](../../strongs/g/g4160.md) all my [thelēma](../../strongs/g/g2307.md).

<a name="acts_13_23"></a>Acts 13:23

Of this man's [sperma](../../strongs/g/g4690.md) hath [theos](../../strongs/g/g2316.md) according to his [epaggelia](../../strongs/g/g1860.md) [egeirō](../../strongs/g/g1453.md) unto [Israēl](../../strongs/g/g2474.md) a [sōtēr](../../strongs/g/g4990.md), [Iēsous](../../strongs/g/g2424.md):

<a name="acts_13_24"></a>Acts 13:24

When [Iōannēs](../../strongs/g/g2491.md) had [prokēryssō](../../strongs/g/g4296.md) before his [prosōpon](../../strongs/g/g4383.md) [eisodos](../../strongs/g/g1529.md) the [baptisma](../../strongs/g/g908.md) of [metanoia](../../strongs/g/g3341.md) to all the [laos](../../strongs/g/g2992.md) of [Israēl](../../strongs/g/g2474.md).

<a name="acts_13_25"></a>Acts 13:25

And as [Iōannēs](../../strongs/g/g2491.md) [plēroō](../../strongs/g/g4137.md) his [dromos](../../strongs/g/g1408.md), he [legō](../../strongs/g/g3004.md), Whom [hyponoeō](../../strongs/g/g5282.md) ye that I am?  I am not he. But, [idou](../../strongs/g/g2400.md), there [erchomai](../../strongs/g/g2064.md) one after me, whose [hypodēma](../../strongs/g/g5266.md) of his [pous](../../strongs/g/g4228.md) I am not [axios](../../strongs/g/g514.md) to [lyō](../../strongs/g/g3089.md).

<a name="acts_13_26"></a>Acts 13:26

[anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), [huios](../../strongs/g/g5207.md) of the [genos](../../strongs/g/g1085.md) of [Abraam](../../strongs/g/g11.md), and whosoever among you [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), to you is the [logos](../../strongs/g/g3056.md) of this [sōtēria](../../strongs/g/g4991.md) [apostellō](../../strongs/g/g649.md).

<a name="acts_13_27"></a>Acts 13:27

For they that [katoikeō](../../strongs/g/g2730.md) at [Ierousalēm](../../strongs/g/g2419.md), and their [archōn](../../strongs/g/g758.md), because they [agnoeō](../../strongs/g/g50.md) him, nor yet the [phōnē](../../strongs/g/g5456.md) of the [prophētēs](../../strongs/g/g4396.md) which are [anaginōskō](../../strongs/g/g314.md) every [sabbaton](../../strongs/g/g4521.md), they have [plēroō](../../strongs/g/g4137.md) them in [krinō](../../strongs/g/g2919.md) him.

<a name="acts_13_28"></a>Acts 13:28

And though they [heuriskō](../../strongs/g/g2147.md) [mēdeis](../../strongs/g/g3367.md) [aitia](../../strongs/g/g156.md) of [thanatos](../../strongs/g/g2288.md) in him, yet [aiteō](../../strongs/g/g154.md) they [Pilatos](../../strongs/g/g4091.md) that he should be [anaireō](../../strongs/g/g337.md).

<a name="acts_13_29"></a>Acts 13:29

And when they had [teleō](../../strongs/g/g5055.md) all that was [graphō](../../strongs/g/g1125.md) of him, they [kathaireō](../../strongs/g/g2507.md) from the [xylon](../../strongs/g/g3586.md), and [tithēmi](../../strongs/g/g5087.md) in a [mnēmeion](../../strongs/g/g3419.md).

<a name="acts_13_30"></a>Acts 13:30

But [theos](../../strongs/g/g2316.md) [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md):

<a name="acts_13_31"></a>Acts 13:31

And he was [optanomai](../../strongs/g/g3700.md) many [hēmera](../../strongs/g/g2250.md) of them which [synanabainō](../../strongs/g/g4872.md) him from [Galilaia](../../strongs/g/g1056.md) to [Ierousalēm](../../strongs/g/g2419.md), who are his [martys](../../strongs/g/g3144.md) unto the [laos](../../strongs/g/g2992.md).

<a name="acts_13_32"></a>Acts 13:32

And we [euaggelizō](../../strongs/g/g2097.md) unto you, how that the [epaggelia](../../strongs/g/g1860.md) which was [ginomai](../../strongs/g/g1096.md) unto the [patēr](../../strongs/g/g3962.md),

<a name="acts_13_33"></a>Acts 13:33

[theos](../../strongs/g/g2316.md) hath [ekplēroō](../../strongs/g/g1603.md) the same unto us their [teknon](../../strongs/g/g5043.md), in that he hath [anistēmi](../../strongs/g/g450.md) [Iēsous](../../strongs/g/g2424.md) again; as it is also [graphō](../../strongs/g/g1125.md) in the second [psalmos](../../strongs/g/g5568.md), Thou art my [huios](../../strongs/g/g5207.md), [sēmeron](../../strongs/g/g4594.md) have I [gennaō](../../strongs/g/g1080.md) thee.

<a name="acts_13_34"></a>Acts 13:34

And as concerning that he [anistēmi](../../strongs/g/g450.md) him up from the [nekros](../../strongs/g/g3498.md), now [mēketi](../../strongs/g/g3371.md) to [hypostrephō](../../strongs/g/g5290.md) to [diaphthora](../../strongs/g/g1312.md), he [eipon](../../strongs/g/g2046.md) on this wise, I will [didōmi](../../strongs/g/g1325.md) you the [pistos](../../strongs/g/g4103.md) [hosios](../../strongs/g/g3741.md) of [Dabid](../../strongs/g/g1138.md).

<a name="acts_13_35"></a>Acts 13:35

Wherefore he [legō](../../strongs/g/g3004.md) also in another, Thou shalt not [didōmi](../../strongs/g/g1325.md) thine [hosios](../../strongs/g/g3741.md) to [eidō](../../strongs/g/g1492.md) [diaphthora](../../strongs/g/g1312.md).

<a name="acts_13_36"></a>Acts 13:36

For [Dabid](../../strongs/g/g1138.md), after he had [hypēreteō](../../strongs/g/g5256.md) his own [genea](../../strongs/g/g1074.md) by the [boulē](../../strongs/g/g1012.md) of [theos](../../strongs/g/g2316.md), [koimaō](../../strongs/g/g2837.md), and was [prostithēmi](../../strongs/g/g4369.md) unto his [patēr](../../strongs/g/g3962.md), and [eidō](../../strongs/g/g1492.md) [diaphthora](../../strongs/g/g1312.md):

<a name="acts_13_37"></a>Acts 13:37

But he, whom [theos](../../strongs/g/g2316.md) [egeirō](../../strongs/g/g1453.md), [eidō](../../strongs/g/g1492.md) no [diaphthora](../../strongs/g/g1312.md).

<a name="acts_13_38"></a>Acts 13:38

Be it [gnōstos](../../strongs/g/g1110.md) unto you therefore, [anēr](../../strongs/g/g435.md) [adelphos](../../strongs/g/g80.md), that through this man is [kataggellō](../../strongs/g/g2605.md) unto you the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md):

<a name="acts_13_39"></a>Acts 13:39

And by him all that [pisteuō](../../strongs/g/g4100.md) are [dikaioō](../../strongs/g/g1344.md) from all things, from which ye could not be [dikaioō](../../strongs/g/g1344.md) by the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md).

<a name="acts_13_40"></a>Acts 13:40

[blepō](../../strongs/g/g991.md) therefore, lest that [eperchomai](../../strongs/g/g1904.md) upon you, which is [eipon](../../strongs/g/g2046.md) of in the [prophētēs](../../strongs/g/g4396.md);

<a name="acts_13_41"></a>Acts 13:41

[eidō](../../strongs/g/g1492.md), [kataphronētēs](../../strongs/g/g2707.md), and [thaumazō](../../strongs/g/g2296.md), and [aphanizō](../../strongs/g/g853.md): for I [ergazomai](../../strongs/g/g2038.md) [ergon](../../strongs/g/g2041.md) in your [hēmera](../../strongs/g/g2250.md), an [ergon](../../strongs/g/g2041.md) which ye shall in no wise [pisteuō](../../strongs/g/g4100.md), though a man [ekdiēgeomai](../../strongs/g/g1555.md) unto you.

<a name="acts_13_42"></a>Acts 13:42

And when the [Ioudaios](../../strongs/g/g2453.md) were [exeimi](../../strongs/g/g1826.md) out of the [synagōgē](../../strongs/g/g4864.md), the [ethnos](../../strongs/g/g1484.md) [parakaleō](../../strongs/g/g3870.md) that these [rhēma](../../strongs/g/g4487.md) might be [laleō](../../strongs/g/g2980.md) to them the next [sabbaton](../../strongs/g/g4521.md). [^1]

<a name="acts_13_43"></a>Acts 13:43

Now when the [synagōgē](../../strongs/g/g4864.md) was [lyō](../../strongs/g/g3089.md), [polys](../../strongs/g/g4183.md) of the [Ioudaios](../../strongs/g/g2453.md) and [sebō](../../strongs/g/g4576.md) [prosēlytos](../../strongs/g/g4339.md) [akoloutheō](../../strongs/g/g190.md) [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md): who, [proslaleō](../../strongs/g/g4354.md) to them, [peithō](../../strongs/g/g3982.md) them to [epimenō](../../strongs/g/g1961.md) in the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_13_44"></a>Acts 13:44

And the [erchomai](../../strongs/g/g2064.md) [sabbaton](../../strongs/g/g4521.md) [synagō](../../strongs/g/g4863.md) [schedon](../../strongs/g/g4975.md) the [pas](../../strongs/g/g3956.md) [polis](../../strongs/g/g4172.md) together to [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_13_45"></a>Acts 13:45

But when the [Ioudaios](../../strongs/g/g2453.md) [eidō](../../strongs/g/g1492.md) the [ochlos](../../strongs/g/g3793.md), they were [pimplēmi](../../strongs/g/g4130.md) with [zēlos](../../strongs/g/g2205.md), and [antilegō](../../strongs/g/g483.md) those things which were [legō](../../strongs/g/g3004.md) by [Paulos](../../strongs/g/g3972.md), [antilegō](../../strongs/g/g483.md) and [blasphēmeō](../../strongs/g/g987.md).

<a name="acts_13_46"></a>Acts 13:46

Then [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md) [parrēsiazomai](../../strongs/g/g3955.md), and [eipon](../../strongs/g/g2036.md), It was [anagkaios](../../strongs/g/g316.md) that the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) should first have been [laleō](../../strongs/g/g2980.md) to you: but [epeidē](../../strongs/g/g1894.md) ye [apōtheō](../../strongs/g/g683.md) it from you, and [krinō](../../strongs/g/g2919.md) yourselves [ou](../../strongs/g/g3756.md) [axios](../../strongs/g/g514.md) of [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md), [idou](../../strongs/g/g2400.md), we [strephō](../../strongs/g/g4762.md) to the [ethnos](../../strongs/g/g1484.md).

<a name="acts_13_47"></a>Acts 13:47

For so hath the [kyrios](../../strongs/g/g2962.md) [entellō](../../strongs/g/g1781.md) us, I have [tithēmi](../../strongs/g/g5087.md) thee to be a [phōs](../../strongs/g/g5457.md) of the [ethnos](../../strongs/g/g1484.md), that thou shouldest be for [sōtēria](../../strongs/g/g4991.md) unto the [eschatos](../../strongs/g/g2078.md) of [gē](../../strongs/g/g1093.md).

<a name="acts_13_48"></a>Acts 13:48

And when the [ethnos](../../strongs/g/g1484.md) [akouō](../../strongs/g/g191.md) this, they were [chairō](../../strongs/g/g5463.md), and [doxazō](../../strongs/g/g1392.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md): and as many as were [tassō](../../strongs/g/g5021.md) to [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md) [pisteuō](../../strongs/g/g4100.md).

<a name="acts_13_49"></a>Acts 13:49

And the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md) was [diapherō](../../strongs/g/g1308.md) throughout all the [chōra](../../strongs/g/g5561.md).

<a name="acts_13_50"></a>Acts 13:50

But the [Ioudaios](../../strongs/g/g2453.md) [parotrynō](../../strongs/g/g3951.md) the [sebō](../../strongs/g/g4576.md) and [euschēmōn](../../strongs/g/g2158.md) [gynē](../../strongs/g/g1135.md), and the [prōtos](../../strongs/g/g4413.md) of the [polis](../../strongs/g/g4172.md), and [epegeirō](../../strongs/g/g1892.md) [diōgmos](../../strongs/g/g1375.md) against [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md), and [ekballō](../../strongs/g/g1544.md) them out of their [horion](../../strongs/g/g3725.md).

<a name="acts_13_51"></a>Acts 13:51

But they [ektinassō](../../strongs/g/g1621.md) the [koniortos](../../strongs/g/g2868.md) of their [pous](../../strongs/g/g4228.md) against them, and [erchomai](../../strongs/g/g2064.md) unto [Ikonion](../../strongs/g/g2430.md).

<a name="acts_13_52"></a>Acts 13:52

And the [mathētēs](../../strongs/g/g3101.md) were [plēroō](../../strongs/g/g4137.md) [chara](../../strongs/g/g5479.md), and with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 12](acts_12.md) - [Acts 14](acts_14.md)

---

[^1]: [Acts 13:42 Commentary](../../commentary/acts/acts_13_commentary.md#acts_13_42)
