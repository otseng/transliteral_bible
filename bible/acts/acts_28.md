# [Acts 28](https://www.blueletterbible.org/kjv/act/28/1/s_1046001)

<a name="acts_28_1"></a>Acts 28:1

And when they were [diasōzō](../../strongs/g/g1295.md), then they [epiginōskō](../../strongs/g/g1921.md) that the [nēsos](../../strongs/g/g3520.md) was [kaleō](../../strongs/g/g2564.md) [Melitē](../../strongs/g/g3194.md).

<a name="acts_28_2"></a>Acts 28:2

And the [barbaros](../../strongs/g/g915.md) [parechō](../../strongs/g/g3930.md) us no [tygchanō](../../strongs/g/g5177.md) [philanthrōpia](../../strongs/g/g5363.md): for they [anaptō](../../strongs/g/g381.md) a [pyra](../../strongs/g/g4443.md), and [proslambanō](../../strongs/g/g4355.md) us every one, because of the [ephistēmi](../../strongs/g/g2186.md) [hyetos](../../strongs/g/g5205.md), and because of the [psychos](../../strongs/g/g5592.md).

<a name="acts_28_3"></a>Acts 28:3

And when [Paulos](../../strongs/g/g3972.md) had [systrephō](../../strongs/g/g4962.md) a [plēthos](../../strongs/g/g4128.md) of [phryganon](../../strongs/g/g5434.md), and [epitithēmi](../../strongs/g/g2007.md) them on the [pyra](../../strongs/g/g4443.md), there [exerchomai](../../strongs/g/g1831.md) an [echidna](../../strongs/g/g2191.md) out of the [thermē](../../strongs/g/g2329.md), and [kathaptō](../../strongs/g/g2510.md) his [cheir](../../strongs/g/g5495.md).

<a name="acts_28_4"></a>Acts 28:4

And when the [barbaros](../../strongs/g/g915.md) [eidō](../../strongs/g/g1492.md) the [thērion](../../strongs/g/g2342.md) [kremannymi](../../strongs/g/g2910.md) on his [cheir](../../strongs/g/g5495.md), they [legō](../../strongs/g/g3004.md) among [allēlōn](../../strongs/g/g240.md), [pantōs](../../strongs/g/g3843.md) this [anthrōpos](../../strongs/g/g444.md) is a [phoneus](../../strongs/g/g5406.md), whom, though he hath [diasōzō](../../strongs/g/g1295.md) the [thalassa](../../strongs/g/g2281.md), yet [dikē](../../strongs/g/g1349.md) [eaō](../../strongs/g/g1439.md) not to [zaō](../../strongs/g/g2198.md).

<a name="acts_28_5"></a>Acts 28:5

And he [apotinassō](../../strongs/g/g660.md) the [thērion](../../strongs/g/g2342.md) into the [pyr](../../strongs/g/g4442.md), and [paschō](../../strongs/g/g3958.md) no [kakos](../../strongs/g/g2556.md).

<a name="acts_28_6"></a>Acts 28:6

Howbeit they [prosdokaō](../../strongs/g/g4328.md) when he should have [pimprēmi](../../strongs/g/g4092.md), or [katapiptō](../../strongs/g/g2667.md) [nekros](../../strongs/g/g3498.md) [aphnō](../../strongs/g/g869.md): but after they had [prosdokaō](../../strongs/g/g4328.md) a [polys](../../strongs/g/g4183.md), and [theōreō](../../strongs/g/g2334.md) [mēdeis](../../strongs/g/g3367.md) [atopos](../../strongs/g/g824.md) [ginomai](../../strongs/g/g1096.md) to him, they [metaballō](../../strongs/g/g3328.md), and [legō](../../strongs/g/g3004.md) that he was a [theos](../../strongs/g/g2316.md).

<a name="acts_28_7"></a>Acts 28:7

In the same [topos](../../strongs/g/g5117.md) were [chōrion](../../strongs/g/g5564.md) of the [prōtos](../../strongs/g/g4413.md) of the [nēsos](../../strongs/g/g3520.md), whose [onoma](../../strongs/g/g3686.md) was [Poplios](../../strongs/g/g4196.md); who [anadechomai](../../strongs/g/g324.md) us, and [xenizō](../../strongs/g/g3579.md) us three [hēmera](../../strongs/g/g2250.md) [philophronōs](../../strong4446gs/g/g5390.md).

<a name="acts_28_8"></a>Acts 28:8

And [ginomai](../../strongs/g/g1096.md), that the [patēr](../../strongs/g/g3962.md) of [Poplios](../../strongs/g/g4196.md) [katakeimai](../../strongs/g/g2621.md) [synechō](../../strongs/g/g4912.md) of a [pyretos](../../strongs/g/g4446.md) and [dysenteria](../../strongs/g/g1420.md): to whom [Paulos](../../strongs/g/g3972.md) [eiserchomai](../../strongs/g/g1525.md), and [proseuchomai](../../strongs/g/g4336.md), and [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) on him, and [iaomai](../../strongs/g/g2390.md) him.

<a name="acts_28_9"></a>Acts 28:9

So when this was [ginomai](../../strongs/g/g1096.md), [loipos](../../strongs/g/g3062.md) also, which had [astheneia](../../strongs/g/g769.md) in the [nēsos](../../strongs/g/g3520.md), [proserchomai](../../strongs/g/g4334.md), and were [therapeuō](../../strongs/g/g2323.md):

<a name="acts_28_10"></a>Acts 28:10

Who also [timē](../../strongs/g/g5092.md) us with [polys](../../strongs/g/g4183.md) [timaō](../../strongs/g/g5091.md); and when we [anagō](../../strongs/g/g321.md), they [epitithēmi](../../strongs/g/g2007.md) us with such things as were [chreia](../../strongs/g/g5532.md).

<a name="acts_28_11"></a>Acts 28:11

And after three [mēn](../../strongs/g/g3376.md) we [anagō](../../strongs/g/g321.md) in a [ploion](../../strongs/g/g4143.md) of [Alexandrinos](../../strongs/g/g222.md), which had [paracheimazō](../../strongs/g/g3914.md) in the [nēsos](../../strongs/g/g3520.md), whose [parasēmos](../../strongs/g/g3902.md) was [Dioskouroi](../../strongs/g/g1359.md).

<a name="acts_28_12"></a>Acts 28:12

And [katagō](../../strongs/g/g2609.md) at [Syrakousai](../../strongs/g/g4946.md), we [epimenō](../../strongs/g/g1961.md) there three [hēmera](../../strongs/g/g2250.md).

<a name="acts_28_13"></a>Acts 28:13

And from thence we [perierchomai](../../strongs/g/g4022.md), and [katantaō](../../strongs/g/g2658.md) to [Rhēgion](../../strongs/g/g4484.md): and after one day the [notos](../../strongs/g/g3558.md) [epiginomai](../../strongs/g/g1920.md), and we [erchomai](../../strongs/g/g2064.md) the [deuteraios](../../strongs/g/g1206.md) to [Potioloi](../../strongs/g/g4223.md):

<a name="acts_28_14"></a>Acts 28:14

Where we [heuriskō](../../strongs/g/g2147.md) [adelphos](../../strongs/g/g80.md), and were [parakaleō](../../strongs/g/g3870.md) to [epimenō](../../strongs/g/g1961.md) with them seven [hēmera](../../strongs/g/g2250.md): and so we [erchomai](../../strongs/g/g2064.md) toward [Rhōmē](../../strongs/g/g4516.md).

<a name="acts_28_15"></a>Acts 28:15

And from thence, when the [adelphos](../../strongs/g/g80.md) [akouō](../../strongs/g/g191.md) of us, they [exerchomai](../../strongs/g/g1831.md) to [apantēsis](../../strongs/g/g529.md) us as far as [Appios](../../strongs/g/g675.md) [phoron](../../strongs/g/g5410.md), and The three [tabernai](../../strongs/g/g4999.md): whom when [Paulos](../../strongs/g/g3972.md) [eidō](../../strongs/g/g1492.md), he [eucharisteō](../../strongs/g/g2168.md) [theos](../../strongs/g/g2316.md), and [lambanō](../../strongs/g/g2983.md) [tharsos](../../strongs/g/g2294.md).

<a name="acts_28_16"></a>Acts 28:16

And when we [erchomai](../../strongs/g/g2064.md) to [Rhōmē](../../strongs/g/g4516.md), the [hekatontarchēs](../../strongs/g/g1543.md) [paradidōmi](../../strongs/g/g3860.md) the [desmios](../../strongs/g/g1198.md) to the [stratopedarchēs](../../strongs/g/g4759.md): but [Paulos](../../strongs/g/g3972.md) was [epitrepō](../../strongs/g/g2010.md) to [menō](../../strongs/g/g3306.md) by himself with a [stratiōtēs](../../strongs/g/g4757.md) that [phylassō](../../strongs/g/g5442.md) him.

<a name="acts_28_17"></a>Acts 28:17

And [ginomai](../../strongs/g/g1096.md), that after three [hēmera](../../strongs/g/g2250.md) [Paulos](../../strongs/g/g3972.md) [sygkaleō](../../strongs/g/g4779.md) the [prōtos](../../strongs/g/g4413.md) of the [Ioudaios](../../strongs/g/g2453.md): and when they were [synerchomai](../../strongs/g/g4905.md), he [legō](../../strongs/g/g3004.md) unto them, [anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), though I have [poieō](../../strongs/g/g4160.md) nothing [enantios](../../strongs/g/g1727.md) the [laos](../../strongs/g/g2992.md), or [ethos](../../strongs/g/g1485.md) of our [patrōos](../../strongs/g/g3971.md), yet was I [paradidōmi](../../strongs/g/g3860.md) [desmios](../../strongs/g/g1198.md) from [Hierosolyma](../../strongs/g/g2414.md) into the [cheir](../../strongs/g/g5495.md) of the [Rhōmaios](../../strongs/g/g4514.md).

<a name="acts_28_18"></a>Acts 28:18

Who, when they had [anakrinō](../../strongs/g/g350.md) me, [boulomai](../../strongs/g/g1014.md) have [apolyō](../../strongs/g/g630.md) me, because there was [mēdeis](../../strongs/g/g3367.md) [aitia](../../strongs/g/g156.md) of [thanatos](../../strongs/g/g2288.md) in me.

<a name="acts_28_19"></a>Acts 28:19

But when the [Ioudaios](../../strongs/g/g2453.md) [antilegō](../../strongs/g/g483.md), I was [anagkazō](../../strongs/g/g315.md) to [epikaleō](../../strongs/g/g1941.md) unto [Kaisar](../../strongs/g/g2541.md); not that I had ought to [katēgoreō](../../strongs/g/g2723.md) my [ethnos](../../strongs/g/g1484.md) of.

<a name="acts_28_20"></a>Acts 28:20

For this [aitia](../../strongs/g/g156.md) therefore have I [parakaleō](../../strongs/g/g3870.md) for you, to [eidō](../../strongs/g/g1492.md), and to [proslaleō](../../strongs/g/g4354.md): because that for the [elpis](../../strongs/g/g1680.md) of [Israēl](../../strongs/g/g2474.md) I am [perikeimai](../../strongs/g/g4029.md) with this [halysis](../../strongs/g/g254.md).

<a name="acts_28_21"></a>Acts 28:21

And they [eipon](../../strongs/g/g2036.md) unto him, We neither [dechomai](../../strongs/g/g1209.md) [gramma](../../strongs/g/g1121.md) out of [Ioudaia](../../strongs/g/g2449.md) concerning thee, neither any of the [adelphos](../../strongs/g/g80.md) that [paraginomai](../../strongs/g/g3854.md) [apaggellō](../../strongs/g/g518.md) or [laleō](../../strongs/g/g2980.md) any [ponēros](../../strongs/g/g4190.md) of thee.

<a name="acts_28_22"></a>Acts 28:22

But we [axioō](../../strongs/g/g515.md) to [akouō](../../strongs/g/g191.md) of thee what thou [phroneō](../../strongs/g/g5426.md): for as concerning this [hairesis](../../strongs/g/g139.md), we [gnōstos](../../strongs/g/g1110.md) that [pantachou](../../strongs/g/g3837.md) it is [antilegō](../../strongs/g/g483.md).

<a name="acts_28_23"></a>Acts 28:23

And when they had [tassō](../../strongs/g/g5021.md) him a [hēmera](../../strongs/g/g2250.md), there [hēkō](../../strongs/g/g2240.md) many to him into [xenia](../../strongs/g/g3578.md); to whom he [ektithēmi](../../strongs/g/g1620.md) and [diamartyromai](../../strongs/g/g1263.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), [peithō](../../strongs/g/g3982.md) them concerning [Iēsous](../../strongs/g/g2424.md), both out of the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md), and out of the [prophētēs](../../strongs/g/g4396.md), from [prōï](../../strongs/g/g4404.md) till [hespera](../../strongs/g/g2073.md).

<a name="acts_28_24"></a>Acts 28:24

And some [peithō](../../strongs/g/g3982.md) the things which were [legō](../../strongs/g/g3004.md), and some [apisteō](../../strongs/g/g569.md).

<a name="acts_28_25"></a>Acts 28:25

And when they [asymphonos](../../strongs/g/g800.md) among [allēlōn](../../strongs/g/g240.md), they [apolyō](../../strongs/g/g630.md), after that [Paulos](../../strongs/g/g3972.md) had [eipon](../../strongs/g/g2036.md) [heis](../../strongs/g/g1520.md) [rhēma](../../strongs/g/g4487.md), [kalōs](../../strongs/g/g2573.md) [laleō](../../strongs/g/g2980.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) by [Ēsaïas](../../strongs/g/g2268.md) the [prophētēs](../../strongs/g/g4396.md) unto our [patēr](../../strongs/g/g3962.md),

<a name="acts_28_26"></a>Acts 28:26

[legō](../../strongs/g/g3004.md), [poreuō](../../strongs/g/g4198.md) unto this [laos](../../strongs/g/g2992.md), and [eipon](../../strongs/g/g2036.md), [akoē](../../strongs/g/g189.md) ye shall [akouō](../../strongs/g/g191.md), and shall not [syniēmi](../../strongs/g/g4920.md); and [blepō](../../strongs/g/g991.md) ye shall [blepō](../../strongs/g/g991.md), and not [eidō](../../strongs/g/g1492.md):

<a name="acts_28_27"></a>Acts 28:27

For the [kardia](../../strongs/g/g2588.md) of this [laos](../../strongs/g/g2992.md) is [pachynō](../../strongs/g/g3975.md), and their [ous](../../strongs/g/g3775.md) are [bareōs](../../strongs/g/g917.md) of [akouō](../../strongs/g/g191.md), and their [ophthalmos](../../strongs/g/g3788.md) have they [kammyō](../../strongs/g/g2576.md); lest they should [eidō](../../strongs/g/g1492.md) with their [ophthalmos](../../strongs/g/g3788.md), and [akouō](../../strongs/g/g191.md) with their [ous](../../strongs/g/g3775.md), and [syniēmi](../../strongs/g/g4920.md) with their [kardia](../../strongs/g/g2588.md), and should be [epistrephō](../../strongs/g/g1994.md), and I should [iaomai](../../strongs/g/g2390.md) them.

<a name="acts_28_28"></a>Acts 28:28

Be it [gnōstos](../../strongs/g/g1110.md) therefore unto you, that the [sōtērios](../../strongs/g/g4992.md) of [theos](../../strongs/g/g2316.md) is [apostellō](../../strongs/g/g649.md) unto the [ethnos](../../strongs/g/g1484.md), and that they will [akouō](../../strongs/g/g191.md) it.

<a name="acts_28_29"></a>Acts 28:29

And when he had [eipon](../../strongs/g/g2036.md) these words, the [Ioudaios](../../strongs/g/g2453.md) [aperchomai](../../strongs/g/g565.md), and had [polys](../../strongs/g/g4183.md) [syzētēsis](../../strongs/g/g4803.md) among themselves. [^1]

<a name="acts_28_30"></a>Acts 28:30

And [Paulos](../../strongs/g/g3972.md) [menō](../../strongs/g/g3306.md) [holos](../../strongs/g/g3650.md) [dietia](../../strongs/g/g1333.md) in his own [misthoma](../../strongs/g/g3410.md), and [apodechomai](../../strongs/g/g588.md) all that [eisporeuomai](../../strongs/g/g1531.md) unto him,

<a name="acts_28_31"></a>Acts 28:31

[kēryssō](../../strongs/g/g2784.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), and [didaskō](../../strongs/g/g1321.md) those things which concern the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), with all [parrēsia](../../strongs/g/g3954.md), [akolytos](../../strongs/g/g209.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 27](acts_27.md)

---

[^1]: [Acts 28:29 Commentary](../../commentary/acts/acts_28_commentary.md#acts_28_29)
