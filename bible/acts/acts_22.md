# [Acts 22](https://www.blueletterbible.org/kjv/act/22/1/s_1040001)

<a name="acts_22_1"></a>Acts 22:1

[anēr](../../strongs/g/g435.md), [adelphos](../../strongs/g/g80.md), and [patēr](../../strongs/g/g3962.md), [akouō](../../strongs/g/g191.md) ye my [apologia](../../strongs/g/g627.md) now unto you.

<a name="acts_22_2"></a>Acts 22:2

(And when they [akouō](../../strongs/g/g191.md) that he [prosphōneō](../../strongs/g/g4377.md) in the [Hebraïs](../../strongs/g/g1446.md) [dialektos](../../strongs/g/g1258.md) to them, they [parechō](../../strongs/g/g3930.md) the more [hēsychia](../../strongs/g/g2271.md): and he [phēmi](../../strongs/g/g5346.md),)

<a name="acts_22_3"></a>Acts 22:3

I am [men](../../strongs/g/g3303.md) an [anēr](../../strongs/g/g435.md) an [Ioudaios](../../strongs/g/g2453.md), [gennaō](../../strongs/g/g1080.md) in [Tarsos](../../strongs/g/g5019.md), in [Kilikia](../../strongs/g/g2791.md), yet [anatrephō](../../strongs/g/g397.md) in this [polis](../../strongs/g/g4172.md) at the [pous](../../strongs/g/g4228.md) of [Gamaliēl](../../strongs/g/g1059.md), and [paideuō](../../strongs/g/g3811.md) according to the [akribeia](../../strongs/g/g195.md) of the [nomos](../../strongs/g/g3551.md) of the [patrōos](../../strongs/g/g3971.md), and was [zēlōtēs](../../strongs/g/g2207.md) toward [theos](../../strongs/g/g2316.md), as ye all are [sēmeron](../../strongs/g/g4594.md).

<a name="acts_22_4"></a>Acts 22:4

And I [diōkō](../../strongs/g/g1377.md) this [hodos](../../strongs/g/g3598.md) unto the [thanatos](../../strongs/g/g2288.md), [desmeuō](../../strongs/g/g1195.md) and [paradidōmi](../../strongs/g/g3860.md) into [phylakē](../../strongs/g/g5438.md) both [anēr](../../strongs/g/g435.md) and [gynē](../../strongs/g/g1135.md).

<a name="acts_22_5"></a>Acts 22:5

As also the [archiereus](../../strongs/g/g749.md) doth [martyreō](../../strongs/g/g3140.md) me, and all the [presbyterion](../../strongs/g/g4244.md): from whom also I [dechomai](../../strongs/g/g1209.md) [epistolē](../../strongs/g/g1992.md) unto the [adelphos](../../strongs/g/g80.md), and [poreuō](../../strongs/g/g4198.md) to [Damaskos](../../strongs/g/g1154.md), to [agō](../../strongs/g/g71.md) them which were there [deō](../../strongs/g/g1210.md) unto [Ierousalēm](../../strongs/g/g2419.md), for to be [timōreō](../../strongs/g/g5097.md).

<a name="acts_22_6"></a>Acts 22:6

And [ginomai](../../strongs/g/g1096.md), that, as I made my [poreuō](../../strongs/g/g4198.md), and was [eggizō](../../strongs/g/g1448.md) unto [Damaskos](../../strongs/g/g1154.md) about [mesēmbria](../../strongs/g/g3314.md), [exaiphnēs](../../strongs/g/g1810.md) there [periastraptō](../../strongs/g/g4015.md) from [ouranos](../../strongs/g/g3772.md) a [hikanos](../../strongs/g/g2425.md) [phōs](../../strongs/g/g5457.md) about me.

<a name="acts_22_7"></a>Acts 22:7

And I [piptō](../../strongs/g/g4098.md) unto the [edaphos](../../strongs/g/g1475.md), and [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) [legō](../../strongs/g/g3004.md) unto me, [Saoul](../../strongs/g/g4549.md), [Saoul](../../strongs/g/g4549.md), why [diōkō](../../strongs/g/g1377.md) thou me?

<a name="acts_22_8"></a>Acts 22:8

And I [apokrinomai](../../strongs/g/g611.md), Who art thou, [kyrios](../../strongs/g/g2962.md)? And he [eipon](../../strongs/g/g2036.md) unto me, I am [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md), whom thou [diōkō](../../strongs/g/g1377.md).

<a name="acts_22_9"></a>Acts 22:9

And they that were with me [theaomai](../../strongs/g/g2300.md) indeed the [phōs](../../strongs/g/g5457.md), and were [emphobos](../../strongs/g/g1719.md); but they [akouō](../../strongs/g/g191.md) not the [phōnē](../../strongs/g/g5456.md) of him that [laleō](../../strongs/g/g2980.md) to me.

<a name="acts_22_10"></a>Acts 22:10

And I [eipon](../../strongs/g/g2036.md), What shall I [poieō](../../strongs/g/g4160.md), [kyrios](../../strongs/g/g2962.md)? And the [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto me, [anistēmi](../../strongs/g/g450.md), and [poreuō](../../strongs/g/g4198.md) into [Damaskos](../../strongs/g/g1154.md); and there it shall be [laleō](../../strongs/g/g2980.md) thee of all things which are [tassō](../../strongs/g/g5021.md) for thee to [poieō](../../strongs/g/g4160.md).

<a name="acts_22_11"></a>Acts 22:11

And when I could not [emblepō](../../strongs/g/g1689.md) for the [doxa](../../strongs/g/g1391.md) of that [phōs](../../strongs/g/g5457.md), being [cheiragōgeō](../../strongs/g/g5496.md) of them that were with me, I [erchomai](../../strongs/g/g2064.md) into [Damaskos](../../strongs/g/g1154.md).

<a name="acts_22_12"></a>Acts 22:12

And one [Hananias](../../strongs/g/g367.md), an [eusebēs](../../strongs/g/g2152.md) [anēr](../../strongs/g/g435.md) according to the [nomos](../../strongs/g/g3551.md), having a [martyreō](../../strongs/g/g3140.md) of all the [Ioudaios](../../strongs/g/g2453.md) which [katoikeō](../../strongs/g/g2730.md) there,

<a name="acts_22_13"></a>Acts 22:13

[erchomai](../../strongs/g/g2064.md) unto me, and [ephistēmi](../../strongs/g/g2186.md), and [eipon](../../strongs/g/g2036.md) unto me, [adelphos](../../strongs/g/g80.md) [Saoul](../../strongs/g/g4549.md), [anablepō](../../strongs/g/g308.md). And the same [hōra](../../strongs/g/g5610.md) I [anablepō](../../strongs/g/g308.md) upon him.

<a name="acts_22_14"></a>Acts 22:14

And he [eipon](../../strongs/g/g2036.md), The [theos](../../strongs/g/g2316.md) of our [patēr](../../strongs/g/g3962.md) hath [procheirizō](../../strongs/g/g4400.md) thee, that thou shouldest [ginōskō](../../strongs/g/g1097.md) his [thelēma](../../strongs/g/g2307.md), and [eidō](../../strongs/g/g1492.md) [dikaios](../../strongs/g/g1342.md), and shouldest [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of his [stoma](../../strongs/g/g4750.md).

<a name="acts_22_15"></a>Acts 22:15

For thou shalt be his [martys](../../strongs/g/g3144.md) unto all [anthrōpos](../../strongs/g/g444.md) of what thou hast [horaō](../../strongs/g/g3708.md) and [akouō](../../strongs/g/g191.md).

<a name="acts_22_16"></a>Acts 22:16

And now why [mellō](../../strongs/g/g3195.md)? [anistēmi](../../strongs/g/g450.md), and be [baptizō](../../strongs/g/g907.md), and [apolouō](../../strongs/g/g628.md) thy [hamartia](../../strongs/g/g266.md), [epikaleō](../../strongs/g/g1941.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="acts_22_17"></a>Acts 22:17

And [ginomai](../../strongs/g/g1096.md), that, when I was [hypostrephō](../../strongs/g/g5290.md) to [Ierousalēm](../../strongs/g/g2419.md), even while I [proseuchomai](../../strongs/g/g4336.md) in the [hieron](../../strongs/g/g2411.md), I was in an [ekstasis](../../strongs/g/g1611.md);

<a name="acts_22_18"></a>Acts 22:18

And [eidō](../../strongs/g/g1492.md) him [legō](../../strongs/g/g3004.md) unto me, [speudō](../../strongs/g/g4692.md), and [exerchomai](../../strongs/g/g1831.md) thee [tachos](../../strongs/g/g5034.md) out of [Ierousalēm](../../strongs/g/g2419.md): for they will not [paradechomai](../../strongs/g/g3858.md) thy [martyria](../../strongs/g/g3141.md) concerning me.

<a name="acts_22_19"></a>Acts 22:19

And I [eipon](../../strongs/g/g2036.md), [kyrios](../../strongs/g/g2962.md), they [epistamai](../../strongs/g/g1987.md) that I [ēmēn](../../strongs/g/g2252.md) [phylakizō](../../strongs/g/g5439.md) and [derō](../../strongs/g/g1194.md) in every [synagōgē](../../strongs/g/g4864.md) them that [pisteuō](../../strongs/g/g4100.md) on thee:

<a name="acts_22_20"></a>Acts 22:20

And when the [haima](../../strongs/g/g129.md) of thy [martys](../../strongs/g/g3144.md) [Stephanos](../../strongs/g/g4736.md) was [ekcheō](../../strongs/g/g1632.md), I also was [ephistēmi](../../strongs/g/g2186.md), and [syneudokeō](../../strongs/g/g4909.md) unto his [anairesis](../../strongs/g/g336.md), and [phylassō](../../strongs/g/g5442.md) the [himation](../../strongs/g/g2440.md) of them that [anaireō](../../strongs/g/g337.md) him.

<a name="acts_22_21"></a>Acts 22:21

And he [eipon](../../strongs/g/g2036.md) unto me, [poreuō](../../strongs/g/g4198.md): for I will [exapostellō](../../strongs/g/g1821.md) thee [makran](../../strongs/g/g3112.md) unto the [ethnos](../../strongs/g/g1484.md).

<a name="acts_22_22"></a>Acts 22:22

And they [akouō](../../strongs/g/g191.md) him unto this [logos](../../strongs/g/g3056.md), and then [epairō](../../strongs/g/g1869.md) their [phōnē](../../strongs/g/g5456.md), and [legō](../../strongs/g/g3004.md), [airō](../../strongs/g/g142.md) such from the [gē](../../strongs/g/g1093.md): for it is not [kathēkō](../../strongs/g/g2520.md) that he should [zaō](../../strongs/g/g2198.md).

<a name="acts_22_23"></a>Acts 22:23

And as they [kraugazō](../../strongs/g/g2905.md), and [rhipteō](../../strongs/g/g4495.md) [himation](../../strongs/g/g2440.md), and [ballō](../../strongs/g/g906.md) [koniortos](../../strongs/g/g2868.md) into the [aēr](../../strongs/g/g109.md),

<a name="acts_22_24"></a>Acts 22:24

The [chiliarchos](../../strongs/g/g5506.md) [keleuō](../../strongs/g/g2753.md) him to be [agō](../../strongs/g/g71.md) into the [parembolē](../../strongs/g/g3925.md), and [eipon](../../strongs/g/g2036.md) that he should be [anetazō](../../strongs/g/g426.md) by [mastix](../../strongs/g/g3148.md); that he might [epiginōskō](../../strongs/g/g1921.md) wherefore they [epiphōneō](../../strongs/g/g2019.md) so against him.

<a name="acts_22_25"></a>Acts 22:25

And as they [proteinō](../../strongs/g/g4385.md) him with [himas](../../strongs/g/g2438.md), [Paulos](../../strongs/g/g3972.md) [eipon](../../strongs/g/g2036.md) unto the [hekatontarchēs](../../strongs/g/g1543.md) that [histēmi](../../strongs/g/g2476.md), Is it [exesti](../../strongs/g/g1832.md) for you to [mastizō](../../strongs/g/g3147.md) an [anthrōpos](../../strongs/g/g444.md) that is a [Rhōmaios](../../strongs/g/g4514.md), and [akatakritos](../../strongs/g/g178.md)?

<a name="acts_22_26"></a>Acts 22:26

When the [hekatontarchēs](../../strongs/g/g1543.md) [akouō](../../strongs/g/g191.md), he [proserchomai](../../strongs/g/g4334.md) and [apaggellō](../../strongs/g/g518.md) the [chiliarchos](../../strongs/g/g5506.md), [legō](../../strongs/g/g3004.md), [horaō](../../strongs/g/g3708.md) what thou [poieō](../../strongs/g/g4160.md): for this [anthrōpos](../../strongs/g/g444.md) is a [Rhōmaios](../../strongs/g/g4514.md).

<a name="acts_22_27"></a>Acts 22:27

Then the [chiliarchos](../../strongs/g/g5506.md) [proserchomai](../../strongs/g/g4334.md), and [eipon](../../strongs/g/g2036.md) unto him, [legō](../../strongs/g/g3004.md) me, art thou a [Rhōmaios](../../strongs/g/g4514.md)? He [phēmi](../../strongs/g/g5346.md), [nai](../../strongs/g/g3483.md).

<a name="acts_22_28"></a>Acts 22:28

And the [chiliarchos](../../strongs/g/g5506.md) [apokrinomai](../../strongs/g/g611.md), With a [polys](../../strongs/g/g4183.md) [kephalaion](../../strongs/g/g2774.md) [ktaomai](../../strongs/g/g2932.md) I this [politeia](../../strongs/g/g4174.md). And [Paulos](../../strongs/g/g3972.md) [phēmi](../../strongs/g/g5346.md), But I was [gennaō](../../strongs/g/g1080.md).

<a name="acts_22_29"></a>Acts 22:29

Then [eutheōs](../../strongs/g/g2112.md) they [aphistēmi](../../strongs/g/g868.md) from him which should have [anetazō](../../strongs/g/g426.md) him: and the [chiliarchos](../../strongs/g/g5506.md) also was [phobeō](../../strongs/g/g5399.md), after he [epiginōskō](../../strongs/g/g1921.md) that he was a [Rhōmaios](../../strongs/g/g4514.md), and because he had [deō](../../strongs/g/g1210.md) him.

<a name="acts_22_30"></a>Acts 22:30

On the [epaurion](../../strongs/g/g1887.md), because he [boulomai](../../strongs/g/g1014.md) have [ginōskō](../../strongs/g/g1097.md) the [asphalēs](../../strongs/g/g804.md) wherefore he was [katēgoreō](../../strongs/g/g2723.md) of the [Ioudaios](../../strongs/g/g2453.md), he [lyō](../../strongs/g/g3089.md) him from his [desmos](../../strongs/g/g1199.md), and [keleuō](../../strongs/g/g2753.md) the [archiereus](../../strongs/g/g749.md) and all their [synedrion](../../strongs/g/g4892.md) to [erchomai](../../strongs/g/g2064.md), and [katagō](../../strongs/g/g2609.md) [Paulos](../../strongs/g/g3972.md), and [histēmi](../../strongs/g/g2476.md) him before them.

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 21](acts_21.md) - [Acts 23](acts_23.md)