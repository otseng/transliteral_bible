# [Acts 14](https://www.blueletterbible.org/kjv/act/14/1/s_1032001)

<a name="acts_14_1"></a>Acts 14:1

And [ginomai](../../strongs/g/g1096.md) in [Ikonion](../../strongs/g/g2430.md), that they [eiserchomai](../../strongs/g/g1525.md) both together into the [synagōgē](../../strongs/g/g4864.md) of the [Ioudaios](../../strongs/g/g2453.md), and so [laleō](../../strongs/g/g2980.md), that a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) both of the [Ioudaios](../../strongs/g/g2453.md) and also of the [Hellēn](../../strongs/g/g1672.md) [pisteuō](../../strongs/g/g4100.md).

<a name="acts_14_2"></a>Acts 14:2

But the [apeitheō](../../strongs/g/g544.md) [Ioudaios](../../strongs/g/g2453.md) [epegeirō](../../strongs/g/g1892.md) the [ethnos](../../strongs/g/g1484.md), and [kakoō](../../strongs/g/g2559.md) their [psychē](../../strongs/g/g5590.md) against the [adelphos](../../strongs/g/g80.md).

<a name="acts_14_3"></a>Acts 14:3

[hikanos](../../strongs/g/g2425.md) [chronos](../../strongs/g/g5550.md) therefore [diatribō](../../strongs/g/g1304.md) they [parrēsiazomai](../../strongs/g/g3955.md) in the [kyrios](../../strongs/g/g2962.md), which [martyreō](../../strongs/g/g3140.md) unto the [logos](../../strongs/g/g3056.md) of his [charis](../../strongs/g/g5485.md), and [didōmi](../../strongs/g/g1325.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md) to be done by their [cheir](../../strongs/g/g5495.md).

<a name="acts_14_4"></a>Acts 14:4

But the [plēthos](../../strongs/g/g4128.md) of the [polis](../../strongs/g/g4172.md) was [schizō](../../strongs/g/g4977.md): and part held with the [Ioudaios](../../strongs/g/g2453.md), and part with the [apostolos](../../strongs/g/g652.md).

<a name="acts_14_5"></a>Acts 14:5

And when there was an [hormē](../../strongs/g/g3730.md) made both of the [ethnos](../../strongs/g/g1484.md), and also of the [Ioudaios](../../strongs/g/g2453.md) with their [archōn](../../strongs/g/g758.md), to [hybrizō](../../strongs/g/g5195.md), and to [lithoboleō](../../strongs/g/g3036.md) them,

<a name="acts_14_6"></a>Acts 14:6

They were [synoraō](../../strongs/g/g4894.md), and [katapheugō](../../strongs/g/g2703.md) unto [Lystra](../../strongs/g/g3082.md) and [Derbē](../../strongs/g/g1191.md), [polis](../../strongs/g/g4172.md) of [Lykaonia](../../strongs/g/g3071.md), and unto the [perichōros](../../strongs/g/g4066.md):

<a name="acts_14_7"></a>Acts 14:7

And there they [euaggelizō](../../strongs/g/g2097.md).

<a name="acts_14_8"></a>Acts 14:8

And there [kathēmai](../../strongs/g/g2521.md) a certain [anēr](../../strongs/g/g435.md) at [Lystra](../../strongs/g/g3082.md), [adynatos](../../strongs/g/g102.md) in his [pous](../../strongs/g/g4228.md), being a [chōlos](../../strongs/g/g5560.md) from his [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md), who never had [peripateō](../../strongs/g/g4043.md):

<a name="acts_14_9"></a>Acts 14:9

The same [akouō](../../strongs/g/g191.md) [Paulos](../../strongs/g/g3972.md) [laleō](../../strongs/g/g2980.md): who [atenizō](../../strongs/g/g816.md) him, and [eidō](../../strongs/g/g1492.md) that he had [pistis](../../strongs/g/g4102.md) to be [sōzō](../../strongs/g/g4982.md),

<a name="acts_14_10"></a>Acts 14:10

[eipon](../../strongs/g/g2036.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [anistēmi](../../strongs/g/g450.md) [orthos](../../strongs/g/g3717.md) on thy [pous](../../strongs/g/g4228.md). And he [hallomai](../../strongs/g/g242.md) and [peripateō](../../strongs/g/g4043.md).

<a name="acts_14_11"></a>Acts 14:11

And when the [ochlos](../../strongs/g/g3793.md) [eidō](../../strongs/g/g1492.md) what [Paulos](../../strongs/g/g3972.md) had [poieō](../../strongs/g/g4160.md), they [epairō](../../strongs/g/g1869.md) their [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md) in [Lykaonisti](../../strongs/g/g3072.md), The [theos](../../strongs/g/g2316.md) are [katabainō](../../strongs/g/g2597.md) to us in the [homoioō](../../strongs/g/g3666.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="acts_14_12"></a>Acts 14:12

And they [kaleō](../../strongs/g/g2564.md) [Barnabas](../../strongs/g/g921.md), [Zeus](../../strongs/g/g2203.md); and [Paulos](../../strongs/g/g3972.md), [Hermēs](../../strongs/g/g2060.md), because he was the [hēgeomai](../../strongs/g/g2233.md) [logos](../../strongs/g/g3056.md).

<a name="acts_14_13"></a>Acts 14:13

Then the [hiereus](../../strongs/g/g2409.md) of [Zeus](../../strongs/g/g2203.md), which was before their [polis](../../strongs/g/g4172.md), [pherō](../../strongs/g/g5342.md) [tauros](../../strongs/g/g5022.md) and [stemma](../../strongs/g/g4725.md) unto the [pylōn](../../strongs/g/g4440.md), and would have done [thyō](../../strongs/g/g2380.md) with the [ochlos](../../strongs/g/g3793.md).

<a name="acts_14_14"></a>Acts 14:14

when the [apostolos](../../strongs/g/g652.md), [Barnabas](../../strongs/g/g921.md) and [Paulos](../../strongs/g/g3972.md), [akouō](../../strongs/g/g191.md), they [diarrēssō](../../strongs/g/g1284.md) their [himation](../../strongs/g/g2440.md), and [eispēdaō](../../strongs/g/g1530.md) among the [ochlos](../../strongs/g/g3793.md), [krazō](../../strongs/g/g2896.md),

<a name="acts_14_15"></a>Acts 14:15

And [legō](../../strongs/g/g3004.md), [anēr](../../strongs/g/g435.md), why [poieō](../../strongs/g/g4160.md) ye these things? We also are [anthrōpos](../../strongs/g/g444.md) [homoiopathēs](../../strongs/g/g3663.md) with you, and [euaggelizō](../../strongs/g/g2097.md) unto you that ye should [epistrephō](../../strongs/g/g1994.md) from these [mataios](../../strongs/g/g3152.md) unto the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md), which [poieō](../../strongs/g/g4160.md) [ouranos](../../strongs/g/g3772.md), and [gē](../../strongs/g/g1093.md), and the [thalassa](../../strongs/g/g2281.md), and all things that are therein:

<a name="acts_14_16"></a>Acts 14:16

Who in [genea](../../strongs/g/g1074.md) [paroichomai](../../strongs/g/g3944.md) [eaō](../../strongs/g/g1439.md) all [ethnos](../../strongs/g/g1484.md) to [poreuō](../../strongs/g/g4198.md) in their own [hodos](../../strongs/g/g3598.md).

<a name="acts_14_17"></a>Acts 14:17

[kaitoige](../../strongs/g/g2544.md) he [aphiēmi](../../strongs/g/g863.md) not himself [amartyros](../../strongs/g/g267.md), in that he [agathopoieō](../../strongs/g/g15.md), and [didōmi](../../strongs/g/g1325.md) us [hyetos](../../strongs/g/g5205.md) [ouranothen](../../strongs/g/g3771.md), and [karpophoros](../../strongs/g/g2593.md) [kairos](../../strongs/g/g2540.md), [empi(m)plēmi](../../strongs/g/g1705.md) our [kardia](../../strongs/g/g2588.md) with [trophē](../../strongs/g/g5160.md) and [euphrosynē](../../strongs/g/g2167.md).

<a name="acts_14_18"></a>Acts 14:18

And with these [legō](../../strongs/g/g3004.md) [molis](../../strongs/g/g3433.md) [katapauō](../../strongs/g/g2664.md) the [ochlos](../../strongs/g/g3793.md), that they had not done [thyō](../../strongs/g/g2380.md) unto them.

<a name="acts_14_19"></a>Acts 14:19

And there [eperchomai](../../strongs/g/g1904.md) [Ioudaios](../../strongs/g/g2453.md) from [Antiocheia](../../strongs/g/g490.md) and [Ikonion](../../strongs/g/g2430.md), who [peithō](../../strongs/g/g3982.md) the [ochlos](../../strongs/g/g3793.md), and having [lithazō](../../strongs/g/g3034.md) [Paulos](../../strongs/g/g3972.md), [syrō](../../strongs/g/g4951.md) out of the [polis](../../strongs/g/g4172.md), [nomizō](../../strongs/g/g3543.md) he had been [thnēskō](../../strongs/g/g2348.md).

<a name="acts_14_20"></a>Acts 14:20

Howbeit, as the [mathētēs](../../strongs/g/g3101.md) [kykloō](../../strongs/g/g2944.md) him, he [anistēmi](../../strongs/g/g450.md), and [eiserchomai](../../strongs/g/g1525.md) into the [polis](../../strongs/g/g4172.md): and the [epaurion](../../strongs/g/g1887.md) he [exerchomai](../../strongs/g/g1831.md) with [Barnabas](../../strongs/g/g921.md) to [Derbē](../../strongs/g/g1191.md).

<a name="acts_14_21"></a>Acts 14:21

And when they had [euaggelizō](../../strongs/g/g2097.md) to that [polis](../../strongs/g/g4172.md), and had [mathēteuō](../../strongs/g/g3100.md) [hikanos](../../strongs/g/g2425.md), they [hypostrephō](../../strongs/g/g5290.md) to [Lystra](../../strongs/g/g3082.md), and to [Ikonion](../../strongs/g/g2430.md), and [Antiocheia](../../strongs/g/g490.md),

<a name="acts_14_22"></a>Acts 14:22

[epistērizō](../../strongs/g/g1991.md) the [psychē](../../strongs/g/g5590.md) of the [mathētēs](../../strongs/g/g3101.md), and [parakaleō](../../strongs/g/g3870.md) them to [emmenō](../../strongs/g/g1696.md) in the [pistis](../../strongs/g/g4102.md), and that we [dei](../../strongs/g/g1163.md) through [polys](../../strongs/g/g4183.md) [thlipsis](../../strongs/g/g2347.md) [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_14_23"></a>Acts 14:23

And when they had [cheirotoneō](../../strongs/g/g5500.md) them [presbyteros](../../strongs/g/g4245.md) in every [ekklēsia](../../strongs/g/g1577.md), and had [proseuchomai](../../strongs/g/g4336.md) with [nēsteia](../../strongs/g/g3521.md), they [paratithēmi](../../strongs/g/g3908.md) them to the [kyrios](../../strongs/g/g2962.md), on whom they [pisteuō](../../strongs/g/g4100.md).

<a name="acts_14_24"></a>Acts 14:24

And after they [dierchomai](../../strongs/g/g1330.md) [Pisidia](../../strongs/g/g4099.md), they [erchomai](../../strongs/g/g2064.md) to [Pamphylia](../../strongs/g/g3828.md).

<a name="acts_14_25"></a>Acts 14:25

And when they had [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) in [Pergē](../../strongs/g/g4011.md), they [katabainō](../../strongs/g/g2597.md) into [Attaleia](../../strongs/g/g825.md):

<a name="acts_14_26"></a>Acts 14:26

And [kakeithen](../../strongs/g/g2547.md) [apopleō](../../strongs/g/g636.md) to [Antiocheia](../../strongs/g/g490.md), from whence they had been [paradidōmi](../../strongs/g/g3860.md) to the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) for the [ergon](../../strongs/g/g2041.md) which they [plēroō](../../strongs/g/g4137.md).

<a name="acts_14_27"></a>Acts 14:27

And when they were [paraginomai](../../strongs/g/g3854.md), and had [synagō](../../strongs/g/g4863.md) the [ekklēsia](../../strongs/g/g1577.md), they [anaggellō](../../strongs/g/g312.md) all that [theos](../../strongs/g/g2316.md) had [poieō](../../strongs/g/g4160.md) with them, and how he had [anoigō](../../strongs/g/g455.md) the [thyra](../../strongs/g/g2374.md) of [pistis](../../strongs/g/g4102.md) unto the [ethnos](../../strongs/g/g1484.md).

<a name="acts_14_28"></a>Acts 14:28

And there they [diatribō](../../strongs/g/g1304.md) [ou](../../strongs/g/g3756.md) [oligos](../../strongs/g/g3641.md) [chronos](../../strongs/g/g5550.md) with the [mathētēs](../../strongs/g/g3101.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 13](acts_13.md) - [Acts 15](acts_15.md)