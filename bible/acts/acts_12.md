# [Acts 12](https://www.blueletterbible.org/kjv/act/12/1/s_1030001)

<a name="acts_12_1"></a>Acts 12:1

Now about that [kairos](../../strongs/g/g2540.md) [Hērōdēs](../../strongs/g/g2264.md) the [basileus](../../strongs/g/g935.md) [epiballō](../../strongs/g/g1911.md) his [cheir](../../strongs/g/g5495.md) to [kakoō](../../strongs/g/g2559.md) certain of the [ekklēsia](../../strongs/g/g1577.md).

<a name="acts_12_2"></a>Acts 12:2

And he [anaireō](../../strongs/g/g337.md) [Iakōbos](../../strongs/g/g2385.md) the [adelphos](../../strongs/g/g80.md) of [Iōannēs](../../strongs/g/g2491.md) with the [machaira](../../strongs/g/g3162.md).

<a name="acts_12_3"></a>Acts 12:3

And because he [eidō](../../strongs/g/g1492.md) it [arestos](../../strongs/g/g701.md) the [Ioudaios](../../strongs/g/g2453.md), he [prostithēmi](../../strongs/g/g4369.md) to [syllambanō](../../strongs/g/g4815.md) [Petros](../../strongs/g/g4074.md) also. (Then were the [hēmera](../../strongs/g/g2250.md) of [azymos](../../strongs/g/g106.md).)

<a name="acts_12_4"></a>Acts 12:4

And when he had [piazō](../../strongs/g/g4084.md) him, he [tithēmi](../../strongs/g/g5087.md) in [phylakē](../../strongs/g/g5438.md), and [paradidōmi](../../strongs/g/g3860.md) to four [tetradion](../../strongs/g/g5069.md) of [stratiōtēs](../../strongs/g/g4757.md) to [phylassō](../../strongs/g/g5442.md) him; [boulomai](../../strongs/g/g1014.md) after [pascha](../../strongs/g/g3957.md) to [anagō](../../strongs/g/g321.md) him to the [laos](../../strongs/g/g2992.md).

<a name="acts_12_5"></a>Acts 12:5

[Petros](../../strongs/g/g4074.md) therefore was [tēreō](../../strongs/g/g5083.md) in [phylakē](../../strongs/g/g5438.md): but [proseuchē](../../strongs/g/g4335.md) was [ginomai](../../strongs/g/g1096.md) [ektenēs](../../strongs/g/g1618.md) of the [ekklēsia](../../strongs/g/g1577.md) unto [theos](../../strongs/g/g2316.md) for him.

<a name="acts_12_6"></a>Acts 12:6

And when [Hērōdēs](../../strongs/g/g2264.md) would have [proagō](../../strongs/g/g4254.md) him, the same [nyx](../../strongs/g/g3571.md) [Petros](../../strongs/g/g4074.md) was [koimaō](../../strongs/g/g2837.md) between two [stratiōtēs](../../strongs/g/g4757.md), [deō](../../strongs/g/g1210.md) with two [halysis](../../strongs/g/g254.md): and the [phylax](../../strongs/g/g5441.md) before the [thyra](../../strongs/g/g2374.md) [tēreō](../../strongs/g/g5083.md) the [phylakē](../../strongs/g/g5438.md).

<a name="acts_12_7"></a>Acts 12:7

And, [idou](../../strongs/g/g2400.md), the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [ephistēmi](../../strongs/g/g2186.md) him, and a [phōs](../../strongs/g/g5457.md) [lampō](../../strongs/g/g2989.md) in the [oikēma](../../strongs/g/g3612.md): and he [patassō](../../strongs/g/g3960.md) [Petros](../../strongs/g/g4074.md) on the [pleura](../../strongs/g/g4125.md), and [egeirō](../../strongs/g/g1453.md) him up, [legō](../../strongs/g/g3004.md), [anistēmi](../../strongs/g/g450.md) [tachos](../../strongs/g/g5034.md). And his [halysis](../../strongs/g/g254.md) [ekpiptō](../../strongs/g/g1601.md) from his [cheir](../../strongs/g/g5495.md).

<a name="acts_12_8"></a>Acts 12:8

And the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) unto him, [perizōnnymi](../../strongs/g/g4024.md), and [hypodeō](../../strongs/g/g5265.md) thy [sandalion](../../strongs/g/g4547.md). And so he [poieō](../../strongs/g/g4160.md). And he [legō](../../strongs/g/g3004.md) unto him, [periballō](../../strongs/g/g4016.md) thy [himation](../../strongs/g/g2440.md) thee, and [akoloutheō](../../strongs/g/g190.md) me.

<a name="acts_12_9"></a>Acts 12:9

And he [exerchomai](../../strongs/g/g1831.md), and [akoloutheō](../../strongs/g/g190.md) him; and [eidō](../../strongs/g/g1492.md) not that it was [alēthēs](../../strongs/g/g227.md) which was [ginomai](../../strongs/g/g1096.md) by the [aggelos](../../strongs/g/g32.md); but [dokeō](../../strongs/g/g1380.md) he [blepō](../../strongs/g/g991.md) a [horama](../../strongs/g/g3705.md).

<a name="acts_12_10"></a>Acts 12:10

When they were [dierchomai](../../strongs/g/g1330.md) the first and the second [phylakē](../../strongs/g/g5438.md), they [erchomai](../../strongs/g/g2064.md) unto the [sidērous](../../strongs/g/g4603.md) [pylē](../../strongs/g/g4439.md) that [pherō](../../strongs/g/g5342.md) unto the [polis](../../strongs/g/g4172.md); which [anoigō](../../strongs/g/g455.md) to them [automatos](../../strongs/g/g844.md): and they [exerchomai](../../strongs/g/g1831.md), and [proerchomai](../../strongs/g/g4281.md) one [rhymē](../../strongs/g/g4505.md); and [eutheōs](../../strongs/g/g2112.md) the [aggelos](../../strongs/g/g32.md) [aphistēmi](../../strongs/g/g868.md) from him.

<a name="acts_12_11"></a>Acts 12:11

And when [Petros](../../strongs/g/g4074.md) was [ginomai](../../strongs/g/g1096.md) to himself, he [eipon](../../strongs/g/g2036.md), Now I [eidō](../../strongs/g/g1492.md) [alēthōs](../../strongs/g/g230.md), that the [kyrios](../../strongs/g/g2962.md) hath [exapostellō](../../strongs/g/g1821.md) his [aggelos](../../strongs/g/g32.md), and hath [exaireō](../../strongs/g/g1807.md) me out of the [cheir](../../strongs/g/g5495.md) of [Hērōdēs](../../strongs/g/g2264.md), and from all the [prosdokia](../../strongs/g/g4329.md) of the [laos](../../strongs/g/g2992.md) of the [Ioudaios](../../strongs/g/g2453.md).

<a name="acts_12_12"></a>Acts 12:12

And when he had [synoraō](../../strongs/g/g4894.md), he [erchomai](../../strongs/g/g2064.md) to the [oikia](../../strongs/g/g3614.md) of [Maria](../../strongs/g/g3137.md) the [mētēr](../../strongs/g/g3384.md) of [Iōannēs](../../strongs/g/g2491.md), whose [epikaleō](../../strongs/g/g1941.md) was [Markos](../../strongs/g/g3138.md); where [hikanos](../../strongs/g/g2425.md) were [synathroizō](../../strongs/g/g4867.md) [proseuchomai](../../strongs/g/g4336.md).

<a name="acts_12_13"></a>Acts 12:13

And as [Petros](../../strongs/g/g4074.md) [krouō](../../strongs/g/g2925.md) the [thyra](../../strongs/g/g2374.md) of the [pylōn](../../strongs/g/g4440.md), a [paidiskē](../../strongs/g/g3814.md) [proserchomai](../../strongs/g/g4334.md) to [hypakouō](../../strongs/g/g5219.md), [onoma](../../strongs/g/g3686.md) [Rhodē](../../strongs/g/g4498.md).

<a name="acts_12_14"></a>Acts 12:14

And when she [epiginōskō](../../strongs/g/g1921.md) [Petros](../../strongs/g/g4074.md) [phōnē](../../strongs/g/g5456.md), she [anoigō](../../strongs/g/g455.md) not the [pylōn](../../strongs/g/g4440.md) for [chara](../../strongs/g/g5479.md), but [eistrechō](../../strongs/g/g1532.md), and [apaggellō](../../strongs/g/g518.md) how [Petros](../../strongs/g/g4074.md) [histēmi](../../strongs/g/g2476.md) before the [pylōn](../../strongs/g/g4440.md).

<a name="acts_12_15"></a>Acts 12:15

And they [eipon](../../strongs/g/g2036.md) unto her, Thou art [mainomai](../../strongs/g/g3105.md). But she [diïschyrizomai](../../strongs/g/g1340.md) that it was even so. Then [legō](../../strongs/g/g3004.md) they, It is his [aggelos](../../strongs/g/g32.md).

<a name="acts_12_16"></a>Acts 12:16

But [Petros](../../strongs/g/g4074.md) [epimenō](../../strongs/g/g1961.md) [krouō](../../strongs/g/g2925.md): and when they had [anoigō](../../strongs/g/g455.md), and [eidō](../../strongs/g/g1492.md) him, they were [existēmi](../../strongs/g/g1839.md).

<a name="acts_12_17"></a>Acts 12:17

But he, [kataseiō](../../strongs/g/g2678.md) unto them with the [cheir](../../strongs/g/g5495.md) to [sigaō](../../strongs/g/g4601.md), [diēgeomai](../../strongs/g/g1334.md) unto them how the [kyrios](../../strongs/g/g2962.md) had [exagō](../../strongs/g/g1806.md) him out of the [phylakē](../../strongs/g/g5438.md)n. And he [eipon](../../strongs/g/g2036.md), [apaggellō](../../strongs/g/g518.md) these things unto [Iakōbos](../../strongs/g/g2385.md), and to the [adelphos](../../strongs/g/g80.md). And he [exerchomai](../../strongs/g/g1831.md), and [poreuō](../../strongs/g/g4198.md) into another [topos](../../strongs/g/g5117.md).

<a name="acts_12_18"></a>Acts 12:18

Now as soon as it was [hēmera](../../strongs/g/g2250.md), there was no [oligos](../../strongs/g/g3641.md) [tarachos](../../strongs/g/g5017.md) among the [stratiōtēs](../../strongs/g/g4757.md), what was become of [Petros](../../strongs/g/g4074.md).

<a name="acts_12_19"></a>Acts 12:19

And when [Hērōdēs](../../strongs/g/g2264.md) had [epizēteō](../../strongs/g/g1934.md) for him, and [heuriskō](../../strongs/g/g2147.md) him not, he [anakrinō](../../strongs/g/g350.md) the [phylax](../../strongs/g/g5441.md), and [keleuō](../../strongs/g/g2753.md) that should be [apagō](../../strongs/g/g520.md). And he [katerchomai](../../strongs/g/g2718.md) from [Ioudaia](../../strongs/g/g2449.md) to [Kaisareia](../../strongs/g/g2542.md), and [diatribō](../../strongs/g/g1304.md).

<a name="acts_12_20"></a>Acts 12:20

And [Hērōdēs](../../strongs/g/g2264.md) was [thymomacheō](../../strongs/g/g2371.md) with them of [Tyrios](../../strongs/g/g5183.md) and [Sidōnios](../../strongs/g/g4606.md): but they [pareimi](../../strongs/g/g3918.md) [homothymadon](../../strongs/g/g3661.md) to him, and, having made [blastos](../../strongs/g/g986.md) the [basileus](../../strongs/g/g935.md) [koitōn](../../strongs/g/g2846.md) their [peithō](../../strongs/g/g3982.md), [aiteō](../../strongs/g/g154.md) [eirēnē](../../strongs/g/g1515.md); because their [chōra](../../strongs/g/g5561.md) was [trephō](../../strongs/g/g5142.md) by the [basilikos](../../strongs/g/g937.md).

<a name="acts_12_21"></a>Acts 12:21

And upon a [taktos](../../strongs/g/g5002.md) [hēmera](../../strongs/g/g2250.md) [Hērōdēs](../../strongs/g/g2264.md), [endyō](../../strongs/g/g1746.md) in [basilikos](../../strongs/g/g937.md) [esthēs](../../strongs/g/g2066.md), [kathizō](../../strongs/g/g2523.md) upon his [bēma](../../strongs/g/g968.md), and [dēmēgoreō](../../strongs/g/g1215.md) unto them.

<a name="acts_12_22"></a>Acts 12:22

And the [dēmos](../../strongs/g/g1218.md) [epiphōneō](../../strongs/g/g2019.md), the [phōnē](../../strongs/g/g5456.md) of [theos](../../strongs/g/g2316.md), and not of [anthrōpos](../../strongs/g/g444.md).

<a name="acts_12_23"></a>Acts 12:23

And [parachrēma](../../strongs/g/g3916.md) the [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) [patassō](../../strongs/g/g3960.md) him, because he [didōmi](../../strongs/g/g1325.md) not [theos](../../strongs/g/g2316.md) [doxa](../../strongs/g/g1391.md): and he was [skōlēkobrōtos](../../strongs/g/g4662.md), and [ekpsychō](../../strongs/g/g1634.md).

<a name="acts_12_24"></a>Acts 12:24

But the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) [auxanō](../../strongs/g/g837.md) and [plēthynō](../../strongs/g/g4129.md).

<a name="acts_12_25"></a>Acts 12:25

And [Barnabas](../../strongs/g/g921.md) and [Saulos](../../strongs/g/g4569.md) [hypostrephō](../../strongs/g/g5290.md) from [Ierousalēm](../../strongs/g/g2419.md), when they had [plēroō](../../strongs/g/g4137.md) their [diakonia](../../strongs/g/g1248.md), and [symparalambanō](../../strongs/g/g4838.md) [Iōannēs](../../strongs/g/g2491.md), whose [epikaleō](../../strongs/g/g1941.md) was [Markos](../../strongs/g/g3138.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 11](acts_11.md) - [Acts 13](acts_13.md)