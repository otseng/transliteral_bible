# [Acts 1](https://www.blueletterbible.org/kjv/act/1/1/s_1019001)

<a name="acts_1_1"></a>Acts 1:1

[men](../../strongs/g/g3303.md) [prōtos](../../strongs/g/g4413.md) [logos](../../strongs/g/g3056.md) have I [poieō](../../strongs/g/g4160.md), [ō](../../strongs/g/g5599.md) [Theophilos](../../strongs/g/g2321.md), of all that [Iēsous](../../strongs/g/g2424.md) [archomai](../../strongs/g/g756.md) both to [poieō](../../strongs/g/g4160.md) and [didaskō](../../strongs/g/g1321.md),

<a name="acts_1_2"></a>Acts 1:2

Until the [hēmera](../../strongs/g/g2250.md) in which he was [analambanō](../../strongs/g/g353.md), after that he through the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) had [entellō](../../strongs/g/g1781.md) unto the [apostolos](../../strongs/g/g652.md) whom he had [eklegomai](../../strongs/g/g1586.md):

<a name="acts_1_3"></a>Acts 1:3

To whom also he [paristēmi](../../strongs/g/g3936.md) himself [zaō](../../strongs/g/g2198.md) after his [paschō](../../strongs/g/g3958.md) by [polys](../../strongs/g/g4183.md) [tekmērion](../../strongs/g/g5039.md), being [optanomai](../../strongs/g/g3700.md) of them forty days, and [legō](../../strongs/g/g3004.md) of the things pertaining to the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md):

<a name="acts_1_4"></a>Acts 1:4

And, [synalizō](../../strongs/g/g4871.md) with them, [paraggellō](../../strongs/g/g3853.md) them that they should not [chōrizō](../../strongs/g/g5563.md) from [Hierosolyma](../../strongs/g/g2414.md), but [perimenō](../../strongs/g/g4037.md) for the [epaggelia](../../strongs/g/g1860.md) of the [patēr](../../strongs/g/g3962.md), which, ye have [akouō](../../strongs/g/g191.md) of me.

<a name="acts_1_5"></a>Acts 1:5

For [Iōannēs](../../strongs/g/g2491.md) [men](../../strongs/g/g3303.md) [baptizō](../../strongs/g/g907.md) with [hydōr](../../strongs/g/g5204.md); but ye shall be [baptizō](../../strongs/g/g907.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) not [polys](../../strongs/g/g4183.md) [hēmera](../../strongs/g/g2250.md) hence.

<a name="acts_1_6"></a>Acts 1:6

When they therefore were [synerchomai](../../strongs/g/g4905.md), they [eperōtaō](../../strongs/g/g1905.md) of him, [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), wilt thou at this [chronos](../../strongs/g/g5550.md) [apokathistēmi](../../strongs/g/g600.md) the [basileia](../../strongs/g/g932.md) to [Israēl](../../strongs/g/g2474.md)?

<a name="acts_1_7"></a>Acts 1:7

And he [eipon](../../strongs/g/g2036.md) unto them, It is not for you to [ginōskō](../../strongs/g/g1097.md) the [chronos](../../strongs/g/g5550.md) or the [kairos](../../strongs/g/g2540.md), which the [patēr](../../strongs/g/g3962.md) hath [tithēmi](../../strongs/g/g5087.md) in his own [exousia](../../strongs/g/g1849.md).

<a name="acts_1_8"></a>Acts 1:8

But ye shall [lambanō](../../strongs/g/g2983.md) [dynamis](../../strongs/g/g1411.md), after that the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) is [eperchomai](../../strongs/g/g1904.md) upon you: and ye shall be [martys](../../strongs/g/g3144.md) unto me both in [Ierousalēm](../../strongs/g/g2419.md), and in all [Ioudaia](../../strongs/g/g2449.md), and in [Samareia](../../strongs/g/g4540.md), and unto [eschatos](../../strongs/g/g2078.md) of [gē](../../strongs/g/g1093.md).

<a name="acts_1_9"></a>Acts 1:9

And when he had [eipon](../../strongs/g/g2036.md) these things, while they [blepō](../../strongs/g/g991.md), he was [epairō](../../strongs/g/g1869.md); and a [nephelē](../../strongs/g/g3507.md) [hypolambanō](../../strongs/g/g5274.md) him out of their [ophthalmos](../../strongs/g/g3788.md).

<a name="acts_1_10"></a>Acts 1:10

And while they [ēn](../../strongs/g/g2258.md) [atenizō](../../strongs/g/g816.md) toward [ouranos](../../strongs/g/g3772.md) as he [poreuō](../../strongs/g/g4198.md), [idou](../../strongs/g/g2400.md), two [anēr](../../strongs/g/g435.md) [paristēmi](../../strongs/g/g3936.md) them in [leukos](../../strongs/g/g3022.md) [esthēs](../../strongs/g/g2066.md);

<a name="acts_1_11"></a>Acts 1:11

Which also [eipon](../../strongs/g/g2036.md), Ye [anēr](../../strongs/g/g435.md) of [Galilaios](../../strongs/g/g1057.md), why [histēmi](../../strongs/g/g2476.md) ye [emblepō](../../strongs/g/g1689.md) up into [ouranos](../../strongs/g/g3772.md)? this same [Iēsous](../../strongs/g/g2424.md), which is [analambanō](../../strongs/g/g353.md) from you into [ouranos](../../strongs/g/g3772.md), shall so [erchomai](../../strongs/g/g2064.md) [hos](../../strongs/g/g3739.md) [tropos](../../strongs/g/g5158.md) ye have [theaomai](../../strongs/g/g2300.md) him [poreuō](../../strongs/g/g4198.md) into [ouranos](../../strongs/g/g3772.md).

<a name="acts_1_12"></a>Acts 1:12

Then [hypostrephō](../../strongs/g/g5290.md) unto [Ierousalēm](../../strongs/g/g2419.md) from the [oros](../../strongs/g/g3735.md) [kaleō](../../strongs/g/g2564.md) [elaiōn](../../strongs/g/g1638.md), which is [eggys](../../strongs/g/g1451.md) [Ierousalēm](../../strongs/g/g2419.md) a [sabbaton](../../strongs/g/g4521.md) [echō](../../strongs/g/g2192.md) [hodos](../../strongs/g/g3598.md).

<a name="acts_1_13"></a>Acts 1:13

And when they were [eiserchomai](../../strongs/g/g1525.md), they [anabainō](../../strongs/g/g305.md) into [hyperōon](../../strongs/g/g5253.md), where [ēn](../../strongs/g/g2258.md) [katamenō](../../strongs/g/g2650.md) [te](../../strongs/g/g5037.md) [Petros](../../strongs/g/g4074.md), and [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md), and [Andreas](../../strongs/g/g406.md), [Philippos](../../strongs/g/g5376.md), and [Thōmas](../../strongs/g/g2381.md), [Bartholomaios](../../strongs/g/g918.md), and [Maththaios](../../strongs/g/g3156.md), [Iakōbos](../../strongs/g/g2385.md) of [Alphaios](../../strongs/g/g256.md), and [Simōn](../../strongs/g/g4613.md) [Zēlōtēs](../../strongs/g/g2208.md), and [Ioudas](../../strongs/g/g2455.md) of [Iakōbos](../../strongs/g/g2385.md).

<a name="acts_1_14"></a>Acts 1:14

These all [ēn](../../strongs/g/g2258.md) [proskartereō](../../strongs/g/g4342.md) with [homothymadon](../../strongs/g/g3661.md) in [proseuchē](../../strongs/g/g4335.md) and [deēsis](../../strongs/g/g1162.md), with the [gynē](../../strongs/g/g1135.md), and [Maria](../../strongs/g/g3137.md) the [mētēr](../../strongs/g/g3384.md) of [Iēsous](../../strongs/g/g2424.md), and with his [adelphos](../../strongs/g/g80.md).

<a name="acts_1_15"></a>Acts 1:15

And in those [hēmera](../../strongs/g/g2250.md) [Petros](../../strongs/g/g4074.md) [anistēmi](../../strongs/g/g450.md) in the [mesos](../../strongs/g/g3319.md) of the [mathētēs](../../strongs/g/g3101.md), and [eipon](../../strongs/g/g2036.md), (the [ochlos](../../strongs/g/g3793.md) of [onoma](../../strongs/g/g3686.md) together were about an hundred and twenty,)

<a name="acts_1_16"></a>Acts 1:16

[anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), this [graphē](../../strongs/g/g1124.md) [dei](../../strongs/g/g1163.md) needs have been [plēroō](../../strongs/g/g4137.md), which the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) by the [stoma](../../strongs/g/g4750.md) of [Dabid](../../strongs/g/g1138.md) [proepō](../../strongs/g/g4277.md) concerning [Ioudas](../../strongs/g/g2455.md), which was [hodēgos](../../strongs/g/g3595.md) [syllambanō](../../strongs/g/g4815.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_1_17"></a>Acts 1:17

For he was [katarithmeō](../../strongs/g/g2674.md) with us, and had [lagchanō](../../strongs/g/g2975.md) [klēros](../../strongs/g/g2819.md) of this [diakonia](../../strongs/g/g1248.md).

<a name="acts_1_18"></a>Acts 1:18

Now [houtos](../../strongs/g/g3778.md) [ktaomai](../../strongs/g/g2932.md) a [chōrion](../../strongs/g/g5564.md) with the [misthos](../../strongs/g/g3408.md) of [adikia](../../strongs/g/g93.md); and [ginomai](../../strongs/g/g1096.md) [prēnēs](../../strongs/g/g4248.md), he [lakaō](../../strongs/g/g2997.md) in the [mesos](../../strongs/g/g3319.md), and all his [splagchnon](../../strongs/g/g4698.md) [ekcheō](../../strongs/g/g1632.md).

<a name="acts_1_19"></a>Acts 1:19

And it was [gnōstos](../../strongs/g/g1110.md) unto all the [katoikeō](../../strongs/g/g2730.md) at [Ierousalēm](../../strongs/g/g2419.md); insomuch as that [chōrion](../../strongs/g/g5564.md) is [kaleō](../../strongs/g/g2564.md) in their [idios](../../strongs/g/g2398.md) [dialektos](../../strongs/g/g1258.md), [Akeldama](../../strongs/g/g184.md), [tout᾿ estin](../../strongs/g/g5123.md), The [chōrion](../../strongs/g/g5564.md) of [haima](../../strongs/g/g129.md).

<a name="acts_1_20"></a>Acts 1:20

For it is [graphō](../../strongs/g/g1125.md) in the [biblos](../../strongs/g/g976.md) of [psalmos](../../strongs/g/g5568.md), Let his [epaulis](../../strongs/g/g1886.md) be [erēmos](../../strongs/g/g2048.md), and let [mē](../../strongs/g/g3361.md) [katoikeō](../../strongs/g/g2730.md) therein: and his [episkopē](../../strongs/g/g1984.md) let another [lambanō](../../strongs/g/g2983.md). [^1]

<a name="acts_1_21"></a>Acts 1:21

Wherefore of these [anēr](../../strongs/g/g435.md) which have [synerchomai](../../strongs/g/g4905.md) with us all the [chronos](../../strongs/g/g5550.md) that the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [exerchomai](../../strongs/g/g1831.md) and [eiserchomai](../../strongs/g/g1525.md) among us,

<a name="acts_1_22"></a>Acts 1:22

[archomai](../../strongs/g/g756.md) from the [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md), unto that same [hēmera](../../strongs/g/g2250.md) that he was [analambanō](../../strongs/g/g353.md) from us, must one [ginomai](../../strongs/g/g1096.md) to be a [martys](../../strongs/g/g3144.md) with us of his [anastasis](../../strongs/g/g386.md).

<a name="acts_1_23"></a>Acts 1:23

And they [histēmi](../../strongs/g/g2476.md) two, [Iōsēph](../../strongs/g/g2501.md) [kaleō](../../strongs/g/g2564.md) [Barsabas](../../strongs/g/g923.md), who was [epikaleō](../../strongs/g/g1941.md) [Ioustos](../../strongs/g/g2459.md), and [Maththias](../../strongs/g/g3159.md).

<a name="acts_1_24"></a>Acts 1:24

And they [proseuchomai](../../strongs/g/g4336.md), and [eipon](../../strongs/g/g2036.md), [sy](../../strongs/g/g4771.md), [kyrios](../../strongs/g/g2962.md), [kardiognōstēs](../../strongs/g/g2589.md) of all, [anadeiknymi](../../strongs/g/g322.md) whether of these two thou hast [eklegomai](../../strongs/g/g1586.md),

<a name="acts_1_25"></a>Acts 1:25

That he may [lambanō](../../strongs/g/g2983.md) [klēros](../../strongs/g/g2819.md) of this [diakonia](../../strongs/g/g1248.md) and [apostolē](../../strongs/g/g651.md), from which [Ioudas](../../strongs/g/g2455.md) [parabainō](../../strongs/g/g3845.md), that he might [poreuō](../../strongs/g/g4198.md) to his own [topos](../../strongs/g/g5117.md).

<a name="acts_1_26"></a>Acts 1:26

And they [didōmi](../../strongs/g/g1325.md) their [klēros](../../strongs/g/g2819.md); and the [klēros](../../strongs/g/g2819.md) [piptō](../../strongs/g/g4098.md) upon [Maththias](../../strongs/g/g3159.md); and he was [sygkatapsēphizomai](../../strongs/g/g4785.md) with the eleven [apostolos](../../strongs/g/g652.md).

---

[^1]: [Psalms 69:25](../psalms/psalms_69.md#psalms_69_25)

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 2](acts_2.md)
