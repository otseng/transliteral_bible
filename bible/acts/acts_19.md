# [Acts 19](https://www.blueletterbible.org/kjv/act/19/1/s_1037001)

<a name="acts_19_1"></a>Acts 19:1

And [ginomai](../../strongs/g/g1096.md), that, while [Apollōs](../../strongs/g/g625.md) was at [Korinthos](../../strongs/g/g2882.md), [Paulos](../../strongs/g/g3972.md) having [dierchomai](../../strongs/g/g1330.md) through the [anōterikos](../../strongs/g/g510.md) [meros](../../strongs/g/g3313.md) [erchomai](../../strongs/g/g2064.md) to [Ephesos](../../strongs/g/g2181.md): and [heuriskō](../../strongs/g/g2147.md) certain [mathētēs](../../strongs/g/g3101.md),

<a name="acts_19_2"></a>Acts 19:2

He [eipon](../../strongs/g/g2036.md) unto them, Have ye [lambanō](../../strongs/g/g2983.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) since ye [pisteuō](../../strongs/g/g4100.md)?  And they [eipon](../../strongs/g/g2036.md) unto him, We have not so much as [akouō](../../strongs/g/g191.md) whether there be any [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_19_3"></a>Acts 19:3

And he [eipon](../../strongs/g/g2036.md) unto them, Unto what then were ye [baptizō](../../strongs/g/g907.md)? And they [eipon](../../strongs/g/g2036.md), Unto [Iōannēs](../../strongs/g/g2491.md) [baptisma](../../strongs/g/g908.md).

<a name="acts_19_4"></a>Acts 19:4

Then [eipon](../../strongs/g/g2036.md) [Paulos](../../strongs/g/g3972.md), [Iōannēs](../../strongs/g/g2491.md) [men](../../strongs/g/g3303.md) [baptizō](../../strongs/g/g907.md) with the [baptisma](../../strongs/g/g908.md) of [metanoia](../../strongs/g/g3341.md), [legō](../../strongs/g/g3004.md) unto the [laos](../../strongs/g/g2992.md), that they should [pisteuō](../../strongs/g/g4100.md) on him which should [erchomai](../../strongs/g/g2064.md) after him, that is, on [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_19_5"></a>Acts 19:5

When they [akouō](../../strongs/g/g191.md), they were [baptizō](../../strongs/g/g907.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_19_6"></a>Acts 19:6

And when [Paulos](../../strongs/g/g3972.md) had [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) upon them, the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [erchomai](../../strongs/g/g2064.md) on them; and they [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md), and [prophēteuō](../../strongs/g/g4395.md).

<a name="acts_19_7"></a>Acts 19:7

And all the [anēr](../../strongs/g/g435.md) were about [dekadyo](../../strongs/g/g1177.md).

<a name="acts_19_8"></a>Acts 19:8

And he [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md), and [parrēsiazomai](../../strongs/g/g3955.md) for the space of three [mēn](../../strongs/g/g3376.md), [dialegomai](../../strongs/g/g1256.md) and [peithō](../../strongs/g/g3982.md) the things concerning the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_19_9"></a>Acts 19:9

But when [tis](../../strongs/g/g5100.md) were [sklērynō](../../strongs/g/g4645.md), and [apeitheō](../../strongs/g/g544.md), but [kakologeō](../../strongs/g/g2551.md) of [hodos](../../strongs/g/g3598.md) before the [plēthos](../../strongs/g/g4128.md), he [aphistēmi](../../strongs/g/g868.md) from them, and [aphorizō](../../strongs/g/g873.md) the [mathētēs](../../strongs/g/g3101.md), [dialegomai](../../strongs/g/g1256.md) daily in the [scholē](../../strongs/g/g4981.md) of one [Tyrannos](../../strongs/g/g5181.md).

<a name="acts_19_10"></a>Acts 19:10

And this [ginomai](../../strongs/g/g1096.md) by the space of two [etos](../../strongs/g/g2094.md); so that all they which [katoikeō](../../strongs/g/g2730.md) in [Asia](../../strongs/g/g773.md) [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), both [Ioudaios](../../strongs/g/g2453.md) and [Hellēn](../../strongs/g/g1672.md).

<a name="acts_19_11"></a>Acts 19:11

And [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) [ou](../../strongs/g/g3756.md) [tygchanō](../../strongs/g/g5177.md) [dynamis](../../strongs/g/g1411.md) by the [cheir](../../strongs/g/g5495.md) of [Paulos](../../strongs/g/g3972.md):

<a name="acts_19_12"></a>Acts 19:12

So that from his [chrōs](../../strongs/g/g5559.md) were [epipherō](../../strongs/g/g2018.md) unto the [astheneō](../../strongs/g/g770.md) [soudarion](../../strongs/g/g4676.md) or [simikinthion](../../strongs/g/g4612.md), and the [nosos](../../strongs/g/g3554.md) [apallassō](../../strongs/g/g525.md) from them, and the [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md) [exerchomai](../../strongs/g/g1831.md) out of them.

<a name="acts_19_13"></a>Acts 19:13

Then certain of the [perierchomai](../../strongs/g/g4022.md) [Ioudaios](../../strongs/g/g2453.md), [exorkistēs](../../strongs/g/g1845.md), [epicheireō](../../strongs/g/g2021.md) them to [onomazō](../../strongs/g/g3687.md) over them which had [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md), We [horkizō](../../strongs/g/g3726.md) you by [Iēsous](../../strongs/g/g2424.md) whom [Paulos](../../strongs/g/g3972.md) [kēryssō](../../strongs/g/g2784.md).

<a name="acts_19_14"></a>Acts 19:14

And there were seven [huios](../../strongs/g/g5207.md) of [Skeuas](../../strongs/g/g4630.md), [Ioudaios](../../strongs/g/g2453.md), and [archiereus](../../strongs/g/g749.md), which [poieō](../../strongs/g/g4160.md) so.

<a name="acts_19_15"></a>Acts 19:15

And the [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [Iēsous](../../strongs/g/g2424.md) I [ginōskō](../../strongs/g/g1097.md), and [Paulos](../../strongs/g/g3972.md) I [epistamai](../../strongs/g/g1987.md); but who are ye?

<a name="acts_19_16"></a>Acts 19:16

And the [anthrōpos](../../strongs/g/g444.md) in whom the [ponēros](../../strongs/g/g4190.md) [pneuma](../../strongs/g/g4151.md) was [ephallomai](../../strongs/g/g2177.md) on them, and [katakyrieuō](../../strongs/g/g2634.md) them, and [ischyō](../../strongs/g/g2480.md) against them, so that they [ekpheugō](../../strongs/g/g1628.md) out of that [oikos](../../strongs/g/g3624.md) [gymnos](../../strongs/g/g1131.md) and [traumatizō](../../strongs/g/g5135.md).

<a name="acts_19_17"></a>Acts 19:17

And this was [gnōstos](../../strongs/g/g1110.md) to all the [Ioudaios](../../strongs/g/g2453.md) and [Hellēn](../../strongs/g/g1672.md) also [katoikeō](../../strongs/g/g2730.md) at [Ephesos](../../strongs/g/g2181.md); and [phobos](../../strongs/g/g5401.md) [epipiptō](../../strongs/g/g1968.md) on them all, and the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) was [megalynō](../../strongs/g/g3170.md).

<a name="acts_19_18"></a>Acts 19:18

And [polys](../../strongs/g/g4183.md) that [pisteuō](../../strongs/g/g4100.md) [erchomai](../../strongs/g/g2064.md), and [exomologeō](../../strongs/g/g1843.md), and [anaggellō](../../strongs/g/g312.md) their [praxis](../../strongs/g/g4234.md).

<a name="acts_19_19"></a>Acts 19:19

[hikanos](../../strongs/g/g2425.md) of them also which [prassō](../../strongs/g/g4238.md) [periergos](../../strongs/g/g4021.md) [sympherō](../../strongs/g/g4851.md) their [biblos](../../strongs/g/g976.md) together, and [katakaiō](../../strongs/g/g2618.md) them before all: and they [sympsēphizō](../../strongs/g/g4860.md) the [timē](../../strongs/g/g5092.md) of them, and [heuriskō](../../strongs/g/g2147.md) it fifty thousand [argyrion](../../strongs/g/g694.md).

<a name="acts_19_20"></a>Acts 19:20

So [kratos](../../strongs/g/g2904.md) [auxanō](../../strongs/g/g837.md) the [logos](../../strongs/g/g3056.md) of [kyrios](../../strongs/g/g2962.md) and [ischyō](../../strongs/g/g2480.md).

<a name="acts_19_21"></a>Acts 19:21

After these things were [plēroō](../../strongs/g/g4137.md), [Paulos](../../strongs/g/g3972.md) [tithēmi](../../strongs/g/g5087.md) in the [pneuma](../../strongs/g/g4151.md), when he had [dierchomai](../../strongs/g/g1330.md) [Makedonia](../../strongs/g/g3109.md) and [Achaïa](../../strongs/g/g882.md), to [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md), [eipon](../../strongs/g/g2036.md), After I have been there, I must also [eidō](../../strongs/g/g1492.md) [Rhōmē](../../strongs/g/g4516.md).

<a name="acts_19_22"></a>Acts 19:22

So he [apostellō](../../strongs/g/g649.md) into [Makedonia](../../strongs/g/g3109.md) two of them that [diakoneō](../../strongs/g/g1247.md) unto him, [Timotheos](../../strongs/g/g5095.md) and [Erastos](../../strongs/g/g2037.md); but he himself [epechō](../../strongs/g/g1907.md) in [Asia](../../strongs/g/g773.md) for a [chronos](../../strongs/g/g5550.md).

<a name="acts_19_23"></a>Acts 19:23

And the same [kairos](../../strongs/g/g2540.md) there arose no [oligos](../../strongs/g/g3641.md) [tarachos](../../strongs/g/g5017.md) about that [hodos](../../strongs/g/g3598.md).

<a name="acts_19_24"></a>Acts 19:24

For a certain [onoma](../../strongs/g/g3686.md) [Dēmētrios](../../strongs/g/g1216.md), an [argyrokopos](../../strongs/g/g695.md), which [poieō](../../strongs/g/g4160.md) [argyreos](../../strongs/g/g693.md) [naos](../../strongs/g/g3485.md) for [Artemis](../../strongs/g/g735.md), [parechō](../../strongs/g/g3930.md) no [oligos](../../strongs/g/g3641.md) [ergasia](../../strongs/g/g2039.md) unto the [technitēs](../../strongs/g/g5079.md);

<a name="acts_19_25"></a>Acts 19:25

Whom he [synathroizō](../../strongs/g/g4867.md) with the [ergatēs](../../strongs/g/g2040.md) of like [toioutos](../../strongs/g/g5108.md), and [eipon](../../strongs/g/g2036.md), [anēr](../../strongs/g/g435.md), ye [epistamai](../../strongs/g/g1987.md) that by this [ergasia](../../strongs/g/g2039.md) we have our [euporia](../../strongs/g/g2142.md).

<a name="acts_19_26"></a>Acts 19:26

Moreover ye [theōreō](../../strongs/g/g2334.md) and [akouō](../../strongs/g/g191.md), that not alone at [Ephesos](../../strongs/g/g2181.md), but [schedon](../../strongs/g/g4975.md) throughout all [Asia](../../strongs/g/g773.md), this [Paulos](../../strongs/g/g3972.md) hath [peithō](../../strongs/g/g3982.md) and [methistēmi](../../strongs/g/g3179.md) [hikanos](../../strongs/g/g2425.md) [ochlos](../../strongs/g/g3793.md), [legō](../../strongs/g/g3004.md) that they be no [theos](../../strongs/g/g2316.md), which are [ginomai](../../strongs/g/g1096.md) with [cheir](../../strongs/g/g5495.md):

<a name="acts_19_27"></a>Acts 19:27

So that not only this our [meros](../../strongs/g/g3313.md) is in [kindyneuō](../../strongs/g/g2793.md) to be [erchomai](../../strongs/g/g2064.md) at [apelegmos](../../strongs/g/g557.md); but also that the [hieron](../../strongs/g/g2411.md) of the [megas](../../strongs/g/g3173.md) [thea](../../strongs/g/g2299.md) [Artemis](../../strongs/g/g735.md) should be [eis](../../strongs/g/g1519.md) [oudeis](../../strongs/g/g3762.md) [logizomai](../../strongs/g/g3049.md), and her [megaleiotēs](../../strongs/g/g3168.md) should be [kathaireō](../../strongs/g/g2507.md), whom all [Asia](../../strongs/g/g773.md) and the [oikoumenē](../../strongs/g/g3625.md) [sebō](../../strongs/g/g4576.md).

<a name="acts_19_28"></a>Acts 19:28

And when they [akouō](../../strongs/g/g191.md), they were [plērēs](../../strongs/g/g4134.md) of [thymos](../../strongs/g/g2372.md), and [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), [megas](../../strongs/g/g3173.md) is [Artemis](../../strongs/g/g735.md) of the [Ephesios](../../strongs/g/g2180.md).

<a name="acts_19_29"></a>Acts 19:29

And the [holos](../../strongs/g/g3650.md) [polis](../../strongs/g/g4172.md) was [pimplēmi](../../strongs/g/g4130.md) with [sygchysis](../../strongs/g/g4799.md): and having [synarpazō](../../strongs/g/g4884.md) [Gaïos](../../strongs/g/g1050.md) and [Aristarchos](../../strongs/g/g708.md), [Makedōn](../../strongs/g/g3110.md), [Paulos](../../strongs/g/g3972.md) [synekdēmos](../../strongs/g/g4898.md), they [hormaō](../../strongs/g/g3729.md) [homothymadon](../../strongs/g/g3661.md) into the [theatron](../../strongs/g/g2302.md).

<a name="acts_19_30"></a>Acts 19:30

And when [Paulos](../../strongs/g/g3972.md) [boulomai](../../strongs/g/g1014.md) have [eiserchomai](../../strongs/g/g1525.md) in unto the [dēmos](../../strongs/g/g1218.md), the [mathētēs](../../strongs/g/g3101.md) [eaō](../../strongs/g/g1439.md) him not.

<a name="acts_19_31"></a>Acts 19:31

And certain of the [Asiarchēs](../../strongs/g/g775.md), which were his [philos](../../strongs/g/g5384.md), [pempō](../../strongs/g/g3992.md) unto him, [parakaleō](../../strongs/g/g3870.md) him that he would not [didōmi](../../strongs/g/g1325.md) himself into the [theatron](../../strongs/g/g2302.md).

<a name="acts_19_32"></a>Acts 19:32

Some therefore [krazō](../../strongs/g/g2896.md) one thing, and some another: for the [ekklēsia](../../strongs/g/g1577.md) was [sygcheō](../../strongs/g/g4797.md): and the more part [eidō](../../strongs/g/g1492.md) not wherefore they were [synerchomai](../../strongs/g/g4905.md).

<a name="acts_19_33"></a>Acts 19:33

And they [probibazō](../../strongs/g/g4264.md) [Alexandros](../../strongs/g/g223.md) out of the [ochlos](../../strongs/g/g3793.md), the [Ioudaios](../../strongs/g/g2453.md) [proballō](../../strongs/g/g4261.md) him. And [Alexandros](../../strongs/g/g223.md) [kataseiō](../../strongs/g/g2678.md) with the [cheir](../../strongs/g/g5495.md), and would have made his [apologeomai](../../strongs/g/g626.md) unto the [dēmos](../../strongs/g/g1218.md).

<a name="acts_19_34"></a>Acts 19:34

But when they [epiginōskō](../../strongs/g/g1921.md) that he was an [Ioudaios](../../strongs/g/g2453.md), all with one [phōnē](../../strongs/g/g5456.md) about the space of two [hōra](../../strongs/g/g5610.md) [krazō](../../strongs/g/g2896.md), [megas](../../strongs/g/g3173.md) is [Artemis](../../strongs/g/g735.md) of the [Ephesios](../../strongs/g/g2180.md).

<a name="acts_19_35"></a>Acts 19:35

And when the [grammateus](../../strongs/g/g1122.md) had [katastellō](../../strongs/g/g2687.md) the [ochlos](../../strongs/g/g3793.md), he [phēmi](../../strongs/g/g5346.md), Ye [anēr](../../strongs/g/g435.md) of [Ephesios](../../strongs/g/g2180.md), what [anthrōpos](../../strongs/g/g444.md) is there that [ginōskō](../../strongs/g/g1097.md) not how that the [polis](../../strongs/g/g4172.md) of the [Ephesios](../../strongs/g/g2180.md) is a [neōkoros](../../strongs/g/g3511.md) of the [megas](../../strongs/g/g3173.md) [thea](../../strongs/g/g2299.md) [Artemis](../../strongs/g/g735.md), and of [diopetēs](../../strongs/g/g1356.md)?

<a name="acts_19_36"></a>Acts 19:36

Seeing then that these things [anantirrētos](../../strongs/g/g368.md), ye [dei](../../strongs/g/g1163.md) to be [katastellō](../../strongs/g/g2687.md), and to [prassō](../../strongs/g/g4238.md) [mēdeis](../../strongs/g/g3367.md) [propetēs](../../strongs/g/g4312.md).

<a name="acts_19_37"></a>Acts 19:37

For ye have [agō](../../strongs/g/g71.md) these [anēr](../../strongs/g/g435.md), which are neither [hierosylos](../../strongs/g/g2417.md), nor yet [blasphēmeō](../../strongs/g/g987.md) of your [thea](../../strongs/g/g2299.md).

<a name="acts_19_38"></a>Acts 19:38

Wherefore if [Dēmētrios](../../strongs/g/g1216.md), and the [technitēs](../../strongs/g/g5079.md) which are with him, have a [logos](../../strongs/g/g3056.md) against [tis](../../strongs/g/g5100.md), the [agoraios](../../strongs/g/g60.md) is [agō](../../strongs/g/g71.md), and there are [anthypatos](../../strongs/g/g446.md): let them [egkaleō](../../strongs/g/g1458.md) [allēlōn](../../strongs/g/g240.md).

<a name="acts_19_39"></a>Acts 19:39

But if ye [epizēteō](../../strongs/g/g1934.md) any thing concerning [heteros](../../strongs/g/g2087.md), it shall be [epilyō](../../strongs/g/g1956.md) in an [ennomos](../../strongs/g/g1772.md) [ekklēsia](../../strongs/g/g1577.md).

<a name="acts_19_40"></a>Acts 19:40

For we are in [kindyneuō](../../strongs/g/g2793.md) to be [egkaleō](../../strongs/g/g1458.md) for this [sēmeron](../../strongs/g/g4594.md) [stasis](../../strongs/g/g4714.md), there being [mēdeis](../../strongs/g/g3367.md) [aition](../../strongs/g/g158.md) whereby we may [apodidōmi](../../strongs/g/g591.md) a [logos](../../strongs/g/g3056.md) of this [systrophē](../../strongs/g/g4963.md).

<a name="acts_19_41"></a>Acts 19:41

And when he had thus [eipon](../../strongs/g/g2036.md), he [apolyō](../../strongs/g/g630.md) the [ekklēsia](../../strongs/g/g1577.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 18](acts_18.md) - [Acts 20](acts_20.md)