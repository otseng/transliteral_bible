# [Acts 11](https://www.blueletterbible.org/kjv/act/11/1/s_1029001)

<a name="acts_11_1"></a>Acts 11:1

And the [apostolos](../../strongs/g/g652.md) and [adelphos](../../strongs/g/g80.md) that were in [Ioudaia](../../strongs/g/g2449.md) [akouō](../../strongs/g/g191.md) that the [ethnos](../../strongs/g/g1484.md) had also [dechomai](../../strongs/g/g1209.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_11_2"></a>Acts 11:2

And when [Petros](../../strongs/g/g4074.md) was [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md), they that were of the [peritomē](../../strongs/g/g4061.md) [diakrinō](../../strongs/g/g1252.md) with him,

<a name="acts_11_3"></a>Acts 11:3

[legō](../../strongs/g/g3004.md), Thou [eiserchomai](../../strongs/g/g1525.md) to [anēr](../../strongs/g/g435.md) [akrobystia](../../strongs/g/g203.md), and didst [synesthiō](../../strongs/g/g4906.md) with them.

<a name="acts_11_4"></a>Acts 11:4

But [Petros](../../strongs/g/g4074.md) [archomai](../../strongs/g/g756.md), and [ektithēmi](../../strongs/g/g1620.md) it [kathexēs](../../strongs/g/g2517.md) unto them, [legō](../../strongs/g/g3004.md),

<a name="acts_11_5"></a>Acts 11:5

I was in the [polis](../../strongs/g/g4172.md) of [Ioppē](../../strongs/g/g2445.md) [proseuchomai](../../strongs/g/g4336.md): and in [ekstasis](../../strongs/g/g1611.md) I [eidō](../../strongs/g/g1492.md) a [horama](../../strongs/g/g3705.md), A certain [skeuos](../../strongs/g/g4632.md) [katabainō](../../strongs/g/g2597.md), as it had been a [megas](../../strongs/g/g3173.md) [othonē](../../strongs/g/g3607.md), [kathiēmi](../../strongs/g/g2524.md) from [ouranos](../../strongs/g/g3772.md) by four [archē](../../strongs/g/g746.md); and it [erchomai](../../strongs/g/g2064.md) even to me:

<a name="acts_11_6"></a>Acts 11:6

Upon the which when I had [atenizō](../../strongs/g/g816.md), I [katanoeō](../../strongs/g/g2657.md), and [eidō](../../strongs/g/g1492.md) [tetrapous](../../strongs/g/g5074.md) of [gē](../../strongs/g/g1093.md), and [thērion](../../strongs/g/g2342.md), and [herpeton](../../strongs/g/g2062.md), and [peteinon](../../strongs/g/g4071.md) of [ouranos](../../strongs/g/g3772.md).

<a name="acts_11_7"></a>Acts 11:7

And I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) [legō](../../strongs/g/g3004.md) unto me, [anistēmi](../../strongs/g/g450.md), [Petros](../../strongs/g/g4074.md); [thyō](../../strongs/g/g2380.md) and [phago](../../strongs/g/g5315.md).

<a name="acts_11_8"></a>Acts 11:8

But I [eipon](../../strongs/g/g2036.md), [mēdamōs](../../strongs/g/g3365.md), [kyrios](../../strongs/g/g2962.md): for nothing [koinos](../../strongs/g/g2839.md) or [akathartos](../../strongs/g/g169.md) hath at any time [eiserchomai](../../strongs/g/g1525.md) into my [stoma](../../strongs/g/g4750.md).

<a name="acts_11_9"></a>Acts 11:9

But the [phōnē](../../strongs/g/g5456.md) [apokrinomai](../../strongs/g/g611.md) me again from [ouranos](../../strongs/g/g3772.md), What [theos](../../strongs/g/g2316.md) hath [katharizō](../../strongs/g/g2511.md), [koinoō](../../strongs/g/g2840.md) not thou.

<a name="acts_11_10"></a>Acts 11:10

And this was [ginomai](../../strongs/g/g1096.md) three times: and all were [anaspaō](../../strongs/g/g385.md) again into [ouranos](../../strongs/g/g3772.md).

<a name="acts_11_11"></a>Acts 11:11

And, [idou](../../strongs/g/g2400.md), [exautēs](../../strongs/g/g1824.md) there were three [anēr](../../strongs/g/g435.md) already [ephistēmi](../../strongs/g/g2186.md) unto the [oikia](../../strongs/g/g3614.md) where I was, [apostellō](../../strongs/g/g649.md) from [Kaisareia](../../strongs/g/g2542.md) unto me.

<a name="acts_11_12"></a>Acts 11:12

And the [pneuma](../../strongs/g/g4151.md) [eipon](../../strongs/g/g2036.md) me [synerchomai](../../strongs/g/g4905.md) them, [mēdeis](../../strongs/g/g3367.md) [diakrinō](../../strongs/g/g1252.md). Moreover these six [adelphos](../../strongs/g/g80.md) [syn](../../strongs/g/g4862.md) [erchomai](../../strongs/g/g2064.md) me, and we [eiserchomai](../../strongs/g/g1525.md) into the [anēr](../../strongs/g/g435.md) [oikos](../../strongs/g/g3624.md):

<a name="acts_11_13"></a>Acts 11:13

And he [apaggellō](../../strongs/g/g518.md) us how he had [eidō](../../strongs/g/g1492.md) an [aggelos](../../strongs/g/g32.md) in his [oikos](../../strongs/g/g3624.md), which [histēmi](../../strongs/g/g2476.md) and [eipon](../../strongs/g/g2036.md) unto him, [apostellō](../../strongs/g/g649.md) [anēr](../../strongs/g/g435.md) to [Ioppē](../../strongs/g/g2445.md), and [metapempō](../../strongs/g/g3343.md) for [Simōn](../../strongs/g/g4613.md), whose [epikaleō](../../strongs/g/g1941.md) is [Petros](../../strongs/g/g4074.md);

<a name="acts_11_14"></a>Acts 11:14

Who shall [laleō](../../strongs/g/g2980.md) thee [rhēma](../../strongs/g/g4487.md), whereby thou and all thy [oikos](../../strongs/g/g3624.md) shall be [sōzō](../../strongs/g/g4982.md).

<a name="acts_11_15"></a>Acts 11:15

And as I [archomai](../../strongs/g/g756.md) to [laleō](../../strongs/g/g2980.md), the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [epipiptō](../../strongs/g/g1968.md) on them, as on us at [archē](../../strongs/g/g746.md).

<a name="acts_11_16"></a>Acts 11:16

Then [mnaomai](../../strongs/g/g3415.md) I the [rhēma](../../strongs/g/g4487.md) of the [kyrios](../../strongs/g/g2962.md), how that he [legō](../../strongs/g/g3004.md), [Iōannēs](../../strongs/g/g2491.md) indeed [baptizō](../../strongs/g/g907.md) with [hydōr](../../strongs/g/g5204.md); but ye shall be [baptizō](../../strongs/g/g907.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_11_17"></a>Acts 11:17

Forasmuch then as [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) them [isos](../../strongs/g/g2470.md) [dōrea](../../strongs/g/g1431.md) as unto us, who [pisteuō](../../strongs/g/g4100.md) on the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md); what was I, that I [dynatos](../../strongs/g/g1415.md) [kōlyō](../../strongs/g/g2967.md) [theos](../../strongs/g/g2316.md)?

<a name="acts_11_18"></a>Acts 11:18

When they [akouō](../../strongs/g/g191.md) these things, they [hēsychazō](../../strongs/g/g2270.md), and [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), Then hath [theos](../../strongs/g/g2316.md) [kai ge](../../strongs/g/g2534.md) to the [ethnos](../../strongs/g/g1484.md) [didōmi](../../strongs/g/g1325.md) [metanoia](../../strongs/g/g3341.md) unto [zōē](../../strongs/g/g2222.md).

<a name="acts_11_19"></a>Acts 11:19

Now [diaspeirō](../../strongs/g/g1289.md) upon the [thlipsis](../../strongs/g/g2347.md) that [ginomai](../../strongs/g/g1096.md) about [Stephanos](../../strongs/g/g4736.md) [dierchomai](../../strongs/g/g1330.md) as far as [Phoinikē](../../strongs/g/g5403.md), and [Kypros](../../strongs/g/g2954.md), and [Antiocheia](../../strongs/g/g490.md), [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) to [mēdeis](../../strongs/g/g3367.md) but unto the [Ioudaios](../../strongs/g/g2453.md) only.

<a name="acts_11_20"></a>Acts 11:20

And some of them were [anēr](../../strongs/g/g435.md) of [Kyprios](../../strongs/g/g2953.md) and [Kyrēnaios](../../strongs/g/g2956.md), which, when they were [eiserchomai](../../strongs/g/g1525.md) to [Antiocheia](../../strongs/g/g490.md), [laleō](../../strongs/g/g2980.md) unto the [Hellēnistēs](../../strongs/g/g1675.md), [euaggelizō](../../strongs/g/g2097.md) the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="acts_11_21"></a>Acts 11:21

And the [cheir](../../strongs/g/g5495.md) of the [kyrios](../../strongs/g/g2962.md) was with them: and a [polys](../../strongs/g/g4183.md) [arithmos](../../strongs/g/g706.md) [pisteuō](../../strongs/g/g4100.md), and [epistrephō](../../strongs/g/g1994.md) unto the [kyrios](../../strongs/g/g2962.md).

<a name="acts_11_22"></a>Acts 11:22

Then [logos](../../strongs/g/g3056.md) of these things [akouō](../../strongs/g/g191.md) unto the [ous](../../strongs/g/g3775.md) of the [ekklēsia](../../strongs/g/g1577.md) which was in [Hierosolyma](../../strongs/g/g2414.md): and they [exapostellō](../../strongs/g/g1821.md) [Barnabas](../../strongs/g/g921.md), that he should [dierchomai](../../strongs/g/g1330.md) as far as [Antiocheia](../../strongs/g/g490.md).

<a name="acts_11_23"></a>Acts 11:23

Who, when he [paraginomai](../../strongs/g/g3854.md), and had [eidō](../../strongs/g/g1492.md) the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md), was [chairō](../../strongs/g/g5463.md), and [parakaleō](../../strongs/g/g3870.md) them all, that with [prothesis](../../strongs/g/g4286.md) of [kardia](../../strongs/g/g2588.md) they would [prosmenō](../../strongs/g/g4357.md) the [kyrios](../../strongs/g/g2962.md).

<a name="acts_11_24"></a>Acts 11:24

For he was an [agathos](../../strongs/g/g18.md) [anēr](../../strongs/g/g435.md), and [plērēs](../../strongs/g/g4134.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) and of [pistis](../../strongs/g/g4102.md): and [hikanos](../../strongs/g/g2425.md) [ochlos](../../strongs/g/g3793.md) was [prostithēmi](../../strongs/g/g4369.md) unto the [kyrios](../../strongs/g/g2962.md).

<a name="acts_11_25"></a>Acts 11:25

Then [exerchomai](../../strongs/g/g1831.md) [Barnabas](../../strongs/g/g921.md) to [Tarsos](../../strongs/g/g5019.md), for to [anazēteō](../../strongs/g/g327.md) [Saulos](../../strongs/g/g4569.md):

<a name="acts_11_26"></a>Acts 11:26

And when he had [heuriskō](../../strongs/g/g2147.md) him, he [agō](../../strongs/g/g71.md) him unto [Antiocheia](../../strongs/g/g490.md). And [ginomai](../../strongs/g/g1096.md), that a [holos](../../strongs/g/g3650.md) [eniautos](../../strongs/g/g1763.md) they [synagō](../../strongs/g/g4863.md) with the [ekklēsia](../../strongs/g/g1577.md), and [didaskō](../../strongs/g/g1321.md) [hikanos](../../strongs/g/g2425.md) [ochlos](../../strongs/g/g3793.md). And the [mathētēs](../../strongs/g/g3101.md) were [chrēmatizō](../../strongs/g/g5537.md) [Christianos](../../strongs/g/g5546.md) first in [Antiocheia](../../strongs/g/g490.md).

<a name="acts_11_27"></a>Acts 11:27

And in these [hēmera](../../strongs/g/g2250.md) [katerchomai](../../strongs/g/g2718.md) [prophētēs](../../strongs/g/g4396.md) from [Hierosolyma](../../strongs/g/g2414.md) unto [Antiocheia](../../strongs/g/g490.md).

<a name="acts_11_28"></a>Acts 11:28

And there [anistēmi](../../strongs/g/g450.md) one of them [onoma](../../strongs/g/g3686.md) [Agabos](../../strongs/g/g13.md), and [sēmainō](../../strongs/g/g4591.md) by the [pneuma](../../strongs/g/g4151.md) that there should be [megas](../../strongs/g/g3173.md) [limos](../../strongs/g/g3042.md) throughout all the [oikoumenē](../../strongs/g/g3625.md): which [ginomai](../../strongs/g/g1096.md) in the days of [Klaudios](../../strongs/g/g2804.md) [Kaisar](../../strongs/g/g2541.md).

<a name="acts_11_29"></a>Acts 11:29

Then the [mathētēs](../../strongs/g/g3101.md), every [autos](../../strongs/g/g846.md) according to his [euporeō](../../strongs/g/g2141.md),[tis](../../strongs/g/g5100.md) [horizō](../../strongs/g/g3724.md) to [pempō](../../strongs/g/g3992.md) [diakonia](../../strongs/g/g1248.md) unto the [adelphos](../../strongs/g/g80.md) which [katoikeō](../../strongs/g/g2730.md) in [Ioudaia](../../strongs/g/g2449.md):

<a name="acts_11_30"></a>Acts 11:30

Which also they [poieō](../../strongs/g/g4160.md), and [apostellō](../../strongs/g/g649.md) it to the [presbyteros](../../strongs/g/g4245.md) by the [cheir](../../strongs/g/g5495.md) of [Barnabas](../../strongs/g/g921.md) and [Saulos](../../strongs/g/g4569.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 10](acts_10.md) - [Acts 12](acts_12.md)