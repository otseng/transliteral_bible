# [Acts 23](https://www.blueletterbible.org/kjv/act/23/1/s_1041001)

<a name="acts_23_1"></a>Acts 23:1

And [Paulos](../../strongs/g/g3972.md), [atenizō](../../strongs/g/g816.md) the [synedrion](../../strongs/g/g4892.md), [eipon](../../strongs/g/g2036.md), [anēr](../../strongs/g/g435.md) [adelphos](../../strongs/g/g80.md), I have [politeuomai](../../strongs/g/g4176.md) in all [agathos](../../strongs/g/g18.md) [syneidēsis](../../strongs/g/g4893.md) before [theos](../../strongs/g/g2316.md) until this day.

<a name="acts_23_2"></a>Acts 23:2

And the [archiereus](../../strongs/g/g749.md) [Hananias](../../strongs/g/g367.md) [epitassō](../../strongs/g/g2004.md) them that [paristēmi](../../strongs/g/g3936.md) him to [typtō](../../strongs/g/g5180.md) him on the [stoma](../../strongs/g/g4750.md).

<a name="acts_23_3"></a>Acts 23:3

Then [eipon](../../strongs/g/g2036.md) [Paulos](../../strongs/g/g3972.md) unto him, [theos](../../strongs/g/g2316.md) shall [typtō](../../strongs/g/g5180.md) thee, thou [koniaō](../../strongs/g/g2867.md) [toichos](../../strongs/g/g5109.md): for [kathēmai](../../strongs/g/g2521.md) thou to [krinō](../../strongs/g/g2919.md) me after the [nomos](../../strongs/g/g3551.md), and [keleuō](../../strongs/g/g2753.md) me to be [typtō](../../strongs/g/g5180.md) [paranomeō](../../strongs/g/g3891.md)?

<a name="acts_23_4"></a>Acts 23:4

And they that [paristēmi](../../strongs/g/g3936.md) [eipon](../../strongs/g/g2036.md), [loidoreō](../../strongs/g/g3058.md) thou [theos](../../strongs/g/g2316.md) [archiereus](../../strongs/g/g749.md)?

<a name="acts_23_5"></a>Acts 23:5

Then [phēmi](../../strongs/g/g5346.md) [Paulos](../../strongs/g/g3972.md), I [eidō](../../strongs/g/g1492.md) not, [adelphos](../../strongs/g/g80.md), that he was the [archiereus](../../strongs/g/g749.md): for it is [graphō](../../strongs/g/g1125.md), Thou shalt not [eipon](../../strongs/g/g2046.md) [kakōs](../../strongs/g/g2560.md) of the [archōn](../../strongs/g/g758.md) of thy [laos](../../strongs/g/g2992.md).

<a name="acts_23_6"></a>Acts 23:6

But when [Paulos](../../strongs/g/g3972.md) [ginōskō](../../strongs/g/g1097.md) that the one [meros](../../strongs/g/g3313.md) were [Saddoukaios](../../strongs/g/g4523.md), and the other [Pharisaios](../../strongs/g/g5330.md), he [krazō](../../strongs/g/g2896.md) in the [synedrion](../../strongs/g/g4892.md), [anēr](../../strongs/g/g435.md) [adelphos](../../strongs/g/g80.md), I am a [Pharisaios](../../strongs/g/g5330.md), the [huios](../../strongs/g/g5207.md) of a [Pharisaios](../../strongs/g/g5330.md): of the [elpis](../../strongs/g/g1680.md) and [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md) I am [krinō](../../strongs/g/g2919.md).

<a name="acts_23_7"></a>Acts 23:7

And when he had so [laleō](../../strongs/g/g2980.md), there [ginomai](../../strongs/g/g1096.md) a [stasis](../../strongs/g/g4714.md) between the [Pharisaios](../../strongs/g/g5330.md) and the [Saddoukaios](../../strongs/g/g4523.md): and the [plēthos](../../strongs/g/g4128.md) was [schizō](../../strongs/g/g4977.md).

<a name="acts_23_8"></a>Acts 23:8

For the [Saddoukaios](../../strongs/g/g4523.md) [legō](../../strongs/g/g3004.md) that there is no [anastasis](../../strongs/g/g386.md), neither [aggelos](../../strongs/g/g32.md), nor [pneuma](../../strongs/g/g4151.md): but the [Pharisaios](../../strongs/g/g5330.md) [homologeō](../../strongs/g/g3670.md) both.

<a name="acts_23_9"></a>Acts 23:9

And there [ginomai](../../strongs/g/g1096.md) a [megas](../../strongs/g/g3173.md) [kraugē](../../strongs/g/g2906.md): and the [grammateus](../../strongs/g/g1122.md) that were of the [Pharisaios](../../strongs/g/g5330.md) [meros](../../strongs/g/g3313.md) [anistēmi](../../strongs/g/g450.md), and [diamachomai](../../strongs/g/g1264.md), [legō](../../strongs/g/g3004.md), We [heuriskō](../../strongs/g/g2147.md) no [kakos](../../strongs/g/g2556.md) in this [anthrōpos](../../strongs/g/g444.md): but if a [pneuma](../../strongs/g/g4151.md) or an [aggelos](../../strongs/g/g32.md) hath [laleō](../../strongs/g/g2980.md) to him, let us not [theomacheō](../../strongs/g/g2313.md). [^1]

<a name="acts_23_10"></a>Acts 23:10

And when there [ginomai](../../strongs/g/g1096.md) a [polys](../../strongs/g/g4183.md) [stasis](../../strongs/g/g4714.md), the [chiliarchos](../../strongs/g/g5506.md), [eulabeomai](../../strongs/g/g2125.md) lest [Paulos](../../strongs/g/g3972.md) should have been [diaspaō](../../strongs/g/g1288.md) of them, [keleuō](../../strongs/g/g2753.md) the [strateuma](../../strongs/g/g4753.md) to [katabainō](../../strongs/g/g2597.md), and to [harpazō](../../strongs/g/g726.md) him from among them, and to [agō](../../strongs/g/g71.md) him into the [parembolē](../../strongs/g/g3925.md).

<a name="acts_23_11"></a>Acts 23:11

And the [nyx](../../strongs/g/g3571.md) [epeimi](../../strongs/g/g1966.md) the [kyrios](../../strongs/g/g2962.md) [ephistēmi](../../strongs/g/g2186.md) him, and [eipon](../../strongs/g/g2036.md), [tharseō](../../strongs/g/g2293.md), [Paulos](../../strongs/g/g3972.md): for as thou hast [diamartyromai](../../strongs/g/g1263.md) of me in [Ierousalēm](../../strongs/g/g2419.md), so must thou [martyreō](../../strongs/g/g3140.md) also at [Rhōmē](../../strongs/g/g4516.md).

<a name="acts_23_12"></a>Acts 23:12

And when it was [hēmera](../../strongs/g/g2250.md), certain of the [Ioudaios](../../strongs/g/g2453.md) [poieō](../../strongs/g/g4160.md) [systrophē](../../strongs/g/g4963.md), and [anathematizō](../../strongs/g/g332.md) themselves, [legō](../../strongs/g/g3004.md) that they would neither [phago](../../strongs/g/g5315.md) nor [pinō](../../strongs/g/g4095.md) till they had [apokteinō](../../strongs/g/g615.md) [Paulos](../../strongs/g/g3972.md).

<a name="acts_23_13"></a>Acts 23:13

And they were more than forty which had [poieō](../../strongs/g/g4160.md) this [synōmosia](../../strongs/g/g4945.md).

<a name="acts_23_14"></a>Acts 23:14

And they [proserchomai](../../strongs/g/g4334.md) to the [archiereus](../../strongs/g/g749.md) and [presbyteros](../../strongs/g/g4245.md), and [eipon](../../strongs/g/g2036.md), We have [anathematizō](../../strongs/g/g332.md) ourselves under an [anathema](../../strongs/g/g331.md), that we will [geuomai](../../strongs/g/g1089.md) [mēdeis](../../strongs/g/g3367.md) until we have [apokteinō](../../strongs/g/g615.md) [Paulos](../../strongs/g/g3972.md).

<a name="acts_23_15"></a>Acts 23:15

Now therefore ye with the [synedrion](../../strongs/g/g4892.md) [emphanizō](../../strongs/g/g1718.md) to the [chiliarchos](../../strongs/g/g5506.md) that he [katagō](../../strongs/g/g2609.md) him down unto you [aurion](../../strongs/g/g839.md), as though ye would [diaginōskō](../../strongs/g/g1231.md) [akribesteron](../../strongs/g/g197.md) concerning him: and we, or ever he [eggizō](../../strongs/g/g1448.md), are [hetoimos](../../strongs/g/g2092.md) to [anaireō](../../strongs/g/g337.md) him.

<a name="acts_23_16"></a>Acts 23:16

And when [Paulos](../../strongs/g/g3972.md) [adelphē](../../strongs/g/g79.md) [huios](../../strongs/g/g5207.md) [akouō](../../strongs/g/g191.md) of their [enedron](../../strongs/g/g1749.md), he [paraginomai](../../strongs/g/g3854.md) and [eiserchomai](../../strongs/g/g1525.md) into the [parembolē](../../strongs/g/g3925.md), and [apaggellō](../../strongs/g/g518.md) [Paulos](../../strongs/g/g3972.md).

<a name="acts_23_17"></a>Acts 23:17

Then [Paulos](../../strongs/g/g3972.md) [proskaleō](../../strongs/g/g4341.md) one of the [hekatontarchēs](../../strongs/g/g1543.md) unto him, and [phēmi](../../strongs/g/g5346.md), [apagō](../../strongs/g/g520.md) this [neanias](../../strongs/g/g3494.md) unto the [chiliarchos](../../strongs/g/g5506.md): for he hath a certain thing to [apaggellō](../../strongs/g/g518.md) him.

<a name="acts_23_18"></a>Acts 23:18

So he [paralambanō](../../strongs/g/g3880.md) him, and [agō](../../strongs/g/g71.md) to the [chiliarchos](../../strongs/g/g5506.md), and [phēmi](../../strongs/g/g5346.md), [Paulos](../../strongs/g/g3972.md) the [desmios](../../strongs/g/g1198.md) [proskaleō](../../strongs/g/g4341.md) me unto him, and [erōtaō](../../strongs/g/g2065.md) me to [agō](../../strongs/g/g71.md) this [neanias](../../strongs/g/g3494.md) unto thee, who hath something to [laleō](../../strongs/g/g2980.md) unto thee.

<a name="acts_23_19"></a>Acts 23:19

Then the [chiliarchos](../../strongs/g/g5506.md) [epilambanomai](../../strongs/g/g1949.md) him by the [cheir](../../strongs/g/g5495.md), and [anachōreō](../../strongs/g/g402.md) [idios](../../strongs/g/g2398.md), and [pynthanomai](../../strongs/g/g4441.md) him, What is that thou hast to [apaggellō](../../strongs/g/g518.md) me?

<a name="acts_23_20"></a>Acts 23:20

And he [eipon](../../strongs/g/g2036.md), The [Ioudaios](../../strongs/g/g2453.md) have [syntithēmi](../../strongs/g/g4934.md) to [erōtaō](../../strongs/g/g2065.md) thee that thou wouldest [katagō](../../strongs/g/g2609.md) [Paulos](../../strongs/g/g3972.md) [aurion](../../strongs/g/g839.md) into the [synedrion](../../strongs/g/g4892.md), as though they would [pynthanomai](../../strongs/g/g4441.md) somewhat of him [akribesteron](../../strongs/g/g197.md).

<a name="acts_23_21"></a>Acts 23:21

But do not thou [peithō](../../strongs/g/g3982.md) unto them: for there [enedreuō](../../strongs/g/g1748.md) him of them more than forty [anēr](../../strongs/g/g435.md), which have [anathematizō](../../strongs/g/g332.md) themselves, that they will neither [phago](../../strongs/g/g5315.md) nor [pinō](../../strongs/g/g4095.md) till they have [anaireō](../../strongs/g/g337.md) him: and now are they [hetoimos](../../strongs/g/g2092.md), [prosdechomai](../../strongs/g/g4327.md) for an [epaggelia](../../strongs/g/g1860.md) from thee.

<a name="acts_23_22"></a>Acts 23:22

So the [chiliarchos](../../strongs/g/g5506.md) let the [neanias](../../strongs/g/g3494.md) [apolyō](../../strongs/g/g630.md), and [paraggellō](../../strongs/g/g3853.md), [eklaleō](../../strongs/g/g1583.md) [mēdeis](../../strongs/g/g3367.md) that thou hast [emphanizō](../../strongs/g/g1718.md) these things to me.

<a name="acts_23_23"></a>Acts 23:23

And he [proskaleō](../../strongs/g/g4341.md) unto two [hekatontarchēs](../../strongs/g/g1543.md), [eipon](../../strongs/g/g2036.md), [hetoimazō](../../strongs/g/g2090.md) two hundred [stratiōtēs](../../strongs/g/g4757.md) to [poreuō](../../strongs/g/g4198.md) to [Kaisareia](../../strongs/g/g2542.md), and [hippeus](../../strongs/g/g2460.md) threescore and ten, and [dexiolabos](../../strongs/g/g1187.md) two hundred, at the third [hōra](../../strongs/g/g5610.md) of the [nyx](../../strongs/g/g3571.md);

<a name="acts_23_24"></a>Acts 23:24

And [paristēmi](../../strongs/g/g3936.md) [ktēnos](../../strongs/g/g2934.md), that they may [epibibazō](../../strongs/g/g1913.md) [Paulos](../../strongs/g/g3972.md) on, and [diasōzō](../../strongs/g/g1295.md) unto [Phēlix](../../strongs/g/g5344.md) the [hēgemōn](../../strongs/g/g2232.md).

<a name="acts_23_25"></a>Acts 23:25

And he [graphō](../../strongs/g/g1125.md) an [epistolē](../../strongs/g/g1992.md) [periechō](../../strongs/g/g4023.md) this [typos](../../strongs/g/g5179.md):

<a name="acts_23_26"></a>Acts 23:26

[Klaudios](../../strongs/g/g2804.md) [Lysias](../../strongs/g/g3079.md) unto [kratistos](../../strongs/g/g2903.md) [hēgemōn](../../strongs/g/g2232.md) [Phēlix](../../strongs/g/g5344.md) [chairō](../../strongs/g/g5463.md).

<a name="acts_23_27"></a>Acts 23:27

This [anēr](../../strongs/g/g435.md) was [syllambanō](../../strongs/g/g4815.md) of the [Ioudaios](../../strongs/g/g2453.md), and should have been [anaireō](../../strongs/g/g337.md) of them: then [ephistēmi](../../strongs/g/g2186.md) I with a [strateuma](../../strongs/g/g4753.md), and [exaireō](../../strongs/g/g1807.md) him, having [manthanō](../../strongs/g/g3129.md) that he was a [Rhōmaios](../../strongs/g/g4514.md).

<a name="acts_23_28"></a>Acts 23:28

And when I [boulomai](../../strongs/g/g1014.md) [ginōskō](../../strongs/g/g1097.md) the [aitia](../../strongs/g/g156.md) wherefore they [egkaleō](../../strongs/g/g1458.md) him, I [katagō](../../strongs/g/g2609.md) him forth into their [synedrion](../../strongs/g/g4892.md):

<a name="acts_23_29"></a>Acts 23:29

Whom I [heuriskō](../../strongs/g/g2147.md) to be [egkaleō](../../strongs/g/g1458.md) of [zētēma](../../strongs/g/g2213.md) of their [nomos](../../strongs/g/g3551.md), but to have [mēdeis](../../strongs/g/g3367.md) to [egklēma](../../strongs/g/g1462.md) [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md) or [desmos](../../strongs/g/g1199.md).

<a name="acts_23_30"></a>Acts 23:30

And when it was [mēnyō](../../strongs/g/g3377.md) me how that the [Ioudaios](../../strongs/g/g2453.md) [epiboulē](../../strongs/g/g1917.md) [mellō](../../strongs/g/g3195.md) for the [anēr](../../strongs/g/g435.md), I [pempō](../../strongs/g/g3992.md) [exautēs](../../strongs/g/g1824.md) to thee, and [paraggellō](../../strongs/g/g3853.md) to his [katēgoros](../../strongs/g/g2725.md) also to [legō](../../strongs/g/g3004.md) before thee what they had against him. [rhōnnymi](../../strongs/g/g4517.md).

<a name="acts_23_31"></a>Acts 23:31

Then the [stratiōtēs](../../strongs/g/g4757.md), as it was [diatassō](../../strongs/g/g1299.md) them, [analambanō](../../strongs/g/g353.md) [Paulos](../../strongs/g/g3972.md), and [agō](../../strongs/g/g71.md) him by [nyx](../../strongs/g/g3571.md) to [Antipatris](../../strongs/g/g494.md).

<a name="acts_23_32"></a>Acts 23:32

On [epaurion](../../strongs/g/g1887.md) they [eaō](../../strongs/g/g1439.md) the [hippeus](../../strongs/g/g2460.md) to [poreuō](../../strongs/g/g4198.md) with him, and [hypostrephō](../../strongs/g/g5290.md) to the [parembolē](../../strongs/g/g3925.md):

<a name="acts_23_33"></a>Acts 23:33

Who, when they [eiserchomai](../../strongs/g/g1525.md) to [Kaisareia](../../strongs/g/g2542.md) and [anadidōmi](../../strongs/g/g325.md) the [epistolē](../../strongs/g/g1992.md) to the [hēgemōn](../../strongs/g/g2232.md), [paristēmi](../../strongs/g/g3936.md) [Paulos](../../strongs/g/g3972.md) also before him.

<a name="acts_23_34"></a>Acts 23:34

And when the [hēgemōn](../../strongs/g/g2232.md) had [anaginōskō](../../strongs/g/g314.md), he [eperōtaō](../../strongs/g/g1905.md) of what [eparcheia](../../strongs/g/g1885.md) he was. And when he [pynthanomai](../../strongs/g/g4441.md) that he was of [Kilikia](../../strongs/g/g2791.md);

<a name="acts_23_35"></a>Acts 23:35

I will [diakouō](../../strongs/g/g1251.md) thee, [phēmi](../../strongs/g/g5346.md) he, when thine [katēgoros](../../strongs/g/g2725.md) are also [paraginomai](../../strongs/g/g3854.md). And he [keleuō](../../strongs/g/g2753.md) him to be [phylassō](../../strongs/g/g5442.md) in [Hērōdēs](../../strongs/g/g2264.md) [praitōrion](../../strongs/g/g4232.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 22](acts_22.md) - [Acts 24](acts_24.md)

---

[^1]: [Acts 23:9 Commentary](../../commentary/acts/acts_23_commentary.md#acts_23_9)
