# [Acts 3](https://www.blueletterbible.org/kjv/act/3/1/s_1021001)

<a name="acts_3_1"></a>Acts 3:1

Now [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md) [anabainō](../../strongs/g/g305.md) together into the [hieron](../../strongs/g/g2411.md) at the [hōra](../../strongs/g/g5610.md) of [proseuchē](../../strongs/g/g4335.md), the ninth. [^1]

<a name="acts_3_2"></a>Acts 3:2

And a certain [anēr](../../strongs/g/g435.md) [hyparchō](../../strongs/g/g5225.md) [chōlos](../../strongs/g/g5560.md) from his [mētēr](../../strongs/g/g3384.md) [koilia](../../strongs/g/g2836.md) was [bastazō](../../strongs/g/g941.md), whom they [tithēmi](../../strongs/g/g5087.md) daily at the [thyra](../../strongs/g/g2374.md) of the [hieron](../../strongs/g/g2411.md) which is [legō](../../strongs/g/g3004.md) [hōraios](../../strongs/g/g5611.md), to [aiteō](../../strongs/g/g154.md) [eleēmosynē](../../strongs/g/g1654.md) of them that [eisporeuomai](../../strongs/g/g1531.md) into the [hieron](../../strongs/g/g2411.md);

<a name="acts_3_3"></a>Acts 3:3

Who [eidō](../../strongs/g/g1492.md) [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md) about to [eiseimi](../../strongs/g/g1524.md) into the [hieron](../../strongs/g/g2411.md) [erōtaō](../../strongs/g/g2065.md) [lambanō](../../strongs/g/g2983.md) [eleēmosynē](../../strongs/g/g1654.md).

<a name="acts_3_4"></a>Acts 3:4

And [Petros](../../strongs/g/g4074.md), [atenizō](../../strongs/g/g816.md) upon him with [Iōannēs](../../strongs/g/g2491.md), [eipon](../../strongs/g/g2036.md), [blepō](../../strongs/g/g991.md) on us.

<a name="acts_3_5"></a>Acts 3:5

And he [epechō](../../strongs/g/g1907.md) unto them, [prosdokaō](../../strongs/g/g4328.md) to [lambanō](../../strongs/g/g2983.md) something of them.

<a name="acts_3_6"></a>Acts 3:6

Then [Petros](../../strongs/g/g4074.md) [eipon](../../strongs/g/g2036.md), [argyrion](../../strongs/g/g694.md) and [chrysion](../../strongs/g/g5553.md) have I none; but such as I have [didōmi](../../strongs/g/g1325.md) I thee: In the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) of [Nazōraios](../../strongs/g/g3480.md) [egeirō](../../strongs/g/g1453.md) and [peripateō](../../strongs/g/g4043.md).

<a name="acts_3_7"></a>Acts 3:7

And he [piazō](../../strongs/g/g4084.md) him by the [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md), and [egeirō](../../strongs/g/g1453.md) him: and [parachrēma](../../strongs/g/g3916.md) his [basis](../../strongs/g/g939.md) and [sphydron](../../strongs/g/g4974.md) [stereoō](../../strongs/g/g4732.md).

<a name="acts_3_8"></a>Acts 3:8

And he [exallomai](../../strongs/g/g1814.md) [histēmi](../../strongs/g/g2476.md), and [peripateō](../../strongs/g/g4043.md), and [eiserchomai](../../strongs/g/g1525.md) with them into the [hieron](../../strongs/g/g2411.md), [peripateō](../../strongs/g/g4043.md), and [hallomai](../../strongs/g/g242.md), and [aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md).

<a name="acts_3_9"></a>Acts 3:9

And all the [laos](../../strongs/g/g2992.md) [eidō](../../strongs/g/g1492.md) him [peripateō](../../strongs/g/g4043.md) and [aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md):

<a name="acts_3_10"></a>Acts 3:10

And they [epiginōskō](../../strongs/g/g1921.md) that it was he which [kathēmai](../../strongs/g/g2521.md) for [eleēmosynē](../../strongs/g/g1654.md) at the [hōraios](../../strongs/g/g5611.md) [pylē](../../strongs/g/g4439.md) of the [hieron](../../strongs/g/g2411.md): and they were [pimplēmi](../../strongs/g/g4130.md) with [thambos](../../strongs/g/g2285.md) and [ekstasis](../../strongs/g/g1611.md) at that which had [symbainō](../../strongs/g/g4819.md) unto him.

<a name="acts_3_11"></a>Acts 3:11

And as the [chōlos](../../strongs/g/g5560.md) which was [iaomai](../../strongs/g/g2390.md) [krateō](../../strongs/g/g2902.md) [Petros](../../strongs/g/g4074.md) and [Iōannēs](../../strongs/g/g2491.md), all the [laos](../../strongs/g/g2992.md) [syntrechō](../../strongs/g/g4936.md) unto them in the [stoa](../../strongs/g/g4745.md) that is [kaleō](../../strongs/g/g2564.md) [Solomōn](../../strongs/g/g4672.md) [ekthambos](../../strongs/g/g1569.md).

<a name="acts_3_12"></a>Acts 3:12

And when [Petros](../../strongs/g/g4074.md) [eidō](../../strongs/g/g1492.md), he [apokrinomai](../../strongs/g/g611.md) unto the [laos](../../strongs/g/g2992.md), Ye [anēr](../../strongs/g/g435.md) [Israēlitēs](../../strongs/g/g2475.md), why [thaumazō](../../strongs/g/g2296.md) ye at this? or why [atenizō](../../strongs/g/g816.md) on us, as though by our own [dynamis](../../strongs/g/g1411.md) or [eusebeia](../../strongs/g/g2150.md) we had [poieō](../../strongs/g/g4160.md) [autos](../../strongs/g/g846.md) to [peripateō](../../strongs/g/g4043.md)?

<a name="acts_3_13"></a>Acts 3:13

The [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md), and of [Isaak](../../strongs/g/g2464.md), and of [Iakōb](../../strongs/g/g2384.md), the [theos](../../strongs/g/g2316.md) of our [patēr](../../strongs/g/g3962.md), hath [doxazō](../../strongs/g/g1392.md) his [pais](../../strongs/g/g3816.md) [Iēsous](../../strongs/g/g2424.md); whom ye [paradidōmi](../../strongs/g/g3860.md), and [arneomai](../../strongs/g/g720.md) him in the [prosōpon](../../strongs/g/g4383.md) of [Pilatos](../../strongs/g/g4091.md), when he [krinō](../../strongs/g/g2919.md) to [apolyō](../../strongs/g/g630.md) him.

<a name="acts_3_14"></a>Acts 3:14

But ye [arneomai](../../strongs/g/g720.md) [hagios](../../strongs/g/g40.md) and [dikaios](../../strongs/g/g1342.md), and [aiteō](../../strongs/g/g154.md) [phoneus](../../strongs/g/g5406.md) [anēr](../../strongs/g/g435.md) to be [charizomai](../../strongs/g/g5483.md) unto you;

<a name="acts_3_15"></a>Acts 3:15

And [apokteinō](../../strongs/g/g615.md) the [archēgos](../../strongs/g/g747.md) of [zōē](../../strongs/g/g2222.md), whom [theos](../../strongs/g/g2316.md) hath [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md); whereof we are [martys](../../strongs/g/g3144.md).

<a name="acts_3_16"></a>Acts 3:16

And his [onoma](../../strongs/g/g3686.md) through [pistis](../../strongs/g/g4102.md) in his [onoma](../../strongs/g/g3686.md) hath [stereoō](../../strongs/g/g4732.md) [tautē](../../strongs/g/g5026.md), whom ye [theōreō](../../strongs/g/g2334.md) and [eidō](../../strongs/g/g1492.md): [kai](../../strongs/g/g2532.md), the [pistis](../../strongs/g/g4102.md) which is by him hath [didōmi](../../strongs/g/g1325.md) him this [holoklēria](../../strongs/g/g3647.md) in the [apenanti](../../strongs/g/g561.md) of you all.

<a name="acts_3_17"></a>Acts 3:17

And now, [adelphos](../../strongs/g/g80.md), I [eidō](../../strongs/g/g1492.md) that through [agnoia](../../strongs/g/g52.md) ye [prassō](../../strongs/g/g4238.md), as also your [archōn](../../strongs/g/g758.md).

<a name="acts_3_18"></a>Acts 3:18

But those things, which [theos](../../strongs/g/g2316.md) before had [prokataggellō](../../strongs/g/g4293.md) by the [stoma](../../strongs/g/g4750.md) of all his [prophētēs](../../strongs/g/g4396.md), that [Christos](../../strongs/g/g5547.md) should [paschō](../../strongs/g/g3958.md), he hath so [plēroō](../../strongs/g/g4137.md).

<a name="acts_3_19"></a>Acts 3:19

[metanoeō](../../strongs/g/g3340.md) ye therefore, and be [epistrephō](../../strongs/g/g1994.md), that your [hamartia](../../strongs/g/g266.md) may be [exaleiphō](../../strongs/g/g1813.md), when the [kairos](../../strongs/g/g2540.md) of [anapsyxis](../../strongs/g/g403.md) shall [erchomai](../../strongs/g/g2064.md) from the [prosōpon](../../strongs/g/g4383.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="acts_3_20"></a>Acts 3:20

And he shall [apostellō](../../strongs/g/g649.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), which before was [prokēryssō](../../strongs/g/g4296.md) unto you:

<a name="acts_3_21"></a>Acts 3:21

Whom the [ouranos](../../strongs/g/g3772.md) must [dechomai](../../strongs/g/g1209.md) until the [chronos](../../strongs/g/g5550.md) of [apokatastasis](../../strongs/g/g605.md) of all things, which [theos](../../strongs/g/g2316.md) hath [laleō](../../strongs/g/g2980.md) by the [stoma](../../strongs/g/g4750.md) of all his [hagios](../../strongs/g/g40.md) [prophētēs](../../strongs/g/g4396.md) since [aiōn](../../strongs/g/g165.md).

<a name="acts_3_22"></a>Acts 3:22

For [Mōÿsēs](../../strongs/g/g3475.md) [men](../../strongs/g/g3303.md) [eipon](../../strongs/g/g2036.md) unto the [patēr](../../strongs/g/g3962.md), A [prophētēs](../../strongs/g/g4396.md) shall the [kyrios](../../strongs/g/g2962.md) your [theos](../../strongs/g/g2316.md) [anistēmi](../../strongs/g/g450.md) unto you of your [adelphos](../../strongs/g/g80.md), like unto me; him shall ye [akouō](../../strongs/g/g191.md) in all things whatsoever he shall [laleō](../../strongs/g/g2980.md) unto you.

<a name="acts_3_23"></a>Acts 3:23

And [esomai](../../strongs/g/g2071.md), that every [psychē](../../strongs/g/g5590.md), which will not [akouō](../../strongs/g/g191.md) that [prophētēs](../../strongs/g/g4396.md), shall be [exolethreuō](../../strongs/g/g1842.md) from among the [laos](../../strongs/g/g2992.md).

<a name="acts_3_24"></a>Acts 3:24

[kai](../../strongs/g/g2532.md), and all the [prophētēs](../../strongs/g/g4396.md) from [Samouēl](../../strongs/g/g4545.md) and [kathexēs](../../strongs/g/g2517.md), as many as have [laleō](../../strongs/g/g2980.md), have likewise [prokataggellō](../../strongs/g/g4293.md) of these [hēmera](../../strongs/g/g2250.md).

<a name="acts_3_25"></a>Acts 3:25

Ye are the [huios](../../strongs/g/g5207.md) of the [prophētēs](../../strongs/g/g4396.md), and of the [diathēkē](../../strongs/g/g1242.md) which [theos](../../strongs/g/g2316.md) [diatithēmi](../../strongs/g/g1303.md) with our [patēr](../../strongs/g/g3962.md), [legō](../../strongs/g/g3004.md) unto [Abraam](../../strongs/g/g11.md), And in thy [sperma](../../strongs/g/g4690.md) shall all the [patria](../../strongs/g/g3965.md) of [gē](../../strongs/g/g1093.md) be [eneulogeō](../../strongs/g/g1757.md).

<a name="acts_3_26"></a>Acts 3:26

Unto you first [theos](../../strongs/g/g2316.md), having [anistēmi](../../strongs/g/g450.md) his [pais](../../strongs/g/g3816.md) [Iēsous](../../strongs/g/g2424.md), [apostellō](../../strongs/g/g649.md) him to [eulogeō](../../strongs/g/g2127.md) you, in [apostrephō](../../strongs/g/g654.md) [hekastos](../../strongs/g/g1538.md) of you from [ponēria](../../strongs/g/g4189.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 2](acts_2.md) - [Acts 4](acts_4.md)

---

[^1]: [Acts 3:1 Commentary](../../commentary/acts/acts_3_commentary.md#acts_3_1)
