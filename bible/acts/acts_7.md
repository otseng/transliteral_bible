# [Acts 7](https://www.blueletterbible.org/kjv/act/7/1/s_1025001)

<a name="acts_7_1"></a>Acts 7:1

Then [eipon](../../strongs/g/g2036.md) the [archiereus](../../strongs/g/g749.md), Are these things so?

<a name="acts_7_2"></a>Acts 7:2

And he [phēmi](../../strongs/g/g5346.md), [anēr](../../strongs/g/g435.md), [adelphos](../../strongs/g/g80.md), and [patēr](../../strongs/g/g3962.md), [akouō](../../strongs/g/g191.md); The [theos](../../strongs/g/g2316.md) of [doxa](../../strongs/g/g1391.md) [optanomai](../../strongs/g/g3700.md) unto our [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md), when he was in [Mesopotamia](../../strongs/g/g3318.md), before he [katoikeō](../../strongs/g/g2730.md) in [Charran](../../strongs/g/g5488.md),

<a name="acts_7_3"></a>Acts 7:3

And [eipon](../../strongs/g/g2036.md) unto him, [exerchomai](../../strongs/g/g1831.md) thee out of thy [gē](../../strongs/g/g1093.md), and from thy [syggeneia](../../strongs/g/g4772.md), and [deuro](../../strongs/g/g1204.md) into the [gē](../../strongs/g/g1093.md) which I shall [deiknyō](../../strongs/g/g1166.md) thee.

<a name="acts_7_4"></a>Acts 7:4

Then [exerchomai](../../strongs/g/g1831.md) he out of the [gē](../../strongs/g/g1093.md) of the [Chaldaios](../../strongs/g/g5466.md), and [katoikeō](../../strongs/g/g2730.md) in [Charran](../../strongs/g/g5488.md): [kakeithen](../../strongs/g/g2547.md), when his [patēr](../../strongs/g/g3962.md) was [apothnēskō](../../strongs/g/g599.md), he [metoikizō](../../strongs/g/g3351.md) him into this [gē](../../strongs/g/g1093.md), wherein ye now [katoikeō](../../strongs/g/g2730.md).

<a name="acts_7_5"></a>Acts 7:5

And he [didōmi](../../strongs/g/g1325.md) him none [klēronomia](../../strongs/g/g2817.md) in it, no, not to [bēma](../../strongs/g/g968.md) his [pous](../../strongs/g/g4228.md) on: yet he [epaggellomai](../../strongs/g/g1861.md) that he would [didōmi](../../strongs/g/g1325.md) it to him for a [kataschesis](../../strongs/g/g2697.md), and to his [sperma](../../strongs/g/g4690.md) after him, when as yet he had no [teknon](../../strongs/g/g5043.md).

<a name="acts_7_6"></a>Acts 7:6

And [theos](../../strongs/g/g2316.md) [laleō](../../strongs/g/g2980.md) on this wise, That his [sperma](../../strongs/g/g4690.md) should [paroikos](../../strongs/g/g3941.md) in an [allotrios](../../strongs/g/g245.md) [gē](../../strongs/g/g1093.md); and that they should [douloō](../../strongs/g/g1402.md) them, and [kakoō](../../strongs/g/g2559.md) four hundred [etos](../../strongs/g/g2094.md).

<a name="acts_7_7"></a>Acts 7:7

And the [ethnos](../../strongs/g/g1484.md) to whom they shall [douleuō](../../strongs/g/g1398.md) will I [krinō](../../strongs/g/g2919.md), [eipon](../../strongs/g/g2036.md) [theos](../../strongs/g/g2316.md): and after that shall they [exerchomai](../../strongs/g/g1831.md), and [latreuō](../../strongs/g/g3000.md) me in this [topos](../../strongs/g/g5117.md).

<a name="acts_7_8"></a>Acts 7:8

And he [didōmi](../../strongs/g/g1325.md) him the [diathēkē](../../strongs/g/g1242.md) of [peritomē](../../strongs/g/g4061.md): and so [gennaō](../../strongs/g/g1080.md) [Isaak](../../strongs/g/g2464.md), and [peritemnō](../../strongs/g/g4059.md) him the eighth [hēmera](../../strongs/g/g2250.md); and [Isaak](../../strongs/g/g2464.md) [Iakōb](../../strongs/g/g2384.md); and [Iakōb](../../strongs/g/g2384.md) the twelve [patriarchēs](../../strongs/g/g3966.md).

<a name="acts_7_9"></a>Acts 7:9

And the [patriarchēs](../../strongs/g/g3966.md), [zēloō](../../strongs/g/g2206.md), [apodidōmi](../../strongs/g/g591.md) [Iōsēph](../../strongs/g/g2501.md) into [Aigyptos](../../strongs/g/g125.md): but [theos](../../strongs/g/g2316.md) was with him,

<a name="acts_7_10"></a>Acts 7:10

And [exaireō](../../strongs/g/g1807.md) him out of all his [thlipsis](../../strongs/g/g2347.md), and [didōmi](../../strongs/g/g1325.md) him [charis](../../strongs/g/g5485.md) and [sophia](../../strongs/g/g4678.md) in the [enantion](../../strongs/g/g1726.md) of [Pharaō](../../strongs/g/g5328.md) [basileus](../../strongs/g/g935.md) of [Aigyptos](../../strongs/g/g125.md); and he [kathistēmi](../../strongs/g/g2525.md) him [hēgeomai](../../strongs/g/g2233.md) over [Aigyptos](../../strongs/g/g125.md) and all his [oikos](../../strongs/g/g3624.md).

<a name="acts_7_11"></a>Acts 7:11

Now there [erchomai](../../strongs/g/g2064.md) a [limos](../../strongs/g/g3042.md) over all the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md) and [Chanaan](../../strongs/g/g5477.md), and [megas](../../strongs/g/g3173.md) [thlipsis](../../strongs/g/g2347.md): and our [patēr](../../strongs/g/g3962.md) [heuriskō](../../strongs/g/g2147.md) no [chortasma](../../strongs/g/g5527.md).

<a name="acts_7_12"></a>Acts 7:12

But when [Iakōb](../../strongs/g/g2384.md) [akouō](../../strongs/g/g191.md) that there was [sitos](../../strongs/g/g4621.md) in [Aigyptos](../../strongs/g/g125.md), he [exapostellō](../../strongs/g/g1821.md) our [patēr](../../strongs/g/g3962.md) first.

<a name="acts_7_13"></a>Acts 7:13

And at the second [Iōsēph](../../strongs/g/g2501.md) was [anagnōrizō](../../strongs/g/g319.md) to his [adelphos](../../strongs/g/g80.md); and [Iōsēph](../../strongs/g/g2501.md) [genos](../../strongs/g/g1085.md) was made [phaneros](../../strongs/g/g5318.md) unto [Pharaō](../../strongs/g/g5328.md).

<a name="acts_7_14"></a>Acts 7:14

Then [apostellō](../../strongs/g/g649.md) [Iōsēph](../../strongs/g/g2501.md), and [metakaleō](../../strongs/g/g3333.md) his [patēr](../../strongs/g/g3962.md) [Iakōb](../../strongs/g/g2384.md), and all his [syggeneia](../../strongs/g/g4772.md), threescore and fifteen [psychē](../../strongs/g/g5590.md).

<a name="acts_7_15"></a>Acts 7:15

So [Iakōb](../../strongs/g/g2384.md) [katabainō](../../strongs/g/g2597.md) into [Aigyptos](../../strongs/g/g125.md), and [teleutaō](../../strongs/g/g5053.md), he, and our [patēr](../../strongs/g/g3962.md),

<a name="acts_7_16"></a>Acts 7:16

And were [metatithēmi](../../strongs/g/g3346.md) into [Sychem](../../strongs/g/g4966.md), and [tithēmi](../../strongs/g/g5087.md) in the [mnēma](../../strongs/g/g3418.md) that [Abraam](../../strongs/g/g11.md) [ōneomai](../../strongs/g/g5608.md) for a [timē](../../strongs/g/g5092.md) of [argyrion](../../strongs/g/g694.md) of the [huios](../../strongs/g/g5207.md) of [Emmor](../../strongs/g/g1697.md) of [Sychem](../../strongs/g/g4966.md).

<a name="acts_7_17"></a>Acts 7:17

But when the [chronos](../../strongs/g/g5550.md) of the [epaggelia](../../strongs/g/g1860.md) [eggizō](../../strongs/g/g1448.md), which [theos](../../strongs/g/g2316.md) had [omnyō](../../strongs/g/g3660.md) to [Abraam](../../strongs/g/g11.md), the [laos](../../strongs/g/g2992.md) [auxanō](../../strongs/g/g837.md) and [plēthynō](../../strongs/g/g4129.md) in [Aigyptos](../../strongs/g/g125.md),

<a name="acts_7_18"></a>Acts 7:18

Till another [basileus](../../strongs/g/g935.md) [anistēmi](../../strongs/g/g450.md), which [eidō](../../strongs/g/g1492.md) not [Iōsēph](../../strongs/g/g2501.md).

<a name="acts_7_19"></a>Acts 7:19

The same [katasophizomai](../../strongs/g/g2686.md) with our [genos](../../strongs/g/g1085.md), and [kakoō](../../strongs/g/g2559.md) our [patēr](../../strongs/g/g3962.md), [poieō](../../strongs/g/g4160.md) they [ekthetos](../../strongs/g/g1570.md) their [brephos](../../strongs/g/g1025.md), to the end they might not [zōogoneō](../../strongs/g/g2225.md).

<a name="acts_7_20"></a>Acts 7:20

In which [kairos](../../strongs/g/g2540.md) [Mōÿsēs](../../strongs/g/g3475.md) was [gennaō](../../strongs/g/g1080.md), and was [theos](../../strongs/g/g2316.md) [asteios](../../strongs/g/g791.md), and [anatrephō](../../strongs/g/g397.md) in his [patēr](../../strongs/g/g3962.md) [oikos](../../strongs/g/g3624.md) three [mēn](../../strongs/g/g3376.md):

<a name="acts_7_21"></a>Acts 7:21

And when he was [ektithēmi](../../strongs/g/g1620.md), [Pharaō](../../strongs/g/g5328.md) [thygatēr](../../strongs/g/g2364.md) [anaireō](../../strongs/g/g337.md) him, and [anatrephō](../../strongs/g/g397.md) him for her own [huios](../../strongs/g/g5207.md).

<a name="acts_7_22"></a>Acts 7:22

And [Mōÿsēs](../../strongs/g/g3475.md) was [paideuō](../../strongs/g/g3811.md) in all the [sophia](../../strongs/g/g4678.md) of the [Aigyptios](../../strongs/g/g124.md), and was [dynatos](../../strongs/g/g1415.md) in [logos](../../strongs/g/g3056.md) and in [ergon](../../strongs/g/g2041.md).

<a name="acts_7_23"></a>Acts 7:23

And when he was [plēroō](../../strongs/g/g4137.md) forty years [chronos](../../strongs/g/g5550.md), it [anabainō](../../strongs/g/g305.md) into his [kardia](../../strongs/g/g2588.md) to [episkeptomai](../../strongs/g/g1980.md) his [adelphos](../../strongs/g/g80.md) the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md).

<a name="acts_7_24"></a>Acts 7:24

And [eidō](../../strongs/g/g1492.md) one [adikeō](../../strongs/g/g91.md), he [amynomai](../../strongs/g/g292.md) him, and [ekdikēsis](../../strongs/g/g1557.md) [poieō](../../strongs/g/g4160.md) him that was [kataponeō](../../strongs/g/g2669.md), and [patassō](../../strongs/g/g3960.md) the [Aigyptios](../../strongs/g/g124.md):

<a name="acts_7_25"></a>Acts 7:25

For he [nomizō](../../strongs/g/g3543.md) his [adelphos](../../strongs/g/g80.md) would have [syniēmi](../../strongs/g/g4920.md) how that [theos](../../strongs/g/g2316.md) by his [cheir](../../strongs/g/g5495.md) would [didōmi](../../strongs/g/g1325.md) [sōtēria](../../strongs/g/g4991.md) them: but they [syniēmi](../../strongs/g/g4920.md) not.

<a name="acts_7_26"></a>Acts 7:26

And the [epeimi](../../strongs/g/g1966.md) day he [optanomai](../../strongs/g/g3700.md) unto them as they [machomai](../../strongs/g/g3164.md), and would have [synallassō](../../strongs/g/g4900.md) them at [eirēnē](../../strongs/g/g1515.md), [eipon](../../strongs/g/g2036.md), [anēr](../../strongs/g/g435.md), ye are [adelphos](../../strongs/g/g80.md); why do ye [adikeō](../../strongs/g/g91.md) [allēlōn](../../strongs/g/g240.md)?

<a name="acts_7_27"></a>Acts 7:27

But he that [adikeō](../../strongs/g/g91.md) his [plēsion](../../strongs/g/g4139.md) [apōtheō](../../strongs/g/g683.md) him, [eipon](../../strongs/g/g2036.md), Who [kathistēmi](../../strongs/g/g2525.md) thee an [archōn](../../strongs/g/g758.md) and a [dikastēs](../../strongs/g/g1348.md) over us?

<a name="acts_7_28"></a>Acts 7:28

Wilt thou [anaireō](../../strongs/g/g337.md) me, as thou [anaireō](../../strongs/g/g337.md) the [Aigyptios](../../strongs/g/g124.md) [echthes](../../strongs/g/g5504.md)?

<a name="acts_7_29"></a>Acts 7:29

Then [pheugō](../../strongs/g/g5343.md) [Mōÿsēs](../../strongs/g/g3475.md) at this [logos](../../strongs/g/g3056.md), and was a [paroikos](../../strongs/g/g3941.md) in the [gē](../../strongs/g/g1093.md) of [Madiam](../../strongs/g/g3099.md), where he [gennaō](../../strongs/g/g1080.md) two [huios](../../strongs/g/g5207.md).

<a name="acts_7_30"></a>Acts 7:30

And when forty [etos](../../strongs/g/g2094.md) were [plēroō](../../strongs/g/g4137.md), there [optanomai](../../strongs/g/g3700.md) to him in the [erēmos](../../strongs/g/g2048.md) of [oros](../../strongs/g/g3735.md) [Sina](../../strongs/g/g4614.md) an [aggelos](../../strongs/g/g32.md) of the [kyrios](../../strongs/g/g2962.md) in a [phlox](../../strongs/g/g5395.md) of [pyr](../../strongs/g/g4442.md) in a [batos](../../strongs/g/g942.md).

<a name="acts_7_31"></a>Acts 7:31

When [Mōÿsēs](../../strongs/g/g3475.md) [eidō](../../strongs/g/g1492.md), he [thaumazō](../../strongs/g/g2296.md) at the [horama](../../strongs/g/g3705.md): and as he [proserchomai](../../strongs/g/g4334.md) to [katanoeō](../../strongs/g/g2657.md), the [phōnē](../../strongs/g/g5456.md) of the [kyrios](../../strongs/g/g2962.md) [ginomai](../../strongs/g/g1096.md) unto him,

<a name="acts_7_32"></a>Acts 7:32

I the [theos](../../strongs/g/g2316.md) of thy [patēr](../../strongs/g/g3962.md), the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md), and the [theos](../../strongs/g/g2316.md) of [Isaak](../../strongs/g/g2464.md), and the [theos](../../strongs/g/g2316.md) of [Iakōb](../../strongs/g/g2384.md). Then [Mōÿsēs](../../strongs/g/g3475.md) [ginomai](../../strongs/g/g1096.md) [entromos](../../strongs/g/g1790.md), and [tolmaō](../../strongs/g/g5111.md) not [katanoeō](../../strongs/g/g2657.md).

<a name="acts_7_33"></a>Acts 7:33

Then [eipon](../../strongs/g/g2036.md) the [kyrios](../../strongs/g/g2962.md) to him, [lyō](../../strongs/g/g3089.md) thy [hypodēma](../../strongs/g/g5266.md) from thy [pous](../../strongs/g/g4228.md): for the [topos](../../strongs/g/g5117.md) where thou [histēmi](../../strongs/g/g2476.md) is [hagios](../../strongs/g/g40.md) [gē](../../strongs/g/g1093.md).

<a name="acts_7_34"></a>Acts 7:34

I have [eidō](../../strongs/g/g1492.md), I have [eidō](../../strongs/g/g1492.md) the [kakōsis](../../strongs/g/g2561.md) of my [laos](../../strongs/g/g2992.md) which is in [Aigyptos](../../strongs/g/g125.md), and I have [akouō](../../strongs/g/g191.md) their [stenagmos](../../strongs/g/g4726.md), and am [katabainō](../../strongs/g/g2597.md) to [exaireō](../../strongs/g/g1807.md) them. And now [deuro](../../strongs/g/g1204.md), I will [apostellō](../../strongs/g/g649.md) thee into [Aigyptos](../../strongs/g/g125.md).

<a name="acts_7_35"></a>Acts 7:35

This [Mōÿsēs](../../strongs/g/g3475.md) whom they [arneomai](../../strongs/g/g720.md), [eipon](../../strongs/g/g2036.md), Who [kathistēmi](../../strongs/g/g2525.md) thee an [archōn](../../strongs/g/g758.md) and a [dikastēs](../../strongs/g/g1348.md)? the same did [theos](../../strongs/g/g2316.md) [apostellō](../../strongs/g/g649.md) to be an [archōn](../../strongs/g/g758.md) and a [lytrōtēs](../../strongs/g/g3086.md) by the [cheir](../../strongs/g/g5495.md) of the [aggelos](../../strongs/g/g32.md) which [optanomai](../../strongs/g/g3700.md) to him in the [batos](../../strongs/g/g942.md).

<a name="acts_7_36"></a>Acts 7:36

He [exagō](../../strongs/g/g1806.md) them, after that he had [poieō](../../strongs/g/g4160.md) [teras](../../strongs/g/g5059.md) and [sēmeion](../../strongs/g/g4592.md) in the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md), and in the [erythros](../../strongs/g/g2063.md) [thalassa](../../strongs/g/g2281.md), and in the [erēmos](../../strongs/g/g2048.md) forty [etos](../../strongs/g/g2094.md).

<a name="acts_7_37"></a>Acts 7:37

This is that [Mōÿsēs](../../strongs/g/g3475.md), which [eipon](../../strongs/g/g2036.md) unto the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md), A [prophētēs](../../strongs/g/g4396.md) shall the [kyrios](../../strongs/g/g2962.md) your [theos](../../strongs/g/g2316.md) [anistēmi](../../strongs/g/g450.md) unto you of your [adelphos](../../strongs/g/g80.md), like unto me; him shall ye [akouō](../../strongs/g/g191.md).

<a name="acts_7_38"></a>Acts 7:38

This is he, that was in the [ekklēsia](../../strongs/g/g1577.md) in the [erēmos](../../strongs/g/g2048.md) with the [aggelos](../../strongs/g/g32.md) which [laleō](../../strongs/g/g2980.md) to him in the [oros](../../strongs/g/g3735.md) [Sina](../../strongs/g/g4614.md), and with our [patēr](../../strongs/g/g3962.md): who [dechomai](../../strongs/g/g1209.md) the [zaō](../../strongs/g/g2198.md) [logion](../../strongs/g/g3051.md) to [didōmi](../../strongs/g/g1325.md) unto us:

<a name="acts_7_39"></a>Acts 7:39

To whom our [patēr](../../strongs/g/g3962.md) would not [ginomai](../../strongs/g/g1096.md) [hypēkoos](../../strongs/g/g5255.md), but [apōtheō](../../strongs/g/g683.md), and in their [kardia](../../strongs/g/g2588.md) [strephō](../../strongs/g/g4762.md) into [Aigyptos](../../strongs/g/g125.md),

<a name="acts_7_40"></a>Acts 7:40

[eipon](../../strongs/g/g2036.md) unto [Aarōn](../../strongs/g/g2.md), [poieō](../../strongs/g/g4160.md) us [theos](../../strongs/g/g2316.md) to [proporeuomai](../../strongs/g/g4313.md) us: for as for this [Mōÿsēs](../../strongs/g/g3475.md), which [exagō](../../strongs/g/g1806.md) us out of the [gē](../../strongs/g/g1093.md) of [Aigyptos](../../strongs/g/g125.md), we [eidō](../../strongs/g/g1492.md) not what is become of him.

<a name="acts_7_41"></a>Acts 7:41

And they [moschopoieō](../../strongs/g/g3447.md) in those [hēmera](../../strongs/g/g2250.md), and [anagō](../../strongs/g/g321.md) [thysia](../../strongs/g/g2378.md) unto the [eidōlon](../../strongs/g/g1497.md), and [euphrainō](../../strongs/g/g2165.md) in the [ergon](../../strongs/g/g2041.md) of their own [cheir](../../strongs/g/g5495.md).

<a name="acts_7_42"></a>Acts 7:42

Then [theos](../../strongs/g/g2316.md) [strephō](../../strongs/g/g4762.md), and [paradidōmi](../../strongs/g/g3860.md) them up to [latreuō](../../strongs/g/g3000.md) the [stratia](../../strongs/g/g4756.md) of [ouranos](../../strongs/g/g3772.md); as it is [graphō](../../strongs/g/g1125.md) in the [biblos](../../strongs/g/g976.md) of the [prophētēs](../../strongs/g/g4396.md), O ye [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md), have ye [prospherō](../../strongs/g/g4374.md) to me [sphagion](../../strongs/g/g4968.md) and [thysia](../../strongs/g/g2378.md) forty [etos](../../strongs/g/g2094.md) in the [erēmos](../../strongs/g/g2048.md)?

<a name="acts_7_43"></a>Acts 7:43

[kai](../../strongs/g/g2532.md), ye [analambanō](../../strongs/g/g353.md) the [skēnē](../../strongs/g/g4633.md) of [Moloch](../../strongs/g/g3434.md), and the [astron](../../strongs/g/g798.md) of your [theos](../../strongs/g/g2316.md) [Rhaiphan](../../strongs/g/g4481.md), [typos](../../strongs/g/g5179.md) which ye [poieō](../../strongs/g/g4160.md) to [proskyneō](../../strongs/g/g4352.md) them: and I will [metoikizō](../../strongs/g/g3351.md) you [epekeina](../../strongs/g/g1900.md) [Babylōn](../../strongs/g/g897.md).

<a name="acts_7_44"></a>Acts 7:44

Our [patēr](../../strongs/g/g3962.md) had the [skēnē](../../strongs/g/g4633.md) of [martyrion](../../strongs/g/g3142.md) in the [erēmos](../../strongs/g/g2048.md), as he had [diatassō](../../strongs/g/g1299.md), [laleō](../../strongs/g/g2980.md) unto [Mōÿsēs](../../strongs/g/g3475.md), that he should [poieō](../../strongs/g/g4160.md) it according to the [typos](../../strongs/g/g5179.md) that he had [horaō](../../strongs/g/g3708.md).

<a name="acts_7_45"></a>Acts 7:45

Which also our [patēr](../../strongs/g/g3962.md) that [diadechomai](../../strongs/g/g1237.md) [eisagō](../../strongs/g/g1521.md) with [Iēsous](../../strongs/g/g2424.md) into the [kataschesis](../../strongs/g/g2697.md) of the [ethnos](../../strongs/g/g1484.md), whom [theos](../../strongs/g/g2316.md) [exōtheō](../../strongs/g/g1856.md) before the [prosōpon](../../strongs/g/g4383.md) of our [patēr](../../strongs/g/g3962.md), unto the [hēmera](../../strongs/g/g2250.md) of [Dabid](../../strongs/g/g1138.md);

<a name="acts_7_46"></a>Acts 7:46

Who [heuriskō](../../strongs/g/g2147.md) [charis](../../strongs/g/g5485.md) before [theos](../../strongs/g/g2316.md), and [aiteō](../../strongs/g/g154.md) to [heuriskō](../../strongs/g/g2147.md) a [skēnōma](../../strongs/g/g4638.md) for the [theos](../../strongs/g/g2316.md) of [Iakōb](../../strongs/g/g2384.md).

<a name="acts_7_47"></a>Acts 7:47

But [Solomōn](../../strongs/g/g4672.md) [oikodomeō](../../strongs/g/g3618.md) him an [oikos](../../strongs/g/g3624.md).

<a name="acts_7_48"></a>Acts 7:48

Howbeit [hypsistos](../../strongs/g/g5310.md) [katoikeō](../../strongs/g/g2730.md) not in [naos](../../strongs/g/g3485.md) [cheiropoiētos](../../strongs/g/g5499.md); as [legō](../../strongs/g/g3004.md) the [prophētēs](../../strongs/g/g4396.md),

<a name="acts_7_49"></a>Acts 7:49

[ouranos](../../strongs/g/g3772.md) my [thronos](../../strongs/g/g2362.md), and [gē](../../strongs/g/g1093.md) my [pous](../../strongs/g/g4228.md) [hypopodion](../../strongs/g/g5286.md): what [oikos](../../strongs/g/g3624.md) will ye [oikodomeō](../../strongs/g/g3618.md) me? [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md): or what the [topos](../../strongs/g/g5117.md) of my [katapausis](../../strongs/g/g2663.md)?

<a name="acts_7_50"></a>Acts 7:50

Hath not my [cheir](../../strongs/g/g5495.md) [poieō](../../strongs/g/g4160.md) all these things?

<a name="acts_7_51"></a>Acts 7:51

Ye [sklērotrachēlos](../../strongs/g/g4644.md) and [aperitmētos](../../strongs/g/g564.md) in [kardia](../../strongs/g/g2588.md) and [ous](../../strongs/g/g3775.md), ye do always [antipiptō](../../strongs/g/g496.md) the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md): as your [patēr](../../strongs/g/g3962.md), so ye.

<a name="acts_7_52"></a>Acts 7:52

Which of the [prophētēs](../../strongs/g/g4396.md) have not your [patēr](../../strongs/g/g3962.md) [diōkō](../../strongs/g/g1377.md)? and they have [apokteinō](../../strongs/g/g615.md) them which [prokataggellō](../../strongs/g/g4293.md) of the [eleusis](../../strongs/g/g1660.md) of the [dikaios](../../strongs/g/g1342.md); of whom ye have been now the [prodotēs](../../strongs/g/g4273.md) and [phoneus](../../strongs/g/g5406.md):

<a name="acts_7_53"></a>Acts 7:53

Who have [lambanō](../../strongs/g/g2983.md) the [nomos](../../strongs/g/g3551.md) by the [diatagē](../../strongs/g/g1296.md) of [aggelos](../../strongs/g/g32.md), and have not [phylassō](../../strongs/g/g5442.md).

<a name="acts_7_54"></a>Acts 7:54

When they [akouō](../../strongs/g/g191.md) these things, they were [diapriō](../../strongs/g/g1282.md) to the [kardia](../../strongs/g/g2588.md), and they [brychō](../../strongs/g/g1031.md) on him with their [odous](../../strongs/g/g3599.md).

<a name="acts_7_55"></a>Acts 7:55

But he, being [plērēs](../../strongs/g/g4134.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), [atenizō](../../strongs/g/g816.md) into [ouranos](../../strongs/g/g3772.md), and [eidō](../../strongs/g/g1492.md) the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md), and [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md) on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md),

<a name="acts_7_56"></a>Acts 7:56

And [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I [theōreō](../../strongs/g/g2334.md) the [ouranos](../../strongs/g/g3772.md) [anoigō](../../strongs/g/g455.md), and the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [histēmi](../../strongs/g/g2476.md) on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_7_57"></a>Acts 7:57

Then they [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), and [synechō](../../strongs/g/g4912.md) their [ous](../../strongs/g/g3775.md), and [hormaō](../../strongs/g/g3729.md) upon him with [homothymadon](../../strongs/g/g3661.md),

<a name="acts_7_58"></a>Acts 7:58

And [ekballō](../../strongs/g/g1544.md) out of the [polis](../../strongs/g/g4172.md), and [lithoboleō](../../strongs/g/g3036.md) him: and the [martys](../../strongs/g/g3144.md) [apotithēmi](../../strongs/g/g659.md) their [himation](../../strongs/g/g2440.md) at a [neanias](../../strongs/g/g3494.md) [pous](../../strongs/g/g4228.md), [kaleō](../../strongs/g/g2564.md) [Saulos](../../strongs/g/g4569.md).

<a name="acts_7_59"></a>Acts 7:59

And they [lithoboleō](../../strongs/g/g3036.md) [Stephanos](../../strongs/g/g4736.md), [epikaleō](../../strongs/g/g1941.md), and [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), [dechomai](../../strongs/g/g1209.md) my [pneuma](../../strongs/g/g4151.md).

<a name="acts_7_60"></a>Acts 7:60

And he [tithēmi](../../strongs/g/g5087.md) [gony](../../strongs/g/g1119.md), and [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [kyrios](../../strongs/g/g2962.md), [histēmi](../../strongs/g/g2476.md) not this [hamartia](../../strongs/g/g266.md) to their. And when he had [eipon](../../strongs/g/g2036.md) this, he [koimaō](../../strongs/g/g2837.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 6](acts_6.md) - [Acts 8](acts_8.md)