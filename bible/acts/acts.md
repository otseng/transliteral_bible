# Acts

[Acts Overview](../../commentary/acts/acts_overview.md)

[Acts 1](acts_1.md)

[Acts 2](acts_2.md)

[Acts 3](acts_3.md)

[Acts 4](acts_4.md)

[Acts 5](acts_5.md)

[Acts 6](acts_6.md)

[Acts 7](acts_7.md)

[Acts 8](acts_8.md)

[Acts 9](acts_9.md)

[Acts 10](acts_10.md)

[Acts 11](acts_11.md)

[Acts 12](acts_12.md)

[Acts 13](acts_13.md)

[Acts 14](acts_14.md)

[Acts 15](acts_15.md)

[Acts 16](acts_16.md)

[Acts 17](acts_17.md)

[Acts 18](acts_18.md)

[Acts 19](acts_19.md)

[Acts 20](acts_20.md)

[Acts 21](acts_21.md)

[Acts 22](acts_22.md)

[Acts 23](acts_23.md)

[Acts 24](acts_24.md)

[Acts 25](acts_25.md)

[Acts 26](acts_26.md)

[Acts 27](acts_27.md)

[Acts 28](acts_28.md)

---

[Transliteral Bible](../index.md)