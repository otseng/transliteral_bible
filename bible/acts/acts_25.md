# [Acts 25](https://www.blueletterbible.org/kjv/act/25/1/s_1043001)

<a name="acts_25_1"></a>Acts 25:1

Now when [Phēstos](../../strongs/g/g5347.md) was [epibainō](../../strongs/g/g1910.md) into the [eparcheia](../../strongs/g/g1885.md), after three [hēmera](../../strongs/g/g2250.md) he [anabainō](../../strongs/g/g305.md) from [Kaisareia](../../strongs/g/g2542.md) to [Hierosolyma](../../strongs/g/g2414.md).

<a name="acts_25_2"></a>Acts 25:2

Then the [archiereus](../../strongs/g/g749.md) and the [prōtos](../../strongs/g/g4413.md) of the [Ioudaios](../../strongs/g/g2453.md) [emphanizō](../../strongs/g/g1718.md) him against [Paulos](../../strongs/g/g3972.md), and [parakaleō](../../strongs/g/g3870.md) him,

<a name="acts_25_3"></a>Acts 25:3

And [aiteō](../../strongs/g/g154.md) [charis](../../strongs/g/g5485.md) against him, that he would [metapempō](../../strongs/g/g3343.md) him to [Ierousalēm](../../strongs/g/g2419.md), [poieō](../../strongs/g/g4160.md) [enedra](../../strongs/g/g1747.md) in the [hodos](../../strongs/g/g3598.md) to [anaireō](../../strongs/g/g337.md) him.

<a name="acts_25_4"></a>Acts 25:4

But [Phēstos](../../strongs/g/g5347.md) [apokrinomai](../../strongs/g/g611.md), that [Paulos](../../strongs/g/g3972.md) should be [tēreō](../../strongs/g/g5083.md) at [Kaisareia](../../strongs/g/g2542.md), and that he himself would [ekporeuomai](../../strongs/g/g1607.md) [tachos](../../strongs/g/g5034.md).

<a name="acts_25_5"></a>Acts 25:5

Let them therefore, [phēmi](../../strongs/g/g5346.md) he, which among you are [dynatos](../../strongs/g/g1415.md), [sygkatabainō](../../strongs/g/g4782.md) with me, and [katēgoreō](../../strongs/g/g2723.md) this [anēr](../../strongs/g/g435.md), if there be [ei tis](../../strongs/g/g1536.md) in him.

<a name="acts_25_6"></a>Acts 25:6

And when he had [diatribō](../../strongs/g/g1304.md) among them more than ten [hēmera](../../strongs/g/g2250.md), he [katabainō](../../strongs/g/g2597.md) unto [Kaisareia](../../strongs/g/g2542.md); and the [epaurion](../../strongs/g/g1887.md) [kathizō](../../strongs/g/g2523.md) on the [bēma](../../strongs/g/g968.md) [keleuō](../../strongs/g/g2753.md) [Paulos](../../strongs/g/g3972.md) to be [agō](../../strongs/g/g71.md).

<a name="acts_25_7"></a>Acts 25:7

And when he was [paraginomai](../../strongs/g/g3854.md), the [Ioudaios](../../strongs/g/g2453.md) which [katabainō](../../strongs/g/g2597.md) from [Hierosolyma](../../strongs/g/g2414.md) [periistēmi](../../strongs/g/g4026.md), and [pherō](../../strongs/g/g5342.md) [polys](../../strongs/g/g4183.md) and [barys](../../strongs/g/g926.md) [aitiama](../../strongs/g/g157.md) against [Paulos](../../strongs/g/g3972.md), which they could not [apodeiknymi](../../strongs/g/g584.md).

<a name="acts_25_8"></a>Acts 25:8

While he [apologeomai](../../strongs/g/g626.md) for himself, Neither against the [nomos](../../strongs/g/g3551.md) of the [Ioudaios](../../strongs/g/g2453.md), neither against the [hieron](../../strongs/g/g2411.md), nor yet against [Kaisar](../../strongs/g/g2541.md), have I [hamartanō](../../strongs/g/g264.md) any thing at all.

<a name="acts_25_9"></a>Acts 25:9

But [Phēstos](../../strongs/g/g5347.md), [thelō](../../strongs/g/g2309.md) to [katatithēmi](../../strongs/g/g2698.md) the [Ioudaios](../../strongs/g/g2453.md) a [charis](../../strongs/g/g5485.md), [apokrinomai](../../strongs/g/g611.md) [Paulos](../../strongs/g/g3972.md), and [eipon](../../strongs/g/g2036.md), Wilt thou [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md), and there be [krinō](../../strongs/g/g2919.md) of these things before me?

<a name="acts_25_10"></a>Acts 25:10

Then [eipon](../../strongs/g/g2036.md) [Paulos](../../strongs/g/g3972.md), I [histēmi](../../strongs/g/g2476.md) at [Kaisar](../../strongs/g/g2541.md) [bēma](../../strongs/g/g968.md), where I ought to be [krinō](../../strongs/g/g2919.md): to the [Ioudaios](../../strongs/g/g2453.md) have I no [adikeō](../../strongs/g/g91.md), as thou [kallion](../../strongs/g/g2566.md) [epiginōskō](../../strongs/g/g1921.md).

<a name="acts_25_11"></a>Acts 25:11

For if I be an [adikeō](../../strongs/g/g91.md), or have [prassō](../../strongs/g/g4238.md) any thing [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md), I [paraiteomai](../../strongs/g/g3868.md) not [apothnēskō](../../strongs/g/g599.md): but if there be none of these things whereof these [katēgoreō](../../strongs/g/g2723.md) me, no man may [charizomai](../../strongs/g/g5483.md) me unto them. I [epikaleō](../../strongs/g/g1941.md) unto [Kaisar](../../strongs/g/g2541.md).

<a name="acts_25_12"></a>Acts 25:12

Then [Phēstos](../../strongs/g/g5347.md), when he had [syllaleō](../../strongs/g/g4814.md) with the [symboulion](../../strongs/g/g4824.md), [apokrinomai](../../strongs/g/g611.md), Hast thou [epikaleō](../../strongs/g/g1941.md) unto [Kaisar](../../strongs/g/g2541.md)? unto [Kaisar](../../strongs/g/g2541.md) shalt thou [poreuō](../../strongs/g/g4198.md).

<a name="acts_25_13"></a>Acts 25:13

And [diaginomai](../../strongs/g/g1230.md) certain [hēmera](../../strongs/g/g2250.md) [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md) and [Bernikē](../../strongs/g/g959.md) [katantaō](../../strongs/g/g2658.md) unto [Kaisareia](../../strongs/g/g2542.md) to [aspazomai](../../strongs/g/g782.md) [Phēstos](../../strongs/g/g5347.md).

<a name="acts_25_14"></a>Acts 25:14

And when they had [diatribō](../../strongs/g/g1304.md) there many [hēmera](../../strongs/g/g2250.md), [Phēstos](../../strongs/g/g5347.md) [anatithēmi](../../strongs/g/g394.md) [Paulos](../../strongs/g/g3972.md) cause unto the [basileus](../../strongs/g/g935.md), [legō](../../strongs/g/g3004.md), There is a certain [anēr](../../strongs/g/g435.md) [kataleipō](../../strongs/g/g2641.md) in [desmios](../../strongs/g/g1198.md) by [Phēlix](../../strongs/g/g5344.md):

<a name="acts_25_15"></a>Acts 25:15

About whom, when I was at [Hierosolyma](../../strongs/g/g2414.md), the [archiereus](../../strongs/g/g749.md) and the [presbyteros](../../strongs/g/g4245.md) of the [Ioudaios](../../strongs/g/g2453.md) [emphanizō](../../strongs/g/g1718.md), [aiteō](../../strongs/g/g154.md)  [dikē](../../strongs/g/g1349.md) against him.

<a name="acts_25_16"></a>Acts 25:16

To whom I [apokrinomai](../../strongs/g/g611.md), It is not the [ethos](../../strongs/g/g1485.md) of the [Rhōmaios](../../strongs/g/g4514.md) to [charizomai](../../strongs/g/g5483.md) any [anthrōpos](../../strongs/g/g444.md) to [apōleia](../../strongs/g/g684.md), before that he which is [katēgoreō](../../strongs/g/g2723.md) have the [katēgoros](../../strongs/g/g2725.md) [prosōpon](../../strongs/g/g4383.md), and [lambanō](../../strongs/g/g2983.md) [topos](../../strongs/g/g5117.md) to [apologia](../../strongs/g/g627.md) concerning the [egklēma](../../strongs/g/g1462.md).

<a name="acts_25_17"></a>Acts 25:17

Therefore, when they were [synerchomai](../../strongs/g/g4905.md) [enthade](../../strongs/g/g1759.md), [poieō](../../strongs/g/g4160.md) [mēdeis](../../strongs/g/g3367.md) [anabolē](../../strongs/g/g311.md) on the [hexēs](../../strongs/g/g1836.md) I [kathizō](../../strongs/g/g2523.md) on the [bēma](../../strongs/g/g968.md), and [keleuō](../../strongs/g/g2753.md) the [anēr](../../strongs/g/g435.md) to be [agō](../../strongs/g/g71.md).

<a name="acts_25_18"></a>Acts 25:18

Against whom when the [katēgoros](../../strongs/g/g2725.md) [histēmi](../../strongs/g/g2476.md), they [epipherō](../../strongs/g/g2018.md) none [aitia](../../strongs/g/g156.md) of such things as I [hyponoeō](../../strongs/g/g5282.md):

<a name="acts_25_19"></a>Acts 25:19

But had certain [zētēma](../../strongs/g/g2213.md) against him of their own [deisidaimonia](../../strongs/g/g1175.md), and of one [Iēsous](../../strongs/g/g2424.md), which was [thnēskō](../../strongs/g/g2348.md), whom [Paulos](../../strongs/g/g3972.md) [phasko](../../strongs/g/g5335.md) [zaō](../../strongs/g/g2198.md).

<a name="acts_25_20"></a>Acts 25:20

And because I [aporeō](../../strongs/g/g639.md) of such manner of [zētēsis](../../strongs/g/g2214.md), I [legō](../../strongs/g/g3004.md) him whether he [boulomai](../../strongs/g/g1014.md) [poreuō](../../strongs/g/g4198.md) to [Ierousalēm](../../strongs/g/g2419.md), and there be [krinō](../../strongs/g/g2919.md) of these matters.

<a name="acts_25_21"></a>Acts 25:21

But when [Paulos](../../strongs/g/g3972.md) had [epikaleō](../../strongs/g/g1941.md) to be [tēreō](../../strongs/g/g5083.md) unto the [diagnōsis](../../strongs/g/g1233.md) of [sebastos](../../strongs/g/g4575.md), I [keleuō](../../strongs/g/g2753.md) him to be [tēreō](../../strongs/g/g5083.md) till I might [pempō](../../strongs/g/g3992.md) him to [Kaisar](../../strongs/g/g2541.md).

<a name="acts_25_22"></a>Acts 25:22

Then [Agrippas](../../strongs/g/g67.md) [phēmi](../../strongs/g/g5346.md) unto [Phēstos](../../strongs/g/g5347.md), I [boulomai](../../strongs/g/g1014.md) also [akouō](../../strongs/g/g191.md) the [anthrōpos](../../strongs/g/g444.md) myself. [aurion](../../strongs/g/g839.md), [phēmi](../../strongs/g/g5346.md) he, thou shalt [akouō](../../strongs/g/g191.md) him.

<a name="acts_25_23"></a>Acts 25:23

And on the [epaurion](../../strongs/g/g1887.md), when [Agrippas](../../strongs/g/g67.md) was [erchomai](../../strongs/g/g2064.md), and [Bernikē](../../strongs/g/g959.md), with [polys](../../strongs/g/g4183.md) [phantasia](../../strongs/g/g5325.md), and was [eiserchomai](../../strongs/g/g1525.md) into the [akroatērion](../../strongs/g/g201.md), with the [chiliarchos](../../strongs/g/g5506.md), and [kata](../../strongs/g/g2596.md) [exochē](../../strongs/g/g1851.md) [anēr](../../strongs/g/g435.md) of the [polis](../../strongs/g/g4172.md), at [Phēstos](../../strongs/g/g5347.md) [keleuō](../../strongs/g/g2753.md) [Paulos](../../strongs/g/g3972.md) was [agō](../../strongs/g/g71.md).

<a name="acts_25_24"></a>Acts 25:24

And [Phēstos](../../strongs/g/g5347.md) [phēmi](../../strongs/g/g5346.md), [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), and all [anēr](../../strongs/g/g435.md) which are [sympareimi](../../strongs/g/g4840.md) with us, ye [theōreō](../../strongs/g/g2334.md) this man, about whom all the [plēthos](../../strongs/g/g4128.md) of the [Ioudaios](../../strongs/g/g2453.md) have [entygchanō](../../strongs/g/g1793.md) with me, both at [Hierosolyma](../../strongs/g/g2414.md), and also [enthade](../../strongs/g/g1759.md), [epiboaō](../../strongs/g/g1916.md) that he ought not to [zaō](../../strongs/g/g2198.md) [mēketi](../../strongs/g/g3371.md).

<a name="acts_25_25"></a>Acts 25:25

But when I [katalambanō](../../strongs/g/g2638.md) that he had [prassō](../../strongs/g/g4238.md) [mēdeis](../../strongs/g/g3367.md) [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md), and that he himself hath [epikaleō](../../strongs/g/g1941.md) to [sebastos](../../strongs/g/g4575.md), I have [krinō](../../strongs/g/g2919.md) to [pempō](../../strongs/g/g3992.md) him.

<a name="acts_25_26"></a>Acts 25:26

Of whom I have no [asphalēs](../../strongs/g/g804.md) thing to [graphō](../../strongs/g/g1125.md) unto my [kyrios](../../strongs/g/g2962.md). Wherefore I have [proagō](../../strongs/g/g4254.md) him forth before you, and [malista](../../strongs/g/g3122.md) before thee, O [basileus](../../strongs/g/g935.md) [Agrippas](../../strongs/g/g67.md), that, after [anakrisis](../../strongs/g/g351.md) had, I might have somewhat to [graphō](../../strongs/g/g1125.md).

<a name="acts_25_27"></a>Acts 25:27

For it [dokeō](../../strongs/g/g1380.md) to me [alogos](../../strongs/g/g249.md) to [pempō](../../strongs/g/g3992.md) a [desmios](../../strongs/g/g1198.md), and not withal to [sēmainō](../../strongs/g/g4591.md) the [aitia](../../strongs/g/g156.md) against him.

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 24](acts_24.md) - [Acts 26](acts_26.md)