# [Acts 2](https://www.blueletterbible.org/kjv/act/2/1/s_1020001)

<a name="acts_2_1"></a>Acts 2:1

And when the [hēmera](../../strongs/g/g2250.md) of [pentēkostē](../../strongs/g/g4005.md) was [symplēroō](../../strongs/g/g4845.md), they were all [homothymadon](../../strongs/g/g3661.md) in [autos](../../strongs/g/g846.md).

<a name="acts_2_2"></a>Acts 2:2

And [aphnō](../../strongs/g/g869.md) there [ginomai](../../strongs/g/g1096.md) an [ēchos](../../strongs/g/g2279.md) from [ouranos](../../strongs/g/g3772.md) as of a [pherō](../../strongs/g/g5342.md) [biaios](../../strongs/g/g972.md) [pnoē](../../strongs/g/g4157.md), and it [plēroō](../../strongs/g/g4137.md) all the [oikos](../../strongs/g/g3624.md)e where they were [kathēmai](../../strongs/g/g2521.md).

<a name="acts_2_3"></a>Acts 2:3

And there [optanomai](../../strongs/g/g3700.md) unto them [diamerizō](../../strongs/g/g1266.md) [glōssa](../../strongs/g/g1100.md) like as of [pyr](../../strongs/g/g4442.md), and it [kathizō](../../strongs/g/g2523.md) upon each of them.

<a name="acts_2_4"></a>Acts 2:4

And they were all [pimplēmi](../../strongs/g/g4130.md) with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and [archomai](../../strongs/g/g756.md) to [laleō](../../strongs/g/g2980.md) with other [glōssa](../../strongs/g/g1100.md), as the [pneuma](../../strongs/g/g4151.md) [didōmi](../../strongs/g/g1325.md) them [apophtheggomai](../../strongs/g/g669.md).

<a name="acts_2_5"></a>Acts 2:5

And there were [katoikeō](../../strongs/g/g2730.md) at [Ierousalēm](../../strongs/g/g2419.md) [Ioudaios](../../strongs/g/g2453.md), [eulabēs](../../strongs/g/g2126.md) [anēr](../../strongs/g/g435.md), out of every [ethnos](../../strongs/g/g1484.md) under [ouranos](../../strongs/g/g3772.md).

<a name="acts_2_6"></a>Acts 2:6

Now when this was [phōnē](../../strongs/g/g5456.md), the [plēthos](../../strongs/g/g4128.md) [synerchomai](../../strongs/g/g4905.md), and were [sygcheō](../../strongs/g/g4797.md), because that every [heis](../../strongs/g/g1520.md) [akouō](../../strongs/g/g191.md) them [laleō](../../strongs/g/g2980.md) in his own [dialektos](../../strongs/g/g1258.md).

<a name="acts_2_7"></a>Acts 2:7

And they were all [existēmi](../../strongs/g/g1839.md) and [thaumazō](../../strongs/g/g2296.md), [legō](../../strongs/g/g3004.md) to [allēlōn](../../strongs/g/g240.md), [idou](../../strongs/g/g2400.md), are not all these which [laleō](../../strongs/g/g2980.md) [Galilaios](../../strongs/g/g1057.md)?

<a name="acts_2_8"></a>Acts 2:8

And how [akouō](../../strongs/g/g191.md) we every [hēmōn](../../strongs/g/g2257.md) [idios](../../strongs/g/g2398.md) [dialektos](../../strongs/g/g1258.md), wherein we were [gennaō](../../strongs/g/g1080.md)?

<a name="acts_2_9"></a>Acts 2:9

[Parthoi](../../strongs/g/g3934.md), and [Mēdos](../../strongs/g/g3370.md), and [Elamitēs](../../strongs/g/g1639.md), and the [katoikeō](../../strongs/g/g2730.md) in [Mesopotamia](../../strongs/g/g3318.md), and in [Ioudaia](../../strongs/g/g2449.md), and [Kappadokia](../../strongs/g/g2587.md), in [Pontos](../../strongs/g/g4195.md), and [Asia](../../strongs/g/g773.md),

<a name="acts_2_10"></a>Acts 2:10

[Phrygia](../../strongs/g/g5435.md), and [Pamphylia](../../strongs/g/g3828.md), in [Aigyptos](../../strongs/g/g125.md), and in the [meros](../../strongs/g/g3313.md) of [Libyē](../../strongs/g/g3033.md) about [Kyrēnē](../../strongs/g/g2957.md), and [epidēmeō](../../strongs/g/g1927.md) of [Rhōmaios](../../strongs/g/g4514.md), [Ioudaios](../../strongs/g/g2453.md) and [prosēlytos](../../strongs/g/g4339.md),

<a name="acts_2_11"></a>Acts 2:11

[Krēs](../../strongs/g/g2912.md) and [Araps](../../strongs/g/g690.md), we do [akouō](../../strongs/g/g191.md) them [laleō](../../strongs/g/g2980.md) in [hēmeteros](../../strongs/g/g2251.md) [glōssa](../../strongs/g/g1100.md) the [megaleios](../../strongs/g/g3167.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_2_12"></a>Acts 2:12

And they were all [existēmi](../../strongs/g/g1839.md), and were in [diaporeō](../../strongs/g/g1280.md), [legō](../../strongs/g/g3004.md) one to another, What [thelō](../../strongs/g/g2309.md) [an](../../strongs/g/g302.md) [einai](../../strongs/g/g1511.md) this?

<a name="acts_2_13"></a>Acts 2:13

Others [chleuazō](../../strongs/g/g5512.md) [legō](../../strongs/g/g3004.md), [eisi](../../strongs/g/g1526.md) [mestoō](../../strongs/g/g3325.md) of [gleukos](../../strongs/g/g1098.md).

<a name="acts_2_14"></a>Acts 2:14

But [Petros](../../strongs/g/g4074.md), [histēmi](../../strongs/g/g2476.md) with the eleven, [epairō](../../strongs/g/g1869.md) his [phōnē](../../strongs/g/g5456.md), and [apophtheggomai](../../strongs/g/g669.md) unto them, Ye [anēr](../../strongs/g/g435.md) [Ioudaios](../../strongs/g/g2453.md), and all ye that [katoikeō](../../strongs/g/g2730.md) at [Ierousalēm](../../strongs/g/g2419.md), be this [gnōstos](../../strongs/g/g1110.md) unto you, and [enōtizomai](../../strongs/g/g1801.md) to my [rhēma](../../strongs/g/g4487.md):

<a name="acts_2_15"></a>Acts 2:15

For these are not [methyō](../../strongs/g/g3184.md), as ye [hypolambanō](../../strongs/g/g5274.md), [gar](../../strongs/g/g1063.md) it is but the third [hōra](../../strongs/g/g5610.md) of the [hēmera](../../strongs/g/g2250.md).

<a name="acts_2_16"></a>Acts 2:16

But this is that which was [eipon](../../strongs/g/g2046.md) by the [prophētēs](../../strongs/g/g4396.md) [Iōēl](../../strongs/g/g2493.md);

<a name="acts_2_17"></a>Acts 2:17

And [esomai](../../strongs/g/g2071.md) in the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md), [legō](../../strongs/g/g3004.md) [theos](../../strongs/g/g2316.md), I will [ekcheō](../../strongs/g/g1632.md) of my [pneuma](../../strongs/g/g4151.md) upon all [sarx](../../strongs/g/g4561.md): and your [huios](../../strongs/g/g5207.md) and your [thygatēr](../../strongs/g/g2364.md) shall [prophēteuō](../../strongs/g/g4395.md), and your [neaniskos](../../strongs/g/g3495.md) shall [optanomai](../../strongs/g/g3700.md) [horasis](../../strongs/g/g3706.md), and your [presbyteros](../../strongs/g/g4245.md) shall [enypniazomai](../../strongs/g/g1797.md) [enypnion](../../strongs/g/g1798.md):

<a name="acts_2_18"></a>Acts 2:18

And on my [doulos](../../strongs/g/g1401.md) and on my [doulē](../../strongs/g/g1399.md) I will [ekcheō](../../strongs/g/g1632.md) in those [hēmera](../../strongs/g/g2250.md) of my [pneuma](../../strongs/g/g4151.md); and they shall [prophēteuō](../../strongs/g/g4395.md):

<a name="acts_2_19"></a>Acts 2:19

And I will [didōmi](../../strongs/g/g1325.md) [teras](../../strongs/g/g5059.md) in [ouranos](../../strongs/g/g3772.md) [anō](../../strongs/g/g507.md), and [sēmeion](../../strongs/g/g4592.md) in the [gē](../../strongs/g/g1093.md) beneath; [haima](../../strongs/g/g129.md), and [pyr](../../strongs/g/g4442.md), and [atmis](../../strongs/g/g822.md) of [kapnos](../../strongs/g/g2586.md):

<a name="acts_2_20"></a>Acts 2:20

The [hēlios](../../strongs/g/g2246.md) shall [metastrephō](../../strongs/g/g3344.md) into [skotos](../../strongs/g/g4655.md), and the [selēnē](../../strongs/g/g4582.md) into [haima](../../strongs/g/g129.md), before the [megas](../../strongs/g/g3173.md) and [epiphanēs](../../strongs/g/g2016.md) [hēmera](../../strongs/g/g2250.md) of the [kyrios](../../strongs/g/g2962.md) [erchomai](../../strongs/g/g2064.md):

<a name="acts_2_21"></a>Acts 2:21

And [esomai](../../strongs/g/g2071.md), whosoever shall [epikaleō](../../strongs/g/g1941.md) the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) shall be [sōzō](../../strongs/g/g4982.md).

<a name="acts_2_22"></a>Acts 2:22

Ye [anēr](../../strongs/g/g435.md) of [Israēlitēs](../../strongs/g/g2475.md), [akouō](../../strongs/g/g191.md) these [logos](../../strongs/g/g3056.md); [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md), an [anēr](../../strongs/g/g435.md) [apodeiknymi](../../strongs/g/g584.md) of [theos](../../strongs/g/g2316.md) among you by [dynamis](../../strongs/g/g1411.md) and [teras](../../strongs/g/g5059.md) and [sēmeion](../../strongs/g/g4592.md), which [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) by him in the [mesos](../../strongs/g/g3319.md) of you, as ye yourselves also [eidō](../../strongs/g/g1492.md):

<a name="acts_2_23"></a>Acts 2:23

Him, being [ekdotos](../../strongs/g/g1560.md) by the [horizō](../../strongs/g/g3724.md) [boulē](../../strongs/g/g1012.md) and [prognōsis](../../strongs/g/g4268.md) of [theos](../../strongs/g/g2316.md), ye have [lambanō](../../strongs/g/g2983.md), and by [anomos](../../strongs/g/g459.md) [cheir](../../strongs/g/g5495.md) have [prospēgnymi](../../strongs/g/g4362.md) and [anaireō](../../strongs/g/g337.md):

<a name="acts_2_24"></a>Acts 2:24

Whom [theos](../../strongs/g/g2316.md) hath [anistēmi](../../strongs/g/g450.md), having [lyō](../../strongs/g/g3089.md) the [ōdin](../../strongs/g/g5604.md) of [thanatos](../../strongs/g/g2288.md): because it was not [dynatos](../../strongs/g/g1415.md) that he should [krateō](../../strongs/g/g2902.md) of it.

<a name="acts_2_25"></a>Acts 2:25

For [Dabid](../../strongs/g/g1138.md) [legō](../../strongs/g/g3004.md) concerning him, I [prooraō](../../strongs/g/g4308.md) the [kyrios](../../strongs/g/g2962.md) always [enōpion](../../strongs/g/g1799.md) [mou](../../strongs/g/g3450.md), for he is on my [dexios](../../strongs/g/g1188.md), that I should not [saleuō](../../strongs/g/g4531.md):

<a name="acts_2_26"></a>Acts 2:26

Therefore did my [kardia](../../strongs/g/g2588.md) [euphrainō](../../strongs/g/g2165.md), and my [glōssa](../../strongs/g/g1100.md) was [agalliaō](../../strongs/g/g21.md); moreover also my [sarx](../../strongs/g/g4561.md) shall [kataskēnoō](../../strongs/g/g2681.md) in [elpis](../../strongs/g/g1680.md):

<a name="acts_2_27"></a>Acts 2:27

Because thou wilt not [egkataleipō](../../strongs/g/g1459.md) my [psychē](../../strongs/g/g5590.md) in [hadēs](../../strongs/g/g86.md), neither wilt thou [didōmi](../../strongs/g/g1325.md) thine [hosios](../../strongs/g/g3741.md) to [eidō](../../strongs/g/g1492.md) [diaphthora](../../strongs/g/g1312.md).

<a name="acts_2_28"></a>Acts 2:28

Thou hast [gnōrizō](../../strongs/g/g1107.md) to me [hodos](../../strongs/g/g3598.md) of [zōē](../../strongs/g/g2222.md); thou shalt make me [plēroō](../../strongs/g/g4137.md) of [euphrosynē](../../strongs/g/g2167.md) with thy [prosōpon](../../strongs/g/g4383.md).

<a name="acts_2_29"></a>Acts 2:29

[anēr](../../strongs/g/g435.md) [adelphos](../../strongs/g/g80.md), [exesti](../../strongs/g/g1832.md) [meta](../../strongs/g/g3326.md) [parrēsia](../../strongs/g/g3954.md) me [eipon](../../strongs/g/g2036.md) unto you of the [patriarchēs](../../strongs/g/g3966.md) [Dabid](../../strongs/g/g1138.md), that he is both [teleutaō](../../strongs/g/g5053.md) and [thaptō](../../strongs/g/g2290.md), and his [mnēma](../../strongs/g/g3418.md) is with us unto this day.

<a name="acts_2_30"></a>Acts 2:30

Therefore being a [prophētēs](../../strongs/g/g4396.md), and [eidō](../../strongs/g/g1492.md) that [theos](../../strongs/g/g2316.md) had [omnyō](../../strongs/g/g3660.md) [horkos](../../strongs/g/g3727.md) to him, that of the [karpos](../../strongs/g/g2590.md) of his [osphys](../../strongs/g/g3751.md), according to the [sarx](../../strongs/g/g4561.md), he would [anistēmi](../../strongs/g/g450.md) [Christos](../../strongs/g/g5547.md) to [kathizō](../../strongs/g/g2523.md) on his [thronos](../../strongs/g/g2362.md); [^1]

<a name="acts_2_31"></a>Acts 2:31

He [prooraō](../../strongs/g/g4275.md) this [laleō](../../strongs/g/g2980.md) of the [anastasis](../../strongs/g/g386.md) of [Christos](../../strongs/g/g5547.md), that his [psychē](../../strongs/g/g5590.md) was not [kataleipō](../../strongs/g/g2641.md) in [hadēs](../../strongs/g/g86.md), neither his [sarx](../../strongs/g/g4561.md) did [eidō](../../strongs/g/g1492.md) [diaphthora](../../strongs/g/g1312.md).

<a name="acts_2_32"></a>Acts 2:32

This [Iēsous](../../strongs/g/g2424.md) hath [theos](../../strongs/g/g2316.md) [anistēmi](../../strongs/g/g450.md), whereof we all are [martys](../../strongs/g/g3144.md).

<a name="acts_2_33"></a>Acts 2:33

Therefore being by the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md) [hypsoō](../../strongs/g/g5312.md), and having [lambanō](../../strongs/g/g2983.md) of the [patēr](../../strongs/g/g3962.md) the [epaggelia](../../strongs/g/g1860.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), he hath [ekcheō](../../strongs/g/g1632.md) this, which ye now [blepō](../../strongs/g/g991.md) and [akouō](../../strongs/g/g191.md).

<a name="acts_2_34"></a>Acts 2:34

For [Dabid](../../strongs/g/g1138.md) is not [anabainō](../../strongs/g/g305.md) into the [ouranos](../../strongs/g/g3772.md): but he [legō](../../strongs/g/g3004.md) himself, The [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) unto my [kyrios](../../strongs/g/g2962.md), [kathēmai](../../strongs/g/g2521.md) on my [dexios](../../strongs/g/g1188.md),

<a name="acts_2_35"></a>Acts 2:35

Until I [tithēmi](../../strongs/g/g5087.md) thy [echthros](../../strongs/g/g2190.md) thy [pous](../../strongs/g/g4228.md) [hypopodion](../../strongs/g/g5286.md).

<a name="acts_2_36"></a>Acts 2:36

Therefore let all the [oikos](../../strongs/g/g3624.md) of [Israēl](../../strongs/g/g2474.md) [ginōskō](../../strongs/g/g1097.md) [asphalōs](../../strongs/g/g806.md), that [theos](../../strongs/g/g2316.md) hath [poieō](../../strongs/g/g4160.md) the same [Iēsous](../../strongs/g/g2424.md), whom ye have [stauroō](../../strongs/g/g4717.md), both [kyrios](../../strongs/g/g2962.md) and [Christos](../../strongs/g/g5547.md).

<a name="acts_2_37"></a>Acts 2:37

Now when they [akouō](../../strongs/g/g191.md), they were [katanyssomai](../../strongs/g/g2660.md) in their [kardia](../../strongs/g/g2588.md), and [eipon](../../strongs/g/g2036.md) unto [Petros](../../strongs/g/g4074.md) and to the [loipos](../../strongs/g/g3062.md) of the [apostolos](../../strongs/g/g652.md), [anēr](../../strongs/g/g435.md) [adelphos](../../strongs/g/g80.md), what shall we [poieō](../../strongs/g/g4160.md)?

<a name="acts_2_38"></a>Acts 2:38

Then [Petros](../../strongs/g/g4074.md) [phēmi](../../strongs/g/g5346.md) unto them, [metanoeō](../../strongs/g/g3340.md), and [baptizō](../../strongs/g/g907.md) every one of you in the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) for the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md), and ye shall [lambanō](../../strongs/g/g2983.md) the [dōrea](../../strongs/g/g1431.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="acts_2_39"></a>Acts 2:39

For the [epaggelia](../../strongs/g/g1860.md) is unto you, and to your [teknon](../../strongs/g/g5043.md), and to all that are [eis](../../strongs/g/g1519.md) [makran](../../strongs/g/g3112.md), even as many as the [kyrios](../../strongs/g/g2962.md) our [theos](../../strongs/g/g2316.md) shall [proskaleō](../../strongs/g/g4341.md).

<a name="acts_2_40"></a>Acts 2:40

And with many other [logos](../../strongs/g/g3056.md) did he [diamartyromai](../../strongs/g/g1263.md) and [parakaleō](../../strongs/g/g3870.md), [legō](../../strongs/g/g3004.md), [sōzō](../../strongs/g/g4982.md) from this [skolios](../../strongs/g/g4646.md) [genea](../../strongs/g/g1074.md).

<a name="acts_2_41"></a>Acts 2:41

Then they that [asmenōs](../../strongs/g/g780.md) [apodechomai](../../strongs/g/g588.md) his [logos](../../strongs/g/g3056.md) were [baptizō](../../strongs/g/g907.md): and the [ekeinos](../../strongs/g/g1565.md) [hēmera](../../strongs/g/g2250.md) there were [prostithēmi](../../strongs/g/g4369.md) about three thousand [psychē](../../strongs/g/g5590.md).

<a name="acts_2_42"></a>Acts 2:42

And they [ēn](../../strongs/g/g2258.md) [proskartereō](../../strongs/g/g4342.md) in the [apostolos](../../strongs/g/g652.md) [didachē](../../strongs/g/g1322.md) and [koinōnia](../../strongs/g/g2842.md), and in [klasis](../../strongs/g/g2800.md) of [artos](../../strongs/g/g740.md), and in [proseuchē](../../strongs/g/g4335.md).

<a name="acts_2_43"></a>Acts 2:43

And [phobos](../../strongs/g/g5401.md) [ginomai](../../strongs/g/g1096.md) every [psychē](../../strongs/g/g5590.md): and [polys](../../strongs/g/g4183.md) [teras](../../strongs/g/g5059.md) and [sēmeion](../../strongs/g/g4592.md) were done by the [apostolos](../../strongs/g/g652.md).

<a name="acts_2_44"></a>Acts 2:44

And all that [pisteuō](../../strongs/g/g4100.md) were [epi](../../strongs/g/g1909.md) [autos](../../strongs/g/g846.md), and had all things [koinos](../../strongs/g/g2839.md);

<a name="acts_2_45"></a>Acts 2:45

And [pipraskō](../../strongs/g/g4097.md) [ktēma](../../strongs/g/g2933.md) and [hyparxis](../../strongs/g/g5223.md), and [diamerizō](../../strongs/g/g1266.md) them to all, as [tis](../../strongs/g/g5100.md) had [chreia](../../strongs/g/g5532.md).

<a name="acts_2_46"></a>Acts 2:46

And [proskartereō](../../strongs/g/g4342.md) [kata](../../strongs/g/g2596.md) [hēmera](../../strongs/g/g2250.md) [homothymadon](../../strongs/g/g3661.md) in the [hieron](../../strongs/g/g2411.md), and [klaō](../../strongs/g/g2806.md) [artos](../../strongs/g/g740.md) from [oikos](../../strongs/g/g3624.md), [metalambanō](../../strongs/g/g3335.md) their [trophē](../../strongs/g/g5160.md) with [agalliasis](../../strongs/g/g20.md) and [aphelotēs](../../strongs/g/g858.md) of [kardia](../../strongs/g/g2588.md),

<a name="acts_2_47"></a>Acts 2:47

[aineō](../../strongs/g/g134.md) [theos](../../strongs/g/g2316.md), and having [charis](../../strongs/g/g5485.md) with all the [laos](../../strongs/g/g2992.md). And the [kyrios](../../strongs/g/g2962.md) [prostithēmi](../../strongs/g/g4369.md) to the [ekklēsia](../../strongs/g/g1577.md) [kata](../../strongs/g/g2596.md) [hēmera](../../strongs/g/g2250.md) [sōzō](../../strongs/g/g4982.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 1](acts_1.md) - [Acts 3](acts_3.md)

---

[^1]: [Acts 2:30 Commentary](../../commentary/acts/acts_2_commentary.md#acts_2_30)
