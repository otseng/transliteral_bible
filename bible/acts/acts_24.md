# [Acts 24](https://www.blueletterbible.org/kjv/act/24/1/s_1042001)

<a name="acts_24_1"></a>Acts 24:1

And after five [hēmera](../../strongs/g/g2250.md) [Hananias](../../strongs/g/g367.md) the [archiereus](../../strongs/g/g749.md) [katabainō](../../strongs/g/g2597.md) with the [presbyteros](../../strongs/g/g4245.md), and a certain [rhētōr](../../strongs/g/g4489.md) [Tertyllos](../../strongs/g/g5061.md), who [emphanizō](../../strongs/g/g1718.md) the [hēgemōn](../../strongs/g/g2232.md) against [Paulos](../../strongs/g/g3972.md).

<a name="acts_24_2"></a>Acts 24:2

And when he was [kaleō](../../strongs/g/g2564.md), [Tertyllos](../../strongs/g/g5061.md) [archomai](../../strongs/g/g756.md) to [katēgoreō](../../strongs/g/g2723.md), [legō](../../strongs/g/g3004.md), [dia](../../strongs/g/g1223.md) thee we [tygchanō](../../strongs/g/g5177.md) [polys](../../strongs/g/g4183.md) [eirēnē](../../strongs/g/g1515.md), and that [diorthōma](../../strongs/g/g2735.md) are [ginomai](../../strongs/g/g1096.md) unto this [ethnos](../../strongs/g/g1484.md) by thy [pronoia](../../strongs/g/g4307.md),

<a name="acts_24_3"></a>Acts 24:3

We [apodechomai](../../strongs/g/g588.md) it [pantē](../../strongs/g/g3839.md), and [pantachou](../../strongs/g/g3837.md), [kratistos](../../strongs/g/g2903.md) [Phēlix](../../strongs/g/g5344.md), with all [eucharistia](../../strongs/g/g2169.md).

<a name="acts_24_4"></a>Acts 24:4

Notwithstanding, that I be not further [egkoptō](../../strongs/g/g1465.md) thee, I [parakaleō](../../strongs/g/g3870.md) thee that thou wouldest [akouō](../../strongs/g/g191.md) us of thy [epieikeia](../../strongs/g/g1932.md) [syntomōs](../../strongs/g/g4935.md).

<a name="acts_24_5"></a>Acts 24:5

For we have [heuriskō](../../strongs/g/g2147.md) this [anēr](../../strongs/g/g435.md) a [loimos](../../strongs/g/g3061.md), and a [kineō](../../strongs/g/g2795.md) of [stasis](../../strongs/g/g4714.md) among all the [Ioudaios](../../strongs/g/g2453.md) throughout the [oikoumenē](../../strongs/g/g3625.md), and a [prōtostatēs](../../strongs/g/g4414.md) of the [hairesis](../../strongs/g/g139.md) of the [Nazōraios](../../strongs/g/g3480.md):

<a name="acts_24_6"></a>Acts 24:6

Who also hath [peirazō](../../strongs/g/g3985.md) to [bebēloō](../../strongs/g/g953.md) the [hieron](../../strongs/g/g2411.md): whom we [krateō](../../strongs/g/g2902.md), and would have [krinō](../../strongs/g/g2919.md) according to [hēmeteros](../../strongs/g/g2251.md) [nomos](../../strongs/g/g3551.md). [^1]

<a name="acts_24_7"></a>Acts 24:7

But the [chiliarchos](../../strongs/g/g5506.md) [Lysias](../../strongs/g/g3079.md) [parerchomai](../../strongs/g/g3928.md) upon us, and with [polys](../../strongs/g/g4183.md) [bia](../../strongs/g/g970.md) [apagō](../../strongs/g/g520.md) out of our [cheir](../../strongs/g/g5495.md),

<a name="acts_24_8"></a>Acts 24:8

[keleuō](../../strongs/g/g2753.md) his [katēgoros](../../strongs/g/g2725.md) to [erchomai](../../strongs/g/g2064.md) unto thee: by [anakrinō](../../strongs/g/g350.md) of whom thyself mayest [epiginōskō](../../strongs/g/g1921.md) of all these things, whereof we [katēgoreō](../../strongs/g/g2723.md) him.

<a name="acts_24_9"></a>Acts 24:9

And the [Ioudaios](../../strongs/g/g2453.md) also [syntithēmi](../../strongs/g/g4934.md), [phasko](../../strongs/g/g5335.md) that these things were so.

<a name="acts_24_10"></a>Acts 24:10

Then [Paulos](../../strongs/g/g3972.md), after that the [hēgemōn](../../strongs/g/g2232.md) had [neuō](../../strongs/g/g3506.md) unto him to [legō](../../strongs/g/g3004.md), [apokrinomai](../../strongs/g/g611.md), Forasmuch as I [epistamai](../../strongs/g/g1987.md) that thou hast been of [polys](../../strongs/g/g4183.md) [etos](../../strongs/g/g2094.md) a [kritēs](../../strongs/g/g2923.md) unto this [ethnos](../../strongs/g/g1484.md), I do the more [euthymos](../../strongs/g/g2115.md) [apologeomai](../../strongs/g/g626.md) for myself:

<a name="acts_24_11"></a>Acts 24:11

Because that thou mayest [ginōskō](../../strongs/g/g1097.md), that there are yet but twelve [hēmera](../../strongs/g/g2250.md) since I [anabainō](../../strongs/g/g305.md) to [Ierousalēm](../../strongs/g/g2419.md) for to [proskyneō](../../strongs/g/g4352.md).

<a name="acts_24_12"></a>Acts 24:12

And they neither [heuriskō](../../strongs/g/g2147.md) me in the [hieron](../../strongs/g/g2411.md) [dialegomai](../../strongs/g/g1256.md) with any man, neither [epistasis](../../strongs/g/g1999.md) [poieō](../../strongs/g/g4160.md) the [ochlos](../../strongs/g/g3793.md), neither in the [synagōgē](../../strongs/g/g4864.md), nor in the [polis](../../strongs/g/g4172.md):

<a name="acts_24_13"></a>Acts 24:13

Neither can they [paristēmi](../../strongs/g/g3936.md) the things whereof they now [katēgoreō](../../strongs/g/g2723.md) me.

<a name="acts_24_14"></a>Acts 24:14

But this I [homologeō](../../strongs/g/g3670.md) unto thee, that after the [hodos](../../strongs/g/g3598.md) which they [legō](../../strongs/g/g3004.md) [hairesis](../../strongs/g/g139.md), so [latreuō](../../strongs/g/g3000.md) I the [theos](../../strongs/g/g2316.md) of my [patrōos](../../strongs/g/g3971.md), [pisteuō](../../strongs/g/g4100.md) all things which are [graphō](../../strongs/g/g1125.md) in the [nomos](../../strongs/g/g3551.md) and in the [prophētēs](../../strongs/g/g4396.md):

<a name="acts_24_15"></a>Acts 24:15

And have [elpis](../../strongs/g/g1680.md) toward [theos](../../strongs/g/g2316.md), which they themselves also [prosdechomai](../../strongs/g/g4327.md), that there shall be an [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md), both of the [dikaios](../../strongs/g/g1342.md) and [adikos](../../strongs/g/g94.md).

<a name="acts_24_16"></a>Acts 24:16

And herein do I [askeō](../../strongs/g/g778.md) myself, to have [diapantos](../../strongs/g/g1275.md) a [syneidēsis](../../strongs/g/g4893.md) [aproskopos](../../strongs/g/g677.md) toward [theos](../../strongs/g/g2316.md), and toward [anthrōpos](../../strongs/g/g444.md).

<a name="acts_24_17"></a>Acts 24:17

Now after many [etos](../../strongs/g/g2094.md) I [paraginomai](../../strongs/g/g3854.md) to [poieō](../../strongs/g/g4160.md) [eleēmosynē](../../strongs/g/g1654.md) to my [ethnos](../../strongs/g/g1484.md), and [prosphora](../../strongs/g/g4376.md).

<a name="acts_24_18"></a>Acts 24:18

Whereupon certain [Ioudaios](../../strongs/g/g2453.md) from [Asia](../../strongs/g/g773.md) [heuriskō](../../strongs/g/g2147.md) me [hagnizō](../../strongs/g/g48.md) in the [hieron](../../strongs/g/g2411.md), neither with [ochlos](../../strongs/g/g3793.md), nor with [thorybos](../../strongs/g/g2351.md).

<a name="acts_24_19"></a>Acts 24:19

Who ought to have been [pareimi](../../strongs/g/g3918.md) before thee, and [katēgoreō](../../strongs/g/g2723.md), if they had ought against me.

<a name="acts_24_20"></a>Acts 24:20

Or else let these same [eipon](../../strongs/g/g2036.md), if they have [heuriskō](../../strongs/g/g2147.md) any [adikēma](../../strongs/g/g92.md) in me, while I [histēmi](../../strongs/g/g2476.md) before the [synedrion](../../strongs/g/g4892.md),

<a name="acts_24_21"></a>Acts 24:21

Except it be for this one [phōnē](../../strongs/g/g5456.md), that I [krazō](../../strongs/g/g2896.md) [histēmi](../../strongs/g/g2476.md) among them, Touching the [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md) I am [krinō](../../strongs/g/g2919.md) by you [sēmeron](../../strongs/g/g4594.md).

<a name="acts_24_22"></a>Acts 24:22

And when [Phēlix](../../strongs/g/g5344.md) [akouō](../../strongs/g/g191.md) these things, having more [akribesteron](../../strongs/g/g197.md) [eidō](../../strongs/g/g1492.md) of [hodos](../../strongs/g/g3598.md), he [anaballō](../../strongs/g/g306.md) them, and [eipon](../../strongs/g/g2036.md), When [Lysias](../../strongs/g/g3079.md) the [chiliarchos](../../strongs/g/g5506.md) shall [katabainō](../../strongs/g/g2597.md), I will [diaginōskō](../../strongs/g/g1231.md) of your matter.

<a name="acts_24_23"></a>Acts 24:23

And he [diatassō](../../strongs/g/g1299.md) a [hekatontarchēs](../../strongs/g/g1543.md) to [tēreō](../../strongs/g/g5083.md) [Paulos](../../strongs/g/g3972.md), and to let have [anesis](../../strongs/g/g425.md), and that he should [kōlyō](../../strongs/g/g2967.md) [mēdeis](../../strongs/g/g3367.md) of his [idios](../../strongs/g/g2398.md) to [hypēreteō](../../strongs/g/g5256.md) or [proserchomai](../../strongs/g/g4334.md) unto him.

<a name="acts_24_24"></a>Acts 24:24

And after certain [hēmera](../../strongs/g/g2250.md), when [Phēlix](../../strongs/g/g5344.md) [paraginomai](../../strongs/g/g3854.md) with his [gynē](../../strongs/g/g1135.md) [Drousilla](../../strongs/g/g1409.md), which was an [Ioudaios](../../strongs/g/g2453.md), he [metapempō](../../strongs/g/g3343.md) [Paulos](../../strongs/g/g3972.md), and [akouō](../../strongs/g/g191.md) him concerning the [pistis](../../strongs/g/g4102.md) in [Christos](../../strongs/g/g5547.md).

<a name="acts_24_25"></a>Acts 24:25

And as he [dialegomai](../../strongs/g/g1256.md) of [dikaiosynē](../../strongs/g/g1343.md), [egkrateia](../../strongs/g/g1466.md), and [krima](../../strongs/g/g2917.md) [mellō](../../strongs/g/g3195.md), [Phēlix](../../strongs/g/g5344.md) [ginomai](../../strongs/g/g1096.md) [emphobos](../../strongs/g/g1719.md), and [apokrinomai](../../strongs/g/g611.md), [poreuō](../../strongs/g/g4198.md) for this time; when I [metalambanō](../../strongs/g/g3335.md) a [kairos](../../strongs/g/g2540.md), I will [metakaleō](../../strongs/g/g3333.md) thee.

<a name="acts_24_26"></a>Acts 24:26

He [elpizō](../../strongs/g/g1679.md) also that [chrēma](../../strongs/g/g5536.md) should have been [didōmi](../../strongs/g/g1325.md) him of [Paulos](../../strongs/g/g3972.md), that he might [lyō](../../strongs/g/g3089.md) him: wherefore he [metapempō](../../strongs/g/g3343.md) for him [pyknos](../../strongs/g/g4437.md), and [homileō](../../strongs/g/g3656.md) with him.

<a name="acts_24_27"></a>Acts 24:27

But [plēroō](../../strongs/g/g4137.md) [dietia](../../strongs/g/g1333.md) [Porkios](../../strongs/g/g4201.md) [Phēstos](../../strongs/g/g5347.md) [lambanō](../../strongs/g/g2983.md) into [Phēlix](../../strongs/g/g5344.md) [diadochos](../../strongs/g/g1240.md): and [Phēlix](../../strongs/g/g5344.md), [thelō](../../strongs/g/g2309.md) to [katatithēmi](../../strongs/g/g2698.md) the [Ioudaios](../../strongs/g/g2453.md) [charis](../../strongs/g/g5485.md), [kataleipō](../../strongs/g/g2641.md) [Paulos](../../strongs/g/g3972.md) [deō](../../strongs/g/g1210.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 23](acts_23.md) - [Acts 25](acts_25.md)

---

[^1]: [Acts 24:6 Commentary](../../commentary/acts/acts_24_commentary.md#acts_24_6)
