# [Acts 15](https://www.blueletterbible.org/kjv/act/15/1/s_1033001)

<a name="acts_15_1"></a>Acts 15:1

And [tis](../../strongs/g/g5100.md) which [katerchomai](../../strongs/g/g2718.md) from [Ioudaia](../../strongs/g/g2449.md) [didaskō](../../strongs/g/g1321.md) the [adelphos](../../strongs/g/g80.md), Except ye be [peritemnō](../../strongs/g/g4059.md) after the [ethos](../../strongs/g/g1485.md) of [Mōÿsēs](../../strongs/g/g3475.md), ye cannot be [sōzō](../../strongs/g/g4982.md).

<a name="acts_15_2"></a>Acts 15:2

When therefore [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md) had no [oligos](../../strongs/g/g3641.md) [stasis](../../strongs/g/g4714.md) and [syzētēsis](../../strongs/g/g4803.md) with them, they [tassō](../../strongs/g/g5021.md) that [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md), and certain other of them, should [anabainō](../../strongs/g/g305.md) to [Ierousalēm](../../strongs/g/g2419.md) unto the [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md) about this [zētēma](../../strongs/g/g2213.md).

<a name="acts_15_3"></a>Acts 15:3

And being [propempō](../../strongs/g/g4311.md) by the [ekklēsia](../../strongs/g/g1577.md), they [dierchomai](../../strongs/g/g1330.md) [Phoinikē](../../strongs/g/g5403.md) and [Samareia](../../strongs/g/g4540.md), [ekdiēgeomai](../../strongs/g/g1555.md) the [epistrophē](../../strongs/g/g1995.md) of the [ethnos](../../strongs/g/g1484.md): and they [poieō](../../strongs/g/g4160.md) [megas](../../strongs/g/g3173.md) [chara](../../strongs/g/g5479.md) unto all the [adelphos](../../strongs/g/g80.md).

<a name="acts_15_4"></a>Acts 15:4

And when they were [paraginomai](../../strongs/g/g3854.md) to [Ierousalēm](../../strongs/g/g2419.md), they were [apodechomai](../../strongs/g/g588.md) of the [ekklēsia](../../strongs/g/g1577.md), and of the [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md), and they [anaggellō](../../strongs/g/g312.md) all things that [theos](../../strongs/g/g2316.md) had [poieō](../../strongs/g/g4160.md) with them.

<a name="acts_15_5"></a>Acts 15:5

But there [exanistēmi](../../strongs/g/g1817.md) certain of the [hairesis](../../strongs/g/g139.md) of the [Pharisaios](../../strongs/g/g5330.md) which [pisteuō](../../strongs/g/g4100.md), [legō](../../strongs/g/g3004.md), That it was [dei](../../strongs/g/g1163.md) to [peritemnō](../../strongs/g/g4059.md) them, and to [paraggellō](../../strongs/g/g3853.md) them to [tēreō](../../strongs/g/g5083.md) the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md).

<a name="acts_15_6"></a>Acts 15:6

And the [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md) [synagō](../../strongs/g/g4863.md) for to [eidō](../../strongs/g/g1492.md) of this [logos](../../strongs/g/g3056.md).

<a name="acts_15_7"></a>Acts 15:7

And when there had been [polys](../../strongs/g/g4183.md) [syzētēsis](../../strongs/g/g4803.md), [Petros](../../strongs/g/g4074.md) [anistēmi](../../strongs/g/g450.md), and [eipon](../../strongs/g/g2036.md) unto them, [anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), ye [epistamai](../../strongs/g/g1987.md) how that an [archaios](../../strongs/g/g744.md) [hēmera](../../strongs/g/g2250.md) ago [theos](../../strongs/g/g2316.md) [eklegomai](../../strongs/g/g1586.md) among us, that the [ethnos](../../strongs/g/g1484.md) by my [stoma](../../strongs/g/g4750.md) should [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of the [euaggelion](../../strongs/g/g2098.md), and [pisteuō](../../strongs/g/g4100.md).

<a name="acts_15_8"></a>Acts 15:8

And [theos](../../strongs/g/g2316.md), which [kardiognōstēs](../../strongs/g/g2589.md), bare them [martyreō](../../strongs/g/g3140.md), [didōmi](../../strongs/g/g1325.md) them the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), even as he did unto us;

<a name="acts_15_9"></a>Acts 15:9

And [diakrinō](../../strongs/g/g1252.md) [oudeis](../../strongs/g/g3762.md) between us and them, [katharizō](../../strongs/g/g2511.md) their [kardia](../../strongs/g/g2588.md) by [pistis](../../strongs/g/g4102.md).

<a name="acts_15_10"></a>Acts 15:10

Now therefore why [peirazō](../../strongs/g/g3985.md) [theos](../../strongs/g/g2316.md), to [epitithēmi](../../strongs/g/g2007.md) a [zygos](../../strongs/g/g2218.md) upon the [trachēlos](../../strongs/g/g5137.md) of the [mathētēs](../../strongs/g/g3101.md), which neither our [patēr](../../strongs/g/g3962.md) nor we were able to [bastazō](../../strongs/g/g941.md)?

<a name="acts_15_11"></a>Acts 15:11

But we [pisteuō](../../strongs/g/g4100.md) that through the [charis](../../strongs/g/g5485.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) we shall be [sōzō](../../strongs/g/g4982.md), even as they.

<a name="acts_15_12"></a>Acts 15:12

Then all the [plēthos](../../strongs/g/g4128.md) [sigaō](../../strongs/g/g4601.md), and [akouō](../../strongs/g/g191.md) to [Barnabas](../../strongs/g/g921.md) and [Paulos](../../strongs/g/g3972.md), [exēgeomai](../../strongs/g/g1834.md) what [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md) [theos](../../strongs/g/g2316.md) had [poieō](../../strongs/g/g4160.md) among the [ethnos](../../strongs/g/g1484.md) by them.

<a name="acts_15_13"></a>Acts 15:13

And after they had [sigaō](../../strongs/g/g4601.md), [Iakōbos](../../strongs/g/g2385.md) [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md), [anēr](../../strongs/g/g435.md) and [adelphos](../../strongs/g/g80.md), [akouō](../../strongs/g/g191.md) unto me:

<a name="acts_15_14"></a>Acts 15:14

[Symeōn](../../strongs/g/g4826.md) hath [exēgeomai](../../strongs/g/g1834.md) how [theos](../../strongs/g/g2316.md) at the first did [episkeptomai](../../strongs/g/g1980.md) the [ethnos](../../strongs/g/g1484.md), to [lambanō](../../strongs/g/g2983.md) out of them a [laos](../../strongs/g/g2992.md) for his [onoma](../../strongs/g/g3686.md).

<a name="acts_15_15"></a>Acts 15:15

And to this [symphōneō](../../strongs/g/g4856.md) the [logos](../../strongs/g/g3056.md) of the [prophētēs](../../strongs/g/g4396.md); as it is [graphō](../../strongs/g/g1125.md),

<a name="acts_15_16"></a>Acts 15:16

After this I will [anastrephō](../../strongs/g/g390.md), and will [anoikodomeō](../../strongs/g/g456.md) the [skēnē](../../strongs/g/g4633.md) of [Dabid](../../strongs/g/g1138.md), which is [piptō](../../strongs/g/g4098.md); and I will [anoikodomeō](../../strongs/g/g456.md) the [kataskaptō](../../strongs/g/g2679.md) thereof, and I will [anorthoō](../../strongs/g/g461.md) it:

<a name="acts_15_17"></a>Acts 15:17

That the [kataloipos](../../strongs/g/g2645.md) of [anthrōpos](../../strongs/g/g444.md) might [ekzēteō](../../strongs/g/g1567.md) the [kyrios](../../strongs/g/g2962.md), and all the [ethnos](../../strongs/g/g1484.md), upon whom my [onoma](../../strongs/g/g3686.md) is [epikaleō](../../strongs/g/g1941.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), who [poieō](../../strongs/g/g4160.md) all these things.

<a name="acts_15_18"></a>Acts 15:18

[gnōstos](../../strongs/g/g1110.md) unto [theos](../../strongs/g/g2316.md) are all his [ergon](../../strongs/g/g2041.md) from [aiōn](../../strongs/g/g165.md).

<a name="acts_15_19"></a>Acts 15:19

Wherefore my [krinō](../../strongs/g/g2919.md) is, that we [parenochleō](../../strongs/g/g3926.md) not them, which from among the [ethnos](../../strongs/g/g1484.md) are [epistrephō](../../strongs/g/g1994.md) to [theos](../../strongs/g/g2316.md):

<a name="acts_15_20"></a>Acts 15:20

But that we [epistellō](../../strongs/g/g1989.md) unto them, that they [apechomai](../../strongs/g/g567.md) from [alisgēma](../../strongs/g/g234.md) of [eidōlon](../../strongs/g/g1497.md), and [porneia](../../strongs/g/g4202.md), and [pniktos](../../strongs/g/g4156.md), and [haima](../../strongs/g/g129.md).

<a name="acts_15_21"></a>Acts 15:21

For [Mōÿsēs](../../strongs/g/g3475.md) of [archaios](../../strongs/g/g744.md) [genea](../../strongs/g/g1074.md) hath in every [polis](../../strongs/g/g4172.md) them that [kēryssō](../../strongs/g/g2784.md) him, being [anaginōskō](../../strongs/g/g314.md) in the [synagōgē](../../strongs/g/g4864.md) every [sabbaton](../../strongs/g/g4521.md).

<a name="acts_15_22"></a>Acts 15:22

Then [dokeō](../../strongs/g/g1380.md) the [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md) with the whole [ekklēsia](../../strongs/g/g1577.md), to [pempō](../../strongs/g/g3992.md) [eklegomai](../../strongs/g/g1586.md) [anēr](../../strongs/g/g435.md) of their own company to [Antiocheia](../../strongs/g/g490.md) with [Paulos](../../strongs/g/g3972.md) and [Barnabas](../../strongs/g/g921.md); namely, [Ioudas](../../strongs/g/g2455.md) [epikaleō](../../strongs/g/g1941.md) [Barsabas](../../strongs/g/g923.md) and [Silas](../../strongs/g/g4609.md), [hēgeomai](../../strongs/g/g2233.md) [anēr](../../strongs/g/g435.md) among the [adelphos](../../strongs/g/g80.md):

<a name="acts_15_23"></a>Acts 15:23

And they [graphō](../../strongs/g/g1125.md) by [cheir](../../strongs/g/g5495.md) after this manner; The [apostolos](../../strongs/g/g652.md) and [presbyteros](../../strongs/g/g4245.md) and [adelphos](../../strongs/g/g80.md) [chairō](../../strongs/g/g5463.md) unto the [adelphos](../../strongs/g/g80.md) which are of the [ethnos](../../strongs/g/g1484.md) in [Antiocheia](../../strongs/g/g490.md) and [Syria](../../strongs/g/g4947.md) and [Kilikia](../../strongs/g/g2791.md).

<a name="acts_15_24"></a>Acts 15:24

Forasmuch as we have [akouō](../../strongs/g/g191.md), that certain which [exerchomai](../../strongs/g/g1831.md) from us have [tarassō](../../strongs/g/g5015.md) you with [logos](../../strongs/g/g3056.md), [anaskeuazō](../../strongs/g/g384.md) your [psychē](../../strongs/g/g5590.md), [legō](../../strongs/g/g3004.md), be [peritemnō](../../strongs/g/g4059.md), and [tēreō](../../strongs/g/g5083.md) the [nomos](../../strongs/g/g3551.md): to whom we no [diastellō](../../strongs/g/g1291.md): [^1]

<a name="acts_15_25"></a>Acts 15:25

It [dokeō](../../strongs/g/g1380.md) unto us, [ginomai](../../strongs/g/g1096.md) with [homothymadon](../../strongs/g/g3661.md), to [pempō](../../strongs/g/g3992.md) [eklegomai](../../strongs/g/g1586.md) [anēr](../../strongs/g/g435.md) unto you with our [agapētos](../../strongs/g/g27.md) [Barnabas](../../strongs/g/g921.md) and [Paulos](../../strongs/g/g3972.md),

<a name="acts_15_26"></a>Acts 15:26

[anthrōpos](../../strongs/g/g444.md) that have [paradidōmi](../../strongs/g/g3860.md) their [psychē](../../strongs/g/g5590.md) for the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="acts_15_27"></a>Acts 15:27

We have [apostellō](../../strongs/g/g649.md) therefore [Ioudas](../../strongs/g/g2455.md) and [Silas](../../strongs/g/g4609.md), who shall also [apaggellō](../../strongs/g/g518.md) you the same things by [logos](../../strongs/g/g3056.md).

<a name="acts_15_28"></a>Acts 15:28

For it [dokeō](../../strongs/g/g1380.md) to the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and to us, to [epitithēmi](../../strongs/g/g2007.md) you [mēdeis](../../strongs/g/g3367.md) [pleiōn](../../strongs/g/g4119.md) [baros](../../strongs/g/g922.md) than these [epanagkes](../../strongs/g/g1876.md);

<a name="acts_15_29"></a>Acts 15:29

That ye [apechomai](../../strongs/g/g567.md) from [eidōlothytos](../../strongs/g/g1494.md), and from [haima](../../strongs/g/g129.md), and from [pniktos](../../strongs/g/g4156.md), and from [porneia](../../strongs/g/g4202.md): from which if ye [diatēreō](../../strongs/g/g1301.md) yourselves, ye shall [prassō](../../strongs/g/g4238.md) [eu](../../strongs/g/g2095.md). [rhōnnymi](../../strongs/g/g4517.md).

<a name="acts_15_30"></a>Acts 15:30

So when they were [apolyō](../../strongs/g/g630.md), they [erchomai](../../strongs/g/g2064.md) to [Antiocheia](../../strongs/g/g490.md): and when they had [synagō](../../strongs/g/g4863.md) the [plēthos](../../strongs/g/g4128.md), they [epididōmi](../../strongs/g/g1929.md) the [epistolē](../../strongs/g/g1992.md):

<a name="acts_15_31"></a>Acts 15:31

when they had [anaginōskō](../../strongs/g/g314.md), they [chairō](../../strongs/g/g5463.md) for the [paraklēsis](../../strongs/g/g3874.md).

<a name="acts_15_32"></a>Acts 15:32

And [Ioudas](../../strongs/g/g2455.md) and [Silas](../../strongs/g/g4609.md), being [prophētēs](../../strongs/g/g4396.md) also themselves, [parakaleō](../../strongs/g/g3870.md) the [adelphos](../../strongs/g/g80.md) with [polys](../../strongs/g/g4183.md) [logos](../../strongs/g/g3056.md), and [epistērizō](../../strongs/g/g1991.md) them.

<a name="acts_15_33"></a>Acts 15:33

And after they had [poieō](../../strongs/g/g4160.md) a [chronos](../../strongs/g/g5550.md), they were [apolyō](../../strongs/g/g630.md) in [eirēnē](../../strongs/g/g1515.md) from the [adelphos](../../strongs/g/g80.md) unto the [apostolos](../../strongs/g/g652.md).

<a name="acts_15_34"></a>Acts 15:34

Notwithstanding it [dokeō](../../strongs/g/g1380.md) [Silas](../../strongs/g/g4609.md) to [epimenō](../../strongs/g/g1961.md) there. [^2]

<a name="acts_15_35"></a>Acts 15:35

[Paulos](../../strongs/g/g3972.md) also and [Barnabas](../../strongs/g/g921.md) [diatribō](../../strongs/g/g1304.md) in [Antiocheia](../../strongs/g/g490.md), [didaskō](../../strongs/g/g1321.md) and [euaggelizō](../../strongs/g/g2097.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), with [polys](../../strongs/g/g4183.md) others also.

<a name="acts_15_36"></a>Acts 15:36

And some [hēmera](../../strongs/g/g2250.md) after [Paulos](../../strongs/g/g3972.md) [eipon](../../strongs/g/g2036.md) unto [Barnabas](../../strongs/g/g921.md), Let us [epistrephō](../../strongs/g/g1994.md) and [episkeptomai](../../strongs/g/g1980.md) our [adelphos](../../strongs/g/g80.md) in every [polis](../../strongs/g/g4172.md) where we have [kataggellō](../../strongs/g/g2605.md) the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), how they do.

<a name="acts_15_37"></a>Acts 15:37

And [Barnabas](../../strongs/g/g921.md) [bouleuō](../../strongs/g/g1011.md) to [symparalambanō](../../strongs/g/g4838.md) them [Iōannēs](../../strongs/g/g2491.md), [kaleō](../../strongs/g/g2564.md) [Markos](../../strongs/g/g3138.md).

<a name="acts_15_38"></a>Acts 15:38

But [Paulos](../../strongs/g/g3972.md) not [axioō](../../strongs/g/g515.md) to [symparalambanō](../../strongs/g/g4838.md) him, who [aphistēmi](../../strongs/g/g868.md) from them from [Pamphylia](../../strongs/g/g3828.md), and [synerchomai](../../strongs/g/g4905.md) not with them to the [ergon](../../strongs/g/g2041.md).

<a name="acts_15_39"></a>Acts 15:39

And the [paroxysmos](../../strongs/g/g3948.md) was between them, that they [apochōrizō](../../strongs/g/g673.md) one from [allēlōn](../../strongs/g/g240.md): and so [Barnabas](../../strongs/g/g921.md) [paralambanō](../../strongs/g/g3880.md) [Markos](../../strongs/g/g3138.md), and [ekpleō](../../strongs/g/g1602.md) unto [Kypros](../../strongs/g/g2954.md);

<a name="acts_15_40"></a>Acts 15:40

And [Paulos](../../strongs/g/g3972.md) [epilegō](../../strongs/g/g1951.md) [Silas](../../strongs/g/g4609.md), and [exerchomai](../../strongs/g/g1831.md), [paradidōmi](../../strongs/g/g3860.md) by the [adelphos](../../strongs/g/g80.md) unto the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md).

<a name="acts_15_41"></a>Acts 15:41

And he [dierchomai](../../strongs/g/g1330.md) [Syria](../../strongs/g/g4947.md) and [Kilikia](../../strongs/g/g2791.md), [epistērizō](../../strongs/g/g1991.md) the [ekklēsia](../../strongs/g/g1577.md).

---

[Transliteral Bible](../bible.md)

[Acts](acts.md)

[Acts 14](acts_14.md) - [Acts 16](acts_16.md)

---

[^1]: [Acts 15:24 Commentary](../../commentary/acts/acts_15_commentary.md#acts_15_24)

[^2]: [Acts 15:34 Commentary](../../commentary/acts/acts_15_commentary.md#acts_15_34)
