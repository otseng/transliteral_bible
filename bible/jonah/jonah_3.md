# [Jonah 3](https://www.blueletterbible.org/kjv/jonah/3)

<a name="jonah_3_1"></a>Jonah 3:1

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yonah](../../strongs/h/h3124.md) the second time, ['āmar](../../strongs/h/h559.md),

<a name="jonah_3_2"></a>Jonah 3:2

[quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) unto [Nînvê](../../strongs/h/h5210.md), that [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) unto it the [qᵊrî'â](../../strongs/h/h7150.md) that I [dabar](../../strongs/h/h1696.md) thee.

<a name="jonah_3_3"></a>Jonah 3:3

So [Yonah](../../strongs/h/h3124.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) unto [Nînvê](../../strongs/h/h5210.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md). Now [Nînvê](../../strongs/h/h5210.md) was an ['Elohiym](../../strongs/h/h430.md) [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md) of three [yowm](../../strongs/h/h3117.md) [mahălāḵ](../../strongs/h/h4109.md).

<a name="jonah_3_4"></a>Jonah 3:4

And [Yonah](../../strongs/h/h3124.md) [ḥālal](../../strongs/h/h2490.md) to [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md) a [yowm](../../strongs/h/h3117.md) [mahălāḵ](../../strongs/h/h4109.md), and he [qara'](../../strongs/h/h7121.md), and ['āmar](../../strongs/h/h559.md), Yet ['arbāʿîm](../../strongs/h/h705.md) [yowm](../../strongs/h/h3117.md), and [Nînvê](../../strongs/h/h5210.md) shall be [hāp̄aḵ](../../strongs/h/h2015.md).

<a name="jonah_3_5"></a>Jonah 3:5

So the ['enowsh](../../strongs/h/h582.md) of [Nînvê](../../strongs/h/h5210.md) ['aman](../../strongs/h/h539.md) ['Elohiym](../../strongs/h/h430.md), and [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md), and [labash](../../strongs/h/h3847.md) on [śaq](../../strongs/h/h8242.md), from the [gadowl](../../strongs/h/h1419.md) of them even to the [qāṭān](../../strongs/h/h6996.md) of them.

<a name="jonah_3_6"></a>Jonah 3:6

For [dabar](../../strongs/h/h1697.md) [naga'](../../strongs/h/h5060.md) unto the [melek](../../strongs/h/h4428.md) of [Nînvê](../../strongs/h/h5210.md), and he [quwm](../../strongs/h/h6965.md) from his [kicce'](../../strongs/h/h3678.md), and he ['abar](../../strongs/h/h5674.md) his ['adereṯ](../../strongs/h/h155.md) from him, and [kāsâ](../../strongs/h/h3680.md) him with [śaq](../../strongs/h/h8242.md), and [yashab](../../strongs/h/h3427.md) in ['ēp̄er](../../strongs/h/h665.md).

<a name="jonah_3_7"></a>Jonah 3:7

And he caused it to be [zāʿaq](../../strongs/h/h2199.md) and ['āmar](../../strongs/h/h559.md) through [Nînvê](../../strongs/h/h5210.md) by the [ṭaʿam](../../strongs/h/h2940.md) of the [melek](../../strongs/h/h4428.md) and his [gadowl](../../strongs/h/h1419.md), ['āmar](../../strongs/h/h559.md), Let neither ['āḏām](../../strongs/h/h120.md) nor [bĕhemah](../../strongs/h/h929.md), [bāqār](../../strongs/h/h1241.md) nor [tso'n](../../strongs/h/h6629.md), [ṭāʿam](../../strongs/h/h2938.md) any [mᵊ'ûmâ](../../strongs/h/h3972.md): let them not [ra'ah](../../strongs/h/h7462.md), nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md):

<a name="jonah_3_8"></a>Jonah 3:8

But let ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md) be [kāsâ](../../strongs/h/h3680.md) with [śaq](../../strongs/h/h8242.md), and [qara'](../../strongs/h/h7121.md) [ḥāzqâ](../../strongs/h/h2394.md) unto ['Elohiym](../../strongs/h/h430.md): yea, let them [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and from the [chamac](../../strongs/h/h2555.md) that is in their [kaph](../../strongs/h/h3709.md).

<a name="jonah_3_9"></a>Jonah 3:9

Who can [yada'](../../strongs/h/h3045.md) if ['Elohiym](../../strongs/h/h430.md) will [shuwb](../../strongs/h/h7725.md) and [nacham](../../strongs/h/h5162.md), and [shuwb](../../strongs/h/h7725.md) from his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md), that we ['abad](../../strongs/h/h6.md) not?

<a name="jonah_3_10"></a>Jonah 3:10

And ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) their [ma'aseh](../../strongs/h/h4639.md), that they [shuwb](../../strongs/h/h7725.md) from their [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md); and ['Elohiym](../../strongs/h/h430.md) [nacham](../../strongs/h/h5162.md) of the [ra'](../../strongs/h/h7451.md), that he had [dabar](../../strongs/h/h1696.md) that he would ['asah](../../strongs/h/h6213.md) unto them; and he ['asah](../../strongs/h/h6213.md) it not.

---

[Transliteral Bible](../bible.md)

[Jonah](jonah.md)

[Jonah 2](jonah_2.md) - [Jonah 4](jonah_4.md)