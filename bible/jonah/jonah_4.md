# [Jonah 4](https://www.blueletterbible.org/kjv/jonah/4)

<a name="jonah_4_1"></a>Jonah 4:1

But it [yāraʿ](../../strongs/h/h3415.md) [Yonah](../../strongs/h/h3124.md) [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md), and he was [ḥārâ](../../strongs/h/h2734.md).

<a name="jonah_4_2"></a>Jonah 4:2

And he [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), I ['ānnā'](../../strongs/h/h577.md) thee, [Yĕhovah](../../strongs/h/h3068.md), was not this my [dabar](../../strongs/h/h1697.md), when I was yet in my ['ăḏāmâ](../../strongs/h/h127.md)? Therefore I [bāraḥ](../../strongs/h/h1272.md) [qadam](../../strongs/h/h6923.md) unto [Taršîš](../../strongs/h/h8659.md): for I [yada'](../../strongs/h/h3045.md) that thou art a [ḥanwn](../../strongs/h/h2587.md) ['el](../../strongs/h/h410.md), and [raḥwm](../../strongs/h/h7349.md), ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and of [rab](../../strongs/h/h7227.md) [checed](../../strongs/h/h2617.md), and [nacham](../../strongs/h/h5162.md) thee of the [ra'](../../strongs/h/h7451.md).

<a name="jonah_4_3"></a>Jonah 4:3

Therefore now, [Yĕhovah](../../strongs/h/h3068.md), [laqach](../../strongs/h/h3947.md), I beseech thee, my [nephesh](../../strongs/h/h5315.md) from me; for it is [towb](../../strongs/h/h2896.md) for me to [maveth](../../strongs/h/h4194.md) than to [chay](../../strongs/h/h2416.md).

<a name="jonah_4_4"></a>Jonah 4:4

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Doest thou [yatab](../../strongs/h/h3190.md) to be [ḥārâ](../../strongs/h/h2734.md)?

<a name="jonah_4_5"></a>Jonah 4:5

So [Yonah](../../strongs/h/h3124.md) [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md), and [yashab](../../strongs/h/h3427.md) on the [qeḏem](../../strongs/h/h6924.md) of the [ʿîr](../../strongs/h/h5892.md), and there ['asah](../../strongs/h/h6213.md) him a [cukkah](../../strongs/h/h5521.md), and [yashab](../../strongs/h/h3427.md) under it in the [ṣēl](../../strongs/h/h6738.md), till he might [ra'ah](../../strongs/h/h7200.md) what would become of the [ʿîr](../../strongs/h/h5892.md).

<a name="jonah_4_6"></a>Jonah 4:6

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) [mānâ](../../strongs/h/h4487.md) a [qîqāyôn](../../strongs/h/h7021.md) , and made it to [ʿālâ](../../strongs/h/h5927.md) over [Yonah](../../strongs/h/h3124.md), that it might be a [ṣēl](../../strongs/h/h6738.md) over his [ro'sh](../../strongs/h/h7218.md), to [natsal](../../strongs/h/h5337.md) him from his [ra'](../../strongs/h/h7451.md). So [Yonah](../../strongs/h/h3124.md) was [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md) [samach](../../strongs/h/h8055.md) of the [qîqāyôn](../../strongs/h/h7021.md).

<a name="jonah_4_7"></a>Jonah 4:7

But ['Elohiym](../../strongs/h/h430.md) [mānâ](../../strongs/h/h4487.md) a [tôlāʿ](../../strongs/h/h8438.md) when the [šaḥar](../../strongs/h/h7837.md) [ʿālâ](../../strongs/h/h5927.md) the next [māḥŏrāṯ](../../strongs/h/h4283.md), and it [nakah](../../strongs/h/h5221.md) the [qîqāyôn](../../strongs/h/h7021.md) that it [yāḇēš](../../strongs/h/h3001.md).

<a name="jonah_4_8"></a>Jonah 4:8

And it came to pass, when the [šemeš](../../strongs/h/h8121.md) did [zāraḥ](../../strongs/h/h2224.md), that ['Elohiym](../../strongs/h/h430.md) [mānâ](../../strongs/h/h4487.md) a [ḥărîšî](../../strongs/h/h2759.md) [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md); and the [šemeš](../../strongs/h/h8121.md) [nakah](../../strongs/h/h5221.md) upon the [ro'sh](../../strongs/h/h7218.md) of [Yonah](../../strongs/h/h3124.md), that he [ʿālap̄](../../strongs/h/h5968.md), and [sha'al](../../strongs/h/h7592.md) in [nephesh](../../strongs/h/h5315.md) to [muwth](../../strongs/h/h4191.md), and ['āmar](../../strongs/h/h559.md), It is [towb](../../strongs/h/h2896.md) for me to [maveth](../../strongs/h/h4194.md) than to [chay](../../strongs/h/h2416.md).

<a name="jonah_4_9"></a>Jonah 4:9

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) to [Yonah](../../strongs/h/h3124.md), Doest thou [yatab](../../strongs/h/h3190.md) to be [ḥārâ](../../strongs/h/h2734.md) for the [qîqāyôn](../../strongs/h/h7021.md)? And he ['āmar](../../strongs/h/h559.md), I do [yatab](../../strongs/h/h3190.md) to be [ḥārâ](../../strongs/h/h2734.md), even unto [maveth](../../strongs/h/h4194.md).

<a name="jonah_4_10"></a>Jonah 4:10

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Thou hast had [ḥûs](../../strongs/h/h2347.md) on the [qîqāyôn](../../strongs/h/h7021.md), for the which thou hast not [ʿāmal](../../strongs/h/h5998.md), neither madest it [gāḏal](../../strongs/h/h1431.md); which came up in a [ben](../../strongs/h/h1121.md) [layil](../../strongs/h/h3915.md), and ['abad](../../strongs/h/h6.md) in a [ben](../../strongs/h/h1121.md) [layil](../../strongs/h/h3915.md):

<a name="jonah_4_11"></a>Jonah 4:11

And should not I [ḥûs](../../strongs/h/h2347.md) [Nînvê](../../strongs/h/h5210.md), that [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md), wherein are [rabah](../../strongs/h/h7235.md) than sixscore [ribô'](../../strongs/h/h7239.md) ['āḏām](../../strongs/h/h120.md) that cannot [yada'](../../strongs/h/h3045.md) between their [yamiyn](../../strongs/h/h3225.md) and their [śᵊmō'l](../../strongs/h/h8040.md); and also [rab](../../strongs/h/h7227.md) [bĕhemah](../../strongs/h/h929.md)?

---

[Transliteral Bible](../bible.md)

[Jonah](jonah.md)

[Jonah 3](jonah_3.md)