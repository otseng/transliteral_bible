# [Jonah 1](https://www.blueletterbible.org/kjv/jonah/1)

<a name="jonah_1_1"></a>Jonah 1:1

Now the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Yonah](../../strongs/h/h3124.md) the [ben](../../strongs/h/h1121.md) of ['Ămitay](../../strongs/h/h573.md), ['āmar](../../strongs/h/h559.md),

<a name="jonah_1_2"></a>Jonah 1:2

[quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) to [Nînvê](../../strongs/h/h5210.md), that [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) against it; for their [ra'](../../strongs/h/h7451.md) is [ʿālâ](../../strongs/h/h5927.md) [paniym](../../strongs/h/h6440.md) me.

<a name="jonah_1_3"></a>Jonah 1:3

But [Yonah](../../strongs/h/h3124.md) [quwm](../../strongs/h/h6965.md) to [bāraḥ](../../strongs/h/h1272.md) unto [Taršîš](../../strongs/h/h8659.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yarad](../../strongs/h/h3381.md) to [Yāp̄Vô](../../strongs/h/h3305.md); and he [māṣā'](../../strongs/h/h4672.md) a ['ŏnîyâ](../../strongs/h/h591.md) [bow'](../../strongs/h/h935.md) to [Taršîš](../../strongs/h/h8659.md): so he [nathan](../../strongs/h/h5414.md) the [śāḵār](../../strongs/h/h7939.md) thereof, and [yarad](../../strongs/h/h3381.md) into it, to [bow'](../../strongs/h/h935.md) with them unto [Taršîš](../../strongs/h/h8659.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jonah_1_4"></a>Jonah 1:4

But [Yĕhovah](../../strongs/h/h3068.md) [ṭûl](../../strongs/h/h2904.md) a [gadowl](../../strongs/h/h1419.md) [ruwach](../../strongs/h/h7307.md) into the [yam](../../strongs/h/h3220.md), and there was a [gadowl](../../strongs/h/h1419.md) [saʿar](../../strongs/h/h5591.md) in the [yam](../../strongs/h/h3220.md), so that the ['ŏnîyâ](../../strongs/h/h591.md) was [chashab](../../strongs/h/h2803.md) to be [shabar](../../strongs/h/h7665.md).

<a name="jonah_1_5"></a>Jonah 1:5

Then the [mallāḥ](../../strongs/h/h4419.md) were [yare'](../../strongs/h/h3372.md), and [zāʿaq](../../strongs/h/h2199.md) every ['iysh](../../strongs/h/h376.md) unto his ['Elohiym](../../strongs/h/h430.md), and [ṭûl](../../strongs/h/h2904.md) the [kĕliy](../../strongs/h/h3627.md) that were in the ['ŏnîyâ](../../strongs/h/h591.md) into the [yam](../../strongs/h/h3220.md), to [qālal](../../strongs/h/h7043.md) it of them. But [Yonah](../../strongs/h/h3124.md) was [yarad](../../strongs/h/h3381.md) into the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [sᵊp̄înâ](../../strongs/h/h5600.md); and he [shakab](../../strongs/h/h7901.md), and was [rāḏam](../../strongs/h/h7290.md).

<a name="jonah_1_6"></a>Jonah 1:6

So the [rab](../../strongs/h/h7227.md) [ḥōḇēl](../../strongs/h/h2259.md) [qāraḇ](../../strongs/h/h7126.md) to him, and ['āmar](../../strongs/h/h559.md) unto him, What meanest thou, O [rāḏam](../../strongs/h/h7290.md)? [quwm](../../strongs/h/h6965.md), [qara'](../../strongs/h/h7121.md) upon thy ['Elohiym](../../strongs/h/h430.md), if so be that ['Elohiym](../../strongs/h/h430.md) will [ʿāšaṯ](../../strongs/h/h6245.md) upon us, that we ['abad](../../strongs/h/h6.md) not.

<a name="jonah_1_7"></a>Jonah 1:7

And they ['āmar](../../strongs/h/h559.md) every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md), [yālaḵ](../../strongs/h/h3212.md), and let us [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md), that we may [yada'](../../strongs/h/h3045.md) for whose [šel](../../strongs/h/h7945.md) this [ra'](../../strongs/h/h7451.md) is upon us. So they [naphal](../../strongs/h/h5307.md) [gôrāl](../../strongs/h/h1486.md), and the [gôrāl](../../strongs/h/h1486.md) [naphal](../../strongs/h/h5307.md) upon [Yonah](../../strongs/h/h3124.md).

<a name="jonah_1_8"></a>Jonah 1:8

Then ['āmar](../../strongs/h/h559.md) they unto him, [nāḡaḏ](../../strongs/h/h5046.md) us, we pray thee, for whose cause this [ra'](../../strongs/h/h7451.md) is upon us; What is thine [mĕla'kah](../../strongs/h/h4399.md)? and whence [bow'](../../strongs/h/h935.md) thou? what is thy ['erets](../../strongs/h/h776.md)? and of what ['am](../../strongs/h/h5971.md) art thou?

<a name="jonah_1_9"></a>Jonah 1:9

And he ['āmar](../../strongs/h/h559.md) unto them, I am an [ʿiḇrî](../../strongs/h/h5680.md); and I [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md), which hath ['asah](../../strongs/h/h6213.md) the [yam](../../strongs/h/h3220.md) and the [yabāšâ](../../strongs/h/h3004.md).

<a name="jonah_1_10"></a>Jonah 1:10

Then were the ['enowsh](../../strongs/h/h582.md) [gadowl](../../strongs/h/h1419.md) [yir'ah](../../strongs/h/h3374.md) [yare'](../../strongs/h/h3372.md), and ['āmar](../../strongs/h/h559.md) unto him, Why hast thou ['asah](../../strongs/h/h6213.md) this? For the ['enowsh](../../strongs/h/h582.md) [yada'](../../strongs/h/h3045.md) that he [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), because he had [nāḡaḏ](../../strongs/h/h5046.md) them.

<a name="jonah_1_11"></a>Jonah 1:11

Then ['āmar](../../strongs/h/h559.md) they unto him, What shall we ['asah](../../strongs/h/h6213.md) unto thee, that the [yam](../../strongs/h/h3220.md) may be [šāṯaq](../../strongs/h/h8367.md) unto us? for the [yam](../../strongs/h/h3220.md) [halak](../../strongs/h/h1980.md), and was [sāʿar](../../strongs/h/h5590.md).

<a name="jonah_1_12"></a>Jonah 1:12

And he ['āmar](../../strongs/h/h559.md) unto them, [nasa'](../../strongs/h/h5375.md) me, and [ṭûl](../../strongs/h/h2904.md) me into the [yam](../../strongs/h/h3220.md); so shall the [yam](../../strongs/h/h3220.md) be [šāṯaq](../../strongs/h/h8367.md) unto you: for I [yada'](../../strongs/h/h3045.md) that for my [šel](../../strongs/h/h7945.md) this [gadowl](../../strongs/h/h1419.md) [saʿar](../../strongs/h/h5591.md) is upon you.

<a name="jonah_1_13"></a>Jonah 1:13

Nevertheless the ['enowsh](../../strongs/h/h582.md) [ḥāṯar](../../strongs/h/h2864.md) to [shuwb](../../strongs/h/h7725.md) it to the [yabāšâ](../../strongs/h/h3004.md); but they [yakol](../../strongs/h/h3201.md) not: for the [yam](../../strongs/h/h3220.md) [halak](../../strongs/h/h1980.md), and was [sāʿar](../../strongs/h/h5590.md) against them.

<a name="jonah_1_14"></a>Jonah 1:14

Wherefore they [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), We ['ānnā'](../../strongs/h/h577.md), [Yĕhovah](../../strongs/h/h3068.md), we beseech thee, let us not ['abad](../../strongs/h/h6.md) for this ['iysh](../../strongs/h/h376.md) [nephesh](../../strongs/h/h5315.md), and [nathan](../../strongs/h/h5414.md) not upon us [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md): for thou, [Yĕhovah](../../strongs/h/h3068.md), hast ['asah](../../strongs/h/h6213.md) as it [ḥāp̄ēṣ](../../strongs/h/h2654.md) thee.

<a name="jonah_1_15"></a>Jonah 1:15

So they [nasa'](../../strongs/h/h5375.md) [Yonah](../../strongs/h/h3124.md), and [ṭûl](../../strongs/h/h2904.md) him into the [yam](../../strongs/h/h3220.md): and the [yam](../../strongs/h/h3220.md) ['amad](../../strongs/h/h5975.md) from her [zaʿap̄](../../strongs/h/h2197.md).

<a name="jonah_1_16"></a>Jonah 1:16

Then the ['enowsh](../../strongs/h/h582.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) [gadowl](../../strongs/h/h1419.md) [yir'ah](../../strongs/h/h3374.md), and [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [nāḏar](../../strongs/h/h5087.md) [neḏer](../../strongs/h/h5088.md).

<a name="jonah_1_17"></a>Jonah 1:17

Now [Yĕhovah](../../strongs/h/h3068.md) had [mānâ](../../strongs/h/h4487.md) a [gadowl](../../strongs/h/h1419.md) [dag](../../strongs/h/h1709.md) to [bālaʿ](../../strongs/h/h1104.md) [Yonah](../../strongs/h/h3124.md). And [Yonah](../../strongs/h/h3124.md) was in the [me'ah](../../strongs/h/h4578.md) of the [dag](../../strongs/h/h1709.md) [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md) and [šālôš](../h/h7969.md) [layil](../../strongs/h/h3915.md).

---

[Transliteral Bible](../bible.md)

[Jonah](jonah.md)

[Jonah 2](jonah_2.md)