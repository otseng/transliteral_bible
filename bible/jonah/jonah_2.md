# [Jonah 2](https://www.blueletterbible.org/kjv/jonah/2)

<a name="jonah_2_1"></a>Jonah 2:1

Then [Yonah](../../strongs/h/h3124.md) [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) out of the [dāḡâ](../../strongs/h/h1710.md) [me'ah](../../strongs/h/h4578.md),

<a name="jonah_2_2"></a>Jonah 2:2

And ['āmar](../../strongs/h/h559.md), I [qara'](../../strongs/h/h7121.md) by reason of mine [tsarah](../../strongs/h/h6869.md) unto [Yĕhovah](../../strongs/h/h3068.md), and he ['anah](../../strongs/h/h6030.md) me; out of the [beten](../../strongs/h/h990.md) of [shĕ'owl](../../strongs/h/h7585.md) [šāvaʿ](../../strongs/h/h7768.md) I, and thou [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md).

<a name="jonah_2_3"></a>Jonah 2:3

For thou hadst [shalak](../../strongs/h/h7993.md) me into the [mᵊṣôlâ](../../strongs/h/h4688.md), in the [lebab](../../strongs/h/h3824.md) of the [yam](../../strongs/h/h3220.md); and the [nāhār](../../strongs/h/h5104.md) [cabab](../../strongs/h/h5437.md) me about: all thy [mišbār](../../strongs/h/h4867.md) and thy [gal](../../strongs/h/h1530.md) ['abar](../../strongs/h/h5674.md) me.

<a name="jonah_2_4"></a>Jonah 2:4

Then I ['āmar](../../strongs/h/h559.md), I am [gāraš](../../strongs/h/h1644.md) of thy ['ayin](../../strongs/h/h5869.md); yet I will [nabat](../../strongs/h/h5027.md) again toward thy [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md).

<a name="jonah_2_5"></a>Jonah 2:5

The [mayim](../../strongs/h/h4325.md) ['āp̄ap̄](../../strongs/h/h661.md) me, even to the [nephesh](../../strongs/h/h5315.md): the [tĕhowm](../../strongs/h/h8415.md) [cabab](../../strongs/h/h5437.md) me, the [sûp̄](../../strongs/h/h5488.md) were [ḥāḇaš](../../strongs/h/h2280.md) my [ro'sh](../../strongs/h/h7218.md).

<a name="jonah_2_6"></a>Jonah 2:6

I [yarad](../../strongs/h/h3381.md) to the [qeṣeḇ](../../strongs/h/h7095.md) of the [har](../../strongs/h/h2022.md); the ['erets](../../strongs/h/h776.md) with her [bᵊrîaḥ](../../strongs/h/h1280.md) was about me ['owlam](../../strongs/h/h5769.md): yet hast thou [ʿālâ](../../strongs/h/h5927.md) my [chay](../../strongs/h/h2416.md) from [shachath](../../strongs/h/h7845.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="jonah_2_7"></a>Jonah 2:7

When my [nephesh](../../strongs/h/h5315.md) [ʿāṭap̄](../../strongs/h/h5848.md) within me I [zakar](../../strongs/h/h2142.md) [Yĕhovah](../../strongs/h/h3068.md): and my [tĕphillah](../../strongs/h/h8605.md) [bow'](../../strongs/h/h935.md) unto thee, into thine [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md).

<a name="jonah_2_8"></a>Jonah 2:8

They that [shamar](../../strongs/h/h8104.md) [shav'](../../strongs/h/h7723.md) [heḇel](../../strongs/h/h1892.md) ['azab](../../strongs/h/h5800.md) their own [checed](../../strongs/h/h2617.md).

<a name="jonah_2_9"></a>Jonah 2:9

But I will [zabach](../../strongs/h/h2076.md) unto thee with the [qowl](../../strongs/h/h6963.md) of [tôḏâ](../../strongs/h/h8426.md); I will [shalam](../../strongs/h/h7999.md) that that I have [nāḏar](../../strongs/h/h5087.md). [yĕshuw'ah](../../strongs/h/h3444.md) is of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jonah_2_10"></a>Jonah 2:10

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto the [dag](../../strongs/h/h1709.md), and it [qî'](../../strongs/h/h6958.md) [Yonah](../../strongs/h/h3124.md) upon the [yabāšâ](../../strongs/h/h3004.md) land.

---

[Transliteral Bible](../bible.md)

[Jonah](jonah.md)

[Jonah 1](jonah_1.md) - [Jonah 3](jonah_3.md)