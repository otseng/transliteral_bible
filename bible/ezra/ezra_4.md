# [Ezra 4](https://www.blueletterbible.org/kjv/ezra/4)

<a name="ezra_4_1"></a>Ezra 4:1

Now when the [tsar](../../strongs/h/h6862.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md) [shama'](../../strongs/h/h8085.md) that the [ben](../../strongs/h/h1121.md) of the [gôlâ](../../strongs/h/h1473.md) [bānâ](../../strongs/h/h1129.md) the [heykal](../../strongs/h/h1964.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="ezra_4_2"></a>Ezra 4:2

Then they [nāḡaš](../../strongs/h/h5066.md) to [Zᵊrubāḇel](../../strongs/h/h2216.md), and to the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), and ['āmar](../../strongs/h/h559.md) unto them, Let us [bānâ](../../strongs/h/h1129.md) with you: for we [darash](../../strongs/h/h1875.md) your ['Elohiym](../../strongs/h/h430.md), as ye do; and we do [zabach](../../strongs/h/h2076.md) unto him since the [yowm](../../strongs/h/h3117.md) of ['Ēsar-ḥadôn](../../strongs/h/h634.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), which brought us up [ʿālâ](../../strongs/h/h5927.md).

<a name="ezra_4_3"></a>Ezra 4:3

But [Zᵊrubāḇel](../../strongs/h/h2216.md), and [Yēšûaʿ](../../strongs/h/h3442.md), and the [šᵊ'ār](../../strongs/h/h7605.md) of the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md) unto them, Ye have nothing to do with us to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto our ['Elohiym](../../strongs/h/h430.md); but we ourselves [yaḥaḏ](../../strongs/h/h3162.md) will [bānâ](../../strongs/h/h1129.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), as [melek](../../strongs/h/h4428.md) [Kôreš](../../strongs/h/h3566.md) the [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md) hath [tsavah](../../strongs/h/h6680.md) us.

<a name="ezra_4_4"></a>Ezra 4:4

Then the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [rāp̄â](../../strongs/h/h7503.md) the [yad](../../strongs/h/h3027.md) of the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md), and [bahal](../../strongs/h/h926.md) [bālah](../../strongs/h/h1089.md) them in [bānâ](../../strongs/h/h1129.md),

<a name="ezra_4_5"></a>Ezra 4:5

And [śāḵar](../../strongs/h/h7936.md) [ya'ats](../../strongs/h/h3289.md) against them, to [pārar](../../strongs/h/h6565.md) their ['etsah](../../strongs/h/h6098.md), all the [yowm](../../strongs/h/h3117.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), even until the [malkuwth](../../strongs/h/h4438.md) of [Daryāveš](../../strongs/h/h1867.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md).

<a name="ezra_4_6"></a>Ezra 4:6

And in the [malkuwth](../../strongs/h/h4438.md) of ['Ăḥašvērôš](../../strongs/h/h325.md), in the [tᵊḥillâ](../../strongs/h/h8462.md) of his [malkuwth](../../strongs/h/h4438.md), [kāṯaḇ](../../strongs/h/h3789.md) they unto him an [śiṭnâ](../../strongs/h/h7855.md) against the [yashab](../../strongs/h/h3427.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_4_7"></a>Ezra 4:7

And in the [yowm](../../strongs/h/h3117.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [kāṯaḇ](../../strongs/h/h3789.md) [Bišlām](../../strongs/h/h1312.md), [Miṯrᵊḏāṯ](../../strongs/h/h4990.md), [ṭāḇ'ēl](../../strongs/h/h2870.md), and the [šᵊ'ār](../../strongs/h/h7605.md) of their [kᵊnāṯ](../../strongs/h/h3674.md), unto ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md); and the [kᵊṯāḇ](../../strongs/h/h3791.md) of the [ništᵊvān](../../strongs/h/h5406.md) was [kāṯaḇ](../../strongs/h/h3789.md) in the ['ărāmy](../../strongs/h/h762.md), and [targēm](../../strongs/h/h8638.md) in the ['ărāmy](../../strongs/h/h762.md).

<a name="ezra_4_8"></a>Ezra 4:8

[Rᵊḥûm](../../strongs/h/h7348.md) the [bᵊʿēl](../../strongs/h/h1169.md) [ṭᵊʿēm](../../strongs/h/h2942.md) and [Šimšay](../../strongs/h/h8124.md) the [sāp̄ēr](../../strongs/h/h5613.md) [kᵊṯaḇ](../../strongs/h/h3790.md) [ḥaḏ](../../strongs/h/h2298.md) ['igrā'](../../strongs/h/h104.md) [ʿal](../../strongs/h/h5922.md) [Yᵊrûšlēm](../../strongs/h/h3390.md) to ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [meleḵ](../../strongs/h/h4430.md) in this [kᵊnēmā'](../../strongs/h/h3660.md):

<a name="ezra_4_9"></a>Ezra 4:9

['ĕḏayin](../../strongs/h/h116.md) wrote [Rᵊḥûm](../../strongs/h/h7348.md) the [bᵊʿēl](../../strongs/h/h1169.md) [ṭᵊʿēm](../../strongs/h/h2942.md), and [Šimšay](../../strongs/h/h8124.md) the [sāp̄ēr](../../strongs/h/h5613.md), and the [šᵊ'ār](../../strongs/h/h7606.md) of their [kᵊnāṯ](../../strongs/h/h3675.md); the [Dînāyē'](../../strongs/h/h1784.md), the ['Ăp̄ārsᵊḵāyē'](../../strongs/h/h671.md), the [Ṭarpᵊlāyē'](../../strongs/h/h2967.md), the ['Ăp̄ārsāyē'](../../strongs/h/h670.md), the ['Arkᵊvay](../../strongs/h/h756.md), the [Baḇlay](../../strongs/h/h896.md), the [Šûšanḵāyē'](../../strongs/h/h7801.md), the [Dehāyē'](../../strongs/h/h1723.md), and the [ʿĒlmay](../../strongs/h/h5962.md),

<a name="ezra_4_10"></a>Ezra 4:10

And the [šᵊ'ār](../../strongs/h/h7606.md) of the ['ummâ](../../strongs/h/h524.md) [dî](../../strongs/h/h1768.md) the [raḇ](../../strongs/h/h7229.md) and [yaqqîr](../../strongs/h/h3358.md) ['Āsnapar](../../strongs/h/h620.md) brought [gᵊlâ](../../strongs/h/h1541.md), and [yᵊṯiḇ](../../strongs/h/h3488.md) [himmô](../../strongs/h/h1994.md) in the [qiryâ](../../strongs/h/h7149.md) of [Šāmrayin](../../strongs/h/h8115.md), and the [šᵊ'ār](../../strongs/h/h7606.md) that are on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), and at such a [kᵊʿeneṯ](../../strongs/h/h3706.md).

<a name="ezra_4_11"></a>Ezra 4:11

[dēn](../../strongs/h/h1836.md) is the [paršeḡen](../../strongs/h/h6573.md) of the ['igrā'](../../strongs/h/h104.md) [dî](../../strongs/h/h1768.md) they [šᵊlaḥ](../../strongs/h/h7972.md) unto [ʿal](../../strongs/h/h5922.md), even unto ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [meleḵ](../../strongs/h/h4430.md); Thy [ʿăḇaḏ](../../strongs/h/h5649.md) the ['ēneš](../../strongs/h/h606.md) on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), and at such a [kᵊʿeneṯ](../../strongs/h/h3706.md).

<a name="ezra_4_12"></a>Ezra 4:12

Be [hăvâ](../../strongs/h/h1934.md) [yᵊḏaʿ](../../strongs/h/h3046.md) unto the [meleḵ](../../strongs/h/h4430.md), that the [yᵊhûḏay](../../strongs/h/h3062.md) [dî](../../strongs/h/h1768.md) came [sᵊlaq](../../strongs/h/h5559.md) [min](../../strongs/h/h4481.md) [lᵊvāṯ](../../strongs/h/h3890.md) to [ʿal](../../strongs/h/h5922.md) are ['ăṯâ](../../strongs/h/h858.md) unto [Yᵊrûšlēm](../../strongs/h/h3390.md), [bᵊnā'](../../strongs/h/h1124.md) the [mārāḏ](../../strongs/h/h4779.md) and the [bi'ûš](../../strongs/h/h873.md) [qiryâ](../../strongs/h/h7149.md), and have [kᵊll](../../strongs/h/h3635.md) [kᵊll](../../strongs/h/h3635.md) the [šûr](../../strongs/h/h7792.md) thereof, and [ḥûṭ](../../strongs/h/h2338.md) the ['š](../../strongs/h/h787.md).

<a name="ezra_4_13"></a>Ezra 4:13

Be it [yᵊḏaʿ](../../strongs/h/h3046.md) [hăvâ](../../strongs/h/h1934.md) [kᵊʿan](../../strongs/h/h3705.md) unto the [meleḵ](../../strongs/h/h4430.md), that, [hēn](../../strongs/h/h2006.md) [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) be [bᵊnā'](../../strongs/h/h1124.md), and the [šûr](../../strongs/h/h7792.md) set [kᵊll](../../strongs/h/h3635.md) again, then will they [lā'](../../strongs/h/h3809.md) [nᵊṯan](../../strongs/h/h5415.md) [midâ](../../strongs/h/h4061.md), [bᵊlô](../../strongs/h/h1093.md), and [hălāḵ](../../strongs/h/h1983.md), and so thou shalt [nᵊzaq](../../strongs/h/h5142.md) the ['apṯōm](../../strongs/h/h674.md) of the [meleḵ](../../strongs/h/h4430.md).

<a name="ezra_4_14"></a>Ezra 4:14

[kᵊʿan](../../strongs/h/h3705.md) [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) we [dî](../../strongs/h/h1768.md) [mᵊlaḥ](../../strongs/h/h4415.md) [mᵊlaḥ](../../strongs/h/h4416.md) from the [hêḵal](../../strongs/h/h1965.md), and it was [lā'](../../strongs/h/h3809.md) ['ăraḵ](../../strongs/h/h749.md) for us to [ḥăzā'](../../strongs/h/h2370.md) the [meleḵ](../../strongs/h/h4430.md) [ʿarvâ](../../strongs/h/h6173.md), [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) have we [šᵊlaḥ](../../strongs/h/h7972.md) and [yᵊḏaʿ](../../strongs/h/h3046.md) the [meleḵ](../../strongs/h/h4430.md);

<a name="ezra_4_15"></a>Ezra 4:15

That [bᵊqar](../../strongs/h/h1240.md) may be made in the [sᵊp̄ar](../../strongs/h/h5609.md) of the [diḵrôn](../../strongs/h/h1799.md) of thy ['ab](../../strongs/h/h2.md): so shalt thou [šᵊḵaḥ](../../strongs/h/h7912.md) in the [sᵊp̄ar](../../strongs/h/h5609.md) of the [diḵrôn](../../strongs/h/h1799.md), and [yᵊḏaʿ](../../strongs/h/h3046.md) that [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) is a [mārāḏ](../../strongs/h/h4779.md) [qiryâ](../../strongs/h/h7149.md), and [nᵊzaq](../../strongs/h/h5142.md) unto [meleḵ](../../strongs/h/h4430.md) and [mᵊḏînâ](../../strongs/h/h4083.md), and that they have [ʿăḇaḏ](../../strongs/h/h5648.md) ['eštadûr](../../strongs/h/h849.md) within the [gav](../../strongs/h/h1459.md) [min](../../strongs/h/h4481.md) [ʿālam](../../strongs/h/h5957.md) [yôm](../../strongs/h/h3118.md): [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) cause was [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) [ḥăraḇ](../../strongs/h/h2718.md).

<a name="ezra_4_16"></a>Ezra 4:16

['ănaḥnā'](../../strongs/h/h586.md) [yᵊḏaʿ](../../strongs/h/h3046.md) the [meleḵ](../../strongs/h/h4430.md) that, [hēn](../../strongs/h/h2006.md) [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) be [bᵊnā'](../../strongs/h/h1124.md) again, and the [šûr](../../strongs/h/h7792.md) thereof set [kᵊll](../../strongs/h/h3635.md), by [dēn](../../strongs/h/h1836.md) [qᵊḇēl](../../strongs/h/h6903.md) thou shalt ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) [ḥălāq](../../strongs/h/h2508.md) on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md).

<a name="ezra_4_17"></a>Ezra 4:17

Then [šᵊlaḥ](../../strongs/h/h7972.md) the [meleḵ](../../strongs/h/h4430.md) a [piṯgām](../../strongs/h/h6600.md) [ʿal](../../strongs/h/h5922.md) [Rᵊḥûm](../../strongs/h/h7348.md) the [bᵊʿēl](../../strongs/h/h1169.md) [ṭᵊʿēm](../../strongs/h/h2942.md), and to [Šimšay](../../strongs/h/h8124.md) the [sāp̄ēr](../../strongs/h/h5613.md), and to the [šᵊ'ār](../../strongs/h/h7606.md) of their [kᵊnāṯ](../../strongs/h/h3675.md) that [yᵊṯiḇ](../../strongs/h/h3488.md) in [Šāmrayin](../../strongs/h/h8115.md), and unto the [šᵊ'ār](../../strongs/h/h7606.md) [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), [šᵊlām](../../strongs/h/h8001.md), and at such a [kᵊʿeneṯ](../../strongs/h/h3706.md).

<a name="ezra_4_18"></a>Ezra 4:18

The [ništᵊvān](../../strongs/h/h5407.md) which ye [šᵊlaḥ](../../strongs/h/h7972.md) unto [ʿal](../../strongs/h/h5922.md) hath been [pᵊraš](../../strongs/h/h6568.md) [qᵊrā'](../../strongs/h/h7123.md) [qŏḏām](../../strongs/h/h6925.md) me.

<a name="ezra_4_19"></a>Ezra 4:19

And [min](../../strongs/h/h4481.md) [śûm](../../strongs/h/h7761.md) [ṭᵊʿēm](../../strongs/h/h2942.md), and [bᵊqar](../../strongs/h/h1240.md) hath been made, and it is [šᵊḵaḥ](../../strongs/h/h7912.md) that [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) [min](../../strongs/h/h4481.md) [ʿālam](../../strongs/h/h5957.md) [yôm](../../strongs/h/h3118.md) hath made [nᵊśā'](../../strongs/h/h5376.md) [ʿal](../../strongs/h/h5922.md) [meleḵ](../../strongs/h/h4430.md), and that [mᵊraḏ](../../strongs/h/h4776.md) and ['eštadûr](../../strongs/h/h849.md) have been [ʿăḇaḏ](../../strongs/h/h5648.md) therein.

<a name="ezra_4_20"></a>Ezra 4:20

There have [hăvâ](../../strongs/h/h1934.md) [taqqîp̄](../../strongs/h/h8624.md) [meleḵ](../../strongs/h/h4430.md) also [ʿal](../../strongs/h/h5922.md) [Yᵊrûšlēm](../../strongs/h/h3390.md), which have [šallîṭ](../../strongs/h/h7990.md) over [kōl](../../strongs/h/h3606.md) [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md); and [midâ](../../strongs/h/h4061.md), [bᵊlô](../../strongs/h/h1093.md), and [hălāḵ](../../strongs/h/h1983.md), was [yᵊhaḇ](../../strongs/h/h3052.md) unto them.

<a name="ezra_4_21"></a>Ezra 4:21

[śûm](../../strongs/h/h7761.md) ye [kᵊʿan](../../strongs/h/h3705.md) [ṭᵊʿēm](../../strongs/h/h2942.md) to [bᵊṭēl](../../strongs/h/h989.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) to [bᵊṭēl](../../strongs/h/h989.md), and that [dēḵ](../../strongs/h/h1791.md) [qiryâ](../../strongs/h/h7149.md) be [lā'](../../strongs/h/h3809.md) [bᵊnā'](../../strongs/h/h1124.md), [ʿaḏ](../../strongs/h/h5705.md) another [ṭaʿam](../../strongs/h/h2941.md) shall be [śûm](../../strongs/h/h7761.md) from [min](../../strongs/h/h4481.md).

<a name="ezra_4_22"></a>Ezra 4:22

[hăvâ](../../strongs/h/h1934.md) [zᵊhar](../../strongs/h/h2095.md) now that ye [šālû](../../strongs/h/h7960.md) not to [ʿal](../../strongs/h/h5922.md) [ʿăḇaḏ](../../strongs/h/h5648.md) [dēn](../../strongs/h/h1836.md): [mâ](../../strongs/h/h4101.md) should [ḥăḇal](../../strongs/h/h2257.md) [śᵊḡā'](../../strongs/h/h7680.md) to the [nᵊzaq](../../strongs/h/h5142.md) of the [meleḵ](../../strongs/h/h4430.md)?

<a name="ezra_4_23"></a>Ezra 4:23

['ĕḏayin](../../strongs/h/h116.md) [min](../../strongs/h/h4481.md) [dî](../../strongs/h/h1768.md) the [paršeḡen](../../strongs/h/h6573.md) of [meleḵ](../../strongs/h/h4430.md) ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [ništᵊvān](../../strongs/h/h5407.md) was [qᵊrā'](../../strongs/h/h7123.md) [qŏḏām](../../strongs/h/h6925.md) [Rᵊḥûm](../../strongs/h/h7348.md), and [Šimšay](../../strongs/h/h8124.md) the [sāp̄ēr](../../strongs/h/h5613.md), and their [kᵊnāṯ](../../strongs/h/h3675.md), they went ['ăzal](../../strongs/h/h236.md) in [bᵊhîlû](../../strongs/h/h924.md) to [Yᵊrûšlēm](../../strongs/h/h3390.md) [ʿal](../../strongs/h/h5922.md) the [yᵊhûḏay](../../strongs/h/h3062.md), and made [himmô](../../strongs/h/h1994.md) to [bᵊṭēl](../../strongs/h/h989.md) by ['eḏrāʿ](../../strongs/h/h153.md) and [ḥayil](../../strongs/h/h2429.md).

<a name="ezra_4_24"></a>Ezra 4:24

['ĕḏayin](../../strongs/h/h116.md) [bᵊṭēl](../../strongs/h/h989.md) the [ʿăḇîḏâ](../../strongs/h/h5673.md) of the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) which is at [Yᵊrûšlēm](../../strongs/h/h3390.md). So it [hăvâ](../../strongs/h/h1934.md) [bᵊṭēl](../../strongs/h/h989.md) [ʿaḏ](../../strongs/h/h5705.md) the [tᵊrên](../../strongs/h/h8648.md) [šᵊnâ](../../strongs/h/h8140.md) of the [malkû](../../strongs/h/h4437.md) of [Daryāveš](../../strongs/h/h1868.md) [meleḵ](../../strongs/h/h4430.md) of [Pāras](../../strongs/h/h6540.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 3](ezra_3.md) - [Ezra 5](ezra_5.md)