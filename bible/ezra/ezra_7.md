# [Ezra 7](https://www.blueletterbible.org/kjv/ezra/7)

<a name="ezra_7_1"></a>Ezra 7:1

Now ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), in the [malkuwth](../../strongs/h/h4438.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), [ʿEzrā'](../../strongs/h/h5830.md) the [ben](../../strongs/h/h1121.md) of [Śᵊrāyâ](../../strongs/h/h8304.md), the [ben](../../strongs/h/h1121.md) of [ʿĂzaryâ](../../strongs/h/h5838.md), the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md),

<a name="ezra_7_2"></a>Ezra 7:2

The [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md), the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md), the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md),

<a name="ezra_7_3"></a>Ezra 7:3

The [ben](../../strongs/h/h1121.md) of ['Ămaryâ](../../strongs/h/h568.md), the [ben](../../strongs/h/h1121.md) of [ʿĂzaryâ](../../strongs/h/h5838.md), the [ben](../../strongs/h/h1121.md) of [Mᵊrāyôṯ](../../strongs/h/h4812.md),

<a name="ezra_7_4"></a>Ezra 7:4

The [ben](../../strongs/h/h1121.md) of [Zᵊraḥyâ](../../strongs/h/h2228.md), the [ben](../../strongs/h/h1121.md) of [ʿUzzî](../../strongs/h/h5813.md), the [ben](../../strongs/h/h1121.md) of [Buqqî](../../strongs/h/h1231.md),

<a name="ezra_7_5"></a>Ezra 7:5

The [ben](../../strongs/h/h1121.md) of ['Ăḇîšûaʿ](../../strongs/h/h50.md), the [ben](../../strongs/h/h1121.md) of [Pînḥās](../../strongs/h/h6372.md), the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md):

<a name="ezra_7_6"></a>Ezra 7:6

This [ʿEzrā'](../../strongs/h/h5830.md) [ʿālâ](../../strongs/h/h5927.md) from [Bāḇel](../../strongs/h/h894.md); and he was a [māhîr](../../strongs/h/h4106.md) [sāp̄ar](../../strongs/h/h5608.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), which [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) had [nathan](../../strongs/h/h5414.md): and the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) him all his [baqqāšâ](../../strongs/h/h1246.md), according to the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) upon him.

<a name="ezra_7_7"></a>Ezra 7:7

And there [ʿālâ](../../strongs/h/h5927.md) some of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and of the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and the [shiyr](../../strongs/h/h7891.md), and the [šôʿēr](../../strongs/h/h7778.md), and the [Nāṯîn](../../strongs/h/h5411.md), unto [Yĕruwshalaim](../../strongs/h/h3389.md), in the seventh [šānâ](../../strongs/h/h8141.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [melek](../../strongs/h/h4428.md).

<a name="ezra_7_8"></a>Ezra 7:8

And he [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) in the fifth [ḥōḏeš](../../strongs/h/h2320.md), which was in the seventh [šānâ](../../strongs/h/h8141.md) of the [melek](../../strongs/h/h4428.md).

<a name="ezra_7_9"></a>Ezra 7:9

For upon the first day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) [yᵊsuḏ](../../strongs/h/h3246.md) he to go [maʿălâ](../../strongs/h/h4609.md) from [Bāḇel](../../strongs/h/h894.md), and on the first day of the fifth [ḥōḏeš](../../strongs/h/h2320.md) [bow'](../../strongs/h/h935.md) he to [Yĕruwshalaim](../../strongs/h/h3389.md), according to the [towb](../../strongs/h/h2896.md) [yad](../../strongs/h/h3027.md) of his ['Elohiym](../../strongs/h/h430.md) upon him.

<a name="ezra_7_10"></a>Ezra 7:10

For [ʿEzrā'](../../strongs/h/h5830.md) had [kuwn](../../strongs/h/h3559.md) his [lebab](../../strongs/h/h3824.md) to [darash](../../strongs/h/h1875.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md), and to ['asah](../../strongs/h/h6213.md) it, and to [lamad](../../strongs/h/h3925.md) in [Yisra'el](../../strongs/h/h3478.md) [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md).

<a name="ezra_7_11"></a>Ezra 7:11

Now this is the [paršeḡen](../../strong.md) of the [ništᵊvān](../../strongs/h/h5406.md) that the [melek](../../strongs/h/h4428.md) ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [nathan](../../strongs/h/h5414.md) unto [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md), the [sāp̄ar](../../strongs/h/h5608.md), even a [sāp̄ar](../../strongs/h/h5608.md) of the [dabar](../../strongs/h/h1697.md) of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), and of his [choq](../../strongs/h/h2706.md) to [Yisra'el](../../strongs/h/h3478.md).

<a name="ezra_7_12"></a>Ezra 7:12

['Artaḥšaštᵊ'](../../strongs/h/h783.md), [meleḵ](../../strongs/h/h4430.md) of [meleḵ](../../strongs/h/h4430.md), unto [ʿEzrā'](../../strongs/h/h5831.md) the [kāhēn](../../strongs/h/h3549.md), a [sāp̄ēr](../../strongs/h/h5613.md) of the [dāṯ](../../strongs/h/h1882.md) of the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md), [gᵊmar](../../strongs/h/h1585.md) peace, and at such a [kᵊʿeneṯ](../../strongs/h/h3706.md).

<a name="ezra_7_13"></a>Ezra 7:13

[min](../../strongs/h/h4481.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), that [kōl](../../strongs/h/h3606.md) they [min](../../strongs/h/h4481.md) the [ʿam](../../strongs/h/h5972.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md), and of his [kāhēn](../../strongs/h/h3549.md) and [lēvay](../../strongs/h/h3879.md), in my [malkû](../../strongs/h/h4437.md), which are minded of their own [nᵊḏaḇ](../../strongs/h/h5069.md) to go [hûḵ](../../strongs/h/h1946.md) to [Yᵊrûšlēm](../../strongs/h/h3390.md), [hûḵ](../../strongs/h/h1946.md) with [ʿim](../../strongs/h/h5974.md).

<a name="ezra_7_14"></a>Ezra 7:14

[dî](../../strongs/h/h1768.md) [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) thou art [šᵊlaḥ](../../strongs/h/h7972.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), and of his [šᵊḇaʿ](../../strongs/h/h7655.md) [yᵊʿaṭ](../../strongs/h/h3272.md), to [bᵊqar](../../strongs/h/h1240.md) [ʿal](../../strongs/h/h5922.md) [Yᵊhûḏ](../../strongs/h/h3061.md) and [Yᵊrûšlēm](../../strongs/h/h3390.md), according to the [dāṯ](../../strongs/h/h1882.md) of thy ['ĕlâ](../../strongs/h/h426.md) which is in thine [yaḏ](../../strongs/h/h3028.md);

<a name="ezra_7_15"></a>Ezra 7:15

And to [yᵊḇal](../../strongs/h/h2987.md) the [kᵊsap̄](../../strongs/h/h3702.md) and [dᵊhaḇ](../../strongs/h/h1722.md), which the [meleḵ](../../strongs/h/h4430.md) and his [yᵊʿaṭ](../../strongs/h/h3272.md) have freely [nᵊḏaḇ](../../strongs/h/h5069.md) unto the ['ĕlâ](../../strongs/h/h426.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md), [dî](../../strongs/h/h1768.md) [miškan](../../strongs/h/h4907.md) is in [Yᵊrûšlēm](../../strongs/h/h3390.md),

<a name="ezra_7_16"></a>Ezra 7:16

And [kōl](../../strongs/h/h3606.md) the [kᵊsap̄](../../strongs/h/h3702.md) and [dᵊhaḇ](../../strongs/h/h1722.md) that thou canst [šᵊḵaḥ](../../strongs/h/h7912.md) in [kōl](../../strongs/h/h3606.md) the [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md), [ʿim](../../strongs/h/h5974.md) the [nᵊḏaḇ](../../strongs/h/h5069.md) of the [ʿam](../../strongs/h/h5972.md), and of the [kāhēn](../../strongs/h/h3549.md), [nᵊḏaḇ](../../strongs/h/h5069.md) for the [bayiṯ](../../strongs/h/h1005.md) of their ['ĕlâ](../../strongs/h/h426.md) which is in [Yᵊrûšlēm](../../strongs/h/h3390.md):

<a name="ezra_7_17"></a>Ezra 7:17

[kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) thou mayest [qᵊnâ](../../strongs/h/h7066.md) ['āsparnā'](../../strongs/h/h629.md) with [dēn](../../strongs/h/h1836.md) [kᵊsap̄](../../strongs/h/h3702.md) [tôr](../../strongs/h/h8450.md), [dᵊḵar](../../strongs/h/h1798.md), ['immar](../../strongs/h/h563.md), with their [minḥâ](../../strongs/h/h4504.md) and their [nᵊsaḵ](../../strongs/h/h5261.md), and [qᵊrēḇ](../../strongs/h/h7127.md) [himmô](../../strongs/h/h1994.md) [ʿal](../../strongs/h/h5922.md) the [maḏbaḥ](../../strongs/h/h4056.md) of the [bayiṯ](../../strongs/h/h1005.md) of your ['ĕlâ](../../strongs/h/h426.md) which is in [Yᵊrûšlēm](../../strongs/h/h3390.md).

<a name="ezra_7_18"></a>Ezra 7:18

And [mâ](../../strongs/h/h4101.md) [dî](../../strongs/h/h1768.md) shall seem [yᵊṭaḇ](../../strongs/h/h3191.md) to thee, and [ʿal](../../strongs/h/h5922.md) thy ['aḥ](../../strongs/h/h252.md), to [ʿăḇaḏ](../../strongs/h/h5648.md) with the [šᵊ'ār](../../strongs/h/h7606.md) of the [kᵊsap̄](../../strongs/h/h3702.md) and the [dᵊhaḇ](../../strongs/h/h1722.md), that [ʿăḇaḏ](../../strongs/h/h5648.md) after the [rᵊʿûṯ](../../strongs/h/h7470.md) of your ['ĕlâ](../../strongs/h/h426.md).

<a name="ezra_7_19"></a>Ezra 7:19

The [mā'n](../../strongs/h/h3984.md) also that are [yᵊhaḇ](../../strongs/h/h3052.md) thee for the [pālḥān](../../strongs/h/h6402.md) of the [bayiṯ](../../strongs/h/h1005.md) of thy ['ĕlâ](../../strongs/h/h426.md), those [šᵊlam](../../strongs/h/h8000.md) thou [qŏḏām](../../strongs/h/h6925.md) the ['ĕlâ](../../strongs/h/h426.md) of [Yᵊrûšlēm](../../strongs/h/h3390.md).

<a name="ezra_7_20"></a>Ezra 7:20

And whatsoever [šᵊ'ār](../../strongs/h/h7606.md) shall be [ḥašḥûṯ](../../strongs/h/h2819.md) for the [bayiṯ](../../strongs/h/h1005.md) of thy ['ĕlâ](../../strongs/h/h426.md), which thou shalt have [nᵊp̄al](../../strongs/h/h5308.md) to [nᵊṯan](../../strongs/h/h5415.md), [nᵊṯan](../../strongs/h/h5415.md) it out [min](../../strongs/h/h4481.md) the [meleḵ](../../strongs/h/h4430.md) [gᵊnaz](../../strongs/h/h1596.md) [bayiṯ](../../strongs/h/h1005.md).

<a name="ezra_7_21"></a>Ezra 7:21

And [min](../../strongs/h/h4481.md), even ['ănā'](../../strongs/h/h576.md) ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [meleḵ](../../strongs/h/h4430.md), do [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md) to [kōl](../../strongs/h/h3606.md) the [gizbār](../../strongs/h/h1490.md) which are [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), that whatsoever [ʿEzrā'](../../strongs/h/h5831.md) the [kāhēn](../../strongs/h/h3549.md), the [sāp̄ēr](../../strongs/h/h5613.md) of the [dāṯ](../../strongs/h/h1882.md) of the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md), shall [šᵊ'ēl](../../strongs/h/h7593.md) of you, it be [ʿăḇaḏ](../../strongs/h/h5648.md) ['āsparnā'](../../strongs/h/h629.md),

<a name="ezra_7_22"></a>Ezra 7:22

[ʿaḏ](../../strongs/h/h5705.md) a [mᵊ'â](../../strongs/h/h3969.md) [kikār](../../strongs/h/h3604.md) of [kᵊsap̄](../../strongs/h/h3702.md), and to a [mᵊ'â](../../strongs/h/h3969.md) [kōr](../../strongs/h/h3734.md) of [ḥinṭîn](../../strongs/h/h2591.md), and to a [mᵊ'â](../../strongs/h/h3969.md) [baṯ](../../strongs/h/h1325.md) of [ḥămar](../../strongs/h/h2562.md), and to a [mᵊ'â](../../strongs/h/h3969.md) [baṯ](../../strongs/h/h1325.md) of [mᵊšaḥ](../../strongs/h/h4887.md), and [mᵊlaḥ](../../strongs/h/h4416.md) [lā'](../../strongs/h/h3809.md) [kᵊṯāḇ](../../strongs/h/h3792.md) how much.

<a name="ezra_7_23"></a>Ezra 7:23

[kōl](../../strongs/h/h3606.md) is [ṭaʿam](../../strongs/h/h2941.md) [min](../../strongs/h/h4481.md) the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md), let it be ['aḏrazdā'](../../strongs/h/h149.md) [ʿăḇaḏ](../../strongs/h/h5648.md) for the [bayiṯ](../../strongs/h/h1005.md) of the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md): [dî](../../strongs/h/h1768.md) [mâ](../../strongs/h/h4101.md) should there [hăvâ](../../strongs/h/h1934.md) [qᵊṣap̄](../../strongs/h/h7109.md) [ʿal](../../strongs/h/h5922.md) the [malkû](../../strongs/h/h4437.md) of the [meleḵ](../../strongs/h/h4430.md) and his [bēn](../../strongs/h/h1123.md)?

<a name="ezra_7_24"></a>Ezra 7:24

Also we [yᵊḏaʿ](../../strongs/h/h3046.md) you, that touching [kōl](../../strongs/h/h3606.md) of the [kāhēn](../../strongs/h/h3549.md) and [lēvay](../../strongs/h/h3879.md), [zammār](../../strongs/h/h2171.md), [tārāʿ](../../strongs/h/h8652.md), [nᵊṯîn](../../strongs/h/h5412.md), or [pᵊlaḥ](../../strongs/h/h6399.md) of [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md), it shall [lā'](../../strongs/h/h3809.md) be [šallîṭ](../../strongs/h/h7990.md) to [rᵊmâ](../../strongs/h/h7412.md) [midâ](../../strongs/h/h4061.md), [bᵊlô](../../strongs/h/h1093.md), or [hălāḵ](../../strongs/h/h1983.md), [ʿal](../../strongs/h/h5922.md) them.

<a name="ezra_7_25"></a>Ezra 7:25

And ['antâ](../../strongs/h/h607.md), [ʿEzrā'](../../strongs/h/h5831.md), after the [ḥāḵmâ](../../strongs/h/h2452.md) of thy ['ĕlâ](../../strongs/h/h426.md), that is in thine [yaḏ](../../strongs/h/h3028.md), [mᵊnā'](../../strongs/h/h4483.md) [šᵊp̄aṭ](../../strongs/h/h8200.md) and [dayyān](../../strongs/h/h1782.md), which may [hăvâ](../../strongs/h/h1934.md) [dîn](../../strongs/h/h1778.md) [kōl](../../strongs/h/h3606.md) the [ʿam](../../strongs/h/h5972.md) that are [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), [kōl](../../strongs/h/h3606.md) such as [yᵊḏaʿ](../../strongs/h/h3046.md) the [dāṯ](../../strongs/h/h1882.md) of thy ['ĕlâ](../../strongs/h/h426.md); and [yᵊḏaʿ](../../strongs/h/h3046.md) ye them that [yᵊḏaʿ](../../strongs/h/h3046.md) them [lā'](../../strongs/h/h3809.md).

<a name="ezra_7_26"></a>Ezra 7:26

And [kōl](../../strongs/h/h3606.md) will [lā'](../../strongs/h/h3809.md) [hăvâ](../../strongs/h/h1934.md) [ʿăḇaḏ](../../strongs/h/h5648.md) the [dāṯ](../../strongs/h/h1882.md) of thy ['ĕlâ](../../strongs/h/h426.md), and the [dāṯ](../../strongs/h/h1882.md) of the [meleḵ](../../strongs/h/h4430.md), let [dîn](../../strongs/h/h1780.md) [hăvâ](../../strongs/h/h1934.md) [ʿăḇaḏ](../../strongs/h/h5648.md) ['āsparnā'](../../strongs/h/h629.md) upon [min](../../strongs/h/h4481.md), [hēn](../../strongs/h/h2006.md) it be unto [môṯ](../../strongs/h/h4193.md), [hēn](../../strongs/h/h2006.md) to [šᵊrōšû](../../strongs/h/h8332.md), [hēn](../../strongs/h/h2006.md) to [ʿănāš](../../strongs/h/h6065.md) of [nᵊḵas](../../strongs/h/h5232.md), or to ['ĕsûr](../../strongs/h/h613.md).

<a name="ezra_7_27"></a>Ezra 7:27

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of our ['ab](../../strongs/h/h1.md), which hath [nathan](../../strongs/h/h5414.md) such a thing as this in the [melek](../../strongs/h/h4428.md) [leb](../../strongs/h/h3820.md), to [pā'ar](../../strongs/h/h6286.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) which is in [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="ezra_7_28"></a>Ezra 7:28

And hath [natah](../../strongs/h/h5186.md) [checed](../../strongs/h/h2617.md) unto me [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and his [ya'ats](../../strongs/h/h3289.md), and before all the [melek](../../strongs/h/h4428.md) [gibôr](../../strongs/h/h1368.md) [śar](../../strongs/h/h8269.md). And I was [ḥāzaq](../../strongs/h/h2388.md) as the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) was upon me, and I [qāḇaṣ](../../strongs/h/h6908.md) out of [Yisra'el](../../strongs/h/h3478.md) [ro'sh](../../strongs/h/h7218.md) to [ʿālâ](../../strongs/h/h5927.md) with me.

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 6](ezra_6.md) - [Ezra 8](ezra_8.md)