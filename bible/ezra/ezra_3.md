# [Ezra 3](https://www.blueletterbible.org/kjv/ezra/3)

<a name="ezra_3_1"></a>Ezra 3:1

And when the seventh [ḥōḏeš](../../strongs/h/h2320.md) was [naga'](../../strongs/h/h5060.md), and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were in the [ʿîr](../../strongs/h/h5892.md), the ['am](../../strongs/h/h5971.md) ['āsap̄](../../strongs/h/h622.md) themselvesas one ['iysh](../../strongs/h/h376.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_3_2"></a>Ezra 3:2

Then [quwm](../../strongs/h/h6965.md) [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of [Yôṣāḏāq](../../strongs/h/h3136.md), and his ['ach](../../strongs/h/h251.md) the [kōhēn](../../strongs/h/h3548.md), and [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), and his ['ach](../../strongs/h/h251.md), and [bānâ](../../strongs/h/h1129.md) the [mizbeach](../../strongs/h/h4196.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) thereon, as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="ezra_3_3"></a>Ezra 3:3

And they [kuwn](../../strongs/h/h3559.md) the [mizbeach](../../strongs/h/h4196.md) upon his [mᵊḵônâ](../../strongs/h/h4350.md); for ['êmâ](../../strongs/h/h367.md) was upon them because of the ['am](../../strongs/h/h5971.md) of those ['erets](../../strongs/h/h776.md): and they [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) thereon unto [Yĕhovah](../../strongs/h/h3068.md), even [ʿōlâ](../../strongs/h/h5930.md) [boqer](../../strongs/h/h1242.md) and ['ereb](../../strongs/h/h6153.md).

<a name="ezra_3_4"></a>Ezra 3:4

They ['asah](../../strongs/h/h6213.md) also the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md), as it is [kāṯaḇ](../../strongs/h/h3789.md), and offered the [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [ʿōlâ](../../strongs/h/h5930.md) by [mispār](../../strongs/h/h4557.md), according to the [mishpat](../../strongs/h/h4941.md), as the [dabar](../../strongs/h/h1697.md) of every [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md);

<a name="ezra_3_5"></a>Ezra 3:5

And ['aḥar](../../strongs/h/h310.md) offered the [tāmîḏ](../../strongs/h/h8548.md) [ʿōlâ](../../strongs/h/h5930.md), both of the [ḥōḏeš](../../strongs/h/h2320.md), and of all the set [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md) that were [qadash](../../strongs/h/h6942.md), and of every one that willingly [nāḏaḇ](../../strongs/h/h5068.md) a [nᵊḏāḇâ](../../strongs/h/h5071.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezra_3_6"></a>Ezra 3:6

From the first [yowm](../../strongs/h/h3117.md) of the seventh [ḥōḏeš](../../strongs/h/h2320.md) [ḥālal](../../strongs/h/h2490.md) they to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md). But the [yacad](../../strongs/h/h3245.md) of the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md) was not yet [yacad](../../strongs/h/h3245.md).

<a name="ezra_3_7"></a>Ezra 3:7

They [nathan](../../strongs/h/h5414.md) [keceph](../../strongs/h/h3701.md) also unto the [ḥāṣaḇ](../../strongs/h/h2672.md), and to the [ḥārāš](../../strongs/h/h2796.md); and [ma'akal](../../strongs/h/h3978.md), and [mištê](../../strongs/h/h4960.md), and [šemen](../../strongs/h/h8081.md), unto them of [Ṣîḏōnî](../../strongs/h/h6722.md), and to them of [ṣōrî](../../strongs/h/h6876.md), to [bow'](../../strongs/h/h935.md) ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md) from [Lᵊḇānôn](../../strongs/h/h3844.md) to the [yam](../../strongs/h/h3220.md) of [Yāp̄Vô](../../strongs/h/h3305.md), according to the [rišyôn](../../strongs/h/h7558.md) that they had of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md).

<a name="ezra_3_8"></a>Ezra 3:8

Now in the second [šānâ](../../strongs/h/h8141.md) of their [bow'](../../strongs/h/h935.md) unto the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), in the second [ḥōḏeš](../../strongs/h/h2320.md), [ḥālal](../../strongs/h/h2490.md) [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), and [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of [Yôṣāḏāq](../../strongs/h/h3136.md), and the [šᵊ'ār](../../strongs/h/h7605.md) of their ['ach](../../strongs/h/h251.md) the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and all they that were [bow'](../../strongs/h/h935.md) of the [šᵊḇî](../../strongs/h/h7628.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md); and ['amad](../../strongs/h/h5975.md) the [Lᵊvî](../../strongs/h/h3881.md), from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), to set [nāṣaḥ](../../strongs/h/h5329.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezra_3_9"></a>Ezra 3:9

Then ['amad](../../strongs/h/h5975.md) [Yēšûaʿ](../../strongs/h/h3442.md) with his [ben](../../strongs/h/h1121.md) and his ['ach](../../strongs/h/h251.md), [Qaḏmî'Ēl](../../strongs/h/h6934.md) and his [ben](../../strongs/h/h1121.md), the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), together, to set [nāṣaḥ](../../strongs/h/h5329.md) the ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md): the [ben](../../strongs/h/h1121.md) of [Ḥēnāḏāḏ](../../strongs/h/h2582.md), with their [ben](../../strongs/h/h1121.md) and their ['ach](../../strongs/h/h251.md) the [Lᵊvî](../../strongs/h/h3881.md).

<a name="ezra_3_10"></a>Ezra 3:10

And when the [bānâ](../../strongs/h/h1129.md) [yacad](../../strongs/h/h3245.md) the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), they ['amad](../../strongs/h/h5975.md) the [kōhēn](../../strongs/h/h3548.md) in their [labash](../../strongs/h/h3847.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md), and the [Lᵊvî](../../strongs/h/h3881.md) the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md) with [mᵊṣēleṯ](../../strongs/h/h4700.md), to [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), after the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezra_3_11"></a>Ezra 3:11

And they ['anah](../../strongs/h/h6030.md) in [halal](../../strongs/h/h1984.md) and [yadah](../../strongs/h/h3034.md) unto [Yĕhovah](../../strongs/h/h3068.md); because he is [towb](../../strongs/h/h2896.md), for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md) toward [Yisra'el](../../strongs/h/h3478.md). And all the ['am](../../strongs/h/h5971.md) [rûaʿ](../../strongs/h/h7321.md) with a [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md), when they [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), because the [yacad](../../strongs/h/h3245.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) was [yacad](../../strongs/h/h3245.md).

<a name="ezra_3_12"></a>Ezra 3:12

But [rab](../../strongs/h/h7227.md) of the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md) and [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), who were [zāqēn](../../strongs/h/h2205.md), that had [ra'ah](../../strongs/h/h7200.md) the [ri'šôn](../../strongs/h/h7223.md) [bayith](../../strongs/h/h1004.md), when the [yacad](../../strongs/h/h3245.md) of this [bayith](../../strongs/h/h1004.md) was [yacad](../../strongs/h/h3245.md) before their ['ayin](../../strongs/h/h5869.md), [bāḵâ](../../strongs/h/h1058.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md); and [rab](../../strongs/h/h7227.md) [tᵊrûʿâ](../../strongs/h/h8643.md) [ruwm](../../strongs/h/h7311.md) for [simchah](../../strongs/h/h8057.md):

<a name="ezra_3_13"></a>Ezra 3:13

So that the ['am](../../strongs/h/h5971.md) could not [nāḵar](../../strongs/h/h5234.md) the [qowl](../../strongs/h/h6963.md) of the [tᵊrûʿâ](../../strongs/h/h8643.md) of [simchah](../../strongs/h/h8057.md) from the [qowl](../../strongs/h/h6963.md) of the [bĕkiy](../../strongs/h/h1065.md) of the ['am](../../strongs/h/h5971.md): for the ['am](../../strongs/h/h5971.md) [rûaʿ](../../strongs/h/h7321.md) with a [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md), and the [qowl](../../strongs/h/h6963.md) was [shama'](../../strongs/h/h8085.md) afar [rachowq](../../strongs/h/h7350.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 2](ezra_2.md) - [Ezra 4](ezra_4.md)