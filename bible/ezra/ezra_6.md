# [Ezra 6](https://www.blueletterbible.org/kjv/ezra/6)

<a name="ezra_6_1"></a>Ezra 6:1

['ĕḏayin](../../strongs/h/h116.md) [Daryāveš](../../strongs/h/h1868.md) the [meleḵ](../../strongs/h/h4430.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), and [bᵊqar](../../strongs/h/h1240.md) was made in the [bayiṯ](../../strongs/h/h1005.md) of the [sᵊp̄ar](../../strongs/h/h5609.md), [tammâ](../../strongs/h/h8536.md) the [gᵊnaz](../../strongs/h/h1596.md) were laid [nᵊḥaṯ](../../strongs/h/h5182.md) in [Bāḇel](../../strongs/h/h895.md).

<a name="ezra_6_2"></a>Ezra 6:2

And there was [šᵊḵaḥ](../../strongs/h/h7912.md) at ['Aḥmᵊṯā'](../../strongs/h/h307.md), in the [bîrâ](../../strongs/h/h1001.md) that is in the [mᵊḏînâ](../../strongs/h/h4083.md) of the [Māḏay](../../strongs/h/h4076.md), [ḥaḏ](../../strongs/h/h2298.md) [mᵊḡallâ](../../strongs/h/h4040.md), and [gav](../../strongs/h/h1459.md) was a [diḵrôn](../../strongs/h/h1799.md) [kēn](../../strongs/h/h3652.md) [kᵊṯaḇ](../../strongs/h/h3790.md):

<a name="ezra_6_3"></a>Ezra 6:3

In the [ḥaḏ](../../strongs/h/h2298.md) [šᵊnâ](../../strongs/h/h8140.md) of [Kôreš](../../strongs/h/h3567.md) the [meleḵ](../../strongs/h/h4430.md) the same [Kôreš](../../strongs/h/h3567.md) the [meleḵ](../../strongs/h/h4430.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md) concerning the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) at [Yᵊrûšlēm](../../strongs/h/h3390.md), Let the [bayiṯ](../../strongs/h/h1005.md) be [bᵊnā'](../../strongs/h/h1124.md), the ['ăṯar](../../strongs/h/h870.md) where they [dᵊḇaḥ](../../strongs/h/h1684.md) [dᵊḇaḥ](../../strongs/h/h1685.md), and let the ['š](../../strongs/h/h787.md) thereof be [sᵊḇal](../../strongs/h/h5446.md); the [rûm](../../strongs/h/h7314.md) thereof [šitîn](../../strongs/h/h8361.md) ['ammâ](../../strongs/h/h521.md), and the [pᵊṯay](../../strongs/h/h6613.md) thereof [šitîn](../../strongs/h/h8361.md) ['ammâ](../../strongs/h/h521.md);

<a name="ezra_6_4"></a>Ezra 6:4

With [tᵊlāṯ](../../strongs/h/h8532.md) [niḏbāḵ](../../strongs/h/h5073.md) of [gᵊlāl](../../strongs/h/h1560.md) ['eḇen](../../strongs/h/h69.md), and a [niḏbāḵ](../../strongs/h/h5073.md) of [ḥăḏaṯ](../../strongs/h/h2323.md) ['āʿ](../../strongs/h/h636.md): and let the [nip̄qā'](../../strongs/h/h5313.md) be [yᵊhaḇ](../../strongs/h/h3052.md) out [min](../../strongs/h/h4481.md) the [meleḵ](../../strongs/h/h4430.md) [bayiṯ](../../strongs/h/h1005.md):

<a name="ezra_6_5"></a>Ezra 6:5

And ['ap̄](../../strongs/h/h638.md) let the [dᵊhaḇ](../../strongs/h/h1722.md) and [kᵊsap̄](../../strongs/h/h3702.md) [mā'n](../../strongs/h/h3984.md) of the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md), which [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) took [nᵊp̄aq](../../strongs/h/h5312.md) out [min](../../strongs/h/h4481.md) the [hêḵal](../../strongs/h/h1965.md) which is at [Yᵊrûšlēm](../../strongs/h/h3390.md), and [yᵊḇal](../../strongs/h/h2987.md) unto [Bāḇel](../../strongs/h/h895.md), be [tûḇ](../../strongs/h/h8421.md), and brought [hûḵ](../../strongs/h/h1946.md) unto the [hêḵal](../../strongs/h/h1965.md) which is at [Yᵊrûšlēm](../../strongs/h/h3390.md), every one to his ['ăṯar](../../strongs/h/h870.md), and [nᵊḥaṯ](../../strongs/h/h5182.md) them in the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md).

<a name="ezra_6_6"></a>Ezra 6:6

[kᵊʿan](../../strongs/h/h3705.md) therefore, [Tatnay](../../strongs/h/h8674.md), [peḥâ](../../strongs/h/h6347.md) [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), [Šᵊṯar Bôznay](../../strongs/h/h8370.md), and your [kᵊnāṯ](../../strongs/h/h3675.md) the ['Ăp̄ārsᵊḵāyē'](../../strongs/h/h671.md), which are [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), be [hăvâ](../../strongs/h/h1934.md) [raḥîq](../../strongs/h/h7352.md) [min](../../strongs/h/h4481.md) [tammâ](../../strongs/h/h8536.md):

<a name="ezra_6_7"></a>Ezra 6:7

Let the [ʿăḇîḏâ](../../strongs/h/h5673.md) of [dēḵ](../../strongs/h/h1791.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) [šᵊḇaq](../../strongs/h/h7662.md); let the [peḥâ](../../strongs/h/h6347.md) of the [yᵊhûḏay](../../strongs/h/h3062.md) and the [śîḇ](../../strongs/h/h7868.md) of the [yᵊhûḏay](../../strongs/h/h3062.md) [bᵊnā'](../../strongs/h/h1124.md) [dēḵ](../../strongs/h/h1791.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) [ʿal](../../strongs/h/h5922.md) his ['ăṯar](../../strongs/h/h870.md).

<a name="ezra_6_8"></a>Ezra 6:8

Moreover [min](../../strongs/h/h4481.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md) [mā'](../../strongs/h/h3964.md) ye shall [ʿăḇaḏ](../../strongs/h/h5648.md) [ʿim](../../strongs/h/h5974.md) the [śîḇ](../../strongs/h/h7868.md) of ['illēḵ](../../strongs/h/h479.md) [yᵊhûḏay](../../strongs/h/h3062.md) for the [bᵊnā'](../../strongs/h/h1124.md) of [dēḵ](../../strongs/h/h1791.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md): that of the [meleḵ](../../strongs/h/h4430.md) [nᵊḵas](../../strongs/h/h5232.md), even [dî](../../strongs/h/h1768.md) the [midâ](../../strongs/h/h4061.md) [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), ['āsparnā'](../../strongs/h/h629.md) [nip̄qā'](../../strongs/h/h5313.md) [hăvâ](../../strongs/h/h1934.md) [yᵊhaḇ](../../strongs/h/h3052.md) unto ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md), that they be [lā'](../../strongs/h/h3809.md) [bᵊṭēl](../../strongs/h/h989.md).

<a name="ezra_6_9"></a>Ezra 6:9

And that [mâ](../../strongs/h/h4101.md) they have need [ḥăšaḥ](../../strongs/h/h2818.md), both [bēn](../../strongs/h/h1123.md) [tôr](../../strongs/h/h8450.md), and [dᵊḵar](../../strongs/h/h1798.md), and ['immar](../../strongs/h/h563.md), for the burnt [ʿălâ](../../strongs/h/h5928.md) of the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md), [ḥinṭîn](../../strongs/h/h2591.md), [mᵊlaḥ](../../strongs/h/h4416.md), [ḥămar](../../strongs/h/h2562.md), and [mᵊšaḥ](../../strongs/h/h4887.md), according to the [mē'mar](../../strongs/h/h3983.md) of the [kāhēn](../../strongs/h/h3549.md) which are at [Yᵊrûšlēm](../../strongs/h/h3390.md), let it [hăvâ](../../strongs/h/h1934.md) [yᵊhaḇ](../../strongs/h/h3052.md) them [yôm](../../strongs/h/h3118.md) by [yôm](../../strongs/h/h3118.md) [lā'](../../strongs/h/h3809.md) [šālû](../../strongs/h/h7960.md):

<a name="ezra_6_10"></a>Ezra 6:10

That they may [hăvâ](../../strongs/h/h1934.md) [qᵊrēḇ](../../strongs/h/h7127.md) of [nîḥôaḥ](../../strongs/h/h5208.md) unto the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md), and [ṣᵊlâ](../../strongs/h/h6739.md) for the [ḥay](../../strongs/h/h2417.md) of the [meleḵ](../../strongs/h/h4430.md), and of his [bēn](../../strongs/h/h1123.md).

<a name="ezra_6_11"></a>Ezra 6:11

[min](../../strongs/h/h4481.md) I have [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), that [kōl](../../strongs/h/h3606.md) ['ēneš](../../strongs/h/h606.md) shall [šᵊnâ](../../strongs/h/h8133.md) [dēn](../../strongs/h/h1836.md) [piṯgām](../../strongs/h/h6600.md), let ['āʿ](../../strongs/h/h636.md) be pulled [nᵊsaḥ](../../strongs/h/h5256.md) [min](../../strongs/h/h4481.md) his [bayiṯ](../../strongs/h/h1005.md), and being set [zᵊqap̄](../../strongs/h/h2211.md), let him be [mᵊḥā'](../../strongs/h/h4223.md) [ʿal](../../strongs/h/h5922.md); and let his [bayiṯ](../../strongs/h/h1005.md) be [ʿăḇaḏ](../../strongs/h/h5648.md) a [nᵊvālû](../../strongs/h/h5122.md) [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md).

<a name="ezra_6_12"></a>Ezra 6:12

And the ['ĕlâ](../../strongs/h/h426.md) that hath caused his [šum](../../strongs/h/h8036.md) to [šᵊḵan](../../strongs/h/h7932.md) [tammâ](../../strongs/h/h8536.md) [mᵊḡar](../../strongs/h/h4049.md) [kōl](../../strongs/h/h3606.md) [meleḵ](../../strongs/h/h4430.md) and [ʿam](../../strongs/h/h5972.md), that shall [šᵊlaḥ](../../strongs/h/h7972.md) to their [yaḏ](../../strongs/h/h3028.md) to [šᵊnâ](../../strongs/h/h8133.md) and to [ḥăḇal](../../strongs/h/h2255.md) [dēḵ](../../strongs/h/h1791.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) which is at [Yᵊrûšlēm](../../strongs/h/h3390.md). ['ănā'](../../strongs/h/h576.md) [Daryāveš](../../strongs/h/h1868.md) have [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md); let it be [ʿăḇaḏ](../../strongs/h/h5648.md) with ['āsparnā'](../../strongs/h/h629.md).

<a name="ezra_6_13"></a>Ezra 6:13

['ĕḏayin](../../strongs/h/h116.md) [Tatnay](../../strongs/h/h8674.md), [peḥâ](../../strongs/h/h6347.md) on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), [Šᵊṯar Bôznay](../../strongs/h/h8370.md), and their [kᵊnāṯ](../../strongs/h/h3675.md), [qᵊḇēl](../../strongs/h/h6903.md) to that which [Daryāveš](../../strongs/h/h1868.md) the [meleḵ](../../strongs/h/h4430.md) had [šᵊlaḥ](../../strongs/h/h7972.md), [kᵊnēmā'](../../strongs/h/h3660.md) they [ʿăḇaḏ](../../strongs/h/h5648.md) ['āsparnā'](../../strongs/h/h629.md).

<a name="ezra_6_14"></a>Ezra 6:14

And the [śîḇ](../../strongs/h/h7868.md) of the [yᵊhûḏay](../../strongs/h/h3062.md) [bᵊnā'](../../strongs/h/h1124.md), and they [ṣᵊlaḥ](../../strongs/h/h6744.md) through the [nᵊḇû'â](../../strongs/h/h5017.md) of [Ḥagay](../../strongs/h/h2292.md) the [nᵊḇî'](../../strongs/h/h5029.md) and [Zᵊḵaryâ](../../strongs/h/h2148.md) the [bar](../../strongs/h/h1247.md) of [ʿIdô](../../strongs/h/h5714.md). And they [bᵊnā'](../../strongs/h/h1124.md), and [kᵊll](../../strongs/h/h3635.md) it, [min](../../strongs/h/h4481.md) to the [ṭaʿam](../../strongs/h/h2941.md) of the ['ĕlâ](../../strongs/h/h426.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md), and according to the [ṭᵊʿēm](../../strongs/h/h2942.md) of [Kôreš](../../strongs/h/h3567.md), and [Daryāveš](../../strongs/h/h1868.md), and ['Artaḥšaštᵊ'](../../strongs/h/h783.md) [meleḵ](../../strongs/h/h4430.md) of [Pāras](../../strongs/h/h6540.md).

<a name="ezra_6_15"></a>Ezra 6:15

And [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md) was [yᵊṣā'](../../strongs/h/h3319.md) [ʿaḏ](../../strongs/h/h5705.md) the [tᵊlāṯ](../../strongs/h/h8532.md) [yôm](../../strongs/h/h3118.md) of the [yᵊraḥ](../../strongs/h/h3393.md) ['ăḏār](../../strongs/h/h144.md), which [hû'](../../strongs/h/h1932.md) in the [šēṯ](../../strongs/h/h8353.md) [šᵊnâ](../../strongs/h/h8140.md) of the [malkû](../../strongs/h/h4437.md) of [Daryāveš](../../strongs/h/h1868.md) the [meleḵ](../../strongs/h/h4430.md).

<a name="ezra_6_16"></a>Ezra 6:16

And the [bēn](../../strongs/h/h1123.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md), the [kāhēn](../../strongs/h/h3549.md), and the [lēvay](../../strongs/h/h3879.md), and the [šᵊ'ār](../../strongs/h/h7606.md) of the [bēn](../../strongs/h/h1123.md) of the [gālûṯ](../../strongs/h/h1547.md), [ʿăḇaḏ](../../strongs/h/h5648.md) the [ḥănukā'](../../strongs/h/h2597.md) of [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) with [ḥeḏvâ](../../strongs/h/h2305.md),

<a name="ezra_6_17"></a>Ezra 6:17

And [qᵊrēḇ](../../strongs/h/h7127.md) at the [ḥănukā'](../../strongs/h/h2597.md) of [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) a [mᵊ'â](../../strongs/h/h3969.md) [tôr](../../strongs/h/h8450.md), two [mᵊ'â](../../strongs/h/h3969.md) [dᵊḵar](../../strongs/h/h1798.md), ['arbaʿ](../../strongs/h/h703.md) [mᵊ'â](../../strongs/h/h3969.md) ['immar](../../strongs/h/h563.md); and for a sin [ḥaṭṭā'â](../../strongs/h/h2402.md) [ḥaṭṭāyā'](../../strongs/h/h2409.md) [ʿal](../../strongs/h/h5922.md) [kōl](../../strongs/h/h3606.md) [Yiśrā'Ēl](../../strongs/h/h3479.md), [tᵊrên](../../strongs/h/h8648.md) [ʿăśar](../../strongs/h/h6236.md) [ṣᵊp̄îr](../../strongs/h/h6841.md) [ʿēz](../../strongs/h/h5796.md), according to the [minyān](../../strongs/h/h4510.md) of the [šᵊḇaṭ](../../strongs/h/h7625.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md).

<a name="ezra_6_18"></a>Ezra 6:18

And they [qûm](../../strongs/h/h6966.md) the [kāhēn](../../strongs/h/h3549.md) in their [pᵊlugâ](../../strongs/h/h6392.md), and the [lēvay](../../strongs/h/h3879.md) in their [maḥlᵊqâ](../../strongs/h/h4255.md), [ʿal](../../strongs/h/h5922.md) the [ʿăḇîḏâ](../../strongs/h/h5673.md) of ['ĕlâ](../../strongs/h/h426.md), which is at [Yᵊrûšlēm](../../strongs/h/h3390.md); as it is [kᵊṯāḇ](../../strongs/h/h3792.md) in the [sᵊp̄ar](../../strongs/h/h5609.md) of [Mōšê](../../strongs/h/h4873.md).

<a name="ezra_6_19"></a>Ezra 6:19

And the [ben](../../strongs/h/h1121.md) of the [gôlâ](../../strongs/h/h1473.md) ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) upon the fourteenth day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="ezra_6_20"></a>Ezra 6:20

For the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) were [ṭāhēr](../../strongs/h/h2891.md) together, all of them were [tahowr](../../strongs/h/h2889.md), and [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md) for all the [ben](../../strongs/h/h1121.md) of the [gôlâ](../../strongs/h/h1473.md), and for their ['ach](../../strongs/h/h251.md) the [kōhēn](../../strongs/h/h3548.md), and for themselves.

<a name="ezra_6_21"></a>Ezra 6:21

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which were [shuwb](../../strongs/h/h7725.md) out of [gôlâ](../../strongs/h/h1473.md), and all such as had [bāḏal](../../strongs/h/h914.md) themselves unto them from the [ṭām'â](../../strongs/h/h2932.md) of the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md), to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), did ['akal](../../strongs/h/h398.md),

<a name="ezra_6_22"></a>Ezra 6:22

And ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) seven [yowm](../../strongs/h/h3117.md) with [simchah](../../strongs/h/h8057.md): for [Yĕhovah](../../strongs/h/h3068.md) had made them [samach](../../strongs/h/h8055.md), and [cabab](../../strongs/h/h5437.md) the [leb](../../strongs/h/h3820.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) unto them, to [ḥāzaq](../../strongs/h/h2388.md) their [yad](../../strongs/h/h3027.md) in the [mĕla'kah](../../strongs/h/h4399.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 5](ezra_5.md) - [Ezra 7](ezra_7.md)