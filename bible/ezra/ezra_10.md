# [Ezra 10](https://www.blueletterbible.org/kjv/ezra/10)

<a name="ezra_10_1"></a>Ezra 10:1

Now when [ʿEzrā'](../../strongs/h/h5830.md) had [palal](../../strongs/h/h6419.md), and when he had [yadah](../../strongs/h/h3034.md), [bāḵâ](../../strongs/h/h1058.md) and [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), there [qāḇaṣ](../../strongs/h/h6908.md) unto him out of [Yisra'el](../../strongs/h/h3478.md) a [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [qāhēl](../../strongs/h/h6951.md) of ['enowsh](../../strongs/h/h582.md) and ['ishshah](../../strongs/h/h802.md) and [yeleḏ](../../strongs/h/h3206.md): for the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md) [rabah](../../strongs/h/h7235.md) [beḵê](../../strongs/h/h1059.md).

<a name="ezra_10_2"></a>Ezra 10:2

And [Šᵊḵanyâ](../../strongs/h/h7935.md) the [ben](../../strongs/h/h1121.md) of [Yᵊḥî'Ēl](../../strongs/h/h3171.md), one of the [ben](../../strongs/h/h1121.md) of [ʿÊlām](../../strongs/h/h5867.md), ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto [ʿEzrā'](../../strongs/h/h5830.md), We have [māʿal](../../strongs/h/h4603.md) against our ['Elohiym](../../strongs/h/h430.md), and have [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md): yet now there is [miqvê](../../strongs/h/h4723.md) in [Yisra'el](../../strongs/h/h3478.md) concerning this thing.

<a name="ezra_10_3"></a>Ezra 10:3

Now therefore let us [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with our ['Elohiym](../../strongs/h/h430.md) to [yāṣā'](../../strongs/h/h3318.md) all the ['ishshah](../../strongs/h/h802.md), and such as are [yalad](../../strongs/h/h3205.md) of them, according to the ['etsah](../../strongs/h/h6098.md) of my ['adonay](../../strongs/h/h136.md), and of those that [ḥārēḏ](../../strongs/h/h2730.md) at the [mitsvah](../../strongs/h/h4687.md) of our ['Elohiym](../../strongs/h/h430.md); and let it be ['asah](../../strongs/h/h6213.md) according to the [towrah](../../strongs/h/h8451.md).

<a name="ezra_10_4"></a>Ezra 10:4

[quwm](../../strongs/h/h6965.md); for this [dabar](../../strongs/h/h1697.md) belongeth unto thee: we also will be with thee: be [ḥāzaq](../../strongs/h/h2388.md), and ['asah](../../strongs/h/h6213.md) it.

<a name="ezra_10_5"></a>Ezra 10:5

Then [quwm](../../strongs/h/h6965.md) [ʿEzrā'](../../strongs/h/h5830.md), and made the [śar](../../strongs/h/h8269.md) [kōhēn](../../strongs/h/h3548.md), the [Lᵊvî](../../strongs/h/h3881.md), and all [Yisra'el](../../strongs/h/h3478.md), to [shaba'](../../strongs/h/h7650.md) that they should ['asah](../../strongs/h/h6213.md) according to this [dabar](../../strongs/h/h1697.md). And they [shaba'](../../strongs/h/h7650.md).

<a name="ezra_10_6"></a>Ezra 10:6

Then [ʿEzrā'](../../strongs/h/h5830.md) [quwm](../../strongs/h/h6965.md) from [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and [yālaḵ](../../strongs/h/h3212.md) into the [liškâ](../../strongs/h/h3957.md) of [Yᵊhôḥānān](../../strongs/h/h3076.md) the [ben](../../strongs/h/h1121.md) of ['Elyāšîḇ](../../strongs/h/h475.md): and when he [yālaḵ](../../strongs/h/h3212.md) thither, he did ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md), nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md): for he ['āḇal](../../strongs/h/h56.md) because of the [maʿal](../../strongs/h/h4604.md) of them that had been [gôlâ](../../strongs/h/h1473.md).

<a name="ezra_10_7"></a>Ezra 10:7

And they made ['abar](../../strongs/h/h5674.md) [qowl](../../strongs/h/h6963.md) throughout [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) unto all the [ben](../../strongs/h/h1121.md) of the [gôlâ](../../strongs/h/h1473.md), that they should [qāḇaṣ](../../strongs/h/h6908.md) themselves unto [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="ezra_10_8"></a>Ezra 10:8

And that whosoever would not [bow'](../../strongs/h/h935.md) within three [yowm](../../strongs/h/h3117.md), according to the ['etsah](../../strongs/h/h6098.md) of the [śar](../../strongs/h/h8269.md) and the [zāqēn](../../strongs/h/h2205.md), all his [rᵊḵûš](../../strongs/h/h7399.md) should be [ḥāram](../../strongs/h/h2763.md), and himself [bāḏal](../../strongs/h/h914.md) from the [qāhēl](../../strongs/h/h6951.md) of those that had been [gôlâ](../../strongs/h/h1473.md).

<a name="ezra_10_9"></a>Ezra 10:9

Then all the ['enowsh](../../strongs/h/h582.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md) [qāḇaṣ](../../strongs/h/h6908.md) themselves unto [Yĕruwshalaim](../../strongs/h/h3389.md) within three [yowm](../../strongs/h/h3117.md). It was the ninth [ḥōḏeš](../../strongs/h/h2320.md), on the twentieth day of the [ḥōḏeš](../../strongs/h/h2320.md); and all the ['am](../../strongs/h/h5971.md) [yashab](../../strongs/h/h3427.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), [rāʿaḏ](../../strongs/h/h7460.md) because of this [dabar](../../strongs/h/h1697.md), and for the [gešem](../../strongs/h/h1653.md).

<a name="ezra_10_10"></a>Ezra 10:10

And [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md) [quwm](../../strongs/h/h6965.md), and ['āmar](../../strongs/h/h559.md) unto them, Ye have [māʿal](../../strongs/h/h4603.md), and have [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md), to increase the ['ašmâ](../../strongs/h/h819.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="ezra_10_11"></a>Ezra 10:11

Now therefore [nathan](../../strongs/h/h5414.md) [tôḏâ](../../strongs/h/h8426.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md), and ['asah](../../strongs/h/h6213.md) his [ratsown](../../strongs/h/h7522.md): and [bāḏal](../../strongs/h/h914.md) yourselves from the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and from the [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md).

<a name="ezra_10_12"></a>Ezra 10:12

Then all the [qāhēl](../../strongs/h/h6951.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), As thou hast [dabar](../../strongs/h/h1697.md), so must we ['asah](../../strongs/h/h6213.md).

<a name="ezra_10_13"></a>Ezra 10:13

['ăḇāl](../../strongs/h/h61.md) the ['am](../../strongs/h/h5971.md) are [rab](../../strongs/h/h7227.md), and it is a [ʿēṯ](../../strongs/h/h6256.md) of much [gešem](../../strongs/h/h1653.md), and we are not [koach](../../strongs/h/h3581.md) to ['amad](../../strongs/h/h5975.md) [ḥûṣ](../../strongs/h/h2351.md), neither is this a [mĕla'kah](../../strongs/h/h4399.md) of one [yowm](../../strongs/h/h3117.md) or two: for we are [rabah](../../strongs/h/h7235.md) that have [pāšaʿ](../../strongs/h/h6586.md) in this [dabar](../../strongs/h/h1697.md).

<a name="ezra_10_14"></a>Ezra 10:14

Let now our [śar](../../strongs/h/h8269.md) of all the [qāhēl](../../strongs/h/h6951.md) ['amad](../../strongs/h/h5975.md), and let all them which have [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md) in our [ʿîr](../../strongs/h/h5892.md) [bow'](../../strongs/h/h935.md) at [zāman](../../strongs/h/h2163.md) [ʿēṯ](../../strongs/h/h6256.md), and with them the [zāqēn](../../strongs/h/h2205.md) of every [ʿîr](../../strongs/h/h5892.md), and the [shaphat](../../strongs/h/h8199.md) thereof, until the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of our ['Elohiym](../../strongs/h/h430.md) for this [dabar](../../strongs/h/h1697.md) be [shuwb](../../strongs/h/h7725.md) from us.

<a name="ezra_10_15"></a>Ezra 10:15

Only [Yônāṯān](../../strongs/h/h3129.md) the [ben](../../strongs/h/h1121.md) of [ʿĂśâ'Ēl](../../strongs/h/h6214.md) and [Yaḥzᵊyâ](../../strongs/h/h3167.md) the [ben](../../strongs/h/h1121.md) of [Tiqvâ](../../strongs/h/h8616.md) were ['amad](../../strongs/h/h5975.md) about this matter: and [Mᵊšullām](../../strongs/h/h4918.md) and [Šabṯay](../../strongs/h/h7678.md) the [Lᵊvî](../../strongs/h/h3881.md) [ʿāzar](../../strongs/h/h5826.md) them.

<a name="ezra_10_16"></a>Ezra 10:16

And the [ben](../../strongs/h/h1121.md) of the [gôlâ](../../strongs/h/h1473.md) did ['asah](../../strongs/h/h6213.md). And [ʿEzrā'](../../strongs/h/h5830.md) the [kōhēn](../../strongs/h/h3548.md), with ['enowsh](../../strongs/h/h582.md) [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), after the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), and all of them by their [shem](../../strongs/h/h8034.md), were [bāḏal](../../strongs/h/h914.md), and [yashab](../../strongs/h/h3427.md) in the first [yowm](../../strongs/h/h3117.md) of the tenth [ḥōḏeš](../../strongs/h/h2320.md) to [darash](../../strongs/h/h1875.md) the [dabar](../../strongs/h/h1697.md).

<a name="ezra_10_17"></a>Ezra 10:17

And they made a [kalah](../../strongs/h/h3615.md) with all the ['enowsh](../../strongs/h/h582.md) that had [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md) by the first [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="ezra_10_18"></a>Ezra 10:18

And among the [ben](../../strongs/h/h1121.md) of the [kōhēn](../../strongs/h/h3548.md) there were [māṣā'](../../strongs/h/h4672.md) that had [yashab](../../strongs/h/h3427.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md): namely, of the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md) the [ben](../../strongs/h/h1121.md) of [Yôṣāḏāq](../../strongs/h/h3136.md), and his ['ach](../../strongs/h/h251.md); [MaʿĂśêâ](../../strongs/h/h4641.md), and ['Ĕlîʿezer](../../strongs/h/h461.md), and [Yārîḇ](../../strongs/h/h3402.md), and [Gᵊḏalyâ](../../strongs/h/h1436.md).

<a name="ezra_10_19"></a>Ezra 10:19

And they [nathan](../../strongs/h/h5414.md) their [yad](../../strongs/h/h3027.md) that they would put [yāṣā'](../../strongs/h/h3318.md) their ['ishshah](../../strongs/h/h802.md); and being ['āšēm](../../strongs/h/h818.md), they offered an ['ayil](../../strongs/h/h352.md) of the [tso'n](../../strongs/h/h6629.md) for their ['ašmâ](../../strongs/h/h819.md).

<a name="ezra_10_20"></a>Ezra 10:20

And of the [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md); [Ḥănānî](../../strongs/h/h2607.md), and [Zᵊḇaḏyâ](../../strongs/h/h2069.md).

<a name="ezra_10_21"></a>Ezra 10:21

And of the [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md); [MaʿĂśêâ](../../strongs/h/h4641.md), and ['Ēlîyâ](../../strongs/h/h452.md), and [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and ['Uzziyah](../../strongs/h/h5818.md).

<a name="ezra_10_22"></a>Ezra 10:22

And of the [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md); ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), [MaʿĂśêâ](../../strongs/h/h4641.md), [Yišmāʿē'l](../../strongs/h/h3458.md), [Nᵊṯan'ēl](../../strongs/h/h5417.md), [Yôzāḇāḏ](../../strongs/h/h3107.md), and ['ElʿĀśâ](../../strongs/h/h501.md).

<a name="ezra_10_23"></a>Ezra 10:23

Also of the [Lᵊvî](../../strongs/h/h3881.md); [Yôzāḇāḏ](../../strongs/h/h3107.md), and [Šimʿî](../../strongs/h/h8096.md), and [Qēlāyâ](../../strongs/h/h7041.md) , (the same is [Qᵊlîṭā'](../../strongs/h/h7042.md),) [Pᵊṯaḥyâ](../../strongs/h/h6611.md), [Yehuwdah](../../strongs/h/h3063.md), and ['Ĕlîʿezer](../../strongs/h/h461.md).

<a name="ezra_10_24"></a>Ezra 10:24

Of the [shiyr](../../strongs/h/h7891.md) also; ['Elyāšîḇ](../../strongs/h/h475.md): and of the [šôʿēr](../../strongs/h/h7778.md); [Šallûm](../../strongs/h/h7967.md), and [Ṭelem](../../strongs/h/h2928.md), and ['ûrî](../../strongs/h/h221.md).

<a name="ezra_10_25"></a>Ezra 10:25

Moreover of [Yisra'el](../../strongs/h/h3478.md): of the [ben](../../strongs/h/h1121.md) of [ParʿŠ](../../strongs/h/h6551.md); [Rᵊmāyâ](../../strongs/h/h7422.md) , and [Yizzîyâ](../../strongs/h/h3150.md) , and [Malkîyâ](../../strongs/h/h4441.md), and [Minyāmîn](../../strongs/h/h4326.md), and ['Elʿāzār](../../strongs/h/h499.md), and [Malkîyâ](../../strongs/h/h4441.md), and [Bᵊnāyâ](../../strongs/h/h1141.md).

<a name="ezra_10_26"></a>Ezra 10:26

And of the [ben](../../strongs/h/h1121.md) of [ʿÊlām](../../strongs/h/h5867.md); [Matanyâ](../../strongs/h/h4983.md), [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [ʿAḇdî](../../strongs/h/h5660.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and ['Ēlîyâ](../../strongs/h/h452.md).

<a name="ezra_10_27"></a>Ezra 10:27

And of the [ben](../../strongs/h/h1121.md) of [Zatû'](../../strongs/h/h2240.md); ['ElyᵊhôʿÊnay](../../strongs/h/h454.md), ['Elyāšîḇ](../../strongs/h/h475.md), [Matanyâ](../../strongs/h/h4983.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and [Zāḇāḏ](../../strongs/h/h2066.md), and [ʿĂzîzā'](../../strongs/h/h5819.md) .

<a name="ezra_10_28"></a>Ezra 10:28

Of the [ben](../../strongs/h/h1121.md) also of [Bēḇay](../../strongs/h/h893.md); [Yᵊhôḥānān](../../strongs/h/h3076.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Zakay](../../strongs/h/h2140.md) [Zabay](../../strongs/h/h2079.md), and [ʿAṯlay](../../strongs/h/h6270.md) .

<a name="ezra_10_29"></a>Ezra 10:29

And of the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md); [Mᵊšullām](../../strongs/h/h4918.md), [Mallûḵ](../../strongs/h/h4409.md), and [ʿĂḏāyâ](../../strongs/h/h5718.md), [yāšûḇ](../../strongs/h/h3437.md), and [Šᵊ'Āl](../../strongs/h/h7594.md) , and [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md).

<a name="ezra_10_30"></a>Ezra 10:30

And of the [ben](../../strongs/h/h1121.md) of [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md); [ʿAḏnā'](../../strongs/h/h5733.md), and [Kᵊlāl](../../strongs/h/h3636.md) , [Bᵊnāyâ](../../strongs/h/h1141.md), [MaʿĂśêâ](../../strongs/h/h4641.md), [Matanyâ](../../strongs/h/h4983.md), [Bᵊṣal'ēl](../../strongs/h/h1212.md), and [Binnûy](../../strongs/h/h1131.md), and [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="ezra_10_31"></a>Ezra 10:31

And of the [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md); ['Ĕlîʿezer](../../strongs/h/h461.md), [Yiššîyâ](../../strongs/h/h3449.md), [Malkîyâ](../../strongs/h/h4441.md), [ŠᵊmaʿYâ](../../strongs/h/h8098.md), [Šimʿôn](../../strongs/h/h8095.md),

<a name="ezra_10_32"></a>Ezra 10:32

[Binyāmîn](../../strongs/h/h1144.md), [Mallûḵ](../../strongs/h/h4409.md), and [Šᵊmaryâ](../../strongs/h/h8114.md).

<a name="ezra_10_33"></a>Ezra 10:33

Of the [ben](../../strongs/h/h1121.md) of [Ḥāšum](../../strongs/h/h2828.md); [Matnay](../../strongs/h/h4982.md), [Matatâ](../../strongs/h/h4992.md) , [Zāḇāḏ](../../strongs/h/h2066.md), ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md), [Yᵊrēmay](../../strongs/h/h3413.md) , [Mᵊnaššê](../../strongs/h/h4519.md), and [Šimʿî](../../strongs/h/h8096.md).

<a name="ezra_10_34"></a>Ezra 10:34

Of the [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md); [MaʿĂḏay](../../strongs/h/h4572.md) , [ʿAmrām](../../strongs/h/h6019.md), and ['Û'Ēl](../../strongs/h/h177.md) ,

<a name="ezra_10_35"></a>Ezra 10:35

[Bᵊnāyâ](../../strongs/h/h1141.md), [Bēḏyâ](../../strongs/h/h912.md) , [Kᵊlûhay](../../strongs/h/h3622.md) ,

<a name="ezra_10_36"></a>Ezra 10:36

[Vanyâ](../../strongs/h/h2057.md) , [Mᵊrēmôṯ](../../strongs/h/h4822.md), ['Elyāšîḇ](../../strongs/h/h475.md),

<a name="ezra_10_37"></a>Ezra 10:37

[Matanyâ](../../strongs/h/h4983.md), [Matnay](../../strongs/h/h4982.md), and [YaʿĂśû](../../strongs/h/h3299.md) ,

<a name="ezra_10_38"></a>Ezra 10:38

And [Bānî](../../strongs/h/h1137.md), and [Binnûy](../../strongs/h/h1131.md), [Šimʿî](../../strongs/h/h8096.md),

<a name="ezra_10_39"></a>Ezra 10:39

And [Šelemyâ](../../strongs/h/h8018.md), and [Nāṯān](../../strongs/h/h5416.md), and [ʿĂḏāyâ](../../strongs/h/h5718.md),

<a name="ezra_10_40"></a>Ezra 10:40

[Maḵnaḏḇay](../../strongs/h/h4367.md) , [Šāšay](../../strongs/h/h8343.md) , [Šāray](../../strongs/h/h8298.md) ,

<a name="ezra_10_41"></a>Ezra 10:41

[ʿĂzar'Ēl](../../strongs/h/h5832.md), and [Šelemyâ](../../strongs/h/h8018.md), [Šᵊmaryâ](../../strongs/h/h8114.md),

<a name="ezra_10_42"></a>Ezra 10:42

[Šallûm](../../strongs/h/h7967.md), ['Ămaryâ](../../strongs/h/h568.md), and [Yôsēp̄](../../strongs/h/h3130.md).

<a name="ezra_10_43"></a>Ezra 10:43

Of the [ben](../../strongs/h/h1121.md) of [Nᵊḇô](../../strongs/h/h5015.md); [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), [Mataṯyâ](../../strongs/h/h4993.md), [Zāḇāḏ](../../strongs/h/h2066.md), [Zᵊḇînā'](../../strongs/h/h2081.md) , [Yidô](../../strongs/h/h3035.md), and [Yô'Ēl](../../strongs/h/h3100.md), [Bᵊnāyâ](../../strongs/h/h1141.md).

<a name="ezra_10_44"></a>Ezra 10:44

All these had [nasa'](../../strongs/h/h5375.md) [nasa'](../../strongs/h/h5375.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md): and some of them had ['ishshah](../../strongs/h/h802.md) by whom they [śûm](../../strongs/h/h7760.md) [ben](../../strongs/h/h1121.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 9](ezra_9.md)