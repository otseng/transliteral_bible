x# [Ezra 1](https://www.blueletterbible.org/kjv/ezra/1)

<a name="ezra_1_1"></a>Ezra 1:1

Now in the first [šānâ](../../strongs/h/h8141.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) might be [kalah](../../strongs/h/h3615.md), [Yĕhovah](../../strongs/h/h3068.md) stirred [ʿûr](../../strongs/h/h5782.md) the [ruwach](../../strongs/h/h7307.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), that he made an ['abar](../../strongs/h/h5674.md) [qowl](../../strongs/h/h6963.md) throughout all his [malkuwth](../../strongs/h/h4438.md), and put it also in [miḵtāḇ](../../strongs/h/h4385.md), ['āmar](../../strongs/h/h559.md),

<a name="ezra_1_2"></a>Ezra 1:2

Thus ['āmar](../../strongs/h/h559.md) [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md) hath [nathan](../../strongs/h/h5414.md) me all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md); and he hath [paqad](../../strongs/h/h6485.md) me to [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), which is in [Yehuwdah](../../strongs/h/h3063.md).

<a name="ezra_1_3"></a>Ezra 1:3

Who is there among you of all his ['am](../../strongs/h/h5971.md)? his ['Elohiym](../../strongs/h/h430.md) be with him, and let him [ʿālâ](../../strongs/h/h5927.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), which is in [Yehuwdah](../../strongs/h/h3063.md), and [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), (he is the ['Elohiym](../../strongs/h/h430.md),) which is in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_1_4"></a>Ezra 1:4

And whosoever [šā'ar](../../strongs/h/h7604.md) in any [maqowm](../../strongs/h/h4725.md) where he [guwr](../../strongs/h/h1481.md), let the ['enowsh](../../strongs/h/h582.md) of his [maqowm](../../strongs/h/h4725.md) [nasa'](../../strongs/h/h5375.md) him with [keceph](../../strongs/h/h3701.md), and with [zāhāḇ](../../strongs/h/h2091.md), and with [rᵊḵûš](../../strongs/h/h7399.md), and with [bĕhemah](../../strongs/h/h929.md), beside the [nᵊḏāḇâ](../../strongs/h/h5071.md) for the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) that is in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_1_5"></a>Ezra 1:5

Then [quwm](../../strongs/h/h6965.md) the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), and the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), with all them whose [ruwach](../../strongs/h/h7307.md) ['Elohiym](../../strongs/h/h430.md) had [ʿûr](../../strongs/h/h5782.md), to [ʿālâ](../../strongs/h/h5927.md) to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) which is in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_1_6"></a>Ezra 1:6

And all they that were [cabiyb](../../strongs/h/h5439.md) them [ḥāzaq](../../strongs/h/h2388.md) their [yad](../../strongs/h/h3027.md) with [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), with [zāhāḇ](../../strongs/h/h2091.md), with [rᵊḵûš](../../strongs/h/h7399.md), and with [bĕhemah](../../strongs/h/h929.md), and with [miḡdānôṯ](../../strongs/h/h4030.md), beside all that was [nāḏaḇ](../../strongs/h/h5068.md).

<a name="ezra_1_7"></a>Ezra 1:7

Also [Kôreš](../../strongs/h/h3566.md) the [melek](../../strongs/h/h4428.md) [yāṣā'](../../strongs/h/h3318.md) the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), which [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) had [yāṣā'](../../strongs/h/h3318.md) out of [Yĕruwshalaim](../../strongs/h/h3389.md), and had [nathan](../../strongs/h/h5414.md) them in the [bayith](../../strongs/h/h1004.md) of his ['Elohiym](../../strongs/h/h430.md);

<a name="ezra_1_8"></a>Ezra 1:8

Even those did [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md) [yāṣā'](../../strongs/h/h3318.md) by the [yad](../../strongs/h/h3027.md) of [Miṯrᵊḏāṯ](../../strongs/h/h4990.md) the [gizbār](../../strongs/h/h1489.md), and [sāp̄ar](../../strongs/h/h5608.md) them unto [Šēšbaṣṣar](../../strongs/h/h8339.md), the [nāśî'](../../strongs/h/h5387.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="ezra_1_9"></a>Ezra 1:9

And this is the [mispār](../../strongs/h/h4557.md) of them: thirty ['ăḡarṭāl](../../strongs/h/h105.md) of [zāhāḇ](../../strongs/h/h2091.md), a thousand ['ăḡarṭāl](../../strongs/h/h105.md) of [keceph](../../strongs/h/h3701.md), nine and twenty [maḥălāp̄](../../strongs/h/h4252.md),

<a name="ezra_1_10"></a>Ezra 1:10

Thirty [kᵊp̄ôr](../../strongs/h/h3713.md) of [zāhāḇ](../../strongs/h/h2091.md), [keceph](../../strongs/h/h3701.md) [kᵊp̄ôr](../../strongs/h/h3713.md) of a [mišnê](../../strongs/h/h4932.md) sort four hundred and ten, and ['aḥēr](../../strongs/h/h312.md) [kĕliy](../../strongs/h/h3627.md) a thousand.

<a name="ezra_1_11"></a>Ezra 1:11

All the [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md) and of [keceph](../../strongs/h/h3701.md) were five thousand and four hundred. All these did [Šēšbaṣṣar](../../strongs/h/h8339.md) [ʿālâ](../../strongs/h/h5927.md) with them of the [gôlâ](../../strongs/h/h1473.md) that were [ʿālâ](../../strongs/h/h5927.md) from [Bāḇel](../../strongs/h/h894.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 2](ezra_2.md)