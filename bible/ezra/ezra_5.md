# [Ezra 5](https://www.blueletterbible.org/kjv/ezra/5)

<a name="ezra_5_1"></a>Ezra 5:1

Then the [nᵊḇî'](../../strongs/h/h5029.md), [Ḥagay](../../strongs/h/h2292.md) the [nᵊḇî'](../../strongs/h/h5029.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md) the [bar](../../strongs/h/h1247.md) of [ʿIdô](../../strongs/h/h5714.md), [nᵊḇā'](../../strongs/h/h5013.md) [ʿal](../../strongs/h/h5922.md) the [yᵊhûḏay](../../strongs/h/h3062.md) that were in [Yᵊhûḏ](../../strongs/h/h3061.md) and [Yᵊrûšlēm](../../strongs/h/h3390.md) in the [šum](../../strongs/h/h8036.md) of the ['ĕlâ](../../strongs/h/h426.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md), even [ʿal](../../strongs/h/h5922.md) them.

<a name="ezra_5_2"></a>Ezra 5:2

['ĕḏayin](../../strongs/h/h116.md) [qûm](../../strongs/h/h6966.md) [Zᵊrubāḇel](../../strongs/h/h2217.md) the [bar](../../strongs/h/h1247.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7598.md), and [Yēšûaʿ](../../strongs/h/h3443.md) the [bar](../../strongs/h/h1247.md) of [Yôṣāḏāq](../../strongs/h/h3136.md), and [śᵊrâ](../../strongs/h/h8271.md) to [bᵊnā'](../../strongs/h/h1124.md) the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) which is at [Yᵊrûšlēm](../../strongs/h/h3390.md): and with [ʿim](../../strongs/h/h5974.md) were the [nᵊḇî'](../../strongs/h/h5029.md) of ['ĕlâ](../../strongs/h/h426.md) [sᵊʿaḏ](../../strongs/h/h5583.md) them.

<a name="ezra_5_3"></a>Ezra 5:3

At the same [zᵊmān](../../strongs/h/h2166.md) ['ăṯâ](../../strongs/h/h858.md) to them [Tatnay](../../strongs/h/h8674.md), [peḥâ](../../strongs/h/h6347.md) on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), and [Šᵊṯar Bôznay](../../strongs/h/h8370.md), and their [kᵊnāṯ](../../strongs/h/h3675.md), and ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md) unto [ʿal](../../strongs/h/h5922.md), [mān](../../strongs/h/h4479.md) hath [śûm](../../strongs/h/h7761.md) [ṭᵊʿēm](../../strongs/h/h2942.md) you to [bᵊnā'](../../strongs/h/h1124.md) [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md), and to make [kᵊll](../../strongs/h/h3635.md) [dēn](../../strongs/h/h1836.md) ['uššarnā'](../../strongs/h/h846.md)?

<a name="ezra_5_4"></a>Ezra 5:4

['ĕḏayin](../../strongs/h/h116.md) ['ămar](../../strongs/h/h560.md) we unto them after this [kᵊnēmā'](../../strongs/h/h3660.md), [mān](../../strongs/h/h4479.md) ['innûn](../../strongs/h/h581.md) the [šum](../../strongs/h/h8036.md) of the [gᵊḇar](../../strongs/h/h1400.md) that [bᵊnā'](../../strongs/h/h1124.md) [dēn](../../strongs/h/h1836.md) [binyān](../../strongs/h/h1147.md)?

<a name="ezra_5_5"></a>Ezra 5:5

But the [ʿayin](../../strongs/h/h5870.md) of their ['ĕlâ](../../strongs/h/h426.md) [hăvâ](../../strongs/h/h1934.md) [ʿal](../../strongs/h/h5922.md) the [śîḇ](../../strongs/h/h7868.md) of the [yᵊhûḏay](../../strongs/h/h3062.md), that they could [lā'](../../strongs/h/h3809.md) [bᵊṭēl](../../strongs/h/h989.md) [himmô](../../strongs/h/h1994.md) to [bᵊṭēl](../../strongs/h/h989.md), [ʿaḏ](../../strongs/h/h5705.md) the [ṭaʿam](../../strongs/h/h2941.md) [hûḵ](../../strongs/h/h1946.md) to [Daryāveš](../../strongs/h/h1868.md): and ['ĕḏayin](../../strongs/h/h116.md) they returned [tûḇ](../../strongs/h/h8421.md) by [ništᵊvān](../../strongs/h/h5407.md) [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) matter.

<a name="ezra_5_6"></a>Ezra 5:6

The [paršeḡen](../../strongs/h/h6573.md) of the ['igrā'](../../strongs/h/h104.md) that [Tatnay](../../strongs/h/h8674.md), [peḥâ](../../strongs/h/h6347.md) on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), and [Šᵊṯar Bôznay](../../strongs/h/h8370.md), and his [kᵊnāṯ](../../strongs/h/h3675.md) the ['Ăp̄ārsᵊḵāyē'](../../strongs/h/h671.md), which were on this [ʿăḇar](../../strongs/h/h5675.md) the [nᵊhar](../../strongs/h/h5103.md), [šᵊlaḥ](../../strongs/h/h7972.md) [ʿal](../../strongs/h/h5922.md) [Daryāveš](../../strongs/h/h1868.md) the [meleḵ](../../strongs/h/h4430.md):

<a name="ezra_5_7"></a>Ezra 5:7

They [šᵊlaḥ](../../strongs/h/h7972.md) a [piṯgām](../../strongs/h/h6600.md) [ʿal](../../strongs/h/h5922.md) him, [gav](../../strongs/h/h1459.md) was [kᵊṯaḇ](../../strongs/h/h3790.md) [dēn](../../strongs/h/h1836.md); Unto [Daryāveš](../../strongs/h/h1868.md) the [meleḵ](../../strongs/h/h4430.md), [kōl](../../strongs/h/h3606.md) [šᵊlām](../../strongs/h/h8001.md).

<a name="ezra_5_8"></a>Ezra 5:8

Be it [hăvâ](../../strongs/h/h1934.md) [yᵊḏaʿ](../../strongs/h/h3046.md) unto the [meleḵ](../../strongs/h/h4430.md), that we ['ăzal](../../strongs/h/h236.md) into the [mᵊḏînâ](../../strongs/h/h4083.md) of [Yᵊhûḏ](../../strongs/h/h3061.md), to the [bayiṯ](../../strongs/h/h1005.md) of the [raḇ](../../strongs/h/h7229.md) ['ĕlâ](../../strongs/h/h426.md), which is [bᵊnā'](../../strongs/h/h1124.md) with [gᵊlāl](../../strongs/h/h1560.md) ['eḇen](../../strongs/h/h69.md), and ['āʿ](../../strongs/h/h636.md) is [śûm](../../strongs/h/h7761.md) in the [kᵊṯal](../../strongs/h/h3797.md), and [dēḵ](../../strongs/h/h1791.md) [ʿăḇîḏâ](../../strongs/h/h5673.md) [ʿăḇaḏ](../../strongs/h/h5648.md) ['āsparnā'](../../strongs/h/h629.md) on, and [ṣᵊlaḥ](../../strongs/h/h6744.md) in their [yaḏ](../../strongs/h/h3028.md).

<a name="ezra_5_9"></a>Ezra 5:9

['ĕḏayin](../../strongs/h/h116.md) [šᵊ'ēl](../../strongs/h/h7593.md) we ['illēḵ](../../strongs/h/h479.md) [śîḇ](../../strongs/h/h7868.md), and ['ămar](../../strongs/h/h560.md) unto them [kᵊnēmā'](../../strongs/h/h3660.md), [mān](../../strongs/h/h4479.md) [śûm](../../strongs/h/h7761.md) [ṭᵊʿēm](../../strongs/h/h2942.md) you to [bᵊnā'](../../strongs/h/h1124.md) [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md), and to make [kᵊll](../../strongs/h/h3635.md) [dēn](../../strongs/h/h1836.md) ['uššarnā'](../../strongs/h/h846.md)?

<a name="ezra_5_10"></a>Ezra 5:10

We [šᵊ'ēl](../../strongs/h/h7593.md) their [šum](../../strongs/h/h8036.md) ['ap̄](../../strongs/h/h638.md), to [yᵊḏaʿ](../../strongs/h/h3046.md) thee, that we might [kᵊṯaḇ](../../strongs/h/h3790.md) the [šum](../../strongs/h/h8036.md) of the [gᵊḇar](../../strongs/h/h1400.md) that were the [rē'š](../../strongs/h/h7217.md) of them.

<a name="ezra_5_11"></a>Ezra 5:11

And [kᵊnēmā'](../../strongs/h/h3660.md) they [tûḇ](../../strongs/h/h8421.md) us [piṯgām](../../strongs/h/h6600.md), ['ămar](../../strongs/h/h560.md), ['ănaḥnā'](../../strongs/h/h586.md) [himmô](../../strongs/h/h1994.md) the [ʿăḇaḏ](../../strongs/h/h5649.md) of the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md) and ['ăraʿ](../../strongs/h/h772.md), and [bᵊnā'](../../strongs/h/h1124.md) the [bayiṯ](../../strongs/h/h1005.md) that [hăvâ](../../strongs/h/h1934.md) [bᵊnā'](../../strongs/h/h1124.md) [dēn](../../strongs/h/h1836.md) [śagî'](../../strongs/h/h7690.md) [šᵊnâ](../../strongs/h/h8140.md) [qaḏmâ](../../strongs/h/h6928.md), which a [raḇ](../../strongs/h/h7229.md) [meleḵ](../../strongs/h/h4430.md) of [Yiśrā'Ēl](../../strongs/h/h3479.md) [bᵊnā'](../../strongs/h/h1124.md) and set [kᵊll](../../strongs/h/h3635.md).

<a name="ezra_5_12"></a>Ezra 5:12

[lāhēn](../../strongs/h/h3861.md) [min](../../strongs/h/h4481.md) that our ['ab](../../strongs/h/h2.md) had [rᵊḡaz](../../strongs/h/h7265.md) the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md) unto [rᵊḡaz](../../strongs/h/h7265.md), he [yᵊhaḇ](../../strongs/h/h3052.md) [himmô](../../strongs/h/h1994.md) into the [yaḏ](../../strongs/h/h3028.md) of [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) of [Bāḇel](../../strongs/h/h895.md), the [kasdî](../../strongs/h/h3679.md), who [sᵊṯar](../../strongs/h/h5642.md) [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md), and [gᵊlâ](../../strongs/h/h1541.md) the [ʿam](../../strongs/h/h5972.md) [gᵊlâ](../../strongs/h/h1541.md) into [Bāḇel](../../strongs/h/h895.md).

<a name="ezra_5_13"></a>Ezra 5:13

[bᵊram](../../strongs/h/h1297.md) in the [ḥaḏ](../../strongs/h/h2298.md) [šᵊnâ](../../strongs/h/h8140.md) of [Kôreš](../../strongs/h/h3567.md) the [meleḵ](../../strongs/h/h4430.md) of [Bāḇel](../../strongs/h/h895.md) the same [meleḵ](../../strongs/h/h4430.md) [Kôreš](../../strongs/h/h3567.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md) to [bᵊnā'](../../strongs/h/h1124.md) [dēn](../../strongs/h/h1836.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md).

<a name="ezra_5_14"></a>Ezra 5:14

And the [mā'n](../../strongs/h/h3984.md) ['ap̄](../../strongs/h/h638.md) [dî](../../strongs/h/h1768.md) [dᵊhaḇ](../../strongs/h/h1722.md) and [kᵊsap̄](../../strongs/h/h3702.md) of the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md), which [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [nᵊp̄aq](../../strongs/h/h5312.md) out [min](../../strongs/h/h4481.md) the [hêḵal](../../strongs/h/h1965.md) that was in [Yᵊrûšlēm](../../strongs/h/h3390.md), and [yᵊḇal](../../strongs/h/h2987.md) them into the [hêḵal](../../strongs/h/h1965.md) of [Bāḇel](../../strongs/h/h895.md), [himmô](../../strongs/h/h1994.md) did [Kôreš](../../strongs/h/h3567.md) the [meleḵ](../../strongs/h/h4430.md) [nᵊp̄aq](../../strongs/h/h5312.md) out [min](../../strongs/h/h4481.md) the [hêḵal](../../strongs/h/h1965.md) of [Bāḇel](../../strongs/h/h895.md), and they were [yᵊhaḇ](../../strongs/h/h3052.md) unto one, whose [šum](../../strongs/h/h8036.md) was [Šēšbaṣṣar](../../strongs/h/h8340.md), whom he had [śûm](../../strongs/h/h7761.md) [peḥâ](../../strongs/h/h6347.md);

<a name="ezra_5_15"></a>Ezra 5:15

And ['ămar](../../strongs/h/h560.md) unto him, [nᵊśā'](../../strongs/h/h5376.md) ['ēl](../../strongs/h/h412.md) [mā'n](../../strongs/h/h3984.md), ['ăzal](../../strongs/h/h236.md), [nᵊḥaṯ](../../strongs/h/h5182.md) [himmô](../../strongs/h/h1994.md) into the [hêḵal](../../strongs/h/h1965.md) that is in [Yᵊrûšlēm](../../strongs/h/h3390.md), and let the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) be [bᵊnā'](../../strongs/h/h1124.md) [ʿal](../../strongs/h/h5922.md) his ['ăṯar](../../strongs/h/h870.md).

<a name="ezra_5_16"></a>Ezra 5:16

['ĕḏayin](../../strongs/h/h116.md) ['ăṯâ](../../strongs/h/h858.md) the [dēḵ](../../strongs/h/h1791.md) [Šēšbaṣṣar](../../strongs/h/h8340.md), and [yᵊhaḇ](../../strongs/h/h3052.md) the ['š](../../strongs/h/h787.md) of the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) which is in [Yᵊrûšlēm](../../strongs/h/h3390.md): and [min](../../strongs/h/h4481.md) that ['ĕḏayin](../../strongs/h/h116.md) even [ʿaḏ](../../strongs/h/h5705.md) [kᵊʿan](../../strongs/h/h3705.md) hath it been in [bᵊnā'](../../strongs/h/h1124.md), and yet it is [lā'](../../strongs/h/h3809.md) [šᵊlam](../../strongs/h/h8000.md).

<a name="ezra_5_17"></a>Ezra 5:17

[kᵊʿan](../../strongs/h/h3705.md) therefore, [hēn](../../strongs/h/h2006.md) it seem [ṭāḇ](../../strongs/h/h2869.md) [ʿal](../../strongs/h/h5922.md) the [meleḵ](../../strongs/h/h4430.md), let there be [bᵊqar](../../strongs/h/h1240.md) made in the [meleḵ](../../strongs/h/h4430.md) [gᵊnaz](../../strongs/h/h1596.md) [bayiṯ](../../strongs/h/h1005.md), which is [tammâ](../../strongs/h/h8536.md) at [Bāḇel](../../strongs/h/h895.md), [hēn](../../strongs/h/h2006.md) it ['îṯay](../../strongs/h/h383.md) so, that a [ṭᵊʿēm](../../strongs/h/h2942.md) was [śûm](../../strongs/h/h7761.md) [min](../../strongs/h/h4481.md) [Kôreš](../../strongs/h/h3567.md) the [meleḵ](../../strongs/h/h4430.md) to [bᵊnā'](../../strongs/h/h1124.md) [dēḵ](../../strongs/h/h1791.md) [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) at [Yᵊrûšlēm](../../strongs/h/h3390.md), and let the [meleḵ](../../strongs/h/h4430.md) [šᵊlaḥ](../../strongs/h/h7972.md) his [rᵊʿûṯ](../../strongs/h/h7470.md) to us [ʿal](../../strongs/h/h5922.md) this [dēn](../../strongs/h/h1836.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 4](ezra_4.md) - [Ezra 6](ezra_6.md)