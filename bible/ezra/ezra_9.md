# [Ezra 9](https://www.blueletterbible.org/kjv/ezra/9)

<a name="ezra_9_1"></a>Ezra 9:1

Now when these things were [kalah](../../strongs/h/h3615.md), the [śar](../../strongs/h/h8269.md) [nāḡaš](../../strongs/h/h5066.md) to me, ['āmar](../../strongs/h/h559.md), The ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md), and the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), have not [bāḏal](../../strongs/h/h914.md) themselves from the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), doing according to their [tôʿēḇâ](../../strongs/h/h8441.md), even of the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [Ḥitî](../../strongs/h/h2850.md), the [Pᵊrizzî](../../strongs/h/h6522.md), the [Yᵊḇûsî](../../strongs/h/h2983.md), the [ʿAmmôn](../../strongs/h/h5984.md), the [Mô'āḇî](../../strongs/h/h4125.md), the [Miṣrî](../../strongs/h/h4713.md), and the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="ezra_9_2"></a>Ezra 9:2

For they have [nasa'](../../strongs/h/h5375.md) of their [bath](../../strongs/h/h1323.md) for themselves, and for their [ben](../../strongs/h/h1121.md): so that the [qodesh](../../strongs/h/h6944.md) [zera'](../../strongs/h/h2233.md) have [ʿāraḇ](../../strongs/h/h6148.md) themselves with the ['am](../../strongs/h/h5971.md) of those ['erets](../../strongs/h/h776.md): yea, the [yad](../../strongs/h/h3027.md) of the [śar](../../strongs/h/h8269.md) and [sāḡān](../../strongs/h/h5461.md) hath been [ri'šôn](../../strongs/h/h7223.md) in this [maʿal](../../strongs/h/h4604.md).

<a name="ezra_9_3"></a>Ezra 9:3

And when I [shama'](../../strongs/h/h8085.md) this [dabar](../../strongs/h/h1697.md), I [qāraʿ](../../strongs/h/h7167.md) my [beḡeḏ](../../strongs/h/h899.md) and my [mᵊʿîl](../../strongs/h/h4598.md), and [māraṭ](../../strongs/h/h4803.md) the [śēʿār](../../strongs/h/h8181.md) of my [ro'sh](../../strongs/h/h7218.md) and of my [zāqān](../../strongs/h/h2206.md), and [yashab](../../strongs/h/h3427.md) [šāmēm](../../strongs/h/h8074.md).

<a name="ezra_9_4"></a>Ezra 9:4

Then were ['āsap̄](../../strongs/h/h622.md) unto me every one that [ḥārēḏ](../../strongs/h/h2730.md) at the [dabar](../../strongs/h/h1697.md) of the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), because of the [maʿal](../../strongs/h/h4604.md) of those that had been [gôlâ](../../strongs/h/h1473.md); and I [yashab](../../strongs/h/h3427.md) [šāmēm](../../strongs/h/h8074.md) until the ['ereb](../../strongs/h/h6153.md) [minchah](../../strongs/h/h4503.md).

<a name="ezra_9_5"></a>Ezra 9:5

And at the ['ereb](../../strongs/h/h6153.md) [minchah](../../strongs/h/h4503.md) I [quwm](../../strongs/h/h6965.md) from my [taʿănîṯ](../../strongs/h/h8589.md); and having [qāraʿ](../../strongs/h/h7167.md) my [beḡeḏ](../../strongs/h/h899.md) and my [mᵊʿîl](../../strongs/h/h4598.md), I [kara'](../../strongs/h/h3766.md) upon my [bereḵ](../../strongs/h/h1290.md), and spread [pāraś](../../strongs/h/h6566.md) my [kaph](../../strongs/h/h3709.md) unto [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md),

<a name="ezra_9_6"></a>Ezra 9:6

And ['āmar](../../strongs/h/h559.md), O my ['Elohiym](../../strongs/h/h430.md), I am [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md) to [ruwm](../../strongs/h/h7311.md) my [paniym](../../strongs/h/h6440.md) to thee, my ['Elohiym](../../strongs/h/h430.md): for our ['avon](../../strongs/h/h5771.md) are [rabah](../../strongs/h/h7235.md) [maʿal](../../strongs/h/h4605.md) our [ro'sh](../../strongs/h/h7218.md), and our ['ašmâ](../../strongs/h/h819.md) is [gāḏal](../../strongs/h/h1431.md) unto the [shamayim](../../strongs/h/h8064.md).

<a name="ezra_9_7"></a>Ezra 9:7

Since the [yowm](../../strongs/h/h3117.md) of our ['ab](../../strongs/h/h1.md) have we been in a [gadowl](../../strongs/h/h1419.md) ['ašmâ](../../strongs/h/h819.md) unto this [yowm](../../strongs/h/h3117.md); and for our ['avon](../../strongs/h/h5771.md) have we, our [melek](../../strongs/h/h4428.md), and our [kōhēn](../../strongs/h/h3548.md), been [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md), to the [chereb](../../strongs/h/h2719.md), to [šᵊḇî](../../strongs/h/h7628.md), and to a [bizzâ](../../strongs/h/h961.md), and to [bšeṯ](../../strongs/h/h1322.md) of [paniym](../../strongs/h/h6440.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="ezra_9_8"></a>Ezra 9:8

And now for a [mᵊʿaṭ](../../strongs/h/h4592.md) [reḡaʿ](../../strongs/h/h7281.md) [tĕchinnah](../../strongs/h/h8467.md) hath been shewed from [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), to [šā'ar](../../strongs/h/h7604.md) us a remnant to [pᵊlêṭâ](../../strongs/h/h6413.md), and to [nathan](../../strongs/h/h5414.md) us a [yāṯēḏ](../../strongs/h/h3489.md) in his [qodesh](../../strongs/h/h6944.md) [maqowm](../../strongs/h/h4725.md), that our ['Elohiym](../../strongs/h/h430.md) may ['owr](../../strongs/h/h215.md) our ['ayin](../../strongs/h/h5869.md), and [nathan](../../strongs/h/h5414.md) us a [mᵊʿaṭ](../../strongs/h/h4592.md) [miḥyâ](../../strongs/h/h4241.md) in our [ʿaḇḏûṯ](../../strongs/h/h5659.md).

<a name="ezra_9_9"></a>Ezra 9:9

For we were ['ebed](../../strongs/h/h5650.md); yet our ['Elohiym](../../strongs/h/h430.md) hath not ['azab](../../strongs/h/h5800.md) us in our [ʿaḇḏûṯ](../../strongs/h/h5659.md), but hath [natah](../../strongs/h/h5186.md) [checed](../../strongs/h/h2617.md) unto us in the [paniym](../../strongs/h/h6440.md) of the [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), to [nathan](../../strongs/h/h5414.md) us a [miḥyâ](../../strongs/h/h4241.md), to [ruwm](../../strongs/h/h7311.md) the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), and to ['amad](../../strongs/h/h5975.md) the [chorbah](../../strongs/h/h2723.md) thereof, and to [nathan](../../strongs/h/h5414.md) us a [gāḏēr](../../strongs/h/h1447.md) in [Yehuwdah](../../strongs/h/h3063.md) and in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="ezra_9_10"></a>Ezra 9:10

And now, O our ['Elohiym](../../strongs/h/h430.md), what shall we ['āmar](../../strongs/h/h559.md) ['aḥar](../../strongs/h/h310.md) this? for we have ['azab](../../strongs/h/h5800.md) thy [mitsvah](../../strongs/h/h4687.md),

<a name="ezra_9_11"></a>Ezra 9:11

Which thou hast [tsavah](../../strongs/h/h6680.md) [yad](../../strongs/h/h3027.md) thy ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md), The ['erets](../../strongs/h/h776.md), unto which ye [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it, is a [nidâ](../../strongs/h/h5079.md) ['erets](../../strongs/h/h776.md) with the [nidâ](../../strongs/h/h5079.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), with their [tôʿēḇâ](../../strongs/h/h8441.md), which have [mālā'](../../strongs/h/h4390.md) it from one [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md) with their [ṭām'â](../../strongs/h/h2932.md).

<a name="ezra_9_12"></a>Ezra 9:12

Now therefore [nathan](../../strongs/h/h5414.md) not your [bath](../../strongs/h/h1323.md) unto their [ben](../../strongs/h/h1121.md), neither [nasa'](../../strongs/h/h5375.md) their [bath](../../strongs/h/h1323.md) unto your [ben](../../strongs/h/h1121.md), nor [darash](../../strongs/h/h1875.md) their [shalowm](../../strongs/h/h7965.md) or their [towb](../../strongs/h/h2896.md) ['owlam](../../strongs/h/h5769.md): that ye may be [ḥāzaq](../../strongs/h/h2388.md), and ['akal](../../strongs/h/h398.md) the [ṭûḇ](../../strongs/h/h2898.md) of the ['erets](../../strongs/h/h776.md), and leave it for a [yarash](../../strongs/h/h3423.md) to your [ben](../../strongs/h/h1121.md) ['owlam](../../strongs/h/h5769.md).

<a name="ezra_9_13"></a>Ezra 9:13

And ['aḥar](../../strongs/h/h310.md) all that is [bow'](../../strongs/h/h935.md) upon us for our [ra'](../../strongs/h/h7451.md) [ma'aseh](../../strongs/h/h4639.md), and for our [gadowl](../../strongs/h/h1419.md) ['ašmâ](../../strongs/h/h819.md), seeing that thou our ['Elohiym](../../strongs/h/h430.md) hast [ḥāśaḵ](../../strongs/h/h2820.md) us [maṭṭâ](../../strongs/h/h4295.md) than our ['avon](../../strongs/h/h5771.md) deserve, and hast [nathan](../../strongs/h/h5414.md) us such [pᵊlêṭâ](../../strongs/h/h6413.md) as this;

<a name="ezra_9_14"></a>Ezra 9:14

Should we [shuwb](../../strongs/h/h7725.md) [pārar](../../strongs/h/h6565.md) thy [mitsvah](../../strongs/h/h4687.md), and join in [ḥāṯan](../../strongs/h/h2859.md) with the ['am](../../strongs/h/h5971.md) of these [tôʿēḇâ](../../strongs/h/h8441.md)? wouldest not thou be ['anaph](../../strongs/h/h599.md) with us till thou hadst [kalah](../../strongs/h/h3615.md) us, so that there should be no [šᵊ'ērîṯ](../../strongs/h/h7611.md) nor [pᵊlêṭâ](../../strongs/h/h6413.md)?

<a name="ezra_9_15"></a>Ezra 9:15

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), thou art [tsaddiyq](../../strongs/h/h6662.md): for we [šā'ar](../../strongs/h/h7604.md) yet [pᵊlêṭâ](../../strongs/h/h6413.md), as it is this [yowm](../../strongs/h/h3117.md): behold, we are [paniym](../../strongs/h/h6440.md) thee in our ['ašmâ](../../strongs/h/h819.md): for we cannot ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) thee because of this.

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 8](ezra_8.md) - [Ezra 10](ezra_10.md)