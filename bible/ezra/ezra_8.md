# [Ezra 8](https://www.blueletterbible.org/kjv/ezra/8)

<a name="ezra_8_1"></a>Ezra 8:1

These are now the [ro'sh](../../strongs/h/h7218.md) of their ['ab](../../strongs/h/h1.md), and this is the [yāḥaś](../../strongs/h/h3187.md) of them that [ʿālâ](../../strongs/h/h5927.md) with me from [Bāḇel](../../strongs/h/h894.md), in the [malkuwth](../../strongs/h/h4438.md) of ['Artaḥšaštᵊ'](../../strongs/h/h783.md) the [melek](../../strongs/h/h4428.md).

<a name="ezra_8_2"></a>Ezra 8:2

Of the [ben](../../strongs/h/h1121.md) of [Pînḥās](../../strongs/h/h6372.md); [Gēršōm](../../strongs/h/h1647.md): of the [ben](../../strongs/h/h1121.md) of ['Îṯāmār](../../strongs/h/h385.md); [Dinîyē'L](../../strongs/h/h1840.md): of the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md); [Ḥaṭṭûš](../../strongs/h/h2407.md).

<a name="ezra_8_3"></a>Ezra 8:3

Of the [ben](../../strongs/h/h1121.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md), of the [ben](../../strongs/h/h1121.md) of [ParʿŠ](../../strongs/h/h6551.md); [Zᵊḵaryâ](../../strongs/h/h2148.md): and with him were [yāḥaś](../../strongs/h/h3187.md) of the [zāḵār](../../strongs/h/h2145.md) an hundred and fifty.

<a name="ezra_8_4"></a>Ezra 8:4

Of the [ben](../../strongs/h/h1121.md) of [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md); ['ElyᵊhôʿÊnay](../../strongs/h/h454.md) the [ben](../../strongs/h/h1121.md) of [Zᵊraḥyâ](../../strongs/h/h2228.md), and with him two hundred [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_5"></a>Ezra 8:5

Of the [ben](../../strongs/h/h1121.md) of [Šᵊḵanyâ](../../strongs/h/h7935.md); the [ben](../../strongs/h/h1121.md) of [Yaḥăzî'Ēl](../../strongs/h/h3166.md), and with him three hundred [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_6"></a>Ezra 8:6

Of the [ben](../../strongs/h/h1121.md) also of [ʿĀḏîn](../../strongs/h/h5720.md); [ʿEḇeḏ](../../strongs/h/h5651.md) the [ben](../../strongs/h/h1121.md) of [Yônāṯān](../../strongs/h/h3129.md), and with him fifty [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_7"></a>Ezra 8:7

And of the [ben](../../strongs/h/h1121.md) of [ʿÊlām](../../strongs/h/h5867.md); [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of [ʿĂṯalyâ](../../strongs/h/h6271.md), and with him seventy [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_8"></a>Ezra 8:8

And of the [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md); [Zᵊḇaḏyâ](../../strongs/h/h2069.md) the [ben](../../strongs/h/h1121.md) of [Mîḵā'ēl](../../strongs/h/h4317.md), and with him fourscore [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_9"></a>Ezra 8:9

Of the [ben](../../strongs/h/h1121.md) of [Yô'āḇ](../../strongs/h/h3097.md); [ʿŌḇaḏyâ](../../strongs/h/h5662.md) the [ben](../../strongs/h/h1121.md) of [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and with him two hundred and eighteen [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_10"></a>Ezra 8:10

And of the [ben](../../strongs/h/h1121.md) of [Šᵊlōmîṯ](../../strongs/h/h8019.md); the [ben](../../strongs/h/h1121.md) of [Yôsip̄Yâ](../../strongs/h/h3131.md), and with him an hundred and threescore [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_11"></a>Ezra 8:11

And of the [ben](../../strongs/h/h1121.md) of [Bēḇay](../../strongs/h/h893.md); [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [Bēḇay](../../strongs/h/h893.md), and with him twenty and eight [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_12"></a>Ezra 8:12

And of the [ben](../../strongs/h/h1121.md) of [ʿAzgāḏ](../../strongs/h/h5803.md); [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qāṭān](../../strongs/h/h6997.md), and with him an hundred and ten [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_13"></a>Ezra 8:13

And of the ['aḥărôn](../../strongs/h/h314.md) [ben](../../strongs/h/h1121.md) of ['Ăḏōnîqām](../../strongs/h/h140.md), whose [shem](../../strongs/h/h8034.md) are these, ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md), [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), and [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and with them threescore [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_14"></a>Ezra 8:14

Of the [ben](../../strongs/h/h1121.md) also of [Biḡvay](../../strongs/h/h902.md); [ʿÛṯay](../../strongs/h/h5793.md), and [Zabûḏ](../../strongs/h/h2072.md) [Zakûr](../../strongs/h/h2139.md), and with them seventy [zāḵār](../../strongs/h/h2145.md).

<a name="ezra_8_15"></a>Ezra 8:15

And I [qāḇaṣ](../../strongs/h/h6908.md) them to the [nāhār](../../strongs/h/h5104.md) that [bow'](../../strongs/h/h935.md) to ['Ahăvā'](../../strongs/h/h163.md); and there [ḥānâ](../../strongs/h/h2583.md) three [yowm](../../strongs/h/h3117.md): and I [bîn](../../strongs/h/h995.md) the ['am](../../strongs/h/h5971.md), and the [kōhēn](../../strongs/h/h3548.md), and [māṣā'](../../strongs/h/h4672.md) there none of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="ezra_8_16"></a>Ezra 8:16

Then [shalach](../../strongs/h/h7971.md) I for ['Ĕlîʿezer](../../strongs/h/h461.md), for ['ărî'ēl](../../strongs/h/h740.md), for [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and for ['Elnāṯān](../../strongs/h/h494.md), and for [Yārîḇ](../../strongs/h/h3402.md), and for ['Elnāṯān](../../strongs/h/h494.md), and for [Nāṯān](../../strongs/h/h5416.md), and for [Zᵊḵaryâ](../../strongs/h/h2148.md), and for [Mᵊšullām](../../strongs/h/h4918.md), [ro'sh](../../strongs/h/h7218.md); also for [Yôyārîḇ](../../strongs/h/h3114.md), and for ['Elnāṯān](../../strongs/h/h494.md), men of [bîn](../../strongs/h/h995.md).

<a name="ezra_8_17"></a>Ezra 8:17

And I [tsavah](../../strongs/h/h6680.md) them with [yāṣā'](../../strongs/h/h3318.md) unto ['Idô](../../strongs/h/h112.md) the [ro'sh](../../strongs/h/h7218.md) at the [maqowm](../../strongs/h/h4725.md) [Kāsip̄Yā'](../../strongs/h/h3703.md), and I [śûm](../../strongs/h/h7760.md) [peh](../../strongs/h/h6310.md) them [dabar](../../strongs/h/h1697.md) they should [dabar](../../strongs/h/h1696.md) unto ['Idô](../../strongs/h/h112.md), and to his ['ach](../../strongs/h/h251.md) the [Nāṯîn](../../strongs/h/h5411.md) [Nāṯîn](../../strongs/h/h5411.md), at the [maqowm](../../strongs/h/h4725.md) [Kāsip̄Yā'](../../strongs/h/h3703.md), that they should [bow'](../../strongs/h/h935.md) unto us [sharath](../../strongs/h/h8334.md) for the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="ezra_8_18"></a>Ezra 8:18

And by the [towb](../../strongs/h/h2896.md) [yad](../../strongs/h/h3027.md) of our ['Elohiym](../../strongs/h/h430.md) upon us they [bow'](../../strongs/h/h935.md) us an ['iysh](../../strongs/h/h376.md) of [śēḵel](../../strongs/h/h7922.md), of the [ben](../../strongs/h/h1121.md) of [Maḥlî](../../strongs/h/h4249.md), the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and [Šērēḇyâ](../../strongs/h/h8274.md), with his [ben](../../strongs/h/h1121.md) and his ['ach](../../strongs/h/h251.md), eighteen ;

<a name="ezra_8_19"></a>Ezra 8:19

And [Ḥăšaḇyâ](../../strongs/h/h2811.md), and with him [Yᵊšaʿyâ](../../strongs/h/h3470.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), his ['ach](../../strongs/h/h251.md) and their [ben](../../strongs/h/h1121.md), twenty;

<a name="ezra_8_20"></a>Ezra 8:20

Also of the [Nāṯîn](../../strongs/h/h5411.md), whom [Dāviḏ](../../strongs/h/h1732.md) and the [śar](../../strongs/h/h8269.md) had [nathan](../../strongs/h/h5414.md) for the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [Lᵊvî](../../strongs/h/h3881.md), two hundred and twenty [Nāṯîn](../../strongs/h/h5411.md): all of them were [nāqaḇ](../../strongs/h/h5344.md) by [shem](../../strongs/h/h8034.md).

<a name="ezra_8_21"></a>Ezra 8:21

Then I [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md) there, at the [nāhār](../../strongs/h/h5104.md) of ['Ahăvā'](../../strongs/h/h163.md), that we might [ʿānâ](../../strongs/h/h6031.md) ourselves [paniym](../../strongs/h/h6440.md) our ['Elohiym](../../strongs/h/h430.md), to [bāqaš](../../strongs/h/h1245.md) of him a [yashar](../../strongs/h/h3477.md) [derek](../../strongs/h/h1870.md) for us, and for our [ṭap̄](../../strongs/h/h2945.md), and for all our [rᵊḵûš](../../strongs/h/h7399.md).

<a name="ezra_8_22"></a>Ezra 8:22

For I was [buwsh](../../strongs/h/h954.md) to [sha'al](../../strongs/h/h7592.md) of the [melek](../../strongs/h/h4428.md) a band of [ḥayil](../../strongs/h/h2428.md) and [pārāš](../../strongs/h/h6571.md) to [ʿāzar](../../strongs/h/h5826.md) us against the ['oyeb](../../strongs/h/h341.md) in the [derek](../../strongs/h/h1870.md): because we had ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), The [yad](../../strongs/h/h3027.md) of our ['Elohiym](../../strongs/h/h430.md) is upon all them for [towb](../../strongs/h/h2896.md) that [bāqaš](../../strongs/h/h1245.md) him; but his ['oz](../../strongs/h/h5797.md) and his ['aph](../../strongs/h/h639.md) is against all them that ['azab](../../strongs/h/h5800.md) him.

<a name="ezra_8_23"></a>Ezra 8:23

So we [ṣûm](../../strongs/h/h6684.md) and [bāqaš](../../strongs/h/h1245.md) our ['Elohiym](../../strongs/h/h430.md) for this: and he was [ʿāṯar](../../strongs/h/h6279.md) of us.

<a name="ezra_8_24"></a>Ezra 8:24

Then I [bāḏal](../../strongs/h/h914.md) twelve of the [śar](../../strongs/h/h8269.md) of the [kōhēn](../../strongs/h/h3548.md), [Šērēḇyâ](../../strongs/h/h8274.md), [Ḥăšaḇyâ](../../strongs/h/h2811.md), and ten of their ['ach](../../strongs/h/h251.md) with them,

<a name="ezra_8_25"></a>Ezra 8:25

And [šāqal](../../strongs/h/h8254.md) unto them the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [kĕliy](../../strongs/h/h3627.md), even the [tᵊrûmâ](../../strongs/h/h8641.md) of the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md), which the [melek](../../strongs/h/h4428.md), and his [ya'ats](../../strongs/h/h3289.md), and his [śar](../../strongs/h/h8269.md), and all [Yisra'el](../../strongs/h/h3478.md) there [māṣā'](../../strongs/h/h4672.md), had [ruwm](../../strongs/h/h7311.md):

<a name="ezra_8_26"></a>Ezra 8:26

I even [šāqal](../../strongs/h/h8254.md) unto their [yad](../../strongs/h/h3027.md) six hundred and fifty [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and [keceph](../../strongs/h/h3701.md) [kĕliy](../../strongs/h/h3627.md) an hundred [kikār](../../strongs/h/h3603.md), and of [zāhāḇ](../../strongs/h/h2091.md) an hundred [kikār](../../strongs/h/h3603.md);

<a name="ezra_8_27"></a>Ezra 8:27

Also twenty [kᵊp̄ôr](../../strongs/h/h3713.md) of [zāhāḇ](../../strongs/h/h2091.md), of a thousand ['ăḏarkôn](../../strongs/h/h150.md); and two [kĕliy](../../strongs/h/h3627.md) of [towb](../../strongs/h/h2896.md) [ṣāhaḇ](../../strongs/h/h6668.md) [nᵊḥšeṯ](../../strongs/h/h5178.md), [ḥemdâ](../../strongs/h/h2532.md) as [zāhāḇ](../../strongs/h/h2091.md).

<a name="ezra_8_28"></a>Ezra 8:28

And I ['āmar](../../strongs/h/h559.md) unto them, Ye are [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md); the [kĕliy](../../strongs/h/h3627.md) are [qodesh](../../strongs/h/h6944.md) also; and the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) are a [nᵊḏāḇâ](../../strongs/h/h5071.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md).

<a name="ezra_8_29"></a>Ezra 8:29

[šāqaḏ](../../strongs/h/h8245.md) ye, and [shamar](../../strongs/h/h8104.md) them, until ye [šāqal](../../strongs/h/h8254.md) them [paniym](../../strongs/h/h6440.md) the [śar](../../strongs/h/h8269.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and [śar](../../strongs/h/h8269.md) of the ['ab](../../strongs/h/h1.md) of [Yisra'el](../../strongs/h/h3478.md), at [Yĕruwshalaim](../../strongs/h/h3389.md), in the [liškâ](../../strongs/h/h3957.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezra_8_30"></a>Ezra 8:30

So [qāḇal](../../strongs/h/h6901.md) the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) the [mišqāl](../../strongs/h/h4948.md) of the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [kĕliy](../../strongs/h/h3627.md), to [bow'](../../strongs/h/h935.md) them to [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md).

<a name="ezra_8_31"></a>Ezra 8:31

Then we [nāsaʿ](../../strongs/h/h5265.md) from the [nāhār](../../strongs/h/h5104.md) of ['Ahăvā'](../../strongs/h/h163.md) on the twelfth day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), to [yālaḵ](../../strongs/h/h3212.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md): and the [yad](../../strongs/h/h3027.md) of our ['Elohiym](../../strongs/h/h430.md) was upon us, and he [natsal](../../strongs/h/h5337.md) us from the [kaph](../../strongs/h/h3709.md) of the ['oyeb](../../strongs/h/h341.md), and of such as lay in ['arab](../../strongs/h/h693.md) by the [derek](../../strongs/h/h1870.md).

<a name="ezra_8_32"></a>Ezra 8:32

And we [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and [yashab](../../strongs/h/h3427.md) there three [yowm](../../strongs/h/h3117.md).

<a name="ezra_8_33"></a>Ezra 8:33

Now on the fourth [yowm](../../strongs/h/h3117.md) was the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) and the [kĕliy](../../strongs/h/h3627.md) [šāqal](../../strongs/h/h8254.md) in the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md) by the [yad](../../strongs/h/h3027.md) of [Mᵊrēmôṯ](../../strongs/h/h4822.md) the [ben](../../strongs/h/h1121.md) of ['Ûrîyâ](../../strongs/h/h223.md) the [kōhēn](../../strongs/h/h3548.md); and with him was ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of [Pînḥās](../../strongs/h/h6372.md); and with them was [Yôzāḇāḏ](../../strongs/h/h3107.md) the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md), and [NôʿAḏyâ](../../strongs/h/h5129.md) the [ben](../../strongs/h/h1121.md) of [Binnûy](../../strongs/h/h1131.md), [Lᵊvî](../../strongs/h/h3881.md);

<a name="ezra_8_34"></a>Ezra 8:34

By [mispār](../../strongs/h/h4557.md) and by [mišqāl](../../strongs/h/h4948.md) of every one: and all the [mišqāl](../../strongs/h/h4948.md) was [kāṯaḇ](../../strongs/h/h3789.md) at that [ʿēṯ](../../strongs/h/h6256.md).

<a name="ezra_8_35"></a>Ezra 8:35

Also the [ben](../../strongs/h/h1121.md) of those that had been [gôlâ](../../strongs/h/h1473.md), which were [bow'](../../strongs/h/h935.md) of the [šᵊḇî](../../strongs/h/h7628.md), [qāraḇ](../../strongs/h/h7126.md) [ʿōlâ](../../strongs/h/h5930.md) unto the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), twelve [par](../../strongs/h/h6499.md) for all [Yisra'el](../../strongs/h/h3478.md), ninety and six ['ayil](../../strongs/h/h352.md), seventy and seven [keḇeś](../../strongs/h/h3532.md), twelve he [ṣāp̄îr](../../strongs/h/h6842.md) for a [chatta'ath](../../strongs/h/h2403.md): all this was a [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="ezra_8_36"></a>Ezra 8:36

And they [nathan](../../strongs/h/h5414.md) the [melek](../../strongs/h/h4428.md) [dāṯ](../../strongs/h/h1881.md) unto the [melek](../../strongs/h/h4428.md) ['ăḥašdarpᵊnîm](../../strongs/h/h323.md), and to the [peḥâ](../../strongs/h/h6346.md) on this [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md): and they [nasa'](../../strongs/h/h5375.md) the ['am](../../strongs/h/h5971.md), and the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 7](ezra_7.md) - [Ezra 9](ezra_9.md)