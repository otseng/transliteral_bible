# [Ezra 2](https://www.blueletterbible.org/kjv/ezra/2)

<a name="ezra_2_1"></a>Ezra 2:1

Now these are the [ben](../../strongs/h/h1121.md) of the [mᵊḏînâ](../../strongs/h/h4082.md) that [ʿālâ](../../strongs/h/h5927.md) out of the [šᵊḇî](../../strongs/h/h7628.md), of those which had been [gôlâ](../../strongs/h/h1473.md), whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had [gālâ](../../strongs/h/h1540.md) unto [Bāḇel](../../strongs/h/h894.md), and [shuwb](../../strongs/h/h7725.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md) and [Yehuwdah](../../strongs/h/h3063.md), every ['iysh](../../strongs/h/h376.md) unto his [ʿîr](../../strongs/h/h5892.md);

<a name="ezra_2_2"></a>Ezra 2:2

Which [bow'](../../strongs/h/h935.md) with [Zᵊrubāḇel](../../strongs/h/h2216.md): [Yēšûaʿ](../../strongs/h/h3442.md), [Nᵊḥemyâ](../../strongs/h/h5166.md), [Śᵊrāyâ](../../strongs/h/h8304.md), [RᵊʿĒlāyâ](../../strongs/h/h7480.md), [Mārdᵊḵay](../../strongs/h/h4782.md), [Bilšān](../../strongs/h/h1114.md), [Mispār](../../strongs/h/h4558.md), [Biḡvay](../../strongs/h/h902.md), [Rᵊḥûm](../../strongs/h/h7348.md), [BaʿĂnâ](../../strongs/h/h1196.md). The [mispār](../../strongs/h/h4557.md) of the ['enowsh](../../strongs/h/h582.md) of the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="ezra_2_3"></a>Ezra 2:3

The [ben](../../strongs/h/h1121.md) of [ParʿŠ](../../strongs/h/h6551.md), two thousand an hundred seventy and two.

<a name="ezra_2_4"></a>Ezra 2:4

The [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), three hundred seventy and two.

<a name="ezra_2_5"></a>Ezra 2:5

The [ben](../../strongs/h/h1121.md) of ['Āraḥ](../../strongs/h/h733.md), seven hundred seventy and five.

<a name="ezra_2_6"></a>Ezra 2:6

The [ben](../../strongs/h/h1121.md) of [Paḥaṯ Mô'Āḇ](../../strongs/h/h6355.md), of the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md) and [Yô'āḇ](../../strongs/h/h3097.md), two thousand eight hundred and twelve .

<a name="ezra_2_7"></a>Ezra 2:7

The [ben](../../strongs/h/h1121.md) of [ʿÊlām](../../strongs/h/h5867.md), a thousand two hundred fifty and four.

<a name="ezra_2_8"></a>Ezra 2:8

The [ben](../../strongs/h/h1121.md) of [Zatû'](../../strongs/h/h2240.md), nine hundred forty and five.

<a name="ezra_2_9"></a>Ezra 2:9

The [ben](../../strongs/h/h1121.md) of [Zakay](../../strongs/h/h2140.md), seven hundred and threescore.

<a name="ezra_2_10"></a>Ezra 2:10

The [ben](../../strongs/h/h1121.md) of [Bānî](../../strongs/h/h1137.md), six hundred forty and two.

<a name="ezra_2_11"></a>Ezra 2:11

The [ben](../../strongs/h/h1121.md) of [Bēḇay](../../strongs/h/h893.md), six hundred twenty and three.

<a name="ezra_2_12"></a>Ezra 2:12

The [ben](../../strongs/h/h1121.md) of [ʿAzgāḏ](../../strongs/h/h5803.md), a thousand two hundred twenty and two.

<a name="ezra_2_13"></a>Ezra 2:13

The [ben](../../strongs/h/h1121.md) of ['Ăḏōnîqām](../../strongs/h/h140.md), six hundred sixty and six.

<a name="ezra_2_14"></a>Ezra 2:14

The [ben](../../strongs/h/h1121.md) of [Biḡvay](../../strongs/h/h902.md), two thousand fifty and six.

<a name="ezra_2_15"></a>Ezra 2:15

The [ben](../../strongs/h/h1121.md) of [ʿĀḏîn](../../strongs/h/h5720.md), four hundred fifty and four.

<a name="ezra_2_16"></a>Ezra 2:16

The [ben](../../strongs/h/h1121.md) of ['Āṭēr](../../strongs/h/h333.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md), ninety and eight.

<a name="ezra_2_17"></a>Ezra 2:17

The [ben](../../strongs/h/h1121.md) of [Bēṣay](../../strongs/h/h1209.md), three hundred twenty and three.

<a name="ezra_2_18"></a>Ezra 2:18

The [ben](../../strongs/h/h1121.md) of [Yôrâ](../../strongs/h/h3139.md), an hundred and twelve .

<a name="ezra_2_19"></a>Ezra 2:19

The [ben](../../strongs/h/h1121.md) of [Ḥāšum](../../strongs/h/h2828.md), two hundred twenty and three.

<a name="ezra_2_20"></a>Ezra 2:20

The [ben](../../strongs/h/h1121.md) of [Gibār](../../strongs/h/h1402.md), ninety and five.

<a name="ezra_2_21"></a>Ezra 2:21

The [ben](../../strongs/h/h1121.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), an hundred twenty and three.

<a name="ezra_2_22"></a>Ezra 2:22

The ['enowsh](../../strongs/h/h582.md) of [Nᵊṭōp̄Â](../../strongs/h/h5199.md), fifty and six.

<a name="ezra_2_23"></a>Ezra 2:23

The ['enowsh](../../strongs/h/h582.md) of [ʿĂnāṯôṯ](../../strongs/h/h6068.md), an hundred twenty and eight.

<a name="ezra_2_24"></a>Ezra 2:24

The [ben](../../strongs/h/h1121.md) of [ʿAzmāveṯ](../../strongs/h/h5820.md), forty and two.

<a name="ezra_2_25"></a>Ezra 2:25

The [ben](../../strongs/h/h1121.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), [Kᵊp̄Îrâ](../../strongs/h/h3716.md), and [Bᵊ'Ērôṯ](../../strongs/h/h881.md), seven hundred and forty and three.

<a name="ezra_2_26"></a>Ezra 2:26

The [ben](../../strongs/h/h1121.md) of [Rāmâ](../../strongs/h/h7414.md) and [Geḇaʿ](../../strongs/h/h1387.md), six hundred twenty and one.

<a name="ezra_2_27"></a>Ezra 2:27

The ['enowsh](../../strongs/h/h582.md) of [Miḵmās](../../strongs/h/h4363.md), an hundred twenty and two.

<a name="ezra_2_28"></a>Ezra 2:28

The ['enowsh](../../strongs/h/h582.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md) and [ʿAy](../../strongs/h/h5857.md), two hundred twenty and three.

<a name="ezra_2_29"></a>Ezra 2:29

The [ben](../../strongs/h/h1121.md) of [Nᵊḇô](../../strongs/h/h5015.md), fifty and two.

<a name="ezra_2_30"></a>Ezra 2:30

The [ben](../../strongs/h/h1121.md) of [Maḡbîš](../../strongs/h/h4019.md), an hundred fifty and six.

<a name="ezra_2_31"></a>Ezra 2:31

The [ben](../../strongs/h/h1121.md) of the ['aḥēr](../../strongs/h/h312.md) [ʿÊlām](../../strongs/h/h5867.md), a thousand two hundred fifty and four.

<a name="ezra_2_32"></a>Ezra 2:32

The [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md), three hundred and twenty.

<a name="ezra_2_33"></a>Ezra 2:33

The [ben](../../strongs/h/h1121.md) of [Lōḏ](../../strongs/h/h3850.md), [Ḥāḏîḏ](../../strongs/h/h2307.md), and ['Ônô](../../strongs/h/h207.md), seven hundred twenty and five.

<a name="ezra_2_34"></a>Ezra 2:34

The [ben](../../strongs/h/h1121.md) of [Yᵊrēḥô](../../strongs/h/h3405.md), three hundred forty and five.

<a name="ezra_2_35"></a>Ezra 2:35

The [ben](../../strongs/h/h1121.md) of [Sᵊnā'Â](../../strongs/h/h5570.md), three thousand and six hundred and thirty.

<a name="ezra_2_36"></a>Ezra 2:36

The [kōhēn](../../strongs/h/h3548.md): the [ben](../../strongs/h/h1121.md) of [YᵊḏaʿYâ](../../strongs/h/h3048.md), of the [bayith](../../strongs/h/h1004.md) of [Yēšûaʿ](../../strongs/h/h3442.md), nine hundred seventy and three.

<a name="ezra_2_37"></a>Ezra 2:37

The [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md), a thousand fifty and two.

<a name="ezra_2_38"></a>Ezra 2:38

The [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md), a thousand two hundred forty and seven.

<a name="ezra_2_39"></a>Ezra 2:39

The [ben](../../strongs/h/h1121.md) of [Ḥārim](../../strongs/h/h2766.md), a thousand and seventeen .

<a name="ezra_2_40"></a>Ezra 2:40

The [Lᵊvî](../../strongs/h/h3881.md): the [ben](../../strongs/h/h1121.md) of [Yēšûaʿ](../../strongs/h/h3442.md) and [Qaḏmî'Ēl](../../strongs/h/h6934.md), of the [ben](../../strongs/h/h1121.md) of [Hôḏavyâ](../../strongs/h/h1938.md), seventy and four.

<a name="ezra_2_41"></a>Ezra 2:41

The [shiyr](../../strongs/h/h7891.md): the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), an hundred twenty and eight.

<a name="ezra_2_42"></a>Ezra 2:42

The [ben](../../strongs/h/h1121.md) of the [šôʿēr](../../strongs/h/h7778.md): the [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md), the [ben](../../strongs/h/h1121.md) of ['Āṭēr](../../strongs/h/h333.md), the [ben](../../strongs/h/h1121.md) of [Ṭalmôn](../../strongs/h/h2929.md), the [ben](../../strongs/h/h1121.md) of [ʿAqqûḇ](../../strongs/h/h6126.md), the [ben](../../strongs/h/h1121.md) of [Ḥăṭîṭā'](../../strongs/h/h2410.md), the [ben](../../strongs/h/h1121.md) of [Šōḇay](../../strongs/h/h7630.md), in all an hundred thirty and nine.

<a name="ezra_2_43"></a>Ezra 2:43

The [Nāṯîn](../../strongs/h/h5411.md): the [ben](../../strongs/h/h1121.md) of [Ṣyḥ'](../../strongs/h/h6727.md), the [ben](../../strongs/h/h1121.md) of [Ḥăśûp̄Ā'](../../strongs/h/h2817.md), the [ben](../../strongs/h/h1121.md) of [ṬabāʿÔṯ](../../strongs/h/h2884.md),

<a name="ezra_2_44"></a>Ezra 2:44

The [ben](../../strongs/h/h1121.md) of [Qêrōs](../../strongs/h/h7026.md), the [ben](../../strongs/h/h1121.md) of [SîʿĀ'](../../strongs/h/h5517.md), the [ben](../../strongs/h/h1121.md) of [Pāḏôn](../../strongs/h/h6303.md),

<a name="ezra_2_45"></a>Ezra 2:45

The [ben](../../strongs/h/h1121.md) of [Lᵊḇānâ](../../strongs/h/h3838.md), the [ben](../../strongs/h/h1121.md) of [Ḥăḡāḇā'](../../strongs/h/h2286.md), the [ben](../../strongs/h/h1121.md) of [ʿAqqûḇ](../../strongs/h/h6126.md),

<a name="ezra_2_46"></a>Ezra 2:46

The [ben](../../strongs/h/h1121.md) of [Ḥāḡāḇ](../../strongs/h/h2285.md), the [ben](../../strongs/h/h1121.md) of [Šamlay](../../strongs/h/h8073.md), the [ben](../../strongs/h/h1121.md) of [Ḥānān](../../strongs/h/h2605.md),

<a name="ezra_2_47"></a>Ezra 2:47

The [ben](../../strongs/h/h1121.md) of [Gidēl](../../strongs/h/h1435.md), the [ben](../../strongs/h/h1121.md) of [Gaḥar](../../strongs/h/h1515.md), the [ben](../../strongs/h/h1121.md) of [Rᵊ'Āyâ](../../strongs/h/h7211.md),

<a name="ezra_2_48"></a>Ezra 2:48

The [ben](../../strongs/h/h1121.md) of [Rᵊṣîn](../../strongs/h/h7526.md), the [ben](../../strongs/h/h1121.md) of [Nᵊqôḏā'](../../strongs/h/h5353.md), the [ben](../../strongs/h/h1121.md) of [Gazzām](../../strongs/h/h1502.md),

<a name="ezra_2_49"></a>Ezra 2:49

The [ben](../../strongs/h/h1121.md) of [ʿUzzā'](../../strongs/h/h5798.md), the [ben](../../strongs/h/h1121.md) of [Pāsēaḥ](../../strongs/h/h6454.md), the [ben](../../strongs/h/h1121.md) of [Bēsay](../../strongs/h/h1153.md),

<a name="ezra_2_50"></a>Ezra 2:50

The [ben](../../strongs/h/h1121.md) of ['Asnâ](../../strongs/h/h619.md), the [ben](../../strongs/h/h1121.md) of [MᵊʿYny](../../strongs/h/h4586.md), the [ben](../../strongs/h/h1121.md) of [nᵊp̄ûšsîm](../../strongs/h/h5300.md) [nᵊp̄îsîm](../../strongs/h/h5304.md) ,

<a name="ezra_2_51"></a>Ezra 2:51

The [ben](../../strongs/h/h1121.md) of [Baqbûq](../../strongs/h/h1227.md), the [ben](../../strongs/h/h1121.md) of [Ḥăqûp̄Ā'](../../strongs/h/h2709.md), the [ben](../../strongs/h/h1121.md) of [Ḥarḥûr](../../strongs/h/h2744.md),

<a name="ezra_2_52"></a>Ezra 2:52

The [ben](../../strongs/h/h1121.md) of [Baṣlûṯ](../../strongs/h/h1213.md), the [ben](../../strongs/h/h1121.md) of [Mᵊḥîḏā'](../../strongs/h/h4240.md), the [ben](../../strongs/h/h1121.md) of [Ḥaršā'](../../strongs/h/h2797.md),

<a name="ezra_2_53"></a>Ezra 2:53

The [ben](../../strongs/h/h1121.md) of [Barqôs](../../strongs/h/h1302.md), the [ben](../../strongs/h/h1121.md) of [Sîsrā'](../../strongs/h/h5516.md), the [ben](../../strongs/h/h1121.md) of [Temaḥ](../../strongs/h/h8547.md),

<a name="ezra_2_54"></a>Ezra 2:54

The [ben](../../strongs/h/h1121.md) of [Nᵊṣîaḥ](../../strongs/h/h5335.md), the [ben](../../strongs/h/h1121.md) of [Ḥăṭîp̄Ā'](../../strongs/h/h2412.md).

<a name="ezra_2_55"></a>Ezra 2:55

The [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md): the [ben](../../strongs/h/h1121.md) of [Sôṭay](../../strongs/h/h5479.md), the [ben](../../strongs/h/h1121.md) of [Sōp̄Ereṯ](../../strongs/h/h5618.md), the [ben](../../strongs/h/h1121.md) of [Pᵊrûḏā'](../../strongs/h/h6514.md),

<a name="ezra_2_56"></a>Ezra 2:56

The [ben](../../strongs/h/h1121.md) of [YaʿĂlā'](../../strongs/h/h3279.md), the [ben](../../strongs/h/h1121.md) of [Darqôn](../../strongs/h/h1874.md), the [ben](../../strongs/h/h1121.md) of [Gidēl](../../strongs/h/h1435.md),

<a name="ezra_2_57"></a>Ezra 2:57

The [ben](../../strongs/h/h1121.md) of [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md), the [ben](../../strongs/h/h1121.md) of [Ḥaṭṭîl](../../strongs/h/h2411.md), the [ben](../../strongs/h/h1121.md) of [Pōḵereṯ Haṣṣᵊḇāyîm](../../strongs/h/h6380.md), the [ben](../../strongs/h/h1121.md) of ['Āmî](../../strongs/h/h532.md).

<a name="ezra_2_58"></a>Ezra 2:58

All the [Nāṯîn](../../strongs/h/h5411.md), and the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md), were three hundred ninety and two.

<a name="ezra_2_59"></a>Ezra 2:59

And these were they which [ʿālâ](../../strongs/h/h5927.md) from [Tēl Melaḥ](../../strongs/h/h8528.md), [Tēl Ḥaršā'](../../strongs/h/h8521.md), [Kᵊrûḇ](../../strongs/h/h3743.md), ['Adān](../../strongs/h/h135.md), and ['Immēr](../../strongs/h/h564.md): but they [yakol](../../strongs/h/h3201.md) not [nāḡaḏ](../../strongs/h/h5046.md) their ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and their [zera'](../../strongs/h/h2233.md), whether they were of [Yisra'el](../../strongs/h/h3478.md):

<a name="ezra_2_60"></a>Ezra 2:60

The [ben](../../strongs/h/h1121.md) of [Dᵊlāyâ](../../strongs/h/h1806.md), the [ben](../../strongs/h/h1121.md) of [Ṭôḇîyâ](../../strongs/h/h2900.md), the [ben](../../strongs/h/h1121.md) of [Nᵊqôḏā'](../../strongs/h/h5353.md), six hundred fifty and two.

<a name="ezra_2_61"></a>Ezra 2:61

And of the [ben](../../strongs/h/h1121.md) of the [kōhēn](../../strongs/h/h3548.md): the [ben](../../strongs/h/h1121.md) of [Ḥŏḇāyâ](../../strongs/h/h2252.md), the [ben](../../strongs/h/h1121.md) of [Qôṣ](../../strongs/h/h6976.md), the [ben](../../strongs/h/h1121.md) of [Barzillay](../../strongs/h/h1271.md); which [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Barzillay](../../strongs/h/h1271.md) the [Gilʿāḏî](../../strongs/h/h1569.md), and was [qara'](../../strongs/h/h7121.md) after their [shem](../../strongs/h/h8034.md):

<a name="ezra_2_62"></a>Ezra 2:62

These [bāqaš](../../strongs/h/h1245.md) their [kᵊṯāḇ](../../strongs/h/h3791.md) among those that were [yāḥaś](../../strongs/h/h3187.md), but they were not [māṣā'](../../strongs/h/h4672.md): therefore were they, as [gā'al](../../strongs/h/h1351.md), put from the [kᵊhunnâ](../../strongs/h/h3550.md).

<a name="ezra_2_63"></a>Ezra 2:63

And the [tiršāṯā'](../../strongs/h/h8660.md) ['āmar](../../strongs/h/h559.md) unto them, that they should not ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), till there ['amad](../../strongs/h/h5975.md) a [kōhēn](../../strongs/h/h3548.md) with ['Ûrîm](../../strongs/h/h224.md) and with [Tummîm](../../strongs/h/h8550.md).

<a name="ezra_2_64"></a>Ezra 2:64

The whole [qāhēl](../../strongs/h/h6951.md) together was forty [ribô'](../../strongs/h/h7239.md) and two thousand three hundred and threescore,

<a name="ezra_2_65"></a>Ezra 2:65

Beside their ['ebed](../../strongs/h/h5650.md) and their ['amah](../../strongs/h/h519.md), of whom there were seven thousand three hundred thirty and seven: and there were among them two hundred [shiyr](../../strongs/h/h7891.md) and [shiyr](../../strongs/h/h7891.md).

<a name="ezra_2_66"></a>Ezra 2:66

Their [sûs](../../strongs/h/h5483.md) were seven hundred thirty and six; their [pereḏ](../../strongs/h/h6505.md), two hundred forty and five;

<a name="ezra_2_67"></a>Ezra 2:67

Their [gāmāl](../../strongs/h/h1581.md), four hundred thirty and five; their [chamowr](../../strongs/h/h2543.md), six thousand seven hundred and twenty.

<a name="ezra_2_68"></a>Ezra 2:68

And some of the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md), when they [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) which is at [Yĕruwshalaim](../../strongs/h/h3389.md), [nāḏaḇ](../../strongs/h/h5068.md) for the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) to set it ['amad](../../strongs/h/h5975.md) in his [māḵôn](../../strongs/h/h4349.md):

<a name="ezra_2_69"></a>Ezra 2:69

They [nathan](../../strongs/h/h5414.md) after their [koach](../../strongs/h/h3581.md) unto the ['ôṣār](../../strongs/h/h214.md) of the [mĕla'kah](../../strongs/h/h4399.md) threescore [ribô'](../../strongs/h/h7239.md) and one thousand [darkᵊmôn](../../strongs/h/h1871.md) of [zāhāḇ](../../strongs/h/h2091.md), and five thousand [mānê](../../strongs/h/h4488.md) of [keceph](../../strongs/h/h3701.md), and one hundred [kōhēn](../../strongs/h/h3548.md) [kĕthoneth](../../strongs/h/h3801.md).

<a name="ezra_2_70"></a>Ezra 2:70

So the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and some of the ['am](../../strongs/h/h5971.md), and the [shiyr](../../strongs/h/h7891.md), and the [šôʿēr](../../strongs/h/h7778.md), and the [Nāṯîn](../../strongs/h/h5411.md), [yashab](../../strongs/h/h3427.md) in their [ʿîr](../../strongs/h/h5892.md), and all [Yisra'el](../../strongs/h/h3478.md) in their [ʿîr](../../strongs/h/h5892.md).

---

[Transliteral Bible](../bible.md)

[Ezra](ezra.md)

[Ezra 1](ezra_1.md) - [Ezra 3](ezra_3.md)