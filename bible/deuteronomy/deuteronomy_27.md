# [Deuteronomy 27](https://www.blueletterbible.org/kjv/deu/27)

<a name="deuteronomy_27_1"></a>Deuteronomy 27:1

And [Mōshe](../../strongs/h/h4872.md) with the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [tsavah](../../strongs/h/h6680.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) all the [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_27_2"></a>Deuteronomy 27:2

And it shall be on the [yowm](../../strongs/h/h3117.md) when ye shall ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, that thou shalt [quwm](../../strongs/h/h6965.md) thee [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md), and [śîḏ](../../strongs/h/h7874.md) them with [śîḏ](../../strongs/h/h7875.md):

<a name="deuteronomy_27_3"></a>Deuteronomy 27:3

And thou shalt [kāṯaḇ](../../strongs/h/h3789.md) upon them all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md), when thou art ['abar](../../strongs/h/h5674.md), that thou mayest [bow'](../../strongs/h/h935.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md); as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md) hath [dabar](../../strongs/h/h1696.md) thee.

<a name="deuteronomy_27_4"></a>Deuteronomy 27:4

Therefore it shall be when ye be ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), that ye shall [quwm](../../strongs/h/h6965.md) these ['eben](../../strongs/h/h68.md), which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md), in [har](../../strongs/h/h2022.md) [ʿÊḇāl](../../strongs/h/h5858.md), and thou shalt [śîḏ](../../strongs/h/h7875.md) them with [śîḏ](../../strongs/h/h7874.md).

<a name="deuteronomy_27_5"></a>Deuteronomy 27:5

And there shalt thou [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), a [mizbeach](../../strongs/h/h4196.md) of ['eben](../../strongs/h/h68.md): thou shalt not [nûp̄](../../strongs/h/h5130.md) any [barzel](../../strongs/h/h1270.md) tool upon them.

<a name="deuteronomy_27_6"></a>Deuteronomy 27:6

Thou shalt [bānâ](../../strongs/h/h1129.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) of [šālēm](../../strongs/h/h8003.md) ['eben](../../strongs/h/h68.md): and thou shalt [ʿālâ](../../strongs/h/h5927.md) an [ʿōlâ](../../strongs/h/h5930.md) thereon unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md):

<a name="deuteronomy_27_7"></a>Deuteronomy 27:7

And thou shalt [zabach](../../strongs/h/h2076.md) [šelem](../../strongs/h/h8002.md), and shalt ['akal](../../strongs/h/h398.md) there, and [samach](../../strongs/h/h8055.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_27_8"></a>Deuteronomy 27:8

And thou shalt [kāṯaḇ](../../strongs/h/h3789.md) upon the ['eben](../../strongs/h/h68.md) all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md) [yatab](../../strongs/h/h3190.md) [bā'ar](../../strongs/h/h874.md).

<a name="deuteronomy_27_9"></a>Deuteronomy 27:9

And [Mōshe](../../strongs/h/h4872.md) and the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) [dabar](../../strongs/h/h1696.md) unto all [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [sāḵaṯ](../../strongs/h/h5535.md), and [shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md); this [yowm](../../strongs/h/h3117.md) thou art [hayah](../../strongs/h/h1961.md) the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_27_10"></a>Deuteronomy 27:10

Thou shalt therefore [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and ['asah](../../strongs/h/h6213.md) his [mitsvah](../../strongs/h/h4687.md) and his [choq](../../strongs/h/h2706.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_27_11"></a>Deuteronomy 27:11

And [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) the ['am](../../strongs/h/h5971.md) the same [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_27_12"></a>Deuteronomy 27:12

These shall ['amad](../../strongs/h/h5975.md) upon [har](../../strongs/h/h2022.md) [Gᵊrizzîm](../../strongs/h/h1630.md) to [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md), when ye are ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md); [Šimʿôn](../../strongs/h/h8095.md), and [Lēvî](../../strongs/h/h3878.md), and [Yehuwdah](../../strongs/h/h3063.md), and [Yiśśāśḵār](../../strongs/h/h3485.md), and [Yôsēp̄](../../strongs/h/h3130.md), and [Binyāmîn](../../strongs/h/h1144.md):

<a name="deuteronomy_27_13"></a>Deuteronomy 27:13

And these shall ['amad](../../strongs/h/h5975.md) upon [har](../../strongs/h/h2022.md) [ʿÊḇāl](../../strongs/h/h5858.md) to [qᵊlālâ](../../strongs/h/h7045.md); [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Gāḏ](../../strongs/h/h1410.md), and ['Āšēr](../../strongs/h/h836.md), and [Zᵊḇûlûn](../../strongs/h/h2074.md), [Dān](../../strongs/h/h1835.md), and [Nap̄tālî](../../strongs/h/h5321.md).

<a name="deuteronomy_27_14"></a>Deuteronomy 27:14

And the [Lᵊvî](../../strongs/h/h3881.md) shall ['anah](../../strongs/h/h6030.md), and ['āmar](../../strongs/h/h559.md) unto all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) with a [ruwm](../../strongs/h/h7311.md) [qowl](../../strongs/h/h6963.md),

<a name="deuteronomy_27_15"></a>Deuteronomy 27:15

['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) that ['asah](../../strongs/h/h6213.md) any [pecel](../../strongs/h/h6459.md) or [massēḵâ](../../strongs/h/h4541.md), a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md), the [ma'aseh](../../strongs/h/h4639.md) of the [yad](../../strongs/h/h3027.md) of the [ḥārāš](../../strongs/h/h2796.md), and [śûm](../../strongs/h/h7760.md) it in a [cether](../../strongs/h/h5643.md). And all the ['am](../../strongs/h/h5971.md) shall ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_16"></a>Deuteronomy 27:16

['arar](../../strongs/h/h779.md) be he that [qālâ](../../strongs/h/h7034.md) by his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_17"></a>Deuteronomy 27:17

['arar](../../strongs/h/h779.md) be he that [nāsaḡ](../../strongs/h/h5253.md) his [rea'](../../strongs/h/h7453.md) [gᵊḇûl](../../strongs/h/h1366.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_18"></a>Deuteronomy 27:18

['arar](../../strongs/h/h779.md) be he that maketh the [ʿiûēr](../../strongs/h/h5787.md) to [šāḡâ](../../strongs/h/h7686.md) out of the [derek](../../strongs/h/h1870.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_19"></a>Deuteronomy 27:19

['arar](../../strongs/h/h779.md) be he that [natah](../../strongs/h/h5186.md) the [mishpat](../../strongs/h/h4941.md) of the [ger](../../strongs/h/h1616.md), [yathowm](../../strongs/h/h3490.md), and ['almānâ](../../strongs/h/h490.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_20"></a>Deuteronomy 27:20

['arar](../../strongs/h/h779.md) be he that [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md); because he [gālâ](../../strongs/h/h1540.md) his ['ab](../../strongs/h/h1.md) [kanaph](../../strongs/h/h3671.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_21"></a>Deuteronomy 27:21

['arar](../../strongs/h/h779.md) be he that [shakab](../../strongs/h/h7901.md) with any manner of [bĕhemah](../../strongs/h/h929.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_22"></a>Deuteronomy 27:22

['arar](../../strongs/h/h779.md) be he that [shakab](../../strongs/h/h7901.md) with his ['āḥôṯ](../../strongs/h/h269.md), the [bath](../../strongs/h/h1323.md) of his ['ab](../../strongs/h/h1.md), or the [bath](../../strongs/h/h1323.md) of his ['em](../../strongs/h/h517.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_23"></a>Deuteronomy 27:23

['arar](../../strongs/h/h779.md) be he that [shakab](../../strongs/h/h7901.md) with his [ḥāṯan](../../strongs/h/h2859.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_24"></a>Deuteronomy 27:24

['arar](../../strongs/h/h779.md) be he that [nakah](../../strongs/h/h5221.md) his [rea'](../../strongs/h/h7453.md) [cether](../../strongs/h/h5643.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_25"></a>Deuteronomy 27:25

['arar](../../strongs/h/h779.md) be he that [laqach](../../strongs/h/h3947.md) [shachad](../../strongs/h/h7810.md) to [nakah](../../strongs/h/h5221.md) a [naqiy](../../strongs/h/h5355.md) [nephesh](../../strongs/h/h5315.md) [dam](../../strongs/h/h1818.md). And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

<a name="deuteronomy_27_26"></a>Deuteronomy 27:26

['arar](../../strongs/h/h779.md) be he that [quwm](../../strongs/h/h6965.md) not all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md) to ['asah](../../strongs/h/h6213.md) them. And all the ['am](../../strongs/h/h5971.md) shall ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 26](deuteronomy_26.md) - [Deuteronomy 28](deuteronomy_28.md)