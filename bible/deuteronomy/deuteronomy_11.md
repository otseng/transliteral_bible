# [Deuteronomy 11](https://www.blueletterbible.org/esv/deu/11)

<a name="deuteronomy_11_1"></a>Deuteronomy 11:1

Therefore thou shalt ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [shamar](../../strongs/h/h8104.md) his [mišmereṯ](../../strongs/h/h4931.md), and his [chuqqah](../../strongs/h/h2708.md), and his [mishpat](../../strongs/h/h4941.md), and his [mitsvah](../../strongs/h/h4687.md), [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_11_2"></a>Deuteronomy 11:2

And [yada'](../../strongs/h/h3045.md) ye this [yowm](../../strongs/h/h3117.md): for I speak not with your [ben](../../strongs/h/h1121.md) which have not [yada'](../../strongs/h/h3045.md), and which have not [ra'ah](../../strongs/h/h7200.md) the [mûsār](../../strongs/h/h4148.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), his [gōḏel](../../strongs/h/h1433.md), his [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and his [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md),

<a name="deuteronomy_11_3"></a>Deuteronomy 11:3

And his ['ôṯ](../../strongs/h/h226.md), and his [ma'aseh](../../strongs/h/h4639.md), which he ['asah](../../strongs/h/h6213.md) in the [tavek](../../strongs/h/h8432.md) of [Mitsrayim](../../strongs/h/h4714.md) unto [Parʿô](../../strongs/h/h6547.md) the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and unto all his ['erets](../../strongs/h/h776.md);

<a name="deuteronomy_11_4"></a>Deuteronomy 11:4

And what he ['asah](../../strongs/h/h6213.md) unto the [ḥayil](../../strongs/h/h2428.md) of [Mitsrayim](../../strongs/h/h4714.md), unto their [sûs](../../strongs/h/h5483.md), and to their [reḵeḇ](../../strongs/h/h7393.md); how he made the [mayim](../../strongs/h/h4325.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md) to [ṣûp̄](../../strongs/h/h6687.md) [paniym](../../strongs/h/h6440.md) as they [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) you, and how [Yĕhovah](../../strongs/h/h3068.md) hath ['abad](../../strongs/h/h6.md) them unto this [yowm](../../strongs/h/h3117.md);

<a name="deuteronomy_11_5"></a>Deuteronomy 11:5

And what he ['asah](../../strongs/h/h6213.md) unto you in the [midbar](../../strongs/h/h4057.md), until ye [bow'](../../strongs/h/h935.md) into this [maqowm](../../strongs/h/h4725.md);

<a name="deuteronomy_11_6"></a>Deuteronomy 11:6

And what he ['asah](../../strongs/h/h6213.md) unto [Dāṯān](../../strongs/h/h1885.md) and ['Ăḇîrām](../../strongs/h/h48.md), the [ben](../../strongs/h/h1121.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md), the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md): how the ['erets](../../strongs/h/h776.md) [pāṣâ](../../strongs/h/h6475.md) her [peh](../../strongs/h/h6310.md), and [bālaʿ](../../strongs/h/h1104.md) them, and their [bayith](../../strongs/h/h1004.md), and their ['ohel](../../strongs/h/h168.md), and all the [yᵊqûm](../../strongs/h/h3351.md) that was in their [regel](../../strongs/h/h7272.md), in the [qereḇ](../../strongs/h/h7130.md) of all [Yisra'el](../../strongs/h/h3478.md):

<a name="deuteronomy_11_7"></a>Deuteronomy 11:7

But your ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) all the [gadowl](../../strongs/h/h1419.md) [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md) which he ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_11_8"></a>Deuteronomy 11:8

Therefore shall ye [shamar](../../strongs/h/h8104.md) all the [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md), that ye may be [ḥāzaq](../../strongs/h/h2388.md), and [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), whither ye ['abar](../../strongs/h/h5674.md) to [yarash](../../strongs/h/h3423.md) it;

<a name="deuteronomy_11_9"></a>Deuteronomy 11:9

And that ye may ['arak](../../strongs/h/h748.md) your [yowm](../../strongs/h/h3117.md) in the ['ăḏāmâ](../../strongs/h/h127.md), which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) unto them and to their [zera'](../../strongs/h/h2233.md), an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="deuteronomy_11_10"></a>Deuteronomy 11:10

For the ['erets](../../strongs/h/h776.md), whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it, is not as the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from whence ye [yāṣā'](../../strongs/h/h3318.md), where thou [zāraʿ](../../strongs/h/h2232.md) thy [zera'](../../strongs/h/h2233.md), and [šāqâ](../../strongs/h/h8248.md) it with thy [regel](../../strongs/h/h7272.md), as a [gan](../../strongs/h/h1588.md) of [yārāq](../../strongs/h/h3419.md):

<a name="deuteronomy_11_11"></a>Deuteronomy 11:11

But the ['erets](../../strongs/h/h776.md), whither ye ['abar](../../strongs/h/h5674.md) to [yarash](../../strongs/h/h3423.md) it, is an ['erets](../../strongs/h/h776.md) of [har](../../strongs/h/h2022.md) and [biqʿâ](../../strongs/h/h1237.md), and [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) of the [māṭār](../../strongs/h/h4306.md) of [shamayim](../../strongs/h/h8064.md):

<a name="deuteronomy_11_12"></a>Deuteronomy 11:12

an ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [darash](../../strongs/h/h1875.md) for: the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) are [tāmîḏ](../../strongs/h/h8548.md) upon it, from the [re'shiyth](../../strongs/h/h7225.md) of the [šānâ](../../strongs/h/h8141.md) even unto the ['aḥărîṯ](../../strongs/h/h319.md) of the [šānâ](../../strongs/h/h8141.md).

<a name="deuteronomy_11_13"></a>Deuteronomy 11:13

And it shall come to pass, if ye shall [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) unto my [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md), to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and to ['abad](../../strongs/h/h5647.md) him with all your [lebab](../../strongs/h/h3824.md) and with all your [nephesh](../../strongs/h/h5315.md),

<a name="deuteronomy_11_14"></a>Deuteronomy 11:14

That I will [nathan](../../strongs/h/h5414.md) you the [māṭār](../../strongs/h/h4306.md) of your ['erets](../../strongs/h/h776.md) in his [ʿēṯ](../../strongs/h/h6256.md), the [yôrâ](../../strongs/h/h3138.md) and the [malqôš](../../strongs/h/h4456.md), that thou mayest ['āsap̄](../../strongs/h/h622.md) in thy [dagan](../../strongs/h/h1715.md), and thy [tiyrowsh](../../strongs/h/h8492.md), and thine [yiṣhār](../../strongs/h/h3323.md).

<a name="deuteronomy_11_15"></a>Deuteronomy 11:15

And I will [nathan](../../strongs/h/h5414.md) ['eseb](../../strongs/h/h6212.md) in thy [sadeh](../../strongs/h/h7704.md) for thy [bĕhemah](../../strongs/h/h929.md), that thou mayest ['akal](../../strongs/h/h398.md) and be [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="deuteronomy_11_16"></a>Deuteronomy 11:16

[shamar](../../strongs/h/h8104.md) to yourselves, that your [lebab](../../strongs/h/h3824.md) be not [pāṯâ](../../strongs/h/h6601.md), and ye [cuwr](../../strongs/h/h5493.md), and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them;

<a name="deuteronomy_11_17"></a>Deuteronomy 11:17

And then [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md) be [ḥārâ](../../strongs/h/h2734.md) against you, and he [ʿāṣar](../../strongs/h/h6113.md) the [shamayim](../../strongs/h/h8064.md), that there be no [māṭār](../../strongs/h/h4306.md), and that the ['ăḏāmâ](../../strongs/h/h127.md) [nathan](../../strongs/h/h5414.md) not her [yᵊḇûl](../../strongs/h/h2981.md); and lest ye ['abad](../../strongs/h/h6.md) [mᵊhērâ](../../strongs/h/h4120.md) from off the [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) you.

<a name="deuteronomy_11_18"></a>Deuteronomy 11:18

Therefore shall ye [śûm](../../strongs/h/h7760.md) these my [dabar](../../strongs/h/h1697.md) in your [lebab](../../strongs/h/h3824.md) and in your [nephesh](../../strongs/h/h5315.md), and [qāšar](../../strongs/h/h7194.md) them for an ['ôṯ](../../strongs/h/h226.md) upon your [yad](../../strongs/h/h3027.md), that they may be as [ṭôṭāp̄ôṯ](../../strongs/h/h2903.md) between your ['ayin](../../strongs/h/h5869.md).

<a name="deuteronomy_11_19"></a>Deuteronomy 11:19

And ye shall [lamad](../../strongs/h/h3925.md) them your [ben](../../strongs/h/h1121.md), [dabar](../../strongs/h/h1696.md) of them when thou [yashab](../../strongs/h/h3427.md) in thine [bayith](../../strongs/h/h1004.md), and when thou [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md), when thou [shakab](../../strongs/h/h7901.md), and when thou [quwm](../../strongs/h/h6965.md).

<a name="deuteronomy_11_20"></a>Deuteronomy 11:20

And thou shalt [kāṯaḇ](../../strongs/h/h3789.md) them upon the [mᵊzûzâ](../../strongs/h/h4201.md) of thine [bayith](../../strongs/h/h1004.md), and upon thy [sha'ar](../../strongs/h/h8179.md):

<a name="deuteronomy_11_21"></a>Deuteronomy 11:21

That your [yowm](../../strongs/h/h3117.md) may be [rabah](../../strongs/h/h7235.md), and the [yowm](../../strongs/h/h3117.md) of your [ben](../../strongs/h/h1121.md), in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) them, as the [yowm](../../strongs/h/h3117.md) of [shamayim](../../strongs/h/h8064.md) upon the ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_11_22"></a>Deuteronomy 11:22

For if ye shall [shamar](../../strongs/h/h8104.md) [shamar](../../strongs/h/h8104.md) all these [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) you, to ['asah](../../strongs/h/h6213.md) them, to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in all his [derek](../../strongs/h/h1870.md), and to [dāḇaq](../../strongs/h/h1692.md) unto him;

<a name="deuteronomy_11_23"></a>Deuteronomy 11:23

Then will [Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md) all these [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) you, and ye shall [yarash](../../strongs/h/h3423.md) [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) and ['atsuwm](../../strongs/h/h6099.md) than yourselves.

<a name="deuteronomy_11_24"></a>Deuteronomy 11:24

Every [maqowm](../../strongs/h/h4725.md) whereon the [kaph](../../strongs/h/h3709.md) of your [regel](../../strongs/h/h7272.md) shall [dāraḵ](../../strongs/h/h1869.md) shall be yours: from the [midbar](../../strongs/h/h4057.md) and [Lᵊḇānôn](../../strongs/h/h3844.md), from the [nāhār](../../strongs/h/h5104.md), the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md), even unto the ['aḥărôn](../../strongs/h/h314.md) [yam](../../strongs/h/h3220.md) shall your [gᵊḇûl](../../strongs/h/h1366.md) be.

<a name="deuteronomy_11_25"></a>Deuteronomy 11:25

There shall no ['iysh](../../strongs/h/h376.md) be able to [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) you: for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) shall [nathan](../../strongs/h/h5414.md) the [paḥaḏ](../../strongs/h/h6343.md) of you and the [mowra'](../../strongs/h/h4172.md) of you [paniym](../../strongs/h/h6440.md) all the ['erets](../../strongs/h/h776.md) that ye shall [dāraḵ](../../strongs/h/h1869.md) upon, as he hath [dabar](../../strongs/h/h1696.md) unto you.

<a name="deuteronomy_11_26"></a>Deuteronomy 11:26

[ra'ah](../../strongs/h/h7200.md), I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you this [yowm](../../strongs/h/h3117.md) a [bĕrakah](../../strongs/h/h1293.md) and a [qᵊlālâ](../../strongs/h/h7045.md);

<a name="deuteronomy_11_27"></a>Deuteronomy 11:27

A [bĕrakah](../../strongs/h/h1293.md), if ye [shama'](../../strongs/h/h8085.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_11_28"></a>Deuteronomy 11:28

And a [qᵊlālâ](../../strongs/h/h7045.md), if ye will not [shama'](../../strongs/h/h8085.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), but [cuwr](../../strongs/h/h5493.md) out of the [derek](../../strongs/h/h1870.md) which I [tsavah](../../strongs/h/h6680.md) you this [yowm](../../strongs/h/h3117.md), to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), which ye have not [yada'](../../strongs/h/h3045.md).

<a name="deuteronomy_11_29"></a>Deuteronomy 11:29

And it shall come to pass, when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bow'](../../strongs/h/h935.md) thee in unto the ['erets](../../strongs/h/h776.md) whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it, that thou shalt [nathan](../../strongs/h/h5414.md) the [bĕrakah](../../strongs/h/h1293.md) upon [har](../../strongs/h/h2022.md) [Gᵊrizzîm](../../strongs/h/h1630.md), and the [qᵊlālâ](../../strongs/h/h7045.md) upon [har](../../strongs/h/h2022.md) [ʿÊḇāl](../../strongs/h/h5858.md).

<a name="deuteronomy_11_30"></a>Deuteronomy 11:30

Are they not on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), ['aḥar](../../strongs/h/h310.md) the [derek](../../strongs/h/h1870.md) where the [šemeš](../../strongs/h/h8121.md) [māḇô'](../../strongs/h/h3996.md), in the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), which [yashab](../../strongs/h/h3427.md) in the ['arabah](../../strongs/h/h6160.md) over [môl](../../strongs/h/h4136.md) [Gilgāl](../../strongs/h/h1537.md), beside the ['ēlôn](../../strongs/h/h436.md) of [Môrê](../../strongs/h/h4176.md)?

<a name="deuteronomy_11_31"></a>Deuteronomy 11:31

For ye shall ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) you, and ye shall [yarash](../../strongs/h/h3423.md) it, and [yashab](../../strongs/h/h3427.md) therein.

<a name="deuteronomy_11_32"></a>Deuteronomy 11:32

And ye shall [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md) which I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 10](deuteronomy_10.md) - [Deuteronomy 12](deuteronomy_12.md)