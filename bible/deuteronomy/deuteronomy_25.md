# [Deuteronomy 25](https://www.blueletterbible.org/kjv/deu/25)

<a name="deuteronomy_25_1"></a>Deuteronomy 25:1

If there be a [rîḇ](../../strongs/h/h7379.md) between ['enowsh](../../strongs/h/h582.md), and they [nāḡaš](../../strongs/h/h5066.md) unto [mishpat](../../strongs/h/h4941.md), that the judges may [shaphat](../../strongs/h/h8199.md) them; then they shall [ṣāḏaq](../../strongs/h/h6663.md) the [tsaddiyq](../../strongs/h/h6662.md), and [rāšaʿ](../../strongs/h/h7561.md) the [rasha'](../../strongs/h/h7563.md).

<a name="deuteronomy_25_2"></a>Deuteronomy 25:2

And it shall be, if the [rasha'](../../strongs/h/h7563.md) be [ben](../../strongs/h/h1121.md) to be [nakah](../../strongs/h/h5221.md), that the [shaphat](../../strongs/h/h8199.md) shall cause him to [naphal](../../strongs/h/h5307.md), and to be [nakah](../../strongs/h/h5221.md) before his [paniym](../../strongs/h/h6440.md), [day](../../strongs/h/h1767.md) to his [rišʿâ](../../strongs/h/h7564.md), by a certain [mispār](../../strongs/h/h4557.md).

<a name="deuteronomy_25_3"></a>Deuteronomy 25:3

Forty [nakah](../../strongs/h/h5221.md) he may give him, and not [yāsap̄](../../strongs/h/h3254.md): lest, if he should [yāsap̄](../../strongs/h/h3254.md), and [nakah](../../strongs/h/h5221.md) him above these with [rab](../../strongs/h/h7227.md) [makâ](../../strongs/h/h4347.md), then thy ['ach](../../strongs/h/h251.md) should [qālâ](../../strongs/h/h7034.md) ['ayin](../../strongs/h/h5869.md) unto thee.

<a name="deuteronomy_25_4"></a>Deuteronomy 25:4

Thou shalt not [ḥāsam](../../strongs/h/h2629.md) the [showr](../../strongs/h/h7794.md) when he [dîn](../../strongs/h/h1778.md).

<a name="deuteronomy_25_5"></a>Deuteronomy 25:5

If ['ach](../../strongs/h/h251.md) [yashab](../../strongs/h/h3427.md) [yaḥaḏ](../../strongs/h/h3162.md), and one of them [muwth](../../strongs/h/h4191.md), and have no [ben](../../strongs/h/h1121.md), the ['ishshah](../../strongs/h/h802.md) of the [muwth](../../strongs/h/h4191.md) shall not [ḥûṣ](../../strongs/h/h2351.md) unto an ['iysh](../../strongs/h/h376.md) [zûr](../../strongs/h/h2114.md): her [yāḇām](../../strongs/h/h2993.md) shall [bow'](../../strongs/h/h935.md) unto her, and [laqach](../../strongs/h/h3947.md) her to him to ['ishshah](../../strongs/h/h802.md), and perform the duty of a [yibēm](../../strongs/h/h2992.md) unto her.

<a name="deuteronomy_25_6"></a>Deuteronomy 25:6

And it shall be, that the [bᵊḵôr](../../strongs/h/h1060.md) which she [yalad](../../strongs/h/h3205.md) shall [quwm](../../strongs/h/h6965.md) in the [shem](../../strongs/h/h8034.md) of his ['ach](../../strongs/h/h251.md) which is [muwth](../../strongs/h/h4191.md), that his [shem](../../strongs/h/h8034.md) be not [māḥâ](../../strongs/h/h4229.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_25_7"></a>Deuteronomy 25:7

And if the ['iysh](../../strongs/h/h376.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) not to [laqach](../../strongs/h/h3947.md) his [yᵊḇēmeṯ](../../strongs/h/h2994.md), then let his [yᵊḇēmeṯ](../../strongs/h/h2994.md) [ʿālâ](../../strongs/h/h5927.md) to the [sha'ar](../../strongs/h/h8179.md) unto the [zāqēn](../../strongs/h/h2205.md), and ['āmar](../../strongs/h/h559.md), My [yāḇām](../../strongs/h/h2993.md) [mā'ēn](../../strongs/h/h3985.md) to [quwm](../../strongs/h/h6965.md) unto his ['ach](../../strongs/h/h251.md) a [shem](../../strongs/h/h8034.md) in [Yisra'el](../../strongs/h/h3478.md), he ['āḇâ](../../strongs/h/h14.md) not [yibēm](../../strongs/h/h2992.md).

<a name="deuteronomy_25_8"></a>Deuteronomy 25:8

Then the [zāqēn](../../strongs/h/h2205.md) of his [ʿîr](../../strongs/h/h5892.md) shall [qara'](../../strongs/h/h7121.md) him, and [dabar](../../strongs/h/h1696.md) unto him: and if he ['amad](../../strongs/h/h5975.md) to it, and ['āmar](../../strongs/h/h559.md), I [ḥāp̄ēṣ](../../strongs/h/h2654.md) not to [laqach](../../strongs/h/h3947.md) her;

<a name="deuteronomy_25_9"></a>Deuteronomy 25:9

Then shall his [yᵊḇēmeṯ](../../strongs/h/h2994.md) [nāḡaš](../../strongs/h/h5066.md) unto him in the ['ayin](../../strongs/h/h5869.md) of the [zāqēn](../../strongs/h/h2205.md), and [chalats](../../strongs/h/h2502.md) his [naʿal](../../strongs/h/h5275.md) from off his [regel](../../strongs/h/h7272.md), and [yāraq](../../strongs/h/h3417.md) in his [paniym](../../strongs/h/h6440.md), and shall ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), So shall it be ['asah](../../strongs/h/h6213.md) unto that ['iysh](../../strongs/h/h376.md) that will not [bānâ](../../strongs/h/h1129.md) his ['ach](../../strongs/h/h251.md) [bayith](../../strongs/h/h1004.md).

<a name="deuteronomy_25_10"></a>Deuteronomy 25:10

And his [shem](../../strongs/h/h8034.md) shall be [qara'](../../strongs/h/h7121.md) in [Yisra'el](../../strongs/h/h3478.md), The [bayith](../../strongs/h/h1004.md) of him that hath his [naʿal](../../strongs/h/h5275.md) [chalats](../../strongs/h/h2502.md).

<a name="deuteronomy_25_11"></a>Deuteronomy 25:11

When ['enowsh](../../strongs/h/h582.md) [nāṣâ](../../strongs/h/h5327.md) [yaḥaḏ](../../strongs/h/h3162.md) ['ach](../../strongs/h/h251.md) ['iysh](../../strongs/h/h376.md), and the ['ishshah](../../strongs/h/h802.md) of the one [qāraḇ](../../strongs/h/h7126.md) for to [natsal](../../strongs/h/h5337.md) her ['iysh](../../strongs/h/h376.md) out of the [yad](../../strongs/h/h3027.md) of him that [nakah](../../strongs/h/h5221.md) him, and [shalach](../../strongs/h/h7971.md) her [yad](../../strongs/h/h3027.md), and [ḥāzaq](../../strongs/h/h2388.md) him by the [mᵊḇušîm](../../strongs/h/h4016.md):

<a name="deuteronomy_25_12"></a>Deuteronomy 25:12

Then thou shalt [qāṣaṣ](../../strongs/h/h7112.md) her [kaph](../../strongs/h/h3709.md), thine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md) her.

<a name="deuteronomy_25_13"></a>Deuteronomy 25:13

Thou shalt not have in thy [kîs](../../strongs/h/h3599.md) ['eben](../../strongs/h/h68.md) ['eben](../../strongs/h/h68.md), a [gadowl](../../strongs/h/h1419.md) and a [qāṭān](../../strongs/h/h6996.md).

<a name="deuteronomy_25_14"></a>Deuteronomy 25:14

Thou shalt not have in thine [bayith](../../strongs/h/h1004.md) ['êp̄â](../../strongs/h/h374.md) ['êp̄â](../../strongs/h/h374.md), a [gadowl](../../strongs/h/h1419.md) and a [qāṭān](../../strongs/h/h6996.md).

<a name="deuteronomy_25_15"></a>Deuteronomy 25:15

But thou shalt have a [šālēm](../../strongs/h/h8003.md) and [tsedeq](../../strongs/h/h6664.md) ['eben](../../strongs/h/h68.md), a [šālēm](../../strongs/h/h8003.md) and [tsedeq](../../strongs/h/h6664.md) ['êp̄â](../../strongs/h/h374.md) shalt thou have: that thy [yowm](../../strongs/h/h3117.md) may be ['arak](../../strongs/h/h748.md) in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_25_16"></a>Deuteronomy 25:16

For all that ['asah](../../strongs/h/h6213.md) such things, and all that ['asah](../../strongs/h/h6213.md) ['evel](../../strongs/h/h5766.md), are a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_25_17"></a>Deuteronomy 25:17

[zakar](../../strongs/h/h2142.md) what [ʿĂmālēq](../../strongs/h/h6002.md) ['asah](../../strongs/h/h6213.md) unto thee by the [derek](../../strongs/h/h1870.md), when ye were [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md);

<a name="deuteronomy_25_18"></a>Deuteronomy 25:18

How he [qārâ](../../strongs/h/h7136.md) thee by the [derek](../../strongs/h/h1870.md), and [zānāḇ](../../strongs/h/h2179.md) of thee, even all that were [ḥāšal](../../strongs/h/h2826.md) ['aḥar](../../strongs/h/h310.md) thee, when thou wast [ʿāyēp̄](../../strongs/h/h5889.md) and [yāḡēaʿ](../../strongs/h/h3023.md); and he [yārē'](../../strongs/h/h3373.md) not ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_25_19"></a>Deuteronomy 25:19

Therefore it shall be, when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nuwach](../../strongs/h/h5117.md) from all thine ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), in the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md) to [yarash](../../strongs/h/h3423.md) it, that thou shalt [māḥâ](../../strongs/h/h4229.md) the [zeker](../../strongs/h/h2143.md) of [ʿĂmālēq](../../strongs/h/h6002.md) from under [shamayim](../../strongs/h/h8064.md); thou shalt not [shakach](../../strongs/h/h7911.md) it.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 24](deuteronomy_24.md) - [Deuteronomy 26](deuteronomy_26.md)