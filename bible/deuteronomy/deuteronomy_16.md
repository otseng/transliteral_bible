# [Deuteronomy 16](https://www.blueletterbible.org/esv/deu/16)

<a name="deuteronomy_16_1"></a>Deuteronomy 16:1

[shamar](../../strongs/h/h8104.md) the [ḥōḏeš](../../strongs/h/h2320.md) of ['āḇîḇ](../../strongs/h/h24.md), and ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): for in the [ḥōḏeš](../../strongs/h/h2320.md) of ['āḇîḇ](../../strongs/h/h24.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yāṣā'](../../strongs/h/h3318.md) thee out of [Mitsrayim](../../strongs/h/h4714.md) by [layil](../../strongs/h/h3915.md).

<a name="deuteronomy_16_2"></a>Deuteronomy 16:2

Thou shalt therefore [zabach](../../strongs/h/h2076.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), of the [tso'n](../../strongs/h/h6629.md) and the [bāqār](../../strongs/h/h1241.md), in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) his [shem](../../strongs/h/h8034.md) there.

<a name="deuteronomy_16_3"></a>Deuteronomy 16:3

Thou shalt ['akal](../../strongs/h/h398.md) no [ḥāmēṣ](../../strongs/h/h2557.md) with it; seven [yowm](../../strongs/h/h3117.md) shalt thou ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md) therewith, even the [lechem](../../strongs/h/h3899.md) of ['oniy](../../strongs/h/h6040.md); for thou [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) in [ḥipāzôn](../../strongs/h/h2649.md): that thou mayest [zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) when thou [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md).

<a name="deuteronomy_16_4"></a>Deuteronomy 16:4

And there shall be no [śᵊ'ōr](../../strongs/h/h7603.md) [ra'ah](../../strongs/h/h7200.md) with thee in all thy [gᵊḇûl](../../strongs/h/h1366.md) seven [yowm](../../strongs/h/h3117.md); neither shall there any thing of the [basar](../../strongs/h/h1320.md), which thou [zabach](../../strongs/h/h2076.md) the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) at ['ereb](../../strongs/h/h6153.md), [lûn](../../strongs/h/h3885.md) until the [boqer](../../strongs/h/h1242.md).

<a name="deuteronomy_16_5"></a>Deuteronomy 16:5

Thou [yakol](../../strongs/h/h3201.md) not [zabach](../../strongs/h/h2076.md) the [pecach](../../strongs/h/h6453.md) within any of thy [sha'ar](../../strongs/h/h8179.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee:

<a name="deuteronomy_16_6"></a>Deuteronomy 16:6

But at the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) his [shem](../../strongs/h/h8034.md) in, there thou shalt [zabach](../../strongs/h/h2076.md) the [pecach](../../strongs/h/h6453.md) at ['ereb](../../strongs/h/h6153.md), at the [bow'](../../strongs/h/h935.md) of the [šemeš](../../strongs/h/h8121.md), at the [môʿēḏ](../../strongs/h/h4150.md) that thou [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_16_7"></a>Deuteronomy 16:7

And thou shalt [bāšal](../../strongs/h/h1310.md) and ['akal](../../strongs/h/h398.md) it in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md): and thou shalt [panah](../../strongs/h/h6437.md) in the [boqer](../../strongs/h/h1242.md), and [halak](../../strongs/h/h1980.md) unto thy ['ohel](../../strongs/h/h168.md).

<a name="deuteronomy_16_8"></a>Deuteronomy 16:8

Six [yowm](../../strongs/h/h3117.md) thou shalt ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md): and on the seventh [yowm](../../strongs/h/h3117.md) shall be a [ʿăṣārâ](../../strongs/h/h6116.md) to [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): thou shalt ['asah](../../strongs/h/h6213.md) no [mĕla'kah](../../strongs/h/h4399.md) therein.

<a name="deuteronomy_16_9"></a>Deuteronomy 16:9

Seven [šāḇûaʿ](../../strongs/h/h7620.md) shalt thou [sāp̄ar](../../strongs/h/h5608.md) unto thee: [ḥālal](../../strongs/h/h2490.md) to [sāp̄ar](../../strongs/h/h5608.md) the seven [šāḇûaʿ](../../strongs/h/h7620.md) from as thou [ḥālal](../../strongs/h/h2490.md) the [ḥermēš](../../strongs/h/h2770.md) to the [qāmâ](../../strongs/h/h7054.md).

<a name="deuteronomy_16_10"></a>Deuteronomy 16:10

And thou shalt ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [šāḇûaʿ](../../strongs/h/h7620.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) with a [missâ](../../strongs/h/h4530.md) of a [nᵊḏāḇâ](../../strongs/h/h5071.md) of thine [yad](../../strongs/h/h3027.md), which thou shalt [nathan](../../strongs/h/h5414.md), according as [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee:

<a name="deuteronomy_16_11"></a>Deuteronomy 16:11

And thou shalt [samach](../../strongs/h/h8055.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), thou, and thy [ben](../../strongs/h/h1121.md), and thy [bath](../../strongs/h/h1323.md), and thy ['ebed](../../strongs/h/h5650.md), and thy ['amah](../../strongs/h/h519.md), and the [Lᵊvî](../../strongs/h/h3881.md) that is within thy [sha'ar](../../strongs/h/h8179.md), and the [ger](../../strongs/h/h1616.md), and the [yathowm](../../strongs/h/h3490.md), and the ['almānâ](../../strongs/h/h490.md), that are [qereḇ](../../strongs/h/h7130.md) you, in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) his [shem](../../strongs/h/h8034.md) there.

<a name="deuteronomy_16_12"></a>Deuteronomy 16:12

And thou shalt [zakar](../../strongs/h/h2142.md) that thou wast an ['ebed](../../strongs/h/h5650.md) in [Mitsrayim](../../strongs/h/h4714.md): and thou shalt [shamar](../../strongs/h/h8104.md) and ['asah](../../strongs/h/h6213.md) these [choq](../../strongs/h/h2706.md).

<a name="deuteronomy_16_13"></a>Deuteronomy 16:13

Thou shalt ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md) seven [yowm](../../strongs/h/h3117.md), after that thou hast ['āsap̄](../../strongs/h/h622.md) in thy [gōren](../../strongs/h/h1637.md) and thy [yeqeḇ](../../strongs/h/h3342.md):

<a name="deuteronomy_16_14"></a>Deuteronomy 16:14

And thou shalt [samach](../../strongs/h/h8055.md) in thy [ḥāḡ](../../strongs/h/h2282.md), thou, and thy [ben](../../strongs/h/h1121.md), and thy [bath](../../strongs/h/h1323.md), and thy ['ebed](../../strongs/h/h5650.md), and thy ['amah](../../strongs/h/h519.md), and the [Lᵊvî](../../strongs/h/h3881.md), the [ger](../../strongs/h/h1616.md), and the [yathowm](../../strongs/h/h3490.md), and the ['almānâ](../../strongs/h/h490.md), that are within thy [sha'ar](../../strongs/h/h8179.md).

<a name="deuteronomy_16_15"></a>Deuteronomy 16:15

Seven [yowm](../../strongs/h/h3117.md) shalt thou [ḥāḡaḡ](../../strongs/h/h2287.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md): because [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [barak](../../strongs/h/h1288.md) thee in all thine [tᵊḇû'â](../../strongs/h/h8393.md), and in all the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md), therefore thou shalt surely [śāmēaḥ](../../strongs/h/h8056.md).

<a name="deuteronomy_16_16"></a>Deuteronomy 16:16

Three [pa'am](../../strongs/h/h6471.md) in a [šānâ](../../strongs/h/h8141.md) shall all thy [zāḵûr](../../strongs/h/h2138.md) [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in the [maqowm](../../strongs/h/h4725.md) which he shall [bāḥar](../../strongs/h/h977.md); in the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md), and in the [ḥāḡ](../../strongs/h/h2282.md) of [šāḇûaʿ](../../strongs/h/h7620.md), and in the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md): and they shall not [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [rêqām](../../strongs/h/h7387.md):

<a name="deuteronomy_16_17"></a>Deuteronomy 16:17

Every ['iysh](../../strongs/h/h376.md) as he is [matānâ](../../strongs/h/h4979.md) [yad](../../strongs/h/h3027.md), according to the [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) which he hath [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_16_18"></a>Deuteronomy 16:18

[shaphat](../../strongs/h/h8199.md) and [šāṭar](../../strongs/h/h7860.md) shalt thou [nathan](../../strongs/h/h5414.md) thee in all thy [sha'ar](../../strongs/h/h8179.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, throughout thy [shebet](../../strongs/h/h7626.md): and they shall [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) with [tsedeq](../../strongs/h/h6664.md) [mishpat](../../strongs/h/h4941.md).

<a name="deuteronomy_16_19"></a>Deuteronomy 16:19

Thou shalt not [natah](../../strongs/h/h5186.md) [mishpat](../../strongs/h/h4941.md); thou shalt not [nāḵar](../../strongs/h/h5234.md) [paniym](../../strongs/h/h6440.md), neither [laqach](../../strongs/h/h3947.md) a [shachad](../../strongs/h/h7810.md): for a [shachad](../../strongs/h/h7810.md) doth [ʿāvar](../../strongs/h/h5786.md) the ['ayin](../../strongs/h/h5869.md) of the [ḥāḵām](../../strongs/h/h2450.md), and [sālap̄](../../strongs/h/h5557.md) the [dabar](../../strongs/h/h1697.md) of the [tsaddiyq](../../strongs/h/h6662.md).

<a name="deuteronomy_16_20"></a>Deuteronomy 16:20

That which is [tsedeq](../../strongs/h/h6664.md) [tsedeq](../../strongs/h/h6664.md) shalt thou [radaph](../../strongs/h/h7291.md), that thou mayest [ḥāyâ](../../strongs/h/h2421.md), and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_16_21"></a>Deuteronomy 16:21

Thou shalt not [nāṭaʿ](../../strongs/h/h5193.md) thee an ['ăšērâ](../../strongs/h/h842.md) of any ['ets](../../strongs/h/h6086.md) ['ēṣel](../../strongs/h/h681.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which thou shalt ['asah](../../strongs/h/h6213.md) thee.

<a name="deuteronomy_16_22"></a>Deuteronomy 16:22

Neither shalt thou [quwm](../../strongs/h/h6965.md) thee any [maṣṣēḇâ](../../strongs/h/h4676.md); which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [sane'](../../strongs/h/h8130.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 15](deuteronomy_15.md) - [Deuteronomy 17](deuteronomy_17.md)