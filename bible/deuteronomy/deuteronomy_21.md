# [Deuteronomy 21](https://www.blueletterbible.org/kjv/deu/21)

<a name="deuteronomy_21_1"></a>Deuteronomy 21:1

If one be [māṣā'](../../strongs/h/h4672.md) [ḥālāl](../../strongs/h/h2491.md) in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee to [yarash](../../strongs/h/h3423.md) it, [naphal](../../strongs/h/h5307.md) in the [sadeh](../../strongs/h/h7704.md), and it be not [yada'](../../strongs/h/h3045.md) who hath [nakah](../../strongs/h/h5221.md) him:

<a name="deuteronomy_21_2"></a>Deuteronomy 21:2

Then thy [zāqēn](../../strongs/h/h2205.md) and thy [shaphat](../../strongs/h/h8199.md) shall [yāṣā'](../../strongs/h/h3318.md), and they shall [māḏaḏ](../../strongs/h/h4058.md) unto the [ʿîr](../../strongs/h/h5892.md) which are [cabiyb](../../strongs/h/h5439.md) him that is [ḥālāl](../../strongs/h/h2491.md):

<a name="deuteronomy_21_3"></a>Deuteronomy 21:3

And it shall be, that the [ʿîr](../../strongs/h/h5892.md) which is [qarowb](../../strongs/h/h7138.md) unto the [ḥālāl](../../strongs/h/h2491.md), even the [zāqēn](../../strongs/h/h2205.md) of that [ʿîr](../../strongs/h/h5892.md) shall [laqach](../../strongs/h/h3947.md) a [bāqār](../../strongs/h/h1241.md) [ʿeḡlâ](../../strongs/h/h5697.md), which hath not been ['abad](../../strongs/h/h5647.md), and which hath not [mashak](../../strongs/h/h4900.md) in the [ʿōl](../../strongs/h/h5923.md);

<a name="deuteronomy_21_4"></a>Deuteronomy 21:4

And the [zāqēn](../../strongs/h/h2205.md) of that [ʿîr](../../strongs/h/h5892.md) shall [yarad](../../strongs/h/h3381.md) the [ʿeḡlâ](../../strongs/h/h5697.md) unto a ['êṯān](../../strongs/h/h386.md) [nachal](../../strongs/h/h5158.md), which is neither ['abad](../../strongs/h/h5647.md) nor [zāraʿ](../../strongs/h/h2232.md), and shall [ʿārap̄](../../strongs/h/h6202.md) the [ʿeḡlâ](../../strongs/h/h5697.md) there in the [nachal](../../strongs/h/h5158.md):

<a name="deuteronomy_21_5"></a>Deuteronomy 21:5

And the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md) shall [nāḡaš](../../strongs/h/h5066.md); for them [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md) to [sharath](../../strongs/h/h8334.md) unto him, and to [barak](../../strongs/h/h1288.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md); and by their [peh](../../strongs/h/h6310.md) shall every [rîḇ](../../strongs/h/h7379.md) and every [neḡaʿ](../../strongs/h/h5061.md) be tried:

<a name="deuteronomy_21_6"></a>Deuteronomy 21:6

And all the [zāqēn](../../strongs/h/h2205.md) of that [ʿîr](../../strongs/h/h5892.md), that are [qarowb](../../strongs/h/h7138.md) unto the [ḥālāl](../../strongs/h/h2491.md), shall [rāḥaṣ](../../strongs/h/h7364.md) their [yad](../../strongs/h/h3027.md) over the [ʿeḡlâ](../../strongs/h/h5697.md) that is [ʿārap̄](../../strongs/h/h6202.md) in the [nachal](../../strongs/h/h5158.md):

<a name="deuteronomy_21_7"></a>Deuteronomy 21:7

And they shall ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Our [yad](../../strongs/h/h3027.md) have not [šāp̄aḵ](../../strongs/h/h8210.md) this [dam](../../strongs/h/h1818.md), neither have our ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md) it.

<a name="deuteronomy_21_8"></a>Deuteronomy 21:8

Be [kāp̄ar](../../strongs/h/h3722.md), [Yĕhovah](../../strongs/h/h3068.md), unto thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), whom thou hast [pāḏâ](../../strongs/h/h6299.md), and [nathan](../../strongs/h/h5414.md) not [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) unto thy ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) [qereḇ](../../strongs/h/h7130.md). And the [dam](../../strongs/h/h1818.md) shall be [kāp̄ar](../../strongs/h/h3722.md) them.

<a name="deuteronomy_21_9"></a>Deuteronomy 21:9

So shalt thou [bāʿar](../../strongs/h/h1197.md) the [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) from [qereḇ](../../strongs/h/h7130.md) you, when thou shalt ['asah](../../strongs/h/h6213.md) [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_21_10"></a>Deuteronomy 21:10

When thou [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against thine ['oyeb](../../strongs/h/h341.md), and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md), and thou hast [šāḇâ](../../strongs/h/h7617.md) them [šᵊḇî](../../strongs/h/h7628.md),

<a name="deuteronomy_21_11"></a>Deuteronomy 21:11

And [ra'ah](../../strongs/h/h7200.md) among the [šiḇyâ](../../strongs/h/h7633.md) a [tō'ar](../../strongs/h/h8389.md) [yāp̄ê](../../strongs/h/h3303.md) ['ishshah](../../strongs/h/h802.md), and hast a [ḥāšaq](../../strongs/h/h2836.md) unto her, that thou wouldest [laqach](../../strongs/h/h3947.md) her to thy ['ishshah](../../strongs/h/h802.md);

<a name="deuteronomy_21_12"></a>Deuteronomy 21:12

Then thou shalt [bow'](../../strongs/h/h935.md) her [tavek](../../strongs/h/h8432.md) to thine [bayith](../../strongs/h/h1004.md), and she shall [gālaḥ](../../strongs/h/h1548.md) her [ro'sh](../../strongs/h/h7218.md), and ['asah](../../strongs/h/h6213.md) her [ṣipōren](../../strongs/h/h6856.md);

<a name="deuteronomy_21_13"></a>Deuteronomy 21:13

And she shall [cuwr](../../strongs/h/h5493.md) the [śimlâ](../../strongs/h/h8071.md) of her [šᵊḇî](../../strongs/h/h7628.md) from off her, and shall [yashab](../../strongs/h/h3427.md) in thine [bayith](../../strongs/h/h1004.md), and [bāḵâ](../../strongs/h/h1058.md) her ['ab](../../strongs/h/h1.md) and her ['em](../../strongs/h/h517.md) a [yowm](../../strongs/h/h3117.md) [yeraḥ](../../strongs/h/h3391.md): and ['aḥar](../../strongs/h/h310.md) that thou shalt [bow'](../../strongs/h/h935.md) in unto her, and be her [bāʿal](../../strongs/h/h1166.md), and she shall be thy ['ishshah](../../strongs/h/h802.md).

<a name="deuteronomy_21_14"></a>Deuteronomy 21:14

And it shall be, if thou have no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in her, then thou shalt let her [shalach](../../strongs/h/h7971.md) whither she [nephesh](../../strongs/h/h5315.md); but thou shalt not [māḵar](../../strongs/h/h4376.md) her at all for [keceph](../../strongs/h/h3701.md), thou shalt not [ʿāmar](../../strongs/h/h6014.md) her, because thou hast [ʿānâ](../../strongs/h/h6031.md) her.

<a name="deuteronomy_21_15"></a>Deuteronomy 21:15

If an ['iysh](../../strongs/h/h376.md) have two ['ishshah](../../strongs/h/h802.md), one ['ahab](../../strongs/h/h157.md), and another [sane'](../../strongs/h/h8130.md), and they have [yalad](../../strongs/h/h3205.md) him [ben](../../strongs/h/h1121.md), both the ['ahab](../../strongs/h/h157.md) and the [sane'](../../strongs/h/h8130.md); and if the [bᵊḵôr](../../strongs/h/h1060.md) [ben](../../strongs/h/h1121.md) be hers that was [śᵊnî'](../../strongs/h/h8146.md):

<a name="deuteronomy_21_16"></a>Deuteronomy 21:16

Then it shall be, [yowm](../../strongs/h/h3117.md) he maketh his [ben](../../strongs/h/h1121.md) to [nāḥal](../../strongs/h/h5157.md) that which he hath, that he [yakol](../../strongs/h/h3201.md) not make the [ben](../../strongs/h/h1121.md) of the ['ahab](../../strongs/h/h157.md) [bāḵar](../../strongs/h/h1069.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of the [sane'](../../strongs/h/h8130.md), which is indeed the [bᵊḵôr](../../strongs/h/h1060.md):

<a name="deuteronomy_21_17"></a>Deuteronomy 21:17

But he shall [nāḵar](../../strongs/h/h5234.md) the [ben](../../strongs/h/h1121.md) of the [sane'](../../strongs/h/h8130.md) for the [bᵊḵôr](../../strongs/h/h1060.md), by [nathan](../../strongs/h/h5414.md) him a [šᵊnayim](../../strongs/h/h8147.md) [peh](../../strongs/h/h6310.md) of all that he [māṣā'](../../strongs/h/h4672.md): for he is the [re'shiyth](../../strongs/h/h7225.md) of his ['ôn](../../strongs/h/h202.md); the [mishpat](../../strongs/h/h4941.md) of the [bᵊḵôrâ](../../strongs/h/h1062.md) is his.

<a name="deuteronomy_21_18"></a>Deuteronomy 21:18

If an ['iysh](../../strongs/h/h376.md) have a [sārar](../../strongs/h/h5637.md) and [marah](../../strongs/h/h4784.md) [ben](../../strongs/h/h1121.md), which will not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of his ['ab](../../strongs/h/h1.md), or the [qowl](../../strongs/h/h6963.md) of his ['em](../../strongs/h/h517.md), and that, when they have [yacar](../../strongs/h/h3256.md) him, will not [shama'](../../strongs/h/h8085.md) unto them:

<a name="deuteronomy_21_19"></a>Deuteronomy 21:19

Then shall his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md) [tāp̄aś](../../strongs/h/h8610.md) on him, and [yāṣā'](../../strongs/h/h3318.md) him unto the [zāqēn](../../strongs/h/h2205.md) of his [ʿîr](../../strongs/h/h5892.md), and unto the [sha'ar](../../strongs/h/h8179.md) of his [maqowm](../../strongs/h/h4725.md);

<a name="deuteronomy_21_20"></a>Deuteronomy 21:20

And they shall ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md) of his [ʿîr](../../strongs/h/h5892.md), This our [ben](../../strongs/h/h1121.md) is [sārar](../../strongs/h/h5637.md) and [marah](../../strongs/h/h4784.md), he will not [shama'](../../strongs/h/h8085.md) our [qowl](../../strongs/h/h6963.md); he is a [zālal](../../strongs/h/h2151.md), and a [sāḇā'](../../strongs/h/h5433.md).

<a name="deuteronomy_21_21"></a>Deuteronomy 21:21

And all the ['enowsh](../../strongs/h/h582.md) of his [ʿîr](../../strongs/h/h5892.md) shall [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md), that he [muwth](../../strongs/h/h4191.md): so shalt thou [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you; and all [Yisra'el](../../strongs/h/h3478.md) shall [shama'](../../strongs/h/h8085.md), and [yare'](../../strongs/h/h3372.md).

<a name="deuteronomy_21_22"></a>Deuteronomy 21:22

And if an ['iysh](../../strongs/h/h376.md) have [ḥēṭĕ'](../../strongs/h/h2399.md) [mishpat](../../strongs/h/h4941.md) [maveth](../../strongs/h/h4194.md), and he be to be [muwth](../../strongs/h/h4191.md), and thou [tālâ](../../strongs/h/h8518.md) him on an ['ets](../../strongs/h/h6086.md):

<a name="deuteronomy_21_23"></a>Deuteronomy 21:23

His [nᵊḇēlâ](../../strongs/h/h5038.md) shall not [lûn](../../strongs/h/h3885.md) upon the ['ets](../../strongs/h/h6086.md), but thou shalt [qāḇar](../../strongs/h/h6912.md) [qāḇar](../../strongs/h/h6912.md) him that [yowm](../../strongs/h/h3117.md); (for he that is [tālâ](../../strongs/h/h8518.md) is [qᵊlālâ](../../strongs/h/h7045.md) of ['Elohiym](../../strongs/h/h430.md);) that thy ['ăḏāmâ](../../strongs/h/h127.md) be not [ṭāmē'](../../strongs/h/h2930.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 20](deuteronomy_20.md) - [Deuteronomy 22](deuteronomy_22.md)