# [Deuteronomy 18](https://www.blueletterbible.org/esv/deu/18)

<a name="deuteronomy_18_1"></a>Deuteronomy 18:1

The [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md), and all the [shebet](../../strongs/h/h7626.md) of [Lēvî](../../strongs/h/h3878.md), shall have no [cheleq](../../strongs/h/h2506.md) nor [nachalah](../../strongs/h/h5159.md) with [Yisra'el](../../strongs/h/h3478.md): they shall ['akal](../../strongs/h/h398.md) the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md), and his [nachalah](../../strongs/h/h5159.md).

<a name="deuteronomy_18_2"></a>Deuteronomy 18:2

Therefore shall they have no [nachalah](../../strongs/h/h5159.md) [qereḇ](../../strongs/h/h7130.md) their ['ach](../../strongs/h/h251.md): [Yĕhovah](../../strongs/h/h3068.md) is their [nachalah](../../strongs/h/h5159.md), as he hath [dabar](../../strongs/h/h1696.md) unto them.

<a name="deuteronomy_18_3"></a>Deuteronomy 18:3

And this shall be the [kōhēn](../../strongs/h/h3548.md) [mishpat](../../strongs/h/h4941.md) from the ['am](../../strongs/h/h5971.md), from them that [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md), whether it be [showr](../../strongs/h/h7794.md) or [śê](../../strongs/h/h7716.md); and they shall [nathan](../../strongs/h/h5414.md) unto the [kōhēn](../../strongs/h/h3548.md) the [zerowa'](../../strongs/h/h2220.md), and the two [lᵊḥî](../../strongs/h/h3895.md), and the [qēḇâ](../../strongs/h/h6896.md).

<a name="deuteronomy_18_4"></a>Deuteronomy 18:4

The [re'shiyth](../../strongs/h/h7225.md) also of thy [dagan](../../strongs/h/h1715.md), of thy [tiyrowsh](../../strongs/h/h8492.md), and of thine [yiṣhār](../../strongs/h/h3323.md), and the [re'shiyth](../../strongs/h/h7225.md) of the [gēz](../../strongs/h/h1488.md) of thy [tso'n](../../strongs/h/h6629.md), shalt thou [nathan](../../strongs/h/h5414.md) him.

<a name="deuteronomy_18_5"></a>Deuteronomy 18:5

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md) him out of all thy [shebet](../../strongs/h/h7626.md), to ['amad](../../strongs/h/h5975.md) to [sharath](../../strongs/h/h8334.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), him and his [ben](../../strongs/h/h1121.md) [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_18_6"></a>Deuteronomy 18:6

And if a [Lᵊvî](../../strongs/h/h3881.md) [bow'](../../strongs/h/h935.md) from any of thy [sha'ar](../../strongs/h/h8179.md) out of all [Yisra'el](../../strongs/h/h3478.md), where he [guwr](../../strongs/h/h1481.md), and [bow'](../../strongs/h/h935.md) with all the ['aûâ](../../strongs/h/h185.md) of his [nephesh](../../strongs/h/h5315.md) unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md);

<a name="deuteronomy_18_7"></a>Deuteronomy 18:7

Then he shall [sharath](../../strongs/h/h8334.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), as all his ['ach](../../strongs/h/h251.md) the [Lᵊvî](../../strongs/h/h3881.md), which ['amad](../../strongs/h/h5975.md) there [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_18_8"></a>Deuteronomy 18:8

They shall have [cheleq](../../strongs/h/h2506.md) to ['akal](../../strongs/h/h398.md), beside [mimkār](../../strongs/h/h4465.md) of his ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_18_9"></a>Deuteronomy 18:9

When thou art [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, thou shalt not [lamad](../../strongs/h/h3925.md) to ['asah](../../strongs/h/h6213.md) after the [tôʿēḇâ](../../strongs/h/h8441.md) of those [gowy](../../strongs/h/h1471.md).

<a name="deuteronomy_18_10"></a>Deuteronomy 18:10

There shall not be [māṣā'](../../strongs/h/h4672.md) among you any one that maketh his [ben](../../strongs/h/h1121.md) or his [bath](../../strongs/h/h1323.md) to ['abar](../../strongs/h/h5674.md) through the ['esh](../../strongs/h/h784.md), or that [qāsam](../../strongs/h/h7080.md) [qesem](../../strongs/h/h7081.md), or an [ʿānan](../../strongs/h/h6049.md), or a [nāḥaš](../../strongs/h/h5172.md), or a [kāšap̄](../../strongs/h/h3784.md).

<a name="deuteronomy_18_11"></a>Deuteronomy 18:11

Or a [ḥāḇar](../../strongs/h/h2266.md) [Ḥeḇer](../../strongs/h/h2267.md), or a [sha'al](../../strongs/h/h7592.md) with ['ôḇ](../../strongs/h/h178.md), or a [yiḏʿōnî](../../strongs/h/h3049.md), or a [darash](../../strongs/h/h1875.md) [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_18_12"></a>Deuteronomy 18:12

For all that ['asah](../../strongs/h/h6213.md) these things are a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md): and because of these [tôʿēḇâ](../../strongs/h/h8441.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) doth [yarash](../../strongs/h/h3423.md) them from [paniym](../../strongs/h/h6440.md) thee.

<a name="deuteronomy_18_13"></a>Deuteronomy 18:13

Thou shalt be [tamiym](../../strongs/h/h8549.md) with [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_18_14"></a>Deuteronomy 18:14

For these [gowy](../../strongs/h/h1471.md), which thou shalt [yarash](../../strongs/h/h3423.md), [shama'](../../strongs/h/h8085.md) unto [ʿānan](../../strongs/h/h6049.md), and unto [qāsam](../../strongs/h/h7080.md): but as for thee, [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath not [nathan](../../strongs/h/h5414.md) thee so to do.

<a name="deuteronomy_18_15"></a>Deuteronomy 18:15

[Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [quwm](../../strongs/h/h6965.md) unto thee a [nāḇî'](../../strongs/h/h5030.md) from the [qereḇ](../../strongs/h/h7130.md) of thee, of thy ['ach](../../strongs/h/h251.md), like unto me; unto him ye shall [shama'](../../strongs/h/h8085.md);

<a name="deuteronomy_18_16"></a>Deuteronomy 18:16

According to all that thou [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in [Hōrēḇ](../../strongs/h/h2722.md) in the [yowm](../../strongs/h/h3117.md) of the [qāhēl](../../strongs/h/h6951.md), ['āmar](../../strongs/h/h559.md), Let me not [shama'](../../strongs/h/h8085.md) again the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), neither let me [ra'ah](../../strongs/h/h7200.md) this [gadowl](../../strongs/h/h1419.md) ['esh](../../strongs/h/h784.md) any more, that I [muwth](../../strongs/h/h4191.md) not.

<a name="deuteronomy_18_17"></a>Deuteronomy 18:17

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, They have [yatab](../../strongs/h/h3190.md) which they have [dabar](../../strongs/h/h1696.md).

<a name="deuteronomy_18_18"></a>Deuteronomy 18:18

I will [quwm](../../strongs/h/h6965.md) them a [nāḇî'](../../strongs/h/h5030.md) from [qereḇ](../../strongs/h/h7130.md) their ['ach](../../strongs/h/h251.md), like unto thee, and will [nathan](../../strongs/h/h5414.md) my [dabar](../../strongs/h/h1697.md) in his [peh](../../strongs/h/h6310.md); and he shall [dabar](../../strongs/h/h1696.md) unto them all that I shall [tsavah](../../strongs/h/h6680.md) him.

<a name="deuteronomy_18_19"></a>Deuteronomy 18:19

And it shall come to pass, ['iysh](../../strongs/h/h376.md) will not [shama'](../../strongs/h/h8085.md) unto my [dabar](../../strongs/h/h1697.md) which he shall [dabar](../../strongs/h/h1696.md) in my [shem](../../strongs/h/h8034.md), I will [darash](../../strongs/h/h1875.md) it of him.

<a name="deuteronomy_18_20"></a>Deuteronomy 18:20

But the [nāḇî'](../../strongs/h/h5030.md), which shall [zûḏ](../../strongs/h/h2102.md) to [dabar](../../strongs/h/h1696.md) a [dabar](../../strongs/h/h1697.md) in my [shem](../../strongs/h/h8034.md), which I have not [tsavah](../../strongs/h/h6680.md) him to [dabar](../../strongs/h/h1696.md), or that shall [dabar](../../strongs/h/h1696.md) in the [shem](../../strongs/h/h8034.md) of ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), even that [nāḇî'](../../strongs/h/h5030.md) shall [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_18_21"></a>Deuteronomy 18:21

And if thou ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), How shall we [yada'](../../strongs/h/h3045.md) the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath not [dabar](../../strongs/h/h1696.md)?

<a name="deuteronomy_18_22"></a>Deuteronomy 18:22

When a [nāḇî'](../../strongs/h/h5030.md) [dabar](../../strongs/h/h1696.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), if the [dabar](../../strongs/h/h1697.md) follow not, nor [bow'](../../strongs/h/h935.md), that is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath not [dabar](../../strongs/h/h1696.md), but the [nāḇî'](../../strongs/h/h5030.md) hath [dabar](../../strongs/h/h1696.md) it [zāḏôn](../../strongs/h/h2087.md): thou shalt not be [guwr](../../strongs/h/h1481.md) of him.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 17](deuteronomy_17.md) - [Deuteronomy 19](deuteronomy_19.md)