# [Deuteronomy 9](https://www.blueletterbible.org/kjv/deu/9)

<a name="deuteronomy_9_1"></a>Deuteronomy 9:1

[shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md): Thou art to ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) this [yowm](../../strongs/h/h3117.md), to [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) [gowy](../../strongs/h/h1471.md) [gadowl](../../strongs/h/h1419.md) and ['atsuwm](../../strongs/h/h6099.md) than thyself, [ʿîr](../../strongs/h/h5892.md) [gadowl](../../strongs/h/h1419.md) and [bāṣar](../../strongs/h/h1219.md) t[shamayim](../../strongs/h/h8064.md),

<a name="deuteronomy_9_2"></a>Deuteronomy 9:2

an ['am](../../strongs/h/h5971.md) [gadowl](../../strongs/h/h1419.md) and [ruwm](../../strongs/h/h7311.md), the [ben](../../strongs/h/h1121.md) of the [ʿĂnāqîm](../../strongs/h/h6062.md), whom thou [yada'](../../strongs/h/h3045.md), and of whom thou hast [shama'](../../strongs/h/h8085.md) say, Who can [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [ʿĂnāq](../../strongs/h/h6061.md)!

<a name="deuteronomy_9_3"></a>Deuteronomy 9:3

[yada'](../../strongs/h/h3045.md) therefore this [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is he which ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) thee; as an ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md) he shall [šāmaḏ](../../strongs/h/h8045.md) them, and he shall [kānaʿ](../../strongs/h/h3665.md) before thy [paniym](../../strongs/h/h6440.md): so shalt thou [yarash](../../strongs/h/h3423.md) them, and ['abad](../../strongs/h/h6.md) them [mahēr](../../strongs/h/h4118.md), as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) unto thee.

<a name="deuteronomy_9_4"></a>Deuteronomy 9:4

['āmar](../../strongs/h/h559.md) not thou in thine [lebab](../../strongs/h/h3824.md), after that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [hāḏap̄](../../strongs/h/h1920.md) them from [paniym](../../strongs/h/h6440.md) thee, ['āmar](../../strongs/h/h559.md), For my [ṣĕdāqāh](../../strongs/h/h6666.md) [Yĕhovah](../../strongs/h/h3068.md) hath [bow'](../../strongs/h/h935.md) me in to [yarash](../../strongs/h/h3423.md) this ['erets](../../strongs/h/h776.md): but for the [rišʿâ](../../strongs/h/h7564.md) of these [gowy](../../strongs/h/h1471.md) [Yĕhovah](../../strongs/h/h3068.md) doth [yarash](../../strongs/h/h3423.md) them from [paniym](../../strongs/h/h6440.md) thee.

<a name="deuteronomy_9_5"></a>Deuteronomy 9:5

Not for thy [ṣĕdāqāh](../../strongs/h/h6666.md), or for the [yōšer](../../strongs/h/h3476.md) of thine [lebab](../../strongs/h/h3824.md), dost thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) their ['erets](../../strongs/h/h776.md): but for the [rišʿâ](../../strongs/h/h7564.md) of these [gowy](../../strongs/h/h1471.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) doth [yarash](../../strongs/h/h3423.md) them from [paniym](../../strongs/h/h6440.md) thee, and that he may [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Ya'aqob](../../strongs/h/h3290.md).

<a name="deuteronomy_9_6"></a>Deuteronomy 9:6

[yada'](../../strongs/h/h3045.md) therefore, that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee not this [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) to [yarash](../../strongs/h/h3423.md) it for thy [ṣĕdāqāh](../../strongs/h/h6666.md); for thou art an [ʿōrep̄](../../strongs/h/h6203.md) [qāšê](../../strongs/h/h7186.md) ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_9_7"></a>Deuteronomy 9:7

[zakar](../../strongs/h/h2142.md), and [shakach](../../strongs/h/h7911.md) not, how thou provokedst [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [qāṣap̄](../../strongs/h/h7107.md) in the [midbar](../../strongs/h/h4057.md): from the [yowm](../../strongs/h/h3117.md) that thou didst [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), until ye [bow'](../../strongs/h/h935.md) unto this [maqowm](../../strongs/h/h4725.md), ye have been [marah](../../strongs/h/h4784.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_9_8"></a>Deuteronomy 9:8

Also in [Hōrēḇ](../../strongs/h/h2722.md) ye [qāṣap̄](../../strongs/h/h7107.md) [Yĕhovah](../../strongs/h/h3068.md), so that [Yĕhovah](../../strongs/h/h3068.md) was ['anaph](../../strongs/h/h599.md) with you to have [šāmaḏ](../../strongs/h/h8045.md) you.

<a name="deuteronomy_9_9"></a>Deuteronomy 9:9

When I was [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md) to [laqach](../../strongs/h/h3947.md) the [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), even the [lûaḥ](../../strongs/h/h3871.md) of the [bĕriyth](../../strongs/h/h1285.md) which [Yĕhovah](../../strongs/h/h3068.md) [karath](../../strongs/h/h3772.md) with you, then I [yashab](../../strongs/h/h3427.md) in the [har](../../strongs/h/h2022.md) forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md), I neither did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md):

<a name="deuteronomy_9_10"></a>Deuteronomy 9:10

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) unto me two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md) [kāṯaḇ](../../strongs/h/h3789.md) with the ['etsba'](../../strongs/h/h676.md) of ['Elohiym](../../strongs/h/h430.md); and on them was written according to all the [dabar](../../strongs/h/h1697.md), which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) with you in the [har](../../strongs/h/h2022.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md) in the [yowm](../../strongs/h/h3117.md) of the [qāhēl](../../strongs/h/h6951.md).

<a name="deuteronomy_9_11"></a>Deuteronomy 9:11

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md), that [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) me the two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), even the [lûaḥ](../../strongs/h/h3871.md) of the [bĕriyth](../../strongs/h/h1285.md).

<a name="deuteronomy_9_12"></a>Deuteronomy 9:12

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [quwm](../../strongs/h/h6965.md), [yarad](../../strongs/h/h3381.md) thee [mahēr](../../strongs/h/h4118.md) from hence; for thy ['am](../../strongs/h/h5971.md) which thou hast [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md) have [shachath](../../strongs/h/h7843.md) themselves; they are [mahēr](../../strongs/h/h4118.md) [cuwr](../../strongs/h/h5493.md) out of the [derek](../../strongs/h/h1870.md) which I [tsavah](../../strongs/h/h6680.md) them; they have ['asah](../../strongs/h/h6213.md) them a [massēḵâ](../../strongs/h/h4541.md).

<a name="deuteronomy_9_13"></a>Deuteronomy 9:13

Furthermore [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md), I have [ra'ah](../../strongs/h/h7200.md) this ['am](../../strongs/h/h5971.md), and, behold, it is a [ʿōrep̄](../../strongs/h/h6203.md) [qāšê](../../strongs/h/h7186.md) ['am](../../strongs/h/h5971.md):

<a name="deuteronomy_9_14"></a>Deuteronomy 9:14

[rāp̄â](../../strongs/h/h7503.md), that I may [šāmaḏ](../../strongs/h/h8045.md) them, and [māḥâ](../../strongs/h/h4229.md) their [shem](../../strongs/h/h8034.md) from under [shamayim](../../strongs/h/h8064.md): and I will ['asah](../../strongs/h/h6213.md) of thee a [gowy](../../strongs/h/h1471.md) ['atsuwm](../../strongs/h/h6099.md) and [rab](../../strongs/h/h7227.md) than they.

<a name="deuteronomy_9_15"></a>Deuteronomy 9:15

So I [panah](../../strongs/h/h6437.md) and [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md), and the [har](../../strongs/h/h2022.md) [bāʿar](../../strongs/h/h1197.md) with ['esh](../../strongs/h/h784.md): and the two [lûaḥ](../../strongs/h/h3871.md) of the [bĕriyth](../../strongs/h/h1285.md) were in my two [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_9_16"></a>Deuteronomy 9:16

And I [ra'ah](../../strongs/h/h7200.md), and, behold, ye had [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and had ['asah](../../strongs/h/h6213.md) you a [massēḵâ](../../strongs/h/h4541.md) [ʿēḡel](../../strongs/h/h5695.md): ye had [cuwr](../../strongs/h/h5493.md) [mahēr](../../strongs/h/h4118.md) out of the [derek](../../strongs/h/h1870.md) which [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) you.

<a name="deuteronomy_9_17"></a>Deuteronomy 9:17

And I [tāp̄aś](../../strongs/h/h8610.md) the two [lûaḥ](../../strongs/h/h3871.md), and [shalak](../../strongs/h/h7993.md) them out of my two [yad](../../strongs/h/h3027.md), and [shabar](../../strongs/h/h7665.md) them before your ['ayin](../../strongs/h/h5869.md).

<a name="deuteronomy_9_18"></a>Deuteronomy 9:18

And I [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), as at the [ri'šôn](../../strongs/h/h7223.md), forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md): I did neither ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md), because of all your [chatta'ath](../../strongs/h/h2403.md) which ye [chata'](../../strongs/h/h2398.md), in ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), to [kāʿas](../../strongs/h/h3707.md) him.

<a name="deuteronomy_9_19"></a>Deuteronomy 9:19

For I was [yāḡōr](../../strongs/h/h3025.md) [paniym](../../strongs/h/h6440.md) the ['aph](../../strongs/h/h639.md) and [chemah](../../strongs/h/h2534.md), wherewith [Yĕhovah](../../strongs/h/h3068.md) was [qāṣap̄](../../strongs/h/h7107.md) against you to [šāmaḏ](../../strongs/h/h8045.md) you. But [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) unto me at that [pa'am](../../strongs/h/h6471.md) also.

<a name="deuteronomy_9_20"></a>Deuteronomy 9:20

And [Yĕhovah](../../strongs/h/h3068.md) was [me'od](../../strongs/h/h3966.md) ['anaph](../../strongs/h/h599.md) with ['Ahărôn](../../strongs/h/h175.md) to have [šāmaḏ](../../strongs/h/h8045.md) him: and I [palal](../../strongs/h/h6419.md) for ['Ahărôn](../../strongs/h/h175.md) also the same [ʿēṯ](../../strongs/h/h6256.md).

<a name="deuteronomy_9_21"></a>Deuteronomy 9:21

And I [laqach](../../strongs/h/h3947.md) your [chatta'ath](../../strongs/h/h2403.md), the [ʿēḡel](../../strongs/h/h5695.md) which ye had ['asah](../../strongs/h/h6213.md), and [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md), and [kāṯaṯ](../../strongs/h/h3807.md) it, and [ṭāḥan](../../strongs/h/h2912.md) it [yatab](../../strongs/h/h3190.md), even until it was as [dāqaq](../../strongs/h/h1854.md) as ['aphar](../../strongs/h/h6083.md): and I [shalak](../../strongs/h/h7993.md) the ['aphar](../../strongs/h/h6083.md) thereof into the [nachal](../../strongs/h/h5158.md) that [yarad](../../strongs/h/h3381.md) of the [har](../../strongs/h/h2022.md).

<a name="deuteronomy_9_22"></a>Deuteronomy 9:22

And at [Taḇʿērâ](../../strongs/h/h8404.md), and at [massâ](../../strongs/h/h4532.md), and at [Qiḇrôṯ hata'ăvâ](../../strongs/h/h6914.md), ye provoked [Yĕhovah](../../strongs/h/h3068.md) [qāṣap̄](../../strongs/h/h7107.md).

<a name="deuteronomy_9_23"></a>Deuteronomy 9:23

Likewise when [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) you from [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md), ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which I have [nathan](../../strongs/h/h5414.md) you; then ye [marah](../../strongs/h/h4784.md) against the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and ye ['aman](../../strongs/h/h539.md) him not, nor [shama'](../../strongs/h/h8085.md) to his [qowl](../../strongs/h/h6963.md).

<a name="deuteronomy_9_24"></a>Deuteronomy 9:24

Ye have been [marah](../../strongs/h/h4784.md) against [Yĕhovah](../../strongs/h/h3068.md) from the [yowm](../../strongs/h/h3117.md) that I [yada'](../../strongs/h/h3045.md) you.

<a name="deuteronomy_9_25"></a>Deuteronomy 9:25

Thus I [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md), as I [naphal](../../strongs/h/h5307.md); because [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md) he would [šāmaḏ](../../strongs/h/h8045.md) you.

<a name="deuteronomy_9_26"></a>Deuteronomy 9:26

I [palal](../../strongs/h/h6419.md) therefore unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [shachath](../../strongs/h/h7843.md) not thy ['am](../../strongs/h/h5971.md) and thine [nachalah](../../strongs/h/h5159.md), which thou hast [pāḏâ](../../strongs/h/h6299.md) through thy [gōḏel](../../strongs/h/h1433.md), which thou hast [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md) with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_9_27"></a>Deuteronomy 9:27

[zakar](../../strongs/h/h2142.md) thy ['ebed](../../strongs/h/h5650.md), ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Ya'aqob](../../strongs/h/h3290.md); [panah](../../strongs/h/h6437.md) not unto the [qᵊšî](../../strongs/h/h7190.md) of this ['am](../../strongs/h/h5971.md), nor to their [resha'](../../strongs/h/h7562.md), nor to their [chatta'ath](../../strongs/h/h2403.md):

<a name="deuteronomy_9_28"></a>Deuteronomy 9:28

Lest the ['erets](../../strongs/h/h776.md) whence thou [yāṣā'](../../strongs/h/h3318.md) us ['āmar](../../strongs/h/h559.md), Because [Yĕhovah](../../strongs/h/h3068.md) was not [yakol](../../strongs/h/h3201.md) to [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md) which he [dabar](../../strongs/h/h1696.md) them, and because he [śin'â](../../strongs/h/h8135.md) them, he hath [yāṣā'](../../strongs/h/h3318.md) them to [muwth](../../strongs/h/h4191.md) them in the [midbar](../../strongs/h/h4057.md).

<a name="deuteronomy_9_29"></a>Deuteronomy 9:29

Yet they are thy ['am](../../strongs/h/h5971.md) and thine [nachalah](../../strongs/h/h5159.md), which thou [yāṣā'](../../strongs/h/h3318.md) by thy [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) and by thy [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 8](deuteronomy_8.md) - [Deuteronomy 10](deuteronomy_10.md)