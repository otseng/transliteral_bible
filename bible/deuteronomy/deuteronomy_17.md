# [Deuteronomy 17](https://www.blueletterbible.org/esv/deu/17)

<a name="deuteronomy_17_1"></a>Deuteronomy 17:1

Thou shalt not [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) any [showr](../../strongs/h/h7794.md), or [śê](../../strongs/h/h7716.md), wherein is [mᵊ'ûm](../../strongs/h/h3971.md), or any [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md): for that is a [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_17_2"></a>Deuteronomy 17:2

If there be [māṣā'](../../strongs/h/h4672.md) [qereḇ](../../strongs/h/h7130.md) you, within any of thy [sha'ar](../../strongs/h/h8179.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md), that hath ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), in ['abar](../../strongs/h/h5674.md) his [bĕriyth](../../strongs/h/h1285.md),


<a name="deuteronomy_17_3"></a>Deuteronomy 17:3

And hath [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them, either the [šemeš](../../strongs/h/h8121.md), or [yareach](../../strongs/h/h3394.md), or any of the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), which I have not [tsavah](../../strongs/h/h6680.md);

<a name="deuteronomy_17_4"></a>Deuteronomy 17:4

And it be [nāḡaḏ](../../strongs/h/h5046.md) thee, and thou hast [shama'](../../strongs/h/h8085.md) of it, and [darash](../../strongs/h/h1875.md) [yatab](../../strongs/h/h3190.md), and, behold, it be ['emeth](../../strongs/h/h571.md), and the [dabar](../../strongs/h/h1697.md) [kuwn](../../strongs/h/h3559.md), that such [tôʿēḇâ](../../strongs/h/h8441.md) is ['asah](../../strongs/h/h6213.md) in [Yisra'el](../../strongs/h/h3478.md):

<a name="deuteronomy_17_5"></a>Deuteronomy 17:5

Then shalt thou [yāṣā'](../../strongs/h/h3318.md) that ['iysh](../../strongs/h/h376.md) or that ['ishshah](../../strongs/h/h802.md), which have ['asah](../../strongs/h/h6213.md) that [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md), unto thy [sha'ar](../../strongs/h/h8179.md), even that ['iysh](../../strongs/h/h376.md) or that ['ishshah](../../strongs/h/h802.md), and shalt [sāqal](../../strongs/h/h5619.md) them with ['eben](../../strongs/h/h68.md), till they [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_17_6"></a>Deuteronomy 17:6

At the [peh](../../strongs/h/h6310.md) of two ['ed](../../strongs/h/h5707.md), or three ['ed](../../strongs/h/h5707.md), shall he that is worthy of [muwth](../../strongs/h/h4191.md) be [muwth](../../strongs/h/h4191.md); but at the [peh](../../strongs/h/h6310.md) of one ['ed](../../strongs/h/h5707.md) he shall not be [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_17_7"></a>Deuteronomy 17:7

The [yad](../../strongs/h/h3027.md) of the ['ed](../../strongs/h/h5707.md) shall be [ri'šôn](../../strongs/h/h7223.md) upon him to put him to [muwth](../../strongs/h/h4191.md), and ['aḥărôn](../../strongs/h/h314.md) the [yad](../../strongs/h/h3027.md) of all the ['am](../../strongs/h/h5971.md). So thou shalt [bāʿar](../../strongs/h/h1197.md) the [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_17_8"></a>Deuteronomy 17:8

If there arise a [dabar](../../strongs/h/h1697.md) too [pala'](../../strongs/h/h6381.md) for thee in [mishpat](../../strongs/h/h4941.md), between [dam](../../strongs/h/h1818.md) and [dam](../../strongs/h/h1818.md), between [diyn](../../strongs/h/h1779.md) and [diyn](../../strongs/h/h1779.md), and between [neḡaʿ](../../strongs/h/h5061.md) and [neḡaʿ](../../strongs/h/h5061.md), being [dabar](../../strongs/h/h1697.md) of [rîḇ](../../strongs/h/h7379.md) within thy [sha'ar](../../strongs/h/h8179.md): then shalt thou [quwm](../../strongs/h/h6965.md), and [ʿālâ](../../strongs/h/h5927.md) thee into the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md);

<a name="deuteronomy_17_9"></a>Deuteronomy 17:9

And thou shalt [bow'](../../strongs/h/h935.md) unto the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md), and unto the [shaphat](../../strongs/h/h8199.md) that shall be in those [yowm](../../strongs/h/h3117.md), and [darash](../../strongs/h/h1875.md); and they shall [nāḡaḏ](../../strongs/h/h5046.md) thee the [dabar](../../strongs/h/h1697.md) of [mishpat](../../strongs/h/h4941.md):

<a name="deuteronomy_17_10"></a>Deuteronomy 17:10

And thou shalt ['asah](../../strongs/h/h6213.md) according to the [peh](../../strongs/h/h6310.md) [dabar](../../strongs/h/h1697.md), which they of that [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md) shall [nāḡaḏ](../../strongs/h/h5046.md) thee; and thou shalt [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) according to all that they [yārâ](../../strongs/h/h3384.md) thee:

<a name="deuteronomy_17_11"></a>Deuteronomy 17:11

According to the [peh](../../strongs/h/h6310.md) of the [towrah](../../strongs/h/h8451.md) which they shall [yārâ](../../strongs/h/h3384.md) thee, and according to the [mishpat](../../strongs/h/h4941.md) which they shall ['āmar](../../strongs/h/h559.md) thee, thou shalt ['asah](../../strongs/h/h6213.md): thou shalt not [cuwr](../../strongs/h/h5493.md) from the [dabar](../../strongs/h/h1697.md) which they shall [nāḡaḏ](../../strongs/h/h5046.md) thee, to the [yamiyn](../../strongs/h/h3225.md), nor to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="deuteronomy_17_12"></a>Deuteronomy 17:12

And the ['iysh](../../strongs/h/h376.md) that will ['asah](../../strongs/h/h6213.md) [zāḏôn](../../strongs/h/h2087.md), and will not [shama'](../../strongs/h/h8085.md) unto the [kōhēn](../../strongs/h/h3548.md) that ['amad](../../strongs/h/h5975.md) to [sharath](../../strongs/h/h8334.md) there before [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), or unto the [shaphat](../../strongs/h/h8199.md), even that ['iysh](../../strongs/h/h376.md) shall [muwth](../../strongs/h/h4191.md): and thou shalt [bāʿar](../../strongs/h/h1197.md) the [ra'](../../strongs/h/h7451.md) from [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_17_13"></a>Deuteronomy 17:13

And all the ['am](../../strongs/h/h5971.md) shall [shama'](../../strongs/h/h8085.md), and [yare'](../../strongs/h/h3372.md), and do no more [zûḏ](../../strongs/h/h2102.md).

<a name="deuteronomy_17_14"></a>Deuteronomy 17:14

When thou art [bow'](../../strongs/h/h935.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, and shalt [yarash](../../strongs/h/h3423.md) it, and shalt [yashab](../../strongs/h/h3427.md) therein, and shalt ['āmar](../../strongs/h/h559.md), I will [śûm](../../strongs/h/h7760.md) a [melek](../../strongs/h/h4428.md) over me, like as all the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) me;

<a name="deuteronomy_17_15"></a>Deuteronomy 17:15

Thou shalt [śûm](../../strongs/h/h7760.md) [śûm](../../strongs/h/h7760.md) him [melek](../../strongs/h/h4428.md) over thee, whom [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md): one from [qereḇ](../../strongs/h/h7130.md) thy ['ach](../../strongs/h/h251.md) shalt thou [śûm](../../strongs/h/h7760.md) [melek](../../strongs/h/h4428.md) over thee: thou [yakol](../../strongs/h/h3201.md) not [nathan](../../strongs/h/h5414.md) a [nāḵrî](../../strongs/h/h5237.md) ['iysh](../../strongs/h/h376.md) over thee, which is not thy ['ach](../../strongs/h/h251.md).

<a name="deuteronomy_17_16"></a>Deuteronomy 17:16

But he shall not [rabah](../../strongs/h/h7235.md) [sûs](../../strongs/h/h5483.md) to himself, nor cause the ['am](../../strongs/h/h5971.md) to [shuwb](../../strongs/h/h7725.md) to [Mitsrayim](../../strongs/h/h4714.md), to the end that he should [rabah](../../strongs/h/h7235.md) [sûs](../../strongs/h/h5483.md): forasmuch as [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) unto you, Ye shall [yāsap̄](../../strongs/h/h3254.md) [shuwb](../../strongs/h/h7725.md) no more that [derek](../../strongs/h/h1870.md).

<a name="deuteronomy_17_17"></a>Deuteronomy 17:17

Neither shall he [rabah](../../strongs/h/h7235.md) ['ishshah](../../strongs/h/h802.md) to himself, that his [lebab](../../strongs/h/h3824.md) [cuwr](../../strongs/h/h5493.md) not away: neither shall he [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) to himself [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md).

<a name="deuteronomy_17_18"></a>Deuteronomy 17:18

And it shall be, when he [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of his [mamlāḵâ](../../strongs/h/h4467.md), that he shall [kāṯaḇ](../../strongs/h/h3789.md) him a [mišnê](../../strongs/h/h4932.md) of this [towrah](../../strongs/h/h8451.md) in a [sēp̄er](../../strongs/h/h5612.md) out of that which is [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md):

<a name="deuteronomy_17_19"></a>Deuteronomy 17:19

And it shall be with him, and he shall [qara'](../../strongs/h/h7121.md) therein all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md): that he may [lamad](../../strongs/h/h3925.md) to [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md) and these [choq](../../strongs/h/h2706.md), to ['asah](../../strongs/h/h6213.md) them:

<a name="deuteronomy_17_20"></a>Deuteronomy 17:20

That his [lebab](../../strongs/h/h3824.md) be not [ruwm](../../strongs/h/h7311.md) above his ['ach](../../strongs/h/h251.md), and that he not [cuwr](../../strongs/h/h5493.md) from the [mitsvah](../../strongs/h/h4687.md), to the [yamiyn](../../strongs/h/h3225.md), or to the [śᵊmō'l](../../strongs/h/h8040.md): to the end that he may ['arak](../../strongs/h/h748.md) his [yowm](../../strongs/h/h3117.md) in his [mamlāḵâ](../../strongs/h/h4467.md), he, and his [ben](../../strongs/h/h1121.md), in the [qereḇ](../../strongs/h/h7130.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 16](deuteronomy_16.md) - [Deuteronomy 18](deuteronomy_18.md)