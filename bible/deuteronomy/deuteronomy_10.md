# [Deuteronomy 10](https://www.blueletterbible.org/esv/deu/10)

<a name="deuteronomy_10_1"></a>Deuteronomy 10:1

At that [ʿēṯ](../../strongs/h/h6256.md) [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [pāsal](../../strongs/h/h6458.md) thee two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md) like unto the [ri'šôn](../../strongs/h/h7223.md), and [ʿālâ](../../strongs/h/h5927.md) unto me into the [har](../../strongs/h/h2022.md), and ['asah](../../strongs/h/h6213.md) thee an ['ārôn](../../strongs/h/h727.md) of ['ets](../../strongs/h/h6086.md).

<a name="deuteronomy_10_2"></a>Deuteronomy 10:2

And I will [kāṯaḇ](../../strongs/h/h3789.md) on the [lûaḥ](../../strongs/h/h3871.md) the [dabar](../../strongs/h/h1697.md) that were in the [ri'šôn](../../strongs/h/h7223.md) [lûaḥ](../../strongs/h/h3871.md) which thou [shabar](../../strongs/h/h7665.md), and thou shalt [śûm](../../strongs/h/h7760.md) them in the ['ārôn](../../strongs/h/h727.md).

<a name="deuteronomy_10_3"></a>Deuteronomy 10:3

And I ['asah](../../strongs/h/h6213.md) an ['ārôn](../../strongs/h/h727.md) of [šiṭṭâ](../../strongs/h/h7848.md) ['ets](../../strongs/h/h6086.md), and [pāsal](../../strongs/h/h6458.md) two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md) like unto the [ri'šôn](../../strongs/h/h7223.md), and [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md), having the two [lûaḥ](../../strongs/h/h3871.md) in mine [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_10_4"></a>Deuteronomy 10:4

And he [kāṯaḇ](../../strongs/h/h3789.md) on the [lûaḥ](../../strongs/h/h3871.md), according to the [ri'šôn](../../strongs/h/h7223.md) [miḵtāḇ](../../strongs/h/h4385.md), the [ʿeśer](../../strongs/h/h6235.md) [dabar](../../strongs/h/h1697.md), which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto you in the [har](../../strongs/h/h2022.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md) in the [yowm](../../strongs/h/h3117.md) of the [qāhēl](../../strongs/h/h6951.md): and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them unto me.

<a name="deuteronomy_10_5"></a>Deuteronomy 10:5

And I [panah](../../strongs/h/h6437.md) myself and [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md), and [śûm](../../strongs/h/h7760.md) the [lûaḥ](../../strongs/h/h3871.md) in the ['ārôn](../../strongs/h/h727.md) which I had ['asah](../../strongs/h/h6213.md); and there they be, as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) me.

<a name="deuteronomy_10_6"></a>Deuteronomy 10:6

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md) from [Bᵊ'Ērôṯ Bᵊnê YaʿĂqān](../../strongs/h/h885.md) to [Môsērâ](../../strongs/h/h4149.md): there ['Ahărôn](../../strongs/h/h175.md) [muwth](../../strongs/h/h4191.md), and there he was [qāḇar](../../strongs/h/h6912.md); and ['Elʿāzār](../../strongs/h/h499.md) his [ben](../../strongs/h/h1121.md) [kāhan](../../strongs/h/h3547.md) in his stead.

<a name="deuteronomy_10_7"></a>Deuteronomy 10:7

From thence they [nāsaʿ](../../strongs/h/h5265.md) unto [Guḏgōḏâ](../../strongs/h/h1412.md); and from [Guḏgōḏâ](../../strongs/h/h1412.md) to [Yāṭḇāṯâ](../../strongs/h/h3193.md), an ['erets](../../strongs/h/h776.md) of [nachal](../../strongs/h/h5158.md) of [mayim](../../strongs/h/h4325.md).

<a name="deuteronomy_10_8"></a>Deuteronomy 10:8

At that [ʿēṯ](../../strongs/h/h6256.md) [Yĕhovah](../../strongs/h/h3068.md) [bāḏal](../../strongs/h/h914.md) the [shebet](../../strongs/h/h7626.md) of [Lēvî](../../strongs/h/h3878.md), to [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) to [sharath](../../strongs/h/h8334.md) unto him, and to [barak](../../strongs/h/h1288.md) in his [shem](../../strongs/h/h8034.md), unto this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_10_9"></a>Deuteronomy 10:9

Wherefore [Lēvî](../../strongs/h/h3878.md) hath no [cheleq](../../strongs/h/h2506.md) nor [nachalah](../../strongs/h/h5159.md) with his ['ach](../../strongs/h/h251.md); [Yĕhovah](../../strongs/h/h3068.md) is his [nachalah](../../strongs/h/h5159.md), according as [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) him.

<a name="deuteronomy_10_10"></a>Deuteronomy 10:10

And I ['amad](../../strongs/h/h5975.md) in the [har](../../strongs/h/h2022.md), according to the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md), forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md); and [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) unto me at that [pa'am](../../strongs/h/h6471.md) also, and [Yĕhovah](../../strongs/h/h3068.md) ['āḇâ](../../strongs/h/h14.md) not [shachath](../../strongs/h/h7843.md) thee.

<a name="deuteronomy_10_11"></a>Deuteronomy 10:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) thy [massāʿ](../../strongs/h/h4550.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md), that they may [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), which I [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) unto them.

<a name="deuteronomy_10_12"></a>Deuteronomy 10:12

And now, [Yisra'el](../../strongs/h/h3478.md), what doth [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [sha'al](../../strongs/h/h7592.md) of thee, but to [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in all his [derek](../../strongs/h/h1870.md), and to ['ahab](../../strongs/h/h157.md) him, and to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) with all thy [lebab](../../strongs/h/h3824.md) and with all thy [nephesh](../../strongs/h/h5315.md),

<a name="deuteronomy_10_13"></a>Deuteronomy 10:13

To [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), and his [chuqqah](../../strongs/h/h2708.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md) for thy [towb](../../strongs/h/h2896.md)?

<a name="deuteronomy_10_14"></a>Deuteronomy 10:14

Behold, the [shamayim](../../strongs/h/h8064.md) and the [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md) is [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), the ['erets](../../strongs/h/h776.md) also, with all that therein is.

<a name="deuteronomy_10_15"></a>Deuteronomy 10:15

Only [Yĕhovah](../../strongs/h/h3068.md) had a [ḥāšaq](../../strongs/h/h2836.md) in thy ['ab](../../strongs/h/h1.md) to ['ahab](../../strongs/h/h157.md) them, and he [bāḥar](../../strongs/h/h977.md) their [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) them, even you above all ['am](../../strongs/h/h5971.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_10_16"></a>Deuteronomy 10:16

[muwl](../../strongs/h/h4135.md) therefore the [ʿārlâ](../../strongs/h/h6190.md) of your [lebab](../../strongs/h/h3824.md), and be no more [ʿōrep̄](../../strongs/h/h6203.md) [qāšâ](../../strongs/h/h7185.md).

<a name="deuteronomy_10_17"></a>Deuteronomy 10:17

For [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) is ['Elohiym](../../strongs/h/h430.md) of ['Elohiym](../../strongs/h/h430.md), and ['adown](../../strongs/h/h113.md) of ['adown](../../strongs/h/h113.md), a [gadowl](../../strongs/h/h1419.md) ['el](../../strongs/h/h410.md), a [gibôr](../../strongs/h/h1368.md), and a [yare'](../../strongs/h/h3372.md), which [nasa'](../../strongs/h/h5375.md) not [paniym](../../strongs/h/h6440.md), nor [laqach](../../strongs/h/h3947.md) [shachad](../../strongs/h/h7810.md):

<a name="deuteronomy_10_18"></a>Deuteronomy 10:18

He doth ['asah](../../strongs/h/h6213.md) the [mishpat](../../strongs/h/h4941.md) of the [yathowm](../../strongs/h/h3490.md) and ['almānâ](../../strongs/h/h490.md), and ['ahab](../../strongs/h/h157.md) the [ger](../../strongs/h/h1616.md), in [nathan](../../strongs/h/h5414.md) him [lechem](../../strongs/h/h3899.md) and [śimlâ](../../strongs/h/h8071.md).

<a name="deuteronomy_10_19"></a>Deuteronomy 10:19

['ahab](../../strongs/h/h157.md) ye therefore the [ger](../../strongs/h/h1616.md): for ye were [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_10_20"></a>Deuteronomy 10:20

Thou shalt [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md); him shalt thou ['abad](../../strongs/h/h5647.md), and to him shalt thou [dāḇaq](../../strongs/h/h1692.md), and [shaba'](../../strongs/h/h7650.md) by his [shem](../../strongs/h/h8034.md).

<a name="deuteronomy_10_21"></a>Deuteronomy 10:21

He is thy [tehillah](../../strongs/h/h8416.md), and he is thy ['Elohiym](../../strongs/h/h430.md), that hath ['asah](../../strongs/h/h6213.md) for thee these [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md), which thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md).

<a name="deuteronomy_10_22"></a>Deuteronomy 10:22

Thy ['ab](../../strongs/h/h1.md) [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md) with threescore and ten [nephesh](../../strongs/h/h5315.md); and now [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [śûm](../../strongs/h/h7760.md) thee as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md) for [rōḇ](../../strongs/h/h7230.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 9](deuteronomy_9.md) - [Deuteronomy 11](deuteronomy_11.md)