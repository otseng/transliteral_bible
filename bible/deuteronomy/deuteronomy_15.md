# [Deuteronomy 15](https://www.blueletterbible.org/esv/deu/15)

<a name="deuteronomy_15_1"></a>Deuteronomy 15:1

At the [qēṣ](../../strongs/h/h7093.md) of every seven [šānâ](../../strongs/h/h8141.md) thou shalt ['asah](../../strongs/h/h6213.md) a [šᵊmiṭṭâ](../../strongs/h/h8059.md).

<a name="deuteronomy_15_2"></a>Deuteronomy 15:2

And this is the [dabar](../../strongs/h/h1697.md) of the [šᵊmiṭṭâ](../../strongs/h/h8059.md): Every [baʿal](../../strongs/h/h1167.md) [maššê](../../strongs/h/h4874.md) [yad](../../strongs/h/h3027.md) that [nāšâ](../../strongs/h/h5383.md) ought unto his [rea'](../../strongs/h/h7453.md) shall [šāmaṭ](../../strongs/h/h8058.md) it; he shall not [nāḡaś](../../strongs/h/h5065.md) it of his [rea'](../../strongs/h/h7453.md), or of his ['ach](../../strongs/h/h251.md); because it is [qara'](../../strongs/h/h7121.md) [Yĕhovah](../../strongs/h/h3068.md) [šᵊmiṭṭâ](../../strongs/h/h8059.md).

<a name="deuteronomy_15_3"></a>Deuteronomy 15:3

Of a [nāḵrî](../../strongs/h/h5237.md) thou mayest [nāḡaś](../../strongs/h/h5065.md) it again: but that which is thine with thy ['ach](../../strongs/h/h251.md) thine [yad](../../strongs/h/h3027.md) shall [šāmaṭ](../../strongs/h/h8058.md);

<a name="deuteronomy_15_4"></a>Deuteronomy 15:4

['ep̄es](../../strongs/h/h657.md) when there shall be no ['ebyown](../../strongs/h/h34.md) among you; for [Yĕhovah](../../strongs/h/h3068.md) shall [barak](../../strongs/h/h1288.md) [barak](../../strongs/h/h1288.md) thee in the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md) to [yarash](../../strongs/h/h3423.md) it:

<a name="deuteronomy_15_5"></a>Deuteronomy 15:5

Only if thou [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all these [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_15_6"></a>Deuteronomy 15:6

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) thee, as he [dabar](../../strongs/h/h1696.md) thee: and thou shalt [ʿāḇaṭ](../../strongs/h/h5670.md) unto [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), but thou shalt not [ʿāḇaṭ](../../strongs/h/h5670.md); and thou shalt [mashal](../../strongs/h/h4910.md) over [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), but they shall not [mashal](../../strongs/h/h4910.md) over thee.

<a name="deuteronomy_15_7"></a>Deuteronomy 15:7

If there be among you an ['ebyown](../../strongs/h/h34.md) of one of thy ['ach](../../strongs/h/h251.md) within any of thy [sha'ar](../../strongs/h/h8179.md) in thy ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, thou shalt not ['amats](../../strongs/h/h553.md) thine [lebab](../../strongs/h/h3824.md), nor [qāp̄aṣ](../../strongs/h/h7092.md) thine [yad](../../strongs/h/h3027.md) from thy ['ebyown](../../strongs/h/h34.md) ['ach](../../strongs/h/h251.md):

<a name="deuteronomy_15_8"></a>Deuteronomy 15:8

But thou shalt [pāṯaḥ](../../strongs/h/h6605.md) thine [yad](../../strongs/h/h3027.md) [pāṯaḥ](../../strongs/h/h6605.md) unto him, and shalt [ʿāḇaṭ](../../strongs/h/h5670.md) [ʿāḇaṭ](../../strongs/h/h5670.md) him [day](../../strongs/h/h1767.md) for his [maḥsôr](../../strongs/h/h4270.md), in that which he [ḥāsēr](../../strongs/h/h2637.md).

<a name="deuteronomy_15_9"></a>Deuteronomy 15:9

[shamar](../../strongs/h/h8104.md) that there be not a [dabar](../../strongs/h/h1697.md) in thy [beliya'al](../../strongs/h/h1100.md) [lebab](../../strongs/h/h3824.md), ['āmar](../../strongs/h/h559.md), The seventh [šānâ](../../strongs/h/h8141.md), the [šānâ](../../strongs/h/h8141.md) of [šᵊmiṭṭâ](../../strongs/h/h8059.md), is [qāraḇ](../../strongs/h/h7126.md); and thine ['ayin](../../strongs/h/h5869.md) be [ra'a'](../../strongs/h/h7489.md) against thy ['ebyown](../../strongs/h/h34.md) ['ach](../../strongs/h/h251.md), and thou [nathan](../../strongs/h/h5414.md) him nought; and he [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md) against thee, and it be [ḥēṭĕ'](../../strongs/h/h2399.md) unto thee.

<a name="deuteronomy_15_10"></a>Deuteronomy 15:10

Thou shalt [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) him, and thine [lebab](../../strongs/h/h3824.md) shall not be [yāraʿ](../../strongs/h/h3415.md) when thou [nathan](../../strongs/h/h5414.md) unto him: [gālāl](../../strongs/h/h1558.md) that for this [dabar](../../strongs/h/h1697.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [barak](../../strongs/h/h1288.md) thee in all thy [ma'aseh](../../strongs/h/h4639.md), and in all that thou [mišlôaḥ](../../strongs/h/h4916.md) thine [yad](../../strongs/h/h3027.md) unto.

<a name="deuteronomy_15_11"></a>Deuteronomy 15:11

For the ['ebyown](../../strongs/h/h34.md) shall never [ḥāḏal](../../strongs/h/h2308.md) [qereḇ](../../strongs/h/h7130.md) the ['erets](../../strongs/h/h776.md): therefore I [tsavah](../../strongs/h/h6680.md) thee, ['āmar](../../strongs/h/h559.md), Thou shalt [pāṯaḥ](../../strongs/h/h6605.md) thine [yad](../../strongs/h/h3027.md) unto thy ['ach](../../strongs/h/h251.md), to thy ['aniy](../../strongs/h/h6041.md), and to thy ['ebyown](../../strongs/h/h34.md), in thy ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_15_12"></a>Deuteronomy 15:12

And if thy ['ach](../../strongs/h/h251.md), an [ʿiḇrî](../../strongs/h/h5680.md), or an [ʿiḇrî](../../strongs/h/h5680.md), be [māḵar](../../strongs/h/h4376.md) unto thee, and ['abad](../../strongs/h/h5647.md) thee six [šānâ](../../strongs/h/h8141.md); then in the seventh [šānâ](../../strongs/h/h8141.md) thou shalt let him [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md) from thee.

<a name="deuteronomy_15_13"></a>Deuteronomy 15:13

And when thou [shalach](../../strongs/h/h7971.md) him out [ḥāp̄šî](../../strongs/h/h2670.md) from thee, thou shalt not let him [shalach](../../strongs/h/h7971.md) [rêqām](../../strongs/h/h7387.md):

<a name="deuteronomy_15_14"></a>Deuteronomy 15:14

Thou shalt [ʿānaq](../../strongs/h/h6059.md) [ʿānaq](../../strongs/h/h6059.md) out of thy [tso'n](../../strongs/h/h6629.md), and out of thy [gōren](../../strongs/h/h1637.md), and out of thy [yeqeḇ](../../strongs/h/h3342.md): of that wherewith [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee thou shalt [nathan](../../strongs/h/h5414.md) unto him.

<a name="deuteronomy_15_15"></a>Deuteronomy 15:15

And thou shalt [zakar](../../strongs/h/h2142.md) that thou wast an ['ebed](../../strongs/h/h5650.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [pāḏâ](../../strongs/h/h6299.md) thee: therefore I [tsavah](../../strongs/h/h6680.md) thee this [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_15_16"></a>Deuteronomy 15:16

And it shall be, if he ['āmar](../../strongs/h/h559.md) unto thee, I will not [yāṣā'](../../strongs/h/h3318.md) from thee; because he ['ahab](../../strongs/h/h157.md) thee and thine [bayith](../../strongs/h/h1004.md), because he is [ṭôḇ](../../strongs/h/h2895.md) with thee;

<a name="deuteronomy_15_17"></a>Deuteronomy 15:17

Then thou shalt [laqach](../../strongs/h/h3947.md) a [marṣēaʿ](../../strongs/h/h4836.md), and [nathan](../../strongs/h/h5414.md) it through his ['ozen](../../strongs/h/h241.md) unto the [deleṯ](../../strongs/h/h1817.md), and he shall be thy ['ebed](../../strongs/h/h5650.md) ['owlam](../../strongs/h/h5769.md). And also unto thy ['amah](../../strongs/h/h519.md) thou shalt ['asah](../../strongs/h/h6213.md) likewise.

<a name="deuteronomy_15_18"></a>Deuteronomy 15:18

It shall not seem [qāšâ](../../strongs/h/h7185.md) ['ayin](../../strongs/h/h5869.md), when thou [shalach](../../strongs/h/h7971.md) him [ḥāp̄šî](../../strongs/h/h2670.md) from thee; for he hath been [śāḵār](../../strongs/h/h7939.md) a [mišnê](../../strongs/h/h4932.md) [śāḵîr](../../strongs/h/h7916.md) to thee, in ['abad](../../strongs/h/h5647.md) thee six [šānâ](../../strongs/h/h8141.md): and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [barak](../../strongs/h/h1288.md) thee in all that thou ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_15_19"></a>Deuteronomy 15:19

All the [bᵊḵôr](../../strongs/h/h1060.md) [zāḵār](../../strongs/h/h2145.md) that [yalad](../../strongs/h/h3205.md) of thy [bāqār](../../strongs/h/h1241.md) and of thy [tso'n](../../strongs/h/h6629.md) thou shalt [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): thou shalt do no ['abad](../../strongs/h/h5647.md) with the [bᵊḵôr](../../strongs/h/h1060.md) of thy [showr](../../strongs/h/h7794.md), nor [gāzaz](../../strongs/h/h1494.md) the [bᵊḵôr](../../strongs/h/h1060.md) of thy [tso'n](../../strongs/h/h6629.md).

<a name="deuteronomy_15_20"></a>Deuteronomy 15:20

Thou shalt ['akal](../../strongs/h/h398.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md) in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md), thou and thy [bayith](../../strongs/h/h1004.md).

<a name="deuteronomy_15_21"></a>Deuteronomy 15:21

And if there be any [mᵊ'ûm](../../strongs/h/h3971.md) therein, as if it be [pissēaḥ](../../strongs/h/h6455.md), or [ʿiûēr](../../strongs/h/h5787.md), or have any [ra'](../../strongs/h/h7451.md) [mᵊ'ûm](../../strongs/h/h3971.md), thou shalt not [zabach](../../strongs/h/h2076.md) it unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_15_22"></a>Deuteronomy 15:22

Thou shalt ['akal](../../strongs/h/h398.md) it within thy [sha'ar](../../strongs/h/h8179.md): the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md) shall eat it [yaḥaḏ](../../strongs/h/h3162.md), as the [ṣᵊḇî](../../strongs/h/h6643.md), and as the ['ayyāl](../../strongs/h/h354.md).

<a name="deuteronomy_15_23"></a>Deuteronomy 15:23

Only thou shalt not ['akal](../../strongs/h/h398.md) the [dam](../../strongs/h/h1818.md) thereof; thou shalt [šāp̄aḵ](../../strongs/h/h8210.md) it upon the ['erets](../../strongs/h/h776.md) as [mayim](../../strongs/h/h4325.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 14](deuteronomy_14.md) - [Deuteronomy 16](deuteronomy_16.md)