# [Deuteronomy 29](https://www.blueletterbible.org/kjv/deu/29)

<a name="deuteronomy_29_1"></a>Deuteronomy 29:1

These are the [dabar](../../strongs/h/h1697.md) of the [bĕriyth](../../strongs/h/h1285.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) to [karath](../../strongs/h/h3772.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), beside the [bĕriyth](../../strongs/h/h1285.md) which he [karath](../../strongs/h/h3772.md) with them in [Hōrēḇ](../../strongs/h/h2722.md).

<a name="deuteronomy_29_2"></a>Deuteronomy 29:2

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) unto all [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Ye have [ra'ah](../../strongs/h/h7200.md) all that [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) before your ['ayin](../../strongs/h/h5869.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) unto [Parʿô](../../strongs/h/h6547.md), and unto all his ['ebed](../../strongs/h/h5650.md), and unto all his ['erets](../../strongs/h/h776.md);

<a name="deuteronomy_29_3"></a>Deuteronomy 29:3

The [gadowl](../../strongs/h/h1419.md) [massâ](../../strongs/h/h4531.md) which thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md), the ['ôṯ](../../strongs/h/h226.md), and those [gadowl](../../strongs/h/h1419.md) [môp̄ēṯ](../../strongs/h/h4159.md):

<a name="deuteronomy_29_4"></a>Deuteronomy 29:4

Yet [Yĕhovah](../../strongs/h/h3068.md) hath not [nathan](../../strongs/h/h5414.md) you a [leb](../../strongs/h/h3820.md) to [yada'](../../strongs/h/h3045.md), and ['ayin](../../strongs/h/h5869.md) to [ra'ah](../../strongs/h/h7200.md), and ['ozen](../../strongs/h/h241.md) to [shama'](../../strongs/h/h8085.md), unto this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_29_5"></a>Deuteronomy 29:5

And I have [yālaḵ](../../strongs/h/h3212.md) you forty [šānâ](../../strongs/h/h8141.md) in the [midbar](../../strongs/h/h4057.md): your [śalmâ](../../strongs/h/h8008.md) are not [bālâ](../../strongs/h/h1086.md) upon you, and thy [naʿal](../../strongs/h/h5275.md) is not [bālâ](../../strongs/h/h1086.md) upon thy [regel](../../strongs/h/h7272.md).

<a name="deuteronomy_29_6"></a>Deuteronomy 29:6

Ye have not ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), neither have ye [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) or [šēḵār](../../strongs/h/h7941.md): that ye might [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_29_7"></a>Deuteronomy 29:7

And when ye [bow'](../../strongs/h/h935.md) unto this [maqowm](../../strongs/h/h4725.md), [Sîḥôn](../../strongs/h/h5511.md) the [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), and [ʿÔḡ](../../strongs/h/h5747.md) the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) us unto [milḥāmâ](../../strongs/h/h4421.md), and we [nakah](../../strongs/h/h5221.md) them:

<a name="deuteronomy_29_8"></a>Deuteronomy 29:8

And we [laqach](../../strongs/h/h3947.md) their ['erets](../../strongs/h/h776.md), and [nathan](../../strongs/h/h5414.md) it for a [nachalah](../../strongs/h/h5159.md) unto the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and to the [Gāḏî](../../strongs/h/h1425.md), and to the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4520.md).

<a name="deuteronomy_29_9"></a>Deuteronomy 29:9

[shamar](../../strongs/h/h8104.md) therefore the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md), and ['asah](../../strongs/h/h6213.md) them, that ye may [sakal](../../strongs/h/h7919.md) in all that ye ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_29_10"></a>Deuteronomy 29:10

Ye [nāṣaḇ](../../strongs/h/h5324.md) this [yowm](../../strongs/h/h3117.md) all of you [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); your [ro'sh](../../strongs/h/h7218.md) of your [shebet](../../strongs/h/h7626.md), your [zāqēn](../../strongs/h/h2205.md), and your [šāṭar](../../strongs/h/h7860.md), with all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="deuteronomy_29_11"></a>Deuteronomy 29:11

Your [ṭap̄](../../strongs/h/h2945.md), your ['ishshah](../../strongs/h/h802.md), and thy [ger](../../strongs/h/h1616.md) that is [qereḇ](../../strongs/h/h7130.md) thy [maḥănê](../../strongs/h/h4264.md), from the [ḥāṭaḇ](../../strongs/h/h2404.md) of thy ['ets](../../strongs/h/h6086.md) unto the [šā'aḇ](../../strongs/h/h7579.md) of thy [mayim](../../strongs/h/h4325.md):

<a name="deuteronomy_29_12"></a>Deuteronomy 29:12

That thou shouldest ['abar](../../strongs/h/h5674.md) into [bĕriyth](../../strongs/h/h1285.md) with [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and into his ['alah](../../strongs/h/h423.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [karath](../../strongs/h/h3772.md) with thee this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_29_13"></a>Deuteronomy 29:13

That he may [quwm](../../strongs/h/h6965.md) thee to [yowm](../../strongs/h/h3117.md) for an ['am](../../strongs/h/h5971.md) unto himself, and that he may be unto thee ['Elohiym](../../strongs/h/h430.md), as he hath [dabar](../../strongs/h/h1696.md) unto thee, and as he hath [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), to ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md).

<a name="deuteronomy_29_14"></a>Deuteronomy 29:14

Neither with you only do I [karath](../../strongs/h/h3772.md) this [bĕriyth](../../strongs/h/h1285.md) and this ['alah](../../strongs/h/h423.md);

<a name="deuteronomy_29_15"></a>Deuteronomy 29:15

But with him that ['amad](../../strongs/h/h5975.md) here with us this [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and also with him that is not here with us this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_29_16"></a>Deuteronomy 29:16

(For ye [yada'](../../strongs/h/h3045.md) how we have [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); and how we ['abar](../../strongs/h/h5674.md) [qereḇ](../../strongs/h/h7130.md) the [gowy](../../strongs/h/h1471.md) which ye ['abar](../../strongs/h/h5674.md);

<a name="deuteronomy_29_17"></a>Deuteronomy 29:17

And ye have [ra'ah](../../strongs/h/h7200.md) their [šiqqûṣ](../../strongs/h/h8251.md), and their [gillûl](../../strongs/h/h1544.md), ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md), [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md), which were among them:)

<a name="deuteronomy_29_18"></a>Deuteronomy 29:18

Lest there should [yēš](../../strongs/h/h3426.md) among you ['iysh](../../strongs/h/h376.md), or ['ishshah](../../strongs/h/h802.md), or [mišpāḥâ](../../strongs/h/h4940.md), or [shebet](../../strongs/h/h7626.md), whose [lebab](../../strongs/h/h3824.md) [panah](../../strongs/h/h6437.md) this [yowm](../../strongs/h/h3117.md) from [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) the ['Elohiym](../../strongs/h/h430.md) of these [gowy](../../strongs/h/h1471.md); lest there should [yēš](../../strongs/h/h3426.md) among you a [šereš](../../strongs/h/h8328.md) that [parah](../../strongs/h/h6509.md) [rō'š](../../strongs/h/h7219.md) and [laʿănâ](../../strongs/h/h3939.md);

<a name="deuteronomy_29_19"></a>Deuteronomy 29:19

And it come to pass, when he [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of this ['alah](../../strongs/h/h423.md), that he [barak](../../strongs/h/h1288.md) himself in his [lebab](../../strongs/h/h3824.md), ['āmar](../../strongs/h/h559.md), I shall have [shalowm](../../strongs/h/h7965.md), though I [yālaḵ](../../strongs/h/h3212.md) in the [šᵊrîrûṯ](../../strongs/h/h8307.md) of mine [leb](../../strongs/h/h3820.md), to [sāp̄â](../../strongs/h/h5595.md) [rāvê](../../strongs/h/h7302.md) to [ṣāmē'](../../strongs/h/h6771.md):

<a name="deuteronomy_29_20"></a>Deuteronomy 29:20

[Yĕhovah](../../strongs/h/h3068.md) ['āḇâ](../../strongs/h/h14.md) not [sālaḥ](../../strongs/h/h5545.md) him, but then the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) and his [qin'â](../../strongs/h/h7068.md) shall [ʿāšēn](../../strongs/h/h6225.md) against that ['iysh](../../strongs/h/h376.md), and all the ['alah](../../strongs/h/h423.md) that are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md) shall [rāḇaṣ](../../strongs/h/h7257.md) upon him, and [Yĕhovah](../../strongs/h/h3068.md) shall [māḥâ](../../strongs/h/h4229.md) his [shem](../../strongs/h/h8034.md) from under [shamayim](../../strongs/h/h8064.md).

<a name="deuteronomy_29_21"></a>Deuteronomy 29:21

And [Yĕhovah](../../strongs/h/h3068.md) shall [bāḏal](../../strongs/h/h914.md) him unto [ra'](../../strongs/h/h7451.md) out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), according to all the ['alah](../../strongs/h/h423.md) of the [bĕriyth](../../strongs/h/h1285.md) that are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md):

<a name="deuteronomy_29_22"></a>Deuteronomy 29:22

So that the [dôr](../../strongs/h/h1755.md) to ['aḥărôn](../../strongs/h/h314.md) of your [ben](../../strongs/h/h1121.md) that shall [quwm](../../strongs/h/h6965.md) ['aḥar](../../strongs/h/h310.md) you, and the [nāḵrî](../../strongs/h/h5237.md) that shall [bow'](../../strongs/h/h935.md) from a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md), shall ['āmar](../../strongs/h/h559.md), when they [ra'ah](../../strongs/h/h7200.md) the [makâ](../../strongs/h/h4347.md) of that ['erets](../../strongs/h/h776.md), and the [taḥălu'iym](../../strongs/h/h8463.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [ḥālâ](../../strongs/h/h2470.md) upon it;

<a name="deuteronomy_29_23"></a>Deuteronomy 29:23

And that the ['erets](../../strongs/h/h776.md) thereof is [gophriyth](../../strongs/h/h1614.md), and [melaḥ](../../strongs/h/h4417.md), and [śᵊrēp̄â](../../strongs/h/h8316.md), that it is not [zāraʿ](../../strongs/h/h2232.md), nor [ṣāmaḥ](../../strongs/h/h6779.md), nor any ['eseb](../../strongs/h/h6212.md) [ʿālâ](../../strongs/h/h5927.md) therein, like the [mahpēḵâ](../../strongs/h/h4114.md) of [Sᵊḏōm](../../strongs/h/h5467.md), and [ʿĂmōrâ](../../strongs/h/h6017.md), ['Aḏmâ](../../strongs/h/h126.md), and [Ṣᵊḇā'îm](../../strongs/h/h6636.md), which [Yĕhovah](../../strongs/h/h3068.md) [hāp̄aḵ](../../strongs/h/h2015.md) in his ['aph](../../strongs/h/h639.md), and in his [chemah](../../strongs/h/h2534.md):

<a name="deuteronomy_29_24"></a>Deuteronomy 29:24

Even all [gowy](../../strongs/h/h1471.md) shall ['āmar](../../strongs/h/h559.md), Wherefore hath [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) thus unto this ['erets](../../strongs/h/h776.md)? what the [ḥŏrî](../../strongs/h/h2750.md) of this [gadowl](../../strongs/h/h1419.md) ['aph](../../strongs/h/h639.md)?

<a name="deuteronomy_29_25"></a>Deuteronomy 29:25

Then men shall ['āmar](../../strongs/h/h559.md), Because they have ['azab](../../strongs/h/h5800.md) the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), which he [karath](../../strongs/h/h3772.md) with them when he [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="deuteronomy_29_26"></a>Deuteronomy 29:26

For they [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them, ['Elohiym](../../strongs/h/h430.md) whom they [yada'](../../strongs/h/h3045.md) not, and he had not [chalaq](../../strongs/h/h2505.md) unto them:

<a name="deuteronomy_29_27"></a>Deuteronomy 29:27

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against this ['erets](../../strongs/h/h776.md), to [bow'](../../strongs/h/h935.md) upon it all the [qᵊlālâ](../../strongs/h/h7045.md) that are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md):

<a name="deuteronomy_29_28"></a>Deuteronomy 29:28

And [Yĕhovah](../../strongs/h/h3068.md) [nathash](../../strongs/h/h5428.md) them out of their ['ăḏāmâ](../../strongs/h/h127.md) in ['aph](../../strongs/h/h639.md), and in [chemah](../../strongs/h/h2534.md), and in [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md), and [shalak](../../strongs/h/h7993.md) them into ['aḥēr](../../strongs/h/h312.md) ['erets](../../strongs/h/h776.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_29_29"></a>Deuteronomy 29:29

The [cathar](../../strongs/h/h5641.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md): but those are [gālâ](../../strongs/h/h1540.md) unto us and to our [ben](../../strongs/h/h1121.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md), that we may ['asah](../../strongs/h/h6213.md) all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 28](deuteronomy_28.md) - [Deuteronomy 30](deuteronomy_30.md)