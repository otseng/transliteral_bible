# [Deuteronomy 12](https://www.blueletterbible.org/esv/deu/12)

<a name="deuteronomy_12_1"></a>Deuteronomy 12:1

These are the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md), which ye shall [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) in the ['erets](../../strongs/h/h776.md), which the [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) thee to [yarash](../../strongs/h/h3423.md) it, all the [yowm](../../strongs/h/h3117.md) that ye [chay](../../strongs/h/h2416.md) upon the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="deuteronomy_12_2"></a>Deuteronomy 12:2

Ye shall ['abad](../../strongs/h/h6.md) ['abad](../../strongs/h/h6.md) all the [maqowm](../../strongs/h/h4725.md), wherein the [gowy](../../strongs/h/h1471.md) ['ăšer](../../strongs/h/h834.md) ye shall [yarash](../../strongs/h/h3423.md) ['abad](../../strongs/h/h5647.md) their ['Elohiym](../../strongs/h/h430.md), upon the [ruwm](../../strongs/h/h7311.md) [har](../../strongs/h/h2022.md), and upon the [giḇʿâ](../../strongs/h/h1389.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md):

<a name="deuteronomy_12_3"></a>Deuteronomy 12:3

And ye shall [nāṯaṣ](../../strongs/h/h5422.md) their [mizbeach](../../strongs/h/h4196.md), and [shabar](../../strongs/h/h7665.md) their [maṣṣēḇâ](../../strongs/h/h4676.md), and [śārap̄](../../strongs/h/h8313.md) their ['ăšērâ](../../strongs/h/h842.md) with ['esh](../../strongs/h/h784.md); and ye shall [gāḏaʿ](../../strongs/h/h1438.md) the [pāsîl](../../strongs/h/h6456.md) of their ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h6.md) the [shem](../../strongs/h/h8034.md) of them out of that [maqowm](../../strongs/h/h4725.md).

<a name="deuteronomy_12_4"></a>Deuteronomy 12:4

Ye shall not ['asah](../../strongs/h/h6213.md) so unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_12_5"></a>Deuteronomy 12:5

But unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md) out of all your [shebet](../../strongs/h/h7626.md) to [śûm](../../strongs/h/h7760.md) his [shem](../../strongs/h/h8034.md) there, even unto his [šeḵen](../../strongs/h/h7933.md) shall ye [darash](../../strongs/h/h1875.md), and thither thou shalt [bow'](../../strongs/h/h935.md):

<a name="deuteronomy_12_6"></a>Deuteronomy 12:6

And thither ye shall [bow'](../../strongs/h/h935.md) your an [ʿōlâ](../../strongs/h/h5930.md), and your [zebach](../../strongs/h/h2077.md), and your [maʿăśēr](../../strongs/h/h4643.md), and [tᵊrûmâ](../../strongs/h/h8641.md) of your [yad](../../strongs/h/h3027.md), and your [neḏer](../../strongs/h/h5088.md), and your [nᵊḏāḇâ](../../strongs/h/h5071.md), and the [bᵊḵôrâ](../../strongs/h/h1062.md) of your [bāqār](../../strongs/h/h1241.md) and of your [tso'n](../../strongs/h/h6629.md):

<a name="deuteronomy_12_7"></a>Deuteronomy 12:7

And there ye shall ['akal](../../strongs/h/h398.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and ye shall [samach](../../strongs/h/h8055.md) in all that ye [mišlôaḥ](../../strongs/h/h4916.md) your [yad](../../strongs/h/h3027.md) unto, ye and your [bayith](../../strongs/h/h1004.md), wherein [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee.

<a name="deuteronomy_12_8"></a>Deuteronomy 12:8

Ye shall not ['asah](../../strongs/h/h6213.md) after all that we ['asah](../../strongs/h/h6213.md) here this [yowm](../../strongs/h/h3117.md), every ['iysh](../../strongs/h/h376.md) whatsoever is [yashar](../../strongs/h/h3477.md) in his own ['ayin](../../strongs/h/h5869.md).

<a name="deuteronomy_12_9"></a>Deuteronomy 12:9

For ye are not as yet [bow'](../../strongs/h/h935.md) to the [mᵊnûḥâ](../../strongs/h/h4496.md) and to the [nachalah](../../strongs/h/h5159.md), which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) you.

<a name="deuteronomy_12_10"></a>Deuteronomy 12:10

But when ye ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) giveth you to [nāḥal](../../strongs/h/h5157.md), and when he giveth you [nuwach](../../strongs/h/h5117.md) from all your ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), so that ye [yashab](../../strongs/h/h3427.md) in [betach](../../strongs/h/h983.md);

<a name="deuteronomy_12_11"></a>Deuteronomy 12:11

Then there shall be a [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md) to cause his [shem](../../strongs/h/h8034.md) to [shakan](../../strongs/h/h7931.md) there; thither shall ye [bow'](../../strongs/h/h935.md) all that I [tsavah](../../strongs/h/h6680.md) you; your an [ʿōlâ](../../strongs/h/h5930.md), and your [zebach](../../strongs/h/h2077.md), your [maʿăśēr](../../strongs/h/h4643.md), and the [tᵊrûmâ](../../strongs/h/h8641.md) of your [yad](../../strongs/h/h3027.md), and all your [miḇḥār](../../strongs/h/h4005.md) [neḏer](../../strongs/h/h5088.md) which ye [nāḏar](../../strongs/h/h5087.md) unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="deuteronomy_12_12"></a>Deuteronomy 12:12

And ye shall [samach](../../strongs/h/h8055.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), ye, and your [ben](../../strongs/h/h1121.md), and your [bath](../../strongs/h/h1323.md), and your ['ebed](../../strongs/h/h5650.md), and your ['amah](../../strongs/h/h519.md), and the [Lᵊvî](../../strongs/h/h3881.md) that is within your [sha'ar](../../strongs/h/h8179.md); forasmuch as he hath no [cheleq](../../strongs/h/h2506.md) nor [nachalah](../../strongs/h/h5159.md) with you.

<a name="deuteronomy_12_13"></a>Deuteronomy 12:13

[shamar](../../strongs/h/h8104.md) to thyself that thou [ʿālâ](../../strongs/h/h5927.md) not thy an [ʿōlâ](../../strongs/h/h5930.md) in every [maqowm](../../strongs/h/h4725.md) that thou [ra'ah](../../strongs/h/h7200.md):

<a name="deuteronomy_12_14"></a>Deuteronomy 12:14

But in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md) in one of thy [shebet](../../strongs/h/h7626.md), there thou shalt [ʿālâ](../../strongs/h/h5927.md) thy an [ʿōlâ](../../strongs/h/h5930.md), and there thou shalt ['asah](../../strongs/h/h6213.md) all that I [tsavah](../../strongs/h/h6680.md) thee.

<a name="deuteronomy_12_15"></a>Deuteronomy 12:15

Notwithstanding thou mayest [zabach](../../strongs/h/h2076.md) and ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md) in all thy [sha'ar](../../strongs/h/h8179.md), whatsoever thy [nephesh](../../strongs/h/h5315.md) ['aûâ](../../strongs/h/h185.md), according to the [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) which he hath [nathan](../../strongs/h/h5414.md) thee: the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md) may ['akal](../../strongs/h/h398.md) thereof, as of the [ṣᵊḇî](../../strongs/h/h6643.md), and as of the ['ayyāl](../../strongs/h/h354.md).

<a name="deuteronomy_12_16"></a>Deuteronomy 12:16

Only ye shall not ['akal](../../strongs/h/h398.md) the [dam](../../strongs/h/h1818.md); ye shall [šāp̄aḵ](../../strongs/h/h8210.md) it upon the ['erets](../../strongs/h/h776.md) as [mayim](../../strongs/h/h4325.md).

<a name="deuteronomy_12_17"></a>Deuteronomy 12:17

Thou [yakol](../../strongs/h/h3201.md) not ['akal](../../strongs/h/h398.md) within thy [sha'ar](../../strongs/h/h8179.md) the [maʿăśēr](../../strongs/h/h4643.md) of thy [dagan](../../strongs/h/h1715.md), or of thy [tiyrowsh](../../strongs/h/h8492.md), or of thy [yiṣhār](../../strongs/h/h3323.md), or the [bᵊḵôrâ](../../strongs/h/h1062.md) of thy [bāqār](../../strongs/h/h1241.md) or of thy [tso'n](../../strongs/h/h6629.md), nor any of thy [neḏer](../../strongs/h/h5088.md) which thou [nāḏar](../../strongs/h/h5087.md), nor thy [nᵊḏāḇâ](../../strongs/h/h5071.md), or [tᵊrûmâ](../../strongs/h/h8641.md) of thine [yad](../../strongs/h/h3027.md):

<a name="deuteronomy_12_18"></a>Deuteronomy 12:18

But thou must ['akal](../../strongs/h/h398.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md), thou, and thy [ben](../../strongs/h/h1121.md), and thy [bath](../../strongs/h/h1323.md), and thy ['ebed](../../strongs/h/h5650.md), and thy ['amah](../../strongs/h/h519.md), and the [Lᵊvî](../../strongs/h/h3881.md) that is within thy [sha'ar](../../strongs/h/h8179.md): and thou shalt [samach](../../strongs/h/h8055.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in all that thou [mišlôaḥ](../../strongs/h/h4916.md) thine [yad](../../strongs/h/h3027.md) unto.

<a name="deuteronomy_12_19"></a>Deuteronomy 12:19

[shamar](../../strongs/h/h8104.md) to thyself that thou ['azab](../../strongs/h/h5800.md) not the [Lᵊvî](../../strongs/h/h3881.md) [yowm](../../strongs/h/h3117.md) upon the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="deuteronomy_12_20"></a>Deuteronomy 12:20

When [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md)God shall [rāḥaḇ](../../strongs/h/h7337.md) thy [gᵊḇûl](../../strongs/h/h1366.md), as he hath [dabar](../../strongs/h/h1696.md) thee, and thou shalt ['āmar](../../strongs/h/h559.md), I will ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md), because thy [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md) to ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md); thou mayest ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md), whatsoever thy [nephesh](../../strongs/h/h5315.md) ['aûâ](../../strongs/h/h185.md).

<a name="deuteronomy_12_21"></a>Deuteronomy 12:21

If the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md) to [śûm](../../strongs/h/h7760.md) his [shem](../../strongs/h/h8034.md) there be too [rachaq](../../strongs/h/h7368.md) from thee, then thou shalt [zabach](../../strongs/h/h2076.md) of thy [bāqār](../../strongs/h/h1241.md) and of thy [tso'n](../../strongs/h/h6629.md), which [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) thee, as I have [tsavah](../../strongs/h/h6680.md) thee, and thou shalt ['akal](../../strongs/h/h398.md) in thy [sha'ar](../../strongs/h/h8179.md) whatsoever thy [nephesh](../../strongs/h/h5315.md) ['aûâ](../../strongs/h/h185.md).

<a name="deuteronomy_12_22"></a>Deuteronomy 12:22

Even as the [ṣᵊḇî](../../strongs/h/h6643.md) and the ['ayyāl](../../strongs/h/h354.md) is ['akal](../../strongs/h/h398.md), so thou shalt ['akal](../../strongs/h/h398.md) them: the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md) shall ['akal](../../strongs/h/h398.md) of them [yaḥaḏ](../../strongs/h/h3162.md).

<a name="deuteronomy_12_23"></a>Deuteronomy 12:23

Only be [ḥāzaq](../../strongs/h/h2388.md) that thou ['akal](../../strongs/h/h398.md) not the [dam](../../strongs/h/h1818.md): for the [dam](../../strongs/h/h1818.md) is the [nephesh](../../strongs/h/h5315.md); and thou mayest not ['akal](../../strongs/h/h398.md) the [nephesh](../../strongs/h/h5315.md) with the [basar](../../strongs/h/h1320.md).

<a name="deuteronomy_12_24"></a>Deuteronomy 12:24

Thou shalt not ['akal](../../strongs/h/h398.md) it; thou shalt [šāp̄aḵ](../../strongs/h/h8210.md) it upon the ['erets](../../strongs/h/h776.md) as [mayim](../../strongs/h/h4325.md).

<a name="deuteronomy_12_25"></a>Deuteronomy 12:25

Thou shalt not ['akal](../../strongs/h/h398.md) it; that it may go [yatab](../../strongs/h/h3190.md) with thee, and with thy [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) thee, when thou shalt ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_12_26"></a>Deuteronomy 12:26

Only thy [qodesh](../../strongs/h/h6944.md) which thou hast, and thy [neḏer](../../strongs/h/h5088.md), thou shalt [nasa'](../../strongs/h/h5375.md), and [bow'](../../strongs/h/h935.md) unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [bāḥar](../../strongs/h/h977.md):

<a name="deuteronomy_12_27"></a>Deuteronomy 12:27

And thou shalt ['asah](../../strongs/h/h6213.md) thy an [ʿōlâ](../../strongs/h/h5930.md), the [basar](../../strongs/h/h1320.md) and the [dam](../../strongs/h/h1818.md), upon the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): and the [dam](../../strongs/h/h1818.md) of thy [zebach](../../strongs/h/h2077.md) shall be [šāp̄aḵ](../../strongs/h/h8210.md) upon the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and thou shalt ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md).

<a name="deuteronomy_12_28"></a>Deuteronomy 12:28

[shamar](../../strongs/h/h8104.md) and [shama'](../../strongs/h/h8085.md) all these [dabar](../../strongs/h/h1697.md) which I [tsavah](../../strongs/h/h6680.md) thee, that it may [yatab](../../strongs/h/h3190.md) with thee, and with thy [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) thee ['owlam](../../strongs/h/h5769.md), when thou ['asah](../../strongs/h/h6213.md) that which is [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_12_29"></a>Deuteronomy 12:29

When [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [karath](../../strongs/h/h3772.md) the [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) thee, whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) them, and thou [yarash](../../strongs/h/h3423.md) them, and [yashab](../../strongs/h/h3427.md) in their ['erets](../../strongs/h/h776.md);

<a name="deuteronomy_12_30"></a>Deuteronomy 12:30

[shamar](../../strongs/h/h8104.md) to thyself that thou be not [nāqaš](../../strongs/h/h5367.md) by ['aḥar](../../strongs/h/h310.md) them, ['aḥar](../../strongs/h/h310.md) that they be [šāmaḏ](../../strongs/h/h8045.md) from [paniym](../../strongs/h/h6440.md) thee; and that thou [darash](../../strongs/h/h1875.md) not after their ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md), How did these [gowy](../../strongs/h/h1471.md) ['abad](../../strongs/h/h5647.md) their ['Elohiym](../../strongs/h/h430.md)? even so will I ['asah](../../strongs/h/h6213.md) [gam](../../strongs/h/h1571.md).

<a name="deuteronomy_12_31"></a>Deuteronomy 12:31

Thou shalt not ['asah](../../strongs/h/h6213.md) so unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): for every [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md), which he [sane'](../../strongs/h/h8130.md), have they ['asah](../../strongs/h/h6213.md) unto their ['Elohiym](../../strongs/h/h430.md); for even their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) they have [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md) to their ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_12_32"></a>Deuteronomy 12:32

What [dabar](../../strongs/h/h1697.md) soever I [tsavah](../../strongs/h/h6680.md) you, [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) it: thou shalt not [yāsap̄](../../strongs/h/h3254.md) thereto, nor [gāraʿ](../../strongs/h/h1639.md) from it.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 11](deuteronomy_11.md) - [Deuteronomy 13](deuteronomy_13.md)