# [Deuteronomy 14](https://www.blueletterbible.org/esv/deu/14)

<a name="deuteronomy_14_1"></a>Deuteronomy 14:1

Ye are the [ben](../../strongs/h/h1121.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): ye shall not [gāḏaḏ](../../strongs/h/h1413.md) yourselves, nor [śûm](../../strongs/h/h7760.md) any [qrḥ](../../strongs/h/h7144.md) between your ['ayin](../../strongs/h/h5869.md) for the [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_14_2"></a>Deuteronomy 14:2

For thou art a [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md) thee to be a [sᵊḡullâ](../../strongs/h/h5459.md) ['am](../../strongs/h/h5971.md) unto himself, above all the ['am](../../strongs/h/h5971.md) that are [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="deuteronomy_14_3"></a>Deuteronomy 14:3

Thou shalt not ['akal](../../strongs/h/h398.md) any [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="deuteronomy_14_4"></a>Deuteronomy 14:4

These are the [bĕhemah](../../strongs/h/h929.md) which ye shall ['akal](../../strongs/h/h398.md): the [showr](../../strongs/h/h7794.md), the [śê](../../strongs/h/h7716.md) [keśeḇ](../../strongs/h/h3775.md), and the [ʿēz](../../strongs/h/h5795.md),

<a name="deuteronomy_14_5"></a>Deuteronomy 14:5

The ['ayyāl](../../strongs/h/h354.md), and the [ṣᵊḇî](../../strongs/h/h6643.md), and the [yaḥmûr](../../strongs/h/h3180.md), and the ['aqqô](../../strongs/h/h689.md), and the [dîšôn](../../strongs/h/h1788.md), and the [tᵊ'ô](../../strongs/h/h8377.md), and the [zemer](../../strongs/h/h2169.md).

<a name="deuteronomy_14_6"></a>Deuteronomy 14:6

And every [bĕhemah](../../strongs/h/h929.md) that [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md), and [šesaʿ](../../strongs/h/h8157.md) the [šāsaʿ](../../strongs/h/h8156.md) into two [parsâ](../../strongs/h/h6541.md), and [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md) among the [bĕhemah](../../strongs/h/h929.md), that ye shall ['akal](../../strongs/h/h398.md).

<a name="deuteronomy_14_7"></a>Deuteronomy 14:7

Nevertheless these ye shall not ['akal](../../strongs/h/h398.md) of them that [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), or of them that [Pāras](../../strongs/h/h6536.md) the [šāsaʿ](../../strongs/h/h8156.md) [parsâ](../../strongs/h/h6541.md); as the [gāmāl](../../strongs/h/h1581.md), and the ['arneḇeṯ](../../strongs/h/h768.md), and the [šāp̄ān](../../strongs/h/h8227.md): for they [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), but [Pāras](../../strongs/h/h6536.md) not the [parsâ](../../strongs/h/h6541.md); therefore they are [tame'](../../strongs/h/h2931.md) unto you.

<a name="deuteronomy_14_8"></a>Deuteronomy 14:8

And the [ḥăzîr](../../strongs/h/h2386.md), because it [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md), yet cheweth not the [gērâ](../../strongs/h/h1625.md), it is [tame'](../../strongs/h/h2931.md) unto you: ye shall not ['akal](../../strongs/h/h398.md) of their [basar](../../strongs/h/h1320.md), nor [naga'](../../strongs/h/h5060.md) their [nᵊḇēlâ](../../strongs/h/h5038.md).

<a name="deuteronomy_14_9"></a>Deuteronomy 14:9

These ye shall ['akal](../../strongs/h/h398.md) of all that are in the [mayim](../../strongs/h/h4325.md): all that have [sᵊnapîr](../../strongs/h/h5579.md) and [qaśqeśeṯ](../../strongs/h/h7193.md) shall ye ['akal](../../strongs/h/h398.md):

<a name="deuteronomy_14_10"></a>Deuteronomy 14:10

And whatsoever hath not [sᵊnapîr](../../strongs/h/h5579.md) and [qaśqeśeṯ](../../strongs/h/h7193.md) ye may not ['akal](../../strongs/h/h398.md); it is [tame'](../../strongs/h/h2931.md) unto you.

<a name="deuteronomy_14_11"></a>Deuteronomy 14:11

Of all [tahowr](../../strongs/h/h2889.md) [tsippowr](../../strongs/h/h6833.md) ye shall ['akal](../../strongs/h/h398.md).

<a name="deuteronomy_14_12"></a>Deuteronomy 14:12

But these are they of which ye shall not ['akal](../../strongs/h/h398.md): the [nesheׁr](../../strongs/h/h5404.md), and the [peres](../../strongs/h/h6538.md), and the [ʿāznîyâ](../../strongs/h/h5822.md),

<a name="deuteronomy_14_13"></a>Deuteronomy 14:13

And the [rā'â](../../strongs/h/h7201.md), and the ['ayyâ](../../strongs/h/h344.md), and the [dayyâ](../../strongs/h/h1772.md) after his [miyn](../../strongs/h/h4327.md),

<a name="deuteronomy_14_14"></a>Deuteronomy 14:14

And every [ʿōrēḇ](../../strongs/h/h6158.md) after his [miyn](../../strongs/h/h4327.md),

<a name="deuteronomy_14_15"></a>Deuteronomy 14:15

And the [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md), and the [taḥmās](../../strongs/h/h8464.md), and the [šaḥap̄](../../strongs/h/h7828.md), and the [nēṣ](../../strongs/h/h5322.md) after his [miyn](../../strongs/h/h4327.md),

<a name="deuteronomy_14_16"></a>Deuteronomy 14:16

The [kowc](../../strongs/h/h3563.md), and the [yanšûp̄](../../strongs/h/h3244.md), and the [tinšemeṯ](../../strongs/h/h8580.md),

<a name="deuteronomy_14_17"></a>Deuteronomy 14:17

And the [qā'aṯ](../../strongs/h/h6893.md), and the [rāḥām](../../strongs/h/h7360.md), and the [šālaḵ](../../strongs/h/h7994.md),

<a name="deuteronomy_14_18"></a>Deuteronomy 14:18

And the [ḥăsîḏâ](../../strongs/h/h2624.md), and the ['ănāp̄â](../../strongs/h/h601.md) after her [miyn](../../strongs/h/h4327.md), and the [dûḵîp̄aṯ](../../strongs/h/h1744.md), and the [ʿăṭallēp̄](../../strongs/h/h5847.md).

<a name="deuteronomy_14_19"></a>Deuteronomy 14:19

And every [šereṣ](../../strongs/h/h8318.md) that [ʿôp̄](../../strongs/h/h5775.md) is [tame'](../../strongs/h/h2931.md) unto you: they shall not be ['akal](../../strongs/h/h398.md).

<a name="deuteronomy_14_20"></a>Deuteronomy 14:20

But of all [tahowr](../../strongs/h/h2889.md) [ʿôp̄](../../strongs/h/h5775.md) ye may ['akal](../../strongs/h/h398.md).

<a name="deuteronomy_14_21"></a>Deuteronomy 14:21

Ye shall not ['akal](../../strongs/h/h398.md) of anything [nᵊḇēlâ](../../strongs/h/h5038.md): thou shalt [nathan](../../strongs/h/h5414.md) it unto the [ger](../../strongs/h/h1616.md) that is in thy [sha'ar](../../strongs/h/h8179.md), that he may ['akal](../../strongs/h/h398.md) it; or thou mayest [māḵar](../../strongs/h/h4376.md) it unto a [nāḵrî](../../strongs/h/h5237.md): for thou art a [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md). Thou shalt not [bāšal](../../strongs/h/h1310.md) a [gᵊḏî](../../strongs/h/h1423.md) in his ['em](../../strongs/h/h517.md) [chalab](../../strongs/h/h2461.md).

<a name="deuteronomy_14_22"></a>Deuteronomy 14:22

Thou shalt [ʿāśar](../../strongs/h/h6237.md) [ʿāśar](../../strongs/h/h6237.md) all the [tᵊḇû'â](../../strongs/h/h8393.md) of thy [zera'](../../strongs/h/h2233.md), that the [sadeh](../../strongs/h/h7704.md) [yāṣā'](../../strongs/h/h3318.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md).

<a name="deuteronomy_14_23"></a>Deuteronomy 14:23

And thou shalt ['akal](../../strongs/h/h398.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), in the [maqowm](../../strongs/h/h4725.md) which he shall [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) his [shem](../../strongs/h/h8034.md) there, the [maʿăśēr](../../strongs/h/h4643.md) of thy [dagan](../../strongs/h/h1715.md), of thy [tiyrowsh](../../strongs/h/h8492.md), and of thine [yiṣhār](../../strongs/h/h3323.md), and the [bᵊḵôrâ](../../strongs/h/h1062.md) of thy [bāqār](../../strongs/h/h1241.md) and of thy [tso'n](../../strongs/h/h6629.md); that thou mayest [lamad](../../strongs/h/h3925.md) to [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_14_24"></a>Deuteronomy 14:24

And if the [derek](../../strongs/h/h1870.md) be [rabah](../../strongs/h/h7235.md) for thee, so that thou art not [yakol](../../strongs/h/h3201.md) to [nasa'](../../strongs/h/h5375.md) it; or if the [maqowm](../../strongs/h/h4725.md) be [rachaq](../../strongs/h/h7368.md) from thee, which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md) to [śûm](../../strongs/h/h7760.md) his [shem](../../strongs/h/h8034.md) there, when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee:

<a name="deuteronomy_14_25"></a>Deuteronomy 14:25

Then shalt thou [nathan](../../strongs/h/h5414.md) it into [keceph](../../strongs/h/h3701.md), and [ṣûr](../../strongs/h/h6696.md) the [keceph](../../strongs/h/h3701.md) in thine [yad](../../strongs/h/h3027.md), and shalt [halak](../../strongs/h/h1980.md) unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md):

<a name="deuteronomy_14_26"></a>Deuteronomy 14:26

And thou shalt [nathan](../../strongs/h/h5414.md) that [keceph](../../strongs/h/h3701.md) for whatsoever thy [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md), for [bāqār](../../strongs/h/h1241.md), or for [tso'n](../../strongs/h/h6629.md), or for [yayin](../../strongs/h/h3196.md), or for [šēḵār](../../strongs/h/h7941.md), or for whatsoever thy [nephesh](../../strongs/h/h5315.md) [sha'al](../../strongs/h/h7592.md): and thou shalt ['akal](../../strongs/h/h398.md) there [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and thou shalt [samach](../../strongs/h/h8055.md), thou, and thine [bayith](../../strongs/h/h1004.md),

<a name="deuteronomy_14_27"></a>Deuteronomy 14:27

And the [Lᵊvî](../../strongs/h/h3881.md) that is within thy [sha'ar](../../strongs/h/h8179.md); thou shalt not ['azab](../../strongs/h/h5800.md) him; for he hath no [cheleq](../../strongs/h/h2506.md) nor [nachalah](../../strongs/h/h5159.md) with thee.

<a name="deuteronomy_14_28"></a>Deuteronomy 14:28

At the [qāṣê](../../strongs/h/h7097.md) of three [šānâ](../../strongs/h/h8141.md) thou shalt [yāṣā'](../../strongs/h/h3318.md) all the [maʿăśēr](../../strongs/h/h4643.md) of thine [tᵊḇû'â](../../strongs/h/h8393.md) the same [šānâ](../../strongs/h/h8141.md), and shalt [yānaḥ](../../strongs/h/h3240.md) within thy [sha'ar](../../strongs/h/h8179.md):

<a name="deuteronomy_14_29"></a>Deuteronomy 14:29

And the [Lᵊvî](../../strongs/h/h3881.md), (because he hath no [cheleq](../../strongs/h/h2506.md) nor [nachalah](../../strongs/h/h5159.md) with thee,) and the [ger](../../strongs/h/h1616.md), and the [yathowm](../../strongs/h/h3490.md), and the ['almānâ](../../strongs/h/h490.md), which are within thy [sha'ar](../../strongs/h/h8179.md), shall [bow'](../../strongs/h/h935.md), and shall ['akal](../../strongs/h/h398.md) and be [sāׂbaʿ](../../strongs/h/h7646.md); that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) may [barak](../../strongs/h/h1288.md) thee in all the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md) which thou ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 13](deuteronomy_13.md) - [Deuteronomy 15](deuteronomy_15.md)