# [Deuteronomy 8](https://www.blueletterbible.org/kjv/deu/8)

<a name="deuteronomy_8_1"></a>Deuteronomy 8:1

All the [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md) shall ye [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md), that ye may [ḥāyâ](../../strongs/h/h2421.md), and [rabah](../../strongs/h/h7235.md), and [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_8_2"></a>Deuteronomy 8:2

And thou shalt [zakar](../../strongs/h/h2142.md) all the [derek](../../strongs/h/h1870.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yālaḵ](../../strongs/h/h3212.md) thee these forty [šānâ](../../strongs/h/h8141.md) in the [midbar](../../strongs/h/h4057.md), to [ʿānâ](../../strongs/h/h6031.md) thee, and to [nāsâ](../../strongs/h/h5254.md) thee, to [yada'](../../strongs/h/h3045.md) what was in thine [lebab](../../strongs/h/h3824.md), whether thou wouldest [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), or no.

<a name="deuteronomy_8_3"></a>Deuteronomy 8:3

And he [ʿānâ](../../strongs/h/h6031.md) thee, and [rāʿēḇ](../../strongs/h/h7456.md) thee, and ['akal](../../strongs/h/h398.md) thee with [man](../../strongs/h/h4478.md), which thou [yada'](../../strongs/h/h3045.md) not, neither did thy ['ab](../../strongs/h/h1.md) [yada'](../../strongs/h/h3045.md); that he might make thee [yada'](../../strongs/h/h3045.md) that ['adam](../../strongs/h/h120.md) doth not [ḥāyâ](../../strongs/h/h2421.md) by [lechem](../../strongs/h/h3899.md) [baḏ](../../strongs/h/h905.md), but by every word that [môṣā'](../../strongs/h/h4161.md) out of the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) doth ['adam](../../strongs/h/h120.md) [ḥāyâ](../../strongs/h/h2421.md).

<a name="deuteronomy_8_4"></a>Deuteronomy 8:4

Thy [śimlâ](../../strongs/h/h8071.md) [bālâ](../../strongs/h/h1086.md) not upon thee, neither did thy [regel](../../strongs/h/h7272.md) [bāṣēq](../../strongs/h/h1216.md), these forty [šānâ](../../strongs/h/h8141.md).

<a name="deuteronomy_8_5"></a>Deuteronomy 8:5

Thou shalt also [yada'](../../strongs/h/h3045.md) in thine [lebab](../../strongs/h/h3824.md), that, as an ['iysh](../../strongs/h/h376.md) [yacar](../../strongs/h/h3256.md) his [ben](../../strongs/h/h1121.md), so [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yacar](../../strongs/h/h3256.md) thee.

<a name="deuteronomy_8_6"></a>Deuteronomy 8:6

Therefore thou shalt [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in his [derek](../../strongs/h/h1870.md), and to [yare'](../../strongs/h/h3372.md) him.

<a name="deuteronomy_8_7"></a>Deuteronomy 8:7

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) thee into a [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md), an ['erets](../../strongs/h/h776.md) of [nachal](../../strongs/h/h5158.md) of [mayim](../../strongs/h/h4325.md), of ['ayin](../../strongs/h/h5869.md) and [tĕhowm](../../strongs/h/h8415.md) that [yāṣā'](../../strongs/h/h3318.md) of [biqʿâ](../../strongs/h/h1237.md) and [har](../../strongs/h/h2022.md);

<a name="deuteronomy_8_8"></a>Deuteronomy 8:8

An ['erets](../../strongs/h/h776.md) of [ḥiṭṭâ](../../strongs/h/h2406.md), and [śᵊʿōrâ](../../strongs/h/h8184.md), and [gep̄en](../../strongs/h/h1612.md), and [tĕ'en](../../strongs/h/h8384.md), and [rimmôn](../../strongs/h/h7416.md); an ['erets](../../strongs/h/h776.md) of [šemen](../../strongs/h/h8081.md) [zayiṯ](../../strongs/h/h2132.md), and [dĕbash](../../strongs/h/h1706.md);

<a name="deuteronomy_8_9"></a>Deuteronomy 8:9

An ['erets](../../strongs/h/h776.md) wherein thou shalt ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) without [miskēnuṯ](../../strongs/h/h4544.md), thou shalt not [ḥāsēr](../../strongs/h/h2637.md) any thing in it; an ['erets](../../strongs/h/h776.md) whose ['eben](../../strongs/h/h68.md) are [barzel](../../strongs/h/h1270.md), and out of whose [har](../../strongs/h/h2042.md) thou mayest [ḥāṣaḇ](../../strongs/h/h2672.md) [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="deuteronomy_8_10"></a>Deuteronomy 8:10

When thou hast ['akal](../../strongs/h/h398.md) and art [sāׂbaʿ](../../strongs/h/h7646.md), then thou shalt [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) for the [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) which he hath [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_8_11"></a>Deuteronomy 8:11

[shamar](../../strongs/h/h8104.md) that thou [shakach](../../strongs/h/h7911.md) not [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), in not [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), and his [mishpat](../../strongs/h/h4941.md), and his [chuqqah](../../strongs/h/h2708.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_8_12"></a>Deuteronomy 8:12

Lest when thou hast ['akal](../../strongs/h/h398.md) and art [sāׂbaʿ](../../strongs/h/h7646.md), and hast [bānâ](../../strongs/h/h1129.md) [towb](../../strongs/h/h2896.md) [bayith](../../strongs/h/h1004.md), and [yashab](../../strongs/h/h3427.md) therein;

<a name="deuteronomy_8_13"></a>Deuteronomy 8:13

And when thy [bāqār](../../strongs/h/h1241.md) and thy [tso'n](../../strongs/h/h6629.md) [rabah](../../strongs/h/h7235.md), and thy [keceph](../../strongs/h/h3701.md) and thy [zāhāḇ](../../strongs/h/h2091.md) is [rabah](../../strongs/h/h7235.md), and all that thou hast is [rabah](../../strongs/h/h7235.md);

<a name="deuteronomy_8_14"></a>Deuteronomy 8:14

Then thine [lebab](../../strongs/h/h3824.md) be [ruwm](../../strongs/h/h7311.md), and thou [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md);

<a name="deuteronomy_8_15"></a>Deuteronomy 8:15

Who [yālaḵ](../../strongs/h/h3212.md) thee through that [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) [midbar](../../strongs/h/h4057.md), wherein were [saraph](../../strongs/h/h8314.md) [nachash](../../strongs/h/h5175.md), and [ʿaqrāḇ](../../strongs/h/h6137.md), and [ṣimmā'ôn](../../strongs/h/h6774.md), where there was no [mayim](../../strongs/h/h4325.md); who [yāṣā'](../../strongs/h/h3318.md) thee [mayim](../../strongs/h/h4325.md) out of the [tsuwr](../../strongs/h/h6697.md) of [ḥallāmîš](../../strongs/h/h2496.md);

<a name="deuteronomy_8_16"></a>Deuteronomy 8:16

Who ['akal](../../strongs/h/h398.md) thee in the [midbar](../../strongs/h/h4057.md) with [man](../../strongs/h/h4478.md), which thy ['ab](../../strongs/h/h1.md) [yada'](../../strongs/h/h3045.md) not, that he might [ʿānâ](../../strongs/h/h6031.md) thee, and that he might [nāsâ](../../strongs/h/h5254.md) thee, to do thee [yatab](../../strongs/h/h3190.md) at thy ['aḥărîṯ](../../strongs/h/h319.md);

<a name="deuteronomy_8_17"></a>Deuteronomy 8:17

And thou ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), My [koach](../../strongs/h/h3581.md) and the [ʿōṣem](../../strongs/h/h6108.md) of mine [yad](../../strongs/h/h3027.md) hath ['asah](../../strongs/h/h6213.md) me this [ḥayil](../../strongs/h/h2428.md).

<a name="deuteronomy_8_18"></a>Deuteronomy 8:18

But thou shalt [zakar](../../strongs/h/h2142.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): for it is he that [nathan](../../strongs/h/h5414.md) thee [koach](../../strongs/h/h3581.md) to ['asah](../../strongs/h/h6213.md) [ḥayil](../../strongs/h/h2428.md), that he may [quwm](../../strongs/h/h6965.md) his [bĕriyth](../../strongs/h/h1285.md) which he [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_8_19"></a>Deuteronomy 8:19

And it shall be, if thou [shakach](../../strongs/h/h7911.md) [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) them, and [shachah](../../strongs/h/h7812.md) them, I [ʿûḏ](../../strongs/h/h5749.md) against you this [yowm](../../strongs/h/h3117.md) that ye shall ['abad](../../strongs/h/h6.md) ['abad](../../strongs/h/h6.md).

<a name="deuteronomy_8_20"></a>Deuteronomy 8:20

As the [gowy](../../strongs/h/h1471.md) which [Yĕhovah](../../strongs/h/h3068.md) ['abad](../../strongs/h/h6.md) before your [paniym](../../strongs/h/h6440.md), so shall ye ['abad](../../strongs/h/h6.md); because ye would not [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 7](deuteronomy_7.md) - [Deuteronomy 9](deuteronomy_9.md)