# [Deuteronomy 30](https://www.blueletterbible.org/kjv/deu/30)

<a name="deuteronomy_30_1"></a>Deuteronomy 30:1

And it shall come to pass, when all these [dabar](../../strongs/h/h1697.md) are [bow'](../../strongs/h/h935.md) upon thee, the [bĕrakah](../../strongs/h/h1293.md) and the [qᵊlālâ](../../strongs/h/h7045.md), which I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) thee, and thou shalt [shuwb](../../strongs/h/h7725.md) them to [lebab](../../strongs/h/h3824.md) among all the [gowy](../../strongs/h/h1471.md), whither [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nāḏaḥ](../../strongs/h/h5080.md) thee,

<a name="deuteronomy_30_2"></a>Deuteronomy 30:2

And shalt [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and shalt [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md) according to all that I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), thou and thy [ben](../../strongs/h/h1121.md), with all thine [lebab](../../strongs/h/h3824.md), and with all thy [nephesh](../../strongs/h/h5315.md);

<a name="deuteronomy_30_3"></a>Deuteronomy 30:3

That then [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [shuwb](../../strongs/h/h7725.md) thy [shebuwth](../../strongs/h/h7622.md), and have [racham](../../strongs/h/h7355.md) upon thee, and will [shuwb](../../strongs/h/h7725.md) and [qāḇaṣ](../../strongs/h/h6908.md) thee from all the ['am](../../strongs/h/h5971.md), whither [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [puwts](../../strongs/h/h6327.md) thee.

<a name="deuteronomy_30_4"></a>Deuteronomy 30:4

If any of thine be [nāḏaḥ](../../strongs/h/h5080.md) unto the [qāṣê](../../strongs/h/h7097.md) parts of [shamayim](../../strongs/h/h8064.md), from thence will [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [qāḇaṣ](../../strongs/h/h6908.md) thee, and from thence will he [laqach](../../strongs/h/h3947.md) thee:

<a name="deuteronomy_30_5"></a>Deuteronomy 30:5

And [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) which thy ['ab](../../strongs/h/h1.md) [yarash](../../strongs/h/h3423.md), and thou shalt [yarash](../../strongs/h/h3423.md) it; and he will do thee [yatab](../../strongs/h/h3190.md), and [rabah](../../strongs/h/h7235.md) thee above thy ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_30_6"></a>Deuteronomy 30:6

And [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [muwl](../../strongs/h/h4135.md) thine [lebab](../../strongs/h/h3824.md), and the [lebab](../../strongs/h/h3824.md) of thy [zera'](../../strongs/h/h2233.md), to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) with all thine [lebab](../../strongs/h/h3824.md), and with all thy [nephesh](../../strongs/h/h5315.md), that thou mayest [chay](../../strongs/h/h2416.md).

<a name="deuteronomy_30_7"></a>Deuteronomy 30:7

And [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [nathan](../../strongs/h/h5414.md) all these ['alah](../../strongs/h/h423.md) upon thine ['oyeb](../../strongs/h/h341.md), and on them that [sane'](../../strongs/h/h8130.md) thee, which [radaph](../../strongs/h/h7291.md) thee.

<a name="deuteronomy_30_8"></a>Deuteronomy 30:8

And thou shalt [shuwb](../../strongs/h/h7725.md) and [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md) all his [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_30_9"></a>Deuteronomy 30:9

And [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [yāṯar](../../strongs/h/h3498.md) thee in every [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md), in the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md), and in the [pĕriy](../../strongs/h/h6529.md) of thy [bĕhemah](../../strongs/h/h929.md), and in the [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md), for [towb](../../strongs/h/h2896.md): for [Yĕhovah](../../strongs/h/h3068.md) will [shuwb](../../strongs/h/h7725.md) [śûś](../../strongs/h/h7797.md) over thee for [towb](../../strongs/h/h2896.md), as he [śûś](../../strongs/h/h7797.md) over thy ['ab](../../strongs/h/h1.md):

<a name="deuteronomy_30_10"></a>Deuteronomy 30:10

If thou shalt [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md) and his [chuqqah](../../strongs/h/h2708.md) which are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md), and if thou [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) with all thine [lebab](../../strongs/h/h3824.md), and with all thy [nephesh](../../strongs/h/h5315.md).

<a name="deuteronomy_30_11"></a>Deuteronomy 30:11

For this [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), it is not [pala'](../../strongs/h/h6381.md) from thee, neither is it [rachowq](../../strongs/h/h7350.md).

<a name="deuteronomy_30_12"></a>Deuteronomy 30:12

It is not in [shamayim](../../strongs/h/h8064.md), that thou shouldest ['āmar](../../strongs/h/h559.md), Who shall [ʿālâ](../../strongs/h/h5927.md) for us to [shamayim](../../strongs/h/h8064.md), and [laqach](../../strongs/h/h3947.md) it unto us, that we may [shama'](../../strongs/h/h8085.md) it, and ['asah](../../strongs/h/h6213.md) it?

<a name="deuteronomy_30_13"></a>Deuteronomy 30:13

Neither is it [ʿēḇer](../../strongs/h/h5676.md) the [yam](../../strongs/h/h3220.md), that thou shouldest ['āmar](../../strongs/h/h559.md), Who shall ['abar](../../strongs/h/h5674.md) [ʿēḇer](../../strongs/h/h5676.md) the [yam](../../strongs/h/h3220.md) for us, and [laqach](../../strongs/h/h3947.md) it unto us, that we may [shama'](../../strongs/h/h8085.md) it, and ['asah](../../strongs/h/h6213.md) it?

<a name="deuteronomy_30_14"></a>Deuteronomy 30:14

But the [dabar](../../strongs/h/h1697.md) is [me'od](../../strongs/h/h3966.md) [qarowb](../../strongs/h/h7138.md) unto thee, in thy [peh](../../strongs/h/h6310.md), and in thy [lebab](../../strongs/h/h3824.md), that thou mayest ['asah](../../strongs/h/h6213.md) it.

<a name="deuteronomy_30_15"></a>Deuteronomy 30:15

[ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) thee this [yowm](../../strongs/h/h3117.md) [chay](../../strongs/h/h2416.md) and [towb](../../strongs/h/h2896.md), and [maveth](../../strongs/h/h4194.md) and [ra'](../../strongs/h/h7451.md);

<a name="deuteronomy_30_16"></a>Deuteronomy 30:16

In that I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md) to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in his [derek](../../strongs/h/h1870.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md) and his [chuqqah](../../strongs/h/h2708.md) and his [mishpat](../../strongs/h/h4941.md), that thou mayest [ḥāyâ](../../strongs/h/h2421.md) and [rabah](../../strongs/h/h7235.md): and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [barak](../../strongs/h/h1288.md) thee in the ['erets](../../strongs/h/h776.md) whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_30_17"></a>Deuteronomy 30:17

But if thine [lebab](../../strongs/h/h3824.md) [panah](../../strongs/h/h6437.md), so that thou wilt not [shama'](../../strongs/h/h8085.md), but shalt be [nāḏaḥ](../../strongs/h/h5080.md), and [shachah](../../strongs/h/h7812.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) them;

<a name="deuteronomy_30_18"></a>Deuteronomy 30:18

I [nāḡaḏ](../../strongs/h/h5046.md) unto you this [yowm](../../strongs/h/h3117.md), that ye shall ['abad](../../strongs/h/h6.md) ['abad](../../strongs/h/h6.md), and that ye shall not ['arak](../../strongs/h/h748.md) your [yowm](../../strongs/h/h3117.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), whither thou ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_30_19"></a>Deuteronomy 30:19

I [ʿûḏ](../../strongs/h/h5749.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md) this [yowm](../../strongs/h/h3117.md) against you, that I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you [chay](../../strongs/h/h2416.md) and [maveth](../../strongs/h/h4194.md), [bĕrakah](../../strongs/h/h1293.md) and [qᵊlālâ](../../strongs/h/h7045.md): therefore [bāḥar](../../strongs/h/h977.md) [chay](../../strongs/h/h2416.md), that both thou and thy [zera'](../../strongs/h/h2233.md) may [ḥāyâ](../../strongs/h/h2421.md):

<a name="deuteronomy_30_20"></a>Deuteronomy 30:20

That thou mayest ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and that thou mayest [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), and that thou mayest [dāḇaq](../../strongs/h/h1692.md) unto him: for he is thy [chay](../../strongs/h/h2416.md), and the ['ōreḵ](../../strongs/h/h753.md) of thy [yowm](../../strongs/h/h3117.md): that thou mayest [yashab](../../strongs/h/h3427.md) in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), to ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md), to [nathan](../../strongs/h/h5414.md) them.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 29](deuteronomy_29.md) - [Deuteronomy 31](deuteronomy_31.md)