# [Deuteronomy 5](https://www.blueletterbible.org/kjv/deu/5)

<a name="deuteronomy_5_1"></a>Deuteronomy 5:1

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) all [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md), the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md) which I [dabar](../../strongs/h/h1696.md) in your ['ozen](../../strongs/h/h241.md) this [yowm](../../strongs/h/h3117.md), that ye may [lamad](../../strongs/h/h3925.md) them, and [shamar](../../strongs/h/h8104.md), and ['asah](../../strongs/h/h6213.md) them.

<a name="deuteronomy_5_2"></a>Deuteronomy 5:2

[Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with us in [Hōrēḇ](../../strongs/h/h2722.md).

<a name="deuteronomy_5_3"></a>Deuteronomy 5:3

[Yĕhovah](../../strongs/h/h3068.md) [karath](../../strongs/h/h3772.md) not this [bĕriyth](../../strongs/h/h1285.md) with our ['ab](../../strongs/h/h1.md), but with us, even us, who are all of us here [chay](../../strongs/h/h2416.md) this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_5_4"></a>Deuteronomy 5:4

[Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) with you [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md) in the [har](../../strongs/h/h2022.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md),

<a name="deuteronomy_5_5"></a>Deuteronomy 5:5

(I ['amad](../../strongs/h/h5975.md) between [Yĕhovah](../../strongs/h/h3068.md) and you at that [ʿēṯ](../../strongs/h/h6256.md), to [nāḡaḏ](../../strongs/h/h5046.md) you the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md): for ye were [yare'](../../strongs/h/h3372.md) by [paniym](../../strongs/h/h6440.md) of the ['esh](../../strongs/h/h784.md), and [ʿālâ](../../strongs/h/h5927.md) not into the [har](../../strongs/h/h2022.md);) ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_5_6"></a>Deuteronomy 5:6

I am [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md).

<a name="deuteronomy_5_7"></a>Deuteronomy 5:7

Thou shalt have none ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) [paniym](../../strongs/h/h6440.md) me.

<a name="deuteronomy_5_8"></a>Deuteronomy 5:8

Thou shalt not ['asah](../../strongs/h/h6213.md) thee any [pecel](../../strongs/h/h6459.md), or any [tĕmuwnah](../../strongs/h/h8544.md) of any thing that is in [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md), or that is in the ['erets](../../strongs/h/h776.md) beneath, or that is in the [mayim](../../strongs/h/h4325.md) beneath the ['erets](../../strongs/h/h776.md):

<a name="deuteronomy_5_9"></a>Deuteronomy 5:9

Thou shalt not [shachah](../../strongs/h/h7812.md) thyself unto them, nor ['abad](../../strongs/h/h5647.md) them: for I [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) am a [qanna'](../../strongs/h/h7067.md) ['el](../../strongs/h/h410.md), [paqad](../../strongs/h/h6485.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md) upon the [ben](../../strongs/h/h1121.md) unto the third and fourth of them that [sane'](../../strongs/h/h8130.md) me,

<a name="deuteronomy_5_10"></a>Deuteronomy 5:10

And ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto thousands of them that ['ahab](../../strongs/h/h157.md) me and [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md).

<a name="deuteronomy_5_11"></a>Deuteronomy 5:11

Thou shalt not [nasa'](../../strongs/h/h5375.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in [shav'](../../strongs/h/h7723.md): for [Yĕhovah](../../strongs/h/h3068.md) will not hold him [naqah](../../strongs/h/h5352.md) that [nasa'](../../strongs/h/h5375.md) his [shem](../../strongs/h/h8034.md) in [shav'](../../strongs/h/h7723.md).

<a name="deuteronomy_5_12"></a>Deuteronomy 5:12

[shamar](../../strongs/h/h8104.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md) to [qadash](../../strongs/h/h6942.md) it, as [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thee.

<a name="deuteronomy_5_13"></a>Deuteronomy 5:13

Six [yowm](../../strongs/h/h3117.md) thou shalt ['abad](../../strongs/h/h5647.md), and ['asah](../../strongs/h/h6213.md) all thy [mĕla'kah](../../strongs/h/h4399.md):

<a name="deuteronomy_5_14"></a>Deuteronomy 5:14

But the seventh [yowm](../../strongs/h/h3117.md) is the [shabbath](../../strongs/h/h7676.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): in it thou shalt not ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md), thou, nor thy [ben](../../strongs/h/h1121.md), nor thy [bath](../../strongs/h/h1323.md), nor thy ['ebed](../../strongs/h/h5650.md), nor thy ['amah](../../strongs/h/h519.md), nor thine [showr](../../strongs/h/h7794.md), nor thine [chamowr](../../strongs/h/h2543.md), nor any of thy [bĕhemah](../../strongs/h/h929.md), nor thy [ger](../../strongs/h/h1616.md) that is within thy [sha'ar](../../strongs/h/h8179.md); that thy ['ebed](../../strongs/h/h5650.md) and thy ['amah](../../strongs/h/h519.md) may [nuwach](../../strongs/h/h5117.md) as well as thou.

<a name="deuteronomy_5_15"></a>Deuteronomy 5:15

And [zakar](../../strongs/h/h2142.md) that thou wast an ['ebed](../../strongs/h/h5650.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yāṣā'](../../strongs/h/h3318.md) thee out thence through a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md) and by a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md): therefore [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) thee to ['asah](../../strongs/h/h6213.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_5_16"></a>Deuteronomy 5:16

[kabad](../../strongs/h/h3513.md) thy ['ab](../../strongs/h/h1.md) and thy ['em](../../strongs/h/h517.md), as [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thee; that thy [yowm](../../strongs/h/h3117.md) may be ['arak](../../strongs/h/h748.md), and that it may [yatab](../../strongs/h/h3190.md) with thee, in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_5_17"></a>Deuteronomy 5:17

Thou shalt not [ratsach](../../strongs/h/h7523.md).

<a name="deuteronomy_5_18"></a>Deuteronomy 5:18

Neither shalt thou commit [na'aph](../../strongs/h/h5003.md).

<a name="deuteronomy_5_19"></a>Deuteronomy 5:19

Neither shalt thou [ganab](../../strongs/h/h1589.md).

<a name="deuteronomy_5_20"></a>Deuteronomy 5:20

Neither shalt thou ['anah](../../strongs/h/h6030.md) [shav'](../../strongs/h/h7723.md) ['ed](../../strongs/h/h5707.md) against thy [rea'](../../strongs/h/h7453.md).

<a name="deuteronomy_5_21"></a>Deuteronomy 5:21

Neither shalt thou [chamad](../../strongs/h/h2530.md) thy [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md), neither shalt thou ['āvâ](../../strongs/h/h183.md) thy [rea'](../../strongs/h/h7453.md) [bayith](../../strongs/h/h1004.md), his [sadeh](../../strongs/h/h7704.md), or his ['ebed](../../strongs/h/h5650.md), or his ['amah](../../strongs/h/h519.md), his [showr](../../strongs/h/h7794.md), or his [chamowr](../../strongs/h/h2543.md), or any thing that is thy [rea'](../../strongs/h/h7453.md).

<a name="deuteronomy_5_22"></a>Deuteronomy 5:22

These [dabar](../../strongs/h/h1697.md) [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto all your [qāhēl](../../strongs/h/h6951.md) in the [har](../../strongs/h/h2022.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md), of the [ʿānān](../../strongs/h/h6051.md), and of the ['araphel](../../strongs/h/h6205.md), with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md): and he added no [yāsap̄](../../strongs/h/h3254.md). And he [kāṯaḇ](../../strongs/h/h3789.md) them in two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), and [nathan](../../strongs/h/h5414.md) them unto me.

<a name="deuteronomy_5_23"></a>Deuteronomy 5:23

And it came to pass, when ye [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) out of the [tavek](../../strongs/h/h8432.md) of the [choshek](../../strongs/h/h2822.md), (for the [har](../../strongs/h/h2022.md) did [bāʿar](../../strongs/h/h1197.md) with ['esh](../../strongs/h/h784.md),) that ye [qāraḇ](../../strongs/h/h7126.md) unto me, even all the [ro'sh](../../strongs/h/h7218.md) of your [shebet](../../strongs/h/h7626.md), and your [zāqēn](../../strongs/h/h2205.md);

<a name="deuteronomy_5_24"></a>Deuteronomy 5:24

And ye ['āmar](../../strongs/h/h559.md), Behold, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) hath [ra'ah](../../strongs/h/h7200.md) us his [kabowd](../../strongs/h/h3519.md) and his [gōḏel](../../strongs/h/h1433.md), and we have [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md): we have [ra'ah](../../strongs/h/h7200.md) this [yowm](../../strongs/h/h3117.md) that ['Elohiym](../../strongs/h/h430.md) doth [dabar](../../strongs/h/h1696.md) with ['āḏām](../../strongs/h/h120.md), and he [chayay](../../strongs/h/h2425.md).

<a name="deuteronomy_5_25"></a>Deuteronomy 5:25

Now therefore why should we [muwth](../../strongs/h/h4191.md)? for this [gadowl](../../strongs/h/h1419.md) ['esh](../../strongs/h/h784.md) will ['akal](../../strongs/h/h398.md) us: if we [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) any [yāsap̄](../../strongs/h/h3254.md), then we shall [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_5_26"></a>Deuteronomy 5:26

For who is there of all [basar](../../strongs/h/h1320.md), that hath [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md), as we have, and [ḥāyâ](../../strongs/h/h2421.md)?

<a name="deuteronomy_5_27"></a>Deuteronomy 5:27

[qāraḇ](../../strongs/h/h7126.md) thou, and [shama'](../../strongs/h/h8085.md) all that [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall ['āmar](../../strongs/h/h559.md): and [dabar](../../strongs/h/h1696.md) thou unto us all that [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall [dabar](../../strongs/h/h1696.md) unto thee; and we will [shama'](../../strongs/h/h8085.md) it, and ['asah](../../strongs/h/h6213.md) it.

<a name="deuteronomy_5_28"></a>Deuteronomy 5:28

And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of your [dabar](../../strongs/h/h1697.md), when ye [dabar](../../strongs/h/h1696.md) unto me; and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, I have [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [dabar](../../strongs/h/h1697.md) of this ['am](../../strongs/h/h5971.md), which they have [dabar](../../strongs/h/h1696.md) unto thee: they have [yatab](../../strongs/h/h3190.md) said all that they have [dabar](../../strongs/h/h1696.md).

<a name="deuteronomy_5_29"></a>Deuteronomy 5:29

O that there were such a [lebab](../../strongs/h/h3824.md) [nathan](../../strongs/h/h5414.md), that they would [yare'](../../strongs/h/h3372.md) me, and [shamar](../../strongs/h/h8104.md) all my [mitsvah](../../strongs/h/h4687.md) [yowm](../../strongs/h/h3117.md), that it might be [yatab](../../strongs/h/h3190.md) with them, and with their [ben](../../strongs/h/h1121.md) ['owlam](../../strongs/h/h5769.md)!

<a name="deuteronomy_5_30"></a>Deuteronomy 5:30

[yālaḵ](../../strongs/h/h3212.md) ['āmar](../../strongs/h/h559.md) to them, [shuwb](../../strongs/h/h7725.md) you into your ['ohel](../../strongs/h/h168.md) [shuwb](../../strongs/h/h7725.md).

<a name="deuteronomy_5_31"></a>Deuteronomy 5:31

But as for thee, ['amad](../../strongs/h/h5975.md) thou here by me, and I will [dabar](../../strongs/h/h1696.md) unto thee all the [mitsvah](../../strongs/h/h4687.md), and the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), which thou shalt [lamad](../../strongs/h/h3925.md) them, that they may ['asah](../../strongs/h/h6213.md) them in the ['erets](../../strongs/h/h776.md) which I [nathan](../../strongs/h/h5414.md) them to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_5_32"></a>Deuteronomy 5:32

Ye shall [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) therefore as [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) you: ye shall not [cuwr](../../strongs/h/h5493.md) to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="deuteronomy_5_33"></a>Deuteronomy 5:33

Ye shall [yālaḵ](../../strongs/h/h3212.md) in all the [derek](../../strongs/h/h1870.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) you, that ye may [ḥāyâ](../../strongs/h/h2421.md), and that it may be [ṭôḇ](../../strongs/h/h2895.md) with you, and that ye may ['arak](../../strongs/h/h748.md) your [yowm](../../strongs/h/h3117.md) in the ['erets](../../strongs/h/h776.md) which ye shall [yarash](../../strongs/h/h3423.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 4](deuteronomy_4.md) - [Deuteronomy 6](deuteronomy_6.md)