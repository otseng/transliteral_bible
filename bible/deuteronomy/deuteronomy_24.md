# [Deuteronomy 24](https://www.blueletterbible.org/kjv/deu/24)

<a name="deuteronomy_24_1"></a>Deuteronomy 24:1

When an ['iysh](../../strongs/h/h376.md) hath [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md), and [bāʿal](../../strongs/h/h1166.md) her, and it come to pass that she [māṣā'](../../strongs/h/h4672.md) no [ḥēn](../../strongs/h/h2580.md) in his ['ayin](../../strongs/h/h5869.md), because he hath [māṣā'](../../strongs/h/h4672.md) [dabar](../../strongs/h/h1697.md) [ʿervâ](../../strongs/h/h6172.md) in her: then let him [kāṯaḇ](../../strongs/h/h3789.md) her a [sēp̄er](../../strongs/h/h5612.md) of [kᵊrîṯûṯ](../../strongs/h/h3748.md), and [nathan](../../strongs/h/h5414.md) it in her [yad](../../strongs/h/h3027.md), and [shalach](../../strongs/h/h7971.md) her out of his [bayith](../../strongs/h/h1004.md).

<a name="deuteronomy_24_2"></a>Deuteronomy 24:2

And when she is [yāṣā'](../../strongs/h/h3318.md) of his [bayith](../../strongs/h/h1004.md), she may [halak](../../strongs/h/h1980.md) and be ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md).

<a name="deuteronomy_24_3"></a>Deuteronomy 24:3

And if the ['aḥărôn](../../strongs/h/h314.md) ['iysh](../../strongs/h/h376.md) [sane'](../../strongs/h/h8130.md) her, and [kāṯaḇ](../../strongs/h/h3789.md) her a [sēp̄er](../../strongs/h/h5612.md) of [kᵊrîṯûṯ](../../strongs/h/h3748.md), and [nathan](../../strongs/h/h5414.md) it in her [yad](../../strongs/h/h3027.md), and [shalach](../../strongs/h/h7971.md) her out of his [bayith](../../strongs/h/h1004.md); or if the ['aḥărôn](../../strongs/h/h314.md) ['iysh](../../strongs/h/h376.md) [muwth](../../strongs/h/h4191.md), which [laqach](../../strongs/h/h3947.md) her to be his ['ishshah](../../strongs/h/h802.md);

<a name="deuteronomy_24_4"></a>Deuteronomy 24:4

Her [ri'šôn](../../strongs/h/h7223.md) [baʿal](../../strongs/h/h1167.md), which [shalach](../../strongs/h/h7971.md) her, [yakol](../../strongs/h/h3201.md) not [laqach](../../strongs/h/h3947.md) her [shuwb](../../strongs/h/h7725.md) to be his ['ishshah](../../strongs/h/h802.md), ['aḥar](../../strongs/h/h310.md) that she is [ṭāmē'](../../strongs/h/h2930.md); for that is [tôʿēḇâ](../../strongs/h/h8441.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and thou shalt not cause the ['erets](../../strongs/h/h776.md) to [chata'](../../strongs/h/h2398.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md).

<a name="deuteronomy_24_5"></a>Deuteronomy 24:5

When an ['iysh](../../strongs/h/h376.md) hath [laqach](../../strongs/h/h3947.md) a [ḥāḏāš](../../strongs/h/h2319.md) ['ishshah](../../strongs/h/h802.md), he shall not [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), neither shall he be ['abar](../../strongs/h/h5674.md) with any [dabar](../../strongs/h/h1697.md): but he shall be [naqiy](../../strongs/h/h5355.md) at [bayith](../../strongs/h/h1004.md) one [šānâ](../../strongs/h/h8141.md), and shall [samach](../../strongs/h/h8055.md) his ['ishshah](../../strongs/h/h802.md) which he hath [laqach](../../strongs/h/h3947.md).

<a name="deuteronomy_24_6"></a>Deuteronomy 24:6

No man shall [chabal](../../strongs/h/h2254.md) the [rēḥayim](../../strongs/h/h7347.md) or the [reḵeḇ](../../strongs/h/h7393.md): for he [chabal](../../strongs/h/h2254.md) a [nephesh](../../strongs/h/h5315.md).

<a name="deuteronomy_24_7"></a>Deuteronomy 24:7

If an ['iysh](../../strongs/h/h376.md) be [māṣā'](../../strongs/h/h4672.md) [ganab](../../strongs/h/h1589.md) [nephesh](../../strongs/h/h5315.md) of his ['ach](../../strongs/h/h251.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [ʿāmar](../../strongs/h/h6014.md) of him, or [māḵar](../../strongs/h/h4376.md) him; then that [gannāḇ](../../strongs/h/h1590.md) shall [muwth](../../strongs/h/h4191.md); and thou shalt [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_24_8"></a>Deuteronomy 24:8

[shamar](../../strongs/h/h8104.md) in the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md), that thou [shamar](../../strongs/h/h8104.md) [me'od](../../strongs/h/h3966.md), and ['asah](../../strongs/h/h6213.md) according to all that the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) shall [yārâ](../../strongs/h/h3384.md) you: as I [tsavah](../../strongs/h/h6680.md) them, so ye shall [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_24_9"></a>Deuteronomy 24:9

[zakar](../../strongs/h/h2142.md) what [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) unto [Miryām](../../strongs/h/h4813.md) by the [derek](../../strongs/h/h1870.md), after that ye were [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_24_10"></a>Deuteronomy 24:10

When thou dost [nāšâ](../../strongs/h/h5383.md) thy [rea'](../../strongs/h/h7453.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) [maššā'â](../../strongs/h/h4859.md), thou shalt not [bow'](../../strongs/h/h935.md) into his [bayith](../../strongs/h/h1004.md) to [ʿāḇaṭ](../../strongs/h/h5670.md) his [ʿăḇôṭ](../../strongs/h/h5667.md).

<a name="deuteronomy_24_11"></a>Deuteronomy 24:11

Thou shalt ['amad](../../strongs/h/h5975.md) [ḥûṣ](../../strongs/h/h2351.md), and the ['iysh](../../strongs/h/h376.md) to whom thou dost [nāšâ](../../strongs/h/h5383.md) shall [yāṣā'](../../strongs/h/h3318.md) the [ʿăḇôṭ](../../strongs/h/h5667.md) [ḥûṣ](../../strongs/h/h2351.md) unto thee.

<a name="deuteronomy_24_12"></a>Deuteronomy 24:12

And if the ['iysh](../../strongs/h/h376.md) be ['aniy](../../strongs/h/h6041.md), thou shalt not [shakab](../../strongs/h/h7901.md) with his [ʿăḇôṭ](../../strongs/h/h5667.md):

<a name="deuteronomy_24_13"></a>Deuteronomy 24:13

In any case thou shalt [shuwb](../../strongs/h/h7725.md) him the [ʿăḇôṭ](../../strongs/h/h5667.md) again when the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md), that he may [shakab](../../strongs/h/h7901.md) in his own [śalmâ](../../strongs/h/h8008.md), and [barak](../../strongs/h/h1288.md) thee: and it shall be [ṣĕdāqāh](../../strongs/h/h6666.md) unto thee [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_24_14"></a>Deuteronomy 24:14

Thou shalt not [ʿāšaq](../../strongs/h/h6231.md) a [śāḵîr](../../strongs/h/h7916.md) that is ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md), whether he be of thy ['ach](../../strongs/h/h251.md), or of thy [ger](../../strongs/h/h1616.md) that are in thy ['erets](../../strongs/h/h776.md) within thy [sha'ar](../../strongs/h/h8179.md):

<a name="deuteronomy_24_15"></a>Deuteronomy 24:15

At his [yowm](../../strongs/h/h3117.md) thou shalt [nathan](../../strongs/h/h5414.md) him his [śāḵār](../../strongs/h/h7939.md), neither shall the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md) upon it; for he is ['aniy](../../strongs/h/h6041.md), and [nasa'](../../strongs/h/h5375.md) his [nephesh](../../strongs/h/h5315.md) upon it: lest he [qara'](../../strongs/h/h7121.md) against thee unto [Yĕhovah](../../strongs/h/h3068.md), and it be [ḥēṭĕ'](../../strongs/h/h2399.md) unto thee.

<a name="deuteronomy_24_16"></a>Deuteronomy 24:16

The ['ab](../../strongs/h/h1.md) shall not be [muwth](../../strongs/h/h4191.md) for the [ben](../../strongs/h/h1121.md), neither shall the [ben](../../strongs/h/h1121.md) be [muwth](../../strongs/h/h4191.md) for the ['ab](../../strongs/h/h1.md): every ['iysh](../../strongs/h/h376.md) shall be [muwth](../../strongs/h/h4191.md) for his own [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="deuteronomy_24_17"></a>Deuteronomy 24:17

Thou shalt not [natah](../../strongs/h/h5186.md) the [mishpat](../../strongs/h/h4941.md) of the [ger](../../strongs/h/h1616.md), nor of the [yathowm](../../strongs/h/h3490.md); nor [chabal](../../strongs/h/h2254.md) an ['almānâ](../../strongs/h/h490.md) [beḡeḏ](../../strongs/h/h899.md):

<a name="deuteronomy_24_18"></a>Deuteronomy 24:18

But thou shalt [zakar](../../strongs/h/h2142.md) that thou wast an ['ebed](../../strongs/h/h5650.md) in [Mitsrayim](../../strongs/h/h4714.md), and [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [pāḏâ](../../strongs/h/h6299.md) thee thence: therefore I [tsavah](../../strongs/h/h6680.md) thee to ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="deuteronomy_24_19"></a>Deuteronomy 24:19

When thou [qāṣar](../../strongs/h/h7114.md) thine [qāṣîr](../../strongs/h/h7105.md) in thy [sadeh](../../strongs/h/h7704.md), and hast [shakach](../../strongs/h/h7911.md) an [ʿōmer](../../strongs/h/h6016.md) in the [sadeh](../../strongs/h/h7704.md), thou shalt not [shuwb](../../strongs/h/h7725.md) to [laqach](../../strongs/h/h3947.md) it: it shall be for the [ger](../../strongs/h/h1616.md), for the [yathowm](../../strongs/h/h3490.md), and for the ['almānâ](../../strongs/h/h490.md): that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) may [barak](../../strongs/h/h1288.md) thee in all the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_24_20"></a>Deuteronomy 24:20

When thou [ḥāḇaṭ](../../strongs/h/h2251.md) thine [zayiṯ](../../strongs/h/h2132.md), thou shalt not [pā'ar](../../strongs/h/h6286.md) ['aḥar](../../strongs/h/h310.md): it shall be for the [ger](../../strongs/h/h1616.md), for the [yathowm](../../strongs/h/h3490.md), and for the ['almānâ](../../strongs/h/h490.md).

<a name="deuteronomy_24_21"></a>Deuteronomy 24:21

When thou [bāṣar](../../strongs/h/h1219.md) the [kerem](../../strongs/h/h3754.md), thou shalt not [ʿālal](../../strongs/h/h5953.md) it ['aḥar](../../strongs/h/h310.md): it shall be for the [ger](../../strongs/h/h1616.md), for the [yathowm](../../strongs/h/h3490.md), and for the ['almānâ](../../strongs/h/h490.md).

<a name="deuteronomy_24_22"></a>Deuteronomy 24:22

And thou shalt [zakar](../../strongs/h/h2142.md) that thou wast an ['ebed](../../strongs/h/h5650.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): therefore I [tsavah](../../strongs/h/h6680.md) thee to ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 23](deuteronomy_23.md) - [Deuteronomy 25](deuteronomy_25.md)