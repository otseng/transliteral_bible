# [Deuteronomy 13](https://www.blueletterbible.org/esv/deu/13)

<a name="deuteronomy_13_1"></a>Deuteronomy 13:1

If there [quwm](../../strongs/h/h6965.md) [qereḇ](../../strongs/h/h7130.md) you a [nāḇî'](../../strongs/h/h5030.md), or a [ḥālam](../../strongs/h/h2492.md) of [ḥălôm](../../strongs/h/h2472.md), and [nathan](../../strongs/h/h5414.md) thee an ['ôṯ](../../strongs/h/h226.md) or a [môp̄ēṯ](../../strongs/h/h4159.md),

<a name="deuteronomy_13_2"></a>Deuteronomy 13:2

And the ['ôṯ](../../strongs/h/h226.md) or the [môp̄ēṯ](../../strongs/h/h4159.md) [bow'](../../strongs/h/h935.md), whereof he [dabar](../../strongs/h/h1696.md) unto thee, ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), which thou hast not [yada'](../../strongs/h/h3045.md), and let us ['abad](../../strongs/h/h5647.md) them;

<a name="deuteronomy_13_3"></a>Deuteronomy 13:3

Thou shalt not [shama'](../../strongs/h/h8085.md) unto the [dabar](../../strongs/h/h1697.md) of that [nāḇî'](../../strongs/h/h5030.md), or that [ḥālam](../../strongs/h/h2492.md) of [ḥălôm](../../strongs/h/h2472.md): for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [nāsâ](../../strongs/h/h5254.md) you, to [yada'](../../strongs/h/h3045.md) whether ye ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) with all your [lebab](../../strongs/h/h3824.md) and with all your [nephesh](../../strongs/h/h5315.md).

<a name="deuteronomy_13_4"></a>Deuteronomy 13:4

Ye shall [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and [yare'](../../strongs/h/h3372.md) him, and [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), and [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), and ye shall ['abad](../../strongs/h/h5647.md) him, and [dāḇaq](../../strongs/h/h1692.md) unto him.

<a name="deuteronomy_13_5"></a>Deuteronomy 13:5

And that [nāḇî'](../../strongs/h/h5030.md), or that [ḥālam](../../strongs/h/h2492.md) of [ḥălôm](../../strongs/h/h2472.md), shall be [muwth](../../strongs/h/h4191.md); because he hath [dabar](../../strongs/h/h1696.md) to [sārâ](../../strongs/h/h5627.md) you from [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [pāḏâ](../../strongs/h/h6299.md) you out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md), to [nāḏaḥ](../../strongs/h/h5080.md) thee out of the [derek](../../strongs/h/h1870.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) thee to [yālaḵ](../../strongs/h/h3212.md) in. So shalt thou [bāʿar](../../strongs/h/h1197.md) the [ra'](../../strongs/h/h7451.md) away from the [qereḇ](../../strongs/h/h7130.md) of thee.

<a name="deuteronomy_13_6"></a>Deuteronomy 13:6

If thy ['ach](../../strongs/h/h251.md), the [ben](../../strongs/h/h1121.md) of thy ['em](../../strongs/h/h517.md), or thy [ben](../../strongs/h/h1121.md), or thy [bath](../../strongs/h/h1323.md), or the ['ishshah](../../strongs/h/h802.md) of thy [ḥêq](../../strongs/h/h2436.md), or thy [rea'](../../strongs/h/h7453.md), which is as thine own [nephesh](../../strongs/h/h5315.md), [sûṯ](../../strongs/h/h5496.md) thee [cether](../../strongs/h/h5643.md), ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), which thou hast not [yada'](../../strongs/h/h3045.md), thou, nor thy ['ab](../../strongs/h/h1.md);

<a name="deuteronomy_13_7"></a>Deuteronomy 13:7

of the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) which are [cabiyb](../../strongs/h/h5439.md) you, [qarowb](../../strongs/h/h7138.md) unto thee, or [rachowq](../../strongs/h/h7350.md) from thee, from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) even unto the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md);

<a name="deuteronomy_13_8"></a>Deuteronomy 13:8

Thou shalt not ['āḇâ](../../strongs/h/h14.md) unto him, nor [shama'](../../strongs/h/h8085.md) unto him; neither shall thine ['ayin](../../strongs/h/h5869.md) [ḥûs](../../strongs/h/h2347.md) him, neither shalt thou [ḥāmal](../../strongs/h/h2550.md), neither shalt thou [kāsâ](../../strongs/h/h3680.md) him:

<a name="deuteronomy_13_9"></a>Deuteronomy 13:9

But thou shalt [harag](../../strongs/h/h2026.md) [harag](../../strongs/h/h2026.md) him; thine [yad](../../strongs/h/h3027.md) shall be [ri'šôn](../../strongs/h/h7223.md) upon him to put him to [muwth](../../strongs/h/h4191.md), and ['aḥărôn](../../strongs/h/h314.md) the [yad](../../strongs/h/h3027.md) of all the ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_13_10"></a>Deuteronomy 13:10

And thou shalt [sāqal](../../strongs/h/h5619.md) him with ['eben](../../strongs/h/h68.md), that he [muwth](../../strongs/h/h4191.md); because he hath [bāqaš](../../strongs/h/h1245.md) to [nāḏaḥ](../../strongs/h/h5080.md) from [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md).

<a name="deuteronomy_13_11"></a>Deuteronomy 13:11

And all [Yisra'el](../../strongs/h/h3478.md) shall [shama'](../../strongs/h/h8085.md), and [yare'](../../strongs/h/h3372.md), and shall ['asah](../../strongs/h/h6213.md) no [yāsap̄](../../strongs/h/h3254.md) [dabar](../../strongs/h/h1697.md) such [ra'](../../strongs/h/h7451.md) as this is [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_13_12"></a>Deuteronomy 13:12

If thou shalt [shama'](../../strongs/h/h8085.md) say in ['echad](../../strongs/h/h259.md) of thy [ʿîr](../../strongs/h/h5892.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) thee to [yashab](../../strongs/h/h3427.md) there, ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_13_13"></a>Deuteronomy 13:13

Certain ['enowsh](../../strongs/h/h582.md), the [ben](../../strongs/h/h1121.md) of [Beliya'al](../../strongs/h/h1100.md), are [yāṣā'](../../strongs/h/h3318.md) from [qereḇ](../../strongs/h/h7130.md) you, and have [nāḏaḥ](../../strongs/h/h5080.md) the [yashab](../../strongs/h/h3427.md) of their [ʿîr](../../strongs/h/h5892.md), ['āmar](../../strongs/h/h559.md), Let us [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), which ye have not [yada'](../../strongs/h/h3045.md);

<a name="deuteronomy_13_14"></a>Deuteronomy 13:14

Then shalt thou [darash](../../strongs/h/h1875.md), and [chaqar](../../strongs/h/h2713.md), and [sha'al](../../strongs/h/h7592.md) [yatab](../../strongs/h/h3190.md); and, behold, if it be ['emeth](../../strongs/h/h571.md), and the [dabar](../../strongs/h/h1697.md) [kuwn](../../strongs/h/h3559.md), that such [tôʿēḇâ](../../strongs/h/h8441.md) is ['asah](../../strongs/h/h6213.md) [qereḇ](../../strongs/h/h7130.md) you;

<a name="deuteronomy_13_15"></a>Deuteronomy 13:15

Thou shalt [nakah](../../strongs/h/h5221.md) [nakah](../../strongs/h/h5221.md) the [yashab](../../strongs/h/h3427.md) of that [ʿîr](../../strongs/h/h5892.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), [ḥāram](../../strongs/h/h2763.md), and all that is therein, and the [bĕhemah](../../strongs/h/h929.md) thereof, with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="deuteronomy_13_16"></a>Deuteronomy 13:16

And thou shalt [qāḇaṣ](../../strongs/h/h6908.md) all the [šālāl](../../strongs/h/h7998.md) of it into the [tavek](../../strongs/h/h8432.md) of the [rᵊḥōḇ](../../strongs/h/h7339.md) thereof, and shalt [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md) the [ʿîr](../../strongs/h/h5892.md), and all the [šālāl](../../strongs/h/h7998.md) thereof every [kālîl](../../strongs/h/h3632.md), for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): and it shall be a [tēl](../../strongs/h/h8510.md) ['owlam](../../strongs/h/h5769.md); it shall not be [bānâ](../../strongs/h/h1129.md).

<a name="deuteronomy_13_17"></a>Deuteronomy 13:17

And there shall [dāḇaq](../../strongs/h/h1692.md) [mᵊ'ûmâ](../../strongs/h/h3972.md) of the [ḥērem](../../strongs/h/h2764.md) to thine [yad](../../strongs/h/h3027.md): that [Yĕhovah](../../strongs/h/h3068.md) may [shuwb](../../strongs/h/h7725.md) from the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md), and [nathan](../../strongs/h/h5414.md) thee [raḥam](../../strongs/h/h7356.md), and have [racham](../../strongs/h/h7355.md) upon thee, and [rabah](../../strongs/h/h7235.md) thee, as he hath [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md);

<a name="deuteronomy_13_18"></a>Deuteronomy 13:18

When thou shalt [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) all his [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), to ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 12](deuteronomy_12.md) - [Deuteronomy 14](deuteronomy_14.md)