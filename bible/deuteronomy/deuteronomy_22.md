# [Deuteronomy 22](https://www.blueletterbible.org/kjv/deu/22)

<a name="deuteronomy_22_1"></a>Deuteronomy 22:1

Thou shalt not [ra'ah](../../strongs/h/h7200.md) thy ['ach](../../strongs/h/h251.md) [showr](../../strongs/h/h7794.md) or his [śê](../../strongs/h/h7716.md) [nāḏaḥ](../../strongs/h/h5080.md), and ['alam](../../strongs/h/h5956.md) thyself from them: thou shalt [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) unto thy ['ach](../../strongs/h/h251.md).

<a name="deuteronomy_22_2"></a>Deuteronomy 22:2

And if thy ['ach](../../strongs/h/h251.md) be not [qarowb](../../strongs/h/h7138.md) unto thee, or if thou [yada'](../../strongs/h/h3045.md) him not, then thou shalt ['āsap̄](../../strongs/h/h622.md) it [tavek](../../strongs/h/h8432.md) thine own [bayith](../../strongs/h/h1004.md), and it shall be with thee until thy ['ach](../../strongs/h/h251.md) [darash](../../strongs/h/h1875.md) after it, and thou shalt [shuwb](../../strongs/h/h7725.md) it.

<a name="deuteronomy_22_3"></a>Deuteronomy 22:3

[kēn](../../strongs/h/h3651.md) shalt thou ['asah](../../strongs/h/h6213.md) with his [chamowr](../../strongs/h/h2543.md); and so shalt thou ['asah](../../strongs/h/h6213.md) with his [śimlâ](../../strongs/h/h8071.md); and with ['ăḇēḏâ](../../strongs/h/h9.md) of thy ['ach](../../strongs/h/h251.md), which he hath ['abad](../../strongs/h/h6.md), and thou hast [māṣā'](../../strongs/h/h4672.md), shalt thou ['asah](../../strongs/h/h6213.md) likewise: thou [yakol](../../strongs/h/h3201.md) not ['alam](../../strongs/h/h5956.md) thyself.

<a name="deuteronomy_22_4"></a>Deuteronomy 22:4

Thou shalt not [ra'ah](../../strongs/h/h7200.md) thy ['ach](../../strongs/h/h251.md) [chamowr](../../strongs/h/h2543.md) or his [showr](../../strongs/h/h7794.md) [naphal](../../strongs/h/h5307.md) by the [derek](../../strongs/h/h1870.md), and ['alam](../../strongs/h/h5956.md) thyself from them: thou shalt [quwm](../../strongs/h/h6965.md) [quwm](../../strongs/h/h6965.md).

<a name="deuteronomy_22_5"></a>Deuteronomy 22:5

The ['ishshah](../../strongs/h/h802.md) shall not [kĕliy](../../strongs/h/h3627.md) unto a [geḇer](../../strongs/h/h1397.md), neither shall a [geḇer](../../strongs/h/h1397.md) [labash](../../strongs/h/h3847.md) on an ['ishshah](../../strongs/h/h802.md) [śimlâ](../../strongs/h/h8071.md): for all that ['asah](../../strongs/h/h6213.md) so are [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_22_6"></a>Deuteronomy 22:6

If a [tsippowr](../../strongs/h/h6833.md) [qēn](../../strongs/h/h7064.md) [qārā'](../../strongs/h/h7122.md) to be [paniym](../../strongs/h/h6440.md) thee in the [derek](../../strongs/h/h1870.md) in any ['ets](../../strongs/h/h6086.md), or on the ['erets](../../strongs/h/h776.md), whether they be ['ep̄rōaḥ](../../strongs/h/h667.md), or [bêṣâ](../../strongs/h/h1000.md), and the ['em](../../strongs/h/h517.md) [rāḇaṣ](../../strongs/h/h7257.md) upon the ['ep̄rōaḥ](../../strongs/h/h667.md), or upon the [bêṣâ](../../strongs/h/h1000.md), thou shalt not [laqach](../../strongs/h/h3947.md) the ['em](../../strongs/h/h517.md) with the [ben](../../strongs/h/h1121.md):

<a name="deuteronomy_22_7"></a>Deuteronomy 22:7

But thou shalt [shalach](../../strongs/h/h7971.md) let the ['em](../../strongs/h/h517.md) [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) the [ben](../../strongs/h/h1121.md) to thee; that it may be [yatab](../../strongs/h/h3190.md) with thee, and that thou mayest ['arak](../../strongs/h/h748.md) thy [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_22_8"></a>Deuteronomy 22:8

When thou [bānâ](../../strongs/h/h1129.md) a [ḥāḏāš](../../strongs/h/h2319.md) [bayith](../../strongs/h/h1004.md), then thou shalt ['asah](../../strongs/h/h6213.md) a [maʿăqê](../../strongs/h/h4624.md) for thy [gāḡ](../../strongs/h/h1406.md), that thou [śûm](../../strongs/h/h7760.md) not [dam](../../strongs/h/h1818.md) upon thine [bayith](../../strongs/h/h1004.md), if any [naphal](../../strongs/h/h5307.md) [naphal](../../strongs/h/h5307.md) from thence.

<a name="deuteronomy_22_9"></a>Deuteronomy 22:9

Thou shalt not [zāraʿ](../../strongs/h/h2232.md) thy [kerem](../../strongs/h/h3754.md) with [kil'ayim](../../strongs/h/h3610.md): lest the [mᵊlē'â](../../strongs/h/h4395.md) of thy [zera'](../../strongs/h/h2233.md) which thou hast [zāraʿ](../../strongs/h/h2232.md), and the [tᵊḇû'â](../../strongs/h/h8393.md) of thy [kerem](../../strongs/h/h3754.md), be [qadash](../../strongs/h/h6942.md).

<a name="deuteronomy_22_10"></a>Deuteronomy 22:10

Thou shalt not [ḥāraš](../../strongs/h/h2790.md) with a [showr](../../strongs/h/h7794.md) and a [chamowr](../../strongs/h/h2543.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="deuteronomy_22_11"></a>Deuteronomy 22:11

Thou shalt not [labash](../../strongs/h/h3847.md) a [šaʿaṭnēz](../../strongs/h/h8162.md), as of [ṣemer](../../strongs/h/h6785.md) and [pēšeṯ](../../strongs/h/h6593.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="deuteronomy_22_12"></a>Deuteronomy 22:12

Thou shalt ['asah](../../strongs/h/h6213.md) thee [gāḏil](../../strongs/h/h1434.md) upon the four [kanaph](../../strongs/h/h3671.md) of thy [kᵊsûṯ](../../strongs/h/h3682.md), wherewith thou [kāsâ](../../strongs/h/h3680.md) thyself.

<a name="deuteronomy_22_13"></a>Deuteronomy 22:13

If any ['iysh](../../strongs/h/h376.md) [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md), and [bow'](../../strongs/h/h935.md) unto her, and [sane'](../../strongs/h/h8130.md) her,

<a name="deuteronomy_22_14"></a>Deuteronomy 22:14

And [śûm](../../strongs/h/h7760.md) ['aliylah](../../strongs/h/h5949.md) of [dabar](../../strongs/h/h1697.md) against her, and [yāṣā'](../../strongs/h/h3318.md) a [ra'](../../strongs/h/h7451.md) [shem](../../strongs/h/h8034.md) upon her, and ['āmar](../../strongs/h/h559.md), I [laqach](../../strongs/h/h3947.md) this ['ishshah](../../strongs/h/h802.md), and when I [qāraḇ](../../strongs/h/h7126.md) to her, I [māṣā'](../../strongs/h/h4672.md) her not a [bᵊṯûlîm](../../strongs/h/h1331.md):

<a name="deuteronomy_22_15"></a>Deuteronomy 22:15

Then shall the ['ab](../../strongs/h/h1.md) of the [naʿărâ](../../strongs/h/h5291.md), and her ['em](../../strongs/h/h517.md), [laqach](../../strongs/h/h3947.md) and [yāṣā'](../../strongs/h/h3318.md) the [naʿărâ](../../strongs/h/h5291.md) [bᵊṯûlîm](../../strongs/h/h1331.md) unto the [zāqēn](../../strongs/h/h2205.md) of the [ʿîr](../../strongs/h/h5892.md) in the [sha'ar](../../strongs/h/h8179.md):

<a name="deuteronomy_22_16"></a>Deuteronomy 22:16

And the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md) shall ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md), I [nathan](../../strongs/h/h5414.md) my [bath](../../strongs/h/h1323.md) unto this ['iysh](../../strongs/h/h376.md) to ['ishshah](../../strongs/h/h802.md), and he [sane'](../../strongs/h/h8130.md) her;

<a name="deuteronomy_22_17"></a>Deuteronomy 22:17

And, lo, he hath [śûm](../../strongs/h/h7760.md) ['aliylah](../../strongs/h/h5949.md) of [dabar](../../strongs/h/h1697.md) against her, ['āmar](../../strongs/h/h559.md), I [māṣā'](../../strongs/h/h4672.md) not thy [bath](../../strongs/h/h1323.md) a [bᵊṯûlîm](../../strongs/h/h1331.md); and yet these are the tokens of my [bath](../../strongs/h/h1323.md) [bᵊṯûlîm](../../strongs/h/h1331.md). And they shall [pāraś](../../strongs/h/h6566.md) the [śimlâ](../../strongs/h/h8071.md) [paniym](../../strongs/h/h6440.md) the [zāqēn](../../strongs/h/h2205.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="deuteronomy_22_18"></a>Deuteronomy 22:18

And the [zāqēn](../../strongs/h/h2205.md) of that [ʿîr](../../strongs/h/h5892.md) shall [laqach](../../strongs/h/h3947.md) that ['iysh](../../strongs/h/h376.md) and [yacar](../../strongs/h/h3256.md) him;

<a name="deuteronomy_22_19"></a>Deuteronomy 22:19

And they shall [ʿānaš](../../strongs/h/h6064.md) him in an hundred [keceph](../../strongs/h/h3701.md), and [nathan](../../strongs/h/h5414.md) them unto the ['ab](../../strongs/h/h1.md) of the [naʿărâ](../../strongs/h/h5291.md), because he hath [yāṣā'](../../strongs/h/h3318.md) a [ra'](../../strongs/h/h7451.md) [shem](../../strongs/h/h8034.md) upon a [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yisra'el](../../strongs/h/h3478.md): and she shall be his ['ishshah](../../strongs/h/h802.md); he [yakol](../../strongs/h/h3201.md) not [shalach](../../strongs/h/h7971.md) her all his [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_22_20"></a>Deuteronomy 22:20

But if this [dabar](../../strongs/h/h1697.md) be ['emeth](../../strongs/h/h571.md), and the [bᵊṯûlîm](../../strongs/h/h1331.md) be not [māṣā'](../../strongs/h/h4672.md) for the [naʿărâ](../../strongs/h/h5291.md):

<a name="deuteronomy_22_21"></a>Deuteronomy 22:21

Then they shall [yāṣā'](../../strongs/h/h3318.md) the [naʿărâ](../../strongs/h/h5291.md) to the [peṯaḥ](../../strongs/h/h6607.md) of her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and the ['enowsh](../../strongs/h/h582.md) of her [ʿîr](../../strongs/h/h5892.md) shall [sāqal](../../strongs/h/h5619.md) her with ['eben](../../strongs/h/h68.md) that she [muwth](../../strongs/h/h4191.md): because she hath ['asah](../../strongs/h/h6213.md) [nᵊḇālâ](../../strongs/h/h5039.md) in [Yisra'el](../../strongs/h/h3478.md), to [zānâ](../../strongs/h/h2181.md) in her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): so shalt thou [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_22_22"></a>Deuteronomy 22:22

If an ['iysh](../../strongs/h/h376.md) be [māṣā'](../../strongs/h/h4672.md) [shakab](../../strongs/h/h7901.md) with an ['ishshah](../../strongs/h/h802.md) [bāʿal](../../strongs/h/h1166.md) to a [baʿal](../../strongs/h/h1167.md), then they shall both of them [muwth](../../strongs/h/h4191.md), both the ['iysh](../../strongs/h/h376.md) that [shakab](../../strongs/h/h7901.md) with the ['ishshah](../../strongs/h/h802.md), and the ['ishshah](../../strongs/h/h802.md): so shalt thou [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_22_23"></a>Deuteronomy 22:23

If a [naʿărâ](../../strongs/h/h5291.md) that is a [bᵊṯûlâ](../../strongs/h/h1330.md) be ['āraś](../../strongs/h/h781.md) unto an ['iysh](../../strongs/h/h376.md), and an ['iysh](../../strongs/h/h376.md) [māṣā'](../../strongs/h/h4672.md) her in the [ʿîr](../../strongs/h/h5892.md), and [shakab](../../strongs/h/h7901.md) with her;

<a name="deuteronomy_22_24"></a>Deuteronomy 22:24

Then ye shall [yāṣā'](../../strongs/h/h3318.md) them both out unto the [sha'ar](../../strongs/h/h8179.md) of that [ʿîr](../../strongs/h/h5892.md), and ye shall [sāqal](../../strongs/h/h5619.md) them with ['eben](../../strongs/h/h68.md) that they [muwth](../../strongs/h/h4191.md); the [naʿărâ](../../strongs/h/h5291.md), [dabar](../../strongs/h/h1697.md) she [ṣāʿaq](../../strongs/h/h6817.md) not, being in the [ʿîr](../../strongs/h/h5892.md); and the ['iysh](../../strongs/h/h376.md), [dabar](../../strongs/h/h1697.md) he hath [ʿānâ](../../strongs/h/h6031.md) his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md): so thou shalt [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_22_25"></a>Deuteronomy 22:25

But if an ['iysh](../../strongs/h/h376.md) [māṣā'](../../strongs/h/h4672.md) an ['āraś](../../strongs/h/h781.md) [naʿărâ](../../strongs/h/h5291.md) in the [sadeh](../../strongs/h/h7704.md), and the ['iysh](../../strongs/h/h376.md) [ḥāzaq](../../strongs/h/h2388.md) her, and [shakab](../../strongs/h/h7901.md) with her: then the ['iysh](../../strongs/h/h376.md) only that [shakab](../../strongs/h/h7901.md) with her shall [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_22_26"></a>Deuteronomy 22:26

But unto the [naʿărâ](../../strongs/h/h5291.md) thou shalt ['asah](../../strongs/h/h6213.md) [dabar](../../strongs/h/h1697.md); there is in the [naʿărâ](../../strongs/h/h5291.md) no [ḥēṭĕ'](../../strongs/h/h2399.md) worthy of [maveth](../../strongs/h/h4194.md): for as when an ['iysh](../../strongs/h/h376.md) [quwm](../../strongs/h/h6965.md) against his [rea'](../../strongs/h/h7453.md), and [ratsach](../../strongs/h/h7523.md) [nephesh](../../strongs/h/h5315.md) him, even so is this [dabar](../../strongs/h/h1697.md):

<a name="deuteronomy_22_27"></a>Deuteronomy 22:27

For he [māṣā'](../../strongs/h/h4672.md) her in the [sadeh](../../strongs/h/h7704.md), and the ['āraś](../../strongs/h/h781.md) [naʿărâ](../../strongs/h/h5291.md) [ṣāʿaq](../../strongs/h/h6817.md), and there was none to [yasha'](../../strongs/h/h3467.md) her.

<a name="deuteronomy_22_28"></a>Deuteronomy 22:28

If an ['iysh](../../strongs/h/h376.md) [māṣā'](../../strongs/h/h4672.md) a [naʿărâ](../../strongs/h/h5291.md) that is a [bᵊṯûlâ](../../strongs/h/h1330.md), which is not ['āraś](../../strongs/h/h781.md), and [tāp̄aś](../../strongs/h/h8610.md) on her, and [shakab](../../strongs/h/h7901.md) with her, and they be [māṣā'](../../strongs/h/h4672.md);

<a name="deuteronomy_22_29"></a>Deuteronomy 22:29

Then the ['iysh](../../strongs/h/h376.md) that [shakab](../../strongs/h/h7901.md) with her shall [nathan](../../strongs/h/h5414.md) unto the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md) fifty [keceph](../../strongs/h/h3701.md), and she shall be his ['ishshah](../../strongs/h/h802.md); because he hath [ʿānâ](../../strongs/h/h6031.md) her, he [yakol](../../strongs/h/h3201.md) not [shalach](../../strongs/h/h7971.md) her all his [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_22_30"></a>Deuteronomy 22:30

an ['iysh](../../strongs/h/h376.md) shall not [laqach](../../strongs/h/h3947.md) his ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md), nor [gālâ](../../strongs/h/h1540.md) his ['ab](../../strongs/h/h1.md) [kanaph](../../strongs/h/h3671.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 21](deuteronomy_21.md) - [Deuteronomy 23](deuteronomy_23.md)