# [Deuteronomy 4](https://www.blueletterbible.org/kjv/deu/4)

<a name="deuteronomy_4_1"></a>Deuteronomy 4:1

Now therefore [shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md), unto the [choq](../../strongs/h/h2706.md) and unto the [mishpat](../../strongs/h/h4941.md), which I [lamad](../../strongs/h/h3925.md) you, for to ['asah](../../strongs/h/h6213.md) them, that ye may [ḥāyâ](../../strongs/h/h2421.md), and [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) you.

<a name="deuteronomy_4_2"></a>Deuteronomy 4:2

Ye shall not [yāsap̄](../../strongs/h/h3254.md) unto the [dabar](../../strongs/h/h1697.md) which I [tsavah](../../strongs/h/h6680.md) you, neither shall ye [gāraʿ](../../strongs/h/h1639.md) ought from it, that ye may [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) which I [tsavah](../../strongs/h/h6680.md) you.

<a name="deuteronomy_4_3"></a>Deuteronomy 4:3

Your ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) what [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) because of [Baʿal pᵊʿôr](../../strongs/h/h1187.md): for all the ['iysh](../../strongs/h/h376.md) that ['aḥar](../../strongs/h/h310.md) [halak](../../strongs/h/h1980.md) [Baʿal pᵊʿôr](../../strongs/h/h1187.md), [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [šāmaḏ](../../strongs/h/h8045.md) them from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_4_4"></a>Deuteronomy 4:4

But ye that did [dāḇēq](../../strongs/h/h1695.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) are [chay](../../strongs/h/h2416.md) every one of you this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_4_5"></a>Deuteronomy 4:5

[ra'ah](../../strongs/h/h7200.md), I have [lamad](../../strongs/h/h3925.md) you [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md), even as [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) me, that ye should ['asah](../../strongs/h/h6213.md) so [qereḇ](../../strongs/h/h7130.md) the ['erets](../../strongs/h/h776.md) whither ye [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_4_6"></a>Deuteronomy 4:6

[shamar](../../strongs/h/h8104.md) therefore and ['asah](../../strongs/h/h6213.md) them; for this is your [ḥāḵmâ](../../strongs/h/h2451.md) and your [bînâ](../../strongs/h/h998.md) in the ['ayin](../../strongs/h/h5869.md) of the ['am](../../strongs/h/h5971.md), which shall [shama'](../../strongs/h/h8085.md) all these [choq](../../strongs/h/h2706.md), and ['āmar](../../strongs/h/h559.md), Surely this [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) is a [ḥāḵām](../../strongs/h/h2450.md) and [bîn](../../strongs/h/h995.md) ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_4_7"></a>Deuteronomy 4:7

For what [gowy](../../strongs/h/h1471.md) is there so [gadowl](../../strongs/h/h1419.md), who hath ['Elohiym](../../strongs/h/h430.md) so [qarowb](../../strongs/h/h7138.md) unto them, as [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) is in that we [qara'](../../strongs/h/h7121.md) upon him?

<a name="deuteronomy_4_8"></a>Deuteronomy 4:8

And what [gowy](../../strongs/h/h1471.md) is there so [gadowl](../../strongs/h/h1419.md), that hath [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md) so [tsaddiyq](../../strongs/h/h6662.md) as all this [towrah](../../strongs/h/h8451.md), which I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you this [yowm](../../strongs/h/h3117.md)?

<a name="deuteronomy_4_9"></a>Deuteronomy 4:9

Only [shamar](../../strongs/h/h8104.md) to thyself, and [shamar](../../strongs/h/h8104.md) thy [nephesh](../../strongs/h/h5315.md) [me'od](../../strongs/h/h3966.md), lest thou [shakach](../../strongs/h/h7911.md) the [dabar](../../strongs/h/h1697.md) which thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md), and lest they [cuwr](../../strongs/h/h5493.md) from thy [lebab](../../strongs/h/h3824.md) all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md): but [yada'](../../strongs/h/h3045.md) them thy [ben](../../strongs/h/h1121.md), and thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md);

<a name="deuteronomy_4_10"></a>Deuteronomy 4:10

the [yowm](../../strongs/h/h3117.md) that thou ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in [Hōrēḇ](../../strongs/h/h2722.md), when [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [qāhal](../../strongs/h/h6950.md) me the ['am](../../strongs/h/h5971.md), and I will make them [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md), that they may [lamad](../../strongs/h/h3925.md) to [yare'](../../strongs/h/h3372.md) me all the [yowm](../../strongs/h/h3117.md) that they shall [chay](../../strongs/h/h2416.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), and that they may [lamad](../../strongs/h/h3925.md) their [ben](../../strongs/h/h1121.md).

<a name="deuteronomy_4_11"></a>Deuteronomy 4:11

And ye [qāraḇ](../../strongs/h/h7126.md) and ['amad](../../strongs/h/h5975.md) under the [har](../../strongs/h/h2022.md); and the [har](../../strongs/h/h2022.md) [bāʿar](../../strongs/h/h1197.md) with ['esh](../../strongs/h/h784.md) unto the [leb](../../strongs/h/h3820.md) of [shamayim](../../strongs/h/h8064.md), with ['araphel](../../strongs/h/h6205.md), [ʿānān](../../strongs/h/h6051.md), and [choshek](../../strongs/h/h2822.md).

<a name="deuteronomy_4_12"></a>Deuteronomy 4:12

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto you out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md): ye [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [dabar](../../strongs/h/h1697.md), but [ra'ah](../../strongs/h/h7200.md) no [tĕmuwnah](../../strongs/h/h8544.md); [zûlâ](../../strongs/h/h2108.md) a [qowl](../../strongs/h/h6963.md).

<a name="deuteronomy_4_13"></a>Deuteronomy 4:13

And he [nāḡaḏ](../../strongs/h/h5046.md) unto you his [bĕriyth](../../strongs/h/h1285.md), which he [tsavah](../../strongs/h/h6680.md) you to ['asah](../../strongs/h/h6213.md), even [ʿeśer](../../strongs/h/h6235.md) [dabar](../../strongs/h/h1697.md); and he [kāṯaḇ](../../strongs/h/h3789.md) them upon [šᵊnayim](../../strongs/h/h8147.md) [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md).

<a name="deuteronomy_4_14"></a>Deuteronomy 4:14

And [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) me at that [ʿēṯ](../../strongs/h/h6256.md) to [lamad](../../strongs/h/h3925.md) you [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md), that ye might ['asah](../../strongs/h/h6213.md) them in the ['erets](../../strongs/h/h776.md) whither ye ['abar](../../strongs/h/h5674.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_4_15"></a>Deuteronomy 4:15

Take ye therefore [me'od](../../strongs/h/h3966.md) [shamar](../../strongs/h/h8104.md) unto [nephesh](../../strongs/h/h5315.md); for ye [ra'ah](../../strongs/h/h7200.md) no manner of [tĕmuwnah](../../strongs/h/h8544.md) on the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto you in [Hōrēḇ](../../strongs/h/h2722.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md):

<a name="deuteronomy_4_16"></a>Deuteronomy 4:16

Lest ye [shachath](../../strongs/h/h7843.md) yourselves, and ['asah](../../strongs/h/h6213.md) you a [pecel](../../strongs/h/h6459.md), the [tĕmuwnah](../../strongs/h/h8544.md) of any [semel](../../strongs/h/h5566.md), the [taḇnîṯ](../../strongs/h/h8403.md) of [zāḵār](../../strongs/h/h2145.md) or [nᵊqēḇâ](../../strongs/h/h5347.md),

<a name="deuteronomy_4_17"></a>Deuteronomy 4:17

The [taḇnîṯ](../../strongs/h/h8403.md) of any [bĕhemah](../../strongs/h/h929.md) that is on the ['erets](../../strongs/h/h776.md), the [taḇnîṯ](../../strongs/h/h8403.md) of any [kanaph](../../strongs/h/h3671.md) [tsippowr](../../strongs/h/h6833.md) that ['uwph](../../strongs/h/h5774.md) in the [shamayim](../../strongs/h/h8064.md),

<a name="deuteronomy_4_18"></a>Deuteronomy 4:18

The [taḇnîṯ](../../strongs/h/h8403.md) of any thing that [rāmaś](../../strongs/h/h7430.md) on the ['adamah](../../strongs/h/h127.md), the [taḇnîṯ](../../strongs/h/h8403.md) of any [dāḡâ](../../strongs/h/h1710.md) that is in the [mayim](../../strongs/h/h4325.md) [taḥaṯ](../../strongs/h/h8478.md) the ['erets](../../strongs/h/h776.md):

<a name="deuteronomy_4_19"></a>Deuteronomy 4:19

And lest thou [nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) unto [shamayim](../../strongs/h/h8064.md), and when thou [ra'ah](../../strongs/h/h7200.md) the [šemeš](../../strongs/h/h8121.md), and the [yareach](../../strongs/h/h3394.md), and the [kowkab](../../strongs/h/h3556.md), even all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), shouldest be [nāḏaḥ](../../strongs/h/h5080.md) to [shachah](../../strongs/h/h7812.md) them, and ['abad](../../strongs/h/h5647.md) them, which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [chalaq](../../strongs/h/h2505.md) unto all ['am](../../strongs/h/h5971.md) under the [shamayim](../../strongs/h/h8064.md).

<a name="deuteronomy_4_20"></a>Deuteronomy 4:20

But [Yĕhovah](../../strongs/h/h3068.md) hath [laqach](../../strongs/h/h3947.md) you, and [yāṣā'](../../strongs/h/h3318.md) you out of the [barzel](../../strongs/h/h1270.md) [kûr](../../strongs/h/h3564.md), even out of [Mitsrayim](../../strongs/h/h4714.md), to be unto him an ['am](../../strongs/h/h5971.md) of [nachalah](../../strongs/h/h5159.md), as ye are this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_4_21"></a>Deuteronomy 4:21

Furthermore [Yĕhovah](../../strongs/h/h3068.md) was ['anaph](../../strongs/h/h599.md) with me for your [dabar](../../strongs/h/h1697.md), and [shaba'](../../strongs/h/h7650.md) that I should not ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and that I should not [bow'](../../strongs/h/h935.md) unto that [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md):

<a name="deuteronomy_4_22"></a>Deuteronomy 4:22

But I must [muwth](../../strongs/h/h4191.md) in this ['erets](../../strongs/h/h776.md), I must not ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md): but ye shall ['abar](../../strongs/h/h5674.md), and [yarash](../../strongs/h/h3423.md) that [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_4_23"></a>Deuteronomy 4:23

[shamar](../../strongs/h/h8104.md) unto yourselves, lest ye [shakach](../../strongs/h/h7911.md) the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which he [karath](../../strongs/h/h3772.md) with you, and ['asah](../../strongs/h/h6213.md) you a [pecel](../../strongs/h/h6459.md), or the [tĕmuwnah](../../strongs/h/h8544.md) of [kōl](../../strongs/h/h3605.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thee.

<a name="deuteronomy_4_24"></a>Deuteronomy 4:24

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is an ['akal](../../strongs/h/h398.md) ['esh](../../strongs/h/h784.md), even a [qanna'](../../strongs/h/h7067.md) ['el](../../strongs/h/h410.md).

<a name="deuteronomy_4_25"></a>Deuteronomy 4:25

When thou shalt [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md), and [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), and ye shall have [yashen](../../strongs/h/h3462.md) in the ['erets](../../strongs/h/h776.md), and shall [shachath](../../strongs/h/h7843.md), and ['asah](../../strongs/h/h6213.md) a [pecel](../../strongs/h/h6459.md), or the [tĕmuwnah](../../strongs/h/h8544.md) of any, and shall ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [kāʿas](../../strongs/h/h3707.md):

<a name="deuteronomy_4_26"></a>Deuteronomy 4:26

I [ʿûḏ](../../strongs/h/h5749.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md) against you this [yowm](../../strongs/h/h3117.md), that ye shall [mahēr](../../strongs/h/h4118.md) ['abad](../../strongs/h/h6.md) ['abad](../../strongs/h/h6.md) from off the ['erets](../../strongs/h/h776.md) whereunto ye ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [yarash](../../strongs/h/h3423.md) it; ye shall not ['arak](../../strongs/h/h748.md) your [yowm](../../strongs/h/h3117.md) upon it, but shall be [šāmaḏ](../../strongs/h/h8045.md) [šāmaḏ](../../strongs/h/h8045.md).

<a name="deuteronomy_4_27"></a>Deuteronomy 4:27

And [Yĕhovah](../../strongs/h/h3068.md) shall [puwts](../../strongs/h/h6327.md) you among the ['am](../../strongs/h/h5971.md), and ye shall be [šā'ar](../../strongs/h/h7604.md) [math](../../strongs/h/h4962.md) in [mispār](../../strongs/h/h4557.md) among the [gowy](../../strongs/h/h1471.md), whither [Yĕhovah](../../strongs/h/h3068.md) shall [nāhaḡ](../../strongs/h/h5090.md) you.

<a name="deuteronomy_4_28"></a>Deuteronomy 4:28

And there ye shall ['abad](../../strongs/h/h5647.md) ['Elohiym](../../strongs/h/h430.md), the [ma'aseh](../../strongs/h/h4639.md) of ['adam](../../strongs/h/h120.md) [yad](../../strongs/h/h3027.md), ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md), which neither [ra'ah](../../strongs/h/h7200.md), nor [shama'](../../strongs/h/h8085.md), nor ['akal](../../strongs/h/h398.md), nor [rîaḥ](../../strongs/h/h7306.md).

<a name="deuteronomy_4_29"></a>Deuteronomy 4:29

But if from thence thou shalt [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), thou shalt [māṣā'](../../strongs/h/h4672.md) him, if thou [darash](../../strongs/h/h1875.md) him with all thy [lebab](../../strongs/h/h3824.md) and with all thy [nephesh](../../strongs/h/h5315.md).

<a name="deuteronomy_4_30"></a>Deuteronomy 4:30

When thou art in [tsar](../../strongs/h/h6862.md), and all these [dabar](../../strongs/h/h1697.md) are [māṣā'](../../strongs/h/h4672.md) upon thee, even in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md), if thou [shuwb](../../strongs/h/h7725.md) to [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and shalt [shama'](../../strongs/h/h8085.md) unto his [qowl](../../strongs/h/h6963.md);

<a name="deuteronomy_4_31"></a>Deuteronomy 4:31

(For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is a [raḥwm](../../strongs/h/h7349.md) ['el](../../strongs/h/h410.md);) he will not [rāp̄â](../../strongs/h/h7503.md) thee, neither [shachath](../../strongs/h/h7843.md) thee, nor [shakach](../../strongs/h/h7911.md) the [bĕriyth](../../strongs/h/h1285.md) of thy ['ab](../../strongs/h/h1.md) which he [shaba'](../../strongs/h/h7650.md) unto them.

<a name="deuteronomy_4_32"></a>Deuteronomy 4:32

For [sha'al](../../strongs/h/h7592.md) now of the [yowm](../../strongs/h/h3117.md) that are [ri'šôn](../../strongs/h/h7223.md), which were [paniym](../../strongs/h/h6440.md) thee, since the [yowm](../../strongs/h/h3117.md) that ['Elohiym](../../strongs/h/h430.md) [bara'](../../strongs/h/h1254.md) ['adam](../../strongs/h/h120.md) upon the ['erets](../../strongs/h/h776.md), and ask from the [qāṣê](../../strongs/h/h7097.md) of [shamayim](../../strongs/h/h8064.md) unto the other, whether there hath been any such thing as this [gadowl](../../strongs/h/h1419.md) [dabar](../../strongs/h/h1697.md) is, or hath been [shama'](../../strongs/h/h8085.md) like it?

<a name="deuteronomy_4_33"></a>Deuteronomy 4:33

Did ever ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md), as thou hast [shama'](../../strongs/h/h8085.md), and [ḥāyâ](../../strongs/h/h2421.md)?

<a name="deuteronomy_4_34"></a>Deuteronomy 4:34

Or hath ['Elohiym](../../strongs/h/h430.md) [nāsâ](../../strongs/h/h5254.md) to [bow'](../../strongs/h/h935.md) and [laqach](../../strongs/h/h3947.md) him a [gowy](../../strongs/h/h1471.md) from the [qereḇ](../../strongs/h/h7130.md) of another [gowy](../../strongs/h/h1471.md), by [massâ](../../strongs/h/h4531.md), by ['ôṯ](../../strongs/h/h226.md), and by [môp̄ēṯ](../../strongs/h/h4159.md), and by [milḥāmâ](../../strongs/h/h4421.md), and by a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and by a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and by [gadowl](../../strongs/h/h1419.md) [mowra'](../../strongs/h/h4172.md), according to all that [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) for you in [Mitsrayim](../../strongs/h/h4714.md) before your ['ayin](../../strongs/h/h5869.md)?

<a name="deuteronomy_4_35"></a>Deuteronomy 4:35

Unto thee it was [ra'ah](../../strongs/h/h7200.md), that thou mightest [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) he is ['Elohiym](../../strongs/h/h430.md); there is [ʿôḏ](../../strongs/h/h5750.md) [baḏ](../../strongs/h/h905.md).

<a name="deuteronomy_4_36"></a>Deuteronomy 4:36

Out of [shamayim](../../strongs/h/h8064.md) he made thee to [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), that he might [yacar](../../strongs/h/h3256.md) thee: and upon ['erets](../../strongs/h/h776.md) he [ra'ah](../../strongs/h/h7200.md) thee his [gadowl](../../strongs/h/h1419.md) ['esh](../../strongs/h/h784.md); and thou [shama'](../../strongs/h/h8085.md) his [dabar](../../strongs/h/h1697.md) out of the [tavek](../../strongs/h/h8432.md) of the ['esh](../../strongs/h/h784.md).

<a name="deuteronomy_4_37"></a>Deuteronomy 4:37

And because he ['ahab](../../strongs/h/h157.md) thy ['ab](../../strongs/h/h1.md), therefore he [bāḥar](../../strongs/h/h977.md) their [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) them, and [yāṣā'](../../strongs/h/h3318.md) thee in his [paniym](../../strongs/h/h6440.md) with his [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) out of [Mitsrayim](../../strongs/h/h4714.md);

<a name="deuteronomy_4_38"></a>Deuteronomy 4:38

To [yarash](../../strongs/h/h3423.md) [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) thee [gadowl](../../strongs/h/h1419.md) and ['atsuwm](../../strongs/h/h6099.md) than thou, to [bow'](../../strongs/h/h935.md) thee in, to [nathan](../../strongs/h/h5414.md) thee their ['erets](../../strongs/h/h776.md) for a [nachalah](../../strongs/h/h5159.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_4_39"></a>Deuteronomy 4:39

[yada'](../../strongs/h/h3045.md) therefore this [yowm](../../strongs/h/h3117.md), and [shuwb](../../strongs/h/h7725.md) it in thine [lebab](../../strongs/h/h3824.md), that [Yĕhovah](../../strongs/h/h3068.md) he is ['Elohiym](../../strongs/h/h430.md) in [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md), and upon the ['erets](../../strongs/h/h776.md) beneath: there is none else.

<a name="deuteronomy_4_40"></a>Deuteronomy 4:40

Thou shalt [shamar](../../strongs/h/h8104.md) therefore his [choq](../../strongs/h/h2706.md), and his [mitsvah](../../strongs/h/h4687.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), that it may [yatab](../../strongs/h/h3190.md) with thee, and with thy [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) thee, and that thou mayest ['arak](../../strongs/h/h748.md) thy [yowm](../../strongs/h/h3117.md) upon the ['ăḏāmâ](../../strongs/h/h127.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, for ever.

<a name="deuteronomy_4_41"></a>Deuteronomy 4:41

Then [Mōshe](../../strongs/h/h4872.md) [bāḏal](../../strongs/h/h914.md) three [ʿîr](../../strongs/h/h5892.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) toward the [šemeš](../../strongs/h/h8121.md) [mizrach](../../strongs/h/h4217.md);

<a name="deuteronomy_4_42"></a>Deuteronomy 4:42

That the [ratsach](../../strongs/h/h7523.md) might [nûs](../../strongs/h/h5127.md) thither, which should [ratsach](../../strongs/h/h7523.md) his [rea'](../../strongs/h/h7453.md) [bᵊlî](../../strongs/h/h1097.md) [da'ath](../../strongs/h/h1847.md), and [sane'](../../strongs/h/h8130.md) him not in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md); and that [nûs](../../strongs/h/h5127.md) unto one of these [ʿîr](../../strongs/h/h5892.md) he might [chayay](../../strongs/h/h2425.md):

<a name="deuteronomy_4_43"></a>Deuteronomy 4:43

Namely, [Beṣer](../../strongs/h/h1221.md) in the [midbar](../../strongs/h/h4057.md), in the [mîšôr](../../strongs/h/h4334.md) ['erets](../../strongs/h/h776.md), of the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md); and [Rā'Môṯ](../../strongs/h/h7216.md) in [Gilʿāḏ](../../strongs/h/h1568.md), of the [Gāḏî](../../strongs/h/h1425.md); and [Gôlān](../../strongs/h/h1474.md) in [Bāšān](../../strongs/h/h1316.md), of the [Mᵊnaššê](../../strongs/h/h4520.md).

<a name="deuteronomy_4_44"></a>Deuteronomy 4:44

And this is the [towrah](../../strongs/h/h8451.md) which [Mōshe](../../strongs/h/h4872.md) [śûm](../../strongs/h/h7760.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="deuteronomy_4_45"></a>Deuteronomy 4:45

These are the [ʿēḏâ](../../strongs/h/h5713.md), and the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), which [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), after they [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_4_46"></a>Deuteronomy 4:46

On this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), in the [gay'](../../strongs/h/h1516.md) over [môl](../../strongs/h/h4136.md) [Bêṯ PᵊʿÔr](../../strongs/h/h1047.md), in the ['erets](../../strongs/h/h776.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), who [yashab](../../strongs/h/h3427.md) at [Hešbôn](../../strongs/h/h2809.md), whom [Mōshe](../../strongs/h/h4872.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md), after they were [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md):

<a name="deuteronomy_4_47"></a>Deuteronomy 4:47

And they [yarash](../../strongs/h/h3423.md) his ['erets](../../strongs/h/h776.md), and the ['erets](../../strongs/h/h776.md) of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), two [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which were on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md);

<a name="deuteronomy_4_48"></a>Deuteronomy 4:48

From [ʿĂrôʿēr](../../strongs/h/h6177.md), which is by the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) ['Arnôn](../../strongs/h/h769.md), even unto [har](../../strongs/h/h2022.md) [Śî'ôn](../../strongs/h/h7865.md), which is [Ḥermôn](../../strongs/h/h2768.md),

<a name="deuteronomy_4_49"></a>Deuteronomy 4:49

And all the ['arabah](../../strongs/h/h6160.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [mizrach](../../strongs/h/h4217.md), even unto the [yam](../../strongs/h/h3220.md) of the ['arabah](../../strongs/h/h6160.md), under the ['ăšēḏâ](../../strongs/h/h794.md) of [Pisgâ](../../strongs/h/h6449.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 3](deuteronomy_3.md) - [Deuteronomy 5](deuteronomy_5.md)