# [Deuteronomy 1](https://www.blueletterbible.org/kjv/deu/1)

<a name="deuteronomy_1_1"></a>Deuteronomy 1:1

These be the [dabar](../../strongs/h/h1697.md) which [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto all [Yisra'el](../../strongs/h/h3478.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) in the [midbar](../../strongs/h/h4057.md), in the ['arabah](../../strongs/h/h6160.md) over [môl](../../strongs/h/h4136.md) the [Sûp̄](../../strongs/h/h5489.md), between [Pā'rān](../../strongs/h/h6290.md), and [Tōp̄El](../../strongs/h/h8603.md), and [Lāḇān](../../strongs/h/h3837.md), and [Ḥăṣērôṯ](../../strongs/h/h2698.md), and [Dî Zāhāḇ](../../strongs/h/h1774.md).

<a name="deuteronomy_1_2"></a>Deuteronomy 1:2

(There are eleven [yowm](../../strongs/h/h3117.md) journey from [Hōrēḇ](../../strongs/h/h2722.md) by the [derek](../../strongs/h/h1870.md) of [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md) unto [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md).)

<a name="deuteronomy_1_3"></a>Deuteronomy 1:3

And it came to pass in the fortieth [šānâ](../../strongs/h/h8141.md), in the eleventh [ḥōḏeš](../../strongs/h/h2320.md), on the first of the [ḥōḏeš](../../strongs/h/h2320.md), that [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), according unto all that [Yĕhovah](../../strongs/h/h3068.md) had given him in [tsavah](../../strongs/h/h6680.md) unto them;

<a name="deuteronomy_1_4"></a>Deuteronomy 1:4

['aḥar](../../strongs/h/h310.md) he had [nakah](../../strongs/h/h5221.md) [Sîḥôn](../../strongs/h/h5511.md) the [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [yashab](../../strongs/h/h3427.md) in [Hešbôn](../../strongs/h/h2809.md), and [ʿÔḡ](../../strongs/h/h5747.md) the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), which [yashab](../../strongs/h/h3427.md) at [ʿAštārōṯ](../../strongs/h/h6252.md) in ['Eḏrᵊʿî](../../strongs/h/h154.md):

<a name="deuteronomy_1_5"></a>Deuteronomy 1:5

On this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), in the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), [yā'al](../../strongs/h/h2974.md) [Mōshe](../../strongs/h/h4872.md) to [bā'ar](../../strongs/h/h874.md) this [towrah](../../strongs/h/h8451.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_1_6"></a>Deuteronomy 1:6

[Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) unto us in [Hōrēḇ](../../strongs/h/h2722.md), ['āmar](../../strongs/h/h559.md), Ye have [yashab](../../strongs/h/h3427.md) long [rab](../../strongs/h/h7227.md) in this [har](../../strongs/h/h2022.md):

<a name="deuteronomy_1_7"></a>Deuteronomy 1:7

[panah](../../strongs/h/h6437.md) you, and [nāsaʿ](../../strongs/h/h5265.md), and [bow'](../../strongs/h/h935.md) to the [har](../../strongs/h/h2022.md) of the ['Ĕmōrî](../../strongs/h/h567.md), and unto all the [šāḵēn](../../strongs/h/h7934.md) thereunto, in the ['arabah](../../strongs/h/h6160.md), in the [har](../../strongs/h/h2022.md), and in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the [neḡeḇ](../../strongs/h/h5045.md), and by the [yam](../../strongs/h/h3220.md) [ḥôp̄](../../strongs/h/h2348.md), to the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), and unto [Lᵊḇānôn](../../strongs/h/h3844.md), unto the [gadowl](../../strongs/h/h1419.md) [nāhār](../../strongs/h/h5104.md), the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="deuteronomy_1_8"></a>Deuteronomy 1:8

[ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) you: [bow'](../../strongs/h/h935.md) in and [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md), ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Ya'aqob](../../strongs/h/h3290.md), to [nathan](../../strongs/h/h5414.md) unto them and to their [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="deuteronomy_1_9"></a>Deuteronomy 1:9

And I ['āmar](../../strongs/h/h559.md) unto you at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md), I am not [yakol](../../strongs/h/h3201.md) to [nasa'](../../strongs/h/h5375.md) you myself [baḏ](../../strongs/h/h905.md):

<a name="deuteronomy_1_10"></a>Deuteronomy 1:10

[Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [rabah](../../strongs/h/h7235.md) you, and, behold, ye are this [yowm](../../strongs/h/h3117.md) as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="deuteronomy_1_11"></a>Deuteronomy 1:11

([Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) make you a thousand [pa'am](../../strongs/h/h6471.md) [yāsap̄](../../strongs/h/h3254.md) as ye are, and [barak](../../strongs/h/h1288.md) you, as he hath [dabar](../../strongs/h/h1696.md) you!)

<a name="deuteronomy_1_12"></a>Deuteronomy 1:12

['êḵ](../../strongs/h/h349.md) can I myself alone [nasa'](../../strongs/h/h5375.md) your [ṭōraḥ](../../strongs/h/h2960.md), and your [maśśā'](../../strongs/h/h4853.md), and your [rîḇ](../../strongs/h/h7379.md)?

<a name="deuteronomy_1_13"></a>Deuteronomy 1:13

[yāhaḇ](../../strongs/h/h3051.md) you [ḥāḵām](../../strongs/h/h2450.md) ['enowsh](../../strongs/h/h582.md), and [bîn](../../strongs/h/h995.md), and [yada'](../../strongs/h/h3045.md) among your [shebet](../../strongs/h/h7626.md), and I will [śûm](../../strongs/h/h7760.md) them [ro'sh](../../strongs/h/h7218.md) over you.

<a name="deuteronomy_1_14"></a>Deuteronomy 1:14

And ye ['anah](../../strongs/h/h6030.md) me, and ['āmar](../../strongs/h/h559.md), The [dabar](../../strongs/h/h1697.md) which thou hast [dabar](../../strongs/h/h1696.md) is [towb](../../strongs/h/h2896.md) for us to ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_1_15"></a>Deuteronomy 1:15

So I [laqach](../../strongs/h/h3947.md) the [ro'sh](../../strongs/h/h7218.md) of your [shebet](../../strongs/h/h7626.md), [ḥāḵām](../../strongs/h/h2450.md) ['enowsh](../../strongs/h/h582.md), and [yada'](../../strongs/h/h3045.md), and [nathan](../../strongs/h/h5414.md) them [ro'sh](../../strongs/h/h7218.md) over you, [śar](../../strongs/h/h8269.md) over thousands, and [śar](../../strongs/h/h8269.md) over hundreds, and [śar](../../strongs/h/h8269.md) over fifties, and [śar](../../strongs/h/h8269.md) over tens, and [šāṭar](../../strongs/h/h7860.md) among your [shebet](../../strongs/h/h7626.md).

<a name="deuteronomy_1_16"></a>Deuteronomy 1:16

And I [tsavah](../../strongs/h/h6680.md) your [shaphat](../../strongs/h/h8199.md) at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) between your ['ach](../../strongs/h/h251.md), and [shaphat](../../strongs/h/h8199.md) [tsedeq](../../strongs/h/h6664.md) between every ['iysh](../../strongs/h/h376.md) and his ['ach](../../strongs/h/h251.md), and the [ger](../../strongs/h/h1616.md) that is with him.

<a name="deuteronomy_1_17"></a>Deuteronomy 1:17

Ye shall not [nāḵar](../../strongs/h/h5234.md) [paniym](../../strongs/h/h6440.md) in [mishpat](../../strongs/h/h4941.md); but ye shall [shama'](../../strongs/h/h8085.md) the [qāṭān](../../strongs/h/h6996.md) as well as the [gadowl](../../strongs/h/h1419.md); ye shall not be [guwr](../../strongs/h/h1481.md) of the [paniym](../../strongs/h/h6440.md) of ['iysh](../../strongs/h/h376.md); for the [mishpat](../../strongs/h/h4941.md) is ['Elohiym](../../strongs/h/h430.md): and the [dabar](../../strongs/h/h1697.md) that is too [qāšâ](../../strongs/h/h7185.md) for you, [qāraḇ](../../strongs/h/h7126.md) it unto me, and I will [shama'](../../strongs/h/h8085.md) it.

<a name="deuteronomy_1_18"></a>Deuteronomy 1:18

And I [tsavah](../../strongs/h/h6680.md) you at that [ʿēṯ](../../strongs/h/h6256.md) all the [dabar](../../strongs/h/h1697.md) which ye should ['asah](../../strongs/h/h6213.md).

<a name="deuteronomy_1_19"></a>Deuteronomy 1:19

And when we [nāsaʿ](../../strongs/h/h5265.md) from [Hōrēḇ](../../strongs/h/h2722.md), we [yālaḵ](../../strongs/h/h3212.md) all that [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) [midbar](../../strongs/h/h4057.md), which ye [ra'ah](../../strongs/h/h7200.md) by the [derek](../../strongs/h/h1870.md) of the [har](../../strongs/h/h2022.md) of the ['Ĕmōrî](../../strongs/h/h567.md), as [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) us; and we [bow'](../../strongs/h/h935.md) to [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md).

<a name="deuteronomy_1_20"></a>Deuteronomy 1:20

And I ['āmar](../../strongs/h/h559.md) unto you, Ye are [bow'](../../strongs/h/h935.md) unto the [har](../../strongs/h/h2022.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) doth [nathan](../../strongs/h/h5414.md) unto us.

<a name="deuteronomy_1_21"></a>Deuteronomy 1:21

[ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) thee: [ʿālâ](../../strongs/h/h5927.md) and [yarash](../../strongs/h/h3423.md) it, as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md) hath [dabar](../../strongs/h/h1696.md) unto thee; [yare'](../../strongs/h/h3372.md) not, neither be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="deuteronomy_1_22"></a>Deuteronomy 1:22

And ye [qāraḇ](../../strongs/h/h7126.md) unto me every one of you, and ['āmar](../../strongs/h/h559.md), we will [shalach](../../strongs/h/h7971.md) ['enowsh](../../strongs/h/h582.md) [paniym](../../strongs/h/h6440.md) us, and they shall [chaphar](../../strongs/h/h2658.md) us the ['erets](../../strongs/h/h776.md), and [shuwb](../../strongs/h/h7725.md) us [dabar](../../strongs/h/h1697.md) by what [derek](../../strongs/h/h1870.md) we must [ʿālâ](../../strongs/h/h5927.md), and into what [ʿîr](../../strongs/h/h5892.md) we shall [bow'](../../strongs/h/h935.md).

<a name="deuteronomy_1_23"></a>Deuteronomy 1:23

And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) me: and I [laqach](../../strongs/h/h3947.md) twelve ['enowsh](../../strongs/h/h582.md) of you, one of an ['iysh](../../strongs/h/h376.md) [shebet](../../strongs/h/h7626.md):

<a name="deuteronomy_1_24"></a>Deuteronomy 1:24

And they [panah](../../strongs/h/h6437.md) and [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md), and [bow'](../../strongs/h/h935.md) unto the [nachal](../../strongs/h/h5158.md) of ['eškōl](../../strongs/h/h812.md), and [ragal](../../strongs/h/h7270.md) it.

<a name="deuteronomy_1_25"></a>Deuteronomy 1:25

And they [laqach](../../strongs/h/h3947.md) of the [pĕriy](../../strongs/h/h6529.md) of the ['erets](../../strongs/h/h776.md) in their [yad](../../strongs/h/h3027.md), and [yarad](../../strongs/h/h3381.md) it unto us, and [shuwb](../../strongs/h/h7725.md) us [dabar](../../strongs/h/h1697.md), and ['āmar](../../strongs/h/h559.md), It is a [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) doth [nathan](../../strongs/h/h5414.md) us.

<a name="deuteronomy_1_26"></a>Deuteronomy 1:26

Notwithstanding ye ['āḇâ](../../strongs/h/h14.md) not [ʿālâ](../../strongs/h/h5927.md), but [marah](../../strongs/h/h4784.md) against the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md):

<a name="deuteronomy_1_27"></a>Deuteronomy 1:27

And ye [rāḡan](../../strongs/h/h7279.md) in your ['ohel](../../strongs/h/h168.md), and ['āmar](../../strongs/h/h559.md), Because [Yĕhovah](../../strongs/h/h3068.md) [śin'â](../../strongs/h/h8135.md) us, he hath [yāṣā'](../../strongs/h/h3318.md) us out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), to [nathan](../../strongs/h/h5414.md) us into the [yad](../../strongs/h/h3027.md) of the ['Ĕmōrî](../../strongs/h/h567.md), to [šāmaḏ](../../strongs/h/h8045.md) us.

<a name="deuteronomy_1_28"></a>Deuteronomy 1:28

Whither shall we [ʿālâ](../../strongs/h/h5927.md)? our ['ach](../../strongs/h/h251.md) have [māsas](../../strongs/h/h4549.md) our [lebab](../../strongs/h/h3824.md), ['āmar](../../strongs/h/h559.md), The ['am](../../strongs/h/h5971.md) is [gadowl](../../strongs/h/h1419.md) and [ruwm](../../strongs/h/h7311.md) than we; the [ʿîr](../../strongs/h/h5892.md) are [gadowl](../../strongs/h/h1419.md) and [bāṣar](../../strongs/h/h1219.md) to [shamayim](../../strongs/h/h8064.md); and moreover we have [ra'ah](../../strongs/h/h7200.md) the [ben](../../strongs/h/h1121.md) of the [ʿĂnāqîm](../../strongs/h/h6062.md) there.

<a name="deuteronomy_1_29"></a>Deuteronomy 1:29

Then I ['āmar](../../strongs/h/h559.md) unto you, [ʿāraṣ](../../strongs/h/h6206.md) not, neither be [yare'](../../strongs/h/h3372.md) of them.

<a name="deuteronomy_1_30"></a>Deuteronomy 1:30

[Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) which [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) you, he shall [lāḥam](../../strongs/h/h3898.md) for you, according to all that he ['asah](../../strongs/h/h6213.md) for you in [Mitsrayim](../../strongs/h/h4714.md) before your ['ayin](../../strongs/h/h5869.md);

<a name="deuteronomy_1_31"></a>Deuteronomy 1:31

And in the [midbar](../../strongs/h/h4057.md), where thou hast [ra'ah](../../strongs/h/h7200.md) how that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nasa'](../../strongs/h/h5375.md) thee, as an ['iysh](../../strongs/h/h376.md) doth [nasa'](../../strongs/h/h5375.md) his [ben](../../strongs/h/h1121.md), in all the [derek](../../strongs/h/h1870.md) that ye [halak](../../strongs/h/h1980.md), until ye [bow'](../../strongs/h/h935.md) into this [maqowm](../../strongs/h/h4725.md).

<a name="deuteronomy_1_32"></a>Deuteronomy 1:32

Yet in this [dabar](../../strongs/h/h1697.md) ye did not ['aman](../../strongs/h/h539.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md),

<a name="deuteronomy_1_33"></a>Deuteronomy 1:33

Who [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) you, to [tûr](../../strongs/h/h8446.md) you a [maqowm](../../strongs/h/h4725.md) to [ḥānâ](../../strongs/h/h2583.md) in, in ['esh](../../strongs/h/h784.md) by [layil](../../strongs/h/h3915.md), to [ra'ah](../../strongs/h/h7200.md) you by what [derek](../../strongs/h/h1870.md) ye should [yālaḵ](../../strongs/h/h3212.md), and in a [ʿānān](../../strongs/h/h6051.md) by [yômām](../../strongs/h/h3119.md).

<a name="deuteronomy_1_34"></a>Deuteronomy 1:34

And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of your [dabar](../../strongs/h/h1697.md), and was [qāṣap̄](../../strongs/h/h7107.md), and [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_1_35"></a>Deuteronomy 1:35

Surely there shall not ['iysh](../../strongs/h/h376.md) of these ['enowsh](../../strongs/h/h582.md) of this [ra'](../../strongs/h/h7451.md) [dôr](../../strongs/h/h1755.md) [ra'ah](../../strongs/h/h7200.md) that [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md), which I [shaba'](../../strongs/h/h7650.md) to [nathan](../../strongs/h/h5414.md) unto your ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_1_36"></a>Deuteronomy 1:36

[zûlâ](../../strongs/h/h2108.md) [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md); he shall [ra'ah](../../strongs/h/h7200.md) it, and to him will I [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) that he hath [dāraḵ](../../strongs/h/h1869.md) upon, and to his [ben](../../strongs/h/h1121.md), because he hath [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md)D.

<a name="deuteronomy_1_37"></a>Deuteronomy 1:37

Also [Yĕhovah](../../strongs/h/h3068.md) was ['anaph](../../strongs/h/h599.md) with me for your [gālāl](../../strongs/h/h1558.md), ['āmar](../../strongs/h/h559.md), Thou also shalt not [bow'](../../strongs/h/h935.md) in thither.

<a name="deuteronomy_1_38"></a>Deuteronomy 1:38

But [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), which ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) thee, he shall [bow'](../../strongs/h/h935.md) thither: [ḥāzaq](../../strongs/h/h2388.md) him: for he shall cause [Yisra'el](../../strongs/h/h3478.md) to [nāḥal](../../strongs/h/h5157.md) it.

<a name="deuteronomy_1_39"></a>Deuteronomy 1:39

Moreover your [ṭap̄](../../strongs/h/h2945.md), which ye ['āmar](../../strongs/h/h559.md) should be a [baz](../../strongs/h/h957.md), and your [ben](../../strongs/h/h1121.md), which in that [yowm](../../strongs/h/h3117.md) had no [yada'](../../strongs/h/h3045.md) between [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md), they shall [bow'](../../strongs/h/h935.md) thither, and unto them will I [nathan](../../strongs/h/h5414.md) it, and they shall [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_1_40"></a>Deuteronomy 1:40

But as for you, [panah](../../strongs/h/h6437.md) you, and [nāsaʿ](../../strongs/h/h5265.md) into the [midbar](../../strongs/h/h4057.md) by the [derek](../../strongs/h/h1870.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="deuteronomy_1_41"></a>Deuteronomy 1:41

Then ye ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto me, We have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), we will [ʿālâ](../../strongs/h/h5927.md) and [lāḥam](../../strongs/h/h3898.md), according to all that [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) us. And when ye had [ḥāḡar](../../strongs/h/h2296.md) on every ['iysh](../../strongs/h/h376.md) his [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md), ye were [hûn](../../strongs/h/h1951.md) to [ʿālâ](../../strongs/h/h5927.md) into the [har](../../strongs/h/h2022.md).

<a name="deuteronomy_1_42"></a>Deuteronomy 1:42

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md) unto them. [ʿālâ](../../strongs/h/h5927.md) not, neither [lāḥam](../../strongs/h/h3898.md); for I am not [qereḇ](../../strongs/h/h7130.md) you; lest ye be [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) your ['oyeb](../../strongs/h/h341.md).

<a name="deuteronomy_1_43"></a>Deuteronomy 1:43

So I [dabar](../../strongs/h/h1696.md) unto you; and ye would not [shama'](../../strongs/h/h8085.md), but [marah](../../strongs/h/h4784.md) against the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ʿālâ](../../strongs/h/h5927.md) [zûḏ](../../strongs/h/h2102.md) into the [har](../../strongs/h/h2022.md).

<a name="deuteronomy_1_44"></a>Deuteronomy 1:44

And the ['Ĕmōrî](../../strongs/h/h567.md), which [yashab](../../strongs/h/h3427.md) in that [har](../../strongs/h/h2022.md), [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) you, and [radaph](../../strongs/h/h7291.md) you, as [dᵊḇôrâ](../../strongs/h/h1682.md) ['asah](../../strongs/h/h6213.md), and [kāṯaṯ](../../strongs/h/h3807.md) you in [Śēʿîr](../../strongs/h/h8165.md), even unto [Ḥārmâ](../../strongs/h/h2767.md).

<a name="deuteronomy_1_45"></a>Deuteronomy 1:45

And ye [shuwb](../../strongs/h/h7725.md) and [bāḵâ](../../strongs/h/h1058.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); but [Yĕhovah](../../strongs/h/h3068.md) would not [shama'](../../strongs/h/h8085.md) to your [qowl](../../strongs/h/h6963.md), nor ['azan](../../strongs/h/h238.md) unto you.

<a name="deuteronomy_1_46"></a>Deuteronomy 1:46

So ye [yashab](../../strongs/h/h3427.md) in [Qāḏēš](../../strongs/h/h6946.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), according unto the [yowm](../../strongs/h/h3117.md) that ye [yashab](../../strongs/h/h3427.md) there.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 2](deuteronomy_2.md)