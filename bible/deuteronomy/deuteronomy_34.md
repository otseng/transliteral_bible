# [Deuteronomy 34](https://www.blueletterbible.org/kjv/deu/34)

<a name="deuteronomy_34_1"></a>Deuteronomy 34:1

And [Mōshe](../../strongs/h/h4872.md) [ʿālâ](../../strongs/h/h5927.md) from the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) unto the [har](../../strongs/h/h2022.md) of [Nᵊḇô](../../strongs/h/h5015.md), to the [ro'sh](../../strongs/h/h7218.md) of [Pisgâ](../../strongs/h/h6449.md), that is over [paniym](../../strongs/h/h6440.md) [Yᵊrēḥô](../../strongs/h/h3405.md). And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) him all the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), unto [Dān](../../strongs/h/h1835.md),

<a name="deuteronomy_34_2"></a>Deuteronomy 34:2

And all [Nap̄tālî](../../strongs/h/h5321.md), and the ['erets](../../strongs/h/h776.md) of ['Ep̄rayim](../../strongs/h/h669.md), and [Mᵊnaššê](../../strongs/h/h4519.md), and all the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), unto the ['aḥărôn](../../strongs/h/h314.md) [yam](../../strongs/h/h3220.md),

<a name="deuteronomy_34_3"></a>Deuteronomy 34:3

And the [neḡeḇ](../../strongs/h/h5045.md), and the [kikār](../../strongs/h/h3603.md) of the [biqʿâ](../../strongs/h/h1237.md) of [Yᵊrēḥô](../../strongs/h/h3405.md), the [ʿîr](../../strongs/h/h5892.md) of [tāmār](../../strongs/h/h8558.md) [ʿîr hatmārîm](../../strongs/h/h5899.md), unto [Ṣōʿar](../../strongs/h/h6820.md).

<a name="deuteronomy_34_4"></a>Deuteronomy 34:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, This is the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md) unto ['Abraham](../../strongs/h/h85.md), unto [Yiṣḥāq](../../strongs/h/h3327.md), and unto [Ya'aqob](../../strongs/h/h3290.md), ['āmar](../../strongs/h/h559.md), I will [nathan](../../strongs/h/h5414.md) it unto thy [zera'](../../strongs/h/h2233.md): I have caused thee to [ra'ah](../../strongs/h/h7200.md) it with thine ['ayin](../../strongs/h/h5869.md), but thou shalt not ['abar](../../strongs/h/h5674.md) thither.

<a name="deuteronomy_34_5"></a>Deuteronomy 34:5

So [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [muwth](../../strongs/h/h4191.md) there in the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_34_6"></a>Deuteronomy 34:6

And he [qāḇar](../../strongs/h/h6912.md) him in a [gay'](../../strongs/h/h1516.md) in the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), over [môl](../../strongs/h/h4136.md) [Bêṯ PᵊʿÔr](../../strongs/h/h1047.md): but no ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) of his [qᵊḇûrâ](../../strongs/h/h6900.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_34_7"></a>Deuteronomy 34:7

And [Mōshe](../../strongs/h/h4872.md) was an hundred and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he [maveth](../../strongs/h/h4194.md): his ['ayin](../../strongs/h/h5869.md) was not [kāhâ](../../strongs/h/h3543.md), nor his [lēaḥ](../../strongs/h/h3893.md) [nûs](../../strongs/h/h5127.md).

<a name="deuteronomy_34_8"></a>Deuteronomy 34:8

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bāḵâ](../../strongs/h/h1058.md) for [Mōshe](../../strongs/h/h4872.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md) thirty [yowm](../../strongs/h/h3117.md): so the [yowm](../../strongs/h/h3117.md) of [bĕkiy](../../strongs/h/h1065.md) and ['ēḇel](../../strongs/h/h60.md) for [Mōshe](../../strongs/h/h4872.md) were [tamam](../../strongs/h/h8552.md).

<a name="deuteronomy_34_9"></a>Deuteronomy 34:9

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) was [mālē'](../../strongs/h/h4392.md) of the [ruwach](../../strongs/h/h7307.md) of [ḥāḵmâ](../../strongs/h/h2451.md); for [Mōshe](../../strongs/h/h4872.md) had [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon him: and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) unto him, and ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="deuteronomy_34_10"></a>Deuteronomy 34:10

And there [quwm](../../strongs/h/h6965.md) not a [nāḇî'](../../strongs/h/h5030.md) since in [Yisra'el](../../strongs/h/h3478.md) like unto [Mōshe](../../strongs/h/h4872.md), whom [Yĕhovah](../../strongs/h/h3068.md) [yada'](../../strongs/h/h3045.md) [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md),

<a name="deuteronomy_34_11"></a>Deuteronomy 34:11

In all the ['ôṯ](../../strongs/h/h226.md) and the [môp̄ēṯ](../../strongs/h/h4159.md), which [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) him to ['asah](../../strongs/h/h6213.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) to [Parʿô](../../strongs/h/h6547.md), and to all his ['ebed](../../strongs/h/h5650.md), and to all his ['erets](../../strongs/h/h776.md),

<a name="deuteronomy_34_12"></a>Deuteronomy 34:12

And in all that [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and in all the [gadowl](../../strongs/h/h1419.md) [mowra'](../../strongs/h/h4172.md) which [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 33](deuteronomy_33.md)