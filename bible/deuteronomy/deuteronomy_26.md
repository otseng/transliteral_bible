# [Deuteronomy 26](https://www.blueletterbible.org/kjv/deu/26)

<a name="deuteronomy_26_1"></a>Deuteronomy 26:1

And it shall be, when thou art [bow'](../../strongs/h/h935.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md), and [yarash](../../strongs/h/h3423.md) it, and [yashab](../../strongs/h/h3427.md) therein;

<a name="deuteronomy_26_2"></a>Deuteronomy 26:2

That thou shalt [laqach](../../strongs/h/h3947.md) of the [re'shiyth](../../strongs/h/h7225.md) of all the [pĕriy](../../strongs/h/h6529.md) of the ['ăḏāmâ](../../strongs/h/h127.md), which thou shalt [bow'](../../strongs/h/h935.md) of thy ['erets](../../strongs/h/h776.md) that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, and shalt [śûm](../../strongs/h/h7760.md) it in a [ṭene'](../../strongs/h/h2935.md), and shalt [halak](../../strongs/h/h1980.md) unto the [maqowm](../../strongs/h/h4725.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bāḥar](../../strongs/h/h977.md) to [shakan](../../strongs/h/h7931.md) his [shem](../../strongs/h/h8034.md) there.

<a name="deuteronomy_26_3"></a>Deuteronomy 26:3

And thou shalt [bow'](../../strongs/h/h935.md) unto the [kōhēn](../../strongs/h/h3548.md) that shall be in those [yowm](../../strongs/h/h3117.md), and ['āmar](../../strongs/h/h559.md) unto him, I [nāḡaḏ](../../strongs/h/h5046.md) this [yowm](../../strongs/h/h3117.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), that I am [bow'](../../strongs/h/h935.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto our ['ab](../../strongs/h/h1.md) for to [nathan](../../strongs/h/h5414.md) us.

<a name="deuteronomy_26_4"></a>Deuteronomy 26:4

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) the [ṭene'](../../strongs/h/h2935.md) out of thine [yad](../../strongs/h/h3027.md), and [yānaḥ](../../strongs/h/h3240.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_26_5"></a>Deuteronomy 26:5

And thou shalt ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), an ['Ărammy](../../strongs/h/h761.md) ready to ['abad](../../strongs/h/h6.md) was my ['ab](../../strongs/h/h1.md), and he [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md), and [guwr](../../strongs/h/h1481.md) there [math](../../strongs/h/h4962.md) a [mᵊʿaṭ](../../strongs/h/h4592.md), and became there a [gowy](../../strongs/h/h1471.md), [gadowl](../../strongs/h/h1419.md), ['atsuwm](../../strongs/h/h6099.md), and [rab](../../strongs/h/h7227.md):

<a name="deuteronomy_26_6"></a>Deuteronomy 26:6

And the [Miṣrî](../../strongs/h/h4713.md) [ra'a'](../../strongs/h/h7489.md) us, and [ʿānâ](../../strongs/h/h6031.md) us, and [nathan](../../strongs/h/h5414.md) upon us [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md):

<a name="deuteronomy_26_7"></a>Deuteronomy 26:7

And when we [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of our ['ab](../../strongs/h/h1.md), [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) our [qowl](../../strongs/h/h6963.md), and [ra'ah](../../strongs/h/h7200.md) on our ['oniy](../../strongs/h/h6040.md), and our ['amal](../../strongs/h/h5999.md), and our [laḥaṣ](../../strongs/h/h3906.md):

<a name="deuteronomy_26_8"></a>Deuteronomy 26:8

And [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) us out of [Mitsrayim](../../strongs/h/h4714.md) with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and with a [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and with [gadowl](../../strongs/h/h1419.md) [mowra'](../../strongs/h/h4172.md), and with ['ôṯ](../../strongs/h/h226.md), and with [môp̄ēṯ](../../strongs/h/h4159.md):

<a name="deuteronomy_26_9"></a>Deuteronomy 26:9

And he hath [bow'](../../strongs/h/h935.md) us into this [maqowm](../../strongs/h/h4725.md), and hath [nathan](../../strongs/h/h5414.md) us this ['erets](../../strongs/h/h776.md), even an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="deuteronomy_26_10"></a>Deuteronomy 26:10

And now, behold, I have [bow'](../../strongs/h/h935.md) the [pĕriy](../../strongs/h/h6529.md) [re'shiyth](../../strongs/h/h7225.md) of the ['ăḏāmâ](../../strongs/h/h127.md), which thou, [Yĕhovah](../../strongs/h/h3068.md), hast [nathan](../../strongs/h/h5414.md) me. And thou shalt [yānaḥ](../../strongs/h/h3240.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md):

<a name="deuteronomy_26_11"></a>Deuteronomy 26:11

And thou shalt [samach](../../strongs/h/h8055.md) in every [towb](../../strongs/h/h2896.md) thing which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) unto thee, and unto thine [bayith](../../strongs/h/h1004.md), thou, and the [Lᵊvî](../../strongs/h/h3881.md), and the [ger](../../strongs/h/h1616.md) that is [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_26_12"></a>Deuteronomy 26:12

When thou hast made a [kalah](../../strongs/h/h3615.md) of [ʿāśar](../../strongs/h/h6237.md) all the [maʿăśēr](../../strongs/h/h4643.md) of thine [tᵊḇû'â](../../strongs/h/h8393.md) the third [šānâ](../../strongs/h/h8141.md), which is the [šānâ](../../strongs/h/h8141.md) of [maʿăśēr](../../strongs/h/h4643.md), and hast [nathan](../../strongs/h/h5414.md) it unto the [Lᵊvî](../../strongs/h/h3881.md), the [ger](../../strongs/h/h1616.md), the [yathowm](../../strongs/h/h3490.md), and the ['almānâ](../../strongs/h/h490.md), that they may ['akal](../../strongs/h/h398.md) within thy [sha'ar](../../strongs/h/h8179.md), and be [sāׂbaʿ](../../strongs/h/h7646.md);

<a name="deuteronomy_26_13"></a>Deuteronomy 26:13

Then thou shalt ['āmar](../../strongs/h/h559.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), I have [bāʿar](../../strongs/h/h1197.md) the [qodesh](../../strongs/h/h6944.md) out of mine [bayith](../../strongs/h/h1004.md), and also have [nathan](../../strongs/h/h5414.md) them unto the [Lᵊvî](../../strongs/h/h3881.md), and unto the [ger](../../strongs/h/h1616.md), to the [yathowm](../../strongs/h/h3490.md), and to the ['almānâ](../../strongs/h/h490.md), according to all thy [mitsvah](../../strongs/h/h4687.md) which thou hast [tsavah](../../strongs/h/h6680.md) me: I have not ['abar](../../strongs/h/h5674.md) thy [mitsvah](../../strongs/h/h4687.md), neither have I [shakach](../../strongs/h/h7911.md) them.

<a name="deuteronomy_26_14"></a>Deuteronomy 26:14

I have not ['akal](../../strongs/h/h398.md) thereof in my ['aven](../../strongs/h/h205.md), neither have I [bāʿar](../../strongs/h/h1197.md) ought thereof for any [tame'](../../strongs/h/h2931.md) use, nor [nathan](../../strongs/h/h5414.md) ought thereof for the [muwth](../../strongs/h/h4191.md): but I have [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of the[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), and have ['asah](../../strongs/h/h6213.md) according to all that thou hast [tsavah](../../strongs/h/h6680.md) me.

<a name="deuteronomy_26_15"></a>Deuteronomy 26:15

[šāqap̄](../../strongs/h/h8259.md) from thy [qodesh](../../strongs/h/h6944.md) [māʿôn](../../strongs/h/h4583.md), from [shamayim](../../strongs/h/h8064.md), and [barak](../../strongs/h/h1288.md) thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and the ['ăḏāmâ](../../strongs/h/h127.md) which thou hast [nathan](../../strongs/h/h5414.md) us, as thou [shaba'](../../strongs/h/h7650.md) unto our ['ab](../../strongs/h/h1.md), an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="deuteronomy_26_16"></a>Deuteronomy 26:16

This [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thee to ['asah](../../strongs/h/h6213.md) these [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md): thou shalt therefore [shamar](../../strongs/h/h8104.md) and ['asah](../../strongs/h/h6213.md) them with all thine [lebab](../../strongs/h/h3824.md), and with all thy [nephesh](../../strongs/h/h5315.md).

<a name="deuteronomy_26_17"></a>Deuteronomy 26:17

Thou hast ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) this [yowm](../../strongs/h/h3117.md) to be thy ['Elohiym](../../strongs/h/h430.md), and to [yālaḵ](../../strongs/h/h3212.md) in his [derek](../../strongs/h/h1870.md), and to [shamar](../../strongs/h/h8104.md) his [choq](../../strongs/h/h2706.md), and his [mitsvah](../../strongs/h/h4687.md), and his [mishpat](../../strongs/h/h4941.md), and to [shama'](../../strongs/h/h8085.md) unto his [qowl](../../strongs/h/h6963.md):

<a name="deuteronomy_26_18"></a>Deuteronomy 26:18

And [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) thee this [yowm](../../strongs/h/h3117.md) to be his [sᵊḡullâ](../../strongs/h/h5459.md) ['am](../../strongs/h/h5971.md), as he hath [dabar](../../strongs/h/h1696.md) thee, and that thou shouldest [shamar](../../strongs/h/h8104.md) all his [mitsvah](../../strongs/h/h4687.md);

<a name="deuteronomy_26_19"></a>Deuteronomy 26:19

And to [nathan](../../strongs/h/h5414.md) thee ['elyown](../../strongs/h/h5945.md) above all [gowy](../../strongs/h/h1471.md) which he hath ['asah](../../strongs/h/h6213.md), in [tehillah](../../strongs/h/h8416.md), and in [shem](../../strongs/h/h8034.md), and in [tip̄'ārâ](../../strongs/h/h8597.md); and that thou mayest be a [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), as he hath [dabar](../../strongs/h/h1696.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 25](deuteronomy_25.md) - [Deuteronomy 27](deuteronomy_27.md)