# [Deuteronomy 19](https://www.blueletterbible.org/esv/deu/19)

<a name="deuteronomy_19_1"></a>Deuteronomy 19:1

When [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [karath](../../strongs/h/h3772.md) the [gowy](../../strongs/h/h1471.md), whose ['erets](../../strongs/h/h776.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee, and thou [yarash](../../strongs/h/h3423.md) them, and [yashab](../../strongs/h/h3427.md) in their [ʿîr](../../strongs/h/h5892.md), and in their [bayith](../../strongs/h/h1004.md);

<a name="deuteronomy_19_2"></a>Deuteronomy 19:2

Thou shalt [bāḏal](../../strongs/h/h914.md) three [ʿîr](../../strongs/h/h5892.md) for thee in the [tavek](../../strongs/h/h8432.md) of thy ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_19_3"></a>Deuteronomy 19:3

Thou shalt [kuwn](../../strongs/h/h3559.md) thee a [derek](../../strongs/h/h1870.md), and divide the [gᵊḇûl](../../strongs/h/h1366.md) of thy ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) giveth thee to [nāḥal](../../strongs/h/h5157.md), into three [šālaš](../../strongs/h/h8027.md), that every [ratsach](../../strongs/h/h7523.md) may [nûs](../../strongs/h/h5127.md) thither.

<a name="deuteronomy_19_4"></a>Deuteronomy 19:4

And this is the [dabar](../../strongs/h/h1697.md) of the [ratsach](../../strongs/h/h7523.md), which shall [nûs](../../strongs/h/h5127.md) thither, that he may [chayay](../../strongs/h/h2425.md): Whoso [nakah](../../strongs/h/h5221.md) his [rea'](../../strongs/h/h7453.md) [bᵊlî](../../strongs/h/h1097.md) [da'ath](../../strongs/h/h1847.md), whom he [sane'](../../strongs/h/h8130.md) not in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md);

<a name="deuteronomy_19_5"></a>Deuteronomy 19:5

As when [bow'](../../strongs/h/h935.md) into the [yaʿar](../../strongs/h/h3293.md) with his [rea'](../../strongs/h/h7453.md) to [ḥāṭaḇ](../../strongs/h/h2404.md) ['ets](../../strongs/h/h6086.md), and his [yad](../../strongs/h/h3027.md) [nāḏaḥ](../../strongs/h/h5080.md) with the [garzen](../../strongs/h/h1631.md) to [karath](../../strongs/h/h3772.md) the ['ets](../../strongs/h/h6086.md), and the [barzel](../../strongs/h/h1270.md) [nāšal](../../strongs/h/h5394.md) from the ['ets](../../strongs/h/h6086.md), and [māṣā'](../../strongs/h/h4672.md) upon his [rea'](../../strongs/h/h7453.md), that he [muwth](../../strongs/h/h4191.md); he shall [nûs](../../strongs/h/h5127.md) unto one of those [ʿîr](../../strongs/h/h5892.md), and [chayay](../../strongs/h/h2425.md):

<a name="deuteronomy_19_6"></a>Deuteronomy 19:6

Lest the [gā'al](../../strongs/h/h1350.md) of the [dam](../../strongs/h/h1818.md) [radaph](../../strongs/h/h7291.md) the ['aḥar](../../strongs/h/h310.md) [ratsach](../../strongs/h/h7523.md), while his [lebab](../../strongs/h/h3824.md) is [yāḥam](../../strongs/h/h3179.md), and [nāśaḡ](../../strongs/h/h5381.md) him, because the [derek](../../strongs/h/h1870.md) is [rabah](../../strongs/h/h7235.md), and [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md); whereas he was not [mishpat](../../strongs/h/h4941.md) of [maveth](../../strongs/h/h4194.md), inasmuch as he [sane'](../../strongs/h/h8130.md) him not in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md).

<a name="deuteronomy_19_7"></a>Deuteronomy 19:7

Wherefore I [tsavah](../../strongs/h/h6680.md) thee, ['āmar](../../strongs/h/h559.md), Thou shalt [bāḏal](../../strongs/h/h914.md) three [ʿîr](../../strongs/h/h5892.md) for thee.

<a name="deuteronomy_19_8"></a>Deuteronomy 19:8

And if [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [rāḥaḇ](../../strongs/h/h7337.md) thy [gᵊḇûl](../../strongs/h/h1366.md), as he hath [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), and [nathan](../../strongs/h/h5414.md) thee all the ['erets](../../strongs/h/h776.md) which he [dabar](../../strongs/h/h1696.md) to [nathan](../../strongs/h/h5414.md) unto thy ['ab](../../strongs/h/h1.md);

<a name="deuteronomy_19_9"></a>Deuteronomy 19:9

If thou shalt [shamar](../../strongs/h/h8104.md) all these [mitsvah](../../strongs/h/h4687.md) to ['asah](../../strongs/h/h6213.md) them, which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and to [yālaḵ](../../strongs/h/h3212.md) [yowm](../../strongs/h/h3117.md) in his [derek](../../strongs/h/h1870.md); then shalt thou [yāsap̄](../../strongs/h/h3254.md) three [ʿîr](../../strongs/h/h5892.md) more for thee, beside these three:

<a name="deuteronomy_19_10"></a>Deuteronomy 19:10

That [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) be not [šāp̄aḵ](../../strongs/h/h8210.md) [qereḇ](../../strongs/h/h7130.md) thy ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md), and so [dam](../../strongs/h/h1818.md) be upon thee.

<a name="deuteronomy_19_11"></a>Deuteronomy 19:11

But if any ['iysh](../../strongs/h/h376.md) [sane'](../../strongs/h/h8130.md) his [rea'](../../strongs/h/h7453.md), and ['arab](../../strongs/h/h693.md) for him, and [quwm](../../strongs/h/h6965.md) against him, and [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md) that he [muwth](../../strongs/h/h4191.md), and [nûs](../../strongs/h/h5127.md) into one of these [ʿîr](../../strongs/h/h5892.md):

<a name="deuteronomy_19_12"></a>Deuteronomy 19:12

Then the [zāqēn](../../strongs/h/h2205.md) of his [ʿîr](../../strongs/h/h5892.md) shall [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) him thence, and [nathan](../../strongs/h/h5414.md) him into the [yad](../../strongs/h/h3027.md) of the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md), that he may [muwth](../../strongs/h/h4191.md).

<a name="deuteronomy_19_13"></a>Deuteronomy 19:13

Thine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md) him, but thou shalt [bāʿar](../../strongs/h/h1197.md) the [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) from [Yisra'el](../../strongs/h/h3478.md), that it may [ṭôḇ](../../strongs/h/h2895.md) with thee.

<a name="deuteronomy_19_14"></a>Deuteronomy 19:14

Thou shalt not [nāsaḡ](../../strongs/h/h5253.md) thy [rea'](../../strongs/h/h7453.md) [gᵊḇûl](../../strongs/h/h1366.md), which they of [ri'šôn](../../strongs/h/h7223.md) have [gāḇal](../../strongs/h/h1379.md) in thine [nachalah](../../strongs/h/h5159.md), which thou shalt [nāḥal](../../strongs/h/h5157.md) in the ['erets](../../strongs/h/h776.md) that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_19_15"></a>Deuteronomy 19:15

One ['ed](../../strongs/h/h5707.md) shall not [quwm](../../strongs/h/h6965.md) against an ['iysh](../../strongs/h/h376.md) for any ['avon](../../strongs/h/h5771.md), or for any [chatta'ath](../../strongs/h/h2403.md), in any [ḥēṭĕ'](../../strongs/h/h2399.md) that he [chata'](../../strongs/h/h2398.md): at the [peh](../../strongs/h/h6310.md) of two ['ed](../../strongs/h/h5707.md), or at the [peh](../../strongs/h/h6310.md) of three ['ed](../../strongs/h/h5707.md), shall the [dabar](../../strongs/h/h1697.md) be [quwm](../../strongs/h/h6965.md).

<a name="deuteronomy_19_16"></a>Deuteronomy 19:16

If a [chamac](../../strongs/h/h2555.md) ['ed](../../strongs/h/h5707.md) [quwm](../../strongs/h/h6965.md) against any ['iysh](../../strongs/h/h376.md) to ['anah](../../strongs/h/h6030.md) against him [sārâ](../../strongs/h/h5627.md);

<a name="deuteronomy_19_17"></a>Deuteronomy 19:17

Then both the ['enowsh](../../strongs/h/h582.md), between whom the [rîḇ](../../strongs/h/h7379.md) is, shall ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md) and the [shaphat](../../strongs/h/h8199.md), which shall be in those [yowm](../../strongs/h/h3117.md);

<a name="deuteronomy_19_18"></a>Deuteronomy 19:18

And the [shaphat](../../strongs/h/h8199.md) shall make [yatab](../../strongs/h/h3190.md) [darash](../../strongs/h/h1875.md): and, behold, if the ['ed](../../strongs/h/h5707.md) be a [sheqer](../../strongs/h/h8267.md) ['ed](../../strongs/h/h5707.md), and hath ['anah](../../strongs/h/h6030.md) [sheqer](../../strongs/h/h8267.md) against his ['ach](../../strongs/h/h251.md);

<a name="deuteronomy_19_19"></a>Deuteronomy 19:19

Then shall ye ['asah](../../strongs/h/h6213.md) unto him, as he had [zāmam](../../strongs/h/h2161.md) to have ['asah](../../strongs/h/h6213.md) unto his ['ach](../../strongs/h/h251.md): so shalt thou [bāʿar](../../strongs/h/h1197.md) the [ra'](../../strongs/h/h7451.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_19_20"></a>Deuteronomy 19:20

And those which [šā'ar](../../strongs/h/h7604.md) shall [shama'](../../strongs/h/h8085.md), and [yare'](../../strongs/h/h3372.md), and shall henceforth ['asah](../../strongs/h/h6213.md) no more [dabar](../../strongs/h/h1697.md) such [ra'](../../strongs/h/h7451.md) [qereḇ](../../strongs/h/h7130.md) you.

<a name="deuteronomy_19_21"></a>Deuteronomy 19:21

And thine ['ayin](../../strongs/h/h5869.md) shall not [ḥûs](../../strongs/h/h2347.md); but [nephesh](../../strongs/h/h5315.md) for [nephesh](../../strongs/h/h5315.md), ['ayin](../../strongs/h/h5869.md) for ['ayin](../../strongs/h/h5869.md), [šēn](../../strongs/h/h8127.md) for [šēn](../../strongs/h/h8127.md), [yad](../../strongs/h/h3027.md) for [yad](../../strongs/h/h3027.md), [regel](../../strongs/h/h7272.md) for [regel](../../strongs/h/h7272.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 18](deuteronomy_18.md) - [Deuteronomy 20](deuteronomy_20.md)