# [Deuteronomy 33](https://www.blueletterbible.org/kjv/deu/33)

<a name="deuteronomy_33_1"></a>Deuteronomy 33:1

And this is the [bĕrakah](../../strongs/h/h1293.md), wherewith [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [barak](../../strongs/h/h1288.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) his [maveth](../../strongs/h/h4194.md).

<a name="deuteronomy_33_2"></a>Deuteronomy 33:2

And he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) from [Sînay](../../strongs/h/h5514.md), and [zāraḥ](../../strongs/h/h2224.md) from [Śēʿîr](../../strongs/h/h8165.md) unto them; he [yāp̄aʿ](../../strongs/h/h3313.md) from [har](../../strongs/h/h2022.md) [Pā'rān](../../strongs/h/h6290.md), and he ['āṯâ](../../strongs/h/h857.md) with ten thousands of [qodesh](../../strongs/h/h6944.md): from his [yamiyn](../../strongs/h/h3225.md) went an ['ēš daṯ](../../strongs/h/h799.md) ['esh](../../strongs/h/h784.md) [dāṯ](../../strongs/h/h1881.md) for them.

<a name="deuteronomy_33_3"></a>Deuteronomy 33:3

['aph](../../strongs/h/h637.md), he [ḥāḇaḇ](../../strongs/h/h2245.md) the ['am](../../strongs/h/h5971.md); all his [qadowsh](../../strongs/h/h6918.md) are in thy [yad](../../strongs/h/h3027.md): and they [tāḵâ](../../strongs/h/h8497.md) at thy [regel](../../strongs/h/h7272.md); every one shall [nasa'](../../strongs/h/h5375.md) of thy [dabrâ](../../strongs/h/h1703.md).

<a name="deuteronomy_33_4"></a>Deuteronomy 33:4

[Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) us a [towrah](../../strongs/h/h8451.md), even the [môrāšâ](../../strongs/h/h4181.md) of the [qᵊhillâ](../../strongs/h/h6952.md) of [Ya'aqob](../../strongs/h/h3290.md).

<a name="deuteronomy_33_5"></a>Deuteronomy 33:5

And he was [melek](../../strongs/h/h4428.md) in [Yᵊšurûn](../../strongs/h/h3484.md), when the [ro'sh](../../strongs/h/h7218.md) of the ['am](../../strongs/h/h5971.md) and the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) were ['āsap̄](../../strongs/h/h622.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="deuteronomy_33_6"></a>Deuteronomy 33:6

Let [Rᵊ'ûḇēn](../../strongs/h/h7205.md) [ḥāyâ](../../strongs/h/h2421.md), and not [muwth](../../strongs/h/h4191.md); and let not his [math](../../strongs/h/h4962.md) be [mispār](../../strongs/h/h4557.md).

<a name="deuteronomy_33_7"></a>Deuteronomy 33:7

And this of [Yehuwdah](../../strongs/h/h3063.md): and he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md), [Yĕhovah](../../strongs/h/h3068.md), the [qowl](../../strongs/h/h6963.md) of [Yehuwdah](../../strongs/h/h3063.md), and [bow'](../../strongs/h/h935.md) him unto his ['am](../../strongs/h/h5971.md): let his [yad](../../strongs/h/h3027.md) be [rab](../../strongs/h/h7227.md) for him; and be thou an ['ezer](../../strongs/h/h5828.md) to him from his [tsar](../../strongs/h/h6862.md).

<a name="deuteronomy_33_8"></a>Deuteronomy 33:8

And of [Lēvî](../../strongs/h/h3878.md) he ['āmar](../../strongs/h/h559.md), Let thy [Tummîm](../../strongs/h/h8550.md) and thy ['Ûrîm](../../strongs/h/h224.md) be with thy [chaciyd](../../strongs/h/h2623.md) ['iysh](../../strongs/h/h376.md), whom thou didst [nāsâ](../../strongs/h/h5254.md) at [massâ](../../strongs/h/h4532.md), and with whom thou didst [riyb](../../strongs/h/h7378.md) at the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4809.md);

<a name="deuteronomy_33_9"></a>Deuteronomy 33:9

Who ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md) and to his ['em](../../strongs/h/h517.md), I have not [ra'ah](../../strongs/h/h7200.md) him; neither did he [nāḵar](../../strongs/h/h5234.md) his ['ach](../../strongs/h/h251.md), nor [yada'](../../strongs/h/h3045.md) his own [ben](../../strongs/h/h1121.md): for they have [shamar](../../strongs/h/h8104.md) thy ['imrah](../../strongs/h/h565.md), and [nāṣar](../../strongs/h/h5341.md) thy [bĕriyth](../../strongs/h/h1285.md).

<a name="deuteronomy_33_10"></a>Deuteronomy 33:10

They shall [yārâ](../../strongs/h/h3384.md) [Ya'aqob](../../strongs/h/h3290.md) thy [mishpat](../../strongs/h/h4941.md), and [Yisra'el](../../strongs/h/h3478.md) thy [towrah](../../strongs/h/h8451.md): they shall [śûm](../../strongs/h/h7760.md) [qᵊṭôrâ](../../strongs/h/h6988.md) ['aph](../../strongs/h/h639.md) thee, and [kālîl](../../strongs/h/h3632.md) upon thine [mizbeach](../../strongs/h/h4196.md).

<a name="deuteronomy_33_11"></a>Deuteronomy 33:11

[barak](../../strongs/h/h1288.md), [Yĕhovah](../../strongs/h/h3068.md), his [ḥayil](../../strongs/h/h2428.md), and [ratsah](../../strongs/h/h7521.md) the [pōʿal](../../strongs/h/h6467.md) of his [yad](../../strongs/h/h3027.md); [māḥaṣ](../../strongs/h/h4272.md) the [māṯnayim](../../strongs/h/h4975.md) of them that [quwm](../../strongs/h/h6965.md) against him, and of them that [sane'](../../strongs/h/h8130.md) him, that they [quwm](../../strongs/h/h6965.md) not.

<a name="deuteronomy_33_12"></a>Deuteronomy 33:12

And of [Binyāmîn](../../strongs/h/h1144.md) he ['āmar](../../strongs/h/h559.md), The [yāḏîḏ](../../strongs/h/h3039.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [shakan](../../strongs/h/h7931.md) in [betach](../../strongs/h/h983.md) by him; and shall [ḥāp̄ap̄](../../strongs/h/h2653.md) him all the [yowm](../../strongs/h/h3117.md) long, and he shall [shakan](../../strongs/h/h7931.md) between his [kāṯēp̄](../../strongs/h/h3802.md).

<a name="deuteronomy_33_13"></a>Deuteronomy 33:13

And of [Yôsēp̄](../../strongs/h/h3130.md) he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) of [Yĕhovah](../../strongs/h/h3068.md) be his ['erets](../../strongs/h/h776.md), for the [meḡeḏ](../../strongs/h/h4022.md) of [shamayim](../../strongs/h/h8064.md), for the [ṭal](../../strongs/h/h2919.md), and for the [tĕhowm](../../strongs/h/h8415.md) that [rāḇaṣ](../../strongs/h/h7257.md),

<a name="deuteronomy_33_14"></a>Deuteronomy 33:14

And for the [meḡeḏ](../../strongs/h/h4022.md) [tᵊḇû'â](../../strongs/h/h8393.md) by the [šemeš](../../strongs/h/h8121.md), and for the [meḡeḏ](../../strongs/h/h4022.md) [gereš](../../strongs/h/h1645.md) by the [yeraḥ](../../strongs/h/h3391.md),

<a name="deuteronomy_33_15"></a>Deuteronomy 33:15

And for the [ro'sh](../../strongs/h/h7218.md) of the [qeḏem](../../strongs/h/h6924.md) [har](../../strongs/h/h2042.md), and for the [meḡeḏ](../../strongs/h/h4022.md) of the ['owlam](../../strongs/h/h5769.md) [giḇʿâ](../../strongs/h/h1389.md),

<a name="deuteronomy_33_16"></a>Deuteronomy 33:16

And for the [meḡeḏ](../../strongs/h/h4022.md) of the ['erets](../../strongs/h/h776.md) and [mᵊlō'](../../strongs/h/h4393.md) thereof, and for the [ratsown](../../strongs/h/h7522.md) of him that [shakan](../../strongs/h/h7931.md) in the [sᵊnê](../../strongs/h/h5572.md): let [bow'](../../strongs/h/h935.md) upon the [ro'sh](../../strongs/h/h7218.md) of [Yôsēp̄](../../strongs/h/h3130.md), and upon the [qodqod](../../strongs/h/h6936.md) of him [nāzîr](../../strongs/h/h5139.md) from his ['ach](../../strongs/h/h251.md).

<a name="deuteronomy_33_17"></a>Deuteronomy 33:17

His [hadar](../../strongs/h/h1926.md) the [bᵊḵôr](../../strongs/h/h1060.md) of his [showr](../../strongs/h/h7794.md), and his [qeren](../../strongs/h/h7161.md) are like the [qeren](../../strongs/h/h7161.md) of [rᵊ'ēm](../../strongs/h/h7214.md): with them he shall [nāḡaḥ](../../strongs/h/h5055.md) the ['am](../../strongs/h/h5971.md) [yaḥaḏ](../../strongs/h/h3162.md) to the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md): and they are the ten thousands of ['Ep̄rayim](../../strongs/h/h669.md), and they are the thousands of [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="deuteronomy_33_18"></a>Deuteronomy 33:18

And of [Zᵊḇûlûn](../../strongs/h/h2074.md) he ['āmar](../../strongs/h/h559.md), [samach](../../strongs/h/h8055.md), [Zᵊḇûlûn](../../strongs/h/h2074.md), in thy [yāṣā'](../../strongs/h/h3318.md); and, [Yiśśāśḵār](../../strongs/h/h3485.md), in thy ['ohel](../../strongs/h/h168.md).

<a name="deuteronomy_33_19"></a>Deuteronomy 33:19

They shall [qara'](../../strongs/h/h7121.md) the ['am](../../strongs/h/h5971.md) unto the [har](../../strongs/h/h2022.md); there they shall [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) of [tsedeq](../../strongs/h/h6664.md): for they shall [yānaq](../../strongs/h/h3243.md) of the [šep̄aʿ](../../strongs/h/h8228.md) of the [yam](../../strongs/h/h3220.md), and of [śāp̄an](../../strongs/h/h8226.md) [taman](../../strongs/h/h2934.md) in the [ḥôl](../../strongs/h/h2344.md).

<a name="deuteronomy_33_20"></a>Deuteronomy 33:20

And of [Gāḏ](../../strongs/h/h1410.md) he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be he that [rāḥaḇ](../../strongs/h/h7337.md) [Gāḏ](../../strongs/h/h1410.md): he [shakan](../../strongs/h/h7931.md) as a [lāḇî'](../../strongs/h/h3833.md), and [taraph](../../strongs/h/h2963.md) the [zerowa'](../../strongs/h/h2220.md) with the [qodqod](../../strongs/h/h6936.md).

<a name="deuteronomy_33_21"></a>Deuteronomy 33:21

And he [ra'ah](../../strongs/h/h7200.md) the [re'shiyth](../../strongs/h/h7225.md) for himself, because there, in a [ḥelqâ](../../strongs/h/h2513.md) of the [ḥāqaq](../../strongs/h/h2710.md), [sāp̄an](../../strongs/h/h5603.md); and he ['āṯâ](../../strongs/h/h857.md) with the [ro'sh](../../strongs/h/h7218.md) of the ['am](../../strongs/h/h5971.md), he ['asah](../../strongs/h/h6213.md) the [tsedaqah](../../strongs/h/h6666.md) of [Yĕhovah](../../strongs/h/h3068.md), and his [mishpat](../../strongs/h/h4941.md) with [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_33_22"></a>Deuteronomy 33:22

And of [Dān](../../strongs/h/h1835.md) he ['āmar](../../strongs/h/h559.md), [Dān](../../strongs/h/h1835.md) is an ['ariy](../../strongs/h/h738.md) [gûr](../../strongs/h/h1482.md): he shall [zānaq](../../strongs/h/h2187.md) from [Bāšān](../../strongs/h/h1316.md).

<a name="deuteronomy_33_23"></a>Deuteronomy 33:23

And of [Nap̄tālî](../../strongs/h/h5321.md) he ['āmar](../../strongs/h/h559.md), [Nap̄tālî](../../strongs/h/h5321.md), [śāḇēaʿ](../../strongs/h/h7649.md) with [ratsown](../../strongs/h/h7522.md), and [mālē'](../../strongs/h/h4392.md) with the [bĕrakah](../../strongs/h/h1293.md) of [Yĕhovah](../../strongs/h/h3068.md): [yarash](../../strongs/h/h3423.md) thou the [yam](../../strongs/h/h3220.md) and the [dārôm](../../strongs/h/h1864.md).

<a name="deuteronomy_33_24"></a>Deuteronomy 33:24

And of ['Āšēr](../../strongs/h/h836.md) he ['āmar](../../strongs/h/h559.md), ['Āšēr](../../strongs/h/h836.md) be [barak](../../strongs/h/h1288.md) with [ben](../../strongs/h/h1121.md); let him be [ratsah](../../strongs/h/h7521.md) to his ['ach](../../strongs/h/h251.md), and let him [ṭāḇal](../../strongs/h/h2881.md) his [regel](../../strongs/h/h7272.md) in [šemen](../../strongs/h/h8081.md).

<a name="deuteronomy_33_25"></a>Deuteronomy 33:25

Thy [minʿāl](../../strongs/h/h4515.md) be [barzel](../../strongs/h/h1270.md) and [nᵊḥšeṯ](../../strongs/h/h5178.md); and as thy [yowm](../../strongs/h/h3117.md), so shall thy [dōḇe'](../../strongs/h/h1679.md) be.

<a name="deuteronomy_33_26"></a>Deuteronomy 33:26

There is none like unto the ['el](../../strongs/h/h410.md) of [Yᵊšurûn](../../strongs/h/h3484.md), who [rāḵaḇ](../../strongs/h/h7392.md) upon the [shamayim](../../strongs/h/h8064.md) in thy ['ezer](../../strongs/h/h5828.md), and in his [ga'avah](../../strongs/h/h1346.md) on the [shachaq](../../strongs/h/h7834.md).

<a name="deuteronomy_33_27"></a>Deuteronomy 33:27

The [qeḏem](../../strongs/h/h6924.md) ['Elohiym](../../strongs/h/h430.md) is thy [mᵊʿônâ](../../strongs/h/h4585.md), and underneath are the ['owlam](../../strongs/h/h5769.md) [zerowa'](../../strongs/h/h2220.md): and he shall [gāraš](../../strongs/h/h1644.md) the ['oyeb](../../strongs/h/h341.md) from [paniym](../../strongs/h/h6440.md) thee; and shall ['āmar](../../strongs/h/h559.md), [šāmaḏ](../../strongs/h/h8045.md) them.

<a name="deuteronomy_33_28"></a>Deuteronomy 33:28

[Yisra'el](../../strongs/h/h3478.md) then shall [shakan](../../strongs/h/h7931.md) in [betach](../../strongs/h/h983.md) [bāḏāḏ](../../strongs/h/h910.md): the ['ayin](../../strongs/h/h5869.md) of [Ya'aqob](../../strongs/h/h3290.md) shall be upon an ['erets](../../strongs/h/h776.md) of [dagan](../../strongs/h/h1715.md) and [tiyrowsh](../../strongs/h/h8492.md); also his [shamayim](../../strongs/h/h8064.md) shall [ʿārap̄](../../strongs/h/h6201.md) [ṭal](../../strongs/h/h2919.md).

<a name="deuteronomy_33_29"></a>Deuteronomy 33:29

['esher](../../strongs/h/h835.md) art thou, [Yisra'el](../../strongs/h/h3478.md): who is like unto thee, O ['am](../../strongs/h/h5971.md) [yasha'](../../strongs/h/h3467.md) by [Yĕhovah](../../strongs/h/h3068.md), the [magen](../../strongs/h/h4043.md) of thy ['ezer](../../strongs/h/h5828.md), and who is the [chereb](../../strongs/h/h2719.md) of thy [ga'avah](../../strongs/h/h1346.md)! and thine ['oyeb](../../strongs/h/h341.md) shall [kāḥaš](../../strongs/h/h3584.md) unto thee; and thou shalt [dāraḵ](../../strongs/h/h1869.md) upon their [bāmâ](../../strongs/h/h1116.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 32](deuteronomy_32.md) - [Deuteronomy 34](deuteronomy_34.md)