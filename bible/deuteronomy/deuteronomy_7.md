# [Deuteronomy 7](https://www.blueletterbible.org/kjv/deu/7)

<a name="deuteronomy_7_1"></a>Deuteronomy 7:1

When [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it, and hath [nāšal](../../strongs/h/h5394.md) [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) thee, the [Ḥitî](../../strongs/h/h2850.md), and the [Girgāšî](../../strongs/h/h1622.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), seven [gowy](../../strongs/h/h1471.md) [rab](../../strongs/h/h7227.md) and ['atsuwm](../../strongs/h/h6099.md) than thou;

<a name="deuteronomy_7_2"></a>Deuteronomy 7:2

And when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [nathan](../../strongs/h/h5414.md) them [paniym](../../strongs/h/h6440.md) thee; thou shalt [nakah](../../strongs/h/h5221.md) them, and [ḥāram](../../strongs/h/h2763.md) [ḥāram](../../strongs/h/h2763.md) them; thou shalt [karath](../../strongs/h/h3772.md) no [bĕriyth](../../strongs/h/h1285.md) with them, nor [chanan](../../strongs/h/h2603.md) unto them:

<a name="deuteronomy_7_3"></a>Deuteronomy 7:3

Neither shalt thou [ḥāṯan](../../strongs/h/h2859.md) with them; thy [bath](../../strongs/h/h1323.md) thou shalt not [nathan](../../strongs/h/h5414.md) unto his [ben](../../strongs/h/h1121.md), nor his [bath](../../strongs/h/h1323.md) shalt thou [laqach](../../strongs/h/h3947.md) unto thy [ben](../../strongs/h/h1121.md).

<a name="deuteronomy_7_4"></a>Deuteronomy 7:4

For they will [cuwr](../../strongs/h/h5493.md) thy [ben](../../strongs/h/h1121.md) from ['aḥar](../../strongs/h/h310.md) me, that they may ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md): so will the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) be [ḥārâ](../../strongs/h/h2734.md) against you, and [šāmaḏ](../../strongs/h/h8045.md) thee [mahēr](../../strongs/h/h4118.md).

<a name="deuteronomy_7_5"></a>Deuteronomy 7:5

But thus shall ye ['asah](../../strongs/h/h6213.md) with them; ye shall [nāṯaṣ](../../strongs/h/h5422.md) their [mizbeach](../../strongs/h/h4196.md), and [shabar](../../strongs/h/h7665.md) their [maṣṣēḇâ](../../strongs/h/h4676.md), and [gāḏaʿ](../../strongs/h/h1438.md) their ['ăšērâ](../../strongs/h/h842.md), and [śārap̄](../../strongs/h/h8313.md) their [pāsîl](../../strongs/h/h6456.md) with ['esh](../../strongs/h/h784.md).

<a name="deuteronomy_7_6"></a>Deuteronomy 7:6

For thou art a [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [bāḥar](../../strongs/h/h977.md) thee to be a [sᵊḡullâ](../../strongs/h/h5459.md) ['am](../../strongs/h/h5971.md) unto himself, above all ['am](../../strongs/h/h5971.md) that are upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="deuteronomy_7_7"></a>Deuteronomy 7:7

[Yĕhovah](../../strongs/h/h3068.md) did not [ḥāšaq](../../strongs/h/h2836.md) upon you, nor [bāḥar](../../strongs/h/h977.md) you, because ye were [rōḇ](../../strongs/h/h7230.md) in number than any ['am](../../strongs/h/h5971.md); for ye were the [mᵊʿaṭ](../../strongs/h/h4592.md) of all ['am](../../strongs/h/h5971.md):

<a name="deuteronomy_7_8"></a>Deuteronomy 7:8

But because [Yĕhovah](../../strongs/h/h3068.md) ['ahăḇâ](../../strongs/h/h160.md) you, and because he would [shamar](../../strongs/h/h8104.md) the [šᵊḇûʿâ](../../strongs/h/h7621.md) which he had [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md), hath [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) you with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and [pāḏâ](../../strongs/h/h6299.md) you out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md), from the [yad](../../strongs/h/h3027.md) of [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_7_9"></a>Deuteronomy 7:9

[yada'](../../strongs/h/h3045.md) therefore that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), he is ['Elohiym](../../strongs/h/h430.md), the ['aman](../../strongs/h/h539.md) ['el](../../strongs/h/h410.md), which [shamar](../../strongs/h/h8104.md) [bĕriyth](../../strongs/h/h1285.md) and [checed](../../strongs/h/h2617.md) with them that ['ahab](../../strongs/h/h157.md) him and [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md) to an ['elep̄](../../strongs/h/h505.md) [dôr](../../strongs/h/h1755.md);

<a name="deuteronomy_7_10"></a>Deuteronomy 7:10

And [shalam](../../strongs/h/h7999.md) them that [sane'](../../strongs/h/h8130.md) him to their [paniym](../../strongs/h/h6440.md), to ['abad](../../strongs/h/h6.md) them: he will not be ['āḥar](../../strongs/h/h309.md) to him that [sane'](../../strongs/h/h8130.md) him, he will [shalam](../../strongs/h/h7999.md) him to his [paniym](../../strongs/h/h6440.md).

<a name="deuteronomy_7_11"></a>Deuteronomy 7:11

Thou shalt therefore [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md), and the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), to ['asah](../../strongs/h/h6213.md) them.

<a name="deuteronomy_7_12"></a>Deuteronomy 7:12

Wherefore it shall come to pass, if ye [shama'](../../strongs/h/h8085.md) to these [mishpat](../../strongs/h/h4941.md), and [shamar](../../strongs/h/h8104.md), and ['asah](../../strongs/h/h6213.md) them, that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [shamar](../../strongs/h/h8104.md) unto thee the [bĕriyth](../../strongs/h/h1285.md) and the [checed](../../strongs/h/h2617.md) which he [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md):

<a name="deuteronomy_7_13"></a>Deuteronomy 7:13

And he will ['ahab](../../strongs/h/h157.md) thee, and [barak](../../strongs/h/h1288.md) thee, and [rabah](../../strongs/h/h7235.md) thee: he will also [barak](../../strongs/h/h1288.md) the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md), and the [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md), thy [dagan](../../strongs/h/h1715.md), and thy [tiyrowsh](../../strongs/h/h8492.md), and thine [yiṣhār](../../strongs/h/h3323.md), the [šeḡer](../../strongs/h/h7698.md) of thy ['eleph](../../strongs/h/h504.md), and the [ʿaštārôṯ](../../strongs/h/h6251.md) of thy [tso'n](../../strongs/h/h6629.md), in the ['ăḏāmâ](../../strongs/h/h127.md) which he [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_7_14"></a>Deuteronomy 7:14

Thou shalt be [barak](../../strongs/h/h1288.md) above all ['am](../../strongs/h/h5971.md): there shall not be [ʿāqār](../../strongs/h/h6135.md) or [ʿāqār](../../strongs/h/h6135.md) among you, or among your [bĕhemah](../../strongs/h/h929.md).

<a name="deuteronomy_7_15"></a>Deuteronomy 7:15

And [Yĕhovah](../../strongs/h/h3068.md) will [cuwr](../../strongs/h/h5493.md) from thee all [ḥŏlî](../../strongs/h/h2483.md), and will [śûm](../../strongs/h/h7760.md) none of the [ra'](../../strongs/h/h7451.md) [maḏvê](../../strongs/h/h4064.md) of [Mitsrayim](../../strongs/h/h4714.md), which thou [yada'](../../strongs/h/h3045.md), upon thee; but will [nathan](../../strongs/h/h5414.md) them upon all them that [sane'](../../strongs/h/h8130.md) thee.

<a name="deuteronomy_7_16"></a>Deuteronomy 7:16

And thou shalt ['akal](../../strongs/h/h398.md) all the ['am](../../strongs/h/h5971.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [nathan](../../strongs/h/h5414.md) thee; thine ['ayin](../../strongs/h/h5869.md) shall have no [ḥûs](../../strongs/h/h2347.md) upon them: neither shalt thou ['abad](../../strongs/h/h5647.md) their ['Elohiym](../../strongs/h/h430.md); for that will be a [mowqesh](../../strongs/h/h4170.md) unto thee.

<a name="deuteronomy_7_17"></a>Deuteronomy 7:17

If thou shalt ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), These [gowy](../../strongs/h/h1471.md) are [rab](../../strongs/h/h7227.md) than I; how [yakol](../../strongs/h/h3201.md) I [yarash](../../strongs/h/h3423.md) them?

<a name="deuteronomy_7_18"></a>Deuteronomy 7:18

Thou shalt not be [yare'](../../strongs/h/h3372.md) of them: but shalt [zakar](../../strongs/h/h2142.md) [zakar](../../strongs/h/h2142.md) what [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) unto [Parʿô](../../strongs/h/h6547.md), and unto all [Mitsrayim](../../strongs/h/h4714.md);

<a name="deuteronomy_7_19"></a>Deuteronomy 7:19

The [gadowl](../../strongs/h/h1419.md) [massâ](../../strongs/h/h4531.md) which thine ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md), and the ['ôṯ](../../strongs/h/h226.md), and the [môp̄ēṯ](../../strongs/h/h4159.md), and the [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and the [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), whereby [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [yāṣā'](../../strongs/h/h3318.md) thee: so shall [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) unto all the ['am](../../strongs/h/h5971.md) of whom thou art [yārē'](../../strongs/h/h3373.md) [paniym](../../strongs/h/h6440.md).

<a name="deuteronomy_7_20"></a>Deuteronomy 7:20

Moreover [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [shalach](../../strongs/h/h7971.md) the [ṣirʿâ](../../strongs/h/h6880.md) among them, until they that are [šā'ar](../../strongs/h/h7604.md), and [cathar](../../strongs/h/h5641.md) [paniym](../../strongs/h/h6440.md) thee, be ['abad](../../strongs/h/h6.md).

<a name="deuteronomy_7_21"></a>Deuteronomy 7:21

Thou shalt not be [ʿāraṣ](../../strongs/h/h6206.md) [paniym](../../strongs/h/h6440.md) them: for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is [qereḇ](../../strongs/h/h7130.md) you, a [gadowl](../../strongs/h/h1419.md) ['el](../../strongs/h/h410.md) and [yare'](../../strongs/h/h3372.md).

<a name="deuteronomy_7_22"></a>Deuteronomy 7:22

And [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [nāšal](../../strongs/h/h5394.md) those [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) thee by [mᵊʿaṭ](../../strongs/h/h4592.md) and [mᵊʿaṭ](../../strongs/h/h4592.md): thou [yakol](../../strongs/h/h3201.md) not [kalah](../../strongs/h/h3615.md) them [mahēr](../../strongs/h/h4118.md), lest the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) [rabah](../../strongs/h/h7235.md) upon thee.

<a name="deuteronomy_7_23"></a>Deuteronomy 7:23

But [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [nathan](../../strongs/h/h5414.md) them unto [paniym](../../strongs/h/h6440.md), and shall [huwm](../../strongs/h/h1949.md) them with a [gadowl](../../strongs/h/h1419.md) [mᵊhûmâ](../../strongs/h/h4103.md), until they be [šāmaḏ](../../strongs/h/h8045.md).

<a name="deuteronomy_7_24"></a>Deuteronomy 7:24

And he shall [nathan](../../strongs/h/h5414.md) their [melek](../../strongs/h/h4428.md) into thine [yad](../../strongs/h/h3027.md), and thou shalt ['abad](../../strongs/h/h6.md) their [shem](../../strongs/h/h8034.md) from under [shamayim](../../strongs/h/h8064.md): there shall no ['iysh](../../strongs/h/h376.md) be able to [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) thee, until thou have [šāmaḏ](../../strongs/h/h8045.md) them.

<a name="deuteronomy_7_25"></a>Deuteronomy 7:25

The [pāsîl](../../strongs/h/h6456.md) of their ['Elohiym](../../strongs/h/h430.md) shall ye [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md): thou shalt not [chamad](../../strongs/h/h2530.md) the [keceph](../../strongs/h/h3701.md) or [zāhāḇ](../../strongs/h/h2091.md) that is on them, nor [laqach](../../strongs/h/h3947.md) it unto thee, lest thou be [yāqōš](../../strongs/h/h3369.md) therin: for it is a [tôʿēḇâ](../../strongs/h/h8441.md) to [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_7_26"></a>Deuteronomy 7:26

Neither shalt thou [bow'](../../strongs/h/h935.md) a [tôʿēḇâ](../../strongs/h/h8441.md) into thine [bayith](../../strongs/h/h1004.md), lest thou be a [ḥērem](../../strongs/h/h2764.md) like it: but thou shalt [šāqaṣ](../../strongs/h/h8262.md) [šāqaṣ](../../strongs/h/h8262.md) it, and thou shalt [ta'ab](../../strongs/h/h8581.md) [ta'ab](../../strongs/h/h8581.md) it; for it is a [ḥērem](../../strongs/h/h2764.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 6](deuteronomy_6.md) - [Deuteronomy 8](deuteronomy_8.md)