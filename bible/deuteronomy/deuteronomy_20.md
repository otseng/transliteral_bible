# [Deuteronomy 20](https://www.blueletterbible.org/kjv/deu/20)

<a name="deuteronomy_20_1"></a>Deuteronomy 20:1

When thou [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against thine ['oyeb](../../strongs/h/h341.md), and [ra'ah](../../strongs/h/h7200.md) [sûs](../../strongs/h/h5483.md), and [reḵeḇ](../../strongs/h/h7393.md), and an ['am](../../strongs/h/h5971.md) [rab](../../strongs/h/h7227.md) than thou, be not [yare'](../../strongs/h/h3372.md) of them: for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is with thee, which [ʿālâ](../../strongs/h/h5927.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="deuteronomy_20_2"></a>Deuteronomy 20:2

And it shall be, when ye are [qāraḇ](../../strongs/h/h7126.md) unto the [milḥāmâ](../../strongs/h/h4421.md), that the [kōhēn](../../strongs/h/h3548.md) shall [nāḡaš](../../strongs/h/h5066.md) and [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md),

<a name="deuteronomy_20_3"></a>Deuteronomy 20:3

And shall ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md), ye [qārēḇ](../../strongs/h/h7131.md) this [yowm](../../strongs/h/h3117.md) unto [milḥāmâ](../../strongs/h/h4421.md) against your ['oyeb](../../strongs/h/h341.md): let not your [lebab](../../strongs/h/h3824.md) [rāḵaḵ](../../strongs/h/h7401.md), [yare'](../../strongs/h/h3372.md) not, and do not [ḥāp̄az](../../strongs/h/h2648.md), neither be ye [ʿāraṣ](../../strongs/h/h6206.md) [paniym](../../strongs/h/h6440.md) of them;

<a name="deuteronomy_20_4"></a>Deuteronomy 20:4

For [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) is he that [halak](../../strongs/h/h1980.md) with you, to [lāḥam](../../strongs/h/h3898.md) for you against your ['oyeb](../../strongs/h/h341.md), to [yasha'](../../strongs/h/h3467.md) you.

<a name="deuteronomy_20_5"></a>Deuteronomy 20:5

And the [šāṭar](../../strongs/h/h7860.md) shall [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), What ['iysh](../../strongs/h/h376.md) is there that hath [bānâ](../../strongs/h/h1129.md) a [ḥāḏāš](../../strongs/h/h2319.md) [bayith](../../strongs/h/h1004.md), and hath not [ḥānaḵ](../../strongs/h/h2596.md) it? let him [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) to his [bayith](../../strongs/h/h1004.md), lest he [muwth](../../strongs/h/h4191.md) in the [milḥāmâ](../../strongs/h/h4421.md), and ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md) [ḥānaḵ](../../strongs/h/h2596.md) it.

<a name="deuteronomy_20_6"></a>Deuteronomy 20:6

And what ['iysh](../../strongs/h/h376.md) is he that hath [nāṭaʿ](../../strongs/h/h5193.md) a [kerem](../../strongs/h/h3754.md), and hath not yet [ḥālal](../../strongs/h/h2490.md) of it? let him also [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) unto his [bayith](../../strongs/h/h1004.md), lest he [muwth](../../strongs/h/h4191.md) in the [milḥāmâ](../../strongs/h/h4421.md), and ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md) [ḥālal](../../strongs/h/h2490.md) of it.

<a name="deuteronomy_20_7"></a>Deuteronomy 20:7

And what ['iysh](../../strongs/h/h376.md) is there that hath ['āraś](../../strongs/h/h781.md) an ['ishshah](../../strongs/h/h802.md), and hath not [laqach](../../strongs/h/h3947.md) her? let him [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) unto his [bayith](../../strongs/h/h1004.md), lest he [muwth](../../strongs/h/h4191.md) in the [milḥāmâ](../../strongs/h/h4421.md), and ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md) [laqach](../../strongs/h/h3947.md) her.

<a name="deuteronomy_20_8"></a>Deuteronomy 20:8

And the [šāṭar](../../strongs/h/h7860.md) shall [dabar](../../strongs/h/h1696.md) [yāsap̄](../../strongs/h/h3254.md) unto the ['am](../../strongs/h/h5971.md), and they shall ['āmar](../../strongs/h/h559.md), What ['iysh](../../strongs/h/h376.md) is there that is [yārē'](../../strongs/h/h3373.md) and [raḵ](../../strongs/h/h7390.md) [lebab](../../strongs/h/h3824.md)? let him [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) unto his [bayith](../../strongs/h/h1004.md), lest his ['ach](../../strongs/h/h251.md) [lebab](../../strongs/h/h3824.md) [māsas](../../strongs/h/h4549.md) as well as his [lebab](../../strongs/h/h3824.md).

<a name="deuteronomy_20_9"></a>Deuteronomy 20:9

And it shall be, when the [šāṭar](../../strongs/h/h7860.md) have made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md) that they shall [paqad](../../strongs/h/h6485.md) [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) to [ro'sh](../../strongs/h/h7218.md) the ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_20_10"></a>Deuteronomy 20:10

When thou [qāraḇ](../../strongs/h/h7126.md) unto an [ʿîr](../../strongs/h/h5892.md) to [lāḥam](../../strongs/h/h3898.md) against it, then [qara'](../../strongs/h/h7121.md) [shalowm](../../strongs/h/h7965.md) unto it.

<a name="deuteronomy_20_11"></a>Deuteronomy 20:11

And it shall be, if it make thee ['anah](../../strongs/h/h6030.md) of [shalowm](../../strongs/h/h7965.md), and [pāṯaḥ](../../strongs/h/h6605.md) unto thee, then it shall be, that all the ['am](../../strongs/h/h5971.md) that is [māṣā'](../../strongs/h/h4672.md) therein shall be [mas](../../strongs/h/h4522.md) unto thee, and they shall ['abad](../../strongs/h/h5647.md) thee.

<a name="deuteronomy_20_12"></a>Deuteronomy 20:12

And if it will make no [shalam](../../strongs/h/h7999.md) with thee, but will ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) against thee, then thou shalt [ṣûr](../../strongs/h/h6696.md) it:

<a name="deuteronomy_20_13"></a>Deuteronomy 20:13

And when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) it into thine [yad](../../strongs/h/h3027.md), thou shalt [nakah](../../strongs/h/h5221.md) every [zāḵûr](../../strongs/h/h2138.md) thereof with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md):

<a name="deuteronomy_20_14"></a>Deuteronomy 20:14

But the ['ishshah](../../strongs/h/h802.md), and the [ṭap̄](../../strongs/h/h2945.md), and the [bĕhemah](../../strongs/h/h929.md), and all that is in the [ʿîr](../../strongs/h/h5892.md), even all the [šālāl](../../strongs/h/h7998.md) thereof, shalt thou [bāzaz](../../strongs/h/h962.md) unto thyself; and thou shalt ['akal](../../strongs/h/h398.md) the [šālāl](../../strongs/h/h7998.md) of thine ['oyeb](../../strongs/h/h341.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_20_15"></a>Deuteronomy 20:15

Thus shalt thou ['asah](../../strongs/h/h6213.md) unto all the [ʿîr](../../strongs/h/h5892.md) which are [me'od](../../strongs/h/h3966.md) [rachowq](../../strongs/h/h7350.md) from thee, which are not of the [ʿîr](../../strongs/h/h5892.md) of these [gowy](../../strongs/h/h1471.md).

<a name="deuteronomy_20_16"></a>Deuteronomy 20:16

But of the [ʿîr](../../strongs/h/h5892.md) of these ['am](../../strongs/h/h5971.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) doth [nathan](../../strongs/h/h5414.md) thee for a [nachalah](../../strongs/h/h5159.md), thou shalt [ḥāyâ](../../strongs/h/h2421.md) nothing that [neshamah](../../strongs/h/h5397.md):

<a name="deuteronomy_20_17"></a>Deuteronomy 20:17

But thou shalt [ḥāram](../../strongs/h/h2763.md) [ḥāram](../../strongs/h/h2763.md) them; namely, the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md); as [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) thee:

<a name="deuteronomy_20_18"></a>Deuteronomy 20:18

That they [lamad](../../strongs/h/h3925.md) you not to ['asah](../../strongs/h/h6213.md) after all their [tôʿēḇâ](../../strongs/h/h8441.md), which they have ['asah](../../strongs/h/h6213.md) unto their ['Elohiym](../../strongs/h/h430.md); so should ye [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_20_19"></a>Deuteronomy 20:19

When thou shalt [ṣûr](../../strongs/h/h6696.md) an [ʿîr](../../strongs/h/h5892.md) a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), in [lāḥam](../../strongs/h/h3898.md) against it to [tāp̄aś](../../strongs/h/h8610.md) it, thou shalt not [shachath](../../strongs/h/h7843.md) the ['ets](../../strongs/h/h6086.md) thereof by [nāḏaḥ](../../strongs/h/h5080.md) a [garzen](../../strongs/h/h1631.md) against them: for thou mayest ['akal](../../strongs/h/h398.md) of them, and thou shalt not [karath](../../strongs/h/h3772.md) them (for the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) is ['adam](../../strongs/h/h120.md)) to [bow'](../../strongs/h/h935.md) them in the [paniym](../../strongs/h/h6440.md) [māṣôr](../../strongs/h/h4692.md):

<a name="deuteronomy_20_20"></a>Deuteronomy 20:20

Only the ['ets](../../strongs/h/h6086.md) which thou [yada'](../../strongs/h/h3045.md) that they be not ['ets](../../strongs/h/h6086.md) for [ma'akal](../../strongs/h/h3978.md), thou shalt [shachath](../../strongs/h/h7843.md) and [karath](../../strongs/h/h3772.md) them; and thou shalt [bānâ](../../strongs/h/h1129.md) [māṣôr](../../strongs/h/h4692.md) against the [ʿîr](../../strongs/h/h5892.md) that ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) with thee, until it be [yarad](../../strongs/h/h3381.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 19](deuteronomy_19.md) - [Deuteronomy 21](deuteronomy_21.md)