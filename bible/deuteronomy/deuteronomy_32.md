# [Deuteronomy 32](https://www.blueletterbible.org/kjv/deu/32)

<a name="deuteronomy_32_1"></a>Deuteronomy 32:1

['azan](../../strongs/h/h238.md), O ye [shamayim](../../strongs/h/h8064.md), and I will [dabar](../../strongs/h/h1696.md); and [shama'](../../strongs/h/h8085.md), ['erets](../../strongs/h/h776.md), the ['emer](../../strongs/h/h561.md) of my [peh](../../strongs/h/h6310.md).

<a name="deuteronomy_32_2"></a>Deuteronomy 32:2

My [leqaḥ](../../strongs/h/h3948.md) shall [ʿārap̄](../../strongs/h/h6201.md) as the [māṭār](../../strongs/h/h4306.md), my ['imrah](../../strongs/h/h565.md) shall [nāzal](../../strongs/h/h5140.md) as the [ṭal](../../strongs/h/h2919.md), as the [śāʿîr](../../strongs/h/h8164.md) upon the [deše'](../../strongs/h/h1877.md), and as the [rᵊḇîḇîm](../../strongs/h/h7241.md) upon the ['eseb](../../strongs/h/h6212.md):

<a name="deuteronomy_32_3"></a>Deuteronomy 32:3

Because I will [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): [yāhaḇ](../../strongs/h/h3051.md) ye [gōḏel](../../strongs/h/h1433.md) unto our ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_32_4"></a>Deuteronomy 32:4

He is the [tsuwr](../../strongs/h/h6697.md), his [pōʿal](../../strongs/h/h6467.md) is [tamiym](../../strongs/h/h8549.md): for all his [derek](../../strongs/h/h1870.md) are [mishpat](../../strongs/h/h4941.md): an ['el](../../strongs/h/h410.md) of ['ĕmûnâ](../../strongs/h/h530.md) and without ['evel](../../strongs/h/h5766.md), [tsaddiyq](../../strongs/h/h6662.md) and [yashar](../../strongs/h/h3477.md) is he.

<a name="deuteronomy_32_5"></a>Deuteronomy 32:5

They have [shachath](../../strongs/h/h7843.md) themselves, their [mᵊ'ûm](../../strongs/h/h3971.md) is not the spot of his [ben](../../strongs/h/h1121.md): they are a [ʿiqqēš](../../strongs/h/h6141.md) and [pᵊṯaltōl](../../strongs/h/h6618.md) [dôr](../../strongs/h/h1755.md).

<a name="deuteronomy_32_6"></a>Deuteronomy 32:6

Do ye thus [gamal](../../strongs/h/h1580.md) [Yĕhovah](../../strongs/h/h3068.md), O [nabal](../../strongs/h/h5036.md) ['am](../../strongs/h/h5971.md) and [ḥāḵām](../../strongs/h/h2450.md)? is not he thy ['ab](../../strongs/h/h1.md) that hath [qānâ](../../strongs/h/h7069.md) thee? hath he not ['asah](../../strongs/h/h6213.md) thee, and [kuwn](../../strongs/h/h3559.md) thee?

<a name="deuteronomy_32_7"></a>Deuteronomy 32:7

[zakar](../../strongs/h/h2142.md) the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md), [bîn](../../strongs/h/h995.md) the [šānâ](../../strongs/h/h8141.md) of [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md): [sha'al](../../strongs/h/h7592.md) thy ['ab](../../strongs/h/h1.md), and he will [nāḡaḏ](../../strongs/h/h5046.md) thee; thy [zāqēn](../../strongs/h/h2205.md), and they will ['āmar](../../strongs/h/h559.md) thee.

<a name="deuteronomy_32_8"></a>Deuteronomy 32:8

When the ['elyown](../../strongs/h/h5945.md) [nāḥal](../../strongs/h/h5157.md) to the [gowy](../../strongs/h/h1471.md), when he [pāraḏ](../../strongs/h/h6504.md) the [ben](../../strongs/h/h1121.md) of ['adam](../../strongs/h/h120.md), he [nāṣaḇ](../../strongs/h/h5324.md) the [gᵊḇûlâ](../../strongs/h/h1367.md) of the ['am](../../strongs/h/h5971.md) according to the [mispār](../../strongs/h/h4557.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_32_9"></a>Deuteronomy 32:9

For [Yĕhovah](../../strongs/h/h3068.md) [cheleq](../../strongs/h/h2506.md) is his ['am](../../strongs/h/h5971.md); [Ya'aqob](../../strongs/h/h3290.md) is the [chebel](../../strongs/h/h2256.md) of his [nachalah](../../strongs/h/h5159.md).

<a name="deuteronomy_32_10"></a>Deuteronomy 32:10

He [māṣā'](../../strongs/h/h4672.md) him in a [midbar](../../strongs/h/h4057.md) ['erets](../../strongs/h/h776.md), and in the [tohuw](../../strongs/h/h8414.md) [yᵊlēl](../../strongs/h/h3214.md) [yᵊšîmôn](../../strongs/h/h3452.md); he [cabab](../../strongs/h/h5437.md) him, he [bîn](../../strongs/h/h995.md) him, he [nāṣar](../../strongs/h/h5341.md) him as the ['iyshown](../../strongs/h/h380.md) of his ['ayin](../../strongs/h/h5869.md).

<a name="deuteronomy_32_11"></a>Deuteronomy 32:11

As a [nesheׁr](../../strongs/h/h5404.md) [ʿûr](../../strongs/h/h5782.md) her [qēn](../../strongs/h/h7064.md), [rachaph](../../strongs/h/h7363.md) over her [gôzāl](../../strongs/h/h1469.md), [pāraś](../../strongs/h/h6566.md) her [kanaph](../../strongs/h/h3671.md), [laqach](../../strongs/h/h3947.md) them, [nasa'](../../strongs/h/h5375.md) them on her ['ēḇrâ](../../strongs/h/h84.md):

<a name="deuteronomy_32_12"></a>Deuteronomy 32:12

So [Yĕhovah](../../strongs/h/h3068.md) [bāḏāḏ](../../strongs/h/h910.md) did [nachah](../../strongs/h/h5148.md) him, and there was no [nēḵār](../../strongs/h/h5236.md) ['el](../../strongs/h/h410.md) with him.

<a name="deuteronomy_32_13"></a>Deuteronomy 32:13

He made him [rāḵaḇ](../../strongs/h/h7392.md) on the [bāmâ](../../strongs/h/h1116.md) of the ['erets](../../strongs/h/h776.md), that he might ['akal](../../strongs/h/h398.md) the [tᵊnûḇâ](../../strongs/h/h8570.md) of the [sadeh](../../strongs/h/h7704.md); and he made him to [yānaq](../../strongs/h/h3243.md) [dĕbash](../../strongs/h/h1706.md) out of the [cela'](../../strongs/h/h5553.md), and [šemen](../../strongs/h/h8081.md) out of the [ḥallāmîš](../../strongs/h/h2496.md) [tsuwr](../../strongs/h/h6697.md);

<a name="deuteronomy_32_14"></a>Deuteronomy 32:14

[ḥem'â](../../strongs/h/h2529.md) of [bāqār](../../strongs/h/h1241.md), and [chalab](../../strongs/h/h2461.md) of [tso'n](../../strongs/h/h6629.md), with [cheleb](../../strongs/h/h2459.md) of [kar](../../strongs/h/h3733.md), and ['ayil](../../strongs/h/h352.md) of the [ben](../../strongs/h/h1121.md) of [Bāšān](../../strongs/h/h1316.md), and [ʿatûḏ](../../strongs/h/h6260.md), with the [cheleb](../../strongs/h/h2459.md) of [kilyah](../../strongs/h/h3629.md) of [ḥiṭṭâ](../../strongs/h/h2406.md); and thou didst [šāṯâ](../../strongs/h/h8354.md) the [ḥemer](../../strongs/h/h2561.md) [dam](../../strongs/h/h1818.md) of the [ʿēnāḇ](../../strongs/h/h6025.md).

<a name="deuteronomy_32_15"></a>Deuteronomy 32:15

But [Yᵊšurûn](../../strongs/h/h3484.md) [šāman](../../strongs/h/h8080.md), and [bāʿaṭ](../../strongs/h/h1163.md): thou art [šāman](../../strongs/h/h8080.md), thou art [ʿāḇâ](../../strongs/h/h5666.md), thou art [kāśâ](../../strongs/h/h3780.md) with fatness; then he [nāṭaš](../../strongs/h/h5203.md) ['ĕlvôha](../../strongs/h/h433.md) which ['asah](../../strongs/h/h6213.md) him, and [nabel](../../strongs/h/h5034.md) the [tsuwr](../../strongs/h/h6697.md) of his [yĕshuw'ah](../../strongs/h/h3444.md).

<a name="deuteronomy_32_16"></a>Deuteronomy 32:16

They provoked him to [qānā'](../../strongs/h/h7065.md) with [zûr](../../strongs/h/h2114.md), with [tôʿēḇâ](../../strongs/h/h8441.md) [kāʿas](../../strongs/h/h3707.md) him.

<a name="deuteronomy_32_17"></a>Deuteronomy 32:17

They [zabach](../../strongs/h/h2076.md) unto [šēḏ](../../strongs/h/h7700.md), not to ['ĕlvôha](../../strongs/h/h433.md); to ['Elohiym](../../strongs/h/h430.md) whom they [yada'](../../strongs/h/h3045.md) not, to [ḥāḏāš](../../strongs/h/h2319.md) that [bow'](../../strongs/h/h935.md) [qarowb](../../strongs/h/h7138.md), whom your ['ab](../../strongs/h/h1.md) [śāʿar](../../strongs/h/h8175.md) not.

<a name="deuteronomy_32_18"></a>Deuteronomy 32:18

Of the [tsuwr](../../strongs/h/h6697.md) that [yalad](../../strongs/h/h3205.md) thee thou art [šāyâ](../../strongs/h/h7876.md), and hast [shakach](../../strongs/h/h7911.md) ['el](../../strongs/h/h410.md) that [chuwl](../../strongs/h/h2342.md) thee.

<a name="deuteronomy_32_19"></a>Deuteronomy 32:19

And when [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) it, he [na'ats](../../strongs/h/h5006.md) them, because of the [ka'ac](../../strongs/h/h3708.md) of his [ben](../../strongs/h/h1121.md), and of his [bath](../../strongs/h/h1323.md).

<a name="deuteronomy_32_20"></a>Deuteronomy 32:20

And he ['āmar](../../strongs/h/h559.md), I will [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) from them, I will [ra'ah](../../strongs/h/h7200.md) what their ['aḥărîṯ](../../strongs/h/h319.md) shall be: for they are a very [tahpuḵôṯ](../../strongs/h/h8419.md) [dôr](../../strongs/h/h1755.md), [ben](../../strongs/h/h1121.md) in whom is no ['ēmûn](../../strongs/h/h529.md).

<a name="deuteronomy_32_21"></a>Deuteronomy 32:21

They have [qānā'](../../strongs/h/h7065.md) me with that which is not ['el](../../strongs/h/h410.md); they have [kāʿas](../../strongs/h/h3707.md) me with their [heḇel](../../strongs/h/h1892.md): and I will [qānā'](../../strongs/h/h7065.md) them with those which are not an ['am](../../strongs/h/h5971.md); I will [kāʿas](../../strongs/h/h3707.md) them with a [nabal](../../strongs/h/h5036.md) [gowy](../../strongs/h/h1471.md).

<a name="deuteronomy_32_22"></a>Deuteronomy 32:22

For an ['esh](../../strongs/h/h784.md) is [qāḏaḥ](../../strongs/h/h6919.md) in mine ['aph](../../strongs/h/h639.md), and shall [yāqaḏ](../../strongs/h/h3344.md) unto the [taḥtî](../../strongs/h/h8482.md) [shĕ'owl](../../strongs/h/h7585.md), and shall ['akal](../../strongs/h/h398.md) the ['erets](../../strongs/h/h776.md) with her [yᵊḇûl](../../strongs/h/h2981.md), and [lāhaṭ](../../strongs/h/h3857.md) the [môsāḏ](../../strongs/h/h4144.md) of the [har](../../strongs/h/h2022.md).

<a name="deuteronomy_32_23"></a>Deuteronomy 32:23

I will [sāp̄â](../../strongs/h/h5595.md) [ra'](../../strongs/h/h7451.md) upon them; I will [kalah](../../strongs/h/h3615.md) mine [chets](../../strongs/h/h2671.md) upon them.

<a name="deuteronomy_32_24"></a>Deuteronomy 32:24

They shall be [māzê](../../strongs/h/h4198.md) with [rāʿāḇ](../../strongs/h/h7458.md), and [lāḥam](../../strongs/h/h3898.md) with [rešep̄](../../strongs/h/h7565.md), and with [mᵊrîrî](../../strongs/h/h4815.md) [qeṭeḇ](../../strongs/h/h6986.md): I will also [shalach](../../strongs/h/h7971.md) the [šēn](../../strongs/h/h8127.md) of [bĕhemah](../../strongs/h/h929.md) upon them, with the [chemah](../../strongs/h/h2534.md) of [zāḥal](../../strongs/h/h2119.md) of the ['aphar](../../strongs/h/h6083.md).

<a name="deuteronomy_32_25"></a>Deuteronomy 32:25

The [chereb](../../strongs/h/h2719.md) [ḥûṣ](../../strongs/h/h2351.md), and ['êmâ](../../strongs/h/h367.md) [ḥeḏer](../../strongs/h/h2315.md), shall [šāḵōl](../../strongs/h/h7921.md) both the [bāḥûr](../../strongs/h/h970.md) and the [bᵊṯûlâ](../../strongs/h/h1330.md), the [yānaq](../../strongs/h/h3243.md) also with the ['iysh](../../strongs/h/h376.md) of [śêḇâ](../../strongs/h/h7872.md).

<a name="deuteronomy_32_26"></a>Deuteronomy 32:26

I ['āmar](../../strongs/h/h559.md), I would [pā'â](../../strongs/h/h6284.md) them, I would make the [zeker](../../strongs/h/h2143.md) of them to [shabath](../../strongs/h/h7673.md) from among ['enowsh](../../strongs/h/h582.md):

<a name="deuteronomy_32_27"></a>Deuteronomy 32:27

Were it not that I [guwr](../../strongs/h/h1481.md) the [ka'ac](../../strongs/h/h3708.md) of the ['oyeb](../../strongs/h/h341.md), lest their [tsar](../../strongs/h/h6862.md) should [nāḵar](../../strongs/h/h5234.md) themselves, and lest they should ['āmar](../../strongs/h/h559.md), Our [yad](../../strongs/h/h3027.md) is [ruwm](../../strongs/h/h7311.md), and [Yĕhovah](../../strongs/h/h3068.md) hath not [pa'al](../../strongs/h/h6466.md) all this.

<a name="deuteronomy_32_28"></a>Deuteronomy 32:28

For they are a [gowy](../../strongs/h/h1471.md) ['abad](../../strongs/h/h6.md) of ['etsah](../../strongs/h/h6098.md), neither is there any [tāḇûn](../../strongs/h/h8394.md) in them.

<a name="deuteronomy_32_29"></a>Deuteronomy 32:29

O that they were [ḥāḵam](../../strongs/h/h2449.md), that they [sakal](../../strongs/h/h7919.md) this, that they would [bîn](../../strongs/h/h995.md) their ['aḥărîṯ](../../strongs/h/h319.md)!

<a name="deuteronomy_32_30"></a>Deuteronomy 32:30

How should one [radaph](../../strongs/h/h7291.md) a thousand, and two put ten thousand to [nûs](../../strongs/h/h5127.md), except their [tsuwr](../../strongs/h/h6697.md) had [māḵar](../../strongs/h/h4376.md) them, and [Yĕhovah](../../strongs/h/h3068.md) had [cagar](../../strongs/h/h5462.md) them?

<a name="deuteronomy_32_31"></a>Deuteronomy 32:31

For their [tsuwr](../../strongs/h/h6697.md) is not as our [tsuwr](../../strongs/h/h6697.md), even our ['oyeb](../../strongs/h/h341.md) themselves being [pālîl](../../strongs/h/h6414.md).

<a name="deuteronomy_32_32"></a>Deuteronomy 32:32

For their [gep̄en](../../strongs/h/h1612.md) is of the [gep̄en](../../strongs/h/h1612.md) of [Sᵊḏōm](../../strongs/h/h5467.md), and of the [šᵊḏēmâ](../../strongs/h/h7709.md) of [ʿĂmōrâ](../../strongs/h/h6017.md): their [ʿēnāḇ](../../strongs/h/h6025.md) are [ʿēnāḇ](../../strongs/h/h6025.md) of [rō'š](../../strongs/h/h7219.md), their ['eškōôl](../../strongs/h/h811.md) are [mᵊrōrâ](../../strongs/h/h4846.md):

<a name="deuteronomy_32_33"></a>Deuteronomy 32:33

Their [yayin](../../strongs/h/h3196.md) is the [chemah](../../strongs/h/h2534.md) of [tannîn](../../strongs/h/h8577.md), and the ['aḵzār](../../strongs/h/h393.md) [rō'š](../../strongs/h/h7219.md) of [peṯen](../../strongs/h/h6620.md).

<a name="deuteronomy_32_34"></a>Deuteronomy 32:34

Is not this laid up in [kāmas](../../strongs/h/h3647.md) with me, and [ḥāṯam](../../strongs/h/h2856.md) among my ['ôṣār](../../strongs/h/h214.md)?

<a name="deuteronomy_32_35"></a>Deuteronomy 32:35

To me belongeth [nāqām](../../strongs/h/h5359.md) and [šillēm](../../strongs/h/h8005.md); their [regel](../../strongs/h/h7272.md) shall [mowt](../../strongs/h/h4131.md) in [ʿēṯ](../../strongs/h/h6256.md): for the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md) is [qarowb](../../strongs/h/h7138.md), and the things that shall [ʿāṯîḏ](../../strongs/h/h6264.md) upon them make [ḥûš](../../strongs/h/h2363.md).

<a name="deuteronomy_32_36"></a>Deuteronomy 32:36

For [Yĕhovah](../../strongs/h/h3068.md) shall [diyn](../../strongs/h/h1777.md) his ['am](../../strongs/h/h5971.md), and [nacham](../../strongs/h/h5162.md) himself for his ['ebed](../../strongs/h/h5650.md), when he [ra'ah](../../strongs/h/h7200.md) that their [yad](../../strongs/h/h3027.md) is ['āzal](../../strongs/h/h235.md), and there is ['ep̄es](../../strongs/h/h657.md) [ʿāṣar](../../strongs/h/h6113.md), or ['azab](../../strongs/h/h5800.md).

<a name="deuteronomy_32_37"></a>Deuteronomy 32:37

And he shall ['āmar](../../strongs/h/h559.md), Where are their ['Elohiym](../../strongs/h/h430.md), their [tsuwr](../../strongs/h/h6697.md) in whom they [chacah](../../strongs/h/h2620.md),

<a name="deuteronomy_32_38"></a>Deuteronomy 32:38

Which did ['akal](../../strongs/h/h398.md) the [cheleb](../../strongs/h/h2459.md) of their [zebach](../../strongs/h/h2077.md), and [šāṯâ](../../strongs/h/h8354.md) the [yayin](../../strongs/h/h3196.md) of their [nāsîḵ](../../strongs/h/h5257.md)? let them [quwm](../../strongs/h/h6965.md) and [ʿāzar](../../strongs/h/h5826.md) you, and be your [cether](../../strongs/h/h5643.md).

<a name="deuteronomy_32_39"></a>Deuteronomy 32:39

[ra'ah](../../strongs/h/h7200.md) now that I, even I, am he, and there is no ['Elohiym](../../strongs/h/h430.md) with me: I [muwth](../../strongs/h/h4191.md), and I make [ḥāyâ](../../strongs/h/h2421.md); I [māḥaṣ](../../strongs/h/h4272.md), and I [rapha'](../../strongs/h/h7495.md): neither is there any that can [natsal](../../strongs/h/h5337.md) out of my [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_32_40"></a>Deuteronomy 32:40

For I [nasa'](../../strongs/h/h5375.md) my [yad](../../strongs/h/h3027.md) to [shamayim](../../strongs/h/h8064.md), and ['āmar](../../strongs/h/h559.md), I [chay](../../strongs/h/h2416.md) ['owlam](../../strongs/h/h5769.md).

<a name="deuteronomy_32_41"></a>Deuteronomy 32:41

If I [šānan](../../strongs/h/h8150.md) my [baraq](../../strongs/h/h1300.md) [chereb](../../strongs/h/h2719.md), and mine [yad](../../strongs/h/h3027.md) ['āḥaz](../../strongs/h/h270.md) on [mishpat](../../strongs/h/h4941.md); I will [shuwb](../../strongs/h/h7725.md) [nāqām](../../strongs/h/h5359.md) to mine [tsar](../../strongs/h/h6862.md), and will [shalam](../../strongs/h/h7999.md) them that [sane'](../../strongs/h/h8130.md) me.

<a name="deuteronomy_32_42"></a>Deuteronomy 32:42

I will make mine [chets](../../strongs/h/h2671.md) [šāḵar](../../strongs/h/h7937.md) with [dam](../../strongs/h/h1818.md), and my [chereb](../../strongs/h/h2719.md) shall ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md); and that with the [dam](../../strongs/h/h1818.md) of the [ḥālāl](../../strongs/h/h2491.md) and of the [šiḇyâ](../../strongs/h/h7633.md), from the [ro'sh](../../strongs/h/h7218.md) of [parʿâ](../../strongs/h/h6546.md) upon the ['oyeb](../../strongs/h/h341.md).

<a name="deuteronomy_32_43"></a>Deuteronomy 32:43

[ranan](../../strongs/h/h7442.md), O ye [gowy](../../strongs/h/h1471.md), with his ['am](../../strongs/h/h5971.md): for he will [naqam](../../strongs/h/h5358.md) the [dam](../../strongs/h/h1818.md) of his ['ebed](../../strongs/h/h5650.md), and will [shuwb](../../strongs/h/h7725.md) [nāqām](../../strongs/h/h5359.md) to his [tsar](../../strongs/h/h6862.md), and will be [kāp̄ar](../../strongs/h/h3722.md) unto his ['ăḏāmâ](../../strongs/h/h127.md), and to his ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_32_44"></a>Deuteronomy 32:44

And [Mōshe](../../strongs/h/h4872.md) [bow'](../../strongs/h/h935.md) and [dabar](../../strongs/h/h1696.md) all the [dabar](../../strongs/h/h1697.md) of this [šîr](../../strongs/h/h7892.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md), he, and [Hôšēaʿ](../../strongs/h/h1954.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

<a name="deuteronomy_32_45"></a>Deuteronomy 32:45

And [Mōshe](../../strongs/h/h4872.md) made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md) to all [Yisra'el](../../strongs/h/h3478.md):

<a name="deuteronomy_32_46"></a>Deuteronomy 32:46

And he ['āmar](../../strongs/h/h559.md) unto them, [śûm](../../strongs/h/h7760.md) your [lebab](../../strongs/h/h3824.md) unto all the [dabar](../../strongs/h/h1697.md) which I [ʿûḏ](../../strongs/h/h5749.md) among you this [yowm](../../strongs/h/h3117.md), which ye shall [tsavah](../../strongs/h/h6680.md) your [ben](../../strongs/h/h1121.md) to [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md), all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md).

<a name="deuteronomy_32_47"></a>Deuteronomy 32:47

For it is not a [reyq](../../strongs/h/h7386.md) [dabar](../../strongs/h/h1697.md) for you; because it is your [chay](../../strongs/h/h2416.md): and through this [dabar](../../strongs/h/h1697.md) ye shall ['arak](../../strongs/h/h748.md) your [yowm](../../strongs/h/h3117.md) in the ['ăḏāmâ](../../strongs/h/h127.md), whither ye ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_32_48"></a>Deuteronomy 32:48

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) that ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_32_49"></a>Deuteronomy 32:49

[ʿālâ](../../strongs/h/h5927.md) thee into this [har](../../strongs/h/h2022.md) [ʿĂḇārîm](../../strongs/h/h5682.md), unto [har](../../strongs/h/h2022.md) [Nᵊḇô](../../strongs/h/h5015.md), which is in the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), that is over [paniym](../../strongs/h/h6440.md) [Yᵊrēḥô](../../strongs/h/h3405.md); and [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which I [nathan](../../strongs/h/h5414.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) for an ['achuzzah](../../strongs/h/h272.md):

<a name="deuteronomy_32_50"></a>Deuteronomy 32:50

And [muwth](../../strongs/h/h4191.md) in the [har](../../strongs/h/h2022.md) whither thou [ʿālâ](../../strongs/h/h5927.md), and be ['āsap̄](../../strongs/h/h622.md) unto thy ['am](../../strongs/h/h5971.md); as ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md) [muwth](../../strongs/h/h4191.md) in [har](../../strongs/h/h2022.md) [Hōr](../../strongs/h/h2023.md), and was ['āsap̄](../../strongs/h/h622.md) unto his ['am](../../strongs/h/h5971.md):

<a name="deuteronomy_32_51"></a>Deuteronomy 32:51

Because ye [māʿal](../../strongs/h/h4603.md) against me [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) at the [mayim](../../strongs/h/h4325.md) of [Mᵊrîḇâ](../../strongs/h/h4808.md) [Qāḏēš](../../strongs/h/h6946.md), in the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md); because ye [qadash](../../strongs/h/h6942.md) me not in the [tavek](../../strongs/h/h8432.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_32_52"></a>Deuteronomy 32:52

Yet thou shalt [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md) before thee; but thou shalt not [bow'](../../strongs/h/h935.md) thither unto the ['erets](../../strongs/h/h776.md) which I [nathan](../../strongs/h/h5414.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 31](deuteronomy_31.md) - [Deuteronomy 33](deuteronomy_33.md)