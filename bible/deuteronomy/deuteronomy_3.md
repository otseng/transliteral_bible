# [Deuteronomy 3](https://www.blueletterbible.org/kjv/deu/3)

<a name="deuteronomy_3_1"></a>Deuteronomy 3:1

Then we [panah](../../strongs/h/h6437.md), and [ʿālâ](../../strongs/h/h5927.md) the [derek](../../strongs/h/h1870.md) to [Bāšān](../../strongs/h/h1316.md): and [ʿÔḡ](../../strongs/h/h5747.md) the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) us, he and all his ['am](../../strongs/h/h5971.md), to [milḥāmâ](../../strongs/h/h4421.md) at ['Eḏrᵊʿî](../../strongs/h/h154.md).

<a name="deuteronomy_3_2"></a>Deuteronomy 3:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [yare'](../../strongs/h/h3372.md) him not: for I will [nathan](../../strongs/h/h5414.md) him, and all his ['am](../../strongs/h/h5971.md), and his ['erets](../../strongs/h/h776.md), into thy [yad](../../strongs/h/h3027.md); and thou shalt ['asah](../../strongs/h/h6213.md) unto him as thou ['asah](../../strongs/h/h6213.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [yashab](../../strongs/h/h3427.md) at [Hešbôn](../../strongs/h/h2809.md).

<a name="deuteronomy_3_3"></a>Deuteronomy 3:3

So [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) into our [yad](../../strongs/h/h3027.md) [ʿÔḡ](../../strongs/h/h5747.md) also, the [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), and all his ['am](../../strongs/h/h5971.md): and we [nakah](../../strongs/h/h5221.md) him until none was [šā'ar](../../strongs/h/h7604.md) to him [śārîḏ](../../strongs/h/h8300.md).

<a name="deuteronomy_3_4"></a>Deuteronomy 3:4

And we [lāḵaḏ](../../strongs/h/h3920.md) all his [ʿîr](../../strongs/h/h5892.md) at that [ʿēṯ](../../strongs/h/h6256.md), there was not a [qiryâ](../../strongs/h/h7151.md) which we [laqach](../../strongs/h/h3947.md) not from them, threescore [ʿîr](../../strongs/h/h5892.md), all the [chebel](../../strongs/h/h2256.md) of ['Argōḇ](../../strongs/h/h709.md), the [mamlāḵâ](../../strongs/h/h4467.md) of [ʿÔḡ](../../strongs/h/h5747.md) in [Bāšān](../../strongs/h/h1316.md).

<a name="deuteronomy_3_5"></a>Deuteronomy 3:5

All these [ʿîr](../../strongs/h/h5892.md) were [bāṣar](../../strongs/h/h1219.md) with [gāḇōha](../../strongs/h/h1364.md) [ḥômâ](../../strongs/h/h2346.md), [deleṯ](../../strongs/h/h1817.md), and [bᵊrîaḥ](../../strongs/h/h1280.md); beside [pᵊrāzî](../../strongs/h/h6521.md) [ʿîr](../../strongs/h/h5892.md) a [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md).

<a name="deuteronomy_3_6"></a>Deuteronomy 3:6

And we [ḥāram](../../strongs/h/h2763.md) them, as we ['asah](../../strongs/h/h6213.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), [ḥāram](../../strongs/h/h2763.md) the [math](../../strongs/h/h4962.md), ['ishshah](../../strongs/h/h802.md), and [ṭap̄](../../strongs/h/h2945.md), of [ʿîr](../../strongs/h/h5892.md).

<a name="deuteronomy_3_7"></a>Deuteronomy 3:7

But all the [bĕhemah](../../strongs/h/h929.md), and the [šālāl](../../strongs/h/h7998.md) of the [ʿîr](../../strongs/h/h5892.md), we [bāzaz](../../strongs/h/h962.md) to ourselves.

<a name="deuteronomy_3_8"></a>Deuteronomy 3:8

And we [laqach](../../strongs/h/h3947.md) at that [ʿēṯ](../../strongs/h/h6256.md) [laqach](../../strongs/h/h3947.md) of the [yad](../../strongs/h/h3027.md) of the two [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md) the ['erets](../../strongs/h/h776.md) that was on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), from the [nachal](../../strongs/h/h5158.md) of ['Arnôn](../../strongs/h/h769.md) unto [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md);

<a name="deuteronomy_3_9"></a>Deuteronomy 3:9

(Which [Ḥermôn](../../strongs/h/h2768.md) the [Ṣîḏōnî](../../strongs/h/h6722.md) [qara'](../../strongs/h/h7121.md) [širyôn](../../strongs/h/h8303.md); and the ['Ĕmōrî](../../strongs/h/h567.md) [qara'](../../strongs/h/h7121.md) it [Śᵊnîr](../../strongs/h/h8149.md);)

<a name="deuteronomy_3_10"></a>Deuteronomy 3:10

All the [ʿîr](../../strongs/h/h5892.md) of the [mîšôr](../../strongs/h/h4334.md), and all [Gilʿāḏ](../../strongs/h/h1568.md), and all [Bāšān](../../strongs/h/h1316.md), unto [Salḵâ](../../strongs/h/h5548.md) and ['Eḏrᵊʿî](../../strongs/h/h154.md), [ʿîr](../../strongs/h/h5892.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of [ʿÔḡ](../../strongs/h/h5747.md) in [Bāšān](../../strongs/h/h1316.md).

<a name="deuteronomy_3_11"></a>Deuteronomy 3:11

For only [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md) [šā'ar](../../strongs/h/h7604.md) of the [yeṯer](../../strongs/h/h3499.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md); behold his ['eres](../../strongs/h/h6210.md) was an ['eres](../../strongs/h/h6210.md) of [barzel](../../strongs/h/h1270.md); is it not in [Rabâ](../../strongs/h/h7237.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md)? nine ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md) thereof, and four ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) of it, after the ['ammâ](../../strongs/h/h520.md) of an ['iysh](../../strongs/h/h376.md).

<a name="deuteronomy_3_12"></a>Deuteronomy 3:12

And this ['erets](../../strongs/h/h776.md), which we [yarash](../../strongs/h/h3423.md) at that [ʿēṯ](../../strongs/h/h6256.md), from [ʿĂrôʿēr](../../strongs/h/h6177.md), which is by the [nachal](../../strongs/h/h5158.md) ['Arnôn](../../strongs/h/h769.md), and [ḥēṣî](../../strongs/h/h2677.md) [har](../../strongs/h/h2022.md) [Gilʿāḏ](../../strongs/h/h1568.md), and the [ʿîr](../../strongs/h/h5892.md) thereof, [nathan](../../strongs/h/h5414.md) I unto the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md) and to the [Gāḏî](../../strongs/h/h1425.md).

<a name="deuteronomy_3_13"></a>Deuteronomy 3:13

And the [yeṯer](../../strongs/h/h3499.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and all [Bāšān](../../strongs/h/h1316.md), being the [mamlāḵâ](../../strongs/h/h4467.md) of [ʿÔḡ](../../strongs/h/h5747.md), [nathan](../../strongs/h/h5414.md) I unto the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md); all the [chebel](../../strongs/h/h2256.md) of ['Argōḇ](../../strongs/h/h709.md), with all [Bāšān](../../strongs/h/h1316.md), which was [qara'](../../strongs/h/h7121.md) the ['erets](../../strongs/h/h776.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="deuteronomy_3_14"></a>Deuteronomy 3:14

[Yā'Îr](../../strongs/h/h2971.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [laqach](../../strongs/h/h3947.md) all the [chebel](../../strongs/h/h2256.md) of ['Argōḇ](../../strongs/h/h709.md) unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Gᵊšûrî](../../strongs/h/h1651.md) and [Maʿăḵāṯî](../../strongs/h/h4602.md); and [qara'](../../strongs/h/h7121.md) them after his own [shem](../../strongs/h/h8034.md), [Bāšān](../../strongs/h/h1316.md) [Ḥaûôṯ YāʿÎr](../../strongs/h/h2334.md), unto this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_3_15"></a>Deuteronomy 3:15

And I [nathan](../../strongs/h/h5414.md) [Gilʿāḏ](../../strongs/h/h1568.md) unto [Māḵîr](../../strongs/h/h4353.md).

<a name="deuteronomy_3_16"></a>Deuteronomy 3:16

And unto the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md) and unto the [Gāḏî](../../strongs/h/h1425.md) I [nathan](../../strongs/h/h5414.md) from [Gilʿāḏ](../../strongs/h/h1568.md) even unto the [nachal](../../strongs/h/h5158.md) ['Arnôn](../../strongs/h/h769.md) [tavek](../../strongs/h/h8432.md) the [nachal](../../strongs/h/h5158.md), and the [gᵊḇûl](../../strongs/h/h1366.md) even unto the [nachal](../../strongs/h/h5158.md) [Yabōq](../../strongs/h/h2999.md), which is the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md);

<a name="deuteronomy_3_17"></a>Deuteronomy 3:17

The ['arabah](../../strongs/h/h6160.md) also, and [Yardēn](../../strongs/h/h3383.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof, from [Kinnᵊrôṯ](../../strongs/h/h3672.md) even unto the [yam](../../strongs/h/h3220.md) of the ['arabah](../../strongs/h/h6160.md), even the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md), under ['שḎvṯ Hp̄Sḡh](../../strongs/h/h798.md) ['ăšēḏâ](../../strongs/h/h794.md) [mizrach](../../strongs/h/h4217.md).

<a name="deuteronomy_3_18"></a>Deuteronomy 3:18

And I [tsavah](../../strongs/h/h6680.md) you at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) you this ['erets](../../strongs/h/h776.md) to [yarash](../../strongs/h/h3423.md) it: ye shall ['abar](../../strongs/h/h5674.md) [chalats](../../strongs/h/h2502.md) [paniym](../../strongs/h/h6440.md) your ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), all that are [ben](../../strongs/h/h1121.md) for the [ḥayil](../../strongs/h/h2428.md).

<a name="deuteronomy_3_19"></a>Deuteronomy 3:19

But your ['ishshah](../../strongs/h/h802.md), and your [ṭap̄](../../strongs/h/h2945.md), and your [miqnê](../../strongs/h/h4735.md), (for I [yada'](../../strongs/h/h3045.md) that ye have [rab](../../strongs/h/h7227.md) [miqnê](../../strongs/h/h4735.md),) shall [yashab](../../strongs/h/h3427.md) in your [ʿîr](../../strongs/h/h5892.md) which I have [nathan](../../strongs/h/h5414.md) you;

<a name="deuteronomy_3_20"></a>Deuteronomy 3:20

Until [Yĕhovah](../../strongs/h/h3068.md) have [nuwach](../../strongs/h/h5117.md) unto your ['ach](../../strongs/h/h251.md), as well as unto you, and until they also [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) them [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md): and then shall ye [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) unto his [yᵊruššâ](../../strongs/h/h3425.md), which I have [nathan](../../strongs/h/h5414.md) you.

<a name="deuteronomy_3_21"></a>Deuteronomy 3:21

And I [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md), Thine ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) all that [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath ['asah](../../strongs/h/h6213.md) unto these two [melek](../../strongs/h/h4428.md): so shall [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) unto all the [mamlāḵâ](../../strongs/h/h4467.md) whither thou ['abar](../../strongs/h/h5674.md).

<a name="deuteronomy_3_22"></a>Deuteronomy 3:22

Ye shall not [yare'](../../strongs/h/h3372.md) them: for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) he shall [lāḥam](../../strongs/h/h3898.md) for you.

<a name="deuteronomy_3_23"></a>Deuteronomy 3:23

And I [chanan](../../strongs/h/h2603.md) [Yĕhovah](../../strongs/h/h3068.md) at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_3_24"></a>Deuteronomy 3:24

['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), thou hast [ḥālal](../../strongs/h/h2490.md) to [ra'ah](../../strongs/h/h7200.md) thy ['ebed](../../strongs/h/h5650.md) thy [gōḏel](../../strongs/h/h1433.md), and thy [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md): for what ['el](../../strongs/h/h410.md) is there in [shamayim](../../strongs/h/h8064.md) or in ['erets](../../strongs/h/h776.md), that can ['asah](../../strongs/h/h6213.md) according to thy [ma'aseh](../../strongs/h/h4639.md), and according to thy [gᵊḇûrâ](../../strongs/h/h1369.md)?

<a name="deuteronomy_3_25"></a>Deuteronomy 3:25

I pray thee, let me ['abar](../../strongs/h/h5674.md), and [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) that is [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), that [towb](../../strongs/h/h2896.md) [har](../../strongs/h/h2022.md), and [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="deuteronomy_3_26"></a>Deuteronomy 3:26

But [Yĕhovah](../../strongs/h/h3068.md) ['abar](../../strongs/h/h5674.md) with me for your sakes, and would not [shama'](../../strongs/h/h8085.md) me: and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, Let it [rab](../../strongs/h/h7227.md) thee; [dabar](../../strongs/h/h1696.md) no [yāsap̄](../../strongs/h/h3254.md) unto me of this [dabar](../../strongs/h/h1697.md).

<a name="deuteronomy_3_27"></a>Deuteronomy 3:27

[ʿālâ](../../strongs/h/h5927.md) thee into the [ro'sh](../../strongs/h/h7218.md) of [Pisgâ](../../strongs/h/h6449.md), and [nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) [yam](../../strongs/h/h3220.md), and [ṣāp̄ôn](../../strongs/h/h6828.md), and [têmān](../../strongs/h/h8486.md), and [mizrach](../../strongs/h/h4217.md), and [ra'ah](../../strongs/h/h7200.md) it with thine ['ayin](../../strongs/h/h5869.md): for thou shalt not ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md).

<a name="deuteronomy_3_28"></a>Deuteronomy 3:28

But [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and [ḥāzaq](../../strongs/h/h2388.md) him, and ['amats](../../strongs/h/h553.md) him: for he shall ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) this ['am](../../strongs/h/h5971.md), and he shall cause them to [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md) which thou shalt [ra'ah](../../strongs/h/h7200.md).

<a name="deuteronomy_3_29"></a>Deuteronomy 3:29

So we [yashab](../../strongs/h/h3427.md) in the [gay'](../../strongs/h/h1516.md) over [môl](../../strongs/h/h4136.md) [Bêṯ PᵊʿÔr](../../strongs/h/h1047.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 2](deuteronomy_2.md) - [Deuteronomy 4](deuteronomy_4.md)