# [Deuteronomy 28](https://www.blueletterbible.org/kjv/deu/28)

<a name="deuteronomy_28_1"></a>Deuteronomy 28:1

And it shall come to pass, if thou shalt [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) and to ['asah](../../strongs/h/h6213.md) all his [mitsvah](../../strongs/h/h4687.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [nathan](../../strongs/h/h5414.md) thee on ['elyown](../../strongs/h/h5945.md) above all [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md):

<a name="deuteronomy_28_2"></a>Deuteronomy 28:2

And all these [bĕrakah](../../strongs/h/h1293.md) shall [bow'](../../strongs/h/h935.md) on thee, and [nāśaḡ](../../strongs/h/h5381.md) thee, if thou shalt [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_28_3"></a>Deuteronomy 28:3

[barak](../../strongs/h/h1288.md) shalt thou be in the [ʿîr](../../strongs/h/h5892.md), and [barak](../../strongs/h/h1288.md) shalt thou be in the [sadeh](../../strongs/h/h7704.md).

<a name="deuteronomy_28_4"></a>Deuteronomy 28:4

[barak](../../strongs/h/h1288.md) shall be the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md), and the [pĕriy](../../strongs/h/h6529.md) of thy ['adamah](../../strongs/h/h127.md), and the [pĕriy](../../strongs/h/h6529.md) of thy [bĕhemah](../../strongs/h/h929.md), the [šeḡer](../../strongs/h/h7698.md) of thy ['eleph](../../strongs/h/h504.md), and the [ʿaštārôṯ](../../strongs/h/h6251.md) of thy [tso'n](../../strongs/h/h6629.md).

<a name="deuteronomy_28_5"></a>Deuteronomy 28:5

[barak](../../strongs/h/h1288.md) shall be thy [ṭene'](../../strongs/h/h2935.md) and thy [miš'ereṯ](../../strongs/h/h4863.md).

<a name="deuteronomy_28_6"></a>Deuteronomy 28:6

[barak](../../strongs/h/h1288.md) shalt thou be when thou [bow'](../../strongs/h/h935.md), and [barak](../../strongs/h/h1288.md) shalt thou be when thou [yāṣā'](../../strongs/h/h3318.md).

<a name="deuteronomy_28_7"></a>Deuteronomy 28:7

[Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) thine ['oyeb](../../strongs/h/h341.md) that [quwm](../../strongs/h/h6965.md) against thee to be [nāḡap̄](../../strongs/h/h5062.md) before thy [paniym](../../strongs/h/h6440.md): they shall [yāṣā'](../../strongs/h/h3318.md) against thee one [derek](../../strongs/h/h1870.md), and [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) thee seven [derek](../../strongs/h/h1870.md).

<a name="deuteronomy_28_8"></a>Deuteronomy 28:8

[Yĕhovah](../../strongs/h/h3068.md) shall [tsavah](../../strongs/h/h6680.md) the [bĕrakah](../../strongs/h/h1293.md) upon thee in thy ['āsām](../../strongs/h/h618.md), and in all that thou [mišlôaḥ](../../strongs/h/h4916.md) thine [yad](../../strongs/h/h3027.md) unto; and he shall [barak](../../strongs/h/h1288.md) thee in the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_28_9"></a>Deuteronomy 28:9

[Yĕhovah](../../strongs/h/h3068.md) shall [quwm](../../strongs/h/h6965.md) thee a [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md) unto himself, as he hath [shaba'](../../strongs/h/h7650.md) unto thee, if thou shalt [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [halak](../../strongs/h/h1980.md) in his [derek](../../strongs/h/h1870.md).

<a name="deuteronomy_28_10"></a>Deuteronomy 28:10

And all ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall [ra'ah](../../strongs/h/h7200.md) that thou art [qara'](../../strongs/h/h7121.md) by the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md); and they shall be [yare'](../../strongs/h/h3372.md) of thee.

<a name="deuteronomy_28_11"></a>Deuteronomy 28:11

And [Yĕhovah](../../strongs/h/h3068.md) shall [yāṯar](../../strongs/h/h3498.md) thee in [towb](../../strongs/h/h2896.md), in the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md), and in the [pĕriy](../../strongs/h/h6529.md) of thy [bĕhemah](../../strongs/h/h929.md), and in the [pĕriy](../../strongs/h/h6529.md) of thy ['adamah](../../strongs/h/h127.md), in the ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_28_12"></a>Deuteronomy 28:12

[Yĕhovah](../../strongs/h/h3068.md) shall [pāṯaḥ](../../strongs/h/h6605.md) unto thee his [towb](../../strongs/h/h2896.md) ['ôṣār](../../strongs/h/h214.md), the [shamayim](../../strongs/h/h8064.md) to [nathan](../../strongs/h/h5414.md) the [māṭār](../../strongs/h/h4306.md) unto thy ['erets](../../strongs/h/h776.md) in his [ʿēṯ](../../strongs/h/h6256.md), and to [barak](../../strongs/h/h1288.md) all the [ma'aseh](../../strongs/h/h4639.md) of thine [yad](../../strongs/h/h3027.md): and thou shalt [lāvâ](../../strongs/h/h3867.md) unto [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), and thou shalt not [lāvâ](../../strongs/h/h3867.md).

<a name="deuteronomy_28_13"></a>Deuteronomy 28:13

And [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) thee the [ro'sh](../../strongs/h/h7218.md), and not the [zānāḇ](../../strongs/h/h2180.md); and thou shalt be [maʿal](../../strongs/h/h4605.md), and thou shalt not be [maṭṭâ](../../strongs/h/h4295.md); if that thou [shama'](../../strongs/h/h8085.md) unto the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), to [shamar](../../strongs/h/h8104.md) and to ['asah](../../strongs/h/h6213.md) them:

<a name="deuteronomy_28_14"></a>Deuteronomy 28:14

And thou shalt not [cuwr](../../strongs/h/h5493.md) from any of the [dabar](../../strongs/h/h1697.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), to the [yamiyn](../../strongs/h/h3225.md), or to the [śᵊmō'l](../../strongs/h/h8040.md), to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to ['abad](../../strongs/h/h5647.md) them.

<a name="deuteronomy_28_15"></a>Deuteronomy 28:15

But it shall come to pass, if thou wilt not [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all his [mitsvah](../../strongs/h/h4687.md) and his [chuqqah](../../strongs/h/h2708.md) which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md); that all these [qᵊlālâ](../../strongs/h/h7045.md) shall [bow'](../../strongs/h/h935.md) upon thee, and [nāśaḡ](../../strongs/h/h5381.md) thee:

<a name="deuteronomy_28_16"></a>Deuteronomy 28:16

['arar](../../strongs/h/h779.md) shalt thou be in the [ʿîr](../../strongs/h/h5892.md), and ['arar](../../strongs/h/h779.md) shalt thou be in the [sadeh](../../strongs/h/h7704.md).

<a name="deuteronomy_28_17"></a>Deuteronomy 28:17

['arar](../../strongs/h/h779.md) shall be thy [ṭene'](../../strongs/h/h2935.md) and thy [miš'ereṯ](../../strongs/h/h4863.md).

<a name="deuteronomy_28_18"></a>Deuteronomy 28:18

['arar](../../strongs/h/h779.md) shall be the [pĕriy](../../strongs/h/h6529.md) of thy [beten](../../strongs/h/h990.md), and the [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md), the [šeḡer](../../strongs/h/h7698.md) of thy ['eleph](../../strongs/h/h504.md), and the [ʿaštārôṯ](../../strongs/h/h6251.md) of thy [tso'n](../../strongs/h/h6629.md).

<a name="deuteronomy_28_19"></a>Deuteronomy 28:19

['arar](../../strongs/h/h779.md) shalt thou be when thou [bow'](../../strongs/h/h935.md), and ['arar](../../strongs/h/h779.md) shalt thou be when thou [yāṣā'](../../strongs/h/h3318.md).

<a name="deuteronomy_28_20"></a>Deuteronomy 28:20

[Yĕhovah](../../strongs/h/h3068.md) shall [shalach](../../strongs/h/h7971.md) upon thee [mᵊ'ērâ](../../strongs/h/h3994.md), [mᵊhûmâ](../../strongs/h/h4103.md), and [miḡʿereṯ](../../strongs/h/h4045.md), in all that thou [mišlôaḥ](../../strongs/h/h4916.md) thine [yad](../../strongs/h/h3027.md) unto for to ['asah](../../strongs/h/h6213.md), until thou be [šāmaḏ](../../strongs/h/h8045.md), and until thou ['abad](../../strongs/h/h6.md) [mahēr](../../strongs/h/h4118.md); [paniym](../../strongs/h/h6440.md) of the [rōaʿ](../../strongs/h/h7455.md) of thy [maʿălāl](../../strongs/h/h4611.md), whereby thou hast ['azab](../../strongs/h/h5800.md) me.

<a name="deuteronomy_28_21"></a>Deuteronomy 28:21

[Yĕhovah](../../strongs/h/h3068.md) shall make the [deḇer](../../strongs/h/h1698.md) [dāḇaq](../../strongs/h/h1692.md) unto thee, until he have [kalah](../../strongs/h/h3615.md) thee from off the ['ăḏāmâ](../../strongs/h/h127.md), whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_28_22"></a>Deuteronomy 28:22

[Yĕhovah](../../strongs/h/h3068.md) shall [nakah](../../strongs/h/h5221.md) thee with a [šaḥep̄eṯ](../../strongs/h/h7829.md), and with a [qadaḥaṯ](../../strongs/h/h6920.md), and with a [dalleqeṯ](../../strongs/h/h1816.md), and with a [ḥarḥur](../../strongs/h/h2746.md), and with the [chereb](../../strongs/h/h2719.md), and with [šᵊḏēp̄â](../../strongs/h/h7711.md), and with [yērāqôn](../../strongs/h/h3420.md); and they shall [radaph](../../strongs/h/h7291.md) thee until thou ['abad](../../strongs/h/h6.md).

<a name="deuteronomy_28_23"></a>Deuteronomy 28:23

And thy [shamayim](../../strongs/h/h8064.md) that is over thy [ro'sh](../../strongs/h/h7218.md) shall be [nᵊḥšeṯ](../../strongs/h/h5178.md), and the ['erets](../../strongs/h/h776.md) that is under thee shall be [barzel](../../strongs/h/h1270.md).

<a name="deuteronomy_28_24"></a>Deuteronomy 28:24

[Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) the [māṭār](../../strongs/h/h4306.md) of thy ['erets](../../strongs/h/h776.md) ['āḇāq](../../strongs/h/h80.md) and ['aphar](../../strongs/h/h6083.md): from [shamayim](../../strongs/h/h8064.md) shall it [yarad](../../strongs/h/h3381.md) upon thee, until thou be [šāmaḏ](../../strongs/h/h8045.md).

<a name="deuteronomy_28_25"></a>Deuteronomy 28:25

[Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) thee to be [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) thine ['oyeb](../../strongs/h/h341.md): thou shalt [yāṣā'](../../strongs/h/h3318.md) one [derek](../../strongs/h/h1870.md) against them, and [nûs](../../strongs/h/h5127.md) seven [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) them: and shalt be [zaʿăvâ](../../strongs/h/h2189.md) into all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_28_26"></a>Deuteronomy 28:26

And thy [nᵊḇēlâ](../../strongs/h/h5038.md) shall be [ma'akal](../../strongs/h/h3978.md) unto all [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and unto the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md), and no man shall [ḥārēḏ](../../strongs/h/h2729.md).

<a name="deuteronomy_28_27"></a>Deuteronomy 28:27

[Yĕhovah](../../strongs/h/h3068.md) will [nakah](../../strongs/h/h5221.md) thee with the [šiḥîn](../../strongs/h/h7822.md) of [Mitsrayim](../../strongs/h/h4714.md), and with the [ṭᵊḥôrîm](../../strongs/h/h2914.md) [ʿōp̄el](../../strongs/h/h6076.md), and with the [gārāḇ](../../strongs/h/h1618.md), and with the [ḥeres](../../strongs/h/h2775.md), whereof thou [yakol](../../strongs/h/h3201.md) not be [rapha'](../../strongs/h/h7495.md).

<a name="deuteronomy_28_28"></a>Deuteronomy 28:28

[Yĕhovah](../../strongs/h/h3068.md) shall [nakah](../../strongs/h/h5221.md) thee with [šigāʿôn](../../strongs/h/h7697.md), and [ʿiûārôn](../../strongs/h/h5788.md), and [timâôn](../../strongs/h/h8541.md) of [lebab](../../strongs/h/h3824.md):

<a name="deuteronomy_28_29"></a>Deuteronomy 28:29

And thou shalt [māšaš](../../strongs/h/h4959.md) at [ṣōhar](../../strongs/h/h6672.md), as the [ʿiûēr](../../strongs/h/h5787.md) [māšaš](../../strongs/h/h4959.md) in ['ăp̄ēlâ](../../strongs/h/h653.md), and thou shalt not [tsalach](../../strongs/h/h6743.md) in thy [derek](../../strongs/h/h1870.md): and thou shalt be only [ʿāšaq](../../strongs/h/h6231.md) and [gāzal](../../strongs/h/h1497.md) [yowm](../../strongs/h/h3117.md), and no man shall [yasha'](../../strongs/h/h3467.md) thee.

<a name="deuteronomy_28_30"></a>Deuteronomy 28:30

Thou shalt ['āraś](../../strongs/h/h781.md) an ['ishshah](../../strongs/h/h802.md), and ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md) shall [shakab](../../strongs/h/h7901.md) [šāḡal](../../strongs/h/h7693.md) with her: thou shalt [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md), and thou shalt not [yashab](../../strongs/h/h3427.md) therein: thou shalt [nāṭaʿ](../../strongs/h/h5193.md) a [kerem](../../strongs/h/h3754.md), and shalt not [ḥālal](../../strongs/h/h2490.md) thereof.

<a name="deuteronomy_28_31"></a>Deuteronomy 28:31

Thine [showr](../../strongs/h/h7794.md) shall be [ṭāḇaḥ](../../strongs/h/h2873.md) before thine ['ayin](../../strongs/h/h5869.md), and thou shalt not ['akal](../../strongs/h/h398.md) thereof: thine [chamowr](../../strongs/h/h2543.md) shall be [gāzal](../../strongs/h/h1497.md) from before thy [paniym](../../strongs/h/h6440.md), and shall not be [shuwb](../../strongs/h/h7725.md) to thee: thy [tso'n](../../strongs/h/h6629.md) shall be [nathan](../../strongs/h/h5414.md) unto thine ['oyeb](../../strongs/h/h341.md), and thou shalt have none to [yasha'](../../strongs/h/h3467.md) them.

<a name="deuteronomy_28_32"></a>Deuteronomy 28:32

Thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md) shall be [nathan](../../strongs/h/h5414.md) unto ['aḥēr](../../strongs/h/h312.md) ['am](../../strongs/h/h5971.md), and thine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md), and [kālê](../../strongs/h/h3616.md) for them all the [yowm](../../strongs/h/h3117.md) long; and there shall be no ['el](../../strongs/h/h410.md) in thine [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_28_33"></a>Deuteronomy 28:33

The [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md), and all thy [yᵊḡîaʿ](../../strongs/h/h3018.md), shall an ['am](../../strongs/h/h5971.md) which thou [yada'](../../strongs/h/h3045.md) not ['akal](../../strongs/h/h398.md); and thou shalt be only [ʿāšaq](../../strongs/h/h6231.md) and [rāṣaṣ](../../strongs/h/h7533.md) [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_28_34"></a>Deuteronomy 28:34

So that thou shalt [šāḡaʿ](../../strongs/h/h7696.md) for the [mar'ê](../../strongs/h/h4758.md) of thine ['ayin](../../strongs/h/h5869.md) which thou shalt [ra'ah](../../strongs/h/h7200.md).

<a name="deuteronomy_28_35"></a>Deuteronomy 28:35

[Yĕhovah](../../strongs/h/h3068.md) shall [nakah](../../strongs/h/h5221.md) thee in the [bereḵ](../../strongs/h/h1290.md), and in the [šôq](../../strongs/h/h7785.md), with a [ra'](../../strongs/h/h7451.md) [šiḥîn](../../strongs/h/h7822.md) that [yakol](../../strongs/h/h3201.md) be [rapha'](../../strongs/h/h7495.md), from the [kaph](../../strongs/h/h3709.md) of thy [regel](../../strongs/h/h7272.md) unto the [qodqod](../../strongs/h/h6936.md).

<a name="deuteronomy_28_36"></a>Deuteronomy 28:36

[Yĕhovah](../../strongs/h/h3068.md) shall [yālaḵ](../../strongs/h/h3212.md) thee, and thy [melek](../../strongs/h/h4428.md) which thou shalt [quwm](../../strongs/h/h6965.md) over thee, unto a [gowy](../../strongs/h/h1471.md) which neither thou nor thy ['ab](../../strongs/h/h1.md) have [yada'](../../strongs/h/h3045.md); and there shalt thou ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md).

<a name="deuteronomy_28_37"></a>Deuteronomy 28:37

And thou shalt become a [šammâ](../../strongs/h/h8047.md), a [māšāl](../../strongs/h/h4912.md), and a [šᵊnînâ](../../strongs/h/h8148.md), among all ['am](../../strongs/h/h5971.md) whither [Yĕhovah](../../strongs/h/h3068.md) shall [nāhaḡ](../../strongs/h/h5090.md) thee.

<a name="deuteronomy_28_38"></a>Deuteronomy 28:38

Thou shalt [yāṣā'](../../strongs/h/h3318.md) [rab](../../strongs/h/h7227.md) [zera'](../../strongs/h/h2233.md) into the [sadeh](../../strongs/h/h7704.md), and shalt ['āsap̄](../../strongs/h/h622.md) but [mᵊʿaṭ](../../strongs/h/h4592.md) in; for the ['arbê](../../strongs/h/h697.md) shall [ḥāsal](../../strongs/h/h2628.md) it.

<a name="deuteronomy_28_39"></a>Deuteronomy 28:39

Thou shalt [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), and ['abad](../../strongs/h/h5647.md) them, but shalt neither [šāṯâ](../../strongs/h/h8354.md) of the [yayin](../../strongs/h/h3196.md), nor ['āḡar](../../strongs/h/h103.md); for the [tôlāʿ](../../strongs/h/h8438.md) shall ['akal](../../strongs/h/h398.md) them.

<a name="deuteronomy_28_40"></a>Deuteronomy 28:40

Thou shalt have [zayiṯ](../../strongs/h/h2132.md) throughout all thy [gᵊḇûl](../../strongs/h/h1366.md), but thou shalt not [sûḵ](../../strongs/h/h5480.md) with the [šemen](../../strongs/h/h8081.md); for thine [zayiṯ](../../strongs/h/h2132.md) shall [nāšal](../../strongs/h/h5394.md).

<a name="deuteronomy_28_41"></a>Deuteronomy 28:41

Thou shalt [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md), but thou shalt not enjoy them; for they shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="deuteronomy_28_42"></a>Deuteronomy 28:42

All thy ['ets](../../strongs/h/h6086.md) and [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md) shall the [ṣᵊlāṣal](../../strongs/h/h6767.md) [yarash](../../strongs/h/h3423.md).

<a name="deuteronomy_28_43"></a>Deuteronomy 28:43

The [ger](../../strongs/h/h1616.md) that is [qereḇ](../../strongs/h/h7130.md) thee shall [ʿālâ](../../strongs/h/h5927.md) above thee [maʿal](../../strongs/h/h4605.md) [maʿal](../../strongs/h/h4605.md); and thou shalt [yarad](../../strongs/h/h3381.md) [maṭṭâ](../../strongs/h/h4295.md) [maṭṭâ](../../strongs/h/h4295.md).

<a name="deuteronomy_28_44"></a>Deuteronomy 28:44

He shall [lāvâ](../../strongs/h/h3867.md) to thee, and thou shalt not [lāvâ](../../strongs/h/h3867.md) to him: he shall be the [ro'sh](../../strongs/h/h7218.md), and thou shalt be the [zānāḇ](../../strongs/h/h2180.md).

<a name="deuteronomy_28_45"></a>Deuteronomy 28:45

Moreover all these [qᵊlālâ](../../strongs/h/h7045.md) shall [bow'](../../strongs/h/h935.md) upon thee, and shall [radaph](../../strongs/h/h7291.md) thee, and [nāśaḡ](../../strongs/h/h5381.md) thee, till thou be [šāmaḏ](../../strongs/h/h8045.md); because thou [shama'](../../strongs/h/h8085.md) not unto the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md) and his [chuqqah](../../strongs/h/h2708.md) which he [tsavah](../../strongs/h/h6680.md) thee:

<a name="deuteronomy_28_46"></a>Deuteronomy 28:46

And they shall be upon thee for an ['ôṯ](../../strongs/h/h226.md) and for a [môp̄ēṯ](../../strongs/h/h4159.md), and upon thy [zera'](../../strongs/h/h2233.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="deuteronomy_28_47"></a>Deuteronomy 28:47

Because thou ['abad](../../strongs/h/h5647.md) not [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) with [simchah](../../strongs/h/h8057.md), and with [ṭûḇ](../../strongs/h/h2898.md) of [lebab](../../strongs/h/h3824.md), for the [rōḇ](../../strongs/h/h7230.md) of all things;

<a name="deuteronomy_28_48"></a>Deuteronomy 28:48

Therefore shalt thou ['abad](../../strongs/h/h5647.md) thine ['oyeb](../../strongs/h/h341.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [shalach](../../strongs/h/h7971.md) against thee, in [rāʿāḇ](../../strongs/h/h7458.md), and in [ṣāmā'](../../strongs/h/h6772.md), and in ['eyrom](../../strongs/h/h5903.md), and in [ḥōser](../../strongs/h/h2640.md) of all things: and he shall [nathan](../../strongs/h/h5414.md) an [ʿōl](../../strongs/h/h5923.md) of [barzel](../../strongs/h/h1270.md) upon thy [ṣaûā'r](../../strongs/h/h6677.md), until he have [šāmaḏ](../../strongs/h/h8045.md) thee.

<a name="deuteronomy_28_49"></a>Deuteronomy 28:49

[Yĕhovah](../../strongs/h/h3068.md) shall [nasa'](../../strongs/h/h5375.md) a [gowy](../../strongs/h/h1471.md) against thee from [rachowq](../../strongs/h/h7350.md), from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md), as swift as the [nesheׁr](../../strongs/h/h5404.md) [da'ah](../../strongs/h/h1675.md); a [gowy](../../strongs/h/h1471.md) whose [lashown](../../strongs/h/h3956.md) thou shalt not [shama'](../../strongs/h/h8085.md);

<a name="deuteronomy_28_50"></a>Deuteronomy 28:50

A [gowy](../../strongs/h/h1471.md) of ['az](../../strongs/h/h5794.md) [paniym](../../strongs/h/h6440.md), which shall not [nasa'](../../strongs/h/h5375.md) the [paniym](../../strongs/h/h6440.md) of the [zāqēn](../../strongs/h/h2205.md), nor [chanan](../../strongs/h/h2603.md) to the [naʿar](../../strongs/h/h5288.md):

<a name="deuteronomy_28_51"></a>Deuteronomy 28:51

And he shall ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of thy [bĕhemah](../../strongs/h/h929.md), and the [pĕriy](../../strongs/h/h6529.md) of thy ['ăḏāmâ](../../strongs/h/h127.md), until thou be [šāmaḏ](../../strongs/h/h8045.md): which also shall not [šā'ar](../../strongs/h/h7604.md) thee [dagan](../../strongs/h/h1715.md), [tiyrowsh](../../strongs/h/h8492.md), or [yiṣhār](../../strongs/h/h3323.md), or the [šeḡer](../../strongs/h/h7698.md) of thy ['eleph](../../strongs/h/h504.md), or [ʿaštārôṯ](../../strongs/h/h6251.md) of thy [tso'n](../../strongs/h/h6629.md), until he have ['abad](../../strongs/h/h6.md) thee.

<a name="deuteronomy_28_52"></a>Deuteronomy 28:52

And he shall [tsarar](../../strongs/h/h6887.md) thee in all thy [sha'ar](../../strongs/h/h8179.md), until thy [gāḇōha](../../strongs/h/h1364.md) and [bāṣar](../../strongs/h/h1219.md) [ḥômâ](../../strongs/h/h2346.md) [yarad](../../strongs/h/h3381.md), wherein thou [batach](../../strongs/h/h982.md), throughout all thy ['erets](../../strongs/h/h776.md): and he shall [tsarar](../../strongs/h/h6887.md) thee in all thy [sha'ar](../../strongs/h/h8179.md) throughout all thy ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) thee.

<a name="deuteronomy_28_53"></a>Deuteronomy 28:53

And thou shalt ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of thine own [beten](../../strongs/h/h990.md), the [basar](../../strongs/h/h1320.md) of thy [ben](../../strongs/h/h1121.md) and of thy [bath](../../strongs/h/h1323.md), which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) thee, in the [māṣôr](../../strongs/h/h4692.md), and in the [māṣôq](../../strongs/h/h4689.md), wherewith thine ['oyeb](../../strongs/h/h341.md) shall [ṣûq](../../strongs/h/h6693.md) thee:

<a name="deuteronomy_28_54"></a>Deuteronomy 28:54

So that the ['iysh](../../strongs/h/h376.md) that is [raḵ](../../strongs/h/h7390.md) among you, and [me'od](../../strongs/h/h3966.md) [ʿānōḡ](../../strongs/h/h6028.md), his ['ayin](../../strongs/h/h5869.md) shall be [yāraʿ](../../strongs/h/h3415.md) toward his ['ach](../../strongs/h/h251.md), and toward the ['ishshah](../../strongs/h/h802.md) of his [ḥêq](../../strongs/h/h2436.md), and toward the [yeṯer](../../strongs/h/h3499.md) of his [ben](../../strongs/h/h1121.md) which he shall [yāṯar](../../strongs/h/h3498.md):

<a name="deuteronomy_28_55"></a>Deuteronomy 28:55

So that he will not [nathan](../../strongs/h/h5414.md) to ['echad](../../strongs/h/h259.md) of them of the [basar](../../strongs/h/h1320.md) of his [ben](../../strongs/h/h1121.md) whom he shall ['akal](../../strongs/h/h398.md): because he hath nothing [šā'ar](../../strongs/h/h7604.md) him in the [māṣôr](../../strongs/h/h4692.md), and in the [māṣôq](../../strongs/h/h4689.md), wherewith thine ['oyeb](../../strongs/h/h341.md) shall [ṣûq](../../strongs/h/h6693.md) thee in all thy [sha'ar](../../strongs/h/h8179.md).

<a name="deuteronomy_28_56"></a>Deuteronomy 28:56

The [raḵ](../../strongs/h/h7390.md) and [ʿānōḡ](../../strongs/h/h6028.md) among you, which would not [nāsâ](../../strongs/h/h5254.md) to [yāṣaḡ](../../strongs/h/h3322.md) the [kaph](../../strongs/h/h3709.md) of her [regel](../../strongs/h/h7272.md) upon the ['erets](../../strongs/h/h776.md) for [ʿānaḡ](../../strongs/h/h6026.md) and [rōḵ](../../strongs/h/h7391.md), her ['ayin](../../strongs/h/h5869.md) shall be [yāraʿ](../../strongs/h/h3415.md) toward the ['iysh](../../strongs/h/h376.md) of her [ḥêq](../../strongs/h/h2436.md), and toward her [ben](../../strongs/h/h1121.md), and toward her [bath](../../strongs/h/h1323.md),

<a name="deuteronomy_28_57"></a>Deuteronomy 28:57

And toward her [šilyâ](../../strongs/h/h7988.md) that [yāṣā'](../../strongs/h/h3318.md) from between her [regel](../../strongs/h/h7272.md), and toward her [ben](../../strongs/h/h1121.md) which she shall [yalad](../../strongs/h/h3205.md): for she shall ['akal](../../strongs/h/h398.md) them for [ḥōser](../../strongs/h/h2640.md) of all things [cether](../../strongs/h/h5643.md) in the [māṣôr](../../strongs/h/h4692.md) and [māṣôq](../../strongs/h/h4689.md), wherewith thine ['oyeb](../../strongs/h/h341.md) shall [ṣûq](../../strongs/h/h6693.md) thee in thy [sha'ar](../../strongs/h/h8179.md).

<a name="deuteronomy_28_58"></a>Deuteronomy 28:58

If thou wilt not [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md) that are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md), that thou mayest [yare'](../../strongs/h/h3372.md) this [kabad](../../strongs/h/h3513.md) and [yare'](../../strongs/h/h3372.md) [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md);

<a name="deuteronomy_28_59"></a>Deuteronomy 28:59

Then [Yĕhovah](../../strongs/h/h3068.md) will [pala'](../../strongs/h/h6381.md) thy [makâ](../../strongs/h/h4347.md), and the [makâ](../../strongs/h/h4347.md) of thy [zera'](../../strongs/h/h2233.md), even [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md), and of ['aman](../../strongs/h/h539.md), and [ra'](../../strongs/h/h7451.md) [ḥŏlî](../../strongs/h/h2483.md), and of ['aman](../../strongs/h/h539.md).

<a name="deuteronomy_28_60"></a>Deuteronomy 28:60

Moreover he will [shuwb](../../strongs/h/h7725.md) upon thee all the [maḏvê](../../strongs/h/h4064.md) of [Mitsrayim](../../strongs/h/h4714.md), which thou wast [yāḡōr](../../strongs/h/h3025.md) [paniym](../../strongs/h/h6440.md); and they shall [dāḇaq](../../strongs/h/h1692.md) unto thee.

<a name="deuteronomy_28_61"></a>Deuteronomy 28:61

Also every [ḥŏlî](../../strongs/h/h2483.md), and every [makâ](../../strongs/h/h4347.md), which is not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of this [towrah](../../strongs/h/h8451.md), them will [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) upon thee, until thou be [šāmaḏ](../../strongs/h/h8045.md).

<a name="deuteronomy_28_62"></a>Deuteronomy 28:62

And ye shall be [šā'ar](../../strongs/h/h7604.md) [mᵊʿaṭ](../../strongs/h/h4592.md) in [math](../../strongs/h/h4962.md), whereas ye were as the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md) for [rōḇ](../../strongs/h/h7230.md); because thou wouldest not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_28_63"></a>Deuteronomy 28:63

And it shall come to pass, that as [Yĕhovah](../../strongs/h/h3068.md) [śûś](../../strongs/h/h7797.md) over you to do you [yatab](../../strongs/h/h3190.md), and to [rabah](../../strongs/h/h7235.md) you; so [Yĕhovah](../../strongs/h/h3068.md) will [śûś](../../strongs/h/h7797.md) over you to ['abad](../../strongs/h/h6.md) you, and to [šāmaḏ](../../strongs/h/h8045.md) you; and ye shall be [nāsaḥ](../../strongs/h/h5255.md) from off the ['ăḏāmâ](../../strongs/h/h127.md) whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_28_64"></a>Deuteronomy 28:64

And [Yĕhovah](../../strongs/h/h3068.md) shall [puwts](../../strongs/h/h6327.md) thee among all ['am](../../strongs/h/h5971.md), from the one [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) even unto the [qāṣê](../../strongs/h/h7097.md); and there thou shalt ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), which neither thou nor thy ['ab](../../strongs/h/h1.md) have [yada'](../../strongs/h/h3045.md), even ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md).

<a name="deuteronomy_28_65"></a>Deuteronomy 28:65

And among these [gowy](../../strongs/h/h1471.md) shalt thou find no [rāḡaʿ](../../strongs/h/h7280.md), neither shall the [kaph](../../strongs/h/h3709.md) of thy [regel](../../strongs/h/h7272.md) have [mānôaḥ](../../strongs/h/h4494.md): but [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) thee there a [ragāz](../../strongs/h/h7268.md) [leb](../../strongs/h/h3820.md), and [killāyôn](../../strongs/h/h3631.md) of ['ayin](../../strongs/h/h5869.md), and [dᵊ'āḇôn](../../strongs/h/h1671.md) of [nephesh](../../strongs/h/h5315.md):

<a name="deuteronomy_28_66"></a>Deuteronomy 28:66

And thy [chay](../../strongs/h/h2416.md) shall [tālā'](../../strongs/h/h8511.md) in doubt [neḡeḏ](../../strongs/h/h5048.md) thee; and thou shalt [pachad](../../strongs/h/h6342.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), and shalt have none ['aman](../../strongs/h/h539.md) of thy [chay](../../strongs/h/h2416.md):

<a name="deuteronomy_28_67"></a>Deuteronomy 28:67

In the [boqer](../../strongs/h/h1242.md) thou shalt ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) were ['ereb](../../strongs/h/h6153.md)! and at ['ereb](../../strongs/h/h6153.md) thou shalt ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) were [boqer](../../strongs/h/h1242.md)! for the [paḥaḏ](../../strongs/h/h6343.md) of thine [lebab](../../strongs/h/h3824.md) wherewith thou shalt [pachad](../../strongs/h/h6342.md), and for the [mar'ê](../../strongs/h/h4758.md) of thine ['ayin](../../strongs/h/h5869.md) which thou shalt [ra'ah](../../strongs/h/h7200.md).

<a name="deuteronomy_28_68"></a>Deuteronomy 28:68

And [Yĕhovah](../../strongs/h/h3068.md) shall [shuwb](../../strongs/h/h7725.md) thee into [Mitsrayim](../../strongs/h/h4714.md) with ['ŏnîyâ](../../strongs/h/h591.md), by the [derek](../../strongs/h/h1870.md) whereof I ['āmar](../../strongs/h/h559.md) unto thee, Thou shalt [ra'ah](../../strongs/h/h7200.md) it no more [yāsap̄](../../strongs/h/h3254.md): and there ye shall be [māḵar](../../strongs/h/h4376.md) unto your ['oyeb](../../strongs/h/h341.md) for ['ebed](../../strongs/h/h5650.md) and [šip̄ḥâ](../../strongs/h/h8198.md), and no man shall [qānâ](../../strongs/h/h7069.md) you.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 27](deuteronomy_27.md) - [Deuteronomy 29](deuteronomy_29.md)