# [Deuteronomy 31](https://www.blueletterbible.org/kjv/deu/31)

<a name="deuteronomy_31_1"></a>Deuteronomy 31:1

And [Mōshe](../../strongs/h/h4872.md) [yālaḵ](../../strongs/h/h3212.md) and [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) unto all [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_31_2"></a>Deuteronomy 31:2

And he ['āmar](../../strongs/h/h559.md) unto them, I am an hundred and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) this [yowm](../../strongs/h/h3117.md); I [yakol](../../strongs/h/h3201.md) no more [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md): also [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) unto me, Thou shalt not ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md).

<a name="deuteronomy_31_3"></a>Deuteronomy 31:3

[Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), he will ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) thee, and he will [šāmaḏ](../../strongs/h/h8045.md) these [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) thee, and thou shalt [yarash](../../strongs/h/h3423.md) them: and [Yᵊhôšûaʿ](../../strongs/h/h3091.md), he shall ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) thee, as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md).

<a name="deuteronomy_31_4"></a>Deuteronomy 31:4

And [Yĕhovah](../../strongs/h/h3068.md) shall ['asah](../../strongs/h/h6213.md) unto them as he ['asah](../../strongs/h/h6213.md) to [Sîḥôn](../../strongs/h/h5511.md) and to [ʿÔḡ](../../strongs/h/h5747.md), [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), and unto the ['erets](../../strongs/h/h776.md) of them, whom he [šāmaḏ](../../strongs/h/h8045.md).

<a name="deuteronomy_31_5"></a>Deuteronomy 31:5

And [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) them before your [paniym](../../strongs/h/h6440.md), that ye may ['asah](../../strongs/h/h6213.md) unto them according unto all the [mitsvah](../../strongs/h/h4687.md) which I have [tsavah](../../strongs/h/h6680.md) you.

<a name="deuteronomy_31_6"></a>Deuteronomy 31:6

Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md), [yare'](../../strongs/h/h3372.md) not, nor be [ʿāraṣ](../../strongs/h/h6206.md) of [paniym](../../strongs/h/h6440.md): for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), he it is that doth [halak](../../strongs/h/h1980.md) with thee; he will not [rāp̄â](../../strongs/h/h7503.md) thee, nor ['azab](../../strongs/h/h5800.md) thee.

<a name="deuteronomy_31_7"></a>Deuteronomy 31:7

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and ['āmar](../../strongs/h/h559.md) unto him in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md), Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md): for thou must [bow'](../../strongs/h/h935.md) with this ['am](../../strongs/h/h5971.md) unto the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) them; and thou shalt cause them to [nāḥal](../../strongs/h/h5157.md) it.

<a name="deuteronomy_31_8"></a>Deuteronomy 31:8

And [Yĕhovah](../../strongs/h/h3068.md), he it is that doth [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) thee; he will be with thee, he will not [rāp̄â](../../strongs/h/h7503.md) thee, neither ['azab](../../strongs/h/h5800.md) thee: [yare'](../../strongs/h/h3372.md) not, neither be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="deuteronomy_31_9"></a>Deuteronomy 31:9

And [Mōshe](../../strongs/h/h4872.md) [kāṯaḇ](../../strongs/h/h3789.md) this [towrah](../../strongs/h/h8451.md), and [nathan](../../strongs/h/h5414.md) it unto the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), which [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and unto all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_31_10"></a>Deuteronomy 31:10

And [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), At the [qēṣ](../../strongs/h/h7093.md) of every seven [šānâ](../../strongs/h/h8141.md), in the [môʿēḏ](../../strongs/h/h4150.md) of the [šānâ](../../strongs/h/h8141.md) of [šᵊmiṭṭâ](../../strongs/h/h8059.md), in the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md),

<a name="deuteronomy_31_11"></a>Deuteronomy 31:11

When all [Yisra'el](../../strongs/h/h3478.md) is [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) in the [maqowm](../../strongs/h/h4725.md) which he shall [bāḥar](../../strongs/h/h977.md), thou shalt [qara'](../../strongs/h/h7121.md) this [towrah](../../strongs/h/h8451.md) before all [Yisra'el](../../strongs/h/h3478.md) in their ['ozen](../../strongs/h/h241.md).

<a name="deuteronomy_31_12"></a>Deuteronomy 31:12

[qāhal](../../strongs/h/h6950.md) the ['am](../../strongs/h/h5971.md), ['enowsh](../../strongs/h/h582.md) and ['ishshah](../../strongs/h/h802.md), and [ṭap̄](../../strongs/h/h2945.md), and thy [ger](../../strongs/h/h1616.md) that is within thy [sha'ar](../../strongs/h/h8179.md), that they may [shama'](../../strongs/h/h8085.md), and that they may [lamad](../../strongs/h/h3925.md), and [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md):

<a name="deuteronomy_31_13"></a>Deuteronomy 31:13

And that their [ben](../../strongs/h/h1121.md), which have not [yada'](../../strongs/h/h3045.md) any thing, may [shama'](../../strongs/h/h8085.md), and [lamad](../../strongs/h/h3925.md) to [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), as [yowm](../../strongs/h/h3117.md) ye [chay](../../strongs/h/h2416.md) in the ['ăḏāmâ](../../strongs/h/h127.md) whither ye ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_31_14"></a>Deuteronomy 31:14

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Behold, thy [yowm](../../strongs/h/h3117.md) [qāraḇ](../../strongs/h/h7126.md) that thou must [muwth](../../strongs/h/h4191.md): [qara'](../../strongs/h/h7121.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and [yatsab](../../strongs/h/h3320.md) yourselves in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), that I may give him a [tsavah](../../strongs/h/h6680.md). And [Mōshe](../../strongs/h/h4872.md) and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [yālaḵ](../../strongs/h/h3212.md), and [yatsab](../../strongs/h/h3320.md) themselves in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="deuteronomy_31_15"></a>Deuteronomy 31:15

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) in the ['ohel](../../strongs/h/h168.md) in a [ʿammûḏ](../../strongs/h/h5982.md) of a [ʿānān](../../strongs/h/h6051.md): and the [ʿammûḏ](../../strongs/h/h5982.md) of the [ʿānān](../../strongs/h/h6051.md) ['amad](../../strongs/h/h5975.md) over the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md).

<a name="deuteronomy_31_16"></a>Deuteronomy 31:16

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), Behold, thou shalt [shakab](../../strongs/h/h7901.md) with thy ['ab](../../strongs/h/h1.md); and this ['am](../../strongs/h/h5971.md) will [quwm](../../strongs/h/h6965.md), and [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) the ['Elohiym](../../strongs/h/h430.md) of the [nēḵār](../../strongs/h/h5236.md) of the ['erets](../../strongs/h/h776.md), whither they [bow'](../../strongs/h/h935.md) to be [qereḇ](../../strongs/h/h7130.md) them, and will ['azab](../../strongs/h/h5800.md) me, and [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) which I have [karath](../../strongs/h/h3772.md) with them.

<a name="deuteronomy_31_17"></a>Deuteronomy 31:17

Then my ['aph](../../strongs/h/h639.md) shall be [ḥārâ](../../strongs/h/h2734.md) against them in that [yowm](../../strongs/h/h3117.md), and I will ['azab](../../strongs/h/h5800.md) them, and I will [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) from them, and they shall be ['akal](../../strongs/h/h398.md), and [rab](../../strongs/h/h7227.md) [ra'](../../strongs/h/h7451.md) and [tsarah](../../strongs/h/h6869.md) shall [māṣā'](../../strongs/h/h4672.md) them; so that they will ['āmar](../../strongs/h/h559.md) in that [yowm](../../strongs/h/h3117.md), Are not these [ra'](../../strongs/h/h7451.md) [māṣā'](../../strongs/h/h4672.md) upon us, because our ['Elohiym](../../strongs/h/h430.md) is not [qereḇ](../../strongs/h/h7130.md) us?

<a name="deuteronomy_31_18"></a>Deuteronomy 31:18

And I will [cathar](../../strongs/h/h5641.md) [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) in that [yowm](../../strongs/h/h3117.md) for all the [ra'](../../strongs/h/h7451.md) which they shall have ['asah](../../strongs/h/h6213.md), in that they are [panah](../../strongs/h/h6437.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_31_19"></a>Deuteronomy 31:19

Now therefore [kāṯaḇ](../../strongs/h/h3789.md) ye this [šîr](../../strongs/h/h7892.md) for you, and [lamad](../../strongs/h/h3925.md) it the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): [śûm](../../strongs/h/h7760.md) it in their [peh](../../strongs/h/h6310.md), that this [šîr](../../strongs/h/h7892.md) may be an ['ed](../../strongs/h/h5707.md) for me against the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_31_20"></a>Deuteronomy 31:20

For when I shall have [bow'](../../strongs/h/h935.md) them into the ['ăḏāmâ](../../strongs/h/h127.md) which I [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md), that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md); and they shall have ['akal](../../strongs/h/h398.md) and [sāׂbaʿ](../../strongs/h/h7646.md) themselves, and [dāšēn](../../strongs/h/h1878.md); then will they [panah](../../strongs/h/h6437.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) them, and [na'ats](../../strongs/h/h5006.md) me, and [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md).

<a name="deuteronomy_31_21"></a>Deuteronomy 31:21

And it shall come to pass, when [rab](../../strongs/h/h7227.md) [ra'](../../strongs/h/h7451.md) and [tsarah](../../strongs/h/h6869.md) are [māṣā'](../../strongs/h/h4672.md) them, that this [šîr](../../strongs/h/h7892.md) shall ['anah](../../strongs/h/h6030.md) [paniym](../../strongs/h/h6440.md) them as an ['ed](../../strongs/h/h5707.md); for it shall not be [shakach](../../strongs/h/h7911.md) out of the [peh](../../strongs/h/h6310.md) of their [zera'](../../strongs/h/h2233.md): for I [yada'](../../strongs/h/h3045.md) their [yetser](../../strongs/h/h3336.md) which they ['asah](../../strongs/h/h6213.md), even [yowm](../../strongs/h/h3117.md), before I have [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md).

<a name="deuteronomy_31_22"></a>Deuteronomy 31:22

[Mōshe](../../strongs/h/h4872.md) therefore [kāṯaḇ](../../strongs/h/h3789.md) this [šîr](../../strongs/h/h7892.md) the same [yowm](../../strongs/h/h3117.md), and [lamad](../../strongs/h/h3925.md) it the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_31_23"></a>Deuteronomy 31:23

And he gave [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) a [tsavah](../../strongs/h/h6680.md), and ['āmar](../../strongs/h/h559.md), Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md): for thou shalt [bow'](../../strongs/h/h935.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) into the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md) unto them: and I will be with thee.

<a name="deuteronomy_31_24"></a>Deuteronomy 31:24

And it came to pass, when [Mōshe](../../strongs/h/h4872.md) had made a [kalah](../../strongs/h/h3615.md) of [kāṯaḇ](../../strongs/h/h3789.md) the [dabar](../../strongs/h/h1697.md) of this [towrah](../../strongs/h/h8451.md) in a [sēp̄er](../../strongs/h/h5612.md), until they were [tamam](../../strongs/h/h8552.md),

<a name="deuteronomy_31_25"></a>Deuteronomy 31:25

That [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) the [Lᵊvî](../../strongs/h/h3881.md), which [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_31_26"></a>Deuteronomy 31:26

[laqach](../../strongs/h/h3947.md) this [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md), and [śûm](../../strongs/h/h7760.md) it in the [ṣaḏ](../../strongs/h/h6654.md) of the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), that it may be there for an ['ed](../../strongs/h/h5707.md) against thee.

<a name="deuteronomy_31_27"></a>Deuteronomy 31:27

For I [yada'](../../strongs/h/h3045.md) thy [mᵊrî](../../strongs/h/h4805.md), and thy [qāšê](../../strongs/h/h7186.md) [ʿōrep̄](../../strongs/h/h6203.md): behold, while I am yet [chay](../../strongs/h/h2416.md) with you this [yowm](../../strongs/h/h3117.md), ye have been [marah](../../strongs/h/h4784.md) against [Yĕhovah](../../strongs/h/h3068.md); and how much more ['aḥar](../../strongs/h/h310.md) my [maveth](../../strongs/h/h4194.md)?

<a name="deuteronomy_31_28"></a>Deuteronomy 31:28

[qāhal](../../strongs/h/h6950.md) unto me all the [zāqēn](../../strongs/h/h2205.md) of your [shebet](../../strongs/h/h7626.md), and your [šāṭar](../../strongs/h/h7860.md), that I may [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) in their ['ozen](../../strongs/h/h241.md), and [ʿûḏ](../../strongs/h/h5749.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md) against them.

<a name="deuteronomy_31_29"></a>Deuteronomy 31:29

For I [yada'](../../strongs/h/h3045.md) that ['aḥar](../../strongs/h/h310.md) my [maveth](../../strongs/h/h4194.md) ye will [shachath](../../strongs/h/h7843.md) [shachath](../../strongs/h/h7843.md), and [cuwr](../../strongs/h/h5493.md) from the [derek](../../strongs/h/h1870.md) which I have [tsavah](../../strongs/h/h6680.md) you; and [ra'](../../strongs/h/h7451.md) will [qārā'](../../strongs/h/h7122.md) you in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md); because ye will ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), to [kāʿas](../../strongs/h/h3707.md) him through the [ma'aseh](../../strongs/h/h4639.md) of your [yad](../../strongs/h/h3027.md).

<a name="deuteronomy_31_30"></a>Deuteronomy 31:30

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) in the ['ozen](../../strongs/h/h241.md) of all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md) the [dabar](../../strongs/h/h1697.md) of this [šîr](../../strongs/h/h7892.md), until they were [tamam](../../strongs/h/h8552.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 30](deuteronomy_30.md) - [Deuteronomy 32](deuteronomy_32.md)