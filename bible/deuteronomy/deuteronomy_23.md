# [Deuteronomy 23](https://www.blueletterbible.org/kjv/deu/23)

<a name="deuteronomy_23_1"></a>Deuteronomy 23:1

He that is [pāṣaʿ](../../strongs/h/h6481.md) [dakâ](../../strongs/h/h1795.md), or hath his [šāp̄ḵâ](../../strongs/h/h8212.md) [karath](../../strongs/h/h3772.md), shall not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_23_2"></a>Deuteronomy 23:2

A [mamzēr](../../strongs/h/h4464.md) shall not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md); even to his tenth [dôr](../../strongs/h/h1755.md) shall he not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="deuteronomy_23_3"></a>Deuteronomy 23:3

An [ʿAmmôn](../../strongs/h/h5984.md) or [Mô'āḇî](../../strongs/h/h4125.md) shall not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md); even to their tenth [dôr](../../strongs/h/h1755.md) shall they not [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md):

<a name="deuteronomy_23_4"></a>Deuteronomy 23:4

[dabar](../../strongs/h/h1697.md) they [qadam](../../strongs/h/h6923.md) you not with [lechem](../../strongs/h/h3899.md) and with [mayim](../../strongs/h/h4325.md) in the [derek](../../strongs/h/h1870.md), when ye [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md); and because they [śāḵar](../../strongs/h/h7936.md) against thee [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) of [Pᵊṯôr](../../strongs/h/h6604.md) of ['Ăram nahărayim](../../strongs/h/h763.md), to [qālal](../../strongs/h/h7043.md) thee.

<a name="deuteronomy_23_5"></a>Deuteronomy 23:5

Nevertheless [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto [Bilʿām](../../strongs/h/h1109.md); but [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [hāp̄aḵ](../../strongs/h/h2015.md) the [qᵊlālâ](../../strongs/h/h7045.md) into a [bĕrakah](../../strongs/h/h1293.md) unto thee, because [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) ['ahab](../../strongs/h/h157.md) thee.

<a name="deuteronomy_23_6"></a>Deuteronomy 23:6

Thou shalt not [darash](../../strongs/h/h1875.md) their [shalowm](../../strongs/h/h7965.md) nor their [towb](../../strongs/h/h2896.md) all thy [yowm](../../strongs/h/h3117.md) ['owlam](../../strongs/h/h5769.md).

<a name="deuteronomy_23_7"></a>Deuteronomy 23:7

Thou shalt not [ta'ab](../../strongs/h/h8581.md) an ['Ăḏōmî](../../strongs/h/h130.md); for he is thy ['ach](../../strongs/h/h251.md): thou shalt not [ta'ab](../../strongs/h/h8581.md) a [Miṣrî](../../strongs/h/h4713.md); because thou wast a [ger](../../strongs/h/h1616.md) in his ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_23_8"></a>Deuteronomy 23:8

The [ben](../../strongs/h/h1121.md) that are [yalad](../../strongs/h/h3205.md) of them shall [bow'](../../strongs/h/h935.md) into the [qāhēl](../../strongs/h/h6951.md) of [Yĕhovah](../../strongs/h/h3068.md) in their third [dôr](../../strongs/h/h1755.md).

<a name="deuteronomy_23_9"></a>Deuteronomy 23:9

When the [maḥănê](../../strongs/h/h4264.md) [yāṣā'](../../strongs/h/h3318.md) against thine ['oyeb](../../strongs/h/h341.md), then [shamar](../../strongs/h/h8104.md) thee from every [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md).

<a name="deuteronomy_23_10"></a>Deuteronomy 23:10

If there be among you any ['iysh](../../strongs/h/h376.md), that is not [tahowr](../../strongs/h/h2889.md) by [qārê](../../strongs/h/h7137.md) him by [layil](../../strongs/h/h3915.md), then shall he [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md) out of the [maḥănê](../../strongs/h/h4264.md), he shall not [bow'](../../strongs/h/h935.md) [tavek](../../strongs/h/h8432.md) the [maḥănê](../../strongs/h/h4264.md):

<a name="deuteronomy_23_11"></a>Deuteronomy 23:11

But it shall be, when ['ereb](../../strongs/h/h6153.md) [panah](../../strongs/h/h6437.md), he shall [rāḥaṣ](../../strongs/h/h7364.md) with [mayim](../../strongs/h/h4325.md): and when the [šemeš](../../strongs/h/h8121.md) is [bow'](../../strongs/h/h935.md), he shall [bow'](../../strongs/h/h935.md) [tavek](../../strongs/h/h8432.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="deuteronomy_23_12"></a>Deuteronomy 23:12

Thou shalt have a [yad](../../strongs/h/h3027.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md), whither thou shalt [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md):

<a name="deuteronomy_23_13"></a>Deuteronomy 23:13

And thou shalt have a [yāṯēḏ](../../strongs/h/h3489.md) upon thy ['āzēn](../../strongs/h/h240.md); and it shall be, when thou wilt [yashab](../../strongs/h/h3427.md) thyself [ḥûṣ](../../strongs/h/h2351.md), thou shalt [chaphar](../../strongs/h/h2658.md) therewith, and shalt [shuwb](../../strongs/h/h7725.md) and [kāsâ](../../strongs/h/h3680.md) that which [ṣē'â](../../strongs/h/h6627.md) from thee:

<a name="deuteronomy_23_14"></a>Deuteronomy 23:14

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [halak](../../strongs/h/h1980.md) in the [qereḇ](../../strongs/h/h7130.md) of thy [maḥănê](../../strongs/h/h4264.md), to [natsal](../../strongs/h/h5337.md) thee, and to [nathan](../../strongs/h/h5414.md) thine ['oyeb](../../strongs/h/h341.md) [paniym](../../strongs/h/h6440.md) thee; therefore shall thy [maḥănê](../../strongs/h/h4264.md) be [qadowsh](../../strongs/h/h6918.md): that he [ra'ah](../../strongs/h/h7200.md) no [ʿervâ](../../strongs/h/h6172.md) [dabar](../../strongs/h/h1697.md) in thee, and [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md).

<a name="deuteronomy_23_15"></a>Deuteronomy 23:15

Thou shalt not [cagar](../../strongs/h/h5462.md) unto his ['adown](../../strongs/h/h113.md) the ['ebed](../../strongs/h/h5650.md) which is [natsal](../../strongs/h/h5337.md) from his ['adown](../../strongs/h/h113.md) unto thee:

<a name="deuteronomy_23_16"></a>Deuteronomy 23:16

He shall [yashab](../../strongs/h/h3427.md) with thee, even [qereḇ](../../strongs/h/h7130.md) you, in that [maqowm](../../strongs/h/h4725.md) which he shall [bāḥar](../../strongs/h/h977.md) in one of thy [sha'ar](../../strongs/h/h8179.md), where it [towb](../../strongs/h/h2896.md) him: thou shalt not [yānâ](../../strongs/h/h3238.md) him.

<a name="deuteronomy_23_17"></a>Deuteronomy 23:17

There shall be no [qᵊḏēšâ](../../strongs/h/h6948.md) of the [bath](../../strongs/h/h1323.md) of [Yisra'el](../../strongs/h/h3478.md), nor a [qāḏēš](../../strongs/h/h6945.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="deuteronomy_23_18"></a>Deuteronomy 23:18

Thou shalt not [bow'](../../strongs/h/h935.md) the ['eṯnan](../../strongs/h/h868.md) of a [zānâ](../../strongs/h/h2181.md), or the [mᵊḥîr](../../strongs/h/h4242.md) of a [keleḇ](../../strongs/h/h3611.md), into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) for any [neḏer](../../strongs/h/h5088.md): for even both these are [tôʿēḇâ](../../strongs/h/h8441.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="deuteronomy_23_19"></a>Deuteronomy 23:19

Thou shalt not [nāšaḵ](../../strongs/h/h5391.md) to thy ['ach](../../strongs/h/h251.md); [neshek](../../strongs/h/h5392.md) of [keceph](../../strongs/h/h3701.md), [neshek](../../strongs/h/h5392.md) of ['ōḵel](../../strongs/h/h400.md), [neshek](../../strongs/h/h5392.md) of any [dabar](../../strongs/h/h1697.md) that is [nāšaḵ](../../strongs/h/h5391.md):

<a name="deuteronomy_23_20"></a>Deuteronomy 23:20

Unto a [nāḵrî](../../strongs/h/h5237.md) thou mayest [nāšaḵ](../../strongs/h/h5391.md); but unto thy ['ach](../../strongs/h/h251.md) thou shalt not [nāšaḵ](../../strongs/h/h5391.md): that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) may [barak](../../strongs/h/h1288.md) thee in all that thou [mišlôaḥ](../../strongs/h/h4916.md) thine [yad](../../strongs/h/h3027.md) to in the ['erets](../../strongs/h/h776.md) whither thou [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="deuteronomy_23_21"></a>Deuteronomy 23:21

When thou shalt [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), thou shalt not ['āḥar](../../strongs/h/h309.md) to [shalam](../../strongs/h/h7999.md) it: for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will [darash](../../strongs/h/h1875.md) [darash](../../strongs/h/h1875.md) it of thee; and it would be [ḥēṭĕ'](../../strongs/h/h2399.md) in thee.

<a name="deuteronomy_23_22"></a>Deuteronomy 23:22

But if thou shalt [ḥāḏal](../../strongs/h/h2308.md) to [nāḏar](../../strongs/h/h5087.md), it shall be no [ḥēṭĕ'](../../strongs/h/h2399.md) in thee.

<a name="deuteronomy_23_23"></a>Deuteronomy 23:23

That which is [môṣā'](../../strongs/h/h4161.md) of thy [saphah](../../strongs/h/h8193.md) thou shalt [shamar](../../strongs/h/h8104.md) and ['asah](../../strongs/h/h6213.md); even a [nᵊḏāḇâ](../../strongs/h/h5071.md), according as thou hast [nāḏar](../../strongs/h/h5087.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which thou hast [dabar](../../strongs/h/h1696.md) with thy [peh](../../strongs/h/h6310.md).

<a name="deuteronomy_23_24"></a>Deuteronomy 23:24

When thou [bow'](../../strongs/h/h935.md) into thy [rea'](../../strongs/h/h7453.md) [kerem](../../strongs/h/h3754.md), then thou mayest ['akal](../../strongs/h/h398.md) [ʿēnāḇ](../../strongs/h/h6025.md) thy [śōḇaʿ](../../strongs/h/h7648.md) at thine own [nephesh](../../strongs/h/h5315.md); but thou shalt not [nathan](../../strongs/h/h5414.md) in thy [kĕliy](../../strongs/h/h3627.md).

<a name="deuteronomy_23_25"></a>Deuteronomy 23:25

When thou [bow'](../../strongs/h/h935.md) into the [qāmâ](../../strongs/h/h7054.md) of thy [rea'](../../strongs/h/h7453.md), then thou mayest [qāṭap̄](../../strongs/h/h6998.md) the [mᵊlîlâ](../../strongs/h/h4425.md) with thine [yad](../../strongs/h/h3027.md); but thou shalt not [nûp̄](../../strongs/h/h5130.md) a [ḥermēš](../../strongs/h/h2770.md) unto thy [rea'](../../strongs/h/h7453.md) [qāmâ](../../strongs/h/h7054.md).

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 22](deuteronomy_22.md) - [Deuteronomy 24](deuteronomy_24.md)