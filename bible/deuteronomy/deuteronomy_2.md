# [Deuteronomy 2](https://www.blueletterbible.org/kjv/deu/2)

<a name="deuteronomy_2_1"></a>Deuteronomy 2:1

Then we [panah](../../strongs/h/h6437.md), and took our [nāsaʿ](../../strongs/h/h5265.md) into the [midbar](../../strongs/h/h4057.md) by the [derek](../../strongs/h/h1870.md) of the [Sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto me: and we [cabab](../../strongs/h/h5437.md) [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_2_2"></a>Deuteronomy 2:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_2_3"></a>Deuteronomy 2:3

Ye have [cabab](../../strongs/h/h5437.md) this [har](../../strongs/h/h2022.md) [rab](../../strongs/h/h7227.md): [panah](../../strongs/h/h6437.md) you [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="deuteronomy_2_4"></a>Deuteronomy 2:4

And [tsavah](../../strongs/h/h6680.md) thou the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Ye are to ['abar](../../strongs/h/h5674.md) the [gᵊḇûl](../../strongs/h/h1366.md) of your ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md), which [yashab](../../strongs/h/h3427.md) in [Śēʿîr](../../strongs/h/h8165.md); and they shall be [yare'](../../strongs/h/h3372.md) of you: take ye [me'od](../../strongs/h/h3966.md) [shamar](../../strongs/h/h8104.md) unto yourselves therefore:

<a name="deuteronomy_2_5"></a>Deuteronomy 2:5

[gārâ](../../strongs/h/h1624.md) not with them; for I will not [nathan](../../strongs/h/h5414.md) you of their ['erets](../../strongs/h/h776.md), no, not so much as a [regel](../../strongs/h/h7272.md) [miḏrāḵ](../../strongs/h/h4096.md) [kaph](../../strongs/h/h3709.md); because I have [nathan](../../strongs/h/h5414.md) [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md) unto [ʿĒśāv](../../strongs/h/h6215.md) for a [yᵊruššâ](../../strongs/h/h3425.md).

<a name="deuteronomy_2_6"></a>Deuteronomy 2:6

Ye shall [šāḇar](../../strongs/h/h7666.md) ['ōḵel](../../strongs/h/h400.md) of them for [keceph](../../strongs/h/h3701.md), that ye may ['akal](../../strongs/h/h398.md); and ye shall also [kārâ](../../strongs/h/h3739.md) [mayim](../../strongs/h/h4325.md) of them for [keceph](../../strongs/h/h3701.md), that ye may [šāṯâ](../../strongs/h/h8354.md).

<a name="deuteronomy_2_7"></a>Deuteronomy 2:7

For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [barak](../../strongs/h/h1288.md) thee in all the [ma'aseh](../../strongs/h/h4639.md) of thy [yad](../../strongs/h/h3027.md): he [yada'](../../strongs/h/h3045.md) thy [yālaḵ](../../strongs/h/h3212.md) through this [gadowl](../../strongs/h/h1419.md) [midbar](../../strongs/h/h4057.md): these forty [šānâ](../../strongs/h/h8141.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath been with thee; thou hast [ḥāsēr](../../strongs/h/h2637.md) [dabar](../../strongs/h/h1697.md).

<a name="deuteronomy_2_8"></a>Deuteronomy 2:8

And when we ['abar](../../strongs/h/h5674.md) by from our ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md), which [yashab](../../strongs/h/h3427.md) in [Śēʿîr](../../strongs/h/h8165.md), through the [derek](../../strongs/h/h1870.md) of the ['arabah](../../strongs/h/h6160.md) from ['Êlôṯ](../../strongs/h/h359.md), and from [ʿEṣyôn Geḇer](../../strongs/h/h6100.md), we [panah](../../strongs/h/h6437.md) and ['abar](../../strongs/h/h5674.md) by the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="deuteronomy_2_9"></a>Deuteronomy 2:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [ṣûr](../../strongs/h/h6696.md) not the [Mô'āḇ](../../strongs/h/h4124.md), neither [gārâ](../../strongs/h/h1624.md) with them in [milḥāmâ](../../strongs/h/h4421.md): for I will not [nathan](../../strongs/h/h5414.md) thee of their ['erets](../../strongs/h/h776.md) for a [yᵊruššâ](../../strongs/h/h3425.md); because I have [nathan](../../strongs/h/h5414.md) [ʿĀr](../../strongs/h/h6144.md) unto the [ben](../../strongs/h/h1121.md) of [Lôṭ](../../strongs/h/h3876.md) for a [yᵊruššâ](../../strongs/h/h3425.md).

<a name="deuteronomy_2_10"></a>Deuteronomy 2:10

The ['Êmîm](../../strongs/h/h368.md) [yashab](../../strongs/h/h3427.md) therein [paniym](../../strongs/h/h6440.md), an ['am](../../strongs/h/h5971.md) [gadowl](../../strongs/h/h1419.md), and [rab](../../strongs/h/h7227.md), and [ruwm](../../strongs/h/h7311.md), as the [ʿĂnāqîm](../../strongs/h/h6062.md);

<a name="deuteronomy_2_11"></a>Deuteronomy 2:11

Which also were [chashab](../../strongs/h/h2803.md) [rᵊp̄ā'îm](../../strongs/h/h7497.md), as the [ʿĂnāqîm](../../strongs/h/h6062.md); but the [Mô'āḇî](../../strongs/h/h4125.md) [qara'](../../strongs/h/h7121.md) them ['Êmîm](../../strongs/h/h368.md).

<a name="deuteronomy_2_12"></a>Deuteronomy 2:12

The [ḥōrî](../../strongs/h/h2752.md) also [yashab](../../strongs/h/h3427.md) in [Śēʿîr](../../strongs/h/h8165.md) [paniym](../../strongs/h/h6440.md); but the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md) [yarash](../../strongs/h/h3423.md) them, when they had [šāmaḏ](../../strongs/h/h8045.md) them from [paniym](../../strongs/h/h6440.md) them, and [yashab](../../strongs/h/h3427.md) in their stead; as [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) unto the ['erets](../../strongs/h/h776.md) of his [yᵊruššâ](../../strongs/h/h3425.md), which [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) unto them.

<a name="deuteronomy_2_13"></a>Deuteronomy 2:13

Now [quwm](../../strongs/h/h6965.md), said I, and ['abar](../../strongs/h/h5674.md) you the [nachal](../../strongs/h/h5158.md) [zereḏ](../../strongs/h/h2218.md). And we ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) [zereḏ](../../strongs/h/h2218.md).

<a name="deuteronomy_2_14"></a>Deuteronomy 2:14

And the [yowm](../../strongs/h/h3117.md) in which we [halak](../../strongs/h/h1980.md) from [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md), until we were ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) [zereḏ](../../strongs/h/h2218.md), was thirty and eight [šānâ](../../strongs/h/h8141.md); until all the [dôr](../../strongs/h/h1755.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) were [tamam](../../strongs/h/h8552.md) from [qereḇ](../../strongs/h/h7130.md) the [maḥănê](../../strongs/h/h4264.md), as [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto them.

<a name="deuteronomy_2_15"></a>Deuteronomy 2:15

For indeed the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was against them, to [hāmam](../../strongs/h/h2000.md) them from [qereḇ](../../strongs/h/h7130.md) the [maḥănê](../../strongs/h/h4264.md), until they were [tamam](../../strongs/h/h8552.md).

<a name="deuteronomy_2_16"></a>Deuteronomy 2:16

So it came to pass, when all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) were [tamam](../../strongs/h/h8552.md) and [muwth](../../strongs/h/h4191.md) from [qereḇ](../../strongs/h/h7130.md) the ['am](../../strongs/h/h5971.md),

<a name="deuteronomy_2_17"></a>Deuteronomy 2:17

That [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto me, ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_2_18"></a>Deuteronomy 2:18

Thou art to ['abar](../../strongs/h/h5674.md) through [ʿĀr](../../strongs/h/h6144.md), the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md), this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_2_19"></a>Deuteronomy 2:19

And when thou [qāraḇ](../../strongs/h/h7126.md) over [môl](../../strongs/h/h4136.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), [ṣûr](../../strongs/h/h6696.md) them not, nor [gārâ](../../strongs/h/h1624.md) with them: for I will not [nathan](../../strongs/h/h5414.md) thee of the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) any [yᵊruššâ](../../strongs/h/h3425.md); because I have [nathan](../../strongs/h/h5414.md) it unto the [ben](../../strongs/h/h1121.md) of [Lôṭ](../../strongs/h/h3876.md) for a [yᵊruššâ](../../strongs/h/h3425.md).

<a name="deuteronomy_2_20"></a>Deuteronomy 2:20

(That also was [chashab](../../strongs/h/h2803.md) an ['erets](../../strongs/h/h776.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md): [rᵊp̄ā'îm](../../strongs/h/h7497.md) [yashab](../../strongs/h/h3427.md) therein in [paniym](../../strongs/h/h6440.md); and the [ʿAmmôn](../../strongs/h/h5984.md) [qara'](../../strongs/h/h7121.md) them [Zamzummîm](../../strongs/h/h2157.md);

<a name="deuteronomy_2_21"></a>Deuteronomy 2:21

An ['am](../../strongs/h/h5971.md) [gadowl](../../strongs/h/h1419.md), and [rab](../../strongs/h/h7227.md), and [ruwm](../../strongs/h/h7311.md), as the [ʿĂnāqîm](../../strongs/h/h6062.md); but [Yĕhovah](../../strongs/h/h3068.md) [šāmaḏ](../../strongs/h/h8045.md) them [paniym](../../strongs/h/h6440.md) them; and they [yarash](../../strongs/h/h3423.md) them, and [yashab](../../strongs/h/h3427.md) in their stead:

<a name="deuteronomy_2_22"></a>Deuteronomy 2:22

As he ['asah](../../strongs/h/h6213.md) to the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md), which [yashab](../../strongs/h/h3427.md) in [Śēʿîr](../../strongs/h/h8165.md), when he [šāmaḏ](../../strongs/h/h8045.md) the [ḥōrî](../../strongs/h/h2752.md) from [paniym](../../strongs/h/h6440.md) them; and they [yarash](../../strongs/h/h3423.md) them, and [yashab](../../strongs/h/h3427.md) in their stead even unto this [yowm](../../strongs/h/h3117.md):

<a name="deuteronomy_2_23"></a>Deuteronomy 2:23

And the [ʿAûî](../../strongs/h/h5761.md) which [yashab](../../strongs/h/h3427.md) in [Ḥăṣērîm](../../strongs/h/h2699.md), even unto [ʿAzzâ](../../strongs/h/h5804.md), the [Kap̄tōrî](../../strongs/h/h3732.md), which [yāṣā'](../../strongs/h/h3318.md) out of [Kap̄Tôr](../../strongs/h/h3731.md), [šāmaḏ](../../strongs/h/h8045.md) them, and [yashab](../../strongs/h/h3427.md) in their stead.)

<a name="deuteronomy_2_24"></a>Deuteronomy 2:24

[quwm](../../strongs/h/h6965.md) ye, [nāsaʿ](../../strongs/h/h5265.md), and ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) ['Arnôn](../../strongs/h/h769.md): [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) into thine [yad](../../strongs/h/h3027.md) [Sîḥôn](../../strongs/h/h5511.md) the ['Ĕmōrî](../../strongs/h/h567.md), [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), and his ['erets](../../strongs/h/h776.md): [ḥālal](../../strongs/h/h2490.md) to [yarash](../../strongs/h/h3423.md) it, and [gārâ](../../strongs/h/h1624.md) with him in [milḥāmâ](../../strongs/h/h4421.md).

<a name="deuteronomy_2_25"></a>Deuteronomy 2:25

This [yowm](../../strongs/h/h3117.md) will I [ḥālal](../../strongs/h/h2490.md) to [nathan](../../strongs/h/h5414.md) the [paḥaḏ](../../strongs/h/h6343.md) of thee and the [yir'ah](../../strongs/h/h3374.md) of thee upon the [paniym](../../strongs/h/h6440.md) ['am](../../strongs/h/h5971.md) that are under the [shamayim](../../strongs/h/h8064.md), who shall [shama'](../../strongs/h/h8085.md) [šēmaʿ](../../strongs/h/h8088.md) of thee, and shall [ragaz](../../strongs/h/h7264.md), and be in [chuwl](../../strongs/h/h2342.md) because of thee.

<a name="deuteronomy_2_26"></a>Deuteronomy 2:26

And I [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) out of the [midbar](../../strongs/h/h4057.md) of [Qᵊḏēmôṯ](../../strongs/h/h6932.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md) with [dabar](../../strongs/h/h1697.md) of [shalowm](../../strongs/h/h7965.md), ['āmar](../../strongs/h/h559.md),

<a name="deuteronomy_2_27"></a>Deuteronomy 2:27

Let me ['abar](../../strongs/h/h5674.md) through thy ['erets](../../strongs/h/h776.md): I will [yālaḵ](../../strongs/h/h3212.md) along by the [derek](../../strongs/h/h1870.md) [derek](../../strongs/h/h1870.md), I will neither [cuwr](../../strongs/h/h5493.md) unto the [yamiyn](../../strongs/h/h3225.md) nor to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="deuteronomy_2_28"></a>Deuteronomy 2:28

Thou shalt [šāḇar](../../strongs/h/h7666.md) me ['ōḵel](../../strongs/h/h400.md) for [keceph](../../strongs/h/h3701.md), that I may ['akal](../../strongs/h/h398.md); and [nathan](../../strongs/h/h5414.md) me [mayim](../../strongs/h/h4325.md) for [keceph](../../strongs/h/h3701.md), that I may [šāṯâ](../../strongs/h/h8354.md): only I will ['abar](../../strongs/h/h5674.md) on my [regel](../../strongs/h/h7272.md);

<a name="deuteronomy_2_29"></a>Deuteronomy 2:29

(As the [ben](../../strongs/h/h1121.md) of [ʿĒśāv](../../strongs/h/h6215.md) which [yashab](../../strongs/h/h3427.md) in [Śēʿîr](../../strongs/h/h8165.md), and the [Mô'āḇî](../../strongs/h/h4125.md) which [yashab](../../strongs/h/h3427.md) in [ʿĀr](../../strongs/h/h6144.md), ['asah](../../strongs/h/h6213.md) unto me;) until I shall ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) into the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) us.

<a name="deuteronomy_2_30"></a>Deuteronomy 2:30

But [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md) ['āḇâ](../../strongs/h/h14.md) not let us ['abar](../../strongs/h/h5674.md) by him: for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [qāšâ](../../strongs/h/h7185.md) his [ruwach](../../strongs/h/h7307.md), and made his [lebab](../../strongs/h/h3824.md) ['amats](../../strongs/h/h553.md), that he might [nathan](../../strongs/h/h5414.md) him into thy [yad](../../strongs/h/h3027.md), as appeareth this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_2_31"></a>Deuteronomy 2:31

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [ra'ah](../../strongs/h/h7200.md), I have [ḥālal](../../strongs/h/h2490.md) to [nathan](../../strongs/h/h5414.md) [Sîḥôn](../../strongs/h/h5511.md) and his ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) thee: [ḥālal](../../strongs/h/h2490.md) to [yarash](../../strongs/h/h3423.md), that thou mayest [yarash](../../strongs/h/h3423.md) his ['erets](../../strongs/h/h776.md).

<a name="deuteronomy_2_32"></a>Deuteronomy 2:32

Then [Sîḥôn](../../strongs/h/h5511.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) us, he and all his ['am](../../strongs/h/h5971.md), to [milḥāmâ](../../strongs/h/h4421.md) at [Yahaṣ](../../strongs/h/h3096.md).

<a name="deuteronomy_2_33"></a>Deuteronomy 2:33

And [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him [paniym](../../strongs/h/h6440.md) us; and we [nakah](../../strongs/h/h5221.md) him, and his [ben](../../strongs/h/h1121.md), and all his ['am](../../strongs/h/h5971.md).

<a name="deuteronomy_2_34"></a>Deuteronomy 2:34

And we [lāḵaḏ](../../strongs/h/h3920.md) all his [ʿîr](../../strongs/h/h5892.md) at that [ʿēṯ](../../strongs/h/h6256.md), and [ḥāram](../../strongs/h/h2763.md) the [math](../../strongs/h/h4962.md), and the ['ishshah](../../strongs/h/h802.md), and the [ṭap̄](../../strongs/h/h2945.md), of every [ʿîr](../../strongs/h/h5892.md), we [šā'ar](../../strongs/h/h7604.md) none to [śārîḏ](../../strongs/h/h8300.md):

<a name="deuteronomy_2_35"></a>Deuteronomy 2:35

Only the [bĕhemah](../../strongs/h/h929.md) we took for a [bāzaz](../../strongs/h/h962.md) unto ourselves, and the [šālāl](../../strongs/h/h7998.md) of the [ʿîr](../../strongs/h/h5892.md) which we [lāḵaḏ](../../strongs/h/h3920.md).

<a name="deuteronomy_2_36"></a>Deuteronomy 2:36

From [ʿĂrôʿēr](../../strongs/h/h6177.md), which is by the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) of ['Arnôn](../../strongs/h/h769.md), and from the [ʿîr](../../strongs/h/h5892.md) that is by the [nachal](../../strongs/h/h5158.md), even unto [Gilʿāḏ](../../strongs/h/h1568.md), there was not one [qiryâ](../../strongs/h/h7151.md) too [śāḡaḇ](../../strongs/h/h7682.md) for us: [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) all [paniym](../../strongs/h/h6440.md):

<a name="deuteronomy_2_37"></a>Deuteronomy 2:37

Only unto the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) thou [qāraḇ](../../strongs/h/h7126.md) not, nor unto any [yad](../../strongs/h/h3027.md) of the [nachal](../../strongs/h/h5158.md) [Yabōq](../../strongs/h/h2999.md), nor unto the [ʿîr](../../strongs/h/h5892.md) in the [har](../../strongs/h/h2022.md), nor unto whatsoever [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) us.

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 1](deuteronomy_1.md) - [Deuteronomy 3](deuteronomy_3.md)