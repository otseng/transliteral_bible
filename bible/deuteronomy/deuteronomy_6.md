# [Deuteronomy 6](https://www.blueletterbible.org/kjv/deu/6/5/s_159001)

<a name="deuteronomy_6_1"></a>Deuteronomy 6:1

Now these the [mitsvah](../../strongs/h/h4687.md), the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) to [lamad](../../strongs/h/h3925.md) you, that ye might ['asah](../../strongs/h/h6213.md) in the ['erets](../../strongs/h/h776.md) whither ye ['abar](../../strongs/h/h5674.md) to [yarash](../../strongs/h/h3423.md) it:

<a name="deuteronomy_6_2"></a>Deuteronomy 6:2

That thou mightest [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [shamar](../../strongs/h/h8104.md) all his [chuqqah](../../strongs/h/h2708.md) and his [mitsvah](../../strongs/h/h4687.md), which I [tsavah](../../strongs/h/h6680.md) thee, thou, and thy [ben](../../strongs/h/h1121.md), and thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md); and that thy [yowm](../../strongs/h/h3117.md) may be ['arak](../../strongs/h/h748.md).

<a name="deuteronomy_6_3"></a>Deuteronomy 6:3

[shama'](../../strongs/h/h8085.md) therefore, [Yisra'el](../../strongs/h/h3478.md), and [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) it; that it may be [yatab](../../strongs/h/h3190.md) with thee, and that ye may [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md), as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of thy ['ab](../../strongs/h/h1.md) hath [dabar](../../strongs/h/h1696.md) thee, in the ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="deuteronomy_6_4"></a>Deuteronomy 6:4

[shama'](../../strongs/h/h8085.md), [Yisra'el](../../strongs/h/h3478.md): [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) is ['echad](../../strongs/h/h259.md) [Yĕhovah](../../strongs/h/h3068.md): [^1]

<a name="deuteronomy_6_5"></a>Deuteronomy 6:5

And thou shalt ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) with all thine [lebab](../../strongs/h/h3824.md), and with all thy [nephesh](../../strongs/h/h5315.md), and with all thy [me'od](../../strongs/h/h3966.md).

<a name="deuteronomy_6_6"></a>Deuteronomy 6:6

And these [dabar](../../strongs/h/h1697.md), which I [tsavah](../../strongs/h/h6680.md) thee this [yowm](../../strongs/h/h3117.md), shall be in thine [lebab](../../strongs/h/h3824.md):

<a name="deuteronomy_6_7"></a>Deuteronomy 6:7

And thou shalt [šānan](../../strongs/h/h8150.md) them unto thy [ben](../../strongs/h/h1121.md), and shalt [dabar](../../strongs/h/h1696.md) of them when thou [yashab](../../strongs/h/h3427.md) in thine [bayith](../../strongs/h/h1004.md), and when thou [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md), and when thou [shakab](../../strongs/h/h7901.md), and when thou [quwm](../../strongs/h/h6965.md).

<a name="deuteronomy_6_8"></a>Deuteronomy 6:8

And thou shalt [qāšar](../../strongs/h/h7194.md) them for an ['ôṯ](../../strongs/h/h226.md) upon thine [yad](../../strongs/h/h3027.md), and they shall be as [ṭôṭāp̄ôṯ](../../strongs/h/h2903.md) between thine ['ayin](../../strongs/h/h5869.md).

<a name="deuteronomy_6_9"></a>Deuteronomy 6:9

And thou shalt [kāṯaḇ](../../strongs/h/h3789.md) them upon the [mᵊzûzâ](../../strongs/h/h4201.md) of thy [bayith](../../strongs/h/h1004.md), and on thy [sha'ar](../../strongs/h/h8179.md).

<a name="deuteronomy_6_10"></a>Deuteronomy 6:10

And it shall be, when [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall have [bow'](../../strongs/h/h935.md) thee into the ['erets](../../strongs/h/h776.md) which he [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md), to ['Abraham](../../strongs/h/h85.md), to [Yiṣḥāq](../../strongs/h/h3327.md), and to [Ya'aqob](../../strongs/h/h3290.md), to [nathan](../../strongs/h/h5414.md) thee [gadowl](../../strongs/h/h1419.md) and [towb](../../strongs/h/h2896.md) [ʿîr](../../strongs/h/h5892.md), which thou [bānâ](../../strongs/h/h1129.md) not,

<a name="deuteronomy_6_11"></a>Deuteronomy 6:11

And [bayith](../../strongs/h/h1004.md) [mālē'](../../strongs/h/h4392.md) of all [ṭûḇ](../../strongs/h/h2898.md), which thou [mālā'](../../strongs/h/h4390.md) not, and [bowr](../../strongs/h/h953.md) [ḥāṣaḇ](../../strongs/h/h2672.md), which thou [ḥāṣaḇ](../../strongs/h/h2672.md) not, [kerem](../../strongs/h/h3754.md) and [zayiṯ](../../strongs/h/h2132.md), which thou [nāṭaʿ](../../strongs/h/h5193.md) not; when thou shalt have ['akal](../../strongs/h/h398.md) and be [sāׂbaʿ](../../strongs/h/h7646.md);

<a name="deuteronomy_6_12"></a>Deuteronomy 6:12

Then [shamar](../../strongs/h/h8104.md) lest thou [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md), which [yāṣā'](../../strongs/h/h3318.md) thee out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md).

<a name="deuteronomy_6_13"></a>Deuteronomy 6:13

Thou shalt [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) him, and shalt [shaba'](../../strongs/h/h7650.md) by his [shem](../../strongs/h/h8034.md).

<a name="deuteronomy_6_14"></a>Deuteronomy 6:14

Ye shall not [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), of the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) which are [cabiyb](../../strongs/h/h5439.md) you;

<a name="deuteronomy_6_15"></a>Deuteronomy 6:15

(For [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is a [qanna'](../../strongs/h/h7067.md) ['el](../../strongs/h/h410.md) [qereḇ](../../strongs/h/h7130.md) you) lest the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) be [ḥārâ](../../strongs/h/h2734.md) against thee, and [šāmaḏ](../../strongs/h/h8045.md) thee from off the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="deuteronomy_6_16"></a>Deuteronomy 6:16

Ye shall not [nāsâ](../../strongs/h/h5254.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), as ye [nāsâ](../../strongs/h/h5254.md) him in [massâ](../../strongs/h/h4532.md).

<a name="deuteronomy_6_17"></a>Deuteronomy 6:17

Ye shall [shamar](../../strongs/h/h8104.md) [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and his [ʿēḏâ](../../strongs/h/h5713.md), and his [choq](../../strongs/h/h2706.md), which he hath [tsavah](../../strongs/h/h6680.md) thee.

<a name="deuteronomy_6_18"></a>Deuteronomy 6:18

And thou shalt ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) and [towb](../../strongs/h/h2896.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): that it may be [yatab](../../strongs/h/h3190.md) with thee, and that thou mayest [bow'](../../strongs/h/h935.md) and [yarash](../../strongs/h/h3423.md) the [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto thy ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_6_19"></a>Deuteronomy 6:19

To [hāḏap̄](../../strongs/h/h1920.md) all thine ['oyeb](../../strongs/h/h341.md) from [paniym](../../strongs/h/h6440.md) thee, as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md).

<a name="deuteronomy_6_20"></a>Deuteronomy 6:20

And when thy [ben](../../strongs/h/h1121.md) [sha'al](../../strongs/h/h7592.md) thee in [māḥār](../../strongs/h/h4279.md), ['āmar](../../strongs/h/h559.md), What mean the [ʿēḏâ](../../strongs/h/h5713.md), and the [choq](../../strongs/h/h2706.md), and the [mishpat](../../strongs/h/h4941.md), which [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) hath [tsavah](../../strongs/h/h6680.md) you?

<a name="deuteronomy_6_21"></a>Deuteronomy 6:21

Then thou shalt ['āmar](../../strongs/h/h559.md) unto thy [ben](../../strongs/h/h1121.md), We were [Parʿô](../../strongs/h/h6547.md) ['ebed](../../strongs/h/h5650.md) in [Mitsrayim](../../strongs/h/h4714.md); and [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) us of [Mitsrayim](../../strongs/h/h4714.md) with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md):

<a name="deuteronomy_6_22"></a>Deuteronomy 6:22

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) ['ôṯ](../../strongs/h/h226.md) and [môp̄ēṯ](../../strongs/h/h4159.md), [gadowl](../../strongs/h/h1419.md) and [ra'](../../strongs/h/h7451.md), upon [Mitsrayim](../../strongs/h/h4714.md), upon [Parʿô](../../strongs/h/h6547.md), and upon all his [bayith](../../strongs/h/h1004.md), before our ['ayin](../../strongs/h/h5869.md):

<a name="deuteronomy_6_23"></a>Deuteronomy 6:23

And he [yāṣā'](../../strongs/h/h3318.md) us from thence, that he might [bow'](../../strongs/h/h935.md) us in, to [nathan](../../strongs/h/h5414.md) us the ['erets](../../strongs/h/h776.md) which he [shaba'](../../strongs/h/h7650.md) unto our ['ab](../../strongs/h/h1.md).

<a name="deuteronomy_6_24"></a>Deuteronomy 6:24

And [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) us to ['asah](../../strongs/h/h6213.md) all these [choq](../../strongs/h/h2706.md), to [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), for our [towb](../../strongs/h/h2896.md) [yowm](../../strongs/h/h3117.md), that he might [ḥāyâ](../../strongs/h/h2421.md) us, as it is at this [yowm](../../strongs/h/h3117.md).

<a name="deuteronomy_6_25"></a>Deuteronomy 6:25

And it shall be our [ṣĕdāqāh](../../strongs/h/h6666.md), if we [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all these [mitsvah](../../strongs/h/h4687.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), as he hath [tsavah](../../strongs/h/h6680.md) us.

---

[^1]: [Deuteronomy 6:4 Commentary](../../commentary/deuteronomy/deuteronomy_6_commentary.md#deuteronomy_6_4)

---

[Transliteral Bible](../bible.md)

[Deuteronomy](deuteronomy.md)

[Deuteronomy 5](deuteronomy_5.md) - [Deuteronomy 7](deuteronomy_7.md)