# [Nahum 3](https://www.blueletterbible.org/kjv/nahum/3)

<a name="nahum_3_1"></a>Nahum 3:1

[hôy](../../strongs/h/h1945.md) to the [dam](../../strongs/h/h1818.md) [ʿîr](../../strongs/h/h5892.md)! it is all [mālē'](../../strongs/h/h4392.md) of [kaḥaš](../../strongs/h/h3585.md) and [pereq](../../strongs/h/h6563.md); the [ṭerep̄](../../strongs/h/h2964.md) [mûš](../../strongs/h/h4185.md) not;

<a name="nahum_3_2"></a>Nahum 3:2

The [qowl](../../strongs/h/h6963.md) of a [šôṭ](../../strongs/h/h7752.md), and the [qowl](../../strongs/h/h6963.md) of the [raʿaš](../../strongs/h/h7494.md) of the ['ôp̄ān](../../strongs/h/h212.md), and of the [dāhar](../../strongs/h/h1725.md) [sûs](../../strongs/h/h5483.md), and of the [rāqaḏ](../../strongs/h/h7540.md) [merkāḇâ](../../strongs/h/h4818.md).

<a name="nahum_3_3"></a>Nahum 3:3

The [pārāš](../../strongs/h/h6571.md) [ʿālâ](../../strongs/h/h5927.md) both the [lahaḇ](../../strongs/h/h3851.md) [chereb](../../strongs/h/h2719.md) and the [baraq](../../strongs/h/h1300.md) [ḥănîṯ](../../strongs/h/h2595.md): and there is a [rōḇ](../../strongs/h/h7230.md) of [ḥālāl](../../strongs/h/h2491.md), and a [kōḇeḏ](../../strongs/h/h3514.md) number of [peḡer](../../strongs/h/h6297.md); and there is none [qāṣê](../../strongs/h/h7097.md) of their [gᵊvîyâ](../../strongs/h/h1472.md); they [kashal](../../strongs/h/h3782.md) [kashal](../../strongs/h/h3782.md) upon their [gᵊvîyâ](../../strongs/h/h1472.md):

<a name="nahum_3_4"></a>Nahum 3:4

Because of the [rōḇ](../../strongs/h/h7230.md) of the [zᵊnûnîm](../../strongs/h/h2183.md) of the [towb](../../strongs/h/h2896.md) [ḥēn](../../strongs/h/h2580.md) [zānâ](../../strongs/h/h2181.md), the [baʿălâ](../../strongs/h/h1172.md) of [kešep̄](../../strongs/h/h3785.md), that [māḵar](../../strongs/h/h4376.md) [gowy](../../strongs/h/h1471.md) through her [zᵊnûnîm](../../strongs/h/h2183.md), and [mišpāḥâ](../../strongs/h/h4940.md) through her [kešep̄](../../strongs/h/h3785.md).

<a name="nahum_3_5"></a>Nahum 3:5

Behold, I am against thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); and I will [gālâ](../../strongs/h/h1540.md) thy [shuwl](../../strongs/h/h7757.md) upon thy [paniym](../../strongs/h/h6440.md), and I will [ra'ah](../../strongs/h/h7200.md) the [gowy](../../strongs/h/h1471.md) thy [maʿar](../../strongs/h/h4626.md), and the [mamlāḵâ](../../strongs/h/h4467.md) thy [qālôn](../../strongs/h/h7036.md).

<a name="nahum_3_6"></a>Nahum 3:6

And I will [shalak](../../strongs/h/h7993.md) [šiqqûṣ](../../strongs/h/h8251.md) upon thee, and make thee [nabel](../../strongs/h/h5034.md), and will [śûm](../../strongs/h/h7760.md) thee as a [rŏ'î](../../strongs/h/h7210.md).

<a name="nahum_3_7"></a>Nahum 3:7

And it shall come to pass, that all they that [ra'ah](../../strongs/h/h7200.md) upon thee shall [nāḏaḏ](../../strongs/h/h5074.md) from thee, and ['āmar](../../strongs/h/h559.md), [Nînvê](../../strongs/h/h5210.md) is [shadad](../../strongs/h/h7703.md): who will [nuwd](../../strongs/h/h5110.md) her? whence shall I [bāqaš](../../strongs/h/h1245.md) [nacham](../../strongs/h/h5162.md) for thee?

<a name="nahum_3_8"></a>Nahum 3:8

Art thou [yatab](../../strongs/h/h3190.md) than ['āmôn](../../strongs/h/h527.md) ['Āmôn](../../strongs/h/h528.md) [Nō'](../../strongs/h/h4996.md), that was [yashab](../../strongs/h/h3427.md) among the [yᵊ'ōr](../../strongs/h/h2975.md), that had the [mayim](../../strongs/h/h4325.md) [cabiyb](../../strongs/h/h5439.md) it, whose [cheyl](../../strongs/h/h2426.md) was the [yam](../../strongs/h/h3220.md), and her [ḥômâ](../../strongs/h/h2346.md) was from the [yam](../../strongs/h/h3220.md)?

<a name="nahum_3_9"></a>Nahum 3:9

[Kûš](../../strongs/h/h3568.md) and [Mitsrayim](../../strongs/h/h4714.md) were her [ʿāṣmâ](../../strongs/h/h6109.md), and it was ['în](../../strongs/h/h369.md) [qāṣê](../../strongs/h/h7097.md); [Pûṭ](../../strongs/h/h6316.md) and [Luḇî](../../strongs/h/h3864.md) were thy [ʿezrâ](../../strongs/h/h5833.md).

<a name="nahum_3_10"></a>Nahum 3:10

Yet was she [gôlâ](../../strongs/h/h1473.md), she [halak](../../strongs/h/h1980.md) into [šᵊḇî](../../strongs/h/h7628.md): her ['owlel](../../strongs/h/h5768.md) also were [rāṭaš](../../strongs/h/h7376.md) at the [ro'sh](../../strongs/h/h7218.md) of all the [ḥûṣ](../../strongs/h/h2351.md): and they [yāḏaḏ](../../strongs/h/h3032.md) [gôrāl](../../strongs/h/h1486.md) for her [kabad](../../strongs/h/h3513.md), and all her [gadowl](../../strongs/h/h1419.md) were [rāṯaq](../../strongs/h/h7576.md) in [zîqôṯ](../../strongs/h/h2131.md).

<a name="nahum_3_11"></a>Nahum 3:11

Thou also shalt be [šāḵar](../../strongs/h/h7937.md): thou shalt be ['alam](../../strongs/h/h5956.md), thou also shalt [bāqaš](../../strongs/h/h1245.md) [māʿôz](../../strongs/h/h4581.md) because of the ['oyeb](../../strongs/h/h341.md).

<a name="nahum_3_12"></a>Nahum 3:12

All thy [miḇṣār](../../strongs/h/h4013.md) shall be like [tĕ'en](../../strongs/h/h8384.md) with the [bikûr](../../strongs/h/h1061.md): if they be [nûaʿ](../../strongs/h/h5128.md), they shall even [naphal](../../strongs/h/h5307.md) into the [peh](../../strongs/h/h6310.md) of the ['akal](../../strongs/h/h398.md).

<a name="nahum_3_13"></a>Nahum 3:13

Behold, thy ['am](../../strongs/h/h5971.md) in the [qereḇ](../../strongs/h/h7130.md) of thee are ['ishshah](../../strongs/h/h802.md): the [sha'ar](../../strongs/h/h8179.md) of thy ['erets](../../strongs/h/h776.md) shall be set [pāṯaḥ](../../strongs/h/h6605.md) [pāṯaḥ](../../strongs/h/h6605.md) unto thine ['oyeb](../../strongs/h/h341.md): the ['esh](../../strongs/h/h784.md) shall ['akal](../../strongs/h/h398.md) thy [bᵊrîaḥ](../../strongs/h/h1280.md).

<a name="nahum_3_14"></a>Nahum 3:14

[šā'aḇ](../../strongs/h/h7579.md) thee [mayim](../../strongs/h/h4325.md) for the [māṣôr](../../strongs/h/h4692.md), [ḥāzaq](../../strongs/h/h2388.md) thy [miḇṣār](../../strongs/h/h4013.md): [bow'](../../strongs/h/h935.md) into [ṭîṭ](../../strongs/h/h2916.md), and [rāmas](../../strongs/h/h7429.md) the [ḥōmer](../../strongs/h/h2563.md), make [ḥāzaq](../../strongs/h/h2388.md) the [malbēn](../../strongs/h/h4404.md).

<a name="nahum_3_15"></a>Nahum 3:15

There shall the ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) thee; the [chereb](../../strongs/h/h2719.md) shall [karath](../../strongs/h/h3772.md) thee, it shall ['akal](../../strongs/h/h398.md) thee like the [yeleq](../../strongs/h/h3218.md): make thyself [kabad](../../strongs/h/h3513.md) as the [yeleq](../../strongs/h/h3218.md), make thyself [kabad](../../strongs/h/h3513.md) as the ['arbê](../../strongs/h/h697.md).

<a name="nahum_3_16"></a>Nahum 3:16

Thou hast [rabah](../../strongs/h/h7235.md) thy [rāḵal](../../strongs/h/h7402.md) above the [kowkab](../../strongs/h/h3556.md) of [shamayim](../../strongs/h/h8064.md): the [yeleq](../../strongs/h/h3218.md) [pāšaṭ](../../strongs/h/h6584.md), and ['uwph](../../strongs/h/h5774.md).

<a name="nahum_3_17"></a>Nahum 3:17

Thy [minnᵊzār](../../strongs/h/h4502.md) are as the ['arbê](../../strongs/h/h697.md), and thy [ṭip̄sar](../../strongs/h/h2951.md) as the [gôḇ](../../strongs/h/h1462.md), which [ḥānâ](../../strongs/h/h2583.md) in the [gᵊḏērâ](../../strongs/h/h1448.md) in the [qārâ](../../strongs/h/h7135.md) [yowm](../../strongs/h/h3117.md), but when the [šemeš](../../strongs/h/h8121.md) [zāraḥ](../../strongs/h/h2224.md) they flee [nāḏaḏ](../../strongs/h/h5074.md), and their [maqowm](../../strongs/h/h4725.md) is not [yada'](../../strongs/h/h3045.md) where they are.

<a name="nahum_3_18"></a>Nahum 3:18

Thy [ra'ah](../../strongs/h/h7462.md) [nûm](../../strongs/h/h5123.md), O [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md): thy ['addiyr](../../strongs/h/h117.md) shall [shakan](../../strongs/h/h7931.md): thy ['am](../../strongs/h/h5971.md) is [pûš](../../strongs/h/h6335.md) upon the [har](../../strongs/h/h2022.md), and no [qāḇaṣ](../../strongs/h/h6908.md) them.

<a name="nahum_3_19"></a>Nahum 3:19

There is no [kêâ](../../strongs/h/h3545.md) of thy [šeḇar](../../strongs/h/h7667.md); thy [makâ](../../strongs/h/h4347.md) is [ḥālâ](../../strongs/h/h2470.md): all that [shama'](../../strongs/h/h8085.md) the [šēmaʿ](../../strongs/h/h8088.md) of thee shall [tāqaʿ](../../strongs/h/h8628.md) the [kaph](../../strongs/h/h3709.md) over thee: for upon whom hath not thy [ra'](../../strongs/h/h7451.md) ['abar](../../strongs/h/h5674.md) [tāmîḏ](../../strongs/h/h8548.md)?

---

[Transliteral Bible](../bible.md)

[Nahum](nahum.md)

[Nahum 2](nahum_2.md)