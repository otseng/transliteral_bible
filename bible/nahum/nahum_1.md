# [Nahum 1](https://www.blueletterbible.org/kjv/nahum/1)

<a name="nahum_1_1"></a>Nahum 1:1

The [maśśā'](../../strongs/h/h4853.md) of [Nînvê](../../strongs/h/h5210.md). The [sēp̄er](../../strongs/h/h5612.md) of the [ḥāzôn](../../strongs/h/h2377.md) of [Naḥûm](../../strongs/h/h5151.md) the ['elqšî](../../strongs/h/h512.md).

<a name="nahum_1_2"></a>Nahum 1:2

['el](../../strongs/h/h410.md) is [qannô'](../../strongs/h/h7072.md), and [Yĕhovah](../../strongs/h/h3068.md) [naqam](../../strongs/h/h5358.md); [Yĕhovah](../../strongs/h/h3068.md) [naqam](../../strongs/h/h5358.md), and is [baʿal](../../strongs/h/h1167.md) [chemah](../../strongs/h/h2534.md); [Yĕhovah](../../strongs/h/h3068.md) will take [naqam](../../strongs/h/h5358.md) on his [tsar](../../strongs/h/h6862.md), and he [nāṭar](../../strongs/h/h5201.md) for his ['oyeb](../../strongs/h/h341.md).

<a name="nahum_1_3"></a>Nahum 1:3

[Yĕhovah](../../strongs/h/h3068.md) is ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and [gadowl](../../strongs/h/h1419.md) in [koach](../../strongs/h/h3581.md), and will not at [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md): [Yĕhovah](../../strongs/h/h3068.md) hath his [derek](../../strongs/h/h1870.md) in the [sûp̄â](../../strongs/h/h5492.md) and in the [śᵊʿārâ](../../strongs/h/h8183.md), and the [ʿānān](../../strongs/h/h6051.md) are the ['āḇāq](../../strongs/h/h80.md) of his [regel](../../strongs/h/h7272.md).

<a name="nahum_1_4"></a>Nahum 1:4

He [gāʿar](../../strongs/h/h1605.md) the [yam](../../strongs/h/h3220.md), and maketh it [ḥāraḇ](../../strongs/h/h2717.md), and [yāḇēš](../../strongs/h/h3001.md) all the [nāhār](../../strongs/h/h5104.md): [Bāšān](../../strongs/h/h1316.md) ['āmal](../../strongs/h/h535.md), and [Karmel](../../strongs/h/h3760.md), and the [peraḥ](../../strongs/h/h6525.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) ['āmal](../../strongs/h/h535.md).

<a name="nahum_1_5"></a>Nahum 1:5

The [har](../../strongs/h/h2022.md) [rāʿaš](../../strongs/h/h7493.md) at him, and the [giḇʿâ](../../strongs/h/h1389.md) [mûḡ](../../strongs/h/h4127.md), and the ['erets](../../strongs/h/h776.md) is [nasa'](../../strongs/h/h5375.md) at his [paniym](../../strongs/h/h6440.md), yea, the [tebel](../../strongs/h/h8398.md), and all that [yashab](../../strongs/h/h3427.md) therein.

<a name="nahum_1_6"></a>Nahum 1:6

Who can ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) his [zaʿam](../../strongs/h/h2195.md)? and who can [quwm](../../strongs/h/h6965.md) in the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md)? his [chemah](../../strongs/h/h2534.md) is [nāṯaḵ](../../strongs/h/h5413.md) like ['esh](../../strongs/h/h784.md), and the [tsuwr](../../strongs/h/h6697.md) are [nāṯaṣ](../../strongs/h/h5422.md) by him.

<a name="nahum_1_7"></a>Nahum 1:7

[Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md), a [māʿôz](../../strongs/h/h4581.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md); and he [yada'](../../strongs/h/h3045.md) them that [chacah](../../strongs/h/h2620.md) in him.

<a name="nahum_1_8"></a>Nahum 1:8

But with an ['abar](../../strongs/h/h5674.md) [šeṭep̄](../../strongs/h/h7858.md) he will ['asah](../../strongs/h/h6213.md) a [kālâ](../../strongs/h/h3617.md) of the [maqowm](../../strongs/h/h4725.md) thereof, and [choshek](../../strongs/h/h2822.md) shall [radaph](../../strongs/h/h7291.md) his ['oyeb](../../strongs/h/h341.md).

<a name="nahum_1_9"></a>Nahum 1:9

What do ye [chashab](../../strongs/h/h2803.md) against [Yĕhovah](../../strongs/h/h3068.md)? he will ['asah](../../strongs/h/h6213.md) an utter [kālâ](../../strongs/h/h3617.md): [tsarah](../../strongs/h/h6869.md) shall not [quwm](../../strongs/h/h6965.md) the second [pa'am](../../strongs/h/h6471.md).

<a name="nahum_1_10"></a>Nahum 1:10

For while they be [sāḇaḵ](../../strongs/h/h5440.md) as [sîr](../../strongs/h/h5518.md), and while they are [sāḇā'](../../strongs/h/h5433.md) as [sōḇe'](../../strongs/h/h5435.md), they shall be ['akal](../../strongs/h/h398.md) as [qaš](../../strongs/h/h7179.md) [mālē'](../../strongs/h/h4392.md) [yāḇēš](../../strongs/h/h3002.md).

<a name="nahum_1_11"></a>Nahum 1:11

There is one [yāṣā'](../../strongs/h/h3318.md) of thee, that [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) against [Yĕhovah](../../strongs/h/h3068.md), a [beliya'al](../../strongs/h/h1100.md) [ya'ats](../../strongs/h/h3289.md).

<a name="nahum_1_12"></a>Nahum 1:12

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Though they be [šālēm](../../strongs/h/h8003.md), and likewise [rab](../../strongs/h/h7227.md), yet thus shall they be [gāzaz](../../strongs/h/h1494.md), when he shall ['abar](../../strongs/h/h5674.md). Though I have [ʿānâ](../../strongs/h/h6031.md) thee, I will [ʿānâ](../../strongs/h/h6031.md) thee no more.

<a name="nahum_1_13"></a>Nahum 1:13

For now will I [shabar](../../strongs/h/h7665.md) his [môṭ](../../strongs/h/h4132.md) from off thee, and will [nathaq](../../strongs/h/h5423.md) thy [mowcer](../../strongs/h/h4147.md) in [nathaq](../../strongs/h/h5423.md).

<a name="nahum_1_14"></a>Nahum 1:14

And [Yĕhovah](../../strongs/h/h3068.md) hath given a [tsavah](../../strongs/h/h6680.md) concerning thee, that no more of thy [shem](../../strongs/h/h8034.md) be [zāraʿ](../../strongs/h/h2232.md): out of the [bayith](../../strongs/h/h1004.md) of thy ['Elohiym](../../strongs/h/h430.md) will I [karath](../../strongs/h/h3772.md) the [pecel](../../strongs/h/h6459.md) and the [massēḵâ](../../strongs/h/h4541.md): I will [śûm](../../strongs/h/h7760.md) thy [qeber](../../strongs/h/h6913.md); for thou art [qālal](../../strongs/h/h7043.md).

<a name="nahum_1_15"></a>Nahum 1:15

Behold upon the [har](../../strongs/h/h2022.md) the [regel](../../strongs/h/h7272.md) of him that bringeth [bāśar](../../strongs/h/h1319.md), that [shama'](../../strongs/h/h8085.md) [shalowm](../../strongs/h/h7965.md)! O [Yehuwdah](../../strongs/h/h3063.md), [ḥāḡaḡ](../../strongs/h/h2287.md) thy solemn [ḥāḡ](../../strongs/h/h2282.md), [shalam](../../strongs/h/h7999.md) thy [neḏer](../../strongs/h/h5088.md): for the [beliya'al](../../strongs/h/h1100.md) shall no more ['abar](../../strongs/h/h5674.md) thee; he is [karath](../../strongs/h/h3772.md).

---

[Transliteral Bible](../bible.md)

[Nahum](nahum.md)

[Nahum 2](nahum_2.md)