# [Nahum 2](https://www.blueletterbible.org/kjv/nahum/2)

<a name="nahum_2_1"></a>Nahum 2:1

He that [puwts](../../strongs/h/h6327.md) is [ʿālâ](../../strongs/h/h5927.md) before thy [paniym](../../strongs/h/h6440.md): [nāṣar](../../strongs/h/h5341.md) the [mᵊṣûrâ](../../strongs/h/h4694.md), [tsaphah](../../strongs/h/h6822.md) the [derek](../../strongs/h/h1870.md), make thy [māṯnayim](../../strongs/h/h4975.md) [ḥāzaq](../../strongs/h/h2388.md), ['amats](../../strongs/h/h553.md) thy [koach](../../strongs/h/h3581.md) [me'od](../../strongs/h/h3966.md).

<a name="nahum_2_2"></a>Nahum 2:2

For [Yĕhovah](../../strongs/h/h3068.md) hath [shuwb](../../strongs/h/h7725.md) the [gā'ôn](../../strongs/h/h1347.md) of [Ya'aqob](../../strongs/h/h3290.md), as the [gā'ôn](../../strongs/h/h1347.md) of [Yisra'el](../../strongs/h/h3478.md): for the [bāqaq](../../strongs/h/h1238.md) have [bāqaq](../../strongs/h/h1238.md) them, and [shachath](../../strongs/h/h7843.md) their [zᵊmôrâ](../../strongs/h/h2156.md).

<a name="nahum_2_3"></a>Nahum 2:3

The [magen](../../strongs/h/h4043.md) of his [gibôr](../../strongs/h/h1368.md) is made ['āḏam](../../strongs/h/h119.md), the [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md) are in [tālaʿ](../../strongs/h/h8529.md): the [reḵeḇ](../../strongs/h/h7393.md) shall be with ['esh](../../strongs/h/h784.md) [paldâ](../../strongs/h/h6393.md) in the [yowm](../../strongs/h/h3117.md) of his [kuwn](../../strongs/h/h3559.md), and the fir [bᵊrôš](../../strongs/h/h1265.md) shall be terribly [rāʿal](../../strongs/h/h7477.md).

<a name="nahum_2_4"></a>Nahum 2:4

The [reḵeḇ](../../strongs/h/h7393.md) shall [halal](../../strongs/h/h1984.md) in the [ḥûṣ](../../strongs/h/h2351.md), they shall [šāqaq](../../strongs/h/h8264.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md): they shall [mar'ê](../../strongs/h/h4758.md) like [lapîḏ](../../strongs/h/h3940.md), they shall [rûṣ](../../strongs/h/h7323.md) like the [baraq](../../strongs/h/h1300.md).

<a name="nahum_2_5"></a>Nahum 2:5

He shall [zakar](../../strongs/h/h2142.md) his ['addiyr](../../strongs/h/h117.md): they shall [kashal](../../strongs/h/h3782.md) in their [hălîḵâ](../../strongs/h/h1979.md); they shall make [māhar](../../strongs/h/h4116.md) to the [ḥômâ](../../strongs/h/h2346.md) thereof, and the [cakak](../../strongs/h/h5526.md) shall be [kuwn](../../strongs/h/h3559.md).

<a name="nahum_2_6"></a>Nahum 2:6

The [sha'ar](../../strongs/h/h8179.md) of the [nāhār](../../strongs/h/h5104.md) shall be [pāṯaḥ](../../strongs/h/h6605.md), and the [heykal](../../strongs/h/h1964.md) shall be [mûḡ](../../strongs/h/h4127.md).

<a name="nahum_2_7"></a>Nahum 2:7

And [nāṣaḇ](../../strongs/h/h5324.md) shall be [gālâ](../../strongs/h/h1540.md), she shall be [ʿālâ](../../strongs/h/h5927.md), and her ['amah](../../strongs/h/h519.md) shall [nāhaḡ](../../strongs/h/h5090.md) her as with the [qowl](../../strongs/h/h6963.md) of [yônâ](../../strongs/h/h3123.md), [tāp̄ap̄](../../strongs/h/h8608.md) upon their [lebab](../../strongs/h/h3824.md). [^1]

<a name="nahum_2_8"></a>Nahum 2:8

But [Nînvê](../../strongs/h/h5210.md) is of [yowm](../../strongs/h/h3117.md) like a [bᵊrēḵâ](../../strongs/h/h1295.md) of [mayim](../../strongs/h/h4325.md): yet they shall [nûs](../../strongs/h/h5127.md). ['amad](../../strongs/h/h5975.md), ['amad](../../strongs/h/h5975.md); but none shall [panah](../../strongs/h/h6437.md).

<a name="nahum_2_9"></a>Nahum 2:9

Take ye the [bāzaz](../../strongs/h/h962.md) of [keceph](../../strongs/h/h3701.md), take the [bāzaz](../../strongs/h/h962.md) of [zāhāḇ](../../strongs/h/h2091.md): for there is none [qāṣê](../../strongs/h/h7097.md) of the [tᵊḵûnâ](../../strongs/h/h8498.md) and [kabowd](../../strongs/h/h3519.md) out of all the [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md).

<a name="nahum_2_10"></a>Nahum 2:10

She is [bûqâ](../../strongs/h/h950.md), and [mᵊḇûqâ](../../strongs/h/h4003.md), and [bālaq](../../strongs/h/h1110.md): and the [leb](../../strongs/h/h3820.md) [māsas](../../strongs/h/h4549.md), and the [bereḵ](../../strongs/h/h1290.md) [pîq](../../strongs/h/h6375.md), and much [ḥalḥālâ](../../strongs/h/h2479.md) is in all [māṯnayim](../../strongs/h/h4975.md), and the [paniym](../../strongs/h/h6440.md) of them all [qāḇaṣ](../../strongs/h/h6908.md) [pā'rûr](../../strongs/h/h6289.md).

<a name="nahum_2_11"></a>Nahum 2:11

Where is the [māʿôn](../../strongs/h/h4583.md) of the ['ariy](../../strongs/h/h738.md), and the [mirʿê](../../strongs/h/h4829.md) of the [kephiyr](../../strongs/h/h3715.md), where the ['ariy](../../strongs/h/h738.md), even the [lāḇî'](../../strongs/h/h3833.md), [halak](../../strongs/h/h1980.md), and the ['ariy](../../strongs/h/h738.md) [gûr](../../strongs/h/h1482.md), and none made them [ḥārēḏ](../../strongs/h/h2729.md)?

<a name="nahum_2_12"></a>Nahum 2:12

The ['ariy](../../strongs/h/h738.md) did [taraph](../../strongs/h/h2963.md) [day](../../strongs/h/h1767.md) for his [gôr](../../strongs/h/h1484.md), and [ḥānaq](../../strongs/h/h2614.md) for his [lāḇî'](../../strongs/h/h3833.md), and [mālā'](../../strongs/h/h4390.md) his [ḥôr](../../strongs/h/h2356.md) with [ṭerep̄](../../strongs/h/h2964.md), and his [mᵊʿônâ](../../strongs/h/h4585.md) with [ṭᵊrēp̄â](../../strongs/h/h2966.md).

<a name="nahum_2_13"></a>Nahum 2:13

Behold, I am against thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and I will [bāʿar](../../strongs/h/h1197.md) her [reḵeḇ](../../strongs/h/h7393.md) in the ['ashan](../../strongs/h/h6227.md), and the [chereb](../../strongs/h/h2719.md) shall ['akal](../../strongs/h/h398.md) thy [kephiyr](../../strongs/h/h3715.md): and I will [karath](../../strongs/h/h3772.md) thy [ṭerep̄](../../strongs/h/h2964.md) from the ['erets](../../strongs/h/h776.md), and the [qowl](../../strongs/h/h6963.md) of thy [mal'ak](../../strongs/h/h4397.md) shall no more be [shama'](../../strongs/h/h8085.md).

---

[Transliteral Bible](../bible.md)

[Nahum](nahum.md)

[Nahum 1](nahum_1.md) - [Nahum 3](nahum_3.md)

---

[^1]: [Nahum 2:7 Commentary](../../commentary/nahum/nahum_2_commentary.md#nahum_2_7)
