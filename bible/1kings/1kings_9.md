# [1Kings 9](https://www.blueletterbible.org/kjv/1kings/9)

<a name="1kings_9_1"></a>1Kings 9:1

And it came to pass, when [Šᵊlōmô](../../strongs/h/h8010.md) had [kalah](../../strongs/h/h3615.md) the [bānâ](../../strongs/h/h1129.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and all [Šᵊlōmô](../../strongs/h/h8010.md) [ḥēšeq](../../strongs/h/h2837.md) which he was [ḥāp̄ēṣ](../../strongs/h/h2654.md) to ['asah](../../strongs/h/h6213.md),

<a name="1kings_9_2"></a>1Kings 9:2

That [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) to [Šᵊlōmô](../../strongs/h/h8010.md) the second time, as he had [ra'ah](../../strongs/h/h7200.md) unto him at [Giḇʿôn](../../strongs/h/h1391.md).

<a name="1kings_9_3"></a>1Kings 9:3

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, I have [shama'](../../strongs/h/h8085.md) thy [tĕphillah](../../strongs/h/h8605.md) and thy [tĕchinnah](../../strongs/h/h8467.md), that thou hast [chanan](../../strongs/h/h2603.md) [paniym](../../strongs/h/h6440.md) me: I have [qadash](../../strongs/h/h6942.md) this [bayith](../../strongs/h/h1004.md), which thou hast [bānâ](../../strongs/h/h1129.md), to [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md) there ['owlam](../../strongs/h/h5769.md); and mine ['ayin](../../strongs/h/h5869.md) and mine [leb](../../strongs/h/h3820.md) shall be there [yowm](../../strongs/h/h3117.md).

<a name="1kings_9_4"></a>1Kings 9:4

And if thou wilt [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) me, as [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md) [halak](../../strongs/h/h1980.md), in [tom](../../strongs/h/h8537.md) of [lebab](../../strongs/h/h3824.md), and in [yōšer](../../strongs/h/h3476.md), to ['asah](../../strongs/h/h6213.md) according to all that I have [tsavah](../../strongs/h/h6680.md) thee, and wilt [shamar](../../strongs/h/h8104.md) my [choq](../../strongs/h/h2706.md) and my [mishpat](../../strongs/h/h4941.md):

<a name="1kings_9_5"></a>1Kings 9:5

Then I will [quwm](../../strongs/h/h6965.md) the [kicce'](../../strongs/h/h3678.md) of thy [mamlāḵâ](../../strongs/h/h4467.md) upon [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md), as I [dabar](../../strongs/h/h1696.md) to [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), There shall not [karath](../../strongs/h/h3772.md) thee an ['iysh](../../strongs/h/h376.md) upon the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_9_6"></a>1Kings 9:6

But if ye shall at [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) me, ye or your [ben](../../strongs/h/h1121.md), and will not [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md) and my [chuqqah](../../strongs/h/h2708.md) which I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you, but [halak](../../strongs/h/h1980.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them:

<a name="1kings_9_7"></a>1Kings 9:7

Then will I [karath](../../strongs/h/h3772.md) [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md) which I have [nathan](../../strongs/h/h5414.md) them; and this [bayith](../../strongs/h/h1004.md), which I have [qadash](../../strongs/h/h6942.md) for my [shem](../../strongs/h/h8034.md), will I [shalach](../../strongs/h/h7971.md) of my [paniym](../../strongs/h/h6440.md); and [Yisra'el](../../strongs/h/h3478.md) shall be a [māšāl](../../strongs/h/h4912.md) and a [šᵊnînâ](../../strongs/h/h8148.md) among all ['am](../../strongs/h/h5971.md):

<a name="1kings_9_8"></a>1Kings 9:8

And at this [bayith](../../strongs/h/h1004.md), which is ['elyown](../../strongs/h/h5945.md), every one that ['abar](../../strongs/h/h5674.md) by it shall be [šāmēm](../../strongs/h/h8074.md), and shall [šāraq](../../strongs/h/h8319.md); and they shall ['āmar](../../strongs/h/h559.md), Why hath [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) thus unto this ['erets](../../strongs/h/h776.md), and to this [bayith](../../strongs/h/h1004.md)?

<a name="1kings_9_9"></a>1Kings 9:9

And they shall ['āmar](../../strongs/h/h559.md), Because they ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), who [yāṣā'](../../strongs/h/h3318.md) their ['ab](../../strongs/h/h1.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and have taken [ḥāzaq](../../strongs/h/h2388.md) upon ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and have [shachah](../../strongs/h/h7812.md) them, and ['abad](../../strongs/h/h5647.md) them: therefore hath [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) upon them all this [ra'](../../strongs/h/h7451.md).

<a name="1kings_9_10"></a>1Kings 9:10

And it came to pass at the [qāṣê](../../strongs/h/h7097.md) of twenty [šānâ](../../strongs/h/h8141.md), when [Šᵊlōmô](../../strongs/h/h8010.md) had [bānâ](../../strongs/h/h1129.md) the two [bayith](../../strongs/h/h1004.md), the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md),

<a name="1kings_9_11"></a>1Kings 9:11

(Now [Ḥîrām](../../strongs/h/h2438.md) the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md) had [nasa'](../../strongs/h/h5375.md) [Šᵊlōmô](../../strongs/h/h8010.md) with ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md) and [bᵊrôš](../../strongs/h/h1265.md) ['ets](../../strongs/h/h6086.md), and with [zāhāḇ](../../strongs/h/h2091.md), according to all his [chephets](../../strongs/h/h2656.md),) that then [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) [Ḥîrām](../../strongs/h/h2438.md) twenty [ʿîr](../../strongs/h/h5892.md) in the ['erets](../../strongs/h/h776.md) of [Gālîl](../../strongs/h/h1551.md).

<a name="1kings_9_12"></a>1Kings 9:12

And [Ḥîrām](../../strongs/h/h2438.md) [yāṣā'](../../strongs/h/h3318.md) from [Ṣōr](../../strongs/h/h6865.md) to [ra'ah](../../strongs/h/h7200.md) the [ʿîr](../../strongs/h/h5892.md) which [Šᵊlōmô](../../strongs/h/h8010.md) had [nathan](../../strongs/h/h5414.md) him; and they [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md) him not.

<a name="1kings_9_13"></a>1Kings 9:13

And he ['āmar](../../strongs/h/h559.md), What [ʿîr](../../strongs/h/h5892.md) are these which thou hast [nathan](../../strongs/h/h5414.md) me, my ['ach](../../strongs/h/h251.md)? And he [qara'](../../strongs/h/h7121.md) them the ['erets](../../strongs/h/h776.md) of [Kāḇûl](../../strongs/h/h3521.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1kings_9_14"></a>1Kings 9:14

And [Ḥîrām](../../strongs/h/h2438.md) [shalach](../../strongs/h/h7971.md) to the [melek](../../strongs/h/h4428.md) sixscore [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="1kings_9_15"></a>1Kings 9:15

And this is the [dabar](../../strongs/h/h1697.md) of the [mas](../../strongs/h/h4522.md) which [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md); for to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and his own [bayith](../../strongs/h/h1004.md), and [Millô'](../../strongs/h/h4407.md), and the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and [Ḥāṣôr](../../strongs/h/h2674.md), and [Mᵊḡidôn](../../strongs/h/h4023.md), and [Gezer](../../strongs/h/h1507.md).

<a name="1kings_9_16"></a>1Kings 9:16

For [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) had [ʿālâ](../../strongs/h/h5927.md), and [lāḵaḏ](../../strongs/h/h3920.md) [Gezer](../../strongs/h/h1507.md), and [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md), and [harag](../../strongs/h/h2026.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md), and [nathan](../../strongs/h/h5414.md) it for a [šillûḥîm](../../strongs/h/h7964.md) unto his [bath](../../strongs/h/h1323.md), [Šᵊlōmô](../../strongs/h/h8010.md) ['ishshah](../../strongs/h/h802.md).

<a name="1kings_9_17"></a>1Kings 9:17

And [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) [Gezer](../../strongs/h/h1507.md), and [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the [taḥtôn](../../strongs/h/h8481.md),

<a name="1kings_9_18"></a>1Kings 9:18

And [BaʿĂlāṯ](../../strongs/h/h1191.md), and [Taḏmōr](../../strongs/h/h8412.md) in the [midbar](../../strongs/h/h4057.md), in the ['erets](../../strongs/h/h776.md),

<a name="1kings_9_19"></a>1Kings 9:19

And all the [ʿîr](../../strongs/h/h5892.md) of [miskᵊnôṯ](../../strongs/h/h4543.md) that [Šᵊlōmô](../../strongs/h/h8010.md) had, and [ʿîr](../../strongs/h/h5892.md) for his [reḵeḇ](../../strongs/h/h7393.md), and [ʿîr](../../strongs/h/h5892.md) for his [pārāš](../../strongs/h/h6571.md), and [ḥēšeq](../../strongs/h/h2837.md) which [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāšaq](../../strongs/h/h2836.md) to [bānâ](../../strongs/h/h1129.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and in [Lᵊḇānôn](../../strongs/h/h3844.md), and in all the ['erets](../../strongs/h/h776.md) of his [memshalah](../../strongs/h/h4475.md).

<a name="1kings_9_20"></a>1Kings 9:20

And all the ['am](../../strongs/h/h5971.md) that were [yāṯar](../../strongs/h/h3498.md) of the ['Ĕmōrî](../../strongs/h/h567.md), [Ḥitî](../../strongs/h/h2850.md), [Pᵊrizzî](../../strongs/h/h6522.md), [Ḥiûî](../../strongs/h/h2340.md), and [Yᵊḇûsî](../../strongs/h/h2983.md), which were not of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="1kings_9_21"></a>1Kings 9:21

Their [ben](../../strongs/h/h1121.md) that were [yāṯar](../../strongs/h/h3498.md) ['aḥar](../../strongs/h/h310.md) them in the ['erets](../../strongs/h/h776.md), whom the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) also were not [yakol](../../strongs/h/h3201.md) utterly to [ḥāram](../../strongs/h/h2763.md), upon those did [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) a [mas](../../strongs/h/h4522.md) of ['abad](../../strongs/h/h5647.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1kings_9_22"></a>1Kings 9:22

But of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) no ['ebed](../../strongs/h/h5650.md): but they were ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and his ['ebed](../../strongs/h/h5650.md), and his [śar](../../strongs/h/h8269.md), and his [šālîš](../../strongs/h/h7991.md), and [śar](../../strongs/h/h8269.md) of his [reḵeḇ](../../strongs/h/h7393.md), and his [pārāš](../../strongs/h/h6571.md).

<a name="1kings_9_23"></a>1Kings 9:23

These were the [śar](../../strongs/h/h8269.md) of the [nāṣaḇ](../../strongs/h/h5324.md) that were over [Šᵊlōmô](../../strongs/h/h8010.md) [mĕla'kah](../../strongs/h/h4399.md), five hundred and fifty, which [radah](../../strongs/h/h7287.md) over the ['am](../../strongs/h/h5971.md) that ['asah](../../strongs/h/h6213.md) in the [mĕla'kah](../../strongs/h/h4399.md).

<a name="1kings_9_24"></a>1Kings 9:24

But [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md) [ʿālâ](../../strongs/h/h5927.md) out of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) unto her [bayith](../../strongs/h/h1004.md) which had [bānâ](../../strongs/h/h1129.md) for her: then did he [bānâ](../../strongs/h/h1129.md) [Millô'](../../strongs/h/h4407.md).

<a name="1kings_9_25"></a>1Kings 9:25

And three [pa'am](../../strongs/h/h6471.md) in a [šānâ](../../strongs/h/h8141.md) did [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md) upon the [mizbeach](../../strongs/h/h4196.md) which he [bānâ](../../strongs/h/h1129.md) unto [Yĕhovah](../../strongs/h/h3068.md), and he [qāṭar](../../strongs/h/h6999.md) upon the [mizbeach](../../strongs/h/h4196.md) that was [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md). So he [shalam](../../strongs/h/h7999.md) the [bayith](../../strongs/h/h1004.md).

<a name="1kings_9_26"></a>1Kings 9:26

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) a navy of ['ŏnî](../../strongs/h/h590.md) in [ʿEṣyôn Geḇer](../../strongs/h/h6100.md), which is beside ['Êlôṯ](../../strongs/h/h359.md), on the [saphah](../../strongs/h/h8193.md) of the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="1kings_9_27"></a>1Kings 9:27

And [Ḥîrām](../../strongs/h/h2438.md) [shalach](../../strongs/h/h7971.md) in the ['ŏnî](../../strongs/h/h590.md) his ['ebed](../../strongs/h/h5650.md), ['enowsh](../../strongs/h/h582.md) ['ŏnîyâ](../../strongs/h/h591.md) that had [yada'](../../strongs/h/h3045.md) of the [yam](../../strongs/h/h3220.md), with the ['ebed](../../strongs/h/h5650.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_9_28"></a>1Kings 9:28

And they [bow'](../../strongs/h/h935.md) to ['Ôp̄îr](../../strongs/h/h211.md), and [laqach](../../strongs/h/h3947.md) from thence [zāhāḇ](../../strongs/h/h2091.md), four hundred and twenty [kikār](../../strongs/h/h3603.md), and [bow'](../../strongs/h/h935.md) it to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 8](1kings_8.md) - [1Kings 10](1kings_10.md)