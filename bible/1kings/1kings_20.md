# [1Kings 20](https://www.blueletterbible.org/kjv/1kings/20)

<a name="1kings_20_1"></a>1Kings 20:1

And [Ben-Hăḏaḏ](../../strongs/h/h1130.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [qāḇaṣ](../../strongs/h/h6908.md) all his [ḥayil](../../strongs/h/h2428.md) [qāḇaṣ](../../strongs/h/h6908.md): and there were thirty and two [melek](../../strongs/h/h4428.md) with him, and [sûs](../../strongs/h/h5483.md), and [reḵeḇ](../../strongs/h/h7393.md): and he [ʿālâ](../../strongs/h/h5927.md) and [ṣûr](../../strongs/h/h6696.md) [Šōmrôn](../../strongs/h/h8111.md), and [lāḥam](../../strongs/h/h3898.md) against it.

<a name="1kings_20_2"></a>1Kings 20:2

And he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) into the [ʿîr](../../strongs/h/h5892.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Ben-Hăḏaḏ](../../strongs/h/h1130.md),

<a name="1kings_20_3"></a>1Kings 20:3

Thy [keceph](../../strongs/h/h3701.md) and thy [zāhāḇ](../../strongs/h/h2091.md) is mine; thy ['ishshah](../../strongs/h/h802.md) also and thy [ben](../../strongs/h/h1121.md), even the [towb](../../strongs/h/h2896.md), are mine.

<a name="1kings_20_4"></a>1Kings 20:4

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), according to thy [dabar](../../strongs/h/h1697.md), I am thine, and all that I have.

<a name="1kings_20_5"></a>1Kings 20:5

And the [mal'ak](../../strongs/h/h4397.md) [shuwb](../../strongs/h/h7725.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Ben-Hăḏaḏ](../../strongs/h/h1130.md), ['āmar](../../strongs/h/h559.md), Although I have [shalach](../../strongs/h/h7971.md) unto thee, ['āmar](../../strongs/h/h559.md), Thou shalt [nathan](../../strongs/h/h5414.md) me thy [keceph](../../strongs/h/h3701.md), and thy [zāhāḇ](../../strongs/h/h2091.md), and thy ['ishshah](../../strongs/h/h802.md), and thy [ben](../../strongs/h/h1121.md);

<a name="1kings_20_6"></a>1Kings 20:6

Yet I will [shalach](../../strongs/h/h7971.md) my ['ebed](../../strongs/h/h5650.md) unto thee [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md), and they shall [ḥāp̄aś](../../strongs/h/h2664.md) thine [bayith](../../strongs/h/h1004.md), and the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md); and it shall be, that whatsoever is [maḥmāḏ](../../strongs/h/h4261.md) in thine ['ayin](../../strongs/h/h5869.md), they shall [śûm](../../strongs/h/h7760.md) it in their [yad](../../strongs/h/h3027.md), and take it [laqach](../../strongs/h/h3947.md).

<a name="1kings_20_7"></a>1Kings 20:7

Then the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [qara'](../../strongs/h/h7121.md) all the [zāqēn](../../strongs/h/h2205.md) of the ['erets](../../strongs/h/h776.md), and ['āmar](../../strongs/h/h559.md), [yada'](../../strongs/h/h3045.md), I pray you, and [ra'ah](../../strongs/h/h7200.md) how this [bāqaš](../../strongs/h/h1245.md) [ra'](../../strongs/h/h7451.md): for he [shalach](../../strongs/h/h7971.md) unto me for my ['ishshah](../../strongs/h/h802.md), and for my [ben](../../strongs/h/h1121.md), and for my [keceph](../../strongs/h/h3701.md), and for my [zāhāḇ](../../strongs/h/h2091.md); and I [mānaʿ](../../strongs/h/h4513.md) him not.

<a name="1kings_20_8"></a>1Kings 20:8

And all the [zāqēn](../../strongs/h/h2205.md) and all the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto him, [shama'](../../strongs/h/h8085.md) not unto him, nor ['āḇâ](../../strongs/h/h14.md).

<a name="1kings_20_9"></a>1Kings 20:9

Wherefore he ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) of [Ben-Hăḏaḏ](../../strongs/h/h1130.md), ['āmar](../../strongs/h/h559.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), All that thou didst [shalach](../../strongs/h/h7971.md) for to thy ['ebed](../../strongs/h/h5650.md) at the [ri'šôn](../../strongs/h/h7223.md) I will ['asah](../../strongs/h/h6213.md): but this [dabar](../../strongs/h/h1697.md) I [yakol](../../strongs/h/h3201.md) not ['asah](../../strongs/h/h6213.md). And the [mal'ak](../../strongs/h/h4397.md) [yālaḵ](../../strongs/h/h3212.md), and [shuwb](../../strongs/h/h7725.md) him [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md).

<a name="1kings_20_10"></a>1Kings 20:10

And [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [shalach](../../strongs/h/h7971.md) unto him, and ['āmar](../../strongs/h/h559.md), The ['Elohiym](../../strongs/h/h430.md) do ['asah](../../strongs/h/h6213.md) unto me, and more also, if the ['aphar](../../strongs/h/h6083.md) of [Šōmrôn](../../strongs/h/h8111.md) shall [sāp̄aq](../../strongs/h/h5606.md) for [šōʿal](../../strongs/h/h8168.md) for all the ['am](../../strongs/h/h5971.md) that [regel](../../strongs/h/h7272.md) me.

<a name="1kings_20_11"></a>1Kings 20:11

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md) him, Let not him that [ḥāḡar](../../strongs/h/h2296.md) [halal](../../strongs/h/h1984.md) himself as he that [pāṯaḥ](../../strongs/h/h6605.md) it.

<a name="1kings_20_12"></a>1Kings 20:12

And it came to pass, when [shama'](../../strongs/h/h8085.md) this [dabar](../../strongs/h/h1697.md), as he was [šāṯâ](../../strongs/h/h8354.md), he and the [melek](../../strongs/h/h4428.md) in the [cukkah](../../strongs/h/h5521.md), that he ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), [śûm](../../strongs/h/h7760.md) yourselves. And they [śûm](../../strongs/h/h7760.md) themselves against the [ʿîr](../../strongs/h/h5892.md).

<a name="1kings_20_13"></a>1Kings 20:13

And, behold, there [nāḡaš](../../strongs/h/h5066.md) a [nāḇî'](../../strongs/h/h5030.md) unto ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Hast thou [ra'ah](../../strongs/h/h7200.md) all this [gadowl](../../strongs/h/h1419.md) [hāmôn](../../strongs/h/h1995.md)? behold, I will [nathan](../../strongs/h/h5414.md) it into thine [yad](../../strongs/h/h3027.md) this [yowm](../../strongs/h/h3117.md); and thou shalt [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_20_14"></a>1Kings 20:14

And ['Aḥ'Āḇ](../../strongs/h/h256.md) ['āmar](../../strongs/h/h559.md), By whom? And he ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Even by the [naʿar](../../strongs/h/h5288.md) of the [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md). Then he ['āmar](../../strongs/h/h559.md), Who shall ['āsar](../../strongs/h/h631.md) the [milḥāmâ](../../strongs/h/h4421.md)? And he ['āmar](../../strongs/h/h559.md), Thou.

<a name="1kings_20_15"></a>1Kings 20:15

Then he [paqad](../../strongs/h/h6485.md) the [naʿar](../../strongs/h/h5288.md) of the [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md), and they were two hundred and thirty two: and ['aḥar](../../strongs/h/h310.md) them he [paqad](../../strongs/h/h6485.md) all the ['am](../../strongs/h/h5971.md), even all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), being seven thousand.

<a name="1kings_20_16"></a>1Kings 20:16

And they [yāṣā'](../../strongs/h/h3318.md) at [ṣōhar](../../strongs/h/h6672.md). But [Ben-Hăḏaḏ](../../strongs/h/h1130.md) was [šāṯâ](../../strongs/h/h8354.md) himself [šikôr](../../strongs/h/h7910.md) in the [cukkah](../../strongs/h/h5521.md), he and the [melek](../../strongs/h/h4428.md), the thirty and two [melek](../../strongs/h/h4428.md) that [ʿāzar](../../strongs/h/h5826.md) him.

<a name="1kings_20_17"></a>1Kings 20:17

And the [naʿar](../../strongs/h/h5288.md) of the [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md) [yāṣā'](../../strongs/h/h3318.md) [ri'šôn](../../strongs/h/h7223.md); and [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [shalach](../../strongs/h/h7971.md), and they [nāḡaḏ](../../strongs/h/h5046.md) him, ['āmar](../../strongs/h/h559.md), There are ['enowsh](../../strongs/h/h582.md) [yāṣā'](../../strongs/h/h3318.md) of [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_20_18"></a>1Kings 20:18

And he ['āmar](../../strongs/h/h559.md), Whether they be [yāṣā'](../../strongs/h/h3318.md) for [shalowm](../../strongs/h/h7965.md), [tāp̄aś](../../strongs/h/h8610.md) them [chay](../../strongs/h/h2416.md); or whether they be [yāṣā'](../../strongs/h/h3318.md) for [milḥāmâ](../../strongs/h/h4421.md), [tāp̄aś](../../strongs/h/h8610.md) them [chay](../../strongs/h/h2416.md).

<a name="1kings_20_19"></a>1Kings 20:19

So these [naʿar](../../strongs/h/h5288.md) of the [śar](../../strongs/h/h8269.md) of the [mᵊḏînâ](../../strongs/h/h4082.md) [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md), and the [ḥayil](../../strongs/h/h2428.md) which ['aḥar](../../strongs/h/h310.md) them.

<a name="1kings_20_20"></a>1Kings 20:20

And they [nakah](../../strongs/h/h5221.md) every ['iysh](../../strongs/h/h376.md) his ['iysh](../../strongs/h/h376.md): and the ['Ărām](../../strongs/h/h758.md) [nûs](../../strongs/h/h5127.md); and [Yisra'el](../../strongs/h/h3478.md) [radaph](../../strongs/h/h7291.md) them: and [Ben-Hăḏaḏ](../../strongs/h/h1130.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [mālaṭ](../../strongs/h/h4422.md) on a [sûs](../../strongs/h/h5483.md) with the [pārāš](../../strongs/h/h6571.md).

<a name="1kings_20_21"></a>1Kings 20:21

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md), and [nakah](../../strongs/h/h5221.md) the [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md), and [nakah](../../strongs/h/h5221.md) the ['Ărām](../../strongs/h/h758.md) with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md).

<a name="1kings_20_22"></a>1Kings 20:22

And the [nāḇî'](../../strongs/h/h5030.md) [nāḡaš](../../strongs/h/h5066.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), [ḥāzaq](../../strongs/h/h2388.md) thyself, and [yada'](../../strongs/h/h3045.md), and [ra'ah](../../strongs/h/h7200.md) what thou ['asah](../../strongs/h/h6213.md): for at the [tᵊšûḇâ](../../strongs/h/h8666.md) of the [šānâ](../../strongs/h/h8141.md) the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) will [ʿālâ](../../strongs/h/h5927.md) against thee.

<a name="1kings_20_23"></a>1Kings 20:23

And the ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) ['āmar](../../strongs/h/h559.md) unto him, Their ['Elohiym](../../strongs/h/h430.md) are ['Elohiym](../../strongs/h/h430.md) of the [har](../../strongs/h/h2022.md); therefore they were [ḥāzaq](../../strongs/h/h2388.md) than we; but let us [lāḥam](../../strongs/h/h3898.md) against them in the [mîšôr](../../strongs/h/h4334.md), and surely we shall be [ḥāzaq](../../strongs/h/h2388.md) than they.

<a name="1kings_20_24"></a>1Kings 20:24

And ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), [cuwr](../../strongs/h/h5493.md) the [melek](../../strongs/h/h4428.md) away, every ['iysh](../../strongs/h/h376.md) out of his [maqowm](../../strongs/h/h4725.md), and [śûm](../../strongs/h/h7760.md) [peḥâ](../../strongs/h/h6346.md) in their rooms:

<a name="1kings_20_25"></a>1Kings 20:25

And [mānâ](../../strongs/h/h4487.md) thee an [ḥayil](../../strongs/h/h2428.md), like the [ḥayil](../../strongs/h/h2428.md) that thou hast [naphal](../../strongs/h/h5307.md), [sûs](../../strongs/h/h5483.md) for [sûs](../../strongs/h/h5483.md), and [reḵeḇ](../../strongs/h/h7393.md) for [reḵeḇ](../../strongs/h/h7393.md): and we will [lāḥam](../../strongs/h/h3898.md) against them in the [mîšôr](../../strongs/h/h4334.md), and surely we shall be [ḥāzaq](../../strongs/h/h2388.md) than they. And he [shama'](../../strongs/h/h8085.md) unto their [qowl](../../strongs/h/h6963.md), and did ['asah](../../strongs/h/h6213.md).

<a name="1kings_20_26"></a>1Kings 20:26

And it came to pass at the [tᵊšûḇâ](../../strongs/h/h8666.md) of the [šānâ](../../strongs/h/h8141.md), that [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [paqad](../../strongs/h/h6485.md) the ['Ărām](../../strongs/h/h758.md), and [ʿālâ](../../strongs/h/h5927.md) to ['Ăp̄Ēq](../../strongs/h/h663.md), to [milḥāmâ](../../strongs/h/h4421.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_20_27"></a>1Kings 20:27

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [paqad](../../strongs/h/h6485.md), and were all [kûl](../../strongs/h/h3557.md), and [yālaḵ](../../strongs/h/h3212.md) [qārā'](../../strongs/h/h7125.md) them: and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md) before them like two little [ḥāśip̄](../../strongs/h/h2835.md) of [ʿēz](../../strongs/h/h5795.md); but the ['Ărām](../../strongs/h/h758.md) [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md).

<a name="1kings_20_28"></a>1Kings 20:28

And there [nāḡaš](../../strongs/h/h5066.md) an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Because the ['Ărām](../../strongs/h/h758.md) have ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) is ['Elohiym](../../strongs/h/h430.md) of the [har](../../strongs/h/h2022.md), but he is not ['Elohiym](../../strongs/h/h430.md) of the [ʿēmeq](../../strongs/h/h6010.md), therefore will I [nathan](../../strongs/h/h5414.md) all this [gadowl](../../strongs/h/h1419.md) [hāmôn](../../strongs/h/h1995.md) into thine [yad](../../strongs/h/h3027.md), and ye shall [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_20_29"></a>1Kings 20:29

And they [ḥānâ](../../strongs/h/h2583.md) one over against the other seven [yowm](../../strongs/h/h3117.md). And so it was, that in the seventh [yowm](../../strongs/h/h3117.md) the [milḥāmâ](../../strongs/h/h4421.md) was [qāraḇ](../../strongs/h/h7126.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md) of the ['Ărām](../../strongs/h/h758.md) an hundred thousand [raḡlî](../../strongs/h/h7273.md) in one [yowm](../../strongs/h/h3117.md).

<a name="1kings_20_30"></a>1Kings 20:30

But the [yāṯar](../../strongs/h/h3498.md) [nûs](../../strongs/h/h5127.md) to ['Ăp̄Ēq](../../strongs/h/h663.md), into the [ʿîr](../../strongs/h/h5892.md); and there a [ḥômâ](../../strongs/h/h2346.md) [naphal](../../strongs/h/h5307.md) upon twenty and seven thousand of the ['iysh](../../strongs/h/h376.md) that were [yāṯar](../../strongs/h/h3498.md). And [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [nûs](../../strongs/h/h5127.md), and [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), into an [ḥeḏer](../../strongs/h/h2315.md) [ḥeḏer](../../strongs/h/h2315.md).

<a name="1kings_20_31"></a>1Kings 20:31

And his ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto him, Behold now, we have [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) are [checed](../../strongs/h/h2617.md) [melek](../../strongs/h/h4428.md): let us, I pray thee, [śûm](../../strongs/h/h7760.md) [śaq](../../strongs/h/h8242.md) on our [māṯnayim](../../strongs/h/h4975.md), and [chebel](../../strongs/h/h2256.md) upon our [ro'sh](../../strongs/h/h7218.md), and [yāṣā'](../../strongs/h/h3318.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md): peradventure he will [ḥāyâ](../../strongs/h/h2421.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="1kings_20_32"></a>1Kings 20:32

So they [ḥāḡar](../../strongs/h/h2296.md) [śaq](../../strongs/h/h8242.md) on their [māṯnayim](../../strongs/h/h4975.md), and put [chebel](../../strongs/h/h2256.md) on their [ro'sh](../../strongs/h/h7218.md), and [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) [Ben-Hăḏaḏ](../../strongs/h/h1130.md) ['āmar](../../strongs/h/h559.md), I pray thee, let me [ḥāyâ](../../strongs/h/h2421.md) [nephesh](../../strongs/h/h5315.md). And he ['āmar](../../strongs/h/h559.md), Is he yet [chay](../../strongs/h/h2416.md)? he is my ['ach](../../strongs/h/h251.md).

<a name="1kings_20_33"></a>1Kings 20:33

Now the ['enowsh](../../strongs/h/h582.md) did [nāḥaš](../../strongs/h/h5172.md) whether from him, and did [māhar](../../strongs/h/h4116.md) [ḥālaṭ](../../strongs/h/h2480.md) it: and they ['āmar](../../strongs/h/h559.md), Thy ['ach](../../strongs/h/h251.md) [Ben-Hăḏaḏ](../../strongs/h/h1130.md). Then he ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) ye, [laqach](../../strongs/h/h3947.md) him. Then [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [yāṣā'](../../strongs/h/h3318.md) to him; and he caused him to [ʿālâ](../../strongs/h/h5927.md) into the [merkāḇâ](../../strongs/h/h4818.md).

<a name="1kings_20_34"></a>1Kings 20:34

And ['āmar](../../strongs/h/h559.md) unto him, The [ʿîr](../../strongs/h/h5892.md), which my ['ab](../../strongs/h/h1.md) [laqach](../../strongs/h/h3947.md) from thy ['ab](../../strongs/h/h1.md), I will [shuwb](../../strongs/h/h7725.md); and thou shalt [śûm](../../strongs/h/h7760.md) [ḥûṣ](../../strongs/h/h2351.md) for thee in [Dammeśeq](../../strongs/h/h1834.md), as my ['ab](../../strongs/h/h1.md) [śûm](../../strongs/h/h7760.md) in [Šōmrôn](../../strongs/h/h8111.md). Then, I will [shalach](../../strongs/h/h7971.md) thee with this [bĕriyth](../../strongs/h/h1285.md). So he [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with him, and sent him [shalach](../../strongs/h/h7971.md).

<a name="1kings_20_35"></a>1Kings 20:35

And a certain ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of the [nāḇî'](../../strongs/h/h5030.md) ['āmar](../../strongs/h/h559.md) unto his [rea'](../../strongs/h/h7453.md) in the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), [nakah](../../strongs/h/h5221.md) me, I pray thee. And the ['iysh](../../strongs/h/h376.md) [mā'ēn](../../strongs/h/h3985.md) to [nakah](../../strongs/h/h5221.md) him.

<a name="1kings_20_36"></a>1Kings 20:36

Then ['āmar](../../strongs/h/h559.md) he unto him, Because thou hast not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), behold, as soon as thou art [halak](../../strongs/h/h1980.md) from me, an ['ariy](../../strongs/h/h738.md) shall [nakah](../../strongs/h/h5221.md) thee. And as soon as he was [yālaḵ](../../strongs/h/h3212.md) from him, an ['ariy](../../strongs/h/h738.md) [māṣā'](../../strongs/h/h4672.md) him, and [nakah](../../strongs/h/h5221.md) him.

<a name="1kings_20_37"></a>1Kings 20:37

Then he [māṣā'](../../strongs/h/h4672.md) ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md), [nakah](../../strongs/h/h5221.md) me, I pray thee. And the ['iysh](../../strongs/h/h376.md) [nakah](../../strongs/h/h5221.md) him, so that in [nakah](../../strongs/h/h5221.md) he [pāṣaʿ](../../strongs/h/h6481.md) him.

<a name="1kings_20_38"></a>1Kings 20:38

So the [nāḇî'](../../strongs/h/h5030.md) [yālaḵ](../../strongs/h/h3212.md), and ['amad](../../strongs/h/h5975.md) for the [melek](../../strongs/h/h4428.md) by the [derek](../../strongs/h/h1870.md), and [ḥāp̄aś](../../strongs/h/h2664.md) himself with ['ăp̄ēr](../../strongs/h/h666.md) upon his ['ayin](../../strongs/h/h5869.md).

<a name="1kings_20_39"></a>1Kings 20:39

And as the [melek](../../strongs/h/h4428.md) ['abar](../../strongs/h/h5674.md), he [ṣāʿaq](../../strongs/h/h6817.md) unto the [melek](../../strongs/h/h4428.md): and he ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) [yāṣā'](../../strongs/h/h3318.md) into the [qereḇ](../../strongs/h/h7130.md) of the [milḥāmâ](../../strongs/h/h4421.md); and, behold, an ['iysh](../../strongs/h/h376.md) [cuwr](../../strongs/h/h5493.md), and [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) unto me, and ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) this ['iysh](../../strongs/h/h376.md): if by any [paqad](../../strongs/h/h6485.md) he be [paqad](../../strongs/h/h6485.md), then shall thy [nephesh](../../strongs/h/h5315.md) be for his [nephesh](../../strongs/h/h5315.md), or else thou shalt [šāqal](../../strongs/h/h8254.md) a [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md).

<a name="1kings_20_40"></a>1Kings 20:40

And as thy ['ebed](../../strongs/h/h5650.md) was ['asah](../../strongs/h/h6213.md) here and there, he was gone. And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto him, So shall thy [mishpat](../../strongs/h/h4941.md) be; thyself hast [ḥāraṣ](../../strongs/h/h2782.md) it.

<a name="1kings_20_41"></a>1Kings 20:41

And he [māhar](../../strongs/h/h4116.md), and took the ['ăp̄ēr](../../strongs/h/h666.md) [cuwr](../../strongs/h/h5493.md) from his ['ayin](../../strongs/h/h5869.md); and the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [nāḵar](../../strongs/h/h5234.md) him that he was of the [nāḇî'](../../strongs/h/h5030.md).

<a name="1kings_20_42"></a>1Kings 20:42

And he ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Because thou hast let [shalach](../../strongs/h/h7971.md) out of thy [yad](../../strongs/h/h3027.md) an ['iysh](../../strongs/h/h376.md) whom I appointed to [ḥērem](../../strongs/h/h2764.md), therefore thy [nephesh](../../strongs/h/h5315.md) shall go for his [nephesh](../../strongs/h/h5315.md), and thy ['am](../../strongs/h/h5971.md) for his ['am](../../strongs/h/h5971.md).

<a name="1kings_20_43"></a>1Kings 20:43

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) to his [bayith](../../strongs/h/h1004.md) [sar](../../strongs/h/h5620.md) and [zāʿēp̄](../../strongs/h/h2198.md), and [bow'](../../strongs/h/h935.md) to [Šōmrôn](../../strongs/h/h8111.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 19](1kings_19.md) - [1Kings 21](1kings_21.md)