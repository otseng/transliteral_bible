# [1Kings 16](https://www.blueletterbible.org/kjv/1kings/16)

<a name="1kings_16_1"></a>1Kings 16:1

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Ḥănānî](../../strongs/h/h2607.md) against [BaʿŠā'](../../strongs/h/h1201.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_16_2"></a>1Kings 16:2

Forasmuch as I [ruwm](../../strongs/h/h7311.md) thee out of the ['aphar](../../strongs/h/h6083.md), and [nathan](../../strongs/h/h5414.md) thee [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md); and thou hast [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), and hast made my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md), to provoke me to [kāʿas](../../strongs/h/h3707.md) with their [chatta'ath](../../strongs/h/h2403.md);

<a name="1kings_16_3"></a>1Kings 16:3

Behold, I will take [bāʿar](../../strongs/h/h1197.md) the ['aḥar](../../strongs/h/h310.md) of [BaʿŠā'](../../strongs/h/h1201.md), and the ['aḥar](../../strongs/h/h310.md) of his [bayith](../../strongs/h/h1004.md); and will [nathan](../../strongs/h/h5414.md) thy [bayith](../../strongs/h/h1004.md) like the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md).

<a name="1kings_16_4"></a>1Kings 16:4

Him that [muwth](../../strongs/h/h4191.md) of [BaʿŠā'](../../strongs/h/h1201.md) in the [ʿîr](../../strongs/h/h5892.md) shall the [keleḇ](../../strongs/h/h3611.md) ['akal](../../strongs/h/h398.md); and him that [muwth](../../strongs/h/h4191.md) of his in the [sadeh](../../strongs/h/h7704.md) shall the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) ['akal](../../strongs/h/h398.md).

<a name="1kings_16_5"></a>1Kings 16:5

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [BaʿŠā'](../../strongs/h/h1201.md), and what he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_16_6"></a>1Kings 16:6

So [BaʿŠā'](../../strongs/h/h1201.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in [Tirṣâ](../../strongs/h/h8656.md): and ['Ēlâ](../../strongs/h/h425.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_16_7"></a>1Kings 16:7

And also by the [yad](../../strongs/h/h3027.md) of the [nāḇî'](../../strongs/h/h5030.md) [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Ḥănānî](../../strongs/h/h2607.md) came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) against [BaʿŠā'](../../strongs/h/h1201.md), and against his [bayith](../../strongs/h/h1004.md), even for all the [ra'](../../strongs/h/h7451.md) that he ['asah](../../strongs/h/h6213.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), in provoking him to [kāʿas](../../strongs/h/h3707.md) with the [ma'aseh](../../strongs/h/h4639.md) of his [yad](../../strongs/h/h3027.md), in being like the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md); and because he [nakah](../../strongs/h/h5221.md) him.

<a name="1kings_16_8"></a>1Kings 16:8

In the twenty [šānâ](../../strongs/h/h8141.md) and sixth [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began ['Ēlâ](../../strongs/h/h425.md) the [ben](../../strongs/h/h1121.md) of [BaʿŠā'](../../strongs/h/h1201.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Tirṣâ](../../strongs/h/h8656.md), two [šānâ](../../strongs/h/h8141.md).

<a name="1kings_16_9"></a>1Kings 16:9

And his ['ebed](../../strongs/h/h5650.md) [Zimrî](../../strongs/h/h2174.md), [śar](../../strongs/h/h8269.md) of [maḥăṣîṯ](../../strongs/h/h4276.md) his [reḵeḇ](../../strongs/h/h7393.md), [qāšar](../../strongs/h/h7194.md) against him, as he was in [Tirṣâ](../../strongs/h/h8656.md), [šāṯâ](../../strongs/h/h8354.md) himself [šikôr](../../strongs/h/h7910.md) in the [bayith](../../strongs/h/h1004.md) of ['Arṣā'](../../strongs/h/h777.md) of his [bayith](../../strongs/h/h1004.md) in [Tirṣâ](../../strongs/h/h8656.md).

<a name="1kings_16_10"></a>1Kings 16:10

And [Zimrî](../../strongs/h/h2174.md) [bow'](../../strongs/h/h935.md) and [nakah](../../strongs/h/h5221.md) him, and [muwth](../../strongs/h/h4191.md) him, in the twenty and seventh [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_16_11"></a>1Kings 16:11

And it came to pass, when he began to [mālaḵ](../../strongs/h/h4427.md), as soon as he [yashab](../../strongs/h/h3427.md) on his [kicce'](../../strongs/h/h3678.md), that he [nakah](../../strongs/h/h5221.md) all the [bayith](../../strongs/h/h1004.md) of [BaʿŠā'](../../strongs/h/h1201.md): he [šā'ar](../../strongs/h/h7604.md) him not one that [šāṯan](../../strongs/h/h8366.md) against a [qîr](../../strongs/h/h7023.md), neither of his [gā'al](../../strongs/h/h1350.md), nor of his [rea'](../../strongs/h/h7453.md).

<a name="1kings_16_12"></a>1Kings 16:12

Thus did [Zimrî](../../strongs/h/h2174.md) [šāmaḏ](../../strongs/h/h8045.md) all the [bayith](../../strongs/h/h1004.md) of [BaʿŠā'](../../strongs/h/h1201.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) against [BaʿŠā'](../../strongs/h/h1201.md) [yad](../../strongs/h/h3027.md) [Yêû'](../../strongs/h/h3058.md) the [nāḇî'](../../strongs/h/h5030.md),

<a name="1kings_16_13"></a>1Kings 16:13

For all the [chatta'ath](../../strongs/h/h2403.md) of [BaʿŠā'](../../strongs/h/h1201.md), and the [chatta'ath](../../strongs/h/h2403.md) of ['Ēlâ](../../strongs/h/h425.md) his [ben](../../strongs/h/h1121.md), by which they [chata'](../../strongs/h/h2398.md), and by which they made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md), in provoking [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) to [kāʿas](../../strongs/h/h3707.md) with their [heḇel](../../strongs/h/h1892.md).

<a name="1kings_16_14"></a>1Kings 16:14

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ēlâ](../../strongs/h/h425.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_16_15"></a>1Kings 16:15

In the twenty [šānâ](../../strongs/h/h8141.md) and seventh [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) did [Zimrî](../../strongs/h/h2174.md) [mālaḵ](../../strongs/h/h4427.md) seven [yowm](../../strongs/h/h3117.md) in [Tirṣâ](../../strongs/h/h8656.md). And the ['am](../../strongs/h/h5971.md) were [ḥānâ](../../strongs/h/h2583.md) against [Gibṯôn](../../strongs/h/h1405.md), which belonged to the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="1kings_16_16"></a>1Kings 16:16

And the ['am](../../strongs/h/h5971.md) that were [ḥānâ](../../strongs/h/h2583.md) [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md), [Zimrî](../../strongs/h/h2174.md) hath [qāšar](../../strongs/h/h7194.md), and hath also [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md): wherefore all [Yisra'el](../../strongs/h/h3478.md) made [ʿĀmrî](../../strongs/h/h6018.md), the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md), [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) that [yowm](../../strongs/h/h3117.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="1kings_16_17"></a>1Kings 16:17

And [ʿĀmrî](../../strongs/h/h6018.md) [ʿālâ](../../strongs/h/h5927.md) from [Gibṯôn](../../strongs/h/h1405.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, and they [ṣûr](../../strongs/h/h6696.md) [Tirṣâ](../../strongs/h/h8656.md).

<a name="1kings_16_18"></a>1Kings 16:18

And it came to pass, when [Zimrî](../../strongs/h/h2174.md) [ra'ah](../../strongs/h/h7200.md) that the [ʿîr](../../strongs/h/h5892.md) was [lāḵaḏ](../../strongs/h/h3920.md), that he [bow'](../../strongs/h/h935.md) into the ['armôn](../../strongs/h/h759.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [śārap̄](../../strongs/h/h8313.md) the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) over him with ['esh](../../strongs/h/h784.md), and [muwth](../../strongs/h/h4191.md),

<a name="1kings_16_19"></a>1Kings 16:19

For his [chatta'ath](../../strongs/h/h2403.md) which he [chata'](../../strongs/h/h2398.md) in ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), in [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), and in his [chatta'ath](../../strongs/h/h2403.md) which he ['asah](../../strongs/h/h6213.md), to make [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="1kings_16_20"></a>1Kings 16:20

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Zimrî](../../strongs/h/h2174.md), and his [qešer](../../strongs/h/h7195.md) that he [qāšar](../../strongs/h/h7194.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_16_21"></a>1Kings 16:21

Then were the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) [chalaq](../../strongs/h/h2505.md) into two [ḥēṣî](../../strongs/h/h2677.md): [ḥēṣî](../../strongs/h/h2677.md) of the ['am](../../strongs/h/h5971.md) followed ['aḥar](../../strongs/h/h310.md) [Tiḇnî](../../strongs/h/h8402.md) the [ben](../../strongs/h/h1121.md) of [Gînaṯ](../../strongs/h/h1527.md), to make him [mālaḵ](../../strongs/h/h4427.md); and [ḥēṣî](../../strongs/h/h2677.md) ['aḥar](../../strongs/h/h310.md) [ʿĀmrî](../../strongs/h/h6018.md).

<a name="1kings_16_22"></a>1Kings 16:22

But the ['am](../../strongs/h/h5971.md) that ['aḥar](../../strongs/h/h310.md) [ʿĀmrî](../../strongs/h/h6018.md) [ḥāzaq](../../strongs/h/h2388.md) against the ['am](../../strongs/h/h5971.md) that ['aḥar](../../strongs/h/h310.md) [Tiḇnî](../../strongs/h/h8402.md) the [ben](../../strongs/h/h1121.md) of [Gînaṯ](../../strongs/h/h1527.md): so [Tiḇnî](../../strongs/h/h8402.md) [muwth](../../strongs/h/h4191.md), and [ʿĀmrî](../../strongs/h/h6018.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="1kings_16_23"></a>1Kings 16:23

In the thirty [šānâ](../../strongs/h/h8141.md) and first [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began [ʿĀmrî](../../strongs/h/h6018.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md), twelve [šānâ](../../strongs/h/h8141.md): six [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Tirṣâ](../../strongs/h/h8656.md).

<a name="1kings_16_24"></a>1Kings 16:24

And he [qānâ](../../strongs/h/h7069.md) the [har](../../strongs/h/h2022.md) [Šōmrôn](../../strongs/h/h8111.md) of [Šemer](../../strongs/h/h8106.md) for two [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and [bānâ](../../strongs/h/h1129.md) on the [har](../../strongs/h/h2022.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) which he [bānâ](../../strongs/h/h1129.md), after the [shem](../../strongs/h/h8034.md) of [Šemer](../../strongs/h/h8106.md), ['adown](../../strongs/h/h113.md) of the [har](../../strongs/h/h2022.md), [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_16_25"></a>1Kings 16:25

But [ʿĀmrî](../../strongs/h/h6018.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and did [ra'a'](../../strongs/h/h7489.md) than all that were [paniym](../../strongs/h/h6440.md) him.

<a name="1kings_16_26"></a>1Kings 16:26

For he [yālaḵ](../../strongs/h/h3212.md) in all the [derek](../../strongs/h/h1870.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), and in his [chatta'ath](../../strongs/h/h2403.md) wherewith he made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md), to provoke [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) to [kāʿas](../../strongs/h/h3707.md) with their [heḇel](../../strongs/h/h1892.md).

<a name="1kings_16_27"></a>1Kings 16:27

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [ʿĀmrî](../../strongs/h/h6018.md) which he ['asah](../../strongs/h/h6213.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md) that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_16_28"></a>1Kings 16:28

So [ʿĀmrî](../../strongs/h/h6018.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in [Šōmrôn](../../strongs/h/h8111.md): and ['Aḥ'Āḇ](../../strongs/h/h256.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_16_29"></a>1Kings 16:29

And in the thirty [šānâ](../../strongs/h/h8141.md) and eighth [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began ['Aḥ'Āḇ](../../strongs/h/h256.md) the [ben](../../strongs/h/h1121.md) of [ʿĀmrî](../../strongs/h/h6018.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md): and ['Aḥ'Āḇ](../../strongs/h/h256.md) the [ben](../../strongs/h/h1121.md) of [ʿĀmrî](../../strongs/h/h6018.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md) twenty and two [šānâ](../../strongs/h/h8141.md).

<a name="1kings_16_30"></a>1Kings 16:30

And ['Aḥ'Āḇ](../../strongs/h/h256.md) the [ben](../../strongs/h/h1121.md) of [ʿĀmrî](../../strongs/h/h6018.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) above all that were [paniym](../../strongs/h/h6440.md) him.

<a name="1kings_16_31"></a>1Kings 16:31

And it came to pass, as if it had been a [qālal](../../strongs/h/h7043.md) for him to [yālaḵ](../../strongs/h/h3212.md) in the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), that he [laqach](../../strongs/h/h3947.md) to ['ishshah](../../strongs/h/h802.md) ['Îzeḇel](../../strongs/h/h348.md) the [bath](../../strongs/h/h1323.md) of ['EṯbaʿAl](../../strongs/h/h856.md) [melek](../../strongs/h/h4428.md) of the [Ṣîḏōnî](../../strongs/h/h6722.md), and [yālaḵ](../../strongs/h/h3212.md) and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md), and [shachah](../../strongs/h/h7812.md) him.

<a name="1kings_16_32"></a>1Kings 16:32

And he [quwm](../../strongs/h/h6965.md) a [mizbeach](../../strongs/h/h4196.md) for [BaʿAl](../../strongs/h/h1168.md) in the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), which he had [bānâ](../../strongs/h/h1129.md) in [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_16_33"></a>1Kings 16:33

And ['Aḥ'Āḇ](../../strongs/h/h256.md) ['asah](../../strongs/h/h6213.md) a ['ăšērâ](../../strongs/h/h842.md); and ['Aḥ'Āḇ](../../strongs/h/h256.md) ['asah](../../strongs/h/h6213.md) more to provoke [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) to [kāʿas](../../strongs/h/h3707.md) than all the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) that were [paniym](../../strongs/h/h6440.md) him.

<a name="1kings_16_34"></a>1Kings 16:34

In his [yowm](../../strongs/h/h3117.md) did [Ḥî'Ēl](../../strongs/h/h2419.md) the [bêṯ hā'ĕlî](../../strongs/h/h1017.md) [bānâ](../../strongs/h/h1129.md) [Yᵊrēḥô](../../strongs/h/h3405.md): he laid the [yacad](../../strongs/h/h3245.md) thereof in ['Ăḇîrām](../../strongs/h/h48.md) his [bᵊḵôr](../../strongs/h/h1060.md), and set [nāṣaḇ](../../strongs/h/h5324.md) the [deleṯ](../../strongs/h/h1817.md) thereof in his [ṣāʿîr](../../strongs/h/h6810.md) son [Śᵊḡûḇ](../../strongs/h/h7687.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 15](1kings_15.md) - [1Kings 17](1kings_17.md)