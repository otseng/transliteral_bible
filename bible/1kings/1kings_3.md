# [1Kings 3](https://www.blueletterbible.org/kjv/1kings/3)

<a name="1kings_3_1"></a>1Kings 3:1

And [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāṯan](../../strongs/h/h2859.md) with [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and [laqach](../../strongs/h/h3947.md) [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md), and [bow'](../../strongs/h/h935.md) her into the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), until he had made a [kalah](../../strongs/h/h3615.md) of [bānâ](../../strongs/h/h1129.md) his own [bayith](../../strongs/h/h1004.md), and the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [cabiyb](../../strongs/h/h5439.md).

<a name="1kings_3_2"></a>1Kings 3:2

Only the ['am](../../strongs/h/h5971.md) [zabach](../../strongs/h/h2076.md) in [bāmâ](../../strongs/h/h1116.md), because there was no [bayith](../../strongs/h/h1004.md) [bānâ](../../strongs/h/h1129.md) unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), until those [yowm](../../strongs/h/h3117.md).

<a name="1kings_3_3"></a>1Kings 3:3

And [Šᵊlōmô](../../strongs/h/h8010.md) ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md), [yālaḵ](../../strongs/h/h3212.md) in the [chuqqah](../../strongs/h/h2708.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): only he [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) in [bāmâ](../../strongs/h/h1116.md).

<a name="1kings_3_4"></a>1Kings 3:4

And the [melek](../../strongs/h/h4428.md) [yālaḵ](../../strongs/h/h3212.md) to [Giḇʿôn](../../strongs/h/h1391.md) to [zabach](../../strongs/h/h2076.md) there; for that was the [gadowl](../../strongs/h/h1419.md) [bāmâ](../../strongs/h/h1116.md): a thousand [ʿōlâ](../../strongs/h/h5930.md) did [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) upon that [mizbeach](../../strongs/h/h4196.md).

<a name="1kings_3_5"></a>1Kings 3:5

In [Giḇʿôn](../../strongs/h/h1391.md) [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) to [Šᵊlōmô](../../strongs/h/h8010.md) in a [ḥălôm](../../strongs/h/h2472.md) by [layil](../../strongs/h/h3915.md): and ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), [sha'al](../../strongs/h/h7592.md) what I shall [nathan](../../strongs/h/h5414.md) thee.

<a name="1kings_3_6"></a>1Kings 3:6

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md), Thou hast ['asah](../../strongs/h/h6213.md) unto thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) [gadowl](../../strongs/h/h1419.md) [checed](../../strongs/h/h2617.md), according as he [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) thee in ['emeth](../../strongs/h/h571.md), and in [tsedaqah](../../strongs/h/h6666.md), and in [yišrâ](../../strongs/h/h3483.md) of [lebab](../../strongs/h/h3824.md) with thee; and thou hast [shamar](../../strongs/h/h8104.md) for him this [gadowl](../../strongs/h/h1419.md) [checed](../../strongs/h/h2617.md), that thou hast [nathan](../../strongs/h/h5414.md) him a [ben](../../strongs/h/h1121.md) to [yashab](../../strongs/h/h3427.md) on his [kicce'](../../strongs/h/h3678.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="1kings_3_7"></a>1Kings 3:7

And now, [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), thou hast made thy ['ebed](../../strongs/h/h5650.md) [mālaḵ](../../strongs/h/h4427.md) instead of [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md): and I am but a [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md): I [yada'](../../strongs/h/h3045.md) not how to [yāṣā'](../../strongs/h/h3318.md) or [bow'](../../strongs/h/h935.md).

<a name="1kings_3_8"></a>1Kings 3:8

And thy ['ebed](../../strongs/h/h5650.md) is in the midst of thy ['am](../../strongs/h/h5971.md) which thou hast [bāḥar](../../strongs/h/h977.md), a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), that cannot be [mānâ](../../strongs/h/h4487.md) nor [sāp̄ar](../../strongs/h/h5608.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="1kings_3_9"></a>1Kings 3:9

[nathan](../../strongs/h/h5414.md) therefore thy ['ebed](../../strongs/h/h5650.md) a [shama'](../../strongs/h/h8085.md) [leb](../../strongs/h/h3820.md) to [shaphat](../../strongs/h/h8199.md) thy ['am](../../strongs/h/h5971.md), that I may [bîn](../../strongs/h/h995.md) between [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md): for who is [yakol](../../strongs/h/h3201.md) to [shaphat](../../strongs/h/h8199.md) this thy so [kāḇēḏ](../../strongs/h/h3515.md) an ['am](../../strongs/h/h5971.md)?

<a name="1kings_3_10"></a>1Kings 3:10

And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) the ['adonay](../../strongs/h/h136.md), that [Šᵊlōmô](../../strongs/h/h8010.md) had [sha'al](../../strongs/h/h7592.md) this [dabar](../../strongs/h/h1697.md).

<a name="1kings_3_11"></a>1Kings 3:11

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him, Because thou hast [sha'al](../../strongs/h/h7592.md) this [dabar](../../strongs/h/h1697.md), and hast not [sha'al](../../strongs/h/h7592.md) for thyself [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md); neither hast [sha'al](../../strongs/h/h7592.md) [ʿōšer](../../strongs/h/h6239.md) for thyself, nor hast [sha'al](../../strongs/h/h7592.md) the [nephesh](../../strongs/h/h5315.md) of thine ['oyeb](../../strongs/h/h341.md); but hast [sha'al](../../strongs/h/h7592.md) for thyself [bîn](../../strongs/h/h995.md) to [shama'](../../strongs/h/h8085.md) [mishpat](../../strongs/h/h4941.md);

<a name="1kings_3_12"></a>1Kings 3:12

Behold, I have ['asah](../../strongs/h/h6213.md) according to thy [dabar](../../strongs/h/h1697.md): lo, I have [nathan](../../strongs/h/h5414.md) thee a [ḥāḵām](../../strongs/h/h2450.md) and a [bîn](../../strongs/h/h995.md) [leb](../../strongs/h/h3820.md); so that there was none like thee [paniym](../../strongs/h/h6440.md) thee, neither ['aḥar](../../strongs/h/h310.md) thee shall any [quwm](../../strongs/h/h6965.md) like unto thee.

<a name="1kings_3_13"></a>1Kings 3:13

And I have also [nathan](../../strongs/h/h5414.md) thee that which thou hast not [sha'al](../../strongs/h/h7592.md), both [ʿōšer](../../strongs/h/h6239.md), and [kabowd](../../strongs/h/h3519.md): so that there shall not be ['iysh](../../strongs/h/h376.md) among the [melek](../../strongs/h/h4428.md) like unto thee all thy [yowm](../../strongs/h/h3117.md).

<a name="1kings_3_14"></a>1Kings 3:14

And if thou wilt [yālaḵ](../../strongs/h/h3212.md) in my [derek](../../strongs/h/h1870.md), to [shamar](../../strongs/h/h8104.md) my [choq](../../strongs/h/h2706.md) and my [mitsvah](../../strongs/h/h4687.md), as thy ['ab](../../strongs/h/h1.md) [Dāviḏ](../../strongs/h/h1732.md) did [halak](../../strongs/h/h1980.md), then I will ['arak](../../strongs/h/h748.md) thy [yowm](../../strongs/h/h3117.md).

<a name="1kings_3_15"></a>1Kings 3:15

And [Šᵊlōmô](../../strongs/h/h8010.md) [yāqaṣ](../../strongs/h/h3364.md); and, behold, it was a [ḥălôm](../../strongs/h/h2472.md). And he [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md), and ['asah](../../strongs/h/h6213.md) [šelem](../../strongs/h/h8002.md), and ['asah](../../strongs/h/h6213.md) a [mištê](../../strongs/h/h4960.md) to all his ['ebed](../../strongs/h/h5650.md).

<a name="1kings_3_16"></a>1Kings 3:16

Then [bow'](../../strongs/h/h935.md) there two ['ishshah](../../strongs/h/h802.md), that were [zānâ](../../strongs/h/h2181.md), unto the [melek](../../strongs/h/h4428.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1kings_3_17"></a>1Kings 3:17

And the one ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) my ['adown](../../strongs/h/h113.md), I and this ['ishshah](../../strongs/h/h802.md) [yashab](../../strongs/h/h3427.md) in one [bayith](../../strongs/h/h1004.md); and I was [yalad](../../strongs/h/h3205.md) with her in the [bayith](../../strongs/h/h1004.md).

<a name="1kings_3_18"></a>1Kings 3:18

And it came to pass the third [yowm](../../strongs/h/h3117.md) after that I was [yalad](../../strongs/h/h3205.md), that this ['ishshah](../../strongs/h/h802.md) was [yalad](../../strongs/h/h3205.md) also: and we were [yaḥaḏ](../../strongs/h/h3162.md); there was no [zûr](../../strongs/h/h2114.md) with us in the [bayith](../../strongs/h/h1004.md), [zûlâ](../../strongs/h/h2108.md) we two in the [bayith](../../strongs/h/h1004.md).

<a name="1kings_3_19"></a>1Kings 3:19

And this ['ishshah](../../strongs/h/h802.md) [ben](../../strongs/h/h1121.md) [muwth](../../strongs/h/h4191.md) in the [layil](../../strongs/h/h3915.md); because she [shakab](../../strongs/h/h7901.md) it.

<a name="1kings_3_20"></a>1Kings 3:20

And she [quwm](../../strongs/h/h6965.md) at midnight [layil](../../strongs/h/h3915.md), and [laqach](../../strongs/h/h3947.md) my [ben](../../strongs/h/h1121.md) from beside me, while thine ['amah](../../strongs/h/h519.md) [yāšēn](../../strongs/h/h3463.md), and [shakab](../../strongs/h/h7901.md) it in her [ḥêq](../../strongs/h/h2436.md), and [shakab](../../strongs/h/h7901.md) her [muwth](../../strongs/h/h4191.md) [ben](../../strongs/h/h1121.md) in my [ḥêq](../../strongs/h/h2436.md).

<a name="1kings_3_21"></a>1Kings 3:21

And when I [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md) to give my [ben](../../strongs/h/h1121.md) [yānaq](../../strongs/h/h3243.md), behold, it was [muwth](../../strongs/h/h4191.md): but when I had [bîn](../../strongs/h/h995.md) it in the [boqer](../../strongs/h/h1242.md), behold, it was not my [ben](../../strongs/h/h1121.md), which I did [yalad](../../strongs/h/h3205.md).

<a name="1kings_3_22"></a>1Kings 3:22

And the ['aḥēr](../../strongs/h/h312.md) ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), Nay; but the [chay](../../strongs/h/h2416.md) is my [ben](../../strongs/h/h1121.md), and the [muwth](../../strongs/h/h4191.md) is thy [ben](../../strongs/h/h1121.md). And this ['āmar](../../strongs/h/h559.md), No; but the [muwth](../../strongs/h/h4191.md) is thy [ben](../../strongs/h/h1121.md), and the [chay](../../strongs/h/h2416.md) is my [ben](../../strongs/h/h1121.md). Thus they [dabar](../../strongs/h/h1696.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="1kings_3_23"></a>1Kings 3:23

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), The one ['āmar](../../strongs/h/h559.md), This is my [ben](../../strongs/h/h1121.md) that [chay](../../strongs/h/h2416.md), and thy [ben](../../strongs/h/h1121.md) is the [muwth](../../strongs/h/h4191.md): and the other ['āmar](../../strongs/h/h559.md), Nay; but thy [ben](../../strongs/h/h1121.md) is the [muwth](../../strongs/h/h4191.md), and my [ben](../../strongs/h/h1121.md) is the [chay](../../strongs/h/h2416.md).

<a name="1kings_3_24"></a>1Kings 3:24

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) me a [chereb](../../strongs/h/h2719.md). And they [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="1kings_3_25"></a>1Kings 3:25

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [gāzar](../../strongs/h/h1504.md) the [chay](../../strongs/h/h2416.md) [yeleḏ](../../strongs/h/h3206.md) in two, and [nathan](../../strongs/h/h5414.md) [ḥēṣî](../../strongs/h/h2677.md) to the one, and [ḥēṣî](../../strongs/h/h2677.md) to the other.

<a name="1kings_3_26"></a>1Kings 3:26

Then ['āmar](../../strongs/h/h559.md) the ['ishshah](../../strongs/h/h802.md) whose the [chay](../../strongs/h/h2416.md) [ben](../../strongs/h/h1121.md) was unto the [melek](../../strongs/h/h4428.md), for her [raḥam](../../strongs/h/h7356.md) [kāmar](../../strongs/h/h3648.md) upon her [ben](../../strongs/h/h1121.md), and she ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) my ['adown](../../strongs/h/h113.md), [nathan](../../strongs/h/h5414.md) her the [chay](../../strongs/h/h2416.md) [yalad](../../strongs/h/h3205.md), and [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md) not it. But the other ['āmar](../../strongs/h/h559.md), Let it be neither mine nor thine, but [gāzar](../../strongs/h/h1504.md) it.

<a name="1kings_3_27"></a>1Kings 3:27

Then the [melek](../../strongs/h/h4428.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) her the [chay](../../strongs/h/h2416.md) [yalad](../../strongs/h/h3205.md), and [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md) not it: she is the ['em](../../strongs/h/h517.md) thereof.

<a name="1kings_3_28"></a>1Kings 3:28

And all [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) of the [mishpat](../../strongs/h/h4941.md) which the [melek](../../strongs/h/h4428.md) had [shaphat](../../strongs/h/h8199.md); and they [yare'](../../strongs/h/h3372.md) the [paniym](../../strongs/h/h6440.md): for they [ra'ah](../../strongs/h/h7200.md) that the [ḥāḵmâ](../../strongs/h/h2451.md) of ['Elohiym](../../strongs/h/h430.md) was in [qereḇ](../../strongs/h/h7130.md), to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 2](1kings_2.md) - [1Kings 4](1kings_4.md)