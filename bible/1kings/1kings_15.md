# [1Kings 15](https://www.blueletterbible.org/kjv/1kings/15)

<a name="1kings_15_1"></a>1Kings 15:1

Now in the eighteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md) [mālaḵ](../../strongs/h/h4427.md) ['Ăḇîyām](../../strongs/h/h38.md) over [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_15_2"></a>1Kings 15:2

Three [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Maʿăḵâ](../../strongs/h/h4601.md), the [bath](../../strongs/h/h1323.md) of ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="1kings_15_3"></a>1Kings 15:3

And he [yālaḵ](../../strongs/h/h3212.md) in all the [chatta'ath](../../strongs/h/h2403.md) of his ['ab](../../strongs/h/h1.md), which he had ['asah](../../strongs/h/h6213.md) [paniym](../../strongs/h/h6440.md) him: and his [lebab](../../strongs/h/h3824.md) was not [šālēm](../../strongs/h/h8003.md) with [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), as the [lebab](../../strongs/h/h3824.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_15_4"></a>1Kings 15:4

Nevertheless for [Dāviḏ](../../strongs/h/h1732.md) sake did [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him a [nîr](../../strongs/h/h5216.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), to [quwm](../../strongs/h/h6965.md) his [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) him, and to ['amad](../../strongs/h/h5975.md) [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="1kings_15_5"></a>1Kings 15:5

Because [Dāviḏ](../../strongs/h/h1732.md) ['asah](../../strongs/h/h6213.md) [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [cuwr](../../strongs/h/h5493.md) not from any thing that he [tsavah](../../strongs/h/h6680.md) him all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md), save only in the [dabar](../../strongs/h/h1697.md) of ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md).

<a name="1kings_15_6"></a>1Kings 15:6

And there was [milḥāmâ](../../strongs/h/h4421.md) between [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) and [YārāḇʿĀm](../../strongs/h/h3379.md) all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="1kings_15_7"></a>1Kings 15:7

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ăḇîyām](../../strongs/h/h38.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)? And there was [milḥāmâ](../../strongs/h/h4421.md) between ['Ăḇîyām](../../strongs/h/h38.md) and [YārāḇʿĀm](../../strongs/h/h3379.md).

<a name="1kings_15_8"></a>1Kings 15:8

And ['Ăḇîyām](../../strongs/h/h38.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Āsā'](../../strongs/h/h609.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_15_9"></a>1Kings 15:9

And in the twentieth [šānâ](../../strongs/h/h8141.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [mālaḵ](../../strongs/h/h4427.md) ['Āsā'](../../strongs/h/h609.md) over [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_15_10"></a>1Kings 15:10

And forty and one [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Maʿăḵâ](../../strongs/h/h4601.md), the [bath](../../strongs/h/h1323.md) of ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="1kings_15_11"></a>1Kings 15:11

And ['Āsā'](../../strongs/h/h609.md) ['asah](../../strongs/h/h6213.md) [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), as did [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_15_12"></a>1Kings 15:12

And he ['abar](../../strongs/h/h5674.md) the [qāḏēš](../../strongs/h/h6945.md) out of the ['erets](../../strongs/h/h776.md), and [cuwr](../../strongs/h/h5493.md) all the [gillûl](../../strongs/h/h1544.md) that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="1kings_15_13"></a>1Kings 15:13

And also [Maʿăḵâ](../../strongs/h/h4601.md) his ['em](../../strongs/h/h517.md), even her he [cuwr](../../strongs/h/h5493.md) from [ḡᵊḇîrâ](../../strongs/h/h1377.md), because she had ['asah](../../strongs/h/h6213.md) a [mip̄leṣeṯ](../../strongs/h/h4656.md) in a ['ăšērâ](../../strongs/h/h842.md); and ['Āsā'](../../strongs/h/h609.md) [karath](../../strongs/h/h3772.md) her [mip̄leṣeṯ](../../strongs/h/h4656.md), and [śārap̄](../../strongs/h/h8313.md) it by the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md).

<a name="1kings_15_14"></a>1Kings 15:14

But the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): nevertheless ['Āsā'](../../strongs/h/h609.md) [lebab](../../strongs/h/h3824.md) was [šālēm](../../strongs/h/h8003.md) with [Yĕhovah](../../strongs/h/h3068.md) all his [yowm](../../strongs/h/h3117.md).

<a name="1kings_15_15"></a>1Kings 15:15

And he [bow'](../../strongs/h/h935.md) in the [qodesh](../../strongs/h/h6944.md) which his ['ab](../../strongs/h/h1.md) had [qodesh](../../strongs/h/h6944.md), and the things which himself had [qodesh](../../strongs/h/h6944.md), into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), [keceph](../../strongs/h/h3701.md), and [zāhāḇ](../../strongs/h/h2091.md), and [kĕliy](../../strongs/h/h3627.md).

<a name="1kings_15_16"></a>1Kings 15:16

And there was [milḥāmâ](../../strongs/h/h4421.md) between ['Āsā'](../../strongs/h/h609.md) and [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) all their [yowm](../../strongs/h/h3117.md).

<a name="1kings_15_17"></a>1Kings 15:17

And [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) against [Yehuwdah](../../strongs/h/h3063.md), and [bānâ](../../strongs/h/h1129.md) [rāmâ](../../strongs/h/h7414.md), that he might not [nathan](../../strongs/h/h5414.md) any to [yāṣā'](../../strongs/h/h3318.md) or [bow'](../../strongs/h/h935.md) to ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_15_18"></a>1Kings 15:18

Then ['Āsā'](../../strongs/h/h609.md) [laqach](../../strongs/h/h3947.md) all the [keceph](../../strongs/h/h3701.md) and the [zāhāḇ](../../strongs/h/h2091.md) that were [yāṯar](../../strongs/h/h3498.md) in the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md): and [melek](../../strongs/h/h4428.md) ['Āsā'](../../strongs/h/h609.md) [shalach](../../strongs/h/h7971.md) them to [Ben-Hăḏaḏ](../../strongs/h/h1130.md), the [ben](../../strongs/h/h1121.md) of [Ṭaḇrimmôn](../../strongs/h/h2886.md), the [ben](../../strongs/h/h1121.md) of [Ḥezyôn](../../strongs/h/h2383.md), [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), that [yashab](../../strongs/h/h3427.md) at [Dammeśeq](../../strongs/h/h1834.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_15_19"></a>1Kings 15:19

There is a [bĕriyth](../../strongs/h/h1285.md) between me and thee, and between my ['ab](../../strongs/h/h1.md) and thy ['ab](../../strongs/h/h1.md): behold, I have [shalach](../../strongs/h/h7971.md) unto thee a [shachad](../../strongs/h/h7810.md) of [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md); [yālaḵ](../../strongs/h/h3212.md) and [pārar](../../strongs/h/h6565.md) thy [bĕriyth](../../strongs/h/h1285.md) with [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that he may [ʿālâ](../../strongs/h/h5927.md) from me.

<a name="1kings_15_20"></a>1Kings 15:20

So [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [shama'](../../strongs/h/h8085.md) unto [melek](../../strongs/h/h4428.md) ['Āsā'](../../strongs/h/h609.md), and [shalach](../../strongs/h/h7971.md) the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) which he had against the [ʿîr](../../strongs/h/h5892.md) of [Yisra'el](../../strongs/h/h3478.md), and [nakah](../../strongs/h/h5221.md) [ʿÎyôn](../../strongs/h/h5859.md), and [Dān](../../strongs/h/h1835.md), and ['Āḇēl Bêṯ-MaʿĂḵâ](../../strongs/h/h62.md), and all [Kinnᵊrôṯ](../../strongs/h/h3672.md), with all the ['erets](../../strongs/h/h776.md) of [Nap̄tālî](../../strongs/h/h5321.md).

<a name="1kings_15_21"></a>1Kings 15:21

And it came to pass, when [BaʿŠā'](../../strongs/h/h1201.md) [shama'](../../strongs/h/h8085.md) thereof, that he [ḥāḏal](../../strongs/h/h2308.md) [bānâ](../../strongs/h/h1129.md) of [rāmâ](../../strongs/h/h7414.md), and [yashab](../../strongs/h/h3427.md) in [Tirṣâ](../../strongs/h/h8656.md).

<a name="1kings_15_22"></a>1Kings 15:22

Then [melek](../../strongs/h/h4428.md) ['Āsā'](../../strongs/h/h609.md) made a [shama'](../../strongs/h/h8085.md) throughout all [Yehuwdah](../../strongs/h/h3063.md); none was [naqiy](../../strongs/h/h5355.md): and they [nasa'](../../strongs/h/h5375.md) away the ['eben](../../strongs/h/h68.md) of [rāmâ](../../strongs/h/h7414.md), and the ['ets](../../strongs/h/h6086.md) thereof, wherewith [BaʿŠā'](../../strongs/h/h1201.md) had [bānâ](../../strongs/h/h1129.md); and [melek](../../strongs/h/h4428.md) ['Āsā'](../../strongs/h/h609.md) [bānâ](../../strongs/h/h1129.md) with them [Geḇaʿ](../../strongs/h/h1387.md) of [Binyāmîn](../../strongs/h/h1144.md), and [Miṣpâ](../../strongs/h/h4709.md).

<a name="1kings_15_23"></a>1Kings 15:23

The [yeṯer](../../strongs/h/h3499.md) of all the [dabar](../../strongs/h/h1697.md) of ['Āsā'](../../strongs/h/h609.md), and all his [gᵊḇûrâ](../../strongs/h/h1369.md), and all that he ['asah](../../strongs/h/h6213.md), and the [ʿîr](../../strongs/h/h5892.md) which he [bānâ](../../strongs/h/h1129.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)? Nevertheless in the [ʿēṯ](../../strongs/h/h6256.md) of his [ziqnâ](../../strongs/h/h2209.md) he was [ḥālâ](../../strongs/h/h2470.md) in his [regel](../../strongs/h/h7272.md).

<a name="1kings_15_24"></a>1Kings 15:24

And ['Āsā'](../../strongs/h/h609.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_15_25"></a>1Kings 15:25

And [Nāḏāḇ](../../strongs/h/h5070.md) the [ben](../../strongs/h/h1121.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in the second [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) two [šānâ](../../strongs/h/h8141.md).

<a name="1kings_15_26"></a>1Kings 15:26

And he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of his ['ab](../../strongs/h/h1.md), and in his [chatta'ath](../../strongs/h/h2403.md) wherewith he made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="1kings_15_27"></a>1Kings 15:27

And [BaʿŠā'](../../strongs/h/h1201.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîyâ](../../strongs/h/h281.md), of the [bayith](../../strongs/h/h1004.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), [qāšar](../../strongs/h/h7194.md) against him; and [BaʿŠā'](../../strongs/h/h1201.md) [nakah](../../strongs/h/h5221.md) him at [Gibṯôn](../../strongs/h/h1405.md), which belonged to the [Pᵊlištî](../../strongs/h/h6430.md); for [Nāḏāḇ](../../strongs/h/h5070.md) and all [Yisra'el](../../strongs/h/h3478.md) laid [ṣûr](../../strongs/h/h6696.md) to [Gibṯôn](../../strongs/h/h1405.md).

<a name="1kings_15_28"></a>1Kings 15:28

Even in the third [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) did [BaʿŠā'](../../strongs/h/h1201.md) [muwth](../../strongs/h/h4191.md) him, and [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_15_29"></a>1Kings 15:29

And it came to pass, when he [mālaḵ](../../strongs/h/h4427.md), that he [nakah](../../strongs/h/h5221.md) all the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md); he [šā'ar](../../strongs/h/h7604.md) not to [YārāḇʿĀm](../../strongs/h/h3379.md) any that [neshamah](../../strongs/h/h5397.md), until he had [šāmaḏ](../../strongs/h/h8045.md) him, according unto the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) ['Ăḥîyâ](../../strongs/h/h281.md) the [Šîlōnî](../../strongs/h/h7888.md):

<a name="1kings_15_30"></a>1Kings 15:30

Because of the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) which he [chata'](../../strongs/h/h2398.md), and which he made [Yisra'el](../../strongs/h/h3478.md) [chata'](../../strongs/h/h2398.md), by his [ka'ac](../../strongs/h/h3708.md) wherewith he provoked [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) to [kāʿas](../../strongs/h/h3707.md).

<a name="1kings_15_31"></a>1Kings 15:31

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Nāḏāḇ](../../strongs/h/h5070.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_15_32"></a>1Kings 15:32

And there was [milḥāmâ](../../strongs/h/h4421.md) between ['Āsā'](../../strongs/h/h609.md) and [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) all their [yowm](../../strongs/h/h3117.md).

<a name="1kings_15_33"></a>1Kings 15:33

In the third [šānâ](../../strongs/h/h8141.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) began [BaʿŠā'](../../strongs/h/h1201.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîyâ](../../strongs/h/h281.md) to [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md) in [Tirṣâ](../../strongs/h/h8656.md), twenty and four [šānâ](../../strongs/h/h8141.md).

<a name="1kings_15_34"></a>1Kings 15:34

And he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), and in his [chatta'ath](../../strongs/h/h2403.md) wherewith he made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 14](1kings_14.md) - [1Kings 16](1kings_16.md)