# [1Kings 4](https://www.blueletterbible.org/kjv/1kings/4)

<a name="1kings_4_1"></a>1Kings 4:1

So [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) was [melek](../../strongs/h/h4428.md) over all [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_4_2"></a>1Kings 4:2

And these were the [śar](../../strongs/h/h8269.md) which he had; [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md),

<a name="1kings_4_3"></a>1Kings 4:3

['Ĕlîḥōrep̄](../../strongs/h/h456.md) and ['Ăḥîyâ](../../strongs/h/h281.md), the [ben](../../strongs/h/h1121.md) of [Šîšā'](../../strongs/h/h7894.md), [sāp̄ar](../../strongs/h/h5608.md); [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîlûḏ](../../strongs/h/h286.md), the [zakar](../../strongs/h/h2142.md).

<a name="1kings_4_4"></a>1Kings 4:4

And [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) was over the [tsaba'](../../strongs/h/h6635.md): and [Ṣāḏôq](../../strongs/h/h6659.md) and ['Eḇyāṯār](../../strongs/h/h54.md) were the [kōhēn](../../strongs/h/h3548.md):

<a name="1kings_4_5"></a>1Kings 4:5

And [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Nāṯān](../../strongs/h/h5416.md) was over the [nāṣaḇ](../../strongs/h/h5324.md): and [Zāḇûḏ](../../strongs/h/h2071.md) the [ben](../../strongs/h/h1121.md) of [Nāṯān](../../strongs/h/h5416.md) was [kōhēn](../../strongs/h/h3548.md), and the [melek](../../strongs/h/h4428.md) [rēʿê](../../strongs/h/h7463.md):

<a name="1kings_4_6"></a>1Kings 4:6

And ['Ăḥîšār](../../strongs/h/h301.md) was over the [bayith](../../strongs/h/h1004.md): and ['Ăḏōnîrām](../../strongs/h/h141.md) the [ben](../../strongs/h/h1121.md) of [ʿAḇdā'](../../strongs/h/h5653.md) was over the [mas](../../strongs/h/h4522.md).

<a name="1kings_4_7"></a>1Kings 4:7

And [Šᵊlōmô](../../strongs/h/h8010.md) had twelve [nāṣaḇ](../../strongs/h/h5324.md) over all [Yisra'el](../../strongs/h/h3478.md), which provided [kûl](../../strongs/h/h3557.md) for the [melek](../../strongs/h/h4428.md) and his [bayith](../../strongs/h/h1004.md): ['echad](../../strongs/h/h259.md) his [ḥōḏeš](../../strongs/h/h2320.md) in a [šānâ](../../strongs/h/h8141.md) [kûl](../../strongs/h/h3557.md).

<a name="1kings_4_8"></a>1Kings 4:8

And these are their [shem](../../strongs/h/h8034.md): [Ben-Ḥûr](../../strongs/h/h1133.md), in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md):

<a name="1kings_4_9"></a>1Kings 4:9

[Ben-Deqer](../../strongs/h/h1128.md) [Deqer](../../strongs/h/h1857.md) , in [Māqaṣ](../../strongs/h/h4739.md), and in [ŠaʿAlḇîm](../../strongs/h/h8169.md), and [Bêṯ Šemeš](../../strongs/h/h1053.md), and ['Ylvn Ḇyṯ Ḥnn](../../strongs/h/h358.md):

<a name="1kings_4_10"></a>1Kings 4:10

[Ben-Ḥeseḏ](../../strongs/h/h1136.md) [Ḥeseḏ](../../strongs/h/h2618.md) , in ['Ărubôṯ](../../strongs/h/h700.md); to him [Śôḵô](../../strongs/h/h7755.md), and all the ['erets](../../strongs/h/h776.md) of [Ḥēp̄er](../../strongs/h/h2660.md):

<a name="1kings_4_11"></a>1Kings 4:11

[Ben-'Ăḇînāḏāḇ](../../strongs/h/h1125.md), in all the [nāp̄â](../../strongs/h/h5299.md) of [Dôr](../../strongs/h/h1756.md); which had [Ṭāp̄Aṯ](../../strongs/h/h2955.md) the [bath](../../strongs/h/h1323.md) of [Šᵊlōmô](../../strongs/h/h8010.md) to ['ishshah](../../strongs/h/h802.md):

<a name="1kings_4_12"></a>1Kings 4:12

[BaʿĂnā'](../../strongs/h/h1195.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîlûḏ](../../strongs/h/h286.md); to him [TaʿNāḵ](../../strongs/h/h8590.md) and [Mᵊḡidôn](../../strongs/h/h4023.md), and all [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md), which is by [Ṣrṯn](../../strongs/h/h6891.md) beneath [YizrᵊʿE'L](../../strongs/h/h3157.md), from [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md) to ['Āḇēl Mᵊḥôlâ](../../strongs/h/h65.md), unto [ʿēḇer](../../strongs/h/h5676.md) [YāqmᵊʿĀm](../../strongs/h/h3361.md):

<a name="1kings_4_13"></a>1Kings 4:13

[Ben-Geḇer](../../strongs/h/h1127.md), in [Gilʿāḏ](../../strongs/h/h1568.md) [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md); to him the [ḥaûâ](../../strongs/h/h2333.md) of [Yā'Îr](../../strongs/h/h2971.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), which are in [Gilʿāḏ](../../strongs/h/h1568.md); to him also the [chebel](../../strongs/h/h2256.md) of ['Argōḇ](../../strongs/h/h709.md), which is in [Bāšān](../../strongs/h/h1316.md), threescore [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md) with [ḥômâ](../../strongs/h/h2346.md) and [nᵊḥšeṯ](../../strongs/h/h5178.md) [bᵊrîaḥ](../../strongs/h/h1280.md):

<a name="1kings_4_14"></a>1Kings 4:14

['Ăḥînāḏāḇ](../../strongs/h/h292.md) the [ben](../../strongs/h/h1121.md) of [ʿIdô](../../strongs/h/h5714.md) had [Maḥănayim](../../strongs/h/h4266.md):

<a name="1kings_4_15"></a>1Kings 4:15

['ĂḥîmaʿAṣ](../../strongs/h/h290.md) was in [Nap̄tālî](../../strongs/h/h5321.md); he also [laqach](../../strongs/h/h3947.md) [bāśmaṯ](../../strongs/h/h1315.md) the [bath](../../strongs/h/h1323.md) of [Šᵊlōmô](../../strongs/h/h8010.md) to ['ishshah](../../strongs/h/h802.md):

<a name="1kings_4_16"></a>1Kings 4:16

[BaʿĂnā'](../../strongs/h/h1195.md) the [ben](../../strongs/h/h1121.md) of [Ḥûšay](../../strongs/h/h2365.md) was in ['Āšēr](../../strongs/h/h836.md) and in [BᵊʿĀlôṯ](../../strongs/h/h1175.md):

<a name="1kings_4_17"></a>1Kings 4:17

[Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of [Pārûaḥ](../../strongs/h/h6515.md), in [Yiśśāśḵār](../../strongs/h/h3485.md):

<a name="1kings_4_18"></a>1Kings 4:18

[Šimʿî](../../strongs/h/h8096.md) the [ben](../../strongs/h/h1121.md) of ['Ēlā'](../../strongs/h/h414.md), in [Binyāmîn](../../strongs/h/h1144.md):

<a name="1kings_4_19"></a>1Kings 4:19

[Geḇer](../../strongs/h/h1398.md) the [ben](../../strongs/h/h1121.md) of ['Ûrî](../../strongs/h/h221.md) was in the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), in the ['erets](../../strongs/h/h776.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), and of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md); and he was the only [nᵊṣîḇ](../../strongs/h/h5333.md) which was in the ['erets](../../strongs/h/h776.md).

<a name="1kings_4_20"></a>1Kings 4:20

[Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md) were [rab](../../strongs/h/h7227.md), as the [ḥôl](../../strongs/h/h2344.md) which is by the [yam](../../strongs/h/h3220.md) in [rōḇ](../../strongs/h/h7230.md), ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and making [śāmēaḥ](../../strongs/h/h8056.md).

<a name="1kings_4_21"></a>1Kings 4:21

And [Šᵊlōmô](../../strongs/h/h8010.md) [mashal](../../strongs/h/h4910.md) over all [mamlāḵâ](../../strongs/h/h4467.md) from the [nāhār](../../strongs/h/h5104.md) unto the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Mitsrayim](../../strongs/h/h4714.md): they [nāḡaš](../../strongs/h/h5066.md) [minchah](../../strongs/h/h4503.md), and ['abad](../../strongs/h/h5647.md) [Šᵊlōmô](../../strongs/h/h8010.md) all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="1kings_4_22"></a>1Kings 4:22

And [Šᵊlōmô](../../strongs/h/h8010.md) [lechem](../../strongs/h/h3899.md) for one [yowm](../../strongs/h/h3117.md) was thirty [kōr](../../strongs/h/h3734.md) of fine [sōleṯ](../../strongs/h/h5560.md), and threescore [kōr](../../strongs/h/h3734.md) of [qemaḥ](../../strongs/h/h7058.md),

<a name="1kings_4_23"></a>1Kings 4:23

Ten [bārî'](../../strongs/h/h1277.md) [bāqār](../../strongs/h/h1241.md), and twenty [bāqār](../../strongs/h/h1241.md) out of the [rᵊʿî](../../strongs/h/h7471.md), and an hundred [tso'n](../../strongs/h/h6629.md), beside ['ayyāl](../../strongs/h/h354.md), and [ṣᵊḇî](../../strongs/h/h6643.md), and [yaḥmûr](../../strongs/h/h3180.md), and ['āḇas](../../strongs/h/h75.md) [barburîm](../../strongs/h/h1257.md).

<a name="1kings_4_24"></a>1Kings 4:24

For he had [radah](../../strongs/h/h7287.md) over all the region on this [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md), from [Tip̄Saḥ](../../strongs/h/h8607.md) even to [ʿAzzâ](../../strongs/h/h5804.md), over all the [melek](../../strongs/h/h4428.md) on this [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md): and he had [shalowm](../../strongs/h/h7965.md) on all ['ebed](../../strongs/h/h5650.md) [ʿēḇer](../../strongs/h/h5676.md) [cabiyb](../../strongs/h/h5439.md) him.

<a name="1kings_4_25"></a>1Kings 4:25

And [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), every ['iysh](../../strongs/h/h376.md) under his [gep̄en](../../strongs/h/h1612.md) and under his [tĕ'en](../../strongs/h/h8384.md), from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), all the [yowm](../../strongs/h/h3117.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_4_26"></a>1Kings 4:26

And [Šᵊlōmô](../../strongs/h/h8010.md) had forty thousand ['urvâ](../../strongs/h/h723.md) of [sûs](../../strongs/h/h5483.md) for his [merkāḇ](../../strongs/h/h4817.md), and twelve thousand [pārāš](../../strongs/h/h6571.md).

<a name="1kings_4_27"></a>1Kings 4:27

And those [nāṣaḇ](../../strongs/h/h5324.md) provided [kûl](../../strongs/h/h3557.md) for [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), and for all that [qārēḇ](../../strongs/h/h7131.md) unto [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [šulḥān](../../strongs/h/h7979.md), every ['iysh](../../strongs/h/h376.md) in his [ḥōḏeš](../../strongs/h/h2320.md): they [ʿāḏar](../../strongs/h/h5737.md) [dabar](../../strongs/h/h1697.md).

<a name="1kings_4_28"></a>1Kings 4:28

[śᵊʿōrâ](../../strongs/h/h8184.md) also and [teḇen](../../strongs/h/h8401.md) for the [sûs](../../strongs/h/h5483.md) and [reḵeš](../../strongs/h/h7409.md) [bow'](../../strongs/h/h935.md) they unto the [maqowm](../../strongs/h/h4725.md) where were, every ['iysh](../../strongs/h/h376.md) according to his [mishpat](../../strongs/h/h4941.md).

<a name="1kings_4_29"></a>1Kings 4:29

And ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāḵmâ](../../strongs/h/h2451.md) and [tāḇûn](../../strongs/h/h8394.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md), and [rōḥaḇ](../../strongs/h/h7341.md) of [leb](../../strongs/h/h3820.md), even as the [ḥôl](../../strongs/h/h2344.md) that is on the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md).

<a name="1kings_4_30"></a>1Kings 4:30

And [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāḵmâ](../../strongs/h/h2451.md) [rabah](../../strongs/h/h7235.md) the [ḥāḵmâ](../../strongs/h/h2451.md) of all the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md), and all the [ḥāḵmâ](../../strongs/h/h2451.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1kings_4_31"></a>1Kings 4:31

For he was [ḥāḵam](../../strongs/h/h2449.md) than all ['āḏām](../../strongs/h/h120.md); than ['Êṯān](../../strongs/h/h387.md) the ['ezrāḥî](../../strongs/h/h250.md), and [Hêmān](../../strongs/h/h1968.md), and [Kalkōl](../../strongs/h/h3633.md), and [Dardaʿ](../../strongs/h/h1862.md), the [ben](../../strongs/h/h1121.md) of [Māḥôl](../../strongs/h/h4235.md): and his [shem](../../strongs/h/h8034.md) was in all [gowy](../../strongs/h/h1471.md) [cabiyb](../../strongs/h/h5439.md).

<a name="1kings_4_32"></a>1Kings 4:32

And he [dabar](../../strongs/h/h1696.md) three thousand [māšāl](../../strongs/h/h4912.md): and his [šîr](../../strongs/h/h7892.md) were a thousand and five.

<a name="1kings_4_33"></a>1Kings 4:33

And he [dabar](../../strongs/h/h1696.md) of ['ets](../../strongs/h/h6086.md), from the cedar ['erez](../../strongs/h/h730.md) that is in [Lᵊḇānôn](../../strongs/h/h3844.md) even unto the ['ēzôḇ](../../strongs/h/h231.md) that [yāṣā'](../../strongs/h/h3318.md) of the [qîr](../../strongs/h/h7023.md): he [dabar](../../strongs/h/h1696.md) also of [bĕhemah](../../strongs/h/h929.md), and of [ʿôp̄](../../strongs/h/h5775.md), and of [remeś](../../strongs/h/h7431.md), and of [dag](../../strongs/h/h1709.md).

<a name="1kings_4_34"></a>1Kings 4:34

And there [bow'](../../strongs/h/h935.md) of all ['am](../../strongs/h/h5971.md) to [shama'](../../strongs/h/h8085.md) the [ḥāḵmâ](../../strongs/h/h2451.md) of [Šᵊlōmô](../../strongs/h/h8010.md), from all [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md), which had [shama'](../../strongs/h/h8085.md) of his [ḥāḵmâ](../../strongs/h/h2451.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 3](1kings_3.md) - [1Kings 5](1kings_5.md)