# [1Kings 7](https://www.blueletterbible.org/kjv/1kings/7)

<a name="1kings_7_1"></a>1Kings 7:1

But [Šᵊlōmô](../../strongs/h/h8010.md) was [bānâ](../../strongs/h/h1129.md) his own [bayith](../../strongs/h/h1004.md) thirteen [šānâ](../../strongs/h/h8141.md), and he [kalah](../../strongs/h/h3615.md) all his [bayith](../../strongs/h/h1004.md).

<a name="1kings_7_2"></a>1Kings 7:2

He [bānâ](../../strongs/h/h1129.md) also the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md) of [Lᵊḇānôn](../../strongs/h/h3844.md); the ['ōreḵ](../../strongs/h/h753.md) thereof was an hundred ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) thereof fifty ['ammâ](../../strongs/h/h520.md), and the [qômâ](../../strongs/h/h6967.md) thereof thirty ['ammâ](../../strongs/h/h520.md), upon four [ṭûr](../../strongs/h/h2905.md) of ['erez](../../strongs/h/h730.md) [ʿammûḏ](../../strongs/h/h5982.md), with ['erez](../../strongs/h/h730.md) [kᵊruṯôṯ](../../strongs/h/h3773.md) upon the [ʿammûḏ](../../strongs/h/h5982.md).

<a name="1kings_7_3"></a>1Kings 7:3

And it was [sāp̄an](../../strongs/h/h5603.md) with ['erez](../../strongs/h/h730.md) [maʿal](../../strongs/h/h4605.md) upon the [tsela'](../../strongs/h/h6763.md), that lay on forty five [ʿammûḏ](../../strongs/h/h5982.md), fifteen in a [ṭûr](../../strongs/h/h2905.md).

<a name="1kings_7_4"></a>1Kings 7:4

And there were [šᵊqup̄îm](../../strongs/h/h8261.md) in three [ṭûr](../../strongs/h/h2905.md), and [meḥĕzâ](../../strongs/h/h4237.md) was against [meḥĕzâ](../../strongs/h/h4237.md) in three [pa'am](../../strongs/h/h6471.md).

<a name="1kings_7_5"></a>1Kings 7:5

And all the [peṯaḥ](../../strongs/h/h6607.md) and [mᵊzûzâ](../../strongs/h/h4201.md) were [rāḇaʿ](../../strongs/h/h7251.md), with the [šeqep̄](../../strongs/h/h8260.md): and [meḥĕzâ](../../strongs/h/h4237.md) was [môl](../../strongs/h/h4136.md) [meḥĕzâ](../../strongs/h/h4237.md) in three [pa'am](../../strongs/h/h6471.md).

<a name="1kings_7_6"></a>1Kings 7:6

And he ['asah](../../strongs/h/h6213.md) a ['ûlām](../../strongs/h/h197.md) of [ʿammûḏ](../../strongs/h/h5982.md); the ['ōreḵ](../../strongs/h/h753.md) thereof was fifty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) thereof thirty ['ammâ](../../strongs/h/h520.md): and the ['ûlām](../../strongs/h/h197.md) was [paniym](../../strongs/h/h6440.md) them: and the other [ʿammûḏ](../../strongs/h/h5982.md) and the thick [ʿāḇ](../../strongs/h/h5646.md) were before them.

<a name="1kings_7_7"></a>1Kings 7:7

Then he ['asah](../../strongs/h/h6213.md) a ['ûlām](../../strongs/h/h197.md) for the [kicce'](../../strongs/h/h3678.md) where he might [shaphat](../../strongs/h/h8199.md), even the ['ûlām](../../strongs/h/h197.md) of [mishpat](../../strongs/h/h4941.md): and it was [sāp̄an](../../strongs/h/h5603.md) with ['erez](../../strongs/h/h730.md) from one side of the [qarqaʿ](../../strongs/h/h7172.md) to the [qarqaʿ](../../strongs/h/h7172.md).

<a name="1kings_7_8"></a>1Kings 7:8

And his [bayith](../../strongs/h/h1004.md) where he [yashab](../../strongs/h/h3427.md) had ['aḥēr](../../strongs/h/h312.md) [ḥāṣēr](../../strongs/h/h2691.md) [bayith](../../strongs/h/h1004.md) the ['ûlām](../../strongs/h/h197.md), which was of the like [ma'aseh](../../strongs/h/h4639.md). [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) also a [bayith](../../strongs/h/h1004.md) for [Parʿô](../../strongs/h/h6547.md) [bath](../../strongs/h/h1323.md), whom he had [laqach](../../strongs/h/h3947.md), like unto this ['ûlām](../../strongs/h/h197.md).

<a name="1kings_7_9"></a>1Kings 7:9

All these were of [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), according to the [midâ](../../strongs/h/h4060.md) of hewed [gāzîṯ](../../strongs/h/h1496.md), [gārar](../../strongs/h/h1641.md) with [mᵊḡērâ](../../strongs/h/h4050.md), [bayith](../../strongs/h/h1004.md) and [ḥûṣ](../../strongs/h/h2351.md), even from the [massaḏ](../../strongs/h/h4527.md) unto the [ṭep̄aḥ](../../strongs/h/h2947.md), and so on the [ḥûṣ](../../strongs/h/h2351.md) toward the [gadowl](../../strongs/h/h1419.md) [ḥāṣēr](../../strongs/h/h2691.md).

<a name="1kings_7_10"></a>1Kings 7:10

And the [yacad](../../strongs/h/h3245.md) was of [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), even [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md), ['eben](../../strongs/h/h68.md) of ten ['ammâ](../../strongs/h/h520.md), and ['eben](../../strongs/h/h68.md) of eight ['ammâ](../../strongs/h/h520.md).

<a name="1kings_7_11"></a>1Kings 7:11

And [maʿal](../../strongs/h/h4605.md) were [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), after the [midâ](../../strongs/h/h4060.md) of [gāzîṯ](../../strongs/h/h1496.md), and ['erez](../../strongs/h/h730.md).

<a name="1kings_7_12"></a>1Kings 7:12

And the [gadowl](../../strongs/h/h1419.md) [ḥāṣēr](../../strongs/h/h2691.md) [cabiyb](../../strongs/h/h5439.md) was with three [ṭûr](../../strongs/h/h2905.md) of hewed [gāzîṯ](../../strongs/h/h1496.md), and a [ṭûr](../../strongs/h/h2905.md) of ['erez](../../strongs/h/h730.md) [kᵊruṯôṯ](../../strongs/h/h3773.md), both for the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the ['ûlām](../../strongs/h/h197.md) of the [bayith](../../strongs/h/h1004.md).

<a name="1kings_7_13"></a>1Kings 7:13

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) [Ḥîrām](../../strongs/h/h2438.md) out of [Ṣōr](../../strongs/h/h6865.md).

<a name="1kings_7_14"></a>1Kings 7:14

He was an ['ishshah](../../strongs/h/h802.md) ['almānâ](../../strongs/h/h490.md) [ben](../../strongs/h/h1121.md) of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), and his ['ab](../../strongs/h/h1.md) was an ['iysh](../../strongs/h/h376.md) of [ṣōrî](../../strongs/h/h6876.md), a [ḥāraš](../../strongs/h/h2790.md) in [nᵊḥšeṯ](../../strongs/h/h5178.md): and he was [mālā'](../../strongs/h/h4390.md) with [ḥāḵmâ](../../strongs/h/h2451.md), and [tāḇûn](../../strongs/h/h8394.md), and [da'ath](../../strongs/h/h1847.md) to ['asah](../../strongs/h/h6213.md) all [mĕla'kah](../../strongs/h/h4399.md) in [nᵊḥšeṯ](../../strongs/h/h5178.md). And he [bow'](../../strongs/h/h935.md) to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), and ['asah](../../strongs/h/h6213.md) all his [mĕla'kah](../../strongs/h/h4399.md).

<a name="1kings_7_15"></a>1Kings 7:15

For he [ṣûr](../../strongs/h/h6696.md) two [ʿammûḏ](../../strongs/h/h5982.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), of eighteen ['ammâ](../../strongs/h/h520.md) [qômâ](../../strongs/h/h6967.md) [ʿammûḏ](../../strongs/h/h5982.md) : and a [ḥûṭ](../../strongs/h/h2339.md) of twelve ['ammâ](../../strongs/h/h520.md) did [cabab](../../strongs/h/h5437.md) either of them [cabab](../../strongs/h/h5437.md).

<a name="1kings_7_16"></a>1Kings 7:16

And he ['asah](../../strongs/h/h6213.md) two [kōṯereṯ](../../strongs/h/h3805.md) of [yāṣaq](../../strongs/h/h3332.md) [nᵊḥšeṯ](../../strongs/h/h5178.md), to [nathan](../../strongs/h/h5414.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md): the [qômâ](../../strongs/h/h6967.md) of the one [kōṯereṯ](../../strongs/h/h3805.md) was five ['ammâ](../../strongs/h/h520.md), and the [qômâ](../../strongs/h/h6967.md) of the other [kōṯereṯ](../../strongs/h/h3805.md) was five ['ammâ](../../strongs/h/h520.md):

<a name="1kings_7_17"></a>1Kings 7:17

And [śāḇāḵ](../../strongs/h/h7638.md) of [śᵊḇāḵâ](../../strongs/h/h7639.md) [ma'aseh](../../strongs/h/h4639.md), and [gāḏil](../../strongs/h/h1434.md) of [šaršᵊrâ](../../strongs/h/h8333.md) [ma'aseh](../../strongs/h/h4639.md), for the [kōṯereṯ](../../strongs/h/h3805.md) which were upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md); seven for the one [kōṯereṯ](../../strongs/h/h3805.md), and seven for the other [kōṯereṯ](../../strongs/h/h3805.md).

<a name="1kings_7_18"></a>1Kings 7:18

And he ['asah](../../strongs/h/h6213.md) the [ʿammûḏ](../../strongs/h/h5982.md), and two [ṭûr](../../strongs/h/h2905.md) [cabiyb](../../strongs/h/h5439.md) upon the one [śᵊḇāḵâ](../../strongs/h/h7639.md), to [kāsâ](../../strongs/h/h3680.md) the [kōṯereṯ](../../strongs/h/h3805.md) that were upon the [ro'sh](../../strongs/h/h7218.md), with [rimmôn](../../strongs/h/h7416.md): and so ['asah](../../strongs/h/h6213.md) he for the other [kōṯereṯ](../../strongs/h/h3805.md).

<a name="1kings_7_19"></a>1Kings 7:19

And the [kōṯereṯ](../../strongs/h/h3805.md) that were upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md) were of [šûšan](../../strongs/h/h7799.md) [ma'aseh](../../strongs/h/h4639.md) in the ['ûlām](../../strongs/h/h197.md), four ['ammâ](../../strongs/h/h520.md).

<a name="1kings_7_20"></a>1Kings 7:20

And the [kōṯereṯ](../../strongs/h/h3805.md) upon the two [ʿammûḏ](../../strongs/h/h5982.md) also [maʿal](../../strongs/h/h4605.md), over [ʿummâ](../../strongs/h/h5980.md) the [beten](../../strongs/h/h990.md) which was [ʿēḇer](../../strongs/h/h5676.md) the [śᵊḇāḵâ](../../strongs/h/h7639.md): and the [rimmôn](../../strongs/h/h7416.md) were two hundred in [ṭûr](../../strongs/h/h2905.md) [cabiyb](../../strongs/h/h5439.md) upon the other [kōṯereṯ](../../strongs/h/h3805.md).

<a name="1kings_7_21"></a>1Kings 7:21

And he [quwm](../../strongs/h/h6965.md) the [ʿammûḏ](../../strongs/h/h5982.md) in the ['ûlām](../../strongs/h/h197.md) of the [heykal](../../strongs/h/h1964.md): and he [quwm](../../strongs/h/h6965.md) the [yᵊmānî](../../strongs/h/h3233.md) [ʿammûḏ](../../strongs/h/h5982.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) thereof [Yāḵîn](../../strongs/h/h3199.md): and he [quwm](../../strongs/h/h6965.md) the [śᵊmā'lî](../../strongs/h/h8042.md) [ʿammûḏ](../../strongs/h/h5982.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) thereof [BōʿAz](../../strongs/h/h1162.md).

<a name="1kings_7_22"></a>1Kings 7:22

And upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md) was [šûšan](../../strongs/h/h7799.md) [ma'aseh](../../strongs/h/h4639.md): so was the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿammûḏ](../../strongs/h/h5982.md) [tamam](../../strongs/h/h8552.md).

<a name="1kings_7_23"></a>1Kings 7:23

And he ['asah](../../strongs/h/h6213.md) a [yāṣaq](../../strongs/h/h3332.md) [yam](../../strongs/h/h3220.md), ten ['ammâ](../../strongs/h/h520.md) from the one [saphah](../../strongs/h/h8193.md) to the [saphah](../../strongs/h/h8193.md): it was [ʿāḡōl](../../strongs/h/h5696.md) all [cabiyb](../../strongs/h/h5439.md), and his [qômâ](../../strongs/h/h6967.md) was five ['ammâ](../../strongs/h/h520.md): and a [qāv](../../strongs/h/h6957.md) [qevê](../../strongs/h/h6961.md) of thirty ['ammâ](../../strongs/h/h520.md) did [cabab](../../strongs/h/h5437.md) it [cabiyb](../../strongs/h/h5439.md).

<a name="1kings_7_24"></a>1Kings 7:24

And under the [saphah](../../strongs/h/h8193.md) of it [cabiyb](../../strongs/h/h5439.md) there were [pᵊqāʿîm](../../strongs/h/h6497.md) [cabab](../../strongs/h/h5437.md) it, ten in an ['ammâ](../../strongs/h/h520.md), [naqaph](../../strongs/h/h5362.md) the [yam](../../strongs/h/h3220.md) [cabiyb](../../strongs/h/h5439.md): the [pᵊqāʿîm](../../strongs/h/h6497.md) were [yᵊṣuqâ](../../strongs/h/h3333.md) in two [ṭûr](../../strongs/h/h2905.md), when it was [yāṣaq](../../strongs/h/h3332.md).

<a name="1kings_7_25"></a>1Kings 7:25

It ['amad](../../strongs/h/h5975.md) upon twelve [bāqār](../../strongs/h/h1241.md), three [panah](../../strongs/h/h6437.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and three [panah](../../strongs/h/h6437.md) toward the [yam](../../strongs/h/h3220.md), and three [panah](../../strongs/h/h6437.md) toward the [neḡeḇ](../../strongs/h/h5045.md), and three [panah](../../strongs/h/h6437.md) toward the [mizrach](../../strongs/h/h4217.md): and the [yam](../../strongs/h/h3220.md) was set [maʿal](../../strongs/h/h4605.md) upon them, and all their hinder ['āḥôr](../../strongs/h/h268.md) were [bayith](../../strongs/h/h1004.md).

<a name="1kings_7_26"></a>1Kings 7:26

And it was an hand [ṭep̄aḥ](../../strongs/h/h2947.md) [ʿăḇî](../../strongs/h/h5672.md), and the [saphah](../../strongs/h/h8193.md) thereof was [ma'aseh](../../strongs/h/h4639.md) like the [saphah](../../strongs/h/h8193.md) of a [kowc](../../strongs/h/h3563.md), with [peraḥ](../../strongs/h/h6525.md) of [šûšan](../../strongs/h/h7799.md): it [kûl](../../strongs/h/h3557.md) two thousand [baṯ](../../strongs/h/h1324.md).

<a name="1kings_7_27"></a>1Kings 7:27

And he ['asah](../../strongs/h/h6213.md) ten [mᵊḵônâ](../../strongs/h/h4350.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md); four ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md) of one [mᵊḵônâ](../../strongs/h/h4350.md), and four ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and three ['ammâ](../../strongs/h/h520.md) the [qômâ](../../strongs/h/h6967.md) of it.

<a name="1kings_7_28"></a>1Kings 7:28

And the [ma'aseh](../../strongs/h/h4639.md) of the [mᵊḵônâ](../../strongs/h/h4350.md) was on this manner: they had [misgereṯ](../../strongs/h/h4526.md), and the [misgereṯ](../../strongs/h/h4526.md) were between the [šālāḇ](../../strongs/h/h7948.md):

<a name="1kings_7_29"></a>1Kings 7:29

And on the [misgereṯ](../../strongs/h/h4526.md) that were between the [šālāḇ](../../strongs/h/h7948.md) were ['ariy](../../strongs/h/h738.md), [bāqār](../../strongs/h/h1241.md), and [kĕruwb](../../strongs/h/h3742.md): and upon the [šālāḇ](../../strongs/h/h7948.md) there was a [kēn](../../strongs/h/h3653.md) [maʿal](../../strongs/h/h4605.md): and beneath the ['ariy](../../strongs/h/h738.md) and [bāqār](../../strongs/h/h1241.md) were certain [lōyâ](../../strongs/h/h3914.md) made of [môrāḏ](../../strongs/h/h4174.md) [ma'aseh](../../strongs/h/h4639.md).

<a name="1kings_7_30"></a>1Kings 7:30

And every [mᵊḵônâ](../../strongs/h/h4350.md) had four [nᵊḥšeṯ](../../strongs/h/h5178.md) ['ôp̄ān](../../strongs/h/h212.md), and [seren](../../strongs/h/h5633.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md): and the four [pa'am](../../strongs/h/h6471.md) thereof had [kāṯēp̄](../../strongs/h/h3802.md): under the [kîyôr](../../strongs/h/h3595.md) were [kāṯēp̄](../../strongs/h/h3802.md) [yāṣaq](../../strongs/h/h3332.md), at the [ʿēḇer](../../strongs/h/h5676.md) of ['iysh](../../strongs/h/h376.md) [lōyâ](../../strongs/h/h3914.md).

<a name="1kings_7_31"></a>1Kings 7:31

And the [peh](../../strongs/h/h6310.md) of it [bayith](../../strongs/h/h1004.md) the [kōṯereṯ](../../strongs/h/h3805.md) and [maʿal](../../strongs/h/h4605.md) was an ['ammâ](../../strongs/h/h520.md): but the [peh](../../strongs/h/h6310.md) thereof was [ʿāḡōl](../../strongs/h/h5696.md) after the [ma'aseh](../../strongs/h/h4639.md) of the [kēn](../../strongs/h/h3653.md), an ['ammâ](../../strongs/h/h520.md) and an [ḥēṣî](../../strongs/h/h2677.md): and also upon the [peh](../../strongs/h/h6310.md) of it were [miqlaʿaṯ](../../strongs/h/h4734.md) with their [misgereṯ](../../strongs/h/h4526.md), [rāḇaʿ](../../strongs/h/h7251.md), not [ʿāḡōl](../../strongs/h/h5696.md).

<a name="1kings_7_32"></a>1Kings 7:32

And under the [misgereṯ](../../strongs/h/h4526.md) were four ['ôp̄ān](../../strongs/h/h212.md); and the [yad](../../strongs/h/h3027.md) of the ['ôp̄ān](../../strongs/h/h212.md) were joined to the [mᵊḵônâ](../../strongs/h/h4350.md): and the [qômâ](../../strongs/h/h6967.md) of a ['ôp̄ān](../../strongs/h/h212.md) was an ['ammâ](../../strongs/h/h520.md) and [ḥēṣî](../../strongs/h/h2677.md) an ['ammâ](../../strongs/h/h520.md).

<a name="1kings_7_33"></a>1Kings 7:33

And the [ma'aseh](../../strongs/h/h4639.md) of the ['ôp̄ān](../../strongs/h/h212.md) was like the [ma'aseh](../../strongs/h/h4639.md) of a [merkāḇâ](../../strongs/h/h4818.md) ['ôp̄ān](../../strongs/h/h212.md): their [yad](../../strongs/h/h3027.md), and their [gaḇ](../../strongs/h/h1354.md), and their [ḥiššuq](../../strongs/h/h2839.md), and their [ḥiššur](../../strongs/h/h2840.md), were all [yāṣaq](../../strongs/h/h3332.md).

<a name="1kings_7_34"></a>1Kings 7:34

And there were four [kāṯēp̄](../../strongs/h/h3802.md) to the four [pinnâ](../../strongs/h/h6438.md) of one [mᵊḵônâ](../../strongs/h/h4350.md): and the [kāṯēp̄](../../strongs/h/h3802.md) were of the very [mᵊḵônâ](../../strongs/h/h4350.md) itself.

<a name="1kings_7_35"></a>1Kings 7:35

And in the [ro'sh](../../strongs/h/h7218.md) of the [mᵊḵônâ](../../strongs/h/h4350.md) was there a [ʿāḡōl](../../strongs/h/h5696.md) [cabiyb](../../strongs/h/h5439.md) of [ḥēṣî](../../strongs/h/h2677.md) an ['ammâ](../../strongs/h/h520.md) [qômâ](../../strongs/h/h6967.md): and on the [ro'sh](../../strongs/h/h7218.md) of the [mᵊḵônâ](../../strongs/h/h4350.md) the [yad](../../strongs/h/h3027.md) thereof and the [misgereṯ](../../strongs/h/h4526.md) thereof were of the same.

<a name="1kings_7_36"></a>1Kings 7:36

For on the [lûaḥ](../../strongs/h/h3871.md) of the [yad](../../strongs/h/h3027.md) thereof, and on the [misgereṯ](../../strongs/h/h4526.md) thereof, he [pāṯaḥ](../../strongs/h/h6605.md) [kĕruwb](../../strongs/h/h3742.md), ['ariy](../../strongs/h/h738.md), and [timmōrâ](../../strongs/h/h8561.md), according to the [maʿar](../../strongs/h/h4626.md) of every ['iysh](../../strongs/h/h376.md), and [lōyâ](../../strongs/h/h3914.md) [cabiyb](../../strongs/h/h5439.md).

<a name="1kings_7_37"></a>1Kings 7:37

After this manner he ['asah](../../strongs/h/h6213.md) the ten [mᵊḵônâ](../../strongs/h/h4350.md): all of them had one [mûṣāq](../../strongs/h/h4165.md), one [midâ](../../strongs/h/h4060.md), and one [qeṣeḇ](../../strongs/h/h7095.md).

<a name="1kings_7_38"></a>1Kings 7:38

Then ['asah](../../strongs/h/h6213.md) he ten [kîyôr](../../strongs/h/h3595.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md): one [kîyôr](../../strongs/h/h3595.md) [kûl](../../strongs/h/h3557.md) forty [baṯ](../../strongs/h/h1324.md): and every [kîyôr](../../strongs/h/h3595.md) was four ['ammâ](../../strongs/h/h520.md): and upon every one of the ten [mᵊḵônâ](../../strongs/h/h4350.md) one [kîyôr](../../strongs/h/h3595.md).

<a name="1kings_7_39"></a>1Kings 7:39

And he [nathan](../../strongs/h/h5414.md) five [mᵊḵônâ](../../strongs/h/h4350.md) on the [yamiyn](../../strongs/h/h3225.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md), and five on the [śᵊmō'l](../../strongs/h/h8040.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md): and he [nathan](../../strongs/h/h5414.md) the [yam](../../strongs/h/h3220.md) on the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md) [qeḏem](../../strongs/h/h6924.md) over [môl](../../strongs/h/h4136.md) the [neḡeḇ](../../strongs/h/h5045.md).

<a name="1kings_7_40"></a>1Kings 7:40

And [Ḥîrām](../../strongs/h/h2438.md) ['asah](../../strongs/h/h6213.md) the [kîyôr](../../strongs/h/h3595.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mizrāq](../../strongs/h/h4219.md). So [Ḥîrām](../../strongs/h/h2438.md) [kalah](../../strongs/h/h3615.md) an end of ['asah](../../strongs/h/h6213.md) all the [mĕla'kah](../../strongs/h/h4399.md) that he ['asah](../../strongs/h/h6213.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="1kings_7_41"></a>1Kings 7:41

The two [ʿammûḏ](../../strongs/h/h5982.md), and the two [gullâ](../../strongs/h/h1543.md) of the [kōṯereṯ](../../strongs/h/h3805.md) that were on the [ro'sh](../../strongs/h/h7218.md) of the two [ʿammûḏ](../../strongs/h/h5982.md); and the two [śᵊḇāḵâ](../../strongs/h/h7639.md), to [kāsâ](../../strongs/h/h3680.md) the two [gullâ](../../strongs/h/h1543.md) of the [kōṯereṯ](../../strongs/h/h3805.md) which were upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md);

<a name="1kings_7_42"></a>1Kings 7:42

And four hundred [rimmôn](../../strongs/h/h7416.md) for the two [śᵊḇāḵâ](../../strongs/h/h7639.md), even two [ṭûr](../../strongs/h/h2905.md) of [rimmôn](../../strongs/h/h7416.md) for one [śᵊḇāḵâ](../../strongs/h/h7639.md), to [kāsâ](../../strongs/h/h3680.md) the two [gullâ](../../strongs/h/h1543.md) of the [kōṯereṯ](../../strongs/h/h3805.md) that were [paniym](../../strongs/h/h6440.md) the [ʿammûḏ](../../strongs/h/h5982.md);

<a name="1kings_7_43"></a>1Kings 7:43

And the ten [mᵊḵônâ](../../strongs/h/h4350.md), and ten [kîyôr](../../strongs/h/h3595.md) on the [mᵊḵônâ](../../strongs/h/h4350.md);

<a name="1kings_7_44"></a>1Kings 7:44

And one [yam](../../strongs/h/h3220.md), and twelve [bāqār](../../strongs/h/h1241.md) under the [yam](../../strongs/h/h3220.md);

<a name="1kings_7_45"></a>1Kings 7:45

And the [sîr](../../strongs/h/h5518.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mizrāq](../../strongs/h/h4219.md): and all these [kĕliy](../../strongs/h/h3627.md), which [Ḥîrām](../../strongs/h/h2438.md) ['asah](../../strongs/h/h6213.md) to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), were of [môrāṭ](../../strongs/h/h4178.md) [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="1kings_7_46"></a>1Kings 7:46

In the [kikār](../../strongs/h/h3603.md) of [Yardēn](../../strongs/h/h3383.md) did the [melek](../../strongs/h/h4428.md) [yāṣaq](../../strongs/h/h3332.md) them, in the [maʿăḇê](../../strongs/h/h4568.md) ['ăḏāmâ](../../strongs/h/h127.md) between [Sukôṯ](../../strongs/h/h5523.md) and [Ṣrṯn](../../strongs/h/h6891.md).

<a name="1kings_7_47"></a>1Kings 7:47

And [Šᵊlōmô](../../strongs/h/h8010.md) [yānaḥ](../../strongs/h/h3240.md) all the [kĕliy](../../strongs/h/h3627.md) unweighed, because they were [me'od](../../strongs/h/h3966.md) [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md): neither was the [mišqāl](../../strongs/h/h4948.md) of the [nᵊḥšeṯ](../../strongs/h/h5178.md) [chaqar](../../strongs/h/h2713.md).

<a name="1kings_7_48"></a>1Kings 7:48

And [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) all the [kĕliy](../../strongs/h/h3627.md) that pertained unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): the [mizbeach](../../strongs/h/h4196.md) of [zāhāḇ](../../strongs/h/h2091.md), and the [šulḥān](../../strongs/h/h7979.md) of [zāhāḇ](../../strongs/h/h2091.md), whereupon the [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) was,

<a name="1kings_7_49"></a>1Kings 7:49

And the [mᵊnôrâ](../../strongs/h/h4501.md) of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md), five on the [yamiyn](../../strongs/h/h3225.md) side, and five on the [śᵊmō'l](../../strongs/h/h8040.md), [paniym](../../strongs/h/h6440.md) the [dᵊḇîr](../../strongs/h/h1687.md), with the [peraḥ](../../strongs/h/h6525.md), and the [nîr](../../strongs/h/h5216.md), and the [malqāḥayim](../../strongs/h/h4457.md) of [zāhāḇ](../../strongs/h/h2091.md),

<a name="1kings_7_50"></a>1Kings 7:50

And the [caph](../../strongs/h/h5592.md), and the [mᵊzammerê](../../strongs/h/h4212.md), and the [mizrāq](../../strongs/h/h4219.md), and the [kaph](../../strongs/h/h3709.md), and the [maḥtâ](../../strongs/h/h4289.md) of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md); and the [pōṯ](../../strongs/h/h6596.md) of [zāhāḇ](../../strongs/h/h2091.md), both for the [deleṯ](../../strongs/h/h1817.md) of the [pᵊnîmî](../../strongs/h/h6442.md) [bayith](../../strongs/h/h1004.md), the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) place, and for the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md), to wit, of the [heykal](../../strongs/h/h1964.md).

<a name="1kings_7_51"></a>1Kings 7:51

So was [shalam](../../strongs/h/h7999.md) all the [mĕla'kah](../../strongs/h/h4399.md) that [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And [Šᵊlōmô](../../strongs/h/h8010.md) brought [bow'](../../strongs/h/h935.md) the things which [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md) had [qodesh](../../strongs/h/h6944.md); even the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [kĕliy](../../strongs/h/h3627.md), did he [nathan](../../strongs/h/h5414.md) among the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 6](1kings_6.md) - [1Kings 8](1kings_8.md)