# [1Kings 11](https://www.blueletterbible.org/kjv/1kings/11)

<a name="1kings_11_1"></a>1Kings 11:1

But [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['ahab](../../strongs/h/h157.md) [rab](../../strongs/h/h7227.md) [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md), together with the [bath](../../strongs/h/h1323.md) of [Parʿô](../../strongs/h/h6547.md), the [Mô'āḇî](../../strongs/h/h4125.md), [ʿAmmôn](../../strongs/h/h5984.md), ['Ăḏōmî](../../strongs/h/h130.md), [Ṣîḏōnî](../../strongs/h/h6722.md), and [Ḥitî](../../strongs/h/h2850.md);

<a name="1kings_11_2"></a>1Kings 11:2

Of the [gowy](../../strongs/h/h1471.md) which [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), Ye shall not [bow'](../../strongs/h/h935.md) to them, neither shall they [bow'](../../strongs/h/h935.md) unto you: for ['āḵēn](../../strongs/h/h403.md) they will [natah](../../strongs/h/h5186.md) your [lebab](../../strongs/h/h3824.md) ['aḥar](../../strongs/h/h310.md) their ['Elohiym](../../strongs/h/h430.md): [Šᵊlōmô](../../strongs/h/h8010.md) [dāḇaq](../../strongs/h/h1692.md) unto these in ['ahab](../../strongs/h/h157.md).

<a name="1kings_11_3"></a>1Kings 11:3

And he had seven hundred ['ishshah](../../strongs/h/h802.md), [śārâ](../../strongs/h/h8282.md), and three hundred [pîleḡeš](../../strongs/h/h6370.md): and his ['ishshah](../../strongs/h/h802.md) [natah](../../strongs/h/h5186.md) his [leb](../../strongs/h/h3820.md).

<a name="1kings_11_4"></a>1Kings 11:4

For it came to pass, [ʿēṯ](../../strongs/h/h6256.md) [Šᵊlōmô](../../strongs/h/h8010.md) was [ziqnâ](../../strongs/h/h2209.md), that his ['ishshah](../../strongs/h/h802.md) [natah](../../strongs/h/h5186.md) his [lebab](../../strongs/h/h3824.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md): and his [lebab](../../strongs/h/h3824.md) was not [šālēm](../../strongs/h/h8003.md) with [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), as was the [lebab](../../strongs/h/h3824.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_11_5"></a>1Kings 11:5

For [Šᵊlōmô](../../strongs/h/h8010.md) [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [ʿAštōreṯ](../../strongs/h/h6253.md) the ['Elohiym](../../strongs/h/h430.md) of the [Ṣîḏōnî](../../strongs/h/h6722.md), and ['aḥar](../../strongs/h/h310.md) [Malkām](../../strongs/h/h4445.md) the [šiqqûṣ](../../strongs/h/h8251.md) of the [ʿAmmôn](../../strongs/h/h5984.md).

<a name="1kings_11_6"></a>1Kings 11:6

And [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and went not [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), as did [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_11_7"></a>1Kings 11:7

Then did [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) a [bāmâ](../../strongs/h/h1116.md) for [kᵊmôš](../../strongs/h/h3645.md), the [šiqqûṣ](../../strongs/h/h8251.md) of [Mô'āḇ](../../strongs/h/h4124.md), in the [har](../../strongs/h/h2022.md) that is [paniym](../../strongs/h/h6440.md) [Yĕruwshalaim](../../strongs/h/h3389.md), and for [mōleḵ](../../strongs/h/h4432.md), the [šiqqûṣ](../../strongs/h/h8251.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="1kings_11_8"></a>1Kings 11:8

And likewise ['asah](../../strongs/h/h6213.md) he for all his [nāḵrî](../../strongs/h/h5237.md) ['ishshah](../../strongs/h/h802.md), which [qāṭar](../../strongs/h/h6999.md) and [zabach](../../strongs/h/h2076.md) unto their ['Elohiym](../../strongs/h/h430.md).

<a name="1kings_11_9"></a>1Kings 11:9

And [Yĕhovah](../../strongs/h/h3068.md) was ['anaph](../../strongs/h/h599.md) with [Šᵊlōmô](../../strongs/h/h8010.md), because his [lebab](../../strongs/h/h3824.md) was [natah](../../strongs/h/h5186.md) from [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), which had [ra'ah](../../strongs/h/h7200.md) unto him [pa'am](../../strongs/h/h6471.md),

<a name="1kings_11_10"></a>1Kings 11:10

And had [tsavah](../../strongs/h/h6680.md) him concerning this [dabar](../../strongs/h/h1697.md), that he should not [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md): but he [shamar](../../strongs/h/h8104.md) not that which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md).

<a name="1kings_11_11"></a>1Kings 11:11

Wherefore [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Šᵊlōmô](../../strongs/h/h8010.md), Forasmuch as this is done of thee, and thou hast not [shamar](../../strongs/h/h8104.md) my [bĕriyth](../../strongs/h/h1285.md) and my [chuqqah](../../strongs/h/h2708.md), which I have [tsavah](../../strongs/h/h6680.md) thee, I will [qāraʿ](../../strongs/h/h7167.md) [qāraʿ](../../strongs/h/h7167.md) the [mamlāḵâ](../../strongs/h/h4467.md) from thee, and will [nathan](../../strongs/h/h5414.md) it to thy ['ebed](../../strongs/h/h5650.md).

<a name="1kings_11_12"></a>1Kings 11:12

Notwithstanding in thy [yowm](../../strongs/h/h3117.md) I will not ['asah](../../strongs/h/h6213.md) it for [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md) sake: but I will [qāraʿ](../../strongs/h/h7167.md) it out of the [yad](../../strongs/h/h3027.md) of thy [ben](../../strongs/h/h1121.md).

<a name="1kings_11_13"></a>1Kings 11:13

Howbeit I will not [qāraʿ](../../strongs/h/h7167.md) all the [mamlāḵâ](../../strongs/h/h4467.md); but will [nathan](../../strongs/h/h5414.md) one [shebet](../../strongs/h/h7626.md) to thy [ben](../../strongs/h/h1121.md) for [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md) sake, and for [Yĕruwshalaim](../../strongs/h/h3389.md) sake which I have [bāḥar](../../strongs/h/h977.md).

<a name="1kings_11_14"></a>1Kings 11:14

And [Yĕhovah](../../strongs/h/h3068.md) stirred [quwm](../../strongs/h/h6965.md) a [satan](../../strongs/h/h7854.md) unto [Šᵊlōmô](../../strongs/h/h8010.md), [Hăḏaḏ](../../strongs/h/h1908.md) the ['Ăḏōmî](../../strongs/h/h130.md): he was of the [melek](../../strongs/h/h4428.md) [zera'](../../strongs/h/h2233.md) in ['Ĕḏōm](../../strongs/h/h123.md).

<a name="1kings_11_15"></a>1Kings 11:15

For it came to pass, when [Dāviḏ](../../strongs/h/h1732.md) was in ['Ĕḏōm](../../strongs/h/h123.md), and [Yô'āḇ](../../strongs/h/h3097.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) was [ʿālâ](../../strongs/h/h5927.md) to [qāḇar](../../strongs/h/h6912.md) the [ḥālāl](../../strongs/h/h2491.md), after he had [nakah](../../strongs/h/h5221.md) every [zāḵār](../../strongs/h/h2145.md) in ['Ĕḏōm](../../strongs/h/h123.md);

<a name="1kings_11_16"></a>1Kings 11:16

(For six [ḥōḏeš](../../strongs/h/h2320.md) did [Yô'āḇ](../../strongs/h/h3097.md) [yashab](../../strongs/h/h3427.md) there with all [Yisra'el](../../strongs/h/h3478.md), until he had [karath](../../strongs/h/h3772.md) every [zāḵār](../../strongs/h/h2145.md) in ['Ĕḏōm](../../strongs/h/h123.md):)

<a name="1kings_11_17"></a>1Kings 11:17

That ['Ăḏaḏ](../../strongs/h/h111.md) [bāraḥ](../../strongs/h/h1272.md), he and ['enowsh](../../strongs/h/h582.md) ['Ăḏōmî](../../strongs/h/h130.md) of his ['ab](../../strongs/h/h1.md) ['ebed](../../strongs/h/h5650.md) with him, to [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md); [Hăḏaḏ](../../strongs/h/h1908.md) being yet a [qāṭān](../../strongs/h/h6996.md) [naʿar](../../strongs/h/h5288.md).

<a name="1kings_11_18"></a>1Kings 11:18

And they [quwm](../../strongs/h/h6965.md) out of [Miḏyān](../../strongs/h/h4080.md), and [bow'](../../strongs/h/h935.md) to [Pā'rān](../../strongs/h/h6290.md): and they [laqach](../../strongs/h/h3947.md) ['enowsh](../../strongs/h/h582.md) with them out of [Pā'rān](../../strongs/h/h6290.md), and they [bow'](../../strongs/h/h935.md) to [Mitsrayim](../../strongs/h/h4714.md), unto [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md); which [nathan](../../strongs/h/h5414.md) him a [bayith](../../strongs/h/h1004.md), and ['āmar](../../strongs/h/h559.md) him [lechem](../../strongs/h/h3899.md), and [nathan](../../strongs/h/h5414.md) him ['erets](../../strongs/h/h776.md).

<a name="1kings_11_19"></a>1Kings 11:19

And [Hăḏaḏ](../../strongs/h/h1908.md) [māṣā'](../../strongs/h/h4672.md) [me'od](../../strongs/h/h3966.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of [Parʿô](../../strongs/h/h6547.md), so that he [nathan](../../strongs/h/h5414.md) him to ['ishshah](../../strongs/h/h802.md) the ['āḥôṯ](../../strongs/h/h269.md) of his own ['ishshah](../../strongs/h/h802.md), the ['āḥôṯ](../../strongs/h/h269.md) of [Taḥpᵊnês](../../strongs/h/h8472.md) the [ḡᵊḇîrâ](../../strongs/h/h1377.md).

<a name="1kings_11_20"></a>1Kings 11:20

And the ['āḥôṯ](../../strongs/h/h269.md) of [Taḥpᵊnês](../../strongs/h/h8472.md) [yalad](../../strongs/h/h3205.md) him [Gᵊnuḇaṯ](../../strongs/h/h1592.md) his [ben](../../strongs/h/h1121.md), whom [Taḥpᵊnês](../../strongs/h/h8472.md) [gamal](../../strongs/h/h1580.md) in [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md): and [Gᵊnuḇaṯ](../../strongs/h/h1592.md) was in [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md) among the [ben](../../strongs/h/h1121.md) of [Parʿô](../../strongs/h/h6547.md).

<a name="1kings_11_21"></a>1Kings 11:21

And when [Hăḏaḏ](../../strongs/h/h1908.md) [shama'](../../strongs/h/h8085.md) in [Mitsrayim](../../strongs/h/h4714.md) that [Dāviḏ](../../strongs/h/h1732.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and that [Yô'āḇ](../../strongs/h/h3097.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) was [muwth](../../strongs/h/h4191.md), [Hăḏaḏ](../../strongs/h/h1908.md) ['āmar](../../strongs/h/h559.md) to [Parʿô](../../strongs/h/h6547.md), Let me [shalach](../../strongs/h/h7971.md), that I may [yālaḵ](../../strongs/h/h3212.md) to mine own ['erets](../../strongs/h/h776.md).

<a name="1kings_11_22"></a>1Kings 11:22

Then [Parʿô](../../strongs/h/h6547.md) ['āmar](../../strongs/h/h559.md) unto him, But what hast thou [ḥāsēr](../../strongs/h/h2638.md) with me, that, behold, thou [bāqaš](../../strongs/h/h1245.md) to [yālaḵ](../../strongs/h/h3212.md) to thine own ['erets](../../strongs/h/h776.md)? And he ['āmar](../../strongs/h/h559.md), Nothing: howbeit let me [shalach](../../strongs/h/h7971.md) in any [shalach](../../strongs/h/h7971.md).

<a name="1kings_11_23"></a>1Kings 11:23

And ['Elohiym](../../strongs/h/h430.md) him [quwm](../../strongs/h/h6965.md) another [satan](../../strongs/h/h7854.md), [Rᵊzôn](../../strongs/h/h7331.md) the [ben](../../strongs/h/h1121.md) of ['Elyāḏāʿ](../../strongs/h/h450.md), which [bāraḥ](../../strongs/h/h1272.md) from his ['adown](../../strongs/h/h113.md) [HăḏaḏʿEzer](../../strongs/h/h1909.md) [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md):

<a name="1kings_11_24"></a>1Kings 11:24

And he [qāḇaṣ](../../strongs/h/h6908.md) ['enowsh](../../strongs/h/h582.md) unto him, and became [śar](../../strongs/h/h8269.md) over a [gᵊḏûḏ](../../strongs/h/h1416.md), when [Dāviḏ](../../strongs/h/h1732.md) [harag](../../strongs/h/h2026.md) them: and they [yālaḵ](../../strongs/h/h3212.md) to [Dammeśeq](../../strongs/h/h1834.md), and [yashab](../../strongs/h/h3427.md) therein, and [mālaḵ](../../strongs/h/h4427.md) in [Dammeśeq](../../strongs/h/h1834.md).

<a name="1kings_11_25"></a>1Kings 11:25

And he was a [satan](../../strongs/h/h7854.md) to [Yisra'el](../../strongs/h/h3478.md) all the [yowm](../../strongs/h/h3117.md) of [Šᵊlōmô](../../strongs/h/h8010.md), beside the [ra'](../../strongs/h/h7451.md) that [Hăḏaḏ](../../strongs/h/h1908.md) did: and he [qûṣ](../../strongs/h/h6973.md) [Yisra'el](../../strongs/h/h3478.md), and [mālaḵ](../../strongs/h/h4427.md) over ['Ărām](../../strongs/h/h758.md).

<a name="1kings_11_26"></a>1Kings 11:26

And [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), an ['ep̄rāṯî](../../strongs/h/h673.md) of [Ṣᵊrāḏâ](../../strongs/h/h6868.md), [Šᵊlōmô](../../strongs/h/h8010.md) ['ebed](../../strongs/h/h5650.md), whose ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [ṢᵊrûʿÂ](../../strongs/h/h6871.md), an ['almānâ](../../strongs/h/h490.md) ['ishshah](../../strongs/h/h802.md), even he lifted [ruwm](../../strongs/h/h7311.md) his [yad](../../strongs/h/h3027.md) against the [melek](../../strongs/h/h4428.md).

<a name="1kings_11_27"></a>1Kings 11:27

And this was the [dabar](../../strongs/h/h1697.md) that he [ruwm](../../strongs/h/h7311.md) his [yad](../../strongs/h/h3027.md) against the [melek](../../strongs/h/h4428.md): [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) [Millô'](../../strongs/h/h4407.md), and [cagar](../../strongs/h/h5462.md) the [pereṣ](../../strongs/h/h6556.md) of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_11_28"></a>1Kings 11:28

And the ['iysh](../../strongs/h/h376.md) [YārāḇʿĀm](../../strongs/h/h3379.md) was a [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md): and [Šᵊlōmô](../../strongs/h/h8010.md) [ra'ah](../../strongs/h/h7200.md) the [naʿar](../../strongs/h/h5288.md) that he was ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md), he made him [paqad](../../strongs/h/h6485.md) over all the [sēḇel](../../strongs/h/h5447.md) of the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="1kings_11_29"></a>1Kings 11:29

And it came to pass at that [ʿēṯ](../../strongs/h/h6256.md) when [YārāḇʿĀm](../../strongs/h/h3379.md) [yāṣā'](../../strongs/h/h3318.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that the [nāḇî'](../../strongs/h/h5030.md) ['Ăḥîyâ](../../strongs/h/h281.md) the [Šîlōnî](../../strongs/h/h7888.md) [māṣā'](../../strongs/h/h4672.md) him in the [derek](../../strongs/h/h1870.md); and he had [kāsâ](../../strongs/h/h3680.md) himself with a [ḥāḏāš](../../strongs/h/h2319.md) [śalmâ](../../strongs/h/h8008.md); and they two were alone in the [sadeh](../../strongs/h/h7704.md):

<a name="1kings_11_30"></a>1Kings 11:30

And ['Ăḥîyâ](../../strongs/h/h281.md) [tāp̄aś](../../strongs/h/h8610.md) the [ḥāḏāš](../../strongs/h/h2319.md) [śalmâ](../../strongs/h/h8008.md) that was on him, and [qāraʿ](../../strongs/h/h7167.md) it in twelve [qᵊrāʿîm](../../strongs/h/h7168.md):

<a name="1kings_11_31"></a>1Kings 11:31

And he ['āmar](../../strongs/h/h559.md) to [YārāḇʿĀm](../../strongs/h/h3379.md), [laqach](../../strongs/h/h3947.md) thee ten [qᵊrāʿîm](../../strongs/h/h7168.md): for thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Behold, I will [qāraʿ](../../strongs/h/h7167.md) the [mamlāḵâ](../../strongs/h/h4467.md) out of the [yad](../../strongs/h/h3027.md) of [Šᵊlōmô](../../strongs/h/h8010.md), and will [nathan](../../strongs/h/h5414.md) ten [shebet](../../strongs/h/h7626.md) to thee:

<a name="1kings_11_32"></a>1Kings 11:32

(But he shall have one [shebet](../../strongs/h/h7626.md) for my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) sake, and for [Yĕruwshalaim](../../strongs/h/h3389.md) sake, the [ʿîr](../../strongs/h/h5892.md) which I have [bāḥar](../../strongs/h/h977.md) out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md):)

<a name="1kings_11_33"></a>1Kings 11:33

Because that they have ['azab](../../strongs/h/h5800.md) me, and have [shachah](../../strongs/h/h7812.md) [ʿAštōreṯ](../../strongs/h/h6253.md) the ['Elohiym](../../strongs/h/h430.md) of the [Ṣîḏōnî](../../strongs/h/h6722.md), [kᵊmôš](../../strongs/h/h3645.md) the ['Elohiym](../../strongs/h/h430.md) of the [Mô'āḇ](../../strongs/h/h4124.md), and [Malkām](../../strongs/h/h4445.md) the ['Elohiym](../../strongs/h/h430.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and have not [halak](../../strongs/h/h1980.md) in my [derek](../../strongs/h/h1870.md), to ['asah](../../strongs/h/h6213.md) that which is [yashar](../../strongs/h/h3477.md) in mine ['ayin](../../strongs/h/h5869.md), and my [chuqqah](../../strongs/h/h2708.md) and my [mishpat](../../strongs/h/h4941.md), as did [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md).

<a name="1kings_11_34"></a>1Kings 11:34

Howbeit I will not [laqach](../../strongs/h/h3947.md) the whole [mamlāḵâ](../../strongs/h/h4467.md) out of his [yad](../../strongs/h/h3027.md): but I will [shiyth](../../strongs/h/h7896.md) him [nāśî'](../../strongs/h/h5387.md) all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md) for [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md) sake, whom I [bāḥar](../../strongs/h/h977.md), because he [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md) and my [chuqqah](../../strongs/h/h2708.md):

<a name="1kings_11_35"></a>1Kings 11:35

But I will [laqach](../../strongs/h/h3947.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) out of his [ben](../../strongs/h/h1121.md) [yad](../../strongs/h/h3027.md), and will [nathan](../../strongs/h/h5414.md) it unto thee, even ten [shebet](../../strongs/h/h7626.md).

<a name="1kings_11_36"></a>1Kings 11:36

And unto his [ben](../../strongs/h/h1121.md) will I [nathan](../../strongs/h/h5414.md) one [shebet](../../strongs/h/h7626.md), that [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md) may have a [nîr](../../strongs/h/h5216.md) [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) me in [Yĕruwshalaim](../../strongs/h/h3389.md), the [ʿîr](../../strongs/h/h5892.md) which I have [bāḥar](../../strongs/h/h977.md) me to [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md) there.

<a name="1kings_11_37"></a>1Kings 11:37

And I will [laqach](../../strongs/h/h3947.md) thee, and thou shalt [mālaḵ](../../strongs/h/h4427.md) according to all that thy [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md), and shalt be [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_11_38"></a>1Kings 11:38

And it shall be, if thou wilt [shama'](../../strongs/h/h8085.md) unto all that I [tsavah](../../strongs/h/h6680.md) thee, and wilt [halak](../../strongs/h/h1980.md) in my [derek](../../strongs/h/h1870.md), and ['asah](../../strongs/h/h6213.md) that is [yashar](../../strongs/h/h3477.md) in my ['ayin](../../strongs/h/h5869.md), to [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md) and my [mitsvah](../../strongs/h/h4687.md), as [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md) ['asah](../../strongs/h/h6213.md); that I will be with thee, and [bānâ](../../strongs/h/h1129.md) thee an ['aman](../../strongs/h/h539.md) [bayith](../../strongs/h/h1004.md), as I [bānâ](../../strongs/h/h1129.md) for [Dāviḏ](../../strongs/h/h1732.md), and will [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) unto thee.

<a name="1kings_11_39"></a>1Kings 11:39

And I will for this [ʿānâ](../../strongs/h/h6031.md) the [zera'](../../strongs/h/h2233.md) of [Dāviḏ](../../strongs/h/h1732.md), but not for [yowm](../../strongs/h/h3117.md).

<a name="1kings_11_40"></a>1Kings 11:40

[Šᵊlōmô](../../strongs/h/h8010.md) [bāqaš](../../strongs/h/h1245.md) therefore to [muwth](../../strongs/h/h4191.md) [YārāḇʿĀm](../../strongs/h/h3379.md). And [YārāḇʿĀm](../../strongs/h/h3379.md) [quwm](../../strongs/h/h6965.md), and [bāraḥ](../../strongs/h/h1272.md) into [Mitsrayim](../../strongs/h/h4714.md), unto [Šîšaq](../../strongs/h/h7895.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and was in [Mitsrayim](../../strongs/h/h4714.md) until the [maveth](../../strongs/h/h4194.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_11_41"></a>1Kings 11:41

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Šᵊlōmô](../../strongs/h/h8010.md), and all that he ['asah](../../strongs/h/h6213.md), and his [ḥāḵmâ](../../strongs/h/h2451.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) of [Šᵊlōmô](../../strongs/h/h8010.md)?

<a name="1kings_11_42"></a>1Kings 11:42

And the [yowm](../../strongs/h/h3117.md) that [Šᵊlōmô](../../strongs/h/h8010.md) [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) over all [Yisra'el](../../strongs/h/h3478.md) was forty [šānâ](../../strongs/h/h8141.md).

<a name="1kings_11_43"></a>1Kings 11:43

And [Šᵊlōmô](../../strongs/h/h8010.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 10](1kings_10.md) - [1Kings 12](1kings_12.md)