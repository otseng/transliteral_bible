# [1Kings 1](https://www.blueletterbible.org/kjv/1kings/1)

<a name="1kings_1_1"></a>1Kings 1:1

Now [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) was [zāqēn](../../strongs/h/h2204.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md); and they [kāsâ](../../strongs/h/h3680.md) him with [beḡeḏ](../../strongs/h/h899.md), but he gat no [yāḥam](../../strongs/h/h3179.md).

<a name="1kings_1_2"></a>1Kings 1:2

Wherefore his ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto him, Let there be [bāqaš](../../strongs/h/h1245.md) for my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) a [naʿărâ](../../strongs/h/h5291.md) [bᵊṯûlâ](../../strongs/h/h1330.md): and let her ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), and let her [sāḵan](../../strongs/h/h5532.md) him, and let her [shakab](../../strongs/h/h7901.md) in thy [ḥêq](../../strongs/h/h2436.md), that my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) may get [ḥāmam](../../strongs/h/h2552.md).

<a name="1kings_1_3"></a>1Kings 1:3

So they [bāqaš](../../strongs/h/h1245.md) for a [yāp̄ê](../../strongs/h/h3303.md) [naʿărâ](../../strongs/h/h5291.md) throughout all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md), and [māṣā'](../../strongs/h/h4672.md) ['Ăḇîšaḡ](../../strongs/h/h49.md) a [Šûnammîṯ](../../strongs/h/h7767.md), and [bow'](../../strongs/h/h935.md) her to the [melek](../../strongs/h/h4428.md).

<a name="1kings_1_4"></a>1Kings 1:4

And the [naʿărâ](../../strongs/h/h5291.md) was [me'od](../../strongs/h/h3966.md) [yāp̄ê](../../strongs/h/h3303.md), and [sāḵan](../../strongs/h/h5532.md) the [melek](../../strongs/h/h4428.md), and [sharath](../../strongs/h/h8334.md) to him: but the [melek](../../strongs/h/h4428.md) [yada'](../../strongs/h/h3045.md) her not.

<a name="1kings_1_5"></a>1Kings 1:5

Then ['Ăḏōnîyâ](../../strongs/h/h138.md) the [ben](../../strongs/h/h1121.md) of [Ḥagîṯ](../../strongs/h/h2294.md) [miṯnaśśē'](../../strongs/h/h4984.md) himself, ['āmar](../../strongs/h/h559.md), I will be [mālaḵ](../../strongs/h/h4427.md): and he ['asah](../../strongs/h/h6213.md) him [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md), and fifty ['iysh](../../strongs/h/h376.md) to [rûṣ](../../strongs/h/h7323.md) [paniym](../../strongs/h/h6440.md) him.

<a name="1kings_1_6"></a>1Kings 1:6

And his ['ab](../../strongs/h/h1.md) had not [ʿāṣaḇ](../../strongs/h/h6087.md) him at any [yowm](../../strongs/h/h3117.md) in ['āmar](../../strongs/h/h559.md), Why hast thou done ['asah](../../strongs/h/h6213.md)? and he also was a [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) [tō'ar](../../strongs/h/h8389.md) man; and his mother [yalad](../../strongs/h/h3205.md) him ['aḥar](../../strongs/h/h310.md) ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="1kings_1_7"></a>1Kings 1:7

And he [dabar](../../strongs/h/h1697.md) with [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), and with ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md): and they ['aḥar](../../strongs/h/h310.md) ['Ăḏōnîyâ](../../strongs/h/h138.md) [ʿāzar](../../strongs/h/h5826.md) him.

<a name="1kings_1_8"></a>1Kings 1:8

But [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and [Šimʿî](../../strongs/h/h8096.md), and [RēʿÎ](../../strongs/h/h7472.md), and the [gibôr](../../strongs/h/h1368.md) which belonged to [Dāviḏ](../../strongs/h/h1732.md), were not with ['Ăḏōnîyâ](../../strongs/h/h138.md).

<a name="1kings_1_9"></a>1Kings 1:9

And ['Ăḏōnîyâ](../../strongs/h/h138.md) [zabach](../../strongs/h/h2076.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md) and fat [mᵊrî'](../../strongs/h/h4806.md) by the ['eben](../../strongs/h/h68.md) of [Zōḥeleṯ](../../strongs/h/h2120.md), which is by [ʿÊn Rōḡēl](../../strongs/h/h5883.md), and [qara'](../../strongs/h/h7121.md) all his ['ach](../../strongs/h/h251.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and all the ['enowsh](../../strongs/h/h582.md) of [Yehuwdah](../../strongs/h/h3063.md) the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md):

<a name="1kings_1_10"></a>1Kings 1:10

But [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), and the [gibôr](../../strongs/h/h1368.md), and [Šᵊlōmô](../../strongs/h/h8010.md) his ['ach](../../strongs/h/h251.md), he [qara'](../../strongs/h/h7121.md) not.

<a name="1kings_1_11"></a>1Kings 1:11

Wherefore [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md) unto [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) the ['em](../../strongs/h/h517.md) of [Šᵊlōmô](../../strongs/h/h8010.md), ['āmar](../../strongs/h/h559.md), Hast thou not [shama'](../../strongs/h/h8085.md) that ['Ăḏōnîyâ](../../strongs/h/h138.md) the [ben](../../strongs/h/h1121.md) of [Ḥagîṯ](../../strongs/h/h2294.md) doth [mālaḵ](../../strongs/h/h4427.md), and [Dāviḏ](../../strongs/h/h1732.md) our ['adown](../../strongs/h/h113.md) [yada'](../../strongs/h/h3045.md) it not?

<a name="1kings_1_12"></a>1Kings 1:12

Now therefore [yālaḵ](../../strongs/h/h3212.md), let me, I pray thee, give thee [ya'ats](../../strongs/h/h3289.md) ['etsah](../../strongs/h/h6098.md), that thou mayest [mālaṭ](../../strongs/h/h4422.md) thine own [nephesh](../../strongs/h/h5315.md), and the [nephesh](../../strongs/h/h5315.md) of thy [ben](../../strongs/h/h1121.md) [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_1_13"></a>1Kings 1:13

[yālaḵ](../../strongs/h/h3212.md) and get thee [bow'](../../strongs/h/h935.md) unto [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto him, Didst not thou, my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), [shaba'](../../strongs/h/h7650.md) unto thine ['amah](../../strongs/h/h519.md), ['āmar](../../strongs/h/h559.md), Assuredly [Šᵊlōmô](../../strongs/h/h8010.md) thy [ben](../../strongs/h/h1121.md) shall [mālaḵ](../../strongs/h/h4427.md) ['aḥar](../../strongs/h/h310.md) me, and he shall [yashab](../../strongs/h/h3427.md) upon my [kicce'](../../strongs/h/h3678.md)? why then doth ['Ăḏōnîyâ](../../strongs/h/h138.md) [mālaḵ](../../strongs/h/h4427.md)?

<a name="1kings_1_14"></a>1Kings 1:14

Behold, while thou yet [dabar](../../strongs/h/h1696.md) there with the [melek](../../strongs/h/h4428.md), I also will [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) thee, and [mālā'](../../strongs/h/h4390.md) thy [dabar](../../strongs/h/h1697.md).

<a name="1kings_1_15"></a>1Kings 1:15

And [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md) into the [ḥeḏer](../../strongs/h/h2315.md): and the [melek](../../strongs/h/h4428.md) was [me'od](../../strongs/h/h3966.md) [zāqēn](../../strongs/h/h2204.md); and ['Ăḇîšaḡ](../../strongs/h/h49.md) the [Šûnammîṯ](../../strongs/h/h7767.md) [sharath](../../strongs/h/h8334.md) unto the [melek](../../strongs/h/h4428.md).

<a name="1kings_1_16"></a>1Kings 1:16

And [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) [qāḏaḏ](../../strongs/h/h6915.md), and did [shachah](../../strongs/h/h7812.md) unto the [melek](../../strongs/h/h4428.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), What wouldest thou?

<a name="1kings_1_17"></a>1Kings 1:17

And she ['āmar](../../strongs/h/h559.md) unto him, My ['adown](../../strongs/h/h113.md), thou [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) unto thine ['amah](../../strongs/h/h519.md), Assuredly [Šᵊlōmô](../../strongs/h/h8010.md) thy [ben](../../strongs/h/h1121.md) shall [mālaḵ](../../strongs/h/h4427.md) ['aḥar](../../strongs/h/h310.md) me, and he shall [yashab](../../strongs/h/h3427.md) upon my [kicce'](../../strongs/h/h3678.md).

<a name="1kings_1_18"></a>1Kings 1:18

And now, behold, ['Ăḏōnîyâ](../../strongs/h/h138.md) [mālaḵ](../../strongs/h/h4427.md); and now, my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), thou [yada'](../../strongs/h/h3045.md) it not:

<a name="1kings_1_19"></a>1Kings 1:19

And he hath [zabach](../../strongs/h/h2076.md) [showr](../../strongs/h/h7794.md) and [mᵊrî'](../../strongs/h/h4806.md) and [tso'n](../../strongs/h/h6629.md) in [rōḇ](../../strongs/h/h7230.md), and hath [qara'](../../strongs/h/h7121.md) all the [ben](../../strongs/h/h1121.md) of the [melek](../../strongs/h/h4428.md), and ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), and [Yô'āḇ](../../strongs/h/h3097.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md): but [Šᵊlōmô](../../strongs/h/h8010.md) thy ['ebed](../../strongs/h/h5650.md) hath he not [qara'](../../strongs/h/h7121.md).

<a name="1kings_1_20"></a>1Kings 1:20

And thou, my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md) are upon thee, that thou shouldest [nāḡaḏ](../../strongs/h/h5046.md) them who shall [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="1kings_1_21"></a>1Kings 1:21

Otherwise it shall come to pass, when my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) shall [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), that I and my [ben](../../strongs/h/h1121.md) [Šᵊlōmô](../../strongs/h/h8010.md) shall be counted [chatta'](../../strongs/h/h2400.md).

<a name="1kings_1_22"></a>1Kings 1:22

And, lo, while she yet [dabar](../../strongs/h/h1696.md) with the [melek](../../strongs/h/h4428.md), [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md) also [bow'](../../strongs/h/h935.md).

<a name="1kings_1_23"></a>1Kings 1:23

And they [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), Behold [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md). And when he was [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), he [shachah](../../strongs/h/h7812.md) himself before the [melek](../../strongs/h/h4428.md) with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md).

<a name="1kings_1_24"></a>1Kings 1:24

And [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), hast thou ['āmar](../../strongs/h/h559.md), ['Ăḏōnîyâ](../../strongs/h/h138.md) shall [mālaḵ](../../strongs/h/h4427.md) ['aḥar](../../strongs/h/h310.md) me, and he shall [yashab](../../strongs/h/h3427.md) upon my [kicce'](../../strongs/h/h3678.md)?

<a name="1kings_1_25"></a>1Kings 1:25

For he is [yarad](../../strongs/h/h3381.md) this [yowm](../../strongs/h/h3117.md), and hath [zabach](../../strongs/h/h2076.md) [showr](../../strongs/h/h7794.md) and fat [mᵊrî'](../../strongs/h/h4806.md) and [tso'n](../../strongs/h/h6629.md) in [rōḇ](../../strongs/h/h7230.md), and hath [qara'](../../strongs/h/h7121.md) all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md), and ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md); and, behold, they ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [paniym](../../strongs/h/h6440.md) him, and ['āmar](../../strongs/h/h559.md), [ḥāyâ](../../strongs/h/h2421.md) [melek](../../strongs/h/h4428.md) ['Ăḏōnîyâ](../../strongs/h/h138.md).

<a name="1kings_1_26"></a>1Kings 1:26

But me, even me thy ['ebed](../../strongs/h/h5650.md), and [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and thy ['ebed](../../strongs/h/h5650.md) [Šᵊlōmô](../../strongs/h/h8010.md), hath he not [qara'](../../strongs/h/h7121.md).

<a name="1kings_1_27"></a>1Kings 1:27

Is this [dabar](../../strongs/h/h1697.md) done by my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), and thou hast not [yada'](../../strongs/h/h3045.md) it unto thy ['ebed](../../strongs/h/h5650.md), who should [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) ['aḥar](../../strongs/h/h310.md) him?

<a name="1kings_1_28"></a>1Kings 1:28

Then [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) me [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md). And she [bow'](../../strongs/h/h935.md) into the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="1kings_1_29"></a>1Kings 1:29

And the [melek](../../strongs/h/h4428.md) [shaba'](../../strongs/h/h7650.md), and ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), that hath [pāḏâ](../../strongs/h/h6299.md) my [nephesh](../../strongs/h/h5315.md) out of all [tsarah](../../strongs/h/h6869.md),

<a name="1kings_1_30"></a>1Kings 1:30

Even as I [shaba'](../../strongs/h/h7650.md) unto thee by [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Assuredly [Šᵊlōmô](../../strongs/h/h8010.md) thy [ben](../../strongs/h/h1121.md) shall [mālaḵ](../../strongs/h/h4427.md) ['aḥar](../../strongs/h/h310.md) me, and he shall [yashab](../../strongs/h/h3427.md) upon my [kicce'](../../strongs/h/h3678.md) in my stead; even so will I certainly ['asah](../../strongs/h/h6213.md) this [yowm](../../strongs/h/h3117.md).

<a name="1kings_1_31"></a>1Kings 1:31

Then [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) [qāḏaḏ](../../strongs/h/h6915.md) with her ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and did [shachah](../../strongs/h/h7812.md) to the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), Let my ['adown](../../strongs/h/h113.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [ḥāyâ](../../strongs/h/h2421.md) ['owlam](../../strongs/h/h5769.md).

<a name="1kings_1_32"></a>1Kings 1:32

And [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) me [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md). And they [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="1kings_1_33"></a>1Kings 1:33

The [melek](../../strongs/h/h4428.md) also ['āmar](../../strongs/h/h559.md) unto them, [laqach](../../strongs/h/h3947.md) with you the ['ebed](../../strongs/h/h5650.md) of your ['adown](../../strongs/h/h113.md), and cause [Šᵊlōmô](../../strongs/h/h8010.md) my [ben](../../strongs/h/h1121.md) to [rāḵaḇ](../../strongs/h/h7392.md) upon mine own [pirdâ](../../strongs/h/h6506.md), and bring him [yarad](../../strongs/h/h3381.md) to [gîḥôn](../../strongs/h/h1521.md):

<a name="1kings_1_34"></a>1Kings 1:34

And let [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md) and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md) [māšaḥ](../../strongs/h/h4886.md) him there [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md): and [tāqaʿ](../../strongs/h/h8628.md) ye with the [šôp̄ār](../../strongs/h/h7782.md), and ['āmar](../../strongs/h/h559.md), God [ḥāyâ](../../strongs/h/h2421.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_1_35"></a>1Kings 1:35

Then ye shall [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) him, that he may [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) upon my [kicce'](../../strongs/h/h3678.md); for he shall be [mālaḵ](../../strongs/h/h4427.md) in my stead: and I have [tsavah](../../strongs/h/h6680.md) him to be [nāḡîḏ](../../strongs/h/h5057.md) over [Yisra'el](../../strongs/h/h3478.md) and over [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_1_36"></a>1Kings 1:36

And [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) ['anah](../../strongs/h/h6030.md) the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md): [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) so too.

<a name="1kings_1_37"></a>1Kings 1:37

As [Yĕhovah](../../strongs/h/h3068.md) hath been with my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), even so be he with [Šᵊlōmô](../../strongs/h/h8010.md), and [gāḏal](../../strongs/h/h1431.md) his [kicce'](../../strongs/h/h3678.md) [gāḏal](../../strongs/h/h1431.md) than the [kicce'](../../strongs/h/h3678.md) of my ['adown](../../strongs/h/h113.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="1kings_1_38"></a>1Kings 1:38

So [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and the [kᵊrēṯî](../../strongs/h/h3774.md), and the [pᵊlēṯî](../../strongs/h/h6432.md), [yarad](../../strongs/h/h3381.md), and caused [Šᵊlōmô](../../strongs/h/h8010.md) to [rāḵaḇ](../../strongs/h/h7392.md) upon [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [pirdâ](../../strongs/h/h6506.md), and [yālaḵ](../../strongs/h/h3212.md) him to [gîḥôn](../../strongs/h/h1521.md).

<a name="1kings_1_39"></a>1Kings 1:39

And [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md) [laqach](../../strongs/h/h3947.md) a [qeren](../../strongs/h/h7161.md) of [šemen](../../strongs/h/h8081.md) out of the ['ohel](../../strongs/h/h168.md), and [māšaḥ](../../strongs/h/h4886.md) [Šᵊlōmô](../../strongs/h/h8010.md). And they [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md); and all the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), [ḥāyâ](../../strongs/h/h2421.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_1_40"></a>1Kings 1:40

And all the ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md) ['aḥar](../../strongs/h/h310.md) him, and the ['am](../../strongs/h/h5971.md) [ḥālal](../../strongs/h/h2490.md) with [ḥālîl](../../strongs/h/h2485.md), and [śāmēaḥ](../../strongs/h/h8056.md) with [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md), so that the ['erets](../../strongs/h/h776.md) [bāqaʿ](../../strongs/h/h1234.md) with the [qowl](../../strongs/h/h6963.md) of them.

<a name="1kings_1_41"></a>1Kings 1:41

And ['Ăḏōnîyâ](../../strongs/h/h138.md) and all the [qara'](../../strongs/h/h7121.md) that were with him [shama'](../../strongs/h/h8085.md) it as they had made a [kalah](../../strongs/h/h3615.md) of ['akal](../../strongs/h/h398.md). And when [Yô'āḇ](../../strongs/h/h3097.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), he ['āmar](../../strongs/h/h559.md), Wherefore is this [qowl](../../strongs/h/h6963.md) of the [qiryâ](../../strongs/h/h7151.md) being in an [hāmâ](../../strongs/h/h1993.md)?

<a name="1kings_1_42"></a>1Kings 1:42

And while he yet [dabar](../../strongs/h/h1696.md), behold, [Yônāṯān](../../strongs/h/h3129.md) the [ben](../../strongs/h/h1121.md) of ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md): and ['Ăḏōnîyâ](../../strongs/h/h138.md) ['āmar](../../strongs/h/h559.md) unto him, Come [bow'](../../strongs/h/h935.md); for thou art a [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) ['îš ḥayil](../../strongs/h/h381.md), and [bāśar](../../strongs/h/h1319.md) [towb](../../strongs/h/h2896.md) [bāśar](../../strongs/h/h1319.md).

<a name="1kings_1_43"></a>1Kings 1:43

And [Yônāṯān](../../strongs/h/h3129.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to ['Ăḏōnîyâ](../../strongs/h/h138.md), ['ăḇāl](../../strongs/h/h61.md) our ['adown](../../strongs/h/h113.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) hath made [Šᵊlōmô](../../strongs/h/h8010.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="1kings_1_44"></a>1Kings 1:44

And the [melek](../../strongs/h/h4428.md) hath [shalach](../../strongs/h/h7971.md) with him [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and the [kᵊrēṯî](../../strongs/h/h3774.md), and the [pᵊlēṯî](../../strongs/h/h6432.md), and they have caused him to [rāḵaḇ](../../strongs/h/h7392.md) upon the [melek](../../strongs/h/h4428.md) [pirdâ](../../strongs/h/h6506.md):

<a name="1kings_1_45"></a>1Kings 1:45

And [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md) and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md) have [māšaḥ](../../strongs/h/h4886.md) him [melek](../../strongs/h/h4428.md) in [gîḥôn](../../strongs/h/h1521.md): and they are [ʿālâ](../../strongs/h/h5927.md) from thence [śāmēaḥ](../../strongs/h/h8056.md), so that the [qiryâ](../../strongs/h/h7151.md) [huwm](../../strongs/h/h1949.md). This is the [qowl](../../strongs/h/h6963.md) that ye have [shama'](../../strongs/h/h8085.md).

<a name="1kings_1_46"></a>1Kings 1:46

And also [Šᵊlōmô](../../strongs/h/h8010.md) [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of the [mᵊlûḵâ](../../strongs/h/h4410.md).

<a name="1kings_1_47"></a>1Kings 1:47

And moreover the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md) to [barak](../../strongs/h/h1288.md) our ['adown](../../strongs/h/h113.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) make the [shem](../../strongs/h/h8034.md) of [Šᵊlōmô](../../strongs/h/h8010.md) [yatab](../../strongs/h/h3190.md) than thy [shem](../../strongs/h/h8034.md), and [gāḏal](../../strongs/h/h1431.md) his [kicce'](../../strongs/h/h3678.md) [gāḏal](../../strongs/h/h1431.md) than thy [kicce'](../../strongs/h/h3678.md). And the [melek](../../strongs/h/h4428.md) [shachah](../../strongs/h/h7812.md) himself upon the [miškāḇ](../../strongs/h/h4904.md).

<a name="1kings_1_48"></a>1Kings 1:48

And also thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), which hath [nathan](../../strongs/h/h5414.md) one to [yashab](../../strongs/h/h3427.md) on my [kicce'](../../strongs/h/h3678.md) this [yowm](../../strongs/h/h3117.md), mine ['ayin](../../strongs/h/h5869.md) even [ra'ah](../../strongs/h/h7200.md) it.

<a name="1kings_1_49"></a>1Kings 1:49

And all the [qara'](../../strongs/h/h7121.md) that were with ['Ăḏōnîyâ](../../strongs/h/h138.md) were [ḥārēḏ](../../strongs/h/h2729.md), and [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) his [derek](../../strongs/h/h1870.md).

<a name="1kings_1_50"></a>1Kings 1:50

And ['Ăḏōnîyâ](../../strongs/h/h138.md) [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) of [Šᵊlōmô](../../strongs/h/h8010.md), and [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md), and [ḥāzaq](../../strongs/h/h2388.md) on the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="1kings_1_51"></a>1Kings 1:51

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šᵊlōmô](../../strongs/h/h8010.md), ['āmar](../../strongs/h/h559.md), Behold, ['Ăḏōnîyâ](../../strongs/h/h138.md) [yare'](../../strongs/h/h3372.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md): for, lo, he hath caught ['āḥaz](../../strongs/h/h270.md) on the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md), ['āmar](../../strongs/h/h559.md), Let [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [shaba'](../../strongs/h/h7650.md) unto me to [yowm](../../strongs/h/h3117.md) that he will not [muwth](../../strongs/h/h4191.md) his ['ebed](../../strongs/h/h5650.md) with the [chereb](../../strongs/h/h2719.md).

<a name="1kings_1_52"></a>1Kings 1:52

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md), If he will shew himself a [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md), there shall not an [śaʿărâ](../../strongs/h/h8185.md) of him [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md): but if [ra'](../../strongs/h/h7451.md) shall be [māṣā'](../../strongs/h/h4672.md) in him, he shall [muwth](../../strongs/h/h4191.md).

<a name="1kings_1_53"></a>1Kings 1:53

So [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md), and they [yarad](../../strongs/h/h3381.md) him from the [mizbeach](../../strongs/h/h4196.md). And he [bow'](../../strongs/h/h935.md) and [shachah](../../strongs/h/h7812.md) himself to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md): and [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) to thine [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 2](1kings_2.md)