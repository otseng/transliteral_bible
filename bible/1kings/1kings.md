# 1 Kings

[1 Kings Overview](../../commentary/1kings/1kings_overview.md)

[1 Kings 1](1kings_1.md)

[1 Kings 2](1kings_2.md)

[1 Kings 3](1kings_3.md)

[1 Kings 4](1kings_4.md)

[1 Kings 5](1kings_5.md)

[1 Kings 6](1kings_6.md)

[1 Kings 7](1kings_7.md)

[1 Kings 8](1kings_8.md)

[1 Kings 9](1kings_9.md)

[1 Kings 10](1kings_10.md)

[1 Kings 11](1kings_11.md)

[1 Kings 12](1kings_12.md)

[1 Kings 13](1kings_13.md)

[1 Kings 14](1kings_14.md)

[1 Kings 15](1kings_15.md)

[1 Kings 16](1kings_16.md)

[1 Kings 17](1kings_17.md)

[1 Kings 18](1kings_18.md)

[1 Kings 19](1kings_19.md)

[1 Kings 20](1kings_20.md)

[1 Kings 21](1kings_21.md)

[1 Kings 22](1kings_22.md)

---

[Transliteral Bible](../index.md)