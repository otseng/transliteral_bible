# [1Kings 10](https://www.blueletterbible.org/kjv/1kings/10)

<a name="1kings_10_1"></a>1Kings 10:1

And when the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) [shama'](../../strongs/h/h8085.md) of the [šēmaʿ](../../strongs/h/h8088.md) of [Šᵊlōmô](../../strongs/h/h8010.md) concerning the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), she [bow'](../../strongs/h/h935.md) to [nāsâ](../../strongs/h/h5254.md) him with [ḥîḏâ](../../strongs/h/h2420.md).

<a name="1kings_10_2"></a>1Kings 10:2

And she [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) with a [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [ḥayil](../../strongs/h/h2428.md), with [gāmāl](../../strongs/h/h1581.md) that [nasa'](../../strongs/h/h5375.md) [beśem](../../strongs/h/h1314.md), and [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [zāhāḇ](../../strongs/h/h2091.md), and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md): and when she was [bow'](../../strongs/h/h935.md) to [Šᵊlōmô](../../strongs/h/h8010.md), she [dabar](../../strongs/h/h1696.md) with him of all that was in her [lebab](../../strongs/h/h3824.md).

<a name="1kings_10_3"></a>1Kings 10:3

And [Šᵊlōmô](../../strongs/h/h8010.md) [nāḡaḏ](../../strongs/h/h5046.md) her all her [dabar](../../strongs/h/h1697.md): there was not any [dabar](../../strongs/h/h1697.md) ['alam](../../strongs/h/h5956.md) from the [melek](../../strongs/h/h4428.md), which he [nāḡaḏ](../../strongs/h/h5046.md) her not.

<a name="1kings_10_4"></a>1Kings 10:4

And when the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) had [ra'ah](../../strongs/h/h7200.md) all [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāḵmâ](../../strongs/h/h2451.md), and the [bayith](../../strongs/h/h1004.md) that he had [bānâ](../../strongs/h/h1129.md),

<a name="1kings_10_5"></a>1Kings 10:5

And the [ma'akal](../../strongs/h/h3978.md) of his [šulḥān](../../strongs/h/h7979.md), and the [môšāḇ](../../strongs/h/h4186.md) of his ['ebed](../../strongs/h/h5650.md), and the [maʿămāḏ](../../strongs/h/h4612.md) of his [sharath](../../strongs/h/h8334.md), and their [malbûš](../../strongs/h/h4403.md), and his [šāqâ](../../strongs/h/h8248.md), and his [ʿōlâ](../../strongs/h/h5930.md) by which he [ʿālâ](../../strongs/h/h5927.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); there was no more [ruwach](../../strongs/h/h7307.md) in her.

<a name="1kings_10_6"></a>1Kings 10:6

And she ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md), It was an ['emeth](../../strongs/h/h571.md) [dabar](../../strongs/h/h1697.md) that I [shama'](../../strongs/h/h8085.md) in mine own ['erets](../../strongs/h/h776.md) of thy [dabar](../../strongs/h/h1697.md) and of thy [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="1kings_10_7"></a>1Kings 10:7

Howbeit I ['aman](../../strongs/h/h539.md) not the [dabar](../../strongs/h/h1697.md), until I [bow'](../../strongs/h/h935.md), and mine ['ayin](../../strongs/h/h5869.md) had [ra'ah](../../strongs/h/h7200.md) it: and, behold, the [ḥēṣî](../../strongs/h/h2677.md) was not [nāḡaḏ](../../strongs/h/h5046.md) me: thy [ḥāḵmâ](../../strongs/h/h2451.md) and [towb](../../strongs/h/h2896.md) [yāsap̄](../../strongs/h/h3254.md) the [šᵊmûʿâ](../../strongs/h/h8052.md) which I [shama'](../../strongs/h/h8085.md).

<a name="1kings_10_8"></a>1Kings 10:8

['esher](../../strongs/h/h835.md) are thy ['enowsh](../../strongs/h/h582.md), ['esher](../../strongs/h/h835.md) are these thy ['ebed](../../strongs/h/h5650.md), which ['amad](../../strongs/h/h5975.md) [tāmîḏ](../../strongs/h/h8548.md) [paniym](../../strongs/h/h6440.md) thee, and that [shama'](../../strongs/h/h8085.md) thy [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="1kings_10_9"></a>1Kings 10:9

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [ḥāp̄ēṣ](../../strongs/h/h2654.md) in thee, to [nathan](../../strongs/h/h5414.md) thee on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md): because [Yĕhovah](../../strongs/h/h3068.md) ['ahăḇâ](../../strongs/h/h160.md) [Yisra'el](../../strongs/h/h3478.md) ['owlam](../../strongs/h/h5769.md), therefore [śûm](../../strongs/h/h7760.md) he thee [melek](../../strongs/h/h4428.md), to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md).

<a name="1kings_10_10"></a>1Kings 10:10

And she [nathan](../../strongs/h/h5414.md) the [melek](../../strongs/h/h4428.md) an hundred and twenty [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), and of [beśem](../../strongs/h/h1314.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md), and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md): there [bow'](../../strongs/h/h935.md) no more such [rōḇ](../../strongs/h/h7230.md) of [beśem](../../strongs/h/h1314.md) as these which the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) [nathan](../../strongs/h/h5414.md) to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_10_11"></a>1Kings 10:11

And the ['ŏnî](../../strongs/h/h590.md) also of [Ḥîrām](../../strongs/h/h2438.md), that [nasa'](../../strongs/h/h5375.md) [zāhāḇ](../../strongs/h/h2091.md) from ['Ôp̄îr](../../strongs/h/h211.md), [bow'](../../strongs/h/h935.md) from ['Ôp̄îr](../../strongs/h/h211.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) of ['almugîm](../../strongs/h/h484.md) ['ets](../../strongs/h/h6086.md), and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md).

<a name="1kings_10_12"></a>1Kings 10:12

And the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) of the ['almugîm](../../strongs/h/h484.md) ['ets](../../strongs/h/h6086.md) [misʿāḏ](../../strongs/h/h4552.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), [kinnôr](../../strongs/h/h3658.md) also and [neḇel](../../strongs/h/h5035.md) for [shiyr](../../strongs/h/h7891.md): there [bow'](../../strongs/h/h935.md) no such ['almugîm](../../strongs/h/h484.md) ['ets](../../strongs/h/h6086.md), nor were [ra'ah](../../strongs/h/h7200.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1kings_10_13"></a>1Kings 10:13

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) unto the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) all her [chephets](../../strongs/h/h2656.md), whatsoever she [sha'al](../../strongs/h/h7592.md), beside that which [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) her of his [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md). So she [panah](../../strongs/h/h6437.md) and [yālaḵ](../../strongs/h/h3212.md) to her own ['erets](../../strongs/h/h776.md), she and her ['ebed](../../strongs/h/h5650.md).

<a name="1kings_10_14"></a>1Kings 10:14

Now the [mišqāl](../../strongs/h/h4948.md) of [zāhāḇ](../../strongs/h/h2091.md) that [bow'](../../strongs/h/h935.md) to [Šᵊlōmô](../../strongs/h/h8010.md) in one [šānâ](../../strongs/h/h8141.md) was six hundred threescore and six [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md),

<a name="1kings_10_15"></a>1Kings 10:15

Beside that he had of the ['enowsh](../../strongs/h/h582.md) [tûr](../../strongs/h/h8446.md), and of the [misḥār](../../strongs/h/h4536.md) of the [rāḵal](../../strongs/h/h7402.md), and of all the [melek](../../strongs/h/h4428.md) of ['ereb](../../strongs/h/h6153.md), and of the [peḥâ](../../strongs/h/h6346.md) of the ['erets](../../strongs/h/h776.md).

<a name="1kings_10_16"></a>1Kings 10:16

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) two hundred [tsinnah](../../strongs/h/h6793.md) of [šāḥaṭ](../../strongs/h/h7820.md) [zāhāḇ](../../strongs/h/h2091.md): six hundred of [zāhāḇ](../../strongs/h/h2091.md) [ʿālâ](../../strongs/h/h5927.md) to one [tsinnah](../../strongs/h/h6793.md).

<a name="1kings_10_17"></a>1Kings 10:17

And he made three hundred [magen](../../strongs/h/h4043.md) of [šāḥaṭ](../../strongs/h/h7820.md) [zāhāḇ](../../strongs/h/h2091.md); three [mānê](../../strongs/h/h4488.md) of [zāhāḇ](../../strongs/h/h2091.md) [ʿālâ](../../strongs/h/h5927.md) to one [magen](../../strongs/h/h4043.md): and the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) them in the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="1kings_10_18"></a>1Kings 10:18

Moreover the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [kicce'](../../strongs/h/h3678.md) of [šēn](../../strongs/h/h8127.md), and [ṣāp̄â](../../strongs/h/h6823.md) it with the [pāzaz](../../strongs/h/h6338.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="1kings_10_19"></a>1Kings 10:19

The [kicce'](../../strongs/h/h3678.md) had six [maʿălâ](../../strongs/h/h4609.md), and the [ro'sh](../../strongs/h/h7218.md) of the [kicce'](../../strongs/h/h3678.md) was [ʿāḡōl](../../strongs/h/h5696.md) ['aḥar](../../strongs/h/h310.md): and there were [yad](../../strongs/h/h3027.md) on either side on the [maqowm](../../strongs/h/h4725.md) of the [šeḇeṯ](../../strongs/h/h7675.md), and two ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) beside the [yad](../../strongs/h/h3027.md).

<a name="1kings_10_20"></a>1Kings 10:20

And twelve ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) there on the one side and on the other upon the six [maʿălâ](../../strongs/h/h4609.md): there was not the like ['asah](../../strongs/h/h6213.md) in any [mamlāḵâ](../../strongs/h/h4467.md).

<a name="1kings_10_21"></a>1Kings 10:21

And all [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [mašqê](../../strongs/h/h4945.md) [kĕliy](../../strongs/h/h3627.md) were of [zāhāḇ](../../strongs/h/h2091.md), and all the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) were of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md); none were of [keceph](../../strongs/h/h3701.md): it was [mᵊ'ûmâ](../../strongs/h/h3972.md) [chashab](../../strongs/h/h2803.md) of in the [yowm](../../strongs/h/h3117.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_10_22"></a>1Kings 10:22

For the [melek](../../strongs/h/h4428.md) had at [yam](../../strongs/h/h3220.md) a ['ŏnî](../../strongs/h/h590.md) of [Taršîš](../../strongs/h/h8659.md) with the ['ŏnî](../../strongs/h/h590.md) of [Ḥîrām](../../strongs/h/h2438.md): once in three [šānâ](../../strongs/h/h8141.md) [bow'](../../strongs/h/h935.md) the ['ŏnî](../../strongs/h/h590.md) of [Taršîš](../../strongs/h/h8659.md), [nasa'](../../strongs/h/h5375.md) [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), [šenhabîm](../../strongs/h/h8143.md), and [qôp̄](../../strongs/h/h6971.md), and [tukîyîm](../../strongs/h/h8500.md).

<a name="1kings_10_23"></a>1Kings 10:23

So [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [gāḏal](../../strongs/h/h1431.md) all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) for [ʿōšer](../../strongs/h/h6239.md) and for [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="1kings_10_24"></a>1Kings 10:24

And all the ['erets](../../strongs/h/h776.md) [bāqaš](../../strongs/h/h1245.md) [paniym](../../strongs/h/h6440.md) [Šᵊlōmô](../../strongs/h/h8010.md), to [shama'](../../strongs/h/h8085.md) his [ḥāḵmâ](../../strongs/h/h2451.md), which ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) in his [leb](../../strongs/h/h3820.md).

<a name="1kings_10_25"></a>1Kings 10:25

And they [bow'](../../strongs/h/h935.md) every ['iysh](../../strongs/h/h376.md) his [minchah](../../strongs/h/h4503.md), [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [śalmâ](../../strongs/h/h8008.md), and [nešeq](../../strongs/h/h5402.md), and [beśem](../../strongs/h/h1314.md), [sûs](../../strongs/h/h5483.md), and [pereḏ](../../strongs/h/h6505.md), a [dabar](../../strongs/h/h1697.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md).

<a name="1kings_10_26"></a>1Kings 10:26

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āsap̄](../../strongs/h/h622.md) [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md): and he had a thousand and four hundred [reḵeḇ](../../strongs/h/h7393.md), and twelve thousand [pārāš](../../strongs/h/h6571.md), whom he [nachah](../../strongs/h/h5148.md) in the [ʿîr](../../strongs/h/h5892.md) for [reḵeḇ](../../strongs/h/h7393.md), and with the [melek](../../strongs/h/h4428.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1kings_10_27"></a>1Kings 10:27

And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) [keceph](../../strongs/h/h3701.md) to be in [Yĕruwshalaim](../../strongs/h/h3389.md) as ['eben](../../strongs/h/h68.md), and ['erez](../../strongs/h/h730.md) [nathan](../../strongs/h/h5414.md) he to be as the [šiqmâ](../../strongs/h/h8256.md) that are in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), for [rōḇ](../../strongs/h/h7230.md).

<a name="1kings_10_28"></a>1Kings 10:28

And [Šᵊlōmô](../../strongs/h/h8010.md) had [sûs](../../strongs/h/h5483.md) [môṣā'](../../strongs/h/h4161.md) out of [Mitsrayim](../../strongs/h/h4714.md), and [miqvê](../../strongs/h/h4723.md): the [melek](../../strongs/h/h4428.md) [sāḥar](../../strongs/h/h5503.md) [laqach](../../strongs/h/h3947.md) the [miqvê](../../strongs/h/h4723.md) at a [mᵊḥîr](../../strongs/h/h4242.md).

<a name="1kings_10_29"></a>1Kings 10:29

And a [merkāḇâ](../../strongs/h/h4818.md) [ʿālâ](../../strongs/h/h5927.md) and [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md) for six hundred of [keceph](../../strongs/h/h3701.md), and a [sûs](../../strongs/h/h5483.md) for an hundred and fifty: and so for all the [melek](../../strongs/h/h4428.md) of the [Ḥitî](../../strongs/h/h2850.md), and for the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), did they [yāṣā'](../../strongs/h/h3318.md) them by their [yad](../../strongs/h/h3027.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 9](1kings_9.md) - [1Kings 11](1kings_11.md)