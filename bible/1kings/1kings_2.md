# [1Kings 2](https://www.blueletterbible.org/kjv/1kings/2)

<a name="1kings_2_1"></a>1Kings 2:1

Now the [yowm](../../strongs/h/h3117.md) of [Dāviḏ](../../strongs/h/h1732.md) [qāraḇ](../../strongs/h/h7126.md) that he should [muwth](../../strongs/h/h4191.md); and he [tsavah](../../strongs/h/h6680.md) [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_2_2"></a>1Kings 2:2

I [halak](../../strongs/h/h1980.md) the [derek](../../strongs/h/h1870.md) of all the ['erets](../../strongs/h/h776.md): be thou [ḥāzaq](../../strongs/h/h2388.md) therefore, and shew thyself an ['iysh](../../strongs/h/h376.md);

<a name="1kings_2_3"></a>1Kings 2:3

And [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in his [derek](../../strongs/h/h1870.md), to [shamar](../../strongs/h/h8104.md) his [chuqqah](../../strongs/h/h2708.md), and his [mitsvah](../../strongs/h/h4687.md), and his [mishpat](../../strongs/h/h4941.md), and his [ʿēḏûṯ](../../strongs/h/h5715.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), that thou mayest [sakal](../../strongs/h/h7919.md) in all that thou ['asah](../../strongs/h/h6213.md), and whithersoever thou [panah](../../strongs/h/h6437.md) thyself:

<a name="1kings_2_4"></a>1Kings 2:4

That [Yĕhovah](../../strongs/h/h3068.md) may [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md) which he [dabar](../../strongs/h/h1696.md) concerning me, ['āmar](../../strongs/h/h559.md), If thy [ben](../../strongs/h/h1121.md) [shamar](../../strongs/h/h8104.md) to their [derek](../../strongs/h/h1870.md), to [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) me in ['emeth](../../strongs/h/h571.md) with all their [lebab](../../strongs/h/h3824.md) and with all their [nephesh](../../strongs/h/h5315.md), there shall not [karath](../../strongs/h/h3772.md) thee (['āmar](../../strongs/h/h559.md) he) an ['iysh](../../strongs/h/h376.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_2_5"></a>1Kings 2:5

Moreover thou [yada'](../../strongs/h/h3045.md) also what [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) ['asah](../../strongs/h/h6213.md) to me, and what he ['asah](../../strongs/h/h6213.md) to the two [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [Yisra'el](../../strongs/h/h3478.md), unto ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), and unto [ʿĂmāśā'](../../strongs/h/h6021.md) the [ben](../../strongs/h/h1121.md) of [Yeṯer](../../strongs/h/h3500.md), whom he [harag](../../strongs/h/h2026.md), and [śûm](../../strongs/h/h7760.md) the [dam](../../strongs/h/h1818.md) of [milḥāmâ](../../strongs/h/h4421.md) in [shalowm](../../strongs/h/h7965.md), and [nathan](../../strongs/h/h5414.md) the [dam](../../strongs/h/h1818.md) of [milḥāmâ](../../strongs/h/h4421.md) upon his [chagowr](../../strongs/h/h2290.md) that was about his [māṯnayim](../../strongs/h/h4975.md), and in his [naʿal](../../strongs/h/h5275.md) that were on his [regel](../../strongs/h/h7272.md).

<a name="1kings_2_6"></a>1Kings 2:6

['asah](../../strongs/h/h6213.md) therefore according to thy [ḥāḵmâ](../../strongs/h/h2451.md), and let not his [śêḇâ](../../strongs/h/h7872.md) [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md) in [shalowm](../../strongs/h/h7965.md).

<a name="1kings_2_7"></a>1Kings 2:7

But ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto the [ben](../../strongs/h/h1121.md) of [Barzillay](../../strongs/h/h1271.md) the [Gilʿāḏî](../../strongs/h/h1569.md), and let them be of those that ['akal](../../strongs/h/h398.md) at thy [šulḥān](../../strongs/h/h7979.md): for so they [qāraḇ](../../strongs/h/h7126.md) to me when I [bāraḥ](../../strongs/h/h1272.md) [paniym](../../strongs/h/h6440.md) of ['Ăbyšālôm](../../strongs/h/h53.md) thy ['ach](../../strongs/h/h251.md).

<a name="1kings_2_8"></a>1Kings 2:8

And, behold, thou hast with thee [Šimʿî](../../strongs/h/h8096.md) the [ben](../../strongs/h/h1121.md) of [Gērā'](../../strongs/h/h1617.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md) of [Baḥurîm](../../strongs/h/h980.md), which [qālal](../../strongs/h/h7043.md) me with a [māraṣ](../../strongs/h/h4834.md) [qᵊlālâ](../../strongs/h/h7045.md) in the [yowm](../../strongs/h/h3117.md) when I [yālaḵ](../../strongs/h/h3212.md) to [Maḥănayim](../../strongs/h/h4266.md): but he [yarad](../../strongs/h/h3381.md) to [qārā'](../../strongs/h/h7125.md) me at [Yardēn](../../strongs/h/h3383.md), and I [shaba'](../../strongs/h/h7650.md) to him by [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), I will not put thee to [muwth](../../strongs/h/h4191.md) with the [chereb](../../strongs/h/h2719.md).

<a name="1kings_2_9"></a>1Kings 2:9

Now therefore hold him not [naqah](../../strongs/h/h5352.md): for thou art a [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md), and [yada'](../../strongs/h/h3045.md) what thou oughtest to ['asah](../../strongs/h/h6213.md) unto him; but his [śêḇâ](../../strongs/h/h7872.md) bring thou [yarad](../../strongs/h/h3381.md) to the [shĕ'owl](../../strongs/h/h7585.md) with [dam](../../strongs/h/h1818.md).

<a name="1kings_2_10"></a>1Kings 2:10

So [Dāviḏ](../../strongs/h/h1732.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1kings_2_11"></a>1Kings 2:11

And the [yowm](../../strongs/h/h3117.md) that [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) were forty [šānâ](../../strongs/h/h8141.md): seven [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Ḥeḇrôn](../../strongs/h/h2275.md), and thirty and three [šānâ](../../strongs/h/h8141.md) [mālaḵ](../../strongs/h/h4427.md) he in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1kings_2_12"></a>1Kings 2:12

Then [yashab](../../strongs/h/h3427.md) [Šᵊlōmô](../../strongs/h/h8010.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md); and his [malkuwth](../../strongs/h/h4438.md) was [kuwn](../../strongs/h/h3559.md) [me'od](../../strongs/h/h3966.md).

<a name="1kings_2_13"></a>1Kings 2:13

And ['Ăḏōnîyâ](../../strongs/h/h138.md) the [ben](../../strongs/h/h1121.md) of [Ḥagîṯ](../../strongs/h/h2294.md) [bow'](../../strongs/h/h935.md) to [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) the ['em](../../strongs/h/h517.md) of [Šᵊlōmô](../../strongs/h/h8010.md). And she ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) thou [shalowm](../../strongs/h/h7965.md)? And he ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md).

<a name="1kings_2_14"></a>1Kings 2:14

He ['āmar](../../strongs/h/h559.md) moreover, I have somewhat to [dabar](../../strongs/h/h1697.md) unto thee. And she ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md).

<a name="1kings_2_15"></a>1Kings 2:15

And he ['āmar](../../strongs/h/h559.md), Thou [yada'](../../strongs/h/h3045.md) that the [mᵊlûḵâ](../../strongs/h/h4410.md) was mine, and that all [Yisra'el](../../strongs/h/h3478.md) [śûm](../../strongs/h/h7760.md) their [paniym](../../strongs/h/h6440.md) on me, that I should [mālaḵ](../../strongs/h/h4427.md): howbeit the [mᵊlûḵâ](../../strongs/h/h4410.md) is [cabab](../../strongs/h/h5437.md), and is become my ['ach](../../strongs/h/h251.md): for it was his from [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_2_16"></a>1Kings 2:16

And now I [sha'al](../../strongs/h/h7592.md) one [šᵊ'ēlâ](../../strongs/h/h7596.md) of thee, [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) not. And she ['āmar](../../strongs/h/h559.md) unto him, [dabar](../../strongs/h/h1696.md).

<a name="1kings_2_17"></a>1Kings 2:17

And he ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), I pray thee, unto [Šᵊlōmô](../../strongs/h/h8010.md) the [melek](../../strongs/h/h4428.md), (for he will not [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) [shuwb](../../strongs/h/h7725.md),) that he [nathan](../../strongs/h/h5414.md) me ['Ăḇîšaḡ](../../strongs/h/h49.md) the [Šûnammîṯ](../../strongs/h/h7767.md) to ['ishshah](../../strongs/h/h802.md).

<a name="1kings_2_18"></a>1Kings 2:18

And [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) ['āmar](../../strongs/h/h559.md), [towb](../../strongs/h/h2896.md); I will [dabar](../../strongs/h/h1696.md) for thee unto the [melek](../../strongs/h/h4428.md).

<a name="1kings_2_19"></a>1Kings 2:19

[Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) therefore [bow'](../../strongs/h/h935.md) unto [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), to [dabar](../../strongs/h/h1696.md) unto him for ['Ăḏōnîyâ](../../strongs/h/h138.md). And the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md) to [qārā'](../../strongs/h/h7125.md) her, and [shachah](../../strongs/h/h7812.md) himself unto her, and [yashab](../../strongs/h/h3427.md) on his [kicce'](../../strongs/h/h3678.md), and caused a [kicce'](../../strongs/h/h3678.md) to be [śûm](../../strongs/h/h7760.md) for the [melek](../../strongs/h/h4428.md) ['em](../../strongs/h/h517.md); and she [yashab](../../strongs/h/h3427.md) on his [yamiyn](../../strongs/h/h3225.md).

<a name="1kings_2_20"></a>1Kings 2:20

Then she ['āmar](../../strongs/h/h559.md), I [sha'al](../../strongs/h/h7592.md) one [qāṭān](../../strongs/h/h6996.md) [šᵊ'ēlâ](../../strongs/h/h7596.md) of thee; I pray thee, [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) not [shuwb](../../strongs/h/h7725.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto her, [sha'al](../../strongs/h/h7592.md), my ['em](../../strongs/h/h517.md): for I will not [shuwb](../../strongs/h/h7725.md) [paniym](../../strongs/h/h6440.md) [shuwb](../../strongs/h/h7725.md).

<a name="1kings_2_21"></a>1Kings 2:21

And she ['āmar](../../strongs/h/h559.md), Let ['Ăḇîšaḡ](../../strongs/h/h49.md) the [Šûnammîṯ](../../strongs/h/h7767.md) be [nathan](../../strongs/h/h5414.md) to ['Ăḏōnîyâ](../../strongs/h/h138.md) thy ['ach](../../strongs/h/h251.md) to ['ishshah](../../strongs/h/h802.md).

<a name="1kings_2_22"></a>1Kings 2:22

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto his ['em](../../strongs/h/h517.md), And why dost thou [sha'al](../../strongs/h/h7592.md) ['Ăḇîšaḡ](../../strongs/h/h49.md) the [Šûnammîṯ](../../strongs/h/h7767.md) for ['Ăḏōnîyâ](../../strongs/h/h138.md)? [sha'al](../../strongs/h/h7592.md) for him the [mᵊlûḵâ](../../strongs/h/h4410.md) also; for he is mine [gadowl](../../strongs/h/h1419.md) ['ach](../../strongs/h/h251.md); even for him, and for ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), and for [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md).

<a name="1kings_2_23"></a>1Kings 2:23

Then [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) so to me, and more also, if ['Ăḏōnîyâ](../../strongs/h/h138.md) have not [dabar](../../strongs/h/h1696.md) this [dabar](../../strongs/h/h1697.md) against his own [nephesh](../../strongs/h/h5315.md).

<a name="1kings_2_24"></a>1Kings 2:24

Now therefore, as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), which hath [kuwn](../../strongs/h/h3559.md) me, and [yashab](../../strongs/h/h3427.md) me on the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and who hath ['asah](../../strongs/h/h6213.md) me a [bayith](../../strongs/h/h1004.md), as he [dabar](../../strongs/h/h1696.md), ['Ăḏōnîyâ](../../strongs/h/h138.md) shall be put to [muwth](../../strongs/h/h4191.md) this [yowm](../../strongs/h/h3117.md).

<a name="1kings_2_25"></a>1Kings 2:25

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md) by the [yad](../../strongs/h/h3027.md) of [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md); and he [pāḡaʿ](../../strongs/h/h6293.md) upon him that he [muwth](../../strongs/h/h4191.md).

<a name="1kings_2_26"></a>1Kings 2:26

And unto ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), [yālaḵ](../../strongs/h/h3212.md) thee to [ʿĂnāṯôṯ](../../strongs/h/h6068.md), unto thine own [sadeh](../../strongs/h/h7704.md); for thou art ['iysh](../../strongs/h/h376.md) of [maveth](../../strongs/h/h4194.md): but I will not at this [yowm](../../strongs/h/h3117.md) put thee to [muwth](../../strongs/h/h4191.md), because thou [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [paniym](../../strongs/h/h6440.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and because thou hast been [ʿānâ](../../strongs/h/h6031.md) in all wherein my ['ab](../../strongs/h/h1.md) was [ʿānâ](../../strongs/h/h6031.md).

<a name="1kings_2_27"></a>1Kings 2:27

So [Šᵊlōmô](../../strongs/h/h8010.md) [gāraš](../../strongs/h/h1644.md) ['Eḇyāṯār](../../strongs/h/h54.md) from being [kōhēn](../../strongs/h/h3548.md) unto [Yĕhovah](../../strongs/h/h3068.md); that he might [mālā'](../../strongs/h/h4390.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) concerning the [bayith](../../strongs/h/h1004.md) of [ʿĒlî](../../strongs/h/h5941.md) in [Šîlô](../../strongs/h/h7887.md).

<a name="1kings_2_28"></a>1Kings 2:28

Then [šᵊmûʿâ](../../strongs/h/h8052.md) [bow'](../../strongs/h/h935.md) to [Yô'āḇ](../../strongs/h/h3097.md): for [Yô'āḇ](../../strongs/h/h3097.md) had [natah](../../strongs/h/h5186.md) ['aḥar](../../strongs/h/h310.md) ['Ăḏōnîyâ](../../strongs/h/h138.md), though he [natah](../../strongs/h/h5186.md) not ['aḥar](../../strongs/h/h310.md) ['Ăbyšālôm](../../strongs/h/h53.md). And [Yô'āḇ](../../strongs/h/h3097.md) [nûs](../../strongs/h/h5127.md) unto the ['ohel](../../strongs/h/h168.md) of [Yĕhovah](../../strongs/h/h3068.md), and caught [ḥāzaq](../../strongs/h/h2388.md) on the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md).

<a name="1kings_2_29"></a>1Kings 2:29

And it was [nāḡaḏ](../../strongs/h/h5046.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) that [Yô'āḇ](../../strongs/h/h3097.md) was [nûs](../../strongs/h/h5127.md) unto the ['ohel](../../strongs/h/h168.md) of [Yĕhovah](../../strongs/h/h3068.md); and, behold, he is by the [mizbeach](../../strongs/h/h4196.md). Then [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md) [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [pāḡaʿ](../../strongs/h/h6293.md) upon him.

<a name="1kings_2_30"></a>1Kings 2:30

And [Bᵊnāyâ](../../strongs/h/h1141.md) [bow'](../../strongs/h/h935.md) to the ['ohel](../../strongs/h/h168.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), [yāṣā'](../../strongs/h/h3318.md). And he ['āmar](../../strongs/h/h559.md), Nay; but I will [muwth](../../strongs/h/h4191.md) here. And [Bᵊnāyâ](../../strongs/h/h1141.md) [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md), ['āmar](../../strongs/h/h559.md), Thus [dabar](../../strongs/h/h1696.md) [Yô'āḇ](../../strongs/h/h3097.md), and thus he ['anah](../../strongs/h/h6030.md) me.

<a name="1kings_2_31"></a>1Kings 2:31

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, ['asah](../../strongs/h/h6213.md) as he hath [dabar](../../strongs/h/h1696.md), and [pāḡaʿ](../../strongs/h/h6293.md) upon him, and [qāḇar](../../strongs/h/h6912.md) him; that thou mayest [cuwr](../../strongs/h/h5493.md) the [ḥinnām](../../strongs/h/h2600.md) [dam](../../strongs/h/h1818.md), which [Yô'āḇ](../../strongs/h/h3097.md) [šāp̄aḵ](../../strongs/h/h8210.md), from me, and from the [bayith](../../strongs/h/h1004.md) of my ['ab](../../strongs/h/h1.md).

<a name="1kings_2_32"></a>1Kings 2:32

And [Yĕhovah](../../strongs/h/h3068.md) shall [shuwb](../../strongs/h/h7725.md) his [dam](../../strongs/h/h1818.md) upon his own [ro'sh](../../strongs/h/h7218.md), who [pāḡaʿ](../../strongs/h/h6293.md) upon two ['enowsh](../../strongs/h/h582.md) more [tsaddiyq](../../strongs/h/h6662.md) and [towb](../../strongs/h/h2896.md) than he, and [harag](../../strongs/h/h2026.md) them with the [chereb](../../strongs/h/h2719.md), my ['ab](../../strongs/h/h1.md) [Dāviḏ](../../strongs/h/h1732.md) not [yada'](../../strongs/h/h3045.md) thereof, to wit, ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [Yisra'el](../../strongs/h/h3478.md), and [ʿĂmāśā'](../../strongs/h/h6021.md) the [ben](../../strongs/h/h1121.md) of [Yeṯer](../../strongs/h/h3500.md), [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_2_33"></a>1Kings 2:33

Their [dam](../../strongs/h/h1818.md) shall therefore [shuwb](../../strongs/h/h7725.md) upon the [ro'sh](../../strongs/h/h7218.md) of [Yô'āḇ](../../strongs/h/h3097.md), and upon the [ro'sh](../../strongs/h/h7218.md) of his [zera'](../../strongs/h/h2233.md) ['owlam](../../strongs/h/h5769.md): but upon [Dāviḏ](../../strongs/h/h1732.md), and upon his [zera'](../../strongs/h/h2233.md), and upon his [bayith](../../strongs/h/h1004.md), and upon his [kicce'](../../strongs/h/h3678.md), shall there be [shalowm](../../strongs/h/h7965.md) ['owlam](../../strongs/h/h5769.md) from [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_2_34"></a>1Kings 2:34

So [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [ʿālâ](../../strongs/h/h5927.md), and [pāḡaʿ](../../strongs/h/h6293.md) upon him, and [muwth](../../strongs/h/h4191.md) him: and he was [qāḇar](../../strongs/h/h6912.md) in his own [bayith](../../strongs/h/h1004.md) in the [midbar](../../strongs/h/h4057.md).

<a name="1kings_2_35"></a>1Kings 2:35

And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) over the [tsaba'](../../strongs/h/h6635.md): and [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md) did the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) of ['Eḇyāṯār](../../strongs/h/h54.md).

<a name="1kings_2_36"></a>1Kings 2:36

And the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) for [Šimʿî](../../strongs/h/h8096.md), and ['āmar](../../strongs/h/h559.md) unto him, [bānâ](../../strongs/h/h1129.md) thee a [bayith](../../strongs/h/h1004.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and [yashab](../../strongs/h/h3427.md) there, and [yāṣā'](../../strongs/h/h3318.md) not thence any whither.

<a name="1kings_2_37"></a>1Kings 2:37

For it shall be, that on the [yowm](../../strongs/h/h3117.md) thou [yāṣā'](../../strongs/h/h3318.md), and ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md), thou shalt [yada'](../../strongs/h/h3045.md) for [yada'](../../strongs/h/h3045.md) that thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): thy [dam](../../strongs/h/h1818.md) shall be upon thine own [ro'sh](../../strongs/h/h7218.md).

<a name="1kings_2_38"></a>1Kings 2:38

And [Šimʿî](../../strongs/h/h8096.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), The [dabar](../../strongs/h/h1697.md) is [towb](../../strongs/h/h2896.md): as my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) hath [dabar](../../strongs/h/h1696.md), so will thy ['ebed](../../strongs/h/h5650.md) ['asah](../../strongs/h/h6213.md). And [Šimʿî](../../strongs/h/h8096.md) [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="1kings_2_39"></a>1Kings 2:39

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of three [šānâ](../../strongs/h/h8141.md), that two of the ['ebed](../../strongs/h/h5650.md) of [Šimʿî](../../strongs/h/h8096.md) [bāraḥ](../../strongs/h/h1272.md) unto ['Āḵîš](../../strongs/h/h397.md) [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md) [melek](../../strongs/h/h4428.md) of [Gaṯ](../../strongs/h/h1661.md). And they [nāḡaḏ](../../strongs/h/h5046.md) [Šimʿî](../../strongs/h/h8096.md), ['āmar](../../strongs/h/h559.md), Behold, thy ['ebed](../../strongs/h/h5650.md) be in [Gaṯ](../../strongs/h/h1661.md).

<a name="1kings_2_40"></a>1Kings 2:40

And [Šimʿî](../../strongs/h/h8096.md) [quwm](../../strongs/h/h6965.md), and [ḥāḇaš](../../strongs/h/h2280.md) his [chamowr](../../strongs/h/h2543.md), and [yālaḵ](../../strongs/h/h3212.md) to [Gaṯ](../../strongs/h/h1661.md) to ['Āḵîš](../../strongs/h/h397.md) to [bāqaš](../../strongs/h/h1245.md) his ['ebed](../../strongs/h/h5650.md): and [Šimʿî](../../strongs/h/h8096.md) [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) his ['ebed](../../strongs/h/h5650.md) from [Gaṯ](../../strongs/h/h1661.md).

<a name="1kings_2_41"></a>1Kings 2:41

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Šᵊlōmô](../../strongs/h/h8010.md) that [Šimʿî](../../strongs/h/h8096.md) had [halak](../../strongs/h/h1980.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) to [Gaṯ](../../strongs/h/h1661.md), and was [shuwb](../../strongs/h/h7725.md).

<a name="1kings_2_42"></a>1Kings 2:42

And the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) for [Šimʿî](../../strongs/h/h8096.md), and ['āmar](../../strongs/h/h559.md) unto him, Did I not make thee to [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md), and [ʿûḏ](../../strongs/h/h5749.md) unto thee, ['āmar](../../strongs/h/h559.md), [yada'](../../strongs/h/h3045.md) for a [yada'](../../strongs/h/h3045.md), on the [yowm](../../strongs/h/h3117.md) thou goest [yāṣā'](../../strongs/h/h3318.md), and [halak](../../strongs/h/h1980.md) abroad any whither, that thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md)? and thou ['āmar](../../strongs/h/h559.md) unto me, The [dabar](../../strongs/h/h1697.md) that I have [shama'](../../strongs/h/h8085.md) is [towb](../../strongs/h/h2896.md).

<a name="1kings_2_43"></a>1Kings 2:43

Why then hast thou not [shamar](../../strongs/h/h8104.md) the [šᵊḇûʿâ](../../strongs/h/h7621.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [mitsvah](../../strongs/h/h4687.md) that I have [tsavah](../../strongs/h/h6680.md) thee with?

<a name="1kings_2_44"></a>1Kings 2:44

The [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) moreover to [Šimʿî](../../strongs/h/h8096.md), Thou [yada'](../../strongs/h/h3045.md) all the [ra'](../../strongs/h/h7451.md) which thine [lebab](../../strongs/h/h3824.md) is [yada'](../../strongs/h/h3045.md), that thou ['asah](../../strongs/h/h6213.md) to [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md): therefore [Yĕhovah](../../strongs/h/h3068.md) shall [shuwb](../../strongs/h/h7725.md) thy [ra'](../../strongs/h/h7451.md) upon thine own [ro'sh](../../strongs/h/h7218.md);

<a name="1kings_2_45"></a>1Kings 2:45

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) shall be [barak](../../strongs/h/h1288.md), and the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md) shall be [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) ['owlam](../../strongs/h/h5769.md).

<a name="1kings_2_46"></a>1Kings 2:46

So the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md); which [yāṣā'](../../strongs/h/h3318.md), and [pāḡaʿ](../../strongs/h/h6293.md) upon him, that he [muwth](../../strongs/h/h4191.md). And the [mamlāḵâ](../../strongs/h/h4467.md) was [kuwn](../../strongs/h/h3559.md) in the [yad](../../strongs/h/h3027.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 1](1kings_1.md) - [1Kings 3](1kings_3.md)