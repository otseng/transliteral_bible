# [1Kings 18](https://www.blueletterbible.org/kjv/1kings/18)

<a name="1kings_18_1"></a>1Kings 18:1

And it came to pass after [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to ['Ēlîyâ](../../strongs/h/h452.md) in the third [šānâ](../../strongs/h/h8141.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [ra'ah](../../strongs/h/h7200.md) thyself unto ['Aḥ'Āḇ](../../strongs/h/h256.md); and I will [nathan](../../strongs/h/h5414.md) [māṭār](../../strongs/h/h4306.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="1kings_18_2"></a>1Kings 18:2

And ['Ēlîyâ](../../strongs/h/h452.md) [yālaḵ](../../strongs/h/h3212.md) to [ra'ah](../../strongs/h/h7200.md) himself unto ['Aḥ'Āḇ](../../strongs/h/h256.md). And there was a [ḥāzāq](../../strongs/h/h2389.md) [rāʿāḇ](../../strongs/h/h7458.md) in [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_18_3"></a>1Kings 18:3

And ['Aḥ'Āḇ](../../strongs/h/h256.md) [qara'](../../strongs/h/h7121.md) [ʿŌḇaḏyâ](../../strongs/h/h5662.md), which was [bayith](../../strongs/h/h1004.md). (Now [ʿŌḇaḏyâ](../../strongs/h/h5662.md) [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md) [me'od](../../strongs/h/h3966.md):

<a name="1kings_18_4"></a>1Kings 18:4

For it was so, when ['Îzeḇel](../../strongs/h/h348.md) [karath](../../strongs/h/h3772.md) the [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md), that [ʿŌḇaḏyâ](../../strongs/h/h5662.md) [laqach](../../strongs/h/h3947.md) an hundred [nāḇî'](../../strongs/h/h5030.md), and [chaba'](../../strongs/h/h2244.md) them by ['iysh](../../strongs/h/h376.md) in a [mᵊʿārâ](../../strongs/h/h4631.md), and [kûl](../../strongs/h/h3557.md) them with [lechem](../../strongs/h/h3899.md) and [mayim](../../strongs/h/h4325.md).)

<a name="1kings_18_5"></a>1Kings 18:5

And ['Aḥ'Āḇ](../../strongs/h/h256.md) ['āmar](../../strongs/h/h559.md) unto [ʿŌḇaḏyâ](../../strongs/h/h5662.md), [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md), unto all [maʿyān](../../strongs/h/h4599.md) of [mayim](../../strongs/h/h4325.md), and unto all [nachal](../../strongs/h/h5158.md): peradventure we may [māṣā'](../../strongs/h/h4672.md) [chatsiyr](../../strongs/h/h2682.md) to [ḥāyâ](../../strongs/h/h2421.md) the [sûs](../../strongs/h/h5483.md) and [pereḏ](../../strongs/h/h6505.md) [ḥāyâ](../../strongs/h/h2421.md), that we [karath](../../strongs/h/h3772.md) not all the [bĕhemah](../../strongs/h/h929.md).

<a name="1kings_18_6"></a>1Kings 18:6

So they [chalaq](../../strongs/h/h2505.md) the ['erets](../../strongs/h/h776.md) between them to ['abar](../../strongs/h/h5674.md) it: ['Aḥ'Āḇ](../../strongs/h/h256.md) [halak](../../strongs/h/h1980.md) one [derek](../../strongs/h/h1870.md) by himself, and [ʿŌḇaḏyâ](../../strongs/h/h5662.md) [halak](../../strongs/h/h1980.md) another [derek](../../strongs/h/h1870.md) by himself.

<a name="1kings_18_7"></a>1Kings 18:7

And as [ʿŌḇaḏyâ](../../strongs/h/h5662.md) was in the [derek](../../strongs/h/h1870.md), behold, ['Ēlîyâ](../../strongs/h/h452.md) [qārā'](../../strongs/h/h7125.md) him: and he [nāḵar](../../strongs/h/h5234.md) him, and [naphal](../../strongs/h/h5307.md) on his [paniym](../../strongs/h/h6440.md), and ['āmar](../../strongs/h/h559.md), Art thou that my ['adown](../../strongs/h/h113.md) ['Ēlîyâ](../../strongs/h/h452.md)?

<a name="1kings_18_8"></a>1Kings 18:8

And he ['āmar](../../strongs/h/h559.md) him, I am: [yālaḵ](../../strongs/h/h3212.md), ['āmar](../../strongs/h/h559.md) thy ['adown](../../strongs/h/h113.md), Behold, ['Ēlîyâ](../../strongs/h/h452.md) is here.

<a name="1kings_18_9"></a>1Kings 18:9

And he ['āmar](../../strongs/h/h559.md), What have I [chata'](../../strongs/h/h2398.md), that thou wouldest [nathan](../../strongs/h/h5414.md) thy ['ebed](../../strongs/h/h5650.md) into the [yad](../../strongs/h/h3027.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), to [muwth](../../strongs/h/h4191.md) me?

<a name="1kings_18_10"></a>1Kings 18:10

As [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [chay](../../strongs/h/h2416.md), there is no [gowy](../../strongs/h/h1471.md) or [mamlāḵâ](../../strongs/h/h4467.md), whither my ['adown](../../strongs/h/h113.md) hath not [shalach](../../strongs/h/h7971.md) to [bāqaš](../../strongs/h/h1245.md) thee: and when they ['āmar](../../strongs/h/h559.md), He is not there; he took a [shaba'](../../strongs/h/h7650.md) of the [mamlāḵâ](../../strongs/h/h4467.md) and [gowy](../../strongs/h/h1471.md), that they [māṣā'](../../strongs/h/h4672.md) thee not.

<a name="1kings_18_11"></a>1Kings 18:11

And now thou ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), ['āmar](../../strongs/h/h559.md) thy ['adown](../../strongs/h/h113.md), Behold, ['Ēlîyâ](../../strongs/h/h452.md) is here.

<a name="1kings_18_12"></a>1Kings 18:12

And it shall come to pass, as soon as I am [yālaḵ](../../strongs/h/h3212.md) from thee, that the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [nasa'](../../strongs/h/h5375.md) thee whither I [yada'](../../strongs/h/h3045.md) not; and so when I [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) ['Aḥ'Āḇ](../../strongs/h/h256.md), and he cannot [māṣā'](../../strongs/h/h4672.md) thee, he shall [harag](../../strongs/h/h2026.md) me: but I thy ['ebed](../../strongs/h/h5650.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) from my [nāʿur](../../strongs/h/h5271.md).

<a name="1kings_18_13"></a>1Kings 18:13

Was it not [nāḡaḏ](../../strongs/h/h5046.md) my ['adown](../../strongs/h/h113.md) what I ['asah](../../strongs/h/h6213.md) when ['Îzeḇel](../../strongs/h/h348.md) [harag](../../strongs/h/h2026.md) the [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md), how I [chaba'](../../strongs/h/h2244.md) an hundred ['iysh](../../strongs/h/h376.md) of [Yĕhovah](../../strongs/h/h3068.md) [nāḇî'](../../strongs/h/h5030.md) by fifty in a [mᵊʿārâ](../../strongs/h/h4631.md), and [kûl](../../strongs/h/h3557.md) them with [lechem](../../strongs/h/h3899.md) and [mayim](../../strongs/h/h4325.md)?

<a name="1kings_18_14"></a>1Kings 18:14

And now thou ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), ['āmar](../../strongs/h/h559.md) thy ['adown](../../strongs/h/h113.md), Behold, ['Ēlîyâ](../../strongs/h/h452.md) is here: and he shall [harag](../../strongs/h/h2026.md) me.

<a name="1kings_18_15"></a>1Kings 18:15

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) [chay](../../strongs/h/h2416.md), [paniym](../../strongs/h/h6440.md) whom I ['amad](../../strongs/h/h5975.md), I will surely [ra'ah](../../strongs/h/h7200.md) myself unto him to [yowm](../../strongs/h/h3117.md).

<a name="1kings_18_16"></a>1Kings 18:16

So [ʿŌḇaḏyâ](../../strongs/h/h5662.md) [yālaḵ](../../strongs/h/h3212.md) to [qārā'](../../strongs/h/h7125.md) ['Aḥ'Āḇ](../../strongs/h/h256.md), and [nāḡaḏ](../../strongs/h/h5046.md) him: and ['Aḥ'Āḇ](../../strongs/h/h256.md) [yālaḵ](../../strongs/h/h3212.md) to [qārā'](../../strongs/h/h7125.md) ['Ēlîyâ](../../strongs/h/h452.md).

<a name="1kings_18_17"></a>1Kings 18:17

And it came to pass, when ['Aḥ'Āḇ](../../strongs/h/h256.md) [ra'ah](../../strongs/h/h7200.md) ['Ēlîyâ](../../strongs/h/h452.md), that ['Aḥ'Āḇ](../../strongs/h/h256.md) ['āmar](../../strongs/h/h559.md) unto him, Art thou he that [ʿāḵar](../../strongs/h/h5916.md) [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_18_18"></a>1Kings 18:18

And he ['āmar](../../strongs/h/h559.md), I have not [ʿāḵar](../../strongs/h/h5916.md) [Yisra'el](../../strongs/h/h3478.md); but thou, and thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), in that ye have ['azab](../../strongs/h/h5800.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), and thou hast [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [BaʿAl](../../strongs/h/h1168.md).

<a name="1kings_18_19"></a>1Kings 18:19

Now therefore [shalach](../../strongs/h/h7971.md), and [qāḇaṣ](../../strongs/h/h6908.md) to me all [Yisra'el](../../strongs/h/h3478.md) unto [har](../../strongs/h/h2022.md) [Karmel](../../strongs/h/h3760.md), and the [nāḇî'](../../strongs/h/h5030.md) of [BaʿAl](../../strongs/h/h1168.md) four hundred and fifty, and the [nāḇî'](../../strongs/h/h5030.md) of the ['ăšērâ](../../strongs/h/h842.md) four hundred, which ['akal](../../strongs/h/h398.md) at ['Îzeḇel](../../strongs/h/h348.md) [šulḥān](../../strongs/h/h7979.md).

<a name="1kings_18_20"></a>1Kings 18:20

So ['Aḥ'Āḇ](../../strongs/h/h256.md) [shalach](../../strongs/h/h7971.md) unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [qāḇaṣ](../../strongs/h/h6908.md) the [nāḇî'](../../strongs/h/h5030.md) [qāḇaṣ](../../strongs/h/h6908.md) unto [har](../../strongs/h/h2022.md) [Karmel](../../strongs/h/h3760.md).

<a name="1kings_18_21"></a>1Kings 18:21

And ['Ēlîyâ](../../strongs/h/h452.md) [nāḡaš](../../strongs/h/h5066.md) unto all the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md), How long [pāsaḥ](../../strongs/h/h6452.md) ye between two [sᵊʿipâ](../../strongs/h/h5587.md)? if [Yĕhovah](../../strongs/h/h3068.md) be ['Elohiym](../../strongs/h/h430.md), follow him: but if [BaʿAl](../../strongs/h/h1168.md), then [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) him. And the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md) him not a [dabar](../../strongs/h/h1697.md).

<a name="1kings_18_22"></a>1Kings 18:22

Then ['āmar](../../strongs/h/h559.md) ['Ēlîyâ](../../strongs/h/h452.md) unto the ['am](../../strongs/h/h5971.md), I, even I only, [yāṯar](../../strongs/h/h3498.md) a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md); but [BaʿAl](../../strongs/h/h1168.md) [nāḇî'](../../strongs/h/h5030.md) are four hundred and fifty ['iysh](../../strongs/h/h376.md).

<a name="1kings_18_23"></a>1Kings 18:23

Let them therefore [nathan](../../strongs/h/h5414.md) us two [par](../../strongs/h/h6499.md); and let them [bāḥar](../../strongs/h/h977.md) one [par](../../strongs/h/h6499.md) for themselves, and [nāṯaḥ](../../strongs/h/h5408.md) it, and [śûm](../../strongs/h/h7760.md) it on ['ets](../../strongs/h/h6086.md), and [śûm](../../strongs/h/h7760.md) no ['esh](../../strongs/h/h784.md) under: and I will ['asah](../../strongs/h/h6213.md) the other [par](../../strongs/h/h6499.md), and [nathan](../../strongs/h/h5414.md) it on ['ets](../../strongs/h/h6086.md), and [śûm](../../strongs/h/h7760.md) no ['esh](../../strongs/h/h784.md) under:

<a name="1kings_18_24"></a>1Kings 18:24

And [qara'](../../strongs/h/h7121.md) ye on the [shem](../../strongs/h/h8034.md) of your ['Elohiym](../../strongs/h/h430.md), and I will [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): and the ['Elohiym](../../strongs/h/h430.md) that ['anah](../../strongs/h/h6030.md) by ['esh](../../strongs/h/h784.md), let him be ['Elohiym](../../strongs/h/h430.md). And all the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), It is [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md).

<a name="1kings_18_25"></a>1Kings 18:25

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto the [nāḇî'](../../strongs/h/h5030.md) of [BaʿAl](../../strongs/h/h1168.md), [bāḥar](../../strongs/h/h977.md) you one [par](../../strongs/h/h6499.md) for yourselves, and ['asah](../../strongs/h/h6213.md) it [ri'šôn](../../strongs/h/h7223.md); for ye are [rab](../../strongs/h/h7227.md); and [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of your ['Elohiym](../../strongs/h/h430.md), but [śûm](../../strongs/h/h7760.md) no ['esh](../../strongs/h/h784.md) under.

<a name="1kings_18_26"></a>1Kings 18:26

And they [laqach](../../strongs/h/h3947.md) the [par](../../strongs/h/h6499.md) which was [nathan](../../strongs/h/h5414.md) them, and they ['asah](../../strongs/h/h6213.md) it, and [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of [BaʿAl](../../strongs/h/h1168.md) from [boqer](../../strongs/h/h1242.md) even until [ṣōhar](../../strongs/h/h6672.md), ['āmar](../../strongs/h/h559.md), O [BaʿAl](../../strongs/h/h1168.md), ['anah](../../strongs/h/h6030.md) us. But there was no [qowl](../../strongs/h/h6963.md), nor any that ['anah](../../strongs/h/h6030.md). And they [pāsaḥ](../../strongs/h/h6452.md) upon the [mizbeach](../../strongs/h/h4196.md) which was ['asah](../../strongs/h/h6213.md).

<a name="1kings_18_27"></a>1Kings 18:27

And it came to pass at [ṣōhar](../../strongs/h/h6672.md), that ['Ēlîyâ](../../strongs/h/h452.md) [hāṯal](../../strongs/h/h2048.md) them, and ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md): for he is an ['Elohiym](../../strongs/h/h430.md); either he is [śîaḥ](../../strongs/h/h7879.md), or he is [śîḡ](../../strongs/h/h7873.md), or he is in a [derek](../../strongs/h/h1870.md), or peradventure he [yāšēn](../../strongs/h/h3463.md), and must be [yāqaṣ](../../strongs/h/h3364.md).

<a name="1kings_18_28"></a>1Kings 18:28

And they [qara'](../../strongs/h/h7121.md) [qowl](../../strongs/h/h6963.md) [gadowl](../../strongs/h/h1419.md), and [gāḏaḏ](../../strongs/h/h1413.md) themselves after their [mishpat](../../strongs/h/h4941.md) with [chereb](../../strongs/h/h2719.md) and [rōmaḥ](../../strongs/h/h7420.md), till the [dam](../../strongs/h/h1818.md) [šāp̄aḵ](../../strongs/h/h8210.md) upon them.

<a name="1kings_18_29"></a>1Kings 18:29

And it came to pass, when [ṣōhar](../../strongs/h/h6672.md) was ['abar](../../strongs/h/h5674.md), and they [nāḇā'](../../strongs/h/h5012.md) until the time of the [ʿālâ](../../strongs/h/h5927.md) of the [minchah](../../strongs/h/h4503.md), that there was neither [qowl](../../strongs/h/h6963.md), nor any to ['anah](../../strongs/h/h6030.md), nor any that [qešeḇ](../../strongs/h/h7182.md).

<a name="1kings_18_30"></a>1Kings 18:30

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), [nāḡaš](../../strongs/h/h5066.md) unto me. And all the ['am](../../strongs/h/h5971.md) [nāḡaš](../../strongs/h/h5066.md) unto him. And he [rapha'](../../strongs/h/h7495.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) that was [harac](../../strongs/h/h2040.md).

<a name="1kings_18_31"></a>1Kings 18:31

And ['Ēlîyâ](../../strongs/h/h452.md) [laqach](../../strongs/h/h3947.md) twelve ['eben](../../strongs/h/h68.md), according to the [mispār](../../strongs/h/h4557.md) of the [shebet](../../strongs/h/h7626.md) of the [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md), unto whom the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came, ['āmar](../../strongs/h/h559.md), [Yisra'el](../../strongs/h/h3478.md) shall be thy [shem](../../strongs/h/h8034.md):

<a name="1kings_18_32"></a>1Kings 18:32

And with the ['eben](../../strongs/h/h68.md) he [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md): and he ['asah](../../strongs/h/h6213.md) a [tᵊʿālâ](../../strongs/h/h8585.md) [cabiyb](../../strongs/h/h5439.md) the [mizbeach](../../strongs/h/h4196.md), as great as would [bayith](../../strongs/h/h1004.md) two [sᵊ'â](../../strongs/h/h5429.md) of [zera'](../../strongs/h/h2233.md).

<a name="1kings_18_33"></a>1Kings 18:33

And he ['arak](../../strongs/h/h6186.md) the ['ets](../../strongs/h/h6086.md) in ['arak](../../strongs/h/h6186.md), and [nāṯaḥ](../../strongs/h/h5408.md) the [par](../../strongs/h/h6499.md) in pieces, and [śûm](../../strongs/h/h7760.md) him on the ['ets](../../strongs/h/h6086.md), and ['āmar](../../strongs/h/h559.md), [mālā'](../../strongs/h/h4390.md) four [kaḏ](../../strongs/h/h3537.md) with [mayim](../../strongs/h/h4325.md), and [yāṣaq](../../strongs/h/h3332.md) it on the [ʿōlâ](../../strongs/h/h5930.md), and on the ['ets](../../strongs/h/h6086.md).

<a name="1kings_18_34"></a>1Kings 18:34

And he ['āmar](../../strongs/h/h559.md), Do it the second [šānâ](../../strongs/h/h8138.md). And they did it the second [šānâ](../../strongs/h/h8138.md). And he ['āmar](../../strongs/h/h559.md), Do it the third [šālaš](../../strongs/h/h8027.md). And they did it the third [šālaš](../../strongs/h/h8027.md).

<a name="1kings_18_35"></a>1Kings 18:35

And the [mayim](../../strongs/h/h4325.md) [yālaḵ](../../strongs/h/h3212.md) [cabiyb](../../strongs/h/h5439.md) the [mizbeach](../../strongs/h/h4196.md); and he [mālā'](../../strongs/h/h4390.md) the [tᵊʿālâ](../../strongs/h/h8585.md) also with [mayim](../../strongs/h/h4325.md).

<a name="1kings_18_36"></a>1Kings 18:36

And it came to pass at the time of the [ʿālâ](../../strongs/h/h5927.md) of the [minchah](../../strongs/h/h4503.md), that ['Ēlîyâ](../../strongs/h/h452.md) the [nāḇî'](../../strongs/h/h5030.md) [nāḡaš](../../strongs/h/h5066.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and of [Yisra'el](../../strongs/h/h3478.md), let it be [yada'](../../strongs/h/h3045.md) this [yowm](../../strongs/h/h3117.md) that thou art ['Elohiym](../../strongs/h/h430.md) in [Yisra'el](../../strongs/h/h3478.md), and that I am thy ['ebed](../../strongs/h/h5650.md), and that I have ['asah](../../strongs/h/h6213.md) all these things at thy [dabar](../../strongs/h/h1697.md).

<a name="1kings_18_37"></a>1Kings 18:37

['anah](../../strongs/h/h6030.md) me, [Yĕhovah](../../strongs/h/h3068.md), ['anah](../../strongs/h/h6030.md) me, that this ['am](../../strongs/h/h5971.md) may [yada'](../../strongs/h/h3045.md) that thou art [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), and that thou hast [cabab](../../strongs/h/h5437.md) their [leb](../../strongs/h/h3820.md) [cabab](../../strongs/h/h5437.md) ['ăḥōrannîṯ](../../strongs/h/h322.md).

<a name="1kings_18_38"></a>1Kings 18:38

Then the ['esh](../../strongs/h/h784.md) of [Yĕhovah](../../strongs/h/h3068.md) [naphal](../../strongs/h/h5307.md), and ['akal](../../strongs/h/h398.md) the [ʿōlâ](../../strongs/h/h5930.md), and the ['ets](../../strongs/h/h6086.md), and the ['eben](../../strongs/h/h68.md), and the ['aphar](../../strongs/h/h6083.md), and [lāḥaḵ](../../strongs/h/h3897.md) the [mayim](../../strongs/h/h4325.md) that was in the [tᵊʿālâ](../../strongs/h/h8585.md).

<a name="1kings_18_39"></a>1Kings 18:39

And when all the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) it, they [naphal](../../strongs/h/h5307.md) on their [paniym](../../strongs/h/h6440.md): and they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), he is the ['Elohiym](../../strongs/h/h430.md); [Yĕhovah](../../strongs/h/h3068.md), he is the ['Elohiym](../../strongs/h/h430.md).

<a name="1kings_18_40"></a>1Kings 18:40

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto them, [tāp̄aś](../../strongs/h/h8610.md) the [nāḇî'](../../strongs/h/h5030.md) of [BaʿAl](../../strongs/h/h1168.md); let not ['iysh](../../strongs/h/h376.md) of them [mālaṭ](../../strongs/h/h4422.md). And they [tāp̄aś](../../strongs/h/h8610.md) them: and ['Ēlîyâ](../../strongs/h/h452.md) [yarad](../../strongs/h/h3381.md) them to the [nachal](../../strongs/h/h5158.md) [Qîšôn](../../strongs/h/h7028.md), and [šāḥaṭ](../../strongs/h/h7819.md) them there.

<a name="1kings_18_41"></a>1Kings 18:41

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto ['Aḥ'Āḇ](../../strongs/h/h256.md), Get thee [ʿālâ](../../strongs/h/h5927.md), ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md); for there is a [qowl](../../strongs/h/h6963.md) of [hāmôn](../../strongs/h/h1995.md) of [gešem](../../strongs/h/h1653.md).

<a name="1kings_18_42"></a>1Kings 18:42

So ['Aḥ'Āḇ](../../strongs/h/h256.md) [ʿālâ](../../strongs/h/h5927.md) to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md). And ['Ēlîyâ](../../strongs/h/h452.md) [ʿālâ](../../strongs/h/h5927.md) to the [ro'sh](../../strongs/h/h7218.md) of [Karmel](../../strongs/h/h3760.md); and he [gāhar](../../strongs/h/h1457.md) himself upon the ['erets](../../strongs/h/h776.md), and [śûm](../../strongs/h/h7760.md) his [paniym](../../strongs/h/h6440.md) between his [bereḵ](../../strongs/h/h1290.md),

<a name="1kings_18_43"></a>1Kings 18:43

And ['āmar](../../strongs/h/h559.md) to his [naʿar](../../strongs/h/h5288.md), [ʿālâ](../../strongs/h/h5927.md) now, [nabat](../../strongs/h/h5027.md) [derek](../../strongs/h/h1870.md) the [yam](../../strongs/h/h3220.md). And he [ʿālâ](../../strongs/h/h5927.md), and [nabat](../../strongs/h/h5027.md), and ['āmar](../../strongs/h/h559.md), There is [mᵊ'ûmâ](../../strongs/h/h3972.md). And he ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) seven [pa'am](../../strongs/h/h6471.md).

<a name="1kings_18_44"></a>1Kings 18:44

And it came to pass at the seventh time, that he ['āmar](../../strongs/h/h559.md), Behold, there [ʿālâ](../../strongs/h/h5927.md) a [qāṭān](../../strongs/h/h6996.md) ['ab](../../strongs/h/h5645.md) out of the [yam](../../strongs/h/h3220.md), like an ['iysh](../../strongs/h/h376.md) [kaph](../../strongs/h/h3709.md). And he ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md), ['āmar](../../strongs/h/h559.md) unto ['Aḥ'Āḇ](../../strongs/h/h256.md), ['āsar](../../strongs/h/h631.md), and get thee [yarad](../../strongs/h/h3381.md), that the [gešem](../../strongs/h/h1653.md) [ʿāṣar](../../strongs/h/h6113.md) thee not.

<a name="1kings_18_45"></a>1Kings 18:45

And it came to pass in the mean while, that the [shamayim](../../strongs/h/h8064.md) was [qāḏar](../../strongs/h/h6937.md) with ['ab](../../strongs/h/h5645.md) and [ruwach](../../strongs/h/h7307.md), and there was a [gadowl](../../strongs/h/h1419.md) [gešem](../../strongs/h/h1653.md). And ['Aḥ'Āḇ](../../strongs/h/h256.md) [rāḵaḇ](../../strongs/h/h7392.md), and [yālaḵ](../../strongs/h/h3212.md) to [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="1kings_18_46"></a>1Kings 18:46

And the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was on ['Ēlîyâ](../../strongs/h/h452.md); and he girded [šānas](../../strongs/h/h8151.md) his [māṯnayim](../../strongs/h/h4975.md), and [rûṣ](../../strongs/h/h7323.md) [paniym](../../strongs/h/h6440.md) ['Aḥ'Āḇ](../../strongs/h/h256.md) to the [bow'](../../strongs/h/h935.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 17](1kings_17.md) - [1Kings 19](1kings_19.md)