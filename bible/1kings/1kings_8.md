# [1Kings 8](https://www.blueletterbible.org/kjv/1kings/8)

<a name="1kings_8_1"></a>1Kings 8:1

Then [Šᵊlōmô](../../strongs/h/h8010.md) [qāhal](../../strongs/h/h6950.md) the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and all the [ro'sh](../../strongs/h/h7218.md) of the [maṭṭê](../../strongs/h/h4294.md), the [nāśî'](../../strongs/h/h5387.md) of the ['ab](../../strongs/h/h1.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), unto [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), that they might [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) out of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), which is [Tsiyown](../../strongs/h/h6726.md).

<a name="1kings_8_2"></a>1Kings 8:2

And all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md) themselves unto [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) at the [ḥāḡ](../../strongs/h/h2282.md) in the [yeraḥ](../../strongs/h/h3391.md) ['Yṯnym](../../strongs/h/h388.md), which is the seventh [ḥōḏeš](../../strongs/h/h2320.md).

<a name="1kings_8_3"></a>1Kings 8:3

And all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md), and the [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md).

<a name="1kings_8_4"></a>1Kings 8:4

And they [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and all the [qodesh](../../strongs/h/h6944.md) [kĕliy](../../strongs/h/h3627.md) that were in the ['ohel](../../strongs/h/h168.md), even those did the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) [ʿālâ](../../strongs/h/h5927.md).

<a name="1kings_8_5"></a>1Kings 8:5

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), and all the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md), that were [yāʿaḏ](../../strongs/h/h3259.md) unto him, were with him [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md), [zabach](../../strongs/h/h2076.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md), that could not be [sāp̄ar](../../strongs/h/h5608.md) nor [mānâ](../../strongs/h/h4487.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="1kings_8_6"></a>1Kings 8:6

And the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) unto his [maqowm](../../strongs/h/h4725.md), into the [dᵊḇîr](../../strongs/h/h1687.md) of the [bayith](../../strongs/h/h1004.md), to the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) place, even under the [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md).

<a name="1kings_8_7"></a>1Kings 8:7

For the [kĕruwb](../../strongs/h/h3742.md) [pāraś](../../strongs/h/h6566.md) their two [kanaph](../../strongs/h/h3671.md) over the [maqowm](../../strongs/h/h4725.md) of the ['ārôn](../../strongs/h/h727.md), and the [kĕruwb](../../strongs/h/h3742.md) [cakak](../../strongs/h/h5526.md) the ['ārôn](../../strongs/h/h727.md) and the [baḏ](../../strongs/h/h905.md) thereof [maʿal](../../strongs/h/h4605.md).

<a name="1kings_8_8"></a>1Kings 8:8

And they ['arak](../../strongs/h/h748.md) the [baḏ](../../strongs/h/h905.md), that the [ro'sh](../../strongs/h/h7218.md) of the [baḏ](../../strongs/h/h905.md) were seen [ra'ah](../../strongs/h/h7200.md) in the [qodesh](../../strongs/h/h6944.md) [paniym](../../strongs/h/h6440.md) the [dᵊḇîr](../../strongs/h/h1687.md), and they were not [ra'ah](../../strongs/h/h7200.md) [ḥûṣ](../../strongs/h/h2351.md): and there they are unto this [yowm](../../strongs/h/h3117.md).

<a name="1kings_8_9"></a>1Kings 8:9

There was nothing in the ['ārôn](../../strongs/h/h727.md) save the two [lûaḥ](../../strongs/h/h3871.md) of ['eben](../../strongs/h/h68.md), which [Mōshe](../../strongs/h/h4872.md) [yānaḥ](../../strongs/h/h3240.md) there at [ḥōrēḇ](../../strongs/h/h2722.md), when [Yĕhovah](../../strongs/h/h3068.md) [karath](../../strongs/h/h3772.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), when they [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1kings_8_10"></a>1Kings 8:10

And it came to pass, when the [kōhēn](../../strongs/h/h3548.md) were [yāṣā'](../../strongs/h/h3318.md) of the [qodesh](../../strongs/h/h6944.md), that the [ʿānān](../../strongs/h/h6051.md) [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="1kings_8_11"></a>1Kings 8:11

So that the [kōhēn](../../strongs/h/h3548.md) [yakol](../../strongs/h/h3201.md) not ['amad](../../strongs/h/h5975.md) to [sharath](../../strongs/h/h8334.md) [paniym](../../strongs/h/h6440.md) of the [ʿānān](../../strongs/h/h6051.md): for the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) had [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_8_12"></a>1Kings 8:12

Then ['āmar](../../strongs/h/h559.md) [Šᵊlōmô](../../strongs/h/h8010.md), [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) that he would [shakan](../../strongs/h/h7931.md) in the ['araphel](../../strongs/h/h6205.md).

<a name="1kings_8_13"></a>1Kings 8:13

I have [bānâ](../../strongs/h/h1129.md) [bānâ](../../strongs/h/h1129.md) thee a [bayith](../../strongs/h/h1004.md) to dwell [zᵊḇûl](../../strongs/h/h2073.md), a settled [māḵôn](../../strongs/h/h4349.md) for thee to [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md).

<a name="1kings_8_14"></a>1Kings 8:14

And the [melek](../../strongs/h/h4428.md) [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md) [cabab](../../strongs/h/h5437.md), and [barak](../../strongs/h/h1288.md) all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md): (and all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md) ['amad](../../strongs/h/h5975.md);)

<a name="1kings_8_15"></a>1Kings 8:15

And he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), which [dabar](../../strongs/h/h1696.md) with his [peh](../../strongs/h/h6310.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and hath with his [yad](../../strongs/h/h3027.md) [mālā'](../../strongs/h/h4390.md) it, ['āmar](../../strongs/h/h559.md),

<a name="1kings_8_16"></a>1Kings 8:16

Since the [yowm](../../strongs/h/h3117.md) that I [yāṣā'](../../strongs/h/h3318.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md), I [bāḥar](../../strongs/h/h977.md) no [ʿîr](../../strongs/h/h5892.md) out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md), that my [shem](../../strongs/h/h8034.md) might be therein; but I [bāḥar](../../strongs/h/h977.md) [Dāviḏ](../../strongs/h/h1732.md) to be over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_8_17"></a>1Kings 8:17

And it was in the [lebab](../../strongs/h/h3824.md) of [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_8_18"></a>1Kings 8:18

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), Whereas it was in thine [lebab](../../strongs/h/h3824.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto my [shem](../../strongs/h/h8034.md), thou didst [ṭôḇ](../../strongs/h/h2895.md) that it was in thine [lebab](../../strongs/h/h3824.md).

<a name="1kings_8_19"></a>1Kings 8:19

Nevertheless thou shalt not [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md); but thy [ben](../../strongs/h/h1121.md) that shall [yāṣā'](../../strongs/h/h3318.md) of thy [ḥālāṣ](../../strongs/h/h2504.md), he shall [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) unto my [shem](../../strongs/h/h8034.md).

<a name="1kings_8_20"></a>1Kings 8:20

And [Yĕhovah](../../strongs/h/h3068.md) hath [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md) that he [dabar](../../strongs/h/h1696.md), and I am [quwm](../../strongs/h/h6965.md) in the room of [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), and have [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_8_21"></a>1Kings 8:21

And I have [śûm](../../strongs/h/h7760.md) there a [maqowm](../../strongs/h/h4725.md) for the ['ārôn](../../strongs/h/h727.md), wherein is the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [karath](../../strongs/h/h3772.md) with our ['ab](../../strongs/h/h1.md), when he [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1kings_8_22"></a>1Kings 8:22

And [Šᵊlōmô](../../strongs/h/h8010.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) in the [neḡeḏ](../../strongs/h/h5048.md) all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), and [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md) toward [shamayim](../../strongs/h/h8064.md):

<a name="1kings_8_23"></a>1Kings 8:23

And he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), there is no ['Elohiym](../../strongs/h/h430.md) like thee, in [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md), or on ['erets](../../strongs/h/h776.md) beneath, who [shamar](../../strongs/h/h8104.md) [bĕriyth](../../strongs/h/h1285.md) and [checed](../../strongs/h/h2617.md) with thy ['ebed](../../strongs/h/h5650.md) that [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) thee with all their [leb](../../strongs/h/h3820.md):

<a name="1kings_8_24"></a>1Kings 8:24

Who hast [shamar](../../strongs/h/h8104.md) with thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) that thou [dabar](../../strongs/h/h1696.md) him: thou [dabar](../../strongs/h/h1696.md) also with thy [peh](../../strongs/h/h6310.md), and hast [mālā'](../../strongs/h/h4390.md) it with thine [yad](../../strongs/h/h3027.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="1kings_8_25"></a>1Kings 8:25

Therefore now, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [shamar](../../strongs/h/h8104.md) with thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) that thou [dabar](../../strongs/h/h1696.md) him, ['āmar](../../strongs/h/h559.md), There shall not [karath](../../strongs/h/h3772.md) thee an ['iysh](../../strongs/h/h376.md) in my [paniym](../../strongs/h/h6440.md) to [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md); so that thy [ben](../../strongs/h/h1121.md) take [shamar](../../strongs/h/h8104.md) to their [derek](../../strongs/h/h1870.md), that they [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) me as thou hast [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) me.

<a name="1kings_8_26"></a>1Kings 8:26

And now, ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), let thy [dabar](../../strongs/h/h1697.md), I pray thee, be ['aman](../../strongs/h/h539.md), which thou [dabar](../../strongs/h/h1696.md) unto thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md).

<a name="1kings_8_27"></a>1Kings 8:27

But will ['Elohiym](../../strongs/h/h430.md) ['umnām](../../strongs/h/h552.md) [yashab](../../strongs/h/h3427.md) on the ['erets](../../strongs/h/h776.md)? behold, the [shamayim](../../strongs/h/h8064.md) and [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md) cannot [kûl](../../strongs/h/h3557.md) thee; how much less this [bayith](../../strongs/h/h1004.md) that I have [bānâ](../../strongs/h/h1129.md)?

<a name="1kings_8_28"></a>1Kings 8:28

Yet have thou [panah](../../strongs/h/h6437.md) unto the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), and to his [tĕchinnah](../../strongs/h/h8467.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), to [shama'](../../strongs/h/h8085.md) unto the [rinnah](../../strongs/h/h7440.md) and to the [tĕphillah](../../strongs/h/h8605.md), which thy ['ebed](../../strongs/h/h5650.md) [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) thee to [yowm](../../strongs/h/h3117.md):

<a name="1kings_8_29"></a>1Kings 8:29

That thine ['ayin](../../strongs/h/h5869.md) may be [pāṯaḥ](../../strongs/h/h6605.md) toward this [bayith](../../strongs/h/h1004.md) [layil](../../strongs/h/h3915.md) and [yowm](../../strongs/h/h3117.md), even toward the [maqowm](../../strongs/h/h4725.md) of which thou hast ['āmar](../../strongs/h/h559.md), My [shem](../../strongs/h/h8034.md) shall be there: that thou mayest [shama'](../../strongs/h/h8085.md) unto the [tĕphillah](../../strongs/h/h8605.md) which thy ['ebed](../../strongs/h/h5650.md) shall [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md).

<a name="1kings_8_30"></a>1Kings 8:30

And [shama'](../../strongs/h/h8085.md) thou to the [tĕchinnah](../../strongs/h/h8467.md) of thy ['ebed](../../strongs/h/h5650.md), and of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), when they shall [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md): and [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md) thy [yashab](../../strongs/h/h3427.md) [maqowm](../../strongs/h/h4725.md): and when thou [shama'](../../strongs/h/h8085.md), [sālaḥ](../../strongs/h/h5545.md).

<a name="1kings_8_31"></a>1Kings 8:31

If any ['iysh](../../strongs/h/h376.md) [chata'](../../strongs/h/h2398.md) against his [rea'](../../strongs/h/h7453.md), and an ['alah](../../strongs/h/h423.md) be [nasa'](../../strongs/h/h5375.md) upon him to cause him to ['ālâ](../../strongs/h/h422.md), and the ['alah](../../strongs/h/h423.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thine [mizbeach](../../strongs/h/h4196.md) in this [bayith](../../strongs/h/h1004.md):

<a name="1kings_8_32"></a>1Kings 8:32

Then [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md), and ['asah](../../strongs/h/h6213.md), and [shaphat](../../strongs/h/h8199.md) thy ['ebed](../../strongs/h/h5650.md), [rāšaʿ](../../strongs/h/h7561.md) the [rasha'](../../strongs/h/h7563.md), to [nathan](../../strongs/h/h5414.md) his [derek](../../strongs/h/h1870.md) upon his [ro'sh](../../strongs/h/h7218.md); and [ṣāḏaq](../../strongs/h/h6663.md) the [tsaddiyq](../../strongs/h/h6662.md), to [nathan](../../strongs/h/h5414.md) him according to his [tsedaqah](../../strongs/h/h6666.md).

<a name="1kings_8_33"></a>1Kings 8:33

When thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) be [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md), because they have [chata'](../../strongs/h/h2398.md) against thee, and shall [shuwb](../../strongs/h/h7725.md) to thee, and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md), and [palal](../../strongs/h/h6419.md), and make [chanan](../../strongs/h/h2603.md) unto thee in this [bayith](../../strongs/h/h1004.md):

<a name="1kings_8_34"></a>1Kings 8:34

Then [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md), and [sālaḥ](../../strongs/h/h5545.md) the [chatta'ath](../../strongs/h/h2403.md) of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and bring them [shuwb](../../strongs/h/h7725.md) unto the ['ăḏāmâ](../../strongs/h/h127.md) which thou [nathan](../../strongs/h/h5414.md) unto their ['ab](../../strongs/h/h1.md).

<a name="1kings_8_35"></a>1Kings 8:35

When [shamayim](../../strongs/h/h8064.md) is [ʿāṣar](../../strongs/h/h6113.md), and there is no [māṭār](../../strongs/h/h4306.md), because they have [chata'](../../strongs/h/h2398.md) against thee; if they [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md), and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md), and [shuwb](../../strongs/h/h7725.md) from their [chatta'ath](../../strongs/h/h2403.md), when thou [ʿānâ](../../strongs/h/h6031.md) them:

<a name="1kings_8_36"></a>1Kings 8:36

Then [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md), and [sālaḥ](../../strongs/h/h5545.md) the [chatta'ath](../../strongs/h/h2403.md) of thy ['ebed](../../strongs/h/h5650.md), and of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), that thou [yārâ](../../strongs/h/h3384.md) them the [towb](../../strongs/h/h2896.md) [derek](../../strongs/h/h1870.md) wherein they should [yālaḵ](../../strongs/h/h3212.md), and [nathan](../../strongs/h/h5414.md) [māṭār](../../strongs/h/h4306.md) upon thy ['erets](../../strongs/h/h776.md), which thou hast [nathan](../../strongs/h/h5414.md) to thy ['am](../../strongs/h/h5971.md) for a [nachalah](../../strongs/h/h5159.md).

<a name="1kings_8_37"></a>1Kings 8:37

If there be in the ['erets](../../strongs/h/h776.md) [rāʿāḇ](../../strongs/h/h7458.md), if there be [deḇer](../../strongs/h/h1698.md), [šᵊḏēp̄â](../../strongs/h/h7711.md), [yērāqôn](../../strongs/h/h3420.md), ['arbê](../../strongs/h/h697.md), or if there be [ḥāsîl](../../strongs/h/h2625.md); if their ['oyeb](../../strongs/h/h341.md) [tsarar](../../strongs/h/h6887.md) them in the ['erets](../../strongs/h/h776.md) of their [sha'ar](../../strongs/h/h8179.md); whatsoever [neḡaʿ](../../strongs/h/h5061.md), whatsoever [maḥălê](../../strongs/h/h4245.md) there be;

<a name="1kings_8_38"></a>1Kings 8:38

What [tĕphillah](../../strongs/h/h8605.md) and [tĕchinnah](../../strongs/h/h8467.md) soever be made by any ['āḏām](../../strongs/h/h120.md), or by all thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), which shall [yada'](../../strongs/h/h3045.md) every ['iysh](../../strongs/h/h376.md) the [neḡaʿ](../../strongs/h/h5061.md) of his own [lebab](../../strongs/h/h3824.md), and [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md) toward this [bayith](../../strongs/h/h1004.md):

<a name="1kings_8_39"></a>1Kings 8:39

Then [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md) thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), and [sālaḥ](../../strongs/h/h5545.md), and ['asah](../../strongs/h/h6213.md), and [nathan](../../strongs/h/h5414.md) to every ['iysh](../../strongs/h/h376.md) according to his [derek](../../strongs/h/h1870.md), whose [lebab](../../strongs/h/h3824.md) thou [yada'](../../strongs/h/h3045.md); (for thou, even thou only, [yada'](../../strongs/h/h3045.md) the [lebab](../../strongs/h/h3824.md) of all the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md);)

<a name="1kings_8_40"></a>1Kings 8:40

That they may [yare'](../../strongs/h/h3372.md) thee all the [yowm](../../strongs/h/h3117.md) that they [chay](../../strongs/h/h2416.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md) which thou [nathan](../../strongs/h/h5414.md) unto our ['ab](../../strongs/h/h1.md).

<a name="1kings_8_41"></a>1Kings 8:41

Moreover concerning a [nāḵrî](../../strongs/h/h5237.md), that is not of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), but [bow'](../../strongs/h/h935.md) of a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md) for thy [shem](../../strongs/h/h8034.md) sake;

<a name="1kings_8_42"></a>1Kings 8:42

(For they shall [shama'](../../strongs/h/h8085.md) of thy [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md), and of thy [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and of thy [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md);) when he shall [bow'](../../strongs/h/h935.md) and [palal](../../strongs/h/h6419.md) toward this [bayith](../../strongs/h/h1004.md);

<a name="1kings_8_43"></a>1Kings 8:43

[shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md) thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), and ['asah](../../strongs/h/h6213.md) according to all that the [nāḵrî](../../strongs/h/h5237.md) [qara'](../../strongs/h/h7121.md) to thee for: that all ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) may [yada'](../../strongs/h/h3045.md) thy [shem](../../strongs/h/h8034.md), to [yare'](../../strongs/h/h3372.md) thee, as do thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md); and that they may [yada'](../../strongs/h/h3045.md) that this [bayith](../../strongs/h/h1004.md), which I have [bānâ](../../strongs/h/h1129.md), is [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md).

<a name="1kings_8_44"></a>1Kings 8:44

If thy ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against their ['oyeb](../../strongs/h/h341.md), [derek](../../strongs/h/h1870.md) thou shalt [shalach](../../strongs/h/h7971.md) them, and shall [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md) [derek](../../strongs/h/h1870.md) the [ʿîr](../../strongs/h/h5892.md) which thou hast [bāḥar](../../strongs/h/h977.md), and the [bayith](../../strongs/h/h1004.md) that I have [bānâ](../../strongs/h/h1129.md) for thy [shem](../../strongs/h/h8034.md):

<a name="1kings_8_45"></a>1Kings 8:45

Then [shama'](../../strongs/h/h8085.md) thou in [shamayim](../../strongs/h/h8064.md) their [tĕphillah](../../strongs/h/h8605.md) and their [tĕchinnah](../../strongs/h/h8467.md), and ['asah](../../strongs/h/h6213.md) their [mishpat](../../strongs/h/h4941.md).

<a name="1kings_8_46"></a>1Kings 8:46

If they [chata'](../../strongs/h/h2398.md) against thee, (for there is no ['āḏām](../../strongs/h/h120.md) that [chata'](../../strongs/h/h2398.md) not,) and thou be ['anaph](../../strongs/h/h599.md) with them, and [nathan](../../strongs/h/h5414.md) them to the ['oyeb](../../strongs/h/h341.md), so that they [šāḇâ](../../strongs/h/h7617.md) them [šāḇâ](../../strongs/h/h7617.md) [paniym](../../strongs/h/h6440.md) the ['erets](../../strongs/h/h776.md) of the ['oyeb](../../strongs/h/h341.md), [rachowq](../../strongs/h/h7350.md) or [qarowb](../../strongs/h/h7138.md);

<a name="1kings_8_47"></a>1Kings 8:47

Yet if they shall [shuwb](../../strongs/h/h7725.md) themselves in the ['erets](../../strongs/h/h776.md) whither they were [šāḇâ](../../strongs/h/h7617.md), and [shuwb](../../strongs/h/h7725.md) [leb](../../strongs/h/h3820.md), and make [chanan](../../strongs/h/h2603.md) unto thee in the ['erets](../../strongs/h/h776.md) of them that [šāḇâ](../../strongs/h/h7617.md) them, ['āmar](../../strongs/h/h559.md), We have [chata'](../../strongs/h/h2398.md), and have done [ʿāvâ](../../strongs/h/h5753.md), we have committed [rāšaʿ](../../strongs/h/h7561.md);

<a name="1kings_8_48"></a>1Kings 8:48

And so [shuwb](../../strongs/h/h7725.md) unto thee with all their [lebab](../../strongs/h/h3824.md), and with all their [nephesh](../../strongs/h/h5315.md), in the ['erets](../../strongs/h/h776.md) of their ['oyeb](../../strongs/h/h341.md), which led them away [šāḇâ](../../strongs/h/h7617.md), and [palal](../../strongs/h/h6419.md) unto thee [derek](../../strongs/h/h1870.md) their ['erets](../../strongs/h/h776.md), which thou [nathan](../../strongs/h/h5414.md) unto their ['ab](../../strongs/h/h1.md), the [ʿîr](../../strongs/h/h5892.md) which thou hast [bāḥar](../../strongs/h/h977.md), and the [bayith](../../strongs/h/h1004.md) which I have [bānâ](../../strongs/h/h1129.md) for thy [shem](../../strongs/h/h8034.md):

<a name="1kings_8_49"></a>1Kings 8:49

Then [shama'](../../strongs/h/h8085.md) thou their [tĕphillah](../../strongs/h/h8605.md) and their [tĕchinnah](../../strongs/h/h8467.md) in [shamayim](../../strongs/h/h8064.md) thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), and ['asah](../../strongs/h/h6213.md) their [mishpat](../../strongs/h/h4941.md),

<a name="1kings_8_50"></a>1Kings 8:50

And [sālaḥ](../../strongs/h/h5545.md) thy ['am](../../strongs/h/h5971.md) that have [chata'](../../strongs/h/h2398.md) against thee, and all their [pesha'](../../strongs/h/h6588.md) wherein they have [pāšaʿ](../../strongs/h/h6586.md) against thee, and [nathan](../../strongs/h/h5414.md) them [raḥam](../../strongs/h/h7356.md) [paniym](../../strongs/h/h6440.md) them who carried them [šāḇâ](../../strongs/h/h7617.md), that they may have [racham](../../strongs/h/h7355.md) on them:

<a name="1kings_8_51"></a>1Kings 8:51

For they be thy ['am](../../strongs/h/h5971.md), and thine [nachalah](../../strongs/h/h5159.md), which thou [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md), from the midst of the [kûr](../../strongs/h/h3564.md) of [barzel](../../strongs/h/h1270.md):

<a name="1kings_8_52"></a>1Kings 8:52

That thine ['ayin](../../strongs/h/h5869.md) may be [pāṯaḥ](../../strongs/h/h6605.md) unto the [tĕchinnah](../../strongs/h/h8467.md) of thy ['ebed](../../strongs/h/h5650.md), and unto the [tĕchinnah](../../strongs/h/h8467.md) of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), to [shama'](../../strongs/h/h8085.md) unto them in all that they [qara'](../../strongs/h/h7121.md) for unto thee.

<a name="1kings_8_53"></a>1Kings 8:53

For thou didst [bāḏal](../../strongs/h/h914.md) them from among all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), to be thine [nachalah](../../strongs/h/h5159.md), as thou [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) thy ['ebed](../../strongs/h/h5650.md), when thou [yāṣā'](../../strongs/h/h3318.md) our ['ab](../../strongs/h/h1.md) out of [Mitsrayim](../../strongs/h/h4714.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="1kings_8_54"></a>1Kings 8:54

And it was so, that when [Šᵊlōmô](../../strongs/h/h8010.md) had made a [kalah](../../strongs/h/h3615.md) of [palal](../../strongs/h/h6419.md) all this [tĕphillah](../../strongs/h/h8605.md) and [tĕchinnah](../../strongs/h/h8467.md) unto [Yĕhovah](../../strongs/h/h3068.md), he [quwm](../../strongs/h/h6965.md) from [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), from [kara'](../../strongs/h/h3766.md) on his [bereḵ](../../strongs/h/h1290.md) with his [kaph](../../strongs/h/h3709.md) [pāraś](../../strongs/h/h6566.md) to [shamayim](../../strongs/h/h8064.md).

<a name="1kings_8_55"></a>1Kings 8:55

And he ['amad](../../strongs/h/h5975.md), and [barak](../../strongs/h/h1288.md) all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_8_56"></a>1Kings 8:56

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md), that hath [nathan](../../strongs/h/h5414.md) [mᵊnûḥâ](../../strongs/h/h4496.md) unto his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), according to all that he [dabar](../../strongs/h/h1696.md): there hath not [naphal](../../strongs/h/h5307.md) one [dabar](../../strongs/h/h1697.md) of all his [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md), which he [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) his ['ebed](../../strongs/h/h5650.md).

<a name="1kings_8_57"></a>1Kings 8:57

[Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) be with us, as he was with our ['ab](../../strongs/h/h1.md): let him not ['azab](../../strongs/h/h5800.md) us, nor [nāṭaš](../../strongs/h/h5203.md) us:

<a name="1kings_8_58"></a>1Kings 8:58

That he may [natah](../../strongs/h/h5186.md) our [lebab](../../strongs/h/h3824.md) unto him, to [yālaḵ](../../strongs/h/h3212.md) in all his [derek](../../strongs/h/h1870.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), and his [choq](../../strongs/h/h2706.md), and his [mishpat](../../strongs/h/h4941.md), which he [tsavah](../../strongs/h/h6680.md) our ['ab](../../strongs/h/h1.md).

<a name="1kings_8_59"></a>1Kings 8:59

And let these my [dabar](../../strongs/h/h1697.md), wherewith I have made [chanan](../../strongs/h/h2603.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), be [qarowb](../../strongs/h/h7138.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), that he ['asah](../../strongs/h/h6213.md) the [mishpat](../../strongs/h/h4941.md) of his ['ebed](../../strongs/h/h5650.md), and the [mishpat](../../strongs/h/h4941.md) of his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) at all [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md), as the [dabar](../../strongs/h/h1697.md) shall require:

<a name="1kings_8_60"></a>1Kings 8:60

That all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) may [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) is ['Elohiym](../../strongs/h/h430.md), and that there is none else.

<a name="1kings_8_61"></a>1Kings 8:61

Let your [lebab](../../strongs/h/h3824.md) therefore be [šālēm](../../strongs/h/h8003.md) with [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in his [choq](../../strongs/h/h2706.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), as at this [yowm](../../strongs/h/h3117.md).

<a name="1kings_8_62"></a>1Kings 8:62

And the [melek](../../strongs/h/h4428.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_8_63"></a>1Kings 8:63

And [Šᵊlōmô](../../strongs/h/h8010.md) [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), which he [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md), two and twenty thousand [bāqār](../../strongs/h/h1241.md), and an hundred and twenty thousand [tso'n](../../strongs/h/h6629.md). So the [melek](../../strongs/h/h4428.md) and all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥānaḵ](../../strongs/h/h2596.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_8_64"></a>1Kings 8:64

The same [yowm](../../strongs/h/h3117.md) did the [melek](../../strongs/h/h4428.md) [qadash](../../strongs/h/h6942.md) the middle of the [ḥāṣēr](../../strongs/h/h2691.md) that was [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): for there he ['asah](../../strongs/h/h6213.md) [ʿōlâ](../../strongs/h/h5930.md), and [minchah](../../strongs/h/h4503.md), and the [cheleb](../../strongs/h/h2459.md) of the [šelem](../../strongs/h/h8002.md): because the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md) that was [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) was too [qāṭān](../../strongs/h/h6996.md) to [kûl](../../strongs/h/h3557.md) the [ʿōlâ](../../strongs/h/h5930.md), and [minchah](../../strongs/h/h4503.md), and the [cheleb](../../strongs/h/h2459.md) of the [šelem](../../strongs/h/h8002.md).

<a name="1kings_8_65"></a>1Kings 8:65

And at that [ʿēṯ](../../strongs/h/h6256.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) a [ḥāḡ](../../strongs/h/h2282.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, a [gadowl](../../strongs/h/h1419.md) [qāhēl](../../strongs/h/h6951.md), from the [bow'](../../strongs/h/h935.md) in of [Ḥămāṯ](../../strongs/h/h2574.md) unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md), [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), seven [yowm](../../strongs/h/h3117.md) and seven [yowm](../../strongs/h/h3117.md), even fourteen [yowm](../../strongs/h/h3117.md).

<a name="1kings_8_66"></a>1Kings 8:66

On the eighth [yowm](../../strongs/h/h3117.md) he [shalach](../../strongs/h/h7971.md) the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md): and they [barak](../../strongs/h/h1288.md) the [melek](../../strongs/h/h4428.md), and [yālaḵ](../../strongs/h/h3212.md) unto their ['ohel](../../strongs/h/h168.md) [śāmēaḥ](../../strongs/h/h8056.md) and [towb](../../strongs/h/h2896.md) of [leb](../../strongs/h/h3820.md) for all the [towb](../../strongs/h/h2896.md) that [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) for [Dāviḏ](../../strongs/h/h1732.md) his ['ebed](../../strongs/h/h5650.md), and for [Yisra'el](../../strongs/h/h3478.md) his ['am](../../strongs/h/h5971.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 7](1kings_7.md) - [1Kings 9](1kings_9.md)