# [1Kings 12](https://www.blueletterbible.org/kjv/1kings/12)

<a name="1kings_12_1"></a>1Kings 12:1

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [yālaḵ](../../strongs/h/h3212.md) to [Šᵊḵem](../../strongs/h/h7927.md): for all [Yisra'el](../../strongs/h/h3478.md) were [bow'](../../strongs/h/h935.md) to [Šᵊḵem](../../strongs/h/h7927.md) to make him [mālaḵ](../../strongs/h/h4427.md).

<a name="1kings_12_2"></a>1Kings 12:2

And it came to pass, when [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who was yet in [Mitsrayim](../../strongs/h/h4714.md), [shama'](../../strongs/h/h8085.md) of it, (for he was [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), and [YārāḇʿĀm](../../strongs/h/h3379.md) [yashab](../../strongs/h/h3427.md) in [Mitsrayim](../../strongs/h/h4714.md);)

<a name="1kings_12_3"></a>1Kings 12:3

That they [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) him. And [YārāḇʿĀm](../../strongs/h/h3379.md) and all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md), and [dabar](../../strongs/h/h1696.md) unto [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_12_4"></a>1Kings 12:4

Thy ['ab](../../strongs/h/h1.md) [qāšâ](../../strongs/h/h7185.md) our [ʿōl](../../strongs/h/h5923.md) [qāšâ](../../strongs/h/h7185.md): now therefore make thou the [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md) of thy ['ab](../../strongs/h/h1.md), and his [kāḇēḏ](../../strongs/h/h3515.md) [ʿōl](../../strongs/h/h5923.md) which he [nathan](../../strongs/h/h5414.md) upon us, [qālal](../../strongs/h/h7043.md), and we will ['abad](../../strongs/h/h5647.md) thee.

<a name="1kings_12_5"></a>1Kings 12:5

And he ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md) yet for three [yowm](../../strongs/h/h3117.md), then [shuwb](../../strongs/h/h7725.md) to me. And the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md).

<a name="1kings_12_6"></a>1Kings 12:6

And [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [ya'ats](../../strongs/h/h3289.md) with the [zāqēn](../../strongs/h/h2205.md), that ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Šᵊlōmô](../../strongs/h/h8010.md) his ['ab](../../strongs/h/h1.md) while he yet [chay](../../strongs/h/h2416.md), and ['āmar](../../strongs/h/h559.md), How do ye [ya'ats](../../strongs/h/h3289.md) that I [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) this ['am](../../strongs/h/h5971.md)?

<a name="1kings_12_7"></a>1Kings 12:7

And they [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), If thou wilt be an ['ebed](../../strongs/h/h5650.md) unto this ['am](../../strongs/h/h5971.md) this [yowm](../../strongs/h/h3117.md), and wilt ['abad](../../strongs/h/h5647.md) them, and ['anah](../../strongs/h/h6030.md) them, and [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) to them, then they will be thy ['ebed](../../strongs/h/h5650.md) for [yowm](../../strongs/h/h3117.md).

<a name="1kings_12_8"></a>1Kings 12:8

But he ['azab](../../strongs/h/h5800.md) the ['etsah](../../strongs/h/h6098.md) of the [zāqēn](../../strongs/h/h2205.md), which they had [ya'ats](../../strongs/h/h3289.md) him, and [ya'ats](../../strongs/h/h3289.md) with the [yeleḏ](../../strongs/h/h3206.md) that were [gāḏal](../../strongs/h/h1431.md) with him, and which ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him:

<a name="1kings_12_9"></a>1Kings 12:9

And he ['āmar](../../strongs/h/h559.md) unto them, What [ya'ats](../../strongs/h/h3289.md) ye that we [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) this ['am](../../strongs/h/h5971.md), who have [dabar](../../strongs/h/h1696.md) to me, ['āmar](../../strongs/h/h559.md), [qālal](../../strongs/h/h7043.md) the [ʿōl](../../strongs/h/h5923.md) which thy ['ab](../../strongs/h/h1.md) did [nathan](../../strongs/h/h5414.md) upon us [qālal](../../strongs/h/h7043.md)?

<a name="1kings_12_10"></a>1Kings 12:10

And the [yeleḏ](../../strongs/h/h3206.md) that were [gāḏal](../../strongs/h/h1431.md) with him [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) unto this ['am](../../strongs/h/h5971.md) that [dabar](../../strongs/h/h1696.md) unto thee, ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) [kabad](../../strongs/h/h3513.md) our [ʿōl](../../strongs/h/h5923.md) [kabad](../../strongs/h/h3513.md), but make thou it [qālal](../../strongs/h/h7043.md) unto us; thus shalt thou [dabar](../../strongs/h/h1696.md) unto them, My [qōṭen](../../strongs/h/h6995.md) shall be [ʿāḇâ](../../strongs/h/h5666.md) than my ['ab](../../strongs/h/h1.md) [māṯnayim](../../strongs/h/h4975.md).

<a name="1kings_12_11"></a>1Kings 12:11

And now whereas my ['ab](../../strongs/h/h1.md) did [ʿāmas](../../strongs/h/h6006.md) you with a [kāḇēḏ](../../strongs/h/h3515.md) [ʿōl](../../strongs/h/h5923.md), I will add to your [ʿōl](../../strongs/h/h5923.md): my ['ab](../../strongs/h/h1.md) hath [yacar](../../strongs/h/h3256.md) you with [šôṭ](../../strongs/h/h7752.md), but I will [yacar](../../strongs/h/h3256.md) you with [ʿaqrāḇ](../../strongs/h/h6137.md).

<a name="1kings_12_12"></a>1Kings 12:12

So [YārāḇʿĀm](../../strongs/h/h3379.md) and all the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the third [yowm](../../strongs/h/h3117.md), as the [melek](../../strongs/h/h4428.md) had [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), Come to me [shuwb](../../strongs/h/h7725.md) the third [yowm](../../strongs/h/h3117.md).

<a name="1kings_12_13"></a>1Kings 12:13

And the [melek](../../strongs/h/h4428.md) ['anah](../../strongs/h/h6030.md) the ['am](../../strongs/h/h5971.md) [qāšê](../../strongs/h/h7186.md), and ['azab](../../strongs/h/h5800.md) the [zāqēn](../../strongs/h/h2205.md) ['etsah](../../strongs/h/h6098.md) that they [ya'ats](../../strongs/h/h3289.md) him;

<a name="1kings_12_14"></a>1Kings 12:14

And [dabar](../../strongs/h/h1696.md) to them after the ['etsah](../../strongs/h/h6098.md) of the [yeleḏ](../../strongs/h/h3206.md), ['āmar](../../strongs/h/h559.md), My ['ab](../../strongs/h/h1.md) [kabad](../../strongs/h/h3513.md) your [ʿōl](../../strongs/h/h5923.md) [kabad](../../strongs/h/h3513.md), and I will add to your [ʿōl](../../strongs/h/h5923.md): my ['ab](../../strongs/h/h1.md) also [yacar](../../strongs/h/h3256.md) you with [šôṭ](../../strongs/h/h7752.md), but I will [yacar](../../strongs/h/h3256.md) you with [ʿaqrāḇ](../../strongs/h/h6137.md).

<a name="1kings_12_15"></a>1Kings 12:15

Wherefore the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) not unto the ['am](../../strongs/h/h5971.md); for the [sibâ](../../strongs/h/h5438.md) was from [Yĕhovah](../../strongs/h/h3068.md), that he might [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md), which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) ['Ăḥîyâ](../../strongs/h/h281.md) the [Šîlōnî](../../strongs/h/h7888.md) unto [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md).

<a name="1kings_12_16"></a>1Kings 12:16

[shuwb](../../strongs/h/h7725.md) when all [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) not unto them, the ['am](../../strongs/h/h5971.md) [dabar](../../strongs/h/h1697.md) the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), What [cheleq](../../strongs/h/h2506.md) have we in [Dāviḏ](../../strongs/h/h1732.md)? neither have we [nachalah](../../strongs/h/h5159.md) in the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md): to your ['ohel](../../strongs/h/h168.md), O [Yisra'el](../../strongs/h/h3478.md): now [ra'ah](../../strongs/h/h7200.md) to thine own [bayith](../../strongs/h/h1004.md), [Dāviḏ](../../strongs/h/h1732.md). So [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) unto their ['ohel](../../strongs/h/h168.md).

<a name="1kings_12_17"></a>1Kings 12:17

But as for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) which [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [mālaḵ](../../strongs/h/h4427.md) over them.

<a name="1kings_12_18"></a>1Kings 12:18

Then [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [shalach](../../strongs/h/h7971.md) ['Ăḏōrām](../../strongs/h/h151.md), who was over the [mas](../../strongs/h/h4522.md); and all [Yisra'el](../../strongs/h/h3478.md) [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md), that he [muwth](../../strongs/h/h4191.md). Therefore [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) made ['amats](../../strongs/h/h553.md) to get him [ʿālâ](../../strongs/h/h5927.md) to his [merkāḇâ](../../strongs/h/h4818.md), to [nûs](../../strongs/h/h5127.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="1kings_12_19"></a>1Kings 12:19

So [Yisra'el](../../strongs/h/h3478.md) [pāšaʿ](../../strongs/h/h6586.md) against the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="1kings_12_20"></a>1Kings 12:20

And it came to pass, when all [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) that [YārāḇʿĀm](../../strongs/h/h3379.md) was [shuwb](../../strongs/h/h7725.md), that they [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) him unto the ['edah](../../strongs/h/h5712.md), and made him [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md): there was none that ['aḥar](../../strongs/h/h310.md) the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), [zûlâ](../../strongs/h/h2108.md) the [shebet](../../strongs/h/h7626.md) of [Yehuwdah](../../strongs/h/h3063.md) only.

<a name="1kings_12_21"></a>1Kings 12:21

And when [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) was [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), he [qāhal](../../strongs/h/h6950.md) all the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), with the [shebet](../../strongs/h/h7626.md) of [Binyāmîn](../../strongs/h/h1144.md), an hundred and fourscore thousand chosen [bāḥar](../../strongs/h/h977.md), which were ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md), to [lāḥam](../../strongs/h/h3898.md) against the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), to [shuwb](../../strongs/h/h7725.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) [shuwb](../../strongs/h/h7725.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="1kings_12_22"></a>1Kings 12:22

But the [dabar](../../strongs/h/h1697.md) of ['Elohiym](../../strongs/h/h430.md) came unto [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_12_23"></a>1Kings 12:23

['āmar](../../strongs/h/h559.md) unto [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and unto all the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), and to the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_12_24"></a>1Kings 12:24

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Ye shall not [ʿālâ](../../strongs/h/h5927.md), nor [lāḥam](../../strongs/h/h3898.md) against your ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md); for this [dabar](../../strongs/h/h1697.md) is from me. They [shama'](../../strongs/h/h8085.md) therefore to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and [shuwb](../../strongs/h/h7725.md) to [yālaḵ](../../strongs/h/h3212.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_12_25"></a>1Kings 12:25

Then [YārāḇʿĀm](../../strongs/h/h3379.md) [bānâ](../../strongs/h/h1129.md) [Šᵊḵem](../../strongs/h/h7927.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and [yashab](../../strongs/h/h3427.md) therein; and [yāṣā'](../../strongs/h/h3318.md) from thence, and [bānâ](../../strongs/h/h1129.md) [Pᵊnû'ēl](../../strongs/h/h6439.md).

<a name="1kings_12_26"></a>1Kings 12:26

And [YārāḇʿĀm](../../strongs/h/h3379.md) ['āmar](../../strongs/h/h559.md) in his [leb](../../strongs/h/h3820.md), Now shall the [mamlāḵâ](../../strongs/h/h4467.md) [shuwb](../../strongs/h/h7725.md) to the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md):

<a name="1kings_12_27"></a>1Kings 12:27

If this ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md) to ['asah](../../strongs/h/h6213.md) [zebach](../../strongs/h/h2077.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), then shall the [leb](../../strongs/h/h3820.md) of this ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) unto their ['adown](../../strongs/h/h113.md), even unto [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and they shall [harag](../../strongs/h/h2026.md) me, and [shuwb](../../strongs/h/h7725.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_12_28"></a>1Kings 12:28

Whereupon the [melek](../../strongs/h/h4428.md) took [ya'ats](../../strongs/h/h3289.md), and ['asah](../../strongs/h/h6213.md) two [ʿēḡel](../../strongs/h/h5695.md) of [zāhāḇ](../../strongs/h/h2091.md), and ['āmar](../../strongs/h/h559.md) unto them, It is too [rab](../../strongs/h/h7227.md) for you to [ʿālâ](../../strongs/h/h5927.md) to [Yĕruwshalaim](../../strongs/h/h3389.md): behold thy ['Elohiym](../../strongs/h/h430.md), O [Yisra'el](../../strongs/h/h3478.md), which brought thee [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="1kings_12_29"></a>1Kings 12:29

And he [śûm](../../strongs/h/h7760.md) the one in [Bêṯ-'ēl](../../strongs/h/h1008.md), and the other [nathan](../../strongs/h/h5414.md) he in [Dān](../../strongs/h/h1835.md).

<a name="1kings_12_30"></a>1Kings 12:30

And this [dabar](../../strongs/h/h1697.md) became a [chatta'ath](../../strongs/h/h2403.md): for the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) the one, even unto [Dān](../../strongs/h/h1835.md).

<a name="1kings_12_31"></a>1Kings 12:31

And he ['asah](../../strongs/h/h6213.md) a [bayith](../../strongs/h/h1004.md) of [bāmâ](../../strongs/h/h1116.md), and ['asah](../../strongs/h/h6213.md) [kōhēn](../../strongs/h/h3548.md) of the [qāṣâ](../../strongs/h/h7098.md) of the ['am](../../strongs/h/h5971.md), which were not of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md).

<a name="1kings_12_32"></a>1Kings 12:32

And [YārāḇʿĀm](../../strongs/h/h3379.md) ['asah](../../strongs/h/h6213.md) a [ḥāḡ](../../strongs/h/h2282.md) in the eighth [ḥōḏeš](../../strongs/h/h2320.md), on the fifteenth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), like unto the [ḥāḡ](../../strongs/h/h2282.md) that is in [Yehuwdah](../../strongs/h/h3063.md), and he [ʿālâ](../../strongs/h/h5927.md) upon the [mizbeach](../../strongs/h/h4196.md). So ['asah](../../strongs/h/h6213.md) he in [Bêṯ-'ēl](../../strongs/h/h1008.md), [zabach](../../strongs/h/h2076.md) unto the [ʿēḡel](../../strongs/h/h5695.md) that he had ['asah](../../strongs/h/h6213.md): and he ['amad](../../strongs/h/h5975.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md) the [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md) which he had ['asah](../../strongs/h/h6213.md).

<a name="1kings_12_33"></a>1Kings 12:33

So he [ʿālâ](../../strongs/h/h5927.md) upon the [mizbeach](../../strongs/h/h4196.md) which he had ['asah](../../strongs/h/h6213.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md) the fifteenth [yowm](../../strongs/h/h3117.md) of the eighth [ḥōḏeš](../../strongs/h/h2320.md), even in the [ḥōḏeš](../../strongs/h/h2320.md) which he had [bāḏā'](../../strongs/h/h908.md) of his own [leb](../../strongs/h/h3820.md); and ['asah](../../strongs/h/h6213.md) a [ḥāḡ](../../strongs/h/h2282.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): and he [ʿālâ](../../strongs/h/h5927.md) upon the [mizbeach](../../strongs/h/h4196.md), and [qāṭar](../../strongs/h/h6999.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 11](1kings_11.md) - [1Kings 13](1kings_13.md)