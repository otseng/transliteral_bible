# [1Kings 5](https://www.blueletterbible.org/kjv/1kings/5)

<a name="1kings_5_1"></a>1Kings 5:1

And [Ḥîrām](../../strongs/h/h2438.md) [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md) [shalach](../../strongs/h/h7971.md) his ['ebed](../../strongs/h/h5650.md) unto [Šᵊlōmô](../../strongs/h/h8010.md); for he had [shama'](../../strongs/h/h8085.md) that they had [māšaḥ](../../strongs/h/h4886.md) him [melek](../../strongs/h/h4428.md) in the room of his ['ab](../../strongs/h/h1.md): for [Ḥîrām](../../strongs/h/h2438.md) was [yowm](../../strongs/h/h3117.md) an ['ahab](../../strongs/h/h157.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="1kings_5_2"></a>1Kings 5:2

And [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md) to [Ḥîrām](../../strongs/h/h2438.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_5_3"></a>1Kings 5:3

Thou [yada'](../../strongs/h/h3045.md) how that [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) [yakol](../../strongs/h/h3201.md) not [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) [paniym](../../strongs/h/h6440.md) the [milḥāmâ](../../strongs/h/h4421.md) which were about him on every [cabab](../../strongs/h/h5437.md), until [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them under the [kaph](../../strongs/h/h3709.md) of his [regel](../../strongs/h/h7272.md).

<a name="1kings_5_4"></a>1Kings 5:4

But now [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) hath given me [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md), so that there is neither [satan](../../strongs/h/h7854.md) nor [ra'](../../strongs/h/h7451.md) [peḡaʿ](../../strongs/h/h6294.md).

<a name="1kings_5_5"></a>1Kings 5:5

And, behold, I ['āmar](../../strongs/h/h559.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), Thy [ben](../../strongs/h/h1121.md), whom I will [nathan](../../strongs/h/h5414.md) upon thy [kicce'](../../strongs/h/h3678.md) in thy room, he shall [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) unto my [shem](../../strongs/h/h8034.md).

<a name="1kings_5_6"></a>1Kings 5:6

Now therefore [tsavah](../../strongs/h/h6680.md) thou that they [karath](../../strongs/h/h3772.md) me cedar ['erez](../../strongs/h/h730.md) out of [Lᵊḇānôn](../../strongs/h/h3844.md); and my ['ebed](../../strongs/h/h5650.md) shall be with thy ['ebed](../../strongs/h/h5650.md): and unto thee will I [nathan](../../strongs/h/h5414.md) [śāḵār](../../strongs/h/h7939.md) for thy ['ebed](../../strongs/h/h5650.md) according to all that thou shalt ['āmar](../../strongs/h/h559.md): for thou [yada'](../../strongs/h/h3045.md) that there is not among us ['iysh](../../strongs/h/h376.md) that can [yada'](../../strongs/h/h3045.md) to [karath](../../strongs/h/h3772.md) ['ets](../../strongs/h/h6086.md) like unto the [Ṣîḏōnî](../../strongs/h/h6722.md).

<a name="1kings_5_7"></a>1Kings 5:7

And it came to pass, when [Ḥîrām](../../strongs/h/h2438.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Šᵊlōmô](../../strongs/h/h8010.md), that he [samach](../../strongs/h/h8055.md) [me'od](../../strongs/h/h3966.md), and ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) this [yowm](../../strongs/h/h3117.md), which hath [nathan](../../strongs/h/h5414.md) unto [Dāviḏ](../../strongs/h/h1732.md) a [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md) over this [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md).

<a name="1kings_5_8"></a>1Kings 5:8

And [Ḥîrām](../../strongs/h/h2438.md) [shalach](../../strongs/h/h7971.md) to [Šᵊlōmô](../../strongs/h/h8010.md), ['āmar](../../strongs/h/h559.md), I have [shama'](../../strongs/h/h8085.md) the things which thou [shalach](../../strongs/h/h7971.md) to me for: and I will ['asah](../../strongs/h/h6213.md) all thy [chephets](../../strongs/h/h2656.md) concerning ['ets](../../strongs/h/h6086.md) of ['erez](../../strongs/h/h730.md), and concerning ['ets](../../strongs/h/h6086.md) of [bᵊrôš](../../strongs/h/h1265.md).

<a name="1kings_5_9"></a>1Kings 5:9

My ['ebed](../../strongs/h/h5650.md) shall bring them [yarad](../../strongs/h/h3381.md) from [Lᵊḇānôn](../../strongs/h/h3844.md) unto the [yam](../../strongs/h/h3220.md): and I will [śûm](../../strongs/h/h7760.md) them by [yam](../../strongs/h/h3220.md) in [dōḇrôṯ](../../strongs/h/h1702.md) unto the [maqowm](../../strongs/h/h4725.md) that thou shalt [shalach](../../strongs/h/h7971.md) me, and will cause them to be [naphats](../../strongs/h/h5310.md) there, and thou shalt [nasa'](../../strongs/h/h5375.md) them: and thou shalt ['asah](../../strongs/h/h6213.md) my [chephets](../../strongs/h/h2656.md), in [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) for my [bayith](../../strongs/h/h1004.md).

<a name="1kings_5_10"></a>1Kings 5:10

So [Ḥîrām](../../strongs/h/h2438.md) [nathan](../../strongs/h/h5414.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md) and [bᵊrôš](../../strongs/h/h1265.md) ['ets](../../strongs/h/h6086.md) according to all his [chephets](../../strongs/h/h2656.md).

<a name="1kings_5_11"></a>1Kings 5:11

And [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) [Ḥîrām](../../strongs/h/h2438.md) twenty thousand [kōr](../../strongs/h/h3734.md) of [ḥiṭṭâ](../../strongs/h/h2406.md) for [makōleṯ](../../strongs/h/h4361.md) to his [bayith](../../strongs/h/h1004.md), and twenty [kōr](../../strongs/h/h3734.md) of [kāṯîṯ](../../strongs/h/h3795.md) [šemen](../../strongs/h/h8081.md): thus [nathan](../../strongs/h/h5414.md) [Šᵊlōmô](../../strongs/h/h8010.md) to [Ḥîrām](../../strongs/h/h2438.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md).

<a name="1kings_5_12"></a>1Kings 5:12

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāḵmâ](../../strongs/h/h2451.md), as he [dabar](../../strongs/h/h1696.md) him: and there was [shalowm](../../strongs/h/h7965.md) between [Ḥîrām](../../strongs/h/h2438.md) and [Šᵊlōmô](../../strongs/h/h8010.md); and they two [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) together.

<a name="1kings_5_13"></a>1Kings 5:13

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) a [mas](../../strongs/h/h4522.md) out of all [Yisra'el](../../strongs/h/h3478.md); and the [mas](../../strongs/h/h4522.md) was thirty thousand ['iysh](../../strongs/h/h376.md).

<a name="1kings_5_14"></a>1Kings 5:14

And he [shalach](../../strongs/h/h7971.md) them to [Lᵊḇānôn](../../strongs/h/h3844.md), ten thousand a [ḥōḏeš](../../strongs/h/h2320.md) by [ḥălîp̄â](../../strongs/h/h2487.md): a [ḥōḏeš](../../strongs/h/h2320.md) they were in [Lᵊḇānôn](../../strongs/h/h3844.md), and two [ḥōḏeš](../../strongs/h/h2320.md) at [bayith](../../strongs/h/h1004.md): and ['Ăḏōnîrām](../../strongs/h/h141.md) was over the [mas](../../strongs/h/h4522.md).

<a name="1kings_5_15"></a>1Kings 5:15

And [Šᵊlōmô](../../strongs/h/h8010.md) had threescore and ten thousand that [nasa'](../../strongs/h/h5375.md) [sabāl](../../strongs/h/h5449.md), and fourscore thousand [ḥāṣaḇ](../../strongs/h/h2672.md) in the [har](../../strongs/h/h2022.md);

<a name="1kings_5_16"></a>1Kings 5:16

Beside the [śar](../../strongs/h/h8269.md) of [Šᵊlōmô](../../strongs/h/h8010.md) [nāṣaḇ](../../strongs/h/h5324.md) which were over the [mĕla'kah](../../strongs/h/h4399.md), three thousand and three hundred, which [radah](../../strongs/h/h7287.md) over the ['am](../../strongs/h/h5971.md) that ['asah](../../strongs/h/h6213.md) in the [mĕla'kah](../../strongs/h/h4399.md).

<a name="1kings_5_17"></a>1Kings 5:17

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md), and they [nāsaʿ](../../strongs/h/h5265.md) [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md), [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), and [gāzîṯ](../../strongs/h/h1496.md) ['eben](../../strongs/h/h68.md), to lay the [yacad](../../strongs/h/h3245.md) of the [bayith](../../strongs/h/h1004.md).

<a name="1kings_5_18"></a>1Kings 5:18

And [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) and [Ḥîrām](../../strongs/h/h2438.md) [bānâ](../../strongs/h/h1129.md) did [pāsal](../../strongs/h/h6458.md) them, and the [giḇlî](../../strongs/h/h1382.md): so they [kuwn](../../strongs/h/h3559.md) ['ets](../../strongs/h/h6086.md) and ['eben](../../strongs/h/h68.md) to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 4](1kings_4.md) - [1Kings 6](1kings_6.md)