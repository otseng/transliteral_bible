# [1Kings 19](https://www.blueletterbible.org/kjv/1kings/19)

<a name="1kings_19_1"></a>1Kings 19:1

And ['Aḥ'Āḇ](../../strongs/h/h256.md) [nāḡaḏ](../../strongs/h/h5046.md) ['Îzeḇel](../../strongs/h/h348.md) all that ['Ēlîyâ](../../strongs/h/h452.md) had ['asah](../../strongs/h/h6213.md), and withal how he had [harag](../../strongs/h/h2026.md) all the [nāḇî'](../../strongs/h/h5030.md) with the [chereb](../../strongs/h/h2719.md).

<a name="1kings_19_2"></a>1Kings 19:2

Then ['Îzeḇel](../../strongs/h/h348.md) [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md) unto ['Ēlîyâ](../../strongs/h/h452.md), ['āmar](../../strongs/h/h559.md), So let the ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) to me, and more also, if I [śûm](../../strongs/h/h7760.md) not thy [nephesh](../../strongs/h/h5315.md) as the [nephesh](../../strongs/h/h5315.md) of one of them by [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md).

<a name="1kings_19_3"></a>1Kings 19:3

And when he [ra'ah](../../strongs/h/h7200.md) that, he [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) for his [nephesh](../../strongs/h/h5315.md), and [bow'](../../strongs/h/h935.md) to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), which belongeth to [Yehuwdah](../../strongs/h/h3063.md), and [yānaḥ](../../strongs/h/h3240.md) his [naʿar](../../strongs/h/h5288.md) there.

<a name="1kings_19_4"></a>1Kings 19:4

But he himself [halak](../../strongs/h/h1980.md) a [yowm](../../strongs/h/h3117.md) [derek](../../strongs/h/h1870.md) into the [midbar](../../strongs/h/h4057.md), and [bow'](../../strongs/h/h935.md) and [yashab](../../strongs/h/h3427.md) under a [rōṯem](../../strongs/h/h7574.md): and he [sha'al](../../strongs/h/h7592.md) for [nephesh](../../strongs/h/h5315.md) that he might [muwth](../../strongs/h/h4191.md); and ['āmar](../../strongs/h/h559.md), It is [rab](../../strongs/h/h7227.md); now, [Yĕhovah](../../strongs/h/h3068.md), [laqach](../../strongs/h/h3947.md) my [nephesh](../../strongs/h/h5315.md); for I am not [towb](../../strongs/h/h2896.md) than my ['ab](../../strongs/h/h1.md).

<a name="1kings_19_5"></a>1Kings 19:5

And as he [shakab](../../strongs/h/h7901.md) and [yashen](../../strongs/h/h3462.md) under a [rōṯem](../../strongs/h/h7574.md), behold, then a [mal'ak](../../strongs/h/h4397.md) [naga'](../../strongs/h/h5060.md) him, and ['āmar](../../strongs/h/h559.md) unto him, [quwm](../../strongs/h/h6965.md) and ['akal](../../strongs/h/h398.md).

<a name="1kings_19_6"></a>1Kings 19:6

And he [nabat](../../strongs/h/h5027.md), and, behold, there was a [ʿugâ](../../strongs/h/h5692.md) on the [reṣep̄](../../strongs/h/h7529.md), and a [ṣapaḥaṯ](../../strongs/h/h6835.md) of [mayim](../../strongs/h/h4325.md) at his [mᵊra'ăšôṯ](../../strongs/h/h4763.md). And he did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [shakab](../../strongs/h/h7901.md) him [shuwb](../../strongs/h/h7725.md).

<a name="1kings_19_7"></a>1Kings 19:7

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) the [šēnî](../../strongs/h/h8145.md), and [naga'](../../strongs/h/h5060.md) him, and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md) and ['akal](../../strongs/h/h398.md); because the [derek](../../strongs/h/h1870.md) is too [rab](../../strongs/h/h7227.md) for thee.

<a name="1kings_19_8"></a>1Kings 19:8

And he [quwm](../../strongs/h/h6965.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [yālaḵ](../../strongs/h/h3212.md) in the [koach](../../strongs/h/h3581.md) of that ['ăḵîlâ](../../strongs/h/h396.md) forty [yowm](../../strongs/h/h3117.md) and forty [layil](../../strongs/h/h3915.md) unto [ḥōrēḇ](../../strongs/h/h2722.md) the [har](../../strongs/h/h2022.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="1kings_19_9"></a>1Kings 19:9

And he [bow'](../../strongs/h/h935.md) thither unto a [mᵊʿārâ](../../strongs/h/h4631.md), and [lûn](../../strongs/h/h3885.md) there; and, behold, the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to him, and he ['āmar](../../strongs/h/h559.md) unto him, What doest thou here, ['Ēlîyâ](../../strongs/h/h452.md)?

<a name="1kings_19_10"></a>1Kings 19:10

And he ['āmar](../../strongs/h/h559.md), I have been [qānā'](../../strongs/h/h7065.md) [qānā'](../../strongs/h/h7065.md) for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md): for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have ['azab](../../strongs/h/h5800.md) thy [bĕriyth](../../strongs/h/h1285.md), [harac](../../strongs/h/h2040.md) thine [mizbeach](../../strongs/h/h4196.md), and [harag](../../strongs/h/h2026.md) thy [nāḇî'](../../strongs/h/h5030.md) with the [chereb](../../strongs/h/h2719.md); and I, even I only, am [yāṯar](../../strongs/h/h3498.md); and they [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md), to [laqach](../../strongs/h/h3947.md) it.

<a name="1kings_19_11"></a>1Kings 19:11

And he ['āmar](../../strongs/h/h559.md), [yāṣā'](../../strongs/h/h3318.md), and ['amad](../../strongs/h/h5975.md) upon the [har](../../strongs/h/h2022.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md). And, behold, [Yĕhovah](../../strongs/h/h3068.md) ['abar](../../strongs/h/h5674.md), and a [gadowl](../../strongs/h/h1419.md) and [ḥāzāq](../../strongs/h/h2389.md) [ruwach](../../strongs/h/h7307.md) [paraq](../../strongs/h/h6561.md) the [har](../../strongs/h/h2022.md), and [shabar](../../strongs/h/h7665.md) the [cela'](../../strongs/h/h5553.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); but [Yĕhovah](../../strongs/h/h3068.md) was not in the [ruwach](../../strongs/h/h7307.md): and ['aḥar](../../strongs/h/h310.md) the [ruwach](../../strongs/h/h7307.md) a [raʿaš](../../strongs/h/h7494.md); but [Yĕhovah](../../strongs/h/h3068.md) was not in the [raʿaš](../../strongs/h/h7494.md):

<a name="1kings_19_12"></a>1Kings 19:12

And ['aḥar](../../strongs/h/h310.md) the [raʿaš](../../strongs/h/h7494.md) an ['esh](../../strongs/h/h784.md); but [Yĕhovah](../../strongs/h/h3068.md) was not in the ['esh](../../strongs/h/h784.md): and ['aḥar](../../strongs/h/h310.md) the ['esh](../../strongs/h/h784.md) a [dᵊmāmâ](../../strongs/h/h1827.md) [daq](../../strongs/h/h1851.md) [qowl](../../strongs/h/h6963.md).

<a name="1kings_19_13"></a>1Kings 19:13

And it was so, when ['Ēlîyâ](../../strongs/h/h452.md) [shama'](../../strongs/h/h8085.md) it, that he [lûṭ](../../strongs/h/h3874.md) his [paniym](../../strongs/h/h6440.md) in his ['adereṯ](../../strongs/h/h155.md), and [yāṣā'](../../strongs/h/h3318.md), and ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [mᵊʿārâ](../../strongs/h/h4631.md). And, behold, there came a [qowl](../../strongs/h/h6963.md) unto him, and ['āmar](../../strongs/h/h559.md), What doest thou here, ['Ēlîyâ](../../strongs/h/h452.md)?

<a name="1kings_19_14"></a>1Kings 19:14

And he ['āmar](../../strongs/h/h559.md), I have been [qānā'](../../strongs/h/h7065.md) [qānā'](../../strongs/h/h7065.md) for [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md): because the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have ['azab](../../strongs/h/h5800.md) thy [bĕriyth](../../strongs/h/h1285.md), [harac](../../strongs/h/h2040.md) thine [mizbeach](../../strongs/h/h4196.md), and [harag](../../strongs/h/h2026.md) thy [nāḇî'](../../strongs/h/h5030.md) with the [chereb](../../strongs/h/h2719.md); and I, even I only, am [yāṯar](../../strongs/h/h3498.md); and they [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md), to [laqach](../../strongs/h/h3947.md) it.

<a name="1kings_19_15"></a>1Kings 19:15

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), [shuwb](../../strongs/h/h7725.md) on thy [derek](../../strongs/h/h1870.md) to the [midbar](../../strongs/h/h4057.md) of [Dammeśeq](../../strongs/h/h1834.md): and when thou [bow'](../../strongs/h/h935.md), [māšaḥ](../../strongs/h/h4886.md) [Ḥăzā'Ēl](../../strongs/h/h2371.md) to be [melek](../../strongs/h/h4428.md) over ['Ărām](../../strongs/h/h758.md):

<a name="1kings_19_16"></a>1Kings 19:16

And [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Nimšî](../../strongs/h/h5250.md) shalt thou [māšaḥ](../../strongs/h/h4886.md) to be [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md): and ['Ĕlîšāʿ](../../strongs/h/h477.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄āṭ](../../strongs/h/h8202.md) of ['Āḇēl Mᵊḥôlâ](../../strongs/h/h65.md) shalt thou [māšaḥ](../../strongs/h/h4886.md) to be [nāḇî'](../../strongs/h/h5030.md) in thy room.

<a name="1kings_19_17"></a>1Kings 19:17

And it shall come to pass, that him that [mālaṭ](../../strongs/h/h4422.md) the [chereb](../../strongs/h/h2719.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md) shall [Yêû'](../../strongs/h/h3058.md) [muwth](../../strongs/h/h4191.md): and him that [mālaṭ](../../strongs/h/h4422.md) from the [chereb](../../strongs/h/h2719.md) of [Yêû'](../../strongs/h/h3058.md) shall ['Ĕlîšāʿ](../../strongs/h/h477.md) [muwth](../../strongs/h/h4191.md).

<a name="1kings_19_18"></a>1Kings 19:18

Yet I have [šā'ar](../../strongs/h/h7604.md) me seven thousand in [Yisra'el](../../strongs/h/h3478.md), all the [bereḵ](../../strongs/h/h1290.md) which have not [kara'](../../strongs/h/h3766.md) unto [BaʿAl](../../strongs/h/h1168.md), and every [peh](../../strongs/h/h6310.md) which hath not [nashaq](../../strongs/h/h5401.md) him.

<a name="1kings_19_19"></a>1Kings 19:19

So he [yālaḵ](../../strongs/h/h3212.md) thence, and [māṣā'](../../strongs/h/h4672.md) ['Ĕlîšāʿ](../../strongs/h/h477.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄āṭ](../../strongs/h/h8202.md), who was [ḥāraš](../../strongs/h/h2790.md) with twelve [ṣemeḏ](../../strongs/h/h6776.md) [paniym](../../strongs/h/h6440.md) him, and he with the twelfth : and ['Ēlîyâ](../../strongs/h/h452.md) ['abar](../../strongs/h/h5674.md) by him, and [shalak](../../strongs/h/h7993.md) his ['adereṯ](../../strongs/h/h155.md) upon him.

<a name="1kings_19_20"></a>1Kings 19:20

And he ['azab](../../strongs/h/h5800.md) the [bāqār](../../strongs/h/h1241.md), and [rûṣ](../../strongs/h/h7323.md) ['aḥar](../../strongs/h/h310.md) ['Ēlîyâ](../../strongs/h/h452.md), and ['āmar](../../strongs/h/h559.md), Let me, I pray thee, [nashaq](../../strongs/h/h5401.md) my ['ab](../../strongs/h/h1.md) and my ['em](../../strongs/h/h517.md), and then I will [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) thee. And he ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) [shuwb](../../strongs/h/h7725.md): for what have I ['asah](../../strongs/h/h6213.md) to thee?

<a name="1kings_19_21"></a>1Kings 19:21

And he [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md), and [laqach](../../strongs/h/h3947.md) a [ṣemeḏ](../../strongs/h/h6776.md) of [bāqār](../../strongs/h/h1241.md), and [zabach](../../strongs/h/h2076.md) them, and [bāšal](../../strongs/h/h1310.md) their [basar](../../strongs/h/h1320.md) with the [kĕliy](../../strongs/h/h3627.md) of the [bāqār](../../strongs/h/h1241.md), and [nathan](../../strongs/h/h5414.md) unto the ['am](../../strongs/h/h5971.md), and they did ['akal](../../strongs/h/h398.md). Then he [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['Ēlîyâ](../../strongs/h/h452.md), and [sharath](../../strongs/h/h8334.md) unto him.

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 18](1kings_18.md) - [1Kings 20](1kings_20.md)