# [1Kings 13](https://www.blueletterbible.org/kjv/1kings/13)

<a name="1kings_13_1"></a>1Kings 13:1

And, behold, there [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) out of [Yehuwdah](../../strongs/h/h3063.md) by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Bêṯ-'ēl](../../strongs/h/h1008.md): and [YārāḇʿĀm](../../strongs/h/h3379.md) ['amad](../../strongs/h/h5975.md) by the [mizbeach](../../strongs/h/h4196.md) to [qāṭar](../../strongs/h/h6999.md).

<a name="1kings_13_2"></a>1Kings 13:2

And he [qara'](../../strongs/h/h7121.md) against the [mizbeach](../../strongs/h/h4196.md) in the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), O [mizbeach](../../strongs/h/h4196.md), [mizbeach](../../strongs/h/h4196.md), thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, a [ben](../../strongs/h/h1121.md) shall be [yalad](../../strongs/h/h3205.md) unto the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), [Yō'Šîyâ](../../strongs/h/h2977.md) by [shem](../../strongs/h/h8034.md); and upon thee shall he [zabach](../../strongs/h/h2076.md) the [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md) that [qāṭar](../../strongs/h/h6999.md) upon thee, and ['āḏām](../../strongs/h/h120.md) ['etsem](../../strongs/h/h6106.md) shall be [śārap̄](../../strongs/h/h8313.md) upon thee.

<a name="1kings_13_3"></a>1Kings 13:3

And he [nathan](../../strongs/h/h5414.md) a [môp̄ēṯ](../../strongs/h/h4159.md) the same [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md), This is the [môp̄ēṯ](../../strongs/h/h4159.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md); Behold, the [mizbeach](../../strongs/h/h4196.md) shall be [qāraʿ](../../strongs/h/h7167.md), and the [dešen](../../strongs/h/h1880.md) that are upon it shall be [šāp̄aḵ](../../strongs/h/h8210.md).

<a name="1kings_13_4"></a>1Kings 13:4

And it came to pass, when [melek](../../strongs/h/h4428.md) [YārāḇʿĀm](../../strongs/h/h3379.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), which had [qara'](../../strongs/h/h7121.md) against the [mizbeach](../../strongs/h/h4196.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md), that he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) from the [mizbeach](../../strongs/h/h4196.md), ['āmar](../../strongs/h/h559.md), [tāp̄aś](../../strongs/h/h8610.md) on him. And his [yad](../../strongs/h/h3027.md), which he [shalach](../../strongs/h/h7971.md) against him, [yāḇēš](../../strongs/h/h3001.md), so that he [yakol](../../strongs/h/h3201.md) not pull it in [shuwb](../../strongs/h/h7725.md) to him.

<a name="1kings_13_5"></a>1Kings 13:5

The [mizbeach](../../strongs/h/h4196.md) also was [qāraʿ](../../strongs/h/h7167.md), and the [dešen](../../strongs/h/h1880.md) [šāp̄aḵ](../../strongs/h/h8210.md) from the [mizbeach](../../strongs/h/h4196.md), according to the [môp̄ēṯ](../../strongs/h/h4159.md) which the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_13_6"></a>1Kings 13:6

And the [melek](../../strongs/h/h4428.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), [ḥālâ](../../strongs/h/h2470.md) now the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and [palal](../../strongs/h/h6419.md) for me, that my [yad](../../strongs/h/h3027.md) may be restored me [shuwb](../../strongs/h/h7725.md). And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md) was restored him [shuwb](../../strongs/h/h7725.md), and became as it was [ri'šôn](../../strongs/h/h7223.md).

<a name="1kings_13_7"></a>1Kings 13:7

And the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1696.md) unto the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), [bow'](../../strongs/h/h935.md) [bayith](../../strongs/h/h1004.md) with me, and [sāʿaḏ](../../strongs/h/h5582.md) thyself, and I will [nathan](../../strongs/h/h5414.md) thee a [mataṯ](../../strongs/h/h4991.md).

<a name="1kings_13_8"></a>1Kings 13:8

And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), If thou wilt [nathan](../../strongs/h/h5414.md) me [ḥēṣî](../../strongs/h/h2677.md) thine [bayith](../../strongs/h/h1004.md), I will not [bow'](../../strongs/h/h935.md) with thee, neither will I ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) in this [maqowm](../../strongs/h/h4725.md):

<a name="1kings_13_9"></a>1Kings 13:9

For so was it [tsavah](../../strongs/h/h6680.md) me by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md), nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md), nor [shuwb](../../strongs/h/h7725.md) by the same [derek](../../strongs/h/h1870.md) that thou [halak](../../strongs/h/h1980.md).

<a name="1kings_13_10"></a>1Kings 13:10

So he [yālaḵ](../../strongs/h/h3212.md) ['aḥēr](../../strongs/h/h312.md) [derek](../../strongs/h/h1870.md), and [shuwb](../../strongs/h/h7725.md) not by the [derek](../../strongs/h/h1870.md) that he [bow'](../../strongs/h/h935.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="1kings_13_11"></a>1Kings 13:11

Now there [yashab](../../strongs/h/h3427.md) a [zāqēn](../../strongs/h/h2205.md) [nāḇî'](../../strongs/h/h5030.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md); and his [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md) and [sāp̄ar](../../strongs/h/h5608.md) him all the [ma'aseh](../../strongs/h/h4639.md) that the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) had ['asah](../../strongs/h/h6213.md) that [yowm](../../strongs/h/h3117.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md): the [dabar](../../strongs/h/h1697.md) which he had [dabar](../../strongs/h/h1696.md) unto the [melek](../../strongs/h/h4428.md), them they [sāp̄ar](../../strongs/h/h5608.md) also to their ['ab](../../strongs/h/h1.md).

<a name="1kings_13_12"></a>1Kings 13:12

And their ['ab](../../strongs/h/h1.md) [dabar](../../strongs/h/h1696.md) unto them, What [derek](../../strongs/h/h1870.md) [halak](../../strongs/h/h1980.md) he? For his [ben](../../strongs/h/h1121.md) had [ra'ah](../../strongs/h/h7200.md) what [derek](../../strongs/h/h1870.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [halak](../../strongs/h/h1980.md), which [bow'](../../strongs/h/h935.md) from [Yehuwdah](../../strongs/h/h3063.md).

<a name="1kings_13_13"></a>1Kings 13:13

And he ['āmar](../../strongs/h/h559.md) unto his [ben](../../strongs/h/h1121.md), [ḥāḇaš](../../strongs/h/h2280.md) me the [chamowr](../../strongs/h/h2543.md). So they [ḥāḇaš](../../strongs/h/h2280.md) him the [chamowr](../../strongs/h/h2543.md): and he [rāḵaḇ](../../strongs/h/h7392.md) thereon,

<a name="1kings_13_14"></a>1Kings 13:14

And [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [māṣā'](../../strongs/h/h4672.md) him [yashab](../../strongs/h/h3427.md) under an ['ēlâ](../../strongs/h/h424.md): and he ['āmar](../../strongs/h/h559.md) unto him, Art thou the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) that [bow'](../../strongs/h/h935.md) from [Yehuwdah](../../strongs/h/h3063.md)? And he ['āmar](../../strongs/h/h559.md), I am.

<a name="1kings_13_15"></a>1Kings 13:15

Then he ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) [bayith](../../strongs/h/h1004.md) with me, and ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md).

<a name="1kings_13_16"></a>1Kings 13:16

And he ['āmar](../../strongs/h/h559.md), I [yakol](../../strongs/h/h3201.md) not [shuwb](../../strongs/h/h7725.md) with thee, nor [bow'](../../strongs/h/h935.md) with thee: neither will I ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) with thee in this [maqowm](../../strongs/h/h4725.md):

<a name="1kings_13_17"></a>1Kings 13:17

For it was [dabar](../../strongs/h/h1697.md) to me by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), Thou shalt ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md) nor [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) there, nor [shuwb](../../strongs/h/h7725.md) to [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md) that thou [halak](../../strongs/h/h1980.md).

<a name="1kings_13_18"></a>1Kings 13:18

He ['āmar](../../strongs/h/h559.md) unto him, I am a [nāḇî'](../../strongs/h/h5030.md) also as thou art; and a [mal'ak](../../strongs/h/h4397.md) [dabar](../../strongs/h/h1696.md) unto me by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) him with thee into thine [bayith](../../strongs/h/h1004.md), that he may ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) and [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md). But he [kāḥaš](../../strongs/h/h3584.md) unto him.

<a name="1kings_13_19"></a>1Kings 13:19

So he went [shuwb](../../strongs/h/h7725.md) with him, and did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) in his [bayith](../../strongs/h/h1004.md), and [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md).

<a name="1kings_13_20"></a>1Kings 13:20

And it came to pass, as they [yashab](../../strongs/h/h3427.md) at the [šulḥān](../../strongs/h/h7979.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto the [nāḇî'](../../strongs/h/h5030.md) that [shuwb](../../strongs/h/h7725.md) him:

<a name="1kings_13_21"></a>1Kings 13:21

And he [qara'](../../strongs/h/h7121.md) unto the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) that [bow'](../../strongs/h/h935.md) from [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Forasmuch as thou hast [marah](../../strongs/h/h4784.md) the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), and hast not [shamar](../../strongs/h/h8104.md) the [mitsvah](../../strongs/h/h4687.md) which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) thee,

<a name="1kings_13_22"></a>1Kings 13:22

But camest [shuwb](../../strongs/h/h7725.md), and hast ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) and [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md) in the [maqowm](../../strongs/h/h4725.md), of the which did [dabar](../../strongs/h/h1696.md) to thee, ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md), and [šāṯâ](../../strongs/h/h8354.md) no [mayim](../../strongs/h/h4325.md); thy [nᵊḇēlâ](../../strongs/h/h5038.md) shall not [bow'](../../strongs/h/h935.md) unto the [qeber](../../strongs/h/h6913.md) of thy ['ab](../../strongs/h/h1.md).

<a name="1kings_13_23"></a>1Kings 13:23

And it came to pass, ['aḥar](../../strongs/h/h310.md) he had ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), and ['aḥar](../../strongs/h/h310.md) he had [šāṯâ](../../strongs/h/h8354.md), that he [ḥāḇaš](../../strongs/h/h2280.md) for him the [chamowr](../../strongs/h/h2543.md), for the [nāḇî'](../../strongs/h/h5030.md) whom he had [shuwb](../../strongs/h/h7725.md).

<a name="1kings_13_24"></a>1Kings 13:24

And when he was [yālaḵ](../../strongs/h/h3212.md), an ['ariy](../../strongs/h/h738.md) [māṣā'](../../strongs/h/h4672.md) him by the [derek](../../strongs/h/h1870.md), and [muwth](../../strongs/h/h4191.md) him: and his [nᵊḇēlâ](../../strongs/h/h5038.md) was [shalak](../../strongs/h/h7993.md) in the [derek](../../strongs/h/h1870.md), and the [chamowr](../../strongs/h/h2543.md) ['amad](../../strongs/h/h5975.md) by it, the ['ariy](../../strongs/h/h738.md) also ['amad](../../strongs/h/h5975.md) by the [nᵊḇēlâ](../../strongs/h/h5038.md).

<a name="1kings_13_25"></a>1Kings 13:25

And, behold, ['enowsh](../../strongs/h/h582.md) ['abar](../../strongs/h/h5674.md), and [ra'ah](../../strongs/h/h7200.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) [shalak](../../strongs/h/h7993.md) in the [derek](../../strongs/h/h1870.md), and the ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) by the [nᵊḇēlâ](../../strongs/h/h5038.md): and they [bow'](../../strongs/h/h935.md) and [dabar](../../strongs/h/h1696.md) it in the [ʿîr](../../strongs/h/h5892.md) where the [zāqēn](../../strongs/h/h2205.md) [nāḇî'](../../strongs/h/h5030.md) [yashab](../../strongs/h/h3427.md).

<a name="1kings_13_26"></a>1Kings 13:26

And when the [nāḇî'](../../strongs/h/h5030.md) that [shuwb](../../strongs/h/h7725.md) him from the [derek](../../strongs/h/h1870.md) [shama'](../../strongs/h/h8085.md) thereof, he ['āmar](../../strongs/h/h559.md), It is the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), who was [marah](../../strongs/h/h4784.md) unto the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md): therefore [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) him unto the ['ariy](../../strongs/h/h738.md), which hath [shabar](../../strongs/h/h7665.md) him, and [muwth](../../strongs/h/h4191.md) him, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) unto him.

<a name="1kings_13_27"></a>1Kings 13:27

And he [dabar](../../strongs/h/h1696.md) to his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), [ḥāḇaš](../../strongs/h/h2280.md) me the [chamowr](../../strongs/h/h2543.md). And they [ḥāḇaš](../../strongs/h/h2280.md) him.

<a name="1kings_13_28"></a>1Kings 13:28

And he [yālaḵ](../../strongs/h/h3212.md) and [māṣā'](../../strongs/h/h4672.md) his [nᵊḇēlâ](../../strongs/h/h5038.md) [shalak](../../strongs/h/h7993.md) in the [derek](../../strongs/h/h1870.md), and the [chamowr](../../strongs/h/h2543.md) and the ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) by the [nᵊḇēlâ](../../strongs/h/h5038.md): the ['ariy](../../strongs/h/h738.md) had not ['akal](../../strongs/h/h398.md) the [nᵊḇēlâ](../../strongs/h/h5038.md), nor [shabar](../../strongs/h/h7665.md) the [chamowr](../../strongs/h/h2543.md).

<a name="1kings_13_29"></a>1Kings 13:29

And the [nāḇî'](../../strongs/h/h5030.md) [nasa'](../../strongs/h/h5375.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) of the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and [yānaḥ](../../strongs/h/h3240.md) it upon the [chamowr](../../strongs/h/h2543.md), and [shuwb](../../strongs/h/h7725.md) it: and the [zāqēn](../../strongs/h/h2205.md) [nāḇî'](../../strongs/h/h5030.md) [bow'](../../strongs/h/h935.md) to the [ʿîr](../../strongs/h/h5892.md), to [sāp̄aḏ](../../strongs/h/h5594.md) and to [qāḇar](../../strongs/h/h6912.md) him.

<a name="1kings_13_30"></a>1Kings 13:30

And he [yānaḥ](../../strongs/h/h3240.md) his [nᵊḇēlâ](../../strongs/h/h5038.md) in his own [qeber](../../strongs/h/h6913.md); and they [sāp̄aḏ](../../strongs/h/h5594.md) over him, saying, [hôy](../../strongs/h/h1945.md), my ['ach](../../strongs/h/h251.md)!

<a name="1kings_13_31"></a>1Kings 13:31

And it came to pass, ['aḥar](../../strongs/h/h310.md) he had [qāḇar](../../strongs/h/h6912.md) him, that he ['āmar](../../strongs/h/h559.md) to his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), When I am [muwth](../../strongs/h/h4191.md), then [qāḇar](../../strongs/h/h6912.md) me in the [qeber](../../strongs/h/h6913.md) wherein the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) is [qāḇar](../../strongs/h/h6912.md); [yānaḥ](../../strongs/h/h3240.md) my ['etsem](../../strongs/h/h6106.md) beside his ['etsem](../../strongs/h/h6106.md):

<a name="1kings_13_32"></a>1Kings 13:32

For the [dabar](../../strongs/h/h1697.md) which he [qara'](../../strongs/h/h7121.md) by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) against the [mizbeach](../../strongs/h/h4196.md) in [Bêṯ-'ēl](../../strongs/h/h1008.md), and against all the [bayith](../../strongs/h/h1004.md) of the [bāmâ](../../strongs/h/h1116.md) which are in the [ʿîr](../../strongs/h/h5892.md) of [Šōmrôn](../../strongs/h/h8111.md), shall surely come to pass.

<a name="1kings_13_33"></a>1Kings 13:33

['aḥar](../../strongs/h/h310.md) this [dabar](../../strongs/h/h1697.md) [YārāḇʿĀm](../../strongs/h/h3379.md) [shuwb](../../strongs/h/h7725.md) not from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), but ['asah](../../strongs/h/h6213.md) [shuwb](../../strongs/h/h7725.md) of the [qāṣâ](../../strongs/h/h7098.md) of the ['am](../../strongs/h/h5971.md) [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md): whosoever [chaphets](../../strongs/h/h2655.md), he [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) him, and he became one of the [kōhēn](../../strongs/h/h3548.md) of the [bāmâ](../../strongs/h/h1116.md).

<a name="1kings_13_34"></a>1Kings 13:34

And this [dabar](../../strongs/h/h1697.md) became [chatta'ath](../../strongs/h/h2403.md) unto the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), even to cut it [kāḥaḏ](../../strongs/h/h3582.md), and to [šāmaḏ](../../strongs/h/h8045.md) it from off the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 12](1kings_12.md) - [1Kings 14](1kings_14.md)