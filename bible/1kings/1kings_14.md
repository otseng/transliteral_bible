# [1Kings 14](https://www.blueletterbible.org/kjv/1kings/14)

<a name="1kings_14_1"></a>1Kings 14:1

At that [ʿēṯ](../../strongs/h/h6256.md) ['Ăḇîâ](../../strongs/h/h29.md) the [ben](../../strongs/h/h1121.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [ḥālâ](../../strongs/h/h2470.md).

<a name="1kings_14_2"></a>1Kings 14:2

And [YārāḇʿĀm](../../strongs/h/h3379.md) ['āmar](../../strongs/h/h559.md) to his ['ishshah](../../strongs/h/h802.md), [quwm](../../strongs/h/h6965.md), I pray thee, and [šānâ](../../strongs/h/h8138.md) thyself, that thou be not [yada'](../../strongs/h/h3045.md) to be the ['ishshah](../../strongs/h/h802.md) of [YārāḇʿĀm](../../strongs/h/h3379.md); and [halak](../../strongs/h/h1980.md) thee to [Šîlô](../../strongs/h/h7887.md): behold, there is ['Ăḥîyâ](../../strongs/h/h281.md) the [nāḇî'](../../strongs/h/h5030.md), which [dabar](../../strongs/h/h1696.md) me that [melek](../../strongs/h/h4428.md) over this ['am](../../strongs/h/h5971.md).

<a name="1kings_14_3"></a>1Kings 14:3

And [laqach](../../strongs/h/h3947.md) with [yad](../../strongs/h/h3027.md) ten [lechem](../../strongs/h/h3899.md), and [niqquḏ](../../strongs/h/h5350.md), and a [baqbûq](../../strongs/h/h1228.md) of [dĕbash](../../strongs/h/h1706.md), and [bow'](../../strongs/h/h935.md) to him: he shall [nāḡaḏ](../../strongs/h/h5046.md) thee what shall become of the [naʿar](../../strongs/h/h5288.md).

<a name="1kings_14_4"></a>1Kings 14:4

And [YārāḇʿĀm](../../strongs/h/h3379.md) ['ishshah](../../strongs/h/h802.md) did ['asah](../../strongs/h/h6213.md), and [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to [Šîlô](../../strongs/h/h7887.md), and [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of ['Ăḥîyâ](../../strongs/h/h281.md). But ['Ăḥîyâ](../../strongs/h/h281.md) [yakol](../../strongs/h/h3201.md) not [ra'ah](../../strongs/h/h7200.md); for his ['ayin](../../strongs/h/h5869.md) were [quwm](../../strongs/h/h6965.md) by reason of his [śêḇ](../../strongs/h/h7869.md).

<a name="1kings_14_5"></a>1Kings 14:5

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto ['Ăḥîyâ](../../strongs/h/h281.md), Behold, the ['ishshah](../../strongs/h/h802.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) [bow'](../../strongs/h/h935.md) to [darash](../../strongs/h/h1875.md) a [dabar](../../strongs/h/h1697.md) of thee for her [ben](../../strongs/h/h1121.md); for he is [ḥālâ](../../strongs/h/h2470.md): [zô](../../strongs/h/h2090.md) and thus shalt thou [dabar](../../strongs/h/h1696.md) unto her: for it shall be, when she [bow'](../../strongs/h/h935.md), that she shall feign herself to be [nāḵar](../../strongs/h/h5234.md).

<a name="1kings_14_6"></a>1Kings 14:6

And it was so, when ['Ăḥîyâ](../../strongs/h/h281.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of her [regel](../../strongs/h/h7272.md), as she [bow'](../../strongs/h/h935.md) at the [peṯaḥ](../../strongs/h/h6607.md), that he ['āmar](../../strongs/h/h559.md), Come [bow'](../../strongs/h/h935.md), thou ['ishshah](../../strongs/h/h802.md) of [YārāḇʿĀm](../../strongs/h/h3379.md); why feignest thou thyself to be [nāḵar](../../strongs/h/h5234.md)? for I am [shalach](../../strongs/h/h7971.md) to thee with [qāšê](../../strongs/h/h7186.md).

<a name="1kings_14_7"></a>1Kings 14:7

[yālaḵ](../../strongs/h/h3212.md), ['āmar](../../strongs/h/h559.md) [YārāḇʿĀm](../../strongs/h/h3379.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Forasmuch as I [ruwm](../../strongs/h/h7311.md) thee from among the ['am](../../strongs/h/h5971.md), and [nathan](../../strongs/h/h5414.md) thee [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md),

<a name="1kings_14_8"></a>1Kings 14:8

And [qāraʿ](../../strongs/h/h7167.md) the [mamlāḵâ](../../strongs/h/h4467.md) [qāraʿ](../../strongs/h/h7167.md) from the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), and [nathan](../../strongs/h/h5414.md) it thee: and yet thou hast not been as my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md), who [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and who [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) me with all his [lebab](../../strongs/h/h3824.md), to ['asah](../../strongs/h/h6213.md) that only which was [yashar](../../strongs/h/h3477.md) in mine ['ayin](../../strongs/h/h5869.md);

<a name="1kings_14_9"></a>1Kings 14:9

But hast ['asah](../../strongs/h/h6213.md) [ra'a'](../../strongs/h/h7489.md) above all that were [paniym](../../strongs/h/h6440.md) thee: for thou hast [yālaḵ](../../strongs/h/h3212.md) and ['asah](../../strongs/h/h6213.md) thee ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [massēḵâ](../../strongs/h/h4541.md), to [kāʿas](../../strongs/h/h3707.md) me, and hast [shalak](../../strongs/h/h7993.md) me ['aḥar](../../strongs/h/h310.md) thy [gav](../../strongs/h/h1458.md):

<a name="1kings_14_10"></a>1Kings 14:10

Therefore, behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), and will [karath](../../strongs/h/h3772.md) from [YārāḇʿĀm](../../strongs/h/h3379.md) him that [šāṯan](../../strongs/h/h8366.md) against the [qîr](../../strongs/h/h7023.md), and him that is [ʿāṣar](../../strongs/h/h6113.md) and ['azab](../../strongs/h/h5800.md) in [Yisra'el](../../strongs/h/h3478.md), and will take [bāʿar](../../strongs/h/h1197.md) the ['aḥar](../../strongs/h/h310.md) of the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), as a man [bāʿar](../../strongs/h/h1197.md) [gālāl](../../strongs/h/h1557.md), till it be all [tamam](../../strongs/h/h8552.md).

<a name="1kings_14_11"></a>1Kings 14:11

Him that [muwth](../../strongs/h/h4191.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) in the [ʿîr](../../strongs/h/h5892.md) shall the [keleḇ](../../strongs/h/h3611.md) ['akal](../../strongs/h/h398.md); and him that [muwth](../../strongs/h/h4191.md) in the [sadeh](../../strongs/h/h7704.md) shall the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) ['akal](../../strongs/h/h398.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="1kings_14_12"></a>1Kings 14:12

[quwm](../../strongs/h/h6965.md) thou therefore, [yālaḵ](../../strongs/h/h3212.md) thee to thine own [bayith](../../strongs/h/h1004.md): and when thy [regel](../../strongs/h/h7272.md) [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), the [yeleḏ](../../strongs/h/h3206.md) shall [muwth](../../strongs/h/h4191.md).

<a name="1kings_14_13"></a>1Kings 14:13

And all [Yisra'el](../../strongs/h/h3478.md) shall [sāp̄aḏ](../../strongs/h/h5594.md) for him, and [qāḇar](../../strongs/h/h6912.md) him: for he only of [YārāḇʿĀm](../../strongs/h/h3379.md) shall [bow'](../../strongs/h/h935.md) to the [qeber](../../strongs/h/h6913.md), because in him there is [māṣā'](../../strongs/h/h4672.md) some [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) toward [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) in the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md).

<a name="1kings_14_14"></a>1Kings 14:14

Moreover [Yĕhovah](../../strongs/h/h3068.md) shall [quwm](../../strongs/h/h6965.md) him a [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), who shall [karath](../../strongs/h/h3772.md) the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) that [yowm](../../strongs/h/h3117.md): but what? even now.

<a name="1kings_14_15"></a>1Kings 14:15

For [Yĕhovah](../../strongs/h/h3068.md) shall [nakah](../../strongs/h/h5221.md) [Yisra'el](../../strongs/h/h3478.md), as a [qānê](../../strongs/h/h7070.md) is [nuwd](../../strongs/h/h5110.md) in the [mayim](../../strongs/h/h4325.md), and he shall [nathash](../../strongs/h/h5428.md) [Yisra'el](../../strongs/h/h3478.md) out of this [towb](../../strongs/h/h2896.md) ['ăḏāmâ](../../strongs/h/h127.md), which he [nathan](../../strongs/h/h5414.md) to their ['ab](../../strongs/h/h1.md), and shall [zārâ](../../strongs/h/h2219.md) them [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md), because they have ['asah](../../strongs/h/h6213.md) their ['ăšērâ](../../strongs/h/h842.md), provoking [Yĕhovah](../../strongs/h/h3068.md) to [kāʿas](../../strongs/h/h3707.md).

<a name="1kings_14_16"></a>1Kings 14:16

And he shall [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) because of the [chatta'ath](../../strongs/h/h2403.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), who did [chata'](../../strongs/h/h2398.md), and who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="1kings_14_17"></a>1Kings 14:17

And [YārāḇʿĀm](../../strongs/h/h3379.md) ['ishshah](../../strongs/h/h802.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) to [Tirṣâ](../../strongs/h/h8656.md): and when she [bow'](../../strongs/h/h935.md) to the [caph](../../strongs/h/h5592.md) of the [bayith](../../strongs/h/h1004.md), the [naʿar](../../strongs/h/h5288.md) [muwth](../../strongs/h/h4191.md);

<a name="1kings_14_18"></a>1Kings 14:18

And they [qāḇar](../../strongs/h/h6912.md) him; and all [Yisra'el](../../strongs/h/h3478.md) [sāp̄aḏ](../../strongs/h/h5594.md) for him, according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md) ['Ăḥîyâ](../../strongs/h/h281.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="1kings_14_19"></a>1Kings 14:19

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [YārāḇʿĀm](../../strongs/h/h3379.md), how he [lāḥam](../../strongs/h/h3898.md), and how he [mālaḵ](../../strongs/h/h4427.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_14_20"></a>1Kings 14:20

And the [yowm](../../strongs/h/h3117.md) which [YārāḇʿĀm](../../strongs/h/h3379.md) [mālaḵ](../../strongs/h/h4427.md) were two and twenty [šānâ](../../strongs/h/h8141.md): and he [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and [Nāḏāḇ](../../strongs/h/h5070.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_14_21"></a>1Kings 14:21

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) [mālaḵ](../../strongs/h/h4427.md) in [Yehuwdah](../../strongs/h/h3063.md). [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) was forty and one [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) seventeen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), the [ʿîr](../../strongs/h/h5892.md) which [Yĕhovah](../../strongs/h/h3068.md) did [bāḥar](../../strongs/h/h977.md) out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), to [śûm](../../strongs/h/h7760.md) his [shem](../../strongs/h/h8034.md) there. And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [naʿămâ](../../strongs/h/h5279.md) an [ʿammônîṯ](../../strongs/h/h5985.md).

<a name="1kings_14_22"></a>1Kings 14:22

And [Yehuwdah](../../strongs/h/h3063.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and they provoked him to [qānā'](../../strongs/h/h7065.md) with their [chatta'ath](../../strongs/h/h2403.md) which they had [chata'](../../strongs/h/h2398.md), above all that their ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="1kings_14_23"></a>1Kings 14:23

For they also [bānâ](../../strongs/h/h1129.md) them [bāmâ](../../strongs/h/h1116.md), and [maṣṣēḇâ](../../strongs/h/h4676.md), and ['ăšērâ](../../strongs/h/h842.md), on every [gāḇōha](../../strongs/h/h1364.md) [giḇʿâ](../../strongs/h/h1389.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md).

<a name="1kings_14_24"></a>1Kings 14:24

And there were also [qāḏēš](../../strongs/h/h6945.md) in the ['erets](../../strongs/h/h776.md): and they ['asah](../../strongs/h/h6213.md) according to all the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md) which [Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_14_25"></a>1Kings 14:25

And it came to pass in the fifth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), that [Šîšaq](../../strongs/h/h7895.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) against [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="1kings_14_26"></a>1Kings 14:26

And he [laqach](../../strongs/h/h3947.md) the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md); he even [laqach](../../strongs/h/h3947.md) all: and he [laqach](../../strongs/h/h3947.md) all the [magen](../../strongs/h/h4043.md) of [zāhāḇ](../../strongs/h/h2091.md) which [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md).

<a name="1kings_14_27"></a>1Kings 14:27

And [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) ['asah](../../strongs/h/h6213.md) in their stead [nᵊḥšeṯ](../../strongs/h/h5178.md) [magen](../../strongs/h/h4043.md), and [paqad](../../strongs/h/h6485.md) them unto the [yad](../../strongs/h/h3027.md) of the [śar](../../strongs/h/h8269.md) of the [rûṣ](../../strongs/h/h7323.md), which [shamar](../../strongs/h/h8104.md) the [peṯaḥ](../../strongs/h/h6607.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="1kings_14_28"></a>1Kings 14:28

And it was so, [day](../../strongs/h/h1767.md) the [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), that the [rûṣ](../../strongs/h/h7323.md) [nasa'](../../strongs/h/h5375.md) them, and [shuwb](../../strongs/h/h7725.md) them into the [rûṣ](../../strongs/h/h7323.md) [tā'](../../strongs/h/h8372.md).

<a name="1kings_14_29"></a>1Kings 14:29

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), and all that he ['asah](../../strongs/h/h6213.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="1kings_14_30"></a>1Kings 14:30

And there was [milḥāmâ](../../strongs/h/h4421.md) between [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) and [YārāḇʿĀm](../../strongs/h/h3379.md) all their [yowm](../../strongs/h/h3117.md).

<a name="1kings_14_31"></a>1Kings 14:31

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [naʿămâ](../../strongs/h/h5279.md) an [ʿammônîṯ](../../strongs/h/h5985.md). And ['Ăḇîyām](../../strongs/h/h38.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 13](1kings_13.md) - [1Kings 15](1kings_15.md)