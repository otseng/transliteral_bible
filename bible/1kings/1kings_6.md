# [1Kings 6](https://www.blueletterbible.org/kjv/1kings/6)

<a name="1kings_6_1"></a>1Kings 6:1

And it came to pass in the four hundred [šānâ](../../strongs/h/h8141.md) and eightieth [šānâ](../../strongs/h/h8141.md) after the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in the fourth [šānâ](../../strongs/h/h8141.md) of [Šᵊlōmô](../../strongs/h/h8010.md) [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md), in the [ḥōḏeš](../../strongs/h/h2320.md) [Ziv](../../strongs/h/h2099.md), which is the second [ḥōḏeš](../../strongs/h/h2320.md), that he began to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_6_2"></a>1Kings 6:2

And the [bayith](../../strongs/h/h1004.md) which [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) for [Yĕhovah](../../strongs/h/h3068.md), the ['ōreḵ](../../strongs/h/h753.md) thereof was threescore ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) thereof twenty cubits, and the [qômâ](../../strongs/h/h6967.md) thereof thirty ['ammâ](../../strongs/h/h520.md).

<a name="1kings_6_3"></a>1Kings 6:3

And the ['ûlām](../../strongs/h/h197.md) [paniym](../../strongs/h/h6440.md) the [heykal](../../strongs/h/h1964.md) of the [bayith](../../strongs/h/h1004.md), twenty ['ammâ](../../strongs/h/h520.md) was the ['ōreḵ](../../strongs/h/h753.md) thereof, according to the [rōḥaḇ](../../strongs/h/h7341.md) of the [bayith](../../strongs/h/h1004.md); and ten ['ammâ](../../strongs/h/h520.md) was the [rōḥaḇ](../../strongs/h/h7341.md) thereof before the [bayith](../../strongs/h/h1004.md).

<a name="1kings_6_4"></a>1Kings 6:4

And for the [bayith](../../strongs/h/h1004.md) he ['asah](../../strongs/h/h6213.md) [ḥallôn](../../strongs/h/h2474.md) of ['āṭam](../../strongs/h/h331.md) [šᵊqup̄îm](../../strongs/h/h8261.md).

<a name="1kings_6_5"></a>1Kings 6:5

And against the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) he [bānâ](../../strongs/h/h1129.md) [yāṣûaʿ](../../strongs/h/h3326.md) [yāṣûaʿ](../../strongs/h/h3326.md) [cabiyb](../../strongs/h/h5439.md), against the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) [cabiyb](../../strongs/h/h5439.md), both of the [heykal](../../strongs/h/h1964.md) and of the [dᵊḇîr](../../strongs/h/h1687.md): and he ['asah](../../strongs/h/h6213.md) [tsela'](../../strongs/h/h6763.md) [cabiyb](../../strongs/h/h5439.md):

<a name="1kings_6_6"></a>1Kings 6:6

The [taḥtôn](../../strongs/h/h8481.md) [yāṣûaʿ](../../strongs/h/h3326.md) [yāṣûaʿ](../../strongs/h/h3326.md) was five ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md), and the [tîḵôn](../../strongs/h/h8484.md) was six ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md), and the third was seven ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md): for without [ḥûṣ](../../strongs/h/h2351.md) the [bayith](../../strongs/h/h1004.md) he [nathan](../../strongs/h/h5414.md) [miḡrāʿôṯ](../../strongs/h/h4052.md) [cabiyb](../../strongs/h/h5439.md), that should not be ['āḥaz](../../strongs/h/h270.md) in the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md).

<a name="1kings_6_7"></a>1Kings 6:7

And the [bayith](../../strongs/h/h1004.md), when it was in [bānâ](../../strongs/h/h1129.md), was [bānâ](../../strongs/h/h1129.md) of ['eben](../../strongs/h/h68.md) made [šālēm](../../strongs/h/h8003.md) before it was [massāʿ](../../strongs/h/h4551.md) thither: so that there was neither [maqqāḇâ](../../strongs/h/h4717.md) nor [garzen](../../strongs/h/h1631.md) nor any [kĕliy](../../strongs/h/h3627.md) of [barzel](../../strongs/h/h1270.md) [shama'](../../strongs/h/h8085.md) in the [bayith](../../strongs/h/h1004.md), while it was in [bānâ](../../strongs/h/h1129.md).

<a name="1kings_6_8"></a>1Kings 6:8

The [peṯaḥ](../../strongs/h/h6607.md) for the [tîḵôn](../../strongs/h/h8484.md) [tsela'](../../strongs/h/h6763.md) was in the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md): and they [ʿālâ](../../strongs/h/h5927.md) with [lûl](../../strongs/h/h3883.md) into the [tîḵôn](../../strongs/h/h8484.md), and out of the [tîḵôn](../../strongs/h/h8484.md) into the third.

<a name="1kings_6_9"></a>1Kings 6:9

So he [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md), and [kalah](../../strongs/h/h3615.md) it; and [sāp̄an](../../strongs/h/h5603.md) the [bayith](../../strongs/h/h1004.md) with [gēḇ](../../strongs/h/h1356.md) and [śᵊḏērâ](../../strongs/h/h7713.md) of ['erez](../../strongs/h/h730.md).

<a name="1kings_6_10"></a>1Kings 6:10

And then he [bānâ](../../strongs/h/h1129.md) [yāṣûaʿ](../../strongs/h/h3326.md) [yāṣûaʿ](../../strongs/h/h3326.md) against all the [bayith](../../strongs/h/h1004.md), five ['ammâ](../../strongs/h/h520.md) [qômâ](../../strongs/h/h6967.md): and they ['āḥaz](../../strongs/h/h270.md) on the [bayith](../../strongs/h/h1004.md) with ['ets](../../strongs/h/h6086.md) of ['erez](../../strongs/h/h730.md).

<a name="1kings_6_11"></a>1Kings 6:11

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Šᵊlōmô](../../strongs/h/h8010.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_6_12"></a>1Kings 6:12

Concerning this [bayith](../../strongs/h/h1004.md) which thou art in [bānâ](../../strongs/h/h1129.md), if thou wilt [yālaḵ](../../strongs/h/h3212.md) in my [chuqqah](../../strongs/h/h2708.md), and ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), and [shamar](../../strongs/h/h8104.md) all my [mitsvah](../../strongs/h/h4687.md) to [yālaḵ](../../strongs/h/h3212.md) in them; then will I [quwm](../../strongs/h/h6965.md) my [dabar](../../strongs/h/h1697.md) with thee, which I [dabar](../../strongs/h/h1696.md) unto [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md):

<a name="1kings_6_13"></a>1Kings 6:13

And I will [shakan](../../strongs/h/h7931.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and will not ['azab](../../strongs/h/h5800.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_6_14"></a>1Kings 6:14

So [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md), and [kalah](../../strongs/h/h3615.md) it.

<a name="1kings_6_15"></a>1Kings 6:15

And he [bānâ](../../strongs/h/h1129.md) the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) [bayith](../../strongs/h/h1004.md) with [tsela'](../../strongs/h/h6763.md) of ['erez](../../strongs/h/h730.md), both the [qarqaʿ](../../strongs/h/h7172.md) of the [bayith](../../strongs/h/h1004.md), and the [qîr](../../strongs/h/h7023.md) of the [sipun](../../strongs/h/h5604.md): and he [ṣāp̄â](../../strongs/h/h6823.md) them on the [bayith](../../strongs/h/h1004.md) with ['ets](../../strongs/h/h6086.md), and [ṣāp̄â](../../strongs/h/h6823.md) the [qarqaʿ](../../strongs/h/h7172.md) of the [bayith](../../strongs/h/h1004.md) with [tsela'](../../strongs/h/h6763.md) of [bᵊrôš](../../strongs/h/h1265.md).

<a name="1kings_6_16"></a>1Kings 6:16

And he [bānâ](../../strongs/h/h1129.md) twenty ['ammâ](../../strongs/h/h520.md) on the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [bayith](../../strongs/h/h1004.md), both the [qarqaʿ](../../strongs/h/h7172.md) and the [qîr](../../strongs/h/h7023.md) with [tsela'](../../strongs/h/h6763.md) of ['erez](../../strongs/h/h730.md): he even [bānâ](../../strongs/h/h1129.md) them for it [bayith](../../strongs/h/h1004.md), even for the [dᵊḇîr](../../strongs/h/h1687.md), even for the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) place.

<a name="1kings_6_17"></a>1Kings 6:17

And the [bayith](../../strongs/h/h1004.md), that is, the [heykal](../../strongs/h/h1964.md) [lip̄nê](../../strongs/h/h3942.md) it, was forty ['ammâ](../../strongs/h/h520.md) long.

<a name="1kings_6_18"></a>1Kings 6:18

And the ['erez](../../strongs/h/h730.md) of the [bayith](../../strongs/h/h1004.md) [pᵊnîmâ](../../strongs/h/h6441.md) was [miqlaʿaṯ](../../strongs/h/h4734.md) with [pᵊqāʿîm](../../strongs/h/h6497.md) and [pāṭar](../../strongs/h/h6362.md) [tsiyts](../../strongs/h/h6731.md): all was ['erez](../../strongs/h/h730.md); there was no ['eben](../../strongs/h/h68.md) [ra'ah](../../strongs/h/h7200.md).

<a name="1kings_6_19"></a>1Kings 6:19

And the [dᵊḇîr](../../strongs/h/h1687.md) he [kuwn](../../strongs/h/h3559.md) in the [bayith](../../strongs/h/h1004.md) [pᵊnîmâ](../../strongs/h/h6441.md), to [nathan](../../strongs/h/h5414.md) there the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_6_20"></a>1Kings 6:20

And the [dᵊḇîr](../../strongs/h/h1687.md) in the [paniym](../../strongs/h/h6440.md) was twenty ['ammâ](../../strongs/h/h520.md) in ['ōreḵ](../../strongs/h/h753.md), and twenty ['ammâ](../../strongs/h/h520.md) in [rōḥaḇ](../../strongs/h/h7341.md), and twenty ['ammâ](../../strongs/h/h520.md) in the [qômâ](../../strongs/h/h6967.md) thereof: and he [ṣāp̄â](../../strongs/h/h6823.md) it with [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md); and so [ṣāp̄â](../../strongs/h/h6823.md) the [mizbeach](../../strongs/h/h4196.md) which was of ['erez](../../strongs/h/h730.md).

<a name="1kings_6_21"></a>1Kings 6:21

So [Šᵊlōmô](../../strongs/h/h8010.md) [ṣāp̄â](../../strongs/h/h6823.md) the [bayith](../../strongs/h/h1004.md) [pᵊnîmâ](../../strongs/h/h6441.md) with [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md): and he made an ['abar](../../strongs/h/h5674.md) by the [rṯyqh](../../strongs/h/h7572.md) [rṯyqh](../../strongs/h/h7572.md) of [zāhāḇ](../../strongs/h/h2091.md) [paniym](../../strongs/h/h6440.md) the [dᵊḇîr](../../strongs/h/h1687.md); and he [ṣāp̄â](../../strongs/h/h6823.md) it with [zāhāḇ](../../strongs/h/h2091.md).

<a name="1kings_6_22"></a>1Kings 6:22

And the [bayith](../../strongs/h/h1004.md) he [ṣāp̄â](../../strongs/h/h6823.md) with [zāhāḇ](../../strongs/h/h2091.md), until he had [tamam](../../strongs/h/h8552.md) all the [bayith](../../strongs/h/h1004.md): also the [mizbeach](../../strongs/h/h4196.md) that was by the [dᵊḇîr](../../strongs/h/h1687.md) he [ṣāp̄â](../../strongs/h/h6823.md) with [zāhāḇ](../../strongs/h/h2091.md).

<a name="1kings_6_23"></a>1Kings 6:23

And within the [dᵊḇîr](../../strongs/h/h1687.md) he ['asah](../../strongs/h/h6213.md) two [kĕruwb](../../strongs/h/h3742.md) of [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md), each ten ['ammâ](../../strongs/h/h520.md) [qômâ](../../strongs/h/h6967.md).

<a name="1kings_6_24"></a>1Kings 6:24

And five ['ammâ](../../strongs/h/h520.md) was the one [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md), and five ['ammâ](../../strongs/h/h520.md) the other [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md): from the [qāṣâ](../../strongs/h/h7098.md) of the one [kanaph](../../strongs/h/h3671.md) unto the [qāṣâ](../../strongs/h/h7098.md) of the [kanaph](../../strongs/h/h3671.md) were ten ['ammâ](../../strongs/h/h520.md).

<a name="1kings_6_25"></a>1Kings 6:25

And the other [kĕruwb](../../strongs/h/h3742.md) was ten ['ammâ](../../strongs/h/h520.md): both the [kĕruwb](../../strongs/h/h3742.md) were of one [midâ](../../strongs/h/h4060.md) and one [qeṣeḇ](../../strongs/h/h7095.md).

<a name="1kings_6_26"></a>1Kings 6:26

The [qômâ](../../strongs/h/h6967.md) of the one [kĕruwb](../../strongs/h/h3742.md) was ten ['ammâ](../../strongs/h/h520.md), and so was it of the other [kĕruwb](../../strongs/h/h3742.md).

<a name="1kings_6_27"></a>1Kings 6:27

And he [nathan](../../strongs/h/h5414.md) the [kĕruwb](../../strongs/h/h3742.md) within the [pᵊnîmî](../../strongs/h/h6442.md) [bayith](../../strongs/h/h1004.md): and they [pāraś](../../strongs/h/h6566.md) the [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md), so that the [kanaph](../../strongs/h/h3671.md) of the one [naga'](../../strongs/h/h5060.md) the one [qîr](../../strongs/h/h7023.md), and the [kanaph](../../strongs/h/h3671.md) of the other [kĕruwb](../../strongs/h/h3742.md) [naga'](../../strongs/h/h5060.md) the other [qîr](../../strongs/h/h7023.md); and their [kanaph](../../strongs/h/h3671.md) [naga'](../../strongs/h/h5060.md) [kanaph](../../strongs/h/h3671.md) [kanaph](../../strongs/h/h3671.md) in the midst of the [bayith](../../strongs/h/h1004.md).

<a name="1kings_6_28"></a>1Kings 6:28

And he [ṣāp̄â](../../strongs/h/h6823.md) the [kĕruwb](../../strongs/h/h3742.md) with [zāhāḇ](../../strongs/h/h2091.md).

<a name="1kings_6_29"></a>1Kings 6:29

And he [qālaʿ](../../strongs/h/h7049.md) all the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) round [mēsaḇ](../../strongs/h/h4524.md) with [pitûaḥ](../../strongs/h/h6603.md) [miqlaʿaṯ](../../strongs/h/h4734.md) of [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md) and [pāṭar](../../strongs/h/h6362.md) [tsiyts](../../strongs/h/h6731.md), [pᵊnîmâ](../../strongs/h/h6441.md) and [ḥîṣôn](../../strongs/h/h2435.md).

<a name="1kings_6_30"></a>1Kings 6:30

And the [qarqaʿ](../../strongs/h/h7172.md) of the [bayith](../../strongs/h/h1004.md) he [ṣāp̄â](../../strongs/h/h6823.md) with [zāhāḇ](../../strongs/h/h2091.md), [pᵊnîmâ](../../strongs/h/h6441.md) and [ḥîṣôn](../../strongs/h/h2435.md).

<a name="1kings_6_31"></a>1Kings 6:31

And for the [peṯaḥ](../../strongs/h/h6607.md) of the [dᵊḇîr](../../strongs/h/h1687.md) he ['asah](../../strongs/h/h6213.md) [deleṯ](../../strongs/h/h1817.md) of [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md): the ['ayil](../../strongs/h/h352.md) and side [mᵊzûzâ](../../strongs/h/h4201.md) were a fifth part of the wall.

<a name="1kings_6_32"></a>1Kings 6:32

The two [deleṯ](../../strongs/h/h1817.md) also were of [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md); and he [qālaʿ](../../strongs/h/h7049.md) upon them [miqlaʿaṯ](../../strongs/h/h4734.md) of [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md) and [pāṭar](../../strongs/h/h6362.md) [tsiyts](../../strongs/h/h6731.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md), and [rāḏaḏ](../../strongs/h/h7286.md) [zāhāḇ](../../strongs/h/h2091.md) upon the [kĕruwb](../../strongs/h/h3742.md), and upon the [timmōrâ](../../strongs/h/h8561.md).

<a name="1kings_6_33"></a>1Kings 6:33

So also ['asah](../../strongs/h/h6213.md) he for the [peṯaḥ](../../strongs/h/h6607.md) of the [heykal](../../strongs/h/h1964.md) [mᵊzûzâ](../../strongs/h/h4201.md) of [šemen](../../strongs/h/h8081.md) ['ets](../../strongs/h/h6086.md), a fourth part.

<a name="1kings_6_34"></a>1Kings 6:34

And the two [deleṯ](../../strongs/h/h1817.md) were of [bᵊrôš](../../strongs/h/h1265.md) ['ets](../../strongs/h/h6086.md): the two [tsela'](../../strongs/h/h6763.md) of the one [deleṯ](../../strongs/h/h1817.md) were [Gālîl](../../strongs/h/h1550.md), and the two [qelaʿ](../../strongs/h/h7050.md) of the other [deleṯ](../../strongs/h/h1817.md) were [Gālîl](../../strongs/h/h1550.md).

<a name="1kings_6_35"></a>1Kings 6:35

And he [qālaʿ](../../strongs/h/h7049.md) thereon [kĕruwb](../../strongs/h/h3742.md) and [timmōrâ](../../strongs/h/h8561.md) and [pāṭar](../../strongs/h/h6362.md) [tsiyts](../../strongs/h/h6731.md): and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md) [yashar](../../strongs/h/h3474.md) upon the [ḥāqâ](../../strongs/h/h2707.md).

<a name="1kings_6_36"></a>1Kings 6:36

And he [bānâ](../../strongs/h/h1129.md) the [pᵊnîmî](../../strongs/h/h6442.md) [ḥāṣēr](../../strongs/h/h2691.md) with three [ṭûr](../../strongs/h/h2905.md) of [gāzîṯ](../../strongs/h/h1496.md), and a [ṭûr](../../strongs/h/h2905.md) of ['erez](../../strongs/h/h730.md) [kᵊruṯôṯ](../../strongs/h/h3773.md).

<a name="1kings_6_37"></a>1Kings 6:37

In the fourth [šānâ](../../strongs/h/h8141.md) was the [yacad](../../strongs/h/h3245.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) [yacad](../../strongs/h/h3245.md), in the [yeraḥ](../../strongs/h/h3391.md) [Ziv](../../strongs/h/h2099.md):

<a name="1kings_6_38"></a>1Kings 6:38

And in the eleventh [šānâ](../../strongs/h/h8141.md), in the [yeraḥ](../../strongs/h/h3391.md) [bûl](../../strongs/h/h945.md), which is the eighth [ḥōḏeš](../../strongs/h/h2320.md), was the [bayith](../../strongs/h/h1004.md) [kalah](../../strongs/h/h3615.md) throughout all the [dabar](../../strongs/h/h1697.md) thereof, and according to all the [mishpat](../../strongs/h/h4941.md) of it. So was he seven [šānâ](../../strongs/h/h8141.md) in [bānâ](../../strongs/h/h1129.md) it.

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 5](1kings_5.md) - [1Kings 7](1kings_7.md)