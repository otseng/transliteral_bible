# [1Kings 22](https://www.blueletterbible.org/kjv/1kings/22)

<a name="1kings_22_1"></a>1Kings 22:1

And they [yashab](../../strongs/h/h3427.md) three [šānâ](../../strongs/h/h8141.md) without [milḥāmâ](../../strongs/h/h4421.md) between ['Ărām](../../strongs/h/h758.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_2"></a>1Kings 22:2

And it came to pass in the third [šānâ](../../strongs/h/h8141.md), that [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yarad](../../strongs/h/h3381.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_3"></a>1Kings 22:3

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), [yada'](../../strongs/h/h3045.md) ye that [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) in [Gilʿāḏ](../../strongs/h/h1568.md) is ours, and we be [ḥāšâ](../../strongs/h/h2814.md), and [laqach](../../strongs/h/h3947.md) it not out of the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md)?

<a name="1kings_22_4"></a>1Kings 22:4

And he ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Wilt thou [yālaḵ](../../strongs/h/h3212.md) with me to [milḥāmâ](../../strongs/h/h4421.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md)? And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), I am as thou art, my ['am](../../strongs/h/h5971.md) as thy ['am](../../strongs/h/h5971.md), my [sûs](../../strongs/h/h5483.md) as thy [sûs](../../strongs/h/h5483.md).

<a name="1kings_22_5"></a>1Kings 22:5

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [darash](../../strongs/h/h1875.md), I pray thee, at the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to [yowm](../../strongs/h/h3117.md).

<a name="1kings_22_6"></a>1Kings 22:6

Then the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [qāḇaṣ](../../strongs/h/h6908.md) the [nāḇî'](../../strongs/h/h5030.md) [qāḇaṣ](../../strongs/h/h6908.md), about four hundred ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md) unto them, Shall I [yālaḵ](../../strongs/h/h3212.md) against [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md) to [milḥāmâ](../../strongs/h/h4421.md), or shall I [ḥāḏal](../../strongs/h/h2308.md)? And they ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md); for the ['adonay](../../strongs/h/h136.md) shall [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="1kings_22_7"></a>1Kings 22:7

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), Is there not here a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md) besides, that we might [darash](../../strongs/h/h1875.md) of him?

<a name="1kings_22_8"></a>1Kings 22:8

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), There is yet one ['iysh](../../strongs/h/h376.md), [Mîḵāyhû](../../strongs/h/h4321.md) the [ben](../../strongs/h/h1121.md) of [Yimlā'](../../strongs/h/h3229.md), by whom we may [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md): but I [sane'](../../strongs/h/h8130.md) him; for he doth not [nāḇā'](../../strongs/h/h5012.md) [towb](../../strongs/h/h2896.md) concerning me, but [ra'](../../strongs/h/h7451.md). And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), Let not the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) so.

<a name="1kings_22_9"></a>1Kings 22:9

Then the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [qara'](../../strongs/h/h7121.md) a [sārîs](../../strongs/h/h5631.md), and ['āmar](../../strongs/h/h559.md), [māhar](../../strongs/h/h4116.md) hither [Mîḵāyhû](../../strongs/h/h4321.md) the [ben](../../strongs/h/h1121.md) of [Yimlā'](../../strongs/h/h3229.md).

<a name="1kings_22_10"></a>1Kings 22:10

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yashab](../../strongs/h/h3427.md) ['iysh](../../strongs/h/h376.md) on his [kicce'](../../strongs/h/h3678.md), having [labash](../../strongs/h/h3847.md) their [beḡeḏ](../../strongs/h/h899.md), in a [gōren](../../strongs/h/h1637.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of [Šōmrôn](../../strongs/h/h8111.md); and all the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) [paniym](../../strongs/h/h6440.md) them.

<a name="1kings_22_11"></a>1Kings 22:11

And [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [KᵊnaʿĂnâ](../../strongs/h/h3668.md) ['asah](../../strongs/h/h6213.md) him [qeren](../../strongs/h/h7161.md) of [barzel](../../strongs/h/h1270.md): and he ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), With these shalt thou [nāḡaḥ](../../strongs/h/h5055.md) the ['Ărām](../../strongs/h/h758.md), until thou have [kalah](../../strongs/h/h3615.md) them.

<a name="1kings_22_12"></a>1Kings 22:12

And all the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) so, ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md), and [tsalach](../../strongs/h/h6743.md): for [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) it into the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md).

<a name="1kings_22_13"></a>1Kings 22:13

And the [mal'ak](../../strongs/h/h4397.md) that was [halak](../../strongs/h/h1980.md) to [qara'](../../strongs/h/h7121.md) [Mîḵāyhû](../../strongs/h/h4321.md) [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), Behold now, the [dabar](../../strongs/h/h1697.md) of the [nāḇî'](../../strongs/h/h5030.md) declare [towb](../../strongs/h/h2896.md) unto the [melek](../../strongs/h/h4428.md) with one [peh](../../strongs/h/h6310.md): let thy [dabar](../../strongs/h/h1697.md), I pray thee, be like the [dabar](../../strongs/h/h1697.md) of one of them, and [dabar](../../strongs/h/h1696.md) that which is [towb](../../strongs/h/h2896.md).

<a name="1kings_22_14"></a>1Kings 22:14

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), what [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, that will I [dabar](../../strongs/h/h1696.md).

<a name="1kings_22_15"></a>1Kings 22:15

So he [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, [Mîḵāyhû](../../strongs/h/h4321.md), shall we [yālaḵ](../../strongs/h/h3212.md) against [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md) to [milḥāmâ](../../strongs/h/h4421.md), or shall we [ḥāḏal](../../strongs/h/h2308.md)? And he ['āmar](../../strongs/h/h559.md) him, [ʿālâ](../../strongs/h/h5927.md), and [tsalach](../../strongs/h/h6743.md): for [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="1kings_22_16"></a>1Kings 22:16

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, How many [pa'am](../../strongs/h/h6471.md) shall I [shaba'](../../strongs/h/h7650.md) thee that thou [dabar](../../strongs/h/h1696.md) me nothing but that which is ['emeth](../../strongs/h/h571.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="1kings_22_17"></a>1Kings 22:17

And he ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) all [Yisra'el](../../strongs/h/h3478.md) [puwts](../../strongs/h/h6327.md) upon the [har](../../strongs/h/h2022.md), as [tso'n](../../strongs/h/h6629.md) that have not a [ra'ah](../../strongs/h/h7462.md): and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), These have no ['adown](../../strongs/h/h113.md): let them [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md) in [shalowm](../../strongs/h/h7965.md).

<a name="1kings_22_18"></a>1Kings 22:18

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Did I not ['āmar](../../strongs/h/h559.md) thee that he would [nāḇā'](../../strongs/h/h5012.md) no [towb](../../strongs/h/h2896.md) concerning me, but [ra'](../../strongs/h/h7451.md)?

<a name="1kings_22_19"></a>1Kings 22:19

And he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) thou therefore the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md): I [ra'ah](../../strongs/h/h7200.md) [Yĕhovah](../../strongs/h/h3068.md) [yashab](../../strongs/h/h3427.md) on his [kicce'](../../strongs/h/h3678.md), and all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) ['amad](../../strongs/h/h5975.md) by him on his [yamiyn](../../strongs/h/h3225.md) and on his [śᵊmō'l](../../strongs/h/h8040.md).

<a name="1kings_22_20"></a>1Kings 22:20

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Who shall [pāṯâ](../../strongs/h/h6601.md) ['Aḥ'Āḇ](../../strongs/h/h256.md), that he may [ʿālâ](../../strongs/h/h5927.md) and [naphal](../../strongs/h/h5307.md) at [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md)? And one ['āmar](../../strongs/h/h559.md) on this manner, and another ['āmar](../../strongs/h/h559.md) on that manner.

<a name="1kings_22_21"></a>1Kings 22:21

And there [yāṣā'](../../strongs/h/h3318.md) a [ruwach](../../strongs/h/h7307.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), I will [pāṯâ](../../strongs/h/h6601.md) him.

<a name="1kings_22_22"></a>1Kings 22:22

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Wherewith? And he ['āmar](../../strongs/h/h559.md), I will [yāṣā'](../../strongs/h/h3318.md), and I will be a [sheqer](../../strongs/h/h8267.md) [ruwach](../../strongs/h/h7307.md) in the [peh](../../strongs/h/h6310.md) of all his [nāḇî'](../../strongs/h/h5030.md). And he ['āmar](../../strongs/h/h559.md), Thou shalt [pāṯâ](../../strongs/h/h6601.md) him, and [yakol](../../strongs/h/h3201.md) also: [yāṣā'](../../strongs/h/h3318.md), and ['asah](../../strongs/h/h6213.md) so.

<a name="1kings_22_23"></a>1Kings 22:23

Now therefore, behold, [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) a [sheqer](../../strongs/h/h8267.md) [ruwach](../../strongs/h/h7307.md) in the [peh](../../strongs/h/h6310.md) of all these thy [nāḇî'](../../strongs/h/h5030.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) [ra'](../../strongs/h/h7451.md) concerning thee.

<a name="1kings_22_24"></a>1Kings 22:24

But [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [KᵊnaʿĂnâ](../../strongs/h/h3668.md) went [nāḡaš](../../strongs/h/h5066.md), and [nakah](../../strongs/h/h5221.md) [Mîḵāyhû](../../strongs/h/h4321.md) on the [lᵊḥî](../../strongs/h/h3895.md), and ['āmar](../../strongs/h/h559.md), Which way ['abar](../../strongs/h/h5674.md) the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) from me to [dabar](../../strongs/h/h1696.md) unto thee?

<a name="1kings_22_25"></a>1Kings 22:25

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), Behold, thou shalt [ra'ah](../../strongs/h/h7200.md) in that [yowm](../../strongs/h/h3117.md), when thou shalt [bow'](../../strongs/h/h935.md) into an [ḥeḏer](../../strongs/h/h2315.md) [ḥeḏer](../../strongs/h/h2315.md) to [ḥāḇâ](../../strongs/h/h2247.md) thyself.

<a name="1kings_22_26"></a>1Kings 22:26

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) [Mîḵāyhû](../../strongs/h/h4321.md), and [shuwb](../../strongs/h/h7725.md) him unto ['Āmôn](../../strongs/h/h526.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md), and to [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md);

<a name="1kings_22_27"></a>1Kings 22:27

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), [śûm](../../strongs/h/h7760.md) this in the [bayith](../../strongs/h/h1004.md) [kele'](../../strongs/h/h3608.md), and ['akal](../../strongs/h/h398.md) him with [lechem](../../strongs/h/h3899.md) of [laḥaṣ](../../strongs/h/h3906.md) and with [mayim](../../strongs/h/h4325.md) of [laḥaṣ](../../strongs/h/h3906.md), until I [bow'](../../strongs/h/h935.md) in [shalowm](../../strongs/h/h7965.md).

<a name="1kings_22_28"></a>1Kings 22:28

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), If thou [shuwb](../../strongs/h/h7725.md) at [shuwb](../../strongs/h/h7725.md) in [shalowm](../../strongs/h/h7965.md), [Yĕhovah](../../strongs/h/h3068.md) hath not [dabar](../../strongs/h/h1696.md) by me. And he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md), O ['am](../../strongs/h/h5971.md), every one of you.

<a name="1kings_22_29"></a>1Kings 22:29

So the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ʿālâ](../../strongs/h/h5927.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="1kings_22_30"></a>1Kings 22:30

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), I will [ḥāp̄aś](../../strongs/h/h2664.md) myself, and [bow'](../../strongs/h/h935.md) into the [milḥāmâ](../../strongs/h/h4421.md); but put thou [labash](../../strongs/h/h3847.md) thy [beḡeḏ](../../strongs/h/h899.md). And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥāp̄aś](../../strongs/h/h2664.md) himself, and [bow'](../../strongs/h/h935.md) into the [milḥāmâ](../../strongs/h/h4421.md).

<a name="1kings_22_31"></a>1Kings 22:31

But the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [tsavah](../../strongs/h/h6680.md) his thirty and two [śar](../../strongs/h/h8269.md) that had rule over his [reḵeḇ](../../strongs/h/h7393.md), ['āmar](../../strongs/h/h559.md), [lāḥam](../../strongs/h/h3898.md) neither with [qāṭān](../../strongs/h/h6996.md) nor [gadowl](../../strongs/h/h1419.md), save only with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_32"></a>1Kings 22:32

And it came to pass, when the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md) [ra'ah](../../strongs/h/h7200.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), that they ['āmar](../../strongs/h/h559.md), Surely it is the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md). And they [cuwr](../../strongs/h/h5493.md) to [lāḥam](../../strongs/h/h3898.md) against him: and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [zāʿaq](../../strongs/h/h2199.md).

<a name="1kings_22_33"></a>1Kings 22:33

And it came to pass, when the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md) [ra'ah](../../strongs/h/h7200.md) that it was not the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that they [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) him.

<a name="1kings_22_34"></a>1Kings 22:34

And an ['iysh](../../strongs/h/h376.md) [mashak](../../strongs/h/h4900.md) a [qesheth](../../strongs/h/h7198.md) at a [tom](../../strongs/h/h8537.md), and [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) between the [deḇeq](../../strongs/h/h1694.md) of the [širyôn](../../strongs/h/h8302.md): wherefore he ['āmar](../../strongs/h/h559.md) unto the [rakāḇ](../../strongs/h/h7395.md), [hāp̄aḵ](../../strongs/h/h2015.md) thine [yad](../../strongs/h/h3027.md), and [yāṣā'](../../strongs/h/h3318.md) me of the [maḥănê](../../strongs/h/h4264.md); for I am [ḥālâ](../../strongs/h/h2470.md).

<a name="1kings_22_35"></a>1Kings 22:35

And the [milḥāmâ](../../strongs/h/h4421.md) [ʿālâ](../../strongs/h/h5927.md) that [yowm](../../strongs/h/h3117.md): and the [melek](../../strongs/h/h4428.md) was ['amad](../../strongs/h/h5975.md) in his [merkāḇâ](../../strongs/h/h4818.md) against the ['Ărām](../../strongs/h/h758.md), and [muwth](../../strongs/h/h4191.md) at ['ereb](../../strongs/h/h6153.md): and the [dam](../../strongs/h/h1818.md) ran [yāṣaq](../../strongs/h/h3332.md) of the [makâ](../../strongs/h/h4347.md) into the [ḥêq](../../strongs/h/h2436.md) of the [reḵeḇ](../../strongs/h/h7393.md).

<a name="1kings_22_36"></a>1Kings 22:36

And there ['abar](../../strongs/h/h5674.md) a [rinnah](../../strongs/h/h7440.md) throughout the [maḥănê](../../strongs/h/h4264.md) about the [bow'](../../strongs/h/h935.md) of the [šemeš](../../strongs/h/h8121.md), ['āmar](../../strongs/h/h559.md), Every ['iysh](../../strongs/h/h376.md) to his [ʿîr](../../strongs/h/h5892.md), and every ['iysh](../../strongs/h/h376.md) to his own ['erets](../../strongs/h/h776.md).

<a name="1kings_22_37"></a>1Kings 22:37

So the [melek](../../strongs/h/h4428.md) [muwth](../../strongs/h/h4191.md), and was [bow'](../../strongs/h/h935.md) to [Šōmrôn](../../strongs/h/h8111.md); and they [qāḇar](../../strongs/h/h6912.md) the [melek](../../strongs/h/h4428.md) in [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_22_38"></a>1Kings 22:38

And one [šāṭap̄](../../strongs/h/h7857.md) the [reḵeḇ](../../strongs/h/h7393.md) in the [bᵊrēḵâ](../../strongs/h/h1295.md) of [Šōmrôn](../../strongs/h/h8111.md); and the [keleḇ](../../strongs/h/h3611.md) [lāqaq](../../strongs/h/h3952.md) his [dam](../../strongs/h/h1818.md); and they [rāḥaṣ](../../strongs/h/h7364.md) his [zōnôṯ](../../strongs/h/h2185.md) ; according unto the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which he [dabar](../../strongs/h/h1696.md).

<a name="1kings_22_39"></a>1Kings 22:39

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), and all that he ['asah](../../strongs/h/h6213.md), and the [šēn](../../strongs/h/h8127.md) [bayith](../../strongs/h/h1004.md) which he [bānâ](../../strongs/h/h1129.md), and all the [ʿîr](../../strongs/h/h5892.md) that he [bānâ](../../strongs/h/h1129.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="1kings_22_40"></a>1Kings 22:40

So ['Aḥ'Āḇ](../../strongs/h/h256.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md); and ['Ăḥazyâ](../../strongs/h/h274.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_22_41"></a>1Kings 22:41

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of ['Āsā'](../../strongs/h/h609.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md) in the fourth [šānâ](../../strongs/h/h8141.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_42"></a>1Kings 22:42

[Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) was thirty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md); and he [mālaḵ](../../strongs/h/h4427.md) twenty and five [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [ʿĂzûḇâ](../../strongs/h/h5806.md) the [bath](../../strongs/h/h1323.md) of [Šilḥî](../../strongs/h/h7977.md).

<a name="1kings_22_43"></a>1Kings 22:43

And he [yālaḵ](../../strongs/h/h3212.md) in all the [derek](../../strongs/h/h1870.md) of ['Āsā'](../../strongs/h/h609.md) his ['ab](../../strongs/h/h1.md); he [cuwr](../../strongs/h/h5493.md) not from it, ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): nevertheless the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md); for the ['am](../../strongs/h/h5971.md) [zabach](../../strongs/h/h2076.md) and [qāṭar](../../strongs/h/h6999.md) yet in the [bāmâ](../../strongs/h/h1116.md).

<a name="1kings_22_44"></a>1Kings 22:44

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) made [shalam](../../strongs/h/h7999.md) with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_45"></a>1Kings 22:45

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), and his [gᵊḇûrâ](../../strongs/h/h1369.md) that he ['asah](../../strongs/h/h6213.md), and how he [lāḥam](../../strongs/h/h3898.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [dabar](../../strongs/h/h1697.md) [yowm](../../strongs/h/h3117.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)?

<a name="1kings_22_46"></a>1Kings 22:46

And the [yeṯer](../../strongs/h/h3499.md) of the [qāḏēš](../../strongs/h/h6945.md), which [šā'ar](../../strongs/h/h7604.md) in the [yowm](../../strongs/h/h3117.md) of his ['ab](../../strongs/h/h1.md) ['Āsā'](../../strongs/h/h609.md), he [bāʿar](../../strongs/h/h1197.md) out of the ['erets](../../strongs/h/h776.md).

<a name="1kings_22_47"></a>1Kings 22:47

There was then no [melek](../../strongs/h/h4428.md) in ['Ĕḏōm](../../strongs/h/h123.md): a [nāṣaḇ](../../strongs/h/h5324.md) was [melek](../../strongs/h/h4428.md).

<a name="1kings_22_48"></a>1Kings 22:48

[Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['asah](../../strongs/h/h6213.md) ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md) to [yālaḵ](../../strongs/h/h3212.md) to ['Ôp̄îr](../../strongs/h/h211.md) for [zāhāḇ](../../strongs/h/h2091.md): but they [halak](../../strongs/h/h1980.md) not; for the ['ŏnîyâ](../../strongs/h/h591.md) were [shabar](../../strongs/h/h7665.md) at [ʿEṣyôn Geḇer](../../strongs/h/h6100.md).

<a name="1kings_22_49"></a>1Kings 22:49

Then ['āmar](../../strongs/h/h559.md) ['Ăḥazyâ](../../strongs/h/h274.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Let my ['ebed](../../strongs/h/h5650.md) [yālaḵ](../../strongs/h/h3212.md) with thy ['ebed](../../strongs/h/h5650.md) in the ['ŏnîyâ](../../strongs/h/h591.md). But [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āḇâ](../../strongs/h/h14.md) not.

<a name="1kings_22_50"></a>1Kings 22:50

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and [Yᵊhôrām](../../strongs/h/h3088.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="1kings_22_51"></a>1Kings 22:51

['Ăḥazyâ](../../strongs/h/h274.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md) in [Šōmrôn](../../strongs/h/h8111.md) the seventeenth [šānâ](../../strongs/h/h8141.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) two [šānâ](../../strongs/h/h8141.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_22_52"></a>1Kings 22:52

And he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of his ['ab](../../strongs/h/h1.md), and in the [derek](../../strongs/h/h1870.md) of his ['em](../../strongs/h/h517.md), and in the [derek](../../strongs/h/h1870.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md):

<a name="1kings_22_53"></a>1Kings 22:53

For he ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md), and [shachah](../../strongs/h/h7812.md) him, and provoked to [kāʿas](../../strongs/h/h3707.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), according to all that his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 21](1kings_21.md)