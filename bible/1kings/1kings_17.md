# [1Kings 17](https://www.blueletterbible.org/kjv/1kings/17)

<a name="1kings_17_1"></a>1Kings 17:1

And ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md), who was of the [tôšāḇ](../../strongs/h/h8453.md) of [Gilʿāḏ](../../strongs/h/h1568.md), ['āmar](../../strongs/h/h559.md) unto ['Aḥ'Āḇ](../../strongs/h/h256.md), As [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [chay](../../strongs/h/h2416.md), [paniym](../../strongs/h/h6440.md) whom I ['amad](../../strongs/h/h5975.md), there shall not be [ṭal](../../strongs/h/h2919.md) nor [māṭār](../../strongs/h/h4306.md) these [šānâ](../../strongs/h/h8141.md), but [peh](../../strongs/h/h6310.md) to my [dabar](../../strongs/h/h1697.md).

<a name="1kings_17_2"></a>1Kings 17:2

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto him, ['āmar](../../strongs/h/h559.md),

<a name="1kings_17_3"></a>1Kings 17:3

[yālaḵ](../../strongs/h/h3212.md) thee hence, and [panah](../../strongs/h/h6437.md) thee [qeḏem](../../strongs/h/h6924.md), and [cathar](../../strongs/h/h5641.md) thyself by the [nachal](../../strongs/h/h5158.md) [kᵊrîṯ](../../strongs/h/h3747.md), that is [paniym](../../strongs/h/h6440.md) [Yardēn](../../strongs/h/h3383.md).

<a name="1kings_17_4"></a>1Kings 17:4

And it shall be, that thou shalt [šāṯâ](../../strongs/h/h8354.md) of the [nachal](../../strongs/h/h5158.md); and I have [tsavah](../../strongs/h/h6680.md) the [ʿōrēḇ](../../strongs/h/h6158.md) to [kûl](../../strongs/h/h3557.md) thee there.

<a name="1kings_17_5"></a>1Kings 17:5

So he [yālaḵ](../../strongs/h/h3212.md) and ['asah](../../strongs/h/h6213.md) according unto the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md): for he [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) by the [nachal](../../strongs/h/h5158.md) [kᵊrîṯ](../../strongs/h/h3747.md), that is [paniym](../../strongs/h/h6440.md) [Yardēn](../../strongs/h/h3383.md).

<a name="1kings_17_6"></a>1Kings 17:6

And the [ʿōrēḇ](../../strongs/h/h6158.md) [bow'](../../strongs/h/h935.md) him [lechem](../../strongs/h/h3899.md) and [basar](../../strongs/h/h1320.md) in the [boqer](../../strongs/h/h1242.md), and [lechem](../../strongs/h/h3899.md) and [basar](../../strongs/h/h1320.md) in the ['ereb](../../strongs/h/h6153.md); and he [šāṯâ](../../strongs/h/h8354.md) of the [nachal](../../strongs/h/h5158.md).

<a name="1kings_17_7"></a>1Kings 17:7

And it came to pass [qēṣ](../../strongs/h/h7093.md) a [yowm](../../strongs/h/h3117.md), that the [nachal](../../strongs/h/h5158.md) [yāḇēš](../../strongs/h/h3001.md), because there had been no [gešem](../../strongs/h/h1653.md) in the ['erets](../../strongs/h/h776.md).

<a name="1kings_17_8"></a>1Kings 17:8

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto him, ['āmar](../../strongs/h/h559.md),

<a name="1kings_17_9"></a>1Kings 17:9

[quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) thee to [Ṣārp̄Aṯ](../../strongs/h/h6886.md), which belongeth to [Ṣîḏôn](../../strongs/h/h6721.md), and [yashab](../../strongs/h/h3427.md) there: behold, I have [tsavah](../../strongs/h/h6680.md) an ['almānâ](../../strongs/h/h490.md) ['ishshah](../../strongs/h/h802.md) there to [kûl](../../strongs/h/h3557.md) thee.

<a name="1kings_17_10"></a>1Kings 17:10

So he [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) to [Ṣārp̄Aṯ](../../strongs/h/h6886.md). And when he [bow'](../../strongs/h/h935.md) to the [peṯaḥ](../../strongs/h/h6607.md) of the [ʿîr](../../strongs/h/h5892.md), behold, the ['almānâ](../../strongs/h/h490.md) ['ishshah](../../strongs/h/h802.md) was there [qāšaš](../../strongs/h/h7197.md) of ['ets](../../strongs/h/h6086.md): and he [qara'](../../strongs/h/h7121.md) to her, and ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) me, I pray thee, a [mᵊʿaṭ](../../strongs/h/h4592.md) [mayim](../../strongs/h/h4325.md) in a [kĕliy](../../strongs/h/h3627.md), that I may [šāṯâ](../../strongs/h/h8354.md).

<a name="1kings_17_11"></a>1Kings 17:11

And as she was [yālaḵ](../../strongs/h/h3212.md) to [laqach](../../strongs/h/h3947.md) it, he [qara'](../../strongs/h/h7121.md) to her, and ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) me, I pray thee, a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md) in thine [yad](../../strongs/h/h3027.md).

<a name="1kings_17_12"></a>1Kings 17:12

And she ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [chay](../../strongs/h/h2416.md), I have not a [māʿôḡ](../../strongs/h/h4580.md), but a [mᵊlō'](../../strongs/h/h4393.md) [kaph](../../strongs/h/h3709.md) of [qemaḥ](../../strongs/h/h7058.md) in a [kaḏ](../../strongs/h/h3537.md), and a [mᵊʿaṭ](../../strongs/h/h4592.md) [šemen](../../strongs/h/h8081.md) in a [ṣapaḥaṯ](../../strongs/h/h6835.md): and, behold, I am [qāšaš](../../strongs/h/h7197.md) two ['ets](../../strongs/h/h6086.md), that I may [bow'](../../strongs/h/h935.md) and ['asah](../../strongs/h/h6213.md) it for me and my [ben](../../strongs/h/h1121.md), that we may ['akal](../../strongs/h/h398.md) it, and [muwth](../../strongs/h/h4191.md).

<a name="1kings_17_13"></a>1Kings 17:13

And ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md) unto her, [yare'](../../strongs/h/h3372.md) not; [bow'](../../strongs/h/h935.md) and ['asah](../../strongs/h/h6213.md) as thou hast [dabar](../../strongs/h/h1697.md): but ['asah](../../strongs/h/h6213.md) me thereof a [qāṭān](../../strongs/h/h6996.md) [ʿugâ](../../strongs/h/h5692.md) [ri'šôn](../../strongs/h/h7223.md), and [yāṣā'](../../strongs/h/h3318.md) it unto me, and ['aḥărôn](../../strongs/h/h314.md) ['asah](../../strongs/h/h6213.md) for thee and for thy [ben](../../strongs/h/h1121.md).

<a name="1kings_17_14"></a>1Kings 17:14

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), The [kaḏ](../../strongs/h/h3537.md) of [qemaḥ](../../strongs/h/h7058.md) shall not [kalah](../../strongs/h/h3615.md), neither shall the [ṣapaḥaṯ](../../strongs/h/h6835.md) of [šemen](../../strongs/h/h8081.md) [ḥāsēr](../../strongs/h/h2637.md), until the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [gešem](../../strongs/h/h1653.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="1kings_17_15"></a>1Kings 17:15

And she [yālaḵ](../../strongs/h/h3212.md) and ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of ['Ēlîyâ](../../strongs/h/h452.md): and she, and he, and her [bayith](../../strongs/h/h1004.md), did ['akal](../../strongs/h/h398.md) many [yowm](../../strongs/h/h3117.md).

<a name="1kings_17_16"></a>1Kings 17:16

And the [kaḏ](../../strongs/h/h3537.md) of [qemaḥ](../../strongs/h/h7058.md) [kalah](../../strongs/h/h3615.md) not, neither did the [ṣapaḥaṯ](../../strongs/h/h6835.md) of [šemen](../../strongs/h/h8081.md) [ḥāsēr](../../strongs/h/h2638.md), according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) ['Ēlîyâ](../../strongs/h/h452.md).

<a name="1kings_17_17"></a>1Kings 17:17

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that the [ben](../../strongs/h/h1121.md) of the ['ishshah](../../strongs/h/h802.md), the [baʿălâ](../../strongs/h/h1172.md) of the [bayith](../../strongs/h/h1004.md), [ḥālâ](../../strongs/h/h2470.md); and his [ḥŏlî](../../strongs/h/h2483.md) was [me'od](../../strongs/h/h3966.md) [ḥāzāq](../../strongs/h/h2389.md), that there was no [neshamah](../../strongs/h/h5397.md) [yāṯar](../../strongs/h/h3498.md) in him.

<a name="1kings_17_18"></a>1Kings 17:18

And she ['āmar](../../strongs/h/h559.md) unto ['Ēlîyâ](../../strongs/h/h452.md), What have I to do with thee, O thou ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md)? art thou [bow'](../../strongs/h/h935.md) unto me to [zakar](../../strongs/h/h2142.md) my ['avon](../../strongs/h/h5771.md) to [zakar](../../strongs/h/h2142.md), and to [muwth](../../strongs/h/h4191.md) my [ben](../../strongs/h/h1121.md)?

<a name="1kings_17_19"></a>1Kings 17:19

And he ['āmar](../../strongs/h/h559.md) unto her, [nathan](../../strongs/h/h5414.md) me thy [ben](../../strongs/h/h1121.md). And he [laqach](../../strongs/h/h3947.md) him out of her [ḥêq](../../strongs/h/h2436.md), and [ʿālâ](../../strongs/h/h5927.md) him into a [ʿălîyâ](../../strongs/h/h5944.md), where he [yashab](../../strongs/h/h3427.md), and [shakab](../../strongs/h/h7901.md) him upon his own [mittah](../../strongs/h/h4296.md).

<a name="1kings_17_20"></a>1Kings 17:20

And he [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), hast thou also brought [ra'a'](../../strongs/h/h7489.md) upon the ['almānâ](../../strongs/h/h490.md) with whom I [guwr](../../strongs/h/h1481.md), by [muwth](../../strongs/h/h4191.md) her [ben](../../strongs/h/h1121.md)?

<a name="1kings_17_21"></a>1Kings 17:21

And he [māḏaḏ](../../strongs/h/h4058.md) himself upon the [yeleḏ](../../strongs/h/h3206.md) three [pa'am](../../strongs/h/h6471.md), and [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), I pray thee, let this [yeleḏ](../../strongs/h/h3206.md) [nephesh](../../strongs/h/h5315.md) [shuwb](../../strongs/h/h7725.md) into [qereḇ](../../strongs/h/h7130.md) [shuwb](../../strongs/h/h7725.md).

<a name="1kings_17_22"></a>1Kings 17:22

And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of ['Ēlîyâ](../../strongs/h/h452.md); and the [nephesh](../../strongs/h/h5315.md) of the [yeleḏ](../../strongs/h/h3206.md) [shuwb](../../strongs/h/h7725.md) into [qereḇ](../../strongs/h/h7130.md) [shuwb](../../strongs/h/h7725.md), and he [ḥāyâ](../../strongs/h/h2421.md).

<a name="1kings_17_23"></a>1Kings 17:23

And ['Ēlîyâ](../../strongs/h/h452.md) [laqach](../../strongs/h/h3947.md) the [yeleḏ](../../strongs/h/h3206.md), and [yarad](../../strongs/h/h3381.md) him out of the [ʿălîyâ](../../strongs/h/h5944.md) into the [bayith](../../strongs/h/h1004.md), and [nathan](../../strongs/h/h5414.md) him unto his ['em](../../strongs/h/h517.md): and ['Ēlîyâ](../../strongs/h/h452.md) ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), thy [ben](../../strongs/h/h1121.md) [chay](../../strongs/h/h2416.md).

<a name="1kings_17_24"></a>1Kings 17:24

And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) to ['Ēlîyâ](../../strongs/h/h452.md), Now by this I [yada'](../../strongs/h/h3045.md) that thou art an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), and that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) in thy [peh](../../strongs/h/h6310.md) is ['emeth](../../strongs/h/h571.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 16](1kings_16.md) - [1Kings 18](1kings_18.md)