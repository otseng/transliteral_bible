# [1Kings 21](https://www.blueletterbible.org/kjv/1kings/21)

<a name="1kings_21_1"></a>1Kings 21:1

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md) had a [kerem](../../strongs/h/h3754.md), which was in [YizrᵊʿE'L](../../strongs/h/h3157.md), ['ēṣel](../../strongs/h/h681.md) the [heykal](../../strongs/h/h1964.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Šōmrôn](../../strongs/h/h8111.md).

<a name="1kings_21_2"></a>1Kings 21:2

And ['Aḥ'Āḇ](../../strongs/h/h256.md) [dabar](../../strongs/h/h1696.md) unto [Nāḇôṯ](../../strongs/h/h5022.md), ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) me thy [kerem](../../strongs/h/h3754.md), that I may have it for a [gan](../../strongs/h/h1588.md) of [yārāq](../../strongs/h/h3419.md), because it is [qarowb](../../strongs/h/h7138.md) unto my [bayith](../../strongs/h/h1004.md): and I will [nathan](../../strongs/h/h5414.md) thee for it a [towb](../../strongs/h/h2896.md) [kerem](../../strongs/h/h3754.md) than it; or, if it seem [towb](../../strongs/h/h2896.md) to ['ayin](../../strongs/h/h5869.md), I will [nathan](../../strongs/h/h5414.md) thee the [mᵊḥîr](../../strongs/h/h4242.md) of it in [keceph](../../strongs/h/h3701.md).

<a name="1kings_21_3"></a>1Kings 21:3

And [Nāḇôṯ](../../strongs/h/h5022.md) ['āmar](../../strongs/h/h559.md) to ['Aḥ'Āḇ](../../strongs/h/h256.md), [Yĕhovah](../../strongs/h/h3068.md) [ḥālîlâ](../../strongs/h/h2486.md) it me, that I should [nathan](../../strongs/h/h5414.md) the [nachalah](../../strongs/h/h5159.md) of my ['ab](../../strongs/h/h1.md) unto thee.

<a name="1kings_21_4"></a>1Kings 21:4

And ['Aḥ'Āḇ](../../strongs/h/h256.md) [bow'](../../strongs/h/h935.md) into his [bayith](../../strongs/h/h1004.md) [sar](../../strongs/h/h5620.md) and [zāʿēp̄](../../strongs/h/h2198.md) because of the [dabar](../../strongs/h/h1697.md) which [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md) had [dabar](../../strongs/h/h1696.md) to him: for he had ['āmar](../../strongs/h/h559.md), I will not [nathan](../../strongs/h/h5414.md) thee the [nachalah](../../strongs/h/h5159.md) of my ['ab](../../strongs/h/h1.md). And he [shakab](../../strongs/h/h7901.md) him upon his [mittah](../../strongs/h/h4296.md), and [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md), and would ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md).

<a name="1kings_21_5"></a>1Kings 21:5

But ['Îzeḇel](../../strongs/h/h348.md) his ['ishshah](../../strongs/h/h802.md) [bow'](../../strongs/h/h935.md) to him, and [dabar](../../strongs/h/h1696.md) unto him, Why is thy [ruwach](../../strongs/h/h7307.md) so [sar](../../strongs/h/h5620.md), that thou ['akal](../../strongs/h/h398.md) no [lechem](../../strongs/h/h3899.md)?

<a name="1kings_21_6"></a>1Kings 21:6

And he [dabar](../../strongs/h/h1696.md) unto her, Because I [dabar](../../strongs/h/h1696.md) unto [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md), and ['āmar](../../strongs/h/h559.md) unto him, [nathan](../../strongs/h/h5414.md) me thy [kerem](../../strongs/h/h3754.md) for [keceph](../../strongs/h/h3701.md); or else, if it [chaphets](../../strongs/h/h2655.md) thee, I will [nathan](../../strongs/h/h5414.md) thee [kerem](../../strongs/h/h3754.md) for it: and he ['āmar](../../strongs/h/h559.md), I will not [nathan](../../strongs/h/h5414.md) thee my [kerem](../../strongs/h/h3754.md).

<a name="1kings_21_7"></a>1Kings 21:7

And ['Îzeḇel](../../strongs/h/h348.md) his ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto him, Dost thou now ['asah](../../strongs/h/h6213.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) of [Yisra'el](../../strongs/h/h3478.md)? [quwm](../../strongs/h/h6965.md), and ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), and let thine [leb](../../strongs/h/h3820.md) be [yatab](../../strongs/h/h3190.md): I will [nathan](../../strongs/h/h5414.md) thee the [kerem](../../strongs/h/h3754.md) of [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md).

<a name="1kings_21_8"></a>1Kings 21:8

So she [kāṯaḇ](../../strongs/h/h3789.md) [sēp̄er](../../strongs/h/h5612.md) in ['Aḥ'Āḇ](../../strongs/h/h256.md) [shem](../../strongs/h/h8034.md), and [ḥāṯam](../../strongs/h/h2856.md) them with his [ḥôṯām](../../strongs/h/h2368.md), and [shalach](../../strongs/h/h7971.md) the [sēp̄er](../../strongs/h/h5612.md) unto the [zāqēn](../../strongs/h/h2205.md) and to the [ḥōr](../../strongs/h/h2715.md) that were in his [ʿîr](../../strongs/h/h5892.md), [yashab](../../strongs/h/h3427.md) with [Nāḇôṯ](../../strongs/h/h5022.md).

<a name="1kings_21_9"></a>1Kings 21:9

And she [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md), ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md), and [yashab](../../strongs/h/h3427.md) [Nāḇôṯ](../../strongs/h/h5022.md) on [ro'sh](../../strongs/h/h7218.md) among the ['am](../../strongs/h/h5971.md):

<a name="1kings_21_10"></a>1Kings 21:10

And [yashab](../../strongs/h/h3427.md) two ['enowsh](../../strongs/h/h582.md), [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), before him, to [ʿûḏ](../../strongs/h/h5749.md) against him, ['āmar](../../strongs/h/h559.md), Thou didst [barak](../../strongs/h/h1288.md) ['Elohiym](../../strongs/h/h430.md) and the [melek](../../strongs/h/h4428.md). And then [yāṣā'](../../strongs/h/h3318.md) him, and [sāqal](../../strongs/h/h5619.md) him, that he may [muwth](../../strongs/h/h4191.md).

<a name="1kings_21_11"></a>1Kings 21:11

And the ['enowsh](../../strongs/h/h582.md) of his [ʿîr](../../strongs/h/h5892.md), even the [zāqēn](../../strongs/h/h2205.md) and the [ḥōr](../../strongs/h/h2715.md) who were the [yashab](../../strongs/h/h3427.md) in his [ʿîr](../../strongs/h/h5892.md), ['asah](../../strongs/h/h6213.md) as ['Îzeḇel](../../strongs/h/h348.md) had [shalach](../../strongs/h/h7971.md) unto them, and as it was [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) which she had [shalach](../../strongs/h/h7971.md) unto them.

<a name="1kings_21_12"></a>1Kings 21:12

They [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md), and [yashab](../../strongs/h/h3427.md) [Nāḇôṯ](../../strongs/h/h5022.md) on [ro'sh](../../strongs/h/h7218.md) among the ['am](../../strongs/h/h5971.md).

<a name="1kings_21_13"></a>1Kings 21:13

And there [bow'](../../strongs/h/h935.md) in two ['enowsh](../../strongs/h/h582.md), [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), and [yashab](../../strongs/h/h3427.md) before him: and the ['enowsh](../../strongs/h/h582.md) of [beliya'al](../../strongs/h/h1100.md) [ʿûḏ](../../strongs/h/h5749.md) against him, even against [Nāḇôṯ](../../strongs/h/h5022.md), in the presence of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [Nāḇôṯ](../../strongs/h/h5022.md) did [barak](../../strongs/h/h1288.md) ['Elohiym](../../strongs/h/h430.md) and the [melek](../../strongs/h/h4428.md). Then they [yāṣā'](../../strongs/h/h3318.md) him [ḥûṣ](../../strongs/h/h2351.md) of the [ʿîr](../../strongs/h/h5892.md), and [sāqal](../../strongs/h/h5619.md) him with ['eben](../../strongs/h/h68.md), that he [muwth](../../strongs/h/h4191.md).

<a name="1kings_21_14"></a>1Kings 21:14

Then they [shalach](../../strongs/h/h7971.md) to ['Îzeḇel](../../strongs/h/h348.md), ['āmar](../../strongs/h/h559.md), [Nāḇôṯ](../../strongs/h/h5022.md) is [sāqal](../../strongs/h/h5619.md), and is [muwth](../../strongs/h/h4191.md).

<a name="1kings_21_15"></a>1Kings 21:15

And it came to pass, when ['Îzeḇel](../../strongs/h/h348.md) [shama'](../../strongs/h/h8085.md) that [Nāḇôṯ](../../strongs/h/h5022.md) was [sāqal](../../strongs/h/h5619.md), and was [muwth](../../strongs/h/h4191.md), that ['Îzeḇel](../../strongs/h/h348.md) ['āmar](../../strongs/h/h559.md) to ['Aḥ'Āḇ](../../strongs/h/h256.md), [quwm](../../strongs/h/h6965.md), [yarash](../../strongs/h/h3423.md) of the [kerem](../../strongs/h/h3754.md) of [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md), which he [mā'ēn](../../strongs/h/h3985.md) to [nathan](../../strongs/h/h5414.md) thee for [keceph](../../strongs/h/h3701.md): for [Nāḇôṯ](../../strongs/h/h5022.md) is not [chay](../../strongs/h/h2416.md), but [muwth](../../strongs/h/h4191.md).

<a name="1kings_21_16"></a>1Kings 21:16

And it came to pass, when ['Aḥ'Āḇ](../../strongs/h/h256.md) [shama'](../../strongs/h/h8085.md) that [Nāḇôṯ](../../strongs/h/h5022.md) was [muwth](../../strongs/h/h4191.md), that ['Aḥ'Āḇ](../../strongs/h/h256.md) [quwm](../../strongs/h/h6965.md) to [yarad](../../strongs/h/h3381.md) to the [kerem](../../strongs/h/h3754.md) of [Nāḇôṯ](../../strongs/h/h5022.md) the [Yizrᵊʿē'lî](../../strongs/h/h3158.md), to [yarash](../../strongs/h/h3423.md) of it.

<a name="1kings_21_17"></a>1Kings 21:17

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_21_18"></a>1Kings 21:18

[quwm](../../strongs/h/h6965.md), [yarad](../../strongs/h/h3381.md) to [qārā'](../../strongs/h/h7125.md) ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), which is in [Šōmrôn](../../strongs/h/h8111.md): behold, he is in the [kerem](../../strongs/h/h3754.md) of [Nāḇôṯ](../../strongs/h/h5022.md), whither he is [yarad](../../strongs/h/h3381.md) to [yarash](../../strongs/h/h3423.md) it.

<a name="1kings_21_19"></a>1Kings 21:19

And thou shalt [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Hast thou [ratsach](../../strongs/h/h7523.md), and also [yarash](../../strongs/h/h3423.md)? And thou shalt [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), In the [maqowm](../../strongs/h/h4725.md) where [keleḇ](../../strongs/h/h3611.md) [lāqaq](../../strongs/h/h3952.md) the [dam](../../strongs/h/h1818.md) of [Nāḇôṯ](../../strongs/h/h5022.md) shall [keleḇ](../../strongs/h/h3611.md) [lāqaq](../../strongs/h/h3952.md) thy [dam](../../strongs/h/h1818.md), even thine.

<a name="1kings_21_20"></a>1Kings 21:20

And ['Aḥ'Āḇ](../../strongs/h/h256.md) ['āmar](../../strongs/h/h559.md) to ['Ēlîyâ](../../strongs/h/h452.md), Hast thou [māṣā'](../../strongs/h/h4672.md) me, O mine ['oyeb](../../strongs/h/h341.md)? And he ['āmar](../../strongs/h/h559.md), I have [māṣā'](../../strongs/h/h4672.md) thee: because thou hast [māḵar](../../strongs/h/h4376.md) thyself to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="1kings_21_21"></a>1Kings 21:21

Behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon thee, and will take [bāʿar](../../strongs/h/h1197.md) thy ['aḥar](../../strongs/h/h310.md), and will [karath](../../strongs/h/h3772.md) from ['Aḥ'Āḇ](../../strongs/h/h256.md) him that [šāṯan](../../strongs/h/h8366.md) against the [qîr](../../strongs/h/h7023.md), and him that is [ʿāṣar](../../strongs/h/h6113.md) and ['azab](../../strongs/h/h5800.md) in [Yisra'el](../../strongs/h/h3478.md),

<a name="1kings_21_22"></a>1Kings 21:22

And will [nathan](../../strongs/h/h5414.md) thine [bayith](../../strongs/h/h1004.md) like the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), and like the [bayith](../../strongs/h/h1004.md) of [BaʿŠā'](../../strongs/h/h1201.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîyâ](../../strongs/h/h281.md), for the [ka'ac](../../strongs/h/h3708.md) wherewith thou hast [kāʿas](../../strongs/h/h3707.md), and made [Yisra'el](../../strongs/h/h3478.md) to [chata'](../../strongs/h/h2398.md).

<a name="1kings_21_23"></a>1Kings 21:23

And of ['Îzeḇel](../../strongs/h/h348.md) also [dabar](../../strongs/h/h1696.md) [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), The [keleḇ](../../strongs/h/h3611.md) shall ['akal](../../strongs/h/h398.md) ['Îzeḇel](../../strongs/h/h348.md) by the [cheyl](../../strongs/h/h2426.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="1kings_21_24"></a>1Kings 21:24

Him that [muwth](../../strongs/h/h4191.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) in the [ʿîr](../../strongs/h/h5892.md) the [keleḇ](../../strongs/h/h3611.md) shall ['akal](../../strongs/h/h398.md); and him that [muwth](../../strongs/h/h4191.md) in the [sadeh](../../strongs/h/h7704.md) shall the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) ['akal](../../strongs/h/h398.md).

<a name="1kings_21_25"></a>1Kings 21:25

But there was none like unto ['Aḥ'Āḇ](../../strongs/h/h256.md), which did [māḵar](../../strongs/h/h4376.md) himself to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), whom ['Îzeḇel](../../strongs/h/h348.md) his ['ishshah](../../strongs/h/h802.md) [sûṯ](../../strongs/h/h5496.md).

<a name="1kings_21_26"></a>1Kings 21:26

And he did [me'od](../../strongs/h/h3966.md) [ta'ab](../../strongs/h/h8581.md) in [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [gillûl](../../strongs/h/h1544.md), according to all things as ['asah](../../strongs/h/h6213.md) the ['Ĕmōrî](../../strongs/h/h567.md), whom [Yĕhovah](../../strongs/h/h3068.md) [yarash](../../strongs/h/h3423.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="1kings_21_27"></a>1Kings 21:27

And it came to pass, when ['Aḥ'Āḇ](../../strongs/h/h256.md) [shama'](../../strongs/h/h8085.md) those [dabar](../../strongs/h/h1697.md), that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and [śûm](../../strongs/h/h7760.md) [śaq](../../strongs/h/h8242.md) upon his [basar](../../strongs/h/h1320.md), and [ṣûm](../../strongs/h/h6684.md), and [shakab](../../strongs/h/h7901.md) in [śaq](../../strongs/h/h8242.md), and [halak](../../strongs/h/h1980.md) ['aṭ](../../strongs/h/h328.md).

<a name="1kings_21_28"></a>1Kings 21:28

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to ['Ēlîyâ](../../strongs/h/h452.md) the [Tišbî](../../strongs/h/h8664.md), ['āmar](../../strongs/h/h559.md),

<a name="1kings_21_29"></a>1Kings 21:29

[ra'ah](../../strongs/h/h7200.md) thou how ['Aḥ'Āḇ](../../strongs/h/h256.md) [kānaʿ](../../strongs/h/h3665.md) himself [paniym](../../strongs/h/h6440.md) me? because he [kānaʿ](../../strongs/h/h3665.md) himself [paniym](../../strongs/h/h6440.md) me, I will not [bow'](../../strongs/h/h935.md) the [ra'](../../strongs/h/h7451.md) in his [yowm](../../strongs/h/h3117.md): but in his [ben](../../strongs/h/h1121.md) [yowm](../../strongs/h/h3117.md) will I [bow'](../../strongs/h/h935.md) the [ra'](../../strongs/h/h7451.md) upon his [bayith](../../strongs/h/h1004.md).

---

[Transliteral Bible](../bible.md)

[1Kings](1kings.md)

[1Kings 20](1kings_20.md) - [1Kings 22](1kings_22.md)