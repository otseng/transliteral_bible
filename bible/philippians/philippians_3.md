# [Philippians 3](https://www.blueletterbible.org/kjv/phl/3/1/s_1106001)

<a name="philippians_3_1"></a>Philippians 3:1

[loipon](../../strongs/g/g3063.md), my [adelphos](../../strongs/g/g80.md), [chairō](../../strongs/g/g5463.md) in the [kyrios](../../strongs/g/g2962.md). To [graphō](../../strongs/g/g1125.md) the same things to you, to me indeed not [oknēros](../../strongs/g/g3636.md), but for you [asphalēs](../../strongs/g/g804.md).

<a name="philippians_3_2"></a>Philippians 3:2

[blepō](../../strongs/g/g991.md) of [kyōn](../../strongs/g/g2965.md), [blepō](../../strongs/g/g991.md) of [kakos](../../strongs/g/g2556.md) [ergatēs](../../strongs/g/g2040.md), [blepō](../../strongs/g/g991.md) of the [katatomē](../../strongs/g/g2699.md).

<a name="philippians_3_3"></a>Philippians 3:3

For we are the [peritomē](../../strongs/g/g4061.md), which [latreuō](../../strongs/g/g3000.md) [theos](../../strongs/g/g2316.md) in the [pneuma](../../strongs/g/g4151.md), and [kauchaomai](../../strongs/g/g2744.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), and have no [peithō](../../strongs/g/g3982.md) in the [sarx](../../strongs/g/g4561.md).

<a name="philippians_3_4"></a>Philippians 3:4

[kaiper](../../strongs/g/g2539.md) I might also have [pepoithēsis](../../strongs/g/g4006.md) in the [sarx](../../strongs/g/g4561.md). If any other man [dokeō](../../strongs/g/g1380.md) that he hath whereof he might [peithō](../../strongs/g/g3982.md) in the [sarx](../../strongs/g/g4561.md), I more:

<a name="philippians_3_5"></a>Philippians 3:5

[peritomē](../../strongs/g/g4061.md) the eighth, of the [genos](../../strongs/g/g1085.md) of [Israēl](../../strongs/g/g2474.md), the [phylē](../../strongs/g/g5443.md) of [Beniam(e)in](../../strongs/g/g958.md), an [Hebraios](../../strongs/g/g1445.md) of the [Hebraios](../../strongs/g/g1445.md); as touching the [nomos](../../strongs/g/g3551.md), a [Pharisaios](../../strongs/g/g5330.md);

<a name="philippians_3_6"></a>Philippians 3:6

Concerning [zēlos](../../strongs/g/g2205.md), [diōkō](../../strongs/g/g1377.md) the [ekklēsia](../../strongs/g/g1577.md); touching the [dikaiosynē](../../strongs/g/g1343.md) which is in the [nomos](../../strongs/g/g3551.md), [ginomai](../../strongs/g/g1096.md) [amemptos](../../strongs/g/g273.md).

<a name="philippians_3_7"></a>Philippians 3:7

But what things were [kerdos](../../strongs/g/g2771.md) to me, those I [hēgeomai](../../strongs/g/g2233.md) [zēmia](../../strongs/g/g2209.md) for [Christos](../../strongs/g/g5547.md).

<a name="philippians_3_8"></a>Philippians 3:8

[menoun](../../strongs/g/g3304.md), and I [hēgeomai](../../strongs/g/g2233.md) all things [zēmia](../../strongs/g/g2209.md) for the [hyperechō](../../strongs/g/g5242.md) of the [gnōsis](../../strongs/g/g1108.md) of [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) my [kyrios](../../strongs/g/g2962.md): for whom I have [zēmioō](../../strongs/g/g2210.md) of all things, and [hēgeomai](../../strongs/g/g2233.md) them [skybalon](../../strongs/g/g4657.md), that I may [kerdainō](../../strongs/g/g2770.md) [Christos](../../strongs/g/g5547.md),

<a name="philippians_3_9"></a>Philippians 3:9

And be [heuriskō](../../strongs/g/g2147.md) in him, not having mine own [dikaiosynē](../../strongs/g/g1343.md), which is of the [nomos](../../strongs/g/g3551.md), but that which is through the [pistis](../../strongs/g/g4102.md) of [Christos](../../strongs/g/g5547.md), the [dikaiosynē](../../strongs/g/g1343.md) which is of [theos](../../strongs/g/g2316.md) by [pistis](../../strongs/g/g4102.md):

<a name="philippians_3_10"></a>Philippians 3:10

That I may [ginōskō](../../strongs/g/g1097.md) him, and the [dynamis](../../strongs/g/g1411.md) of his [anastasis](../../strongs/g/g386.md), and the [koinōnia](../../strongs/g/g2842.md) of his [pathēma](../../strongs/g/g3804.md), being [symmorphizō](../../strongs/g/g4833.md) unto his [thanatos](../../strongs/g/g2288.md);

<a name="philippians_3_11"></a>Philippians 3:11

If by any means I might [katantaō](../../strongs/g/g2658.md) unto the [exanastasis](../../strongs/g/g1815.md) of the [nekros](../../strongs/g/g3498.md).

<a name="philippians_3_12"></a>Philippians 3:12

Not as though I had already [lambanō](../../strongs/g/g2983.md), either were already [teleioō](../../strongs/g/g5048.md): but I [diōkō](../../strongs/g/g1377.md), if that I may [katalambanō](../../strongs/g/g2638.md) that for which also I am [katalambanō](../../strongs/g/g2638.md) of [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="philippians_3_13"></a>Philippians 3:13

[adelphos](../../strongs/g/g80.md), I [logizomai](../../strongs/g/g3049.md) not myself to have [katalambanō](../../strongs/g/g2638.md): but one thing, [epilanthanomai](../../strongs/g/g1950.md) those things which are [opisō](../../strongs/g/g3694.md), and [epekteinomai](../../strongs/g/g1901.md) forth unto those things which are before,

<a name="philippians_3_14"></a>Philippians 3:14

I [diōkō](../../strongs/g/g1377.md) toward the [skopos](../../strongs/g/g4649.md) for the [brabeion](../../strongs/g/g1017.md) of the [anō](../../strongs/g/g507.md) [klēsis](../../strongs/g/g2821.md) of [theos](../../strongs/g/g2316.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="philippians_3_15"></a>Philippians 3:15

Let us therefore, as many as be [teleios](../../strongs/g/g5046.md), be thus [phroneō](../../strongs/g/g5426.md): and if in any thing ye be [heterōs](../../strongs/g/g2088.md) [phroneō](../../strongs/g/g5426.md), [theos](../../strongs/g/g2316.md) shall [apokalyptō](../../strongs/g/g601.md) even this unto you.

<a name="philippians_3_16"></a>Philippians 3:16

Nevertheless, whereto we have already [phthanō](../../strongs/g/g5348.md), let us [stoicheō](../../strongs/g/g4748.md) by the same [kanōn](../../strongs/g/g2583.md), let us [phroneō](../../strongs/g/g5426.md) the same thing. [^1]

<a name="philippians_3_17"></a>Philippians 3:17

[adelphos](../../strongs/g/g80.md), be [symmimētēs](../../strongs/g/g4831.md) of me, and [skopeō](../../strongs/g/g4648.md) them which [peripateō](../../strongs/g/g4043.md) so as ye have us for a [typos](../../strongs/g/g5179.md).

<a name="philippians_3_18"></a>Philippians 3:18

(For [polys](../../strongs/g/g4183.md) [peripateō](../../strongs/g/g4043.md), of whom I have [legō](../../strongs/g/g3004.md) you often, and now tell you even [klaiō](../../strongs/g/g2799.md), the [echthros](../../strongs/g/g2190.md) of the [stauros](../../strongs/g/g4716.md) of [Christos](../../strongs/g/g5547.md):

<a name="philippians_3_19"></a>Philippians 3:19

Whose [telos](../../strongs/g/g5056.md) [apōleia](../../strongs/g/g684.md), whose [theos](../../strongs/g/g2316.md) [koilia](../../strongs/g/g2836.md), and whose [doxa](../../strongs/g/g1391.md) in their [aischynē](../../strongs/g/g152.md), who [phroneō](../../strongs/g/g5426.md) [epigeios](../../strongs/g/g1919.md).)

<a name="philippians_3_20"></a>Philippians 3:20

For our [politeuma](../../strongs/g/g4175.md) is in [ouranos](../../strongs/g/g3772.md); from whence also we [apekdechomai](../../strongs/g/g553.md) the [sōtēr](../../strongs/g/g4990.md), the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md)t:

<a name="philippians_3_21"></a>Philippians 3:21

Who shall [metaschēmatizō](../../strongs/g/g3345.md) our [tapeinōsis](../../strongs/g/g5014.md) [sōma](../../strongs/g/g4983.md), that it may be [symmorphos](../../strongs/g/g4832.md) like unto his [doxa](../../strongs/g/g1391.md) [sōma](../../strongs/g/g4983.md), according to the [energeia](../../strongs/g/g1753.md) whereby he is able even to [hypotassō](../../strongs/g/g5293.md) all things unto himself.

---

[Transliteral Bible](../bible.md)

[Philippians](philippians.md)

[Philippians 2](philippians_2.md) - [Philippians 4](philippians_4.md)

---

[^1]: [Philippians 3:16 Commentary](../../commentary/philippians/philippians_3_commentary.md#philippians_3_16)
