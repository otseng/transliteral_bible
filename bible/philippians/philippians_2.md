# [Philippians 2](https://www.blueletterbible.org/kjv/phl/2/4/s_1105004)

<a name="philippians_2_1"></a>Philippians 2:1

If therefore any [paraklēsis](../../strongs/g/g3874.md) in [Christos](../../strongs/g/g5547.md), if any [paramythion](../../strongs/g/g3890.md) of [agapē](../../strongs/g/g26.md), if any [koinōnia](../../strongs/g/g2842.md) of the [pneuma](../../strongs/g/g4151.md), if any [splagchnon](../../strongs/g/g4698.md) and [oiktirmos](../../strongs/g/g3628.md),

<a name="philippians_2_2"></a>Philippians 2:2

[plēroō](../../strongs/g/g4137.md) ye my [chara](../../strongs/g/g5479.md), that ye be [autos](../../strongs/g/g846.md) [phroneō](../../strongs/g/g5426.md), having the same [agapē](../../strongs/g/g26.md), [sympsychos](../../strongs/g/g4861.md), of [heis](../../strongs/g/g1520.md) [phroneō](../../strongs/g/g5426.md).

<a name="philippians_2_3"></a>Philippians 2:3

[mēdeis](../../strongs/g/g3367.md) through [eritheia](../../strongs/g/g2052.md) or [kenodoxia](../../strongs/g/g2754.md); but in [tapeinophrosynē](../../strongs/g/g5012.md) let [allēlōn](../../strongs/g/g240.md) [hēgeomai](../../strongs/g/g2233.md) [hyperechō](../../strongs/g/g5242.md) themselves.

<a name="philippians_2_4"></a>Philippians 2:4

[skopeō](../../strongs/g/g4648.md) not [hekastos](../../strongs/g/g1538.md) on [heautou](../../strongs/g/g1438.md), but [hekastos](../../strongs/g/g1538.md) also on the things [heteros](../../strongs/g/g2087.md).

<a name="philippians_2_5"></a>Philippians 2:5

Let this [phroneō](../../strongs/g/g5426.md) be in you, which was also in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="philippians_2_6"></a>Philippians 2:6

Who, [hyparchō](../../strongs/g/g5225.md) in the [morphē](../../strongs/g/g3444.md) of [theos](../../strongs/g/g2316.md), [hēgeomai](../../strongs/g/g2233.md) it not [harpagmos](../../strongs/g/g725.md) to be [isos](../../strongs/g/g2470.md) with [theos](../../strongs/g/g2316.md):

<a name="philippians_2_7"></a>Philippians 2:7

But [kenoō](../../strongs/g/g2758.md) himself, and [lambanō](../../strongs/g/g2983.md) the [morphē](../../strongs/g/g3444.md) of a [doulos](../../strongs/g/g1401.md), and was [ginomai](../../strongs/g/g1096.md) in the [homoiōma](../../strongs/g/g3667.md) of [anthrōpos](../../strongs/g/g444.md):

<a name="philippians_2_8"></a>Philippians 2:8

And [heuriskō](../../strongs/g/g2147.md) in [schēma](../../strongs/g/g4976.md) as an [anthrōpos](../../strongs/g/g444.md), he [tapeinoō](../../strongs/g/g5013.md) himself, and became [hypēkoos](../../strongs/g/g5255.md) unto [thanatos](../../strongs/g/g2288.md), even the [thanatos](../../strongs/g/g2288.md) of the [stauros](../../strongs/g/g4716.md).

<a name="philippians_2_9"></a>Philippians 2:9

Wherefore [theos](../../strongs/g/g2316.md) also hath [hyperypsoō](../../strongs/g/g5251.md) him, and [charizomai](../../strongs/g/g5483.md) him an [onoma](../../strongs/g/g3686.md) which is [hyper](../../strongs/g/g5228.md) [pas](../../strongs/g/g3956.md) [onoma](../../strongs/g/g3686.md):

<a name="philippians_2_10"></a>Philippians 2:10

That at the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [pas](../../strongs/g/g3956.md) [gony](../../strongs/g/g1119.md) should [kamptō](../../strongs/g/g2578.md), of [epouranios](../../strongs/g/g2032.md), and [epigeios](../../strongs/g/g1919.md), and [katachthonios](../../strongs/g/g2709.md);

<a name="philippians_2_11"></a>Philippians 2:11

And [pas](../../strongs/g/g3956.md) [glōssa](../../strongs/g/g1100.md) should [exomologeō](../../strongs/g/g1843.md) that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is [kyrios](../../strongs/g/g2962.md), to the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md).

<a name="philippians_2_12"></a>Philippians 2:12

Wherefore, my [agapētos](../../strongs/g/g27.md), as ye have [pantote](../../strongs/g/g3842.md) [hypakouō](../../strongs/g/g5219.md), not as in my [parousia](../../strongs/g/g3952.md) only, but now [polys](../../strongs/g/g4183.md) more in my [apousia](../../strongs/g/g666.md), [katergazomai](../../strongs/g/g2716.md) your own [sōtēria](../../strongs/g/g4991.md) with [phobos](../../strongs/g/g5401.md) and [tromos](../../strongs/g/g5156.md).

<a name="philippians_2_13"></a>Philippians 2:13

For it is [theos](../../strongs/g/g2316.md) which [energeō](../../strongs/g/g1754.md) in you both to [thelō](../../strongs/g/g2309.md) and to [energeō](../../strongs/g/g1754.md) of his [eudokia](../../strongs/g/g2107.md).

<a name="philippians_2_14"></a>Philippians 2:14

[poieō](../../strongs/g/g4160.md) all things without [goggysmos](../../strongs/g/g1112.md) and [dialogismos](../../strongs/g/g1261.md):

<a name="philippians_2_15"></a>Philippians 2:15

That ye may be [amemptos](../../strongs/g/g273.md) and [akeraios](../../strongs/g/g185.md), the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md), without [amōmētos](../../strongs/g/g298.md), in the midst of a [skolios](../../strongs/g/g4646.md) and [diastrephō](../../strongs/g/g1294.md) [genea](../../strongs/g/g1074.md), among whom ye [phainō](../../strongs/g/g5316.md) as [phōstēr](../../strongs/g/g5458.md) in the [kosmos](../../strongs/g/g2889.md);

<a name="philippians_2_16"></a>Philippians 2:16

[epechō](../../strongs/g/g1907.md) the [logos](../../strongs/g/g3056.md) of [zōē](../../strongs/g/g2222.md); that I may [kauchēma](../../strongs/g/g2745.md) in the [hēmera](../../strongs/g/g2250.md) of [Christos](../../strongs/g/g5547.md), that I have not [trechō](../../strongs/g/g5143.md) in [kenos](../../strongs/g/g2756.md), neither [kopiaō](../../strongs/g/g2872.md) in [kenos](../../strongs/g/g2756.md).

<a name="philippians_2_17"></a>Philippians 2:17

Yea, and if I be [spendō](../../strongs/g/g4689.md) upon the [thysia](../../strongs/g/g2378.md) and [leitourgia](../../strongs/g/g3009.md) of your [pistis](../../strongs/g/g4102.md), I [chairō](../../strongs/g/g5463.md), and [sygchairō](../../strongs/g/g4796.md) with you all.

<a name="philippians_2_18"></a>Philippians 2:18

For the same cause also [chairō](../../strongs/g/g5463.md) ye, and [sygchairō](../../strongs/g/g4796.md) with me.

<a name="philippians_2_19"></a>Philippians 2:19

But I [elpizō](../../strongs/g/g1679.md) in the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) to [pempō](../../strongs/g/g3992.md) [Timotheos](../../strongs/g/g5095.md) [tacheōs](../../strongs/g/g5030.md) unto you, that I also may be of [eupsycheō](../../strongs/g/g2174.md), when I [ginōskō](../../strongs/g/g1097.md) your state.

<a name="philippians_2_20"></a>Philippians 2:20

For I have [oudeis](../../strongs/g/g3762.md) [isopsychos](../../strongs/g/g2473.md), who will [gnēsiōs](../../strongs/g/g1104.md) [merimnaō](../../strongs/g/g3309.md) for your state.

<a name="philippians_2_21"></a>Philippians 2:21

For all [zēteō](../../strongs/g/g2212.md) their own, not the things which are [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="philippians_2_22"></a>Philippians 2:22

But ye [ginōskō](../../strongs/g/g1097.md) the [dokimē](../../strongs/g/g1382.md) of him, that, as a [teknon](../../strongs/g/g5043.md) with the [patēr](../../strongs/g/g3962.md), he hath [douleuō](../../strongs/g/g1398.md) with me in the [euaggelion](../../strongs/g/g2098.md).

<a name="philippians_2_23"></a>Philippians 2:23

Him therefore I [elpizō](../../strongs/g/g1679.md) to [pempō](../../strongs/g/g3992.md) [exautēs](../../strongs/g/g1824.md), so soon as I shall [apeidō](../../strongs/g/g542.md) how it will go with me.

<a name="philippians_2_24"></a>Philippians 2:24

But I [peithō](../../strongs/g/g3982.md) in the [kyrios](../../strongs/g/g2962.md) that I also myself shall [erchomai](../../strongs/g/g2064.md) [tacheōs](../../strongs/g/g5030.md).

<a name="philippians_2_25"></a>Philippians 2:25

Yet I [hēgeomai](../../strongs/g/g2233.md) it [anagkaios](../../strongs/g/g316.md) to [pempō](../../strongs/g/g3992.md) to you [epaphroditos](../../strongs/g/g1891.md), my [adelphos](../../strongs/g/g80.md), and [synergos](../../strongs/g/g4904.md), and [systratiōtēs](../../strongs/g/g4961.md), but your [apostolos](../../strongs/g/g652.md), and he that [leitourgos](../../strongs/g/g3011.md) to my [chreia](../../strongs/g/g5532.md).

<a name="philippians_2_26"></a>Philippians 2:26

For he [epipotheo](../../strongs/g/g1971.md) after you all, and was [adēmone](../../strongs/g/g85.md), because that ye had [akouō](../../strongs/g/g191.md) that he had been [astheneō](../../strongs/g/g770.md).

<a name="philippians_2_27"></a>Philippians 2:27

For indeed he was [astheneō](../../strongs/g/g770.md) [paraplēsios](../../strongs/g/g3897.md) unto [thanatos](../../strongs/g/g2288.md): but [theos](../../strongs/g/g2316.md) had [eleeō](../../strongs/g/g1653.md) on him; and not on him only, but on me also, lest I should have [lypē](../../strongs/g/g3077.md) upon [lypē](../../strongs/g/g3077.md).

<a name="philippians_2_28"></a>Philippians 2:28

I [pempō](../../strongs/g/g3992.md) him therefore the [spoudaioterōs](../../strongs/g/g4708.md), that, when ye [eidō](../../strongs/g/g1492.md) him again, ye may [chairō](../../strongs/g/g5463.md), and that I may be [alypos](../../strongs/g/g253.md).

<a name="philippians_2_29"></a>Philippians 2:29

[prosdechomai](../../strongs/g/g4327.md) him therefore in the [kyrios](../../strongs/g/g2962.md) with all [chara](../../strongs/g/g5479.md); and [echō](../../strongs/g/g2192.md) such in [entimos](../../strongs/g/g1784.md):

<a name="philippians_2_30"></a>Philippians 2:30

Because for the [ergon](../../strongs/g/g2041.md) of [Christos](../../strongs/g/g5547.md) he was [eggizō](../../strongs/g/g1448.md) unto [thanatos](../../strongs/g/g2288.md), not [paraboleuomai](../../strongs/g/g3851.md) his [psychē](../../strongs/g/g5590.md), to [anaplēroō](../../strongs/g/g378.md) your [hysterēma](../../strongs/g/g5303.md) of [leitourgia](../../strongs/g/g3009.md) toward me.

---

[Transliteral Bible](../bible.md)

[Philippians](philippians.md)

[Philippians 1](philippians_1.md) - [Philippians 3](philippians_3.md)