# [Philippians 1](https://www.blueletterbible.org/kjv/phl/1/10/rl1/s_1104010)

<a name="philippians_1_1"></a>Philippians 1:1

[Paulos](../../strongs/g/g3972.md) and [Timotheos](../../strongs/g/g5095.md), the [doulos](../../strongs/g/g1401.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), to all the [hagios](../../strongs/g/g40.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) which are at [Philippoi](../../strongs/g/g5375.md), with the [episkopos](../../strongs/g/g1985.md) and [diakonos](../../strongs/g/g1249.md):

<a name="philippians_1_2"></a>Philippians 1:2

[charis](../../strongs/g/g5485.md) unto you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and from the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="philippians_1_3"></a>Philippians 1:3

I [eucharisteō](../../strongs/g/g2168.md) my [theos](../../strongs/g/g2316.md) upon every [mneia](../../strongs/g/g3417.md) of you,

<a name="philippians_1_4"></a>Philippians 1:4

[pantote](../../strongs/g/g3842.md) in every [deēsis](../../strongs/g/g1162.md) of mine for you all [poieō](../../strongs/g/g4160.md) [deēsis](../../strongs/g/g1162.md) with [chara](../../strongs/g/g5479.md),

<a name="philippians_1_5"></a>Philippians 1:5

For your [koinōnia](../../strongs/g/g2842.md) in the [euaggelion](../../strongs/g/g2098.md) from the first [hēmera](../../strongs/g/g2250.md) until now;

<a name="philippians_1_6"></a>Philippians 1:6

Being [peithō](../../strongs/g/g3982.md) of this very thing, that he which hath [enarchomai](../../strongs/g/g1728.md) an [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md) in you will [epiteleō](../../strongs/g/g2005.md) it until the [hēmera](../../strongs/g/g2250.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="philippians_1_7"></a>Philippians 1:7

Even as it is [dikaios](../../strongs/g/g1342.md) for me to [phroneō](../../strongs/g/g5426.md) this of you all, because I have you in my [kardia](../../strongs/g/g2588.md); inasmuch as both in my [desmos](../../strongs/g/g1199.md), and in the [apologia](../../strongs/g/g627.md) and [bebaiōsis](../../strongs/g/g951.md) of the [euaggelion](../../strongs/g/g2098.md), ye all are [sygkoinōnos](../../strongs/g/g4791.md) of my [charis](../../strongs/g/g5485.md).

<a name="philippians_1_8"></a>Philippians 1:8

For [theos](../../strongs/g/g2316.md) is my [martys](../../strongs/g/g3144.md), how I [epipotheo](../../strongs/g/g1971.md) you all in the [splagchnon](../../strongs/g/g4698.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="philippians_1_9"></a>Philippians 1:9

And this I [proseuchomai](../../strongs/g/g4336.md), that your [agapē](../../strongs/g/g26.md) may [perisseuō](../../strongs/g/g4052.md) yet more and more in [epignōsis](../../strongs/g/g1922.md) and in all [aisthēsis](../../strongs/g/g144.md);

<a name="philippians_1_10"></a>Philippians 1:10

That ye may [dokimazō](../../strongs/g/g1381.md) things that are [diapherō](../../strongs/g/g1308.md); that ye may be [eilikrinēs](../../strongs/g/g1506.md) and [aproskopos](../../strongs/g/g677.md) till the [hēmera](../../strongs/g/g2250.md) of [Christos](../../strongs/g/g5547.md).

<a name="philippians_1_11"></a>Philippians 1:11

Being [plēroō](../../strongs/g/g4137.md) with the [karpos](../../strongs/g/g2590.md) of [dikaiosynē](../../strongs/g/g1343.md), which are by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), unto the [doxa](../../strongs/g/g1391.md) and [epainos](../../strongs/g/g1868.md) of [theos](../../strongs/g/g2316.md).

<a name="philippians_1_12"></a>Philippians 1:12

But I [boulomai](../../strongs/g/g1014.md) ye should [ginōskō](../../strongs/g/g1097.md), [adelphos](../../strongs/g/g80.md), that the things unto me have [erchomai](../../strongs/g/g2064.md) rather unto the [prokopē](../../strongs/g/g4297.md) of the [euaggelion](../../strongs/g/g2098.md);

<a name="philippians_1_13"></a>Philippians 1:13

So that my [desmos](../../strongs/g/g1199.md) in [Christos](../../strongs/g/g5547.md) are [phaneros](../../strongs/g/g5318.md) in all the [praitōrion](../../strongs/g/g4232.md), and in all [loipos](../../strongs/g/g3062.md);

<a name="philippians_1_14"></a>Philippians 1:14

And many of the [adelphos](../../strongs/g/g80.md) in the [kyrios](../../strongs/g/g2962.md), [peithō](../../strongs/g/g3982.md) by my [desmos](../../strongs/g/g1199.md), are [perissoterōs](../../strongs/g/g4056.md) [tolmaō](../../strongs/g/g5111.md) to [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) [aphobōs](../../strongs/g/g870.md).

<a name="philippians_1_15"></a>Philippians 1:15

Some indeed [kēryssō](../../strongs/g/g2784.md) [Christos](../../strongs/g/g5547.md) even of [phthonos](../../strongs/g/g5355.md) and [eris](../../strongs/g/g2054.md); and some also of [eudokia](../../strongs/g/g2107.md):

<a name="philippians_1_16"></a>Philippians 1:16

The one [kataggellō](../../strongs/g/g2605.md) [Christos](../../strongs/g/g5547.md) of [eritheia](../../strongs/g/g2052.md), not [hagnōs](../../strongs/g/g55.md), [oiomai](../../strongs/g/g3633.md) to [epipherō](../../strongs/g/g2018.md) [thlipsis](../../strongs/g/g2347.md) to my [desmos](../../strongs/g/g1199.md):

<a name="philippians_1_17"></a>Philippians 1:17

But the other of [agapē](../../strongs/g/g26.md), [eidō](../../strongs/g/g1492.md) that I am [keimai](../../strongs/g/g2749.md) for the [apologia](../../strongs/g/g627.md) of the [euaggelion](../../strongs/g/g2098.md).

<a name="philippians_1_18"></a>Philippians 1:18

What then? notwithstanding, every way, whether in [prophasis](../../strongs/g/g4392.md), or in [alētheia](../../strongs/g/g225.md), [Christos](../../strongs/g/g5547.md) is [kataggellō](../../strongs/g/g2605.md); and I therein [chairō](../../strongs/g/g5463.md), yea, and will [chairō](../../strongs/g/g5463.md).

<a name="philippians_1_19"></a>Philippians 1:19

For I [eidō](../../strongs/g/g1492.md) that this shall [apobainō](../../strongs/g/g576.md) to my [sōtēria](../../strongs/g/g4991.md) through your [deēsis](../../strongs/g/g1162.md), and the [epichorēgia](../../strongs/g/g2024.md) of the [pneuma](../../strongs/g/g4151.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md),

<a name="philippians_1_20"></a>Philippians 1:20

According to my [apokaradokia](../../strongs/g/g603.md) and [elpis](../../strongs/g/g1680.md), that in nothing I shall be [aischynō](../../strongs/g/g153.md), but with all [parrēsia](../../strongs/g/g3954.md), as [pantote](../../strongs/g/g3842.md), so now also [Christos](../../strongs/g/g5547.md) shall be [megalynō](../../strongs/g/g3170.md) in my [sōma](../../strongs/g/g4983.md), whether by [zōē](../../strongs/g/g2222.md), or by [thanatos](../../strongs/g/g2288.md).

<a name="philippians_1_21"></a>Philippians 1:21

For to me to [zaō](../../strongs/g/g2198.md) is [Christos](../../strongs/g/g5547.md), and to [apothnēskō](../../strongs/g/g599.md) is [kerdos](../../strongs/g/g2771.md).

<a name="philippians_1_22"></a>Philippians 1:22

But if I [zaō](../../strongs/g/g2198.md) in the [sarx](../../strongs/g/g4561.md), this the [karpos](../../strongs/g/g2590.md) of my [ergon](../../strongs/g/g2041.md): yet what I shall [haireō](../../strongs/g/g138.md) I [gnōrizō](../../strongs/g/g1107.md) not.

<a name="philippians_1_23"></a>Philippians 1:23

For I am [synechō](../../strongs/g/g4912.md) betwixt two, having [epithymia](../../strongs/g/g1939.md) to [analyō](../../strongs/g/g360.md), and to be with [Christos](../../strongs/g/g5547.md); which is [polys](../../strongs/g/g4183.md) [mallon](../../strongs/g/g3123.md) [kreisson](../../strongs/g/g2908.md):

<a name="philippians_1_24"></a>Philippians 1:24

Nevertheless to [epimenō](../../strongs/g/g1961.md) in the [sarx](../../strongs/g/g4561.md) is more [anagkaios](../../strongs/g/g316.md) for you.

<a name="philippians_1_25"></a>Philippians 1:25

And having this [peithō](../../strongs/g/g3982.md), I [eidō](../../strongs/g/g1492.md) that I shall [menō](../../strongs/g/g3306.md) and [symparamenō](../../strongs/g/g4839.md) with you all for your [prokopē](../../strongs/g/g4297.md) and [chara](../../strongs/g/g5479.md) of [pistis](../../strongs/g/g4102.md);

<a name="philippians_1_26"></a>Philippians 1:26

That your [kauchēma](../../strongs/g/g2745.md) may be more [perisseuō](../../strongs/g/g4052.md) in [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) for me by my [parousia](../../strongs/g/g3952.md) to you again.

<a name="philippians_1_27"></a>Philippians 1:27

Only let your [politeuomai](../../strongs/g/g4176.md) be as it [axiōs](../../strongs/g/g516.md) the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md): that whether I [erchomai](../../strongs/g/g2064.md) and [eidō](../../strongs/g/g1492.md) you, or else be [apeimi](../../strongs/g/g548.md), I may [akouō](../../strongs/g/g191.md) of your [peri](../../strongs/g/g4012.md), that ye [stēkō](../../strongs/g/g4739.md) in one [pneuma](../../strongs/g/g4151.md), with one [psychē](../../strongs/g/g5590.md) [synathleō](../../strongs/g/g4866.md) for the [pistis](../../strongs/g/g4102.md) of the [euaggelion](../../strongs/g/g2098.md);

<a name="philippians_1_28"></a>Philippians 1:28

And in [mēdeis](../../strongs/g/g3367.md) [ptyrō](../../strongs/g/g4426.md) by your [antikeimai](../../strongs/g/g480.md): which is to them an [endeixis](../../strongs/g/g1732.md) of [apōleia](../../strongs/g/g684.md), but to you of [sōtēria](../../strongs/g/g4991.md), and that of [theos](../../strongs/g/g2316.md).

<a name="philippians_1_29"></a>Philippians 1:29

For unto you it is [charizomai](../../strongs/g/g5483.md) in the behalf of [Christos](../../strongs/g/g5547.md), not only to [pisteuō](../../strongs/g/g4100.md) on him, but also to [paschō](../../strongs/g/g3958.md) for his sake;

<a name="philippians_1_30"></a>Philippians 1:30

Having the same [agōn](../../strongs/g/g73.md) which ye [eidō](../../strongs/g/g1492.md) in me, and now [akouō](../../strongs/g/g191.md) to be in me.

---

[Transliteral Bible](../bible.md)

[Philippians](philippians.md)

[Philippians 2](philippians_2.md)