# [Philippians 4](https://www.blueletterbible.org/kjv/phl/4/1/s_1107001)

<a name="philippians_4_1"></a>Philippians 4:1

Therefore, my [adelphos](../../strongs/g/g80.md) [agapētos](../../strongs/g/g27.md) and [epipothētos](../../strongs/g/g1973.md), my [chara](../../strongs/g/g5479.md) and [stephanos](../../strongs/g/g4735.md), so [stēkō](../../strongs/g/g4739.md) in the [kyrios](../../strongs/g/g2962.md), [agapētos](../../strongs/g/g27.md).

<a name="philippians_4_2"></a>Philippians 4:2

I [parakaleō](../../strongs/g/g3870.md) [euodia](../../strongs/g/g2136.md), and [parakaleō](../../strongs/g/g3870.md) [syntychē](../../strongs/g/g4941.md), that they be of the same [phroneō](../../strongs/g/g5426.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="philippians_4_3"></a>Philippians 4:3

And I [erōtaō](../../strongs/g/g2065.md) thee also, [gnēsios](../../strongs/g/g1103.md) [syzygos](../../strongs/g/g4805.md), [syllambanō](../../strongs/g/g4815.md) [autos](../../strongs/g/g846.md) which [synathleō](../../strongs/g/g4866.md) me in the [euaggelion](../../strongs/g/g2098.md), with [klēmēs](../../strongs/g/g2815.md) also, and [loipos](../../strongs/g/g3062.md) my [synergos](../../strongs/g/g4904.md), whose [onoma](../../strongs/g/g3686.md) in the [biblos](../../strongs/g/g976.md) of [zōē](../../strongs/g/g2222.md).

<a name="philippians_4_4"></a>Philippians 4:4

[chairō](../../strongs/g/g5463.md) in the [kyrios](../../strongs/g/g2962.md) [pantote](../../strongs/g/g3842.md): again I [eipon](../../strongs/g/g2046.md), [chairō](../../strongs/g/g5463.md).

<a name="philippians_4_5"></a>Philippians 4:5

Let your [epieikēs](../../strongs/g/g1933.md) be [ginōskō](../../strongs/g/g1097.md) unto all [anthrōpos](../../strongs/g/g444.md). The [kyrios](../../strongs/g/g2962.md) [eggys](../../strongs/g/g1451.md).

<a name="philippians_4_6"></a>Philippians 4:6

[merimnaō](../../strongs/g/g3309.md) for [mēdeis](../../strongs/g/g3367.md); but in [pas](../../strongs/g/g3956.md) by [proseuchē](../../strongs/g/g4335.md) and [deēsis](../../strongs/g/g1162.md) with [eucharistia](../../strongs/g/g2169.md) let your [aitēma](../../strongs/g/g155.md) be [gnōrizō](../../strongs/g/g1107.md) unto [theos](../../strongs/g/g2316.md).

<a name="philippians_4_7"></a>Philippians 4:7

And the [eirēnē](../../strongs/g/g1515.md) of [theos](../../strongs/g/g2316.md), which [hyperechō](../../strongs/g/g5242.md) all [nous](../../strongs/g/g3563.md), shall [phroureō](../../strongs/g/g5432.md) your [kardia](../../strongs/g/g2588.md) and [noēma](../../strongs/g/g3540.md) through [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="philippians_4_8"></a>Philippians 4:8

[loipon](../../strongs/g/g3063.md), [adelphos](../../strongs/g/g80.md), whatsoever things are [alēthēs](../../strongs/g/g227.md), whatsoever things [semnos](../../strongs/g/g4586.md), whatsoever things [dikaios](../../strongs/g/g1342.md), whatsoever things [hagnos](../../strongs/g/g53.md), whatsoever things [prosphilēs](../../strongs/g/g4375.md), whatsoever things of [euphēmos](../../strongs/g/g2163.md); if any [aretē](../../strongs/g/g703.md), and if any [epainos](../../strongs/g/g1868.md), [logizomai](../../strongs/g/g3049.md) on these things.

<a name="philippians_4_9"></a>Philippians 4:9

Those things, which ye have both [manthanō](../../strongs/g/g3129.md), and [paralambanō](../../strongs/g/g3880.md), and [akouō](../../strongs/g/g191.md), and [eidō](../../strongs/g/g1492.md) in me, [prassō](../../strongs/g/g4238.md): and the [theos](../../strongs/g/g2316.md) of [eirēnē](../../strongs/g/g1515.md) shall be with you.

<a name="philippians_4_10"></a>Philippians 4:10

But I [chairō](../../strongs/g/g5463.md) in the [kyrios](../../strongs/g/g2962.md) [megalōs](../../strongs/g/g3171.md), that now at the last your [phroneō](../../strongs/g/g5426.md) of me hath [anathallō](../../strongs/g/g330.md) again; wherein ye were also [phroneō](../../strongs/g/g5426.md), but ye [akaireomai](../../strongs/g/g170.md).

<a name="philippians_4_11"></a>Philippians 4:11

Not that I [legō](../../strongs/g/g3004.md) in respect of [hysterēsis](../../strongs/g/g5304.md): for I have [manthanō](../../strongs/g/g3129.md), in whatsoever state I am, to be [autarkēs](../../strongs/g/g842.md).

<a name="philippians_4_12"></a>Philippians 4:12

I [eidō](../../strongs/g/g1492.md) both how to be [tapeinoō](../../strongs/g/g5013.md), and I [eidō](../../strongs/g/g1492.md) how to [perisseuō](../../strongs/g/g4052.md): every where and in all things I am [myeō](../../strongs/g/g3453.md) both to be [chortazō](../../strongs/g/g5526.md) and to be [peinaō](../../strongs/g/g3983.md), both to [perisseuō](../../strongs/g/g4052.md) and to [hystereō](../../strongs/g/g5302.md).

<a name="philippians_4_13"></a>Philippians 4:13

I [ischyō](../../strongs/g/g2480.md) [pas](../../strongs/g/g3956.md) through [Christos](../../strongs/g/g5547.md) which [endynamoō](../../strongs/g/g1743.md) me.

<a name="philippians_4_14"></a>Philippians 4:14

Notwithstanding ye have [kalōs](../../strongs/g/g2573.md) [poieō](../../strongs/g/g4160.md), that ye did [sygkoinōneō](../../strongs/g/g4790.md) with my [thlipsis](../../strongs/g/g2347.md).

<a name="philippians_4_15"></a>Philippians 4:15

Now ye [philippēsios](../../strongs/g/g5374.md) [eidō](../../strongs/g/g1492.md) also, that in the [archē](../../strongs/g/g746.md) of the [euaggelion](../../strongs/g/g2098.md), when I [exerchomai](../../strongs/g/g1831.md) from [Makedonia](../../strongs/g/g3109.md), no [ekklēsia](../../strongs/g/g1577.md) [koinōneō](../../strongs/g/g2841.md) with me as [logos](../../strongs/g/g3056.md) [dosis](../../strongs/g/g1394.md) and [lēmpsis](../../strongs/g/g3028.md), but ye only.

<a name="philippians_4_16"></a>Philippians 4:16

For even in [Thessalonikē](../../strongs/g/g2332.md) ye [pempō](../../strongs/g/g3992.md) [hapax](../../strongs/g/g530.md) and again unto my [chreia](../../strongs/g/g5532.md).

<a name="philippians_4_17"></a>Philippians 4:17

Not because I [epizēteō](../../strongs/g/g1934.md) a [doma](../../strongs/g/g1390.md): but I [epizēteō](../../strongs/g/g1934.md) [karpos](../../strongs/g/g2590.md) that may [pleonazō](../../strongs/g/g4121.md) to your [logos](../../strongs/g/g3056.md).

<a name="philippians_4_18"></a>Philippians 4:18

But I have all, and [perisseuō](../../strongs/g/g4052.md): I am [plēroō](../../strongs/g/g4137.md), having [dechomai](../../strongs/g/g1209.md) of [epaphroditos](../../strongs/g/g1891.md) the things from you, an [osmē](../../strongs/g/g3744.md) of an [euōdia](../../strongs/g/g2175.md), a [thysia](../../strongs/g/g2378.md) [dektos](../../strongs/g/g1184.md), [euarestos](../../strongs/g/g2101.md) to [theos](../../strongs/g/g2316.md).

<a name="philippians_4_19"></a>Philippians 4:19

But my [theos](../../strongs/g/g2316.md) shall [plēroō](../../strongs/g/g4137.md) all your [chreia](../../strongs/g/g5532.md) according to his [ploutos](../../strongs/g/g4149.md) in [doxa](../../strongs/g/g1391.md) by [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="philippians_4_20"></a>Philippians 4:20

Now unto [theos](../../strongs/g/g2316.md) and our [patēr](../../strongs/g/g3962.md) [doxa](../../strongs/g/g1391.md) for [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="philippians_4_21"></a>Philippians 4:21

[aspazomai](../../strongs/g/g782.md) every [hagios](../../strongs/g/g40.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md). The [adelphos](../../strongs/g/g80.md) which are with me [aspazomai](../../strongs/g/g782.md) you.

<a name="philippians_4_22"></a>Philippians 4:22

All the [hagios](../../strongs/g/g40.md) [aspazomai](../../strongs/g/g782.md) you, [malista](../../strongs/g/g3122.md) they that are of [Kaisar](../../strongs/g/g2541.md) [oikia](../../strongs/g/g3614.md).

<a name="philippians_4_23"></a>Philippians 4:23

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Philippians](philippians.md)

[Philippians 3](philippians_3.md)