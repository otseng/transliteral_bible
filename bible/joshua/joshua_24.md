# [Joshua 24](https://www.blueletterbible.org/kjv/joshua/24)

<a name="joshua_24_1"></a>Joshua 24:1

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āsap̄](../../strongs/h/h622.md) all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [Šᵊḵem](../../strongs/h/h7927.md), and [qara'](../../strongs/h/h7121.md) for the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and for their [ro'sh](../../strongs/h/h7218.md), and for their [shaphat](../../strongs/h/h8199.md), and for their [šāṭar](../../strongs/h/h7860.md); and they [yatsab](../../strongs/h/h3320.md) themselves [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_24_2"></a>Joshua 24:2

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Your ['ab](../../strongs/h/h1.md) [yashab](../../strongs/h/h3427.md) on the other [ʿēḇer](../../strongs/h/h5676.md) of the [nāhār](../../strongs/h/h5104.md) in ['owlam](../../strongs/h/h5769.md), even [Teraḥ](../../strongs/h/h8646.md), the ['ab](../../strongs/h/h1.md) of ['Abraham](../../strongs/h/h85.md), and the ['ab](../../strongs/h/h1.md) of [Nāḥôr](../../strongs/h/h5152.md): and they ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_24_3"></a>Joshua 24:3

And I [laqach](../../strongs/h/h3947.md) your ['ab](../../strongs/h/h1.md) ['Abraham](../../strongs/h/h85.md) from the other [ʿēḇer](../../strongs/h/h5676.md) of the [nāhār](../../strongs/h/h5104.md), and [yālaḵ](../../strongs/h/h3212.md) him throughout all the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and [rabah](../../strongs/h/h7235.md) his [zera'](../../strongs/h/h2233.md), and [nathan](../../strongs/h/h5414.md) him [Yiṣḥāq](../../strongs/h/h3327.md).

<a name="joshua_24_4"></a>Joshua 24:4

And I [nathan](../../strongs/h/h5414.md) unto [Yiṣḥāq](../../strongs/h/h3327.md) [Ya'aqob](../../strongs/h/h3290.md) and [ʿĒśāv](../../strongs/h/h6215.md): and I [nathan](../../strongs/h/h5414.md) unto [ʿĒśāv](../../strongs/h/h6215.md) [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), to [yarash](../../strongs/h/h3423.md) it; but [Ya'aqob](../../strongs/h/h3290.md) and his [ben](../../strongs/h/h1121.md) [yarad](../../strongs/h/h3381.md) into [Mitsrayim](../../strongs/h/h4714.md).

<a name="joshua_24_5"></a>Joshua 24:5

I [shalach](../../strongs/h/h7971.md) [Mōshe](../../strongs/h/h4872.md) also and ['Ahărôn](../../strongs/h/h175.md), and I [nāḡap̄](../../strongs/h/h5062.md) [Mitsrayim](../../strongs/h/h4714.md), according to that ['ăšer](../../strongs/h/h834.md) I ['asah](../../strongs/h/h6213.md) [qereḇ](../../strongs/h/h7130.md) them: and ['aḥar](../../strongs/h/h310.md) I [yāṣā'](../../strongs/h/h3318.md) you.

<a name="joshua_24_6"></a>Joshua 24:6

And I [yāṣā'](../../strongs/h/h3318.md) your ['ab](../../strongs/h/h1.md) out of [Mitsrayim](../../strongs/h/h4714.md): and ye [bow'](../../strongs/h/h935.md) unto the [yam](../../strongs/h/h3220.md); and the [Mitsrayim](../../strongs/h/h4714.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) your ['ab](../../strongs/h/h1.md) with [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md) unto the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="joshua_24_7"></a>Joshua 24:7

And when they [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md), he [śûm](../../strongs/h/h7760.md) [ma'ăp̄ēl](../../strongs/h/h3990.md) between you and the [Miṣrî](../../strongs/h/h4713.md), and [bow'](../../strongs/h/h935.md) the [yam](../../strongs/h/h3220.md) upon them, and [kāsâ](../../strongs/h/h3680.md) them; and your ['ayin](../../strongs/h/h5869.md) have [ra'ah](../../strongs/h/h7200.md) what I have ['asah](../../strongs/h/h6213.md) in [Mitsrayim](../../strongs/h/h4714.md): and ye [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md) a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_24_8"></a>Joshua 24:8

And I [bow'](../../strongs/h/h935.md) you into the ['erets](../../strongs/h/h776.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [yashab](../../strongs/h/h3427.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md); and they [lāḥam](../../strongs/h/h3898.md) with you: and I [nathan](../../strongs/h/h5414.md) them into your [yad](../../strongs/h/h3027.md), that ye might [yarash](../../strongs/h/h3423.md) their ['erets](../../strongs/h/h776.md); and I [šāmaḏ](../../strongs/h/h8045.md) them from [paniym](../../strongs/h/h6440.md) you.

<a name="joshua_24_9"></a>Joshua 24:9

Then [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md), [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), [quwm](../../strongs/h/h6965.md) and [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md), and [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) [Bilʿām](../../strongs/h/h1109.md) the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md) to [qālal](../../strongs/h/h7043.md) you:

<a name="joshua_24_10"></a>Joshua 24:10

But I ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto [Bilʿām](../../strongs/h/h1109.md); therefore he [barak](../../strongs/h/h1288.md) you [barak](../../strongs/h/h1288.md): so I [natsal](../../strongs/h/h5337.md) you out of his [yad](../../strongs/h/h3027.md).

<a name="joshua_24_11"></a>Joshua 24:11

And ye ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [bow'](../../strongs/h/h935.md) unto [Yᵊrēḥô](../../strongs/h/h3405.md): and the [baʿal](../../strongs/h/h1167.md) of [Yᵊrēḥô](../../strongs/h/h3405.md) [lāḥam](../../strongs/h/h3898.md) against you, the ['Ĕmōrî](../../strongs/h/h567.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Girgāšî](../../strongs/h/h1622.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md); and I [nathan](../../strongs/h/h5414.md) them into your [yad](../../strongs/h/h3027.md).

<a name="joshua_24_12"></a>Joshua 24:12

And I [shalach](../../strongs/h/h7971.md) the [ṣirʿâ](../../strongs/h/h6880.md) [paniym](../../strongs/h/h6440.md) you, which drave them [gāraš](../../strongs/h/h1644.md) from [paniym](../../strongs/h/h6440.md) you, even the [šᵊnayim](../../strongs/h/h8147.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md); but not with thy [chereb](../../strongs/h/h2719.md), nor with thy [qesheth](../../strongs/h/h7198.md).

<a name="joshua_24_13"></a>Joshua 24:13

And I have [nathan](../../strongs/h/h5414.md) you an ['erets](../../strongs/h/h776.md) for which ye did not [yaga'](../../strongs/h/h3021.md), and [ʿîr](../../strongs/h/h5892.md) which ye [bānâ](../../strongs/h/h1129.md) not, and ye [yashab](../../strongs/h/h3427.md) in them; of the [kerem](../../strongs/h/h3754.md) and [zayiṯ](../../strongs/h/h2132.md) which ye [nāṭaʿ](../../strongs/h/h5193.md) not do ye ['akal](../../strongs/h/h398.md).

<a name="joshua_24_14"></a>Joshua 24:14

Now therefore [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) him in [tamiym](../../strongs/h/h8549.md) and in ['emeth](../../strongs/h/h571.md): and put [cuwr](../../strongs/h/h5493.md) the ['Elohiym](../../strongs/h/h430.md) which your ['ab](../../strongs/h/h1.md) ['abad](../../strongs/h/h5647.md) on the other [ʿēḇer](../../strongs/h/h5676.md) of the [nāhār](../../strongs/h/h5104.md), and in [Mitsrayim](../../strongs/h/h4714.md); and ['abad](../../strongs/h/h5647.md) ye [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_24_15"></a>Joshua 24:15

And if it seem [ra'a'](../../strongs/h/h7489.md) unto [mî](../../strongs/h/h4310.md) to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md), [bāḥar](../../strongs/h/h977.md) you this [yowm](../../strongs/h/h3117.md) ['ayin](../../strongs/h/h5869.md) ye will ['abad](../../strongs/h/h5647.md); whether the ['Elohiym](../../strongs/h/h430.md) which your ['ab](../../strongs/h/h1.md) ['abad](../../strongs/h/h5647.md) that were on the other [ʿēḇer](../../strongs/h/h5676.md) of the [nāhār](../../strongs/h/h5104.md), or the ['Elohiym](../../strongs/h/h430.md) of the ['Ĕmōrî](../../strongs/h/h567.md), in whose ['erets](../../strongs/h/h776.md) ye [yashab](../../strongs/h/h3427.md): but as for me and my [bayith](../../strongs/h/h1004.md), we will ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_24_16"></a>Joshua 24:16

And the ['am](../../strongs/h/h5971.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [ḥālîlâ](../../strongs/h/h2486.md) that we should ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), to ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md);

<a name="joshua_24_17"></a>Joshua 24:17

For [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), he it is that [ʿālâ](../../strongs/h/h5927.md) us and our ['ab](../../strongs/h/h1.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md), and which ['asah](../../strongs/h/h6213.md) those [gadowl](../../strongs/h/h1419.md) ['ôṯ](../../strongs/h/h226.md) in our ['ayin](../../strongs/h/h5869.md), and [shamar](../../strongs/h/h8104.md) us in all the [derek](../../strongs/h/h1870.md) wherein we [halak](../../strongs/h/h1980.md), and among all the ['am](../../strongs/h/h5971.md) [qereḇ](../../strongs/h/h7130.md) whom we ['abar](../../strongs/h/h5674.md):

<a name="joshua_24_18"></a>Joshua 24:18

And [Yĕhovah](../../strongs/h/h3068.md) [gāraš](../../strongs/h/h1644.md) from [paniym](../../strongs/h/h6440.md) us all the ['am](../../strongs/h/h5971.md), even the ['Ĕmōrî](../../strongs/h/h567.md) which [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md): therefore will we also ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md); for he is our ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_24_19"></a>Joshua 24:19

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), Ye [yakol](../../strongs/h/h3201.md) ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md): for he is a [qadowsh](../../strongs/h/h6918.md) ['Elohiym](../../strongs/h/h430.md); he is a [qannô'](../../strongs/h/h7072.md) ['el](../../strongs/h/h410.md); he will not [nasa'](../../strongs/h/h5375.md) your [pesha'](../../strongs/h/h6588.md) nor your [chatta'ath](../../strongs/h/h2403.md).

<a name="joshua_24_20"></a>Joshua 24:20

If ye ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md), then he will [shuwb](../../strongs/h/h7725.md) and do you [ra'a'](../../strongs/h/h7489.md), and [kalah](../../strongs/h/h3615.md) you, ['aḥar](../../strongs/h/h310.md) that he hath [yatab](../../strongs/h/h3190.md) you.

<a name="joshua_24_21"></a>Joshua 24:21

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), Nay; but we will ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_24_22"></a>Joshua 24:22

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), Ye are ['ed](../../strongs/h/h5707.md) against yourselves that ye have [bāḥar](../../strongs/h/h977.md) you [Yĕhovah](../../strongs/h/h3068.md), to ['abad](../../strongs/h/h5647.md) him. And they ['āmar](../../strongs/h/h559.md), We are ['ed](../../strongs/h/h5707.md).

<a name="joshua_24_23"></a>Joshua 24:23

Now therefore put [cuwr](../../strongs/h/h5493.md), said he, the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) which are [qereḇ](../../strongs/h/h7130.md) you, and [natah](../../strongs/h/h5186.md) your [lebab](../../strongs/h/h3824.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_24_24"></a>Joshua 24:24

And the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) will we ['abad](../../strongs/h/h5647.md), and his [qowl](../../strongs/h/h6963.md) will we [shama'](../../strongs/h/h8085.md).

<a name="joshua_24_25"></a>Joshua 24:25

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with the ['am](../../strongs/h/h5971.md) that [yowm](../../strongs/h/h3117.md), and [śûm](../../strongs/h/h7760.md) them a [choq](../../strongs/h/h2706.md) and a [mishpat](../../strongs/h/h4941.md) in [Šᵊḵem](../../strongs/h/h7927.md).

<a name="joshua_24_26"></a>Joshua 24:26

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [kāṯaḇ](../../strongs/h/h3789.md) these [dabar](../../strongs/h/h1697.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of ['Elohiym](../../strongs/h/h430.md), and [laqach](../../strongs/h/h3947.md) a [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md), and set it [quwm](../../strongs/h/h6965.md) there under an ['allâ](../../strongs/h/h427.md), that was by the [miqdash](../../strongs/h/h4720.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_24_27"></a>Joshua 24:27

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), Behold, this ['eben](../../strongs/h/h68.md) shall be a [ʿēḏâ](../../strongs/h/h5713.md) unto us; for it hath [shama'](../../strongs/h/h8085.md) all the ['emer](../../strongs/h/h561.md) of [Yĕhovah](../../strongs/h/h3068.md) which he [dabar](../../strongs/h/h1696.md) unto us: it shall be therefore a [ʿēḏâ](../../strongs/h/h5713.md) unto you, lest ye [kāḥaš](../../strongs/h/h3584.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_24_28"></a>Joshua 24:28

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), every ['iysh](../../strongs/h/h376.md) unto his [nachalah](../../strongs/h/h5159.md).

<a name="joshua_24_29"></a>Joshua 24:29

And it came to pass ['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), that [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), [muwth](../../strongs/h/h4191.md), being a [mē'â](../../strongs/h/h3967.md) and [ʿeśer](../../strongs/h/h6235.md) [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md).

<a name="joshua_24_30"></a>Joshua 24:30

And they [qāḇar](../../strongs/h/h6912.md) him in the [gᵊḇûl](../../strongs/h/h1366.md) of his [nachalah](../../strongs/h/h5159.md) in [Timnaṯ-Ḥeres](../../strongs/h/h8556.md), which is in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), on the [ṣāp̄ôn](../../strongs/h/h6828.md) of the [har](../../strongs/h/h2022.md) of [GaʿAš](../../strongs/h/h1608.md).

<a name="joshua_24_31"></a>Joshua 24:31

And [Yisra'el](../../strongs/h/h3478.md) ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and all the [yowm](../../strongs/h/h3117.md) of the [zāqēn](../../strongs/h/h2205.md) that ['arak](../../strongs/h/h748.md) [yowm](../../strongs/h/h3117.md) ['aḥar](../../strongs/h/h310.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and which had [yada'](../../strongs/h/h3045.md) all the [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md), that he had ['asah](../../strongs/h/h6213.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_24_32"></a>Joshua 24:32

And the ['etsem](../../strongs/h/h6106.md) of [Yôsēp̄](../../strongs/h/h3130.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), [qāḇar](../../strongs/h/h6912.md) they in [Šᵊḵem](../../strongs/h/h7927.md), in a [ḥelqâ](../../strongs/h/h2513.md) of [sadeh](../../strongs/h/h7704.md) which [Ya'aqob](../../strongs/h/h3290.md) [qānâ](../../strongs/h/h7069.md) of the [ben](../../strongs/h/h1121.md) of [ḥămôr](../../strongs/h/h2544.md) the ['ab](../../strongs/h/h1.md) of [Šᵊḵem](../../strongs/h/h7927.md) for a [mē'â](../../strongs/h/h3967.md) pieces of [qᵊśîṭâ](../../strongs/h/h7192.md): and it became the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="joshua_24_33"></a>Joshua 24:33

And ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) [muwth](../../strongs/h/h4191.md); and they [qāḇar](../../strongs/h/h6912.md) him in a [giḇʿâ](../../strongs/h/h1389.md) that pertained to [Pînḥās](../../strongs/h/h6372.md) his [ben](../../strongs/h/h1121.md), which was [nathan](../../strongs/h/h5414.md) him in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 23](joshua_23.md)