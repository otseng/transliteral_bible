# [Joshua 2](https://www.blueletterbible.org/kjv/joshua/2)

<a name="joshua_2_1"></a>Joshua 2:1

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) [shalach](../../strongs/h/h7971.md) out of [Šiṭṭāym](../../strongs/h/h7851.md) [šᵊnayim](../../strongs/h/h8147.md) ['enowsh](../../strongs/h/h582.md) to [ragal](../../strongs/h/h7270.md) [ḥereš](../../strongs/h/h2791.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md), even [Yᵊrēḥô](../../strongs/h/h3405.md). And they [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) into a [zānâ](../../strongs/h/h2181.md) ['ishshah](../../strongs/h/h802.md) [bayith](../../strongs/h/h1004.md), [shem](../../strongs/h/h8034.md) [Rāḥāḇ](../../strongs/h/h7343.md), and [shakab](../../strongs/h/h7901.md) there.

<a name="joshua_2_2"></a>Joshua 2:2

And it was ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) of [Yᵊrēḥô](../../strongs/h/h3405.md), ['āmar](../../strongs/h/h559.md), Behold, there [bow'](../../strongs/h/h935.md) ['enowsh](../../strongs/h/h582.md) in hither to [layil](../../strongs/h/h3915.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) to [chaphar](../../strongs/h/h2658.md) the ['erets](../../strongs/h/h776.md).

<a name="joshua_2_3"></a>Joshua 2:3

And the [melek](../../strongs/h/h4428.md) of [Yᵊrēḥô](../../strongs/h/h3405.md) [shalach](../../strongs/h/h7971.md) unto [Rāḥāḇ](../../strongs/h/h7343.md), ['āmar](../../strongs/h/h559.md), Bring [yāṣā'](../../strongs/h/h3318.md) the ['enowsh](../../strongs/h/h582.md) that are [bow'](../../strongs/h/h935.md) to thee, which are [bow'](../../strongs/h/h935.md) into thine [bayith](../../strongs/h/h1004.md): for they be [bow'](../../strongs/h/h935.md) to [chaphar](../../strongs/h/h2658.md) all the ['erets](../../strongs/h/h776.md).

<a name="joshua_2_4"></a>Joshua 2:4

And the ['ishshah](../../strongs/h/h802.md) [laqach](../../strongs/h/h3947.md) the [šᵊnayim](../../strongs/h/h8147.md) ['enowsh](../../strongs/h/h582.md), and [tsaphan](../../strongs/h/h6845.md) them, and ['āmar](../../strongs/h/h559.md) thus, There [bow'](../../strongs/h/h935.md) ['enowsh](../../strongs/h/h582.md) unto me, but I [yada'](../../strongs/h/h3045.md) not ['ayin](../../strongs/h/h370.md) they were:

<a name="joshua_2_5"></a>Joshua 2:5

And it came to pass about the time of [cagar](../../strongs/h/h5462.md) of the [sha'ar](../../strongs/h/h8179.md), when it was [choshek](../../strongs/h/h2822.md), that the ['enowsh](../../strongs/h/h582.md) [yāṣā'](../../strongs/h/h3318.md): whither the ['enowsh](../../strongs/h/h582.md) [halak](../../strongs/h/h1980.md) I [yada'](../../strongs/h/h3045.md) not: [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them [mahēr](../../strongs/h/h4118.md); for ye shall [nāśaḡ](../../strongs/h/h5381.md) them.

<a name="joshua_2_6"></a>Joshua 2:6

But she had [ʿālâ](../../strongs/h/h5927.md) them to the [gāḡ](../../strongs/h/h1406.md) of the house, and [taman](../../strongs/h/h2934.md) them with the ['ets](../../strongs/h/h6086.md) of [pēšeṯ](../../strongs/h/h6593.md), which she had laid in ['arak](../../strongs/h/h6186.md) upon the [gāḡ](../../strongs/h/h1406.md).

<a name="joshua_2_7"></a>Joshua 2:7

And the ['enowsh](../../strongs/h/h582.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them the [derek](../../strongs/h/h1870.md) to [Yardēn](../../strongs/h/h3383.md) unto the [maʿăḇār](../../strongs/h/h4569.md): and as soon as they which [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them were [yāṣā'](../../strongs/h/h3318.md), they [cagar](../../strongs/h/h5462.md) the [sha'ar](../../strongs/h/h8179.md).

<a name="joshua_2_8"></a>Joshua 2:8

And before they were [shakab](../../strongs/h/h7901.md), she [ʿālâ](../../strongs/h/h5927.md) unto them upon the [gāḡ](../../strongs/h/h1406.md);

<a name="joshua_2_9"></a>Joshua 2:9

And she ['āmar](../../strongs/h/h559.md) unto the ['enowsh](../../strongs/h/h582.md), I [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) you the ['erets](../../strongs/h/h776.md), and that your ['êmâ](../../strongs/h/h367.md) is [naphal](../../strongs/h/h5307.md) upon us, and that all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) [mûḡ](../../strongs/h/h4127.md) [paniym](../../strongs/h/h6440.md) of you.

<a name="joshua_2_10"></a>Joshua 2:10

For we have [shama'](../../strongs/h/h8085.md) how [Yĕhovah](../../strongs/h/h3068.md) [yāḇēš](../../strongs/h/h3001.md) the [mayim](../../strongs/h/h4325.md) of the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md) for [paniym](../../strongs/h/h6440.md), when ye came [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md); and what ye ['asah](../../strongs/h/h6213.md) unto the [šᵊnayim](../../strongs/h/h8147.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), that were on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), [Sîḥôn](../../strongs/h/h5511.md) and [ʿÔḡ](../../strongs/h/h5747.md), whom ye utterly [ḥāram](../../strongs/h/h2763.md).

<a name="joshua_2_11"></a>Joshua 2:11

And as soon as we had [shama'](../../strongs/h/h8085.md) these things, our [lebab](../../strongs/h/h3824.md) did [māsas](../../strongs/h/h4549.md), neither did there [quwm](../../strongs/h/h6965.md) any more [ruwach](../../strongs/h/h7307.md) in any ['iysh](../../strongs/h/h376.md), [paniym](../../strongs/h/h6440.md) of you: for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), he is ['Elohiym](../../strongs/h/h430.md) in [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md), and in ['erets](../../strongs/h/h776.md) beneath.

<a name="joshua_2_12"></a>Joshua 2:12

Now therefore, I pray you, [shaba'](../../strongs/h/h7650.md) unto me by [Yĕhovah](../../strongs/h/h3068.md), [kî](../../strongs/h/h3588.md) I have ['asah](../../strongs/h/h6213.md) you [checed](../../strongs/h/h2617.md), that ye will also ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and [nathan](../../strongs/h/h5414.md) me an ['emeth](../../strongs/h/h571.md) ['ôṯ](../../strongs/h/h226.md):

<a name="joshua_2_13"></a>Joshua 2:13

And that ye will save [ḥāyâ](../../strongs/h/h2421.md) my ['ab](../../strongs/h/h1.md), and my ['em](../../strongs/h/h517.md), and my ['ach](../../strongs/h/h251.md), and my ['āḥôṯ](../../strongs/h/h269.md), and all that they have, and [natsal](../../strongs/h/h5337.md) our [nephesh](../../strongs/h/h5315.md) from [maveth](../../strongs/h/h4194.md).

<a name="joshua_2_14"></a>Joshua 2:14

And the ['enowsh](../../strongs/h/h582.md) ['āmar](../../strongs/h/h559.md) her, Our [nephesh](../../strongs/h/h5315.md) for [muwth](../../strongs/h/h4191.md), if ye [nāḡaḏ](../../strongs/h/h5046.md) not this our [dabar](../../strongs/h/h1697.md). And it shall be, when [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) us the ['erets](../../strongs/h/h776.md), that we will ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) with thee.

<a name="joshua_2_15"></a>Joshua 2:15

Then she [yarad](../../strongs/h/h3381.md) them by a [chebel](../../strongs/h/h2256.md) [bᵊʿaḏ](../../strongs/h/h1157.md) the [ḥallôn](../../strongs/h/h2474.md): for her [bayith](../../strongs/h/h1004.md) was upon the [qîr](../../strongs/h/h7023.md) [ḥômâ](../../strongs/h/h2346.md), and she [yashab](../../strongs/h/h3427.md) upon the [ḥômâ](../../strongs/h/h2346.md).

<a name="joshua_2_16"></a>Joshua 2:16

And she ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md) you to the [har](../../strongs/h/h2022.md), lest the [radaph](../../strongs/h/h7291.md) [pāḡaʿ](../../strongs/h/h6293.md) you; and [ḥāḇâ](../../strongs/h/h2247.md) yourselves there [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md), until the [radaph](../../strongs/h/h7291.md) be [shuwb](../../strongs/h/h7725.md): and ['aḥar](../../strongs/h/h310.md) may ye [yālaḵ](../../strongs/h/h3212.md) your [derek](../../strongs/h/h1870.md).

<a name="joshua_2_17"></a>Joshua 2:17

And the ['enowsh](../../strongs/h/h582.md) ['āmar](../../strongs/h/h559.md) unto her, We will be [naqiy](../../strongs/h/h5355.md) of this thine [šᵊḇûʿâ](../../strongs/h/h7621.md) which thou hast made us [shaba'](../../strongs/h/h7650.md).

<a name="joshua_2_18"></a>Joshua 2:18

Behold, when we [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md), thou shalt [qāšar](../../strongs/h/h7194.md) this [tiqvâ](../../strongs/h/h8615.md) of [šānî](../../strongs/h/h8144.md) [ḥûṭ](../../strongs/h/h2339.md) in the [ḥallôn](../../strongs/h/h2474.md) which thou didst [yarad](../../strongs/h/h3381.md) us: and thou shalt ['āsap̄](../../strongs/h/h622.md) thy ['ab](../../strongs/h/h1.md), and thy ['em](../../strongs/h/h517.md), and thy ['ach](../../strongs/h/h251.md), and all thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), [bayith](../../strongs/h/h1004.md) unto thee.

<a name="joshua_2_19"></a>Joshua 2:19

And it shall be, that ['ăšer](../../strongs/h/h834.md) shall [yāṣā'](../../strongs/h/h3318.md) of the [deleṯ](../../strongs/h/h1817.md) of thy [bayith](../../strongs/h/h1004.md) into the [ḥûṣ](../../strongs/h/h2351.md), his [dam](../../strongs/h/h1818.md) shall be upon his [ro'sh](../../strongs/h/h7218.md), and we will be [naqiy](../../strongs/h/h5355.md): and whosoever shall be with thee in the [bayith](../../strongs/h/h1004.md), his [dam](../../strongs/h/h1818.md) shall be on our [ro'sh](../../strongs/h/h7218.md), if any [yad](../../strongs/h/h3027.md) be upon him.

<a name="joshua_2_20"></a>Joshua 2:20

And if thou [nāḡaḏ](../../strongs/h/h5046.md) this our [dabar](../../strongs/h/h1697.md), then we will be [naqiy](../../strongs/h/h5355.md) of thine [šᵊḇûʿâ](../../strongs/h/h7621.md) which thou hast made us to [shaba'](../../strongs/h/h7650.md).

<a name="joshua_2_21"></a>Joshua 2:21

And she ['āmar](../../strongs/h/h559.md), According unto your [dabar](../../strongs/h/h1697.md), so be it. And she [shalach](../../strongs/h/h7971.md) them, and they [yālaḵ](../../strongs/h/h3212.md): and she [qāšar](../../strongs/h/h7194.md) the [šānî](../../strongs/h/h8144.md) [tiqvâ](../../strongs/h/h8615.md) in the [ḥallôn](../../strongs/h/h2474.md).

<a name="joshua_2_22"></a>Joshua 2:22

And they [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) unto the [har](../../strongs/h/h2022.md), and [yashab](../../strongs/h/h3427.md) there [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md), until the [radaph](../../strongs/h/h7291.md) were [shuwb](../../strongs/h/h7725.md): and the [radaph](../../strongs/h/h7291.md) [bāqaš](../../strongs/h/h1245.md) them throughout all the [derek](../../strongs/h/h1870.md), but [māṣā'](../../strongs/h/h4672.md) them not.

<a name="joshua_2_23"></a>Joshua 2:23

So the [šᵊnayim](../../strongs/h/h8147.md) ['enowsh](../../strongs/h/h582.md) [shuwb](../../strongs/h/h7725.md), and [yarad](../../strongs/h/h3381.md) from the [har](../../strongs/h/h2022.md), and ['abar](../../strongs/h/h5674.md), and [bow'](../../strongs/h/h935.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and [sāp̄ar](../../strongs/h/h5608.md) him all things that [māṣā'](../../strongs/h/h4672.md) them:

<a name="joshua_2_24"></a>Joshua 2:24

And they ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [kî](../../strongs/h/h3588.md) [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) into our [yad](../../strongs/h/h3027.md) all the ['erets](../../strongs/h/h776.md); for even all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) do [mûḡ](../../strongs/h/h4127.md) [paniym](../../strongs/h/h6440.md) of us.

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 1](joshua_1.md) - [Joshua 3](joshua_3.md)