# [Joshua 18](https://www.blueletterbible.org/kjv/joshua/18)

<a name="joshua_18_1"></a>Joshua 18:1

And the whole ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md) at [Šîlô](../../strongs/h/h7887.md), and [shakan](../../strongs/h/h7931.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) there. And the ['erets](../../strongs/h/h776.md) was [kāḇaš](../../strongs/h/h3533.md) [paniym](../../strongs/h/h6440.md) them.

<a name="joshua_18_2"></a>Joshua 18:2

And there [yāṯar](../../strongs/h/h3498.md) among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [šeḇaʿ](../../strongs/h/h7651.md) [shebet](../../strongs/h/h7626.md), which had not yet [chalaq](../../strongs/h/h2505.md) their [nachalah](../../strongs/h/h5159.md).

<a name="joshua_18_3"></a>Joshua 18:3

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), How long are ye [rāp̄â](../../strongs/h/h7503.md) to [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) hath [nathan](../../strongs/h/h5414.md) you?

<a name="joshua_18_4"></a>Joshua 18:4

[yāhaḇ](../../strongs/h/h3051.md) from among you [šālôš](../../strongs/h/h7969.md) ['enowsh](../../strongs/h/h582.md) for each [shebet](../../strongs/h/h7626.md): and I will [shalach](../../strongs/h/h7971.md) them, and they shall [quwm](../../strongs/h/h6965.md), and [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md), and [kāṯaḇ](../../strongs/h/h3789.md) it [peh](../../strongs/h/h6310.md) to the [nachalah](../../strongs/h/h5159.md) of them; and they shall [bow'](../../strongs/h/h935.md) again to me.

<a name="joshua_18_5"></a>Joshua 18:5

And they shall [chalaq](../../strongs/h/h2505.md) it into [šeḇaʿ](../../strongs/h/h7651.md) [cheleq](../../strongs/h/h2506.md): [Yehuwdah](../../strongs/h/h3063.md) shall ['amad](../../strongs/h/h5975.md) in their [gᵊḇûl](../../strongs/h/h1366.md) on the [neḡeḇ](../../strongs/h/h5045.md), and the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md) shall ['amad](../../strongs/h/h5975.md) in their [gᵊḇûl](../../strongs/h/h1366.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="joshua_18_6"></a>Joshua 18:6

Ye shall therefore [kāṯaḇ](../../strongs/h/h3789.md) the ['erets](../../strongs/h/h776.md) into [šeḇaʿ](../../strongs/h/h7651.md) [cheleq](../../strongs/h/h2506.md), and [bow'](../../strongs/h/h935.md) the description hither to me, that I may [yārâ](../../strongs/h/h3384.md) [gôrāl](../../strongs/h/h1486.md) for you here [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_18_7"></a>Joshua 18:7

But the [Lᵊvî](../../strongs/h/h3881.md) have no [cheleq](../../strongs/h/h2506.md) [qereḇ](../../strongs/h/h7130.md) you; for the [kᵊhunnâ](../../strongs/h/h3550.md) of [Yĕhovah](../../strongs/h/h3068.md) is their [nachalah](../../strongs/h/h5159.md): and [Gāḏ](../../strongs/h/h1410.md), and [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and [ḥēṣî](../../strongs/h/h2677.md) the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), have [laqach](../../strongs/h/h3947.md) their [nachalah](../../strongs/h/h5159.md) [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) on the [mizrach](../../strongs/h/h4217.md), which [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them.

<a name="joshua_18_8"></a>Joshua 18:8

And the ['enowsh](../../strongs/h/h582.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md): and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [tsavah](../../strongs/h/h6680.md) them that [halak](../../strongs/h/h1980.md) to [kāṯaḇ](../../strongs/h/h3789.md) the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [halak](../../strongs/h/h1980.md) through the ['erets](../../strongs/h/h776.md), and [kāṯaḇ](../../strongs/h/h3789.md) it, and [shuwb](../../strongs/h/h7725.md) to me, that I may here [shalak](../../strongs/h/h7993.md) [gôrāl](../../strongs/h/h1486.md) for you [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in [Šîlô](../../strongs/h/h7887.md).

<a name="joshua_18_9"></a>Joshua 18:9

And the ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) and ['abar](../../strongs/h/h5674.md) the ['erets](../../strongs/h/h776.md), and [kāṯaḇ](../../strongs/h/h3789.md) it by [ʿîr](../../strongs/h/h5892.md) into [šeḇaʿ](../../strongs/h/h7651.md) [cheleq](../../strongs/h/h2506.md) in a [sēp̄er](../../strongs/h/h5612.md), and [bow'](../../strongs/h/h935.md) again to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) to the [maḥănê](../../strongs/h/h4264.md) at [Šîlô](../../strongs/h/h7887.md).

<a name="joshua_18_10"></a>Joshua 18:10

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shalak](../../strongs/h/h7993.md) [gôrāl](../../strongs/h/h1486.md) for them in [Šîlô](../../strongs/h/h7887.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and there [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [chalaq](../../strongs/h/h2505.md) the ['erets](../../strongs/h/h776.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) according to their [maḥălōqeṯ](../../strongs/h/h4256.md).

<a name="joshua_18_11"></a>Joshua 18:11

And the [gôrāl](../../strongs/h/h1486.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) [ʿālâ](../../strongs/h/h5927.md) according to their [mišpāḥâ](../../strongs/h/h4940.md): and the [gᵊḇûl](../../strongs/h/h1366.md) of their [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) between the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) and the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="joshua_18_12"></a>Joshua 18:12

And their [gᵊḇûl](../../strongs/h/h1366.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) was from [Yardēn](../../strongs/h/h3383.md); and the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) to the [kāṯēp̄](../../strongs/h/h3802.md) of [Yᵊrēḥô](../../strongs/h/h3405.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) side, and [ʿālâ](../../strongs/h/h5927.md) through the [har](../../strongs/h/h2022.md) [yam](../../strongs/h/h3220.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof were at the [midbar](../../strongs/h/h4057.md) of [Bêṯ 'Āven](../../strongs/h/h1007.md).

<a name="joshua_18_13"></a>Joshua 18:13

And the [gᵊḇûl](../../strongs/h/h1366.md) ['abar](../../strongs/h/h5674.md) from thence toward [Lûz](../../strongs/h/h3870.md), to the [kāṯēp̄](../../strongs/h/h3802.md) of [Lûz](../../strongs/h/h3870.md), which is [Bêṯ-'ēl](../../strongs/h/h1008.md), [neḡeḇ](../../strongs/h/h5045.md); and the [gᵊḇûl](../../strongs/h/h1366.md) [yarad](../../strongs/h/h3381.md) to [ʿAṭrôṯ 'Adār](../../strongs/h/h5853.md), near the [har](../../strongs/h/h2022.md) that lieth on the [neḡeḇ](../../strongs/h/h5045.md) of the [taḥtôn](../../strongs/h/h8481.md) [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md).

<a name="joshua_18_14"></a>Joshua 18:14

And the [gᵊḇûl](../../strongs/h/h1366.md) was [tā'ar](../../strongs/h/h8388.md) thence, and [cabab](../../strongs/h/h5437.md) the [pē'â](../../strongs/h/h6285.md) of the [yam](../../strongs/h/h3220.md) [neḡeḇ](../../strongs/h/h5045.md), from the [har](../../strongs/h/h2022.md) that [paniym](../../strongs/h/h6440.md) [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) [neḡeḇ](../../strongs/h/h5045.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof were at [Qiryaṯ-BaʿAl](../../strongs/h/h7154.md), which is [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), a [ʿîr](../../strongs/h/h5892.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md): this was the [yam](../../strongs/h/h3220.md) [pē'â](../../strongs/h/h6285.md).

<a name="joshua_18_15"></a>Joshua 18:15

And the [neḡeḇ](../../strongs/h/h5045.md) [pē'â](../../strongs/h/h6285.md) was from the [qāṣê](../../strongs/h/h7097.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), and the [gᵊḇûl](../../strongs/h/h1366.md) [yāṣā'](../../strongs/h/h3318.md) on the [yam](../../strongs/h/h3220.md), and [yāṣā'](../../strongs/h/h3318.md) to the [maʿyān](../../strongs/h/h4599.md) of [mayim](../../strongs/h/h4325.md) of [Nep̄Tôaḥ](../../strongs/h/h5318.md):

<a name="joshua_18_16"></a>Joshua 18:16

And the [gᵊḇûl](../../strongs/h/h1366.md) [yarad](../../strongs/h/h3381.md) to the [qāṣê](../../strongs/h/h7097.md) of the [har](../../strongs/h/h2022.md) that [paniym](../../strongs/h/h6440.md) the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), and which is in the [ʿēmeq](../../strongs/h/h6010.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md), and [yarad](../../strongs/h/h3381.md) to the [gay'](../../strongs/h/h1516.md) of [Hinnōm](../../strongs/h/h2011.md), to the [kāṯēp̄](../../strongs/h/h3802.md) of [Yᵊḇûsî](../../strongs/h/h2983.md) on the [neḡeḇ](../../strongs/h/h5045.md), and [yarad](../../strongs/h/h3381.md) to [ʿÊn Rōḡēl](../../strongs/h/h5883.md),

<a name="joshua_18_17"></a>Joshua 18:17

And was [tā'ar](../../strongs/h/h8388.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md), and [yāṣā'](../../strongs/h/h3318.md) to [ʿÊn Šemeš](../../strongs/h/h5885.md), and [yāṣā'](../../strongs/h/h3318.md) toward [Gᵊlîlôṯ](../../strongs/h/h1553.md), which is over [nōḵaḥ](../../strongs/h/h5227.md) the [maʿălê](../../strongs/h/h4608.md) of ['Ăḏummîm](../../strongs/h/h131.md), and [yarad](../../strongs/h/h3381.md) to the ['eben](../../strongs/h/h68.md) of [Bōhan](../../strongs/h/h932.md) the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md),

<a name="joshua_18_18"></a>Joshua 18:18

And ['abar](../../strongs/h/h5674.md) toward the [kāṯēp̄](../../strongs/h/h3802.md) over [môl](../../strongs/h/h4136.md) ['Arabah](../../strongs/h/h6160.md) [ṣāp̄ôn](../../strongs/h/h6828.md), and [yarad](../../strongs/h/h3381.md) unto ['Arabah](../../strongs/h/h6160.md):

<a name="joshua_18_19"></a>Joshua 18:19

And the [gᵊḇûl](../../strongs/h/h1366.md) ['abar](../../strongs/h/h5674.md) to the [kāṯēp̄](../../strongs/h/h3802.md) of [Bêṯ-Ḥāḡlâ](../../strongs/h/h1031.md) [ṣāp̄ôn](../../strongs/h/h6828.md): and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of the [gᵊḇûl](../../strongs/h/h1366.md) were at the [ṣāp̄ôn](../../strongs/h/h6828.md) [lashown](../../strongs/h/h3956.md) of the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md) at the [neḡeḇ](../../strongs/h/h5045.md) [qāṣê](../../strongs/h/h7097.md) of [Yardēn](../../strongs/h/h3383.md): this was the [neḡeḇ](../../strongs/h/h5045.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="joshua_18_20"></a>Joshua 18:20

And [Yardēn](../../strongs/h/h3383.md) was the [gāḇal](../../strongs/h/h1379.md) of it on the [qeḏem](../../strongs/h/h6924.md) [pē'â](../../strongs/h/h6285.md). This was the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), by the [gᵊḇûlâ](../../strongs/h/h1367.md) thereof [cabiyb](../../strongs/h/h5439.md), according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_18_21"></a>Joshua 18:21

Now the [ʿîr](../../strongs/h/h5892.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) according to their [mišpāḥâ](../../strongs/h/h4940.md) were [Yᵊrēḥô](../../strongs/h/h3405.md), and [Bêṯ-Ḥāḡlâ](../../strongs/h/h1031.md), and the [ʿēmeq](../../strongs/h/h6010.md) of [Qᵊṣîṣ](../../strongs/h/h7104.md),

<a name="joshua_18_22"></a>Joshua 18:22

And [Bêṯ HāʿĂrāḇâ](../../strongs/h/h1026.md), and [Ṣᵊmārayim](../../strongs/h/h6787.md), and [Bêṯ-'ēl](../../strongs/h/h1008.md),

<a name="joshua_18_23"></a>Joshua 18:23

And [ʿAûî](../../strongs/h/h5761.md), and [Pārâ](../../strongs/h/h6511.md), and [ʿĀp̄Râ](../../strongs/h/h6084.md),

<a name="joshua_18_24"></a>Joshua 18:24

And [Kᵊp̄Ar HāʿAmmônî](../../strongs/h/h3726.md), and [ʿĀp̄Nî](../../strongs/h/h6078.md), and [Geḇaʿ](../../strongs/h/h1387.md); [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_18_25"></a>Joshua 18:25

[Giḇʿôn](../../strongs/h/h1391.md), and [rāmâ](../../strongs/h/h7414.md), and [Bᵊ'Ērôṯ](../../strongs/h/h881.md),

<a name="joshua_18_26"></a>Joshua 18:26

And [Miṣpê](../../strongs/h/h4708.md), and [Kᵊp̄Îrâ](../../strongs/h/h3716.md), and [mōṣâ](../../strongs/h/h4681.md),

<a name="joshua_18_27"></a>Joshua 18:27

And [Reqem](../../strongs/h/h7552.md), and [Yirpᵊ'Ēl](../../strongs/h/h3416.md), and [Tar'Ălâ](../../strongs/h/h8634.md),

<a name="joshua_18_28"></a>Joshua 18:28

And [Ṣēlāʿ](../../strongs/h/h6762.md), ['Elep̄](../../strongs/h/h507.md), and [Yᵊḇûsî](../../strongs/h/h2983.md), which is [Yĕruwshalaim](../../strongs/h/h3389.md), [GiḇʿĀṯ](../../strongs/h/h1394.md), and [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md); ['arbaʿ](../../strongs/h/h702.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md). This is the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 17](joshua_17.md) - [Joshua 19](joshua_19.md)