# [Joshua 11](https://www.blueletterbible.org/kjv/joshua/11)

<a name="joshua_11_1"></a>Joshua 11:1

And it came to pass, when [Yāḇîn](../../strongs/h/h2985.md) [melek](../../strongs/h/h4428.md) of [Ḥāṣôr](../../strongs/h/h2674.md) had [shama'](../../strongs/h/h8085.md) those things, that he [shalach](../../strongs/h/h7971.md) to [Yôḇāḇ](../../strongs/h/h3103.md) [melek](../../strongs/h/h4428.md) of [Māḏôn](../../strongs/h/h4068.md), and to the [melek](../../strongs/h/h4428.md) of [šimrôn](../../strongs/h/h8110.md), and to the [melek](../../strongs/h/h4428.md) of ['Aḵšāp̄](../../strongs/h/h407.md),

<a name="joshua_11_2"></a>Joshua 11:2

And to the [melek](../../strongs/h/h4428.md) that were on the [ṣāp̄ôn](../../strongs/h/h6828.md) of the [har](../../strongs/h/h2022.md), and of the ['arabah](../../strongs/h/h6160.md) [neḡeḇ](../../strongs/h/h5045.md) of [Kinnᵊrôṯ](../../strongs/h/h3672.md), and in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the [nāp̄â](../../strongs/h/h5299.md) of [Dôr](../../strongs/h/h1756.md) on the [yam](../../strongs/h/h3220.md),

<a name="joshua_11_3"></a>Joshua 11:3

And to the [Kᵊnaʿănî](../../strongs/h/h3669.md) on the [mizrach](../../strongs/h/h4217.md) and on the [yam](../../strongs/h/h3220.md), and to the ['Ĕmōrî](../../strongs/h/h567.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md) in the [har](../../strongs/h/h2022.md), and to the [Ḥiûî](../../strongs/h/h2340.md) under [Ḥermôn](../../strongs/h/h2768.md) in the ['erets](../../strongs/h/h776.md) of [Miṣpâ](../../strongs/h/h4709.md).

<a name="joshua_11_4"></a>Joshua 11:4

And they [yāṣā'](../../strongs/h/h3318.md), they and all their [maḥănê](../../strongs/h/h4264.md) with them, [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), even as the [ḥôl](../../strongs/h/h2344.md) that is upon the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md) in [rōḇ](../../strongs/h/h7230.md), with [sûs](../../strongs/h/h5483.md) and [reḵeḇ](../../strongs/h/h7393.md) [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md).

<a name="joshua_11_5"></a>Joshua 11:5

And when all these [melek](../../strongs/h/h4428.md) were met [yāʿaḏ](../../strongs/h/h3259.md), they [bow'](../../strongs/h/h935.md) and [ḥānâ](../../strongs/h/h2583.md) [yaḥaḏ](../../strongs/h/h3162.md) at the [mayim](../../strongs/h/h4325.md) of [Mērôm](../../strongs/h/h4792.md), to [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_11_6"></a>Joshua 11:6

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), Be not [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) of them: for [māḥār](../../strongs/h/h4279.md) about this [ʿēṯ](../../strongs/h/h6256.md) will I [nathan](../../strongs/h/h5414.md) them all [ḥālāl](../../strongs/h/h2491.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md): thou shalt [ʿāqar](../../strongs/h/h6131.md) their [sûs](../../strongs/h/h5483.md), and [śārap̄](../../strongs/h/h8313.md) their [merkāḇâ](../../strongs/h/h4818.md) with ['esh](../../strongs/h/h784.md).

<a name="joshua_11_7"></a>Joshua 11:7

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [bow'](../../strongs/h/h935.md), and all the ['am](../../strongs/h/h5971.md) of [milḥāmâ](../../strongs/h/h4421.md) with him, against them by the [mayim](../../strongs/h/h4325.md) of [Mērôm](../../strongs/h/h4792.md) [piṯ'ōm](../../strongs/h/h6597.md); and they [naphal](../../strongs/h/h5307.md) them.

<a name="joshua_11_8"></a>Joshua 11:8

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md), who [nakah](../../strongs/h/h5221.md) them, and [radaph](../../strongs/h/h7291.md) them unto [rab](../../strongs/h/h7227.md) [Ṣîḏôn](../../strongs/h/h6721.md), and unto [Miśrᵊp̄Ôṯ Mayim](../../strongs/h/h4956.md), and unto the [biqʿâ](../../strongs/h/h1237.md) of [Miṣpê](../../strongs/h/h4708.md) [mizrach](../../strongs/h/h4217.md); and they [nakah](../../strongs/h/h5221.md) them, until they [šā'ar](../../strongs/h/h7604.md) them none [śārîḏ](../../strongs/h/h8300.md).

<a name="joshua_11_9"></a>Joshua 11:9

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) unto them as [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) him: he [ʿāqar](../../strongs/h/h6131.md) their [sûs](../../strongs/h/h5483.md), and [śārap̄](../../strongs/h/h8313.md) their [merkāḇâ](../../strongs/h/h4818.md) with ['esh](../../strongs/h/h784.md).

<a name="joshua_11_10"></a>Joshua 11:10

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) at that [ʿēṯ](../../strongs/h/h6256.md) [shuwb](../../strongs/h/h7725.md), and [lāḵaḏ](../../strongs/h/h3920.md) [Ḥāṣôr](../../strongs/h/h2674.md), and [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) thereof with the [chereb](../../strongs/h/h2719.md): for [Ḥāṣôr](../../strongs/h/h2674.md) [paniym](../../strongs/h/h6440.md) was the [ro'sh](../../strongs/h/h7218.md) of all those [mamlāḵâ](../../strongs/h/h4467.md).

<a name="joshua_11_11"></a>Joshua 11:11

And they [nakah](../../strongs/h/h5221.md) all the [nephesh](../../strongs/h/h5315.md) that were therein with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), [ḥāram](../../strongs/h/h2763.md) them: there was not any [yāṯar](../../strongs/h/h3498.md) to [neshamah](../../strongs/h/h5397.md): and he [śārap̄](../../strongs/h/h8313.md) [Ḥāṣôr](../../strongs/h/h2674.md) with ['esh](../../strongs/h/h784.md).

<a name="joshua_11_12"></a>Joshua 11:12

And all the [ʿîr](../../strongs/h/h5892.md) of those [melek](../../strongs/h/h4428.md), and all the [melek](../../strongs/h/h4428.md) of them, did [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [lāḵaḏ](../../strongs/h/h3920.md), and [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and he [ḥāram](../../strongs/h/h2763.md) them, as [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md).

<a name="joshua_11_13"></a>Joshua 11:13

But as for the [ʿîr](../../strongs/h/h5892.md) that ['amad](../../strongs/h/h5975.md) still in their [tēl](../../strongs/h/h8510.md), [Yisra'el](../../strongs/h/h3478.md) [śārap̄](../../strongs/h/h8313.md) none of them, [zûlâ](../../strongs/h/h2108.md) [Ḥāṣôr](../../strongs/h/h2674.md) only; that did [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [śārap̄](../../strongs/h/h8313.md).

<a name="joshua_11_14"></a>Joshua 11:14

And all the [šālāl](../../strongs/h/h7998.md) of these [ʿîr](../../strongs/h/h5892.md), and the [bĕhemah](../../strongs/h/h929.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) took for a [bāzaz](../../strongs/h/h962.md) unto themselves; but every ['āḏām](../../strongs/h/h120.md) they [nakah](../../strongs/h/h5221.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), until they had [šāmaḏ](../../strongs/h/h8045.md) them, neither [šā'ar](../../strongs/h/h7604.md) they any to [neshamah](../../strongs/h/h5397.md).

<a name="joshua_11_15"></a>Joshua 11:15

As [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) his ['ebed](../../strongs/h/h5650.md), so did [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and so ['asah](../../strongs/h/h6213.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md); he [cuwr](../../strongs/h/h5493.md) [dabar](../../strongs/h/h1697.md) of all that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_11_16"></a>Joshua 11:16

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [laqach](../../strongs/h/h3947.md) all that ['erets](../../strongs/h/h776.md), the [har](../../strongs/h/h2022.md), and all the [neḡeḇ](../../strongs/h/h5045.md), and all the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md), and the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and the ['arabah](../../strongs/h/h6160.md), and the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md), and the [šᵊp̄ēlâ](../../strongs/h/h8219.md) of the same;

<a name="joshua_11_17"></a>Joshua 11:17

Even from the [har](../../strongs/h/h2022.md) [Ḥālāq](../../strongs/h/h2510.md), that [ʿālâ](../../strongs/h/h5927.md) to [Śēʿîr](../../strongs/h/h8165.md), even unto [BaʿAl Gaḏ](../../strongs/h/h1171.md) in the [biqʿâ](../../strongs/h/h1237.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) under [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md): and all their [melek](../../strongs/h/h4428.md) he [lāḵaḏ](../../strongs/h/h3920.md), and [nakah](../../strongs/h/h5221.md) them, and [muwth](../../strongs/h/h4191.md) them.

<a name="joshua_11_18"></a>Joshua 11:18

[Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) with all those [melek](../../strongs/h/h4428.md).

<a name="joshua_11_19"></a>Joshua 11:19

There was not a [ʿîr](../../strongs/h/h5892.md) that made [shalam](../../strongs/h/h7999.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), save the [Ḥiûî](../../strongs/h/h2340.md) the [yashab](../../strongs/h/h3427.md) of [Giḇʿôn](../../strongs/h/h1391.md): all other they [laqach](../../strongs/h/h3947.md) in [milḥāmâ](../../strongs/h/h4421.md).

<a name="joshua_11_20"></a>Joshua 11:20

For it was of [Yĕhovah](../../strongs/h/h3068.md) to [ḥāzaq](../../strongs/h/h2388.md) their [leb](../../strongs/h/h3820.md), that they should come [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md) in [milḥāmâ](../../strongs/h/h4421.md), that he might [ḥāram](../../strongs/h/h2763.md) them, and that they might have no [tĕchinnah](../../strongs/h/h8467.md), but that he might [šāmaḏ](../../strongs/h/h8045.md) them, as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_11_21"></a>Joshua 11:21

And at that [ʿēṯ](../../strongs/h/h6256.md) [bow'](../../strongs/h/h935.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and [karath](../../strongs/h/h3772.md) the [ʿĂnāqîm](../../strongs/h/h6062.md) from the [har](../../strongs/h/h2022.md), from [Ḥeḇrôn](../../strongs/h/h2275.md), from [dᵊḇîr](../../strongs/h/h1688.md), from [ʿĀnāḇ](../../strongs/h/h6024.md), and from all the [har](../../strongs/h/h2022.md) of [Yehuwdah](../../strongs/h/h3063.md), and from all the [har](../../strongs/h/h2022.md) of [Yisra'el](../../strongs/h/h3478.md): [Yᵊhôšûaʿ](../../strongs/h/h3091.md) destroyed them [ḥāram](../../strongs/h/h2763.md) with their [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_11_22"></a>Joshua 11:22

There was none of the [ʿĂnāqîm](../../strongs/h/h6062.md) [yāṯar](../../strongs/h/h3498.md) in the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): only in [ʿAzzâ](../../strongs/h/h5804.md), in [Gaṯ](../../strongs/h/h1661.md), and in ['Ašdôḏ](../../strongs/h/h795.md), there [šā'ar](../../strongs/h/h7604.md).

<a name="joshua_11_23"></a>Joshua 11:23

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [laqach](../../strongs/h/h3947.md) the ['erets](../../strongs/h/h776.md), according to all that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md); and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nathan](../../strongs/h/h5414.md) it for a [nachalah](../../strongs/h/h5159.md) unto [Yisra'el](../../strongs/h/h3478.md) according to their [maḥălōqeṯ](../../strongs/h/h4256.md) by their [shebet](../../strongs/h/h7626.md). And the ['erets](../../strongs/h/h776.md) [šāqaṭ](../../strongs/h/h8252.md) from [milḥāmâ](../../strongs/h/h4421.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 10](joshua_10.md) - [Joshua 12](joshua_12.md)