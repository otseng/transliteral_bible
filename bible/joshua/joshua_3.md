# [Joshua 3](https://www.blueletterbible.org/kjv/joshua/3)

<a name="joshua_3_1"></a>Joshua 3:1

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md); and they [nāsaʿ](../../strongs/h/h5265.md) from [Šiṭṭāym](../../strongs/h/h7851.md), and [bow'](../../strongs/h/h935.md) to [Yardēn](../../strongs/h/h3383.md), he and all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [lûn](../../strongs/h/h3885.md) there before they ['abar](../../strongs/h/h5674.md).

<a name="joshua_3_2"></a>Joshua 3:2

And it came to pass [qāṣê](../../strongs/h/h7097.md) [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md), that the [šāṭar](../../strongs/h/h7860.md) ['abar](../../strongs/h/h5674.md) [qereḇ](../../strongs/h/h7130.md) the [maḥănê](../../strongs/h/h4264.md);

<a name="joshua_3_3"></a>Joshua 3:3

And they [tsavah](../../strongs/h/h6680.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), When ye [ra'ah](../../strongs/h/h7200.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) [nasa'](../../strongs/h/h5375.md) it, then ye shall [nāsaʿ](../../strongs/h/h5265.md) from your [maqowm](../../strongs/h/h4725.md), and [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) it.

<a name="joshua_3_4"></a>Joshua 3:4

Yet there shall be a [rachowq](../../strongs/h/h7350.md) between you and it, about two ['elep̄](../../strongs/h/h505.md) ['ammâ](../../strongs/h/h520.md) by [midâ](../../strongs/h/h4060.md): [qāraḇ](../../strongs/h/h7126.md) not unto it, that ye may [yada'](../../strongs/h/h3045.md) the [derek](../../strongs/h/h1870.md) by which ye must [yālaḵ](../../strongs/h/h3212.md): for ye have not ['abar](../../strongs/h/h5674.md) this [derek](../../strongs/h/h1870.md) [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md).

<a name="joshua_3_5"></a>Joshua 3:5

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [qadash](../../strongs/h/h6942.md) yourselves: for [māḥār](../../strongs/h/h4279.md) [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md) [qereḇ](../../strongs/h/h7130.md) you.

<a name="joshua_3_6"></a>Joshua 3:6

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md), and ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md). And they [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md), and [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md).

<a name="joshua_3_7"></a>Joshua 3:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), This [yowm](../../strongs/h/h3117.md) will I [ḥālal](../../strongs/h/h2490.md) to [gāḏal](../../strongs/h/h1431.md) thee in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md), that they may [yada'](../../strongs/h/h3045.md) that, as I was with [Mōshe](../../strongs/h/h4872.md), so I will be with thee.

<a name="joshua_3_8"></a>Joshua 3:8

And thou shalt [tsavah](../../strongs/h/h6680.md) the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md), ['āmar](../../strongs/h/h559.md), When ye are [bow'](../../strongs/h/h935.md) to the [qāṣê](../../strongs/h/h7097.md) of the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md), ye shall ['amad](../../strongs/h/h5975.md) in [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_3_9"></a>Joshua 3:9

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [nāḡaš](../../strongs/h/h5066.md), and [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_3_10"></a>Joshua 3:10

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md), Hereby ye shall [yada'](../../strongs/h/h3045.md) that the [chay](../../strongs/h/h2416.md) ['el](../../strongs/h/h410.md) is [qereḇ](../../strongs/h/h7130.md) you, and that he will [yarash](../../strongs/h/h3423.md) [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) you the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ḥitî](../../strongs/h/h2850.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Girgāšî](../../strongs/h/h1622.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="joshua_3_11"></a>Joshua 3:11

Behold, the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of the ['adown](../../strongs/h/h113.md) of all the ['erets](../../strongs/h/h776.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) you into [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_3_12"></a>Joshua 3:12

Now therefore [laqach](../../strongs/h/h3947.md) you [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['iysh](../../strongs/h/h376.md) out of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), out of ['echad](../../strongs/h/h259.md) [shebet](../../strongs/h/h7626.md) an ['iysh](../../strongs/h/h376.md).

<a name="joshua_3_13"></a>Joshua 3:13

And it shall come to pass, as soon as the [kaph](../../strongs/h/h3709.md) of the [regel](../../strongs/h/h7272.md) of the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), the ['adown](../../strongs/h/h113.md) of all the ['erets](../../strongs/h/h776.md), shall [nuwach](../../strongs/h/h5117.md) in the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md), that the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) shall be [karath](../../strongs/h/h3772.md) from the [mayim](../../strongs/h/h4325.md) that [yarad](../../strongs/h/h3381.md) from [maʿal](../../strongs/h/h4605.md); and they shall ['amad](../../strongs/h/h5975.md) upon ['echad](../../strongs/h/h259.md) [nēḏ](../../strongs/h/h5067.md).

<a name="joshua_3_14"></a>Joshua 3:14

And it came to pass, when the ['am](../../strongs/h/h5971.md) [nāsaʿ](../../strongs/h/h5265.md) from their ['ohel](../../strongs/h/h168.md), to ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and the [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md);

<a name="joshua_3_15"></a>Joshua 3:15

And as they that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) were [bow'](../../strongs/h/h935.md) unto [Yardēn](../../strongs/h/h3383.md), and the [regel](../../strongs/h/h7272.md) of the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) were [ṭāḇal](../../strongs/h/h2881.md) in the [qāṣê](../../strongs/h/h7097.md) of the [mayim](../../strongs/h/h4325.md), (for [Yardēn](../../strongs/h/h3383.md) [mālā'](../../strongs/h/h4390.md) all his [gāḏâ](../../strongs/h/h1415.md) all the [yowm](../../strongs/h/h3117.md) of [qāṣîr](../../strongs/h/h7105.md),)

<a name="joshua_3_16"></a>Joshua 3:16

That the [mayim](../../strongs/h/h4325.md) which [yarad](../../strongs/h/h3381.md) from [maʿal](../../strongs/h/h4605.md) ['amad](../../strongs/h/h5975.md) and [quwm](../../strongs/h/h6965.md) upon ['echad](../../strongs/h/h259.md) [nēḏ](../../strongs/h/h5067.md) [me'od](../../strongs/h/h3966.md) [rachaq](../../strongs/h/h7368.md) from the [ʿîr](../../strongs/h/h5892.md) ['Āḏām](../../strongs/h/h121.md), that is [ṣaḏ](../../strongs/h/h6654.md) [Ṣrṯn](../../strongs/h/h6891.md): and those that came [yarad](../../strongs/h/h3381.md) toward the [yam](../../strongs/h/h3220.md) of the ['arabah](../../strongs/h/h6160.md), even the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md), [tamam](../../strongs/h/h8552.md), and were [karath](../../strongs/h/h3772.md): and the ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md) right against [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_3_17"></a>Joshua 3:17

And the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) ['amad](../../strongs/h/h5975.md) [kuwn](../../strongs/h/h3559.md) on [ḥārāḇâ](../../strongs/h/h2724.md) in the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), and all the [Yisra'el](../../strongs/h/h3478.md) ['abar](../../strongs/h/h5674.md) on [ḥārāḇâ](../../strongs/h/h2724.md), until all the [gowy](../../strongs/h/h1471.md) were ['abar](../../strongs/h/h5674.md) [tamam](../../strongs/h/h8552.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 2](joshua_2.md) - [Joshua 4](joshua_4.md)