# [Joshua 23](https://www.blueletterbible.org/kjv/joshua/23)

<a name="joshua_23_1"></a>Joshua 23:1

And it came to pass a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) ['aḥar](../../strongs/h/h310.md) that [Yĕhovah](../../strongs/h/h3068.md) had [nuwach](../../strongs/h/h5117.md) unto [Yisra'el](../../strongs/h/h3478.md) from all their ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), that [Yᵊhôšûaʿ](../../strongs/h/h3091.md) waxed [zāqēn](../../strongs/h/h2204.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md).

<a name="joshua_23_2"></a>Joshua 23:2

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) for all [Yisra'el](../../strongs/h/h3478.md), and for their [zāqēn](../../strongs/h/h2205.md), and for their [ro'sh](../../strongs/h/h7218.md), and for their [shaphat](../../strongs/h/h8199.md), and for their [šāṭar](../../strongs/h/h7860.md), and ['āmar](../../strongs/h/h559.md) unto them, I am [zāqēn](../../strongs/h/h2204.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md):

<a name="joshua_23_3"></a>Joshua 23:3

And ye have [ra'ah](../../strongs/h/h7200.md) all that [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath ['asah](../../strongs/h/h6213.md) unto all these [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) of you; for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) is he that hath [lāḥam](../../strongs/h/h3898.md) for you.

<a name="joshua_23_4"></a>Joshua 23:4

[ra'ah](../../strongs/h/h7200.md), I have [naphal](../../strongs/h/h5307.md) unto you by lot these [gowy](../../strongs/h/h1471.md) that [šā'ar](../../strongs/h/h7604.md), to be a [nachalah](../../strongs/h/h5159.md) for your [shebet](../../strongs/h/h7626.md), from [Yardēn](../../strongs/h/h3383.md), with all the [gowy](../../strongs/h/h1471.md) that I have [karath](../../strongs/h/h3772.md), even unto the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) [māḇô'](../../strongs/h/h3996.md) [šemeš](../../strongs/h/h8121.md).

<a name="joshua_23_5"></a>Joshua 23:5

And [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), he shall [hāḏap̄](../../strongs/h/h1920.md) them from [paniym](../../strongs/h/h6440.md) you, and [yarash](../../strongs/h/h3423.md) them from out of your [paniym](../../strongs/h/h6440.md); and ye shall [yarash](../../strongs/h/h3423.md) their ['erets](../../strongs/h/h776.md), as [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [dabar](../../strongs/h/h1696.md) unto you.

<a name="joshua_23_6"></a>Joshua 23:6

Be ye therefore [me'od](../../strongs/h/h3966.md) [ḥāzaq](../../strongs/h/h2388.md) to [shamar](../../strongs/h/h8104.md) and to ['asah](../../strongs/h/h6213.md) all that is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), that ye [cuwr](../../strongs/h/h5493.md) not aside therefrom to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md);

<a name="joshua_23_7"></a>Joshua 23:7

That ye [bow'](../../strongs/h/h935.md) not among these [gowy](../../strongs/h/h1471.md), these that [šā'ar](../../strongs/h/h7604.md) among you; neither make [zakar](../../strongs/h/h2142.md) of the [shem](../../strongs/h/h8034.md) of their ['Elohiym](../../strongs/h/h430.md), nor cause to [shaba'](../../strongs/h/h7650.md) by them, neither ['abad](../../strongs/h/h5647.md) them, nor [shachah](../../strongs/h/h7812.md) yourselves unto them:

<a name="joshua_23_8"></a>Joshua 23:8

But [dāḇaq](../../strongs/h/h1692.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), as ye have ['asah](../../strongs/h/h6213.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_23_9"></a>Joshua 23:9

For [Yĕhovah](../../strongs/h/h3068.md) hath [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) you [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) and ['atsuwm](../../strongs/h/h6099.md): but as for you, no ['iysh](../../strongs/h/h376.md) hath been able to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) you unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_23_10"></a>Joshua 23:10

['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md) of you shall [radaph](../../strongs/h/h7291.md) an ['elep̄](../../strongs/h/h505.md): for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), he it is that [lāḥam](../../strongs/h/h3898.md) for you, as he hath [dabar](../../strongs/h/h1696.md) you.

<a name="joshua_23_11"></a>Joshua 23:11

[me'od](../../strongs/h/h3966.md) [shamar](../../strongs/h/h8104.md) therefore unto [nephesh](../../strongs/h/h5315.md), that ye ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_23_12"></a>Joshua 23:12

Else if ye do in any [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md), and [dāḇaq](../../strongs/h/h1692.md) unto the [yeṯer](../../strongs/h/h3499.md) of these [gowy](../../strongs/h/h1471.md), even these that [šā'ar](../../strongs/h/h7604.md) among you, and shall make [ḥāṯan](../../strongs/h/h2859.md) with them, and go [bow'](../../strongs/h/h935.md) unto them, and they to you:

<a name="joshua_23_13"></a>Joshua 23:13

[yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) will no [yāsap̄](../../strongs/h/h3254.md) [yarash](../../strongs/h/h3423.md) any of these [gowy](../../strongs/h/h1471.md) from [paniym](../../strongs/h/h6440.md) you; but they shall be [paḥ](../../strongs/h/h6341.md) and [mowqesh](../../strongs/h/h4170.md) unto you, and [šōṭēṭ](../../strongs/h/h7850.md) in your [ṣaḏ](../../strongs/h/h6654.md), and [ṣᵊnînîm](../../strongs/h/h6796.md) in your ['ayin](../../strongs/h/h5869.md), until ye ['abad](../../strongs/h/h6.md) from off this [towb](../../strongs/h/h2896.md) ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) you.

<a name="joshua_23_14"></a>Joshua 23:14

And, behold, this [yowm](../../strongs/h/h3117.md) I am [halak](../../strongs/h/h1980.md) the [derek](../../strongs/h/h1870.md) of all the ['erets](../../strongs/h/h776.md): and ye [yada'](../../strongs/h/h3045.md) in all your [lebab](../../strongs/h/h3824.md) and in all your [nephesh](../../strongs/h/h5315.md), that not ['echad](../../strongs/h/h259.md) [dabar](../../strongs/h/h1697.md) hath [naphal](../../strongs/h/h5307.md) of all the [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) concerning you; all are [bow'](../../strongs/h/h935.md) unto you, and not ['echad](../../strongs/h/h259.md) [dabar](../../strongs/h/h1697.md) hath [naphal](../../strongs/h/h5307.md) thereof.

<a name="joshua_23_15"></a>Joshua 23:15

Therefore it shall come to pass, that as all [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) are [bow'](../../strongs/h/h935.md) upon you, which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [dabar](../../strongs/h/h1696.md) you; so shall [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) upon you all [ra'](../../strongs/h/h7451.md) [dabar](../../strongs/h/h1697.md), until he have [šāmaḏ](../../strongs/h/h8045.md) you from off this [towb](../../strongs/h/h2896.md) ['ăḏāmâ](../../strongs/h/h127.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) you.

<a name="joshua_23_16"></a>Joshua 23:16

When ye have ['abar](../../strongs/h/h5674.md) the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which he [tsavah](../../strongs/h/h6680.md) you, and have [halak](../../strongs/h/h1980.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) yourselves to them; then shall the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) be [ḥārâ](../../strongs/h/h2734.md) against you, and ye shall ['abad](../../strongs/h/h6.md) [mᵊhērâ](../../strongs/h/h4120.md) from off the [towb](../../strongs/h/h2896.md) ['erets](../../strongs/h/h776.md) which he hath [nathan](../../strongs/h/h5414.md) unto you.

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 22](joshua_22.md) - [Joshua 24](joshua_24.md)