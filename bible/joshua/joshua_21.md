# [Joshua 21](https://www.blueletterbible.org/kjv/joshua/21)

<a name="joshua_21_1"></a>Joshua 21:1

Then came [nāḡaš](../../strongs/h/h5066.md) the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [Lᵊvî](../../strongs/h/h3881.md) unto ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and unto the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="joshua_21_2"></a>Joshua 21:2

And they [dabar](../../strongs/h/h1696.md) unto them at [Šîlô](../../strongs/h/h7887.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md) to [nathan](../../strongs/h/h5414.md) us [ʿîr](../../strongs/h/h5892.md) to [yashab](../../strongs/h/h3427.md), with the [miḡrāš](../../strongs/h/h4054.md) thereof for our [bĕhemah](../../strongs/h/h929.md).

<a name="joshua_21_3"></a>Joshua 21:3

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) unto the [Lᵊvî](../../strongs/h/h3881.md) out of their [nachalah](../../strongs/h/h5159.md), at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md), these [ʿîr](../../strongs/h/h5892.md) and their [miḡrāš](../../strongs/h/h4054.md).

<a name="joshua_21_4"></a>Joshua 21:4

And the [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) for the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md): and the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), which were of the [Lᵊvî](../../strongs/h/h3881.md), had by [gôrāl](../../strongs/h/h1486.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Šimʿōnî](../../strongs/h/h8099.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md), [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_5"></a>Joshua 21:5

And the [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) had by [gôrāl](../../strongs/h/h1486.md) out of the [mišpāḥâ](../../strongs/h/h4940.md) of the [maṭṭê](../../strongs/h/h4294.md) of ['Ep̄rayim](../../strongs/h/h669.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md), and out of the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [ʿeśer](../../strongs/h/h6235.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_6"></a>Joshua 21:6

And the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md) had by [gôrāl](../../strongs/h/h1486.md) out of the [mišpāḥâ](../../strongs/h/h4940.md) of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), and out of the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md) in [Bāšān](../../strongs/h/h1316.md), [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_7"></a>Joshua 21:7

The [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) by their [mišpāḥâ](../../strongs/h/h4940.md) had out of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_8"></a>Joshua 21:8

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) by [gôrāl](../../strongs/h/h1486.md) unto the [Lᵊvî](../../strongs/h/h3881.md) these [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_21_9"></a>Joshua 21:9

And they [nathan](../../strongs/h/h5414.md) out of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and out of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md), these [ʿîr](../../strongs/h/h5892.md) which are here [qara'](../../strongs/h/h7121.md) by [shem](../../strongs/h/h8034.md),

<a name="joshua_21_10"></a>Joshua 21:10

Which the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), being of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md), who were of the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), had: for theirs was the [ri'šôn](../../strongs/h/h7223.md) [ri'šôn](../../strongs/h/h7223.md) [gôrāl](../../strongs/h/h1486.md).

<a name="joshua_21_11"></a>Joshua 21:11

And they [nathan](../../strongs/h/h5414.md) them the [qiryâ](../../strongs/h/h7151.md) of ['Arbaʿ](../../strongs/h/h704.md) [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md) the ['ab](../../strongs/h/h1.md) of [ʿĂnāq](../../strongs/h/h6061.md), which city is [Ḥeḇrôn](../../strongs/h/h2275.md), in the [har](../../strongs/h/h2022.md) country of [Yehuwdah](../../strongs/h/h3063.md), with the [miḡrāš](../../strongs/h/h4054.md) thereof [cabiyb](../../strongs/h/h5439.md) it.

<a name="joshua_21_12"></a>Joshua 21:12

But the [sadeh](../../strongs/h/h7704.md) of the [ʿîr](../../strongs/h/h5892.md), and the [ḥāṣēr](../../strongs/h/h2691.md) thereof, [nathan](../../strongs/h/h5414.md) they to [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) for his ['achuzzah](../../strongs/h/h272.md).

<a name="joshua_21_13"></a>Joshua 21:13

Thus they [nathan](../../strongs/h/h5414.md) to the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) [Ḥeḇrôn](../../strongs/h/h2275.md) with her [miḡrāš](../../strongs/h/h4054.md), to be a [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for the [ratsach](../../strongs/h/h7523.md); and [Liḇnâ](../../strongs/h/h3841.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_14"></a>Joshua 21:14

And [Yatîr](../../strongs/h/h3492.md) with her [miḡrāš](../../strongs/h/h4054.md), and ['Eštᵊmōaʿ](../../strongs/h/h851.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_15"></a>Joshua 21:15

And [Ḥōlôn](../../strongs/h/h2473.md) with her [miḡrāš](../../strongs/h/h4054.md), and [dᵊḇîr](../../strongs/h/h1688.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_16"></a>Joshua 21:16

And [ʿAyin](../../strongs/h/h5871.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Yuṭṭâ](../../strongs/h/h3194.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Bêṯ Šemeš](../../strongs/h/h1053.md) with her [miḡrāš](../../strongs/h/h4054.md); [tēšaʿ](../../strongs/h/h8672.md) [ʿîr](../../strongs/h/h5892.md) out of those [šᵊnayim](../../strongs/h/h8147.md) [shebet](../../strongs/h/h7626.md).

<a name="joshua_21_17"></a>Joshua 21:17

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Binyāmîn](../../strongs/h/h1144.md), [Giḇʿôn](../../strongs/h/h1391.md) with her [miḡrāš](../../strongs/h/h4054.md), [Geḇaʿ](../../strongs/h/h1387.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_18"></a>Joshua 21:18

[ʿĂnāṯôṯ](../../strongs/h/h6068.md) with her [miḡrāš](../../strongs/h/h4054.md), and [ʿAlmôn](../../strongs/h/h5960.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_19"></a>Joshua 21:19

All the [ʿîr](../../strongs/h/h5892.md) of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), the [kōhēn](../../strongs/h/h3548.md), were [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md).

<a name="joshua_21_20"></a>Joshua 21:20

And the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md), the [Lᵊvî](../../strongs/h/h3881.md) which [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md), even they had the [ʿîr](../../strongs/h/h5892.md) of their [gôrāl](../../strongs/h/h1486.md) out of the [maṭṭê](../../strongs/h/h4294.md) of ['Ep̄rayim](../../strongs/h/h669.md).

<a name="joshua_21_21"></a>Joshua 21:21

For they [nathan](../../strongs/h/h5414.md) them [Šᵊḵem](../../strongs/h/h7927.md) with her [miḡrāš](../../strongs/h/h4054.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), to be a [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for the [ratsach](../../strongs/h/h7523.md); and [Gezer](../../strongs/h/h1507.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_22"></a>Joshua 21:22

And [Qiḇṣayim](../../strongs/h/h6911.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_23"></a>Joshua 21:23

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md), ['Eltᵊqē'](../../strongs/h/h514.md) with her [miḡrāš](../../strongs/h/h4054.md), [Gibṯôn](../../strongs/h/h1405.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_24"></a>Joshua 21:24

['Ayyālôn](../../strongs/h/h357.md) with her [miḡrāš](../../strongs/h/h4054.md), [Gaṯ-Rimmôn](../../strongs/h/h1667.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_25"></a>Joshua 21:25

And out of the [maḥăṣîṯ](../../strongs/h/h4276.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md), [TaʿNāḵ](../../strongs/h/h8590.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Gaṯ-Rimmôn](../../strongs/h/h1667.md) with her [miḡrāš](../../strongs/h/h4054.md); [šᵊnayim](../../strongs/h/h8147.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_26"></a>Joshua 21:26

All the [ʿîr](../../strongs/h/h5892.md) were [ʿeśer](../../strongs/h/h6235.md) with their [miḡrāš](../../strongs/h/h4054.md) for the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Qᵊhāṯ](../../strongs/h/h6955.md) that [yāṯar](../../strongs/h/h3498.md).

<a name="joshua_21_27"></a>Joshua 21:27

And unto the [ben](../../strongs/h/h1121.md) of [Gēršôn](../../strongs/h/h1648.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Lᵊvî](../../strongs/h/h3881.md), out of the other [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md) they gave [Gôlān](../../strongs/h/h1474.md) in [Bāšān](../../strongs/h/h1316.md) with her [miḡrāš](../../strongs/h/h4054.md), to be a [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for the [ratsach](../../strongs/h/h7523.md); and [BᵊʿEštᵊrâ](../../strongs/h/h1203.md) with her [miḡrāš](../../strongs/h/h4054.md); [šᵊnayim](../../strongs/h/h8147.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_28"></a>Joshua 21:28

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Yiśśāśḵār](../../strongs/h/h3485.md), [Qišyôn](../../strongs/h/h7191.md) with her [miḡrāš](../../strongs/h/h4054.md), [Dāḇraṯ](../../strongs/h/h1705.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_29"></a>Joshua 21:29

[Yarmûṯ](../../strongs/h/h3412.md) with her [miḡrāš](../../strongs/h/h4054.md), [ʿÊn Ḡannîm](../../strongs/h/h5873.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_30"></a>Joshua 21:30

And out of the [maṭṭê](../../strongs/h/h4294.md) of ['Āšēr](../../strongs/h/h836.md), [Miš'Āl](../../strongs/h/h4861.md) with her [miḡrāš](../../strongs/h/h4054.md), [ʿAḇdôn](../../strongs/h/h5658.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_31"></a>Joshua 21:31

[Ḥelqāṯ](../../strongs/h/h2520.md) with her [miḡrāš](../../strongs/h/h4054.md), and [rᵊḥōḇ](../../strongs/h/h7340.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_32"></a>Joshua 21:32

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Nap̄tālî](../../strongs/h/h5321.md), [Qeḏeš](../../strongs/h/h6943.md) in [Gālîl](../../strongs/h/h1551.md) with her [miḡrāš](../../strongs/h/h4054.md), to be a [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for the [ratsach](../../strongs/h/h7523.md); and [Ḥmmōṯ Dō'R](../../strongs/h/h2576.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Qartān](../../strongs/h/h7178.md) with her [miḡrāš](../../strongs/h/h4054.md); [šālôš](../../strongs/h/h7969.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_33"></a>Joshua 21:33

All the [ʿîr](../../strongs/h/h5892.md) of the [Gēršunnî](../../strongs/h/h1649.md) according to their [mišpāḥâ](../../strongs/h/h4940.md) were [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md).

<a name="joshua_21_34"></a>Joshua 21:34

And unto the [mišpāḥâ](../../strongs/h/h4940.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), the [yāṯar](../../strongs/h/h3498.md) of the [Lᵊvî](../../strongs/h/h3881.md), out of the [maṭṭê](../../strongs/h/h4294.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md), [YāqnᵊʿĀm](../../strongs/h/h3362.md) with her [miḡrāš](../../strongs/h/h4054.md), and [Qartâ](../../strongs/h/h7177.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_35"></a>Joshua 21:35

[Dimnâ](../../strongs/h/h1829.md) with her [miḡrāš](../../strongs/h/h4054.md), [Nahălāl](../../strongs/h/h5096.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_36"></a>Joshua 21:36

And out of the tribe of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), [Beṣer](../../strongs/h/h1221.md) with her suburbs, and [Yahaṣ](../../strongs/h/h3096.md) with her suburbs,

<a name="joshua_21_37"></a>Joshua 21:37

[Qᵊḏēmôṯ](../../strongs/h/h6932.md) with her suburbs, and [Mēvp̄AʿAṯ](../../strongs/h/h4158.md) with her suburbs; ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_38"></a>Joshua 21:38

And out of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) in [Gilʿāḏ](../../strongs/h/h1568.md) with her [miḡrāš](../../strongs/h/h4054.md), to be a [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md) for the [ratsach](../../strongs/h/h7523.md); and [Maḥănayim](../../strongs/h/h4266.md) with her [miḡrāš](../../strongs/h/h4054.md),

<a name="joshua_21_39"></a>Joshua 21:39

[Hešbôn](../../strongs/h/h2809.md) with her [miḡrāš](../../strongs/h/h4054.md), [Yaʿzêr](../../strongs/h/h3270.md) with her [miḡrāš](../../strongs/h/h4054.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md) in all.

<a name="joshua_21_40"></a>Joshua 21:40

So all the [ʿîr](../../strongs/h/h5892.md) for the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md) by their [mišpāḥâ](../../strongs/h/h4940.md), which were [yāṯar](../../strongs/h/h3498.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [Lᵊvî](../../strongs/h/h3881.md), were by their [gôrāl](../../strongs/h/h1486.md) [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_41"></a>Joshua 21:41

All the [ʿîr](../../strongs/h/h5892.md) of the [Lᵊvî](../../strongs/h/h3881.md) [tavek](../../strongs/h/h8432.md) the ['achuzzah](../../strongs/h/h272.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were ['arbāʿîm](../../strongs/h/h705.md) and [šᵊmōnê](../../strongs/h/h8083.md) [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md).

<a name="joshua_21_42"></a>Joshua 21:42

These [ʿîr](../../strongs/h/h5892.md) were every [ʿîr](../../strongs/h/h5892.md) [ʿîr](../../strongs/h/h5892.md) with their [miḡrāš](../../strongs/h/h4054.md) [cabiyb](../../strongs/h/h5439.md) them: thus were all these [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_21_43"></a>Joshua 21:43

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) unto [Yisra'el](../../strongs/h/h3478.md) all the ['erets](../../strongs/h/h776.md) which he [shaba'](../../strongs/h/h7650.md) to [nathan](../../strongs/h/h5414.md) unto their ['ab](../../strongs/h/h1.md); and they [yarash](../../strongs/h/h3423.md) it, and [yashab](../../strongs/h/h3427.md) therein.

<a name="joshua_21_44"></a>Joshua 21:44

And [Yĕhovah](../../strongs/h/h3068.md) gave them [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md), according to all that he [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md): and there ['amad](../../strongs/h/h5975.md) not an ['iysh](../../strongs/h/h376.md) of all their ['oyeb](../../strongs/h/h341.md) [paniym](../../strongs/h/h6440.md) them; [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) all their ['oyeb](../../strongs/h/h341.md) into their [yad](../../strongs/h/h3027.md).

<a name="joshua_21_45"></a>Joshua 21:45

There [naphal](../../strongs/h/h5307.md) not [dabar](../../strongs/h/h1697.md) of any [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); all came to [bow'](../../strongs/h/h935.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 20](joshua_20.md) - [Joshua 22](joshua_22.md)