# [Joshua 19](https://www.blueletterbible.org/kjv/joshua/19)

<a name="joshua_19_1"></a>Joshua 19:1

And the [šēnî](../../strongs/h/h8145.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) to [Šimʿôn](../../strongs/h/h8095.md), even for the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) according to their [mišpāḥâ](../../strongs/h/h4940.md): and their [nachalah](../../strongs/h/h5159.md) was [tavek](../../strongs/h/h8432.md) the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="joshua_19_2"></a>Joshua 19:2

And they had in their [nachalah](../../strongs/h/h5159.md) [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), or [Šeḇaʿ](../../strongs/h/h7652.md), and [Môlāḏâ](../../strongs/h/h4137.md),

<a name="joshua_19_3"></a>Joshua 19:3

And [Ḥăṣar ShÛʿĀl](../../strongs/h/h2705.md), and [Bālâ](../../strongs/h/h1088.md), and [ʿEṣem](../../strongs/h/h6107.md),

<a name="joshua_19_4"></a>Joshua 19:4

And ['Eltôlaḏ](../../strongs/h/h513.md), and [Bᵊṯûl](../../strongs/h/h1329.md), and [Ḥārmâ](../../strongs/h/h2767.md),

<a name="joshua_19_5"></a>Joshua 19:5

And [Ṣiqlāḡ](../../strongs/h/h6860.md), and [Bêṯ-Hammarkāḇôṯ](../../strongs/h/h1024.md), and [Ḥăṣar Sûsâ](../../strongs/h/h2701.md),

<a name="joshua_19_6"></a>Joshua 19:6

And [Bêṯ Lᵊḇā'Ôṯ](../../strongs/h/h1034.md), and [Šārûḥen](../../strongs/h/h8287.md); [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) and their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_19_7"></a>Joshua 19:7

[ʿAyin](../../strongs/h/h5871.md), [Rimmôn](../../strongs/h/h7417.md), and [ʿEṯer](../../strongs/h/h6281.md), and [ʿĀšān](../../strongs/h/h6228.md); ['arbaʿ](../../strongs/h/h702.md) [ʿîr](../../strongs/h/h5892.md) and their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_19_8"></a>Joshua 19:8

And all the [ḥāṣēr](../../strongs/h/h2691.md) that were [cabiyb](../../strongs/h/h5439.md) these [ʿîr](../../strongs/h/h5892.md) to [BaʿĂlaṯ Bᵊ'Ēr](../../strongs/h/h1192.md), [rāmâ](../../strongs/h/h7414.md) of the [neḡeḇ](../../strongs/h/h5045.md). This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_19_9"></a>Joshua 19:9

Out of the [chebel](../../strongs/h/h2256.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) was the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md): for the [cheleq](../../strongs/h/h2506.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) was too [rab](../../strongs/h/h7227.md) for them: therefore the [ben](../../strongs/h/h1121.md) of [Šimʿôn](../../strongs/h/h8095.md) had their [nāḥal](../../strongs/h/h5157.md) [tavek](../../strongs/h/h8432.md) the [nachalah](../../strongs/h/h5159.md) of them.

<a name="joshua_19_10"></a>Joshua 19:10

And the [šᵊlîšî](../../strongs/h/h7992.md) [gôrāl](../../strongs/h/h1486.md) [ʿālâ](../../strongs/h/h5927.md) for the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md) according to their [mišpāḥâ](../../strongs/h/h4940.md): and the [gᵊḇûl](../../strongs/h/h1366.md) of their [nachalah](../../strongs/h/h5159.md) was unto [Śārîḏ](../../strongs/h/h8301.md):

<a name="joshua_19_11"></a>Joshua 19:11

And their [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) toward the [yam](../../strongs/h/h3220.md), and [MarʿĂlâ](../../strongs/h/h4831.md), and [pāḡaʿ](../../strongs/h/h6293.md) to [Dabešeṯ](../../strongs/h/h1708.md), and [pāḡaʿ](../../strongs/h/h6293.md) to the [nachal](../../strongs/h/h5158.md) that is [paniym](../../strongs/h/h6440.md) [YāqnᵊʿĀm](../../strongs/h/h3362.md);

<a name="joshua_19_12"></a>Joshua 19:12

And [shuwb](../../strongs/h/h7725.md) from [Śārîḏ](../../strongs/h/h8301.md) [qeḏem](../../strongs/h/h6924.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md) unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Kislôṯ-Tāḇôr](../../strongs/h/h3696.md), and then [yāṣā'](../../strongs/h/h3318.md) to [Dāḇraṯ](../../strongs/h/h1705.md), and [ʿālâ](../../strongs/h/h5927.md) to [Yāp̄îaʿ](../../strongs/h/h3309.md),

<a name="joshua_19_13"></a>Joshua 19:13

And from thence ['abar](../../strongs/h/h5674.md) on the [mizrach](../../strongs/h/h4217.md) [qeḏem](../../strongs/h/h6924.md) to [Ḡṯ Ḥp̄R](../../strongs/h/h1662.md), to [ʿĒṯ Qāṣîn](../../strongs/h/h6278.md), and [yāṣā'](../../strongs/h/h3318.md) to [Rimmôn](../../strongs/h/h7417.md) to [NēʿÂ](../../strongs/h/h5269.md);

<a name="joshua_19_14"></a>Joshua 19:14

And the [gᵊḇûl](../../strongs/h/h1366.md) [cabab](../../strongs/h/h5437.md) it on the [ṣāp̄ôn](../../strongs/h/h6828.md) to [Ḥannāṯōn](../../strongs/h/h2615.md): and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof are in the [gay'](../../strongs/h/h1516.md) of [Yip̄Taḥ-'Ēl](../../strongs/h/h3317.md):

<a name="joshua_19_15"></a>Joshua 19:15

And [Qaṭṭaṯ](../../strongs/h/h7005.md), and [Nahălāl](../../strongs/h/h5096.md), and [šimrôn](../../strongs/h/h8110.md), and [Yiḏ'Ălâ](../../strongs/h/h3030.md), and [Bêṯ leḥem](../../strongs/h/h1035.md): [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_16"></a>Joshua 19:16

This is the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md) according to their [mišpāḥâ](../../strongs/h/h4940.md), these [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_17"></a>Joshua 19:17

And the [rᵊḇîʿî](../../strongs/h/h7243.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) to [Yiśśāśḵār](../../strongs/h/h3485.md), for the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_19_18"></a>Joshua 19:18

And their [gᵊḇûl](../../strongs/h/h1366.md) was toward [YizrᵊʿE'L](../../strongs/h/h3157.md), and [Kᵊsullôṯ](../../strongs/h/h3694.md), and [Šûnēm](../../strongs/h/h7766.md),

<a name="joshua_19_19"></a>Joshua 19:19

And [Ḥăp̄Ārayim](../../strongs/h/h2663.md), and [Šî'Ôn](../../strongs/h/h7866.md), and ['Ănāḥărāṯ](../../strongs/h/h588.md),

<a name="joshua_19_20"></a>Joshua 19:20

And [Rabîṯ](../../strongs/h/h7245.md), and [Qišyôn](../../strongs/h/h7191.md), and ['Eḇeṣ](../../strongs/h/h77.md),

<a name="joshua_19_21"></a>Joshua 19:21

And [Remeṯ](../../strongs/h/h7432.md), and [ʿÊn Ḡannîm](../../strongs/h/h5873.md), and [ʿÊn Ḥadâ](../../strongs/h/h5876.md), and [Bêṯ Paṣṣēṣ](../../strongs/h/h1048.md);

<a name="joshua_19_22"></a>Joshua 19:22

And the [gᵊḇûl](../../strongs/h/h1366.md) [pāḡaʿ](../../strongs/h/h6293.md) to [Tāḇôr](../../strongs/h/h8396.md), and [Šaḥăṣîmâ](../../strongs/h/h7831.md), and [Bêṯ Šemeš](../../strongs/h/h1053.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of their [gᵊḇûl](../../strongs/h/h1366.md) were at [Yardēn](../../strongs/h/h3383.md): [šēš](../../strongs/h/h8337.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_23"></a>Joshua 19:23

This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) according to their [mišpāḥâ](../../strongs/h/h4940.md), the [ʿîr](../../strongs/h/h5892.md) and their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_24"></a>Joshua 19:24

And the [ḥămîšî](../../strongs/h/h2549.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) for the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_19_25"></a>Joshua 19:25

And their [gᵊḇûl](../../strongs/h/h1366.md) was [Ḥelqāṯ](../../strongs/h/h2520.md), and [Ḥălî](../../strongs/h/h2482.md), and [Beṭen](../../strongs/h/h991.md), and ['Aḵšāp̄](../../strongs/h/h407.md),

<a name="joshua_19_26"></a>Joshua 19:26

And ['Allammeleḵ](../../strongs/h/h487.md), and [ʿAmʿĀḏ](../../strongs/h/h6008.md), and [Miš'Āl](../../strongs/h/h4861.md); and [pāḡaʿ](../../strongs/h/h6293.md) to [Karmel](../../strongs/h/h3760.md) [yam](../../strongs/h/h3220.md), and to [Šîḥôr Liḇnāṯ](../../strongs/h/h7884.md);

<a name="joshua_19_27"></a>Joshua 19:27

And [shuwb](../../strongs/h/h7725.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md) to [Bêṯ-Dāḡôn](../../strongs/h/h1016.md), and [pāḡaʿ](../../strongs/h/h6293.md) to [Zᵊḇûlûn](../../strongs/h/h2074.md), and to the [gay'](../../strongs/h/h1516.md) of [Yip̄Taḥ-'Ēl](../../strongs/h/h3317.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md) of [Bêṯ HāʿĒmeq](../../strongs/h/h1025.md), and [NᵊʿÎ'Ēl](../../strongs/h/h5272.md), and [yāṣā'](../../strongs/h/h3318.md) to [Kāḇûl](../../strongs/h/h3521.md) on the left [śᵊmō'l](../../strongs/h/h8040.md),

<a name="joshua_19_28"></a>Joshua 19:28

And [ʿEḇrōn](../../strongs/h/h5683.md), and [rᵊḥōḇ](../../strongs/h/h7340.md), and [Ḥammôn](../../strongs/h/h2540.md), and [Qānâ](../../strongs/h/h7071.md), even unto [rab](../../strongs/h/h7227.md) [Ṣîḏôn](../../strongs/h/h6721.md);

<a name="joshua_19_29"></a>Joshua 19:29

And then the [gᵊḇûl](../../strongs/h/h1366.md) [shuwb](../../strongs/h/h7725.md) to [rāmâ](../../strongs/h/h7414.md), and to the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) [Ṣōr](../../strongs/h/h6865.md); and the [gᵊḇûl](../../strongs/h/h1366.md) [shuwb](../../strongs/h/h7725.md) to [Ḥōsâ](../../strongs/h/h2621.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof are at the [yam](../../strongs/h/h3220.md) from the [chebel](../../strongs/h/h2256.md) to ['Aḵzîḇ](../../strongs/h/h392.md):

<a name="joshua_19_30"></a>Joshua 19:30

[ʿUmmâ](../../strongs/h/h5981.md) also, and ['Ăp̄Ēq](../../strongs/h/h663.md), and [rᵊḥōḇ](../../strongs/h/h7340.md): [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_31"></a>Joshua 19:31

This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Āšēr](../../strongs/h/h836.md) according to their [mišpāḥâ](../../strongs/h/h4940.md), these [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_32"></a>Joshua 19:32

The [šiššî](../../strongs/h/h8345.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) to the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md), even for the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_19_33"></a>Joshua 19:33

And their [gᵊḇûl](../../strongs/h/h1366.md) was from [Ḥēlep̄](../../strongs/h/h2501.md), from ['Allôn](../../strongs/h/h438.md) to [ṢaʿĂnannîm](../../strongs/h/h6815.md), and ['Ăḏāmî](../../strongs/h/h129.md), [Neqeḇ](../../strongs/h/h5346.md), and [Yaḇnᵊ'Ēl](../../strongs/h/h2995.md), unto [Laqqûm](../../strongs/h/h3946.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof were at [Yardēn](../../strongs/h/h3383.md):

<a name="joshua_19_34"></a>Joshua 19:34

And then the [gᵊḇûl](../../strongs/h/h1366.md) [shuwb](../../strongs/h/h7725.md) [yam](../../strongs/h/h3220.md) to ['Aznôṯ-Tāḇôr](../../strongs/h/h243.md), and [yāṣā'](../../strongs/h/h3318.md) from thence to [Ḥuqqōq](../../strongs/h/h2712.md), and [pāḡaʿ](../../strongs/h/h6293.md) to [Zᵊḇûlûn](../../strongs/h/h2074.md) on the [neḡeḇ](../../strongs/h/h5045.md), and [pāḡaʿ](../../strongs/h/h6293.md) to ['Āšēr](../../strongs/h/h836.md) on the [yam](../../strongs/h/h3220.md), and to [Yehuwdah](../../strongs/h/h3063.md) upon [Yardēn](../../strongs/h/h3383.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md).

<a name="joshua_19_35"></a>Joshua 19:35

And the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) are [Ṣidîm](../../strongs/h/h6661.md), [Ṣēr](../../strongs/h/h6863.md), and [Ḥammaṯ](../../strongs/h/h2575.md), [Raqqaṯ](../../strongs/h/h7557.md), and [Kinnᵊrôṯ](../../strongs/h/h3672.md),

<a name="joshua_19_36"></a>Joshua 19:36

And ['Ăḏāmâ](../../strongs/h/h128.md), and [rāmâ](../../strongs/h/h7414.md), and [Ḥāṣôr](../../strongs/h/h2674.md),

<a name="joshua_19_37"></a>Joshua 19:37

And [Qeḏeš](../../strongs/h/h6943.md), and ['Eḏrᵊʿî](../../strongs/h/h154.md), and [ʿÊn Ḥāṣôr](../../strongs/h/h5877.md),

<a name="joshua_19_38"></a>Joshua 19:38

And [Yir'Ôn](../../strongs/h/h3375.md), and [Miḡdal-'Ēl](../../strongs/h/h4027.md), [Ḥārēm](../../strongs/h/h2765.md), and [Bêṯ-ʿĂnāṯ](../../strongs/h/h1043.md), and [Bêṯ Šemeš](../../strongs/h/h1053.md); [tēšaʿ](../../strongs/h/h8672.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_39"></a>Joshua 19:39

This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) according to their [mišpāḥâ](../../strongs/h/h4940.md), the [ʿîr](../../strongs/h/h5892.md) and their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_40"></a>Joshua 19:40

And the [šᵊḇîʿî](../../strongs/h/h7637.md) [gôrāl](../../strongs/h/h1486.md) [yāṣā'](../../strongs/h/h3318.md) for the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_19_41"></a>Joshua 19:41

And the [gᵊḇûl](../../strongs/h/h1366.md) of their [nachalah](../../strongs/h/h5159.md) was [ṢārʿÂ](../../strongs/h/h6881.md), and ['Eštā'Ōl](../../strongs/h/h847.md), and [ʿÎr Šemeš](../../strongs/h/h5905.md),

<a name="joshua_19_42"></a>Joshua 19:42

And [ŠaʿAlḇîm](../../strongs/h/h8169.md), and ['Ayyālôn](../../strongs/h/h357.md), and [Yiṯlâ](../../strongs/h/h3494.md),

<a name="joshua_19_43"></a>Joshua 19:43

And ['êlôn](../../strongs/h/h356.md), and [Timnâ](../../strongs/h/h8553.md), and [ʿEqrôn](../../strongs/h/h6138.md),

<a name="joshua_19_44"></a>Joshua 19:44

And ['Eltᵊqē'](../../strongs/h/h514.md), and [Gibṯôn](../../strongs/h/h1405.md), and [BaʿĂlāṯ](../../strongs/h/h1191.md),

<a name="joshua_19_45"></a>Joshua 19:45

And [Yᵊhûḏ](../../strongs/h/h3055.md), and [Bᵊnê-Ḇᵊraq](../../strongs/h/h1139.md), and [Gaṯ-Rimmôn](../../strongs/h/h1667.md),

<a name="joshua_19_46"></a>Joshua 19:46

And [Mê Hayyarqôn](../../strongs/h/h4313.md), and [Raqqôn](../../strongs/h/h7542.md), with the [gᵊḇûl](../../strongs/h/h1366.md) [môl](../../strongs/h/h4136.md) [Yāp̄Vô](../../strongs/h/h3305.md).

<a name="joshua_19_47"></a>Joshua 19:47

And the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [yāṣā'](../../strongs/h/h3318.md) too little for them: therefore the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [ʿālâ](../../strongs/h/h5927.md) to [lāḥam](../../strongs/h/h3898.md) against [Lešem](../../strongs/h/h3959.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [yarash](../../strongs/h/h3423.md) it, and [yashab](../../strongs/h/h3427.md) therein, and [qara'](../../strongs/h/h7121.md) [Lešem](../../strongs/h/h3959.md), [Dān](../../strongs/h/h1835.md), after the [shem](../../strongs/h/h8034.md) of [Dān](../../strongs/h/h1835.md) their ['ab](../../strongs/h/h1.md).

<a name="joshua_19_48"></a>Joshua 19:48

This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) according to their [mišpāḥâ](../../strongs/h/h4940.md), these [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_19_49"></a>Joshua 19:49

When they had made a [kalah](../../strongs/h/h3615.md) of dividing the ['erets](../../strongs/h/h776.md) for [nāḥal](../../strongs/h/h5157.md) by their [gᵊḇûlâ](../../strongs/h/h1367.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) a [nachalah](../../strongs/h/h5159.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) [tavek](../../strongs/h/h8432.md) them:

<a name="joshua_19_50"></a>Joshua 19:50

According to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) they [nathan](../../strongs/h/h5414.md) him the [ʿîr](../../strongs/h/h5892.md) which he [sha'al](../../strongs/h/h7592.md), even [Timnaṯ-Ḥeres](../../strongs/h/h8556.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md): and he [bānâ](../../strongs/h/h1129.md) the [ʿîr](../../strongs/h/h5892.md), and [yashab](../../strongs/h/h3427.md) therein.

<a name="joshua_19_51"></a>Joshua 19:51

These are the [nachalah](../../strongs/h/h5159.md), which ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), divided for a [nāḥal](../../strongs/h/h5157.md) by [gôrāl](../../strongs/h/h1486.md) in [Šîlô](../../strongs/h/h7887.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md). So they made a [kalah](../../strongs/h/h3615.md) of [chalaq](../../strongs/h/h2505.md) the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 18](joshua_18.md) - [Joshua 20](joshua_20.md)