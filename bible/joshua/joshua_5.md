# [Joshua 5](https://www.blueletterbible.org/kjv/joshua/5)

<a name="joshua_5_1"></a>Joshua 5:1

And it came to pass, when all the [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which were on the [ʿēḇer](../../strongs/h/h5676.md) of [Yardēn](../../strongs/h/h3383.md) [yam](../../strongs/h/h3220.md), and all the [melek](../../strongs/h/h4428.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), which were by the [yam](../../strongs/h/h3220.md), [shama'](../../strongs/h/h8085.md) that [Yĕhovah](../../strongs/h/h3068.md) had [yāḇēš](../../strongs/h/h3001.md) the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) from [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), until we were ['abar](../../strongs/h/h5674.md), that their [lebab](../../strongs/h/h3824.md) [māsas](../../strongs/h/h4549.md), neither was there [ruwach](../../strongs/h/h7307.md) in them any more, [paniym](../../strongs/h/h6440.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_5_2"></a>Joshua 5:2

At that [ʿēṯ](../../strongs/h/h6256.md) [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['asah](../../strongs/h/h6213.md) thee [tsuwr](../../strongs/h/h6697.md) [chereb](../../strongs/h/h2719.md), and [muwl](../../strongs/h/h4135.md) [shuwb](../../strongs/h/h7725.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) the [šēnî](../../strongs/h/h8145.md).

<a name="joshua_5_3"></a>Joshua 5:3

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) him [tsuwr](../../strongs/h/h6697.md) [chereb](../../strongs/h/h2719.md), and [muwl](../../strongs/h/h4135.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) at the [giḇʿâ](../../strongs/h/h1389.md) of the [ʿārlâ](../../strongs/h/h6190.md).

<a name="joshua_5_4"></a>Joshua 5:4

And this is the [dabar](../../strongs/h/h1697.md) why [Yᵊhôšûaʿ](../../strongs/h/h3091.md) did [muwl](../../strongs/h/h4135.md): All the ['am](../../strongs/h/h5971.md) that [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md), that were [zāḵār](../../strongs/h/h2145.md), even all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), [muwth](../../strongs/h/h4191.md) in the [midbar](../../strongs/h/h4057.md) by the [derek](../../strongs/h/h1870.md), after they [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="joshua_5_5"></a>Joshua 5:5

[kî](../../strongs/h/h3588.md) all the ['am](../../strongs/h/h5971.md) that [yāṣā'](../../strongs/h/h3318.md) were [muwl](../../strongs/h/h4135.md): but all the ['am](../../strongs/h/h5971.md) that were [yillôḏ](../../strongs/h/h3209.md) in the [midbar](../../strongs/h/h4057.md) by the [derek](../../strongs/h/h1870.md) as they [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md), them they had not [muwl](../../strongs/h/h4135.md).

<a name="joshua_5_6"></a>Joshua 5:6

For the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [halak](../../strongs/h/h1980.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md) in the [midbar](../../strongs/h/h4057.md), till all the [gowy](../../strongs/h/h1471.md) that were ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), which [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md), were [tamam](../../strongs/h/h8552.md), because they [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md): unto whom [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) that he would not [ra'ah](../../strongs/h/h7200.md) them the ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md) that he would [nathan](../../strongs/h/h5414.md) us, an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md).

<a name="joshua_5_7"></a>Joshua 5:7

And their [ben](../../strongs/h/h1121.md), whom he [quwm](../../strongs/h/h6965.md) in their stead, them [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [muwl](../../strongs/h/h4135.md): for they were [ʿārēl](../../strongs/h/h6189.md), because they had not [muwl](../../strongs/h/h4135.md) them by the [derek](../../strongs/h/h1870.md).

<a name="joshua_5_8"></a>Joshua 5:8

And it came to pass, when they had [tamam](../../strongs/h/h8552.md) [muwl](../../strongs/h/h4135.md) all the [gowy](../../strongs/h/h1471.md), that they [yashab](../../strongs/h/h3427.md) in their places in the [maḥănê](../../strongs/h/h4264.md), till they were [ḥāyâ](../../strongs/h/h2421.md).

<a name="joshua_5_9"></a>Joshua 5:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), This [yowm](../../strongs/h/h3117.md) have I [gālal](../../strongs/h/h1556.md) the [cherpah](../../strongs/h/h2781.md) of [Mitsrayim](../../strongs/h/h4714.md) from off you. Wherefore the [shem](../../strongs/h/h8034.md) of the [maqowm](../../strongs/h/h4725.md) is [qara'](../../strongs/h/h7121.md) [Gilgāl](../../strongs/h/h1537.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_5_10"></a>Joshua 5:10

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥānâ](../../strongs/h/h2583.md) in [Gilgāl](../../strongs/h/h1537.md), and ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) on the ['arbaʿ](../../strongs/h/h702.md) [ʿeśer](../../strongs/h/h6240.md) [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md) in the ['arabah](../../strongs/h/h6160.md) of [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_5_11"></a>Joshua 5:11

And they did ['akal](../../strongs/h/h398.md) of the [ʿāḇûr](../../strongs/h/h5669.md) of the ['erets](../../strongs/h/h776.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md) after the [pecach](../../strongs/h/h6453.md), [maṣṣâ](../../strongs/h/h4682.md), and [qālâ](../../strongs/h/h7033.md) corn in the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_5_12"></a>Joshua 5:12

And the [man](../../strongs/h/h4478.md) [shabath](../../strongs/h/h7673.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md) after they had ['akal](../../strongs/h/h398.md) of the [ʿāḇûr](../../strongs/h/h5669.md) of the ['erets](../../strongs/h/h776.md); neither had the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [man](../../strongs/h/h4478.md) any more; but they did ['akal](../../strongs/h/h398.md) of the [tᵊḇû'â](../../strongs/h/h8393.md) of the ['erets](../../strongs/h/h776.md) of [kĕna'an](../../strongs/h/h3667.md) that [šānâ](../../strongs/h/h8141.md).

<a name="joshua_5_13"></a>Joshua 5:13

And it came to pass, when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) was by [Yᵊrēḥô](../../strongs/h/h3405.md), that he [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md) and [ra'ah](../../strongs/h/h7200.md), and, behold, there ['amad](../../strongs/h/h5975.md) an ['iysh](../../strongs/h/h376.md) over against him with his [chereb](../../strongs/h/h2719.md) [šālap̄](../../strongs/h/h8025.md) in his [yad](../../strongs/h/h3027.md): and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [yālaḵ](../../strongs/h/h3212.md) unto him, and ['āmar](../../strongs/h/h559.md) unto him, Art thou for us, or for our [tsar](../../strongs/h/h6862.md)?

<a name="joshua_5_14"></a>Joshua 5:14

And he ['āmar](../../strongs/h/h559.md), Nay; but as [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [Yĕhovah](../../strongs/h/h3068.md) am I now [bow'](../../strongs/h/h935.md). And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [naphal](../../strongs/h/h5307.md) on his [paniym](../../strongs/h/h6440.md) to the ['erets](../../strongs/h/h776.md), and did [shachah](../../strongs/h/h7812.md), and ['āmar](../../strongs/h/h559.md) unto him, What [dabar](../../strongs/h/h1696.md) my ['adown](../../strongs/h/h113.md) unto his ['ebed](../../strongs/h/h5650.md)?

<a name="joshua_5_15"></a>Joshua 5:15

And the [śar](../../strongs/h/h8269.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsaba'](../../strongs/h/h6635.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [nāšal](../../strongs/h/h5394.md) thy [naʿal](../../strongs/h/h5275.md) from off thy [regel](../../strongs/h/h7272.md); for the [maqowm](../../strongs/h/h4725.md) whereon thou ['amad](../../strongs/h/h5975.md) is [qodesh](../../strongs/h/h6944.md). And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) so.

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 4](joshua_4.md) - [Joshua 6](joshua_6.md)