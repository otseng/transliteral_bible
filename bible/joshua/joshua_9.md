# [Joshua 9](https://www.blueletterbible.org/kjv/joshua/9)

<a name="joshua_9_1"></a>Joshua 9:1

And it came to pass, when all the [melek](../../strongs/h/h4428.md) which were on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), in the [har](../../strongs/h/h2022.md), and in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in all the [ḥôp̄](../../strongs/h/h2348.md) of the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) over [môl](../../strongs/h/h4136.md) [Lᵊḇānôn](../../strongs/h/h3844.md), the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [Pᵊrizzî](../../strongs/h/h6522.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), [shama'](../../strongs/h/h8085.md) thereof;

<a name="joshua_9_2"></a>Joshua 9:2

That they [qāḇaṣ](../../strongs/h/h6908.md) themselves [yaḥaḏ](../../strongs/h/h3162.md), to [lāḥam](../../strongs/h/h3898.md) with [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and with [Yisra'el](../../strongs/h/h3478.md), with ['echad](../../strongs/h/h259.md) [peh](../../strongs/h/h6310.md).

<a name="joshua_9_3"></a>Joshua 9:3

And when the [yashab](../../strongs/h/h3427.md) of [Giḇʿôn](../../strongs/h/h1391.md) [shama'](../../strongs/h/h8085.md) what [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had ['asah](../../strongs/h/h6213.md) unto [Yᵊrēḥô](../../strongs/h/h3405.md) and to [ʿAy](../../strongs/h/h5857.md),

<a name="joshua_9_4"></a>Joshua 9:4

They did ['asah](../../strongs/h/h6213.md) [ʿārmâ](../../strongs/h/h6195.md), and [yālaḵ](../../strongs/h/h3212.md) and made as if they had been [ṣāyar](../../strongs/h/h6737.md), and [laqach](../../strongs/h/h3947.md) [bālê](../../strongs/h/h1087.md) [śaq](../../strongs/h/h8242.md) upon their [chamowr](../../strongs/h/h2543.md), and [yayin](../../strongs/h/h3196.md) [nō'ḏ](../../strongs/h/h4997.md), [bālê](../../strongs/h/h1087.md), and [bāqaʿ](../../strongs/h/h1234.md), and [tsarar](../../strongs/h/h6887.md);

<a name="joshua_9_5"></a>Joshua 9:5

And [bālê](../../strongs/h/h1087.md) [naʿal](../../strongs/h/h5275.md) and [ṭālā'](../../strongs/h/h2921.md) upon their [regel](../../strongs/h/h7272.md), and [bālê](../../strongs/h/h1087.md) [śalmâ](../../strongs/h/h8008.md) upon them; and all the [lechem](../../strongs/h/h3899.md) of their [ṣayiḏ](../../strongs/h/h6718.md) was [yāḇēš](../../strongs/h/h3001.md) and [niqquḏ](../../strongs/h/h5350.md).

<a name="joshua_9_6"></a>Joshua 9:6

And they [yālaḵ](../../strongs/h/h3212.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) unto the [maḥănê](../../strongs/h/h4264.md) at [Gilgāl](../../strongs/h/h1537.md), and ['āmar](../../strongs/h/h559.md) unto him, and to the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), We be [bow'](../../strongs/h/h935.md) from a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md): now therefore [karath](../../strongs/h/h3772.md) ye a [bĕriyth](../../strongs/h/h1285.md) with us.

<a name="joshua_9_7"></a>Joshua 9:7

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto the [Ḥiûî](../../strongs/h/h2340.md), ['ûlay](../../strongs/h/h194.md) ye [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) us; and how shall we [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with you?

<a name="joshua_9_8"></a>Joshua 9:8

And they ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), We are thy ['ebed](../../strongs/h/h5650.md). And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto them, Who are ye? and from ['ayin](../../strongs/h/h370.md) [bow'](../../strongs/h/h935.md) ye?

<a name="joshua_9_9"></a>Joshua 9:9

And they ['āmar](../../strongs/h/h559.md) unto him, From a [me'od](../../strongs/h/h3966.md) [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md) thy ['ebed](../../strongs/h/h5650.md) are [bow'](../../strongs/h/h935.md) because of the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): for we have [shama'](../../strongs/h/h8085.md) the [šōmaʿ](../../strongs/h/h8089.md) of him, and all that he ['asah](../../strongs/h/h6213.md) in [Mitsrayim](../../strongs/h/h4714.md),

<a name="joshua_9_10"></a>Joshua 9:10

And all that he ['asah](../../strongs/h/h6213.md) to the [šᵊnayim](../../strongs/h/h8147.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), that were [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), to [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), and to [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), which was at [ʿAštārōṯ](../../strongs/h/h6252.md).

<a name="joshua_9_11"></a>Joshua 9:11

Wherefore our [zāqēn](../../strongs/h/h2205.md) and all the [yashab](../../strongs/h/h3427.md) of our ['erets](../../strongs/h/h776.md) ['āmar](../../strongs/h/h559.md) to us, ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) [ṣêḏâ](../../strongs/h/h6720.md) with [yad](../../strongs/h/h3027.md) for the [derek](../../strongs/h/h1870.md), and [yālaḵ](../../strongs/h/h3212.md) to [qārā'](../../strongs/h/h7125.md) them, and ['āmar](../../strongs/h/h559.md) unto them, We are your ['ebed](../../strongs/h/h5650.md): therefore now [karath](../../strongs/h/h3772.md) ye a [bĕriyth](../../strongs/h/h1285.md) with us.

<a name="joshua_9_12"></a>Joshua 9:12

This our [lechem](../../strongs/h/h3899.md) we [ḥām](../../strongs/h/h2525.md) for our [ṣûḏ](../../strongs/h/h6679.md) out of our [bayith](../../strongs/h/h1004.md) on the [yowm](../../strongs/h/h3117.md) we [yāṣā'](../../strongs/h/h3318.md) to [yālaḵ](../../strongs/h/h3212.md) unto you; but now, behold, it is [yāḇēš](../../strongs/h/h3001.md), and it is [niqquḏ](../../strongs/h/h5350.md):

<a name="joshua_9_13"></a>Joshua 9:13

And these [nō'ḏ](../../strongs/h/h4997.md) of [yayin](../../strongs/h/h3196.md), which we [mālā'](../../strongs/h/h4390.md), were [ḥāḏāš](../../strongs/h/h2319.md); and, behold, they be [bāqaʿ](../../strongs/h/h1234.md): and these our [śalmâ](../../strongs/h/h8008.md) and our [naʿal](../../strongs/h/h5275.md) are become [bālâ](../../strongs/h/h1086.md) by reason of the [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md) [derek](../../strongs/h/h1870.md).

<a name="joshua_9_14"></a>Joshua 9:14

And the ['enowsh](../../strongs/h/h582.md) [laqach](../../strongs/h/h3947.md) of their [ṣayiḏ](../../strongs/h/h6718.md), and [sha'al](../../strongs/h/h7592.md) not counsel at the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_9_15"></a>Joshua 9:15

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['asah](../../strongs/h/h6213.md) [shalowm](../../strongs/h/h7965.md) with them, and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with them, to let them [ḥāyâ](../../strongs/h/h2421.md): and the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) [shaba'](../../strongs/h/h7650.md) unto them.

<a name="joshua_9_16"></a>Joshua 9:16

And it came to pass at the [qāṣê](../../strongs/h/h7097.md) of [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md) ['aḥar](../../strongs/h/h310.md) they had [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with them, that they [shama'](../../strongs/h/h8085.md) that they were their [qarowb](../../strongs/h/h7138.md), and that they [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) them.

<a name="joshua_9_17"></a>Joshua 9:17

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāsaʿ](../../strongs/h/h5265.md), and [bow'](../../strongs/h/h935.md) unto their [ʿîr](../../strongs/h/h5892.md) on the [šᵊlîšî](../../strongs/h/h7992.md) [yowm](../../strongs/h/h3117.md). Now their [ʿîr](../../strongs/h/h5892.md) were [Giḇʿôn](../../strongs/h/h1391.md), and [Kᵊp̄Îrâ](../../strongs/h/h3716.md), and [Bᵊ'Ērôṯ](../../strongs/h/h881.md), and [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md).

<a name="joshua_9_18"></a>Joshua 9:18

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md) them not, because the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) had [shaba'](../../strongs/h/h7650.md) unto them by [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md). And all the ['edah](../../strongs/h/h5712.md) [lûn](../../strongs/h/h3885.md) against the [nāśî'](../../strongs/h/h5387.md).

<a name="joshua_9_19"></a>Joshua 9:19

But all the [nāśî'](../../strongs/h/h5387.md) ['āmar](../../strongs/h/h559.md) unto all the ['edah](../../strongs/h/h5712.md), We have [shaba'](../../strongs/h/h7650.md) unto them by [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md): now therefore we [yakol](../../strongs/h/h3201.md) not [naga'](../../strongs/h/h5060.md) them.

<a name="joshua_9_20"></a>Joshua 9:20

This we will ['asah](../../strongs/h/h6213.md) to them; we will even let them [ḥāyâ](../../strongs/h/h2421.md), lest [qeṣep̄](../../strongs/h/h7110.md) be upon us, because of the [šᵊḇûʿâ](../../strongs/h/h7621.md) which we [shaba'](../../strongs/h/h7650.md) unto them.

<a name="joshua_9_21"></a>Joshua 9:21

And the [nāśî'](../../strongs/h/h5387.md) ['āmar](../../strongs/h/h559.md) unto them, Let them [ḥāyâ](../../strongs/h/h2421.md); but let them be [ḥāṭaḇ](../../strongs/h/h2404.md) of ['ets](../../strongs/h/h6086.md) and [šā'aḇ](../../strongs/h/h7579.md) of [mayim](../../strongs/h/h4325.md) unto all the ['edah](../../strongs/h/h5712.md); as the [nāśî'](../../strongs/h/h5387.md) had [dabar](../../strongs/h/h1696.md) them.

<a name="joshua_9_22"></a>Joshua 9:22

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) for them, and he [dabar](../../strongs/h/h1696.md) unto them, ['āmar](../../strongs/h/h559.md), Wherefore have ye [rāmâ](../../strongs/h/h7411.md) us, ['āmar](../../strongs/h/h559.md), We are [me'od](../../strongs/h/h3966.md) [rachowq](../../strongs/h/h7350.md) from you; when ye [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) us?

<a name="joshua_9_23"></a>Joshua 9:23

Now therefore ye are ['arar](../../strongs/h/h779.md), and there shall none of you be [karath](../../strongs/h/h3772.md) from being ['ebed](../../strongs/h/h5650.md), and [ḥāṭaḇ](../../strongs/h/h2404.md) of ['ets](../../strongs/h/h6086.md) and [šā'aḇ](../../strongs/h/h7579.md) of [mayim](../../strongs/h/h4325.md) for the [bayith](../../strongs/h/h1004.md) of my ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_9_24"></a>Joshua 9:24

And they ['anah](../../strongs/h/h6030.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and ['āmar](../../strongs/h/h559.md), Because it was [nāḡaḏ](../../strongs/h/h5046.md) [nāḡaḏ](../../strongs/h/h5046.md) thy ['ebed](../../strongs/h/h5650.md), how that [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [tsavah](../../strongs/h/h6680.md) his ['ebed](../../strongs/h/h5650.md) [Mōshe](../../strongs/h/h4872.md) to [nathan](../../strongs/h/h5414.md) you all the ['erets](../../strongs/h/h776.md), and to [šāmaḏ](../../strongs/h/h8045.md) all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) from [paniym](../../strongs/h/h6440.md) you, therefore we were [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md) of our [nephesh](../../strongs/h/h5315.md) [paniym](../../strongs/h/h6440.md) of you, and have ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="joshua_9_25"></a>Joshua 9:25

And now, [hinneh](../../strongs/h/h2009.md), we are in thine [yad](../../strongs/h/h3027.md): as it ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) unto thee to ['asah](../../strongs/h/h6213.md) unto us, ['asah](../../strongs/h/h6213.md).

<a name="joshua_9_26"></a>Joshua 9:26

And so ['asah](../../strongs/h/h6213.md) he unto them, and [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [harag](../../strongs/h/h2026.md) them not.

<a name="joshua_9_27"></a>Joshua 9:27

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nathan](../../strongs/h/h5414.md) them that [yowm](../../strongs/h/h3117.md) [ḥāṭaḇ](../../strongs/h/h2404.md) of ['ets](../../strongs/h/h6086.md) and [šā'aḇ](../../strongs/h/h7579.md) of [mayim](../../strongs/h/h4325.md) for the ['edah](../../strongs/h/h5712.md), and for the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), even unto this [yowm](../../strongs/h/h3117.md), in the [maqowm](../../strongs/h/h4725.md) which he should [bāḥar](../../strongs/h/h977.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 8](joshua_8.md) - [Joshua 10](joshua_10.md)