# [Joshua 10](https://www.blueletterbible.org/kjv/joshua/10)

<a name="joshua_10_1"></a>Joshua 10:1

Now it came to pass, when ['Ăḏōnî-ṣeḏeq](../../strongs/h/h139.md) [melek](../../strongs/h/h4428.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) had [shama'](../../strongs/h/h8085.md) [kî](../../strongs/h/h3588.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had [lāḵaḏ](../../strongs/h/h3920.md) [ʿAy](../../strongs/h/h5857.md), and had [ḥāram](../../strongs/h/h2763.md) it; as he had ['asah](../../strongs/h/h6213.md) to [Yᵊrēḥô](../../strongs/h/h3405.md) and her [melek](../../strongs/h/h4428.md), so he had ['asah](../../strongs/h/h6213.md) to [ʿAy](../../strongs/h/h5857.md) and her [melek](../../strongs/h/h4428.md); and how the [yashab](../../strongs/h/h3427.md) of [Giḇʿôn](../../strongs/h/h1391.md) had made [shalam](../../strongs/h/h7999.md) with [Yisra'el](../../strongs/h/h3478.md), and were [qereḇ](../../strongs/h/h7130.md) them;

<a name="joshua_10_2"></a>Joshua 10:2

That they [yare'](../../strongs/h/h3372.md) [me'od](../../strongs/h/h3966.md), because [Giḇʿôn](../../strongs/h/h1391.md) was a [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md), as ['echad](../../strongs/h/h259.md) of the [mamlāḵâ](../../strongs/h/h4467.md) [ʿîr](../../strongs/h/h5892.md), and because it was [gadowl](../../strongs/h/h1419.md) than [ʿAy](../../strongs/h/h5857.md), and all the ['enowsh](../../strongs/h/h582.md) thereof were [gibôr](../../strongs/h/h1368.md).

<a name="joshua_10_3"></a>Joshua 10:3

Wherefore ['Ăḏōnî-ṣeḏeq](../../strongs/h/h139.md) [melek](../../strongs/h/h4428.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [shalach](../../strongs/h/h7971.md) unto [hôhām](../../strongs/h/h1944.md) [melek](../../strongs/h/h4428.md) of [Ḥeḇrôn](../../strongs/h/h2275.md), and unto [pir'ām](../../strongs/h/h6502.md) [melek](../../strongs/h/h4428.md) of [Yarmûṯ](../../strongs/h/h3412.md), and unto [Yāp̄îaʿ](../../strongs/h/h3309.md) [melek](../../strongs/h/h4428.md) of [Lāḵîš](../../strongs/h/h3923.md), and unto [dᵊḇîr](../../strongs/h/h1688.md) [melek](../../strongs/h/h4428.md) of [ʿEḡlôn](../../strongs/h/h5700.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_10_4"></a>Joshua 10:4

[ʿālâ](../../strongs/h/h5927.md) unto me, and [ʿāzar](../../strongs/h/h5826.md) me, that we may [nakah](../../strongs/h/h5221.md) [Giḇʿôn](../../strongs/h/h1391.md): for it hath made [shalam](../../strongs/h/h7999.md) with [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_10_5"></a>Joshua 10:5

Therefore the [ḥāmēš](../../strongs/h/h2568.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), the [melek](../../strongs/h/h4428.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [melek](../../strongs/h/h4428.md) of [Ḥeḇrôn](../../strongs/h/h2275.md), the [melek](../../strongs/h/h4428.md) of [Yarmûṯ](../../strongs/h/h3412.md), the [melek](../../strongs/h/h4428.md) of [Lāḵîš](../../strongs/h/h3923.md), the [melek](../../strongs/h/h4428.md) of [ʿEḡlôn](../../strongs/h/h5700.md), gathered themselves ['āsap̄](../../strongs/h/h622.md), and [ʿālâ](../../strongs/h/h5927.md), they and all their [maḥănê](../../strongs/h/h4264.md), and [ḥānâ](../../strongs/h/h2583.md) before [Giḇʿôn](../../strongs/h/h1391.md), and made [lāḥam](../../strongs/h/h3898.md) against it.

<a name="joshua_10_6"></a>Joshua 10:6

And the ['enowsh](../../strongs/h/h582.md) of [Giḇʿôn](../../strongs/h/h1391.md) [shalach](../../strongs/h/h7971.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md) to the [maḥănê](../../strongs/h/h4264.md) to [Gilgāl](../../strongs/h/h1537.md), ['āmar](../../strongs/h/h559.md), [rāp̄â](../../strongs/h/h7503.md) not thy [yad](../../strongs/h/h3027.md) from thy ['ebed](../../strongs/h/h5650.md); [ʿālâ](../../strongs/h/h5927.md) to us [mᵊhērâ](../../strongs/h/h4120.md), and [yasha'](../../strongs/h/h3467.md) us, and [ʿāzar](../../strongs/h/h5826.md) us: for all the [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md) that [yashab](../../strongs/h/h3427.md) in the [har](../../strongs/h/h2022.md) are [qāḇaṣ](../../strongs/h/h6908.md) against us.

<a name="joshua_10_7"></a>Joshua 10:7

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [ʿālâ](../../strongs/h/h5927.md) from [Gilgāl](../../strongs/h/h1537.md), he, and all the ['am](../../strongs/h/h5971.md) of [milḥāmâ](../../strongs/h/h4421.md) with him, and all the [gibôr](../../strongs/h/h1368.md) men of [ḥayil](../../strongs/h/h2428.md).

<a name="joshua_10_8"></a>Joshua 10:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [yare'](../../strongs/h/h3372.md) them not: for I have [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md); there shall not an ['iysh](../../strongs/h/h376.md) of them ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="joshua_10_9"></a>Joshua 10:9

[Yᵊhôšûaʿ](../../strongs/h/h3091.md) therefore [bow'](../../strongs/h/h935.md) unto them [piṯ'ōm](../../strongs/h/h6597.md), and [ʿālâ](../../strongs/h/h5927.md) from [Gilgāl](../../strongs/h/h1537.md) all [layil](../../strongs/h/h3915.md).

<a name="joshua_10_10"></a>Joshua 10:10

And [Yĕhovah](../../strongs/h/h3068.md) [hāmam](../../strongs/h/h2000.md) them [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), and [nakah](../../strongs/h/h5221.md) them with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md) at [Giḇʿôn](../../strongs/h/h1391.md), and [radaph](../../strongs/h/h7291.md) them along the [derek](../../strongs/h/h1870.md) that goeth [maʿălê](../../strongs/h/h4608.md) to [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md), and [nakah](../../strongs/h/h5221.md) them to [ʿĂzēqâ](../../strongs/h/h5825.md), and unto [Maqqēḏâ](../../strongs/h/h4719.md).

<a name="joshua_10_11"></a>Joshua 10:11

And it came to pass, as they [nûs](../../strongs/h/h5127.md) from [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), and were in the [môrāḏ](../../strongs/h/h4174.md) to [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md), that [Yĕhovah](../../strongs/h/h3068.md) [shalak](../../strongs/h/h7993.md) [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) from [shamayim](../../strongs/h/h8064.md) upon them unto [ʿĂzēqâ](../../strongs/h/h5825.md), and they [muwth](../../strongs/h/h4191.md): they were [rab](../../strongs/h/h7227.md) which [muwth](../../strongs/h/h4191.md) with [barad](../../strongs/h/h1259.md) ['eben](../../strongs/h/h68.md) than they ['ăšer](../../strongs/h/h834.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md).

<a name="joshua_10_12"></a>Joshua 10:12

Then [dabar](../../strongs/h/h1696.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) to [Yĕhovah](../../strongs/h/h3068.md) in the [yowm](../../strongs/h/h3117.md) when [Yĕhovah](../../strongs/h/h3068.md) delivered [nathan](../../strongs/h/h5414.md) the ['Ĕmōrî](../../strongs/h/h567.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and he ['āmar](../../strongs/h/h559.md) in the ['ayin](../../strongs/h/h5869.md) of [Yisra'el](../../strongs/h/h3478.md), [šemeš](../../strongs/h/h8121.md), stand thou [damam](../../strongs/h/h1826.md) upon [Giḇʿôn](../../strongs/h/h1391.md); and thou, [yareach](../../strongs/h/h3394.md), in the [ʿēmeq](../../strongs/h/h6010.md) of ['Ayyālôn](../../strongs/h/h357.md).

<a name="joshua_10_13"></a>Joshua 10:13

And the [šemeš](../../strongs/h/h8121.md) [damam](../../strongs/h/h1826.md), and the [yareach](../../strongs/h/h3394.md) ['amad](../../strongs/h/h5975.md), until the [gowy](../../strongs/h/h1471.md) had [naqam](../../strongs/h/h5358.md) themselves upon their ['oyeb](../../strongs/h/h341.md). Is not [hû'](../../strongs/h/h1931.md) [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of [yashar](../../strongs/h/h3477.md)? So the [šemeš](../../strongs/h/h8121.md) ['amad](../../strongs/h/h5975.md) in the [ḥēṣî](../../strongs/h/h2677.md) of [shamayim](../../strongs/h/h8064.md), and ['ûṣ](../../strongs/h/h213.md) not to go [bow'](../../strongs/h/h935.md) about a [tamiym](../../strongs/h/h8549.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_10_14"></a>Joshua 10:14

And there was no [yowm](../../strongs/h/h3117.md) like that [paniym](../../strongs/h/h6440.md) it or ['aḥar](../../strongs/h/h310.md) it, that [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) unto the [qowl](../../strongs/h/h6963.md) of an ['iysh](../../strongs/h/h376.md): for [Yĕhovah](../../strongs/h/h3068.md) [lāḥam](../../strongs/h/h3898.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_10_15"></a>Joshua 10:15

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shuwb](../../strongs/h/h7725.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, unto the [maḥănê](../../strongs/h/h4264.md) to [Gilgāl](../../strongs/h/h1537.md).

<a name="joshua_10_16"></a>Joshua 10:16

But these [ḥāmēš](../../strongs/h/h2568.md) [melek](../../strongs/h/h4428.md) [nûs](../../strongs/h/h5127.md), and [chaba'](../../strongs/h/h2244.md) themselves in a [mᵊʿārâ](../../strongs/h/h4631.md) at [Maqqēḏâ](../../strongs/h/h4719.md).

<a name="joshua_10_17"></a>Joshua 10:17

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md), The [ḥāmēš](../../strongs/h/h2568.md) [melek](../../strongs/h/h4428.md) are [māṣā'](../../strongs/h/h4672.md) [chaba'](../../strongs/h/h2244.md) in a [mᵊʿārâ](../../strongs/h/h4631.md) at [Maqqēḏâ](../../strongs/h/h4719.md).

<a name="joshua_10_18"></a>Joshua 10:18

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md), [gālal](../../strongs/h/h1556.md) [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) upon the [peh](../../strongs/h/h6310.md) of the [mᵊʿārâ](../../strongs/h/h4631.md), and [paqad](../../strongs/h/h6485.md) ['enowsh](../../strongs/h/h582.md) by it for to [shamar](../../strongs/h/h8104.md) them:

<a name="joshua_10_19"></a>Joshua 10:19

And ['amad](../../strongs/h/h5975.md) ye not, but [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) your ['oyeb](../../strongs/h/h341.md), and smite the [zānāḇ](../../strongs/h/h2179.md) of them; [nathan](../../strongs/h/h5414.md) them not to [bow'](../../strongs/h/h935.md) into their [ʿîr](../../strongs/h/h5892.md): for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) them into your [yad](../../strongs/h/h3027.md).

<a name="joshua_10_20"></a>Joshua 10:20

And it came to pass, when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) had made a [kalah](../../strongs/h/h3615.md) of [nakah](../../strongs/h/h5221.md) them with a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md), till they were [tamam](../../strongs/h/h8552.md), that the [śārîḏ](../../strongs/h/h8300.md) which [śāraḏ](../../strongs/h/h8277.md) of them [bow'](../../strongs/h/h935.md) into [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_10_21"></a>Joshua 10:21

And all the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) to the [maḥănê](../../strongs/h/h4264.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) at [Maqqēḏâ](../../strongs/h/h4719.md) in [shalowm](../../strongs/h/h7965.md): none [ḥāraṣ](../../strongs/h/h2782.md) his [lashown](../../strongs/h/h3956.md) against ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_10_22"></a>Joshua 10:22

Then ['āmar](../../strongs/h/h559.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [pāṯaḥ](../../strongs/h/h6605.md) the [peh](../../strongs/h/h6310.md) of the [mᵊʿārâ](../../strongs/h/h4631.md), and [yāṣā'](../../strongs/h/h3318.md) those [ḥāmēš](../../strongs/h/h2568.md) [melek](../../strongs/h/h4428.md) unto me out of the [mᵊʿārâ](../../strongs/h/h4631.md).

<a name="joshua_10_23"></a>Joshua 10:23

And they ['asah](../../strongs/h/h6213.md) so, and [yāṣā'](../../strongs/h/h3318.md) those [ḥāmēš](../../strongs/h/h2568.md) [melek](../../strongs/h/h4428.md) unto him out of the [mᵊʿārâ](../../strongs/h/h4631.md), the [melek](../../strongs/h/h4428.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [melek](../../strongs/h/h4428.md) of [Ḥeḇrôn](../../strongs/h/h2275.md), the [melek](../../strongs/h/h4428.md) of [Yarmûṯ](../../strongs/h/h3412.md), the [melek](../../strongs/h/h4428.md) of [Lāḵîš](../../strongs/h/h3923.md), and the [melek](../../strongs/h/h4428.md) of [ʿEḡlôn](../../strongs/h/h5700.md).

<a name="joshua_10_24"></a>Joshua 10:24

And it came to pass, when they [yāṣā'](../../strongs/h/h3318.md) those [melek](../../strongs/h/h4428.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), that [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) for all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto the [qāṣîn](../../strongs/h/h7101.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) which [halak](../../strongs/h/h1980.md) with him, [qāraḇ](../../strongs/h/h7126.md), [śûm](../../strongs/h/h7760.md) your [regel](../../strongs/h/h7272.md) upon the [ṣaûā'r](../../strongs/h/h6677.md) of these [melek](../../strongs/h/h4428.md). And they [qāraḇ](../../strongs/h/h7126.md), and [śûm](../../strongs/h/h7760.md) their [regel](../../strongs/h/h7272.md) upon the [ṣaûā'r](../../strongs/h/h6677.md) of them.

<a name="joshua_10_25"></a>Joshua 10:25

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto them, [yare'](../../strongs/h/h3372.md) not, nor be [ḥāṯaṯ](../../strongs/h/h2865.md), be [ḥāzaq](../../strongs/h/h2388.md) and of ['amats](../../strongs/h/h553.md): for thus shall [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) to all your ['oyeb](../../strongs/h/h341.md) against whom ye [lāḥam](../../strongs/h/h3898.md).

<a name="joshua_10_26"></a>Joshua 10:26

And ['aḥar](../../strongs/h/h310.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nakah](../../strongs/h/h5221.md) them, and [muwth](../../strongs/h/h4191.md) them, and [tālâ](../../strongs/h/h8518.md) them on [ḥāmēš](../../strongs/h/h2568.md) ['ets](../../strongs/h/h6086.md): and they were [tālâ](../../strongs/h/h8518.md) upon the ['ets](../../strongs/h/h6086.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="joshua_10_27"></a>Joshua 10:27

And it came to pass at the [ʿēṯ](../../strongs/h/h6256.md) of the going [bow'](../../strongs/h/h935.md) of the [šemeš](../../strongs/h/h8121.md), that [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [tsavah](../../strongs/h/h6680.md), and they took them [yarad](../../strongs/h/h3381.md) off the ['ets](../../strongs/h/h6086.md), and [shalak](../../strongs/h/h7993.md) them into the [mᵊʿārâ](../../strongs/h/h4631.md) wherein they had been [chaba'](../../strongs/h/h2244.md), and [śûm](../../strongs/h/h7760.md) [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) in the [mᵊʿārâ](../../strongs/h/h4631.md) [peh](../../strongs/h/h6310.md), which remain until this ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_10_28"></a>Joshua 10:28

And that [yowm](../../strongs/h/h3117.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [lāḵaḏ](../../strongs/h/h3920.md) [Maqqēḏâ](../../strongs/h/h4719.md), and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and the [melek](../../strongs/h/h4428.md) thereof he [ḥāram](../../strongs/h/h2763.md), them, and all the [nephesh](../../strongs/h/h5315.md) that were therein; he [šā'ar](../../strongs/h/h7604.md) none [śārîḏ](../../strongs/h/h8300.md): and he ['asah](../../strongs/h/h6213.md) to the [melek](../../strongs/h/h4428.md) of [Maqqēḏâ](../../strongs/h/h4719.md) as he ['asah](../../strongs/h/h6213.md) unto the [melek](../../strongs/h/h4428.md) of [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_10_29"></a>Joshua 10:29

Then [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['abar](../../strongs/h/h5674.md) from [Maqqēḏâ](../../strongs/h/h4719.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, unto [Liḇnâ](../../strongs/h/h3841.md), and [lāḥam](../../strongs/h/h3898.md) against [Liḇnâ](../../strongs/h/h3841.md):

<a name="joshua_10_30"></a>Joshua 10:30

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) it also, and the [melek](../../strongs/h/h4428.md) thereof, into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md); and he [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and all the [nephesh](../../strongs/h/h5315.md) that were therein; he [šā'ar](../../strongs/h/h7604.md) none [śārîḏ](../../strongs/h/h8300.md) in it; but ['asah](../../strongs/h/h6213.md) unto the [melek](../../strongs/h/h4428.md) thereof as he ['asah](../../strongs/h/h6213.md) unto the [melek](../../strongs/h/h4428.md) of [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_10_31"></a>Joshua 10:31

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['abar](../../strongs/h/h5674.md) from [Liḇnâ](../../strongs/h/h3841.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, unto [Lāḵîš](../../strongs/h/h3923.md), and [ḥānâ](../../strongs/h/h2583.md) against it, and [lāḥam](../../strongs/h/h3898.md) against it:

<a name="joshua_10_32"></a>Joshua 10:32

And [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [Lāḵîš](../../strongs/h/h3923.md) into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md), which [lāḵaḏ](../../strongs/h/h3920.md) it on the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md), and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and all the [nephesh](../../strongs/h/h5315.md) that were therein, according to all that he had ['asah](../../strongs/h/h6213.md) to [Liḇnâ](../../strongs/h/h3841.md).

<a name="joshua_10_33"></a>Joshua 10:33

Then [Hōrām](../../strongs/h/h2036.md) [melek](../../strongs/h/h4428.md) of [Gezer](../../strongs/h/h1507.md) [ʿālâ](../../strongs/h/h5927.md) to [ʿāzar](../../strongs/h/h5826.md) [Lāḵîš](../../strongs/h/h3923.md); and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nakah](../../strongs/h/h5221.md) him and his ['am](../../strongs/h/h5971.md), until he had [šā'ar](../../strongs/h/h7604.md) him none [śārîḏ](../../strongs/h/h8300.md).

<a name="joshua_10_34"></a>Joshua 10:34

And from [Lāḵîš](../../strongs/h/h3923.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['abar](../../strongs/h/h5674.md) unto [ʿEḡlôn](../../strongs/h/h5700.md), and all [Yisra'el](../../strongs/h/h3478.md) with him; and they [ḥānâ](../../strongs/h/h2583.md) against it, and [lāḥam](../../strongs/h/h3898.md) against it:

<a name="joshua_10_35"></a>Joshua 10:35

And they [lāḵaḏ](../../strongs/h/h3920.md) it on that [yowm](../../strongs/h/h3117.md), and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and all the [nephesh](../../strongs/h/h5315.md) that were therein he [ḥāram](../../strongs/h/h2763.md) that [yowm](../../strongs/h/h3117.md), according to all that he had ['asah](../../strongs/h/h6213.md) to [Lāḵîš](../../strongs/h/h3923.md).

<a name="joshua_10_36"></a>Joshua 10:36

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [ʿālâ](../../strongs/h/h5927.md) from [ʿEḡlôn](../../strongs/h/h5700.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, unto [Ḥeḇrôn](../../strongs/h/h2275.md); and they [lāḥam](../../strongs/h/h3898.md) against it:

<a name="joshua_10_37"></a>Joshua 10:37

And they [lāḵaḏ](../../strongs/h/h3920.md) it, and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and the [melek](../../strongs/h/h4428.md) thereof, and all the [ʿîr](../../strongs/h/h5892.md) thereof, and all the [nephesh](../../strongs/h/h5315.md) that were therein; he [šā'ar](../../strongs/h/h7604.md) none [śārîḏ](../../strongs/h/h8300.md), according to all that he had ['asah](../../strongs/h/h6213.md) to [ʿEḡlôn](../../strongs/h/h5700.md); but [ḥāram](../../strongs/h/h2763.md) it utterly, and all the [nephesh](../../strongs/h/h5315.md) that were therein.

<a name="joshua_10_38"></a>Joshua 10:38

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shuwb](../../strongs/h/h7725.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, to [dᵊḇîr](../../strongs/h/h1688.md); and [lāḥam](../../strongs/h/h3898.md) against it:

<a name="joshua_10_39"></a>Joshua 10:39

And he [lāḵaḏ](../../strongs/h/h3920.md) it, and the [melek](../../strongs/h/h4428.md) thereof, and all the [ʿîr](../../strongs/h/h5892.md) thereof; and they [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [ḥāram](../../strongs/h/h2763.md) all the [nephesh](../../strongs/h/h5315.md) that were therein; he [šā'ar](../../strongs/h/h7604.md) none [śārîḏ](../../strongs/h/h8300.md): as he had ['asah](../../strongs/h/h6213.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), so he ['asah](../../strongs/h/h6213.md) to [dᵊḇîr](../../strongs/h/h1688.md), and to the [melek](../../strongs/h/h4428.md) thereof; as he had ['asah](../../strongs/h/h6213.md) also to [Liḇnâ](../../strongs/h/h3841.md), and to her [melek](../../strongs/h/h4428.md).

<a name="joshua_10_40"></a>Joshua 10:40

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nakah](../../strongs/h/h5221.md) all the ['erets](../../strongs/h/h776.md) of the [har](../../strongs/h/h2022.md), and of the [neḡeḇ](../../strongs/h/h5045.md), and of the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and of the ['ăšēḏâ](../../strongs/h/h794.md), and all their [melek](../../strongs/h/h4428.md): he [šā'ar](../../strongs/h/h7604.md) none [śārîḏ](../../strongs/h/h8300.md), but [ḥāram](../../strongs/h/h2763.md) all that [neshamah](../../strongs/h/h5397.md), as [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [tsavah](../../strongs/h/h6680.md).

<a name="joshua_10_41"></a>Joshua 10:41

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nakah](../../strongs/h/h5221.md) them from [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md) even unto [ʿAzzâ](../../strongs/h/h5804.md), and all the ['erets](../../strongs/h/h776.md) of [gōšen](../../strongs/h/h1657.md), even unto [Giḇʿôn](../../strongs/h/h1391.md).

<a name="joshua_10_42"></a>Joshua 10:42

And all these [melek](../../strongs/h/h4428.md) and their ['erets](../../strongs/h/h776.md) did [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [lāḵaḏ](../../strongs/h/h3920.md) at ['echad](../../strongs/h/h259.md) [pa'am](../../strongs/h/h6471.md), because [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [lāḥam](../../strongs/h/h3898.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_10_43"></a>Joshua 10:43

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shuwb](../../strongs/h/h7725.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, unto the [maḥănê](../../strongs/h/h4264.md) to [Gilgāl](../../strongs/h/h1537.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 9](joshua_9.md) - [Joshua 11](joshua_11.md)