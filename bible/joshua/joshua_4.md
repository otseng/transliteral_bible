# [Joshua 4](https://www.blueletterbible.org/kjv/joshua/4)

<a name="joshua_4_1"></a>Joshua 4:1

And it came to pass, when all the [gowy](../../strongs/h/h1471.md) were [tamam](../../strongs/h/h8552.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_4_2"></a>Joshua 4:2

[laqach](../../strongs/h/h3947.md) you [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['enowsh](../../strongs/h/h582.md) out of the ['am](../../strongs/h/h5971.md), out of ['echad](../../strongs/h/h259.md) [shebet](../../strongs/h/h7626.md) an ['iysh](../../strongs/h/h376.md),

<a name="joshua_4_3"></a>Joshua 4:3

And [tsavah](../../strongs/h/h6680.md) ye them, ['āmar](../../strongs/h/h559.md), [nasa'](../../strongs/h/h5375.md) you hence out of the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), out of the place where the [kōhēn](../../strongs/h/h3548.md) [regel](../../strongs/h/h7272.md) [maṣṣāḇ](../../strongs/h/h4673.md) [kuwn](../../strongs/h/h3559.md), [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['eben](../../strongs/h/h68.md), and ye shall ['abar](../../strongs/h/h5674.md) them with you, and [yānaḥ](../../strongs/h/h3240.md) them in the [mālôn](../../strongs/h/h4411.md), where ye shall [lûn](../../strongs/h/h3885.md) this [layil](../../strongs/h/h3915.md).

<a name="joshua_4_4"></a>Joshua 4:4

Then [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) the [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['iysh](../../strongs/h/h376.md), whom he had [kuwn](../../strongs/h/h3559.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), out of ['echad](../../strongs/h/h259.md) [shebet](../../strongs/h/h7626.md) an ['iysh](../../strongs/h/h376.md):

<a name="joshua_4_5"></a>Joshua 4:5

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto them, ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) into the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), and [ruwm](../../strongs/h/h7311.md) ye ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md) of you an ['eben](../../strongs/h/h68.md) upon his [šᵊḵem](../../strongs/h/h7926.md), according unto the [mispār](../../strongs/h/h4557.md) of the [shebet](../../strongs/h/h7626.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="joshua_4_6"></a>Joshua 4:6

That this may be a ['ôṯ](../../strongs/h/h226.md) [qereḇ](../../strongs/h/h7130.md) you, that when your [ben](../../strongs/h/h1121.md) [sha'al](../../strongs/h/h7592.md) their fathers in time [māḥār](../../strongs/h/h4279.md), ['āmar](../../strongs/h/h559.md), What mean ye by these ['eben](../../strongs/h/h68.md)?

<a name="joshua_4_7"></a>Joshua 4:7

Then ye shall ['āmar](../../strongs/h/h559.md) them, That the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) were [karath](../../strongs/h/h3772.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md); when it ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) were [karath](../../strongs/h/h3772.md): and these ['eben](../../strongs/h/h68.md) shall be for a [zikārôn](../../strongs/h/h2146.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="joshua_4_8"></a>Joshua 4:8

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) so as [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [tsavah](../../strongs/h/h6680.md), and [nasa'](../../strongs/h/h5375.md) [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['eben](../../strongs/h/h68.md) out of the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), according to the [mispār](../../strongs/h/h4557.md) of the [shebet](../../strongs/h/h7626.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['abar](../../strongs/h/h5674.md) them with them unto the place where they [mālôn](../../strongs/h/h4411.md), and [yānaḥ](../../strongs/h/h3240.md) them there.

<a name="joshua_4_9"></a>Joshua 4:9

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [quwm](../../strongs/h/h6965.md) [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['eben](../../strongs/h/h68.md) in the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), in the place where the [regel](../../strongs/h/h7272.md) of the [kōhēn](../../strongs/h/h3548.md) which [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) [maṣṣāḇ](../../strongs/h/h4673.md): and they are there unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_4_10"></a>Joshua 4:10

For the [kōhēn](../../strongs/h/h3548.md) which [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) ['amad](../../strongs/h/h5975.md) in the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), until every [dabar](../../strongs/h/h1697.md) was [tamam](../../strongs/h/h8552.md) that [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) to [dabar](../../strongs/h/h1696.md) unto the ['am](../../strongs/h/h5971.md), according to all that [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md): and the ['am](../../strongs/h/h5971.md) [māhar](../../strongs/h/h4116.md) and ['abar](../../strongs/h/h5674.md).

<a name="joshua_4_11"></a>Joshua 4:11

And it came to pass, when all the ['am](../../strongs/h/h5971.md) were [tamam](../../strongs/h/h8552.md) ['abar](../../strongs/h/h5674.md), that the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) ['abar](../../strongs/h/h5674.md), and the [kōhēn](../../strongs/h/h3548.md), in the [paniym](../../strongs/h/h6440.md) of the ['am](../../strongs/h/h5971.md).

<a name="joshua_4_12"></a>Joshua 4:12

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), and [ḥēṣî](../../strongs/h/h2677.md) the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), ['abar](../../strongs/h/h5674.md) [ḥāmaš](../../strongs/h/h2571.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), as [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto them:

<a name="joshua_4_13"></a>Joshua 4:13

About ['arbāʿîm](../../strongs/h/h705.md) ['elep̄](../../strongs/h/h505.md) [chalats](../../strongs/h/h2502.md) for [tsaba'](../../strongs/h/h6635.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) unto [milḥāmâ](../../strongs/h/h4421.md), to the ['arabah](../../strongs/h/h6160.md) of [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_4_14"></a>Joshua 4:14

On that [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) [gāḏal](../../strongs/h/h1431.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md); and they [yare'](../../strongs/h/h3372.md) him, as they [yare'](../../strongs/h/h3372.md) [Mōshe](../../strongs/h/h4872.md), all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="joshua_4_15"></a>Joshua 4:15

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_4_16"></a>Joshua 4:16

[tsavah](../../strongs/h/h6680.md) the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), that they [ʿālâ](../../strongs/h/h5927.md) out of [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_4_17"></a>Joshua 4:17

[Yᵊhôšûaʿ](../../strongs/h/h3091.md) therefore [tsavah](../../strongs/h/h6680.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) ye out of [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_4_18"></a>Joshua 4:18

And it came to pass, when the [kōhēn](../../strongs/h/h3548.md) that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) were [ʿālâ](../../strongs/h/h5927.md) out of the [tavek](../../strongs/h/h8432.md) of [Yardēn](../../strongs/h/h3383.md), and the [kaph](../../strongs/h/h3709.md) of the [kōhēn](../../strongs/h/h3548.md) [regel](../../strongs/h/h7272.md) were lifted [nathaq](../../strongs/h/h5423.md) unto the dry [ḥārāḇâ](../../strongs/h/h2724.md), that the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) [shuwb](../../strongs/h/h7725.md) unto their [maqowm](../../strongs/h/h4725.md), and [yālaḵ](../../strongs/h/h3212.md) over all his [gāḏâ](../../strongs/h/h1415.md), [tᵊmôl](../../strongs/h/h8543.md) they did [šilšôm](../../strongs/h/h8032.md).

<a name="joshua_4_19"></a>Joshua 4:19

And the ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md) out of [Yardēn](../../strongs/h/h3383.md) on the [ʿāśôr](../../strongs/h/h6218.md) day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), and [ḥānâ](../../strongs/h/h2583.md) in [Gilgāl](../../strongs/h/h1537.md), in the [mizrach](../../strongs/h/h4217.md) [qāṣê](../../strongs/h/h7097.md) of [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_4_20"></a>Joshua 4:20

And those [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['eben](../../strongs/h/h68.md), which they [laqach](../../strongs/h/h3947.md) of [Yardēn](../../strongs/h/h3383.md), did [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [quwm](../../strongs/h/h6965.md) in [Gilgāl](../../strongs/h/h1537.md).

<a name="joshua_4_21"></a>Joshua 4:21

And he ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), When your [ben](../../strongs/h/h1121.md) shall [sha'al](../../strongs/h/h7592.md) their ['ab](../../strongs/h/h1.md) in time [māḥār](../../strongs/h/h4279.md), ['āmar](../../strongs/h/h559.md), What mean these ['eben](../../strongs/h/h68.md)?

<a name="joshua_4_22"></a>Joshua 4:22

Then ye shall let your [ben](../../strongs/h/h1121.md) [yada'](../../strongs/h/h3045.md), ['āmar](../../strongs/h/h559.md), [Yisra'el](../../strongs/h/h3478.md) ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md) on [yabāšâ](../../strongs/h/h3004.md).

<a name="joshua_4_23"></a>Joshua 4:23

For [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) dried [yāḇēš](../../strongs/h/h3001.md) the [mayim](../../strongs/h/h4325.md) of [Yardēn](../../strongs/h/h3383.md) from [paniym](../../strongs/h/h6440.md) you, until ye were ['abar](../../strongs/h/h5674.md), as [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) to the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), which he [yāḇēš](../../strongs/h/h3001.md) from [paniym](../../strongs/h/h6440.md) us, until we were ['abar](../../strongs/h/h5674.md):

<a name="joshua_4_24"></a>Joshua 4:24

That all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) might [yada'](../../strongs/h/h3045.md) the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md), that it is [ḥāzāq](../../strongs/h/h2389.md): that ye might [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) for [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 3](joshua_3.md) - [Joshua 5](joshua_5.md)