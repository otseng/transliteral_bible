# [Joshua 15](https://www.blueletterbible.org/kjv/joshua/15)

<a name="joshua_15_1"></a>Joshua 15:1

This then was the [gôrāl](../../strongs/h/h1486.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) by their [mišpāḥâ](../../strongs/h/h4940.md); even to the [gᵊḇûl](../../strongs/h/h1366.md) of ['Ĕḏōm](../../strongs/h/h123.md) the [midbar](../../strongs/h/h4057.md) of [Ṣin](../../strongs/h/h6790.md) [neḡeḇ](../../strongs/h/h5045.md) was the uttermost [qāṣê](../../strongs/h/h7097.md) of the [têmān](../../strongs/h/h8486.md).

<a name="joshua_15_2"></a>Joshua 15:2

And their [neḡeḇ](../../strongs/h/h5045.md) [gᵊḇûl](../../strongs/h/h1366.md) was from the [qāṣê](../../strongs/h/h7097.md) of the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md), from the [lashown](../../strongs/h/h3956.md) that [panah](../../strongs/h/h6437.md) [neḡeḇ](../../strongs/h/h5045.md):

<a name="joshua_15_3"></a>Joshua 15:3

And it [yāṣā'](../../strongs/h/h3318.md) to the [neḡeḇ](../../strongs/h/h5045.md) to [MaʿĂlê ʿAqrabîm](../../strongs/h/h4610.md), and ['abar](../../strongs/h/h5674.md) along to [Ṣin](../../strongs/h/h6790.md), and [ʿālâ](../../strongs/h/h5927.md) on the [neḡeḇ](../../strongs/h/h5045.md) unto [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md), and ['abar](../../strongs/h/h5674.md) along to [Ḥeṣrôn](../../strongs/h/h2696.md), and [ʿālâ](../../strongs/h/h5927.md) to ['Adār](../../strongs/h/h146.md), and fetched a [cabab](../../strongs/h/h5437.md) to [Qarqaʿ](../../strongs/h/h7173.md):

<a name="joshua_15_4"></a>Joshua 15:4

From thence it ['abar](../../strongs/h/h5674.md) toward [ʿAṣmôn](../../strongs/h/h6111.md), and [yāṣā'](../../strongs/h/h3318.md) unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of that [gᵊḇûl](../../strongs/h/h1366.md) were at the [yam](../../strongs/h/h3220.md): this shall be your [neḡeḇ](../../strongs/h/h5045.md) [gᵊḇûl](../../strongs/h/h1366.md).

<a name="joshua_15_5"></a>Joshua 15:5

And the [qeḏem](../../strongs/h/h6924.md) [gᵊḇûl](../../strongs/h/h1366.md) was the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md), even unto the [qāṣê](../../strongs/h/h7097.md) of [Yardēn](../../strongs/h/h3383.md). And their [gᵊḇûl](../../strongs/h/h1366.md) in the [ṣāp̄ôn](../../strongs/h/h6828.md) [pē'â](../../strongs/h/h6285.md) was from the [lashown](../../strongs/h/h3956.md) of the [yam](../../strongs/h/h3220.md) at the uttermost [qāṣê](../../strongs/h/h7097.md) of [Yardēn](../../strongs/h/h3383.md):

<a name="joshua_15_6"></a>Joshua 15:6

And the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) to [Bêṯ-Ḥāḡlâ](../../strongs/h/h1031.md), and ['abar](../../strongs/h/h5674.md) along by the [ṣāp̄ôn](../../strongs/h/h6828.md) of [Bêṯ HāʿĂrāḇâ](../../strongs/h/h1026.md); and the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) to the ['eben](../../strongs/h/h68.md) of [Bōhan](../../strongs/h/h932.md) the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md):

<a name="joshua_15_7"></a>Joshua 15:7

And the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) toward [dᵊḇîr](../../strongs/h/h1688.md) from the [ʿēmeq](../../strongs/h/h6010.md) of [ʿāḵôr](../../strongs/h/h5911.md), and so [ṣāp̄ôn](../../strongs/h/h6828.md), [panah](../../strongs/h/h6437.md) toward [Gilgāl](../../strongs/h/h1537.md), that is [nōḵaḥ](../../strongs/h/h5227.md) the [maʿălê](../../strongs/h/h4608.md) to ['Ăḏummîm](../../strongs/h/h131.md), which is on the [neḡeḇ](../../strongs/h/h5045.md) of the [nachal](../../strongs/h/h5158.md): and the [gᵊḇûl](../../strongs/h/h1366.md) ['abar](../../strongs/h/h5674.md) toward the [mayim](../../strongs/h/h4325.md) of [ʿÊn Šemeš](../../strongs/h/h5885.md), and the goings [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof were at [ʿÊn Rōḡēl](../../strongs/h/h5883.md):

<a name="joshua_15_8"></a>Joshua 15:8

And the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) by the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md) unto the [neḡeḇ](../../strongs/h/h5045.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [Yᵊḇûsî](../../strongs/h/h2983.md); the same is [Yĕruwshalaim](../../strongs/h/h3389.md): and the [gᵊḇûl](../../strongs/h/h1366.md) [ʿālâ](../../strongs/h/h5927.md) to the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md) that lieth [paniym](../../strongs/h/h6440.md) the [gay'](../../strongs/h/h1516.md) of [Hinnōm](../../strongs/h/h2011.md) [yam](../../strongs/h/h3220.md), which is at the [qāṣê](../../strongs/h/h7097.md) of the [ʿēmeq](../../strongs/h/h6010.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md) [ṣāp̄ôn](../../strongs/h/h6828.md):

<a name="joshua_15_9"></a>Joshua 15:9

And the [gᵊḇûl](../../strongs/h/h1366.md) was [tā'ar](../../strongs/h/h8388.md) from the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md) unto the [maʿyān](../../strongs/h/h4599.md) of the [mayim](../../strongs/h/h4325.md) of [Nep̄Tôaḥ](../../strongs/h/h5318.md), and [yāṣā'](../../strongs/h/h3318.md) to the [ʿîr](../../strongs/h/h5892.md) of [har](../../strongs/h/h2022.md) [ʿep̄rôn](../../strongs/h/h6085.md); and the [gᵊḇûl](../../strongs/h/h1366.md) was [tā'ar](../../strongs/h/h8388.md) to [BaʿĂlâ](../../strongs/h/h1173.md), which is [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md):

<a name="joshua_15_10"></a>Joshua 15:10

And the [gᵊḇûl](../../strongs/h/h1366.md) [cabab](../../strongs/h/h5437.md) from [BaʿĂlâ](../../strongs/h/h1173.md) [yam](../../strongs/h/h3220.md) unto [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), and ['abar](../../strongs/h/h5674.md) along unto the [kāṯēp̄](../../strongs/h/h3802.md) of [har](../../strongs/h/h2022.md) [YᵊʿĀrîm](../../strongs/h/h3297.md), which is [Kᵊsālôn](../../strongs/h/h3693.md), on the [ṣāp̄ôn](../../strongs/h/h6828.md), and [yarad](../../strongs/h/h3381.md) to [Bêṯ Šemeš](../../strongs/h/h1053.md), and ['abar](../../strongs/h/h5674.md) to [Timnâ](../../strongs/h/h8553.md):

<a name="joshua_15_11"></a>Joshua 15:11

And the [gᵊḇûl](../../strongs/h/h1366.md) [yāṣā'](../../strongs/h/h3318.md) unto the [kāṯēp̄](../../strongs/h/h3802.md) of [ʿEqrôn](../../strongs/h/h6138.md) [ṣāp̄ôn](../../strongs/h/h6828.md): and the [gᵊḇûl](../../strongs/h/h1366.md) was [tā'ar](../../strongs/h/h8388.md) to [Šiḵrôn](../../strongs/h/h7942.md), and ['abar](../../strongs/h/h5674.md) to [har](../../strongs/h/h2022.md) [BaʿĂlâ](../../strongs/h/h1173.md), and [yāṣā'](../../strongs/h/h3318.md) unto [Yaḇnᵊ'Ēl](../../strongs/h/h2995.md); and the goings [tôṣā'ôṯ](../../strongs/h/h8444.md) of the [gᵊḇûl](../../strongs/h/h1366.md) were at the [yam](../../strongs/h/h3220.md).

<a name="joshua_15_12"></a>Joshua 15:12

And the [yam](../../strongs/h/h3220.md) [gᵊḇûl](../../strongs/h/h1366.md) was to the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof. This is the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [cabiyb](../../strongs/h/h5439.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_15_13"></a>Joshua 15:13

And unto [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) he [nathan](../../strongs/h/h5414.md) a [cheleq](../../strongs/h/h2506.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), ['ēl](../../strongs/h/h413.md) to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md), even the [qiryâ](../../strongs/h/h7151.md) of ['Arbaʿ](../../strongs/h/h704.md) [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md) the ['ab](../../strongs/h/h1.md) of [ʿĂnāq](../../strongs/h/h6061.md), which city is [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="joshua_15_14"></a>Joshua 15:14

And [Kālēḇ](../../strongs/h/h3612.md) [yarash](../../strongs/h/h3423.md) thence the [šālôš](../../strongs/h/h7969.md) [ben](../../strongs/h/h1121.md) of [ʿĂnāq](../../strongs/h/h6061.md), [šēšay](../../strongs/h/h8344.md), and ['Ăḥîmān](../../strongs/h/h289.md), and [Talmay](../../strongs/h/h8526.md), the [yālîḏ](../../strongs/h/h3211.md) of [ʿĂnāq](../../strongs/h/h6061.md).

<a name="joshua_15_15"></a>Joshua 15:15

And he [ʿālâ](../../strongs/h/h5927.md) thence to the [yashab](../../strongs/h/h3427.md) of [dᵊḇîr](../../strongs/h/h1688.md): and the [shem](../../strongs/h/h8034.md) of [dᵊḇîr](../../strongs/h/h1688.md) [paniym](../../strongs/h/h6440.md) was [Qiryaṯ-Sēnê](../../strongs/h/h7158.md).

<a name="joshua_15_16"></a>Joshua 15:16

And [Kālēḇ](../../strongs/h/h3612.md) ['āmar](../../strongs/h/h559.md), He that [nakah](../../strongs/h/h5221.md) [Qiryaṯ-Sēnê](../../strongs/h/h7158.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, to him will I [nathan](../../strongs/h/h5414.md) [ʿAḵsâ](../../strongs/h/h5915.md) my [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md).

<a name="joshua_15_17"></a>Joshua 15:17

And [ʿĀṯnî'Ēl](../../strongs/h/h6274.md) the [ben](../../strongs/h/h1121.md) of [Qᵊnaz](../../strongs/h/h7073.md), the ['ach](../../strongs/h/h251.md) of [Kālēḇ](../../strongs/h/h3612.md), [lāḵaḏ](../../strongs/h/h3920.md) it: and he [nathan](../../strongs/h/h5414.md) him [ʿAḵsâ](../../strongs/h/h5915.md) his [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md).

<a name="joshua_15_18"></a>Joshua 15:18

And it came to pass, as she [bow'](../../strongs/h/h935.md) unto him, that she [sûṯ](../../strongs/h/h5496.md) him to [sha'al](../../strongs/h/h7592.md) of her ['ab](../../strongs/h/h1.md) a [sadeh](../../strongs/h/h7704.md): and she [ṣānaḥ](../../strongs/h/h6795.md) her [chamowr](../../strongs/h/h2543.md); and [Kālēḇ](../../strongs/h/h3612.md) ['āmar](../../strongs/h/h559.md) unto her, What wouldest thou?

<a name="joshua_15_19"></a>Joshua 15:19

Who ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) me a [bĕrakah](../../strongs/h/h1293.md); for thou hast [nathan](../../strongs/h/h5414.md) me a [neḡeḇ](../../strongs/h/h5045.md) ['erets](../../strongs/h/h776.md); [nathan](../../strongs/h/h5414.md) me also [gullâ](../../strongs/h/h1543.md) of [mayim](../../strongs/h/h4325.md). And he [nathan](../../strongs/h/h5414.md) her the [ʿillî](../../strongs/h/h5942.md) [gullâ](../../strongs/h/h1543.md), and the [taḥtî](../../strongs/h/h8482.md) [gullâ](../../strongs/h/h1543.md).

<a name="joshua_15_20"></a>Joshua 15:20

This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_15_21"></a>Joshua 15:21

And the [qāṣê](../../strongs/h/h7097.md) [ʿîr](../../strongs/h/h5892.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) toward the [gᵊḇûl](../../strongs/h/h1366.md) of ['Ĕḏōm](../../strongs/h/h123.md) [neḡeḇ](../../strongs/h/h5045.md) were [Qaḇṣᵊ'Ēl](../../strongs/h/h6909.md), and [ʿĒḏer](../../strongs/h/h5740.md), and [Yāḡûr](../../strongs/h/h3017.md),

<a name="joshua_15_22"></a>Joshua 15:22

And [Qînâ](../../strongs/h/h7016.md), and [Dîmônâ](../../strongs/h/h1776.md), and [ʿAḏʿĀḏâ](../../strongs/h/h5735.md),

<a name="joshua_15_23"></a>Joshua 15:23

And [Qeḏeš](../../strongs/h/h6943.md), and [Ḥāṣôr](../../strongs/h/h2674.md), and [Yiṯnān](../../strongs/h/h3497.md),

<a name="joshua_15_24"></a>Joshua 15:24

[Zîp̄](../../strongs/h/h2128.md), and [Ṭelem](../../strongs/h/h2928.md), and [BᵊʿĀlôṯ](../../strongs/h/h1175.md),

<a name="joshua_15_25"></a>Joshua 15:25

And [Ḥāṣôr](../../strongs/h/h2674.md), [Ḥāṣôr Ḥăḏatâ](../../strongs/h/h2675.md), and [Qᵊrîyôṯ](../../strongs/h/h7152.md), and [Ḥeṣrôn](../../strongs/h/h2696.md), which is [Ḥāṣôr](../../strongs/h/h2674.md),

<a name="joshua_15_26"></a>Joshua 15:26

['Ămām](../../strongs/h/h538.md), and [Šᵊmāʿ](../../strongs/h/h8090.md), and [Môlāḏâ](../../strongs/h/h4137.md),

<a name="joshua_15_27"></a>Joshua 15:27

And [Ḥăṣar Ḡadâ](../../strongs/h/h2693.md), and [Ḥešmôn](../../strongs/h/h2829.md), and [Bêṯ Peleṭ](../../strongs/h/h1046.md),

<a name="joshua_15_28"></a>Joshua 15:28

And [Ḥăṣar ShÛʿĀl](../../strongs/h/h2705.md), and [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [Bizyôṯyâ](../../strongs/h/h964.md),

<a name="joshua_15_29"></a>Joshua 15:29

[BaʿĂlâ](../../strongs/h/h1173.md), and [ʿÎyîm](../../strongs/h/h5864.md), and [ʿEṣem](../../strongs/h/h6107.md),

<a name="joshua_15_30"></a>Joshua 15:30

And ['Eltôlaḏ](../../strongs/h/h513.md), and [Kᵊsîl](../../strongs/h/h3686.md), and [Ḥārmâ](../../strongs/h/h2767.md),

<a name="joshua_15_31"></a>Joshua 15:31

And [Ṣiqlāḡ](../../strongs/h/h6860.md), and [Maḏmannâ](../../strongs/h/h4089.md), and [sansannâ](../../strongs/h/h5578.md),

<a name="joshua_15_32"></a>Joshua 15:32

And [Lᵊḇā'Ôṯ](../../strongs/h/h3822.md), and [Šilḥîm](../../strongs/h/h7978.md), and [ʿAyin](../../strongs/h/h5871.md), and [Rimmôn](../../strongs/h/h7417.md): all the [ʿîr](../../strongs/h/h5892.md) are [ʿeśrîm](../../strongs/h/h6242.md) and [tēšaʿ](../../strongs/h/h8672.md), with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_33"></a>Joshua 15:33

And in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), ['Eštā'Ōl](../../strongs/h/h847.md), and [ṢārʿÂ](../../strongs/h/h6881.md), and ['Ašnâ](../../strongs/h/h823.md),

<a name="joshua_15_34"></a>Joshua 15:34

And [Zānôaḥ](../../strongs/h/h2182.md), and [ʿÊn Ḡannîm](../../strongs/h/h5873.md), [Tapûaḥ](../../strongs/h/h8599.md), and [ʿÊnayim](../../strongs/h/h5879.md),

<a name="joshua_15_35"></a>Joshua 15:35

[Yarmûṯ](../../strongs/h/h3412.md), and [ʿĂḏullām](../../strongs/h/h5725.md), [Śôḵô](../../strongs/h/h7755.md), and [ʿĂzēqâ](../../strongs/h/h5825.md),

<a name="joshua_15_36"></a>Joshua 15:36

And [ŠaʿĂrayim](../../strongs/h/h8189.md), and [ʿĂḏîṯayim](../../strongs/h/h5723.md), and [Gᵊḏērâ](../../strongs/h/h1449.md), and [Gᵊḏērōṯāyim](../../strongs/h/h1453.md); ['arbaʿ](../../strongs/h/h702.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_37"></a>Joshua 15:37

[Ṣᵊnān](../../strongs/h/h6799.md), and [Ḥăḏāšâ](../../strongs/h/h2322.md), and [Miḡdal-Gāḏ](../../strongs/h/h4028.md),

<a name="joshua_15_38"></a>Joshua 15:38

And [DilʿĀn](../../strongs/h/h1810.md), and [Miṣpê](../../strongs/h/h4708.md), and [Yāqṯᵊ'Ēl](../../strongs/h/h3371.md),

<a name="joshua_15_39"></a>Joshua 15:39

[Lāḵîš](../../strongs/h/h3923.md), and [Bāṣqaṯ](../../strongs/h/h1218.md), and [ʿEḡlôn](../../strongs/h/h5700.md),

<a name="joshua_15_40"></a>Joshua 15:40

And [Kabôn](../../strongs/h/h3522.md), and [Laḥmās](../../strongs/h/h3903.md), and [Kiṯlîš](../../strongs/h/h3798.md),

<a name="joshua_15_41"></a>Joshua 15:41

And [Gᵊḏērôṯ](../../strongs/h/h1450.md), [Bêṯ-Dāḡôn](../../strongs/h/h1016.md), and [naʿămâ](../../strongs/h/h5279.md), and [Maqqēḏâ](../../strongs/h/h4719.md); [šēš](../../strongs/h/h8337.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_42"></a>Joshua 15:42

[Liḇnâ](../../strongs/h/h3841.md), and [ʿEṯer](../../strongs/h/h6281.md), and [ʿĀšān](../../strongs/h/h6228.md),

<a name="joshua_15_43"></a>Joshua 15:43

And [Yip̄tāḥ](../../strongs/h/h3316.md), and ['Ašnâ](../../strongs/h/h823.md), and [Nᵊṣîḇ](../../strongs/h/h5334.md),

<a name="joshua_15_44"></a>Joshua 15:44

And [QᵊʿÎlâ](../../strongs/h/h7084.md), and ['Aḵzîḇ](../../strongs/h/h392.md), and [Mārē'Šâ](../../strongs/h/h4762.md); [tēšaʿ](../../strongs/h/h8672.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_45"></a>Joshua 15:45

[ʿEqrôn](../../strongs/h/h6138.md), with her [bath](../../strongs/h/h1323.md) and her [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_46"></a>Joshua 15:46

From [ʿEqrôn](../../strongs/h/h6138.md) even unto the [yam](../../strongs/h/h3220.md), all that lay [yad](../../strongs/h/h3027.md) ['Ašdôḏ](../../strongs/h/h795.md), with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_47"></a>Joshua 15:47

['Ašdôḏ](../../strongs/h/h795.md) with her [bath](../../strongs/h/h1323.md) and her [ḥāṣēr](../../strongs/h/h2691.md), [ʿAzzâ](../../strongs/h/h5804.md) with her [bath](../../strongs/h/h1323.md) and her [ḥāṣēr](../../strongs/h/h2691.md), unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md), and the [gadowl](../../strongs/h/h1419.md) [gᵊḇûl](../../strongs/h/h1366.md) [yam](../../strongs/h/h3220.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof:

<a name="joshua_15_48"></a>Joshua 15:48

And in the [har](../../strongs/h/h2022.md), [Šāmîr](../../strongs/h/h8069.md), and [Yatîr](../../strongs/h/h3492.md), and [Śôḵô](../../strongs/h/h7755.md),

<a name="joshua_15_49"></a>Joshua 15:49

And [Dannâ](../../strongs/h/h1837.md), and [Qiryaṯ-Sēnê](../../strongs/h/h7158.md), which is [dᵊḇîr](../../strongs/h/h1688.md),

<a name="joshua_15_50"></a>Joshua 15:50

And [ʿĀnāḇ](../../strongs/h/h6024.md), and ['Eštᵊmōaʿ](../../strongs/h/h851.md), and [ʿĀnîm](../../strongs/h/h6044.md),

<a name="joshua_15_51"></a>Joshua 15:51

And [gōšen](../../strongs/h/h1657.md), and [Ḥōlôn](../../strongs/h/h2473.md), and [Gilô](../../strongs/h/h1542.md); ['echad](../../strongs/h/h259.md) [ʿeśer](../../strongs/h/h6240.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_52"></a>Joshua 15:52

['Ărāḇ](../../strongs/h/h694.md), and [Dûmâ](../../strongs/h/h1746.md), and ['EšʿĀn](../../strongs/h/h824.md),

<a name="joshua_15_53"></a>Joshua 15:53

And [Yānûm](../../strongs/h/h3241.md), and [Bêṯ-Tapûaḥ](../../strongs/h/h1054.md), and ['Ăp̄Ēqâ](../../strongs/h/h664.md),

<a name="joshua_15_54"></a>Joshua 15:54

And [Ḥumṭâ](../../strongs/h/h2547.md), and [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md), which is [Ḥeḇrôn](../../strongs/h/h2275.md), and [ṢîʿŌr](../../strongs/h/h6730.md); [tēšaʿ](../../strongs/h/h8672.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_55"></a>Joshua 15:55

[MāʿÔn](../../strongs/h/h4584.md), [Karmel](../../strongs/h/h3760.md), and [Zîp̄](../../strongs/h/h2128.md), and [Yuṭṭâ](../../strongs/h/h3194.md),

<a name="joshua_15_56"></a>Joshua 15:56

And [YizrᵊʿE'L](../../strongs/h/h3157.md), and [YāqdᵊʿĀm](../../strongs/h/h3347.md), and [Zānôaḥ](../../strongs/h/h2182.md),

<a name="joshua_15_57"></a>Joshua 15:57

[Qayin](../../strongs/h/h7014.md), [giḇʿâ](../../strongs/h/h1390.md), and [Timnâ](../../strongs/h/h8553.md); [ʿeśer](../../strongs/h/h6235.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_58"></a>Joshua 15:58

[Ḥalḥûl](../../strongs/h/h2478.md), [Bêṯ-Ṣûr](../../strongs/h/h1049.md), and [Gᵊḏōr](../../strongs/h/h1446.md),

<a name="joshua_15_59"></a>Joshua 15:59

And [MaʿĂrāṯ](../../strongs/h/h4638.md), and [Bêṯ-ʿĂnôṯ](../../strongs/h/h1042.md), and ['Eltᵊqōn](../../strongs/h/h515.md); [šēš](../../strongs/h/h8337.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_60"></a>Joshua 15:60

[Qiryaṯ-BaʿAl](../../strongs/h/h7154.md), which is [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), and [Rabâ](../../strongs/h/h7237.md); [šᵊnayim](../../strongs/h/h8147.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md):

<a name="joshua_15_61"></a>Joshua 15:61

In the [midbar](../../strongs/h/h4057.md), [Bêṯ HāʿĂrāḇâ](../../strongs/h/h1026.md), [Midîn](../../strongs/h/h4081.md), and [Sᵊḵāḵâ](../../strongs/h/h5527.md),

<a name="joshua_15_62"></a>Joshua 15:62

And [Niḇšān](../../strongs/h/h5044.md), and the city of [ʿÎr Hammelaḥ](../../strongs/h/h5898.md), and [ʿÊn Ḡeḏî](../../strongs/h/h5872.md); [šēš](../../strongs/h/h8337.md) [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_15_63"></a>Joshua 15:63

As for the [Yᵊḇûsî](../../strongs/h/h2983.md) the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [yakol](../../strongs/h/h3201.md) [yakol](../../strongs/h/h3201.md) not drive them [yarash](../../strongs/h/h3423.md): but the [Yᵊḇûsî](../../strongs/h/h2983.md) [yashab](../../strongs/h/h3427.md) with the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 14](joshua_14.md) - [Joshua 16](joshua_16.md)