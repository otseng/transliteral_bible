# [Joshua 17](https://www.blueletterbible.org/kjv/joshua/17)

<a name="joshua_17_1"></a>Joshua 17:1

There was also a [gôrāl](../../strongs/h/h1486.md) for the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md); for he was the [bᵊḵôr](../../strongs/h/h1060.md) of [Yôsēp̄](../../strongs/h/h3130.md); to wit, for [Māḵîr](../../strongs/h/h4353.md) the [bᵊḵôr](../../strongs/h/h1060.md) of [Mᵊnaššê](../../strongs/h/h4519.md), the ['ab](../../strongs/h/h1.md) of [Gilʿāḏ](../../strongs/h/h1568.md): because he was an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), therefore he had [Gilʿāḏ](../../strongs/h/h1568.md) and [Bāšān](../../strongs/h/h1316.md).

<a name="joshua_17_2"></a>Joshua 17:2

There was also a lot for the [yāṯar](../../strongs/h/h3498.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) by their [mišpāḥâ](../../strongs/h/h4940.md); for the [ben](../../strongs/h/h1121.md) of ['ĂḇîʿEzer](../../strongs/h/h44.md), and for the [ben](../../strongs/h/h1121.md) of [Ḥēleq](../../strongs/h/h2507.md), and for the [ben](../../strongs/h/h1121.md) of ['Aśrî'Ēl](../../strongs/h/h844.md), and for the [ben](../../strongs/h/h1121.md) of [Šeḵem](../../strongs/h/h7928.md), and for the [ben](../../strongs/h/h1121.md) of [Ḥēp̄er](../../strongs/h/h2660.md), and for the [ben](../../strongs/h/h1121.md) of [Šᵊmîḏāʿ](../../strongs/h/h8061.md): these were the [zāḵār](../../strongs/h/h2145.md) [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) by their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_17_3"></a>Joshua 17:3

But [Ṣᵊlāp̄Ḥāḏ](../../strongs/h/h6765.md), the [ben](../../strongs/h/h1121.md) of [Ḥēp̄er](../../strongs/h/h2660.md), the [ben](../../strongs/h/h1121.md) of [Gilʿāḏ](../../strongs/h/h1568.md), the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), had no [ben](../../strongs/h/h1121.md), but [bath](../../strongs/h/h1323.md): and these are the [shem](../../strongs/h/h8034.md) of his [bath](../../strongs/h/h1323.md), [Maḥlâ](../../strongs/h/h4244.md), and [NōʿÂ](../../strongs/h/h5270.md), [Ḥāḡlâ](../../strongs/h/h2295.md), [Milkâ](../../strongs/h/h4435.md), and [Tirṣâ](../../strongs/h/h8656.md).

<a name="joshua_17_4"></a>Joshua 17:4

And they [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [paniym](../../strongs/h/h6440.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and [paniym](../../strongs/h/h6440.md) the [nāśî'](../../strongs/h/h5387.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) to [nathan](../../strongs/h/h5414.md) us a [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) our ['ach](../../strongs/h/h251.md). Therefore according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) he [nathan](../../strongs/h/h5414.md) them a [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) the ['ach](../../strongs/h/h251.md) of their ['ab](../../strongs/h/h1.md).

<a name="joshua_17_5"></a>Joshua 17:5

And there [naphal](../../strongs/h/h5307.md) [ʿeśer](../../strongs/h/h6235.md) [chebel](../../strongs/h/h2256.md) to [Mᵊnaššê](../../strongs/h/h4519.md), beside the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md) and [Bāšān](../../strongs/h/h1316.md), which were on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md);

<a name="joshua_17_6"></a>Joshua 17:6

Because the [bath](../../strongs/h/h1323.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [nāḥal](../../strongs/h/h5157.md) a [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) his [ben](../../strongs/h/h1121.md): and the [yāṯar](../../strongs/h/h3498.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [ben](../../strongs/h/h1121.md) had the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="joshua_17_7"></a>Joshua 17:7

And the [gᵊḇûl](../../strongs/h/h1366.md) of [Mᵊnaššê](../../strongs/h/h4519.md) was from ['Āšēr](../../strongs/h/h836.md) to [Miḵmᵊṯāṯ](../../strongs/h/h4366.md), that lieth [paniym](../../strongs/h/h6440.md) [Šᵊḵem](../../strongs/h/h7927.md); and the [gᵊḇûl](../../strongs/h/h1366.md) [halak](../../strongs/h/h1980.md) on the [yamiyn](../../strongs/h/h3225.md) unto the [yashab](../../strongs/h/h3427.md) of [ʿÊn Tapûaḥ](../../strongs/h/h5887.md).

<a name="joshua_17_8"></a>Joshua 17:8

Now [Mᵊnaššê](../../strongs/h/h4519.md) had the ['erets](../../strongs/h/h776.md) of [Tapûaḥ](../../strongs/h/h8599.md): but [Tapûaḥ](../../strongs/h/h8599.md) on the [gᵊḇûl](../../strongs/h/h1366.md) of [Mᵊnaššê](../../strongs/h/h4519.md) belonged to the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md);

<a name="joshua_17_9"></a>Joshua 17:9

And the [gᵊḇûl](../../strongs/h/h1366.md) [yarad](../../strongs/h/h3381.md) unto the [nachal](../../strongs/h/h5158.md) [Qānâ](../../strongs/h/h7071.md), [neḡeḇ](../../strongs/h/h5045.md) of the [nachal](../../strongs/h/h5158.md): these [ʿîr](../../strongs/h/h5892.md) of ['Ep̄rayim](../../strongs/h/h669.md) are [tavek](../../strongs/h/h8432.md) the [ʿîr](../../strongs/h/h5892.md) of [Mᵊnaššê](../../strongs/h/h4519.md): the [gᵊḇûl](../../strongs/h/h1366.md) of [Mᵊnaššê](../../strongs/h/h4519.md) also was on the [ṣāp̄ôn](../../strongs/h/h6828.md) of the [nachal](../../strongs/h/h5158.md), and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of it were at the [yam](../../strongs/h/h3220.md):

<a name="joshua_17_10"></a>Joshua 17:10

[neḡeḇ](../../strongs/h/h5045.md) it was ['Ep̄rayim](../../strongs/h/h669.md), and [ṣāp̄ôn](../../strongs/h/h6828.md) it was [Mᵊnaššê](../../strongs/h/h4519.md), and the [yam](../../strongs/h/h3220.md) is his [gᵊḇûl](../../strongs/h/h1366.md); and they met [pāḡaʿ](../../strongs/h/h6293.md) in ['Āšēr](../../strongs/h/h836.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md), and in [Yiśśāśḵār](../../strongs/h/h3485.md) on the [mizrach](../../strongs/h/h4217.md).

<a name="joshua_17_11"></a>Joshua 17:11

And [Mᵊnaššê](../../strongs/h/h4519.md) had in [Yiśśāśḵār](../../strongs/h/h3485.md) and in ['Āšēr](../../strongs/h/h836.md) [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md) and her [bath](../../strongs/h/h1323.md), and [YiḇlᵊʿĀm](../../strongs/h/h2991.md) and her [bath](../../strongs/h/h1323.md), and the [yashab](../../strongs/h/h3427.md) of [Dôr](../../strongs/h/h1756.md) and her [bath](../../strongs/h/h1323.md), and the [yashab](../../strongs/h/h3427.md) of [ʿÊn-Ḏôr](../../strongs/h/h5874.md) and her [bath](../../strongs/h/h1323.md), and the [yashab](../../strongs/h/h3427.md) of [TaʿNāḵ](../../strongs/h/h8590.md) and her [bath](../../strongs/h/h1323.md), and the [yashab](../../strongs/h/h3427.md) of [Mᵊḡidôn](../../strongs/h/h4023.md) and her [bath](../../strongs/h/h1323.md), even [šālôš](../../strongs/h/h7969.md) [nep̄eṯ](../../strongs/h/h5316.md).

<a name="joshua_17_12"></a>Joshua 17:12

Yet the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [yakol](../../strongs/h/h3201.md) not [yarash](../../strongs/h/h3423.md) the inhabitants of those [ʿîr](../../strongs/h/h5892.md); but the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yā'al](../../strongs/h/h2974.md) [yashab](../../strongs/h/h3427.md) in that ['erets](../../strongs/h/h776.md).

<a name="joshua_17_13"></a>Joshua 17:13

Yet it came to pass, when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were waxen [ḥāzaq](../../strongs/h/h2388.md), that they [nathan](../../strongs/h/h5414.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) to [mas](../../strongs/h/h4522.md); but did not [yarash](../../strongs/h/h3423.md) [yarash](../../strongs/h/h3423.md) them.

<a name="joshua_17_14"></a>Joshua 17:14

And the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) [dabar](../../strongs/h/h1696.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md), Why hast thou [nathan](../../strongs/h/h5414.md) me but ['echad](../../strongs/h/h259.md) [gôrāl](../../strongs/h/h1486.md) and ['echad](../../strongs/h/h259.md) [chebel](../../strongs/h/h2256.md) to [nachalah](../../strongs/h/h5159.md), seeing I am a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), forasmuch [ʿaḏ](../../strongs/h/h5704.md) [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) me [kô](../../strongs/h/h3541.md)?

<a name="joshua_17_15"></a>Joshua 17:15

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) them, If thou be a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), then get thee [ʿālâ](../../strongs/h/h5927.md) to the [yaʿar](../../strongs/h/h3293.md) country, and cut [bara'](../../strongs/h/h1254.md) for thyself there in the ['erets](../../strongs/h/h776.md) of the [Pᵊrizzî](../../strongs/h/h6522.md) and of the [rᵊp̄ā'îm](../../strongs/h/h7497.md), if [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) be too ['ûṣ](../../strongs/h/h213.md) for thee.

<a name="joshua_17_16"></a>Joshua 17:16

And the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) ['āmar](../../strongs/h/h559.md), The [har](../../strongs/h/h2022.md) is not [māṣā'](../../strongs/h/h4672.md) for us: and all the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of the [ʿēmeq](../../strongs/h/h6010.md) have [reḵeḇ](../../strongs/h/h7393.md) of [barzel](../../strongs/h/h1270.md), both they who are of [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md) and her [bath](../../strongs/h/h1323.md), and they who are of the [ʿēmeq](../../strongs/h/h6010.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="joshua_17_17"></a>Joshua 17:17

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md), even to ['Ep̄rayim](../../strongs/h/h669.md) and to [Mᵊnaššê](../../strongs/h/h4519.md), ['āmar](../../strongs/h/h559.md), Thou art a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), and hast [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md): thou shalt not have ['echad](../../strongs/h/h259.md) [gôrāl](../../strongs/h/h1486.md) only:

<a name="joshua_17_18"></a>Joshua 17:18

But the [har](../../strongs/h/h2022.md) shall be thine; for it is a [yaʿar](../../strongs/h/h3293.md), and thou shalt  [bara'](../../strongs/h/h1254.md) it: and the [tôṣā'ôṯ](../../strongs/h/h8444.md) of it shall be thine: for thou shalt [yarash](../../strongs/h/h3423.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), though they have [barzel](../../strongs/h/h1270.md) [reḵeḇ](../../strongs/h/h7393.md), and though they be [ḥāzāq](../../strongs/h/h2389.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 16](joshua_16.md) - [Joshua 18](joshua_18.md)