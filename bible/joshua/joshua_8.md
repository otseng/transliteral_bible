# [Joshua 8](https://www.blueletterbible.org/kjv/joshua/8)

<a name="joshua_8_1"></a>Joshua 8:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [yare'](../../strongs/h/h3372.md) not, neither be thou [ḥāṯaṯ](../../strongs/h/h2865.md): [laqach](../../strongs/h/h3947.md) all the ['am](../../strongs/h/h5971.md) of [milḥāmâ](../../strongs/h/h4421.md) with thee, and [quwm](../../strongs/h/h6965.md), [ʿālâ](../../strongs/h/h5927.md) to [ʿAy](../../strongs/h/h5857.md): [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) into thy [yad](../../strongs/h/h3027.md) the [melek](../../strongs/h/h4428.md) of [ʿAy](../../strongs/h/h5857.md), and his ['am](../../strongs/h/h5971.md), and his [ʿîr](../../strongs/h/h5892.md), and his ['erets](../../strongs/h/h776.md):

<a name="joshua_8_2"></a>Joshua 8:2

And thou shalt ['asah](../../strongs/h/h6213.md) to [ʿAy](../../strongs/h/h5857.md) and her [melek](../../strongs/h/h4428.md) as thou ['asah](../../strongs/h/h6213.md) unto [Yᵊrēḥô](../../strongs/h/h3405.md) and her [melek](../../strongs/h/h4428.md): only the [šālāl](../../strongs/h/h7998.md) thereof, and the [bĕhemah](../../strongs/h/h929.md) thereof, shall ye take for a [bāzaz](../../strongs/h/h962.md) unto yourselves: [śûm](../../strongs/h/h7760.md) thee an ['arab](../../strongs/h/h693.md) for the [ʿîr](../../strongs/h/h5892.md) ['aḥar](../../strongs/h/h310.md) it.

<a name="joshua_8_3"></a>Joshua 8:3

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [quwm](../../strongs/h/h6965.md), and all the ['am](../../strongs/h/h5971.md) of [milḥāmâ](../../strongs/h/h4421.md), to [ʿālâ](../../strongs/h/h5927.md) against [ʿAy](../../strongs/h/h5857.md): and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [bāḥar](../../strongs/h/h977.md) [šᵊlōšîm](../../strongs/h/h7970.md) ['elep̄](../../strongs/h/h505.md) [gibôr](../../strongs/h/h1368.md) ['iysh](../../strongs/h/h376.md) of [ḥayil](../../strongs/h/h2428.md), and [shalach](../../strongs/h/h7971.md) them by [layil](../../strongs/h/h3915.md).

<a name="joshua_8_4"></a>Joshua 8:4

And he [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md), ye shall ['arab](../../strongs/h/h693.md) against the [ʿîr](../../strongs/h/h5892.md), even ['aḥar](../../strongs/h/h310.md) the [ʿîr](../../strongs/h/h5892.md): [rachaq](../../strongs/h/h7368.md) not [me'od](../../strongs/h/h3966.md) [rachaq](../../strongs/h/h7368.md) from the [ʿîr](../../strongs/h/h5892.md), but be ye all [kuwn](../../strongs/h/h3559.md):

<a name="joshua_8_5"></a>Joshua 8:5

And I, and all the ['am](../../strongs/h/h5971.md) that are with me, will [qāraḇ](../../strongs/h/h7126.md) unto the [ʿîr](../../strongs/h/h5892.md): and it shall come to pass, when they [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) us, as at the [ri'šôn](../../strongs/h/h7223.md), that we will [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) them,

<a name="joshua_8_6"></a>Joshua 8:6

(For they will [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) us) till we have [nathaq](../../strongs/h/h5423.md) them from the [ʿîr](../../strongs/h/h5892.md); for they will ['āmar](../../strongs/h/h559.md), They [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) us, as at the [ri'šôn](../../strongs/h/h7223.md): therefore we will [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) them.

<a name="joshua_8_7"></a>Joshua 8:7

Then ye shall [quwm](../../strongs/h/h6965.md) from the ['arab](../../strongs/h/h693.md), and [yarash](../../strongs/h/h3423.md) upon the [ʿîr](../../strongs/h/h5892.md): for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) will [nathan](../../strongs/h/h5414.md) it into your [yad](../../strongs/h/h3027.md).

<a name="joshua_8_8"></a>Joshua 8:8

And it shall be, when ye have [tāp̄aś](../../strongs/h/h8610.md) the [ʿîr](../../strongs/h/h5892.md), that ye shall [yāṣaṯ](../../strongs/h/h3341.md) the [ʿîr](../../strongs/h/h5892.md) on ['esh](../../strongs/h/h784.md): according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) shall ye ['asah](../../strongs/h/h6213.md). [ra'ah](../../strongs/h/h7200.md), I have [tsavah](../../strongs/h/h6680.md) you.

<a name="joshua_8_9"></a>Joshua 8:9

[Yᵊhôšûaʿ](../../strongs/h/h3091.md) therefore [shalach](../../strongs/h/h7971.md) them: and they [yālaḵ](../../strongs/h/h3212.md) to [ma'ărāḇ](../../strongs/h/h3993.md), and [yashab](../../strongs/h/h3427.md) between [Bêṯ-'ēl](../../strongs/h/h1008.md) and [ʿAy](../../strongs/h/h5857.md), on the [yam](../../strongs/h/h3220.md) of [ʿAy](../../strongs/h/h5857.md): but [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [lûn](../../strongs/h/h3885.md) that [layil](../../strongs/h/h3915.md) [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md).

<a name="joshua_8_10"></a>Joshua 8:10

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [paqad](../../strongs/h/h6485.md) the ['am](../../strongs/h/h5971.md), and went [ʿālâ](../../strongs/h/h5927.md), he and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md) to [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_8_11"></a>Joshua 8:11

And all the ['am](../../strongs/h/h5971.md), even the people of [milḥāmâ](../../strongs/h/h4421.md) that were with him, [ʿālâ](../../strongs/h/h5927.md), and [nāḡaš](../../strongs/h/h5066.md), and [bow'](../../strongs/h/h935.md) before the [ʿîr](../../strongs/h/h5892.md), and [ḥānâ](../../strongs/h/h2583.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md) of [ʿAy](../../strongs/h/h5857.md): now there was a [gay'](../../strongs/h/h1516.md) between them and [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_8_12"></a>Joshua 8:12

And he [laqach](../../strongs/h/h3947.md) about [ḥāmēš](../../strongs/h/h2568.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md), and [śûm](../../strongs/h/h7760.md) them to ['arab](../../strongs/h/h693.md) between [Bêṯ-'ēl](../../strongs/h/h1008.md) and [ʿAy](../../strongs/h/h5857.md), on the [yam](../../strongs/h/h3220.md) of the [ʿAy](../../strongs/h/h5857.md) [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_8_13"></a>Joshua 8:13

And when they had [śûm](../../strongs/h/h7760.md) the ['am](../../strongs/h/h5971.md), even all the [maḥănê](../../strongs/h/h4264.md) that was on the [ṣāp̄ôn](../../strongs/h/h6828.md) of the [ʿîr](../../strongs/h/h5892.md), and their liers in ['aqeb](../../strongs/h/h6119.md) on the [yam](../../strongs/h/h3220.md) of the [ʿîr](../../strongs/h/h5892.md), [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [yālaḵ](../../strongs/h/h3212.md) that [layil](../../strongs/h/h3915.md) into the [tavek](../../strongs/h/h8432.md) of the [ʿēmeq](../../strongs/h/h6010.md).

<a name="joshua_8_14"></a>Joshua 8:14

And it came to pass, when the [melek](../../strongs/h/h4428.md) of [ʿAy](../../strongs/h/h5857.md) [ra'ah](../../strongs/h/h7200.md) it, that they [māhar](../../strongs/h/h4116.md) and [šāḵam](../../strongs/h/h7925.md), and the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md) to [milḥāmâ](../../strongs/h/h4421.md), he and all his ['am](../../strongs/h/h5971.md), at a time [môʿēḏ](../../strongs/h/h4150.md), [paniym](../../strongs/h/h6440.md) the ['arabah](../../strongs/h/h6160.md); but he [yada'](../../strongs/h/h3045.md) not that there were ['arab](../../strongs/h/h693.md) against him ['aḥar](../../strongs/h/h310.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_8_15"></a>Joshua 8:15

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and all [Yisra'el](../../strongs/h/h3478.md) made as if they were [naga'](../../strongs/h/h5060.md) [paniym](../../strongs/h/h6440.md) them, and [nûs](../../strongs/h/h5127.md) by the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md).

<a name="joshua_8_16"></a>Joshua 8:16

And all the ['am](../../strongs/h/h5971.md) that were in [ʿAy](../../strongs/h/h5857.md) [ʿîr](../../strongs/h/h5892.md) were [zāʿaq](../../strongs/h/h2199.md) together to [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them: and they [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and were [nathaq](../../strongs/h/h5423.md) from the [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_8_17"></a>Joshua 8:17

And there was not an ['iysh](../../strongs/h/h376.md) [šā'ar](../../strongs/h/h7604.md) in [ʿAy](../../strongs/h/h5857.md) or [Bêṯ-'ēl](../../strongs/h/h1008.md), that went not [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) [Yisra'el](../../strongs/h/h3478.md): and they ['azab](../../strongs/h/h5800.md) the [ʿîr](../../strongs/h/h5892.md) [pāṯaḥ](../../strongs/h/h6605.md), and [radaph](../../strongs/h/h7291.md) after [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_8_18"></a>Joshua 8:18

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [natah](../../strongs/h/h5186.md) the [kîḏôn](../../strongs/h/h3591.md) that is in thy [yad](../../strongs/h/h3027.md) toward [ʿAy](../../strongs/h/h5857.md); for I will [nathan](../../strongs/h/h5414.md) it into thine [yad](../../strongs/h/h3027.md). And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [natah](../../strongs/h/h5186.md) the [kîḏôn](../../strongs/h/h3591.md) that he had in his [yad](../../strongs/h/h3027.md) toward the [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_8_19"></a>Joshua 8:19

And the ['arab](../../strongs/h/h693.md) [quwm](../../strongs/h/h6965.md) [mᵊhērâ](../../strongs/h/h4120.md) out of their [maqowm](../../strongs/h/h4725.md), and they [rûṣ](../../strongs/h/h7323.md) as soon as he had [natah](../../strongs/h/h5186.md) his [yad](../../strongs/h/h3027.md): and they [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, and [māhar](../../strongs/h/h4116.md) and [yāṣaṯ](../../strongs/h/h3341.md) the [ʿîr](../../strongs/h/h5892.md) on ['esh](../../strongs/h/h784.md).

<a name="joshua_8_20"></a>Joshua 8:20

And when the ['enowsh](../../strongs/h/h582.md) of [ʿAy](../../strongs/h/h5857.md) [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md) them, they [ra'ah](../../strongs/h/h7200.md), and, behold, the ['ashan](../../strongs/h/h6227.md) of the [ʿîr](../../strongs/h/h5892.md) [ʿālâ](../../strongs/h/h5927.md) to [shamayim](../../strongs/h/h8064.md), and they had no [yad](../../strongs/h/h3027.md) to [nûs](../../strongs/h/h5127.md) this way or that [hēnnâ](../../strongs/h/h2008.md): and the ['am](../../strongs/h/h5971.md) that [nûs](../../strongs/h/h5127.md) to the [midbar](../../strongs/h/h4057.md) turned [hāp̄aḵ](../../strongs/h/h2015.md) upon the [radaph](../../strongs/h/h7291.md).

<a name="joshua_8_21"></a>Joshua 8:21

And when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and all [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that the ['arab](../../strongs/h/h693.md) had [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md), and that the ['ashan](../../strongs/h/h6227.md) of the [ʿîr](../../strongs/h/h5892.md) [ʿālâ](../../strongs/h/h5927.md), then they [shuwb](../../strongs/h/h7725.md), and [nakah](../../strongs/h/h5221.md) the ['enowsh](../../strongs/h/h582.md) of [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_8_22"></a>Joshua 8:22

And the ['ēllê](../../strongs/h/h428.md) [yāṣā'](../../strongs/h/h3318.md) of the [ʿîr](../../strongs/h/h5892.md) [qārā'](../../strongs/h/h7125.md) them; so they were in the [tavek](../../strongs/h/h8432.md) of [Yisra'el](../../strongs/h/h3478.md), some on this side, and some on that side: and they [nakah](../../strongs/h/h5221.md) them, so [ʿaḏ](../../strongs/h/h5704.md) they [šā'ar](../../strongs/h/h7604.md) none of them [śārîḏ](../../strongs/h/h8300.md) or [pālîṭ](../../strongs/h/h6412.md).

<a name="joshua_8_23"></a>Joshua 8:23

And the [melek](../../strongs/h/h4428.md) of [ʿAy](../../strongs/h/h5857.md) they [tāp̄aś](../../strongs/h/h8610.md) [chay](../../strongs/h/h2416.md), and [qāraḇ](../../strongs/h/h7126.md) him to [Yᵊhôšûaʿ](../../strongs/h/h3091.md).

<a name="joshua_8_24"></a>Joshua 8:24

And it came to pass, when [Yisra'el](../../strongs/h/h3478.md) had made a [kalah](../../strongs/h/h3615.md) of [harag](../../strongs/h/h2026.md) all the [yashab](../../strongs/h/h3427.md) of [ʿAy](../../strongs/h/h5857.md) in the [sadeh](../../strongs/h/h7704.md), in the [midbar](../../strongs/h/h4057.md) wherein they [radaph](../../strongs/h/h7291.md) them, and when they were all [naphal](../../strongs/h/h5307.md) on the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), until they were [tamam](../../strongs/h/h8552.md), that all the [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md) unto [ʿAy](../../strongs/h/h5857.md), and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="joshua_8_25"></a>Joshua 8:25

And so it was, that all that [naphal](../../strongs/h/h5307.md) that [yowm](../../strongs/h/h3117.md), both of ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), were [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['elep̄](../../strongs/h/h505.md), even all the ['enowsh](../../strongs/h/h582.md) of [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_8_26"></a>Joshua 8:26

For [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shuwb](../../strongs/h/h7725.md) not his [yad](../../strongs/h/h3027.md) [shuwb](../../strongs/h/h7725.md), wherewith he [natah](../../strongs/h/h5186.md) the [kîḏôn](../../strongs/h/h3591.md), until he had [ḥāram](../../strongs/h/h2763.md) all the [yashab](../../strongs/h/h3427.md) of [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_8_27"></a>Joshua 8:27

Only the [bĕhemah](../../strongs/h/h929.md) and the [šālāl](../../strongs/h/h7998.md) of that [ʿîr](../../strongs/h/h5892.md) [Yisra'el](../../strongs/h/h3478.md) took for a [bāzaz](../../strongs/h/h962.md) unto themselves, according unto the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which he [tsavah](../../strongs/h/h6680.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md).

<a name="joshua_8_28"></a>Joshua 8:28

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [śārap̄](../../strongs/h/h8313.md) [ʿAy](../../strongs/h/h5857.md), and [śûm](../../strongs/h/h7760.md) it a [tēl](../../strongs/h/h8510.md) ['owlam](../../strongs/h/h5769.md), even a [šᵊmāmâ](../../strongs/h/h8077.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_8_29"></a>Joshua 8:29

And the [melek](../../strongs/h/h4428.md) of [ʿAy](../../strongs/h/h5857.md) he [tālâ](../../strongs/h/h8518.md) on an ['ets](../../strongs/h/h6086.md) until [ʿēṯ](../../strongs/h/h6256.md) ['ereb](../../strongs/h/h6153.md): and as soon as the [šemeš](../../strongs/h/h8121.md) was [bow'](../../strongs/h/h935.md), [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [tsavah](../../strongs/h/h6680.md) that they should [yarad](../../strongs/h/h3381.md) his [nᵊḇēlâ](../../strongs/h/h5038.md) [yarad](../../strongs/h/h3381.md) from the ['ets](../../strongs/h/h6086.md), and [shalak](../../strongs/h/h7993.md) it at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md), and [quwm](../../strongs/h/h6965.md) thereon a [gadowl](../../strongs/h/h1419.md) [gal](../../strongs/h/h1530.md) of ['eben](../../strongs/h/h68.md), that remaineth unto this [yowm](../../strongs/h/h3117.md).

<a name="joshua_8_30"></a>Joshua 8:30

Then [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) in [har](../../strongs/h/h2022.md) [ʿÊḇāl](../../strongs/h/h5858.md),

<a name="joshua_8_31"></a>Joshua 8:31

As [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), a [mizbeach](../../strongs/h/h4196.md) of [šālēm](../../strongs/h/h8003.md) ['eben](../../strongs/h/h68.md), over which no man hath lift [nûp̄](../../strongs/h/h5130.md) any [barzel](../../strongs/h/h1270.md): and they [ʿālâ](../../strongs/h/h5927.md) thereon [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [zabach](../../strongs/h/h2076.md) [šelem](../../strongs/h/h8002.md).

<a name="joshua_8_32"></a>Joshua 8:32

And he [kāṯaḇ](../../strongs/h/h3789.md) there upon the ['eben](../../strongs/h/h68.md) a [mišnê](../../strongs/h/h4932.md) of the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), which he [kāṯaḇ](../../strongs/h/h3789.md) in the [paniym](../../strongs/h/h6440.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_8_33"></a>Joshua 8:33

And all [Yisra'el](../../strongs/h/h3478.md), and their [zāqēn](../../strongs/h/h2205.md), and [šāṭar](../../strongs/h/h7860.md), and their [shaphat](../../strongs/h/h8199.md), ['amad](../../strongs/h/h5975.md) on this side the ['ārôn](../../strongs/h/h727.md) and on that side before the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md), which [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), as well the [ger](../../strongs/h/h1616.md), as he that was ['ezrāḥ](../../strongs/h/h249.md) among them; [ḥēṣî](../../strongs/h/h2677.md) of them ['ēl](../../strongs/h/h413.md) [môl](../../strongs/h/h4136.md) [har](../../strongs/h/h2022.md) [Gᵊrizzîm](../../strongs/h/h1630.md), and [ḥēṣî](../../strongs/h/h2677.md) of them over [môl](../../strongs/h/h4136.md) [har](../../strongs/h/h2022.md) [ʿÊḇāl](../../strongs/h/h5858.md); as [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) [ri'šôn](../../strongs/h/h7223.md), that they should [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_8_34"></a>Joshua 8:34

And ['aḥar](../../strongs/h/h310.md) he [qara'](../../strongs/h/h7121.md) all the [dabar](../../strongs/h/h1697.md) of the [towrah](../../strongs/h/h8451.md), the [bĕrakah](../../strongs/h/h1293.md) and [qᵊlālâ](../../strongs/h/h7045.md), according to all that is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md).

<a name="joshua_8_35"></a>Joshua 8:35

There was not a [dabar](../../strongs/h/h1697.md) of all that [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md), which [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) not before all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), with the ['ishshah](../../strongs/h/h802.md), and the [ṭap̄](../../strongs/h/h2945.md), and the [ger](../../strongs/h/h1616.md) that were [halak](../../strongs/h/h1980.md) [qereḇ](../../strongs/h/h7130.md) them.

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 7](joshua_7.md) - [Joshua 9](joshua_9.md)