# [Joshua 14](https://www.blueletterbible.org/kjv/joshua/14)

<a name="joshua_14_1"></a>Joshua 14:1

And these which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nāḥal](../../strongs/h/h5157.md) in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), and the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), distributed for [nāḥal](../../strongs/h/h5157.md) to them.

<a name="joshua_14_2"></a>Joshua 14:2

By [gôrāl](../../strongs/h/h1486.md) was their [nachalah](../../strongs/h/h5159.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md), for the [tēšaʿ](../../strongs/h/h8672.md) [maṭṭê](../../strongs/h/h4294.md), and for the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md).

<a name="joshua_14_3"></a>Joshua 14:3

For [Mōshe](../../strongs/h/h4872.md) had [nathan](../../strongs/h/h5414.md) the [nachalah](../../strongs/h/h5159.md) of [šᵊnayim](../../strongs/h/h8147.md) [maṭṭê](../../strongs/h/h4294.md) and an [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md): but unto the [Lᵊvî](../../strongs/h/h3881.md) he [nathan](../../strongs/h/h5414.md) none [nachalah](../../strongs/h/h5159.md) [tavek](../../strongs/h/h8432.md) them.

<a name="joshua_14_4"></a>Joshua 14:4

For the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) were [šᵊnayim](../../strongs/h/h8147.md) [maṭṭê](../../strongs/h/h4294.md), [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md): therefore they [nathan](../../strongs/h/h5414.md) no [cheleq](../../strongs/h/h2506.md) unto the [Lᵊvî](../../strongs/h/h3881.md) in the ['erets](../../strongs/h/h776.md), save [ʿîr](../../strongs/h/h5892.md) to [yashab](../../strongs/h/h3427.md) in, with their [miḡrāš](../../strongs/h/h4054.md) for their [miqnê](../../strongs/h/h4735.md) and for their [qinyān](../../strongs/h/h7075.md).

<a name="joshua_14_5"></a>Joshua 14:5

As [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md), so the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md), and they [chalaq](../../strongs/h/h2505.md) the ['erets](../../strongs/h/h776.md).

<a name="joshua_14_6"></a>Joshua 14:6

Then the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [nāḡaš](../../strongs/h/h5066.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md) in [Gilgāl](../../strongs/h/h1537.md): and [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) the [qᵊnizzî](../../strongs/h/h7074.md) ['āmar](../../strongs/h/h559.md) unto him, Thou [yada'](../../strongs/h/h3045.md) the [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) concerning ['ôḏôṯ](../../strongs/h/h182.md) and ['ôḏôṯ](../../strongs/h/h182.md) in [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md).

<a name="joshua_14_7"></a>Joshua 14:7

['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was I when [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) me from [Qāḏēš Barnēaʿ](../../strongs/h/h6947.md) to espy [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md); and I [shuwb](../../strongs/h/h7725.md) him [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md) as it was in mine [lebab](../../strongs/h/h3824.md).

<a name="joshua_14_8"></a>Joshua 14:8

Nevertheless my ['ach](../../strongs/h/h251.md) that [ʿālâ](../../strongs/h/h5927.md) with me made the [leb](../../strongs/h/h3820.md) of the ['am](../../strongs/h/h5971.md) [macah](../../strongs/h/h4529.md): but I [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_14_9"></a>Joshua 14:9

And [Mōshe](../../strongs/h/h4872.md) [shaba'](../../strongs/h/h7650.md) on that [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md), Surely the ['erets](../../strongs/h/h776.md) whereon thy [regel](../../strongs/h/h7272.md) have [dāraḵ](../../strongs/h/h1869.md) shall be thine [nachalah](../../strongs/h/h5159.md), and thy [ben](../../strongs/h/h1121.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md), because thou hast [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_14_10"></a>Joshua 14:10

And now, behold, [Yĕhovah](../../strongs/h/h3068.md) hath [ḥāyâ](../../strongs/h/h2421.md) me, as he [dabar](../../strongs/h/h1696.md), these ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md) [šānâ](../../strongs/h/h8141.md), even ['āz](../../strongs/h/h227.md) [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) this [dabar](../../strongs/h/h1697.md) unto [Mōshe](../../strongs/h/h4872.md), while the children of [Yisra'el](../../strongs/h/h3478.md) [halak](../../strongs/h/h1980.md) in the [midbar](../../strongs/h/h4057.md): and now, lo, I am this [yowm](../../strongs/h/h3117.md) [šᵊmōnîm](../../strongs/h/h8084.md) and [ḥāmēš](../../strongs/h/h2568.md) [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md).

<a name="joshua_14_11"></a>Joshua 14:11

As yet I am as [ḥāzāq](../../strongs/h/h2389.md) this [yowm](../../strongs/h/h3117.md) as I was in the [yowm](../../strongs/h/h3117.md) that [Mōshe](../../strongs/h/h4872.md) [shalach](../../strongs/h/h7971.md) me: as my [koach](../../strongs/h/h3581.md) was then, even so is my [koach](../../strongs/h/h3581.md) now, for [milḥāmâ](../../strongs/h/h4421.md), both to [yāṣā'](../../strongs/h/h3318.md), and to [bow'](../../strongs/h/h935.md).

<a name="joshua_14_12"></a>Joshua 14:12

Now therefore [nathan](../../strongs/h/h5414.md) me this [har](../../strongs/h/h2022.md), whereof [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) in that [yowm](../../strongs/h/h3117.md); for thou [shama'](../../strongs/h/h8085.md) in that [yowm](../../strongs/h/h3117.md) how the [ʿĂnāqîm](../../strongs/h/h6062.md) were there, and that the [ʿîr](../../strongs/h/h5892.md) were [gadowl](../../strongs/h/h1419.md) and [bāṣar](../../strongs/h/h1219.md): if so ['ûlay](../../strongs/h/h194.md) [Yĕhovah](../../strongs/h/h3068.md) will be ['ēṯ](../../strongs/h/h854.md) me, then I shall be able to [yarash](../../strongs/h/h3423.md) them, as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md).

<a name="joshua_14_13"></a>Joshua 14:13

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [barak](../../strongs/h/h1288.md) him, and [nathan](../../strongs/h/h5414.md) unto [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) [Ḥeḇrôn](../../strongs/h/h2275.md) for a [nachalah](../../strongs/h/h5159.md).

<a name="joshua_14_14"></a>Joshua 14:14

[Ḥeḇrôn](../../strongs/h/h2275.md) therefore became the [nachalah](../../strongs/h/h5159.md) of [Kālēḇ](../../strongs/h/h3612.md) the [ben](../../strongs/h/h1121.md) of [Yᵊp̄unnê](../../strongs/h/h3312.md) the [qᵊnizzî](../../strongs/h/h7074.md) unto this [yowm](../../strongs/h/h3117.md), because that he [mālā'](../../strongs/h/h4390.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_14_15"></a>Joshua 14:15

And the [shem](../../strongs/h/h8034.md) of [Ḥeḇrôn](../../strongs/h/h2275.md) [paniym](../../strongs/h/h6440.md) was [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md); a [gadowl](../../strongs/h/h1419.md) ['āḏām](../../strongs/h/h120.md) among the [ʿĂnāqîm](../../strongs/h/h6062.md). And the ['erets](../../strongs/h/h776.md) had [šāqaṭ](../../strongs/h/h8252.md) from [milḥāmâ](../../strongs/h/h4421.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 13](joshua_13.md) - [Joshua 15](joshua_15.md)