# [Joshua 1](https://www.blueletterbible.org/kjv/joshua/1)

<a name="joshua_1_1"></a>Joshua 1:1

Now ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) it came to pass, that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), [Mōshe](../../strongs/h/h4872.md) [sharath](../../strongs/h/h8334.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_1_2"></a>Joshua 1:2

[Mōshe](../../strongs/h/h4872.md) my ['ebed](../../strongs/h/h5650.md) is [muwth](../../strongs/h/h4191.md); now therefore [quwm](../../strongs/h/h6965.md), ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md), thou, and all this ['am](../../strongs/h/h5971.md), unto the ['erets](../../strongs/h/h776.md) which I do [nathan](../../strongs/h/h5414.md) to them, even to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_1_3"></a>Joshua 1:3

Every [maqowm](../../strongs/h/h4725.md) that the [kaph](../../strongs/h/h3709.md) of your [regel](../../strongs/h/h7272.md) shall tread [dāraḵ](../../strongs/h/h1869.md), that have I [nathan](../../strongs/h/h5414.md) unto you, as I [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_1_4"></a>Joshua 1:4

From the [midbar](../../strongs/h/h4057.md) and this [Lᵊḇānôn](../../strongs/h/h3844.md) even unto the [gadowl](../../strongs/h/h1419.md) [nāhār](../../strongs/h/h5104.md), the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md), all the ['erets](../../strongs/h/h776.md) of the [Ḥitî](../../strongs/h/h2850.md), and unto the [gadowl](../../strongs/h/h1419.md) [yam](../../strongs/h/h3220.md) toward the [māḇô'](../../strongs/h/h3996.md) of the [šemeš](../../strongs/h/h8121.md), shall be your [gᵊḇûl](../../strongs/h/h1366.md).

<a name="joshua_1_5"></a>Joshua 1:5

There shall not any ['iysh](../../strongs/h/h376.md) be able to [yatsab](../../strongs/h/h3320.md) [paniym](../../strongs/h/h6440.md) thee all the [yowm](../../strongs/h/h3117.md) of thy [chay](../../strongs/h/h2416.md): as I was with [Mōshe](../../strongs/h/h4872.md), so I will be with thee: I will not [rāp̄â](../../strongs/h/h7503.md) thee, nor ['azab](../../strongs/h/h5800.md) thee.

<a name="joshua_1_6"></a>Joshua 1:6

Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md): for unto this ['am](../../strongs/h/h5971.md) shalt thou divide for a [nāḥal](../../strongs/h/h5157.md) the ['erets](../../strongs/h/h776.md), which I [shaba'](../../strongs/h/h7650.md) unto their ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) them.

<a name="joshua_1_7"></a>Joshua 1:7

Only be thou [ḥāzaq](../../strongs/h/h2388.md) and [me'od](../../strongs/h/h3966.md) ['amats](../../strongs/h/h553.md), that thou mayest [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) according to all the [towrah](../../strongs/h/h8451.md), which [Mōshe](../../strongs/h/h4872.md) my ['ebed](../../strongs/h/h5650.md) [tsavah](../../strongs/h/h6680.md) thee: [cuwr](../../strongs/h/h5493.md) not from it to the [yamiyn](../../strongs/h/h3225.md) or to the [śᵊmō'l](../../strongs/h/h8040.md), that thou mayest [sakal](../../strongs/h/h7919.md) whithersoever thou [yālaḵ](../../strongs/h/h3212.md).

<a name="joshua_1_8"></a>Joshua 1:8

This [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) shall not [mûš](../../strongs/h/h4185.md) out of thy [peh](../../strongs/h/h6310.md); but thou shalt [hagah](../../strongs/h/h1897.md) therein [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), that thou mayest [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) according to all that is [kāṯaḇ](../../strongs/h/h3789.md) therein: for then thou shalt [tsalach](../../strongs/h/h6743.md) thy [derek](../../strongs/h/h1870.md) [tsalach](../../strongs/h/h6743.md), and then thou shalt have [sakal](../../strongs/h/h7919.md).

<a name="joshua_1_9"></a>Joshua 1:9

Have not I [tsavah](../../strongs/h/h6680.md) thee? Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md); be not [ʿāraṣ](../../strongs/h/h6206.md), neither be thou [ḥāṯaṯ](../../strongs/h/h2865.md): for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) is with thee whithersoever thou [yālaḵ](../../strongs/h/h3212.md).

<a name="joshua_1_10"></a>Joshua 1:10

Then [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [tsavah](../../strongs/h/h6680.md) the [šāṭar](../../strongs/h/h7860.md) of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_1_11"></a>Joshua 1:11

['abar](../../strongs/h/h5674.md) [qereḇ](../../strongs/h/h7130.md) the [maḥănê](../../strongs/h/h4264.md), and [tsavah](../../strongs/h/h6680.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [kuwn](../../strongs/h/h3559.md) you [ṣêḏâ](../../strongs/h/h6720.md); for within [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md) ye shall ['abar](../../strongs/h/h5674.md) this [Yardēn](../../strongs/h/h3383.md), to go [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md), which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) you to [yarash](../../strongs/h/h3423.md) it.

<a name="joshua_1_12"></a>Joshua 1:12

And to the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and to the [Gāḏî](../../strongs/h/h1425.md), and to [ḥēṣî](../../strongs/h/h2677.md) the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), ['āmar](../../strongs/h/h559.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_1_13"></a>Joshua 1:13

[zakar](../../strongs/h/h2142.md) the [dabar](../../strongs/h/h1697.md) which [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) you, ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath given you [nuwach](../../strongs/h/h5117.md), and hath [nathan](../../strongs/h/h5414.md) you this ['erets](../../strongs/h/h776.md).

<a name="joshua_1_14"></a>Joshua 1:14

Your ['ishshah](../../strongs/h/h802.md), your little [ṭap̄](../../strongs/h/h2945.md), and your [miqnê](../../strongs/h/h4735.md), shall [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) which [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) you on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md); but ye shall ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) your ['ach](../../strongs/h/h251.md) [ḥāmaš](../../strongs/h/h2571.md), all the [gibôr](../../strongs/h/h1368.md) men of [ḥayil](../../strongs/h/h2428.md), and [ʿāzar](../../strongs/h/h5826.md) them;

<a name="joshua_1_15"></a>Joshua 1:15

Until [Yĕhovah](../../strongs/h/h3068.md) have [nuwach](../../strongs/h/h5117.md) your ['ach](../../strongs/h/h251.md) [nuwach](../../strongs/h/h5117.md), as he hath given you, and they also have [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) which [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) them: then ye shall [shuwb](../../strongs/h/h7725.md) unto the ['erets](../../strongs/h/h776.md) of your [yᵊruššâ](../../strongs/h/h3425.md), and [yarash](../../strongs/h/h3423.md) it, which [Mōshe](../../strongs/h/h4872.md) [Yĕhovah](../../strongs/h/h3068.md) ['ebed](../../strongs/h/h5650.md) [nathan](../../strongs/h/h5414.md) you on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md).

<a name="joshua_1_16"></a>Joshua 1:16

And they ['anah](../../strongs/h/h6030.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md), All that thou [tsavah](../../strongs/h/h6680.md) us we will ['asah](../../strongs/h/h6213.md), and ['ăšer](../../strongs/h/h834.md) thou [shalach](../../strongs/h/h7971.md) us, we will [yālaḵ](../../strongs/h/h3212.md).

<a name="joshua_1_17"></a>Joshua 1:17

According as we [shama'](../../strongs/h/h8085.md) unto [Mōshe](../../strongs/h/h4872.md) in all things, so will we [shama'](../../strongs/h/h8085.md) unto thee: only [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) be with thee, as he was with [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_1_18"></a>Joshua 1:18

['iysh](../../strongs/h/h376.md) he be that doth [marah](../../strongs/h/h4784.md) against thy [peh](../../strongs/h/h6310.md), and will not [shama'](../../strongs/h/h8085.md) unto thy [dabar](../../strongs/h/h1697.md) in all that thou [tsavah](../../strongs/h/h6680.md) him, he shall be put to [muwth](../../strongs/h/h4191.md): only be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 2](joshua_2.md)