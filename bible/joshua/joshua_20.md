# [Joshua 20](https://www.blueletterbible.org/kjv/joshua/20)

<a name="joshua_20_1"></a>Joshua 20:1

[Yĕhovah](../../strongs/h/h3068.md) also [dabar](../../strongs/h/h1696.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), ['āmar](../../strongs/h/h559.md),

<a name="joshua_20_2"></a>Joshua 20:2

[dabar](../../strongs/h/h1696.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) for you [ʿîr](../../strongs/h/h5892.md) of [miqlāṭ](../../strongs/h/h4733.md), whereof I [dabar](../../strongs/h/h1696.md) unto you by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md):

<a name="joshua_20_3"></a>Joshua 20:3

That the [ratsach](../../strongs/h/h7523.md) that [nakah](../../strongs/h/h5221.md) any [nephesh](../../strongs/h/h5315.md) [šᵊḡāḡâ](../../strongs/h/h7684.md) and [da'ath](../../strongs/h/h1847.md) may [nûs](../../strongs/h/h5127.md) thither: and they shall be your [miqlāṭ](../../strongs/h/h4733.md) from the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md).

<a name="joshua_20_4"></a>Joshua 20:4

And when he that doth [nûs](../../strongs/h/h5127.md) unto ['echad](../../strongs/h/h259.md) of those [ʿîr](../../strongs/h/h5892.md) shall ['amad](../../strongs/h/h5975.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md), and shall [dabar](../../strongs/h/h1696.md) his [dabar](../../strongs/h/h1697.md) in the ['ozen](../../strongs/h/h241.md) of the [zāqēn](../../strongs/h/h2205.md) of that [ʿîr](../../strongs/h/h5892.md), they shall ['āsap̄](../../strongs/h/h622.md) him into the [ʿîr](../../strongs/h/h5892.md) unto them, and [nathan](../../strongs/h/h5414.md) him a [maqowm](../../strongs/h/h4725.md), that he may [yashab](../../strongs/h/h3427.md) among them.

<a name="joshua_20_5"></a>Joshua 20:5

And if the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) him, then they shall not [cagar](../../strongs/h/h5462.md) the [ratsach](../../strongs/h/h7523.md) up into his [yad](../../strongs/h/h3027.md); because he [nakah](../../strongs/h/h5221.md) his [rea'](../../strongs/h/h7453.md) [bᵊlî](../../strongs/h/h1097.md) [da'ath](../../strongs/h/h1847.md), and [sane'](../../strongs/h/h8130.md) him not [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md).

<a name="joshua_20_6"></a>Joshua 20:6

And he shall [yashab](../../strongs/h/h3427.md) in that [ʿîr](../../strongs/h/h5892.md), until he ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['edah](../../strongs/h/h5712.md) for [mishpat](../../strongs/h/h4941.md), and until the [maveth](../../strongs/h/h4194.md) of the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) ['ăšer](../../strongs/h/h834.md) shall be in those [yowm](../../strongs/h/h3117.md): then shall the [ratsach](../../strongs/h/h7523.md) [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) unto his own [ʿîr](../../strongs/h/h5892.md), and unto his own [bayith](../../strongs/h/h1004.md), unto the [ʿîr](../../strongs/h/h5892.md) from whence he [nûs](../../strongs/h/h5127.md).

<a name="joshua_20_7"></a>Joshua 20:7

And they [qadash](../../strongs/h/h6942.md) [Qeḏeš](../../strongs/h/h6943.md) in [Gālîl](../../strongs/h/h1551.md) in [har](../../strongs/h/h2022.md) [Nap̄tālî](../../strongs/h/h5321.md), and [Šᵊḵem](../../strongs/h/h7927.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md), which is [Ḥeḇrôn](../../strongs/h/h2275.md), in the [har](../../strongs/h/h2022.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="joshua_20_8"></a>Joshua 20:8

And on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) by [Yᵊrēḥô](../../strongs/h/h3405.md) [mizrach](../../strongs/h/h4217.md), they [nathan](../../strongs/h/h5414.md) [Beṣer](../../strongs/h/h1221.md) in the [midbar](../../strongs/h/h4057.md) upon the [mîšôr](../../strongs/h/h4334.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and [Rā'Môṯ](../../strongs/h/h7216.md) in [Gilʿāḏ](../../strongs/h/h1568.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), and [Gôlān](../../strongs/h/h1474.md) in [Bāšān](../../strongs/h/h1316.md) out of the [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="joshua_20_9"></a>Joshua 20:9

These were the [ʿîr](../../strongs/h/h5892.md) [mûʿāḏâ](../../strongs/h/h4152.md) for all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and for the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) them, that whosoever [nakah](../../strongs/h/h5221.md) any [nephesh](../../strongs/h/h5315.md) at [šᵊḡāḡâ](../../strongs/h/h7684.md) might [nûs](../../strongs/h/h5127.md) thither, and not [muwth](../../strongs/h/h4191.md) by the [yad](../../strongs/h/h3027.md) of the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md), until he ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['edah](../../strongs/h/h5712.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 19](joshua_19.md) - [Joshua 21](joshua_21.md)