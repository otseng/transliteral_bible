# [Joshua 12](https://www.blueletterbible.org/kjv/joshua/12)

<a name="joshua_12_1"></a>Joshua 12:1

Now these are the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md), and [yarash](../../strongs/h/h3423.md) their ['erets](../../strongs/h/h776.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) toward the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md), from the [nachal](../../strongs/h/h5158.md) ['arnôn](../../strongs/h/h769.md) unto [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md), and all the ['arabah](../../strongs/h/h6160.md) on the [mizrach](../../strongs/h/h4217.md):

<a name="joshua_12_2"></a>Joshua 12:2

[Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), who [yashab](../../strongs/h/h3427.md) in [Hešbôn](../../strongs/h/h2809.md), and [mashal](../../strongs/h/h4910.md) from [ʿĂrôʿēr](../../strongs/h/h6177.md), which is upon the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) ['arnôn](../../strongs/h/h769.md), and from the [tavek](../../strongs/h/h8432.md) of the [nachal](../../strongs/h/h5158.md), and from [ḥēṣî](../../strongs/h/h2677.md) [Gilʿāḏ](../../strongs/h/h1568.md), even unto the [nachal](../../strongs/h/h5158.md) [Yabōq](../../strongs/h/h2999.md), which is the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md);

<a name="joshua_12_3"></a>Joshua 12:3

And from the ['arabah](../../strongs/h/h6160.md) to the [yam](../../strongs/h/h3220.md) of [Kinnᵊrôṯ](../../strongs/h/h3672.md) on the [mizrach](../../strongs/h/h4217.md), and unto the [yam](../../strongs/h/h3220.md) of the ['arabah](../../strongs/h/h6160.md), even the [melaḥ](../../strongs/h/h4417.md) [yam](../../strongs/h/h3220.md) on the [mizrach](../../strongs/h/h4217.md), the [derek](../../strongs/h/h1870.md) to [Bêṯ Hayšîmôṯ](../../strongs/h/h1020.md); and from the [têmān](../../strongs/h/h8486.md), under [ʼAshdôwth hap-Piçgâh](../../strongs/h/h798.md) ['ăšēḏâ](../../strongs/h/h794.md):

<a name="joshua_12_4"></a>Joshua 12:4

And the [gᵊḇûl](../../strongs/h/h1366.md) of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), which was of the [yeṯer](../../strongs/h/h3499.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md), that [yashab](../../strongs/h/h3427.md) at [ʿAštārōṯ](../../strongs/h/h6252.md) and at ['Eḏrᵊʿî](../../strongs/h/h154.md),

<a name="joshua_12_5"></a>Joshua 12:5

And [mashal](../../strongs/h/h4910.md) in [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md), and in [Salḵâ](../../strongs/h/h5548.md), and in all [Bāšān](../../strongs/h/h1316.md), unto the [gᵊḇûl](../../strongs/h/h1366.md) of the [Gᵊšûrî](../../strongs/h/h1651.md) and the [Maʿăḵāṯî](../../strongs/h/h4602.md), and [ḥēṣî](../../strongs/h/h2677.md) [Gilʿāḏ](../../strongs/h/h1568.md), the [gᵊḇûl](../../strongs/h/h1366.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md).

<a name="joshua_12_6"></a>Joshua 12:6

Them did [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md): and [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) it for a [yᵊruššâ](../../strongs/h/h3425.md) unto the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and the [Gāḏî](../../strongs/h/h1425.md), and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="joshua_12_7"></a>Joshua 12:7

And these are the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) which [Yᵊhôšûaʿ](../../strongs/h/h3091.md) and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nakah](../../strongs/h/h5221.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) on the [yam](../../strongs/h/h3220.md), from [BaʿAl Gaḏ](../../strongs/h/h1171.md) in the [biqʿâ](../../strongs/h/h1237.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) even unto the [har](../../strongs/h/h2022.md) [Ḥālāq](../../strongs/h/h2510.md), that [ʿālâ](../../strongs/h/h5927.md) to [Śēʿîr](../../strongs/h/h8165.md); which [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [nathan](../../strongs/h/h5414.md) unto the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) for a [yᵊruššâ](../../strongs/h/h3425.md) according to their [maḥălōqeṯ](../../strongs/h/h4256.md);

<a name="joshua_12_8"></a>Joshua 12:8

In the [har](../../strongs/h/h2022.md), and in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the ['arabah](../../strongs/h/h6160.md), and in the ['ăšēḏâ](../../strongs/h/h794.md), and in the [midbar](../../strongs/h/h4057.md), and in the [neḡeḇ](../../strongs/h/h5045.md); the [Ḥitî](../../strongs/h/h2850.md), the ['Ĕmōrî](../../strongs/h/h567.md), and the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [Pᵊrizzî](../../strongs/h/h6522.md), the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md):

<a name="joshua_12_9"></a>Joshua 12:9

The [melek](../../strongs/h/h4428.md) of [Yᵊrēḥô](../../strongs/h/h3405.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [ʿAy](../../strongs/h/h5857.md), which is [ṣaḏ](../../strongs/h/h6654.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_10"></a>Joshua 12:10

The [melek](../../strongs/h/h4428.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Ḥeḇrôn](../../strongs/h/h2275.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_11"></a>Joshua 12:11

The [melek](../../strongs/h/h4428.md) of [Yarmûṯ](../../strongs/h/h3412.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Lāḵîš](../../strongs/h/h3923.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_12"></a>Joshua 12:12

The [melek](../../strongs/h/h4428.md) of [ʿEḡlôn](../../strongs/h/h5700.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Gezer](../../strongs/h/h1507.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_13"></a>Joshua 12:13

The [melek](../../strongs/h/h4428.md) of [dᵊḇîr](../../strongs/h/h1688.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Geḏer](../../strongs/h/h1445.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_14"></a>Joshua 12:14

The [melek](../../strongs/h/h4428.md) of [Ḥārmâ](../../strongs/h/h2767.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [ʿĂrāḏ](../../strongs/h/h6166.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_15"></a>Joshua 12:15

The [melek](../../strongs/h/h4428.md) of [Liḇnâ](../../strongs/h/h3841.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [ʿĂḏullām](../../strongs/h/h5725.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_16"></a>Joshua 12:16

The [melek](../../strongs/h/h4428.md) of [Maqqēḏâ](../../strongs/h/h4719.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_17"></a>Joshua 12:17

The [melek](../../strongs/h/h4428.md) of [Tapûaḥ](../../strongs/h/h8599.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Ḥēp̄er](../../strongs/h/h2660.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_18"></a>Joshua 12:18

The [melek](../../strongs/h/h4428.md) of ['Ăp̄Ēq](../../strongs/h/h663.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Šārôn](../../strongs/h/h8289.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_19"></a>Joshua 12:19

The [melek](../../strongs/h/h4428.md) of [Māḏôn](../../strongs/h/h4068.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Ḥāṣôr](../../strongs/h/h2674.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_20"></a>Joshua 12:20

The [melek](../../strongs/h/h4428.md) of [Šimrôn Mᵊr'Ôn](../../strongs/h/h8112.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of ['Aḵšāp̄](../../strongs/h/h407.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_21"></a>Joshua 12:21

The [melek](../../strongs/h/h4428.md) of [TaʿNāḵ](../../strongs/h/h8590.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [Mᵊḡidôn](../../strongs/h/h4023.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_22"></a>Joshua 12:22

The [melek](../../strongs/h/h4428.md) of [Qeḏeš](../../strongs/h/h6943.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of [YāqnᵊʿĀm](../../strongs/h/h3362.md) of [Karmel](../../strongs/h/h3760.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_23"></a>Joshua 12:23

The [melek](../../strongs/h/h4428.md) of [Dôr](../../strongs/h/h1756.md) in the [nāp̄â](../../strongs/h/h5299.md) of [Dôr](../../strongs/h/h1756.md), ['echad](../../strongs/h/h259.md); the [melek](../../strongs/h/h4428.md) of the [gowy](../../strongs/h/h1471.md) of [Gilgāl](../../strongs/h/h1537.md), ['echad](../../strongs/h/h259.md);

<a name="joshua_12_24"></a>Joshua 12:24

The [melek](../../strongs/h/h4428.md) of [Tirṣâ](../../strongs/h/h8656.md), ['echad](../../strongs/h/h259.md): all the [melek](../../strongs/h/h4428.md) [šᵊlōšîm](../../strongs/h/h7970.md) and ['echad](../../strongs/h/h259.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 11](joshua_11.md) - [Joshua 13](joshua_13.md)