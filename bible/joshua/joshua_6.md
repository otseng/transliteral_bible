# [Joshua 6](https://www.blueletterbible.org/kjv/joshua/6)

<a name="joshua_6_1"></a>Joshua 6:1

Now [Yᵊrēḥô](../../strongs/h/h3405.md) was [cagar](../../strongs/h/h5462.md) shut [cagar](../../strongs/h/h5462.md) [paniym](../../strongs/h/h6440.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): none [yāṣā'](../../strongs/h/h3318.md), and none [bow'](../../strongs/h/h935.md).

<a name="joshua_6_2"></a>Joshua 6:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [ra'ah](../../strongs/h/h7200.md), I have [nathan](../../strongs/h/h5414.md) into thine [yad](../../strongs/h/h3027.md) [Yᵊrēḥô](../../strongs/h/h3405.md), and the [melek](../../strongs/h/h4428.md) thereof, and the [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="joshua_6_3"></a>Joshua 6:3

And ye shall [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md), all ye ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and [naqaph](../../strongs/h/h5362.md) the [ʿîr](../../strongs/h/h5892.md) [pa'am](../../strongs/h/h6471.md) ['echad](../../strongs/h/h259.md). Thus shalt thou ['asah](../../strongs/h/h6213.md) [šēš](../../strongs/h/h8337.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_6_4"></a>Joshua 6:4

And [šeḇaʿ](../../strongs/h/h7651.md) [kōhēn](../../strongs/h/h3548.md) shall [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) [šeḇaʿ](../../strongs/h/h7651.md) [šôp̄ār](../../strongs/h/h7782.md) of [yôḇēl](../../strongs/h/h3104.md): and the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) ye shall [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) [šeḇaʿ](../../strongs/h/h7651.md) [pa'am](../../strongs/h/h6471.md), and the [kōhēn](../../strongs/h/h3548.md) shall [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md).

<a name="joshua_6_5"></a>Joshua 6:5

And it shall come to pass, that when they make a [mashak](../../strongs/h/h4900.md) blast with the [yôḇēl](../../strongs/h/h3104.md) [qeren](../../strongs/h/h7161.md), and when ye [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), all the ['am](../../strongs/h/h5971.md) shall [rûaʿ](../../strongs/h/h7321.md) with a [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md); and the [ḥômâ](../../strongs/h/h2346.md) of the [ʿîr](../../strongs/h/h5892.md) shall [naphal](../../strongs/h/h5307.md) [taḥaṯ](../../strongs/h/h8478.md), and the ['am](../../strongs/h/h5971.md) shall [ʿālâ](../../strongs/h/h5927.md) every ['iysh](../../strongs/h/h376.md) straight before him.

<a name="joshua_6_6"></a>Joshua 6:6

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md) [qara'](../../strongs/h/h7121.md) the [kōhēn](../../strongs/h/h3548.md), and ['āmar](../../strongs/h/h559.md) unto them, [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md), and let [šeḇaʿ](../../strongs/h/h7651.md) [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) [šeḇaʿ](../../strongs/h/h7651.md) [šôp̄ār](../../strongs/h/h7782.md) of [yôḇēl](../../strongs/h/h3104.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_6_7"></a>Joshua 6:7

And he ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), ['abar](../../strongs/h/h5674.md), and [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md), and let him that is [chalats](../../strongs/h/h2502.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_6_8"></a>Joshua 6:8

And it came to pass, when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), that the [šeḇaʿ](../../strongs/h/h7651.md) [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) the [šeḇaʿ](../../strongs/h/h7651.md) [šôp̄ār](../../strongs/h/h7782.md) of [yôḇēl](../../strongs/h/h3104.md) ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md): and the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md).

<a name="joshua_6_9"></a>Joshua 6:9

And the [chalats](../../strongs/h/h2502.md) men [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md) that [tāqaʿ](../../strongs/h/h8628.md) [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md), and the ['āsap̄](../../strongs/h/h622.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) the ['ārôn](../../strongs/h/h727.md), the priests [halak](../../strongs/h/h1980.md), and [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md).

<a name="joshua_6_10"></a>Joshua 6:10

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had [tsavah](../../strongs/h/h6680.md) the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Ye shall not [rûaʿ](../../strongs/h/h7321.md), nor make any [shama'](../../strongs/h/h8085.md) with your [qowl](../../strongs/h/h6963.md), neither shall any [dabar](../../strongs/h/h1697.md) [yāṣā'](../../strongs/h/h3318.md) out of your [peh](../../strongs/h/h6310.md), until the [yowm](../../strongs/h/h3117.md) I ['āmar](../../strongs/h/h559.md) you [rûaʿ](../../strongs/h/h7321.md); then shall ye [rûaʿ](../../strongs/h/h7321.md).

<a name="joshua_6_11"></a>Joshua 6:11

So the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md), going [naqaph](../../strongs/h/h5362.md) it [pa'am](../../strongs/h/h6471.md) ['echad](../../strongs/h/h259.md): and they [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md), and [lûn](../../strongs/h/h3885.md) in the [maḥănê](../../strongs/h/h4264.md).

<a name="joshua_6_12"></a>Joshua 6:12

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and the [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_6_13"></a>Joshua 6:13

And [šeḇaʿ](../../strongs/h/h7651.md) [kōhēn](../../strongs/h/h3548.md) [nasa'](../../strongs/h/h5375.md) [šeḇaʿ](../../strongs/h/h7651.md) [šôp̄ār](../../strongs/h/h7782.md) of [yôḇēl](../../strongs/h/h3104.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) [halak](../../strongs/h/h1980.md) [halak](../../strongs/h/h1980.md), and [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md): and the [chalats](../../strongs/h/h2502.md) [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) them; but the ['āsap̄](../../strongs/h/h622.md) [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), the priests [halak](../../strongs/h/h1980.md), and [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md).

<a name="joshua_6_14"></a>Joshua 6:14

And the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md) they [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) [pa'am](../../strongs/h/h6471.md) ['echad](../../strongs/h/h259.md), and [shuwb](../../strongs/h/h7725.md) into the [maḥănê](../../strongs/h/h4264.md): so they ['asah](../../strongs/h/h6213.md) [šēš](../../strongs/h/h8337.md) [yowm](../../strongs/h/h3117.md).

<a name="joshua_6_15"></a>Joshua 6:15

And it came to pass on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md), that they [šāḵam](../../strongs/h/h7925.md) about the [ʿālâ](../../strongs/h/h5927.md) of the [šaḥar](../../strongs/h/h7837.md), and [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) after the same [mishpat](../../strongs/h/h4941.md) [šeḇaʿ](../../strongs/h/h7651.md) [pa'am](../../strongs/h/h6471.md): only on that [yowm](../../strongs/h/h3117.md) they [cabab](../../strongs/h/h5437.md) the [ʿîr](../../strongs/h/h5892.md) [šeḇaʿ](../../strongs/h/h7651.md) [pa'am](../../strongs/h/h6471.md).

<a name="joshua_6_16"></a>Joshua 6:16

And it came to pass at the [šᵊḇîʿî](../../strongs/h/h7637.md) [pa'am](../../strongs/h/h6471.md), when the [kōhēn](../../strongs/h/h3548.md) [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md), [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), [rûaʿ](../../strongs/h/h7321.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) you the [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_6_17"></a>Joshua 6:17

And the [ʿîr](../../strongs/h/h5892.md) shall be [ḥērem](../../strongs/h/h2764.md), even it, and all that are therein, to [Yĕhovah](../../strongs/h/h3068.md): only [Rāḥāḇ](../../strongs/h/h7343.md) the [zānâ](../../strongs/h/h2181.md) shall [ḥāyâ](../../strongs/h/h2421.md), she and all that are with her in the [bayith](../../strongs/h/h1004.md), because she [chaba'](../../strongs/h/h2244.md) the [mal'ak](../../strongs/h/h4397.md) that we [shalach](../../strongs/h/h7971.md).

<a name="joshua_6_18"></a>Joshua 6:18

And ye, in any [raq](../../strongs/h/h7535.md) [shamar](../../strongs/h/h8104.md) yourselves from the [ḥērem](../../strongs/h/h2764.md), lest ye make yourselves [ḥāram](../../strongs/h/h2763.md), when ye [laqach](../../strongs/h/h3947.md) of the [ḥērem](../../strongs/h/h2764.md), and [śûm](../../strongs/h/h7760.md) the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md) a [ḥērem](../../strongs/h/h2764.md), and [ʿāḵar](../../strongs/h/h5916.md) it.

<a name="joshua_6_19"></a>Joshua 6:19

But all the [keceph](../../strongs/h/h3701.md), and [zāhāḇ](../../strongs/h/h2091.md), and [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) and [barzel](../../strongs/h/h1270.md), are [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md): they shall [bow'](../../strongs/h/h935.md) into the ['ôṣār](../../strongs/h/h214.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_6_20"></a>Joshua 6:20

So the ['am](../../strongs/h/h5971.md) [rûaʿ](../../strongs/h/h7321.md) when the priests [tāqaʿ](../../strongs/h/h8628.md) with the [šôp̄ār](../../strongs/h/h7782.md): and it came to pass, when the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), and the ['am](../../strongs/h/h5971.md) [rûaʿ](../../strongs/h/h7321.md) with a [gadowl](../../strongs/h/h1419.md) [tᵊrûʿâ](../../strongs/h/h8643.md), that the [ḥômâ](../../strongs/h/h2346.md) fell down [naphal](../../strongs/h/h5307.md), so that the ['am](../../strongs/h/h5971.md) went [ʿālâ](../../strongs/h/h5927.md) into the [ʿîr](../../strongs/h/h5892.md), every ['iysh](../../strongs/h/h376.md) straight before him, and they [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="joshua_6_21"></a>Joshua 6:21

And they [ḥāram](../../strongs/h/h2763.md) all that was in the [ʿîr](../../strongs/h/h5892.md), both ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), [naʿar](../../strongs/h/h5288.md) and [zāqēn](../../strongs/h/h2205.md), and [showr](../../strongs/h/h7794.md), and [śê](../../strongs/h/h7716.md), and [chamowr](../../strongs/h/h2543.md), with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="joshua_6_22"></a>Joshua 6:22

But [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had ['āmar](../../strongs/h/h559.md) unto the [šᵊnayim](../../strongs/h/h8147.md) ['enowsh](../../strongs/h/h582.md) that had [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md), [bow'](../../strongs/h/h935.md) into the [zānâ](../../strongs/h/h2181.md) [bayith](../../strongs/h/h1004.md), and [yāṣā'](../../strongs/h/h3318.md) thence the ['ishshah](../../strongs/h/h802.md), and all that she hath, as ye [shaba'](../../strongs/h/h7650.md) unto her.

<a name="joshua_6_23"></a>Joshua 6:23

And the [naʿar](../../strongs/h/h5288.md) that were [ragal](../../strongs/h/h7270.md) [bow'](../../strongs/h/h935.md), and [yāṣā'](../../strongs/h/h3318.md) [Rāḥāḇ](../../strongs/h/h7343.md), and her ['ab](../../strongs/h/h1.md), and her ['em](../../strongs/h/h517.md), and her ['ach](../../strongs/h/h251.md), and all that she had; and they [yāṣā'](../../strongs/h/h3318.md) all her [mišpāḥâ](../../strongs/h/h4940.md), and [yānaḥ](../../strongs/h/h3240.md) them [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_6_24"></a>Joshua 6:24

And they [śārap̄](../../strongs/h/h8313.md) the [ʿîr](../../strongs/h/h5892.md) with ['esh](../../strongs/h/h784.md), and all that was therein: only the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and the [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) and of [barzel](../../strongs/h/h1270.md), they [nathan](../../strongs/h/h5414.md) into the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_6_25"></a>Joshua 6:25

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [ḥāyâ](../../strongs/h/h2421.md) [Rāḥāḇ](../../strongs/h/h7343.md) the [zānâ](../../strongs/h/h2181.md) [ḥāyâ](../../strongs/h/h2421.md), and her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and all that she had; and she [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) [Yisra'el](../../strongs/h/h3478.md) even unto this [yowm](../../strongs/h/h3117.md); because she [chaba'](../../strongs/h/h2244.md) the [mal'ak](../../strongs/h/h4397.md), which [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shalach](../../strongs/h/h7971.md) to [ragal](../../strongs/h/h7270.md) [Yᵊrēḥô](../../strongs/h/h3405.md).

<a name="joshua_6_26"></a>Joshua 6:26

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shaba'](../../strongs/h/h7650.md) them at that [ʿēṯ](../../strongs/h/h6256.md), ['āmar](../../strongs/h/h559.md), ['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), that riseth [quwm](../../strongs/h/h6965.md) and [bānâ](../../strongs/h/h1129.md) this [ʿîr](../../strongs/h/h5892.md) [Yᵊrēḥô](../../strongs/h/h3405.md): he shall lay the [yacad](../../strongs/h/h3245.md) thereof in his [bᵊḵôr](../../strongs/h/h1060.md), and in his [ṣāʿîr](../../strongs/h/h6810.md) son shall he set [nāṣaḇ](../../strongs/h/h5324.md) the [deleṯ](../../strongs/h/h1817.md) of it.

<a name="joshua_6_27"></a>Joshua 6:27

So [Yĕhovah](../../strongs/h/h3068.md) was with [Yᵊhôšûaʿ](../../strongs/h/h3091.md); and his [šōmaʿ](../../strongs/h/h8089.md) was noised throughout all the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 5](joshua_5.md) - [Joshua 7](joshua_7.md)