# [Joshua 22](https://www.blueletterbible.org/kjv/joshua/22)

<a name="joshua_22_1"></a>Joshua 22:1

Then [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qara'](../../strongs/h/h7121.md) the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md), and the [Gāḏî](../../strongs/h/h1425.md), and the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of [Mᵊnaššê](../../strongs/h/h4519.md),

<a name="joshua_22_2"></a>Joshua 22:2

And ['āmar](../../strongs/h/h559.md) unto them, Ye have [shamar](../../strongs/h/h8104.md) all that [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) you, and have [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) in all that I [tsavah](../../strongs/h/h6680.md) you:

<a name="joshua_22_3"></a>Joshua 22:3

Ye have not ['azab](../../strongs/h/h5800.md) your ['ach](../../strongs/h/h251.md) these [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) unto this [yowm](../../strongs/h/h3117.md), but have [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_22_4"></a>Joshua 22:4

And now [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) hath given [nuwach](../../strongs/h/h5117.md) unto your ['ach](../../strongs/h/h251.md), as he [dabar](../../strongs/h/h1696.md) them: therefore now [panah](../../strongs/h/h6437.md) ye, and [yālaḵ](../../strongs/h/h3212.md) you unto your ['ohel](../../strongs/h/h168.md), and unto the ['erets](../../strongs/h/h776.md) of your ['achuzzah](../../strongs/h/h272.md), which [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) you on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_22_5"></a>Joshua 22:5

But [me'od](../../strongs/h/h3966.md) [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) the [mitsvah](../../strongs/h/h4687.md) and the [towrah](../../strongs/h/h8451.md), which [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) you, to ['ahab](../../strongs/h/h157.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and to [yālaḵ](../../strongs/h/h3212.md) in all his [derek](../../strongs/h/h1870.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), and to [dāḇaq](../../strongs/h/h1692.md) unto him, and to ['abad](../../strongs/h/h5647.md) him with all your [lebab](../../strongs/h/h3824.md) and with all your [nephesh](../../strongs/h/h5315.md).

<a name="joshua_22_6"></a>Joshua 22:6

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [barak](../../strongs/h/h1288.md) them, and [shalach](../../strongs/h/h7971.md) them: and they [yālaḵ](../../strongs/h/h3212.md) unto their ['ohel](../../strongs/h/h168.md).

<a name="joshua_22_7"></a>Joshua 22:7

Now to the [ḥēṣî](../../strongs/h/h2677.md) of the [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [Mōshe](../../strongs/h/h4872.md) had [nathan](../../strongs/h/h5414.md) possession in [Bāšān](../../strongs/h/h1316.md): but unto the other [ḥēṣî](../../strongs/h/h2677.md) thereof [nathan](../../strongs/h/h5414.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [ʿim](../../strongs/h/h5973.md) their ['ach](../../strongs/h/h251.md) on this [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [yam](../../strongs/h/h3220.md). And when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shalach](../../strongs/h/h7971.md) them also unto their ['ohel](../../strongs/h/h168.md), then he [barak](../../strongs/h/h1288.md) them,

<a name="joshua_22_8"></a>Joshua 22:8

And he ['āmar](../../strongs/h/h559.md) unto them, ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) with [rab](../../strongs/h/h7227.md) [neḵes](../../strongs/h/h5233.md) unto your ['ohel](../../strongs/h/h168.md), and with [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md) [miqnê](../../strongs/h/h4735.md), with [keceph](../../strongs/h/h3701.md), and with [zāhāḇ](../../strongs/h/h2091.md), and with [nᵊḥšeṯ](../../strongs/h/h5178.md), and with [barzel](../../strongs/h/h1270.md), and with [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [śalmâ](../../strongs/h/h8008.md): [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md) of your ['oyeb](../../strongs/h/h341.md) with your ['ach](../../strongs/h/h251.md).

<a name="joshua_22_9"></a>Joshua 22:9

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [shuwb](../../strongs/h/h7725.md), and [yālaḵ](../../strongs/h/h3212.md) from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of [Šîlô](../../strongs/h/h7887.md), which is in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), to [yālaḵ](../../strongs/h/h3212.md) unto the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), to the ['erets](../../strongs/h/h776.md) of their ['achuzzah](../../strongs/h/h272.md), whereof they were ['āḥaz](../../strongs/h/h270.md), according to the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="joshua_22_10"></a>Joshua 22:10

And when they [bow'](../../strongs/h/h935.md) unto the [gᵊlîlâ](../../strongs/h/h1552.md) of [Yardēn](../../strongs/h/h3383.md), that are in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md) by [Yardēn](../../strongs/h/h3383.md), a [gadowl](../../strongs/h/h1419.md) [mizbeach](../../strongs/h/h4196.md) to see [mar'ê](../../strongs/h/h4758.md).

<a name="joshua_22_11"></a>Joshua 22:11

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md), Behold, the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) have [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) over [môl](../../strongs/h/h4136.md) the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), in the [gᵊlîlâ](../../strongs/h/h1552.md) of [Yardēn](../../strongs/h/h3383.md), at the [ʿēḇer](../../strongs/h/h5676.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_22_12"></a>Joshua 22:12

And when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shama'](../../strongs/h/h8085.md) of it, the whole ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md) themselves at [Šîlô](../../strongs/h/h7887.md), to [ʿālâ](../../strongs/h/h5927.md) to [tsaba'](../../strongs/h/h6635.md) against them.

<a name="joshua_22_13"></a>Joshua 22:13

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) unto the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and to the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), and to the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), into the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), [Pînḥās](../../strongs/h/h6372.md) the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md),

<a name="joshua_22_14"></a>Joshua 22:14

And with him [ʿeśer](../../strongs/h/h6235.md) [nāśî'](../../strongs/h/h5387.md), of ['echad](../../strongs/h/h259.md) ['echad](../../strongs/h/h259.md) [nāśî'](../../strongs/h/h5387.md) [nāśî'](../../strongs/h/h5387.md) [bayith](../../strongs/h/h1004.md) an ['ab](../../strongs/h/h1.md) throughout all the [maṭṭê](../../strongs/h/h4294.md) of [Yisra'el](../../strongs/h/h3478.md); and each ['iysh](../../strongs/h/h376.md) was a [ro'sh](../../strongs/h/h7218.md) of the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md) among the ['elep̄](../../strongs/h/h505.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_22_15"></a>Joshua 22:15

And they [bow'](../../strongs/h/h935.md) unto the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and to the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), and to the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md), unto the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and they [dabar](../../strongs/h/h1696.md) with them, ['āmar](../../strongs/h/h559.md),

<a name="joshua_22_16"></a>Joshua 22:16

Thus ['āmar](../../strongs/h/h559.md) the ['edah](../../strongs/h/h5712.md) of [Yĕhovah](../../strongs/h/h3068.md), What [maʿal](../../strongs/h/h4604.md) is this that ye have [māʿal](../../strongs/h/h4603.md) against the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), to [shuwb](../../strongs/h/h7725.md) this [yowm](../../strongs/h/h3117.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), in that ye have [bānâ](../../strongs/h/h1129.md) you a [mizbeach](../../strongs/h/h4196.md), that ye might [māraḏ](../../strongs/h/h4775.md) this [yowm](../../strongs/h/h3117.md) against [Yĕhovah](../../strongs/h/h3068.md)?

<a name="joshua_22_17"></a>Joshua 22:17

Is the ['avon](../../strongs/h/h5771.md) of [P̄ᵊʿôr](../../strongs/h/h6465.md) too [mᵊʿaṭ](../../strongs/h/h4592.md) for us, from which we are not [ṭāhēr](../../strongs/h/h2891.md) until this [yowm](../../strongs/h/h3117.md), although there was a [neḡep̄](../../strongs/h/h5063.md) in the ['edah](../../strongs/h/h5712.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="joshua_22_18"></a>Joshua 22:18

But that ye must [shuwb](../../strongs/h/h7725.md) this [yowm](../../strongs/h/h3117.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md)? and it will be, seeing ye [māraḏ](../../strongs/h/h4775.md) to [yowm](../../strongs/h/h3117.md) against [Yĕhovah](../../strongs/h/h3068.md), that [māḥār](../../strongs/h/h4279.md) he will be [qāṣap̄](../../strongs/h/h7107.md) with the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_22_19"></a>Joshua 22:19

['aḵ](../../strongs/h/h389.md), if the ['erets](../../strongs/h/h776.md) of your ['achuzzah](../../strongs/h/h272.md) be [tame'](../../strongs/h/h2931.md), then ['abar](../../strongs/h/h5674.md) ye unto the ['erets](../../strongs/h/h776.md) of the ['achuzzah](../../strongs/h/h272.md) of [Yĕhovah](../../strongs/h/h3068.md), wherein [Yĕhovah](../../strongs/h/h3068.md) [miškān](../../strongs/h/h4908.md) [shakan](../../strongs/h/h7931.md), and take ['āḥaz](../../strongs/h/h270.md) [tavek](../../strongs/h/h8432.md) us: but [māraḏ](../../strongs/h/h4775.md) not against [Yĕhovah](../../strongs/h/h3068.md), ['al](../../strongs/h/h408.md) [māraḏ](../../strongs/h/h4775.md) against us, in [bānâ](../../strongs/h/h1129.md) you a [mizbeach](../../strongs/h/h4196.md) [bilʿăḏê](../../strongs/h/h1107.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="joshua_22_20"></a>Joshua 22:20

Did not [ʿĀḵān](../../strongs/h/h5912.md) the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md) [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md) in the [ḥērem](../../strongs/h/h2764.md), and [qeṣep̄](../../strongs/h/h7110.md) [hayah](../../strongs/h/h1961.md) on all the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md)? and that ['iysh](../../strongs/h/h376.md) [gāvaʿ](../../strongs/h/h1478.md) not ['echad](../../strongs/h/h259.md) in his ['avon](../../strongs/h/h5771.md).

<a name="joshua_22_21"></a>Joshua 22:21

Then the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md) ['anah](../../strongs/h/h6030.md), and [dabar](../../strongs/h/h1696.md) unto the [ro'sh](../../strongs/h/h7218.md) of the ['elep̄](../../strongs/h/h505.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="joshua_22_22"></a>Joshua 22:22

[Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md) of ['Elohiym](../../strongs/h/h430.md), [Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md) of ['Elohiym](../../strongs/h/h430.md), he [yada'](../../strongs/h/h3045.md), and [Yisra'el](../../strongs/h/h3478.md) he shall [yada'](../../strongs/h/h3045.md); if it be in [mereḏ](../../strongs/h/h4777.md), or if in [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md), (save [yasha'](../../strongs/h/h3467.md) us not this [yowm](../../strongs/h/h3117.md),)

<a name="joshua_22_23"></a>Joshua 22:23

That we have [bānâ](../../strongs/h/h1129.md) us a [mizbeach](../../strongs/h/h4196.md) to [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), or if to [ʿālâ](../../strongs/h/h5927.md) thereon [ʿōlâ](../../strongs/h/h5930.md) or meat [minchah](../../strongs/h/h4503.md), or if to ['asah](../../strongs/h/h6213.md) [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md) thereon, let [Yĕhovah](../../strongs/h/h3068.md) himself [bāqaš](../../strongs/h/h1245.md) it;

<a name="joshua_22_24"></a>Joshua 22:24

And if we have not rather ['asah](../../strongs/h/h6213.md) it for [dᵊ'āḡâ](../../strongs/h/h1674.md) of this [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), In time [māḥār](../../strongs/h/h4279.md) your [ben](../../strongs/h/h1121.md) might ['āmar](../../strongs/h/h559.md) unto our [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), What have ye to do with [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="joshua_22_25"></a>Joshua 22:25

For [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) [Yardēn](../../strongs/h/h3383.md) a [gᵊḇûl](../../strongs/h/h1366.md) between us and you, ye [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md); ye have no [cheleq](../../strongs/h/h2506.md) in [Yĕhovah](../../strongs/h/h3068.md): so shall your [ben](../../strongs/h/h1121.md) [shabath](../../strongs/h/h7673.md) our [ben](../../strongs/h/h1121.md) [shabath](../../strongs/h/h7673.md) [biltî](../../strongs/h/h1115.md) [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_22_26"></a>Joshua 22:26

Therefore we ['āmar](../../strongs/h/h559.md), Let us now ['asah](../../strongs/h/h6213.md) to [bānâ](../../strongs/h/h1129.md) us a [mizbeach](../../strongs/h/h4196.md), not for [ʿōlâ](../../strongs/h/h5930.md), nor for [zebach](../../strongs/h/h2077.md):

<a name="joshua_22_27"></a>Joshua 22:27

But that it may be an ['ed](../../strongs/h/h5707.md) between us, and you, and our [dôr](../../strongs/h/h1755.md) ['aḥar](../../strongs/h/h310.md) us, that we might ['abad](../../strongs/h/h5647.md) the [ʿăḇōḏâ](../../strongs/h/h5656.md) of [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) him with our [ʿōlâ](../../strongs/h/h5930.md), and with our [zebach](../../strongs/h/h2077.md), and with our [šelem](../../strongs/h/h8002.md); that your [ben](../../strongs/h/h1121.md) may not ['āmar](../../strongs/h/h559.md) to our [ben](../../strongs/h/h1121.md) in time [māḥār](../../strongs/h/h4279.md), Ye have no [cheleq](../../strongs/h/h2506.md) in [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_22_28"></a>Joshua 22:28

Therefore ['āmar](../../strongs/h/h559.md) we, that it shall be, when they should so ['āmar](../../strongs/h/h559.md) to us or to our [dôr](../../strongs/h/h1755.md) in [māḥār](../../strongs/h/h4279.md), that we may ['āmar](../../strongs/h/h559.md) again, [ra'ah](../../strongs/h/h7200.md) the [taḇnîṯ](../../strongs/h/h8403.md) of the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), which our ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md), not for [ʿōlâ](../../strongs/h/h5930.md), nor for [zebach](../../strongs/h/h2077.md); but it is an ['ed](../../strongs/h/h5707.md) between us and you.

<a name="joshua_22_29"></a>Joshua 22:29

[ḥālîlâ](../../strongs/h/h2486.md) that we should [māraḏ](../../strongs/h/h4775.md) against [Yĕhovah](../../strongs/h/h3068.md), and [shuwb](../../strongs/h/h7725.md) this [yowm](../../strongs/h/h3117.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), to [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) for [ʿōlâ](../../strongs/h/h5930.md), for [minchah](../../strongs/h/h4503.md), or for [zebach](../../strongs/h/h2077.md), beside the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) that is [paniym](../../strongs/h/h6440.md) his [miškān](../../strongs/h/h4908.md).

<a name="joshua_22_30"></a>Joshua 22:30

And when [Pînḥās](../../strongs/h/h6372.md) the [kōhēn](../../strongs/h/h3548.md), and the [nāśî'](../../strongs/h/h5387.md) of the ['edah](../../strongs/h/h5712.md) and [ro'sh](../../strongs/h/h7218.md) of the ['elep̄](../../strongs/h/h505.md) of [Yisra'el](../../strongs/h/h3478.md) which were with him, [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) that the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) and the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) [dabar](../../strongs/h/h1696.md), it [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) them.

<a name="joshua_22_31"></a>Joshua 22:31

And [Pînḥās](../../strongs/h/h6372.md) the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and to the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), and to the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), This [yowm](../../strongs/h/h3117.md) we [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) is [tavek](../../strongs/h/h8432.md) us, because ye have not [māʿal](../../strongs/h/h4603.md) this [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md): ['āz](../../strongs/h/h227.md) ye have [natsal](../../strongs/h/h5337.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_22_32"></a>Joshua 22:32

And [Pînḥās](../../strongs/h/h6372.md) the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md) the [kōhēn](../../strongs/h/h3548.md), and the [nāśî'](../../strongs/h/h5387.md), [shuwb](../../strongs/h/h7725.md) from the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md), and from the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md), out of the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), unto the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [shuwb](../../strongs/h/h7725.md) them [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md).

<a name="joshua_22_33"></a>Joshua 22:33

And the [dabar](../../strongs/h/h1697.md) [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [barak](../../strongs/h/h1288.md) ['Elohiym](../../strongs/h/h430.md), and did not ['āmar](../../strongs/h/h559.md) to [ʿālâ](../../strongs/h/h5927.md) against them in [tsaba'](../../strongs/h/h6635.md), to [shachath](../../strongs/h/h7843.md) the ['erets](../../strongs/h/h776.md) wherein the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and [Gāḏ](../../strongs/h/h1410.md) [yashab](../../strongs/h/h3427.md).

<a name="joshua_22_34"></a>Joshua 22:34

And the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) and the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) [qara'](../../strongs/h/h7121.md) the [mizbeach](../../strongs/h/h4196.md): for it shall be an ['ed](../../strongs/h/h5707.md) between us that [Yĕhovah](../../strongs/h/h3068.md) is ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 21](joshua_21.md) - [Joshua 23](joshua_23.md)