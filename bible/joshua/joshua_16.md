# [Joshua 16](https://www.blueletterbible.org/kjv/joshua/16)

<a name="joshua_16_1"></a>Joshua 16:1

And the [gôrāl](../../strongs/h/h1486.md) of the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md) [yāṣā'](../../strongs/h/h3318.md) from [Yardēn](../../strongs/h/h3383.md) by [Yᵊrēḥô](../../strongs/h/h3405.md), unto the [mayim](../../strongs/h/h4325.md) of [Yᵊrēḥô](../../strongs/h/h3405.md) on the [mizrach](../../strongs/h/h4217.md), to the [midbar](../../strongs/h/h4057.md) that [ʿālâ](../../strongs/h/h5927.md) from [Yᵊrēḥô](../../strongs/h/h3405.md) throughout [har](../../strongs/h/h2022.md) [Bêṯ-'ēl](../../strongs/h/h1008.md),

<a name="joshua_16_2"></a>Joshua 16:2

And [yāṣā'](../../strongs/h/h3318.md) from [Bêṯ-'ēl](../../strongs/h/h1008.md) to [Lûz](../../strongs/h/h3870.md), and ['abar](../../strongs/h/h5674.md) unto the [gᵊḇûl](../../strongs/h/h1366.md) of ['Arkî](../../strongs/h/h757.md) to [ʿĂṭārôṯ](../../strongs/h/h5852.md),

<a name="joshua_16_3"></a>Joshua 16:3

And goeth [yarad](../../strongs/h/h3381.md) [yam](../../strongs/h/h3220.md) to the [gᵊḇûl](../../strongs/h/h1366.md) of [yap̄lēṭî](../../strongs/h/h3311.md), unto the [gᵊḇûl](../../strongs/h/h1366.md) of [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the [taḥtôn](../../strongs/h/h8481.md), and to [Gezer](../../strongs/h/h1507.md): and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof are at the [yam](../../strongs/h/h3220.md).

<a name="joshua_16_4"></a>Joshua 16:4

So the [ben](../../strongs/h/h1121.md) of [Yôsēp̄](../../strongs/h/h3130.md), [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md), took their [nāḥal](../../strongs/h/h5157.md).

<a name="joshua_16_5"></a>Joshua 16:5

And the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) according to their [mišpāḥâ](../../strongs/h/h4940.md) was thus: even the [gᵊḇûl](../../strongs/h/h1366.md) of their [nachalah](../../strongs/h/h5159.md) on the [mizrach](../../strongs/h/h4217.md) was [ʿAṭrôṯ 'Adār](../../strongs/h/h5853.md), unto [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the ['elyown](../../strongs/h/h5945.md);

<a name="joshua_16_6"></a>Joshua 16:6

And the [gᵊḇûl](../../strongs/h/h1366.md) [yāṣā'](../../strongs/h/h3318.md) toward the [yam](../../strongs/h/h3220.md) to [Miḵmᵊṯāṯ](../../strongs/h/h4366.md) on the [ṣāp̄ôn](../../strongs/h/h6828.md); and the [gᵊḇûl](../../strongs/h/h1366.md) went [cabab](../../strongs/h/h5437.md) [mizrach](../../strongs/h/h4217.md) unto [Ta'Ănaṯ Šilô](../../strongs/h/h8387.md), and ['abar](../../strongs/h/h5674.md) by it on the [mizrach](../../strongs/h/h4217.md) to [Yānôaḥ](../../strongs/h/h3239.md);

<a name="joshua_16_7"></a>Joshua 16:7

And it [yarad](../../strongs/h/h3381.md) from [Yānôaḥ](../../strongs/h/h3239.md) to [ʿĂṭārôṯ](../../strongs/h/h5852.md), and to [NaʿĂrâ](../../strongs/h/h5292.md), and [pāḡaʿ](../../strongs/h/h6293.md) to [Yᵊrēḥô](../../strongs/h/h3405.md), and [yāṣā'](../../strongs/h/h3318.md) at [Yardēn](../../strongs/h/h3383.md).

<a name="joshua_16_8"></a>Joshua 16:8

The [gᵊḇûl](../../strongs/h/h1366.md) went [yālaḵ](../../strongs/h/h3212.md) from [Tapûaḥ](../../strongs/h/h8599.md) [yam](../../strongs/h/h3220.md) unto the [nachal](../../strongs/h/h5158.md) [Qānâ](../../strongs/h/h7071.md); and the [tôṣā'ôṯ](../../strongs/h/h8444.md) thereof were at the [yam](../../strongs/h/h3220.md). This is the [nachalah](../../strongs/h/h5159.md) of the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) by their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_16_9"></a>Joshua 16:9

And the [miḇdālôṯ](../../strongs/h/h3995.md) [ʿîr](../../strongs/h/h5892.md) for the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md) were [tavek](../../strongs/h/h8432.md) the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), all the [ʿîr](../../strongs/h/h5892.md) with their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_16_10"></a>Joshua 16:10

And they [yarash](../../strongs/h/h3423.md) not the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) in [Gezer](../../strongs/h/h1507.md): but the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) the ['Ep̄rayim](../../strongs/h/h669.md) unto this [yowm](../../strongs/h/h3117.md), and ['abad](../../strongs/h/h5647.md) under [mas](../../strongs/h/h4522.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 15](joshua_15.md) - [Joshua 17](joshua_17.md)