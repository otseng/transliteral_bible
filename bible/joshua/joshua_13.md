# [Joshua 13](https://www.blueletterbible.org/kjv/joshua/13)

<a name="joshua_13_1"></a>Joshua 13:1

Now [Yᵊhôšûaʿ](../../strongs/h/h3091.md) was [zāqēn](../../strongs/h/h2204.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md); and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Thou art [zāqēn](../../strongs/h/h2204.md) and [bow'](../../strongs/h/h935.md) in [yowm](../../strongs/h/h3117.md), and there [šā'ar](../../strongs/h/h7604.md) yet [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) ['erets](../../strongs/h/h776.md) to be [yarash](../../strongs/h/h3423.md).

<a name="joshua_13_2"></a>Joshua 13:2

This is the ['erets](../../strongs/h/h776.md) that yet [šā'ar](../../strongs/h/h7604.md): all the [gᵊlîlâ](../../strongs/h/h1552.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and all [Gᵊšûrî](../../strongs/h/h1651.md),

<a name="joshua_13_3"></a>Joshua 13:3

From [šîḥôr](../../strongs/h/h7883.md), which is [paniym](../../strongs/h/h6440.md) [Mitsrayim](../../strongs/h/h4714.md), even unto the [gᵊḇûl](../../strongs/h/h1366.md) of [ʿEqrôn](../../strongs/h/h6138.md) [ṣāp̄ôn](../../strongs/h/h6828.md), which is [chashab](../../strongs/h/h2803.md) to the [Kᵊnaʿănî](../../strongs/h/h3669.md): [ḥāmēš](../../strongs/h/h2568.md) [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md); the [ʿĂzzāṯî](../../strongs/h/h5841.md), and the ['ašdôḏî](../../strongs/h/h796.md), the ['ešqᵊlônî](../../strongs/h/h832.md), the [Gitî](../../strongs/h/h1663.md), and the [ʿeqrônî](../../strongs/h/h6139.md); also the [ʿAûî](../../strongs/h/h5761.md):

<a name="joshua_13_4"></a>Joshua 13:4

From the [têmān](../../strongs/h/h8486.md), all the ['erets](../../strongs/h/h776.md) of the [Kᵊnaʿănî](../../strongs/h/h3669.md), and [MᵊʿĀrâ](../../strongs/h/h4632.md) that is beside the [Ṣîḏōnî](../../strongs/h/h6722.md), unto ['Ăp̄Ēq](../../strongs/h/h663.md), to the [gᵊḇûl](../../strongs/h/h1366.md) of the ['Ĕmōrî](../../strongs/h/h567.md):

<a name="joshua_13_5"></a>Joshua 13:5

And the ['erets](../../strongs/h/h776.md) of the [giḇlî](../../strongs/h/h1382.md), and all [Lᵊḇānôn](../../strongs/h/h3844.md), toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md), from [BaʿAl Gaḏ](../../strongs/h/h1171.md) under [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md) unto the [bow'](../../strongs/h/h935.md) into [Ḥămāṯ](../../strongs/h/h2574.md).

<a name="joshua_13_6"></a>Joshua 13:6

All the [yashab](../../strongs/h/h3427.md) of the hill [har](../../strongs/h/h2022.md) from [Lᵊḇānôn](../../strongs/h/h3844.md) unto [Miśrᵊp̄Ôṯ Mayim](../../strongs/h/h4956.md), and all the [Ṣîḏōnî](../../strongs/h/h6722.md), them will I [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): only divide thou it by [naphal](../../strongs/h/h5307.md) unto the [Yisra'el](../../strongs/h/h3478.md) for a [nachalah](../../strongs/h/h5159.md), as I have [tsavah](../../strongs/h/h6680.md) thee.

<a name="joshua_13_7"></a>Joshua 13:7

Now therefore [chalaq](../../strongs/h/h2505.md) this ['erets](../../strongs/h/h776.md) for a [nachalah](../../strongs/h/h5159.md) unto the [tēšaʿ](../../strongs/h/h8672.md) [shebet](../../strongs/h/h7626.md), and the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md),

<a name="joshua_13_8"></a>Joshua 13:8

With whom the [Rᵊ'ûḇēnî](../../strongs/h/h7206.md) and the [Gāḏî](../../strongs/h/h1425.md) have [laqach](../../strongs/h/h3947.md) their [nachalah](../../strongs/h/h5159.md), which [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) them, [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [mizrach](../../strongs/h/h4217.md), even as [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them;

<a name="joshua_13_9"></a>Joshua 13:9

From [ʿĂrôʿēr](../../strongs/h/h6177.md), that is upon the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) ['arnôn](../../strongs/h/h769.md), and the [ʿîr](../../strongs/h/h5892.md) that is in the [tavek](../../strongs/h/h8432.md) of the [nachal](../../strongs/h/h5158.md), and all the [mîšôr](../../strongs/h/h4334.md) of [Mêḏḇā'](../../strongs/h/h4311.md) unto [Dîḇōvn](../../strongs/h/h1769.md);

<a name="joshua_13_10"></a>Joshua 13:10

And all the [ʿîr](../../strongs/h/h5892.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [mālaḵ](../../strongs/h/h4427.md) in [Hešbôn](../../strongs/h/h2809.md), unto the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md);

<a name="joshua_13_11"></a>Joshua 13:11

And [Gilʿāḏ](../../strongs/h/h1568.md), and the [gᵊḇûl](../../strongs/h/h1366.md) of the [Gᵊšûrî](../../strongs/h/h1651.md) and [Maʿăḵāṯî](../../strongs/h/h4602.md), and all [har](../../strongs/h/h2022.md) [Ḥermôn](../../strongs/h/h2768.md), and all [Bāšān](../../strongs/h/h1316.md) unto [Salḵâ](../../strongs/h/h5548.md);

<a name="joshua_13_12"></a>Joshua 13:12

All the [mamlāḵûṯ](../../strongs/h/h4468.md) of [ʿÔḡ](../../strongs/h/h5747.md) in [Bāšān](../../strongs/h/h1316.md), which [mālaḵ](../../strongs/h/h4427.md) in [ʿAštārōṯ](../../strongs/h/h6252.md) and in ['Eḏrᵊʿî](../../strongs/h/h154.md), who [šā'ar](../../strongs/h/h7604.md) of the [yeṯer](../../strongs/h/h3499.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md): for these did [Mōshe](../../strongs/h/h4872.md) [nakah](../../strongs/h/h5221.md), and cast them [yarash](../../strongs/h/h3423.md).

<a name="joshua_13_13"></a>Joshua 13:13

Nevertheless the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yarash](../../strongs/h/h3423.md) not the [Gᵊšûrî](../../strongs/h/h1651.md), nor the [Maʿăḵāṯî](../../strongs/h/h4602.md): but the [Gᵊšûr](../../strongs/h/h1650.md) and the [Maʿăḵâ](../../strongs/h/h4601.md) [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) the [Yisra'el](../../strongs/h/h3478.md) until this [yowm](../../strongs/h/h3117.md).

<a name="joshua_13_14"></a>Joshua 13:14

Only unto the [shebet](../../strongs/h/h7626.md) of [Lēvî](../../strongs/h/h3878.md) he [nathan](../../strongs/h/h5414.md) none [nachalah](../../strongs/h/h5159.md); the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) made by ['iššê](../../strongs/h/h801.md) are their [nachalah](../../strongs/h/h5159.md), as he [dabar](../../strongs/h/h1696.md) unto them.

<a name="joshua_13_15"></a>Joshua 13:15

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) unto the [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_13_16"></a>Joshua 13:16

And their [gᵊḇûl](../../strongs/h/h1366.md) was from [ʿĂrôʿēr](../../strongs/h/h6177.md), that is on the [saphah](../../strongs/h/h8193.md) of the [nachal](../../strongs/h/h5158.md) ['arnôn](../../strongs/h/h769.md), and the [ʿîr](../../strongs/h/h5892.md) that is in the [tavek](../../strongs/h/h8432.md) of the [nachal](../../strongs/h/h5158.md), and all the [mîšôr](../../strongs/h/h4334.md) by [Mêḏḇā'](../../strongs/h/h4311.md);

<a name="joshua_13_17"></a>Joshua 13:17

[Hešbôn](../../strongs/h/h2809.md), and all her [ʿîr](../../strongs/h/h5892.md) that are in the [mîšôr](../../strongs/h/h4334.md); [Dîḇōvn](../../strongs/h/h1769.md), and [bāmôṯ](../../strongs/h/h1120.md), and [Bêṯ BaʿAl MᵊʿVn](../../strongs/h/h1010.md),

<a name="joshua_13_18"></a>Joshua 13:18

And [Yahaṣ](../../strongs/h/h3096.md), and [Qᵊḏēmôṯ](../../strongs/h/h6932.md), and [Mēvp̄AʿAṯ](../../strongs/h/h4158.md),

<a name="joshua_13_19"></a>Joshua 13:19

And [Qiryāṯayim](../../strongs/h/h7156.md), and [Śᵊḇām](../../strongs/h/h7643.md), and [Ṣereṯ Haššaḥar](../../strongs/h/h6890.md) in the [har](../../strongs/h/h2022.md) of the [ʿēmeq](../../strongs/h/h6010.md),

<a name="joshua_13_20"></a>Joshua 13:20

And [Bêṯ PᵊʿÔr](../../strongs/h/h1047.md), and [ʼAshdôwth hap-Piçgâh](../../strongs/h/h798.md) ['ăšēḏâ](../../strongs/h/h794.md), and [Bêṯ Hayšîmôṯ](../../strongs/h/h1020.md),

<a name="joshua_13_21"></a>Joshua 13:21

And all the [ʿîr](../../strongs/h/h5892.md) of the [mîšôr](../../strongs/h/h4334.md), and all the [mamlāḵûṯ](../../strongs/h/h4468.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which [mālaḵ](../../strongs/h/h4427.md) in [Hešbôn](../../strongs/h/h2809.md), whom [Mōshe](../../strongs/h/h4872.md) [nakah](../../strongs/h/h5221.md) with the [nāśî'](../../strongs/h/h5387.md) of [Miḏyān](../../strongs/h/h4080.md), ['Ĕvî](../../strongs/h/h189.md), and [Reqem](../../strongs/h/h7552.md), and [Ṣaûār](../../strongs/h/h6698.md), and [Ḥûr](../../strongs/h/h2354.md), and [Reḇaʿ](../../strongs/h/h7254.md), which were [nāsîḵ](../../strongs/h/h5257.md) of [Sîḥôn](../../strongs/h/h5511.md), [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md).

<a name="joshua_13_22"></a>Joshua 13:22

[Bilʿām](../../strongs/h/h1109.md) also the [ben](../../strongs/h/h1121.md) of [Bᵊʿôr](../../strongs/h/h1160.md), the [qāsam](../../strongs/h/h7080.md), did the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md) among ['ēl](../../strongs/h/h413.md) that were [ḥālāl](../../strongs/h/h2491.md) by them.

<a name="joshua_13_23"></a>Joshua 13:23

And the [gᵊḇûl](../../strongs/h/h1366.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) was [Yardēn](../../strongs/h/h3383.md), and the [gᵊḇûl](../../strongs/h/h1366.md) thereof. This was the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) after their [mišpāḥâ](../../strongs/h/h4940.md), the [ʿîr](../../strongs/h/h5892.md) and the [ḥāṣēr](../../strongs/h/h2691.md) thereof.

<a name="joshua_13_24"></a>Joshua 13:24

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) unto the [maṭṭê](../../strongs/h/h4294.md) of [Gāḏ](../../strongs/h/h1410.md), even unto the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) according to their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_13_25"></a>Joshua 13:25

And their [gᵊḇûl](../../strongs/h/h1366.md) was [Yaʿzêr](../../strongs/h/h3270.md), and all the [ʿîr](../../strongs/h/h5892.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and [ḥēṣî](../../strongs/h/h2677.md) the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), unto [ʿĂrôʿēr](../../strongs/h/h6177.md) that is [paniym](../../strongs/h/h6440.md) [Rabâ](../../strongs/h/h7237.md);

<a name="joshua_13_26"></a>Joshua 13:26

And from [Hešbôn](../../strongs/h/h2809.md) unto [Rāmaṯ Hammiṣpê](../../strongs/h/h7434.md), and [Bᵊṭōnîm](../../strongs/h/h993.md); and from [Maḥănayim](../../strongs/h/h4266.md) unto the [gᵊḇûl](../../strongs/h/h1366.md) of [dᵊḇîr](../../strongs/h/h1688.md);

<a name="joshua_13_27"></a>Joshua 13:27

And in the [ʿēmeq](../../strongs/h/h6010.md), [Bêṯ Hārām](../../strongs/h/h1027.md), and [Bêṯ Nimrâ](../../strongs/h/h1039.md), and [Sukôṯ](../../strongs/h/h5523.md), and [Ṣāp̄Ôn](../../strongs/h/h6829.md), the [yeṯer](../../strongs/h/h3499.md) of the [mamlāḵûṯ](../../strongs/h/h4468.md) of [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md), [Yardēn](../../strongs/h/h3383.md) and his [gᵊḇûl](../../strongs/h/h1366.md), even unto the [qāṣê](../../strongs/h/h7097.md) of the [yam](../../strongs/h/h3220.md) of [Kinnᵊrôṯ](../../strongs/h/h3672.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) [mizrach](../../strongs/h/h4217.md).

<a name="joshua_13_28"></a>Joshua 13:28

This is the [nachalah](../../strongs/h/h5159.md) of the [ben](../../strongs/h/h1121.md) of [Gāḏ](../../strongs/h/h1410.md) after their [mišpāḥâ](../../strongs/h/h4940.md), the [ʿîr](../../strongs/h/h5892.md), and their [ḥāṣēr](../../strongs/h/h2691.md).

<a name="joshua_13_29"></a>Joshua 13:29

And [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) unto the [ḥēṣî](../../strongs/h/h2677.md) [shebet](../../strongs/h/h7626.md) of [Mᵊnaššê](../../strongs/h/h4519.md): and the [ḥēṣî](../../strongs/h/h2677.md) [maṭṭê](../../strongs/h/h4294.md) of the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md) by their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_13_30"></a>Joshua 13:30

And their [gᵊḇûl](../../strongs/h/h1366.md) was from [Maḥănayim](../../strongs/h/h4266.md), all [Bāšān](../../strongs/h/h1316.md), all the [mamlāḵûṯ](../../strongs/h/h4468.md) of [ʿÔḡ](../../strongs/h/h5747.md) [melek](../../strongs/h/h4428.md) of [Bāšān](../../strongs/h/h1316.md), and all the [ḥaûâ](../../strongs/h/h2333.md) of [Yā'Îr](../../strongs/h/h2971.md), which are in [Bāšān](../../strongs/h/h1316.md), [šiššîm](../../strongs/h/h8346.md) [ʿîr](../../strongs/h/h5892.md):

<a name="joshua_13_31"></a>Joshua 13:31

And [ḥēṣî](../../strongs/h/h2677.md) [Gilʿāḏ](../../strongs/h/h1568.md), and [ʿAštārōṯ](../../strongs/h/h6252.md), and ['Eḏrᵊʿî](../../strongs/h/h154.md), [ʿîr](../../strongs/h/h5892.md) of the [mamlāḵûṯ](../../strongs/h/h4468.md) of [ʿÔḡ](../../strongs/h/h5747.md) in [Bāšān](../../strongs/h/h1316.md), were pertaining unto the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md) the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), even to the one [ḥēṣî](../../strongs/h/h2677.md) of the [ben](../../strongs/h/h1121.md) of [Māḵîr](../../strongs/h/h4353.md) by their [mišpāḥâ](../../strongs/h/h4940.md).

<a name="joshua_13_32"></a>Joshua 13:32

These are which [Mōshe](../../strongs/h/h4872.md) [nāḥal](../../strongs/h/h5157.md) in the ['arabah](../../strongs/h/h6160.md) of [Mô'āḇ](../../strongs/h/h4124.md), on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md), by [Yᵊrēḥô](../../strongs/h/h3405.md), [mizrach](../../strongs/h/h4217.md).

<a name="joshua_13_33"></a>Joshua 13:33

But unto the [shebet](../../strongs/h/h7626.md) of [Lēvî](../../strongs/h/h3878.md) [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) not any [nachalah](../../strongs/h/h5159.md): [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) was their [nachalah](../../strongs/h/h5159.md), as he [dabar](../../strongs/h/h1696.md) unto them.

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 12](joshua_12.md) - [Joshua 14](joshua_14.md)