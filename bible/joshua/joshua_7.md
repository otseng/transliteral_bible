# [Joshua 7](https://www.blueletterbible.org/kjv/joshua/7)

<a name="joshua_7_1"></a>Joshua 7:1

But the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md) in the [ḥērem](../../strongs/h/h2764.md): for [ʿĀḵān](../../strongs/h/h5912.md), the [ben](../../strongs/h/h1121.md) of [Karmî](../../strongs/h/h3756.md), the [ben](../../strongs/h/h1121.md) of [Zaḇdî](../../strongs/h/h2067.md), the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), [laqach](../../strongs/h/h3947.md) of the [ḥērem](../../strongs/h/h2764.md): and the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_7_2"></a>Joshua 7:2

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shalach](../../strongs/h/h7971.md) ['enowsh](../../strongs/h/h582.md) from [Yᵊrēḥô](../../strongs/h/h3405.md) to [ʿAy](../../strongs/h/h5857.md), which is [ʿim](../../strongs/h/h5973.md) [Bêṯ 'Āven](../../strongs/h/h1007.md), on the [qeḏem](../../strongs/h/h6924.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md), and ['āmar](../../strongs/h/h559.md) unto them, ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) and [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md). And the ['enowsh](../../strongs/h/h582.md) went [ʿālâ](../../strongs/h/h5927.md) and [ragal](../../strongs/h/h7270.md) [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_7_3"></a>Joshua 7:3

And they [shuwb](../../strongs/h/h7725.md) to [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and ['āmar](../../strongs/h/h559.md) unto him, Let not all the ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md); but let about two or [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) [ʿālâ](../../strongs/h/h5927.md) and [nakah](../../strongs/h/h5221.md) [ʿAy](../../strongs/h/h5857.md); and make not all the ['am](../../strongs/h/h5971.md) to [yaga'](../../strongs/h/h3021.md) thither; for they are but [mᵊʿaṭ](../../strongs/h/h4592.md).

<a name="joshua_7_4"></a>Joshua 7:4

So there went [ʿālâ](../../strongs/h/h5927.md) thither of the ['am](../../strongs/h/h5971.md) about [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md): and they [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) the ['enowsh](../../strongs/h/h582.md) of [ʿAy](../../strongs/h/h5857.md).

<a name="joshua_7_5"></a>Joshua 7:5

And the ['enowsh](../../strongs/h/h582.md) of [ʿAy](../../strongs/h/h5857.md) [nakah](../../strongs/h/h5221.md) of them about [šᵊlōšîm](../../strongs/h/h7970.md) and [šēš](../../strongs/h/h8337.md) ['iysh](../../strongs/h/h376.md): for they [radaph](../../strongs/h/h7291.md) them from [paniym](../../strongs/h/h6440.md) the [sha'ar](../../strongs/h/h8179.md) even unto [Šᵊḇārîm](../../strongs/h/h7671.md), and [nakah](../../strongs/h/h5221.md) them in the [môrāḏ](../../strongs/h/h4174.md): wherefore the [lebab](../../strongs/h/h3824.md) of the ['am](../../strongs/h/h5971.md) [māsas](../../strongs/h/h4549.md), and became as [mayim](../../strongs/h/h4325.md).

<a name="joshua_7_6"></a>Joshua 7:6

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [qāraʿ](../../strongs/h/h7167.md) his [śimlâ](../../strongs/h/h8071.md), and [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md) upon his [paniym](../../strongs/h/h6440.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) until the ['ereb](../../strongs/h/h6153.md), he and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and [ʿālâ](../../strongs/h/h5927.md) ['aphar](../../strongs/h/h6083.md) upon their [ro'sh](../../strongs/h/h7218.md).

<a name="joshua_7_7"></a>Joshua 7:7

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md), Alas, ['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), wherefore hast thou at ['abar](../../strongs/h/h5674.md) ['abar](../../strongs/h/h5674.md) this ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), to [nathan](../../strongs/h/h5414.md) us into the [yad](../../strongs/h/h3027.md) of the ['Ĕmōrî](../../strongs/h/h567.md), to ['abad](../../strongs/h/h6.md) us? would to [lû'](../../strongs/h/h3863.md) we had been [yā'al](../../strongs/h/h2974.md), and [yashab](../../strongs/h/h3427.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md)!

<a name="joshua_7_8"></a>Joshua 7:8

[bî](../../strongs/h/h994.md) ['adonay](../../strongs/h/h136.md), what shall I ['āmar](../../strongs/h/h559.md), ['aḥar](../../strongs/h/h310.md) [Yisra'el](../../strongs/h/h3478.md) [hāp̄aḵ](../../strongs/h/h2015.md) their [ʿōrep̄](../../strongs/h/h6203.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md)!

<a name="joshua_7_9"></a>Joshua 7:9

For the [Kᵊnaʿănî](../../strongs/h/h3669.md) and all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) shall [shama'](../../strongs/h/h8085.md) of it, and shall [cabab](../../strongs/h/h5437.md) us, and [karath](../../strongs/h/h3772.md) our [shem](../../strongs/h/h8034.md) from the ['erets](../../strongs/h/h776.md): and what wilt thou ['asah](../../strongs/h/h6213.md) unto thy [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md)?

<a name="joshua_7_10"></a>Joshua 7:10

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [quwm](../../strongs/h/h6965.md) thee; wherefore [naphal](../../strongs/h/h5307.md) thou thus upon thy [paniym](../../strongs/h/h6440.md)?

<a name="joshua_7_11"></a>Joshua 7:11

[Yisra'el](../../strongs/h/h3478.md) hath [chata'](../../strongs/h/h2398.md), and they have also ['abar](../../strongs/h/h5674.md) my [bĕriyth](../../strongs/h/h1285.md) which I [tsavah](../../strongs/h/h6680.md) them: for they have even [laqach](../../strongs/h/h3947.md) of the [ḥērem](../../strongs/h/h2764.md), and have also [ganab](../../strongs/h/h1589.md), and [kāḥaš](../../strongs/h/h3584.md) also, and they have [śûm](../../strongs/h/h7760.md) it even among their own [kĕliy](../../strongs/h/h3627.md).

<a name="joshua_7_12"></a>Joshua 7:12

Therefore the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yakol](../../strongs/h/h3201.md) not [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), but [panah](../../strongs/h/h6437.md) their [ʿōrep̄](../../strongs/h/h6203.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), because they were [ḥērem](../../strongs/h/h2764.md): neither will I be with you any [yāsap̄](../../strongs/h/h3254.md), [lō'](../../strongs/h/h3808.md) ye [šāmaḏ](../../strongs/h/h8045.md) the [ḥērem](../../strongs/h/h2764.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="joshua_7_13"></a>Joshua 7:13

[quwm](../../strongs/h/h6965.md), [qadash](../../strongs/h/h6942.md) the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md), [qadash](../../strongs/h/h6942.md) yourselves against [māḥār](../../strongs/h/h4279.md): for thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), There is an [ḥērem](../../strongs/h/h2764.md) in the [qereḇ](../../strongs/h/h7130.md) of thee, O [Yisra'el](../../strongs/h/h3478.md): thou [yakol](../../strongs/h/h3201.md) not [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) thine ['oyeb](../../strongs/h/h341.md), until ye [cuwr](../../strongs/h/h5493.md) the [ḥērem](../../strongs/h/h2764.md) from [qereḇ](../../strongs/h/h7130.md) you.

<a name="joshua_7_14"></a>Joshua 7:14

In the [boqer](../../strongs/h/h1242.md) therefore ye shall be [qāraḇ](../../strongs/h/h7126.md) according to your [shebet](../../strongs/h/h7626.md): and it shall be, that the [shebet](../../strongs/h/h7626.md) which [Yĕhovah](../../strongs/h/h3068.md) [lāḵaḏ](../../strongs/h/h3920.md) shall [qāraḇ](../../strongs/h/h7126.md) according to the [mišpāḥâ](../../strongs/h/h4940.md) thereof; and the [mišpāḥâ](../../strongs/h/h4940.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [lāḵaḏ](../../strongs/h/h3920.md) shall [qāraḇ](../../strongs/h/h7126.md) by [bayith](../../strongs/h/h1004.md); and the [bayith](../../strongs/h/h1004.md) which [Yĕhovah](../../strongs/h/h3068.md) shall [lāḵaḏ](../../strongs/h/h3920.md) shall [qāraḇ](../../strongs/h/h7126.md) [geḇer](../../strongs/h/h1397.md) by [geḇer](../../strongs/h/h1397.md).

<a name="joshua_7_15"></a>Joshua 7:15

And it shall be, that he that is [lāḵaḏ](../../strongs/h/h3920.md) with the [ḥērem](../../strongs/h/h2764.md) shall be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md), he and all that he hath: because he hath ['abar](../../strongs/h/h5674.md) the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), and because he hath ['asah](../../strongs/h/h6213.md) [nᵊḇālâ](../../strongs/h/h5039.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="joshua_7_16"></a>Joshua 7:16

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [qāraḇ](../../strongs/h/h7126.md) [Yisra'el](../../strongs/h/h3478.md) by their [shebet](../../strongs/h/h7626.md); and the [shebet](../../strongs/h/h7626.md) of [Yehuwdah](../../strongs/h/h3063.md) was [lāḵaḏ](../../strongs/h/h3920.md):

<a name="joshua_7_17"></a>Joshua 7:17

And he [qāraḇ](../../strongs/h/h7126.md) the [mišpāḥâ](../../strongs/h/h4940.md) of [Yehuwdah](../../strongs/h/h3063.md); and he [lāḵaḏ](../../strongs/h/h3920.md) the [mišpāḥâ](../../strongs/h/h4940.md) of the [Zarḥî](../../strongs/h/h2227.md): and he [qāraḇ](../../strongs/h/h7126.md) the [mišpāḥâ](../../strongs/h/h4940.md) of the [Zarḥî](../../strongs/h/h2227.md) [geḇer](../../strongs/h/h1397.md) by [geḇer](../../strongs/h/h1397.md); and [Zaḇdî](../../strongs/h/h2067.md) was [lāḵaḏ](../../strongs/h/h3920.md):

<a name="joshua_7_18"></a>Joshua 7:18

And he [qāraḇ](../../strongs/h/h7126.md) his [bayith](../../strongs/h/h1004.md) [geḇer](../../strongs/h/h1397.md) by [geḇer](../../strongs/h/h1397.md); and [ʿĀḵān](../../strongs/h/h5912.md), the [ben](../../strongs/h/h1121.md) of [Karmî](../../strongs/h/h3756.md), the [ben](../../strongs/h/h1121.md) of [Zaḇdî](../../strongs/h/h2067.md), the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Yehuwdah](../../strongs/h/h3063.md), was [lāḵaḏ](../../strongs/h/h3920.md).

<a name="joshua_7_19"></a>Joshua 7:19

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md) unto [ʿĀḵān](../../strongs/h/h5912.md), My [ben](../../strongs/h/h1121.md), [śûm](../../strongs/h/h7760.md), I pray thee, [kabowd](../../strongs/h/h3519.md) to [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), and [nathan](../../strongs/h/h5414.md) [tôḏâ](../../strongs/h/h8426.md) unto him; and [nāḡaḏ](../../strongs/h/h5046.md) me now what thou hast ['asah](../../strongs/h/h6213.md); [kāḥaḏ](../../strongs/h/h3582.md) it not from me.

<a name="joshua_7_20"></a>Joshua 7:20

And [ʿĀḵān](../../strongs/h/h5912.md) ['anah](../../strongs/h/h6030.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and ['āmar](../../strongs/h/h559.md), ['āmnâ](../../strongs/h/h546.md) I have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), and thus and thus have I ['asah](../../strongs/h/h6213.md):

<a name="joshua_7_21"></a>Joshua 7:21

When I [ra'ah](../../strongs/h/h7200.md) among the [šālāl](../../strongs/h/h7998.md) ['echad](../../strongs/h/h259.md) [towb](../../strongs/h/h2896.md) [Šinʿār](../../strongs/h/h8152.md) ['adereṯ](../../strongs/h/h155.md), and two [mē'â](../../strongs/h/h3967.md) [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), and ['echad](../../strongs/h/h259.md) [lashown](../../strongs/h/h3956.md) of [zāhāḇ](../../strongs/h/h2091.md) of [ḥămisheem](../../strongs/h/h2572.md) [šeqel](../../strongs/h/h8255.md) [mišqāl](../../strongs/h/h4948.md), then I [chamad](../../strongs/h/h2530.md) them, and [laqach](../../strongs/h/h3947.md) them; and, behold, they are [taman](../../strongs/h/h2934.md) in the ['erets](../../strongs/h/h776.md) in the [tavek](../../strongs/h/h8432.md) of my ['ohel](../../strongs/h/h168.md), and the [keceph](../../strongs/h/h3701.md) under it.

<a name="joshua_7_22"></a>Joshua 7:22

So [Yᵊhôšûaʿ](../../strongs/h/h3091.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md), and they [rûṣ](../../strongs/h/h7323.md) unto the ['ohel](../../strongs/h/h168.md); and, behold, it was [taman](../../strongs/h/h2934.md) in his ['ohel](../../strongs/h/h168.md), and the [keceph](../../strongs/h/h3701.md) under it.

<a name="joshua_7_23"></a>Joshua 7:23

And they [laqach](../../strongs/h/h3947.md) them out of the [tavek](../../strongs/h/h8432.md) of the ['ohel](../../strongs/h/h168.md), and [bow'](../../strongs/h/h935.md) them unto [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and [yāṣaq](../../strongs/h/h3332.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="joshua_7_24"></a>Joshua 7:24

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, [laqach](../../strongs/h/h3947.md) [ʿĀḵān](../../strongs/h/h5912.md) the [ben](../../strongs/h/h1121.md) of [Zeraḥ](../../strongs/h/h2226.md), and the [keceph](../../strongs/h/h3701.md), and the ['adereṯ](../../strongs/h/h155.md), and the [lashown](../../strongs/h/h3956.md) of [zāhāḇ](../../strongs/h/h2091.md), and his [ben](../../strongs/h/h1121.md), and his [bath](../../strongs/h/h1323.md), and his [showr](../../strongs/h/h7794.md), and his [chamowr](../../strongs/h/h2543.md), and his [tso'n](../../strongs/h/h6629.md), and his ['ohel](../../strongs/h/h168.md), and all that he had: and they [ʿālâ](../../strongs/h/h5927.md) them unto the [ʿēmeq](../../strongs/h/h6010.md) of [ʿāḵôr](../../strongs/h/h5911.md).

<a name="joshua_7_25"></a>Joshua 7:25

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['āmar](../../strongs/h/h559.md), [mah](../../strongs/h/h4100.md) hast thou [ʿāḵar](../../strongs/h/h5916.md) us? [Yĕhovah](../../strongs/h/h3068.md) shall [ʿāḵar](../../strongs/h/h5916.md) thee this [yowm](../../strongs/h/h3117.md). And all [Yisra'el](../../strongs/h/h3478.md) [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md), and [śārap̄](../../strongs/h/h8313.md) them with ['esh](../../strongs/h/h784.md), after they had [sāqal](../../strongs/h/h5619.md) them with ['eben](../../strongs/h/h68.md).

<a name="joshua_7_26"></a>Joshua 7:26

And they [quwm](../../strongs/h/h6965.md) over him a [gadowl](../../strongs/h/h1419.md) [gal](../../strongs/h/h1530.md) of ['eben](../../strongs/h/h68.md) unto this [yowm](../../strongs/h/h3117.md). So [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) from the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md). Wherefore the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) was [qara'](../../strongs/h/h7121.md), The [ʿēmeq](../../strongs/h/h6010.md) of [ʿāḵôr](../../strongs/h/h5911.md), unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Joshua](joshua.md)

[Joshua 6](joshua_6.md) - [Joshua 8](joshua_8.md)