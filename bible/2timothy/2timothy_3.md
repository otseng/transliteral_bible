# [2 Timothy 3](https://www.blueletterbible.org/kjv/2ti/3/16/s_1128001)

<a name="2 Timothy_3_1"></a>2 Timothy 3:1

This [ginōskō](../../strongs/g/g1097.md) also, that in the [eschatos](../../strongs/g/g2078.md) [hēmera](../../strongs/g/g2250.md) [chalepos](../../strongs/g/g5467.md) [kairos](../../strongs/g/g2540.md) shall [enistēmi](../../strongs/g/g1764.md).

<a name="2 Timothy_3_2"></a>2 Timothy 3:2

For [anthrōpos](../../strongs/g/g444.md) shall be [philautos](../../strongs/g/g5367.md), [philargyros](../../strongs/g/g5366.md), [alazon](../../strongs/g/g213.md), [hyperēphanos](../../strongs/g/g5244.md), [blasphēmos](../../strongs/g/g989.md), [apeithēs](../../strongs/g/g545.md) to [goneus](../../strongs/g/g1118.md), [acharistos](../../strongs/g/g884.md), [anosios](../../strongs/g/g462.md),

<a name="2 Timothy_3_3"></a>2 Timothy 3:3

[astorgos](../../strongs/g/g794.md), [aspondos](../../strongs/g/g786.md), [diabolos](../../strongs/g/g1228.md), [akratēs](../../strongs/g/g193.md), [anēmeros](../../strongs/g/g434.md), [aphilagathos](../../strongs/g/g865.md),

<a name="2 Timothy_3_4"></a>2 Timothy 3:4

[prodotēs](../../strongs/g/g4273.md), [propetēs](../../strongs/g/g4312.md), [typhoō](../../strongs/g/g5187.md), [philēdonos](../../strongs/g/g5369.md) more than [philotheos](../../strongs/g/g5377.md);

<a name="2 Timothy_3_5"></a>2 Timothy 3:5

Having a [morphōsis](../../strongs/g/g3446.md) of [eusebeia](../../strongs/g/g2150.md), but [arneomai](../../strongs/g/g720.md) the [dynamis](../../strongs/g/g1411.md) thereof: from such [apotrepō](../../strongs/g/g665.md).

<a name="2 Timothy_3_6"></a>2 Timothy 3:6

For of this sort are they which [endynō](../../strongs/g/g1744.md) into [oikia](../../strongs/g/g3614.md), and [aichmalōteuō](../../strongs/g/g162.md) [gynaikarion](../../strongs/g/g1133.md) [sōreuō](../../strongs/g/g4987.md) with [hamartia](../../strongs/g/g266.md), [agō](../../strongs/g/g71.md) with [poikilos](../../strongs/g/g4164.md) [epithymia](../../strongs/g/g1939.md),

<a name="2 Timothy_3_7"></a>2 Timothy 3:7

[pantote](../../strongs/g/g3842.md) [manthanō](../../strongs/g/g3129.md), and [mēdepote](../../strongs/g/g3368.md) [dynamai](../../strongs/g/g1410.md) to [erchomai](../../strongs/g/g2064.md) to the [epignōsis](../../strongs/g/g1922.md) of the [alētheia](../../strongs/g/g225.md).

<a name="2 Timothy_3_8"></a>2 Timothy 3:8

Now as [iannēs](../../strongs/g/g2389.md) and [iambrēs](../../strongs/g/g2387.md) [anthistēmi](../../strongs/g/g436.md) [Mōÿsēs](../../strongs/g/g3475.md), so do these also [anthistēmi](../../strongs/g/g436.md) the [alētheia](../../strongs/g/g225.md): [anthrōpos](../../strongs/g/g444.md) of [kataphtheirō](../../strongs/g/g2704.md) [nous](../../strongs/g/g3563.md), [adokimos](../../strongs/g/g96.md) concerning the [pistis](../../strongs/g/g4102.md). [^1]

<a name="2 Timothy_3_9"></a>2 Timothy 3:9

But they shall [prokoptō](../../strongs/g/g4298.md) no further: for their [anoia](../../strongs/g/g454.md) shall be [ekdēlos](../../strongs/g/g1552.md) unto all, as their's also was.

<a name="2 Timothy_3_10"></a>2 Timothy 3:10

But thou hast [parakoloutheō](../../strongs/g/g3877.md) my [didaskalia](../../strongs/g/g1319.md), [agōgē](../../strongs/g/g72.md), [prothesis](../../strongs/g/g4286.md), [pistis](../../strongs/g/g4102.md), [makrothymia](../../strongs/g/g3115.md), [agapē](../../strongs/g/g26.md), [hypomonē](../../strongs/g/g5281.md),

<a name="2 Timothy_3_11"></a>2 Timothy 3:11

[diōgmos](../../strongs/g/g1375.md), [pathēma](../../strongs/g/g3804.md), which [ginomai](../../strongs/g/g1096.md) unto me at [Antiocheia](../../strongs/g/g490.md), at [Ikonion](../../strongs/g/g2430.md), at [Lystra](../../strongs/g/g3082.md); what [diōgmos](../../strongs/g/g1375.md) I [hypopherō](../../strongs/g/g5297.md): but out of them all the [kyrios](../../strongs/g/g2962.md) [rhyomai](../../strongs/g/g4506.md) me.

<a name="2 Timothy_3_12"></a>2 Timothy 3:12

Yea, and all that will [zaō](../../strongs/g/g2198.md) [eusebōs](../../strongs/g/g2153.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) shall [diōkō](../../strongs/g/g1377.md).

<a name="2 Timothy_3_13"></a>2 Timothy 3:13

But [ponēros](../../strongs/g/g4190.md) [anthrōpos](../../strongs/g/g444.md) and [goēs](../../strongs/g/g1114.md) shall [prokoptō](../../strongs/g/g4298.md) [epi](../../strongs/g/g1909.md) [cheirōn](../../strongs/g/g5501.md), [planaō](../../strongs/g/g4105.md), and being [planaō](../../strongs/g/g4105.md).

<a name="2 Timothy_3_14"></a>2 Timothy 3:14

But [menō](../../strongs/g/g3306.md) thou in the things which thou hast [manthanō](../../strongs/g/g3129.md) and hast been [pistoō](../../strongs/g/g4104.md), [eidō](../../strongs/g/g1492.md) of whom thou hast [manthanō](../../strongs/g/g3129.md);

<a name="2 Timothy_3_15"></a>2 Timothy 3:15

And that from a [brephos](../../strongs/g/g1025.md) thou hast [eidō](../../strongs/g/g1492.md) the [hieros](../../strongs/g/g2413.md) [gramma](../../strongs/g/g1121.md), which are able to [sophizō](../../strongs/g/g4679.md) thee unto [sōtēria](../../strongs/g/g4991.md) through [pistis](../../strongs/g/g4102.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="2 Timothy_3_16"></a>2 Timothy 3:16

All [graphē](../../strongs/g/g1124.md) is [theopneustos](../../strongs/g/g2315.md), and is [ōphelimos](../../strongs/g/g5624.md) for [didaskalia](../../strongs/g/g1319.md), for [elegchos](../../strongs/g/g1650.md), for [epanorthōsis](../../strongs/g/g1882.md), for [paideia](../../strongs/g/g3809.md) in [dikaiosynē](../../strongs/g/g1343.md):

<a name="2 Timothy_3_17"></a>2 Timothy 3:17

That the [anthrōpos](../../strongs/g/g444.md) of [theos](../../strongs/g/g2316.md) may be [artios](../../strongs/g/g739.md), [exartizō](../../strongs/g/g1822.md) unto all [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md).

---

[Transliteral Bible](../bible.md)

[2 Timothy](2timothy.md)

[2 Timothy 2](2timothy_2.md) - [2 Timothy 4](2timothy_4.md)

---

[^1]: [2 Timothy 3:8 Commentary](../../commentary/2timothy/2timothy_3_commentary.md#2timothy_3_8)