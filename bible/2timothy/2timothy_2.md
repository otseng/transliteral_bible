# [2 Timothy 2](https://www.blueletterbible.org/kjv/2ti/2/1/s_1127001)

<a name="2timothy_2_1"></a>2 Timothy 2:1

Thou therefore, my [teknon](../../strongs/g/g5043.md), be [endynamoō](../../strongs/g/g1743.md) in the [charis](../../strongs/g/g5485.md) that is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="2timothy_2_2"></a>2 Timothy 2:2

And the things that thou hast [akouō](../../strongs/g/g191.md) of me among [polys](../../strongs/g/g4183.md) [martys](../../strongs/g/g3144.md), the same [paratithēmi](../../strongs/g/g3908.md) thou to [pistos](../../strongs/g/g4103.md) [anthrōpos](../../strongs/g/g444.md), who shall be [hikanos](../../strongs/g/g2425.md) to [didaskō](../../strongs/g/g1321.md) others also.

<a name="2timothy_2_3"></a>2 Timothy 2:3

Thou therefore [kakopatheō](../../strongs/g/g2553.md), as a [kalos](../../strongs/g/g2570.md) [stratiōtēs](../../strongs/g/g4757.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2timothy_2_4"></a>2 Timothy 2:4

[oudeis](../../strongs/g/g3762.md) that [strateuō](../../strongs/g/g4754.md) [emplekō](../../strongs/g/g1707.md) himself with the [pragmateia](../../strongs/g/g4230.md) of [bios](../../strongs/g/g979.md); that he may [areskō](../../strongs/g/g700.md) him who hath [stratologeō](../../strongs/g/g4758.md).

<a name="2timothy_2_5"></a>2 Timothy 2:5

And if [tis](../../strongs/g/g5100.md) also [athleō](../../strongs/g/g118.md), is he not [stephanoō](../../strongs/g/g4737.md), except he [athleō](../../strongs/g/g118.md) [nomimōs](../../strongs/g/g3545.md).

<a name="2timothy_2_6"></a>2 Timothy 2:6

The [geōrgos](../../strongs/g/g1092.md) that [kopiaō](../../strongs/g/g2872.md) must be first [metalambanō](../../strongs/g/g3335.md) of the [karpos](../../strongs/g/g2590.md).

<a name="2timothy_2_7"></a>2 Timothy 2:7

[noeō](../../strongs/g/g3539.md) what I [legō](../../strongs/g/g3004.md); and the [kyrios](../../strongs/g/g2962.md) [didōmi](../../strongs/g/g1325.md) thee [synesis](../../strongs/g/g4907.md) in all things.

<a name="2timothy_2_8"></a>2 Timothy 2:8

[mnēmoneuō](../../strongs/g/g3421.md) that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) of the [sperma](../../strongs/g/g4690.md) of [Dabid](../../strongs/g/g1138.md) was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md) according to my [euaggelion](../../strongs/g/g2098.md):

<a name="2timothy_2_9"></a>2 Timothy 2:9

Wherein I [kakopatheō](../../strongs/g/g2553.md), as a [kakourgos](../../strongs/g/g2557.md), unto [desmos](../../strongs/g/g1199.md); but the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) is not [deō](../../strongs/g/g1210.md).

<a name="2timothy_2_10"></a>2 Timothy 2:10

Therefore I [hypomenō](../../strongs/g/g5278.md) all things for the [eklektos](../../strongs/g/g1588.md) sakes, that they may also [tygchanō](../../strongs/g/g5177.md) the [sōtēria](../../strongs/g/g4991.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) with [aiōnios](../../strongs/g/g166.md) [doxa](../../strongs/g/g1391.md).

<a name="2timothy_2_11"></a>2 Timothy 2:11

a [pistos](../../strongs/g/g4103.md) [logos](../../strongs/g/g3056.md): For if we be [synapothnēskō](../../strongs/g/g4880.md), we shall also [syzaō](../../strongs/g/g4800.md) with him:

<a name="2timothy_2_12"></a>2 Timothy 2:12

If we [hypomenō](../../strongs/g/g5278.md), we shall also [symbasileuō](../../strongs/g/g4821.md) with: if we [arneomai](../../strongs/g/g720.md), he also will [arneomai](../../strongs/g/g720.md) us:

<a name="2timothy_2_13"></a>2 Timothy 2:13

If we [apisteō](../../strongs/g/g569.md) not, he [menō](../../strongs/g/g3306.md) [pistos](../../strongs/g/g4103.md): he cannot [arneomai](../../strongs/g/g720.md) himself.

<a name="2timothy_2_14"></a>2 Timothy 2:14

Of these things put in [hypomimnēskō](../../strongs/g/g5279.md), [diamartyromai](../../strongs/g/g1263.md) before the [kyrios](../../strongs/g/g2962.md) that they [logomacheō](../../strongs/g/g3054.md) not to no [chrēsimos](../../strongs/g/g5539.md), to the [katastrophē](../../strongs/g/g2692.md) of the [akouō](../../strongs/g/g191.md).

<a name="2timothy_2_15"></a>2 Timothy 2:15

[spoudazō](../../strongs/g/g4704.md) to [paristēmi](../../strongs/g/g3936.md) thyself [dokimos](../../strongs/g/g1384.md) unto [theos](../../strongs/g/g2316.md), an [ergatēs](../../strongs/g/g2040.md) that [anepaischyntos](../../strongs/g/g422.md), [orthotomeō](../../strongs/g/g3718.md) the [logos](../../strongs/g/g3056.md) of [alētheia](../../strongs/g/g225.md).

<a name="2timothy_2_16"></a>2 Timothy 2:16

But [periistēmi](../../strongs/g/g4026.md) [bebēlos](../../strongs/g/g952.md) [kenophōnia](../../strongs/g/g2757.md): for they will [prokoptō](../../strongs/g/g4298.md) unto more [asebeia](../../strongs/g/g763.md).

<a name="2timothy_2_17"></a>2 Timothy 2:17

And their [logos](../../strongs/g/g3056.md) will [nomē](../../strongs/g/g3542.md) as doth a [gangraina](../../strongs/g/g1044.md): of whom is [hymenaios](../../strongs/g/g5211.md) and [philētos](../../strongs/g/g5372.md);

<a name="2timothy_2_18"></a>2 Timothy 2:18

Who concerning the [alētheia](../../strongs/g/g225.md) have [astocheō](../../strongs/g/g795.md), [legō](../../strongs/g/g3004.md) that the [anastasis](../../strongs/g/g386.md) is [ginomai](../../strongs/g/g1096.md) [ēdē](../../strongs/g/g2235.md); and [anatrepō](../../strongs/g/g396.md) the [pistis](../../strongs/g/g4102.md) of some.

<a name="2timothy_2_19"></a>2 Timothy 2:19

Nevertheless the [themelios](../../strongs/g/g2310.md) of [theos](../../strongs/g/g2316.md) [histēmi](../../strongs/g/g2476.md) [stereos](../../strongs/g/g4731.md), having this [sphragis](../../strongs/g/g4973.md), The [kyrios](../../strongs/g/g2962.md) [ginōskō](../../strongs/g/g1097.md) them that are his. And, Let every one that [onomazō](../../strongs/g/g3687.md) the [onoma](../../strongs/g/g3686.md) of [Christos](../../strongs/g/g5547.md) [aphistēmi](../../strongs/g/g868.md) from [adikia](../../strongs/g/g93.md).

<a name="2timothy_2_20"></a>2 Timothy 2:20

But in a [megas](../../strongs/g/g3173.md) [oikia](../../strongs/g/g3614.md) there are not only [skeuos](../../strongs/g/g4632.md) of [chrysous](../../strongs/g/g5552.md) and of [argyreos](../../strongs/g/g693.md), but also of [xylinos](../../strongs/g/g3585.md) and of [ostrakinos](../../strongs/g/g3749.md); and some to [timē](../../strongs/g/g5092.md), and some to [atimia](../../strongs/g/g819.md).

<a name="2timothy_2_21"></a>2 Timothy 2:21

If a man therefore [ekkathairō](../../strongs/g/g1571.md) himself from these, he shall be a [skeuos](../../strongs/g/g4632.md) unto [timē](../../strongs/g/g5092.md), [hagiazō](../../strongs/g/g37.md), and [euchrēstos](../../strongs/g/g2173.md) for the [despotēs](../../strongs/g/g1203.md), and [hetoimazō](../../strongs/g/g2090.md) unto every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md).

<a name="2timothy_2_22"></a>2 Timothy 2:22

[pheugō](../../strongs/g/g5343.md) also [neōterikos](../../strongs/g/g3512.md) [epithymia](../../strongs/g/g1939.md): but [diōkō](../../strongs/g/g1377.md) [dikaiosynē](../../strongs/g/g1343.md), [pistis](../../strongs/g/g4102.md), [agapē](../../strongs/g/g26.md), [eirēnē](../../strongs/g/g1515.md), with them that [epikaleō](../../strongs/g/g1941.md) the [kyrios](../../strongs/g/g2962.md) out of a [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md).

<a name="2timothy_2_23"></a>2 Timothy 2:23

But [mōros](../../strongs/g/g3474.md) and [apaideutos](../../strongs/g/g521.md) [zētēsis](../../strongs/g/g2214.md) [paraiteomai](../../strongs/g/g3868.md), [eidō](../../strongs/g/g1492.md) that they do [gennaō](../../strongs/g/g1080.md) [machē](../../strongs/g/g3163.md).

<a name="2timothy_2_24"></a>2 Timothy 2:24

And the [doulos](../../strongs/g/g1401.md) of the [kyrios](../../strongs/g/g2962.md) must not [machomai](../../strongs/g/g3164.md); but be [ēpios](../../strongs/g/g2261.md) unto all, [didaktikos](../../strongs/g/g1317.md), [anexikakos](../../strongs/g/g420.md),

<a name="2timothy_2_25"></a>2 Timothy 2:25

In [praotēs](../../strongs/g/g4236.md) [paideuō](../../strongs/g/g3811.md) those that [antidiatithēmi](../../strongs/g/g475.md); if [theos](../../strongs/g/g2316.md) peradventure will [didōmi](../../strongs/g/g1325.md) them [metanoia](../../strongs/g/g3341.md) to the [epignōsis](../../strongs/g/g1922.md) of the [alētheia](../../strongs/g/g225.md);

<a name="2timothy_2_26"></a>2 Timothy 2:26

And they may [ananēphō](../../strongs/g/g366.md) themselves out of the [pagis](../../strongs/g/g3803.md) of the [diabolos](../../strongs/g/g1228.md), who are [zōgreō](../../strongs/g/g2221.md) by him at his [thelēma](../../strongs/g/g2307.md).

---

[Transliteral Bible](../bible.md)

[2 Timothy](2timothy.md)

[2 Timothy 1](2timothy_1.md) - [2 Timothy 3](2timothy_3.md)