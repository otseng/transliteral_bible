# [2 Timothy 4](https://www.blueletterbible.org/kjv/2ti/4/1/s_1129001)

<a name="2timothy_4_1"></a>2 Timothy 4:1

I [diamartyromai](../../strongs/g/g1263.md) thee therefore before [theos](../../strongs/g/g2316.md), and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), who shall [krinō](../../strongs/g/g2919.md) the [zaō](../../strongs/g/g2198.md) and the [nekros](../../strongs/g/g3498.md) at his [epiphaneia](../../strongs/g/g2015.md) and his [basileia](../../strongs/g/g932.md);

<a name="2timothy_4_2"></a>2 Timothy 4:2

[kēryssō](../../strongs/g/g2784.md) the [logos](../../strongs/g/g3056.md); [ephistēmi](../../strongs/g/g2186.md) [eukairōs](../../strongs/g/g2122.md), [akairōs](../../strongs/g/g171.md); [elegchō](../../strongs/g/g1651.md), [epitimaō](../../strongs/g/g2008.md), [parakaleō](../../strongs/g/g3870.md) with all [makrothymia](../../strongs/g/g3115.md) and [didachē](../../strongs/g/g1322.md).

<a name="2timothy_4_3"></a>2 Timothy 4:3

For the [kairos](../../strongs/g/g2540.md) will [esomai](../../strongs/g/g2071.md) when they will not [anechō](../../strongs/g/g430.md) [hygiainō](../../strongs/g/g5198.md) [didaskalia](../../strongs/g/g1319.md); but after their own [epithymia](../../strongs/g/g1939.md) shall they [episōreuō](../../strongs/g/g2002.md) to themselves [didaskalos](../../strongs/g/g1320.md), having [knēthō](../../strongs/g/g2833.md) [akoē](../../strongs/g/g189.md);

<a name="2timothy_4_4"></a>2 Timothy 4:4

And they shall [apostrephō](../../strongs/g/g654.md) their [akoē](../../strongs/g/g189.md) from the [alētheia](../../strongs/g/g225.md), and shall be [ektrepō](../../strongs/g/g1624.md) unto [mythos](../../strongs/g/g3454.md).

<a name="2timothy_4_5"></a>2 Timothy 4:5

But [nēphō](../../strongs/g/g3525.md) thou in all things, [kakopatheō](../../strongs/g/g2553.md), [poieō](../../strongs/g/g4160.md) the [ergon](../../strongs/g/g2041.md) of an [euaggelistēs](../../strongs/g/g2099.md), [plērophoreō](../../strongs/g/g4135.md) of thy [diakonia](../../strongs/g/g1248.md).

<a name="2timothy_4_6"></a>2 Timothy 4:6

For I am now [spendō](../../strongs/g/g4689.md), and the [kairos](../../strongs/g/g2540.md) of my [analysis](../../strongs/g/g359.md) is [ephistēmi](../../strongs/g/g2186.md).

<a name="2timothy_4_7"></a>2 Timothy 4:7

I have [agōnizomai](../../strongs/g/g75.md) a [kalos](../../strongs/g/g2570.md) [agōn](../../strongs/g/g73.md), I have [teleō](../../strongs/g/g5055.md) my [dromos](../../strongs/g/g1408.md), I have [tēreō](../../strongs/g/g5083.md) the [pistis](../../strongs/g/g4102.md):

<a name="2timothy_4_8"></a>2 Timothy 4:8

Henceforth there is [apokeimai](../../strongs/g/g606.md) for me a [stephanos](../../strongs/g/g4735.md) of [dikaiosynē](../../strongs/g/g1343.md), which the [kyrios](../../strongs/g/g2962.md), the [dikaios](../../strongs/g/g1342.md) [kritēs](../../strongs/g/g2923.md), shall [apodidōmi](../../strongs/g/g591.md) me at that [hēmera](../../strongs/g/g2250.md): and not to me only, but unto all them also that [agapaō](../../strongs/g/g25.md) his [epiphaneia](../../strongs/g/g2015.md).

<a name="2timothy_4_9"></a>2 Timothy 4:9

[spoudazō](../../strongs/g/g4704.md) thy to [erchomai](../../strongs/g/g2064.md) [tacheōs](../../strongs/g/g5030.md) unto me:

<a name="2timothy_4_10"></a>2 Timothy 4:10

For [dēmas](../../strongs/g/g1214.md) hath [egkataleipō](../../strongs/g/g1459.md) me, having [agapaō](../../strongs/g/g25.md) this [nyn](../../strongs/g/g3568.md) [aiōn](../../strongs/g/g165.md), and is [poreuō](../../strongs/g/g4198.md) unto [Thessalonikē](../../strongs/g/g2332.md); [krēskēs](../../strongs/g/g2913.md) to [galatia](../../strongs/g/g1053.md), [titos](../../strongs/g/g5103.md) unto [dalmatia](../../strongs/g/g1149.md).

<a name="2timothy_4_11"></a>2 Timothy 4:11

Only [loukas](../../strongs/g/g3065.md) is with me. [analambanō](../../strongs/g/g353.md) [Markos](../../strongs/g/g3138.md), and [agō](../../strongs/g/g71.md) him with thee: for he is [euchrēstos](../../strongs/g/g2173.md) to me for the [diakonia](../../strongs/g/g1248.md).

<a name="2timothy_4_12"></a>2 Timothy 4:12

And [Tychikos](../../strongs/g/g5190.md) have I [apostellō](../../strongs/g/g649.md) to [Ephesos](../../strongs/g/g2181.md).

<a name="2timothy_4_13"></a>2 Timothy 4:13

The [phailonēs](../../strongs/g/g5341.md) that I [apoleipō](../../strongs/g/g620.md) at [Trōas](../../strongs/g/g5174.md) with [karpos](../../strongs/g/g2591.md), when thou [erchomai](../../strongs/g/g2064.md), [pherō](../../strongs/g/g5342.md) with thee, and the [biblion](../../strongs/g/g975.md), but [malista](../../strongs/g/g3122.md) the [membrana](../../strongs/g/g3200.md).

<a name="2timothy_4_14"></a>2 Timothy 4:14

[Alexandros](../../strongs/g/g223.md) the [chalkeus](../../strongs/g/g5471.md) [endeiknymi](../../strongs/g/g1731.md) me [polys](../../strongs/g/g4183.md) [kakos](../../strongs/g/g2556.md): the [kyrios](../../strongs/g/g2962.md) [apodidōmi](../../strongs/g/g591.md) him according to his [ergon](../../strongs/g/g2041.md):

<a name="2timothy_4_15"></a>2 Timothy 4:15

Of whom thou [phylassō](../../strongs/g/g5442.md) also; for he hath [lian](../../strongs/g/g3029.md) [anthistēmi](../../strongs/g/g436.md) our [logos](../../strongs/g/g3056.md).

<a name="2timothy_4_16"></a>2 Timothy 4:16

At my first [apologia](../../strongs/g/g627.md) no man [symparaginomai](../../strongs/g/g4836.md) me, but all [egkataleipō](../../strongs/g/g1459.md) me: that it [logizomai](../../strongs/g/g3049.md) not to their charge.

<a name="2timothy_4_17"></a>2 Timothy 4:17

[de](../../strongs/g/g1161.md) the [kyrios](../../strongs/g/g2962.md) [paristēmi](../../strongs/g/g3936.md) with me, and [endynamoō](../../strongs/g/g1743.md) me; that by me the [kērygma](../../strongs/g/g2782.md) might be [plērophoreō](../../strongs/g/g4135.md), and that all the [ethnos](../../strongs/g/g1484.md) might [akouō](../../strongs/g/g191.md): and I was [rhyomai](../../strongs/g/g4506.md) out of the [stoma](../../strongs/g/g4750.md) of the [leōn](../../strongs/g/g3023.md).

<a name="2timothy_4_18"></a>2 Timothy 4:18

And the [kyrios](../../strongs/g/g2962.md) shall [rhyomai](../../strongs/g/g4506.md) me from every [ponēros](../../strongs/g/g4190.md) [ergon](../../strongs/g/g2041.md), and will [sōzō](../../strongs/g/g4982.md) me unto his [epouranios](../../strongs/g/g2032.md) [basileia](../../strongs/g/g932.md): to whom be [doxa](../../strongs/g/g1391.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="2timothy_4_19"></a>2 Timothy 4:19

[aspazomai](../../strongs/g/g782.md) [priska](../../strongs/g/g4251.md) and [Akylas](../../strongs/g/g207.md), and the [oikos](../../strongs/g/g3624.md) of [onēsiphoros](../../strongs/g/g3683.md).

<a name="2timothy_4_20"></a>2 Timothy 4:20

[Erastos](../../strongs/g/g2037.md) [menō](../../strongs/g/g3306.md) at [Korinthos](../../strongs/g/g2882.md): but [Trophimos](../../strongs/g/g5161.md) have I [apoleipō](../../strongs/g/g620.md) at [Milētos](../../strongs/g/g3399.md) [astheneō](../../strongs/g/g770.md).

<a name="2timothy_4_21"></a>2 Timothy 4:21

[spoudazō](../../strongs/g/g4704.md) to [erchomai](../../strongs/g/g2064.md) before [cheimōn](../../strongs/g/g5494.md). [euboulos](../../strongs/g/g2103.md) [aspazomai](../../strongs/g/g782.md) thee, and [poudēs](../../strongs/g/g4227.md), and [linos](../../strongs/g/g3044.md), and [klaudia](../../strongs/g/g2803.md), and all the [adelphos](../../strongs/g/g80.md).

<a name="2timothy_4_22"></a>2 Timothy 4:22

The [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with thy [pneuma](../../strongs/g/g4151.md). [charis](../../strongs/g/g5485.md) be with you. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[2 Timothy](2timothy.md)

[2 Timothy 3](2timothy_3.md)