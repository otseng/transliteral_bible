# [2 Timothy 1](https://www.blueletterbible.org/kjv/2ti/1/1/s_1126001)

<a name="2timothy_1_1"></a>2 Timothy 1:1

[Paulos](../../strongs/g/g3972.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), according to the [epaggelia](../../strongs/g/g1860.md) of [zōē](../../strongs/g/g2222.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md),

<a name="2timothy_1_2"></a>2 Timothy 1:2

To [Timotheos](../../strongs/g/g5095.md), [agapētos](../../strongs/g/g27.md) [teknon](../../strongs/g/g5043.md): [charis](../../strongs/g/g5485.md), [eleos](../../strongs/g/g1656.md), [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) and [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md).

<a name="2timothy_1_3"></a>2 Timothy 1:3

I [charis](../../strongs/g/g5485.md) [theos](../../strongs/g/g2316.md), whom I [latreuō](../../strongs/g/g3000.md) from [progonos](../../strongs/g/g4269.md) with [katharos](../../strongs/g/g2513.md) [syneidēsis](../../strongs/g/g4893.md), that [adialeiptos](../../strongs/g/g88.md) I have [mneia](../../strongs/g/g3417.md) of thee in my [deēsis](../../strongs/g/g1162.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md);

<a name="2timothy_1_4"></a>2 Timothy 1:4

[epipotheo](../../strongs/g/g1971.md) to [eidō](../../strongs/g/g1492.md) thee, [mnaomai](../../strongs/g/g3415.md) of thy [dakry](../../strongs/g/g1144.md), that I may be [plēroō](../../strongs/g/g4137.md) with [chara](../../strongs/g/g5479.md);

<a name="2timothy_1_5"></a>2 Timothy 1:5

When I [lambanō](../../strongs/g/g2983.md) to [hypomnēsis](../../strongs/g/g5280.md) the [anypokritos](../../strongs/g/g505.md) [pistis](../../strongs/g/g4102.md) that is in thee, which [enoikeō](../../strongs/g/g1774.md) first in thy [mammē](../../strongs/g/g3125.md) [lōis](../../strongs/g/g3090.md), and thy [mētēr](../../strongs/g/g3384.md) [eunikē](../../strongs/g/g2131.md); and I am [peithō](../../strongs/g/g3982.md) that in thee also.

<a name="2timothy_1_6"></a>2 Timothy 1:6

Wherefore I [anamimnēskō](../../strongs/g/g363.md) thee that thou [anazōpyreō](../../strongs/g/g329.md) the [charisma](../../strongs/g/g5486.md) of [theos](../../strongs/g/g2316.md), which is in thee by the [epithesis](../../strongs/g/g1936.md) of my [cheir](../../strongs/g/g5495.md).

<a name="2timothy_1_7"></a>2 Timothy 1:7

For [theos](../../strongs/g/g2316.md) hath not [didōmi](../../strongs/g/g1325.md) us the [pneuma](../../strongs/g/g4151.md) of [deilia](../../strongs/g/g1167.md); but of [dynamis](../../strongs/g/g1411.md), and of [agapē](../../strongs/g/g26.md), and of [sōphronismos](../../strongs/g/g4995.md).

<a name="2timothy_1_8"></a>2 Timothy 1:8

Be not thou therefore [epaischynomai](../../strongs/g/g1870.md) of the [martyrion](../../strongs/g/g3142.md) of our [kyrios](../../strongs/g/g2962.md), nor of me his [desmios](../../strongs/g/g1198.md): but be thou [sygkakopatheō](../../strongs/g/g4777.md) of the [euaggelion](../../strongs/g/g2098.md) according to the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md);

<a name="2timothy_1_9"></a>2 Timothy 1:9

Who hath [sōzō](../../strongs/g/g4982.md) us, and [kaleō](../../strongs/g/g2564.md) with an [hagios](../../strongs/g/g40.md) [klēsis](../../strongs/g/g2821.md), not according to our [ergon](../../strongs/g/g2041.md), but according to his own [prothesis](../../strongs/g/g4286.md) and [charis](../../strongs/g/g5485.md), which was [didōmi](../../strongs/g/g1325.md) us in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) before the [chronos](../../strongs/g/g5550.md) [aiōnios](../../strongs/g/g166.md),

<a name="2timothy_1_10"></a>2 Timothy 1:10

But is now [phaneroō](../../strongs/g/g5319.md) by the [epiphaneia](../../strongs/g/g2015.md) of our [sōtēr](../../strongs/g/g4990.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), who hath [katargeō](../../strongs/g/g2673.md) [thanatos](../../strongs/g/g2288.md), and hath brought [zōē](../../strongs/g/g2222.md) and [aphtharsia](../../strongs/g/g861.md) to [phōtizō](../../strongs/g/g5461.md) through the [euaggelion](../../strongs/g/g2098.md):

<a name="2timothy_1_11"></a>2 Timothy 1:11

Whereunto I am [tithēmi](../../strongs/g/g5087.md) a [kēryx](../../strongs/g/g2783.md), and an [apostolos](../../strongs/g/g652.md), and a [didaskalos](../../strongs/g/g1320.md) of the [ethnos](../../strongs/g/g1484.md).

<a name="2timothy_1_12"></a>2 Timothy 1:12

For the which [aitia](../../strongs/g/g156.md) I also [paschō](../../strongs/g/g3958.md) these things: nevertheless I am not [epaischynomai](../../strongs/g/g1870.md): for I [eidō](../../strongs/g/g1492.md) whom I have [pisteuō](../../strongs/g/g4100.md), and am [peithō](../../strongs/g/g3982.md) that he is [dynatos](../../strongs/g/g1415.md) to [phylassō](../../strongs/g/g5442.md) that which I have [parathēkē](../../strongs/g/g3866.md) unto him against that [hēmera](../../strongs/g/g2250.md).

<a name="2timothy_1_13"></a>2 Timothy 1:13

[echō](../../strongs/g/g2192.md) the [hypotypōsis](../../strongs/g/g5296.md) of [hygiainō](../../strongs/g/g5198.md) [logos](../../strongs/g/g3056.md), which thou hast [akouō](../../strongs/g/g191.md) of me, in [pistis](../../strongs/g/g4102.md) and [agapē](../../strongs/g/g26.md) which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="2timothy_1_14"></a>2 Timothy 1:14

That [kalos](../../strongs/g/g2570.md) thing which was [parakatathēkē](../../strongs/g/g3872.md) unto thee [phylassō](../../strongs/g/g5442.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) which [enoikeō](../../strongs/g/g1774.md) in us.

<a name="2timothy_1_15"></a>2 Timothy 1:15

This thou [eidō](../../strongs/g/g1492.md), that all they which are in [Asia](../../strongs/g/g773.md) be [apostrephō](../../strongs/g/g654.md) from me; of whom are [phygellos](../../strongs/g/g5436.md) and [hermogenēs](../../strongs/g/g2061.md).

<a name="2timothy_1_16"></a>2 Timothy 1:16

The [kyrios](../../strongs/g/g2962.md) [didōmi](../../strongs/g/g1325.md) [eleos](../../strongs/g/g1656.md) unto the [oikos](../../strongs/g/g3624.md) of [onēsiphoros](../../strongs/g/g3683.md); for he oft [anapsychō](../../strongs/g/g404.md) me, and was not [epaischynomai](../../strongs/g/g1870.md) of my [halysis](../../strongs/g/g254.md):

<a name="2timothy_1_17"></a>2 Timothy 1:17

But, when he was in [Rhōmē](../../strongs/g/g4516.md), he [zēteō](../../strongs/g/g2212.md) me out [spoudaioteron](../../strongs/g/g4706.md), and [heuriskō](../../strongs/g/g2147.md) me.

<a name="2timothy_1_18"></a>2 Timothy 1:18

The [kyrios](../../strongs/g/g2962.md) [didōmi](../../strongs/g/g1325.md) unto him that he may [heuriskō](../../strongs/g/g2147.md) [eleos](../../strongs/g/g1656.md) of the [kyrios](../../strongs/g/g2962.md) in that [hēmera](../../strongs/g/g2250.md): and in how many things he [diakoneō](../../strongs/g/g1247.md) me at [Ephesos](../../strongs/g/g2181.md), thou [ginōskō](../../strongs/g/g1097.md) [beltiōn](../../strongs/g/g957.md).

---

[Transliteral Bible](../bible.md)

[2 Timothy](2timothy.md)

[2 Timothy 2](2timothy_2.md)