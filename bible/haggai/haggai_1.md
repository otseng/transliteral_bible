# [Haggai 1](https://www.blueletterbible.org/kjv/haggai/1)

<a name="haggai_1_1"></a>Haggai 1:1

In the second [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md) the [melek](../../strongs/h/h4428.md), in the sixth [ḥōḏeš](../../strongs/h/h2320.md), in the first [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md), came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) [Ḥagay](../../strongs/h/h2292.md) the [nāḇî'](../../strongs/h/h5030.md) unto [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), [peḥâ](../../strongs/h/h6346.md) of [Yehuwdah](../../strongs/h/h3063.md), and to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_1_2"></a>Haggai 1:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['āmar](../../strongs/h/h559.md), This ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), The [ʿēṯ](../../strongs/h/h6256.md) is not [bow'](../../strongs/h/h935.md), the [ʿēṯ](../../strongs/h/h6256.md) that [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) should be [bānâ](../../strongs/h/h1129.md).

<a name="haggai_1_3"></a>Haggai 1:3

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) [Ḥagay](../../strongs/h/h2292.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_1_4"></a>Haggai 1:4

Is it [ʿēṯ](../../strongs/h/h6256.md) for you, O ye, to [yashab](../../strongs/h/h3427.md) in your [sāp̄an](../../strongs/h/h5603.md) [bayith](../../strongs/h/h1004.md), and this [bayith](../../strongs/h/h1004.md) lie [ḥārēḇ](../../strongs/h/h2720.md)?

<a name="haggai_1_5"></a>Haggai 1:5

Now therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [śûm](../../strongs/h/h7760.md) [lebab](../../strongs/h/h3824.md) your [derek](../../strongs/h/h1870.md).

<a name="haggai_1_6"></a>Haggai 1:6

Ye have [zāraʿ](../../strongs/h/h2232.md) [rabah](../../strongs/h/h7235.md), and [bow'](../../strongs/h/h935.md) in [mᵊʿaṭ](../../strongs/h/h4592.md); ye ['akal](../../strongs/h/h398.md), but ye have not [śāḇʿâ](../../strongs/h/h7654.md); ye [šāṯâ](../../strongs/h/h8354.md), but ye are not [šāḵar](../../strongs/h/h7937.md); ye [labash](../../strongs/h/h3847.md) you, but there is none [ḥōm](../../strongs/h/h2527.md); and he that earneth [śāḵar](../../strongs/h/h7936.md) earneth [śāḵar](../../strongs/h/h7936.md) to put it into a [ṣᵊrôr](../../strongs/h/h6872.md) with [nāqaḇ](../../strongs/h/h5344.md).

<a name="haggai_1_7"></a>Haggai 1:7

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [śûm](../../strongs/h/h7760.md) [lebab](../../strongs/h/h3824.md) your [derek](../../strongs/h/h1870.md).

<a name="haggai_1_8"></a>Haggai 1:8

[ʿālâ](../../strongs/h/h5927.md) to the [har](../../strongs/h/h2022.md), and [bow'](../../strongs/h/h935.md) ['ets](../../strongs/h/h6086.md), and [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md); and I will [ratsah](../../strongs/h/h7521.md) in it, and I will be [kabad](../../strongs/h/h3513.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="haggai_1_9"></a>Haggai 1:9

Ye [panah](../../strongs/h/h6437.md) for [rabah](../../strongs/h/h7235.md), and, lo, it came to [mᵊʿaṭ](../../strongs/h/h4592.md); and when ye [bow'](../../strongs/h/h935.md) it [bayith](../../strongs/h/h1004.md), I did [nāp̄aḥ](../../strongs/h/h5301.md) upon it. Why? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md). Because of mine [bayith](../../strongs/h/h1004.md) that is [ḥārēḇ](../../strongs/h/h2720.md), and ye [rûṣ](../../strongs/h/h7323.md) every ['iysh](../../strongs/h/h376.md) unto his own [bayith](../../strongs/h/h1004.md).

<a name="haggai_1_10"></a>Haggai 1:10

Therefore the [shamayim](../../strongs/h/h8064.md) over you is [kālā'](../../strongs/h/h3607.md) from [ṭal](../../strongs/h/h2919.md), and the ['erets](../../strongs/h/h776.md) is [kālā'](../../strongs/h/h3607.md) from her [yᵊḇûl](../../strongs/h/h2981.md).

<a name="haggai_1_11"></a>Haggai 1:11

And I [qara'](../../strongs/h/h7121.md) for a [ḥōreḇ](../../strongs/h/h2721.md) upon the ['erets](../../strongs/h/h776.md), and upon the [har](../../strongs/h/h2022.md), and upon the [dagan](../../strongs/h/h1715.md), and upon the [tiyrowsh](../../strongs/h/h8492.md), and upon the [yiṣhār](../../strongs/h/h3323.md), and upon that which the ['ăḏāmâ](../../strongs/h/h127.md) [yāṣā'](../../strongs/h/h3318.md), and upon ['āḏām](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md), and upon all the [yᵊḡîaʿ](../../strongs/h/h3018.md) of the [kaph](../../strongs/h/h3709.md).

<a name="haggai_1_12"></a>Haggai 1:12

Then [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), and [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), with all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md), [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and the [dabar](../../strongs/h/h1697.md) of [Ḥagay](../../strongs/h/h2292.md) the [nāḇî'](../../strongs/h/h5030.md), as [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) had [shalach](../../strongs/h/h7971.md) him, and the ['am](../../strongs/h/h5971.md) did [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="haggai_1_13"></a>Haggai 1:13

Then ['āmar](../../strongs/h/h559.md) [Ḥagay](../../strongs/h/h2292.md) [Yĕhovah](../../strongs/h/h3068.md) [mal'ak](../../strongs/h/h4397.md) in [Yĕhovah](../../strongs/h/h3068.md) [mal'āḵûṯ](../../strongs/h/h4400.md) unto the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), I am with you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="haggai_1_14"></a>Haggai 1:14

And [Yĕhovah](../../strongs/h/h3068.md) stirred [ʿûr](../../strongs/h/h5782.md) the [ruwach](../../strongs/h/h7307.md) of [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), [peḥâ](../../strongs/h/h6346.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [ruwach](../../strongs/h/h7307.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), and the [ruwach](../../strongs/h/h7307.md) of all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md); and they [bow'](../../strongs/h/h935.md) and ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), their ['Elohiym](../../strongs/h/h430.md),

<a name="haggai_1_15"></a>Haggai 1:15

In the four and twentieth [yowm](../../strongs/h/h3117.md) of the sixth [ḥōḏeš](../../strongs/h/h2320.md), in the second [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md) the [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[Haggai](haggai.md)

[Haggai 2](haggai_2.md)