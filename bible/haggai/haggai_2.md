# [Haggai 2](https://www.blueletterbible.org/kjv/haggai/2)

<a name="haggai_2_1"></a>Haggai 2:1

In the seventh month, in the one and twentieth day of the [ḥōḏeš](../../strongs/h/h2320.md), came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) the [nāḇî'](../../strongs/h/h5030.md) [Ḥagay](../../strongs/h/h2292.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_2_2"></a>Haggai 2:2

['āmar](../../strongs/h/h559.md) now to [Zᵊrubāḇel](../../strongs/h/h2216.md) the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), [peḥâ](../../strongs/h/h6346.md) of [Yehuwdah](../../strongs/h/h3063.md), and to [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), and to the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_2_3"></a>Haggai 2:3

Who is [šā'ar](../../strongs/h/h7604.md) among you that [ra'ah](../../strongs/h/h7200.md) this [bayith](../../strongs/h/h1004.md) in her [ri'šôn](../../strongs/h/h7223.md) [kabowd](../../strongs/h/h3519.md)? and how do ye [ra'ah](../../strongs/h/h7200.md) it now? is it not in your ['ayin](../../strongs/h/h5869.md) in [kᵊmô](../../strongs/h/h3644.md)?

<a name="haggai_2_4"></a>Haggai 2:4

Yet now be [ḥāzaq](../../strongs/h/h2388.md), O [Zᵊrubāḇel](../../strongs/h/h2216.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and be [ḥāzaq](../../strongs/h/h2388.md), O [Yᵊhôšûaʿ](../../strongs/h/h3091.md), [ben](../../strongs/h/h1121.md) of [Yᵊhôṣāḏāq](../../strongs/h/h3087.md), the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md); and be [ḥāzaq](../../strongs/h/h2388.md), all ye ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md): for I am with you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md):

<a name="haggai_2_5"></a>Haggai 2:5

According to the [dabar](../../strongs/h/h1697.md) that I [karath](../../strongs/h/h3772.md) with you when ye [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md), so my [ruwach](../../strongs/h/h7307.md) ['amad](../../strongs/h/h5975.md) [tavek](../../strongs/h/h8432.md) you: [yare'](../../strongs/h/h3372.md) ye not.

<a name="haggai_2_6"></a>Haggai 2:6

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Yet once, it is a [mᵊʿaṭ](../../strongs/h/h4592.md), and I will [rāʿaš](../../strongs/h/h7493.md) the [shamayim](../../strongs/h/h8064.md), and the ['erets](../../strongs/h/h776.md), and the [yam](../../strongs/h/h3220.md), and the [ḥārāḇâ](../../strongs/h/h2724.md) land;

<a name="haggai_2_7"></a>Haggai 2:7

And I will [rāʿaš](../../strongs/h/h7493.md) all [gowy](../../strongs/h/h1471.md), and the [ḥemdâ](../../strongs/h/h2532.md) of all [gowy](../../strongs/h/h1471.md) shall [bow'](../../strongs/h/h935.md): and I will [mālā'](../../strongs/h/h4390.md) this [bayith](../../strongs/h/h1004.md) with [kabowd](../../strongs/h/h3519.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="haggai_2_8"></a>Haggai 2:8

The [keceph](../../strongs/h/h3701.md) is mine, and the [zāhāḇ](../../strongs/h/h2091.md) is mine, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="haggai_2_9"></a>Haggai 2:9

The [kabowd](../../strongs/h/h3519.md) of this ['aḥărôn](../../strongs/h/h314.md) [bayith](../../strongs/h/h1004.md) shall be [gadowl](../../strongs/h/h1419.md) of the [ri'šôn](../../strongs/h/h7223.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): and in this [maqowm](../../strongs/h/h4725.md) will I [nathan](../../strongs/h/h5414.md) [shalowm](../../strongs/h/h7965.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="haggai_2_10"></a>Haggai 2:10

In the four and twentieth day of the ninth month, in the second [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md), came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) [Ḥagay](../../strongs/h/h2292.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_2_11"></a>Haggai 2:11

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [sha'al](../../strongs/h/h7592.md) now the [kōhēn](../../strongs/h/h3548.md) concerning the [towrah](../../strongs/h/h8451.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_2_12"></a>Haggai 2:12

If ['iysh](../../strongs/h/h376.md) [nasa'](../../strongs/h/h5375.md) [qodesh](../../strongs/h/h6944.md) [basar](../../strongs/h/h1320.md) in the [kanaph](../../strongs/h/h3671.md) of his [beḡeḏ](../../strongs/h/h899.md), and with his [kanaph](../../strongs/h/h3671.md) do [naga'](../../strongs/h/h5060.md) [lechem](../../strongs/h/h3899.md), or [nāzîḏ](../../strongs/h/h5138.md), or [yayin](../../strongs/h/h3196.md), or [šemen](../../strongs/h/h8081.md), or any [ma'akal](../../strongs/h/h3978.md), shall it be [qadash](../../strongs/h/h6942.md)? And the [kōhēn](../../strongs/h/h3548.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), No.

<a name="haggai_2_13"></a>Haggai 2:13

Then ['āmar](../../strongs/h/h559.md) [Ḥagay](../../strongs/h/h2292.md), If one that is [tame'](../../strongs/h/h2931.md) by a dead [nephesh](../../strongs/h/h5315.md) [naga'](../../strongs/h/h5060.md) any of these, shall it be [ṭāmē'](../../strongs/h/h2930.md)? And the [kōhēn](../../strongs/h/h3548.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), It shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="haggai_2_14"></a>Haggai 2:14

Then ['anah](../../strongs/h/h6030.md) [Ḥagay](../../strongs/h/h2292.md), and ['āmar](../../strongs/h/h559.md), So is this ['am](../../strongs/h/h5971.md), and so is this [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and so is every [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md); and that which they [qāraḇ](../../strongs/h/h7126.md) there is [tame'](../../strongs/h/h2931.md).

<a name="haggai_2_15"></a>Haggai 2:15

And now, I pray you, [śûm](../../strongs/h/h7760.md) [lebab](../../strongs/h/h3824.md) from this [yowm](../../strongs/h/h3117.md) and [maʿal](../../strongs/h/h4605.md), from before an ['eben](../../strongs/h/h68.md) was [śûm](../../strongs/h/h7760.md) upon an ['eben](../../strongs/h/h68.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="haggai_2_16"></a>Haggai 2:16

Since those days were, when one [bow'](../../strongs/h/h935.md) to an [ʿărēmâ](../../strongs/h/h6194.md) of twenty measures, there were but ten: when one [bow'](../../strongs/h/h935.md) to the [yeqeḇ](../../strongs/h/h3342.md) for to draw [ḥāśap̄](../../strongs/h/h2834.md) fifty out of the [pûrâ](../../strongs/h/h6333.md), there were but twenty.

<a name="haggai_2_17"></a>Haggai 2:17

I [nakah](../../strongs/h/h5221.md) you with [šᵊḏēp̄â](../../strongs/h/h7711.md) and with [yērāqôn](../../strongs/h/h3420.md) and with [barad](../../strongs/h/h1259.md) in all the [ma'aseh](../../strongs/h/h4639.md) of your [yad](../../strongs/h/h3027.md); yet ye not to me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="haggai_2_18"></a>Haggai 2:18

[śûm](../../strongs/h/h7760.md) [lebab](../../strongs/h/h3824.md) now from this [yowm](../../strongs/h/h3117.md) and [maʿal](../../strongs/h/h4605.md), from the four and twentieth [yowm](../../strongs/h/h3117.md) of the ninth month, even from the [yowm](../../strongs/h/h3117.md) that the [yacad](../../strongs/h/h3245.md) of [Yĕhovah](../../strongs/h/h3068.md) [heykal](../../strongs/h/h1964.md), [śûm](../../strongs/h/h7760.md) [lebab](../../strongs/h/h3824.md) it.

<a name="haggai_2_19"></a>Haggai 2:19

Is the [zera'](../../strongs/h/h2233.md) yet in the [mᵊḡûrâ](../../strongs/h/h4035.md)? yea, as yet the [gep̄en](../../strongs/h/h1612.md), and the [tĕ'en](../../strongs/h/h8384.md), and the [rimmôn](../../strongs/h/h7416.md), and the [zayiṯ](../../strongs/h/h2132.md) ['ets](../../strongs/h/h6086.md), hath not [nasa'](../../strongs/h/h5375.md): from this [yowm](../../strongs/h/h3117.md) will I [barak](../../strongs/h/h1288.md) you.

<a name="haggai_2_20"></a>Haggai 2:20

And again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Ḥagay](../../strongs/h/h2292.md) in the four and twentieth day of the [ḥōḏeš](../../strongs/h/h2320.md), ['āmar](../../strongs/h/h559.md),

<a name="haggai_2_21"></a>Haggai 2:21

['āmar](../../strongs/h/h559.md) to [Zᵊrubāḇel](../../strongs/h/h2216.md), [peḥâ](../../strongs/h/h6346.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), I will [rāʿaš](../../strongs/h/h7493.md) the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md);

<a name="haggai_2_22"></a>Haggai 2:22

And I will [hāp̄aḵ](../../strongs/h/h2015.md) the [kicce'](../../strongs/h/h3678.md) of [mamlāḵâ](../../strongs/h/h4467.md), and I will [šāmaḏ](../../strongs/h/h8045.md) the [ḥōzeq](../../strongs/h/h2392.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of the [gowy](../../strongs/h/h1471.md); and I will [hāp̄aḵ](../../strongs/h/h2015.md) the [merkāḇâ](../../strongs/h/h4818.md), and those that [rāḵaḇ](../../strongs/h/h7392.md) in them; and the [sûs](../../strongs/h/h5483.md) and their [rāḵaḇ](../../strongs/h/h7392.md) shall [yarad](../../strongs/h/h3381.md), every ['iysh](../../strongs/h/h376.md) by the [chereb](../../strongs/h/h2719.md) of his ['ach](../../strongs/h/h251.md).

<a name="haggai_2_23"></a>Haggai 2:23

In that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), will I [laqach](../../strongs/h/h3947.md) thee, O [Zᵊrubāḇel](../../strongs/h/h2216.md), my ['ebed](../../strongs/h/h5650.md), the [ben](../../strongs/h/h1121.md) of [Šᵊ'Altî'Ēl](../../strongs/h/h7597.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and will [śûm](../../strongs/h/h7760.md) thee as a [ḥôṯām](../../strongs/h/h2368.md): for I have [bāḥar](../../strongs/h/h977.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

---

[Transliteral Bible](../bible.md)

[Haggai](haggai.md)

[Haggai 1](haggai_1.md)