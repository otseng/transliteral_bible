# [2 Corinthians 3](https://www.blueletterbible.org/kjv/2co/3/1/s_1081001)

<a name="2corinthians_3_1"></a>2 Corinthians 3:1

Do we [archomai](../../strongs/g/g756.md) again to [synistēmi](../../strongs/g/g4921.md) ourselves? or [chrēzō](../../strongs/g/g5535.md) we, as some, [epistolē](../../strongs/g/g1992.md) of [systatikos](../../strongs/g/g4956.md) to you,  of [systatikos](../../strongs/g/g4956.md) from you?

<a name="2corinthians_3_2"></a>2 Corinthians 3:2

Ye are our [epistolē](../../strongs/g/g1992.md) [engraphō](../../strongs/g/g1449.md) in our [kardia](../../strongs/g/g2588.md), [ginōskō](../../strongs/g/g1097.md) and [anaginōskō](../../strongs/g/g314.md) of all [anthrōpos](../../strongs/g/g444.md):

<a name="2corinthians_3_3"></a>2 Corinthians 3:3

[phaneroō](../../strongs/g/g5319.md) to be the [epistolē](../../strongs/g/g1992.md) of [Christos](../../strongs/g/g5547.md) [diakoneō](../../strongs/g/g1247.md) by us, [engraphō](../../strongs/g/g1449.md) not with [melan](../../strongs/g/g3188.md), but with the [pneuma](../../strongs/g/g4151.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md); not in [plax](../../strongs/g/g4109.md) of [lithinos](../../strongs/g/g3035.md), but in [sarkinos](../../strongs/g/g4560.md) [plax](../../strongs/g/g4109.md) of the [kardia](../../strongs/g/g2588.md).

<a name="2corinthians_3_4"></a>2 Corinthians 3:4

And such [pepoithēsis](../../strongs/g/g4006.md) have we through [Christos](../../strongs/g/g5547.md) to [theos](../../strongs/g/g2316.md):

<a name="2corinthians_3_5"></a>2 Corinthians 3:5

Not that we are [hikanos](../../strongs/g/g2425.md) of ourselves to [logizomai](../../strongs/g/g3049.md) any thing as of ourselves; but our [hikanotēs](../../strongs/g/g2426.md) is of [theos](../../strongs/g/g2316.md);

<a name="2corinthians_3_6"></a>2 Corinthians 3:6

Who also hath [hikanoō](../../strongs/g/g2427.md) us [diakonos](../../strongs/g/g1249.md) of the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md); not of the [gramma](../../strongs/g/g1121.md), but of the [pneuma](../../strongs/g/g4151.md): for the [gramma](../../strongs/g/g1121.md) [apokteinō](../../strongs/g/g615.md), but the [pneuma](../../strongs/g/g4151.md) [zōopoieō](../../strongs/g/g2227.md).

<a name="2corinthians_3_7"></a>2 Corinthians 3:7

But if the [diakonia](../../strongs/g/g1248.md) of [thanatos](../../strongs/g/g2288.md), [gramma](../../strongs/g/g1121.md) and [entypoō](../../strongs/g/g1795.md) in [lithos](../../strongs/g/g3037.md), was [en](../../strongs/g/g1722.md) [doxa](../../strongs/g/g1391.md), so that the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md) could not [atenizō](../../strongs/g/g816.md) the face of [Mōÿsēs](../../strongs/g/g3475.md) for the [doxa](../../strongs/g/g1391.md) of his [prosōpon](../../strongs/g/g4383.md); which [katargeō](../../strongs/g/g2673.md):

<a name="2corinthians_3_8"></a>2 Corinthians 3:8

How shall not the [diakonia](../../strongs/g/g1248.md) of the [pneuma](../../strongs/g/g4151.md) be rather [doxa](../../strongs/g/g1391.md)?

<a name="2corinthians_3_9"></a>2 Corinthians 3:9

For if the [diakonia](../../strongs/g/g1248.md) of[katakrisis](../../strongs/g/g2633.md) [doxa](../../strongs/g/g1391.md), [polys](../../strongs/g/g4183.md) more doth the [diakonia](../../strongs/g/g1248.md) of [dikaiosynē](../../strongs/g/g1343.md) [perisseuō](../../strongs/g/g4052.md) in [doxa](../../strongs/g/g1391.md).

<a name="2corinthians_3_10"></a>2 Corinthians 3:10

For even that which was [doxazō](../../strongs/g/g1392.md) had no [doxazō](../../strongs/g/g1392.md) in this [meros](../../strongs/g/g3313.md), [heneka](../../strongs/g/g1752.md) the [doxa](../../strongs/g/g1391.md) that [hyperballō](../../strongs/g/g5235.md).

<a name="2corinthians_3_11"></a>2 Corinthians 3:11

For if that which is [katargeō](../../strongs/g/g2673.md) was [dia](../../strongs/g/g1223.md) [doxa](../../strongs/g/g1391.md), [polys](../../strongs/g/g4183.md) more that which [menō](../../strongs/g/g3306.md) is [en](../../strongs/g/g1722.md) [doxa](../../strongs/g/g1391.md).

<a name="2corinthians_3_12"></a>2 Corinthians 3:12

Seeing then that we have such [elpis](../../strongs/g/g1680.md), we [chraomai](../../strongs/g/g5530.md) [polys](../../strongs/g/g4183.md) [parrēsia](../../strongs/g/g3954.md):

<a name="2corinthians_3_13"></a>2 Corinthians 3:13

And not as [Mōÿsēs](../../strongs/g/g3475.md), which [tithēmi](../../strongs/g/g5087.md) a [kalymma](../../strongs/g/g2571.md) over his [prosōpon](../../strongs/g/g4383.md), that the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md) could not [atenizō](../../strongs/g/g816.md) to the [telos](../../strongs/g/g5056.md) of [katargeō](../../strongs/g/g2673.md):

<a name="2corinthians_3_14"></a>2 Corinthians 3:14

But their [noēma](../../strongs/g/g3540.md) were [pōroō](../../strongs/g/g4456.md): for until [sēmeron](../../strongs/g/g4594.md) [menō](../../strongs/g/g3306.md) the same [kalymma](../../strongs/g/g2571.md) [mē](../../strongs/g/g3361.md) [anakalyptō](../../strongs/g/g343.md) in the [anagnōsis](../../strongs/g/g320.md) of the [palaios](../../strongs/g/g3820.md) [diathēkē](../../strongs/g/g1242.md); which is [katargeō](../../strongs/g/g2673.md) in [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_3_15"></a>2 Corinthians 3:15

But even unto [sēmeron](../../strongs/g/g4594.md), when [Mōÿsēs](../../strongs/g/g3475.md) is [anaginōskō](../../strongs/g/g314.md), the [kalymma](../../strongs/g/g2571.md) is [keimai](../../strongs/g/g2749.md) their [kardia](../../strongs/g/g2588.md).

<a name="2corinthians_3_16"></a>2 Corinthians 3:16

Nevertheless when it shall [epistrephō](../../strongs/g/g1994.md) to the [kyrios](../../strongs/g/g2962.md), the [kalymma](../../strongs/g/g2571.md) shall be [periaireō](../../strongs/g/g4014.md).

<a name="2corinthians_3_17"></a>2 Corinthians 3:17

Now the [kyrios](../../strongs/g/g2962.md) is that [pneuma](../../strongs/g/g4151.md): and where the [pneuma](../../strongs/g/g4151.md) of the [kyrios](../../strongs/g/g2962.md) is, there is [eleutheria](../../strongs/g/g1657.md).

<a name="2corinthians_3_18"></a>2 Corinthians 3:18

But we all, with [anakalyptō](../../strongs/g/g343.md) [prosōpon](../../strongs/g/g4383.md) [katoptrizō](../../strongs/g/g2734.md) the [doxa](../../strongs/g/g1391.md) of the [kyrios](../../strongs/g/g2962.md), are [metamorphoō](../../strongs/g/g3339.md) into the same [eikōn](../../strongs/g/g1504.md) from [doxa](../../strongs/g/g1391.md) to [doxa](../../strongs/g/g1391.md), even as by the [pneuma](../../strongs/g/g4151.md) of the [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[2Corinthians](2corinthians.md)

[2Corinthians 2](2corinthians_2.md) - [2Corinthians 4](2corinthians_4.md)