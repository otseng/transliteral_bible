# [2 Corinthians 8](https://www.blueletterbible.org/kjv/2co/8/1/s_1086001)

<a name="2corinthians_8_1"></a>2 Corinthians 8:1

Moreover, [adelphos](../../strongs/g/g80.md), we do you to [gnōrizō](../../strongs/g/g1107.md) of the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) on the [ekklēsia](../../strongs/g/g1577.md) of [Makedonia](../../strongs/g/g3109.md);

<a name="2corinthians_8_2"></a>2 Corinthians 8:2

How that in a [polys](../../strongs/g/g4183.md) [dokimē](../../strongs/g/g1382.md) of [thlipsis](../../strongs/g/g2347.md) the [perisseia](../../strongs/g/g4050.md) of their [chara](../../strongs/g/g5479.md) and their [bathos](../../strongs/g/g899.md) [ptōcheia](../../strongs/g/g4432.md) [perisseuō](../../strongs/g/g4052.md) unto the [ploutos](../../strongs/g/g4149.md) of their [haplotēs](../../strongs/g/g572.md).

<a name="2corinthians_8_3"></a>2 Corinthians 8:3

For to [dynamis](../../strongs/g/g1411.md), I [martyreō](../../strongs/g/g3140.md), yea, and beyond [dynamis](../../strongs/g/g1411.md) were [authairetos](../../strongs/g/g830.md);

<a name="2corinthians_8_4"></a>2 Corinthians 8:4

[deomai](../../strongs/g/g1189.md) us with [polys](../../strongs/g/g4183.md) [paraklēsis](../../strongs/g/g3874.md) that we would [dechomai](../../strongs/g/g1209.md) the [charis](../../strongs/g/g5485.md), and take upon us the [koinōnia](../../strongs/g/g2842.md) of [diakonia](../../strongs/g/g1248.md) to the [hagios](../../strongs/g/g40.md).

<a name="2corinthians_8_5"></a>2 Corinthians 8:5

And , not as we [elpizō](../../strongs/g/g1679.md), but first [didōmi](../../strongs/g/g1325.md) their own selves to the [kyrios](../../strongs/g/g2962.md), and unto us by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md).

<a name="2corinthians_8_6"></a>2 Corinthians 8:6

Insomuch that we [parakaleō](../../strongs/g/g3870.md) [titos](../../strongs/g/g5103.md), that as he had [proenarchomai](../../strongs/g/g4278.md), so he would also [epiteleō](../../strongs/g/g2005.md) in you the same [charis](../../strongs/g/g5485.md) also.

<a name="2corinthians_8_7"></a>2 Corinthians 8:7

Therefore, as ye [perisseuō](../../strongs/g/g4052.md) in every, [pistis](../../strongs/g/g4102.md), and [logos](../../strongs/g/g3056.md), and [gnōsis](../../strongs/g/g1108.md), and all [spoudē](../../strongs/g/g4710.md), and your [agapē](../../strongs/g/g26.md) to us, see that ye [perisseuō](../../strongs/g/g4052.md) in this [charis](../../strongs/g/g5485.md) also.

<a name="2corinthians_8_8"></a>2 Corinthians 8:8

I [legō](../../strongs/g/g3004.md) not by [epitagē](../../strongs/g/g2003.md), but by occasion of the [spoudē](../../strongs/g/g4710.md) of others, and to [dokimazō](../../strongs/g/g1381.md) the [gnēsios](../../strongs/g/g1103.md) of your [agapē](../../strongs/g/g26.md).

<a name="2corinthians_8_9"></a>2 Corinthians 8:9

For ye [ginōskō](../../strongs/g/g1097.md) the [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), that, though he was [plousios](../../strongs/g/g4145.md), yet for your sakes he [ptōcheuō](../../strongs/g/g4433.md), that ye through his [ptōcheia](../../strongs/g/g4432.md) might be [plouteō](../../strongs/g/g4147.md).

<a name="2corinthians_8_10"></a>2 Corinthians 8:10

And herein I [didōmi](../../strongs/g/g1325.md) [gnōmē](../../strongs/g/g1106.md): for this is [sympherō](../../strongs/g/g4851.md) for you, who have [proenarchomai](../../strongs/g/g4278.md), not only to [poieō](../../strongs/g/g4160.md), but also to [thelō](../../strongs/g/g2309.md) [perysi](../../strongs/g/g4070.md) ago.

<a name="2corinthians_8_11"></a>2 Corinthians 8:11

[nyni](../../strongs/g/g3570.md) therefore [epiteleō](../../strongs/g/g2005.md) the [poieō](../../strongs/g/g4160.md); that as a [prothymia](../../strongs/g/g4288.md) to [thelō](../../strongs/g/g2309.md), so an [epiteleō](../../strongs/g/g2005.md) also out of that which ye have.

<a name="2corinthians_8_12"></a>2 Corinthians 8:12

For if there be [prokeimai](../../strongs/g/g4295.md) a [prothymia](../../strongs/g/g4288.md), [euprosdektos](../../strongs/g/g2144.md) according to that a man hath, and not according to that he hath not.

<a name="2corinthians_8_13"></a>2 Corinthians 8:13

For not that other men be [anesis](../../strongs/g/g425.md), and ye [thlipsis](../../strongs/g/g2347.md):

<a name="2corinthians_8_14"></a>2 Corinthians 8:14

But by an [isotēs](../../strongs/g/g2471.md), now at this [kairos](../../strongs/g/g2540.md) your [perisseuma](../../strongs/g/g4051.md) for their want, that their [perisseuma](../../strongs/g/g4051.md) also may be for your [hysterēma](../../strongs/g/g5303.md): that there may be [isotēs](../../strongs/g/g2471.md):

<a name="2corinthians_8_15"></a>2 Corinthians 8:15

As it is [graphō](../../strongs/g/g1125.md), He that [polys](../../strongs/g/g4183.md) [pleonazō](../../strongs/g/g4121.md) nothing; and he that [oligos](../../strongs/g/g3641.md) [elattoneō](../../strongs/g/g1641.md) no.

<a name="2corinthians_8_16"></a>2 Corinthians 8:16

But [charis](../../strongs/g/g5485.md) to [theos](../../strongs/g/g2316.md), which [didōmi](../../strongs/g/g1325.md) the same [spoudē](../../strongs/g/g4710.md) into the [kardia](../../strongs/g/g2588.md) of [titos](../../strongs/g/g5103.md) for you.

<a name="2corinthians_8_17"></a>2 Corinthians 8:17

For indeed he [dechomai](../../strongs/g/g1209.md) the [paraklēsis](../../strongs/g/g3874.md); but being [spoudaioteros](../../strongs/g/g4707.md), of his own [authairetos](../../strongs/g/g830.md) he [exerchomai](../../strongs/g/g1831.md) unto you.

<a name="2corinthians_8_18"></a>2 Corinthians 8:18

And we have [sympempō](../../strongs/g/g4842.md) with him the [adelphos](../../strongs/g/g80.md), whose [epainos](../../strongs/g/g1868.md) is in the [euaggelion](../../strongs/g/g2098.md) throughout all the [ekklēsia](../../strongs/g/g1577.md);

<a name="2corinthians_8_19"></a>2 Corinthians 8:19

And not only, but who was also [cheirotoneō](../../strongs/g/g5500.md) of the [ekklēsia](../../strongs/g/g1577.md) to [synekdēmos](../../strongs/g/g4898.md) with us with this [charis](../../strongs/g/g5485.md), which is [diakoneō](../../strongs/g/g1247.md) by us to the [doxa](../../strongs/g/g1391.md) of the same [kyrios](../../strongs/g/g2962.md), and your [prothymia](../../strongs/g/g4288.md):

<a name="2corinthians_8_20"></a>2 Corinthians 8:20

[stellō](../../strongs/g/g4724.md) this, that no man should [mōmaomai](../../strongs/g/g3469.md) us in this [hadrotēs](../../strongs/g/g100.md) which is [diakoneō](../../strongs/g/g1247.md) by us:

<a name="2corinthians_8_21"></a>2 Corinthians 8:21

[pronoeō](../../strongs/g/g4306.md) for [kalos](../../strongs/g/g2570.md), not only in the [enōpion](../../strongs/g/g1799.md) of the [kyrios](../../strongs/g/g2962.md), but also in the [enōpion](../../strongs/g/g1799.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="2corinthians_8_22"></a>2 Corinthians 8:22

And we have [sympempō](../../strongs/g/g4842.md) with them our [adelphos](../../strongs/g/g80.md), whom we have oftentimes [dokimazō](../../strongs/g/g1381.md) [spoudaios](../../strongs/g/g4705.md) in many things, but [nyni](../../strongs/g/g3570.md) [polys](../../strongs/g/g4183.md) [spoudaioteros](../../strongs/g/g4707.md), upon the [polys](../../strongs/g/g4183.md) [pepoithēsis](../../strongs/g/g4006.md) which I have in you.

<a name="2corinthians_8_23"></a>2 Corinthians 8:23

Whether of [titos](../../strongs/g/g5103.md), he is my [koinōnos](../../strongs/g/g2844.md) and [synergos](../../strongs/g/g4904.md) concerning you: or our [adelphos](../../strongs/g/g80.md), the [apostolos](../../strongs/g/g652.md) of the [ekklēsia](../../strongs/g/g1577.md), and the [doxa](../../strongs/g/g1391.md) of [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_8_24"></a>2 Corinthians 8:24

Wherefore [endeiknymi](../../strongs/g/g1731.md) ye to them, and [eis](../../strongs/g/g1519.md) [prosōpon](../../strongs/g/g4383.md) the [ekklēsia](../../strongs/g/g1577.md), the [endeixis](../../strongs/g/g1732.md) of your [agapē](../../strongs/g/g26.md), and of our [kauchēsis](../../strongs/g/g2746.md) on your behalf.

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 7](2corinthians_7.md) - [2 Corinthians 9](2corinthians_9.md)