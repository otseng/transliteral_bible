# [2 Corinthians 1](https://www.blueletterbible.org/kjv/2co/1/1/s_1079001)

<a name="2corinthians_1_1"></a>2 Corinthians 1:1

[Paulos](../../strongs/g/g3972.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), and [Timotheos](../../strongs/g/g5095.md) our [adelphos](../../strongs/g/g80.md), unto the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md) which is at [Korinthos](../../strongs/g/g2882.md), with all the [hagios](../../strongs/g/g40.md) which are in all [Achaïa](../../strongs/g/g882.md):

<a name="2corinthians_1_2"></a>2 Corinthians 1:2

[charis](../../strongs/g/g5485.md) be to you and [eirēnē](../../strongs/g/g1515.md) from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and from the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_1_3"></a>2 Corinthians 1:3

[eulogētos](../../strongs/g/g2128.md) be [theos](../../strongs/g/g2316.md), even the [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [patēr](../../strongs/g/g3962.md) of [oiktirmos](../../strongs/g/g3628.md), and the [theos](../../strongs/g/g2316.md) of all [paraklēsis](../../strongs/g/g3874.md);

<a name="2corinthians_1_4"></a>2 Corinthians 1:4

Who [parakaleō](../../strongs/g/g3870.md) us in all our [thlipsis](../../strongs/g/g2347.md), that we may be able to [parakaleō](../../strongs/g/g3870.md) them which are in any [thlipsis](../../strongs/g/g2347.md), by the [paraklēsis](../../strongs/g/g3874.md) wherewith we ourselves are [parakaleō](../../strongs/g/g3870.md) of [theos](../../strongs/g/g2316.md).

<a name="2corinthians_1_5"></a>2 Corinthians 1:5

For as the [pathēma](../../strongs/g/g3804.md) of [Christos](../../strongs/g/g5547.md) [perisseuō](../../strongs/g/g4052.md) in us, so our [paraklēsis](../../strongs/g/g3874.md) also [perisseuō](../../strongs/g/g4052.md) by [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_1_6"></a>2 Corinthians 1:6

And whether we be [thlibō](../../strongs/g/g2346.md), it is for your [paraklēsis](../../strongs/g/g3874.md) and [sōtēria](../../strongs/g/g4991.md), which is [energeō](../../strongs/g/g1754.md) in the [hypomonē](../../strongs/g/g5281.md) of the same [pathēma](../../strongs/g/g3804.md) which we also [paschō](../../strongs/g/g3958.md): or whether we be [parakaleō](../../strongs/g/g3870.md), it is for your [paraklēsis](../../strongs/g/g3874.md) and [sōtēria](../../strongs/g/g4991.md).

<a name="2corinthians_1_7"></a>2 Corinthians 1:7

And our [elpis](../../strongs/g/g1680.md) of you is [bebaios](../../strongs/g/g949.md), [eidō](../../strongs/g/g1492.md), that as ye are [koinōnos](../../strongs/g/g2844.md) of the [pathēma](../../strongs/g/g3804.md), so shall ye be also of the [paraklēsis](../../strongs/g/g3874.md).

<a name="2corinthians_1_8"></a>2 Corinthians 1:8

For we would not, [adelphos](../../strongs/g/g80.md), have you [agnoeō](../../strongs/g/g50.md) of our [thlipsis](../../strongs/g/g2347.md) which [ginomai](../../strongs/g/g1096.md) to us in [Asia](../../strongs/g/g773.md), that we were [bareō](../../strongs/g/g916.md) [hyperbolē](../../strongs/g/g5236.md), above [dynamis](../../strongs/g/g1411.md), insomuch that we [exaporeō](../../strongs/g/g1820.md) even of [zaō](../../strongs/g/g2198.md):

<a name="2corinthians_1_9"></a>2 Corinthians 1:9

But we had the [apokrima](../../strongs/g/g610.md) of [thanatos](../../strongs/g/g2288.md) in ourselves, that we should not [peithō](../../strongs/g/g3982.md) in ourselves, but in [theos](../../strongs/g/g2316.md) which [egeirō](../../strongs/g/g1453.md) the [nekros](../../strongs/g/g3498.md):

<a name="2corinthians_1_10"></a>2 Corinthians 1:10

Who [rhyomai](../../strongs/g/g4506.md) us from [tēlikoutos](../../strongs/g/g5082.md) a [thanatos](../../strongs/g/g2288.md), and doth [rhyomai](../../strongs/g/g4506.md): in whom we [elpizō](../../strongs/g/g1679.md) that he will yet [rhyomai](../../strongs/g/g4506.md) us;

<a name="2corinthians_1_11"></a>2 Corinthians 1:11

Ye also [synypourgeō](../../strongs/g/g4943.md) by [deēsis](../../strongs/g/g1162.md) for us, that for the [charisma](../../strongs/g/g5486.md) bestowed upon us by the means of [polys](../../strongs/g/g4183.md) [prosōpon](../../strongs/g/g4383.md) [eucharisteō](../../strongs/g/g2168.md) may be [hyper](../../strongs/g/g5228.md) by many on our behalf.

<a name="2corinthians_1_12"></a>2 Corinthians 1:12

For our [kauchēsis](../../strongs/g/g2746.md) is this, the [martyrion](../../strongs/g/g3142.md) of our [syneidēsis](../../strongs/g/g4893.md), that in [haplotēs](../../strongs/g/g572.md) and [theos](../../strongs/g/g2316.md) [eilikrineia](../../strongs/g/g1505.md), not with [sarkikos](../../strongs/g/g4559.md) [sophia](../../strongs/g/g4678.md), but by the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md), we have had our [anastrephō](../../strongs/g/g390.md) in the [kosmos](../../strongs/g/g2889.md), and [perissoterōs](../../strongs/g/g4056.md) to you-ward.

<a name="2corinthians_1_13"></a>2 Corinthians 1:13

For we [graphō](../../strongs/g/g1125.md) none other things unto you, than what ye [anaginōskō](../../strongs/g/g314.md) or [epiginōskō](../../strongs/g/g1921.md); and I [elpizō](../../strongs/g/g1679.md) ye shall [epiginōskō](../../strongs/g/g1921.md) even to the [telos](../../strongs/g/g5056.md);

<a name="2corinthians_1_14"></a>2 Corinthians 1:14

As also ye have [epiginōskō](../../strongs/g/g1921.md) us in [meros](../../strongs/g/g3313.md), that we are your [kauchēma](../../strongs/g/g2745.md), even as ye also are our's in the [hēmera](../../strongs/g/g2250.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="2corinthians_1_15"></a>2 Corinthians 1:15

And in this [pepoithēsis](../../strongs/g/g4006.md) I was [boulomai](../../strongs/g/g1014.md) to [erchomai](../../strongs/g/g2064.md) unto you before, that ye might have a second [charis](../../strongs/g/g5485.md);

<a name="2corinthians_1_16"></a>2 Corinthians 1:16

And to [dierchomai](../../strongs/g/g1330.md) by you into [Makedonia](../../strongs/g/g3109.md), and to [erchomai](../../strongs/g/g2064.md) again out of [Makedonia](../../strongs/g/g3109.md) unto you, and of you to be [propempō](../../strongs/g/g4311.md) toward [Ioudaia](../../strongs/g/g2449.md).

<a name="2corinthians_1_17"></a>2 Corinthians 1:17

When I therefore was thus [bouleuō](../../strongs/g/g1011.md), did I [chraomai](../../strongs/g/g5530.md) [elaphria](../../strongs/g/g1644.md)? or the things that I [bouleuō](../../strongs/g/g1011.md), do I [bouleuō](../../strongs/g/g1011.md) according to the [sarx](../../strongs/g/g4561.md), that with me there should be [nai](../../strongs/g/g3483.md) [nai](../../strongs/g/g3483.md), and [ou](../../strongs/g/g3756.md) [ou](../../strongs/g/g3756.md)?

<a name="2corinthians_1_18"></a>2 Corinthians 1:18

But as [theos](../../strongs/g/g2316.md) is [pistos](../../strongs/g/g4103.md) [hoti](../../strongs/g/g3754.md), our [logos](../../strongs/g/g3056.md) toward you was not [nai](../../strongs/g/g3483.md) and [ou](../../strongs/g/g3756.md).

<a name="2corinthians_1_19"></a>2 Corinthians 1:19

For the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), who was [kēryssō](../../strongs/g/g2784.md) among you by us, even by me and [silouanos](../../strongs/g/g4610.md) and [Timotheos](../../strongs/g/g5095.md), was not [nai](../../strongs/g/g3483.md) and [ou](../../strongs/g/g3756.md), but in him was [nai](../../strongs/g/g3483.md).

<a name="2corinthians_1_20"></a>2 Corinthians 1:20

For all the [epaggelia](../../strongs/g/g1860.md) of [theos](../../strongs/g/g2316.md) in him are [nai](../../strongs/g/g3483.md), and in him [amēn](../../strongs/g/g281.md), unto the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md) by us.

<a name="2corinthians_1_21"></a>2 Corinthians 1:21

Now he which [bebaioō](../../strongs/g/g950.md) us with you in [Christos](../../strongs/g/g5547.md), and hath [chriō](../../strongs/g/g5548.md) us, is [theos](../../strongs/g/g2316.md);

<a name="2corinthians_1_22"></a>2 Corinthians 1:22

Who hath also [sphragizō](../../strongs/g/g4972.md) us, and [didōmi](../../strongs/g/g1325.md) the [arrabōn](../../strongs/g/g728.md) of the [pneuma](../../strongs/g/g4151.md) in our [kardia](../../strongs/g/g2588.md).

<a name="2corinthians_1_23"></a>2 Corinthians 1:23

Moreover I [epikaleō](../../strongs/g/g1941.md) [theos](../../strongs/g/g2316.md) for a [martys](../../strongs/g/g3144.md) upon my [psychē](../../strongs/g/g5590.md), that to [pheidomai](../../strongs/g/g5339.md) you I [erchomai](../../strongs/g/g2064.md) not as yet unto [Korinthos](../../strongs/g/g2882.md).

<a name="2corinthians_1_24"></a>2 Corinthians 1:24

Not for that we have [kyrieuō](../../strongs/g/g2961.md) your [pistis](../../strongs/g/g4102.md), but are [synergos](../../strongs/g/g4904.md) of your [chara](../../strongs/g/g5479.md): for by [pistis](../../strongs/g/g4102.md) ye [histēmi](../../strongs/g/g2476.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[1 Corinthians 16](../1corinthians/1corinthians_16.md) - [2 Corinthians 2](2corinthians_2.md)