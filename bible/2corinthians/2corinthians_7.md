# [2 Corinthians 7](https://www.blueletterbible.org/kjv/2co/7/1/s_1085001)

<a name="2corinthians_7_1"></a>2 Corinthians 7:1

Having therefore these [epaggelia](../../strongs/g/g1860.md), [agapētos](../../strongs/g/g27.md), let us [katharizō](../../strongs/g/g2511.md) ourselves from all [molysmos](../../strongs/g/g3436.md) of the [sarx](../../strongs/g/g4561.md) and [pneuma](../../strongs/g/g4151.md), [epiteleō](../../strongs/g/g2005.md) [hagiosyne](../../strongs/g/g42.md) in the [phobos](../../strongs/g/g5401.md) of [theos](../../strongs/g/g2316.md).

<a name="2corinthians_7_2"></a>2 Corinthians 7:2

[chōreō](../../strongs/g/g5562.md) us; we have [adikeō](../../strongs/g/g91.md) [oudeis](../../strongs/g/g3762.md), we have [phtheirō](../../strongs/g/g5351.md) [oudeis](../../strongs/g/g3762.md), we have [pleonekteō](../../strongs/g/g4122.md) [oudeis](../../strongs/g/g3762.md).

<a name="2corinthians_7_3"></a>2 Corinthians 7:3

I [legō](../../strongs/g/g3004.md) not this to [katakrisis](../../strongs/g/g2633.md) you: for I have [proereō](../../strongs/g/g4280.md), that ye are in our [kardia](../../strongs/g/g2588.md) to [synapothnēskō](../../strongs/g/g4880.md) and [syzaō](../../strongs/g/g4800.md) you.

<a name="2corinthians_7_4"></a>2 Corinthians 7:4

[polys](../../strongs/g/g4183.md) is my [parrēsia](../../strongs/g/g3954.md) toward you, [polys](../../strongs/g/g4183.md) is my [kauchēsis](../../strongs/g/g2746.md) of you: I am [plēroō](../../strongs/g/g4137.md) with [paraklēsis](../../strongs/g/g3874.md), I am [hyperperisseuō](../../strongs/g/g5248.md) [chara](../../strongs/g/g5479.md) in all our [thlipsis](../../strongs/g/g2347.md).

<a name="2corinthians_7_5"></a>2 Corinthians 7:5

For, when we were [erchomai](../../strongs/g/g2064.md) into [Makedonia](../../strongs/g/g3109.md), our [sarx](../../strongs/g/g4561.md) had no [anesis](../../strongs/g/g425.md), but we were [thlibō](../../strongs/g/g2346.md) on every side; [exōthen](../../strongs/g/g1855.md) were [machē](../../strongs/g/g3163.md), [esōthen](../../strongs/g/g2081.md) were [phobos](../../strongs/g/g5401.md).

<a name="2corinthians_7_6"></a>2 Corinthians 7:6

Nevertheless [theos](../../strongs/g/g2316.md), that [parakaleō](../../strongs/g/g3870.md) [tapeinos](../../strongs/g/g5011.md), [parakaleō](../../strongs/g/g3870.md) us by the [parousia](../../strongs/g/g3952.md) of [titos](../../strongs/g/g5103.md);

<a name="2corinthians_7_7"></a>2 Corinthians 7:7

And not by his [parousia](../../strongs/g/g3952.md) only, but by the [paraklēsis](../../strongs/g/g3874.md) wherewith he was [parakaleō](../../strongs/g/g3870.md) in you, when he [anaggellō](../../strongs/g/g312.md) us your [epipothēsis](../../strongs/g/g1972.md), your [odyrmos](../../strongs/g/g3602.md), your [zēlos](../../strongs/g/g2205.md) toward me; so that I [chairō](../../strongs/g/g5463.md) the more.

<a name="2corinthians_7_8"></a>2 Corinthians 7:8

For though I [lypeō](../../strongs/g/g3076.md) you with an [epistolē](../../strongs/g/g1992.md), I do not [metamelomai](../../strongs/g/g3338.md), though I did [metamelomai](../../strongs/g/g3338.md): for I [blepō](../../strongs/g/g991.md) that the same [epistolē](../../strongs/g/g1992.md) hath [lypeō](../../strongs/g/g3076.md) you, though it were but for a [hōra](../../strongs/g/g5610.md).

<a name="2corinthians_7_9"></a>2 Corinthians 7:9

Now I [chairō](../../strongs/g/g5463.md), not that ye were [lypeō](../../strongs/g/g3076.md), but that ye [lypeō](../../strongs/g/g3076.md) to [metanoia](../../strongs/g/g3341.md): for ye were [lypeō](../../strongs/g/g3076.md) after [theos](../../strongs/g/g2316.md), that ye might [zēmioō](../../strongs/g/g2210.md) by us in [mēdeis](../../strongs/g/g3367.md).

<a name="2corinthians_7_10"></a>2 Corinthians 7:10

For [theos](../../strongs/g/g2316.md) [kata](../../strongs/g/g2596.md) [lypē](../../strongs/g/g3077.md) [katergazomai](../../strongs/g/g2716.md) [metanoia](../../strongs/g/g3341.md) to [sōtēria](../../strongs/g/g4991.md) [ametamelētos](../../strongs/g/g278.md): but the [lypē](../../strongs/g/g3077.md) of the [kosmos](../../strongs/g/g2889.md) [katergazomai](../../strongs/g/g2716.md) [thanatos](../../strongs/g/g2288.md).

<a name="2corinthians_7_11"></a>2 Corinthians 7:11

For [idou](../../strongs/g/g2400.md) this selfsame thing, that ye [lypeō](../../strongs/g/g3076.md) after [theos](../../strongs/g/g2316.md), what [spoudē](../../strongs/g/g4710.md) it [katergazomai](../../strongs/g/g2716.md) in you, [alla](../../strongs/g/g235.md), [apologia](../../strongs/g/g627.md), [alla](../../strongs/g/g235.md), [aganaktēsis](../../strongs/g/g24.md), [alla](../../strongs/g/g235.md), [phobos](../../strongs/g/g5401.md), [alla](../../strongs/g/g235.md), [epipothēsis](../../strongs/g/g1972.md), [alla](../../strongs/g/g235.md), [zēlos](../../strongs/g/g2205.md), [alla](../../strongs/g/g235.md), [ekdikēsis](../../strongs/g/g1557.md)! In all things ye have [synistēmi](../../strongs/g/g4921.md) yourselves to be [hagnos](../../strongs/g/g53.md) in this [pragma](../../strongs/g/g4229.md).

<a name="2corinthians_7_12"></a>2 Corinthians 7:12

Wherefore, though I [graphō](../../strongs/g/g1125.md) unto you, I did it not for his cause [adikeō](../../strongs/g/g91.md), nor for his cause [adikeō](../../strongs/g/g91.md), but that our [spoudē](../../strongs/g/g4710.md) for you in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md) might [phaneroō](../../strongs/g/g5319.md) unto you.

<a name="2corinthians_7_13"></a>2 Corinthians 7:13

Therefore we were [parakaleō](../../strongs/g/g3870.md) in your [paraklēsis](../../strongs/g/g3874.md): yea, and [perissoterōs](../../strongs/g/g4056.md) the more [chairō](../../strongs/g/g5463.md) we for the [chara](../../strongs/g/g5479.md) of [titos](../../strongs/g/g5103.md), because his [pneuma](../../strongs/g/g4151.md) was [anapauō](../../strongs/g/g373.md) by you all.

<a name="2corinthians_7_14"></a>2 Corinthians 7:14

For if I have [kauchaomai](../../strongs/g/g2744.md) any thing to him of you, I am not [kataischynō](../../strongs/g/g2617.md); but as we [laleō](../../strongs/g/g2980.md) all things to you in [alētheia](../../strongs/g/g225.md), even so our [kauchēsis](../../strongs/g/g2746.md), which I before [titos](../../strongs/g/g5103.md), is [ginomai](../../strongs/g/g1096.md) an [alētheia](../../strongs/g/g225.md).

<a name="2corinthians_7_15"></a>2 Corinthians 7:15

And his [splagchnon](../../strongs/g/g4698.md) is [perissoterōs](../../strongs/g/g4056.md) toward you, whilst he [anamimnēskō](../../strongs/g/g363.md) the [hypakoē](../../strongs/g/g5218.md) of you all, how with [phobos](../../strongs/g/g5401.md) and [tromos](../../strongs/g/g5156.md) ye [dechomai](../../strongs/g/g1209.md) him.

<a name="2corinthians_7_16"></a>2 Corinthians 7:16

I [chairō](../../strongs/g/g5463.md) therefore that I have [tharreō](../../strongs/g/g2292.md) in you in all.

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 6](2corinthians_6.md) - [2 Corinthians 8](2corinthians_8.md)