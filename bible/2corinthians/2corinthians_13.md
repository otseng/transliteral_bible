# [2 Corinthians 13](https://www.blueletterbible.org/kjv/2co/13/1/s_1091001)

<a name="2corinthians_13_1"></a>2 Corinthians 13:1

This is the third I am [erchomai](../../strongs/g/g2064.md) to you. In the [stoma](../../strongs/g/g4750.md) of [dyo](../../strongs/g/g1417.md) or [treis](../../strongs/g/g5140.md) [martys](../../strongs/g/g3144.md) shall every [rhēma](../../strongs/g/g4487.md) be [histēmi](../../strongs/g/g2476.md).

<a name="2corinthians_13_2"></a>2 Corinthians 13:2

I [proereō](../../strongs/g/g4280.md) you, and [prolegō](../../strongs/g/g4302.md) you, as if I were [pareimi](../../strongs/g/g3918.md), the second time; and being [apeimi](../../strongs/g/g548.md) now I [graphō](../../strongs/g/g1125.md) to [proamartanō](../../strongs/g/g4258.md), and to all [loipos](../../strongs/g/g3062.md), that, if I [erchomai](../../strongs/g/g2064.md) again, I will not [pheidomai](../../strongs/g/g5339.md):

<a name="2corinthians_13_3"></a>2 Corinthians 13:3

Since ye [zēteō](../../strongs/g/g2212.md) a [dokimē](../../strongs/g/g1382.md) of [Christos](../../strongs/g/g5547.md) [laleō](../../strongs/g/g2980.md) in me, which to you-ward is not [astheneō](../../strongs/g/g770.md), but is [dynateō](../../strongs/g/g1414.md) in you.

<a name="2corinthians_13_4"></a>2 Corinthians 13:4

For though he was [stauroō](../../strongs/g/g4717.md) through [astheneia](../../strongs/g/g769.md), yet he [zaō](../../strongs/g/g2198.md) by the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md). For we also are [astheneō](../../strongs/g/g770.md) in him, but we shall [zaō](../../strongs/g/g2198.md) with him by the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md) toward you.

<a name="2corinthians_13_5"></a>2 Corinthians 13:5

[peirazō](../../strongs/g/g3985.md) yourselves, whether ye be in the [pistis](../../strongs/g/g4102.md); [dokimazō](../../strongs/g/g1381.md) your own selves. [epiginōskō](../../strongs/g/g1921.md) ye not your own selves, how that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) is in you, except ye be [adokimos](../../strongs/g/g96.md)?

<a name="2corinthians_13_6"></a>2 Corinthians 13:6

But I [elpizō](../../strongs/g/g1679.md) that ye shall [ginōskō](../../strongs/g/g1097.md) that we are not [adokimos](../../strongs/g/g96.md).

<a name="2corinthians_13_7"></a>2 Corinthians 13:7

Now I [euchomai](../../strongs/g/g2172.md) to [theos](../../strongs/g/g2316.md) that ye [poieō](../../strongs/g/g4160.md) [mēdeis](../../strongs/g/g3367.md) [kakos](../../strongs/g/g2556.md); not that we should [phainō](../../strongs/g/g5316.md) [dokimos](../../strongs/g/g1384.md), but that ye should [poieō](../../strongs/g/g4160.md) [kalos](../../strongs/g/g2570.md), though we be as [adokimos](../../strongs/g/g96.md).

<a name="2corinthians_13_8"></a>2 Corinthians 13:8

For we [dynamai](../../strongs/g/g1410.md) nothing against the [alētheia](../../strongs/g/g225.md), but for the [alētheia](../../strongs/g/g225.md).

<a name="2corinthians_13_9"></a>2 Corinthians 13:9

For we are [chairō](../../strongs/g/g5463.md), when we are [astheneō](../../strongs/g/g770.md), and ye are [dynatos](../../strongs/g/g1415.md): and this also we [euchomai](../../strongs/g/g2172.md), even your [katartisis](../../strongs/g/g2676.md).

<a name="2corinthians_13_10"></a>2 Corinthians 13:10

Therefore I [graphō](../../strongs/g/g1125.md) these things being [apeimi](../../strongs/g/g548.md), lest being [pareimi](../../strongs/g/g3918.md) I should [chraomai](../../strongs/g/g5530.md) [apotomōs](../../strongs/g/g664.md), according to the [exousia](../../strongs/g/g1849.md) which the [kyrios](../../strongs/g/g2962.md) hath [didōmi](../../strongs/g/g1325.md) me to [oikodomē](../../strongs/g/g3619.md), and not to [kathairesis](../../strongs/g/g2506.md).

<a name="2corinthians_13_11"></a>2 Corinthians 13:11

Finally, [adelphos](../../strongs/g/g80.md), [chairō](../../strongs/g/g5463.md). Be [katartizō](../../strongs/g/g2675.md), be [parakaleō](../../strongs/g/g3870.md), be of one [phroneō](../../strongs/g/g5426.md), [eirēneuō](../../strongs/g/g1514.md); and the [theos](../../strongs/g/g2316.md) of [agapē](../../strongs/g/g26.md) and [eirēnē](../../strongs/g/g1515.md) shall be with you.

<a name="2corinthians_13_12"></a>2 Corinthians 13:12

[aspazomai](../../strongs/g/g782.md) [allēlōn](../../strongs/g/g240.md) with an [hagios](../../strongs/g/g40.md) [philēma](../../strongs/g/g5370.md).

<a name="2corinthians_13_13"></a>2 Corinthians 13:13

All the [hagios](../../strongs/g/g40.md) [aspazomai](../../strongs/g/g782.md) you.

<a name="2corinthians_13_14"></a>2 Corinthians 13:14

The [charis](../../strongs/g/g5485.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md), and the [koinōnia](../../strongs/g/g2842.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 12](2corinthians_12.md)