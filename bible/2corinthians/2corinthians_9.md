# [2 Corinthians 9](https://www.blueletterbible.org/kjv/2co/9/1/s_1087001)

<a name="2corinthians_9_1"></a>2 Corinthians 9:1

For as touching the [diakonia](../../strongs/g/g1248.md) to the [hagios](../../strongs/g/g40.md), it is [perissos](../../strongs/g/g4053.md) for me to [graphō](../../strongs/g/g1125.md) to you:

<a name="2corinthians_9_2"></a>2 Corinthians 9:2

For I [eidō](../../strongs/g/g1492.md) your [prothymia](../../strongs/g/g4288.md), for which I [kauchaomai](../../strongs/g/g2744.md) of you to them of [Makedōn](../../strongs/g/g3110.md), that [Achaïa](../../strongs/g/g882.md) was [paraskeuazō](../../strongs/g/g3903.md) [perysi](../../strongs/g/g4070.md) [apo](../../strongs/g/g575.md); and your [zēlos](../../strongs/g/g2205.md) hath [erethizō](../../strongs/g/g2042.md) very many.

<a name="2corinthians_9_3"></a>2 Corinthians 9:3

Yet have I [pempō](../../strongs/g/g3992.md) the [adelphos](../../strongs/g/g80.md), lest our [kauchēma](../../strongs/g/g2745.md) of you should be [kenoō](../../strongs/g/g2758.md) in this [meros](../../strongs/g/g3313.md); that, as I [legō](../../strongs/g/g3004.md), ye may be [paraskeuazō](../../strongs/g/g3903.md):

<a name="2corinthians_9_4"></a>2 Corinthians 9:4

Lest haply if they of [Makedōn](../../strongs/g/g3110.md) [erchomai](../../strongs/g/g2064.md) with me, and [heuriskō](../../strongs/g/g2147.md) you [aparaskeuastos](../../strongs/g/g532.md), we (that we [legō](../../strongs/g/g3004.md) not, ye) should be [kataischynō](../../strongs/g/g2617.md) in this same [hypostasis](../../strongs/g/g5287.md) [kauchēsis](../../strongs/g/g2746.md).

<a name="2corinthians_9_5"></a>2 Corinthians 9:5

Therefore I [hēgeomai](../../strongs/g/g2233.md) it [anagkaios](../../strongs/g/g316.md) to [parakaleō](../../strongs/g/g3870.md) the [adelphos](../../strongs/g/g80.md), that they would [proerchomai](../../strongs/g/g4281.md) unto you, and [prokatartizō](../../strongs/g/g4294.md) your [eulogia](../../strongs/g/g2129.md), whereof ye had [prokataggellō](../../strongs/g/g4293.md), that the same might be [hetoimos](../../strongs/g/g2092.md), as [eulogia](../../strongs/g/g2129.md), and not as of [pleonexia](../../strongs/g/g4124.md).

<a name="2corinthians_9_6"></a>2 Corinthians 9:6

But this, He which [speirō](../../strongs/g/g4687.md) [pheidomenōs](../../strongs/g/g5340.md) shall [therizō](../../strongs/g/g2325.md) also [pheidomenōs](../../strongs/g/g5340.md); and he which [speirō](../../strongs/g/g4687.md) [epi](../../strongs/g/g1909.md) [eulogia](../../strongs/g/g2129.md) shall [therizō](../../strongs/g/g2325.md) also [epi](../../strongs/g/g1909.md) [eulogia](../../strongs/g/g2129.md) .

<a name="2corinthians_9_7"></a>2 Corinthians 9:7

[hekastos](../../strongs/g/g1538.md) according as he [proaireō](../../strongs/g/g4255.md) in his [kardia](../../strongs/g/g2588.md); not [ek](../../strongs/g/g1537.md) [lypē](../../strongs/g/g3077.md), or of [anagkē](../../strongs/g/g318.md): for [theos](../../strongs/g/g2316.md) [agapaō](../../strongs/g/g25.md) a [hilaros](../../strongs/g/g2431.md) [dotēs](../../strongs/g/g1395.md).

<a name="2corinthians_9_8"></a>2 Corinthians 9:8

And [theos](../../strongs/g/g2316.md) is [dynatos](../../strongs/g/g1415.md) to make all [charis](../../strongs/g/g5485.md) [perisseuō](../../strongs/g/g4052.md) toward you; that ye, [pantote](../../strongs/g/g3842.md) having all [autarkeia](../../strongs/g/g841.md) in all things, may [perisseuō](../../strongs/g/g4052.md) to every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md):

<a name="2corinthians_9_9"></a>2 Corinthians 9:9

(As it is [graphō](../../strongs/g/g1125.md), He hath [skorpizō](../../strongs/g/g4650.md); he hath [didōmi](../../strongs/g/g1325.md) to the [penēs](../../strongs/g/g3993.md): his [dikaiosynē](../../strongs/g/g1343.md) [menō](../../strongs/g/g3306.md) for [aiōn](../../strongs/g/g165.md).

<a name="2corinthians_9_10"></a>2 Corinthians 9:10

Now he that [epichorēgeō](../../strongs/g/g2023.md) [sperma](../../strongs/g/g4690.md) to the [speirō](../../strongs/g/g4687.md) both [chorēgeō](../../strongs/g/g5524.md) [artos](../../strongs/g/g740.md) for [brōsis](../../strongs/g/g1035.md), and [plēthynō](../../strongs/g/g4129.md) your [sporos](../../strongs/g/g4703.md), and [auxanō](../../strongs/g/g837.md) the [gennēma](../../strongs/g/g1081.md) of your [dikaiosynē](../../strongs/g/g1343.md);)

<a name="2corinthians_9_11"></a>2 Corinthians 9:11

Being [ploutizō](../../strongs/g/g4148.md) in every thing to all [haplotēs](../../strongs/g/g572.md), which [katergazomai](../../strongs/g/g2716.md) through us [eucharistia](../../strongs/g/g2169.md) to [theos](../../strongs/g/g2316.md).

<a name="2corinthians_9_12"></a>2 Corinthians 9:12

For the [diakonia](../../strongs/g/g1248.md) of this [leitourgia](../../strongs/g/g3009.md) not only [prosanaplēroō](../../strongs/g/g4322.md) the [hysterēma](../../strongs/g/g5303.md) of the [hagios](../../strongs/g/g40.md), but is [perisseuō](../../strongs/g/g4052.md) also by [polys](../../strongs/g/g4183.md) [eucharistia](../../strongs/g/g2169.md) unto [theos](../../strongs/g/g2316.md);

<a name="2corinthians_9_13"></a>2 Corinthians 9:13

Whiles by the [dokimē](../../strongs/g/g1382.md) of this [diakonia](../../strongs/g/g1248.md) they [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) for your [homologia](../../strongs/g/g3671.md) [hypotagē](../../strongs/g/g5292.md) unto the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md), and for your [haplotēs](../../strongs/g/g572.md) [koinōnia](../../strongs/g/g2842.md) unto them, and unto all men;

<a name="2corinthians_9_14"></a>2 Corinthians 9:14

And by their [deēsis](../../strongs/g/g1162.md) for you, which [epipotheo](../../strongs/g/g1971.md) you for the [hyperballō](../../strongs/g/g5235.md) [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) in you.

<a name="2corinthians_9_15"></a>2 Corinthians 9:15

[charis](../../strongs/g/g5485.md) unto [theos](../../strongs/g/g2316.md) for his [anekdiēgētos](../../strongs/g/g411.md) [dōrea](../../strongs/g/g1431.md).

---

[Transliteral Bible](../bible.md)

[2Corinthians](2corinthians.md)

[2Corinthians 8](2corinthianis_8.md) - [2Corinthians 10](2corinthians_10.md)