# [2 Corinthians 10](https://www.blueletterbible.org/kjv/2co/10/1/s_1088001)

<a name="2corinthians_10_1"></a>2 Corinthians 10:1

Now I [Paulos](../../strongs/g/g3972.md) myself [parakaleō](../../strongs/g/g3870.md) you by the [praotēs](../../strongs/g/g4236.md) and [epieikeia](../../strongs/g/g1932.md) of [Christos](../../strongs/g/g5547.md), who in [prosōpon](../../strongs/g/g4383.md) am [tapeinos](../../strongs/g/g5011.md) among you, but being [apeimi](../../strongs/g/g548.md) am [tharreō](../../strongs/g/g2292.md) toward you:

<a name="2corinthians_10_2"></a>2 Corinthians 10:2

But I [deomai](../../strongs/g/g1189.md), that I may not be [tharreō](../../strongs/g/g2292.md) when I am [pareimi](../../strongs/g/g3918.md) with that [pepoithēsis](../../strongs/g/g4006.md), wherewith I [logizomai](../../strongs/g/g3049.md) to be [tolmaō](../../strongs/g/g5111.md) against some, which [logizomai](../../strongs/g/g3049.md) of us as if we [peripateō](../../strongs/g/g4043.md) according to the [sarx](../../strongs/g/g4561.md).

<a name="2corinthians_10_3"></a>2 Corinthians 10:3

For though we [peripateō](../../strongs/g/g4043.md) in the [sarx](../../strongs/g/g4561.md), we do not [strateuō](../../strongs/g/g4754.md) after the [sarx](../../strongs/g/g4561.md):

<a name="2corinthians_10_4"></a>2 Corinthians 10:4

(For the [hoplon](../../strongs/g/g3696.md) of our [strateia](../../strongs/g/g4752.md) are not [sarkikos](../../strongs/g/g4559.md), but [dynatos](../../strongs/g/g1415.md) through [theos](../../strongs/g/g2316.md) to the [kathairesis](../../strongs/g/g2506.md) of [ochyrōma](../../strongs/g/g3794.md);)

<a name="2corinthians_10_5"></a>2 Corinthians 10:5

[kathaireō](../../strongs/g/g2507.md) [logismos](../../strongs/g/g3053.md), and every [hypsōma](../../strongs/g/g5313.md) that [epairō](../../strongs/g/g1869.md) against the [gnōsis](../../strongs/g/g1108.md) of [theos](../../strongs/g/g2316.md), and [aichmalōtizō](../../strongs/g/g163.md) every [noēma](../../strongs/g/g3540.md) to the [hypakoē](../../strongs/g/g5218.md) of [Christos](../../strongs/g/g5547.md);

<a name="2corinthians_10_6"></a>2 Corinthians 10:6

And having in a [hetoimos](../../strongs/g/g2092.md) to [ekdikeō](../../strongs/g/g1556.md) all [parakoē](../../strongs/g/g3876.md), when your [hypakoē](../../strongs/g/g5218.md) is [plēroō](../../strongs/g/g4137.md).

<a name="2corinthians_10_7"></a>2 Corinthians 10:7

Do ye [blepō](../../strongs/g/g991.md) things after the [prosōpon](../../strongs/g/g4383.md)? If any man [peithō](../../strongs/g/g3982.md) to himself that he is [Christos](../../strongs/g/g5547.md), let him of himself [logizomai](../../strongs/g/g3049.md) this again, that, as he is [Christos](../../strongs/g/g5547.md), even so are we [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_10_8"></a>2 Corinthians 10:8

For though I should [kauchaomai](../../strongs/g/g2744.md) somewhat [perissoteros](../../strongs/g/g4055.md) of our [exousia](../../strongs/g/g1849.md), which the [kyrios](../../strongs/g/g2962.md) hath [didōmi](../../strongs/g/g1325.md) us for [oikodomē](../../strongs/g/g3619.md), and not for your [kathairesis](../../strongs/g/g2506.md), I should not be [aischynō](../../strongs/g/g153.md):

<a name="2corinthians_10_9"></a>2 Corinthians 10:9

That I may not [dokeō](../../strongs/g/g1380.md) as if I would [ekphobeō](../../strongs/g/g1629.md) you by [epistolē](../../strongs/g/g1992.md).

<a name="2corinthians_10_10"></a>2 Corinthians 10:10

For [epistolē](../../strongs/g/g1992.md), [phēmi](../../strongs/g/g5346.md) they, [barys](../../strongs/g/g926.md) and [ischyros](../../strongs/g/g2478.md); but [sōma](../../strongs/g/g4983.md) [parousia](../../strongs/g/g3952.md) [asthenēs](../../strongs/g/g772.md), and [logos](../../strongs/g/g3056.md) [exoutheneō](../../strongs/g/g1848.md).

<a name="2corinthians_10_11"></a>2 Corinthians 10:11

Let such an one [logizomai](../../strongs/g/g3049.md) this, that, such as we are in [logos](../../strongs/g/g3056.md) by [epistolē](../../strongs/g/g1992.md) when we are [apeimi](../../strongs/g/g548.md), such will we be also in [ergon](../../strongs/g/g2041.md) when we are [pareimi](../../strongs/g/g3918.md).

<a name="2corinthians_10_12"></a>2 Corinthians 10:12

For we [tolmaō](../../strongs/g/g5111.md) not [egkrinō](../../strongs/g/g1469.md), or [sygkrinō](../../strongs/g/g4793.md) ourselves with some that [synistēmi](../../strongs/g/g4921.md) themselves: but they [metreō](../../strongs/g/g3354.md) themselves by themselves, and [sygkrinō](../../strongs/g/g4793.md) themselves among themselves, are not [syniēmi](../../strongs/g/g4920.md).

<a name="2corinthians_10_13"></a>2 Corinthians 10:13

But we will not [kauchaomai](../../strongs/g/g2744.md) of things without our [ametros](../../strongs/g/g280.md), but according to the [metron](../../strongs/g/g3358.md) of the [kanōn](../../strongs/g/g2583.md) which [theos](../../strongs/g/g2316.md) hath [merizō](../../strongs/g/g3307.md) to us, a measure to [ephikneomai](../../strongs/g/g2185.md) even unto you.

<a name="2corinthians_10_14"></a>2 Corinthians 10:14

For we [hyperekteinō](../../strongs/g/g5239.md) not ourselves, as though we [ephikneomai](../../strongs/g/g2185.md) not unto you: for we are [phthanō](../../strongs/g/g5348.md) as far as to you also in [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md):

<a name="2corinthians_10_15"></a>2 Corinthians 10:15

Not [kauchaomai](../../strongs/g/g2744.md) of things without our [ametros](../../strongs/g/g280.md), that is, of [allotrios](../../strongs/g/g245.md) [kopos](../../strongs/g/g2873.md); but having [elpis](../../strongs/g/g1680.md), when your [pistis](../../strongs/g/g4102.md) is [auxanō](../../strongs/g/g837.md), that we shall be [megalynō](../../strongs/g/g3170.md) by you according to our [kanōn](../../strongs/g/g2583.md) [perisseia](../../strongs/g/g4050.md),

<a name="2corinthians_10_16"></a>2 Corinthians 10:16

To [euaggelizō](../../strongs/g/g2097.md) in the [hyperekeina](../../strongs/g/g5238.md) you, and not to [kauchaomai](../../strongs/g/g2744.md) in [allotrios](../../strongs/g/g245.md) [kanōn](../../strongs/g/g2583.md) of [hetoimos](../../strongs/g/g2092.md).

<a name="2corinthians_10_17"></a>2 Corinthians 10:17

But he that [kauchaomai](../../strongs/g/g2744.md), let him [kauchaomai](../../strongs/g/g2744.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="2corinthians_10_18"></a>2 Corinthians 10:18

For not he that [synistēmi](../../strongs/g/g4921.md) himself is [dokimos](../../strongs/g/g1384.md), but whom the [kyrios](../../strongs/g/g2962.md) [synistēmi](../../strongs/g/g4921.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 9](2corinthians_9.md) - [2 Corinthians 11](2corinthians_11.md)