# [2 Corinthians 11](https://www.blueletterbible.org/kjv/2co/11/1/s_1089001i)

<a name="2corinthians_11_1"></a>2 Corinthians 11:1

[ophelon](../../strongs/g/g3785.md) ye could [anechō](../../strongs/g/g430.md) with me a [mikron](../../strongs/g/g3397.md) in [aphrosynē](../../strongs/g/g877.md): and indeed [anechō](../../strongs/g/g430.md) with me.

<a name="2corinthians_11_2"></a>2 Corinthians 11:2

For I am [zēloō](../../strongs/g/g2206.md) over you with [theos](../../strongs/g/g2316.md) [zēlos](../../strongs/g/g2205.md): for I have [harmozō](../../strongs/g/g718.md) you to one [anēr](../../strongs/g/g435.md), that I may [paristēmi](../../strongs/g/g3936.md) you as a [hagnos](../../strongs/g/g53.md) [parthenos](../../strongs/g/g3933.md) to [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_11_3"></a>2 Corinthians 11:3

But I [phobeō](../../strongs/g/g5399.md), lest by any means, as the [ophis](../../strongs/g/g3789.md) [exapataō](../../strongs/g/g1818.md) [heya](../../strongs/g/g2096.md) through his [panourgia](../../strongs/g/g3834.md), so your [noēma](../../strongs/g/g3540.md) should be [phtheirō](../../strongs/g/g5351.md) from the [haplotēs](../../strongs/g/g572.md) that is in [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_11_4"></a>2 Corinthians 11:4

For if he that [erchomai](../../strongs/g/g2064.md) [kēryssō](../../strongs/g/g2784.md) another [Iēsous](../../strongs/g/g2424.md), whom we have not [kēryssō](../../strongs/g/g2784.md), or ye [lambanō](../../strongs/g/g2983.md) another [pneuma](../../strongs/g/g4151.md), which ye have not [lambanō](../../strongs/g/g2983.md), or another [euaggelion](../../strongs/g/g2098.md), which ye have not [dechomai](../../strongs/g/g1209.md), ye might [kalōs](../../strongs/g/g2573.md) [anechō](../../strongs/g/g430.md).

<a name="2corinthians_11_5"></a>2 Corinthians 11:5

For I [logizomai](../../strongs/g/g3049.md) I was [mēdeis](../../strongs/g/g3367.md) [hystereō](../../strongs/g/g5302.md) the very [lian](../../strongs/g/g3029.md) [apostolos](../../strongs/g/g652.md).

<a name="2corinthians_11_6"></a>2 Corinthians 11:6

But though [idiōtēs](../../strongs/g/g2399.md) in [logos](../../strongs/g/g3056.md), yet not in [gnōsis](../../strongs/g/g1108.md); but we have been throughly [phaneroō](../../strongs/g/g5319.md) among you in all things.

<a name="2corinthians_11_7"></a>2 Corinthians 11:7

Have I [poieō](../../strongs/g/g4160.md) [hamartia](../../strongs/g/g266.md) in [tapeinoō](../../strongs/g/g5013.md) myself that ye might be [hypsoō](../../strongs/g/g5312.md), because I have [euaggelizō](../../strongs/g/g2097.md) to you the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md) [dōrean](../../strongs/g/g1432.md)?

<a name="2corinthians_11_8"></a>2 Corinthians 11:8

I [sylaō](../../strongs/g/g4813.md) other [ekklēsia](../../strongs/g/g1577.md), [lambanō](../../strongs/g/g2983.md) [opsōnion](../../strongs/g/g3800.md) of them, to [diakonia](../../strongs/g/g1248.md) you.

<a name="2corinthians_11_9"></a>2 Corinthians 11:9

And when I was [pareimi](../../strongs/g/g3918.md) with you, and [hystereō](../../strongs/g/g5302.md), I was [katanarkaō](../../strongs/g/g2655.md) to no man: for that which was [hysterēma](../../strongs/g/g5303.md) to me the [adelphos](../../strongs/g/g80.md) which [erchomai](../../strongs/g/g2064.md) from [Makedonia](../../strongs/g/g3109.md) [prosanaplēroō](../../strongs/g/g4322.md): and in all things I have [tēreō](../../strongs/g/g5083.md) myself from being [abarēs](../../strongs/g/g4.md) unto you, and so will I [tēreō](../../strongs/g/g5083.md) myself.

<a name="2corinthians_11_10"></a>2 Corinthians 11:10

As the [alētheia](../../strongs/g/g225.md) of [Christos](../../strongs/g/g5547.md) is in me, no man shall [sphragizō](../../strongs/g/g4972.md) [phrassō](../../strongs/g/g5420.md) me of this [kauchēsis](../../strongs/g/g2746.md) in the [klima](../../strongs/g/g2824.md) of [Achaïa](../../strongs/g/g882.md).

<a name="2corinthians_11_11"></a>2 Corinthians 11:11

Wherefore? because I [agapaō](../../strongs/g/g25.md) you not? [theos](../../strongs/g/g2316.md) [eidō](../../strongs/g/g1492.md).

<a name="2corinthians_11_12"></a>2 Corinthians 11:12

But what I [poieō](../../strongs/g/g4160.md), that I will [poieō](../../strongs/g/g4160.md), that I may [ekkoptō](../../strongs/g/g1581.md) [aphormē](../../strongs/g/g874.md) from them which [thelō](../../strongs/g/g2309.md) [aphormē](../../strongs/g/g874.md); that wherein they [kauchaomai](../../strongs/g/g2744.md), they may be [heuriskō](../../strongs/g/g2147.md) even as we.

<a name="2corinthians_11_13"></a>2 Corinthians 11:13

For such are [pseudapostolos](../../strongs/g/g5570.md), [dolios](../../strongs/g/g1386.md) [ergatēs](../../strongs/g/g2040.md), [metaschēmatizō](../../strongs/g/g3345.md) into the [apostolos](../../strongs/g/g652.md) of [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_11_14"></a>2 Corinthians 11:14

And no [thaumastos](../../strongs/g/g2298.md); for [Satanas](../../strongs/g/g4567.md) himself is [metaschēmatizō](../../strongs/g/g3345.md) into an [aggelos](../../strongs/g/g32.md) of [phōs](../../strongs/g/g5457.md).

<a name="2corinthians_11_15"></a>2 Corinthians 11:15

Therefore it is no [megas](../../strongs/g/g3173.md) if his [diakonos](../../strongs/g/g1249.md) also be [metaschēmatizō](../../strongs/g/g3345.md) as the [diakonos](../../strongs/g/g1249.md) of [dikaiosynē](../../strongs/g/g1343.md); whose [telos](../../strongs/g/g5056.md) shall be according to their [ergon](../../strongs/g/g2041.md).

<a name="2corinthians_11_16"></a>2 Corinthians 11:16

I [legō](../../strongs/g/g3004.md) again, Let no man [dokeō](../../strongs/g/g1380.md) me an [aphrōn](../../strongs/g/g878.md); if otherwise, yet as an [aphrōn](../../strongs/g/g878.md) [dechomai](../../strongs/g/g1209.md) me, that I may [kauchaomai](../../strongs/g/g2744.md) myself a [mikron](../../strongs/g/g3397.md).

<a name="2corinthians_11_17"></a>2 Corinthians 11:17

That which I [laleō](../../strongs/g/g2980.md), I [laleō](../../strongs/g/g2980.md) it not after the [kyrios](../../strongs/g/g2962.md), but as it were [aphrosynē](../../strongs/g/g877.md), in this [hypostasis](../../strongs/g/g5287.md) of [kauchēsis](../../strongs/g/g2746.md).

<a name="2corinthians_11_18"></a>2 Corinthians 11:18

[epei](../../strongs/g/g1893.md) [polys](../../strongs/g/g4183.md) [kauchaomai](../../strongs/g/g2744.md) after the [sarx](../../strongs/g/g4561.md), I will [kauchaomai](../../strongs/g/g2744.md) also.

<a name="2corinthians_11_19"></a>2 Corinthians 11:19

For ye [anechō](../../strongs/g/g430.md) [aphrōn](../../strongs/g/g878.md) [hēdeōs](../../strongs/g/g2234.md), seeing ye yourselves are [phronimos](../../strongs/g/g5429.md).

<a name="2corinthians_11_20"></a>2 Corinthians 11:20

For ye [anechō](../../strongs/g/g430.md), if a man [katadouloō](../../strongs/g/g2615.md) you, [ei tis](../../strongs/g/g1536.md) [katesthiō](../../strongs/g/g2719.md) you, [ei tis](../../strongs/g/g1536.md) [lambanō](../../strongs/g/g2983.md) you, [ei tis](../../strongs/g/g1536.md) [epairō](../../strongs/g/g1869.md), [ei tis](../../strongs/g/g1536.md) [derō](../../strongs/g/g1194.md) you on the [prosōpon](../../strongs/g/g4383.md).

<a name="2corinthians_11_21"></a>2 Corinthians 11:21

I [legō](../../strongs/g/g3004.md) as concerning [atimia](../../strongs/g/g819.md), as though we had been [astheneō](../../strongs/g/g770.md). Howbeit whereinsoever any is [tolmaō](../../strongs/g/g5111.md), (I [legō](../../strongs/g/g3004.md) [aphrosynē](../../strongs/g/g877.md),) I am [tolmaō](../../strongs/g/g5111.md) also.

<a name="2corinthians_11_22"></a>2 Corinthians 11:22

Are they [Hebraios](../../strongs/g/g1445.md)? so am I. Are they [Israēlitēs](../../strongs/g/g2475.md)? so am I. Are they the [sperma](../../strongs/g/g4690.md) of [Abraam](../../strongs/g/g11.md)? so am I.

<a name="2corinthians_11_23"></a>2 Corinthians 11:23

Are they [diakonos](../../strongs/g/g1249.md) of [Christos](../../strongs/g/g5547.md)? (I [laleō](../../strongs/g/g2980.md) as a [paraphroneō](../../strongs/g/g3912.md)) I am more; in [kopos](../../strongs/g/g2873.md) [perissoterōs](../../strongs/g/g4056.md), in [plēgē](../../strongs/g/g4127.md) [hyperballontōs](../../strongs/g/g5234.md), in [phylakē](../../strongs/g/g5438.md) [perissoterōs](../../strongs/g/g4056.md), in [thanatos](../../strongs/g/g2288.md) [pollakis](../../strongs/g/g4178.md).

<a name="2corinthians_11_24"></a>2 Corinthians 11:24

Of the [Ioudaios](../../strongs/g/g2453.md) five times [lambanō](../../strongs/g/g2983.md) I forty save one.

<a name="2corinthians_11_25"></a>2 Corinthians 11:25

Thrice was I [rhabdizō](../../strongs/g/g4463.md), [hapax](../../strongs/g/g530.md) was I [lithazō](../../strongs/g/g3034.md), thrice I [nauageō](../../strongs/g/g3489.md), [nychthēmeron](../../strongs/g/g3574.md) I have [poieō](../../strongs/g/g4160.md) in the [bythos](../../strongs/g/g1037.md);

<a name="2corinthians_11_26"></a>2 Corinthians 11:26

[hodoiporia](../../strongs/g/g3597.md) often, [kindynos](../../strongs/g/g2794.md) of [potamos](../../strongs/g/g4215.md), [kindynos](../../strongs/g/g2794.md) of [lēstēs](../../strongs/g/g3027.md), [kindynos](../../strongs/g/g2794.md) by [genos](../../strongs/g/g1085.md), [kindynos](../../strongs/g/g2794.md) by [ethnos](../../strongs/g/g1484.md), [kindynos](../../strongs/g/g2794.md) in the [polis](../../strongs/g/g4172.md), [kindynos](../../strongs/g/g2794.md) the [erēmia](../../strongs/g/g2047.md), [kindynos](../../strongs/g/g2794.md) the [thalassa](../../strongs/g/g2281.md), [kindynos](../../strongs/g/g2794.md) among [pseudadelphos](../../strongs/g/g5569.md);

<a name="2corinthians_11_27"></a>2 Corinthians 11:27

In [kopos](../../strongs/g/g2873.md) and [mochthos](../../strongs/g/g3449.md), in [agrypnia](../../strongs/g/g70.md) often, in [limos](../../strongs/g/g3042.md) and [dipsos](../../strongs/g/g1373.md), in [nēsteia](../../strongs/g/g3521.md) often, in [psychos](../../strongs/g/g5592.md) and [gymnotēs](../../strongs/g/g1132.md).

<a name="2corinthians_11_28"></a>2 Corinthians 11:28

Beside [parektos](../../strongs/g/g3924.md), that which [epistasis](../../strongs/g/g1999.md) me [hēmera](../../strongs/g/g2250.md), the [merimna](../../strongs/g/g3308.md) of all the [ekklēsia](../../strongs/g/g1577.md).

<a name="2corinthians_11_29"></a>2 Corinthians 11:29

Who is [astheneō](../../strongs/g/g770.md), and I am not [astheneō](../../strongs/g/g770.md)? who is [skandalizō](../../strongs/g/g4624.md), and I [pyroō](../../strongs/g/g4448.md) not?

<a name="2corinthians_11_30"></a>2 Corinthians 11:30

If I must needs [kauchaomai](../../strongs/g/g2744.md), I will [kauchaomai](../../strongs/g/g2744.md) of the things which concern mine [astheneia](../../strongs/g/g769.md).

<a name="2corinthians_11_31"></a>2 Corinthians 11:31

The [theos](../../strongs/g/g2316.md) and [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), which is [eulogētos](../../strongs/g/g2128.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md), [eidō](../../strongs/g/g1492.md) that I [pseudomai](../../strongs/g/g5574.md) not.

<a name="2corinthians_11_32"></a>2 Corinthians 11:32

In [Damaskos](../../strongs/g/g1154.md) the [ethnarchēs](../../strongs/g/g1481.md) under [aretas](../../strongs/g/g702.md) the [basileus](../../strongs/g/g935.md) [phroureō](../../strongs/g/g5432.md) the [polis](../../strongs/g/g4172.md) of the [damaskēnos](../../strongs/g/g1153.md) with a [phroureō](../../strongs/g/g5432.md), [thelō](../../strongs/g/g2309.md) to [piazō](../../strongs/g/g4084.md) me:

<a name="2corinthians_11_33"></a>2 Corinthians 11:33

And through a [thyris](../../strongs/g/g2376.md) in a [sarganē](../../strongs/g/g4553.md) was I [chalaō](../../strongs/g/g5465.md) by the [teichos](../../strongs/g/g5038.md), and [ekpheugō](../../strongs/g/g1628.md) his [cheir](../../strongs/g/g5495.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 10](2corinthians_1i0.md) - [2 Corinthians 12](2corinthians_12.md)