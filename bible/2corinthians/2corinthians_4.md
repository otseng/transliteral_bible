# [2 Corinthians 4](https://www.blueletterbible.org/kjv/2co/4/1/s_1082001)

<a name="2corinthians_4_1"></a>2 Corinthians 4:1

Therefore seeing we have this [diakonia](../../strongs/g/g1248.md), as we have [eleeō](../../strongs/g/g1653.md), we [ekkakeō](../../strongs/g/g1573.md) not;

<a name="2corinthians_4_2"></a>2 Corinthians 4:2

But have [apeipon](../../strongs/g/g550.md) the [kryptos](../../strongs/g/g2927.md) of [aischynē](../../strongs/g/g152.md), not [peripateō](../../strongs/g/g4043.md) in [panourgia](../../strongs/g/g3834.md), nor [doloō](../../strongs/g/g1389.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md); but by [phanerōsis](../../strongs/g/g5321.md) of the [alētheia](../../strongs/g/g225.md) [synistēmi](../../strongs/g/g4921.md) ourselves to every [anthrōpos](../../strongs/g/g444.md) [syneidēsis](../../strongs/g/g4893.md) [enōpion](../../strongs/g/g1799.md) [theos](../../strongs/g/g2316.md).

<a name="2corinthians_4_3"></a>2 Corinthians 4:3

But if our [euaggelion](../../strongs/g/g2098.md) be [kalyptō](../../strongs/g/g2572.md), it is [kalyptō](../../strongs/g/g2572.md) to [apollymi](../../strongs/g/g622.md):

<a name="2corinthians_4_4"></a>2 Corinthians 4:4

In whom the [theos](../../strongs/g/g2316.md) of this [aiōn](../../strongs/g/g165.md) hath [typhloō](../../strongs/g/g5186.md) the [noēma](../../strongs/g/g3540.md) of [apistos](../../strongs/g/g571.md), lest the [phōtismos](../../strongs/g/g5462.md) of the [doxa](../../strongs/g/g1391.md) [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md), who is the [eikōn](../../strongs/g/g1504.md) of [theos](../../strongs/g/g2316.md), should [augazō](../../strongs/g/g826.md) unto them.

<a name="2corinthians_4_5"></a>2 Corinthians 4:5

For we [kēryssō](../../strongs/g/g2784.md) not ourselves, but [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) the [kyrios](../../strongs/g/g2962.md); and ourselves your [doulos](../../strongs/g/g1401.md) for [Iēsous](../../strongs/g/g2424.md) sake.

<a name="2corinthians_4_6"></a>2 Corinthians 4:6

For [theos](../../strongs/g/g2316.md), who [eipon](../../strongs/g/g2036.md) the [phōs](../../strongs/g/g5457.md) to [lampō](../../strongs/g/g2989.md) out of [skotos](../../strongs/g/g4655.md), hath [lampō](../../strongs/g/g2989.md) in our [kardia](../../strongs/g/g2588.md), to [phōtismos](../../strongs/g/g5462.md) of the [gnōsis](../../strongs/g/g1108.md) of the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md) in the [prosōpon](../../strongs/g/g4383.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2corinthians_4_7"></a>2 Corinthians 4:7

But we have this [thēsauros](../../strongs/g/g2344.md) in [ostrakinos](../../strongs/g/g3749.md) [skeuos](../../strongs/g/g4632.md), that the [hyperbolē](../../strongs/g/g5236.md) of the [dynamis](../../strongs/g/g1411.md) may be of [theos](../../strongs/g/g2316.md), and not of us.

<a name="2corinthians_4_8"></a>2 Corinthians 4:8

We are [thlibō](../../strongs/g/g2346.md) on every side, yet not [stenochōreō](../../strongs/g/g4729.md); we are [aporeō](../../strongs/g/g639.md), but not in [exaporeō](../../strongs/g/g1820.md);

<a name="2corinthians_4_9"></a>2 Corinthians 4:9

[diōkō](../../strongs/g/g1377.md), but not [egkataleipō](../../strongs/g/g1459.md); [kataballō](../../strongs/g/g2598.md), but not [apollymi](../../strongs/g/g622.md);

<a name="2corinthians_4_10"></a>2 Corinthians 4:10

[pantote](../../strongs/g/g3842.md) [peripherō](../../strongs/g/g4064.md) in the [sōma](../../strongs/g/g4983.md) the [nekrōsis](../../strongs/g/g3500.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), that the [zōē](../../strongs/g/g2222.md) also of [Iēsous](../../strongs/g/g2424.md) might be [phaneroō](../../strongs/g/g5319.md) in our [sōma](../../strongs/g/g4983.md).

<a name="2corinthians_4_11"></a>2 Corinthians 4:11

For we which [zaō](../../strongs/g/g2198.md) are alway [paradidōmi](../../strongs/g/g3860.md) unto [thanatos](../../strongs/g/g2288.md) for [Iēsous](../../strongs/g/g2424.md) sake, that the [zōē](../../strongs/g/g2222.md) also of [Iēsous](../../strongs/g/g2424.md) might be [phaneroō](../../strongs/g/g5319.md) in our [thnētos](../../strongs/g/g2349.md) [sarx](../../strongs/g/g4561.md).

<a name="2corinthians_4_12"></a>2 Corinthians 4:12

So then [thanatos](../../strongs/g/g2288.md) [energeō](../../strongs/g/g1754.md) in us, but [zōē](../../strongs/g/g2222.md) in you.

<a name="2corinthians_4_13"></a>2 Corinthians 4:13

We having the same [pneuma](../../strongs/g/g4151.md) of [pistis](../../strongs/g/g4102.md), according as it is [graphō](../../strongs/g/g1125.md), I [pisteuō](../../strongs/g/g4100.md), and therefore have I [laleō](../../strongs/g/g2980.md); we also [pisteuō](../../strongs/g/g4100.md), and therefore [laleō](../../strongs/g/g2980.md);

<a name="2corinthians_4_14"></a>2 Corinthians 4:14

[eidō](../../strongs/g/g1492.md) that he which [egeirō](../../strongs/g/g1453.md) the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) shall [egeirō](../../strongs/g/g1453.md) us also by [Iēsous](../../strongs/g/g2424.md), and shall [paristēmi](../../strongs/g/g3936.md) us with you.

<a name="2corinthians_4_15"></a>2 Corinthians 4:15

For all things for your sakes, that the [pleonazō](../../strongs/g/g4121.md) [charis](../../strongs/g/g5485.md) might through the [eucharistia](../../strongs/g/g2169.md) of many [perisseuō](../../strongs/g/g4052.md) to the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md).

<a name="2corinthians_4_16"></a>2 Corinthians 4:16

[dio](../../strongs/g/g1352.md) we [ekkakeō](../../strongs/g/g1573.md) not; but though our [exō](../../strongs/g/g1854.md) [anthrōpos](../../strongs/g/g444.md) [diaphtheirō](../../strongs/g/g1311.md), yet the [esōthen](../../strongs/g/g2081.md) is [anakainoō](../../strongs/g/g341.md) day by day.

<a name="2corinthians_4_17"></a>2 Corinthians 4:17

For our [elaphros](../../strongs/g/g1645.md) [thlipsis](../../strongs/g/g2347.md), which is [parautika](../../strongs/g/g3910.md), [katergazomai](../../strongs/g/g2716.md) for us a [hyperbolē](../../strongs/g/g5236.md) [eis](../../strongs/g/g1519.md) [hyperbolē](../../strongs/g/g5236.md) and [aiōnios](../../strongs/g/g166.md) [baros](../../strongs/g/g922.md) of [doxa](../../strongs/g/g1391.md);

<a name="2corinthians_4_18"></a>2 Corinthians 4:18

While we [skopeō](../../strongs/g/g4648.md) not at [blepō](../../strongs/g/g991.md), but at [blepō](../../strongs/g/g991.md) not: for the [blepō](../../strongs/g/g991.md) are [proskairos](../../strongs/g/g4340.md); but the [blepō](../../strongs/g/g991.md) not seen are [aiōnios](../../strongs/g/g166.md).

---

[Transliteral Bible](../bible.md)

[2Corinthians](2corinthians.md)

[2Corinthians 3](2corinthians_3.md) - [2Corinthians 5](2corinthians_5.md)