# [2 Corinthians 5](https://www.blueletterbible.org/kjv/2co/5/1/s_1083001)

<a name="2corinthians_5_1"></a>2 Corinthians 5:1

For we [eidō](../../strongs/g/g1492.md) that if our [epigeios](../../strongs/g/g1919.md) [oikia](../../strongs/g/g3614.md) of this [skēnos](../../strongs/g/g4636.md) were [katalyō](../../strongs/g/g2647.md), we have a [oikodomē](../../strongs/g/g3619.md) of [theos](../../strongs/g/g2316.md), an [oikia](../../strongs/g/g3614.md) [acheiropoiētos](../../strongs/g/g886.md), [aiōnios](../../strongs/g/g166.md) in the [ouranos](../../strongs/g/g3772.md).

<a name="2corinthians_5_2"></a>2 Corinthians 5:2

For in this we [stenazō](../../strongs/g/g4727.md), [epipotheo](../../strongs/g/g1971.md) to [ependyomai](../../strongs/g/g1902.md) with our [oikētērion](../../strongs/g/g3613.md) which is from [ouranos](../../strongs/g/g3772.md):

<a name="2corinthians_5_3"></a>2 Corinthians 5:3

If so be that being [endyō](../../strongs/g/g1746.md) we shall not be [heuriskō](../../strongs/g/g2147.md) [gymnos](../../strongs/g/g1131.md).

<a name="2corinthians_5_4"></a>2 Corinthians 5:4

For we that are in [skēnos](../../strongs/g/g4636.md) do [stenazō](../../strongs/g/g4727.md), being [bareō](../../strongs/g/g916.md): not for that we would be [ekdyō](../../strongs/g/g1562.md), but [ependyomai](../../strongs/g/g1902.md), that [thnētos](../../strongs/g/g2349.md) might be [katapinō](../../strongs/g/g2666.md) of [zōē](../../strongs/g/g2222.md).

<a name="2corinthians_5_5"></a>2 Corinthians 5:5

Now he that hath [katergazomai](../../strongs/g/g2716.md) us for the selfsame thing is [theos](../../strongs/g/g2316.md), who also hath [didōmi](../../strongs/g/g1325.md) unto us the [arrabōn](../../strongs/g/g728.md) of the [pneuma](../../strongs/g/g4151.md).

<a name="2corinthians_5_6"></a>2 Corinthians 5:6

Therefore we are [pantote](../../strongs/g/g3842.md) [tharreō](../../strongs/g/g2292.md), [eidō](../../strongs/g/g1492.md) that, whilst we are [endēmeō](../../strongs/g/g1736.md) in the [sōma](../../strongs/g/g4983.md), we are [ekdēmeō](../../strongs/g/g1553.md) from the [kyrios](../../strongs/g/g2962.md):

<a name="2corinthians_5_7"></a>2 Corinthians 5:7

(For we [peripateō](../../strongs/g/g4043.md) by [pistis](../../strongs/g/g4102.md), not by [eidos](../../strongs/g/g1491.md):)

<a name="2corinthians_5_8"></a>2 Corinthians 5:8

We are [tharreō](../../strongs/g/g2292.md), and [eudokeō](../../strongs/g/g2106.md) rather to be [ekdēmeō](../../strongs/g/g1553.md) from the [sōma](../../strongs/g/g4983.md), and to be [endēmeō](../../strongs/g/g1736.md) with the [kyrios](../../strongs/g/g2962.md).

<a name="2corinthians_5_9"></a>2 Corinthians 5:9

Wherefore we [philotimeomai](../../strongs/g/g5389.md), that, whether [endēmeō](../../strongs/g/g1736.md) or [ekdēmeō](../../strongs/g/g1553.md), we may be [euarestos](../../strongs/g/g2101.md) of him.

<a name="2corinthians_5_10"></a>2 Corinthians 5:10

For we must all [phaneroō](../../strongs/g/g5319.md) before the [bēma](../../strongs/g/g968.md) of [Christos](../../strongs/g/g5547.md); that every one may [komizō](../../strongs/g/g2865.md) the things done in his [sōma](../../strongs/g/g4983.md), according to that he hath [prassō](../../strongs/g/g4238.md), whether it be [agathos](../../strongs/g/g18.md) or [kakos](../../strongs/g/g2556.md).

<a name="2corinthians_5_11"></a>2 Corinthians 5:11

[eidō](../../strongs/g/g1492.md) therefore the [phobos](../../strongs/g/g5401.md) of the [kyrios](../../strongs/g/g2962.md), we [peithō](../../strongs/g/g3982.md) [anthrōpos](../../strongs/g/g444.md); but we are [phaneroō](../../strongs/g/g5319.md) unto [theos](../../strongs/g/g2316.md); and I [elpizō](../../strongs/g/g1679.md) also are [phaneroō](../../strongs/g/g5319.md) in your [syneidēsis](../../strongs/g/g4893.md).

<a name="2corinthians_5_12"></a>2 Corinthians 5:12

For we [synistēmi](../../strongs/g/g4921.md) not ourselves again unto you, but [didōmi](../../strongs/g/g1325.md) you [aphormē](../../strongs/g/g874.md) to [kauchēma](../../strongs/g/g2745.md) on our behalf, that ye may have somewhat to [kauchaomai](../../strongs/g/g2744.md) in [prosōpon](../../strongs/g/g4383.md), and not in [kardia](../../strongs/g/g2588.md).

<a name="2corinthians_5_13"></a>2 Corinthians 5:13

For whether we [existēmi](../../strongs/g/g1839.md), it is to [theos](../../strongs/g/g2316.md): or whether we be [sōphroneō](../../strongs/g/g4993.md), it is for your cause.

<a name="2corinthians_5_14"></a>2 Corinthians 5:14

For the [agapē](../../strongs/g/g26.md) of [Christos](../../strongs/g/g5547.md) [synechō](../../strongs/g/g4912.md) us; because we thus [krinō](../../strongs/g/g2919.md), that if one [apothnēskō](../../strongs/g/g599.md) for all, then were all [apothnēskō](../../strongs/g/g599.md):

<a name="2corinthians_5_15"></a>2 Corinthians 5:15

And that he [apothnēskō](../../strongs/g/g599.md) for all, that they which [zaō](../../strongs/g/g2198.md) should not henceforth [zaō](../../strongs/g/g2198.md) unto themselves, but unto him which [apothnēskō](../../strongs/g/g599.md) for them, and [egeirō](../../strongs/g/g1453.md).

<a name="2corinthians_5_16"></a>2 Corinthians 5:16

Wherefore henceforth [eidō](../../strongs/g/g1492.md) we no man after the [sarx](../../strongs/g/g4561.md): yea, though we have [ginōskō](../../strongs/g/g1097.md) [Christos](../../strongs/g/g5547.md) after the [sarx](../../strongs/g/g4561.md), yet now henceforth [ginōskō](../../strongs/g/g1097.md) we him no more.

<a name="2corinthians_5_17"></a>2 Corinthians 5:17

Therefore if any in [Christos](../../strongs/g/g5547.md), he is a [kainos](../../strongs/g/g2537.md) [ktisis](../../strongs/g/g2937.md): [archaios](../../strongs/g/g744.md) are [parerchomai](../../strongs/g/g3928.md); [idou](../../strongs/g/g2400.md), all things are become [kainos](../../strongs/g/g2537.md).

<a name="2corinthians_5_18"></a>2 Corinthians 5:18

And all things are of [theos](../../strongs/g/g2316.md), who hath [katallassō](../../strongs/g/g2644.md) us to himself by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and hath [didōmi](../../strongs/g/g1325.md) to us the [diakonia](../../strongs/g/g1248.md) of [katallagē](../../strongs/g/g2643.md);

<a name="2corinthians_5_19"></a>2 Corinthians 5:19

To wit, that [theos](../../strongs/g/g2316.md) was in [Christos](../../strongs/g/g5547.md), [katallassō](../../strongs/g/g2644.md) the [kosmos](../../strongs/g/g2889.md) unto himself, not [logizomai](../../strongs/g/g3049.md) their [paraptōma](../../strongs/g/g3900.md) unto them; and hath [tithēmi](../../strongs/g/g5087.md) unto us the [logos](../../strongs/g/g3056.md) of [katallagē](../../strongs/g/g2643.md).

<a name="2corinthians_5_20"></a>2 Corinthians 5:20

Now then we are [presbeuō](../../strongs/g/g4243.md) for [Christos](../../strongs/g/g5547.md), as though [theos](../../strongs/g/g2316.md) did [parakaleō](../../strongs/g/g3870.md) you by us: we [deomai](../../strongs/g/g1189.md) you in [Christos](../../strongs/g/g5547.md) stead, be ye [katallassō](../../strongs/g/g2644.md) to [theos](../../strongs/g/g2316.md).

<a name="2corinthians_5_21"></a>2 Corinthians 5:21

For he hath [poieō](../../strongs/g/g4160.md) him to be [hamartia](../../strongs/g/g266.md) for us, who [ginōskō](../../strongs/g/g1097.md) no [hamartia](../../strongs/g/g266.md); that we might be [ginomai](../../strongs/g/g1096.md) the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md) in him.

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 4](2corinthians_4.md) - [2 Corinthians 6](2corinthians_6.md)