# [2 Corinthians 12](https://www.blueletterbible.org/kjv/2co/12/1/s_1090001)

<a name="2corinthians_12_1"></a>2 Corinthians 12:1

It is not [sympherō](../../strongs/g/g4851.md) for me [dē](../../strongs/g/g1211.md) to [kauchaomai](../../strongs/g/g2744.md). I will [erchomai](../../strongs/g/g2064.md) to [optasia](../../strongs/g/g3701.md) and [apokalypsis](../../strongs/g/g602.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="2corinthians_12_2"></a>2 Corinthians 12:2

I [eidō](../../strongs/g/g1492.md) an [anthrōpos](../../strongs/g/g444.md) in [Christos](../../strongs/g/g5547.md) above fourteen [etos](../../strongs/g/g2094.md) ago, (whether in the [sōma](../../strongs/g/g4983.md), I cannot [eidō](../../strongs/g/g1492.md); or whether [ektos](../../strongs/g/g1622.md) of the [sōma](../../strongs/g/g4983.md), I cannot [eidō](../../strongs/g/g1492.md): [theos](../../strongs/g/g2316.md) [eidō](../../strongs/g/g1492.md);) such an one [harpazō](../../strongs/g/g726.md) to the [tritos](../../strongs/g/g5154.md) [ouranos](../../strongs/g/g3772.md).

<a name="2corinthians_12_3"></a>2 Corinthians 12:3

And I [eidō](../../strongs/g/g1492.md) such an [anthrōpos](../../strongs/g/g444.md), (whether in the [sōma](../../strongs/g/g4983.md), or out of the [sōma](../../strongs/g/g4983.md), I cannot [eidō](../../strongs/g/g1492.md): [theos](../../strongs/g/g2316.md) [eidō](../../strongs/g/g1492.md);)

<a name="2corinthians_12_4"></a>2 Corinthians 12:4

How that he was [harpazō](../../strongs/g/g726.md) into [paradeisos](../../strongs/g/g3857.md), and [akouō](../../strongs/g/g191.md) [arrētos](../../strongs/g/g731.md) [rhēma](../../strongs/g/g4487.md), which it is not [exesti](../../strongs/g/g1832.md) for an [anthrōpos](../../strongs/g/g444.md) to [laleō](../../strongs/g/g2980.md).

<a name="2corinthians_12_5"></a>2 Corinthians 12:5

Of such an one will I [kauchaomai](../../strongs/g/g2744.md): yet of myself I will not [kauchaomai](../../strongs/g/g2744.md), but in mine [astheneia](../../strongs/g/g769.md).

<a name="2corinthians_12_6"></a>2 Corinthians 12:6

For though I would [thelō](../../strongs/g/g2309.md) to [kauchaomai](../../strongs/g/g2744.md), I shall not be an [aphrōn](../../strongs/g/g878.md); for I will [eipon](../../strongs/g/g2046.md) the [alētheia](../../strongs/g/g225.md): but now I [pheidomai](../../strongs/g/g5339.md), lest any man should [logizomai](../../strongs/g/g3049.md) of me above that which he [blepō](../../strongs/g/g991.md) me to be, or that he [akouō](../../strongs/g/g191.md) of me.

<a name="2corinthians_12_7"></a>2 Corinthians 12:7

And lest I should be [hyperairō](../../strongs/g/g5229.md) through the [hyperbolē](../../strongs/g/g5236.md) of the [apokalypsis](../../strongs/g/g602.md), there was [didōmi](../../strongs/g/g1325.md) to me a [skolops](../../strongs/g/g4647.md) in the [sarx](../../strongs/g/g4561.md), the [aggelos](../../strongs/g/g32.md) of [Satan](../../strongs/g/g4566.md) to [kolaphizō](../../strongs/g/g2852.md) me, lest I should be [hyperairō](../../strongs/g/g5229.md).

<a name="2corinthians_12_8"></a>2 Corinthians 12:8

For this thing I [parakaleō](../../strongs/g/g3870.md) the [kyrios](../../strongs/g/g2962.md) thrice, that it might [aphistēmi](../../strongs/g/g868.md) from me.

<a name="2corinthians_12_9"></a>2 Corinthians 12:9

And he [eipon](../../strongs/g/g2046.md) unto me, My [charis](../../strongs/g/g5485.md) is [arkeō](../../strongs/g/g714.md) for thee: for my [dynamis](../../strongs/g/g1411.md) is [teleioō](../../strongs/g/g5048.md) in [astheneia](../../strongs/g/g769.md). [hēdista](../../strongs/g/g2236.md) therefore will I rather [kauchaomai](../../strongs/g/g2744.md) in my [astheneia](../../strongs/g/g769.md), that the [dynamis](../../strongs/g/g1411.md) of [Christos](../../strongs/g/g5547.md) may [episkēnoō](../../strongs/g/g1981.md) upon me.

<a name="2corinthians_12_10"></a>2 Corinthians 12:10

Therefore I [eudokeō](../../strongs/g/g2106.md) in [astheneia](../../strongs/g/g769.md), in [hybris](../../strongs/g/g5196.md), in [anagkē](../../strongs/g/g318.md), in [diōgmos](../../strongs/g/g1375.md), in [stenochoria](../../strongs/g/g4730.md) for [Christos](../../strongs/g/g5547.md) sake: for when I am [astheneō](../../strongs/g/g770.md), then am I [dynatos](../../strongs/g/g1415.md).

<a name="2corinthians_12_11"></a>2 Corinthians 12:11

I am become an [aphrōn](../../strongs/g/g878.md) in [kauchaomai](../../strongs/g/g2744.md); ye have [anagkazō](../../strongs/g/g315.md) me: for I [opheilō](../../strongs/g/g3784.md) to have been [synistēmi](../../strongs/g/g4921.md) of you: for in nothing am I [hystereō](../../strongs/g/g5302.md) the [lian](../../strongs/g/g3029.md) [apostolos](../../strongs/g/g652.md), though I be nothing.

<a name="2corinthians_12_12"></a>2 Corinthians 12:12

[men](../../strongs/g/g3303.md) the [sēmeion](../../strongs/g/g4592.md) of an [apostolos](../../strongs/g/g652.md) were [katergazomai](../../strongs/g/g2716.md) among you in all [hypomonē](../../strongs/g/g5281.md), in [sēmeion](../../strongs/g/g4592.md), and [teras](../../strongs/g/g5059.md), and [dynamis](../../strongs/g/g1411.md).

<a name="2corinthians_12_13"></a>2 Corinthians 12:13

For what is it wherein ye were [hēttaomai](../../strongs/g/g2274.md) to [loipos](../../strongs/g/g3062.md) [ekklēsia](../../strongs/g/g1577.md), except it be that I myself was not [katanarkaō](../../strongs/g/g2655.md) to you? [charizomai](../../strongs/g/g5483.md) me this [adikia](../../strongs/g/g93.md).

<a name="2corinthians_12_14"></a>2 Corinthians 12:14

[idou](../../strongs/g/g2400.md), the third time I am [hetoimōs](../../strongs/g/g2093.md) to [erchomai](../../strongs/g/g2064.md) to you; and I will not be [katanarkaō](../../strongs/g/g2655.md) to you: for I [zēteō](../../strongs/g/g2212.md) not your's but you: for the [teknon](../../strongs/g/g5043.md) [opheilō](../../strongs/g/g3784.md) not to [thēsaurizō](../../strongs/g/g2343.md) for the [goneus](../../strongs/g/g1118.md), but the [goneus](../../strongs/g/g1118.md) for the [teknon](../../strongs/g/g5043.md).

<a name="2corinthians_12_15"></a>2 Corinthians 12:15

And I will [hēdista](../../strongs/g/g2236.md) [dapanaō](../../strongs/g/g1159.md) and [ekdapanaō](../../strongs/g/g1550.md) for [psychē](../../strongs/g/g5590.md) [hymōn](../../strongs/g/g5216.md); though the [perissoterōs](../../strongs/g/g4056.md) I [agapaō](../../strongs/g/g25.md) you, the [hēssōn](../../strongs/g/g2276.md) I be [agapaō](../../strongs/g/g25.md).

<a name="2corinthians_12_16"></a>2 Corinthians 12:16

But be it so, I did not [katabareō](../../strongs/g/g2599.md) you: nevertheless, being [panourgos](../../strongs/g/g3835.md), I [lambanō](../../strongs/g/g2983.md) you with [dolos](../../strongs/g/g1388.md).

<a name="2corinthians_12_17"></a>2 Corinthians 12:17

Did I make a [pleonekteō](../../strongs/g/g4122.md) of you by any of them whom I [apostellō](../../strongs/g/g649.md) unto you?

<a name="2corinthians_12_18"></a>2 Corinthians 12:18

I [parakaleō](../../strongs/g/g3870.md) [titos](../../strongs/g/g5103.md), and I [synapostellō](../../strongs/g/g4882.md) an [adelphos](../../strongs/g/g80.md). Did [titos](../../strongs/g/g5103.md) [pleonekteō](../../strongs/g/g4122.md) of you? [peripateō](../../strongs/g/g4043.md) we not in the same [pneuma](../../strongs/g/g4151.md)? not in the same [ichnos](../../strongs/g/g2487.md)?

<a name="2corinthians_12_19"></a>2 Corinthians 12:19

Again, [dokeō](../../strongs/g/g1380.md) ye that we [apologeomai](../../strongs/g/g626.md) unto you? we [laleō](../../strongs/g/g2980.md) [katenōpion](../../strongs/g/g2714.md) [theos](../../strongs/g/g2316.md) in [Christos](../../strongs/g/g5547.md): but we do all things, [agapētos](../../strongs/g/g27.md), for your [oikodomē](../../strongs/g/g3619.md).

<a name="2corinthians_12_20"></a>2 Corinthians 12:20

For I [phobeō](../../strongs/g/g5399.md), lest, when I [erchomai](../../strongs/g/g2064.md), I shall not [heuriskō](../../strongs/g/g2147.md) you such as I would, and that I shall be [heuriskō](../../strongs/g/g2147.md) unto you such as ye would not: lest there be [eris](../../strongs/g/g2054.md), [zēlos](../../strongs/g/g2205.md), [thymos](../../strongs/g/g2372.md), [eritheia](../../strongs/g/g2052.md), [katalalia](../../strongs/g/g2636.md), [psithyrismos](../../strongs/g/g5587.md), [physiōsis](../../strongs/g/g5450.md), [akatastasia](../../strongs/g/g181.md):

<a name="2corinthians_12_21"></a>2 Corinthians 12:21

And lest, when I [erchomai](../../strongs/g/g2064.md) again, my [theos](../../strongs/g/g2316.md) will [tapeinoō](../../strongs/g/g5013.md) me among you, and that I shall [pentheō](../../strongs/g/g3996.md) [polys](../../strongs/g/g4183.md) which have [proamartanō](../../strongs/g/g4258.md), and have not [metanoeō](../../strongs/g/g3340.md) of the [akatharsia](../../strongs/g/g167.md) and [porneia](../../strongs/g/g4202.md) and [aselgeia](../../strongs/g/g766.md) which they have [prassō](../../strongs/g/g4238.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 11](2corinthians_11.md) - [2 Corinthians 13](2corinthians_13.md)