# [2 Corinthians 6](https://www.blueletterbible.org/kjv/2co/6/1/s_1084001)

<a name="2corinthians_6_1"></a>2 Corinthians 6:1

We then, [synergeō](../../strongs/g/g4903.md), [parakaleō](../../strongs/g/g3870.md) also that ye [dechomai](../../strongs/g/g1209.md) not the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) in [kenos](../../strongs/g/g2756.md).

<a name="2corinthians_6_2"></a>2 Corinthians 6:2

(For he [legō](../../strongs/g/g3004.md), I have [epakouō](../../strongs/g/g1873.md) thee in a [kairos](../../strongs/g/g2540.md) [dektos](../../strongs/g/g1184.md), and in the [hēmera](../../strongs/g/g2250.md) of [sōtēria](../../strongs/g/g4991.md) have I [boētheō](../../strongs/g/g997.md) thee: [idou](../../strongs/g/g2400.md), now is the [euprosdektos](../../strongs/g/g2144.md) [kairos](../../strongs/g/g2540.md); [idou](../../strongs/g/g2400.md), now is the [hēmera](../../strongs/g/g2250.md) of [sōtēria](../../strongs/g/g4991.md).)

<a name="2corinthians_6_3"></a>2 Corinthians 6:3

[didōmi](../../strongs/g/g1325.md) [proskopē](../../strongs/g/g4349.md) in [mēdeis](../../strongs/g/g3367.md), that the [diakonia](../../strongs/g/g1248.md) be not [mōmaomai](../../strongs/g/g3469.md):

<a name="2corinthians_6_4"></a>2 Corinthians 6:4

But in all [synistēmi](../../strongs/g/g4921.md) ourselves as the [diakonos](../../strongs/g/g1249.md) of [theos](../../strongs/g/g2316.md), in [polys](../../strongs/g/g4183.md) [hypomonē](../../strongs/g/g5281.md), in [thlipsis](../../strongs/g/g2347.md), in [anagkē](../../strongs/g/g318.md), in [stenochoria](../../strongs/g/g4730.md),

<a name="2corinthians_6_5"></a>2 Corinthians 6:5

In [plēgē](../../strongs/g/g4127.md), in [phylakē](../../strongs/g/g5438.md), in [akatastasia](../../strongs/g/g181.md), in [kopos](../../strongs/g/g2873.md), in [agrypnia](../../strongs/g/g70.md), in [nēsteia](../../strongs/g/g3521.md);

<a name="2corinthians_6_6"></a>2 Corinthians 6:6

By [hagnotēs](../../strongs/g/g54.md), by [gnōsis](../../strongs/g/g1108.md), by [makrothymia](../../strongs/g/g3115.md), by [chrēstotēs](../../strongs/g/g5544.md), by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), by [agapē](../../strongs/g/g26.md) [anypokritos](../../strongs/g/g505.md),

<a name="2corinthians_6_7"></a>2 Corinthians 6:7

By the [logos](../../strongs/g/g3056.md) of [alētheia](../../strongs/g/g225.md), by the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md), by the [hoplon](../../strongs/g/g3696.md) of [dikaiosynē](../../strongs/g/g1343.md) on the [dexios](../../strongs/g/g1188.md) and on the [aristeros](../../strongs/g/g710.md),

<a name="2corinthians_6_8"></a>2 Corinthians 6:8

By [doxa](../../strongs/g/g1391.md) and [atimia](../../strongs/g/g819.md), by [dysphēmia](../../strongs/g/g1426.md) and [euphēmia](../../strongs/g/g2162.md): as [planos](../../strongs/g/g4108.md), and [alēthēs](../../strongs/g/g227.md);

<a name="2corinthians_6_9"></a>2 Corinthians 6:9

As [agnoeō](../../strongs/g/g50.md), and [epiginōskō](../../strongs/g/g1921.md); as [apothnēskō](../../strongs/g/g599.md), and, [idou](../../strongs/g/g2400.md), we [zaō](../../strongs/g/g2198.md); as [paideuō](../../strongs/g/g3811.md), and not [thanatoō](../../strongs/g/g2289.md);

<a name="2corinthians_6_10"></a>2 Corinthians 6:10

As [lypeō](../../strongs/g/g3076.md), yet alway [chairō](../../strongs/g/g5463.md); as [ptōchos](../../strongs/g/g4434.md), yet [ploutizō](../../strongs/g/g4148.md) [polys](../../strongs/g/g4183.md); as having [mēdeis](../../strongs/g/g3367.md), and [katechō](../../strongs/g/g2722.md) all things.

<a name="2corinthians_6_11"></a>2 Corinthians 6:11

[Korinthios](../../strongs/g/g2881.md), our [stoma](../../strongs/g/g4750.md) is [anoigō](../../strongs/g/g455.md) unto you, our [kardia](../../strongs/g/g2588.md) is [platynō](../../strongs/g/g4115.md).

<a name="2corinthians_6_12"></a>2 Corinthians 6:12

Ye are not [stenochōreō](../../strongs/g/g4729.md) in us, but ye are [stenochōreō](../../strongs/g/g4729.md) in your own [splagchnon](../../strongs/g/g4698.md).

<a name="2corinthians_6_13"></a>2 Corinthians 6:13

Now for an [antimisthia](../../strongs/g/g489.md) in the same, (I [legō](../../strongs/g/g3004.md) as unto my [teknon](../../strongs/g/g5043.md),) be ye also [platynō](../../strongs/g/g4115.md).

<a name="2corinthians_6_14"></a>2 Corinthians 6:14

Be ye not [heterozygeō](../../strongs/g/g2086.md) with [apistos](../../strongs/g/g571.md): for what [metochē](../../strongs/g/g3352.md) hath [dikaiosynē](../../strongs/g/g1343.md) with [anomia](../../strongs/g/g458.md)? and what [koinōnia](../../strongs/g/g2842.md) hath [phōs](../../strongs/g/g5457.md) with [skotos](../../strongs/g/g4655.md)?

<a name="2corinthians_6_15"></a>2 Corinthians 6:15

And what [symphōnēsis](../../strongs/g/g4857.md) hath [Christos](../../strongs/g/g5547.md) with [belial](../../strongs/g/g955.md)? or what [meris](../../strongs/g/g3310.md) hath [pistos](../../strongs/g/g4103.md) with [apistos](../../strongs/g/g571.md)?

<a name="2corinthians_6_16"></a>2 Corinthians 6:16

And what [sygkatathesis](../../strongs/g/g4783.md) hath the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md) with [eidōlon](../../strongs/g/g1497.md)? for ye are the [naos](../../strongs/g/g3485.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md); as [theos](../../strongs/g/g2316.md) hath [eipon](../../strongs/g/g2036.md), I will [enoikeō](../../strongs/g/g1774.md) in them, and [emperipateō](../../strongs/g/g1704.md) in them; and I will be their [theos](../../strongs/g/g2316.md), and they shall be my [laos](../../strongs/g/g2992.md).

<a name="2corinthians_6_17"></a>2 Corinthians 6:17

Wherefore [exerchomai](../../strongs/g/g1831.md) from among them, and be ye [aphorizō](../../strongs/g/g873.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), and [haptomai](../../strongs/g/g680.md) not the [akathartos](../../strongs/g/g169.md) thing; and I will [eisdechomai](../../strongs/g/g1523.md) you.

<a name="2corinthians_6_18"></a>2 Corinthians 6:18

And will be a [patēr](../../strongs/g/g3962.md) unto you, and ye shall be my [huios](../../strongs/g/g5207.md) and [thygatēr](../../strongs/g/g2364.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md) [pantokratōr](../../strongs/g/g3841.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 5](2corinthians_5.md) - [2 Corinthians 7](2corinthians_7.md)