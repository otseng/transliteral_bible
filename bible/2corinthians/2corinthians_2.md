# [2 Corinthians 2](https://www.blueletterbible.org/kjv/2co/2/1/s_1080001)

<a name="2corinthians_2_1"></a>2 Corinthians 2:1

But I [krinō](../../strongs/g/g2919.md) this with myself, that I would not [erchomai](../../strongs/g/g2064.md) again to you in [lypē](../../strongs/g/g3077.md).

<a name="2corinthians_2_2"></a>2 Corinthians 2:2

For if I [lypeō](../../strongs/g/g3076.md) you, who is he then that [euphrainō](../../strongs/g/g2165.md) me, but [lypeō](../../strongs/g/g3076.md) by me?

<a name="2corinthians_2_3"></a>2 Corinthians 2:3

And I [graphō](../../strongs/g/g1125.md) this same unto you, lest, when I [erchomai](../../strongs/g/g2064.md), I should have [lypē](../../strongs/g/g3077.md) from them of whom I ought to [chairō](../../strongs/g/g5463.md); [peithō](../../strongs/g/g3982.md) in you all, that my [chara](../../strongs/g/g5479.md) is of you all.

<a name="2corinthians_2_4"></a>2 Corinthians 2:4

For out of [polys](../../strongs/g/g4183.md) [thlipsis](../../strongs/g/g2347.md) and [synochē](../../strongs/g/g4928.md) of [kardia](../../strongs/g/g2588.md) I [graphō](../../strongs/g/g1125.md) unto you with [polys](../../strongs/g/g4183.md) [dakry](../../strongs/g/g1144.md); not that ye should be [lypeō](../../strongs/g/g3076.md), but that ye might [ginōskō](../../strongs/g/g1097.md) the [agapē](../../strongs/g/g26.md) which I have [perissoterōs](../../strongs/g/g4056.md) unto you.

<a name="2corinthians_2_5"></a>2 Corinthians 2:5

But if any have [lypeō](../../strongs/g/g3076.md), he hath not [lypeō](../../strongs/g/g3076.md) me, but in [meros](../../strongs/g/g3313.md): that I may not [epibareō](../../strongs/g/g1912.md) you all.

<a name="2corinthians_2_6"></a>2 Corinthians 2:6

[hikanos](../../strongs/g/g2425.md) to [toioutos](../../strongs/g/g5108.md) is this [epitimia](../../strongs/g/g2009.md), which was of many.

<a name="2corinthians_2_7"></a>2 Corinthians 2:7

So that [tounantion](../../strongs/g/g5121.md) ye rather to [charizomai](../../strongs/g/g5483.md) him, and [parakaleō](../../strongs/g/g3870.md) him, lest perhaps such a one should be [katapinō](../../strongs/g/g2666.md) with [perissoteros](../../strongs/g/g4055.md) [lypē](../../strongs/g/g3077.md).

<a name="2corinthians_2_8"></a>2 Corinthians 2:8

Wherefore I [parakaleō](../../strongs/g/g3870.md) you that ye would [kyroō](../../strongs/g/g2964.md) your [agapē](../../strongs/g/g26.md) toward him.

<a name="2corinthians_2_9"></a>2 Corinthians 2:9

For to [touto](../../strongs/g/g5124.md) also did I [graphō](../../strongs/g/g1125.md), that I might [ginōskō](../../strongs/g/g1097.md) the [dokimē](../../strongs/g/g1382.md) of you, whether ye be [hypēkoos](../../strongs/g/g5255.md) in all things.

<a name="2corinthians_2_10"></a>2 Corinthians 2:10

To whom ye [charizomai](../../strongs/g/g5483.md) any thing, I also: for if I [charizomai](../../strongs/g/g5483.md) any thing, to whom I [charizomai](../../strongs/g/g5483.md), for your sakes in the [prosōpon](../../strongs/g/g4383.md) of [Christos](../../strongs/g/g5547.md);

<a name="2corinthians_2_11"></a>2 Corinthians 2:11

Lest [hypo](../../strongs/g/g5259.md) [Satanas](../../strongs/g/g4567.md) should [pleonekteō](../../strongs/g/g4122.md) us: for we are not [agnoeō](../../strongs/g/g50.md) of his [noēma](../../strongs/g/g3540.md).

<a name="2corinthians_2_12"></a>2 Corinthians 2:12

Furthermore, when I [erchomai](../../strongs/g/g2064.md) to [Trōas](../../strongs/g/g5174.md) to [Christos](../../strongs/g/g5547.md) [euaggelion](../../strongs/g/g2098.md), and a [thyra](../../strongs/g/g2374.md) was [anoigō](../../strongs/g/g455.md) unto me of the [kyrios](../../strongs/g/g2962.md),

<a name="2corinthians_2_13"></a>2 Corinthians 2:13

I had no [anesis](../../strongs/g/g425.md) in my [pneuma](../../strongs/g/g4151.md), because I [heuriskō](../../strongs/g/g2147.md) not [titos](../../strongs/g/g5103.md) my [adelphos](../../strongs/g/g80.md): but [apotassō](../../strongs/g/g657.md) of them, I [exerchomai](../../strongs/g/g1831.md) into [Makedonia](../../strongs/g/g3109.md).

<a name="2corinthians_2_14"></a>2 Corinthians 2:14

Now [charis](../../strongs/g/g5485.md) unto [theos](../../strongs/g/g2316.md), which [pantote](../../strongs/g/g3842.md) [thriambeuō](../../strongs/g/g2358.md) us in [Christos](../../strongs/g/g5547.md), and [phaneroō](../../strongs/g/g5319.md) the [osmē](../../strongs/g/g3744.md) of his [gnōsis](../../strongs/g/g1108.md) by us in every [topos](../../strongs/g/g5117.md).

<a name="2corinthians_2_15"></a>2 Corinthians 2:15

For we are unto [theos](../../strongs/g/g2316.md) an [euōdia](../../strongs/g/g2175.md) of [Christos](../../strongs/g/g5547.md), in them that are [sōzō](../../strongs/g/g4982.md), and in them that [apollymi](../../strongs/g/g622.md):

<a name="2corinthians_2_16"></a>2 Corinthians 2:16

To the one the [osmē](../../strongs/g/g3744.md) of [thanatos](../../strongs/g/g2288.md) unto [thanatos](../../strongs/g/g2288.md); and to the other the [osmē](../../strongs/g/g3744.md) of [zōē](../../strongs/g/g2222.md) unto [zōē](../../strongs/g/g2222.md). And who [hikanos](../../strongs/g/g2425.md) for these things?

<a name="2corinthians_2_17"></a>2 Corinthians 2:17

For we are not as [polys](../../strongs/g/g4183.md), which [kapēleuō](../../strongs/g/g2585.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md): but as of [eilikrineia](../../strongs/g/g1505.md), but as of [theos](../../strongs/g/g2316.md), [katenōpion](../../strongs/g/g2714.md) [theos](../../strongs/g/g2316.md) [laleō](../../strongs/g/g2980.md) we in [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[2 Corinthians](2corinthians.md)

[2 Corinthians 1](2corinthians_1.md) - [2 Corinthians 3](2corinthians_3.md)