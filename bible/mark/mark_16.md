# [Mark 16](https://www.blueletterbible.org/kjv/mar/16/1/rl1/s_966001)

<a name="mark_16_1"></a>Mark 16:1

And when the [sabbaton](../../strongs/g/g4521.md) was [diaginomai](../../strongs/g/g1230.md), [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md), and [Maria](../../strongs/g/g3137.md) of [Iakōbos](../../strongs/g/g2385.md), and [Salōmē](../../strongs/g/g4539.md), had [agorazō](../../strongs/g/g59.md) [arōma](../../strongs/g/g759.md), that they might [erchomai](../../strongs/g/g2064.md) and [aleiphō](../../strongs/g/g218.md) him.

<a name="mark_16_2"></a>Mark 16:2

And [lian](../../strongs/g/g3029.md) [prōï](../../strongs/g/g4404.md) the first [sabbaton](../../strongs/g/g4521.md), they [erchomai](../../strongs/g/g2064.md) unto the [mnēmeion](../../strongs/g/g3419.md) at the [anatellō](../../strongs/g/g393.md) of the [hēlios](../../strongs/g/g2246.md).

<a name="mark_16_3"></a>Mark 16:3

And they [legō](../../strongs/g/g3004.md) among themselves, Who shall [apokyliō](../../strongs/g/g617.md) the [lithos](../../strongs/g/g3037.md) from the [thyra](../../strongs/g/g2374.md) of the [mnēmeion](../../strongs/g/g3419.md)?

<a name="mark_16_4"></a>Mark 16:4

And when they [anablepō](../../strongs/g/g308.md), they [theōreō](../../strongs/g/g2334.md) that the [lithos](../../strongs/g/g3037.md) was [apokyliō](../../strongs/g/g617.md): for it was [sphodra](../../strongs/g/g4970.md) [megas](../../strongs/g/g3173.md).

<a name="mark_16_5"></a>Mark 16:5

And [eiserchomai](../../strongs/g/g1525.md) into the [mnēmeion](../../strongs/g/g3419.md), they [eidō](../../strongs/g/g1492.md) a [neaniskos](../../strongs/g/g3495.md) [kathēmai](../../strongs/g/g2521.md) on the [dexios](../../strongs/g/g1188.md), [periballō](../../strongs/g/g4016.md) in a [leukos](../../strongs/g/g3022.md) [stolē](../../strongs/g/g4749.md); and they were [ekthambeō](../../strongs/g/g1568.md).

<a name="mark_16_6"></a>Mark 16:6

And he [legō](../../strongs/g/g3004.md) unto them, Be not [ekthambeō](../../strongs/g/g1568.md): Ye [zēteō](../../strongs/g/g2212.md) [Iēsous](../../strongs/g/g2424.md) of [Nazarēnos](../../strongs/g/g3479.md), which was [stauroō](../../strongs/g/g4717.md): he is [egeirō](../../strongs/g/g1453.md); he is not here: [ide](../../strongs/g/g2396.md) the [topos](../../strongs/g/g5117.md) where they [tithēmi](../../strongs/g/g5087.md) him.

<a name="mark_16_7"></a>Mark 16:7

But [hypagō](../../strongs/g/g5217.md), [eipon](../../strongs/g/g2036.md) his [mathētēs](../../strongs/g/g3101.md) and [Petros](../../strongs/g/g4074.md) that he [proagō](../../strongs/g/g4254.md) you into [Galilaia](../../strongs/g/g1056.md): there shall ye [optanomai](../../strongs/g/g3700.md) him, as he [eipon](../../strongs/g/g2036.md) unto you.

<a name="mark_16_8"></a>Mark 16:8

And they [exerchomai](../../strongs/g/g1831.md) [tachy](../../strongs/g/g5035.md), and [pheugō](../../strongs/g/g5343.md) from the [mnēmeion](../../strongs/g/g3419.md); for they [tromos](../../strongs/g/g5156.md) and were [ekstasis](../../strongs/g/g1611.md): neither [eipon](../../strongs/g/g2036.md) they any thing to any; for they were [phobeō](../../strongs/g/g5399.md).

<a name="mark_16_9"></a>Mark 16:9

Now when was [anistēmi](../../strongs/g/g450.md) [prōï](../../strongs/g/g4404.md) the first of the [sabbaton](../../strongs/g/g4521.md), he [phainō](../../strongs/g/g5316.md) first to [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md), out of whom he had [ekballō](../../strongs/g/g1544.md) seven [daimonion](../../strongs/g/g1140.md).

<a name="mark_16_10"></a>Mark 16:10

And she [poreuō](../../strongs/g/g4198.md) and [apaggellō](../../strongs/g/g518.md) them that had been with him, as they [pentheō](../../strongs/g/g3996.md) and [klaiō](../../strongs/g/g2799.md).

<a name="mark_16_11"></a>Mark 16:11

And they, when they had [akouō](../../strongs/g/g191.md) that he was [zaō](../../strongs/g/g2198.md), and had been [theaomai](../../strongs/g/g2300.md) of her, [apisteō](../../strongs/g/g569.md).

<a name="mark_16_12"></a>Mark 16:12

After that he [phaneroō](../../strongs/g/g5319.md) in another [morphē](../../strongs/g/g3444.md) unto two of them, as they [peripateō](../../strongs/g/g4043.md), and [poreuō](../../strongs/g/g4198.md) into the [agros](../../strongs/g/g68.md).

<a name="mark_16_13"></a>Mark 16:13

And they [aperchomai](../../strongs/g/g565.md) and [apaggellō](../../strongs/g/g518.md) unto the [loipos](../../strongs/g/g3062.md): neither [pisteuō](../../strongs/g/g4100.md) they them.

<a name="mark_16_14"></a>Mark 16:14

Afterward he [phaneroō](../../strongs/g/g5319.md) unto the [hendeka](../../strongs/g/g1733.md) as they [anakeimai](../../strongs/g/g345.md), and [oneidizō](../../strongs/g/g3679.md) them with their [apistia](../../strongs/g/g570.md) and [sklērokardia](../../strongs/g/g4641.md), because they [pisteuō](../../strongs/g/g4100.md) not them which had [theaomai](../../strongs/g/g2300.md) him after he was [egeirō](../../strongs/g/g1453.md).

<a name="mark_16_15"></a>Mark 16:15

And he [eipon](../../strongs/g/g2036.md) unto them, **[poreuō](../../strongs/g/g4198.md) ye into all the [kosmos](../../strongs/g/g2889.md), and [kēryssō](../../strongs/g/g2784.md) the [euaggelion](../../strongs/g/g2098.md) to every [ktisis](../../strongs/g/g2937.md).**

<a name="mark_16_16"></a>Mark 16:16

**He that [pisteuō](../../strongs/g/g4100.md) and is [baptizō](../../strongs/g/g907.md) shall be [sōzō](../../strongs/g/g4982.md); but he that [apisteō](../../strongs/g/g569.md) shall be [katakrinō](../../strongs/g/g2632.md).**

<a name="mark_16_17"></a>Mark 16:17

**And these [sēmeion](../../strongs/g/g4592.md) shall [parakoloutheō](../../strongs/g/g3877.md) them that [pisteuō](../../strongs/g/g4100.md); In my [onoma](../../strongs/g/g3686.md) shall they [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md); they shall [laleō](../../strongs/g/g2980.md) with [kainos](../../strongs/g/g2537.md) [glōssa](../../strongs/g/g1100.md);**

<a name="mark_16_18"></a>Mark 16:18

**They shall [airō](../../strongs/g/g142.md) [ophis](../../strongs/g/g3789.md); and if they [pinō](../../strongs/g/g4095.md) any [thanasimos](../../strongs/g/g2286.md) thing, it shall not [blaptō](../../strongs/g/g984.md) them; they shall [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) on the [arrōstos](../../strongs/g/g732.md), and they shall [kalōs](../../strongs/g/g2573.md).**

<a name="mark_16_19"></a>Mark 16:19

So then after the [kyrios](../../strongs/g/g2962.md) had [laleō](../../strongs/g/g2980.md) unto them, he was [analambanō](../../strongs/g/g353.md) into [ouranos](../../strongs/g/g3772.md), and [kathizō](../../strongs/g/g2523.md) on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md).

<a name="mark_16_20"></a>Mark 16:20

And they [exerchomai](../../strongs/g/g1831.md), and [kēryssō](../../strongs/g/g2784.md) [pantachou](../../strongs/g/g3837.md), the [kyrios](../../strongs/g/g2962.md) [synergeō](../../strongs/g/g4903.md), and [bebaioō](../../strongs/g/g950.md) the [logos](../../strongs/g/g3056.md) with [sēmeion](../../strongs/g/g4592.md) [epakoloutheō](../../strongs/g/g1872.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 15](mark_15.md)

---

[^1]: [Mark 16:9 Commentary](../../commentary/mark/mark_16_commentary.md#mark_16_9)
