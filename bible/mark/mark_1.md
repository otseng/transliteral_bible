# [Mark 1](https://www.blueletterbible.org/kjv/mar/1/7/rl1/s_958001)

<a name="mark_1_1"></a>Mark 1:1

The [archē](../../strongs/g/g746.md) of the [euaggelion](../../strongs/g/g2098.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md);

<a name="mark_1_2"></a>Mark 1:2

As it is [graphō](../../strongs/g/g1125.md) in the [prophētēs](../../strongs/g/g4396.md), [idou](../../strongs/g/g2400.md), I [apostellō](../../strongs/g/g649.md) my [aggelos](../../strongs/g/g32.md) before thy [prosōpon](../../strongs/g/g4383.md), which shall [kataskeuazō](../../strongs/g/g2680.md) thy [hodos](../../strongs/g/g3598.md) before thee.

<a name="mark_1_3"></a>Mark 1:3

The [phōnē](../../strongs/g/g5456.md) [boaō](../../strongs/g/g994.md) in the [erēmos](../../strongs/g/g2048.md), [hetoimazō](../../strongs/g/g2090.md) ye [hodos](../../strongs/g/g3598.md) of the [kyrios](../../strongs/g/g2962.md), [poieō](../../strongs/g/g4160.md) his [tribos](../../strongs/g/g5147.md) [euthys](../../strongs/g/g2117.md).

<a name="mark_1_4"></a>Mark 1:4

[Iōannēs](../../strongs/g/g2491.md) did [baptizō](../../strongs/g/g907.md) in the [erēmos](../../strongs/g/g2048.md), and [kēryssō](../../strongs/g/g2784.md) the [baptisma](../../strongs/g/g908.md) of [metanoia](../../strongs/g/g3341.md) for the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md).

<a name="mark_1_5"></a>Mark 1:5

And there [ekporeuomai](../../strongs/g/g1607.md) unto him all the [chōra](../../strongs/g/g5561.md) of [Ioudaia](../../strongs/g/g2449.md), and they of [Ierosolymitēs](../../strongs/g/g2415.md), and were all [baptizō](../../strongs/g/g907.md) of him in the [potamos](../../strongs/g/g4215.md) of [Iordanēs](../../strongs/g/g2446.md), [exomologeō](../../strongs/g/g1843.md) their [hamartia](../../strongs/g/g266.md).

<a name="mark_1_6"></a>Mark 1:6

And [Iōannēs](../../strongs/g/g2491.md) was [endyō](../../strongs/g/g1746.md) with [kamēlos](../../strongs/g/g2574.md) [thrix](../../strongs/g/g2359.md), and with a [zōnē](../../strongs/g/g2223.md) of a [dermatinos](../../strongs/g/g1193.md) about his [osphys](../../strongs/g/g3751.md); and he did [esthiō](../../strongs/g/g2068.md) [akris](../../strongs/g/g200.md) and [agrios](../../strongs/g/g66.md) [meli](../../strongs/g/g3192.md);

<a name="mark_1_7"></a>Mark 1:7

And [kēryssō](../../strongs/g/g2784.md), [legō](../../strongs/g/g3004.md), There [erchomai](../../strongs/g/g2064.md) one [ischyros](../../strongs/g/g2478.md) than I after me, the [himas](../../strongs/g/g2438.md) of whose [hypodēma](../../strongs/g/g5266.md) I am not [hikanos](../../strongs/g/g2425.md) to [kyptō](../../strongs/g/g2955.md) and [lyō](../../strongs/g/g3089.md).

<a name="mark_1_8"></a>Mark 1:8

I indeed have [baptizō](../../strongs/g/g907.md) you with [hydōr](../../strongs/g/g5204.md): but he shall [baptizō](../../strongs/g/g907.md) you with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="mark_1_9"></a>Mark 1:9

And [ginomai](../../strongs/g/g1096.md) in those [hēmera](../../strongs/g/g2250.md), that [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) from [Nazara](../../strongs/g/g3478.md) of [Galilaia](../../strongs/g/g1056.md), and was [baptizō](../../strongs/g/g907.md) of [Iōannēs](../../strongs/g/g2491.md) in [Iordanēs](../../strongs/g/g2446.md).

<a name="mark_1_10"></a>Mark 1:10

And [eutheōs](../../strongs/g/g2112.md) [anabainō](../../strongs/g/g305.md) out of the [hydōr](../../strongs/g/g5204.md), he [eidō](../../strongs/g/g1492.md) the [ouranos](../../strongs/g/g3772.md) [schizō](../../strongs/g/g4977.md), and the [pneuma](../../strongs/g/g4151.md) like a [peristera](../../strongs/g/g4058.md) [katabainō](../../strongs/g/g2597.md) upon him:

<a name="mark_1_11"></a>Mark 1:11

And there [ginomai](../../strongs/g/g1096.md) a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md), Thou art my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md), in whom [eudokeō](../../strongs/g/g2106.md).

<a name="mark_1_12"></a>Mark 1:12

And [euthys](../../strongs/g/g2117.md) the [pneuma](../../strongs/g/g4151.md) [ekballō](../../strongs/g/g1544.md) him into the [erēmos](../../strongs/g/g2048.md).

<a name="mark_1_13"></a>Mark 1:13

And he was there in the [erēmos](../../strongs/g/g2048.md) forty [hēmera](../../strongs/g/g2250.md), [peirazō](../../strongs/g/g3985.md) of [Satanas](../../strongs/g/g4567.md); and was with the [thērion](../../strongs/g/g2342.md); and the [aggelos](../../strongs/g/g32.md) [diakoneō](../../strongs/g/g1247.md) unto him.

<a name="mark_1_14"></a>Mark 1:14

Now after that [Iōannēs](../../strongs/g/g2491.md) was [paradidōmi](../../strongs/g/g3860.md), [Iēsous](../../strongs/g/g2424.md) [erchomai](../../strongs/g/g2064.md) into [Galilaia](../../strongs/g/g1056.md), [kēryssō](../../strongs/g/g2784.md) the [euaggelion](../../strongs/g/g2098.md) of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md),

<a name="mark_1_15"></a>Mark 1:15

And [legō](../../strongs/g/g3004.md), The [kairos](../../strongs/g/g2540.md) is [plēroō](../../strongs/g/g4137.md), and the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is [eggizō](../../strongs/g/g1448.md): [metanoeō](../../strongs/g/g3340.md), and [pisteuō](../../strongs/g/g4100.md) the [euaggelion](../../strongs/g/g2098.md).

<a name="mark_1_16"></a>Mark 1:16

Now as he [peripateō](../../strongs/g/g4043.md) by the [thalassa](../../strongs/g/g2281.md) of [Galilaia](../../strongs/g/g1056.md), he [eidō](../../strongs/g/g1492.md) [Simōn](../../strongs/g/g4613.md) and [Andreas](../../strongs/g/g406.md) his [adelphos](../../strongs/g/g80.md) [ballō](../../strongs/g/g906.md) an [amphiblēstron](../../strongs/g/g293.md) into the [thalassa](../../strongs/g/g2281.md): for they were [halieus](../../strongs/g/g231.md).

<a name="mark_1_17"></a>Mark 1:17

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, [deute](../../strongs/g/g1205.md) ye after me, and I will [poieō](../../strongs/g/g4160.md) you to become [halieus](../../strongs/g/g231.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="mark_1_18"></a>Mark 1:18

And [eutheōs](../../strongs/g/g2112.md) they [aphiēmi](../../strongs/g/g863.md) their [diktyon](../../strongs/g/g1350.md), and [akoloutheō](../../strongs/g/g190.md) him.

<a name="mark_1_19"></a>Mark 1:19

And when he had [probainō](../../strongs/g/g4260.md) an [oligos](../../strongs/g/g3641.md) thence, he [eidō](../../strongs/g/g1492.md) [Iakōbos](../../strongs/g/g2385.md) of [Zebedaios](../../strongs/g/g2199.md), and [Iōannēs](../../strongs/g/g2491.md) his [adelphos](../../strongs/g/g80.md), who also were in the [ploion](../../strongs/g/g4143.md) [katartizō](../../strongs/g/g2675.md) their [diktyon](../../strongs/g/g1350.md).

<a name="mark_1_20"></a>Mark 1:20

And [eutheōs](../../strongs/g/g2112.md) he [kaleō](../../strongs/g/g2564.md) them: and they [aphiēmi](../../strongs/g/g863.md) their [patēr](../../strongs/g/g3962.md) [Zebedaios](../../strongs/g/g2199.md) in the [ploion](../../strongs/g/g4143.md) with the [misthōtos](../../strongs/g/g3411.md), and [aperchomai](../../strongs/g/g565.md) after him.

<a name="mark_1_21"></a>Mark 1:21

And they [eisporeuomai](../../strongs/g/g1531.md) into [Kapharnaoum](../../strongs/g/g2584.md); and [eutheōs](../../strongs/g/g2112.md) on the [sabbaton](../../strongs/g/g4521.md) he [eiserchomai](../../strongs/g/g1525.md) into the [synagōgē](../../strongs/g/g4864.md), and [didaskō](../../strongs/g/g1321.md).

<a name="mark_1_22"></a>Mark 1:22

And they were [ekplēssō](../../strongs/g/g1605.md) at his [didachē](../../strongs/g/g1322.md): for he [didaskō](../../strongs/g/g1321.md) them as one that had [exousia](../../strongs/g/g1849.md), and not as the [grammateus](../../strongs/g/g1122.md).

<a name="mark_1_23"></a>Mark 1:23

And there was in their [synagōgē](../../strongs/g/g4864.md) an [anthrōpos](../../strongs/g/g444.md) with an [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md); and he [anakrazō](../../strongs/g/g349.md),

<a name="mark_1_24"></a>Mark 1:24

[legō](../../strongs/g/g3004.md), [ea](../../strongs/g/g1436.md) what have we to do with thee, thou [Iēsous](../../strongs/g/g2424.md) of [Nazarēnos](../../strongs/g/g3479.md)? art thou [erchomai](../../strongs/g/g2064.md) to [apollymi](../../strongs/g/g622.md) us? I [eidō](../../strongs/g/g1492.md) thee who thou art, the [hagios](../../strongs/g/g40.md) of [theos](../../strongs/g/g2316.md).

<a name="mark_1_25"></a>Mark 1:25

And [Iēsous](../../strongs/g/g2424.md) [epitimaō](../../strongs/g/g2008.md) him, [legō](../../strongs/g/g3004.md), [phimoō](../../strongs/g/g5392.md), and [exerchomai](../../strongs/g/g1831.md) out of him.

<a name="mark_1_26"></a>Mark 1:26

And when the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) had [sparassō](../../strongs/g/g4682.md) him, and [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), he [exerchomai](../../strongs/g/g1831.md) of him.

<a name="mark_1_27"></a>Mark 1:27

And they were all [thambeō](../../strongs/g/g2284.md), insomuch that they [syzēteō](../../strongs/g/g4802.md) among themselves, [legō](../../strongs/g/g3004.md), What thing is this? what [kainos](../../strongs/g/g2537.md) [didachē](../../strongs/g/g1322.md) this? for with [exousia](../../strongs/g/g1849.md) [epitassō](../../strongs/g/g2004.md) he even the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), and they [hypakouō](../../strongs/g/g5219.md) him.

<a name="mark_1_28"></a>Mark 1:28

And [euthys](../../strongs/g/g2117.md) his [akoē](../../strongs/g/g189.md) [exerchomai](../../strongs/g/g1831.md) throughout all the [perichōros](../../strongs/g/g4066.md) [Galilaia](../../strongs/g/g1056.md).

<a name="mark_1_29"></a>Mark 1:29

And [eutheōs](../../strongs/g/g2112.md), when they were [exerchomai](../../strongs/g/g1831.md) out of the [synagōgē](../../strongs/g/g4864.md), they [erchomai](../../strongs/g/g2064.md) into the [oikia](../../strongs/g/g3614.md) of [Simōn](../../strongs/g/g4613.md) and [Andreas](../../strongs/g/g406.md), with [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md).

<a name="mark_1_30"></a>Mark 1:30

But [Simōn](../../strongs/g/g4613.md) [penthera](../../strongs/g/g3994.md) [katakeimai](../../strongs/g/g2621.md) [pyressō](../../strongs/g/g4445.md), and [eutheōs](../../strongs/g/g2112.md) they [legō](../../strongs/g/g3004.md) him of her.

<a name="mark_1_31"></a>Mark 1:31

And he [proserchomai](../../strongs/g/g4334.md) and [krateō](../../strongs/g/g2902.md) her by the [cheir](../../strongs/g/g5495.md), and [egeirō](../../strongs/g/g1453.md) her up; and [eutheōs](../../strongs/g/g2112.md) the [pyretos](../../strongs/g/g4446.md) [aphiēmi](../../strongs/g/g863.md) her, and she [diakoneō](../../strongs/g/g1247.md) unto them.

<a name="mark_1_32"></a>Mark 1:32

And at [opsios](../../strongs/g/g3798.md), when the [hēlios](../../strongs/g/g2246.md) did [dynō](../../strongs/g/g1416.md), they [pherō](../../strongs/g/g5342.md) unto him all that were [kakōs](../../strongs/g/g2560.md) and [daimonizomai](../../strongs/g/g1139.md).

<a name="mark_1_33"></a>Mark 1:33

And all the [polis](../../strongs/g/g4172.md) was [episynagō](../../strongs/g/g1996.md) at the [thyra](../../strongs/g/g2374.md).

<a name="mark_1_34"></a>Mark 1:34

And he [therapeuō](../../strongs/g/g2323.md) [polys](../../strongs/g/g4183.md) that were [kakōs](../../strongs/g/g2560.md) of [poikilos](../../strongs/g/g4164.md) [nosos](../../strongs/g/g3554.md), and [ekballō](../../strongs/g/g1544.md) [polys](../../strongs/g/g4183.md) [daimonion](../../strongs/g/g1140.md); and [aphiēmi](../../strongs/g/g863.md) not the [daimonion](../../strongs/g/g1140.md) to [laleō](../../strongs/g/g2980.md), because they [eidō](../../strongs/g/g1492.md) him.

<a name="mark_1_35"></a>Mark 1:35

And [prōï](../../strongs/g/g4404.md), [anistēmi](../../strongs/g/g450.md) a [lian](../../strongs/g/g3029.md) [ennychos](../../strongs/g/g1773.md), he [exerchomai](../../strongs/g/g1831.md), and [aperchomai](../../strongs/g/g565.md) into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md), and there [proseuchomai](../../strongs/g/g4336.md).

<a name="mark_1_36"></a>Mark 1:36

And [Simōn](../../strongs/g/g4613.md) and they that were with him [katadiōkō](../../strongs/g/g2614.md) him.

<a name="mark_1_37"></a>Mark 1:37

And when they had [heuriskō](../../strongs/g/g2147.md) him, they [legō](../../strongs/g/g3004.md) unto him, All [zēteō](../../strongs/g/g2212.md) thee.

<a name="mark_1_38"></a>Mark 1:38

And he [legō](../../strongs/g/g3004.md) unto them, Let us [agō](../../strongs/g/g71.md) into the next [kōmopolis](../../strongs/g/g2969.md), that I may [kēryssō](../../strongs/g/g2784.md) there also: for therefore I [exerchomai](../../strongs/g/g1831.md).

<a name="mark_1_39"></a>Mark 1:39

And he [kēryssō](../../strongs/g/g2784.md) in their [synagōgē](../../strongs/g/g4864.md) throughout all [Galilaia](../../strongs/g/g1056.md), and [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md).

<a name="mark_1_40"></a>Mark 1:40

And there [erchomai](../../strongs/g/g2064.md) a [lepros](../../strongs/g/g3015.md) to him, [parakaleō](../../strongs/g/g3870.md) him, and [gonypeteō](../../strongs/g/g1120.md) to him, and [legō](../../strongs/g/g3004.md) unto him, If thou [thelō](../../strongs/g/g2309.md), thou canst [katharizō](../../strongs/g/g2511.md) me.

<a name="mark_1_41"></a>Mark 1:41

And [Iēsous](../../strongs/g/g2424.md), [splagchnizomai](../../strongs/g/g4697.md), [ekteinō](../../strongs/g/g1614.md) [cheir](../../strongs/g/g5495.md), and [haptomai](../../strongs/g/g680.md) him, and [legō](../../strongs/g/g3004.md) unto him, I [thelō](../../strongs/g/g2309.md); be thou [katharizō](../../strongs/g/g2511.md).

<a name="mark_1_42"></a>Mark 1:42

And as soon as he had [eipon](../../strongs/g/g2036.md), [eutheōs](../../strongs/g/g2112.md) the [lepra](../../strongs/g/g3014.md) [aperchomai](../../strongs/g/g565.md) from him, and he was [katharizō](../../strongs/g/g2511.md).

<a name="mark_1_43"></a>Mark 1:43
And he [embrimaomai](../../strongs/g/g1690.md) him, and [eutheōs](../../strongs/g/g2112.md) [ekballō](../../strongs/g/g1544.md) him away;

<a name="mark_1_44"></a>Mark 1:44

And [legō](../../strongs/g/g3004.md) unto him, [horaō](../../strongs/g/g3708.md) thou [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md): but [hypagō](../../strongs/g/g5217.md), [deiknyō](../../strongs/g/g1166.md) thyself to the [hiereus](../../strongs/g/g2409.md), and [prospherō](../../strongs/g/g4374.md) for thy [katharismos](../../strongs/g/g2512.md) those things which [Mōÿsēs](../../strongs/g/g3475.md) [prostassō](../../strongs/g/g4367.md), for a [martyrion](../../strongs/g/g3142.md) unto them.

<a name="mark_1_45"></a>Mark 1:45

But he [exerchomai](../../strongs/g/g1831.md), and [archomai](../../strongs/g/g756.md) to [kēryssō](../../strongs/g/g2784.md) [polys](../../strongs/g/g4183.md), and to [diaphēmizō](../../strongs/g/g1310.md) the [logos](../../strongs/g/g3056.md), insomuch that [autos](../../strongs/g/g846.md) could [mēketi](../../strongs/g/g3371.md) [phanerōs](../../strongs/g/g5320.md) [eiserchomai](../../strongs/g/g1525.md) into the [polis](../../strongs/g/g4172.md), but was without in [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md): and they [erchomai](../../strongs/g/g2064.md) to him [pantachothen](../../strongs/g/g3836.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 2](mark_2.md)