# [Mark 15](https://www.blueletterbible.org/kjv/mar/15/1/rl1/s_966001)

<a name="mark_15_1"></a>Mark 15:1

And [eutheōs](../../strongs/g/g2112.md) in the [prōï](../../strongs/g/g4404.md) the [archiereus](../../strongs/g/g749.md) [poieō](../../strongs/g/g4160.md) a [symboulion](../../strongs/g/g4824.md) with the [presbyteros](../../strongs/g/g4245.md) and [grammateus](../../strongs/g/g1122.md) and the [holos](../../strongs/g/g3650.md) [synedrion](../../strongs/g/g4892.md), and [deō](../../strongs/g/g1210.md) [Iēsous](../../strongs/g/g2424.md), and [apopherō](../../strongs/g/g667.md) him, and [paradidōmi](../../strongs/g/g3860.md) to [Pilatos](../../strongs/g/g4091.md).

<a name="mark_15_2"></a>Mark 15:2

And [Pilatos](../../strongs/g/g4091.md) [eperōtaō](../../strongs/g/g1905.md) him, Art thou the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)? And he [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **Thou [legō](../../strongs/g/g3004.md).**

<a name="mark_15_3"></a>Mark 15:3

And the [archiereus](../../strongs/g/g749.md) [katēgoreō](../../strongs/g/g2723.md) him of [polys](../../strongs/g/g4183.md): but he [apokrinomai](../../strongs/g/g611.md) nothing.

<a name="mark_15_4"></a>Mark 15:4

And [Pilatos](../../strongs/g/g4091.md) [eperōtaō](../../strongs/g/g1905.md) him again, [legō](../../strongs/g/g3004.md), [apokrinomai](../../strongs/g/g611.md) thou nothing? [ide](../../strongs/g/g2396.md) how many things they [katamartyreō](../../strongs/g/g2649.md) against thee.

<a name="mark_15_5"></a>Mark 15:5

But [Iēsous](../../strongs/g/g2424.md) yet [apokrinomai](../../strongs/g/g611.md) nothing; so that [Pilatos](../../strongs/g/g4091.md) [thaumazō](../../strongs/g/g2296.md).

<a name="mark_15_6"></a>Mark 15:6

Now at [heortē](../../strongs/g/g1859.md) he [apolyō](../../strongs/g/g630.md) unto them one [desmios](../../strongs/g/g1198.md), [osper](../../strongs/g/g3746.md) they [aiteō](../../strongs/g/g154.md).

<a name="mark_15_7"></a>Mark 15:7

And there was one [legō](../../strongs/g/g3004.md) [Barabbas](../../strongs/g/g912.md), [deō](../../strongs/g/g1210.md) with them that had [stasiastēs](../../strongs/g/g4955.md) him, who had [poieō](../../strongs/g/g4160.md) [phonos](../../strongs/g/g5408.md) in the [stasis](../../strongs/g/g4714.md).

<a name="mark_15_8"></a>Mark 15:8

And the [ochlos](../../strongs/g/g3793.md) [anaboaō](../../strongs/g/g310.md) [archomai](../../strongs/g/g756.md) to [aiteō](../../strongs/g/g154.md) to do as he had ever [poieō](../../strongs/g/g4160.md) unto them.

<a name="mark_15_9"></a>Mark 15:9

But [Pilatos](../../strongs/g/g4091.md) [apokrinomai](../../strongs/g/g611.md) them, [legō](../../strongs/g/g3004.md), Will ye that I [apolyō](../../strongs/g/g630.md) unto you the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)?

<a name="mark_15_10"></a>Mark 15:10

For he [ginōskō](../../strongs/g/g1097.md) that the [archiereus](../../strongs/g/g749.md) had [paradidōmi](../../strongs/g/g3860.md) him for [phthonos](../../strongs/g/g5355.md).

<a name="mark_15_11"></a>Mark 15:11

But the [archiereus](../../strongs/g/g749.md) [anaseiō](../../strongs/g/g383.md) the [ochlos](../../strongs/g/g3793.md), that he should rather [apolyō](../../strongs/g/g630.md) [Barabbas](../../strongs/g/g912.md) unto them.

<a name="mark_15_12"></a>Mark 15:12

And [Pilatos](../../strongs/g/g4091.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) again unto them, What will ye then that I shall [poieō](../../strongs/g/g4160.md) unto him whom ye [legō](../../strongs/g/g3004.md) the [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)?

<a name="mark_15_13"></a>Mark 15:13

And they [krazō](../../strongs/g/g2896.md) again, [stauroō](../../strongs/g/g4717.md) him.

<a name="mark_15_14"></a>Mark 15:14

Then [Pilatos](../../strongs/g/g4091.md) [legō](../../strongs/g/g3004.md) unto them, Why, what [kakos](../../strongs/g/g2556.md) hath he [poieō](../../strongs/g/g4160.md)? And they [krazō](../../strongs/g/g2896.md) [perissoterōs](../../strongs/g/g4056.md), [stauroō](../../strongs/g/g4717.md) him.

<a name="mark_15_15"></a>Mark 15:15

And so [Pilatos](../../strongs/g/g4091.md), [boulomai](../../strongs/g/g1014.md) to [hikanos](../../strongs/g/g2425.md) [poieō](../../strongs/g/g4160.md) the [ochlos](../../strongs/g/g3793.md), [apolyō](../../strongs/g/g630.md) [Barabbas](../../strongs/g/g912.md) unto them, and [paradidōmi](../../strongs/g/g3860.md) [Iēsous](../../strongs/g/g2424.md), when he had [phragelloō](../../strongs/g/g5417.md), to be [stauroō](../../strongs/g/g4717.md).

<a name="mark_15_16"></a>Mark 15:16

And the [stratiōtēs](../../strongs/g/g4757.md) [apagō](../../strongs/g/g520.md) him into the [aulē](../../strongs/g/g833.md), [o estin](../../strongs/g/g3603.md) [praitōrion](../../strongs/g/g4232.md); and they [sygkaleō](../../strongs/g/g4779.md) the [holos](../../strongs/g/g3650.md) [speira](../../strongs/g/g4686.md).

<a name="mark_15_17"></a>Mark 15:17

And they [endyō](../../strongs/g/g1746.md) him with [porphyra](../../strongs/g/g4209.md), and [plekō](../../strongs/g/g4120.md) a [stephanos](../../strongs/g/g4735.md) of [akanthinos](../../strongs/g/g174.md), and [peritithēmi](../../strongs/g/g4060.md) about him,

<a name="mark_15_18"></a>Mark 15:18

And [archomai](../../strongs/g/g756.md) to [aspazomai](../../strongs/g/g782.md) him, [chairō](../../strongs/g/g5463.md), [basileus](../../strongs/g/g935.md) of the [Ioudaios](../../strongs/g/g2453.md)!

<a name="mark_15_19"></a>Mark 15:19

And they [typtō](../../strongs/g/g5180.md) him on the [kephalē](../../strongs/g/g2776.md) with a [kalamos](../../strongs/g/g2563.md), and did [emptyō](../../strongs/g/g1716.md) upon him, and [tithēmi](../../strongs/g/g5087.md) [gony](../../strongs/g/g1119.md) [proskyneō](../../strongs/g/g4352.md) him.

<a name="mark_15_20"></a>Mark 15:20

And when they had [empaizō](../../strongs/g/g1702.md) him, they [ekdyō](../../strongs/g/g1562.md) the [porphyra](../../strongs/g/g4209.md) from him, and [endyō](../../strongs/g/g1746.md) his own [himation](../../strongs/g/g2440.md) on him, and [exagō](../../strongs/g/g1806.md) him to [stauroō](../../strongs/g/g4717.md) him.

<a name="mark_15_21"></a>Mark 15:21

And they [aggareuō](../../strongs/g/g29.md) one [Simōn](../../strongs/g/g4613.md) a [Kyrēnaios](../../strongs/g/g2956.md), who [paragō](../../strongs/g/g3855.md), [erchomai](../../strongs/g/g2064.md) out of the [agros](../../strongs/g/g68.md), the [patēr](../../strongs/g/g3962.md) of [Alexandros](../../strongs/g/g223.md) and [Rhouphos](../../strongs/g/g4504.md), to [airō](../../strongs/g/g142.md) his [stauros](../../strongs/g/g4716.md).

<a name="mark_15_22"></a>Mark 15:22

And they [pherō](../../strongs/g/g5342.md) him unto the [topos](../../strongs/g/g5117.md) [Golgotha](../../strongs/g/g1115.md), which is, [methermēneuō](../../strongs/g/g3177.md), The [topos](../../strongs/g/g5117.md) of a [kranion](../../strongs/g/g2898.md).

<a name="mark_15_23"></a>Mark 15:23

And they [didōmi](../../strongs/g/g1325.md) him to [pinō](../../strongs/g/g4095.md) [oinos](../../strongs/g/g3631.md) [smyrnizō](../../strongs/g/g4669.md): but he [lambanō](../../strongs/g/g2983.md) not.

<a name="mark_15_24"></a>Mark 15:24

And when they had [stauroō](../../strongs/g/g4717.md) him, they [diamerizō](../../strongs/g/g1266.md) his [himation](../../strongs/g/g2440.md), [ballō](../../strongs/g/g906.md) [klēros](../../strongs/g/g2819.md) upon them, [tis](../../strongs/g/g5101.md) should [airō](../../strongs/g/g142.md).

<a name="mark_15_25"></a>Mark 15:25

And it was the third [hōra](../../strongs/g/g5610.md), and they [stauroō](../../strongs/g/g4717.md) him.

<a name="mark_15_26"></a>Mark 15:26

And the [epigraphē](../../strongs/g/g1923.md) of his [aitia](../../strongs/g/g156.md) was [epigraphō](../../strongs/g/g1924.md), [basileus](../../strongs/g/g935.md) [Ioudaios](../../strongs/g/g2453.md).

<a name="mark_15_27"></a>Mark 15:27

And with him they [stauroō](../../strongs/g/g4717.md) two [lēstēs](../../strongs/g/g3027.md); the one on his [dexios](../../strongs/g/g1188.md), and the other on his [euōnymos](../../strongs/g/g2176.md).

<a name="mark_15_28"></a>Mark 15:28

And the [graphē](../../strongs/g/g1124.md) was [plēroō](../../strongs/g/g4137.md), which [legō](../../strongs/g/g3004.md), And he was [logizomai](../../strongs/g/g3049.md) with the [anomos](../../strongs/g/g459.md). [^1]

<a name="mark_15_29"></a>Mark 15:29

And they that [paraporeuomai](../../strongs/g/g3899.md) [blasphēmeō](../../strongs/g/g987.md) on him, [kineō](../../strongs/g/g2795.md) their [kephalē](../../strongs/g/g2776.md), and [legō](../../strongs/g/g3004.md), [oua](../../strongs/g/g3758.md), thou that [katalyō](../../strongs/g/g2647.md) the [naos](../../strongs/g/g3485.md), and [oikodomeō](../../strongs/g/g3618.md) in three [hēmera](../../strongs/g/g2250.md),

<a name="mark_15_30"></a>Mark 15:30

[sōzō](../../strongs/g/g4982.md) [seautou](../../strongs/g/g4572.md), and [katabainō](../../strongs/g/g2597.md) from the [stauros](../../strongs/g/g4716.md).

<a name="mark_15_31"></a>Mark 15:31

[homoiōs](../../strongs/g/g3668.md) also the [archiereus](../../strongs/g/g749.md) [empaizō](../../strongs/g/g1702.md) [legō](../../strongs/g/g3004.md) among [allēlōn](../../strongs/g/g240.md) with the [grammateus](../../strongs/g/g1122.md), He [sōzō](../../strongs/g/g4982.md) others; himself he cannot [sōzō](../../strongs/g/g4982.md).

<a name="mark_15_32"></a>Mark 15:32

Let [Christos](../../strongs/g/g5547.md) the [basileus](../../strongs/g/g935.md) of [Israēl](../../strongs/g/g2474.md) [katabainō](../../strongs/g/g2597.md) now from the [stauros](../../strongs/g/g4716.md), that we may [eidō](../../strongs/g/g1492.md) and [pisteuō](../../strongs/g/g4100.md). And they that were [systauroō](../../strongs/g/g4957.md) with him [oneidizō](../../strongs/g/g3679.md) him.

<a name="mark_15_33"></a>Mark 15:33

And when the sixth [hōra](../../strongs/g/g5610.md) was [ginomai](../../strongs/g/g1096.md), there was [skotos](../../strongs/g/g4655.md) over the [holos](../../strongs/g/g3650.md) [gē](../../strongs/g/g1093.md) until the ninth [hōra](../../strongs/g/g5610.md).

<a name="mark_15_34"></a>Mark 15:34

And at the ninth [hōra](../../strongs/g/g5610.md) [Iēsous](../../strongs/g/g2424.md) [boaō](../../strongs/g/g994.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), **[elōï](../../strongs/g/g1682.md), [elōï](../../strongs/g/g1682.md), [lema](../../strongs/g/g2982.md) [sabachthani](../../strongs/g/g4518.md)?** which is, [methermēneuō](../../strongs/g/g3177.md), **My [theos](../../strongs/g/g2316.md), my [theos](../../strongs/g/g2316.md), why hast thou [egkataleipō](../../strongs/g/g1459.md) me?**

<a name="mark_15_35"></a>Mark 15:35

And some of them that [paristēmi](../../strongs/g/g3936.md), when they [akouō](../../strongs/g/g191.md) it, [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), he [phōneō](../../strongs/g/g5455.md) [Ēlias](../../strongs/g/g2243.md).

<a name="mark_15_36"></a>Mark 15:36

And one [trechō](../../strongs/g/g5143.md) and [gemizō](../../strongs/g/g1072.md) a [spoggos](../../strongs/g/g4699.md) full of [oxos](../../strongs/g/g3690.md), and [peritithēmi](../../strongs/g/g4060.md) on a [kalamos](../../strongs/g/g2563.md), and [potizō](../../strongs/g/g4222.md) him, [legō](../../strongs/g/g3004.md), [aphiēmi](../../strongs/g/g863.md); let us [eidō](../../strongs/g/g1492.md) whether [Ēlias](../../strongs/g/g2243.md) will [erchomai](../../strongs/g/g2064.md) to [kathaireō](../../strongs/g/g2507.md) him.

<a name="mark_15_37"></a>Mark 15:37

And [Iēsous](../../strongs/g/g2424.md) [aphiēmi](../../strongs/g/g863.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), and [ekpneō](../../strongs/g/g1606.md).

<a name="mark_15_38"></a>Mark 15:38

And the [katapetasma](../../strongs/g/g2665.md) of the [naos](../../strongs/g/g3485.md) was [schizō](../../strongs/g/g4977.md) in [dyo](../../strongs/g/g1417.md) from the [anōthen](../../strongs/g/g509.md) to the [katō](../../strongs/g/g2736.md).

<a name="mark_15_39"></a>Mark 15:39

And when the [kentyriōn](../../strongs/g/g2760.md), which [paristēmi](../../strongs/g/g3936.md) over [enantios](../../strongs/g/g1727.md) him, [eidō](../../strongs/g/g1492.md) that he so [krazō](../../strongs/g/g2896.md), and [ekpneō](../../strongs/g/g1606.md), he [eipon](../../strongs/g/g2036.md), [alēthōs](../../strongs/g/g230.md) this [anthrōpos](../../strongs/g/g444.md) was the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="mark_15_40"></a>Mark 15:40

There were also [gynē](../../strongs/g/g1135.md) [theōreō](../../strongs/g/g2334.md) on [makrothen](../../strongs/g/g3113.md) off: among whom was [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md), and [Maria](../../strongs/g/g3137.md) the [mētēr](../../strongs/g/g3384.md) of [Iakōbos](../../strongs/g/g2385.md) the [mikros](../../strongs/g/g3398.md) and of [Iōsēs](../../strongs/g/g2500.md), and [Salōmē](../../strongs/g/g4539.md);

<a name="mark_15_41"></a>Mark 15:41

(Who also, when he was in [Galilaia](../../strongs/g/g1056.md), [akoloutheō](../../strongs/g/g190.md) him, and [diakoneō](../../strongs/g/g1247.md) unto him;) and [polys](../../strongs/g/g4183.md) other which [synanabainō](../../strongs/g/g4872.md) with him unto [Hierosolyma](../../strongs/g/g2414.md).

<a name="mark_15_42"></a>Mark 15:42

And now when the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), because it was the [paraskeuē](../../strongs/g/g3904.md), that is, the [prosabbaton](../../strongs/g/g4315.md),

<a name="mark_15_43"></a>Mark 15:43

[Iōsēph](../../strongs/g/g2501.md) of [Harimathaia](../../strongs/g/g707.md), an [euschēmōn](../../strongs/g/g2158.md) [bouleutēs](../../strongs/g/g1010.md), which also [prosdechomai](../../strongs/g/g4327.md) for the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), [erchomai](../../strongs/g/g2064.md), and [eiserchomai](../../strongs/g/g1525.md) [tolmaō](../../strongs/g/g5111.md) unto [Pilatos](../../strongs/g/g4091.md), and [aiteō](../../strongs/g/g154.md) the [sōma](../../strongs/g/g4983.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="mark_15_44"></a>Mark 15:44

And [Pilatos](../../strongs/g/g4091.md) [thaumazō](../../strongs/g/g2296.md) if he were already [thnēskō](../../strongs/g/g2348.md): and [proskaleō](../../strongs/g/g4341.md) the [kentyriōn](../../strongs/g/g2760.md), he [eperōtaō](../../strongs/g/g1905.md) him whether he had been any [palai](../../strongs/g/g3819.md) [apothnēskō](../../strongs/g/g599.md).

<a name="mark_15_45"></a>Mark 15:45

And when he [ginōskō](../../strongs/g/g1097.md) of the [kentyriōn](../../strongs/g/g2760.md), he [dōreomai](../../strongs/g/g1433.md) the [sōma](../../strongs/g/g4983.md) to [Iōsēph](../../strongs/g/g2501.md).

<a name="mark_15_46"></a>Mark 15:46

And he [agorazō](../../strongs/g/g59.md) [sindōn](../../strongs/g/g4616.md), and [kathaireō](../../strongs/g/g2507.md) him, and [eneileō](../../strongs/g/g1750.md) him in the [sindōn](../../strongs/g/g4616.md), and [katatithēmi](../../strongs/g/g2698.md) him in a [mnēmeion](../../strongs/g/g3419.md) which was [latomeō](../../strongs/g/g2998.md) out of a [petra](../../strongs/g/g4073.md), and [proskyliō](../../strongs/g/g4351.md) a [lithos](../../strongs/g/g3037.md) unto the [thyra](../../strongs/g/g2374.md) of the [mnēmeion](../../strongs/g/g3419.md).

<a name="mark_15_47"></a>Mark 15:47

And [Maria](../../strongs/g/g3137.md) [Magdalēnē](../../strongs/g/g3094.md) and [Maria](../../strongs/g/g3137.md) of [Iōsēs](../../strongs/g/g2500.md) [theōreō](../../strongs/g/g2334.md) where he was [tithēmi](../../strongs/g/g5087.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 14](mark_14.md) - [Mark 16](mark_16.md)

---

[^1]: [Mark 15:28 Commentary](../../commentary/mark/mark_15_commentary.md#mark_15_28)
