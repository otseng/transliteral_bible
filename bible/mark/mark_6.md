# [Mark 6](https://www.blueletterbible.org/kjv/mar/6/1/rl1/s_961001)

<a name="mark_6_1"></a>Mark 6:1

And he [exerchomai](../../strongs/g/g1831.md) from thence, and [erchomai](../../strongs/g/g2064.md) into his own [patris](../../strongs/g/g3968.md); and his [mathētēs](../../strongs/g/g3101.md) [akoloutheō](../../strongs/g/g190.md) him.

<a name="mark_6_2"></a>Mark 6:2

And when the [sabbaton](../../strongs/g/g4521.md) was [ginomai](../../strongs/g/g1096.md), he [archomai](../../strongs/g/g756.md) to [didaskō](../../strongs/g/g1321.md) in the [synagōgē](../../strongs/g/g4864.md): and [polys](../../strongs/g/g4183.md) [akouō](../../strongs/g/g191.md) were [ekplēssō](../../strongs/g/g1605.md), [legō](../../strongs/g/g3004.md), From whence hath this these things? and what [sophia](../../strongs/g/g4678.md) this which is [didōmi](../../strongs/g/g1325.md) unto him, that even such [dynamis](../../strongs/g/g1411.md) are [ginomai](../../strongs/g/g1096.md) by his [cheir](../../strongs/g/g5495.md)?

<a name="mark_6_3"></a>Mark 6:3

Is not this the [tektōn](../../strongs/g/g5045.md), the [huios](../../strongs/g/g5207.md) of [Maria](../../strongs/g/g3137.md), the [adelphos](../../strongs/g/g80.md) of [Iakōbos](../../strongs/g/g2385.md), and [Iōsēs](../../strongs/g/g2500.md), and of [Ioudas](../../strongs/g/g2455.md), and [Simōn](../../strongs/g/g4613.md)? and are not his [adelphē](../../strongs/g/g79.md) here with us? And they were [skandalizō](../../strongs/g/g4624.md) at him.

<a name="mark_6_4"></a>Mark 6:4

But [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md) unto them, **A [prophētēs](../../strongs/g/g4396.md) is not without [atimos](../../strongs/g/g820.md), but in his own [patris](../../strongs/g/g3968.md), and among [syggenēs](../../strongs/g/g4773.md), and in his own [oikia](../../strongs/g/g3614.md).**

<a name="mark_6_5"></a>Mark 6:5

And he could there [poieō](../../strongs/g/g4160.md) no [dynamis](../../strongs/g/g1411.md), save that he [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) upon an [oligos](../../strongs/g/g3641.md) [arrōstos](../../strongs/g/g732.md), and [therapeuō](../../strongs/g/g2323.md).

<a name="mark_6_6"></a>Mark 6:6

And he [thaumazō](../../strongs/g/g2296.md) because of their [apistia](../../strongs/g/g570.md). And he [periagō](../../strongs/g/g4013.md) [kyklō](../../strongs/g/g2945.md) the [kōmē](../../strongs/g/g2968.md), [didaskō](../../strongs/g/g1321.md).

<a name="mark_6_7"></a>Mark 6:7

And he [proskaleō](../../strongs/g/g4341.md) the [dōdeka](../../strongs/g/g1427.md), and [archomai](../../strongs/g/g756.md) to [apostellō](../../strongs/g/g649.md) them forth by two and two; and [didōmi](../../strongs/g/g1325.md) them [exousia](../../strongs/g/g1849.md) over [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md);

<a name="mark_6_8"></a>Mark 6:8

And [paraggellō](../../strongs/g/g3853.md) them that they should [airō](../../strongs/g/g142.md) [mēdeis](../../strongs/g/g3367.md) for [hodos](../../strongs/g/g3598.md), save a [rhabdos](../../strongs/g/g4464.md) only; no [pēra](../../strongs/g/g4082.md), no [artos](../../strongs/g/g740.md), no [chalkos](../../strongs/g/g5475.md) in [zōnē](../../strongs/g/g2223.md):

<a name="mark_6_9"></a>Mark 6:9

But be [hypodeō](../../strongs/g/g5265.md) with [sandalion](../../strongs/g/g4547.md); and not [endyō](../../strongs/g/g1746.md) two [chitōn](../../strongs/g/g5509.md).

<a name="mark_6_10"></a>Mark 6:10

And he [legō](../../strongs/g/g3004.md) unto them, **In [opou](../../strongs/g/g3699.md) [ean](../../strongs/g/g1437.md) ye [eiserchomai](../../strongs/g/g1525.md) into an [oikia](../../strongs/g/g3614.md), there [menō](../../strongs/g/g3306.md) till ye [exerchomai](../../strongs/g/g1831.md) [ekeithen](../../strongs/g/g1564.md).**

<a name="mark_6_11"></a>Mark 6:11

**And whosoever shall not [dechomai](../../strongs/g/g1209.md) you, nor [akouō](../../strongs/g/g191.md) you, when ye [ekporeuomai](../../strongs/g/g1607.md) thence, [ektinassō](../../strongs/g/g1621.md) the [chous](../../strongs/g/g5522.md) [hypokatō](../../strongs/g/g5270.md) your [pous](../../strongs/g/g4228.md) for a [martyrion](../../strongs/g/g3142.md) against them. [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, It shall be [anektos](../../strongs/g/g414.md) for [Sodoma](../../strongs/g/g4670.md) and [Gomorra](../../strongs/g/g1116.md) in the [hēmera](../../strongs/g/g2250.md) of [krisis](../../strongs/g/g2920.md), than for that [polis](../../strongs/g/g4172.md).** [^1]

<a name="mark_6_12"></a>Mark 6:12

And they [exerchomai](../../strongs/g/g1831.md), and [kēryssō](../../strongs/g/g2784.md) that men should [metanoeō](../../strongs/g/g3340.md).

<a name="mark_6_13"></a>Mark 6:13

And they [ekballō](../../strongs/g/g1544.md) [polys](../../strongs/g/g4183.md) [daimonion](../../strongs/g/g1140.md), and [aleiphō](../../strongs/g/g218.md) with [elaion](../../strongs/g/g1637.md) [polys](../../strongs/g/g4183.md) that were [arrōstos](../../strongs/g/g732.md), and [therapeuō](../../strongs/g/g2323.md).

<a name="mark_6_14"></a>Mark 6:14

And [basileus](../../strongs/g/g935.md) [Hērōdēs](../../strongs/g/g2264.md) [akouō](../../strongs/g/g191.md); (for his [onoma](../../strongs/g/g3686.md) was [phaneros](../../strongs/g/g5318.md):) and he [legō](../../strongs/g/g3004.md), That [Iōannēs](../../strongs/g/g2491.md) the [baptizō](../../strongs/g/g907.md) was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), and therefore [dynamis](../../strongs/g/g1411.md) do [energeō](../../strongs/g/g1754.md) themselves in him.

<a name="mark_6_15"></a>Mark 6:15

Others [legō](../../strongs/g/g3004.md), That it is [Ēlias](../../strongs/g/g2243.md). And others [legō](../../strongs/g/g3004.md), That it is a [prophētēs](../../strongs/g/g4396.md), or as one of the [prophētēs](../../strongs/g/g4396.md).

<a name="mark_6_16"></a>Mark 6:16

But when [Hērōdēs](../../strongs/g/g2264.md) [akouō](../../strongs/g/g191.md), he [eipon](../../strongs/g/g2036.md), It is [Iōannēs](../../strongs/g/g2491.md), whom I [apokephalizō](../../strongs/g/g607.md): he is [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md).

<a name="mark_6_17"></a>Mark 6:17

For [Hērōdēs](../../strongs/g/g2264.md) himself had [apostellō](../../strongs/g/g649.md) and [krateō](../../strongs/g/g2902.md) upon [Iōannēs](../../strongs/g/g2491.md), and [deō](../../strongs/g/g1210.md) him in [phylakē](../../strongs/g/g5438.md) for [Hērōdias](../../strongs/g/g2266.md) sake, his [adelphos](../../strongs/g/g80.md) [Philippos](../../strongs/g/g5376.md) [gynē](../../strongs/g/g1135.md): for he had [gameō](../../strongs/g/g1060.md) her.

<a name="mark_6_18"></a>Mark 6:18

For [Iōannēs](../../strongs/g/g2491.md) had [legō](../../strongs/g/g3004.md) unto [Hērōdēs](../../strongs/g/g2264.md), It is not [exesti](../../strongs/g/g1832.md) for thee to have thy [adelphos](../../strongs/g/g80.md) [gynē](../../strongs/g/g1135.md).

<a name="mark_6_19"></a>Mark 6:19

Therefore [Hērōdias](../../strongs/g/g2266.md) had an [enechō](../../strongs/g/g1758.md) against him, and would have [apokteinō](../../strongs/g/g615.md) him; but she could not:

<a name="mark_6_20"></a>Mark 6:20

For [Hērōdēs](../../strongs/g/g2264.md) [phobeō](../../strongs/g/g5399.md) [Iōannēs](../../strongs/g/g2491.md), [eidō](../../strongs/g/g1492.md) that he was a [dikaios](../../strongs/g/g1342.md) [anēr](../../strongs/g/g435.md) and an [hagios](../../strongs/g/g40.md), and [syntēreō](../../strongs/g/g4933.md) him; and when he [akouō](../../strongs/g/g191.md) him, he [poieō](../../strongs/g/g4160.md) [polys](../../strongs/g/g4183.md), and [akouō](../../strongs/g/g191.md) him [hēdeōs](../../strongs/g/g2234.md).

<a name="mark_6_21"></a>Mark 6:21

And when an [eukairos](../../strongs/g/g2121.md) [hēmera](../../strongs/g/g2250.md) was [ginomai](../../strongs/g/g1096.md), that [Hērōdēs](../../strongs/g/g2264.md) on his [genesia](../../strongs/g/g1077.md) [poieō](../../strongs/g/g4160.md) a [deipnon](../../strongs/g/g1173.md) to his [megistan](../../strongs/g/g3175.md), [chiliarchos](../../strongs/g/g5506.md), and [prōtos](../../strongs/g/g4413.md) of [Galilaia](../../strongs/g/g1056.md);

<a name="mark_6_22"></a>Mark 6:22

And when the [thygatēr](../../strongs/g/g2364.md) of the [autos](../../strongs/g/g846.md) [Hērōdias](../../strongs/g/g2266.md) [eiserchomai](../../strongs/g/g1525.md), and [orcheomai](../../strongs/g/g3738.md), and [areskō](../../strongs/g/g700.md) [Hērōdēs](../../strongs/g/g2264.md) and them that [synanakeimai](../../strongs/g/g4873.md), the [basileus](../../strongs/g/g935.md) [eipon](../../strongs/g/g2036.md) unto the [korasion](../../strongs/g/g2877.md), [aiteō](../../strongs/g/g154.md) of me whatsoever thou wilt, and I will [didōmi](../../strongs/g/g1325.md) it thee.

<a name="mark_6_23"></a>Mark 6:23

And he [omnyō](../../strongs/g/g3660.md) unto her, Whatsoever thou shalt [aiteō](../../strongs/g/g154.md) of me, I will [didōmi](../../strongs/g/g1325.md) thee, unto the [ēmisys](../../strongs/g/g2255.md) of my [basileia](../../strongs/g/g932.md).

<a name="mark_6_24"></a>Mark 6:24

And she [exerchomai](../../strongs/g/g1831.md), and [eipon](../../strongs/g/g2036.md) unto her [mētēr](../../strongs/g/g3384.md), What shall I [aiteō](../../strongs/g/g154.md)? And she [eipon](../../strongs/g/g2036.md), The [kephalē](../../strongs/g/g2776.md) of [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md).

<a name="mark_6_25"></a>Mark 6:25

And she [eiserchomai](../../strongs/g/g1525.md) [eutheōs](../../strongs/g/g2112.md) with [spoudē](../../strongs/g/g4710.md) unto the [basileus](../../strongs/g/g935.md), and [aiteō](../../strongs/g/g154.md), [legō](../../strongs/g/g3004.md), I [thelō](../../strongs/g/g2309.md) that thou [didōmi](../../strongs/g/g1325.md) me [exautēs](../../strongs/g/g1824.md) in a [pinax](../../strongs/g/g4094.md) the [kephalē](../../strongs/g/g2776.md) of [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md).

<a name="mark_6_26"></a>Mark 6:26

And the [basileus](../../strongs/g/g935.md) was [perilypos](../../strongs/g/g4036.md); for his [horkos](../../strongs/g/g3727.md), and for their sakes which [synanakeimai](../../strongs/g/g4873.md), he would not [atheteō](../../strongs/g/g114.md) her.

<a name="mark_6_27"></a>Mark 6:27

And [eutheōs](../../strongs/g/g2112.md) the [basileus](../../strongs/g/g935.md) [apostellō](../../strongs/g/g649.md) a [spekoulatōr](../../strongs/g/g4688.md), and [epitassō](../../strongs/g/g2004.md) his [kephalē](../../strongs/g/g2776.md) to be [pherō](../../strongs/g/g5342.md): and he [aperchomai](../../strongs/g/g565.md) and [apokephalizō](../../strongs/g/g607.md) him in the [phylakē](../../strongs/g/g5438.md),

<a name="mark_6_28"></a>Mark 6:28

And [pherō](../../strongs/g/g5342.md) his [kephalē](../../strongs/g/g2776.md) in a [pinax](../../strongs/g/g4094.md), and [didōmi](../../strongs/g/g1325.md) it to the [korasion](../../strongs/g/g2877.md): and the [korasion](../../strongs/g/g2877.md) [didōmi](../../strongs/g/g1325.md) it to her [mētēr](../../strongs/g/g3384.md).

<a name="mark_6_29"></a>Mark 6:29

And when his [mathētēs](../../strongs/g/g3101.md) [akouō](../../strongs/g/g191.md), they [erchomai](../../strongs/g/g2064.md) and [airō](../../strongs/g/g142.md) his [ptōma](../../strongs/g/g4430.md), and [tithēmi](../../strongs/g/g5087.md) it in a [mnēmeion](../../strongs/g/g3419.md).

<a name="mark_6_30"></a>Mark 6:30

And the [apostolos](../../strongs/g/g652.md) [synagō](../../strongs/g/g4863.md) unto [Iēsous](../../strongs/g/g2424.md), and [apaggellō](../../strongs/g/g518.md) him all things, both what they had [poieō](../../strongs/g/g4160.md), and what they had [didaskō](../../strongs/g/g1321.md).

<a name="mark_6_31"></a>Mark 6:31

And he [eipon](../../strongs/g/g2036.md) unto them, **[deute](../../strongs/g/g1205.md) ye yourselves apart into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md), and [anapauō](../../strongs/g/g373.md) [oligos](../../strongs/g/g3641.md):** for there were [polys](../../strongs/g/g4183.md) [erchomai](../../strongs/g/g2064.md) and [hypagō](../../strongs/g/g5217.md), and they had no [eukaireō](../../strongs/g/g2119.md) so much as to [phago](../../strongs/g/g5315.md).

<a name="mark_6_32"></a>Mark 6:32

And they [aperchomai](../../strongs/g/g565.md) into an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md) by [ploion](../../strongs/g/g4143.md) [idios](../../strongs/g/g2398.md).

<a name="mark_6_33"></a>Mark 6:33

And the [ochlos](../../strongs/g/g3793.md) [eidō](../../strongs/g/g1492.md) them [hypagō](../../strongs/g/g5217.md), and [polys](../../strongs/g/g4183.md) [epiginōskō](../../strongs/g/g1921.md) him, and [syntrechō](../../strongs/g/g4936.md) [pezē](../../strongs/g/g3979.md) thither out of all [polis](../../strongs/g/g4172.md), and [proerchomai](../../strongs/g/g4281.md) them, and [synerchomai](../../strongs/g/g4905.md) unto him.

<a name="mark_6_34"></a>Mark 6:34

And [Iēsous](../../strongs/g/g2424.md), when he [exerchomai](../../strongs/g/g1831.md), [eidō](../../strongs/g/g1492.md) [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md), and was [splagchnizomai](../../strongs/g/g4697.md) toward them, because they were as [probaton](../../strongs/g/g4263.md) not having a [poimēn](../../strongs/g/g4166.md): and he [archomai](../../strongs/g/g756.md) to [didaskō](../../strongs/g/g1321.md) them [polys](../../strongs/g/g4183.md).

<a name="mark_6_35"></a>Mark 6:35

And when the [hōra](../../strongs/g/g5610.md) was now [polys](../../strongs/g/g4183.md), his [mathētēs](../../strongs/g/g3101.md) [proserchomai](../../strongs/g/g4334.md) unto him, and [legō](../../strongs/g/g3004.md), This is an [erēmos](../../strongs/g/g2048.md) [topos](../../strongs/g/g5117.md), and now the [hōra](../../strongs/g/g5610.md) is [polys](../../strongs/g/g4183.md):

<a name="mark_6_36"></a>Mark 6:36

[apolyō](../../strongs/g/g630.md) them, that they may [aperchomai](../../strongs/g/g565.md) into the [agros](../../strongs/g/g68.md) [kyklō](../../strongs/g/g2945.md), and into the [kōmē](../../strongs/g/g2968.md), and [agorazō](../../strongs/g/g59.md) themselves [artos](../../strongs/g/g740.md): for they have nothing to [phago](../../strongs/g/g5315.md).

<a name="mark_6_37"></a>Mark 6:37

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[didōmi](../../strongs/g/g1325.md) ye them to [phago](../../strongs/g/g5315.md).** And they [legō](../../strongs/g/g3004.md) unto him, Shall we [aperchomai](../../strongs/g/g565.md) and [agorazō](../../strongs/g/g59.md) two hundred [dēnarion](../../strongs/g/g1220.md) of [artos](../../strongs/g/g740.md), and [didōmi](../../strongs/g/g1325.md) them to [phago](../../strongs/g/g5315.md)?

<a name="mark_6_38"></a>Mark 6:38

He [legō](../../strongs/g/g3004.md) unto them, **How many [artos](../../strongs/g/g740.md) have ye? [hypagō](../../strongs/g/g5217.md) and [eidō](../../strongs/g/g1492.md).** And when they [ginōskō](../../strongs/g/g1097.md), they [legō](../../strongs/g/g3004.md), Five, and two [ichthys](../../strongs/g/g2486.md).

<a name="mark_6_39"></a>Mark 6:39

And he [epitassō](../../strongs/g/g2004.md) them to make all [anaklinō](../../strongs/g/g347.md) by [symposion](../../strongs/g/g4849.md) [symposion](../../strongs/g/g4849.md) upon the [chlōros](../../strongs/g/g5515.md) [chortos](../../strongs/g/g5528.md).

<a name="mark_6_40"></a>Mark 6:40

And they [anapiptō](../../strongs/g/g377.md) in [prasia](../../strongs/g/g4237.md) [prasia](../../strongs/g/g4237.md), by hundreds, and by fifties.

<a name="mark_6_41"></a>Mark 6:41

And when he had [lambanō](../../strongs/g/g2983.md) the five [artos](../../strongs/g/g740.md) and the two [ichthys](../../strongs/g/g2486.md), he [anablepō](../../strongs/g/g308.md) to [ouranos](../../strongs/g/g3772.md), and [eulogeō](../../strongs/g/g2127.md), and [kataklaō](../../strongs/g/g2622.md) the [artos](../../strongs/g/g740.md), and [didōmi](../../strongs/g/g1325.md) to his [mathētēs](../../strongs/g/g3101.md) to [paratithēmi](../../strongs/g/g3908.md) them; and the two [ichthys](../../strongs/g/g2486.md) [merizō](../../strongs/g/g3307.md) he among them all.

<a name="mark_6_42"></a>Mark 6:42

And they did all [phago](../../strongs/g/g5315.md), and were [chortazō](../../strongs/g/g5526.md).

<a name="mark_6_43"></a>Mark 6:43

And they [airō](../../strongs/g/g142.md) [dōdeka](../../strongs/g/g1427.md) [kophinos](../../strongs/g/g2894.md) [plērēs](../../strongs/g/g4134.md) of the [klasma](../../strongs/g/g2801.md), and of the [ichthys](../../strongs/g/g2486.md).

<a name="mark_6_44"></a>Mark 6:44

And they that did [phago](../../strongs/g/g5315.md) of the [artos](../../strongs/g/g740.md) were about five thousand [anēr](../../strongs/g/g435.md).

<a name="mark_6_45"></a>Mark 6:45

And [eutheōs](../../strongs/g/g2112.md) he [anagkazō](../../strongs/g/g315.md) his [mathētēs](../../strongs/g/g3101.md) to [embainō](../../strongs/g/g1684.md) into the [ploion](../../strongs/g/g4143.md), and to [proagō](../../strongs/g/g4254.md) to the [peran](../../strongs/g/g4008.md) before unto [Bēthsaïda](../../strongs/g/g966.md), while he [apolyō](../../strongs/g/g630.md) the [ochlos](../../strongs/g/g3793.md).

<a name="mark_6_46"></a>Mark 6:46

And when he had [apotassō](../../strongs/g/g657.md) them, he [aperchomai](../../strongs/g/g565.md) into a [oros](../../strongs/g/g3735.md) to [proseuchomai](../../strongs/g/g4336.md).

<a name="mark_6_47"></a>Mark 6:47

And when [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), the [ploion](../../strongs/g/g4143.md) was in the [mesos](../../strongs/g/g3319.md) of the [thalassa](../../strongs/g/g2281.md), and he [monos](../../strongs/g/g3441.md) on the [gē](../../strongs/g/g1093.md).

<a name="mark_6_48"></a>Mark 6:48

And he [eidō](../../strongs/g/g1492.md) them [basanizō](../../strongs/g/g928.md) in [elaunō](../../strongs/g/g1643.md); for the [anemos](../../strongs/g/g417.md) was [enantios](../../strongs/g/g1727.md) unto them: and about the fourth [phylakē](../../strongs/g/g5438.md) of the [nyx](../../strongs/g/g3571.md) he [erchomai](../../strongs/g/g2064.md) unto them, [peripateō](../../strongs/g/g4043.md) upon the [thalassa](../../strongs/g/g2281.md), and would have [parerchomai](../../strongs/g/g3928.md) them.

<a name="mark_6_49"></a>Mark 6:49

But when they [eidō](../../strongs/g/g1492.md) him [peripateō](../../strongs/g/g4043.md) upon the [thalassa](../../strongs/g/g2281.md), they [dokeō](../../strongs/g/g1380.md) it had been a [phantasma](../../strongs/g/g5326.md), and [anakrazō](../../strongs/g/g349.md):

<a name="mark_6_50"></a>Mark 6:50

For they all [eidō](../../strongs/g/g1492.md) him, and were [tarassō](../../strongs/g/g5015.md). And [eutheōs](../../strongs/g/g2112.md) he [laleō](../../strongs/g/g2980.md) with them, and [legō](../../strongs/g/g3004.md) unto them, **[tharseō](../../strongs/g/g2293.md): it is I; be not [phobeō](../../strongs/g/g5399.md).**

<a name="mark_6_51"></a>Mark 6:51

And he [anabainō](../../strongs/g/g305.md) unto them into the [ploion](../../strongs/g/g4143.md); and the [anemos](../../strongs/g/g417.md) [kopazō](../../strongs/g/g2869.md): and they were [lian](../../strongs/g/g3029.md) [existēmi](../../strongs/g/g1839.md) in themselves beyond [perissos](../../strongs/g/g4053.md), and [thaumazō](../../strongs/g/g2296.md).

<a name="mark_6_52"></a>Mark 6:52

For they [syniēmi](../../strongs/g/g4920.md) not of the [artos](../../strongs/g/g740.md): for their [kardia](../../strongs/g/g2588.md) was [pōroō](../../strongs/g/g4456.md).

<a name="mark_6_53"></a>Mark 6:53

And when they had [diaperaō](../../strongs/g/g1276.md), they [erchomai](../../strongs/g/g2064.md) into the [gē](../../strongs/g/g1093.md) of [Gennēsaret](../../strongs/g/g1082.md), and [prosormizō](../../strongs/g/g4358.md).

<a name="mark_6_54"></a>Mark 6:54

And when they were [exerchomai](../../strongs/g/g1831.md) out of the [ploion](../../strongs/g/g4143.md), [eutheōs](../../strongs/g/g2112.md) they [epiginōskō](../../strongs/g/g1921.md) him,

<a name="mark_6_55"></a>Mark 6:55

And [peritrechō](../../strongs/g/g4063.md) through that whole [perichōros](../../strongs/g/g4066.md), and [archomai](../../strongs/g/g756.md) to [peripherō](../../strongs/g/g4064.md) in [krabattos](../../strongs/g/g2895.md) those that were [kakōs](../../strongs/g/g2560.md), where they [akouō](../../strongs/g/g191.md) he was.

<a name="mark_6_56"></a>Mark 6:56

And whithersoever he [eisporeuomai](../../strongs/g/g1531.md), into [kōmē](../../strongs/g/g2968.md), or [polis](../../strongs/g/g4172.md), or [agros](../../strongs/g/g68.md), they [tithēmi](../../strongs/g/g5087.md) the [astheneō](../../strongs/g/g770.md) in the [agora](../../strongs/g/g58.md), and [parakaleō](../../strongs/g/g3870.md) him that they might [haptomai](../../strongs/g/g680.md) if it were but the [kraspedon](../../strongs/g/g2899.md) of his [himation](../../strongs/g/g2440.md): and as many as [haptomai](../../strongs/g/g680.md) him were [sōzō](../../strongs/g/g4982.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 5](mark_5.md) - [Mark 7](mark_7.md)

---

[^1]: [Mark 6:11 Commentary](../../commentary/mark/mark_6_commentary.md#mark_6_11)
