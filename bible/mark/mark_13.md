# [Mark 13](https://www.blueletterbible.org/kjv/mar/13/1/rl1/s_966001)

<a name="mark_13_1"></a>Mark 13:1

And as he [ekporeuomai](../../strongs/g/g1607.md) out of the [hieron](../../strongs/g/g2411.md), one of his [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, [didaskalos](../../strongs/g/g1320.md), [ide](../../strongs/g/g2396.md) [potapos](../../strongs/g/g4217.md) of [lithos](../../strongs/g/g3037.md) and [potapos](../../strongs/g/g4217.md) [oikodomē](../../strongs/g/g3619.md)!

<a name="mark_13_2"></a>Mark 13:2

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto him, **[blepō](../../strongs/g/g991.md) thou these [megas](../../strongs/g/g3173.md) [oikodomē](../../strongs/g/g3619.md)?  there shall not be [aphiēmi](../../strongs/g/g863.md) [lithos](../../strongs/g/g3037.md) upon another, that shall not be [katalyō](../../strongs/g/g2647.md).**

<a name="mark_13_3"></a>Mark 13:3

And as he [kathēmai](../../strongs/g/g2521.md) upon the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md) over [katenanti](../../strongs/g/g2713.md) the [hieron](../../strongs/g/g2411.md), [Petros](../../strongs/g/g4074.md) and [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md) and [Andreas](../../strongs/g/g406.md) [eperōtaō](../../strongs/g/g1905.md) him [idios](../../strongs/g/g2398.md),

<a name="mark_13_4"></a>Mark 13:4

[eipon](../../strongs/g/g2036.md) us, when shall these things be? and what the [sēmeion](../../strongs/g/g4592.md) when all these things shall be [synteleō](../../strongs/g/g4931.md)?

<a name="mark_13_5"></a>Mark 13:5

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) them [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md), **[blepō](../../strongs/g/g991.md) lest any [planaō](../../strongs/g/g4105.md) you:**

<a name="mark_13_6"></a>Mark 13:6

**For [polys](../../strongs/g/g4183.md) shall [erchomai](../../strongs/g/g2064.md) in my [onoma](../../strongs/g/g3686.md), [legō](../../strongs/g/g3004.md), [egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md); and shall [planaō](../../strongs/g/g4105.md) [polys](../../strongs/g/g4183.md).**

<a name="mark_13_7"></a>Mark 13:7

**And when ye shall [akouō](../../strongs/g/g191.md) of [polemos](../../strongs/g/g4171.md) and [akoē](../../strongs/g/g189.md) of [polemos](../../strongs/g/g4171.md), be ye not [throeō](../../strongs/g/g2360.md): for must needs be; but the [telos](../../strongs/g/g5056.md) not be yet.**

<a name="mark_13_8"></a>Mark 13:8

**For [ethnos](../../strongs/g/g1484.md) shall [egeirō](../../strongs/g/g1453.md) against [ethnos](../../strongs/g/g1484.md), and [basileia](../../strongs/g/g932.md) against [basileia](../../strongs/g/g932.md): and there shall be [seismos](../../strongs/g/g4578.md) [kata](../../strongs/g/g2596.md) [topos](../../strongs/g/g5117.md), and there shall be [limos](../../strongs/g/g3042.md) and [tarachē](../../strongs/g/g5016.md): these are the [archē](../../strongs/g/g746.md) of [ōdin](../../strongs/g/g5604.md).**

<a name="mark_13_9"></a>Mark 13:9

**But [blepō](../../strongs/g/g991.md) to yourselves: for they shall [paradidōmi](../../strongs/g/g3860.md) you up to [synedrion](../../strongs/g/g4892.md); and in the [synagōgē](../../strongs/g/g4864.md) ye shall be [derō](../../strongs/g/g1194.md): and ye shall [histēmi](../../strongs/g/g2476.md) [agō](../../strongs/g/g71.md) before [hēgemōn](../../strongs/g/g2232.md) and [basileus](../../strongs/g/g935.md) for my sake, for a [martyrion](../../strongs/g/g3142.md) against them.**

<a name="mark_13_10"></a>Mark 13:10

**And the [euaggelion](../../strongs/g/g2098.md) must [prōton](../../strongs/g/g4412.md) be [kēryssō](../../strongs/g/g2784.md) among all [ethnos](../../strongs/g/g1484.md).**

<a name="mark_13_11"></a>Mark 13:11

**But when they shall [agō](../../strongs/g/g71.md), and [paradidōmi](../../strongs/g/g3860.md) you up, [promerimnaō](../../strongs/g/g4305.md) not what ye shall [laleō](../../strongs/g/g2980.md), neither do ye [meletaō](../../strongs/g/g3191.md): but whatsoever shall be [didōmi](../../strongs/g/g1325.md) you in that [hōra](../../strongs/g/g5610.md), that [laleō](../../strongs/g/g2980.md) ye: for it is not ye that [laleō](../../strongs/g/g2980.md), but the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).**

<a name="mark_13_12"></a>Mark 13:12

**Now the [adelphos](../../strongs/g/g80.md) shall [paradidōmi](../../strongs/g/g3860.md) the [adelphos](../../strongs/g/g80.md) to [thanatos](../../strongs/g/g2288.md), and the [patēr](../../strongs/g/g3962.md) the [teknon](../../strongs/g/g5043.md); and [teknon](../../strongs/g/g5043.md) shall [epanistēmi](../../strongs/g/g1881.md) against [goneus](../../strongs/g/g1118.md), and shall cause them to be [thanatoō](../../strongs/g/g2289.md).**

<a name="mark_13_13"></a>Mark 13:13

**And ye shall be [miseō](../../strongs/g/g3404.md) of all for my [onoma](../../strongs/g/g3686.md) sake: but he that shall [hypomenō](../../strongs/g/g5278.md) unto the [telos](../../strongs/g/g5056.md), the same shall be [sōzō](../../strongs/g/g4982.md).**

<a name="mark_13_14"></a>Mark 13:14

**But when ye shall [eidō](../../strongs/g/g1492.md) the [bdelygma](../../strongs/g/g946.md) of [erēmōsis](../../strongs/g/g2050.md), [rheō](../../strongs/g/g4483.md) of by [Daniēl](../../strongs/g/g1158.md) the [prophētēs](../../strongs/g/g4396.md), [histēmi](../../strongs/g/g2476.md) where it ought not,** (let him that [anaginōskō](../../strongs/g/g314.md) [noeō](../../strongs/g/g3539.md),) **then let them that be in [Ioudaia](../../strongs/g/g2449.md) [pheugō](../../strongs/g/g5343.md) to the [oros](../../strongs/g/g3735.md):**

<a name="mark_13_15"></a>Mark 13:15

**And let him that is on the [dōma](../../strongs/g/g1430.md) not [katabainō](../../strongs/g/g2597.md) into the [oikia](../../strongs/g/g3614.md), neither [eiserchomai](../../strongs/g/g1525.md), to [airō](../../strongs/g/g142.md) any thing out of his [oikia](../../strongs/g/g3614.md):**

<a name="mark_13_16"></a>Mark 13:16

**And let him that is in the [agros](../../strongs/g/g68.md) not [epistrephō](../../strongs/g/g1994.md) back again for to [airō](../../strongs/g/g142.md) his [himation](../../strongs/g/g2440.md).**

<a name="mark_13_17"></a>Mark 13:17

**But [ouai](../../strongs/g/g3759.md) to them that are with [gastēr](../../strongs/g/g1064.md), and to them that [thēlazō](../../strongs/g/g2337.md) in those [hēmera](../../strongs/g/g2250.md)!**

<a name="mark_13_18"></a>Mark 13:18

**And [proseuchomai](../../strongs/g/g4336.md) ye that your [phygē](../../strongs/g/g5437.md) be not in the [cheimōn](../../strongs/g/g5494.md).**

<a name="mark_13_19"></a>Mark 13:19

**For those [hēmera](../../strongs/g/g2250.md) shall be [thlipsis](../../strongs/g/g2347.md), such as was not from the [archē](../../strongs/g/g746.md) of the [ktisis](../../strongs/g/g2937.md) which [theos](../../strongs/g/g2316.md) [ktizō](../../strongs/g/g2936.md) unto [nyn](../../strongs/g/g3568.md), neither shall be.**

<a name="mark_13_20"></a>Mark 13:20

**And except that the [kyrios](../../strongs/g/g2962.md) had [koloboō](../../strongs/g/g2856.md) those [hēmera](../../strongs/g/g2250.md), no [sarx](../../strongs/g/g4561.md) should be [sōzō](../../strongs/g/g4982.md): but for the [eklektos](../../strongs/g/g1588.md) sake, whom he hath [eklegomai](../../strongs/g/g1586.md), he hath [koloboō](../../strongs/g/g2856.md) the [hēmera](../../strongs/g/g2250.md).**

<a name="mark_13_21"></a>Mark 13:21

**And then if [tis](../../strongs/g/g5100.md) shall [eipon](../../strongs/g/g2036.md) to you, [idou](../../strongs/g/g2400.md), here [Christos](../../strongs/g/g5547.md); or, [idou](../../strongs/g/g2400.md), there; [pisteuō](../../strongs/g/g4100.md) not:**

<a name="mark_13_22"></a>Mark 13:22

**For [pseudochristos](../../strongs/g/g5580.md) and [pseudoprophētēs](../../strongs/g/g5578.md) shall [egeirō](../../strongs/g/g1453.md), and shall [didōmi](../../strongs/g/g1325.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md), to [apoplanaō](../../strongs/g/g635.md), if it were [dynatos](../../strongs/g/g1415.md), even the [eklektos](../../strongs/g/g1588.md).**

<a name="mark_13_23"></a>Mark 13:23

**But [blepō](../../strongs/g/g991.md) ye: [idou](../../strongs/g/g2400.md), I have [proereō](../../strongs/g/g4280.md) you all things.**

<a name="mark_13_24"></a>Mark 13:24

**But in those [hēmera](../../strongs/g/g2250.md), after that [thlipsis](../../strongs/g/g2347.md), the [hēlios](../../strongs/g/g2246.md) shall be [skotizō](../../strongs/g/g4654.md), and the [selēnē](../../strongs/g/g4582.md) shall not [didōmi](../../strongs/g/g1325.md) her [pheggos](../../strongs/g/g5338.md),**

<a name="mark_13_25"></a>Mark 13:25

**And the [astēr](../../strongs/g/g792.md) of [ouranos](../../strongs/g/g3772.md) shall [ekpiptō](../../strongs/g/g1601.md), and the [dynamis](../../strongs/g/g1411.md) that are in [ouranos](../../strongs/g/g3772.md) shall be [saleuō](../../strongs/g/g4531.md).**

<a name="mark_13_26"></a>Mark 13:26

**And then shall they [optanomai](../../strongs/g/g3700.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) in the [nephelē](../../strongs/g/g3507.md) with [polys](../../strongs/g/g4183.md) [dynamis](../../strongs/g/g1411.md) and [doxa](../../strongs/g/g1391.md).**

<a name="mark_13_27"></a>Mark 13:27

**And then shall he [apostellō](../../strongs/g/g649.md) his [aggelos](../../strongs/g/g32.md), and shall [episynagō](../../strongs/g/g1996.md) his [eklektos](../../strongs/g/g1588.md) from the four [anemos](../../strongs/g/g417.md), from the [akron](../../strongs/g/g206.md) of the [gē](../../strongs/g/g1093.md) to the [akron](../../strongs/g/g206.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="mark_13_28"></a>Mark 13:28

**Now [manthanō](../../strongs/g/g3129.md) a [parabolē](../../strongs/g/g3850.md) of the [sykē](../../strongs/g/g4808.md); When her [klados](../../strongs/g/g2798.md) is yet [hapalos](../../strongs/g/g527.md), and [ekphyō](../../strongs/g/g1631.md) [phyllon](../../strongs/g/g5444.md), ye [ginōskō](../../strongs/g/g1097.md) that [theros](../../strongs/g/g2330.md) is [eggys](../../strongs/g/g1451.md):**

<a name="mark_13_29"></a>Mark 13:29

**So ye in like manner, when ye shall [eidō](../../strongs/g/g1492.md) these things [ginomai](../../strongs/g/g1096.md), [ginōskō](../../strongs/g/g1097.md) that it is [eggys](../../strongs/g/g1451.md), at the [thyra](../../strongs/g/g2374.md).**

<a name="mark_13_30"></a>Mark 13:30

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, that this [genea](../../strongs/g/g1074.md) shall not [parerchomai](../../strongs/g/g3928.md), till all these things be [ginomai](../../strongs/g/g1096.md).**

<a name="mark_13_31"></a>Mark 13:31

**[ouranos](../../strongs/g/g3772.md) and [gē](../../strongs/g/g1093.md) shall [parerchomai](../../strongs/g/g3928.md): but my [logos](../../strongs/g/g3056.md) shall not [parerchomai](../../strongs/g/g3928.md).**

<a name="mark_13_32"></a>Mark 13:32

**But of that [hēmera](../../strongs/g/g2250.md) and that [hōra](../../strongs/g/g5610.md) [eidō](../../strongs/g/g1492.md) [oudeis](../../strongs/g/g3762.md), no, not the [aggelos](../../strongs/g/g32.md) which are in [ouranos](../../strongs/g/g3772.md), neither the [huios](../../strongs/g/g5207.md), but the [patēr](../../strongs/g/g3962.md).**

<a name="mark_13_33"></a>Mark 13:33

**[blepō](../../strongs/g/g991.md), [agrypneō](../../strongs/g/g69.md) and [proseuchomai](../../strongs/g/g4336.md): for ye [eidō](../../strongs/g/g1492.md) not when the [kairos](../../strongs/g/g2540.md) is.**

<a name="mark_13_34"></a>Mark 13:34

**as an [anthrōpos](../../strongs/g/g444.md) [apodēmos](../../strongs/g/g590.md), who [aphiēmi](../../strongs/g/g863.md) his [oikia](../../strongs/g/g3614.md), and [didōmi](../../strongs/g/g1325.md) [exousia](../../strongs/g/g1849.md) to his [doulos](../../strongs/g/g1401.md), and to [hekastos](../../strongs/g/g1538.md) his [ergon](../../strongs/g/g2041.md), and [entellō](../../strongs/g/g1781.md) the [thyrōros](../../strongs/g/g2377.md) to [grēgoreō](../../strongs/g/g1127.md).**

<a name="mark_13_35"></a>Mark 13:35

**[grēgoreō](../../strongs/g/g1127.md) ye therefore: for ye [eidō](../../strongs/g/g1492.md) not when the [kyrios](../../strongs/g/g2962.md) of the [oikia](../../strongs/g/g3614.md) [erchomai](../../strongs/g/g2064.md), at [opse](../../strongs/g/g3796.md), or at [mesonyktion](../../strongs/g/g3317.md), or at the [alektorophōnia](../../strongs/g/g219.md), or in the [prōï](../../strongs/g/g4404.md):**

<a name="mark_13_36"></a>Mark 13:36

**Lest [erchomai](../../strongs/g/g2064.md) [exaiphnēs](../../strongs/g/g1810.md) he [heuriskō](../../strongs/g/g2147.md) you [katheudō](../../strongs/g/g2518.md).**

<a name="mark_13_37"></a>Mark 13:37

**And what I [legō](../../strongs/g/g3004.md) unto you I [legō](../../strongs/g/g3004.md) unto all, [grēgoreō](../../strongs/g/g1127.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 12](mark_12.md) - [Mark 14](mark_14.md)