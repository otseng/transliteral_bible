# [Mark 8](https://www.blueletterbible.org/kjv/mar/8/1/rl1/s_961001)

<a name="mark_8_1"></a>Mark 8:1

In those [hēmera](../../strongs/g/g2250.md) the [ochlos](../../strongs/g/g3793.md) being [pampolys](../../strongs/g/g3827.md), and having nothing to [phago](../../strongs/g/g5315.md), [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md) unto them,

<a name="mark_8_2"></a>Mark 8:2

**I have [splagchnizomai](../../strongs/g/g4697.md) on the [ochlos](../../strongs/g/g3793.md), because they [ēdē](../../strongs/g/g2235.md) [prosmenō](../../strongs/g/g4357.md) with me three [hēmera](../../strongs/g/g2250.md), and have nothing to [phago](../../strongs/g/g5315.md):**

<a name="mark_8_3"></a>Mark 8:3

**And if I [apolyō](../../strongs/g/g630.md) them [nēstis](../../strongs/g/g3523.md) to their own [oikos](../../strongs/g/g3624.md), they will [eklyō](../../strongs/g/g1590.md) by [hodos](../../strongs/g/g3598.md): for divers of them [hēkō](../../strongs/g/g2240.md) from [makrothen](../../strongs/g/g3113.md).**

<a name="mark_8_4"></a>Mark 8:4

And his [mathētēs](../../strongs/g/g3101.md) [apokrinomai](../../strongs/g/g611.md) him, From whence can a [tis](../../strongs/g/g5100.md) [chortazō](../../strongs/g/g5526.md) these with [artos](../../strongs/g/g740.md) here in the [erēmia](../../strongs/g/g2047.md)?

<a name="mark_8_5"></a>Mark 8:5

And he [eperōtaō](../../strongs/g/g1905.md) them, **How many [artos](../../strongs/g/g740.md) have ye?** And they [eipon](../../strongs/g/g2036.md), [hepta](../../strongs/g/g2033.md).

<a name="mark_8_6"></a>Mark 8:6

And he [paraggellō](../../strongs/g/g3853.md) the [ochlos](../../strongs/g/g3793.md) to [anapiptō](../../strongs/g/g377.md) on the [gē](../../strongs/g/g1093.md): and he [lambanō](../../strongs/g/g2983.md) the [hepta](../../strongs/g/g2033.md) [artos](../../strongs/g/g740.md), and [eucharisteō](../../strongs/g/g2168.md), and [klaō](../../strongs/g/g2806.md), and [didōmi](../../strongs/g/g1325.md) to his [mathētēs](../../strongs/g/g3101.md) to [paratithēmi](../../strongs/g/g3908.md) them; and they did [paratithēmi](../../strongs/g/g3908.md) the [ochlos](../../strongs/g/g3793.md).

<a name="mark_8_7"></a>Mark 8:7

And they had a [oligos](../../strongs/g/g3641.md) [ichthydion](../../strongs/g/g2485.md): and he [eulogeō](../../strongs/g/g2127.md), and [eipon](../../strongs/g/g2036.md) to [paratithēmi](../../strongs/g/g3908.md) them also.

<a name="mark_8_8"></a>Mark 8:8

So they did [phago](../../strongs/g/g5315.md), and were [chortazō](../../strongs/g/g5526.md): and they [airō](../../strongs/g/g142.md) of the [klasma](../../strongs/g/g2801.md) that was [perisseuma](../../strongs/g/g4051.md) [hepta](../../strongs/g/g2033.md) [spyris](../../strongs/g/g4711.md).

<a name="mark_8_9"></a>Mark 8:9

And they that had [phago](../../strongs/g/g5315.md) were about four thousand: and he [apolyō](../../strongs/g/g630.md) them.

<a name="mark_8_10"></a>Mark 8:10

And [eutheōs](../../strongs/g/g2112.md) he [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md) with his [mathētēs](../../strongs/g/g3101.md), and [erchomai](../../strongs/g/g2064.md) into the [meros](../../strongs/g/g3313.md) of [Dalmanoutha](../../strongs/g/g1148.md).

<a name="mark_8_11"></a>Mark 8:11

And the [Pharisaios](../../strongs/g/g5330.md) [exerchomai](../../strongs/g/g1831.md), and [archomai](../../strongs/g/g756.md) to [syzēteō](../../strongs/g/g4802.md) with him, [zēteō](../../strongs/g/g2212.md) of him a [sēmeion](../../strongs/g/g4592.md) from [ouranos](../../strongs/g/g3772.md), [peirazō](../../strongs/g/g3985.md) him.

<a name="mark_8_12"></a>Mark 8:12

And he [anastenazō](../../strongs/g/g389.md) in his [pneuma](../../strongs/g/g4151.md), and [legō](../../strongs/g/g3004.md), **Why doth this [genea](../../strongs/g/g1074.md) [epizēteō](../../strongs/g/g1934.md) a [sēmeion](../../strongs/g/g4592.md)? [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, There shall no [sēmeion](../../strongs/g/g4592.md) be [didōmi](../../strongs/g/g1325.md) unto this [genea](../../strongs/g/g1074.md).**

<a name="mark_8_13"></a>Mark 8:13

And he [aphiēmi](../../strongs/g/g863.md) them, and [embainō](../../strongs/g/g1684.md) into the [ploion](../../strongs/g/g4143.md) again [aperchomai](../../strongs/g/g565.md) to the [peran](../../strongs/g/g4008.md).

<a name="mark_8_14"></a>Mark 8:14

Now had [epilanthanomai](../../strongs/g/g1950.md) to [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), neither had they in the [ploion](../../strongs/g/g4143.md) with them more than one [artos](../../strongs/g/g740.md).

<a name="mark_8_15"></a>Mark 8:15

And he [diastellō](../../strongs/g/g1291.md) them, [legō](../../strongs/g/g3004.md), **[horaō](../../strongs/g/g3708.md), [blepō](../../strongs/g/g991.md) of the [zymē](../../strongs/g/g2219.md) of the [Pharisaios](../../strongs/g/g5330.md), and of the [zymē](../../strongs/g/g2219.md) of [Hērōdēs](../../strongs/g/g2264.md).**

<a name="mark_8_16"></a>Mark 8:16

And they [dialogizomai](../../strongs/g/g1260.md) among [allēlōn](../../strongs/g/g240.md), [legō](../../strongs/g/g3004.md), because we have no [artos](../../strongs/g/g740.md).

<a name="mark_8_17"></a>Mark 8:17

And when [Iēsous](../../strongs/g/g2424.md) [ginōskō](../../strongs/g/g1097.md), he [legō](../../strongs/g/g3004.md) unto them, **Why [dialogizomai](../../strongs/g/g1260.md) ye, because ye have no [artos](../../strongs/g/g740.md)? [noeō](../../strongs/g/g3539.md) ye not yet, neither [syniēmi](../../strongs/g/g4920.md)? have ye your [kardia](../../strongs/g/g2588.md) yet [pōroō](../../strongs/g/g4456.md)?**

<a name="mark_8_18"></a>Mark 8:18

**Having [ophthalmos](../../strongs/g/g3788.md), [blepō](../../strongs/g/g991.md) ye not? and having [ous](../../strongs/g/g3775.md), [akouō](../../strongs/g/g191.md) ye not? and do ye not [mnēmoneuō](../../strongs/g/g3421.md)?**

<a name="mark_8_19"></a>Mark 8:19

**When I [klaō](../../strongs/g/g2806.md) the five [artos](../../strongs/g/g740.md) among five thousand, how many [kophinos](../../strongs/g/g2894.md) [plērēs](../../strongs/g/g4134.md) of [klasma](../../strongs/g/g2801.md) [airō](../../strongs/g/g142.md) ye?** They [legō](../../strongs/g/g3004.md) unto him, [dōdeka](../../strongs/g/g1427.md).

<a name="mark_8_20"></a>Mark 8:20

And when the [hepta](../../strongs/g/g2033.md) among four thousand, how many [spyris](../../strongs/g/g4711.md) [plērōma](../../strongs/g/g4138.md) of [klasma](../../strongs/g/g2801.md) [airō](../../strongs/g/g142.md) ye? And they [eipon](../../strongs/g/g2036.md), [hepta](../../strongs/g/g2033.md).

<a name="mark_8_21"></a>Mark 8:21

And he [legō](../../strongs/g/g3004.md) unto them, **How is it that ye do not [syniēmi](../../strongs/g/g4920.md)?**

<a name="mark_8_22"></a>Mark 8:22

And he [erchomai](../../strongs/g/g2064.md) to [Bēthsaïda](../../strongs/g/g966.md); and they [pherō](../../strongs/g/g5342.md) a [typhlos](../../strongs/g/g5185.md) unto him, and [parakaleō](../../strongs/g/g3870.md) him to [haptomai](../../strongs/g/g680.md) him.

<a name="mark_8_23"></a>Mark 8:23

And he [epilambanomai](../../strongs/g/g1949.md) the [typhlos](../../strongs/g/g5185.md) by the [cheir](../../strongs/g/g5495.md), and [exagō](../../strongs/g/g1806.md) him of the [kōmē](../../strongs/g/g2968.md); and when he had [ptyō](../../strongs/g/g4429.md) on his [omma](../../strongs/g/g3659.md), and [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) upon him, he [eperōtaō](../../strongs/g/g1905.md) him if he [blepō](../../strongs/g/g991.md).

<a name="mark_8_24"></a>Mark 8:24

And he [anablepō](../../strongs/g/g308.md), and [legō](../../strongs/g/g3004.md), I [blepō](../../strongs/g/g991.md) [anthrōpos](../../strongs/g/g444.md) as [horaō](../../strongs/g/g3708.md) [dendron](../../strongs/g/g1186.md), [peripateō](../../strongs/g/g4043.md).

<a name="mark_8_25"></a>Mark 8:25

After that he [epitithēmi](../../strongs/g/g2007.md) [cheir](../../strongs/g/g5495.md) again upon his [ophthalmos](../../strongs/g/g3788.md), and [poieō](../../strongs/g/g4160.md) him [anablepō](../../strongs/g/g308.md): and he was [apokathistēmi](../../strongs/g/g600.md), and [emblepō](../../strongs/g/g1689.md) [hapas](../../strongs/g/g537.md) [tēlaugōs](../../strongs/g/g5081.md).

<a name="mark_8_26"></a>Mark 8:26

And he [apostellō](../../strongs/g/g649.md) him to his [oikos](../../strongs/g/g3624.md), [legō](../../strongs/g/g3004.md), **Neither [eiserchomai](../../strongs/g/g1525.md) into the [kōmē](../../strongs/g/g2968.md), nor [eipon](../../strongs/g/g2036.md) to any in the [kōmē](../../strongs/g/g2968.md).**

<a name="mark_8_27"></a>Mark 8:27

And [Iēsous](../../strongs/g/g2424.md) [exerchomai](../../strongs/g/g1831.md), and his [mathētēs](../../strongs/g/g3101.md), into the [kōmē](../../strongs/g/g2968.md) of [Kaisareia](../../strongs/g/g2542.md) [Philippos](../../strongs/g/g5376.md): and by [hodos](../../strongs/g/g3598.md) he [eperōtaō](../../strongs/g/g1905.md) his [mathētēs](../../strongs/g/g3101.md), [legō](../../strongs/g/g3004.md) unto them, **Whom do [anthrōpos](../../strongs/g/g444.md) [legō](../../strongs/g/g3004.md) that I am?**

<a name="mark_8_28"></a>Mark 8:28

And they [apokrinomai](../../strongs/g/g611.md), [Iōannēs](../../strongs/g/g2491.md) the [baptistēs](../../strongs/g/g910.md); but some, [Ēlias](../../strongs/g/g2243.md); and others, One of the [prophētēs](../../strongs/g/g4396.md).

<a name="mark_8_29"></a>Mark 8:29

And he [legō](../../strongs/g/g3004.md) unto them, But whom [legō](../../strongs/g/g3004.md) ye that I am? And [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto him, Thou art the [Christos](../../strongs/g/g5547.md).

<a name="mark_8_30"></a>Mark 8:30

And he [epitimaō](../../strongs/g/g2008.md) them that they should [legō](../../strongs/g/g3004.md) [mēdeis](../../strongs/g/g3367.md) of him.

<a name="mark_8_31"></a>Mark 8:31

And he [archomai](../../strongs/g/g756.md) to [didaskō](../../strongs/g/g1321.md) them, that the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) must [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md), and be [apodokimazō](../../strongs/g/g593.md) of the [presbyteros](../../strongs/g/g4245.md), and of the [archiereus](../../strongs/g/g749.md), and [grammateus](../../strongs/g/g1122.md), and be [apokteinō](../../strongs/g/g615.md), and after three [hēmera](../../strongs/g/g2250.md) [anistēmi](../../strongs/g/g450.md).

<a name="mark_8_32"></a>Mark 8:32

And he [laleō](../../strongs/g/g2980.md) that [logos](../../strongs/g/g3056.md) [parrēsia](../../strongs/g/g3954.md). And [Petros](../../strongs/g/g4074.md) [proslambanō](../../strongs/g/g4355.md) him, and [archomai](../../strongs/g/g756.md) to [epitimaō](../../strongs/g/g2008.md) him.

<a name="mark_8_33"></a>Mark 8:33

But when he had [epistrephō](../../strongs/g/g1994.md) and [eidō](../../strongs/g/g1492.md) on his [mathētēs](../../strongs/g/g3101.md), he [epitimaō](../../strongs/g/g2008.md) [Petros](../../strongs/g/g4074.md), [legō](../../strongs/g/g3004.md), **[hypagō](../../strongs/g/g5217.md) thee [opisō](../../strongs/g/g3694.md) me, [Satanas](../../strongs/g/g4567.md): for thou [phroneō](../../strongs/g/g5426.md) not [theos](../../strongs/g/g2316.md), but [anthrōpos](../../strongs/g/g444.md).**

<a name="mark_8_34"></a>Mark 8:34

And when he had [proskaleō](../../strongs/g/g4341.md) the [ochlos](../../strongs/g/g3793.md) with his [mathētēs](../../strongs/g/g3101.md) also, he [eipon](../../strongs/g/g2036.md) unto them, **Whosoever will [erchomai](../../strongs/g/g2064.md) after me, let him [aparneomai](../../strongs/g/g533.md) himself, and [airō](../../strongs/g/g142.md) his [stauros](../../strongs/g/g4716.md), and [akoloutheō](../../strongs/g/g190.md) me.**

<a name="mark_8_35"></a>Mark 8:35

**For whosoever will [sōzō](../../strongs/g/g4982.md) his [psychē](../../strongs/g/g5590.md) shall [apollymi](../../strongs/g/g622.md) it; but whosoever shall [apollymi](../../strongs/g/g622.md) his [psychē](../../strongs/g/g5590.md) for my sake and the [euaggelion](../../strongs/g/g2098.md), the same shall [sōzō](../../strongs/g/g4982.md) it.**

<a name="mark_8_36"></a>Mark 8:36

**For what shall it [ōpheleō](../../strongs/g/g5623.md) an [anthrōpos](../../strongs/g/g444.md), if he shall [kerdainō](../../strongs/g/g2770.md) the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md), and [zēmioō](../../strongs/g/g2210.md) his own [psychē](../../strongs/g/g5590.md)?**

<a name="mark_8_37"></a>Mark 8:37

**Or what shall an [anthrōpos](../../strongs/g/g444.md) [didōmi](../../strongs/g/g1325.md) [antallagma](../../strongs/g/g465.md) for his [psychē](../../strongs/g/g5590.md)?**

<a name="mark_8_38"></a>Mark 8:38

**Whosoever therefore shall be [epaischynomai](../../strongs/g/g1870.md) of me and of my [logos](../../strongs/g/g3056.md) in this [moichalis](../../strongs/g/g3428.md) and [hamartōlos](../../strongs/g/g268.md) [genea](../../strongs/g/g1074.md); of him also shall the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) be [epaischynomai](../../strongs/g/g1870.md), when he [erchomai](../../strongs/g/g2064.md) in the [doxa](../../strongs/g/g1391.md) of his [patēr](../../strongs/g/g3962.md) with the [hagios](../../strongs/g/g40.md) [aggelos](../../strongs/g/g32.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 7](mark_7.md) - [Mark 9](mark_9.md)