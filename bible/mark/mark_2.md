# [Mark 2](https://www.blueletterbible.org/kjv/mar/2/7/rl1/s_958001)

<a name="mark_2_1"></a>Mark 2:1

And [palin](../../strongs/g/g3825.md) he [eiserchomai](../../strongs/g/g1525.md) into [Kapharnaoum](../../strongs/g/g2584.md) after [hēmera](../../strongs/g/g2250.md); and it was [akouō](../../strongs/g/g191.md) that he was in the [oikos](../../strongs/g/g3624.md).

<a name="mark_2_2"></a>Mark 2:2

And [eutheōs](../../strongs/g/g2112.md) [polys](../../strongs/g/g4183.md) were [synagō](../../strongs/g/g4863.md), insomuch that there was [mēketi](../../strongs/g/g3371.md) [chōreō](../../strongs/g/g5562.md), [mēde](../../strongs/g/g3366.md) as about the [thyra](../../strongs/g/g2374.md): and he [laleō](../../strongs/g/g2980.md) the [logos](../../strongs/g/g3056.md) unto them.

<a name="mark_2_3"></a>Mark 2:3

And they [erchomai](../../strongs/g/g2064.md) unto him, [pherō](../../strongs/g/g5342.md) [paralytikos](../../strongs/g/g3885.md), [airō](../../strongs/g/g142.md) of four.

<a name="mark_2_4"></a>Mark 2:4

And when they could not [prosengizō](../../strongs/g/g4331.md) unto him for the [ochlos](../../strongs/g/g3793.md), they [apostegazō](../../strongs/g/g648.md) the [stegē](../../strongs/g/g4721.md) where he was: and when they had [exoryssō](../../strongs/g/g1846.md), they [chalaō](../../strongs/g/g5465.md) the [krabattos](../../strongs/g/g2895.md) wherein the [paralytikos](../../strongs/g/g3885.md) [katakeimai](../../strongs/g/g2621.md).

<a name="mark_2_5"></a>Mark 2:5

When [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) their [pistis](../../strongs/g/g4102.md), he [legō](../../strongs/g/g3004.md) unto the [paralytikos](../../strongs/g/g3885.md), **[teknon](../../strongs/g/g5043.md), thy [hamartia](../../strongs/g/g266.md) [aphiēmi](../../strongs/g/g863.md) thee.**

<a name="mark_2_6"></a>Mark 2:6

But there was certain of the [grammateus](../../strongs/g/g1122.md) [kathēmai](../../strongs/g/g2521.md) there, and [dialogizomai](../../strongs/g/g1260.md) in their [kardia](../../strongs/g/g2588.md),

<a name="mark_2_7"></a>Mark 2:7

Why doth this thus [laleō](../../strongs/g/g2980.md) [blasphēmia](../../strongs/g/g988.md)? who can [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md) but [theos](../../strongs/g/g2316.md) only?

<a name="mark_2_8"></a>Mark 2:8

And [eutheōs](../../strongs/g/g2112.md) when [Iēsous](../../strongs/g/g2424.md) [epiginōskō](../../strongs/g/g1921.md) in his [pneuma](../../strongs/g/g4151.md) that they so [dialogizomai](../../strongs/g/g1260.md) within themselves, he [eipon](../../strongs/g/g2036.md) unto them, **Why [dialogizomai](../../strongs/g/g1260.md) ye these things in your [kardia](../../strongs/g/g2588.md)?**

<a name="mark_2_9"></a>Mark 2:9

**Whether is it [eukopos](../../strongs/g/g2123.md) to [eipon](../../strongs/g/g2036.md) to the [paralytikos](../../strongs/g/g3885.md), Thy [hamartia](../../strongs/g/g266.md) [aphiēmi](../../strongs/g/g863.md) thee; or to [eipon](../../strongs/g/g2036.md), [egeirō](../../strongs/g/g1453.md), and [airō](../../strongs/g/g142.md) thy [krabattos](../../strongs/g/g2895.md), and [peripateō](../../strongs/g/g4043.md)?**

<a name="mark_2_10"></a>Mark 2:10

**But that ye may [eidō](../../strongs/g/g1492.md) that the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) hath [exousia](../../strongs/g/g1849.md) on [gē](../../strongs/g/g1093.md) to [aphiēmi](../../strongs/g/g863.md) [hamartia](../../strongs/g/g266.md),** (he [legō](../../strongs/g/g3004.md) to [paralytikos](../../strongs/g/g3885.md),)

<a name="mark_2_11"></a>Mark 2:11

**I [legō](../../strongs/g/g3004.md) unto thee, [egeirō](../../strongs/g/g1453.md), and [airō](../../strongs/g/g142.md) thy [krabattos](../../strongs/g/g2895.md), and [hypagō](../../strongs/g/g5217.md) into thine [oikos](../../strongs/g/g3624.md).**

<a name="mark_2_12"></a>Mark 2:12

And [eutheōs](../../strongs/g/g2112.md) he [egeirō](../../strongs/g/g1453.md), [airō](../../strongs/g/g142.md) the [krabattos](../../strongs/g/g2895.md), and [exerchomai](../../strongs/g/g1831.md) [enantion](../../strongs/g/g1726.md) them all; insomuch that they were all [existēmi](../../strongs/g/g1839.md), and [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), [legō](../../strongs/g/g3004.md), We never [eidō](../../strongs/g/g1492.md) it [houtō(s)](../../strongs/g/g3779.md).

<a name="mark_2_13"></a>Mark 2:13

And he [exerchomai](../../strongs/g/g1831.md) again by the [thalassa](../../strongs/g/g2281.md); and all the [ochlos](../../strongs/g/g3793.md) [erchomai](../../strongs/g/g2064.md) unto him, and he [didaskō](../../strongs/g/g1321.md) them.

<a name="mark_2_14"></a>Mark 2:14

And as he [paragō](../../strongs/g/g3855.md), he [eidō](../../strongs/g/g1492.md) [Leuis](../../strongs/g/g3018.md) of [Alphaios](../../strongs/g/g256.md) [kathēmai](../../strongs/g/g2521.md) at the [telōnion](../../strongs/g/g5058.md), and [legō](../../strongs/g/g3004.md) unto him, **[akoloutheō](../../strongs/g/g190.md) me**. And he [anistēmi](../../strongs/g/g450.md) and [akoloutheō](../../strongs/g/g190.md) him.

<a name="mark_2_15"></a>Mark 2:15

And [ginomai](../../strongs/g/g1096.md), that, as [autos](../../strongs/g/g846.md) [katakeimai](../../strongs/g/g2621.md) in his [oikia](../../strongs/g/g3614.md), many [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md) [synanakeimai](../../strongs/g/g4873.md) also with [Iēsous](../../strongs/g/g2424.md) and his [mathētēs](../../strongs/g/g3101.md): for there were [polys](../../strongs/g/g4183.md), and they [akoloutheō](../../strongs/g/g190.md) him.

<a name="mark_2_16"></a>Mark 2:16

And when the [grammateus](../../strongs/g/g1122.md) and [Pharisaios](../../strongs/g/g5330.md) [eidō](../../strongs/g/g1492.md) him [esthiō](../../strongs/g/g2068.md) with [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md), they [legō](../../strongs/g/g3004.md) unto his [mathētēs](../../strongs/g/g3101.md), How is it that he [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) with [telōnēs](../../strongs/g/g5057.md) and [hamartōlos](../../strongs/g/g268.md)? [^1]

<a name="mark_2_17"></a>Mark 2:17

When [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md), he [legō](../../strongs/g/g3004.md) unto them, **They that are [ischyō](../../strongs/g/g2480.md) have no [chreia](../../strongs/g/g5532.md) of the [iatros](../../strongs/g/g2395.md), but they that are [kakōs](../../strongs/g/g2560.md): I [erchomai](../../strongs/g/g2064.md) not to [kaleō](../../strongs/g/g2564.md) the [dikaios](../../strongs/g/g1342.md), but [hamartōlos](../../strongs/g/g268.md) to [metanoia](../../strongs/g/g3341.md).**

<a name="mark_2_18"></a>Mark 2:18

And the [mathētēs](../../strongs/g/g3101.md) of [Iōannēs](../../strongs/g/g2491.md) and of the [Pharisaios](../../strongs/g/g5330.md) used to [nēsteuō](../../strongs/g/g3522.md): and they [erchomai](../../strongs/g/g2064.md) and [legō](../../strongs/g/g3004.md) unto him, Why do the [mathētēs](../../strongs/g/g3101.md) of [Iōannēs](../../strongs/g/g2491.md) and of the [Pharisaios](../../strongs/g/g5330.md) [nēsteuō](../../strongs/g/g3522.md), but thy [mathētēs](../../strongs/g/g3101.md) [nēsteuō](../../strongs/g/g3522.md) not?

<a name="mark_2_19"></a>Mark 2:19

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Can the [huios](../../strongs/g/g5207.md) of the [nymphōn](../../strongs/g/g3567.md) [nēsteuō](../../strongs/g/g3522.md), while the [nymphios](../../strongs/g/g3566.md) is with them? [hosos](../../strongs/g/g3745.md) [chronos](../../strongs/g/g5550.md) they have the [nymphios](../../strongs/g/g3566.md) with them, they cannot [nēsteuō](../../strongs/g/g3522.md).**

<a name="mark_2_20"></a>Mark 2:20

**But the [hēmera](../../strongs/g/g2250.md) will [erchomai](../../strongs/g/g2064.md), when the [nymphios](../../strongs/g/g3566.md) shall be [apairō](../../strongs/g/g522.md) from them, and then shall they [nēsteuō](../../strongs/g/g3522.md) in those [hēmera](../../strongs/g/g2250.md).**

<a name="mark_2_21"></a>Mark 2:21

**No man also [epiraptō](../../strongs/g/g1976.md) an [epiblēma](../../strongs/g/g1915.md) of [agnaphos](../../strongs/g/g46.md) [rhakos](../../strongs/g/g4470.md) on a [palaios](../../strongs/g/g3820.md) [himation](../../strongs/g/g2440.md): else the [kainos](../../strongs/g/g2537.md) that [plērōma](../../strongs/g/g4138.md) it [airō](../../strongs/g/g142.md) from the [palaios](../../strongs/g/g3820.md), and the [schisma](../../strongs/g/g4978.md) is made [cheirōn](../../strongs/g/g5501.md).**

<a name="mark_2_22"></a>Mark 2:22

**And [oudeis](../../strongs/g/g3762.md) [ballō](../../strongs/g/g906.md) [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) into [palaios](../../strongs/g/g3820.md) [askos](../../strongs/g/g779.md): else the [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) doth [rhēgnymi](../../strongs/g/g4486.md) the [askos](../../strongs/g/g779.md), and the [oinos](../../strongs/g/g3631.md) is [ekcheō](../../strongs/g/g1632.md), and the [askos](../../strongs/g/g779.md) will be [apollymi](../../strongs/g/g622.md): but [neos](../../strongs/g/g3501.md) [oinos](../../strongs/g/g3631.md) must be [blēteos](../../strongs/g/g992.md) into [kainos](../../strongs/g/g2537.md) [askos](../../strongs/g/g779.md).**

<a name="mark_2_23"></a>Mark 2:23

And [ginomai](../../strongs/g/g1096.md), that he [paraporeuomai](../../strongs/g/g3899.md) through the [sporimos](../../strongs/g/g4702.md) on the [sabbaton](../../strongs/g/g4521.md); and his [mathētēs](../../strongs/g/g3101.md) [archomai](../../strongs/g/g756.md), as they [hodos](../../strongs/g/g3598.md) [poieō](../../strongs/g/g4160.md), to [tillō](../../strongs/g/g5089.md) the [stachys](../../strongs/g/g4719.md).

<a name="mark_2_24"></a>Mark 2:24

And the [Pharisaios](../../strongs/g/g5330.md) [legō](../../strongs/g/g3004.md) unto him, [ide](../../strongs/g/g2396.md), why [poieō](../../strongs/g/g4160.md) they on the [sabbaton](../../strongs/g/g4521.md) that which is not [exesti](../../strongs/g/g1832.md)?

<a name="mark_2_25"></a>Mark 2:25

And he [legō](../../strongs/g/g3004.md) unto them, **Have ye never [anaginōskō](../../strongs/g/g314.md) what [Dabid](../../strongs/g/g1138.md) [poieō](../../strongs/g/g4160.md), when he had [chreia](../../strongs/g/g5532.md), and was [peinaō](../../strongs/g/g3983.md), he, and they that were with him?**

<a name="mark_2_26"></a>Mark 2:26

**How he [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md) [epi](../../strongs/g/g1909.md) [Abiathar](../../strongs/g/g8.md) the [archiereus](../../strongs/g/g749.md), and did [phago](../../strongs/g/g5315.md) the [artos](../../strongs/g/g740.md) [prothesis](../../strongs/g/g4286.md), which is not [exesti](../../strongs/g/g1832.md) to [phago](../../strongs/g/g5315.md) but for the [hiereus](../../strongs/g/g2409.md), and [didōmi](../../strongs/g/g1325.md) also to them which were with him?** [^2]

<a name="mark_2_27"></a>Mark 2:27

And he [legō](../../strongs/g/g3004.md) unto them, **The [sabbaton](../../strongs/g/g4521.md) was [ginomai](../../strongs/g/g1096.md) for [anthrōpos](../../strongs/g/g444.md), and not [anthrōpos](../../strongs/g/g444.md) for the [sabbaton](../../strongs/g/g4521.md):**

<a name="mark_2_28"></a>Mark 2:28

**Therefore the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [kyrios](../../strongs/g/g2962.md) also of the [sabbaton](../../strongs/g/g4521.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 1](mark_1.md) - [Mark 3](mark_3.md)

---

[^1]: [Mark 2:16 Commentary](../../commentary/mark/mark_2_commentary.md#mark_2_16)

[^2]: [Mark 2:26 Commentary](../../commentary/mark/mark_2_commentary.md#mark_2_26)
