# [Mark 10](https://www.blueletterbible.org/kjv/mar/10/1/rl1/s_966001)

<a name="mark_10_1"></a>Mark 10:1

[kakeithen](../../strongs/g/g2547.md) he [anistēmi](../../strongs/g/g450.md), and [erchomai](../../strongs/g/g2064.md) into the [horion](../../strongs/g/g3725.md) of [Ioudaia](../../strongs/g/g2449.md) by the [peran](../../strongs/g/g4008.md) of [Iordanēs](../../strongs/g/g2446.md): and the [ochlos](../../strongs/g/g3793.md) [symporeuomai](../../strongs/g/g4848.md) unto him again; and, as [ethō](../../strongs/g/g1486.md), he [didaskō](../../strongs/g/g1321.md) them again.

<a name="mark_10_2"></a>Mark 10:2

And the [Pharisaios](../../strongs/g/g5330.md) [proserchomai](../../strongs/g/g4334.md) to him, and [eperōtaō](../../strongs/g/g1905.md) him, Is it [exesti](../../strongs/g/g1832.md) for an [anēr](../../strongs/g/g435.md) to [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md)? [peirazō](../../strongs/g/g3985.md) him.

<a name="mark_10_3"></a>Mark 10:3

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **What did [Mōÿsēs](../../strongs/g/g3475.md) [entellō](../../strongs/g/g1781.md) you?**

<a name="mark_10_4"></a>Mark 10:4

And they [eipon](../../strongs/g/g2036.md), [Mōÿsēs](../../strongs/g/g3475.md) [epitrepō](../../strongs/g/g2010.md) to [graphō](../../strongs/g/g1125.md) a [biblion](../../strongs/g/g975.md) of [apostasion](../../strongs/g/g647.md), and [apolyō](../../strongs/g/g630.md).

<a name="mark_10_5"></a>Mark 10:5

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **For the [sklērokardia](../../strongs/g/g4641.md) he [graphō](../../strongs/g/g1125.md) you this [entolē](../../strongs/g/g1785.md).**

<a name="mark_10_6"></a>Mark 10:6

**But from the [archē](../../strongs/g/g746.md) of the [ktisis](../../strongs/g/g2937.md) [theos](../../strongs/g/g2316.md) [poieō](../../strongs/g/g4160.md) them [arrēn](../../strongs/g/g730.md) and [thēlys](../../strongs/g/g2338.md).**

<a name="mark_10_7"></a>Mark 10:7

**For this cause shall an [anthrōpos](../../strongs/g/g444.md) [kataleipō](../../strongs/g/g2641.md) his [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md), and [proskollaō](../../strongs/g/g4347.md) to his [gynē](../../strongs/g/g1135.md);**

<a name="mark_10_8"></a>Mark 10:8

**And they [dyo](../../strongs/g/g1417.md) shall be one [sarx](../../strongs/g/g4561.md): so then they are no more [dyo](../../strongs/g/g1417.md), but one [sarx](../../strongs/g/g4561.md).**

<a name="mark_10_9"></a>Mark 10:9

**What therefore [theos](../../strongs/g/g2316.md) hath [syzeugnymi](../../strongs/g/g4801.md), let not [anthrōpos](../../strongs/g/g444.md) [chōrizō](../../strongs/g/g5563.md).**

<a name="mark_10_10"></a>Mark 10:10

And in the [oikia](../../strongs/g/g3614.md) his [mathētēs](../../strongs/g/g3101.md) [eperōtaō](../../strongs/g/g1905.md) him again of the same.

<a name="mark_10_11"></a>Mark 10:11

And he [legō](../../strongs/g/g3004.md) unto them, **Whosoever shall [apolyō](../../strongs/g/g630.md) his [gynē](../../strongs/g/g1135.md), and [gameō](../../strongs/g/g1060.md) another, [moichaō](../../strongs/g/g3429.md) against her.**

<a name="mark_10_12"></a>Mark 10:12

**And if a [gynē](../../strongs/g/g1135.md) shall [apolyō](../../strongs/g/g630.md) her [anēr](../../strongs/g/g435.md), and be [gameō](../../strongs/g/g1060.md) to another, she [moichaō](../../strongs/g/g3429.md).**

<a name="mark_10_13"></a>Mark 10:13

And they [prospherō](../../strongs/g/g4374.md) [paidion](../../strongs/g/g3813.md) to him, that he should [haptomai](../../strongs/g/g680.md) them: and his [mathētēs](../../strongs/g/g3101.md) [epitimaō](../../strongs/g/g2008.md) those that [prospherō](../../strongs/g/g4374.md) them.

<a name="mark_10_14"></a>Mark 10:14

But when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md), he was [aganakteō](../../strongs/g/g23.md), and [eipon](../../strongs/g/g2036.md) unto them, **[aphiēmi](../../strongs/g/g863.md) the [paidion](../../strongs/g/g3813.md) to [erchomai](../../strongs/g/g2064.md) unto me, and [kōlyō](../../strongs/g/g2967.md) them not: for of such is the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="mark_10_15"></a>Mark 10:15

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Whosoever shall not [dechomai](../../strongs/g/g1209.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) as a [paidion](../../strongs/g/g3813.md), he shall not [eiserchomai](../../strongs/g/g1525.md) therein.**

<a name="mark_10_16"></a>Mark 10:16

And he [enagkalizomai](../../strongs/g/g1723.md), [tithēmi](../../strongs/g/g5087.md) his [cheir](../../strongs/g/g5495.md) upon them, and [eulogeō](../../strongs/g/g2127.md) them.

<a name="mark_10_17"></a>Mark 10:17

And when he was [ekporeuomai](../../strongs/g/g1607.md) into [hodos](../../strongs/g/g3598.md), there one [prostrechō](../../strongs/g/g4370.md), and [gonypeteō](../../strongs/g/g1120.md) to him, and [eperōtaō](../../strongs/g/g1905.md) him, [agathos](../../strongs/g/g18.md) [didaskalos](../../strongs/g/g1320.md), what shall I [poieō](../../strongs/g/g4160.md) that I may [klēronomeō](../../strongs/g/g2816.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)?

<a name="mark_10_18"></a>Mark 10:18

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Why [legō](../../strongs/g/g3004.md) thou me [agathos](../../strongs/g/g18.md)? none [agathos](../../strongs/g/g18.md) but one, [theos](../../strongs/g/g2316.md).**

<a name="mark_10_19"></a>Mark 10:19

**Thou [eidō](../../strongs/g/g1492.md) the [entolē](../../strongs/g/g1785.md), Do not [moicheuō](../../strongs/g/g3431.md), Do not [phoneuō](../../strongs/g/g5407.md), Do not [kleptō](../../strongs/g/g2813.md), Do not [pseudomartyreō](../../strongs/g/g5576.md), [apostereō](../../strongs/g/g650.md) not, [timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and [mētēr](../../strongs/g/g3384.md).**

<a name="mark_10_20"></a>Mark 10:20

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto him, [didaskalos](../../strongs/g/g1320.md), all these have I [phylassō](../../strongs/g/g5442.md) from my [neotēs](../../strongs/g/g3503.md).

<a name="mark_10_21"></a>Mark 10:21

Then [Iēsous](../../strongs/g/g2424.md) [emblepō](../../strongs/g/g1689.md) him [agapaō](../../strongs/g/g25.md) him, and [eipon](../../strongs/g/g2036.md) unto him, **One thing thou [hystereō](../../strongs/g/g5302.md): [hypagō](../../strongs/g/g5217.md), [pōleō](../../strongs/g/g4453.md) whatsoever thou hast, and [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md), and thou shalt have [thēsauros](../../strongs/g/g2344.md) in [ouranos](../../strongs/g/g3772.md): and [deuro](../../strongs/g/g1204.md), [airō](../../strongs/g/g142.md) the [stauros](../../strongs/g/g4716.md), and [akoloutheō](../../strongs/g/g190.md) me.**

<a name="mark_10_22"></a>Mark 10:22

And he was [stygnazō](../../strongs/g/g4768.md) at that [logos](../../strongs/g/g3056.md), and [aperchomai](../../strongs/g/g565.md) [lypeō](../../strongs/g/g3076.md): for he had [polys](../../strongs/g/g4183.md) [ktēma](../../strongs/g/g2933.md).

<a name="mark_10_23"></a>Mark 10:23

And [Iēsous](../../strongs/g/g2424.md) [periblepō](../../strongs/g/g4017.md), and [legō](../../strongs/g/g3004.md) unto his [mathētēs](../../strongs/g/g3101.md), **How [dyskolōs](../../strongs/g/g1423.md) shall they that have [chrēma](../../strongs/g/g5536.md) [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)!**

<a name="mark_10_24"></a>Mark 10:24

And the [mathētēs](../../strongs/g/g3101.md) were [thambeō](../../strongs/g/g2284.md) at his [logos](../../strongs/g/g3056.md). But [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) again, and [legō](../../strongs/g/g3004.md) unto them, **[teknon](../../strongs/g/g5043.md), how [dyskolos](../../strongs/g/g1422.md) is it for them that [peithō](../../strongs/g/g3982.md) in [chrēma](../../strongs/g/g5536.md) to [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)!** [^1]

<a name="mark_10_25"></a>Mark 10:25

**It is [eukopos](../../strongs/g/g2123.md) for a [kamēlos](../../strongs/g/g2574.md) to [eiserchomai](../../strongs/g/g1525.md) [dierchomai](../../strongs/g/g1330.md) through the [trymalia](../../strongs/g/g5168.md) of a [rhaphis](../../strongs/g/g4476.md), than for a [plousios](../../strongs/g/g4145.md) to [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="mark_10_26"></a>Mark 10:26

And they were [ekplēssō](../../strongs/g/g1605.md) [perissōs](../../strongs/g/g4057.md), [legō](../../strongs/g/g3004.md) among themselves, Who then can be [sōzō](../../strongs/g/g4982.md)?

<a name="mark_10_27"></a>Mark 10:27

And [Iēsous](../../strongs/g/g2424.md) [emblepō](../../strongs/g/g1689.md) them [legō](../../strongs/g/g3004.md), **With [anthrōpos](../../strongs/g/g444.md) [adynatos](../../strongs/g/g102.md), but not with [theos](../../strongs/g/g2316.md): for with [theos](../../strongs/g/g2316.md) all things are [dynatos](../../strongs/g/g1415.md).**

<a name="mark_10_28"></a>Mark 10:28

Then [Petros](../../strongs/g/g4074.md) [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto him, [idou](../../strongs/g/g2400.md), we have [aphiēmi](../../strongs/g/g863.md) all, and have [akoloutheō](../../strongs/g/g190.md) thee.

<a name="mark_10_29"></a>Mark 10:29

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, There is [oudeis](../../strongs/g/g3762.md) that hath [aphiēmi](../../strongs/g/g863.md) [oikia](../../strongs/g/g3614.md), or [adelphos](../../strongs/g/g80.md), or [adelphē](../../strongs/g/g79.md), or [patēr](../../strongs/g/g3962.md), or [mētēr](../../strongs/g/g3384.md), or [gynē](../../strongs/g/g1135.md), or [teknon](../../strongs/g/g5043.md), or [agros](../../strongs/g/g68.md), for my sake, and the [euaggelion](../../strongs/g/g2098.md),**

<a name="mark_10_30"></a>Mark 10:30

**But he shall [lambanō](../../strongs/g/g2983.md) an [hekatontaplasiōn](../../strongs/g/g1542.md) now in this [kairos](../../strongs/g/g2540.md), [oikia](../../strongs/g/g3614.md), and [adelphos](../../strongs/g/g80.md), and [adelphē](../../strongs/g/g79.md), and [mētēr](../../strongs/g/g3384.md), and [teknon](../../strongs/g/g5043.md), and [agros](../../strongs/g/g68.md), with [diōgmos](../../strongs/g/g1375.md); and in the [aiōn](../../strongs/g/g165.md) to [erchomai](../../strongs/g/g2064.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).**

<a name="mark_10_31"></a>Mark 10:31

But [polys](../../strongs/g/g4183.md) [prōtos](../../strongs/g/g4413.md) shall be [eschatos](../../strongs/g/g2078.md); and the [eschatos](../../strongs/g/g2078.md) [prōtos](../../strongs/g/g4413.md).

<a name="mark_10_32"></a>Mark 10:32

And they were in [hodos](../../strongs/g/g3598.md) [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md); and [Iēsous](../../strongs/g/g2424.md) [proagō](../../strongs/g/g4254.md) them: and they were [thambeō](../../strongs/g/g2284.md); and as they [akoloutheō](../../strongs/g/g190.md), they were [phobeō](../../strongs/g/g5399.md). And he [paralambanō](../../strongs/g/g3880.md) again the [dōdeka](../../strongs/g/g1427.md), and [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) them what things should [symbainō](../../strongs/g/g4819.md) unto him,

<a name="mark_10_33"></a>Mark 10:33

**[idou](../../strongs/g/g2400.md), we [anabainō](../../strongs/g/g305.md) to [Hierosolyma](../../strongs/g/g2414.md); and the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) shall be [paradidōmi](../../strongs/g/g3860.md) unto the [archiereus](../../strongs/g/g749.md), and unto the [grammateus](../../strongs/g/g1122.md); and they shall [katakrinō](../../strongs/g/g2632.md) him to [thanatos](../../strongs/g/g2288.md), and shall [paradidōmi](../../strongs/g/g3860.md) him to the [ethnos](../../strongs/g/g1484.md):**

<a name="mark_10_34"></a>Mark 10:34

**And they shall [empaizō](../../strongs/g/g1702.md) him, and shall [mastigoō](../../strongs/g/g3146.md) him, and shall [emptyō](../../strongs/g/g1716.md) upon him, and shall [apokteinō](../../strongs/g/g615.md) him: and the third [hēmera](../../strongs/g/g2250.md) he shall [anistēmi](../../strongs/g/g450.md).**

<a name="mark_10_35"></a>Mark 10:35

And [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md), the [huios](../../strongs/g/g5207.md) of [Zebedaios](../../strongs/g/g2199.md), [prosporeuomai](../../strongs/g/g4365.md) unto him, [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), we would that thou shouldest [poieō](../../strongs/g/g4160.md) for us whatsoever we shall [aiteō](../../strongs/g/g154.md).

<a name="mark_10_36"></a>Mark 10:36

And he [eipon](../../strongs/g/g2036.md) unto them, **What would ye that I should [poieō](../../strongs/g/g4160.md) for you?**

<a name="mark_10_37"></a>Mark 10:37

They [eipon](../../strongs/g/g2036.md) unto him, [didōmi](../../strongs/g/g1325.md) unto us that we may [kathizō](../../strongs/g/g2523.md), one on thy [dexios](../../strongs/g/g1188.md), and the other on thy [euōnymos](../../strongs/g/g2176.md), in thy [doxa](../../strongs/g/g1391.md).

<a name="mark_10_38"></a>Mark 10:38

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Ye [eidō](../../strongs/g/g1492.md) not what ye [aiteō](../../strongs/g/g154.md): can ye [pinō](../../strongs/g/g4095.md) of the [potērion](../../strongs/g/g4221.md) that I [pinō](../../strongs/g/g4095.md) of? and be [baptizō](../../strongs/g/g907.md) with the [baptisma](../../strongs/g/g908.md) that I am [baptizō](../../strongs/g/g907.md) with?**

<a name="mark_10_39"></a>Mark 10:39

And they [eipon](../../strongs/g/g2036.md) unto him, We [dynamai](../../strongs/g/g1410.md). And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto them, **Ye shall indeed [pinō](../../strongs/g/g4095.md) of the [potērion](../../strongs/g/g4221.md) that I [pinō](../../strongs/g/g4095.md) of; and with the [baptisma](../../strongs/g/g908.md) that I am [baptizō](../../strongs/g/g907.md) withal shall ye be [baptizō](../../strongs/g/g907.md):**

<a name="mark_10_40"></a>Mark 10:40

**But to [kathizō](../../strongs/g/g2523.md) on my [dexios](../../strongs/g/g1188.md) and on my [euōnymos](../../strongs/g/g2176.md) is not mine to [didōmi](../../strongs/g/g1325.md); but for whom it is [hetoimazō](../../strongs/g/g2090.md).**

<a name="mark_10_41"></a>Mark 10:41

And when the ten [akouō](../../strongs/g/g191.md), they [archomai](../../strongs/g/g756.md) to be [aganakteō](../../strongs/g/g23.md) with [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md).

<a name="mark_10_42"></a>Mark 10:42

But [Iēsous](../../strongs/g/g2424.md) [proskaleō](../../strongs/g/g4341.md) them, and [legō](../../strongs/g/g3004.md) unto them, **Ye [eidō](../../strongs/g/g1492.md) that they [dokeō](../../strongs/g/g1380.md) to [archō](../../strongs/g/g757.md) the [ethnos](../../strongs/g/g1484.md) [katakyrieuō](../../strongs/g/g2634.md) over them; and their [megas](../../strongs/g/g3173.md) [katexousiazō](../../strongs/g/g2715.md) upon them.**

<a name="mark_10_43"></a>Mark 10:43

**But so shall it not be among you: but whosoever will be [megas](../../strongs/g/g3173.md) among you, shall be your [diakonos](../../strongs/g/g1249.md):**

<a name="mark_10_44"></a>Mark 10:44

**And whosoever of you will be the [prōtos](../../strongs/g/g4413.md), shall be [doulos](../../strongs/g/g1401.md) of all.**

<a name="mark_10_45"></a>Mark 10:45

**For even the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [erchomai](../../strongs/g/g2064.md) not to be [diakoneō](../../strongs/g/g1247.md) unto, but to [diakoneō](../../strongs/g/g1247.md), and to [didōmi](../../strongs/g/g1325.md) his [psychē](../../strongs/g/g5590.md) a [lytron](../../strongs/g/g3083.md) for [polys](../../strongs/g/g4183.md).**

<a name="mark_10_46"></a>Mark 10:46

And they [erchomai](../../strongs/g/g2064.md) to [Ierichō](../../strongs/g/g2410.md): and as he [ekporeuomai](../../strongs/g/g1607.md) out of [Ierichō](../../strongs/g/g2410.md) with his [mathētēs](../../strongs/g/g3101.md) and a [hikanos](../../strongs/g/g2425.md) [ochlos](../../strongs/g/g3793.md), [typhlos](../../strongs/g/g5185.md) [Bartimaios](../../strongs/g/g924.md), the [huios](../../strongs/g/g5207.md) of [Timaios](../../strongs/g/g5090.md), [kathēmai](../../strongs/g/g2521.md) by the [hodos](../../strongs/g/g3598.md) [prosaiteō](../../strongs/g/g4319.md).

<a name="mark_10_47"></a>Mark 10:47

And when he [akouō](../../strongs/g/g191.md) that it was [Iēsous](../../strongs/g/g2424.md) of [Nazōraios](../../strongs/g/g3480.md), he [archomai](../../strongs/g/g756.md) to [krazō](../../strongs/g/g2896.md), and [legō](../../strongs/g/g3004.md), [Iēsous](../../strongs/g/g2424.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), [eleeō](../../strongs/g/g1653.md) on me.

<a name="mark_10_48"></a>Mark 10:48

And many [epitimaō](../../strongs/g/g2008.md) him that [siōpaō](../../strongs/g/g4623.md): but he [krazō](../../strongs/g/g2896.md) the [mallon](../../strongs/g/g3123.md) [polys](../../strongs/g/g4183.md), [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md), [eleeō](../../strongs/g/g1653.md) on me.

<a name="mark_10_49"></a>Mark 10:49

And [Iēsous](../../strongs/g/g2424.md) [histēmi](../../strongs/g/g2476.md), and [eipon](../../strongs/g/g2036.md) him to be [phōneō](../../strongs/g/g5455.md). And they [phōneō](../../strongs/g/g5455.md) the [typhlos](../../strongs/g/g5185.md), [legō](../../strongs/g/g3004.md) unto him, [tharseō](../../strongs/g/g2293.md), [egeirō](../../strongs/g/g1453.md); he [phōneō](../../strongs/g/g5455.md) thee.

<a name="mark_10_50"></a>Mark 10:50

And he, [apoballō](../../strongs/g/g577.md) his [himation](../../strongs/g/g2440.md), [anistēmi](../../strongs/g/g450.md), and [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md).

<a name="mark_10_51"></a>Mark 10:51

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto him, **What wilt thou that I should [poieō](../../strongs/g/g4160.md) unto thee?** The [typhlos](../../strongs/g/g5185.md) [eipon](../../strongs/g/g2036.md) unto him, [rhabbouni](../../strongs/g/g4462.md), that I might [anablepō](../../strongs/g/g308.md).

<a name="mark_10_52"></a>Mark 10:52

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **[hypagō](../../strongs/g/g5217.md); thy [pistis](../../strongs/g/g4102.md) hath made thee [sōzō](../../strongs/g/g4982.md).** And [eutheōs](../../strongs/g/g2112.md) he [anablepō](../../strongs/g/g308.md), and [akoloutheō](../../strongs/g/g190.md) [Iēsous](../../strongs/g/g2424.md) in [hodos](../../strongs/g/g3598.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 9](mark_9.md) - [Mark 11](mark_11.md)

---

[^1]: [Mark 10:24 Commentary](../../commentary/mark/mark_10_commentary.md#mark_10_24)
