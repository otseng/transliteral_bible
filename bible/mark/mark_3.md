# [Mark 3](https://www.blueletterbible.org/kjv/mar/3/7/rl1/s_958001)

<a name="mark_3_1"></a>Mark 3:1

And he [eiserchomai](../../strongs/g/g1525.md) [palin](../../strongs/g/g3825.md) into the [synagōgē](../../strongs/g/g4864.md); and there was an [anthrōpos](../../strongs/g/g444.md) there which had a [xērainō](../../strongs/g/g3583.md) [cheir](../../strongs/g/g5495.md).

<a name="mark_3_2"></a>Mark 3:2

And they [paratēreō](../../strongs/g/g3906.md) him, whether he would [therapeuō](../../strongs/g/g2323.md) him on the [sabbaton](../../strongs/g/g4521.md); that they might [katēgoreō](../../strongs/g/g2723.md) him.

<a name="mark_3_3"></a>Mark 3:3

And he [legō](../../strongs/g/g3004.md) unto the [anthrōpos](../../strongs/g/g444.md) which had the [xērainō](../../strongs/g/g3583.md) [cheir](../../strongs/g/g5495.md), **[egeirō](../../strongs/g/g1453.md) forth.**

<a name="mark_3_4"></a>Mark 3:4

And he [legō](../../strongs/g/g3004.md) unto them, **Is it [exesti](../../strongs/g/g1832.md) to [agathopoieō](../../strongs/g/g15.md) on the [sabbaton](../../strongs/g/g4521.md), or to [kakopoieō](../../strongs/g/g2554.md)? to [sōzō](../../strongs/g/g4982.md) [psychē](../../strongs/g/g5590.md), or to [apokteinō](../../strongs/g/g615.md)?** But they [siōpaō](../../strongs/g/g4623.md).

<a name="mark_3_5"></a>Mark 3:5

And when he had [periblepō](../../strongs/g/g4017.md) on them with [orgē](../../strongs/g/g3709.md), being [syllypeō](../../strongs/g/g4818.md) for the [pōrōsis](../../strongs/g/g4457.md) of their [kardia](../../strongs/g/g2588.md), he [legō](../../strongs/g/g3004.md) unto the [anthrōpos](../../strongs/g/g444.md), **[ekteinō](../../strongs/g/g1614.md) forth thine [cheir](../../strongs/g/g5495.md).** And he [ekteinō](../../strongs/g/g1614.md) it out: and his [cheir](../../strongs/g/g5495.md) was [apokathistēmi](../../strongs/g/g600.md) [hygiēs](../../strongs/g/g5199.md) as the other.

<a name="mark_3_6"></a>Mark 3:6

And the [Pharisaios](../../strongs/g/g5330.md) [exerchomai](../../strongs/g/g1831.md), and [eutheōs](../../strongs/g/g2112.md) [poieō](../../strongs/g/g4160.md) [symboulion](../../strongs/g/g4824.md) with the [Hērōdianoi](../../strongs/g/g2265.md) against him, how they might [apollymi](../../strongs/g/g622.md) him.

<a name="mark_3_7"></a>Mark 3:7

But [Iēsous](../../strongs/g/g2424.md) [anachōreō](../../strongs/g/g402.md) with his [mathētēs](../../strongs/g/g3101.md) to the [thalassa](../../strongs/g/g2281.md): and a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md) from [Galilaia](../../strongs/g/g1056.md) [akoloutheō](../../strongs/g/g190.md) him, and from [Ioudaia](../../strongs/g/g2449.md),

<a name="mark_3_8"></a>Mark 3:8

And from [Hierosolyma](../../strongs/g/g2414.md), and from [Idoumaia](../../strongs/g/g2401.md), and from beyond [Iordanēs](../../strongs/g/g2446.md); and they about [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), a [polys](../../strongs/g/g4183.md) [plēthos](../../strongs/g/g4128.md), when they had [akouō](../../strongs/g/g191.md) what [hosos](../../strongs/g/g3745.md) he [poieō](../../strongs/g/g4160.md), [erchomai](../../strongs/g/g2064.md) unto him.

<a name="mark_3_9"></a>Mark 3:9

And he [eipon](../../strongs/g/g2036.md) to his [mathētēs](../../strongs/g/g3101.md), that a [ploiarion](../../strongs/g/g4142.md) should [proskartereō](../../strongs/g/g4342.md) on him because of the [ochlos](../../strongs/g/g3793.md), lest they should [thlibō](../../strongs/g/g2346.md) him.

<a name="mark_3_10"></a>Mark 3:10

For he had [therapeuō](../../strongs/g/g2323.md) [polys](../../strongs/g/g4183.md); insomuch that they [epipiptō](../../strongs/g/g1968.md) him for to [haptomai](../../strongs/g/g680.md) him, as many as had [mastix](../../strongs/g/g3148.md).

<a name="mark_3_11"></a>Mark 3:11

And [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), when they [theōreō](../../strongs/g/g2334.md) him, [prospiptō](../../strongs/g/g4363.md) him, and [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), Thou art the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="mark_3_12"></a>Mark 3:12

And he [polys](../../strongs/g/g4183.md) [epitimaō](../../strongs/g/g2008.md) them that they should not [poieō](../../strongs/g/g4160.md) him [phaneros](../../strongs/g/g5318.md).

<a name="mark_3_13"></a>Mark 3:13

And he [anabainō](../../strongs/g/g305.md) into a [oros](../../strongs/g/g3735.md), and [proskaleō](../../strongs/g/g4341.md) unto him whom he would: and they [aperchomai](../../strongs/g/g565.md) unto him.

<a name="mark_3_14"></a>Mark 3:14

And he [poieō](../../strongs/g/g4160.md) [dōdeka](../../strongs/g/g1427.md), that they should be with him, and that he might [apostellō](../../strongs/g/g649.md) them to [kēryssō](../../strongs/g/g2784.md),

<a name="mark_3_15"></a>Mark 3:15

And to have [exousia](../../strongs/g/g1849.md) to [therapeuō](../../strongs/g/g2323.md) [nosos](../../strongs/g/g3554.md), and to [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md):

<a name="mark_3_16"></a>Mark 3:16

And [Simōn](../../strongs/g/g4613.md) he [epitithēmi](../../strongs/g/g2007.md) [onoma](../../strongs/g/g3686.md) [Petros](../../strongs/g/g4074.md);

<a name="mark_3_17"></a>Mark 3:17

And [Iakōbos](../../strongs/g/g2385.md) of [Zebedaios](../../strongs/g/g2199.md), and [Iōannēs](../../strongs/g/g2491.md) the [adelphos](../../strongs/g/g80.md) of [Iakōbos](../../strongs/g/g2385.md); and he [epitithēmi](../../strongs/g/g2007.md) [onoma](../../strongs/g/g3686.md) them [Boanērges](../../strongs/g/g993.md), which is, The [huios](../../strongs/g/g5207.md) of [brontē](../../strongs/g/g1027.md):

<a name="mark_3_18"></a>Mark 3:18

And [Andreas](../../strongs/g/g406.md), and [Philippos](../../strongs/g/g5376.md), and [Bartholomaios](../../strongs/g/g918.md), and [Maththaios](../../strongs/g/g3156.md), and [Thōmas](../../strongs/g/g2381.md), and [Iakōbos](../../strongs/g/g2385.md) of [Alphaios](../../strongs/g/g256.md), and [Thaddaios](../../strongs/g/g2280.md), and [Simōn](../../strongs/g/g4613.md) the [Kananaios](../../strongs/g/g2581.md),

<a name="mark_3_19"></a>Mark 3:19

And [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), which also [paradidōmi](../../strongs/g/g3860.md) him: and they [erchomai](../../strongs/g/g2064.md) into an [oikos](../../strongs/g/g3624.md).

<a name="mark_3_20"></a>Mark 3:20

And the [ochlos](../../strongs/g/g3793.md) [synerchomai](../../strongs/g/g4905.md) [palin](../../strongs/g/g3825.md), so that they could not so much as [phago](../../strongs/g/g5315.md) [artos](../../strongs/g/g740.md).

<a name="mark_3_21"></a>Mark 3:21

And when his [para](../../strongs/g/g3844.md) [akouō](../../strongs/g/g191.md), they [exerchomai](../../strongs/g/g1831.md) to [krateō](../../strongs/g/g2902.md) on him: for they [legō](../../strongs/g/g3004.md), He is [existēmi](../../strongs/g/g1839.md).

<a name="mark_3_22"></a>Mark 3:22

And the [grammateus](../../strongs/g/g1122.md) which [katabainō](../../strongs/g/g2597.md) from [Hierosolyma](../../strongs/g/g2414.md) [legō](../../strongs/g/g3004.md), He hath [Beelzeboul](../../strongs/g/g954.md), and by the [archōn](../../strongs/g/g758.md) of [daimonion](../../strongs/g/g1140.md) [ekballō](../../strongs/g/g1544.md) he out [daimonion](../../strongs/g/g1140.md).

<a name="mark_3_23"></a>Mark 3:23

And he [proskaleō](../../strongs/g/g4341.md) them, and [legō](../../strongs/g/g3004.md) unto them in [parabolē](../../strongs/g/g3850.md), **How can [Satanas](../../strongs/g/g4567.md) [ekballō](../../strongs/g/g1544.md) [Satanas](../../strongs/g/g4567.md)?**

<a name="mark_3_24"></a>Mark 3:24

**And if a [basileia](../../strongs/g/g932.md) be [merizō](../../strongs/g/g3307.md) against itself, that [basileia](../../strongs/g/g932.md) cannot [histēmi](../../strongs/g/g2476.md).**

<a name="mark_3_25"></a>Mark 3:25

**And if an [oikia](../../strongs/g/g3614.md) be [merizō](../../strongs/g/g3307.md) against itself, that [oikia](../../strongs/g/g3614.md) cannot [histēmi](../../strongs/g/g2476.md).**

<a name="mark_3_26"></a>Mark 3:26

**And if [Satanas](../../strongs/g/g4567.md) [anistēmi](../../strongs/g/g450.md) against himself, and be [merizō](../../strongs/g/g3307.md), he cannot [histēmi](../../strongs/g/g2476.md), but hath a [telos](../../strongs/g/g5056.md).**

<a name="mark_3_27"></a>Mark 3:27

**[oudeis](../../strongs/g/g3762.md) can [eiserchomai](../../strongs/g/g1525.md) into an [ischyros](../../strongs/g/g2478.md) [oikia](../../strongs/g/g3614.md), and [diarpazō](../../strongs/g/g1283.md) his [skeuos](../../strongs/g/g4632.md), except he will first [deō](../../strongs/g/g1210.md) the [ischyros](../../strongs/g/g2478.md); and then he will [diarpazō](../../strongs/g/g1283.md) his [oikia](../../strongs/g/g3614.md).**

<a name="mark_3_28"></a>Mark 3:28

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, All [hamartēma](../../strongs/g/g265.md) shall be [aphiēmi](../../strongs/g/g863.md) unto the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), and [blasphēmia](../../strongs/g/g988.md) wherewith soever they shall [blasphēmeō](../../strongs/g/g987.md):**

<a name="mark_3_29"></a>Mark 3:29

**But he that shall [blasphēmeō](../../strongs/g/g987.md) against the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) hath [ou](../../strongs/g/g3756.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) [aphesis](../../strongs/g/g859.md), but is in [enochos](../../strongs/g/g1777.md) of [aiōnios](../../strongs/g/g166.md) [krisis](../../strongs/g/g2920.md).**

<a name="mark_3_30"></a>Mark 3:30

Because they [legō](../../strongs/g/g3004.md), He hath an [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md).

<a name="mark_3_31"></a>Mark 3:31

There [erchomai](../../strongs/g/g2064.md) then his [adelphos](../../strongs/g/g80.md) and his [mētēr](../../strongs/g/g3384.md), and, [histēmi](../../strongs/g/g2476.md) without, [apostellō](../../strongs/g/g649.md) unto him, [phōneō](../../strongs/g/g5455.md) him.

<a name="mark_3_32"></a>Mark 3:32

And the [ochlos](../../strongs/g/g3793.md) [kathēmai](../../strongs/g/g2521.md) about him, and they [eipon](../../strongs/g/g2036.md) unto him, [idou](../../strongs/g/g2400.md), thy [mētēr](../../strongs/g/g3384.md) and thy [adelphos](../../strongs/g/g80.md) without [zēteō](../../strongs/g/g2212.md) for thee.

<a name="mark_3_33"></a>Mark 3:33

And he [apokrinomai](../../strongs/g/g611.md) them, [legō](../../strongs/g/g3004.md), **Who is my [mētēr](../../strongs/g/g3384.md), or my [adelphos](../../strongs/g/g80.md)?**

<a name="mark_3_34"></a>Mark 3:34

And he [periblepō](../../strongs/g/g4017.md) [kyklō](../../strongs/g/g2945.md) on them which [kathēmai](../../strongs/g/g2521.md) about him, and [legō](../../strongs/g/g3004.md), **[ide](../../strongs/g/g2396.md) my [mētēr](../../strongs/g/g3384.md) and my [adelphos](../../strongs/g/g80.md)!**

<a name="mark_3_35"></a>Mark 3:35

**For whosoever shall [poieō](../../strongs/g/g4160.md) the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), the same is my [adelphos](../../strongs/g/g80.md), and my [adelphē](../../strongs/g/g79.md), and [mētēr](../../strongs/g/g3384.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 2](mark_2.md) - [Mark 4](mark_4.md)