# [Mark 14](https://www.blueletterbible.org/kjv/mar/14/1/rl1/s_966001)

<a name="mark_14_1"></a>Mark 14:1

After two [hēmera](../../strongs/g/g2250.md) was the [pascha](../../strongs/g/g3957.md), and of [azymos](../../strongs/g/g106.md): and the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) [zēteō](../../strongs/g/g2212.md) how they might [krateō](../../strongs/g/g2902.md) him by [dolos](../../strongs/g/g1388.md), and [apokteinō](../../strongs/g/g615.md).

<a name="mark_14_2"></a>Mark 14:2

But they [legō](../../strongs/g/g3004.md), Not on the [heortē](../../strongs/g/g1859.md), lest there be a [thorybos](../../strongs/g/g2351.md) of the [laos](../../strongs/g/g2992.md).

<a name="mark_14_3"></a>Mark 14:3

And being in [Bēthania](../../strongs/g/g963.md) in the [oikia](../../strongs/g/g3614.md) of [Simōn](../../strongs/g/g4613.md) the [lepros](../../strongs/g/g3015.md), as he [katakeimai](../../strongs/g/g2621.md), there [erchomai](../../strongs/g/g2064.md) a [gynē](../../strongs/g/g1135.md) having an [alabastron](../../strongs/g/g211.md) of [myron](../../strongs/g/g3464.md) of [nardos](../../strongs/g/g3487.md) [pistikos](../../strongs/g/g4101.md) [polytelēs](../../strongs/g/g4185.md); and she [syntribō](../../strongs/g/g4937.md) the [alabastron](../../strongs/g/g211.md), and [katacheō](../../strongs/g/g2708.md) on his [kephalē](../../strongs/g/g2776.md).

<a name="mark_14_4"></a>Mark 14:4

And there were some that had [aganakteō](../../strongs/g/g23.md) within themselves, and [legō](../../strongs/g/g3004.md), Why was this [apōleia](../../strongs/g/g684.md) of the [myron](../../strongs/g/g3464.md) [ginomai](../../strongs/g/g1096.md)?

<a name="mark_14_5"></a>Mark 14:5

For it might have been [pipraskō](../../strongs/g/g4097.md) for more than three hundred [dēnarion](../../strongs/g/g1220.md), and have been [didōmi](../../strongs/g/g1325.md) to the [ptōchos](../../strongs/g/g4434.md). And they [embrimaomai](../../strongs/g/g1690.md) against her.

<a name="mark_14_6"></a>Mark 14:6

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **[aphiēmi](../../strongs/g/g863.md) her; why [kopos](../../strongs/g/g2873.md) [parechō](../../strongs/g/g3930.md) ye her? she hath [ergazomai](../../strongs/g/g2038.md) a [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md) on me.**

<a name="mark_14_7"></a>Mark 14:7

**For ye have the [ptōchos](../../strongs/g/g4434.md) with you [pantote](../../strongs/g/g3842.md), and whensoever ye will ye may [poieō](../../strongs/g/g4160.md) them [eu](../../strongs/g/g2095.md): but me ye have not [pantote](../../strongs/g/g3842.md).**

<a name="mark_14_8"></a>Mark 14:8

**She hath [poieō](../../strongs/g/g4160.md) what she could: she is [prolambanō](../../strongs/g/g4301.md) to [myrizō](../../strongs/g/g3462.md) my [sōma](../../strongs/g/g4983.md) to the [entaphiasmos](../../strongs/g/g1780.md).**

<a name="mark_14_9"></a>Mark 14:9

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, Wheresoever this [euaggelion](../../strongs/g/g2098.md) shall be [kēryssō](../../strongs/g/g2784.md) throughout the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md), also that she hath [poieō](../../strongs/g/g4160.md) shall be [laleō](../../strongs/g/g2980.md) of for a [mnēmosynon](../../strongs/g/g3422.md) of her.**

<a name="mark_14_10"></a>Mark 14:10

And [Ioudas](../../strongs/g/g2455.md) [Iskariōth](../../strongs/g/g2469.md), one of the [dōdeka](../../strongs/g/g1427.md), [aperchomai](../../strongs/g/g565.md) unto the [archiereus](../../strongs/g/g749.md), to [paradidōmi](../../strongs/g/g3860.md) him unto them.

<a name="mark_14_11"></a>Mark 14:11

And when they [akouō](../../strongs/g/g191.md), they were [chairō](../../strongs/g/g5463.md), and [epaggellomai](../../strongs/g/g1861.md) to [didōmi](../../strongs/g/g1325.md) him [argyrion](../../strongs/g/g694.md). And he [zēteō](../../strongs/g/g2212.md) how he might [eukairōs](../../strongs/g/g2122.md) [paradidōmi](../../strongs/g/g3860.md) him.

<a name="mark_14_12"></a>Mark 14:12

And the first [hēmera](../../strongs/g/g2250.md) of [azymos](../../strongs/g/g106.md), when they [thyō](../../strongs/g/g2380.md) the [pascha](../../strongs/g/g3957.md), his [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, Where wilt thou that we [aperchomai](../../strongs/g/g565.md) and [hetoimazō](../../strongs/g/g2090.md) that thou mayest [phago](../../strongs/g/g5315.md) the [pascha](../../strongs/g/g3957.md)?

<a name="mark_14_13"></a>Mark 14:13

And he [apostellō](../../strongs/g/g649.md) two of his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md) unto them, **[hypagō](../../strongs/g/g5217.md) ye into the [polis](../../strongs/g/g4172.md), and there shall [apantaō](../../strongs/g/g528.md) you an [anthrōpos](../../strongs/g/g444.md) [bastazō](../../strongs/g/g941.md) a [keramion](../../strongs/g/g2765.md) of [hydōr](../../strongs/g/g5204.md): [akoloutheō](../../strongs/g/g190.md) him.**

<a name="mark_14_14"></a>Mark 14:14

**And wheresoever he shall [eiserchomai](../../strongs/g/g1525.md), [eipon](../../strongs/g/g2036.md) ye to the [oikodespotēs](../../strongs/g/g3617.md), The [didaskalos](../../strongs/g/g1320.md) [legō](../../strongs/g/g3004.md), Where is the [katalyma](../../strongs/g/g2646.md), where I shall [phago](../../strongs/g/g5315.md) the [pascha](../../strongs/g/g3957.md) with my [mathētēs](../../strongs/g/g3101.md)?**

<a name="mark_14_15"></a>Mark 14:15

**And he will [deiknyō](../../strongs/g/g1166.md) you a [megas](../../strongs/g/g3173.md) [anōgeon](../../strongs/g/g508.md) [strōnnyō](../../strongs/g/g4766.md) [hetoimos](../../strongs/g/g2092.md): there make [hetoimazō](../../strongs/g/g2090.md) for us.**

<a name="mark_14_16"></a>Mark 14:16

And his [mathētēs](../../strongs/g/g3101.md) [exerchomai](../../strongs/g/g1831.md), and [erchomai](../../strongs/g/g2064.md) into the [polis](../../strongs/g/g4172.md), and [heuriskō](../../strongs/g/g2147.md) as he had [eipon](../../strongs/g/g2036.md) unto them: and they [hetoimazō](../../strongs/g/g2090.md) the [pascha](../../strongs/g/g3957.md).

<a name="mark_14_17"></a>Mark 14:17

And in the [opsios](../../strongs/g/g3798.md) he [erchomai](../../strongs/g/g2064.md) with the [dōdeka](../../strongs/g/g1427.md).

<a name="mark_14_18"></a>Mark 14:18

And as they [anakeimai](../../strongs/g/g345.md) and did [esthiō](../../strongs/g/g2068.md), [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, One of you which [esthiō](../../strongs/g/g2068.md) with me shall [paradidōmi](../../strongs/g/g3860.md) me.**

<a name="mark_14_19"></a>Mark 14:19

And they [archomai](../../strongs/g/g756.md) to be [lypeō](../../strongs/g/g3076.md), and to [legō](../../strongs/g/g3004.md) unto him [heis kath' heis](../../strongs/g/g1527.md) [mēti](../../strongs/g/g3385.md), it I? and another, it I? [^1]

<a name="mark_14_20"></a>Mark 14:20

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **one of the [dōdeka](../../strongs/g/g1427.md), that [embaptō](../../strongs/g/g1686.md) with me in the [tryblion](../../strongs/g/g5165.md).**

<a name="mark_14_21"></a>Mark 14:21

**The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [men](../../strongs/g/g3303.md) [hypagō](../../strongs/g/g5217.md), as it is [graphō](../../strongs/g/g1125.md) of him: but [ouai](../../strongs/g/g3759.md) to that [anthrōpos](../../strongs/g/g444.md) by whom the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md)! [kalos](../../strongs/g/g2570.md) were it for that [anthrōpos](../../strongs/g/g444.md) if he had never been [gennaō](../../strongs/g/g1080.md).**

<a name="mark_14_22"></a>Mark 14:22

And as they did [esthiō](../../strongs/g/g2068.md), [Iēsous](../../strongs/g/g2424.md) [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md), and [eulogeō](../../strongs/g/g2127.md), and [klaō](../../strongs/g/g2806.md), and [didōmi](../../strongs/g/g1325.md) to them, and [eipon](../../strongs/g/g2036.md), **[lambanō](../../strongs/g/g2983.md), [phago](../../strongs/g/g5315.md): this is my [sōma](../../strongs/g/g4983.md).**

<a name="mark_14_23"></a>Mark 14:23

And he [lambanō](../../strongs/g/g2983.md) the [potērion](../../strongs/g/g4221.md), and [eucharisteō](../../strongs/g/g2168.md), he [didōmi](../../strongs/g/g1325.md) it to them: and they all [pinō](../../strongs/g/g4095.md) of it.

<a name="mark_14_24"></a>Mark 14:24

And he [eipon](../../strongs/g/g2036.md) unto them, **This is my [haima](../../strongs/g/g129.md) of the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md), which is [ekcheō](../../strongs/g/g1632.md) for [polys](../../strongs/g/g4183.md).**

<a name="mark_14_25"></a>Mark 14:25

**[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, I will [pinō](../../strongs/g/g4095.md) no more of the [gennēma](../../strongs/g/g1081.md) of the [ampelos](../../strongs/g/g288.md), until that [hēmera](../../strongs/g/g2250.md) that I [pinō](../../strongs/g/g4095.md) it [kainos](../../strongs/g/g2537.md) in the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).**

<a name="mark_14_26"></a>Mark 14:26

And when they had [hymneō](../../strongs/g/g5214.md), they [exerchomai](../../strongs/g/g1831.md) into the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md).

<a name="mark_14_27"></a>Mark 14:27

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto them, **All ye shall be [skandalizō](../../strongs/g/g4624.md) because of me this [nyx](../../strongs/g/g3571.md): for it is [graphō](../../strongs/g/g1125.md), I will [patassō](../../strongs/g/g3960.md) the [poimēn](../../strongs/g/g4166.md), and the [probaton](../../strongs/g/g4263.md) shall be [diaskorpizō](../../strongs/g/g1287.md).**

<a name="mark_14_28"></a>Mark 14:28

**But after that I am [egeirō](../../strongs/g/g1453.md), I will [proagō](../../strongs/g/g4254.md) you into [Galilaia](../../strongs/g/g1056.md).**

<a name="mark_14_29"></a>Mark 14:29

But [Petros](../../strongs/g/g4074.md) [phēmi](../../strongs/g/g5346.md) unto him, Although all shall be [skandalizō](../../strongs/g/g4624.md), yet not I.

<a name="mark_14_30"></a>Mark 14:30

And [Iēsous](../../strongs/g/g2424.md) [legō](../../strongs/g/g3004.md) unto him, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto thee, That [sēmeron](../../strongs/g/g4594.md), in this [nyx](../../strongs/g/g3571.md), before the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md) twice, thou shalt [aparneomai](../../strongs/g/g533.md) me thrice.**

<a name="mark_14_31"></a>Mark 14:31

But he [legō](../../strongs/g/g3004.md) the [mallon](../../strongs/g/g3123.md) [ek](../../strongs/g/g1537.md) [perissos](../../strongs/g/g4053.md), If I should [synapothnēskō](../../strongs/g/g4880.md) thee, I will not [aparneomai](../../strongs/g/g533.md) thee in any wise. [hōsautōs](../../strongs/g/g5615.md) also [legō](../../strongs/g/g3004.md) they all.

<a name="mark_14_32"></a>Mark 14:32

And they [erchomai](../../strongs/g/g2064.md) to a [chōrion](../../strongs/g/g5564.md) which was [onoma](../../strongs/g/g3686.md) [Gethsēmani](../../strongs/g/g1068.md): and he [legō](../../strongs/g/g3004.md) to his [mathētēs](../../strongs/g/g3101.md), **[kathizō](../../strongs/g/g2523.md) here, while I shall [proseuchomai](../../strongs/g/g4336.md).**

<a name="mark_14_33"></a>Mark 14:33

And he [paralambanō](../../strongs/g/g3880.md) with him [Petros](../../strongs/g/g4074.md) and [Iakōbos](../../strongs/g/g2385.md) and [Iōannēs](../../strongs/g/g2491.md), and [archomai](../../strongs/g/g756.md) to be [ekthambeō](../../strongs/g/g1568.md), and [adēmoneō](../../strongs/g/g85.md);

<a name="mark_14_34"></a>Mark 14:34

And [legō](../../strongs/g/g3004.md) unto them, **My [psychē](../../strongs/g/g5590.md) is [perilypos](../../strongs/g/g4036.md) unto [thanatos](../../strongs/g/g2288.md): [menō](../../strongs/g/g3306.md) here, and [grēgoreō](../../strongs/g/g1127.md).**

<a name="mark_14_35"></a>Mark 14:35

And he [proerchomai](../../strongs/g/g4281.md) [mikron](../../strongs/g/g3397.md), and [piptō](../../strongs/g/g4098.md) on the [gē](../../strongs/g/g1093.md), and [proseuchomai](../../strongs/g/g4336.md) that, if it were [dynatos](../../strongs/g/g1415.md), the [hōra](../../strongs/g/g5610.md) [parerchomai](../../strongs/g/g3928.md) from him.

<a name="mark_14_36"></a>Mark 14:36

And he [legō](../../strongs/g/g3004.md), **[abba](../../strongs/g/g5.md), [patēr](../../strongs/g/g3962.md), all things [dynatos](../../strongs/g/g1415.md) unto thee; [parapherō](../../strongs/g/g3911.md) this [potērion](../../strongs/g/g4221.md) from me: nevertheless not what I [thelō](../../strongs/g/g2309.md), but what thou wilt.**

<a name="mark_14_37"></a>Mark 14:37

And he [erchomai](../../strongs/g/g2064.md), and [heuriskō](../../strongs/g/g2147.md) them [katheudō](../../strongs/g/g2518.md), and [legō](../../strongs/g/g3004.md) unto [Petros](../../strongs/g/g4074.md), **[Simōn](../../strongs/g/g4613.md), [katheudō](../../strongs/g/g2518.md) thou? couldest not thou [grēgoreō](../../strongs/g/g1127.md) one [hōra](../../strongs/g/g5610.md)?**

<a name="mark_14_38"></a>Mark 14:38

**[grēgoreō](../../strongs/g/g1127.md) and [proseuchomai](../../strongs/g/g4336.md), lest ye [eiserchomai](../../strongs/g/g1525.md) into [peirasmos](../../strongs/g/g3986.md). The [pneuma](../../strongs/g/g4151.md) truly [prothymos](../../strongs/g/g4289.md), but the [sarx](../../strongs/g/g4561.md) is [asthenēs](../../strongs/g/g772.md).**

<a name="mark_14_39"></a>Mark 14:39

And again he [aperchomai](../../strongs/g/g565.md), and [proseuchomai](../../strongs/g/g4336.md), and [eipon](../../strongs/g/g2036.md) the same [logos](../../strongs/g/g3056.md).

<a name="mark_14_40"></a>Mark 14:40

And when he [hypostrephō](../../strongs/g/g5290.md), he [heuriskō](../../strongs/g/g2147.md) them [katheudō](../../strongs/g/g2518.md) again, (for their [ophthalmos](../../strongs/g/g3788.md) were [bareō](../../strongs/g/g916.md),) neither [eidō](../../strongs/g/g1492.md) they what to [apokrinomai](../../strongs/g/g611.md) him.

<a name="mark_14_41"></a>Mark 14:41

And he [erchomai](../../strongs/g/g2064.md) the [tritos](../../strongs/g/g5154.md), and [legō](../../strongs/g/g3004.md) unto them, **[katheudō](../../strongs/g/g2518.md) now, and take [anapauō](../../strongs/g/g373.md): it is [apechei](../../strongs/g/g566.md), the [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md); [idou](../../strongs/g/g2400.md), the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [hamartōlos](../../strongs/g/g268.md).**

<a name="mark_14_42"></a>Mark 14:42

**[egeirō](../../strongs/g/g1453.md), let us [agō](../../strongs/g/g71.md); [idou](../../strongs/g/g2400.md), he that [paradidōmi](../../strongs/g/g3860.md) me [eggizō](../../strongs/g/g1448.md).**

<a name="mark_14_43"></a>Mark 14:43

And [eutheōs](../../strongs/g/g2112.md), while he yet [laleō](../../strongs/g/g2980.md), [paraginomai](../../strongs/g/g3854.md) [Ioudas](../../strongs/g/g2455.md), one of the [dōdeka](../../strongs/g/g1427.md), and with him a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) with [machaira](../../strongs/g/g3162.md) and [xylon](../../strongs/g/g3586.md), from the [archiereus](../../strongs/g/g749.md) and the [grammateus](../../strongs/g/g1122.md) and the [presbyteros](../../strongs/g/g4245.md).

<a name="mark_14_44"></a>Mark 14:44

And he that [paradidōmi](../../strongs/g/g3860.md) him had [didōmi](../../strongs/g/g1325.md) them a [syssēmon](../../strongs/g/g4953.md), [legō](../../strongs/g/g3004.md), Whomsoever I shall [phileō](../../strongs/g/g5368.md), that same is he; [krateō](../../strongs/g/g2902.md) him, and [apagō](../../strongs/g/g520.md) [asphalōs](../../strongs/g/g806.md).

<a name="mark_14_45"></a>Mark 14:45

And as soon as he was [erchomai](../../strongs/g/g2064.md), he [proserchomai](../../strongs/g/g4334.md) [eutheōs](../../strongs/g/g2112.md) to him, and [legō](../../strongs/g/g3004.md), [rhabbi](../../strongs/g/g4461.md), [rhabbi](../../strongs/g/g4461.md); and [kataphileō](../../strongs/g/g2705.md) him.

<a name="mark_14_46"></a>Mark 14:46

And they [epiballō](../../strongs/g/g1911.md) their [cheir](../../strongs/g/g5495.md) on him, and [krateō](../../strongs/g/g2902.md) him.

<a name="mark_14_47"></a>Mark 14:47

And one of them that [paristēmi](../../strongs/g/g3936.md) [spaō](../../strongs/g/g4685.md) a [machaira](../../strongs/g/g3162.md), and [paiō](../../strongs/g/g3817.md) a [doulos](../../strongs/g/g1401.md) of the [archiereus](../../strongs/g/g749.md), and [aphaireō](../../strongs/g/g851.md) his [ōtion](../../strongs/g/g5621.md).

<a name="mark_14_48"></a>Mark 14:48

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **Are ye [exerchomai](../../strongs/g/g1831.md), as against a [lēstēs](../../strongs/g/g3027.md), with [machaira](../../strongs/g/g3162.md) and [xylon](../../strongs/g/g3586.md) to [syllambanō](../../strongs/g/g4815.md) me?**

<a name="mark_14_49"></a>Mark 14:49

**I was [hēmera](../../strongs/g/g2250.md) with you in the [hieron](../../strongs/g/g2411.md) [didaskō](../../strongs/g/g1321.md), and ye [krateō](../../strongs/g/g2902.md) me not: but the [graphē](../../strongs/g/g1124.md) must be [plēroō](../../strongs/g/g4137.md).**

<a name="mark_14_50"></a>Mark 14:50

And they all [aphiēmi](../../strongs/g/g863.md) him, and [pheugō](../../strongs/g/g5343.md).

<a name="mark_14_51"></a>Mark 14:51

And there [akoloutheō](../../strongs/g/g190.md) him a certain [neaniskos](../../strongs/g/g3495.md), having a [sindōn](../../strongs/g/g4616.md) [periballō](../../strongs/g/g4016.md) about [gymnos](../../strongs/g/g1131.md); and the [neaniskos](../../strongs/g/g3495.md) [krateō](../../strongs/g/g2902.md) on him:

<a name="mark_14_52"></a>Mark 14:52

And he [kataleipō](../../strongs/g/g2641.md) the [sindōn](../../strongs/g/g4616.md), and [pheugō](../../strongs/g/g5343.md) from them [gymnos](../../strongs/g/g1131.md).

<a name="mark_14_53"></a>Mark 14:53

And they [apagō](../../strongs/g/g520.md) [Iēsous](../../strongs/g/g2424.md) to the [archiereus](../../strongs/g/g749.md): and with him were [synerchomai](../../strongs/g/g4905.md) all the [archiereus](../../strongs/g/g749.md) and the [presbyteros](../../strongs/g/g4245.md) and the [grammateus](../../strongs/g/g1122.md).

<a name="mark_14_54"></a>Mark 14:54

And [Petros](../../strongs/g/g4074.md) [akoloutheō](../../strongs/g/g190.md) him [makrothen](../../strongs/g/g3113.md), even into the [aulē](../../strongs/g/g833.md) of the [archiereus](../../strongs/g/g749.md): and he [sygkathēmai](../../strongs/g/g4775.md) with the [hypēretēs](../../strongs/g/g5257.md), and [thermainō](../../strongs/g/g2328.md) himself at the [phōs](../../strongs/g/g5457.md).

<a name="mark_14_55"></a>Mark 14:55

And the [archiereus](../../strongs/g/g749.md) and all the [synedrion](../../strongs/g/g4892.md) [zēteō](../../strongs/g/g2212.md) for [martyria](../../strongs/g/g3141.md) against [Iēsous](../../strongs/g/g2424.md) to [thanatoō](../../strongs/g/g2289.md) him; and [heuriskō](../../strongs/g/g2147.md) none.

<a name="mark_14_56"></a>Mark 14:56

For [polys](../../strongs/g/g4183.md) [pseudomartyreō](../../strongs/g/g5576.md) against him, but their [martyria](../../strongs/g/g3141.md) agreed not together.

<a name="mark_14_57"></a>Mark 14:57

And there [anistēmi](../../strongs/g/g450.md) certain, and [pseudomartyreō](../../strongs/g/g5576.md) against him, [legō](../../strongs/g/g3004.md),

<a name="mark_14_58"></a>Mark 14:58

We [akouō](../../strongs/g/g191.md) him [legō](../../strongs/g/g3004.md), I will [katalyō](../../strongs/g/g2647.md) this [naos](../../strongs/g/g3485.md) that is [cheiropoiētos](../../strongs/g/g5499.md), and within three [hēmera](../../strongs/g/g2250.md) I will [oikodomeō](../../strongs/g/g3618.md) [allos](../../strongs/g/g243.md) [acheiropoiētos](../../strongs/g/g886.md).

<a name="mark_14_59"></a>Mark 14:59

But neither so did their [martyria](../../strongs/g/g3141.md) agree together.

<a name="mark_14_60"></a>Mark 14:60

And the [archiereus](../../strongs/g/g749.md) [anistēmi](../../strongs/g/g450.md) in the [mesos](../../strongs/g/g3319.md), and [eperōtaō](../../strongs/g/g1905.md) [Iēsous](../../strongs/g/g2424.md), [legō](../../strongs/g/g3004.md), [apokrinomai](../../strongs/g/g611.md) thou nothing? what these [katamartyreō](../../strongs/g/g2649.md) against thee?

<a name="mark_14_61"></a>Mark 14:61

But he [siōpaō](../../strongs/g/g4623.md), and [apokrinomai](../../strongs/g/g611.md) nothing. Again the [archiereus](../../strongs/g/g749.md) [eperōtaō](../../strongs/g/g1905.md) him, and [legō](../../strongs/g/g3004.md) unto him, Art thou the [Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of the [eulogētos](../../strongs/g/g2128.md)?

<a name="mark_14_62"></a>Mark 14:62

And [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **I am: and ye shall [optanomai](../../strongs/g/g3700.md) the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) [kathēmai](../../strongs/g/g2521.md) on the [dexios](../../strongs/g/g1188.md) of [dynamis](../../strongs/g/g1411.md), and [erchomai](../../strongs/g/g2064.md) in the [nephelē](../../strongs/g/g3507.md) of [ouranos](../../strongs/g/g3772.md).**

<a name="mark_14_63"></a>Mark 14:63

Then the [archiereus](../../strongs/g/g749.md) [diarrēssō](../../strongs/g/g1284.md) his [chitōn](../../strongs/g/g5509.md), and [legō](../../strongs/g/g3004.md), What [chreia](../../strongs/g/g5532.md) we any further [martys](../../strongs/g/g3144.md)?

<a name="mark_14_64"></a>Mark 14:64

Ye have [akouō](../../strongs/g/g191.md) the [blasphēmia](../../strongs/g/g988.md): what [phainō](../../strongs/g/g5316.md) ye? And they all [katakrinō](../../strongs/g/g2632.md) him to be [enochos](../../strongs/g/g1777.md) of [thanatos](../../strongs/g/g2288.md).

<a name="mark_14_65"></a>Mark 14:65

And some [archomai](../../strongs/g/g756.md) to [emptyō](../../strongs/g/g1716.md) on him, and to [perikalyptō](../../strongs/g/g4028.md) his [prosōpon](../../strongs/g/g4383.md), and to [kolaphizō](../../strongs/g/g2852.md) him, and to [legō](../../strongs/g/g3004.md) unto him, [prophēteuō](../../strongs/g/g4395.md): and the [hypēretēs](../../strongs/g/g5257.md) did [ballō](../../strongs/g/g906.md) him with [rhapisma](../../strongs/g/g4475.md).

<a name="mark_14_66"></a>Mark 14:66

And as [Petros](../../strongs/g/g4074.md) was [katō](../../strongs/g/g2736.md) in the [aulē](../../strongs/g/g833.md), there [erchomai](../../strongs/g/g2064.md) one of the [paidiskē](../../strongs/g/g3814.md) of the [archiereus](../../strongs/g/g749.md):

<a name="mark_14_67"></a>Mark 14:67

And when she [eidō](../../strongs/g/g1492.md) [Petros](../../strongs/g/g4074.md) [thermainō](../../strongs/g/g2328.md) himself, she [emblepō](../../strongs/g/g1689.md) upon him, and [legō](../../strongs/g/g3004.md), And thou also wast with [Iēsous](../../strongs/g/g2424.md) of [Nazarēnos](../../strongs/g/g3479.md).

<a name="mark_14_68"></a>Mark 14:68

But he [arneomai](../../strongs/g/g720.md), [legō](../../strongs/g/g3004.md), I [eidō](../../strongs/g/g1492.md) not, neither [epistamai](../../strongs/g/g1987.md) what thou [legō](../../strongs/g/g3004.md). And he [exerchomai](../../strongs/g/g1831.md) out into the [proaulion](../../strongs/g/g4259.md); and the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md).

<a name="mark_14_69"></a>Mark 14:69

And a [paidiskē](../../strongs/g/g3814.md) [eidō](../../strongs/g/g1492.md) him again, and [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) to [paristēmi](../../strongs/g/g3936.md), This is of them.

<a name="mark_14_70"></a>Mark 14:70

And he [arneomai](../../strongs/g/g720.md) it again. And a [mikron](../../strongs/g/g3397.md) after, they that [paristēmi](../../strongs/g/g3936.md) [legō](../../strongs/g/g3004.md) again to [Petros](../../strongs/g/g4074.md), [alēthōs](../../strongs/g/g230.md) thou art of them: for thou art a [Galilaios](../../strongs/g/g1057.md), and thy [lalia](../../strongs/g/g2981.md) [homoiazō](../../strongs/g/g3662.md).

<a name="mark_14_71"></a>Mark 14:71

But he [archomai](../../strongs/g/g756.md) to [anathematizō](../../strongs/g/g332.md) and to [omnyō](../../strongs/g/g3660.md), I [eidō](../../strongs/g/g1492.md) not this [anthrōpos](../../strongs/g/g444.md) of whom ye [legō](../../strongs/g/g3004.md).

<a name="mark_14_72"></a>Mark 14:72

And the [deuteros](../../strongs/g/g1208.md) the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md). And [Petros](../../strongs/g/g4074.md) [anamimnēskō](../../strongs/g/g363.md) the [rhēma](../../strongs/g/g4487.md) that [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **Before the [alektōr](../../strongs/g/g220.md) [phōneō](../../strongs/g/g5455.md) twice, thou shalt [aparneomai](../../strongs/g/g533.md) me thrice.** And when he [epiballō](../../strongs/g/g1911.md), he [klaiō](../../strongs/g/g2799.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 13](mark_13.md) - [Mark 15](mark_15.md)

---

[^1]: [Mark 14:19 Commentary](../../commentary/mark/mark_14_commentary.md#mark_14_19)
