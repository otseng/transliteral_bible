# [Mark 9](https://www.blueletterbible.org/kjv/mar/9/1/rl1/s_966001)

<a name="mark_9_1"></a>Mark 9:1

And he [legō](../../strongs/g/g3004.md) unto them, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That there be some of them that [histēmi](../../strongs/g/g2476.md) here, which shall not [geuomai](../../strongs/g/g1089.md) of [thanatos](../../strongs/g/g2288.md), till they have [eidō](../../strongs/g/g1492.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) [erchomai](../../strongs/g/g2064.md) with [dynamis](../../strongs/g/g1411.md).**

<a name="mark_9_2"></a>Mark 9:2

And after six [hēmera](../../strongs/g/g2250.md) [Iēsous](../../strongs/g/g2424.md) [paralambanō](../../strongs/g/g3880.md) [Petros](../../strongs/g/g4074.md), and [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md), and [anapherō](../../strongs/g/g399.md) them into an [hypsēlos](../../strongs/g/g5308.md) [oros](../../strongs/g/g3735.md) apart by themselves: and he was [metamorphoō](../../strongs/g/g3339.md) before them.

<a name="mark_9_3"></a>Mark 9:3

And his [himation](../../strongs/g/g2440.md) became [stilbō](../../strongs/g/g4744.md), [lian](../../strongs/g/g3029.md) [leukos](../../strongs/g/g3022.md) as [chiōn](../../strongs/g/g5510.md); so as no [gnapheus](../../strongs/g/g1102.md) on [gē](../../strongs/g/g1093.md) can [leukainō](../../strongs/g/g3021.md) them.

<a name="mark_9_4"></a>Mark 9:4

And there [optanomai](../../strongs/g/g3700.md) unto them [Ēlias](../../strongs/g/g2243.md) with [Mōÿsēs](../../strongs/g/g3475.md): and they were [syllaleō](../../strongs/g/g4814.md) with [Iēsous](../../strongs/g/g2424.md).

<a name="mark_9_5"></a>Mark 9:5

And [Petros](../../strongs/g/g4074.md) [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) to [Iēsous](../../strongs/g/g2424.md), [rhabbi](../../strongs/g/g4461.md), it is [kalos](../../strongs/g/g2570.md) for us to be here: and let us [poieō](../../strongs/g/g4160.md) three [skēnē](../../strongs/g/g4633.md); one for thee, and one for [Mōÿsēs](../../strongs/g/g3475.md), and one for [Ēlias](../../strongs/g/g2243.md).

<a name="mark_9_6"></a>Mark 9:6

For he [eidō](../../strongs/g/g1492.md) not what to [laleō](../../strongs/g/g2980.md); for they were sore [ekphobos](../../strongs/g/g1630.md).

<a name="mark_9_7"></a>Mark 9:7

And there was a [nephelē](../../strongs/g/g3507.md) that [episkiazō](../../strongs/g/g1982.md) them: and a [phōnē](../../strongs/g/g5456.md) [erchomai](../../strongs/g/g2064.md) out of the [nephelē](../../strongs/g/g3507.md), [legō](../../strongs/g/g3004.md), This is my [agapētos](../../strongs/g/g27.md) [huios](../../strongs/g/g5207.md): [akouō](../../strongs/g/g191.md) him.

<a name="mark_9_8"></a>Mark 9:8

And [exapina](../../strongs/g/g1819.md), when they had [periblepō](../../strongs/g/g4017.md), they [eidō](../../strongs/g/g1492.md) [oudeis](../../strongs/g/g3762.md) any more, save [Iēsous](../../strongs/g/g2424.md) only with themselves.

<a name="mark_9_9"></a>Mark 9:9

And as they [katabainō](../../strongs/g/g2597.md) from the [oros](../../strongs/g/g3735.md), he [diastellō](../../strongs/g/g1291.md) them that they should [diēgeomai](../../strongs/g/g1334.md) [mēdeis](../../strongs/g/g3367.md) what things they had [eidō](../../strongs/g/g1492.md), till the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) were [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md).

<a name="mark_9_10"></a>Mark 9:10

And they [krateō](../../strongs/g/g2902.md) that [logos](../../strongs/g/g3056.md) with themselves, [syzēteō](../../strongs/g/g4802.md) what the [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md) should mean.

<a name="mark_9_11"></a>Mark 9:11

And they [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md), Why [legō](../../strongs/g/g3004.md) the [grammateus](../../strongs/g/g1122.md) that [Ēlias](../../strongs/g/g2243.md) must [prōton](../../strongs/g/g4412.md) [erchomai](../../strongs/g/g2064.md)?

<a name="mark_9_12"></a>Mark 9:12

And he [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) them, **[Ēlias](../../strongs/g/g2243.md) [men](../../strongs/g/g3303.md) [erchomai](../../strongs/g/g2064.md) [prōton](../../strongs/g/g4412.md), and [apokathistēmi](../../strongs/g/g600.md) all things; and how it is [graphō](../../strongs/g/g1125.md) of the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), that he must [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md), and be [exoudeneō](../../strongs/g/g1847.md).**

<a name="mark_9_13"></a>Mark 9:13

**But I [legō](../../strongs/g/g3004.md) unto you, That [Ēlias](../../strongs/g/g2243.md) is indeed [erchomai](../../strongs/g/g2064.md), and they have [poieō](../../strongs/g/g4160.md) unto him whatsoever they [thelō](../../strongs/g/g2309.md), as it is [graphō](../../strongs/g/g1125.md) of him.**

<a name="mark_9_14"></a>Mark 9:14

And when he [erchomai](../../strongs/g/g2064.md) to his [mathētēs](../../strongs/g/g3101.md), he [eidō](../../strongs/g/g1492.md) a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) about them, and the [grammateus](../../strongs/g/g1122.md) [syzēteō](../../strongs/g/g4802.md) with them.

<a name="mark_9_15"></a>Mark 9:15

And [eutheōs](../../strongs/g/g2112.md) all the [ochlos](../../strongs/g/g3793.md), when they [eidō](../../strongs/g/g1492.md) him, [ekthambeō](../../strongs/g/g1568.md), and [prostrechō](../../strongs/g/g4370.md) [aspazomai](../../strongs/g/g782.md) him.

<a name="mark_9_16"></a>Mark 9:16

And he [eperōtaō](../../strongs/g/g1905.md) the [grammateus](../../strongs/g/g1122.md), **What [syzēteō](../../strongs/g/g4802.md) ye with them?**

<a name="mark_9_17"></a>Mark 9:17

And one of the [ochlos](../../strongs/g/g3793.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md), [didaskalos](../../strongs/g/g1320.md), I have [pherō](../../strongs/g/g5342.md) unto thee my [huios](../../strongs/g/g5207.md), which hath an [alalos](../../strongs/g/g216.md) [pneuma](../../strongs/g/g4151.md);

<a name="mark_9_18"></a>Mark 9:18

And wheresoever he [katalambanō](../../strongs/g/g2638.md) him, he [rhēgnymi](../../strongs/g/g4486.md) him: and he [aphrizō](../../strongs/g/g875.md), and [trizō](../../strongs/g/g5149.md) with his [odous](../../strongs/g/g3599.md), and [xērainō](../../strongs/g/g3583.md): and I [eipon](../../strongs/g/g2036.md) to thy [mathētēs](../../strongs/g/g3101.md) that they should [ekballō](../../strongs/g/g1544.md) him; and they could not.

<a name="mark_9_19"></a>Mark 9:19

He [apokrinomai](../../strongs/g/g611.md) him, and [legō](../../strongs/g/g3004.md), **[ō](../../strongs/g/g5599.md) [apistos](../../strongs/g/g571.md) [genea](../../strongs/g/g1074.md), how long shall I be with you? how long shall I [anechō](../../strongs/g/g430.md) you? [pherō](../../strongs/g/g5342.md) him unto me.**

<a name="mark_9_20"></a>Mark 9:20

And they [pherō](../../strongs/g/g5342.md) him unto him: and when he [eidō](../../strongs/g/g1492.md) him, [eutheōs](../../strongs/g/g2112.md) the [pneuma](../../strongs/g/g4151.md) [sparassō](../../strongs/g/g4682.md) him; and he [piptō](../../strongs/g/g4098.md) on the [gē](../../strongs/g/g1093.md), and [kyliō](../../strongs/g/g2947.md) [aphrizō](../../strongs/g/g875.md).

<a name="mark_9_21"></a>Mark 9:21

And he [eperōtaō](../../strongs/g/g1905.md) his [patēr](../../strongs/g/g3962.md), **How long is it [chronos](../../strongs/g/g5550.md) since this [ginomai](../../strongs/g/g1096.md) unto him?**  And he [eipon](../../strongs/g/g2036.md), [paidiothen](../../strongs/g/g3812.md).

<a name="mark_9_22"></a>Mark 9:22

And [pollakis](../../strongs/g/g4178.md) it hath [ballō](../../strongs/g/g906.md) him into the [pyr](../../strongs/g/g4442.md), and into the [hydōr](../../strongs/g/g5204.md), to [apollymi](../../strongs/g/g622.md) him: but if thou canst do any thing, have [splagchnizomai](../../strongs/g/g4697.md) on us, and [boētheō](../../strongs/g/g997.md) us.

<a name="mark_9_23"></a>Mark 9:23

[Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto him, **If thou canst [pisteuō](../../strongs/g/g4100.md), all things are [dynatos](../../strongs/g/g1415.md) to him that [pisteuō](../../strongs/g/g4100.md).**

<a name="mark_9_24"></a>Mark 9:24

And [eutheōs](../../strongs/g/g2112.md) the [patēr](../../strongs/g/g3962.md) of the [paidion](../../strongs/g/g3813.md) [krazō](../../strongs/g/g2896.md), and [legō](../../strongs/g/g3004.md) with [dakry](../../strongs/g/g1144.md), [kyrios](../../strongs/g/g2962.md), I [pisteuō](../../strongs/g/g4100.md); [boētheō](../../strongs/g/g997.md) thou mine [apistia](../../strongs/g/g570.md).

<a name="mark_9_25"></a>Mark 9:25

When [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that the [ochlos](../../strongs/g/g3793.md) [episyntrechō](../../strongs/g/g1998.md), he [epitimaō](../../strongs/g/g2008.md) the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), [legō](../../strongs/g/g3004.md) unto him, **[alalos](../../strongs/g/g216.md) and [kōphos](../../strongs/g/g2974.md) [pneuma](../../strongs/g/g4151.md), I [epitassō](../../strongs/g/g2004.md) thee, [exerchomai](../../strongs/g/g1831.md) out of him, and [eiserchomai](../../strongs/g/g1525.md) [mēketi](../../strongs/g/g3371.md) into him.**

<a name="mark_9_26"></a>Mark 9:26

And [krazō](../../strongs/g/g2896.md), and [sparassō](../../strongs/g/g4682.md) him [polys](../../strongs/g/g4183.md), and [exerchomai](../../strongs/g/g1831.md): and he was as [nekros](../../strongs/g/g3498.md); insomuch that many [legō](../../strongs/g/g3004.md), He is [apothnēskō](../../strongs/g/g599.md).

<a name="mark_9_27"></a>Mark 9:27

But [Iēsous](../../strongs/g/g2424.md) [krateō](../../strongs/g/g2902.md) him by the [cheir](../../strongs/g/g5495.md), and [egeirō](../../strongs/g/g1453.md) him up; and he [anistēmi](../../strongs/g/g450.md).

<a name="mark_9_28"></a>Mark 9:28

And when he was [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md), his [mathētēs](../../strongs/g/g3101.md) [eperōtaō](../../strongs/g/g1905.md) him [idios](../../strongs/g/g2398.md), Why could not we [ekballō](../../strongs/g/g1544.md) him out?

<a name="mark_9_29"></a>Mark 9:29

And he [eipon](../../strongs/g/g2036.md) unto them, **This [genos](../../strongs/g/g1085.md) can [exerchomai](../../strongs/g/g1831.md) by [oudeis](../../strongs/g/g3762.md), but by [proseuchē](../../strongs/g/g4335.md) and [nēsteia](../../strongs/g/g3521.md).**

<a name="mark_9_30"></a>Mark 9:30

And they [exerchomai](../../strongs/g/g1831.md) thence, and [paraporeuomai](../../strongs/g/g3899.md) through [Galilaia](../../strongs/g/g1056.md); and he would not that [tis](../../strongs/g/g5100.md) should [ginōskō](../../strongs/g/g1097.md).

<a name="mark_9_31"></a>Mark 9:31

For he [didaskō](../../strongs/g/g1321.md) his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md) unto them, **The [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md) is [paradidōmi](../../strongs/g/g3860.md) into the [cheir](../../strongs/g/g5495.md) of [anthrōpos](../../strongs/g/g444.md), and they shall [apokteinō](../../strongs/g/g615.md) him; and after that he is [apokteinō](../../strongs/g/g615.md), he shall [anistēmi](../../strongs/g/g450.md) the third [hēmera](../../strongs/g/g2250.md).**

<a name="mark_9_32"></a>Mark 9:32

But they [agnoeō](../../strongs/g/g50.md) that [rhēma](../../strongs/g/g4487.md), and were [phobeō](../../strongs/g/g5399.md) to [eperōtaō](../../strongs/g/g1905.md) him.

<a name="mark_9_33"></a>Mark 9:33

And he [erchomai](../../strongs/g/g2064.md) to [Kapharnaoum](../../strongs/g/g2584.md): and being in the [oikia](../../strongs/g/g3614.md) he [eperōtaō](../../strongs/g/g1905.md) them, **What was it that ye [dialogizomai](../../strongs/g/g1260.md) among yourselves by the [hodos](../../strongs/g/g3598.md)?**

<a name="mark_9_34"></a>Mark 9:34

But they [siōpaō](../../strongs/g/g4623.md): for by the [hodos](../../strongs/g/g3598.md) they had [dialegomai](../../strongs/g/g1256.md) among [allēlōn](../../strongs/g/g240.md), who the [meizōn](../../strongs/g/g3187.md).

<a name="mark_9_35"></a>Mark 9:35

And he [kathizō](../../strongs/g/g2523.md), and [phōneō](../../strongs/g/g5455.md) the [dōdeka](../../strongs/g/g1427.md), and [legō](../../strongs/g/g3004.md) unto them, **[ei tis](../../strongs/g/g1536.md) [thelō](../../strongs/g/g2309.md) to be [prōtos](../../strongs/g/g4413.md), shall be [eschatos](../../strongs/g/g2078.md) of all, and [diakonos](../../strongs/g/g1249.md) of all.**

<a name="mark_9_36"></a>Mark 9:36

And he [lambanō](../../strongs/g/g2983.md) a [paidion](../../strongs/g/g3813.md), and [histēmi](../../strongs/g/g2476.md) him in the [mesos](../../strongs/g/g3319.md) of them: and when he had [enagkalizomai](../../strongs/g/g1723.md) him, he [eipon](../../strongs/g/g2036.md) unto them,

<a name="mark_9_37"></a>Mark 9:37

**Whosoever shall [dechomai](../../strongs/g/g1209.md) one of such [paidion](../../strongs/g/g3813.md) in my [onoma](../../strongs/g/g3686.md), [dechomai](../../strongs/g/g1209.md) me: and whosoever shall [dechomai](../../strongs/g/g1209.md) me, [dechomai](../../strongs/g/g1209.md) not me, but him that [apostellō](../../strongs/g/g649.md) me.**

<a name="mark_9_38"></a>Mark 9:38

And [Iōannēs](../../strongs/g/g2491.md) [apokrinomai](../../strongs/g/g611.md) him, [legō](../../strongs/g/g3004.md), [didaskalos](../../strongs/g/g1320.md), we [eidō](../../strongs/g/g1492.md) one [ekballō](../../strongs/g/g1544.md) [daimonion](../../strongs/g/g1140.md) in thy [onoma](../../strongs/g/g3686.md), and he [akoloutheō](../../strongs/g/g190.md) not us: and we [kōlyō](../../strongs/g/g2967.md) him, because he [akoloutheō](../../strongs/g/g190.md) not us. [^1]

<a name="mark_9_39"></a>Mark 9:39

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md), **[kōlyō](../../strongs/g/g2967.md) him not: for there is [oudeis](../../strongs/g/g3762.md) which shall [poieō](../../strongs/g/g4160.md) a [dynamis](../../strongs/g/g1411.md) in my [onoma](../../strongs/g/g3686.md), that can [tachy](../../strongs/g/g5035.md) [kakologeō](../../strongs/g/g2551.md) of me.**

<a name="mark_9_40"></a>Mark 9:40

**For he that is not against us is on our part.**

<a name="mark_9_41"></a>Mark 9:41

**For whosoever shall [potizō](../../strongs/g/g4222.md) you a [potērion](../../strongs/g/g4221.md) of [hydōr](../../strongs/g/g5204.md) in my [onoma](../../strongs/g/g3686.md), because ye belong to [Christos](../../strongs/g/g5547.md), [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, he shall not [apollymi](../../strongs/g/g622.md) his [misthos](../../strongs/g/g3408.md).**

<a name="mark_9_42"></a>Mark 9:42

**And whosoever shall [skandalizō](../../strongs/g/g4624.md) one of [mikros](../../strongs/g/g3398.md) that [pisteuō](../../strongs/g/g4100.md) in me, it is [kalos](../../strongs/g/g2570.md) for him that a [lithos](../../strongs/g/g3037.md) [mylikos](../../strongs/g/g3457.md) were [perikeimai](../../strongs/g/g4029.md) about his [trachēlos](../../strongs/g/g5137.md), and he were [ballō](../../strongs/g/g906.md) into the [thalassa](../../strongs/g/g2281.md).**

<a name="mark_9_43"></a>Mark 9:43

**And if thy [cheir](../../strongs/g/g5495.md) [skandalizō](../../strongs/g/g4624.md) thee, [apokoptō](../../strongs/g/g609.md) it: it is [kalos](../../strongs/g/g2570.md) for thee to [eiserchomai](../../strongs/g/g1525.md) into [zōē](../../strongs/g/g2222.md) [kyllos](../../strongs/g/g2948.md), than having two [cheir](../../strongs/g/g5495.md) to [aperchomai](../../strongs/g/g565.md) into [geenna](../../strongs/g/g1067.md), into the [pyr](../../strongs/g/g4442.md) that [asbestos](../../strongs/g/g762.md):** [^2]

<a name="mark_9_44"></a>Mark 9:44

**Where their [skōlēx](../../strongs/g/g4663.md) [teleutaō](../../strongs/g/g5053.md) not, and the [pyr](../../strongs/g/g4442.md) is not [sbennymi](../../strongs/g/g4570.md).**

<a name="mark_9_45"></a>Mark 9:45

**And if thy [pous](../../strongs/g/g4228.md) [skandalizō](../../strongs/g/g4624.md) thee, [apokoptō](../../strongs/g/g609.md) it off: it is [kalos](../../strongs/g/g2570.md) for thee to [eiserchomai](../../strongs/g/g1525.md) [chōlos](../../strongs/g/g5560.md) into [zōē](../../strongs/g/g2222.md), than having two [pous](../../strongs/g/g4228.md) to be [ballō](../../strongs/g/g906.md) into [geenna](../../strongs/g/g1067.md), into the [pyr](../../strongs/g/g4442.md) that never shall be [asbestos](../../strongs/g/g762.md):**

<a name="mark_9_46"></a>Mark 9:46

**Where their [skōlēx](../../strongs/g/g4663.md) [teleutaō](../../strongs/g/g5053.md) not, and the [pyr](../../strongs/g/g4442.md) is not [sbennymi](../../strongs/g/g4570.md).**

<a name="mark_9_47"></a>Mark 9:47

**And if thine [ophthalmos](../../strongs/g/g3788.md) [skandalizō](../../strongs/g/g4624.md) thee, [ekballō](../../strongs/g/g1544.md) it: it is [kalos](../../strongs/g/g2570.md) for thee to [eiserchomai](../../strongs/g/g1525.md) into the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) with [monophthalmos](../../strongs/g/g3442.md), than having two [ophthalmos](../../strongs/g/g3788.md) to be [ballō](../../strongs/g/g906.md) into [geenna](../../strongs/g/g1067.md) [pyr](../../strongs/g/g4442.md):**

<a name="mark_9_48"></a>Mark 9:48

**Where their [skōlēx](../../strongs/g/g4663.md) [teleutaō](../../strongs/g/g5053.md) not, and the [pyr](../../strongs/g/g4442.md) is not [sbennymi](../../strongs/g/g4570.md).**

<a name="mark_9_49"></a>Mark 9:49

**For every one shall be [halizō](../../strongs/g/g233.md) with [pyr](../../strongs/g/g4442.md), and every [thysia](../../strongs/g/g2378.md) shall be [halizō](../../strongs/g/g233.md) with [hals](../../strongs/g/g251.md).** [^3]

<a name="mark_9_50"></a>Mark 9:50

**[halas](../../strongs/g/g217.md) is [kalos](../../strongs/g/g2570.md): but if the [halas](../../strongs/g/g217.md) have [analos](../../strongs/g/g358.md), wherewith will ye [artyō](../../strongs/g/g741.md) it? Have [halas](../../strongs/g/g217.md) in yourselves, and [eirēneuō](../../strongs/g/g1514.md) with [allēlōn](../../strongs/g/g240.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 8](mark_8.md) - [Mark 10](mark_10.md)

---

[^1]: [Mark 9:38 Commentary](../../commentary/mark/mark_9_commentary.md#mark_9_38)

[^2]: [Mark 9:43 Commentary](../../commentary/mark/mark_9_commentary.md#mark_9_43)

[^3]: [Mark 9:49 Commentary](../../commentary/mark/mark_9_commentary.md#mark_9_49)
