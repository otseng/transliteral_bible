# [Mark 5](https://www.blueletterbible.org/kjv/mar/5/1/rl1/s_961001)

<a name="mark_5_1"></a>Mark 5:1

And they [erchomai](../../strongs/g/g2064.md) unto the [peran](../../strongs/g/g4008.md) of the [thalassa](../../strongs/g/g2281.md), into the [chōra](../../strongs/g/g5561.md) of the [Gadarēnos](../../strongs/g/g1046.md).

<a name="mark_5_2"></a>Mark 5:2

And when he was [exerchomai](../../strongs/g/g1831.md) of the [ploion](../../strongs/g/g4143.md), [eutheōs](../../strongs/g/g2112.md) there [apantaō](../../strongs/g/g528.md) him out of the [mnēmeion](../../strongs/g/g3419.md) an [anthrōpos](../../strongs/g/g444.md) with an [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md),

<a name="mark_5_3"></a>Mark 5:3

Who had [katoikēsis](../../strongs/g/g2731.md) among the [mnēmeion](../../strongs/g/g3419.md); and [oudeis](../../strongs/g/g3762.md) could [deō](../../strongs/g/g1210.md) him, no, not with [halysis](../../strongs/g/g254.md):

<a name="mark_5_4"></a>Mark 5:4

Because that he had been often [deō](../../strongs/g/g1210.md) with [pedē](../../strongs/g/g3976.md) and [halysis](../../strongs/g/g254.md), and the [halysis](../../strongs/g/g254.md) had been [diaspaō](../../strongs/g/g1288.md) by him, and the [pedē](../../strongs/g/g3976.md) [syntribō](../../strongs/g/g4937.md): neither could any [damazō](../../strongs/g/g1150.md) him.

<a name="mark_5_5"></a>Mark 5:5

And [diapantos](../../strongs/g/g1275.md), [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md), he was in the [oros](../../strongs/g/g3735.md), and in the [mnēma](../../strongs/g/g3418.md), [krazō](../../strongs/g/g2896.md), and [katakoptō](../../strongs/g/g2629.md) himself with [lithos](../../strongs/g/g3037.md).

<a name="mark_5_6"></a>Mark 5:6

But when he [eidō](../../strongs/g/g1492.md) [Iēsous](../../strongs/g/g2424.md) [makrothen](../../strongs/g/g3113.md), he [trechō](../../strongs/g/g5143.md) and [proskyneō](../../strongs/g/g4352.md) him,

<a name="mark_5_7"></a>Mark 5:7

And [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), and [eipon](../../strongs/g/g2036.md), What have I to do with thee, [Iēsous](../../strongs/g/g2424.md), [huios](../../strongs/g/g5207.md) of the [hypsistos](../../strongs/g/g5310.md) [theos](../../strongs/g/g2316.md)? I [horkizō](../../strongs/g/g3726.md) thee by [theos](../../strongs/g/g2316.md), that thou [basanizō](../../strongs/g/g928.md) me not.

<a name="mark_5_8"></a>Mark 5:8

For he [legō](../../strongs/g/g3004.md) unto him, **[exerchomai](../../strongs/g/g1831.md) of the [anthrōpos](../../strongs/g/g444.md), [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md).**

<a name="mark_5_9"></a>Mark 5:9

And he [eperōtaō](../../strongs/g/g1905.md) him, **What thy [onoma](../../strongs/g/g3686.md)?** And he [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md), My [onoma](../../strongs/g/g3686.md) [legiōn](../../strongs/g/g3003.md): for we are [polys](../../strongs/g/g4183.md).

<a name="mark_5_10"></a>Mark 5:10

And he [parakaleō](../../strongs/g/g3870.md) him [polys](../../strongs/g/g4183.md) that he would not [apostellō](../../strongs/g/g649.md) them away out of the [chōra](../../strongs/g/g5561.md).

<a name="mark_5_11"></a>Mark 5:11

Now there was there nigh unto the [oros](../../strongs/g/g3735.md) a [megas](../../strongs/g/g3173.md) [agelē](../../strongs/g/g34.md) of [choiros](../../strongs/g/g5519.md) [boskō](../../strongs/g/g1006.md).

<a name="mark_5_12"></a>Mark 5:12

And all the [daimōn](../../strongs/g/g1142.md) [parakaleō](../../strongs/g/g3870.md) him, [legō](../../strongs/g/g3004.md), [pempō](../../strongs/g/g3992.md) us into the [choiros](../../strongs/g/g5519.md), that we may [eiserchomai](../../strongs/g/g1525.md) into them.

<a name="mark_5_13"></a>Mark 5:13

And [eutheōs](../../strongs/g/g2112.md) [Iēsous](../../strongs/g/g2424.md) [epitrepō](../../strongs/g/g2010.md) them. And the [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) [exerchomai](../../strongs/g/g1831.md), and [eiserchomai](../../strongs/g/g1525.md) into the [choiros](../../strongs/g/g5519.md): and the [agelē](../../strongs/g/g34.md) [hormaō](../../strongs/g/g3729.md) violently down a [krēmnos](../../strongs/g/g2911.md) into the [thalassa](../../strongs/g/g2281.md), (they were about two thousand;) and were [pnigō](../../strongs/g/g4155.md) in the [thalassa](../../strongs/g/g2281.md).

<a name="mark_5_14"></a>Mark 5:14

And they that [boskō](../../strongs/g/g1006.md) the [choiros](../../strongs/g/g5519.md) [pheugō](../../strongs/g/g5343.md), and [anaggellō](../../strongs/g/g312.md) in the [polis](../../strongs/g/g4172.md), and in the [agros](../../strongs/g/g68.md). And they [exerchomai](../../strongs/g/g1831.md) to [eidō](../../strongs/g/g1492.md) what it was that was [ginomai](../../strongs/g/g1096.md).

<a name="mark_5_15"></a>Mark 5:15

And they [erchomai](../../strongs/g/g2064.md) to [Iēsous](../../strongs/g/g2424.md), and [theōreō](../../strongs/g/g2334.md) [daimonizomai](../../strongs/g/g1139.md), and had the [legiōn](../../strongs/g/g3003.md), [kathēmai](../../strongs/g/g2521.md), and [himatizō](../../strongs/g/g2439.md), and [sōphroneō](../../strongs/g/g4993.md): and they were [phobeō](../../strongs/g/g5399.md).

<a name="mark_5_16"></a>Mark 5:16

And they that [eidō](../../strongs/g/g1492.md) [diēgeomai](../../strongs/g/g1334.md) them how it [ginomai](../../strongs/g/g1096.md) to him that was [daimonizomai](../../strongs/g/g1139.md), and also concerning the [choiros](../../strongs/g/g5519.md).

<a name="mark_5_17"></a>Mark 5:17

And they [archomai](../../strongs/g/g756.md) to [parakaleō](../../strongs/g/g3870.md) him to [aperchomai](../../strongs/g/g565.md) out of their [horion](../../strongs/g/g3725.md).

<a name="mark_5_18"></a>Mark 5:18

And when he was [embainō](../../strongs/g/g1684.md) into the [ploion](../../strongs/g/g4143.md), [daimonizomai](../../strongs/g/g1139.md) [parakaleō](../../strongs/g/g3870.md) him that he might be with him.

<a name="mark_5_19"></a>Mark 5:19

Howbeit [Iēsous](../../strongs/g/g2424.md) [aphiēmi](../../strongs/g/g863.md) him not, but [legō](../../strongs/g/g3004.md) unto him, **[hypagō](../../strongs/g/g5217.md) [eis](../../strongs/g/g1519.md) [oikos](../../strongs/g/g3624.md) to thy [sos](../../strongs/g/g4674.md), and [anaggellō](../../strongs/g/g312.md) them [hosos](../../strongs/g/g3745.md) the [kyrios](../../strongs/g/g2962.md) hath [poieō](../../strongs/g/g4160.md) for thee, and hath had [eleeō](../../strongs/g/g1653.md) on thee.**

<a name="mark_5_20"></a>Mark 5:20

And he [aperchomai](../../strongs/g/g565.md), and [archomai](../../strongs/g/g756.md) to [kēryssō](../../strongs/g/g2784.md) in [Dekapolis](../../strongs/g/g1179.md) [hosos](../../strongs/g/g3745.md) [Iēsous](../../strongs/g/g2424.md) had [poieō](../../strongs/g/g4160.md) for him: and all did [thaumazō](../../strongs/g/g2296.md).

<a name="mark_5_21"></a>Mark 5:21

And when [Iēsous](../../strongs/g/g2424.md) was [diaperaō](../../strongs/g/g1276.md) again by [ploion](../../strongs/g/g4143.md) unto the [peran](../../strongs/g/g4008.md), [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [synagō](../../strongs/g/g4863.md) unto him: and he was [para](../../strongs/g/g3844.md) unto the [thalassa](../../strongs/g/g2281.md).

<a name="mark_5_22"></a>Mark 5:22

And, [idou](../../strongs/g/g2400.md), there [erchomai](../../strongs/g/g2064.md) one [archisynagōgos](../../strongs/g/g752.md), [Iaïros](../../strongs/g/g2383.md) by [onoma](../../strongs/g/g3686.md); and when he [eidō](../../strongs/g/g1492.md) him, he [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md),

<a name="mark_5_23"></a>Mark 5:23

And [parakaleō](../../strongs/g/g3870.md) him [polys](../../strongs/g/g4183.md), [legō](../../strongs/g/g3004.md), My [thygatrion](../../strongs/g/g2365.md) [eschatōs](../../strongs/g/g2079.md) [echō](../../strongs/g/g2192.md): [erchomai](../../strongs/g/g2064.md) and [epitithēmi](../../strongs/g/g2007.md) thy [cheir](../../strongs/g/g5495.md) on her, that she may be [sōzō](../../strongs/g/g4982.md); and she shall [zaō](../../strongs/g/g2198.md).

<a name="mark_5_24"></a>Mark 5:24

And [aperchomai](../../strongs/g/g565.md) with him; and [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akoloutheō](../../strongs/g/g190.md) him, and [synthlibō](../../strongs/g/g4918.md) him.

<a name="mark_5_25"></a>Mark 5:25

And a certain [gynē](../../strongs/g/g1135.md), which had a [rhysis](../../strongs/g/g4511.md) of [haima](../../strongs/g/g129.md) [dōdeka](../../strongs/g/g1427.md) [etos](../../strongs/g/g2094.md),

<a name="mark_5_26"></a>Mark 5:26

And had [paschō](../../strongs/g/g3958.md) [polys](../../strongs/g/g4183.md) of [polys](../../strongs/g/g4183.md) [iatros](../../strongs/g/g2395.md), and had [dapanaō](../../strongs/g/g1159.md) all that she had, and [ōpheleō](../../strongs/g/g5623.md) [mēdeis](../../strongs/g/g3367.md), but rather [erchomai](../../strongs/g/g2064.md) [cheirōn](../../strongs/g/g5501.md),

<a name="mark_5_27"></a>Mark 5:27

When she had [akouō](../../strongs/g/g191.md) of [Iēsous](../../strongs/g/g2424.md), [erchomai](../../strongs/g/g2064.md) in the [ochlos](../../strongs/g/g3793.md) [opisthen](../../strongs/g/g3693.md), and [haptomai](../../strongs/g/g680.md) his [himation](../../strongs/g/g2440.md).

<a name="mark_5_28"></a>Mark 5:28

For she [legō](../../strongs/g/g3004.md), If I may [haptomai](../../strongs/g/g680.md) but his [himation](../../strongs/g/g2440.md), I shall be [sōzō](../../strongs/g/g4982.md).

<a name="mark_5_29"></a>Mark 5:29

And [eutheōs](../../strongs/g/g2112.md) the [pēgē](../../strongs/g/g4077.md) of her [haima](../../strongs/g/g129.md) was [xērainō](../../strongs/g/g3583.md); and she [ginōskō](../../strongs/g/g1097.md) in [sōma](../../strongs/g/g4983.md) that she was [iaomai](../../strongs/g/g2390.md) of that [mastix](../../strongs/g/g3148.md).

<a name="mark_5_30"></a>Mark 5:30

And [Iēsous](../../strongs/g/g2424.md), [eutheōs](../../strongs/g/g2112.md) [epiginōskō](../../strongs/g/g1921.md) in himself that [dynamis](../../strongs/g/g1411.md) had [exerchomai](../../strongs/g/g1831.md) of him, [epistrephō](../../strongs/g/g1994.md) in the [ochlos](../../strongs/g/g3793.md), and [legō](../../strongs/g/g3004.md), **Who [haptomai](../../strongs/g/g680.md) my [himation](../../strongs/g/g2440.md)?**

<a name="mark_5_31"></a>Mark 5:31

And his [mathētēs](../../strongs/g/g3101.md) [legō](../../strongs/g/g3004.md) unto him, Thou [blepō](../../strongs/g/g991.md) the [ochlos](../../strongs/g/g3793.md) [synthlibō](../../strongs/g/g4918.md) thee, and [legō](../../strongs/g/g3004.md) thou, **Who [haptomai](../../strongs/g/g680.md) me?**

<a name="mark_5_32"></a>Mark 5:32

And he [periblepō](../../strongs/g/g4017.md) to [eidō](../../strongs/g/g1492.md) her that had [poieō](../../strongs/g/g4160.md) this thing.

<a name="mark_5_33"></a>Mark 5:33

But the [gynē](../../strongs/g/g1135.md) [phobeō](../../strongs/g/g5399.md) and [tremō](../../strongs/g/g5141.md), [eidō](../../strongs/g/g1492.md) what was [ginomai](../../strongs/g/g1096.md) in her, [erchomai](../../strongs/g/g2064.md) and [prospiptō](../../strongs/g/g4363.md) him, and [eipon](../../strongs/g/g2036.md) him all the [alētheia](../../strongs/g/g225.md).

<a name="mark_5_34"></a>Mark 5:34

And he [eipon](../../strongs/g/g2036.md) unto her, **[thygatēr](../../strongs/g/g2364.md), thy [pistis](../../strongs/g/g4102.md) hath made thee [sōzō](../../strongs/g/g4982.md); [hypagō](../../strongs/g/g5217.md) in [eirēnē](../../strongs/g/g1515.md), and be [hygiēs](../../strongs/g/g5199.md) of thy [mastix](../../strongs/g/g3148.md).**

<a name="mark_5_35"></a>Mark 5:35

While he yet [laleō](../../strongs/g/g2980.md), there [erchomai](../../strongs/g/g2064.md) from [archisynagōgos](../../strongs/g/g752.md) which [legō](../../strongs/g/g3004.md), Thy [thygatēr](../../strongs/g/g2364.md) is [apothnēskō](../../strongs/g/g599.md): why [skyllō](../../strongs/g/g4660.md) thou the [didaskalos](../../strongs/g/g1320.md) any further?

<a name="mark_5_36"></a>Mark 5:36

As [eutheōs](../../strongs/g/g2112.md) [Iēsous](../../strongs/g/g2424.md) [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) that was [laleō](../../strongs/g/g2980.md), he [legō](../../strongs/g/g3004.md) unto [archisynagōgos](../../strongs/g/g752.md), **Be not [phobeō](../../strongs/g/g5399.md), only [pisteuō](../../strongs/g/g4100.md).**

<a name="mark_5_37"></a>Mark 5:37

And he [aphiēmi](../../strongs/g/g863.md) [oudeis](../../strongs/g/g3762.md) to [synakoloutheō](../../strongs/g/g4870.md) him, save [Petros](../../strongs/g/g4074.md), and [Iakōbos](../../strongs/g/g2385.md), and [Iōannēs](../../strongs/g/g2491.md) the [adelphos](../../strongs/g/g80.md) of [Iakōbos](../../strongs/g/g2385.md).

<a name="mark_5_38"></a>Mark 5:38

And he [erchomai](../../strongs/g/g2064.md) to the [oikos](../../strongs/g/g3624.md) of [archisynagōgos](../../strongs/g/g752.md), and [theōreō](../../strongs/g/g2334.md) the [thorybos](../../strongs/g/g2351.md), and them that [klaiō](../../strongs/g/g2799.md) and [alalazō](../../strongs/g/g214.md) [polys](../../strongs/g/g4183.md).

<a name="mark_5_39"></a>Mark 5:39

And when he [eiserchomai](../../strongs/g/g1525.md), he [legō](../../strongs/g/g3004.md) unto them, **Why [thorybeō](../../strongs/g/g2350.md), and [klaiō](../../strongs/g/g2799.md)? the [paidion](../../strongs/g/g3813.md) is not [apothnēskō](../../strongs/g/g599.md), but [katheudō](../../strongs/g/g2518.md).**

<a name="mark_5_40"></a>Mark 5:40

And they [katagelaō](../../strongs/g/g2606.md). But when he had [ekballō](../../strongs/g/g1544.md) them all, he [paralambanō](../../strongs/g/g3880.md) the [patēr](../../strongs/g/g3962.md) and the [mētēr](../../strongs/g/g3384.md) of the [paidion](../../strongs/g/g3813.md), and them that were with him, and [eisporeuomai](../../strongs/g/g1531.md) in where the [paidion](../../strongs/g/g3813.md) was [anakeimai](../../strongs/g/g345.md).

<a name="mark_5_41"></a>Mark 5:41

And he [krateō](../../strongs/g/g2902.md) the [paidion](../../strongs/g/g3813.md) by the [cheir](../../strongs/g/g5495.md), and [legō](../../strongs/g/g3004.md) unto her, **[talitha](../../strongs/g/g5008.md) [koum](../../strongs/g/g2891.md)**; which is, [methermēneuō](../../strongs/g/g3177.md), [korasion](../../strongs/g/g2877.md), **I [legō](../../strongs/g/g3004.md) unto thee, [egeirō](../../strongs/g/g1453.md).**

<a name="mark_5_42"></a>Mark 5:42

And [eutheōs](../../strongs/g/g2112.md) the [korasion](../../strongs/g/g2877.md) [anistēmi](../../strongs/g/g450.md), and [peripateō](../../strongs/g/g4043.md); for she was of [dōdeka](../../strongs/g/g1427.md) [etos](../../strongs/g/g2094.md). And they were [existēmi](../../strongs/g/g1839.md) with a [megas](../../strongs/g/g3173.md) [ekstasis](../../strongs/g/g1611.md).

<a name="mark_5_43"></a>Mark 5:43

And he [diastellō](../../strongs/g/g1291.md) them [polys](../../strongs/g/g4183.md) that [mēdeis](../../strongs/g/g3367.md) should [ginōskō](../../strongs/g/g1097.md) it; and [eipon](../../strongs/g/g2036.md) that something should be [didōmi](../../strongs/g/g1325.md) her to [phago](../../strongs/g/g5315.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 4](mark_4.md) - [Mark 6](mark_6.md)