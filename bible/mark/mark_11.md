# [Mark 11](https://www.blueletterbible.org/kjv/mar/11/1/rl1/s_966001)

<a name="mark_11_1"></a>Mark 11:1

And when they [eggizō](../../strongs/g/g1448.md) to [Ierousalēm](../../strongs/g/g2419.md), unto [Bēthphagē](../../strongs/g/g967.md) and [Bēthania](../../strongs/g/g963.md), at the [oros](../../strongs/g/g3735.md) of [elaia](../../strongs/g/g1636.md), he [apostellō](../../strongs/g/g649.md) two of his [mathētēs](../../strongs/g/g3101.md),

<a name="mark_11_2"></a>Mark 11:2

And [legō](../../strongs/g/g3004.md) unto them, **[hypagō](../../strongs/g/g5217.md) into the [kōmē](../../strongs/g/g2968.md) over [katenanti](../../strongs/g/g2713.md) you: and [eutheōs](../../strongs/g/g2112.md) ye be [eisporeuomai](../../strongs/g/g1531.md) into it, ye shall [heuriskō](../../strongs/g/g2147.md) a [pōlos](../../strongs/g/g4454.md) [deō](../../strongs/g/g1210.md), whereon never [anthrōpos](../../strongs/g/g444.md) [kathizō](../../strongs/g/g2523.md); [lyō](../../strongs/g/g3089.md) him, and [agō](../../strongs/g/g71.md).**

<a name="mark_11_3"></a>Mark 11:3

**And if [tis](../../strongs/g/g5100.md) [eipon](../../strongs/g/g2036.md) unto you, Why [poieō](../../strongs/g/g4160.md) ye this? [eipon](../../strongs/g/g2036.md) ye that the [kyrios](../../strongs/g/g2962.md) hath [chreia](../../strongs/g/g5532.md) of him; and [eutheōs](../../strongs/g/g2112.md) he will [apostellō](../../strongs/g/g649.md) him hither.**

<a name="mark_11_4"></a>Mark 11:4

And they [aperchomai](../../strongs/g/g565.md), and [heuriskō](../../strongs/g/g2147.md) the [pōlos](../../strongs/g/g4454.md) [deō](../../strongs/g/g1210.md) by the [thyra](../../strongs/g/g2374.md) without in [amphodon](../../strongs/g/g296.md); and they [lyō](../../strongs/g/g3089.md) him.

<a name="mark_11_5"></a>Mark 11:5

And certain of them that [histēmi](../../strongs/g/g2476.md) there [legō](../../strongs/g/g3004.md) unto them, What [poieō](../../strongs/g/g4160.md) ye, [lyō](../../strongs/g/g3089.md) the [pōlos](../../strongs/g/g4454.md)?

<a name="mark_11_6"></a>Mark 11:6

And they [eipon](../../strongs/g/g2036.md) unto them even as [Iēsous](../../strongs/g/g2424.md) had [entellō](../../strongs/g/g1781.md): and they [aphiēmi](../../strongs/g/g863.md) them.

<a name="mark_11_7"></a>Mark 11:7

And they [agō](../../strongs/g/g71.md) the [pōlos](../../strongs/g/g4454.md) to [Iēsous](../../strongs/g/g2424.md), and [epiballō](../../strongs/g/g1911.md) their [himation](../../strongs/g/g2440.md) on him; and he [kathizō](../../strongs/g/g2523.md) upon him.

<a name="mark_11_8"></a>Mark 11:8

And [polys](../../strongs/g/g4183.md) [strōnnyō](../../strongs/g/g4766.md) their [himation](../../strongs/g/g2440.md) in [hodos](../../strongs/g/g3598.md): and others [koptō](../../strongs/g/g2875.md) [stibas](../../strongs/g/g4746.md) off the [dendron](../../strongs/g/g1186.md), and [strōnnyō](../../strongs/g/g4766.md) them in [hodos](../../strongs/g/g3598.md).

<a name="mark_11_9"></a>Mark 11:9

And they that [proagō](../../strongs/g/g4254.md), and they that [akoloutheō](../../strongs/g/g190.md), [krazō](../../strongs/g/g2896.md), [legō](../../strongs/g/g3004.md), [hōsanna](../../strongs/g/g5614.md); [eulogeō](../../strongs/g/g2127.md) is he that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md):

<a name="mark_11_10"></a>Mark 11:10

[eulogeō](../../strongs/g/g2127.md) the [basileia](../../strongs/g/g932.md) of our [patēr](../../strongs/g/g3962.md) [Dabid](../../strongs/g/g1138.md), that [erchomai](../../strongs/g/g2064.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md): [hōsanna](../../strongs/g/g5614.md) in [hypsistos](../../strongs/g/g5310.md).

<a name="mark_11_11"></a>Mark 11:11

And [Iēsous](../../strongs/g/g2424.md) [eiserchomai](../../strongs/g/g1525.md) into [Hierosolyma](../../strongs/g/g2414.md), and into the [hieron](../../strongs/g/g2411.md): and when he had [periblepō](../../strongs/g/g4017.md) upon all things, and now [opsios](../../strongs/g/g3798.md) [hōra](../../strongs/g/g5610.md) [ōn](../../strongs/g/g5607.md), he [exerchomai](../../strongs/g/g1831.md) unto [Bēthania](../../strongs/g/g963.md) with the [dōdeka](../../strongs/g/g1427.md).

<a name="mark_11_12"></a>Mark 11:12

And on [epaurion](../../strongs/g/g1887.md), when they were [exerchomai](../../strongs/g/g1831.md) from [Bēthania](../../strongs/g/g963.md), he was [peinaō](../../strongs/g/g3983.md):

<a name="mark_11_13"></a>Mark 11:13

And [eidō](../../strongs/g/g1492.md) a [sykē](../../strongs/g/g4808.md) [makrothen](../../strongs/g/g3113.md) having [phyllon](../../strongs/g/g5444.md), he [erchomai](../../strongs/g/g2064.md), if haply he might [heuriskō](../../strongs/g/g2147.md) any thing thereon: and when he [erchomai](../../strongs/g/g2064.md) to it, he [heuriskō](../../strongs/g/g2147.md) nothing but [phyllon](../../strongs/g/g5444.md); for the [kairos](../../strongs/g/g2540.md) of [sykon](../../strongs/g/g4810.md) was not yet.

<a name="mark_11_14"></a>Mark 11:14

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto it, **[mēdeis](../../strongs/g/g3367.md) [phago](../../strongs/g/g5315.md) [karpos](../../strongs/g/g2590.md) of thee [mēketi](../../strongs/g/g3371.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). And his [mathētēs](../../strongs/g/g3101.md) [akouō](../../strongs/g/g191.md).**

<a name="mark_11_15"></a>Mark 11:15

And they [erchomai](../../strongs/g/g2064.md) to [Hierosolyma](../../strongs/g/g2414.md): and [Iēsous](../../strongs/g/g2424.md) [eiserchomai](../../strongs/g/g1525.md) into the [hieron](../../strongs/g/g2411.md), and [archomai](../../strongs/g/g756.md) to [ekballō](../../strongs/g/g1544.md) them that [pōleō](../../strongs/g/g4453.md) and [agorazō](../../strongs/g/g59.md) in the [hieron](../../strongs/g/g2411.md), and [katastrephō](../../strongs/g/g2690.md) the [trapeza](../../strongs/g/g5132.md) of the [kollybistēs](../../strongs/g/g2855.md), and the [kathedra](../../strongs/g/g2515.md) of them that [pōleō](../../strongs/g/g4453.md) [peristera](../../strongs/g/g4058.md);

<a name="mark_11_16"></a>Mark 11:16

And would not [aphiēmi](../../strongs/g/g863.md) that [tis](../../strongs/g/g5100.md) should [diapherō](../../strongs/g/g1308.md) [skeuos](../../strongs/g/g4632.md) through the [hieron](../../strongs/g/g2411.md).

<a name="mark_11_17"></a>Mark 11:17

And he [didaskō](../../strongs/g/g1321.md), [legō](../../strongs/g/g3004.md) unto them, **Is it not [graphō](../../strongs/g/g1125.md), My [oikos](../../strongs/g/g3624.md) shall be [kaleō](../../strongs/g/g2564.md) of all [ethnos](../../strongs/g/g1484.md) the [oikos](../../strongs/g/g3624.md) of [proseuchē](../../strongs/g/g4335.md)? but ye have [poieō](../../strongs/g/g4160.md) it a [spēlaion](../../strongs/g/g4693.md) of [lēstēs](../../strongs/g/g3027.md).**

<a name="mark_11_18"></a>Mark 11:18

And the [grammateus](../../strongs/g/g1122.md) and [archiereus](../../strongs/g/g749.md) [akouō](../../strongs/g/g191.md), and [zēteō](../../strongs/g/g2212.md) how they might [apollymi](../../strongs/g/g622.md) him: for they [phobeō](../../strongs/g/g5399.md) him, because all the [ochlos](../../strongs/g/g3793.md) was [ekplēssō](../../strongs/g/g1605.md) at his [didachē](../../strongs/g/g1322.md).

<a name="mark_11_19"></a>Mark 11:19

And when [opse](../../strongs/g/g3796.md) was [ginomai](../../strongs/g/g1096.md), he [ekporeuomai](../../strongs/g/g1607.md) out of the [polis](../../strongs/g/g4172.md).

<a name="mark_11_20"></a>Mark 11:20

And in the [prōï](../../strongs/g/g4404.md), as they [paraporeuomai](../../strongs/g/g3899.md), they [eidō](../../strongs/g/g1492.md) the [sykē](../../strongs/g/g4808.md) [xērainō](../../strongs/g/g3583.md) from the [rhiza](../../strongs/g/g4491.md).

<a name="mark_11_21"></a>Mark 11:21

And [Petros](../../strongs/g/g4074.md) [anamimnēskō](../../strongs/g/g363.md) [legō](../../strongs/g/g3004.md) unto him, [rhabbi](../../strongs/g/g4461.md), [ide](../../strongs/g/g2396.md), the [sykē](../../strongs/g/g4808.md) which thou [kataraomai](../../strongs/g/g2672.md) is [xērainō](../../strongs/g/g3583.md).

<a name="mark_11_22"></a>Mark 11:22

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [legō](../../strongs/g/g3004.md) unto them, **Have [pistis](../../strongs/g/g4102.md) in [theos](../../strongs/g/g2316.md).**

<a name="mark_11_23"></a>Mark 11:23

**For [amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That whosoever shall [eipon](../../strongs/g/g2036.md) unto this [oros](../../strongs/g/g3735.md), [airō](../../strongs/g/g142.md), and be thou [ballō](../../strongs/g/g906.md) into the [thalassa](../../strongs/g/g2281.md); and shall not [diakrinō](../../strongs/g/g1252.md) in his [kardia](../../strongs/g/g2588.md), but shall [pisteuō](../../strongs/g/g4100.md) that those things which he [legō](../../strongs/g/g3004.md) shall [ginomai](../../strongs/g/g1096.md); he shall have whatsoever he [eipon](../../strongs/g/g2036.md).**

<a name="mark_11_24"></a>Mark 11:24

**Therefore I [legō](../../strongs/g/g3004.md) unto you, What things soever ye [aiteō](../../strongs/g/g154.md), when ye [proseuchomai](../../strongs/g/g4336.md), [pisteuō](../../strongs/g/g4100.md) that ye [lambanō](../../strongs/g/g2983.md), and ye shall have.**

<a name="mark_11_25"></a>Mark 11:25

**And when ye [stēkō](../../strongs/g/g4739.md) [proseuchomai](../../strongs/g/g4336.md), [aphiēmi](../../strongs/g/g863.md), if ye have ought against any: that your [patēr](../../strongs/g/g3962.md) also which is in [ouranos](../../strongs/g/g3772.md) may [aphiēmi](../../strongs/g/g863.md) you your [paraptōma](../../strongs/g/g3900.md).**

<a name="mark_11_26"></a>Mark 11:26

**But if ye do not [aphiēmi](../../strongs/g/g863.md), neither will your [patēr](../../strongs/g/g3962.md) which is in [ouranos](../../strongs/g/g3772.md) [aphiēmi](../../strongs/g/g863.md) your [paraptōma](../../strongs/g/g3900.md).** [^1]

<a name="mark_11_27"></a>Mark 11:27

And they [erchomai](../../strongs/g/g2064.md) again to [Hierosolyma](../../strongs/g/g2414.md): and as he was [peripateō](../../strongs/g/g4043.md) in the [hieron](../../strongs/g/g2411.md), there [erchomai](../../strongs/g/g2064.md) to him the [archiereus](../../strongs/g/g749.md), and the [grammateus](../../strongs/g/g1122.md), and the [presbyteros](../../strongs/g/g4245.md),

<a name="mark_11_28"></a>Mark 11:28

And [legō](../../strongs/g/g3004.md) unto him, By what [exousia](../../strongs/g/g1849.md) [poieō](../../strongs/g/g4160.md) thou these things? and who [didōmi](../../strongs/g/g1325.md) thee this [exousia](../../strongs/g/g1849.md) to [poieō](../../strongs/g/g4160.md) these things?

<a name="mark_11_29"></a>Mark 11:29

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **I will also [eperōtaō](../../strongs/g/g1905.md) of you one [logos](../../strongs/g/g3056.md), and [apokrinomai](../../strongs/g/g611.md) me, and I will [eipon](../../strongs/g/g2046.md) you by what [exousia](../../strongs/g/g1849.md) I [poieō](../../strongs/g/g4160.md) these things.**

<a name="mark_11_30"></a>Mark 11:30

**The [baptisma](../../strongs/g/g908.md) of [Iōannēs](../../strongs/g/g2491.md), was from [ouranos](../../strongs/g/g3772.md), or of [anthrōpos](../../strongs/g/g444.md)? [apokrinomai](../../strongs/g/g611.md) me.**

<a name="mark_11_31"></a>Mark 11:31

And they [logizomai](../../strongs/g/g3049.md) with themselves, [legō](../../strongs/g/g3004.md), If we shall [eipon](../../strongs/g/g2036.md), From [ouranos](../../strongs/g/g3772.md); he will [eipon](../../strongs/g/g2046.md), Why then did ye not [pisteuō](../../strongs/g/g4100.md) him?

<a name="mark_11_32"></a>Mark 11:32

But if we shall [eipon](../../strongs/g/g2036.md), Of [anthrōpos](../../strongs/g/g444.md); they [phobeō](../../strongs/g/g5399.md) the [laos](../../strongs/g/g2992.md): for all [echō](../../strongs/g/g2192.md) [Iōannēs](../../strongs/g/g2491.md), that he was a [prophētēs](../../strongs/g/g4396.md) [ontōs](../../strongs/g/g3689.md).

<a name="mark_11_33"></a>Mark 11:33

And they [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto [Iēsous](../../strongs/g/g2424.md), We cannot [eidō](../../strongs/g/g1492.md). And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [legō](../../strongs/g/g3004.md) unto them, **Neither do I [legō](../../strongs/g/g3004.md) you by what [exousia](../../strongs/g/g1849.md) I [poieō](../../strongs/g/g4160.md) these things.**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 10](mark_10.md) - [Mark 12](mark_12.md)

---

[^1]: [Mark 11:26 Commentary](../../commentary/mark/mark_11_commentary.md#mark_11_26)
