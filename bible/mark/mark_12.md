# [Mark 12](https://www.blueletterbible.org/kjv/mar/12/1/rl1/s_966001)

<a name="mark_12_1"></a>Mark 12:1

And he [archomai](../../strongs/g/g756.md) to [legō](../../strongs/g/g3004.md) unto them by [parabolē](../../strongs/g/g3850.md). **An [anthrōpos](../../strongs/g/g444.md) [phyteuō](../../strongs/g/g5452.md) an [ampelōn](../../strongs/g/g290.md), and [peritithēmi](../../strongs/g/g4060.md) [phragmos](../../strongs/g/g5418.md) about, and [oryssō](../../strongs/g/g3736.md) the [hypolēnion](../../strongs/g/g5276.md), and [oikodomeō](../../strongs/g/g3618.md) a [pyrgos](../../strongs/g/g4444.md), and [ekdidōmi](../../strongs/g/g1554.md) it to [geōrgos](../../strongs/g/g1092.md), and [apodēmeō](../../strongs/g/g589.md).**

<a name="mark_12_2"></a>Mark 12:2

**And at the [kairos](../../strongs/g/g2540.md) he [apostellō](../../strongs/g/g649.md) to the [geōrgos](../../strongs/g/g1092.md) a [doulos](../../strongs/g/g1401.md), that he might [lambanō](../../strongs/g/g2983.md) from the [geōrgos](../../strongs/g/g1092.md) of the [karpos](../../strongs/g/g2590.md) of the [ampelōn](../../strongs/g/g290.md).**

<a name="mark_12_3"></a>Mark 12:3

**And they [lambanō](../../strongs/g/g2983.md), and [derō](../../strongs/g/g1194.md) him, and [apostellō](../../strongs/g/g649.md) [kenos](../../strongs/g/g2756.md).**

<a name="mark_12_4"></a>Mark 12:4

**And again he [apostellō](../../strongs/g/g649.md) unto them another [doulos](../../strongs/g/g1401.md); and at him they [lithoboleō](../../strongs/g/g3036.md), and [kephalioō](../../strongs/g/g2775.md), and [apostellō](../../strongs/g/g649.md) [atimoō](../../strongs/g/g821.md).**

<a name="mark_12_5"></a>Mark 12:5

**And again he [apostellō](../../strongs/g/g649.md) another; and him they [apokteinō](../../strongs/g/g615.md), and [polys](../../strongs/g/g4183.md) others; [derō](../../strongs/g/g1194.md) some, and [apokteinō](../../strongs/g/g615.md) some.**

<a name="mark_12_6"></a>Mark 12:6

**Having yet therefore one [huios](../../strongs/g/g5207.md), his [agapētos](../../strongs/g/g27.md), he [apostellō](../../strongs/g/g649.md) him also [eschatos](../../strongs/g/g2078.md) unto them, [legō](../../strongs/g/g3004.md), They will [entrepō](../../strongs/g/g1788.md) my [huios](../../strongs/g/g5207.md).**

<a name="mark_12_7"></a>Mark 12:7

**But those [geōrgos](../../strongs/g/g1092.md) [eipon](../../strongs/g/g2036.md) among themselves, This is the [klēronomos](../../strongs/g/g2818.md); [deute](../../strongs/g/g1205.md), let us [apokteinō](../../strongs/g/g615.md) him, and the [klēronomia](../../strongs/g/g2817.md) shall be ours.**

<a name="mark_12_8"></a>Mark 12:8

**And they [lambanō](../../strongs/g/g2983.md) him, and [apokteinō](../../strongs/g/g615.md), and [ekballō](../../strongs/g/g1544.md) of the [ampelōn](../../strongs/g/g290.md).**

<a name="mark_12_9"></a>Mark 12:9

**What shall therefore the [kyrios](../../strongs/g/g2962.md) of the [ampelōn](../../strongs/g/g290.md) [poieō](../../strongs/g/g4160.md)? he will [erchomai](../../strongs/g/g2064.md) and [apollymi](../../strongs/g/g622.md) the [geōrgos](../../strongs/g/g1092.md), and will [didōmi](../../strongs/g/g1325.md) the [ampelōn](../../strongs/g/g290.md) unto others.**

<a name="mark_12_10"></a>Mark 12:10

**And have ye not [anaginōskō](../../strongs/g/g314.md) this [graphē](../../strongs/g/g1124.md); The [lithos](../../strongs/g/g3037.md) which the [oikodomeō](../../strongs/g/g3618.md) [apodokimazō](../../strongs/g/g593.md) is become the [kephalē](../../strongs/g/g2776.md) of the [gōnia](../../strongs/g/g1137.md):**

<a name="mark_12_11"></a>Mark 12:11

**This was the [kyrios](../../strongs/g/g2962.md) [ginomai](../../strongs/g/g1096.md), and it is [thaumastos](../../strongs/g/g2298.md) in our [ophthalmos](../../strongs/g/g3788.md)?**

<a name="mark_12_12"></a>Mark 12:12

And they [zēteō](../../strongs/g/g2212.md) to [krateō](../../strongs/g/g2902.md) on him, but [phobeō](../../strongs/g/g5399.md) the [ochlos](../../strongs/g/g3793.md): for they [ginōskō](../../strongs/g/g1097.md) that he had [eipon](../../strongs/g/g2036.md) the [parabolē](../../strongs/g/g3850.md) against them: and they [aphiēmi](../../strongs/g/g863.md) him, and [aperchomai](../../strongs/g/g565.md).

<a name="mark_12_13"></a>Mark 12:13

And they [apostellō](../../strongs/g/g649.md) unto him certain of the [Pharisaios](../../strongs/g/g5330.md) and of the [Hērōdianoi](../../strongs/g/g2265.md), to [agreuō](../../strongs/g/g64.md) him in [logos](../../strongs/g/g3056.md).

<a name="mark_12_14"></a>Mark 12:14

And when they [erchomai](../../strongs/g/g2064.md), they [legō](../../strongs/g/g3004.md) unto him, [didaskalos](../../strongs/g/g1320.md), we [eidō](../../strongs/g/g1492.md) that thou art [alēthēs](../../strongs/g/g227.md), and [melei](../../strongs/g/g3199.md) for [oudeis](../../strongs/g/g3762.md): for thou [blepō](../../strongs/g/g991.md) not the [prosōpon](../../strongs/g/g4383.md) of [anthrōpos](../../strongs/g/g444.md), but [didaskō](../../strongs/g/g1321.md) the [hodos](../../strongs/g/g3598.md) of [theos](../../strongs/g/g2316.md) in [alētheia](../../strongs/g/g225.md): Is it [exesti](../../strongs/g/g1832.md) to [didōmi](../../strongs/g/g1325.md) [kēnsos](../../strongs/g/g2778.md) to [Kaisar](../../strongs/g/g2541.md), or not?

<a name="mark_12_15"></a>Mark 12:15

Shall we [didōmi](../../strongs/g/g1325.md), or shall we not [didōmi](../../strongs/g/g1325.md)? But he, [eidō](../../strongs/g/g1492.md) their [hypokrisis](../../strongs/g/g5272.md), [eipon](../../strongs/g/g2036.md) unto them, **Why [peirazō](../../strongs/g/g3985.md) me? [pherō](../../strongs/g/g5342.md) me a [dēnarion](../../strongs/g/g1220.md), that I may [eidō](../../strongs/g/g1492.md) it.**

<a name="mark_12_16"></a>Mark 12:16

And they [pherō](../../strongs/g/g5342.md) it. And he [legō](../../strongs/g/g3004.md) unto them, **Whose this [eikōn](../../strongs/g/g1504.md) and [epigraphē](../../strongs/g/g1923.md)?** And they [eipon](../../strongs/g/g2036.md) unto him, [Kaisar](../../strongs/g/g2541.md).

<a name="mark_12_17"></a>Mark 12:17

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **[apodidōmi](../../strongs/g/g591.md) to [Kaisar](../../strongs/g/g2541.md) the things that are [Kaisar](../../strongs/g/g2541.md), and to [theos](../../strongs/g/g2316.md) the things that are [theos](../../strongs/g/g2316.md).** And they [thaumazō](../../strongs/g/g2296.md) at him.

<a name="mark_12_18"></a>Mark 12:18

Then [erchomai](../../strongs/g/g2064.md) unto him the [Saddoukaios](../../strongs/g/g4523.md), which [legō](../../strongs/g/g3004.md) there is no [anastasis](../../strongs/g/g386.md); and they [eperōtaō](../../strongs/g/g1905.md) him, [legō](../../strongs/g/g3004.md),

<a name="mark_12_19"></a>Mark 12:19

[didaskalos](../../strongs/g/g1320.md), [Mōÿsēs](../../strongs/g/g3475.md) [graphō](../../strongs/g/g1125.md) unto us, If [tis](../../strongs/g/g5100.md) [adelphos](../../strongs/g/g80.md) [apothnēskō](../../strongs/g/g599.md), and [kataleipō](../../strongs/g/g2641.md) [gynē](../../strongs/g/g1135.md), and [aphiēmi](../../strongs/g/g863.md) no [teknon](../../strongs/g/g5043.md), that his [adelphos](../../strongs/g/g80.md) should [lambanō](../../strongs/g/g2983.md) his [gynē](../../strongs/g/g1135.md), and [exanistēmi](../../strongs/g/g1817.md) [sperma](../../strongs/g/g4690.md) unto his [adelphos](../../strongs/g/g80.md).

<a name="mark_12_20"></a>Mark 12:20

Now there were seven [adelphos](../../strongs/g/g80.md): and the first [lambanō](../../strongs/g/g2983.md) a [gynē](../../strongs/g/g1135.md), and [apothnēskō](../../strongs/g/g599.md) [aphiēmi](../../strongs/g/g863.md) no [sperma](../../strongs/g/g4690.md).

<a name="mark_12_21"></a>Mark 12:21

And the second [lambanō](../../strongs/g/g2983.md) her, and [apothnēskō](../../strongs/g/g599.md), neither [aphiēmi](../../strongs/g/g863.md) he any [sperma](../../strongs/g/g4690.md): and the third [hōsautōs](../../strongs/g/g5615.md).

<a name="mark_12_22"></a>Mark 12:22

And the seven [lambanō](../../strongs/g/g2983.md) her, and [aphiēmi](../../strongs/g/g863.md) no [sperma](../../strongs/g/g4690.md): [eschatos](../../strongs/g/g2078.md) of all the [gynē](../../strongs/g/g1135.md) [apothnēskō](../../strongs/g/g599.md) also.

<a name="mark_12_23"></a>Mark 12:23

In the [anastasis](../../strongs/g/g386.md) therefore, when they shall [anistēmi](../../strongs/g/g450.md), whose [gynē](../../strongs/g/g1135.md) shall she be of them? for the seven had her to [gynē](../../strongs/g/g1135.md).

<a name="mark_12_24"></a>Mark 12:24

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) [eipon](../../strongs/g/g2036.md) unto them, **Do ye not therefore [planaō](../../strongs/g/g4105.md), because ye [eidō](../../strongs/g/g1492.md) not the [graphē](../../strongs/g/g1124.md), neither the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md)?**

<a name="mark_12_25"></a>Mark 12:25

**For when they shall [anistēmi](../../strongs/g/g450.md) from the [nekros](../../strongs/g/g3498.md), they neither [gameō](../../strongs/g/g1060.md), nor are [gamizō](../../strongs/g/g1061.md); but are as the [aggelos](../../strongs/g/g32.md) which are in [ouranos](../../strongs/g/g3772.md).**

<a name="mark_12_26"></a>Mark 12:26

**And [peri](../../strongs/g/g4012.md) the [nekros](../../strongs/g/g3498.md), that they [egeirō](../../strongs/g/g1453.md): have ye not [anaginōskō](../../strongs/g/g314.md) in the [biblos](../../strongs/g/g976.md) of [Mōÿsēs](../../strongs/g/g3475.md), how in the [batos](../../strongs/g/g942.md) [theos](../../strongs/g/g2316.md) [eipon](../../strongs/g/g2036.md) unto him, [legō](../../strongs/g/g3004.md), I am the [theos](../../strongs/g/g2316.md) of [Abraam](../../strongs/g/g11.md), and the [theos](../../strongs/g/g2316.md) of [Isaak](../../strongs/g/g2464.md), and the [theos](../../strongs/g/g2316.md) of [Iakōb](../../strongs/g/g2384.md)?**

<a name="mark_12_27"></a>Mark 12:27

**He is not the [theos](../../strongs/g/g2316.md) of the [nekros](../../strongs/g/g3498.md), but the [theos](../../strongs/g/g2316.md) of the [zaō](../../strongs/g/g2198.md): ye therefore do [polys](../../strongs/g/g4183.md) [planaō](../../strongs/g/g4105.md).**

<a name="mark_12_28"></a>Mark 12:28

And one of the [grammateus](../../strongs/g/g1122.md) [proserchomai](../../strongs/g/g4334.md), and having [akouō](../../strongs/g/g191.md) them [syzēteō](../../strongs/g/g4802.md), and [eidō](../../strongs/g/g1492.md) that he had [apokrinomai](../../strongs/g/g611.md) them [kalōs](../../strongs/g/g2573.md), [eperōtaō](../../strongs/g/g1905.md) him, Which is the [prōtos](../../strongs/g/g4413.md) [entolē](../../strongs/g/g1785.md) of all?

<a name="mark_12_29"></a>Mark 12:29

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) him, **The [prōtos](../../strongs/g/g4413.md) of all the [entolē](../../strongs/g/g1785.md), [akouō](../../strongs/g/g191.md), [Israēl](../../strongs/g/g2474.md); The [kyrios](../../strongs/g/g2962.md) our [theos](../../strongs/g/g2316.md) is one [kyrios](../../strongs/g/g2962.md):**

<a name="mark_12_30"></a>Mark 12:30

**And thou shalt [agapaō](../../strongs/g/g25.md) the [kyrios](../../strongs/g/g2962.md) thy [theos](../../strongs/g/g2316.md) with all thy [kardia](../../strongs/g/g2588.md), and with all thy [psychē](../../strongs/g/g5590.md), and with all thy [dianoia](../../strongs/g/g1271.md), and with all thy [ischys](../../strongs/g/g2479.md): this the [prōtos](../../strongs/g/g4413.md) [entolē](../../strongs/g/g1785.md).**

<a name="mark_12_31"></a>Mark 12:31

**And the [deuteros](../../strongs/g/g1208.md) [homoios](../../strongs/g/g3664.md), this, Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as thyself. There is none other [entolē](../../strongs/g/g1785.md) [meizōn](../../strongs/g/g3187.md) than these.**

<a name="mark_12_32"></a>Mark 12:32

And the [grammateus](../../strongs/g/g1122.md) [eipon](../../strongs/g/g2036.md) unto him, [kalōs](../../strongs/g/g2573.md), [didaskalos](../../strongs/g/g1320.md), thou hast [eipon](../../strongs/g/g2036.md) the [alētheia](../../strongs/g/g225.md): for there is one [theos](../../strongs/g/g2316.md); and there is none other but he:

<a name="mark_12_33"></a>Mark 12:33

And to [agapaō](../../strongs/g/g25.md) him with all the [kardia](../../strongs/g/g2588.md), and with all the [synesis](../../strongs/g/g4907.md), and with all the [psychē](../../strongs/g/g5590.md), and with all the [ischys](../../strongs/g/g2479.md), and to [agapaō](../../strongs/g/g25.md) his [plēsion](../../strongs/g/g4139.md) as himself, is more than all [holokautōma](../../strongs/g/g3646.md) and [thysia](../../strongs/g/g2378.md).

<a name="mark_12_34"></a>Mark 12:34

And when [Iēsous](../../strongs/g/g2424.md) [eidō](../../strongs/g/g1492.md) that he [apokrinomai](../../strongs/g/g611.md) [nounechōs](../../strongs/g/g3562.md), he [eipon](../../strongs/g/g2036.md) unto him, **Thou art not [makran](../../strongs/g/g3112.md) from the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).** And [oudeis](../../strongs/g/g3762.md) after that [tolmaō](../../strongs/g/g5111.md) [eperōtaō](../../strongs/g/g1905.md) him.

<a name="mark_12_35"></a>Mark 12:35

And [Iēsous](../../strongs/g/g2424.md) [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md), while he [didaskō](../../strongs/g/g1321.md) in the [hieron](../../strongs/g/g2411.md), **How [legō](../../strongs/g/g3004.md) the [grammateus](../../strongs/g/g1122.md) that [Christos](../../strongs/g/g5547.md) is the [huios](../../strongs/g/g5207.md) of [Dabid](../../strongs/g/g1138.md)?**

<a name="mark_12_36"></a>Mark 12:36

**For [Dabid](../../strongs/g/g1138.md) himself [eipon](../../strongs/g/g2036.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), The [kyrios](../../strongs/g/g2962.md) [eipon](../../strongs/g/g2036.md) to my [kyrios](../../strongs/g/g2962.md), [kathēmai](../../strongs/g/g2521.md) thou on my [dexios](../../strongs/g/g1188.md), till I [tithēmi](../../strongs/g/g5087.md) thine [echthros](../../strongs/g/g2190.md) thy [hypopodion](../../strongs/g/g5286.md) [pous](../../strongs/g/g4228.md).**

<a name="mark_12_37"></a>Mark 12:37

**[Dabid](../../strongs/g/g1138.md) therefore himself [legō](../../strongs/g/g3004.md) him [kyrios](../../strongs/g/g2962.md); and whence is he his [huios](../../strongs/g/g5207.md)? And the [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) [akouō](../../strongs/g/g191.md) him [hēdeōs](../../strongs/g/g2234.md).**

<a name="mark_12_38"></a>Mark 12:38

And he [legō](../../strongs/g/g3004.md) unto them in his [didachē](../../strongs/g/g1322.md), **[blepō](../../strongs/g/g991.md) of the [grammateus](../../strongs/g/g1122.md), which [thelō](../../strongs/g/g2309.md) to [peripateō](../../strongs/g/g4043.md) in [stolē](../../strongs/g/g4749.md), and [aspasmos](../../strongs/g/g783.md) in the [agora](../../strongs/g/g58.md),**

<a name="mark_12_39"></a>Mark 12:39

**And the [prōtokathedria](../../strongs/g/g4410.md) in the [synagōgē](../../strongs/g/g4864.md), and the [prōtoklisia](../../strongs/g/g4411.md) at [deipnon](../../strongs/g/g1173.md):**

<a name="mark_12_40"></a>Mark 12:40

**Which [katesthiō](../../strongs/g/g2719.md) [chēra](../../strongs/g/g5503.md) [oikia](../../strongs/g/g3614.md), and for a [prophasis](../../strongs/g/g4392.md) make [makros](../../strongs/g/g3117.md) [proseuchomai](../../strongs/g/g4336.md): these shall [lambanō](../../strongs/g/g2983.md) [perissoteros](../../strongs/g/g4055.md) [krima](../../strongs/g/g2917.md).**

<a name="mark_12_41"></a>Mark 12:41

And [Iēsous](../../strongs/g/g2424.md) [kathizō](../../strongs/g/g2523.md) [katenanti](../../strongs/g/g2713.md) the [gazophylakion](../../strongs/g/g1049.md), and [theōreō](../../strongs/g/g2334.md) how the [ochlos](../../strongs/g/g3793.md) [ballō](../../strongs/g/g906.md) [chalkos](../../strongs/g/g5475.md) into the [gazophylakion](../../strongs/g/g1049.md): and [polys](../../strongs/g/g4183.md) that were [plousios](../../strongs/g/g4145.md) [ballō](../../strongs/g/g906.md) in [polys](../../strongs/g/g4183.md).

<a name="mark_12_42"></a>Mark 12:42

And there [erchomai](../../strongs/g/g2064.md) a certain [ptōchos](../../strongs/g/g4434.md) [chēra](../../strongs/g/g5503.md), and she [ballō](../../strongs/g/g906.md) in two [lepton](../../strongs/g/g3016.md), which make a [kodrantēs](../../strongs/g/g2835.md).

<a name="mark_12_43"></a>Mark 12:43

And he [proskaleō](../../strongs/g/g4341.md) his [mathētēs](../../strongs/g/g3101.md), and [legō](../../strongs/g/g3004.md) unto them, **[amēn](../../strongs/g/g281.md) I [legō](../../strongs/g/g3004.md) unto you, That this [ptōchos](../../strongs/g/g4434.md) [chēra](../../strongs/g/g5503.md) hath [ballō](../../strongs/g/g906.md) [pleiōn](../../strongs/g/g4119.md) in, than all they which have [ballō](../../strongs/g/g906.md) into the [gazophylakion](../../strongs/g/g1049.md):**

<a name="mark_12_44"></a>Mark 12:44

**For all did [ballō](../../strongs/g/g906.md) in of their [perisseuō](../../strongs/g/g4052.md); but she of her [hysterēsis](../../strongs/g/g5304.md) did [ballō](../../strongs/g/g906.md) in all that she had, all her [bios](../../strongs/g/g979.md).**

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 11](mark_11.md) - [Mark 13](mark_13.md)