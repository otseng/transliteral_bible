# [Mark 4](https://www.blueletterbible.org/kjv/mar/4/1/rl1/s_961001)

<a name="mark_4_1"></a>Mark 4:1

And he [archomai](../../strongs/g/g756.md) again to [didaskō](../../strongs/g/g1321.md) by the [thalassa](../../strongs/g/g2281.md): and there was [synagō](../../strongs/g/g4863.md) unto him a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md), so that he [embainō](../../strongs/g/g1684.md) into a [ploion](../../strongs/g/g4143.md), and [kathēmai](../../strongs/g/g2521.md) in the [thalassa](../../strongs/g/g2281.md); and the [pas](../../strongs/g/g3956.md) [ochlos](../../strongs/g/g3793.md) was by the [thalassa](../../strongs/g/g2281.md) on the [gē](../../strongs/g/g1093.md).

<a name="mark_4_2"></a>Mark 4:2

And he [didaskō](../../strongs/g/g1321.md) them [polys](../../strongs/g/g4183.md) by [parabolē](../../strongs/g/g3850.md), and [legō](../../strongs/g/g3004.md) unto them in his [didachē](../../strongs/g/g1322.md),

<a name="mark_4_3"></a>Mark 4:3

**[akouō](../../strongs/g/g191.md); [idou](../../strongs/g/g2400.md), there [exerchomai](../../strongs/g/g1831.md) a [speirō](../../strongs/g/g4687.md) to [speirō](../../strongs/g/g4687.md):**

<a name="mark_4_4"></a>Mark 4:4

**And [ginomai](../../strongs/g/g1096.md), as he [speirō](../../strongs/g/g4687.md), some [piptō](../../strongs/g/g4098.md) by the [hodos](../../strongs/g/g3598.md), and the [peteinon](../../strongs/g/g4071.md) of [ouranos](../../strongs/g/g3772.md) [erchomai](../../strongs/g/g2064.md) and [katesthiō](../../strongs/g/g2719.md).**

<a name="mark_4_5"></a>Mark 4:5

**And some [piptō](../../strongs/g/g4098.md) on [petrōdēs](../../strongs/g/g4075.md), where it had not [polys](../../strongs/g/g4183.md) [gē](../../strongs/g/g1093.md); and [eutheōs](../../strongs/g/g2112.md) it [exanatellō](../../strongs/g/g1816.md), because it had no [bathos](../../strongs/g/g899.md) of [gē](../../strongs/g/g1093.md):**

<a name="mark_4_6"></a>Mark 4:6

**But when the [hēlios](../../strongs/g/g2246.md) [anatellō](../../strongs/g/g393.md), it was [kaumatizō](../../strongs/g/g2739.md); and because it had no [rhiza](../../strongs/g/g4491.md), it [xērainō](../../strongs/g/g3583.md).**

<a name="mark_4_7"></a>Mark 4:7

**And some [piptō](../../strongs/g/g4098.md) among [akantha](../../strongs/g/g173.md), and the [akantha](../../strongs/g/g173.md) [anabainō](../../strongs/g/g305.md), and [sympnigō](../../strongs/g/g4846.md) it, and it [didōmi](../../strongs/g/g1325.md) no [karpos](../../strongs/g/g2590.md).**

<a name="mark_4_8"></a>Mark 4:8

**And other [piptō](../../strongs/g/g4098.md) on [kalos](../../strongs/g/g2570.md) [gē](../../strongs/g/g1093.md), and did [didōmi](../../strongs/g/g1325.md) [karpos](../../strongs/g/g2590.md) that [anabainō](../../strongs/g/g305.md) and [auxanō](../../strongs/g/g837.md); and [pherō](../../strongs/g/g5342.md), some thirty, and some sixty, and some an hundred.**

<a name="mark_4_9"></a>Mark 4:9

And he [legō](../../strongs/g/g3004.md) unto them, **He that hath [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).**

<a name="mark_4_10"></a>Mark 4:10

And when he was [katamonas](../../strongs/g/g2651.md), they that were about him with the [dōdeka](../../strongs/g/g1427.md) [erōtaō](../../strongs/g/g2065.md) of him the [parabolē](../../strongs/g/g3850.md).

<a name="mark_4_11"></a>Mark 4:11

And he [legō](../../strongs/g/g3004.md) unto them, **Unto you it is [didōmi](../../strongs/g/g1325.md) to [ginōskō](../../strongs/g/g1097.md) the [mystērion](../../strongs/g/g3466.md) of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md): but unto them that are [exō](../../strongs/g/g1854.md), all these things are [ginomai](../../strongs/g/g1096.md) in [parabolē](../../strongs/g/g3850.md):**

<a name="mark_4_12"></a>Mark 4:12

**That [blepō](../../strongs/g/g991.md) they may [blepō](../../strongs/g/g991.md), and not [eidō](../../strongs/g/g1492.md); and [akouō](../../strongs/g/g191.md) they may [akouō](../../strongs/g/g191.md), and not [syniēmi](../../strongs/g/g4920.md); lest at any time they should [epistrephō](../../strongs/g/g1994.md), and [hamartēma](../../strongs/g/g265.md) should be [aphiēmi](../../strongs/g/g863.md) them.**

<a name="mark_4_13"></a>Mark 4:13

And he [legō](../../strongs/g/g3004.md) unto them, **[eidō](../../strongs/g/g1492.md) ye not this [parabolē](../../strongs/g/g3850.md)? and how then will ye [ginōskō](../../strongs/g/g1097.md) all [parabolē](../../strongs/g/g3850.md)?**

<a name="mark_4_14"></a>Mark 4:14

**The [speirō](../../strongs/g/g4687.md) [speirō](../../strongs/g/g4687.md) the [logos](../../strongs/g/g3056.md).**

<a name="mark_4_15"></a>Mark 4:15

**And these are they by the [hodos](../../strongs/g/g3598.md), where the [logos](../../strongs/g/g3056.md) is [speirō](../../strongs/g/g4687.md); but when they have [akouō](../../strongs/g/g191.md), [Satanas](../../strongs/g/g4567.md) [erchomai](../../strongs/g/g2064.md) [eutheōs](../../strongs/g/g2112.md), and [airō](../../strongs/g/g142.md) the [logos](../../strongs/g/g3056.md) that was [speirō](../../strongs/g/g4687.md) in their [kardia](../../strongs/g/g2588.md).**

<a name="mark_4_16"></a>Mark 4:16

**And these are they [homoiōs](../../strongs/g/g3668.md) which are [speirō](../../strongs/g/g4687.md) on [petrōdēs](../../strongs/g/g4075.md); who, when they have [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md), [eutheōs](../../strongs/g/g2112.md) [lambanō](../../strongs/g/g2983.md) it with [chara](../../strongs/g/g5479.md);**

<a name="mark_4_17"></a>Mark 4:17

**And have no [rhiza](../../strongs/g/g4491.md) in themselves, and so [eisi](../../strongs/g/g1526.md) but for a [proskairos](../../strongs/g/g4340.md): afterward, when [thlipsis](../../strongs/g/g2347.md) or [diōgmos](../../strongs/g/g1375.md) [ginomai](../../strongs/g/g1096.md) for the [logos](../../strongs/g/g3056.md), [eutheōs](../../strongs/g/g2112.md) they are [skandalizō](../../strongs/g/g4624.md).**

<a name="mark_4_18"></a>Mark 4:18

**And these are they which are [speirō](../../strongs/g/g4687.md) among [akantha](../../strongs/g/g173.md); such as [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md),**

<a name="mark_4_19"></a>Mark 4:19

**And the [merimna](../../strongs/g/g3308.md) of this [aiōn](../../strongs/g/g165.md), and the [apatē](../../strongs/g/g539.md) of [ploutos](../../strongs/g/g4149.md), and the [epithymia](../../strongs/g/g1939.md) of [loipos](../../strongs/g/g3062.md) [eisporeuomai](../../strongs/g/g1531.md), [sympnigō](../../strongs/g/g4846.md) the [logos](../../strongs/g/g3056.md), and it becometh [akarpos](../../strongs/g/g175.md).**

<a name="mark_4_20"></a>Mark 4:20

**And these are they which are [speirō](../../strongs/g/g4687.md) on [kalos](../../strongs/g/g2570.md) [gē](../../strongs/g/g1093.md); such as [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md), and [paradechomai](../../strongs/g/g3858.md) it, and [karpophoreō](../../strongs/g/g2592.md), some thirtyfold, some sixty, and some an hundred.**

<a name="mark_4_21"></a>Mark 4:21

And he [legō](../../strongs/g/g3004.md) unto them, **Is a [lychnos](../../strongs/g/g3088.md) [erchomai](../../strongs/g/g2064.md) to be [tithēmi](../../strongs/g/g5087.md) under a [modios](../../strongs/g/g3426.md), or under a [klinē](../../strongs/g/g2825.md)? and not to be [epitithēmi](../../strongs/g/g2007.md) on a [lychnia](../../strongs/g/g3087.md)?**

<a name="mark_4_22"></a>Mark 4:22

**For there is nothing [kryptos](../../strongs/g/g2927.md), which shall not be [phaneroō](../../strongs/g/g5319.md); neither was any thing kept [apokryphos](../../strongs/g/g614.md), but that it should [erchomai](../../strongs/g/g2064.md) [phaneros](../../strongs/g/g5318.md).**

<a name="mark_4_23"></a>Mark 4:23

**[ei tis](../../strongs/g/g1536.md) have [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).**

<a name="mark_4_24"></a>Mark 4:24

And he [legō](../../strongs/g/g3004.md) unto them, **[blepō](../../strongs/g/g991.md) what ye [akouō](../../strongs/g/g191.md): with what [metron](../../strongs/g/g3358.md) ye [metreō](../../strongs/g/g3354.md), it shall be [metreō](../../strongs/g/g3354.md) to you: and unto you that [akouō](../../strongs/g/g191.md) shall more be [prostithēmi](../../strongs/g/g4369.md).**

<a name="mark_4_25"></a>Mark 4:25

**For he that hath, to him shall be [didōmi](../../strongs/g/g1325.md): and he that hath not, from him shall be [airō](../../strongs/g/g142.md) even that which he hath.**

<a name="mark_4_26"></a>Mark 4:26

And he [legō](../../strongs/g/g3004.md), **So is the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), as if an [anthrōpos](../../strongs/g/g444.md) should [ballō](../../strongs/g/g906.md) [sporos](../../strongs/g/g4703.md) into the [gē](../../strongs/g/g1093.md);**

<a name="mark_4_27"></a>Mark 4:27

**And should [katheudō](../../strongs/g/g2518.md), and [egeirō](../../strongs/g/g1453.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md), and the [sporos](../../strongs/g/g4703.md) should [blastanō](../../strongs/g/g985.md) and [mēkynō](../../strongs/g/g3373.md), he [eidō](../../strongs/g/g1492.md) not how.**

<a name="mark_4_28"></a>Mark 4:28

**For the [gē](../../strongs/g/g1093.md) [karpophoreō](../../strongs/g/g2592.md) [automatos](../../strongs/g/g844.md); first the [chortos](../../strongs/g/g5528.md), then the [stachys](../../strongs/g/g4719.md), after that the [plērēs](../../strongs/g/g4134.md) [sitos](../../strongs/g/g4621.md) in the [stachys](../../strongs/g/g4719.md).**

<a name="mark_4_29"></a>Mark 4:29

**But when the [karpos](../../strongs/g/g2590.md) is [paradidōmi](../../strongs/g/g3860.md), [eutheōs](../../strongs/g/g2112.md) he [apostellō](../../strongs/g/g649.md) in the [drepanon](../../strongs/g/g1407.md), because the [therismos](../../strongs/g/g2326.md) is [paristēmi](../../strongs/g/g3936.md).**

<a name="mark_4_30"></a>Mark 4:30

And he [legō](../../strongs/g/g3004.md), **Whereunto shall we [homoioō](../../strongs/g/g3666.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)? or with what [parabolē](../../strongs/g/g3850.md) shall we [paraballō](../../strongs/g/g3846.md) it?**

<a name="mark_4_31"></a>Mark 4:31

**like a [kokkos](../../strongs/g/g2848.md) of [sinapi](../../strongs/g/g4615.md), which, when it is [speirō](../../strongs/g/g4687.md) in the [gē](../../strongs/g/g1093.md), is [mikros](../../strongs/g/g3398.md) than all the [sperma](../../strongs/g/g4690.md) that be in the [gē](../../strongs/g/g1093.md):**

<a name="mark_4_32"></a>Mark 4:32

**But when it is [speirō](../../strongs/g/g4687.md), it [anabainō](../../strongs/g/g305.md), and becometh [meizōn](../../strongs/g/g3187.md) than all [lachanon](../../strongs/g/g3001.md), and [poieō](../../strongs/g/g4160.md) [megas](../../strongs/g/g3173.md) [klados](../../strongs/g/g2798.md); so that the [peteinon](../../strongs/g/g4071.md) of the [ouranos](../../strongs/g/g3772.md) may [kataskēnoō](../../strongs/g/g2681.md) under the [skia](../../strongs/g/g4639.md) of it.**

<a name="mark_4_33"></a>Mark 4:33

And with [polys](../../strongs/g/g4183.md) such [parabolē](../../strongs/g/g3850.md) [laleō](../../strongs/g/g2980.md) he the [logos](../../strongs/g/g3056.md) unto them, as they were able to [akouō](../../strongs/g/g191.md).

<a name="mark_4_34"></a>Mark 4:34

But without a [parabolē](../../strongs/g/g3850.md) [laleō](../../strongs/g/g2980.md) he not unto them: and when they were [idios](../../strongs/g/g2398.md), he [epilyō](../../strongs/g/g1956.md) all things to his [mathētēs](../../strongs/g/g3101.md).

<a name="mark_4_35"></a>Mark 4:35

And the same [hēmera](../../strongs/g/g2250.md), when the [opsios](../../strongs/g/g3798.md) was [ginomai](../../strongs/g/g1096.md), he [legō](../../strongs/g/g3004.md) unto them, **Let us [dierchomai](../../strongs/g/g1330.md) unto the [peran](../../strongs/g/g4008.md).**

<a name="mark_4_36"></a>Mark 4:36

And when they had [aphiēmi](../../strongs/g/g863.md) the [ochlos](../../strongs/g/g3793.md), they [paralambanō](../../strongs/g/g3880.md) him even as he was in the [ploion](../../strongs/g/g4143.md). And there were also with him other [ploiarion](../../strongs/g/g4142.md).

<a name="mark_4_37"></a>Mark 4:37

And there [ginomai](../../strongs/g/g1096.md) a [megas](../../strongs/g/g3173.md) [lailaps](../../strongs/g/g2978.md) of [anemos](../../strongs/g/g417.md), and the [kyma](../../strongs/g/g2949.md) [epiballō](../../strongs/g/g1911.md) into the [ploion](../../strongs/g/g4143.md), so that it now [gemizō](../../strongs/g/g1072.md).

<a name="mark_4_38"></a>Mark 4:38

And he was in the [prymna](../../strongs/g/g4403.md), [katheudō](../../strongs/g/g2518.md) on a [proskephalaion](../../strongs/g/g4344.md): and they [diegeirō](../../strongs/g/g1326.md) him, and [legō](../../strongs/g/g3004.md) unto him, [didaskalos](../../strongs/g/g1320.md), [melei](../../strongs/g/g3199.md) thou not that we [apollymi](../../strongs/g/g622.md)?

<a name="mark_4_39"></a>Mark 4:39

And he [diegeirō](../../strongs/g/g1326.md), and [epitimaō](../../strongs/g/g2008.md) the [anemos](../../strongs/g/g417.md), and [eipon](../../strongs/g/g2036.md) unto the [thalassa](../../strongs/g/g2281.md), **[siōpaō](../../strongs/g/g4623.md), [phimoō](../../strongs/g/g5392.md).** And the [anemos](../../strongs/g/g417.md) [kopazō](../../strongs/g/g2869.md), and there was a [megas](../../strongs/g/g3173.md) [galēnē](../../strongs/g/g1055.md).

<a name="mark_4_40"></a>Mark 4:40

And he [eipon](../../strongs/g/g2036.md) unto them, **Why are ye so [deilos](../../strongs/g/g1169.md)? how is it that ye have no [pistis](../../strongs/g/g4102.md)?**

<a name="mark_4_41"></a>Mark 4:41

And they [phobeō](../../strongs/g/g5399.md) [phobos](../../strongs/g/g5401.md) [megas](../../strongs/g/g3173.md), and [legō](../../strongs/g/g3004.md) [pros](../../strongs/g/g4314.md) [allēlōn](../../strongs/g/g240.md), What [ara](../../strongs/g/g686.md) is this, that even the [anemos](../../strongs/g/g417.md) and the [thalassa](../../strongs/g/g2281.md) [hypakouō](../../strongs/g/g5219.md) him?

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 3](mark_3.md) - [Mark 5](mark_5.md)