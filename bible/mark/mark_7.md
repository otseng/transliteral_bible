# [Mark 7](https://www.blueletterbible.org/kjv/mar/7/1/rl1/s_961001)

<a name="mark_7_1"></a>Mark 7:1

Then [synagō](../../strongs/g/g4863.md) unto him the [Pharisaios](../../strongs/g/g5330.md), and certain of the [grammateus](../../strongs/g/g1122.md), which [erchomai](../../strongs/g/g2064.md) from [Hierosolyma](../../strongs/g/g2414.md).

<a name="mark_7_2"></a>Mark 7:2

And when they [eidō](../../strongs/g/g1492.md) some of his [mathētēs](../../strongs/g/g3101.md) [esthiō](../../strongs/g/g2068.md) [artos](../../strongs/g/g740.md) with [koinos](../../strongs/g/g2839.md), [tout᾿ estin](../../strongs/g/g5123.md), with [aniptos](../../strongs/g/g449.md), [cheir](../../strongs/g/g5495.md), they [memphomai](../../strongs/g/g3201.md).

<a name="mark_7_3"></a>Mark 7:3

For the [Pharisaios](../../strongs/g/g5330.md), and all the [Ioudaios](../../strongs/g/g2453.md), except they [niptō](../../strongs/g/g3538.md) [cheir](../../strongs/g/g5495.md) [pygmē](../../strongs/g/g4435.md), [esthiō](../../strongs/g/g2068.md) not, [krateō](../../strongs/g/g2902.md) the [paradosis](../../strongs/g/g3862.md) of the [presbyteros](../../strongs/g/g4245.md).

<a name="mark_7_4"></a>Mark 7:4

And from the [agora](../../strongs/g/g58.md), except they [baptizō](../../strongs/g/g907.md), they [esthiō](../../strongs/g/g2068.md) not. And [polys](../../strongs/g/g4183.md) [allos](../../strongs/g/g243.md) there be, which they have [paralambanō](../../strongs/g/g3880.md) to [krateō](../../strongs/g/g2902.md), the [baptismos](../../strongs/g/g909.md) of [potērion](../../strongs/g/g4221.md), and [xestēs](../../strongs/g/g3582.md), [chalkion](../../strongs/g/g5473.md), and of [klinē](../../strongs/g/g2825.md).

<a name="mark_7_5"></a>Mark 7:5

Then the [Pharisaios](../../strongs/g/g5330.md) and [grammateus](../../strongs/g/g1122.md) [eperōtaō](../../strongs/g/g1905.md) him, Why [peripateō](../../strongs/g/g4043.md) not thy [mathētēs](../../strongs/g/g3101.md) according to the [paradosis](../../strongs/g/g3862.md) of the [presbyteros](../../strongs/g/g4245.md), but [esthiō](../../strongs/g/g2068.md) [artos](../../strongs/g/g740.md) with [aniptos](../../strongs/g/g449.md) [cheir](../../strongs/g/g5495.md)?

<a name="mark_7_6"></a>Mark 7:6

He [apokrinomai](../../strongs/g/g611.md) and [eipon](../../strongs/g/g2036.md) unto them, **[kalōs](../../strongs/g/g2573.md) hath [Ēsaïas](../../strongs/g/g2268.md) [prophēteuō](../../strongs/g/g4395.md) of you [hypokritēs](../../strongs/g/g5273.md), as it is [graphō](../../strongs/g/g1125.md), This [laos](../../strongs/g/g2992.md) [timaō](../../strongs/g/g5091.md) me with their [cheilos](../../strongs/g/g5491.md), but their [kardia](../../strongs/g/g2588.md) is [porrō](../../strongs/g/g4206.md) from me.**

<a name="mark_7_7"></a>Mark 7:7

**Howbeit [matēn](../../strongs/g/g3155.md) do they [sebō](../../strongs/g/g4576.md) me, [didaskō](../../strongs/g/g1321.md) for [didaskalia](../../strongs/g/g1319.md) the [entalma](../../strongs/g/g1778.md) of [anthrōpos](../../strongs/g/g444.md).**

<a name="mark_7_8"></a>Mark 7:8

**For [aphiēmi](../../strongs/g/g863.md) the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md), ye [krateō](../../strongs/g/g2902.md) the [paradosis](../../strongs/g/g3862.md) of [anthrōpos](../../strongs/g/g444.md), as the [baptismos](../../strongs/g/g909.md) of [xestēs](../../strongs/g/g3582.md) and [potērion](../../strongs/g/g4221.md): and [polys](../../strongs/g/g4183.md) other such [paromoios](../../strongs/g/g3946.md) ye [poieō](../../strongs/g/g4160.md).** [^1]

<a name="mark_7_9"></a>Mark 7:9

And he [legō](../../strongs/g/g3004.md) unto them, **[kalōs](../../strongs/g/g2573.md) ye [atheteō](../../strongs/g/g114.md) the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md), that ye may [tēreō](../../strongs/g/g5083.md) your own [paradosis](../../strongs/g/g3862.md).**

<a name="mark_7_10"></a>Mark 7:10

**For [Mōÿsēs](../../strongs/g/g3475.md) [eipon](../../strongs/g/g2036.md), [timaō](../../strongs/g/g5091.md) thy [patēr](../../strongs/g/g3962.md) and thy [mētēr](../../strongs/g/g3384.md); and, Whoso [kakologeō](../../strongs/g/g2551.md) [patēr](../../strongs/g/g3962.md) or [mētēr](../../strongs/g/g3384.md), let him [teleutaō](../../strongs/g/g5053.md) the [thanatos](../../strongs/g/g2288.md):**

<a name="mark_7_11"></a>Mark 7:11

**But ye [legō](../../strongs/g/g3004.md), If an [anthrōpos](../../strongs/g/g444.md) shall [eipon](../../strongs/g/g2036.md) to his [patēr](../../strongs/g/g3962.md) or [mētēr](../../strongs/g/g3384.md), It is [korban](../../strongs/g/g2878.md), [o estin](../../strongs/g/g3603.md), a [dōron](../../strongs/g/g1435.md), by whatsoever thou mightest be [ōpheleō](../../strongs/g/g5623.md) by me;.**

<a name="mark_7_12"></a>Mark 7:12

**And ye [aphiēmi](../../strongs/g/g863.md) him no more to [poieō](../../strongs/g/g4160.md) ought for his [patēr](../../strongs/g/g3962.md) or his [mētēr](../../strongs/g/g3384.md);**

<a name="mark_7_13"></a>Mark 7:13

**[akyroō](../../strongs/g/g208.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) through your [paradosis](../../strongs/g/g3862.md), which ye have [paradidōmi](../../strongs/g/g3860.md): and [polys](../../strongs/g/g4183.md) such [paromoios](../../strongs/g/g3946.md) [poieō](../../strongs/g/g4160.md) ye.**

<a name="mark_7_14"></a>Mark 7:14

And when he had [proskaleō](../../strongs/g/g4341.md) all the [ochlos](../../strongs/g/g3793.md) unto him, he [legō](../../strongs/g/g3004.md) unto them, **[akouō](../../strongs/g/g191.md) unto me every one, and [syniēmi](../../strongs/g/g4920.md):**

<a name="mark_7_15"></a>Mark 7:15

**There is nothing from [exōthen](../../strongs/g/g1855.md) an [anthrōpos](../../strongs/g/g444.md), that [eisporeuomai](../../strongs/g/g1531.md) into him can [koinoō](../../strongs/g/g2840.md) him: but the things which [ekporeuomai](../../strongs/g/g1607.md) of him, those are they that [koinoō](../../strongs/g/g2840.md) the [anthrōpos](../../strongs/g/g444.md).**

<a name="mark_7_16"></a>Mark 7:16

**[ei tis](../../strongs/g/g1536.md) have [ous](../../strongs/g/g3775.md) to [akouō](../../strongs/g/g191.md), let him [akouō](../../strongs/g/g191.md).** [^2]

<a name="mark_7_17"></a>Mark 7:17

And when he was [eiserchomai](../../strongs/g/g1525.md) into the [oikos](../../strongs/g/g3624.md) from the [ochlos](../../strongs/g/g3793.md), his [mathētēs](../../strongs/g/g3101.md) [eperōtaō](../../strongs/g/g1905.md) him concerning the [parabolē](../../strongs/g/g3850.md).

<a name="mark_7_18"></a>Mark 7:18

And he [legō](../../strongs/g/g3004.md) unto them, **Are ye so [asynetos](../../strongs/g/g801.md) also? Do ye not [noeō](../../strongs/g/g3539.md), that whatsoever thing from [exōthen](../../strongs/g/g1855.md) [eisporeuomai](../../strongs/g/g1531.md) into the [anthrōpos](../../strongs/g/g444.md), it cannot [koinoō](../../strongs/g/g2840.md) him;**

<a name="mark_7_19"></a>Mark 7:19

**Because it [eisporeuomai](../../strongs/g/g1531.md) not into his [kardia](../../strongs/g/g2588.md), but into the [koilia](../../strongs/g/g2836.md), and [ekporeuomai](../../strongs/g/g1607.md) into the [aphedrōn](../../strongs/g/g856.md), [katharizō](../../strongs/g/g2511.md) all [brōma](../../strongs/g/g1033.md)?**

<a name="mark_7_20"></a>Mark 7:20

And he [legō](../../strongs/g/g3004.md), **That which [ekporeuomai](../../strongs/g/g1607.md) out of the [anthrōpos](../../strongs/g/g444.md), that [koinoō](../../strongs/g/g2840.md) the [anthrōpos](../../strongs/g/g444.md).**

<a name="mark_7_21"></a>Mark 7:21

**For [esōthen](../../strongs/g/g2081.md), out of the [kardia](../../strongs/g/g2588.md) of [anthrōpos](../../strongs/g/g444.md), [ekporeuomai](../../strongs/g/g1607.md) [kakos](../../strongs/g/g2556.md) [dialogismos](../../strongs/g/g1261.md), [moicheia](../../strongs/g/g3430.md), [porneia](../../strongs/g/g4202.md), [phonos](../../strongs/g/g5408.md),**

<a name="mark_7_22"></a>Mark 7:22

**[klopē](../../strongs/g/g2829.md), [pleonexia](../../strongs/g/g4124.md), [ponēria](../../strongs/g/g4189.md), [dolos](../../strongs/g/g1388.md), [aselgeia](../../strongs/g/g766.md), a [ponēros](../../strongs/g/g4190.md) [ophthalmos](../../strongs/g/g3788.md), [blasphēmia](../../strongs/g/g988.md), [hyperēphania](../../strongs/g/g5243.md), [aphrosynē](../../strongs/g/g877.md):**

<a name="mark_7_23"></a>Mark 7:23

**All these [ponēros](../../strongs/g/g4190.md) [ekporeuomai](../../strongs/g/g1607.md) from [esōthen](../../strongs/g/g2081.md), and [koinoō](../../strongs/g/g2840.md) the [anthrōpos](../../strongs/g/g444.md).**

<a name="mark_7_24"></a>Mark 7:24

And from thence he [anistēmi](../../strongs/g/g450.md), and [aperchomai](../../strongs/g/g565.md) into the [methorion](../../strongs/g/g3181.md) of [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), and [eiserchomai](../../strongs/g/g1525.md) into an [oikia](../../strongs/g/g3614.md), and would have [oudeis](../../strongs/g/g3762.md) [ginōskō](../../strongs/g/g1097.md) it: but he could not be [lanthanō](../../strongs/g/g2990.md).

<a name="mark_7_25"></a>Mark 7:25

For a [gynē](../../strongs/g/g1135.md), whose [thygatrion](../../strongs/g/g2365.md) had an [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), [akouō](../../strongs/g/g191.md) of him, and [erchomai](../../strongs/g/g2064.md) and [prospiptō](../../strongs/g/g4363.md) at his [pous](../../strongs/g/g4228.md):

<a name="mark_7_26"></a>Mark 7:26

The [gynē](../../strongs/g/g1135.md) was a [Hellēnis](../../strongs/g/g1674.md), a [syrophoinikissa](../../strongs/g/g4949.md) by [genos](../../strongs/g/g1085.md); and she [erōtaō](../../strongs/g/g2065.md) him that he would [ekballō](../../strongs/g/g1544.md) the [daimonion](../../strongs/g/g1140.md) out of her [thygatēr](../../strongs/g/g2364.md).

<a name="mark_7_27"></a>Mark 7:27

But [Iēsous](../../strongs/g/g2424.md) [eipon](../../strongs/g/g2036.md) unto her, **[aphiēmi](../../strongs/g/g863.md) the [teknon](../../strongs/g/g5043.md) first be [chortazō](../../strongs/g/g5526.md): for it is not [kalos](../../strongs/g/g2570.md) to [lambanō](../../strongs/g/g2983.md) the [teknon](../../strongs/g/g5043.md) [artos](../../strongs/g/g740.md), and to [ballō](../../strongs/g/g906.md) it unto the [kynarion](../../strongs/g/g2952.md).**

<a name="mark_7_28"></a>Mark 7:28

And she [apokrinomai](../../strongs/g/g611.md) and [legō](../../strongs/g/g3004.md) unto him, [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md): yet the [kynarion](../../strongs/g/g2952.md) [hypokatō](../../strongs/g/g5270.md) the [trapeza](../../strongs/g/g5132.md) [esthiō](../../strongs/g/g2068.md) of the [paidion](../../strongs/g/g3813.md) [psichion](../../strongs/g/g5589.md).

<a name="mark_7_29"></a>Mark 7:29

And he [eipon](../../strongs/g/g2036.md) unto her, **For this [logos](../../strongs/g/g3056.md) [hypagō](../../strongs/g/g5217.md); the [daimonion](../../strongs/g/g1140.md) is [exerchomai](../../strongs/g/g1831.md) out of thy [thygatēr](../../strongs/g/g2364.md).**

<a name="mark_7_30"></a>Mark 7:30

And when she was [aperchomai](../../strongs/g/g565.md) to her [oikos](../../strongs/g/g3624.md), she [heuriskō](../../strongs/g/g2147.md) the [daimonion](../../strongs/g/g1140.md) [exerchomai](../../strongs/g/g1831.md), and her [thygatēr](../../strongs/g/g2364.md) [ballō](../../strongs/g/g906.md) upon the [klinē](../../strongs/g/g2825.md).

<a name="mark_7_31"></a>Mark 7:31

And [palin](../../strongs/g/g3825.md), [exerchomai](../../strongs/g/g1831.md) from the [horion](../../strongs/g/g3725.md) of [Tyros](../../strongs/g/g5184.md) and [Sidōn](../../strongs/g/g4605.md), he [erchomai](../../strongs/g/g2064.md) unto the [thalassa](../../strongs/g/g2281.md) of [Galilaia](../../strongs/g/g1056.md), through the [mesos](../../strongs/g/g3319.md) of the [horion](../../strongs/g/g3725.md) of [Dekapolis](../../strongs/g/g1179.md).

<a name="mark_7_32"></a>Mark 7:32

And they [pherō](../../strongs/g/g5342.md) unto him one [kōphos](../../strongs/g/g2974.md), and had a [mogilalos](../../strongs/g/g3424.md); and they [parakaleō](../../strongs/g/g3870.md) him to [epitithēmi](../../strongs/g/g2007.md) his [cheir](../../strongs/g/g5495.md) upon him.

<a name="mark_7_33"></a>Mark 7:33

And he [apolambanō](../../strongs/g/g618.md) him [idios](../../strongs/g/g2398.md) from the [ochlos](../../strongs/g/g3793.md), and [ballō](../../strongs/g/g906.md) his [daktylos](../../strongs/g/g1147.md) into his [ous](../../strongs/g/g3775.md), and he [ptyō](../../strongs/g/g4429.md), and [haptomai](../../strongs/g/g680.md) his [glōssa](../../strongs/g/g1100.md);

<a name="mark_7_34"></a>Mark 7:34

And [anablepō](../../strongs/g/g308.md) to [ouranos](../../strongs/g/g3772.md), he [stenazō](../../strongs/g/g4727.md), and [legō](../../strongs/g/g3004.md) unto him, **[ephphatha](../../strongs/g/g2188.md)**, that is, **[dianoigō](../../strongs/g/g1272.md)**.

<a name="mark_7_35"></a>Mark 7:35

And [eutheōs](../../strongs/g/g2112.md) his [akoē](../../strongs/g/g189.md) [dianoigō](../../strongs/g/g1272.md), and the [desmos](../../strongs/g/g1199.md) of his [glōssa](../../strongs/g/g1100.md) was [lyō](../../strongs/g/g3089.md), and he [laleō](../../strongs/g/g2980.md) [orthōs](../../strongs/g/g3723.md).

<a name="mark_7_36"></a>Mark 7:36

And he [diastellō](../../strongs/g/g1291.md) them that they should [eipon](../../strongs/g/g2036.md) [mēdeis](../../strongs/g/g3367.md): but the more he [diastellō](../../strongs/g/g1291.md) them, so much the more a [perissoteron](../../strongs/g/g4054.md) they [kēryssō](../../strongs/g/g2784.md);

<a name="mark_7_37"></a>Mark 7:37

And were [hyperperissōs](../../strongs/g/g5249.md) [ekplēssō](../../strongs/g/g1605.md), [legō](../../strongs/g/g3004.md), He hath [poieō](../../strongs/g/g4160.md) all things [kalōs](../../strongs/g/g2573.md): he [poieō](../../strongs/g/g4160.md) both the [kōphos](../../strongs/g/g2974.md) to [akouō](../../strongs/g/g191.md), and the [alalos](../../strongs/g/g216.md) to [laleō](../../strongs/g/g2980.md).

---

[Transliteral Bible](../bible.md)

[Mark](mark.md)

[Mark 6](mark_6.md) - [Mark 8](mark_8.md)

---

[^1]: [Mark 7:8 Commentary](../../commentary/mark/mark_7_commentary.md#mark_7_8)

[^2]: [Mark 7:16 Commentary](../../commentary/mark/mark_7_commentary.md#mark_7_16)
