# [2Samuel 4](https://www.blueletterbible.org/kjv/2samuel/4)

<a name="2samuel_4_1"></a>2Samuel 4:1

And when [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md) [shama'](../../strongs/h/h8085.md) that ['Aḇnēr](../../strongs/h/h74.md) was [muwth](../../strongs/h/h4191.md) in [Ḥeḇrôn](../../strongs/h/h2275.md), his [yad](../../strongs/h/h3027.md) were [rāp̄â](../../strongs/h/h7503.md), and all the [Yisra'el](../../strongs/h/h3478.md) were [bahal](../../strongs/h/h926.md).

<a name="2samuel_4_2"></a>2Samuel 4:2

And [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md) had two ['enowsh](../../strongs/h/h582.md) that were [śar](../../strongs/h/h8269.md) of [gᵊḏûḏ](../../strongs/h/h1416.md): the [shem](../../strongs/h/h8034.md) of the one was [BaʿĂnâ](../../strongs/h/h1196.md), and the [shem](../../strongs/h/h8034.md) of the other [Rēḵāḇ](../../strongs/h/h7394.md), the [ben](../../strongs/h/h1121.md) of [Rimmôn](../../strongs/h/h7417.md) a [Bᵊ'ērōṯî](../../strongs/h/h886.md), of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md): (for [Bᵊ'Ērôṯ](../../strongs/h/h881.md) also was [chashab](../../strongs/h/h2803.md) to [Binyāmîn](../../strongs/h/h1144.md):

<a name="2samuel_4_3"></a>2Samuel 4:3

And the [Bᵊ'ērōṯî](../../strongs/h/h886.md) [bāraḥ](../../strongs/h/h1272.md) to [Gitayim](../../strongs/h/h1664.md), and were [guwr](../../strongs/h/h1481.md) there until this [yowm](../../strongs/h/h3117.md).)

<a name="2samuel_4_4"></a>2Samuel 4:4

And [Yᵊhônāṯān](../../strongs/h/h3083.md), [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md), had a [ben](../../strongs/h/h1121.md) that was [nāḵê](../../strongs/h/h5223.md) of his [regel](../../strongs/h/h7272.md). He was five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when the [šᵊmûʿâ](../../strongs/h/h8052.md) [bow'](../../strongs/h/h935.md) of [Šā'ûl](../../strongs/h/h7586.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) out of [YizrᵊʿE'L](../../strongs/h/h3157.md), and his ['aman](../../strongs/h/h539.md) took him [nasa'](../../strongs/h/h5375.md), and [nûs](../../strongs/h/h5127.md): and it came to pass, as she made [ḥāp̄az](../../strongs/h/h2648.md) to [nûs](../../strongs/h/h5127.md), that he [naphal](../../strongs/h/h5307.md), and became [pāsaḥ](../../strongs/h/h6452.md). And his [shem](../../strongs/h/h8034.md) was [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md).

<a name="2samuel_4_5"></a>2Samuel 4:5

And the [ben](../../strongs/h/h1121.md) of [Rimmôn](../../strongs/h/h7417.md) the [Bᵊ'ērōṯî](../../strongs/h/h886.md), [Rēḵāḇ](../../strongs/h/h7394.md) and [BaʿĂnâ](../../strongs/h/h1196.md), [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) about the [ḥōm](../../strongs/h/h2527.md) of the [yowm](../../strongs/h/h3117.md) to the [bayith](../../strongs/h/h1004.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md), who [shakab](../../strongs/h/h7901.md) on a [miškāḇ](../../strongs/h/h4904.md) at [ṣōhar](../../strongs/h/h6672.md).

<a name="2samuel_4_6"></a>2Samuel 4:6

And they [bow'](../../strongs/h/h935.md) into the [tavek](../../strongs/h/h8432.md) of the [bayith](../../strongs/h/h1004.md), as though they would have [laqach](../../strongs/h/h3947.md) [ḥiṭṭâ](../../strongs/h/h2406.md); and they [nakah](../../strongs/h/h5221.md) him under the [ḥōmeš](../../strongs/h/h2570.md) rib: and [Rēḵāḇ](../../strongs/h/h7394.md) and [BaʿĂnâ](../../strongs/h/h1196.md) his ['ach](../../strongs/h/h251.md) [mālaṭ](../../strongs/h/h4422.md).

<a name="2samuel_4_7"></a>2Samuel 4:7

For when they [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md), he [shakab](../../strongs/h/h7901.md) on his [mittah](../../strongs/h/h4296.md) in his [ḥeḏer](../../strongs/h/h2315.md) [miškāḇ](../../strongs/h/h4904.md), and they [nakah](../../strongs/h/h5221.md) him, and [muwth](../../strongs/h/h4191.md) him, and [cuwr](../../strongs/h/h5493.md) him, and [laqach](../../strongs/h/h3947.md) his [ro'sh](../../strongs/h/h7218.md), and gat them [yālaḵ](../../strongs/h/h3212.md) [derek](../../strongs/h/h1870.md) the ['arabah](../../strongs/h/h6160.md) all [layil](../../strongs/h/h3915.md).

<a name="2samuel_4_8"></a>2Samuel 4:8

And they [bow'](../../strongs/h/h935.md) the [ro'sh](../../strongs/h/h7218.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md) unto [Dāviḏ](../../strongs/h/h1732.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), and ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md), Behold the [ro'sh](../../strongs/h/h7218.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md) thine ['oyeb](../../strongs/h/h341.md), which [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md); and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [nᵊqāmâ](../../strongs/h/h5360.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) this [yowm](../../strongs/h/h3117.md) of [Šā'ûl](../../strongs/h/h7586.md), and of his [zera'](../../strongs/h/h2233.md).

<a name="2samuel_4_9"></a>2Samuel 4:9

And [Dāviḏ](../../strongs/h/h1732.md) ['anah](../../strongs/h/h6030.md) [Rēḵāḇ](../../strongs/h/h7394.md) and [BaʿĂnâ](../../strongs/h/h1196.md) his ['ach](../../strongs/h/h251.md), the [ben](../../strongs/h/h1121.md) of [Rimmôn](../../strongs/h/h7417.md) the [Bᵊ'ērōṯî](../../strongs/h/h886.md), and ['āmar](../../strongs/h/h559.md) unto them, As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), who hath [pāḏâ](../../strongs/h/h6299.md) my [nephesh](../../strongs/h/h5315.md) out of all [tsarah](../../strongs/h/h6869.md),

<a name="2samuel_4_10"></a>2Samuel 4:10

When one [nāḡaḏ](../../strongs/h/h5046.md) me, ['āmar](../../strongs/h/h559.md), Behold, [Šā'ûl](../../strongs/h/h7586.md) is [muwth](../../strongs/h/h4191.md), thinking to have ['ayin](../../strongs/h/h5869.md) good [bāśar](../../strongs/h/h1319.md), I took ['āḥaz](../../strongs/h/h270.md) of him, and [harag](../../strongs/h/h2026.md) him in [Ṣiqlāḡ](../../strongs/h/h6860.md), who thought that I would have [nathan](../../strongs/h/h5414.md) him a reward for his [bᵊśôrâ](../../strongs/h/h1309.md):

<a name="2samuel_4_11"></a>2Samuel 4:11

How much more, when [rasha'](../../strongs/h/h7563.md) ['enowsh](../../strongs/h/h582.md) have [harag](../../strongs/h/h2026.md) a [tsaddiyq](../../strongs/h/h6662.md) ['iysh](../../strongs/h/h376.md) in his own [bayith](../../strongs/h/h1004.md) upon his [miškāḇ](../../strongs/h/h4904.md)? shall I not therefore now [bāqaš](../../strongs/h/h1245.md) his [dam](../../strongs/h/h1818.md) of your [yad](../../strongs/h/h3027.md), and take you [bāʿar](../../strongs/h/h1197.md) from the ['erets](../../strongs/h/h776.md)?

<a name="2samuel_4_12"></a>2Samuel 4:12

And [Dāviḏ](../../strongs/h/h1732.md) [tsavah](../../strongs/h/h6680.md) his [naʿar](../../strongs/h/h5288.md), and they [harag](../../strongs/h/h2026.md) them, and [qāṣaṣ](../../strongs/h/h7112.md) their [yad](../../strongs/h/h3027.md) and their [regel](../../strongs/h/h7272.md), and hanged them [tālâ](../../strongs/h/h8518.md) over the [bᵊrēḵâ](../../strongs/h/h1295.md) in [Ḥeḇrôn](../../strongs/h/h2275.md). But they [laqach](../../strongs/h/h3947.md) the [ro'sh](../../strongs/h/h7218.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md), and [qāḇar](../../strongs/h/h6912.md) it in the [qeber](../../strongs/h/h6913.md) of ['Aḇnēr](../../strongs/h/h74.md) in [Ḥeḇrôn](../../strongs/h/h2275.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 3](2samuel_3.md) - [2Samuel 5](2samuel_5.md)