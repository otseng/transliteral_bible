# [2Samuel 19](https://www.blueletterbible.org/kjv/2samuel/19)

<a name="2samuel_19_1"></a>2Samuel 19:1

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Yô'āḇ](../../strongs/h/h3097.md), Behold, the [melek](../../strongs/h/h4428.md) [bāḵâ](../../strongs/h/h1058.md) and ['āḇal](../../strongs/h/h56.md) for ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_19_2"></a>2Samuel 19:2

And the [tᵊšûʿâ](../../strongs/h/h8668.md) that [yowm](../../strongs/h/h3117.md) was turned into ['ēḇel](../../strongs/h/h60.md) unto all the ['am](../../strongs/h/h5971.md): for the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) ['āmar](../../strongs/h/h559.md) that [yowm](../../strongs/h/h3117.md) how the [melek](../../strongs/h/h4428.md) was [ʿāṣaḇ](../../strongs/h/h6087.md) for his [ben](../../strongs/h/h1121.md).

<a name="2samuel_19_3"></a>2Samuel 19:3

And the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) them by [ganab](../../strongs/h/h1589.md) that [yowm](../../strongs/h/h3117.md) into the [ʿîr](../../strongs/h/h5892.md), as ['am](../../strongs/h/h5971.md) being [kālam](../../strongs/h/h3637.md) [ganab](../../strongs/h/h1589.md) when they [nûs](../../strongs/h/h5127.md) in [milḥāmâ](../../strongs/h/h4421.md).

<a name="2samuel_19_4"></a>2Samuel 19:4

But the [melek](../../strongs/h/h4428.md) [lā'aṭ](../../strongs/h/h3813.md) his [paniym](../../strongs/h/h6440.md), and the [melek](../../strongs/h/h4428.md) [zāʿaq](../../strongs/h/h2199.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), O my [ben](../../strongs/h/h1121.md) ['Ăbyšālôm](../../strongs/h/h53.md), O ['Ăbyšālôm](../../strongs/h/h53.md), my [ben](../../strongs/h/h1121.md), my [ben](../../strongs/h/h1121.md)!

<a name="2samuel_19_5"></a>2Samuel 19:5

And [Yô'āḇ](../../strongs/h/h3097.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) to the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), Thou hast [yāḇēš](../../strongs/h/h3001.md) this [yowm](../../strongs/h/h3117.md) the [paniym](../../strongs/h/h6440.md) of all thy ['ebed](../../strongs/h/h5650.md), which this [yowm](../../strongs/h/h3117.md) have [mālaṭ](../../strongs/h/h4422.md) thy [nephesh](../../strongs/h/h5315.md), and the [nephesh](../../strongs/h/h5315.md) of thy [ben](../../strongs/h/h1121.md) and of thy [bath](../../strongs/h/h1323.md), and the [nephesh](../../strongs/h/h5315.md) of thy ['ishshah](../../strongs/h/h802.md), and the [nephesh](../../strongs/h/h5315.md) of thy [pîleḡeš](../../strongs/h/h6370.md);

<a name="2samuel_19_6"></a>2Samuel 19:6

In that thou ['ahab](../../strongs/h/h157.md) thine [sane'](../../strongs/h/h8130.md), and [sane'](../../strongs/h/h8130.md) thy ['ahab](../../strongs/h/h157.md). For thou hast [nāḡaḏ](../../strongs/h/h5046.md) this [yowm](../../strongs/h/h3117.md), that thou regardest neither [śar](../../strongs/h/h8269.md) nor ['ebed](../../strongs/h/h5650.md): for this [yowm](../../strongs/h/h3117.md) I [yada'](../../strongs/h/h3045.md), that if ['Ăbyšālôm](../../strongs/h/h53.md) had [chay](../../strongs/h/h2416.md), and all we had [muwth](../../strongs/h/h4191.md) this [yowm](../../strongs/h/h3117.md), then it had pleased thee [yashar](../../strongs/h/h3477.md) ['ayin](../../strongs/h/h5869.md).

<a name="2samuel_19_7"></a>2Samuel 19:7

Now therefore [quwm](../../strongs/h/h6965.md), [yāṣā'](../../strongs/h/h3318.md), and [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto thy ['ebed](../../strongs/h/h5650.md): for I [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md), if thou go not [yāṣā'](../../strongs/h/h3318.md), there will not [lûn](../../strongs/h/h3885.md) ['iysh](../../strongs/h/h376.md) with thee this [layil](../../strongs/h/h3915.md): and that will be [ra'a'](../../strongs/h/h7489.md) unto thee than all the [ra'](../../strongs/h/h7451.md) that [bow'](../../strongs/h/h935.md) thee from thy [nāʿur](../../strongs/h/h5271.md) until now.

<a name="2samuel_19_8"></a>2Samuel 19:8

Then the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md), and [yashab](../../strongs/h/h3427.md) in the [sha'ar](../../strongs/h/h8179.md). And they [nāḡaḏ](../../strongs/h/h5046.md) unto all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Behold, the [melek](../../strongs/h/h4428.md) doth [yashab](../../strongs/h/h3427.md) in the [sha'ar](../../strongs/h/h8179.md). And all the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md): for [Yisra'el](../../strongs/h/h3478.md) had [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md).

<a name="2samuel_19_9"></a>2Samuel 19:9

And all the ['am](../../strongs/h/h5971.md) were at [diyn](../../strongs/h/h1777.md) throughout all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The [melek](../../strongs/h/h4428.md) [natsal](../../strongs/h/h5337.md) us out of the [kaph](../../strongs/h/h3709.md) of our ['oyeb](../../strongs/h/h341.md), and he [mālaṭ](../../strongs/h/h4422.md) us out of the [kaph](../../strongs/h/h3709.md) of the [Pᵊlištî](../../strongs/h/h6430.md); and now he is [bāraḥ](../../strongs/h/h1272.md) out of the ['erets](../../strongs/h/h776.md) for ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_19_10"></a>2Samuel 19:10

And ['Ăbyšālôm](../../strongs/h/h53.md), whom we [māšaḥ](../../strongs/h/h4886.md) over us, is [muwth](../../strongs/h/h4191.md) in [milḥāmâ](../../strongs/h/h4421.md). Now therefore why [ḥāraš](../../strongs/h/h2790.md) ye not a word of [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [shuwb](../../strongs/h/h7725.md)?

<a name="2samuel_19_11"></a>2Samuel 19:11

And [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) to [Ṣāḏôq](../../strongs/h/h6659.md) and to ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), Why are ye the ['aḥărôn](../../strongs/h/h314.md) to [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [shuwb](../../strongs/h/h7725.md) to his [bayith](../../strongs/h/h1004.md)? seeing the [dabar](../../strongs/h/h1697.md) of all [Yisra'el](../../strongs/h/h3478.md) is [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), even to his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_19_12"></a>2Samuel 19:12

Ye are my ['ach](../../strongs/h/h251.md), ye are my ['etsem](../../strongs/h/h6106.md) and my [basar](../../strongs/h/h1320.md): wherefore then are ye the ['aḥărôn](../../strongs/h/h314.md) to [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md)?

<a name="2samuel_19_13"></a>2Samuel 19:13

And ['āmar](../../strongs/h/h559.md) ye to [ʿĂmāśā'](../../strongs/h/h6021.md), Art thou not of my ['etsem](../../strongs/h/h6106.md), and of my [basar](../../strongs/h/h1320.md)? ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) so to me, and more also, if thou be not [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) [paniym](../../strongs/h/h6440.md) me [yowm](../../strongs/h/h3117.md) in the [taḥaṯ](../../strongs/h/h8478.md) of [Yô'āḇ](../../strongs/h/h3097.md).

<a name="2samuel_19_14"></a>2Samuel 19:14

And he [natah](../../strongs/h/h5186.md) the [lebab](../../strongs/h/h3824.md) of all the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), even as one ['iysh](../../strongs/h/h376.md); so that they [shalach](../../strongs/h/h7971.md) unto the [melek](../../strongs/h/h4428.md), [shuwb](../../strongs/h/h7725.md) thou, and all thy ['ebed](../../strongs/h/h5650.md).

<a name="2samuel_19_15"></a>2Samuel 19:15

So the [melek](../../strongs/h/h4428.md) [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) to [Yardēn](../../strongs/h/h3383.md). And [Yehuwdah](../../strongs/h/h3063.md) [bow'](../../strongs/h/h935.md) to [Gilgāl](../../strongs/h/h1537.md), to [yālaḵ](../../strongs/h/h3212.md) to [qārā'](../../strongs/h/h7125.md) the [melek](../../strongs/h/h4428.md), to ['abar](../../strongs/h/h5674.md) the [melek](../../strongs/h/h4428.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md).

<a name="2samuel_19_16"></a>2Samuel 19:16

And [Šimʿî](../../strongs/h/h8096.md) the [ben](../../strongs/h/h1121.md) of [Gērā'](../../strongs/h/h1617.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md), which was of [Baḥurîm](../../strongs/h/h980.md), [māhar](../../strongs/h/h4116.md) and [yarad](../../strongs/h/h3381.md) with the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) to [qārā'](../../strongs/h/h7125.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_19_17"></a>2Samuel 19:17

And there were a thousand ['iysh](../../strongs/h/h376.md) of [Ben-yᵊmînî](../../strongs/h/h1145.md) with him, and [Ṣîḇā'](../../strongs/h/h6717.md) the [naʿar](../../strongs/h/h5288.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), and his fifteen [ben](../../strongs/h/h1121.md) and his twenty ['ebed](../../strongs/h/h5650.md) with him; and they went [tsalach](../../strongs/h/h6743.md) [Yardēn](../../strongs/h/h3383.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="2samuel_19_18"></a>2Samuel 19:18

And there ['abar](../../strongs/h/h5674.md) a [ʿăḇārâ](../../strongs/h/h5679.md) to ['abar](../../strongs/h/h5674.md) the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and to ['asah](../../strongs/h/h6213.md) what he ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md). And [Šimʿî](../../strongs/h/h8096.md) the [ben](../../strongs/h/h1121.md) of [Gērā'](../../strongs/h/h1617.md) [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), as he was ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md);

<a name="2samuel_19_19"></a>2Samuel 19:19

And ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Let not my ['adown](../../strongs/h/h113.md) [chashab](../../strongs/h/h2803.md) ['avon](../../strongs/h/h5771.md) unto me, neither do thou [zakar](../../strongs/h/h2142.md) that which thy ['ebed](../../strongs/h/h5650.md) did [ʿāvâ](../../strongs/h/h5753.md) the [yowm](../../strongs/h/h3117.md) that my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [yāṣā'](../../strongs/h/h3318.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that the [melek](../../strongs/h/h4428.md) should [śûm](../../strongs/h/h7760.md) it to his [leb](../../strongs/h/h3820.md).

<a name="2samuel_19_20"></a>2Samuel 19:20

For thy ['ebed](../../strongs/h/h5650.md) doth [yada'](../../strongs/h/h3045.md) that I have [chata'](../../strongs/h/h2398.md): therefore, behold, I am [bow'](../../strongs/h/h935.md) the [ri'šôn](../../strongs/h/h7223.md) this [yowm](../../strongs/h/h3117.md) of all the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md) to [yarad](../../strongs/h/h3381.md) to [qārā'](../../strongs/h/h7125.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md).

<a name="2samuel_19_21"></a>2Samuel 19:21

But ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Shall not [Šimʿî](../../strongs/h/h8096.md) be put to [muwth](../../strongs/h/h4191.md) for this, because he [qālal](../../strongs/h/h7043.md) [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md)?

<a name="2samuel_19_22"></a>2Samuel 19:22

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), What have I to do with you, ye [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), that ye should this [yowm](../../strongs/h/h3117.md) be [satan](../../strongs/h/h7854.md) unto me? shall there any ['iysh](../../strongs/h/h376.md) be put to [muwth](../../strongs/h/h4191.md) this [yowm](../../strongs/h/h3117.md) in [Yisra'el](../../strongs/h/h3478.md)? for do not I [yada'](../../strongs/h/h3045.md) that I am this [yowm](../../strongs/h/h3117.md) [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md)?

<a name="2samuel_19_23"></a>2Samuel 19:23

Therefore the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Šimʿî](../../strongs/h/h8096.md), Thou shalt not [muwth](../../strongs/h/h4191.md). And the [melek](../../strongs/h/h4428.md) [shaba'](../../strongs/h/h7650.md) unto him.

<a name="2samuel_19_24"></a>2Samuel 19:24

And [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md) [yarad](../../strongs/h/h3381.md) to [qārā'](../../strongs/h/h7125.md) the [melek](../../strongs/h/h4428.md), and had neither ['asah](../../strongs/h/h6213.md) his [regel](../../strongs/h/h7272.md), nor ['asah](../../strongs/h/h6213.md) his [śāp̄ām](../../strongs/h/h8222.md), nor [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), from the [yowm](../../strongs/h/h3117.md) the [melek](../../strongs/h/h4428.md) [yālaḵ](../../strongs/h/h3212.md) until the [yowm](../../strongs/h/h3117.md) he [bow'](../../strongs/h/h935.md) again in [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_19_25"></a>2Samuel 19:25

And it came to pass, when he was [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) to [qārā'](../../strongs/h/h7125.md) the [melek](../../strongs/h/h4428.md), that the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, Wherefore [halak](../../strongs/h/h1980.md) not thou with me, [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md)?

<a name="2samuel_19_26"></a>2Samuel 19:26

And he ['āmar](../../strongs/h/h559.md), My ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), my ['ebed](../../strongs/h/h5650.md) [rāmâ](../../strongs/h/h7411.md) me: for thy ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md), I will [ḥāḇaš](../../strongs/h/h2280.md) me a [chamowr](../../strongs/h/h2543.md), that I may [rāḵaḇ](../../strongs/h/h7392.md) thereon, and [yālaḵ](../../strongs/h/h3212.md) to the [melek](../../strongs/h/h4428.md); because thy ['ebed](../../strongs/h/h5650.md) is [pissēaḥ](../../strongs/h/h6455.md).

<a name="2samuel_19_27"></a>2Samuel 19:27

And he hath [ragal](../../strongs/h/h7270.md) thy ['ebed](../../strongs/h/h5650.md) unto my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md); but my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) is as a [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md): ['asah](../../strongs/h/h6213.md) therefore what is [towb](../../strongs/h/h2896.md) in thine ['ayin](../../strongs/h/h5869.md).

<a name="2samuel_19_28"></a>2Samuel 19:28

For all of my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) were but [maveth](../../strongs/h/h4194.md) ['enowsh](../../strongs/h/h582.md) before my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md): yet didst thou [shiyth](../../strongs/h/h7896.md) thy ['ebed](../../strongs/h/h5650.md) among them that did ['akal](../../strongs/h/h398.md) at thine own [šulḥān](../../strongs/h/h7979.md). What [tsedaqah](../../strongs/h/h6666.md) therefore have I yet to [zāʿaq](../../strongs/h/h2199.md) any more unto the [melek](../../strongs/h/h4428.md)?

<a name="2samuel_19_29"></a>2Samuel 19:29

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, Why [dabar](../../strongs/h/h1696.md) thou any more of thy [dabar](../../strongs/h/h1697.md)? I have ['āmar](../../strongs/h/h559.md), Thou and [Ṣîḇā'](../../strongs/h/h6717.md) [chalaq](../../strongs/h/h2505.md) the [sadeh](../../strongs/h/h7704.md).

<a name="2samuel_19_30"></a>2Samuel 19:30

And [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Yea, let him [laqach](../../strongs/h/h3947.md) all, ['aḥar](../../strongs/h/h310.md) as my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) is [bow'](../../strongs/h/h935.md) in [shalowm](../../strongs/h/h7965.md) unto his own [bayith](../../strongs/h/h1004.md).

<a name="2samuel_19_31"></a>2Samuel 19:31

And [Barzillay](../../strongs/h/h1271.md) the [Gilʿāḏî](../../strongs/h/h1569.md) [yarad](../../strongs/h/h3381.md) from [Rōḡlîm](../../strongs/h/h7274.md), and ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) with the [melek](../../strongs/h/h4428.md), to [shalach](../../strongs/h/h7971.md) him over [Yardēn](../../strongs/h/h3383.md).

<a name="2samuel_19_32"></a>2Samuel 19:32

Now [Barzillay](../../strongs/h/h1271.md) was a [me'od](../../strongs/h/h3966.md) [zāqēn](../../strongs/h/h2204.md) man, even fourscore [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md): and he had provided the [melek](../../strongs/h/h4428.md) of [kûl](../../strongs/h/h3557.md) while he [šîḇâ](../../strongs/h/h7871.md) at [Maḥănayim](../../strongs/h/h4266.md); for he was a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) ['iysh](../../strongs/h/h376.md).

<a name="2samuel_19_33"></a>2Samuel 19:33

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Barzillay](../../strongs/h/h1271.md), Come thou ['abar](../../strongs/h/h5674.md) with me, and I will [kûl](../../strongs/h/h3557.md) thee with me in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_19_34"></a>2Samuel 19:34

And [Barzillay](../../strongs/h/h1271.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), How [yowm](../../strongs/h/h3117.md) have I to [šānâ](../../strongs/h/h8141.md) [chay](../../strongs/h/h2416.md), that I should [ʿālâ](../../strongs/h/h5927.md) with the [melek](../../strongs/h/h4428.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="2samuel_19_35"></a>2Samuel 19:35

I am this [yowm](../../strongs/h/h3117.md) fourscore [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md): and can I [yada'](../../strongs/h/h3045.md) between [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md)? can thy ['ebed](../../strongs/h/h5650.md) [ṭāʿam](../../strongs/h/h2938.md) what I ['akal](../../strongs/h/h398.md) or what I [šāṯâ](../../strongs/h/h8354.md)? can I [shama'](../../strongs/h/h8085.md) any more the [qowl](../../strongs/h/h6963.md) of [shiyr](../../strongs/h/h7891.md) and [shiyr](../../strongs/h/h7891.md)? wherefore then should thy ['ebed](../../strongs/h/h5650.md) be yet a [maśśā'](../../strongs/h/h4853.md) unto my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md)?

<a name="2samuel_19_36"></a>2Samuel 19:36

Thy ['ebed](../../strongs/h/h5650.md) will ['abar](../../strongs/h/h5674.md) a [mᵊʿaṭ](../../strongs/h/h4592.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) with the [melek](../../strongs/h/h4428.md): and why should the [melek](../../strongs/h/h4428.md) [gamal](../../strongs/h/h1580.md) it me with such a [gᵊmûlâ](../../strongs/h/h1578.md)?

<a name="2samuel_19_37"></a>2Samuel 19:37

Let thy ['ebed](../../strongs/h/h5650.md), I pray thee, [shuwb](../../strongs/h/h7725.md), that I may [muwth](../../strongs/h/h4191.md) in mine own [ʿîr](../../strongs/h/h5892.md), and by the [qeber](../../strongs/h/h6913.md) of my ['ab](../../strongs/h/h1.md) and of my ['em](../../strongs/h/h517.md). But behold thy ['ebed](../../strongs/h/h5650.md) [Kimhām](../../strongs/h/h3643.md); let him ['abar](../../strongs/h/h5674.md) with my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md); and ['asah](../../strongs/h/h6213.md) to him what shall seem [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md) unto thee.

<a name="2samuel_19_38"></a>2Samuel 19:38

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [Kimhām](../../strongs/h/h3643.md) shall ['abar](../../strongs/h/h5674.md) over with me, and I will ['asah](../../strongs/h/h6213.md) to him that which shall seem [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md) unto thee: and whatsoever thou shalt [bāḥar](../../strongs/h/h977.md) of me, that will I ['asah](../../strongs/h/h6213.md) for thee.

<a name="2samuel_19_39"></a>2Samuel 19:39

And all the ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md). And when the [melek](../../strongs/h/h4428.md) was ['abar](../../strongs/h/h5674.md), the [melek](../../strongs/h/h4428.md) [nashaq](../../strongs/h/h5401.md) [Barzillay](../../strongs/h/h1271.md), and [barak](../../strongs/h/h1288.md) him; and he [shuwb](../../strongs/h/h7725.md) unto his own [maqowm](../../strongs/h/h4725.md).

<a name="2samuel_19_40"></a>2Samuel 19:40

Then the [melek](../../strongs/h/h4428.md) ['abar](../../strongs/h/h5674.md) to [Gilgāl](../../strongs/h/h1537.md), and [Kimhām](../../strongs/h/h3643.md) ['abar](../../strongs/h/h5674.md) with him: and all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md) ['abar](../../strongs/h/h5674.md) ['abar](../../strongs/h/h5674.md) the [melek](../../strongs/h/h4428.md), and also half the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_19_41"></a>2Samuel 19:41

And, behold, all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Why have our ['ach](../../strongs/h/h251.md) the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) [ganab](../../strongs/h/h1589.md) thee, and have ['abar](../../strongs/h/h5674.md) the [melek](../../strongs/h/h4428.md), and his [bayith](../../strongs/h/h1004.md), and all [Dāviḏ](../../strongs/h/h1732.md) ['enowsh](../../strongs/h/h582.md) with him, over [Yardēn](../../strongs/h/h3383.md)?

<a name="2samuel_19_42"></a>2Samuel 19:42

And all the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) ['anah](../../strongs/h/h6030.md) the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), Because the [melek](../../strongs/h/h4428.md) is near of [qarowb](../../strongs/h/h7138.md) to us: wherefore then be ye [ḥārâ](../../strongs/h/h2734.md) for this [dabar](../../strongs/h/h1697.md)? have we ['akal](../../strongs/h/h398.md) at ['akal](../../strongs/h/h398.md) of the [melek](../../strongs/h/h4428.md)? or hath he [nasa'](../../strongs/h/h5375.md) us any [niśśē'ṯ](../../strongs/h/h5379.md)?

<a name="2samuel_19_43"></a>2Samuel 19:43

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) ['anah](../../strongs/h/h6030.md) the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md), We have ten [yad](../../strongs/h/h3027.md) in the [melek](../../strongs/h/h4428.md), and we have also more right in [Dāviḏ](../../strongs/h/h1732.md) than ye: why then did ye [qālal](../../strongs/h/h7043.md) us, that our [dabar](../../strongs/h/h1697.md) should not be [ri'šôn](../../strongs/h/h7223.md) had in bringing [shuwb](../../strongs/h/h7725.md) our [melek](../../strongs/h/h4428.md)? And the [dabar](../../strongs/h/h1697.md) of the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) were [qāšâ](../../strongs/h/h7185.md) than the [dabar](../../strongs/h/h1697.md) of the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 18](2samuel_18.md) - [2Samuel 20](2samuel_20.md)