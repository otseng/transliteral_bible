# [2Samuel 13](https://www.blueletterbible.org/kjv/2samuel/13)

<a name="2samuel_13_1"></a>2Samuel 13:1

And it came to pass ['aḥar](../../strongs/h/h310.md) this, that ['Ăbyšālôm](../../strongs/h/h53.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) had a [yāp̄ê](../../strongs/h/h3303.md) ['āḥôṯ](../../strongs/h/h269.md), whose [shem](../../strongs/h/h8034.md) was [Tāmār](../../strongs/h/h8559.md); and ['Amnôn](../../strongs/h/h550.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) ['ahab](../../strongs/h/h157.md) her.

<a name="2samuel_13_2"></a>2Samuel 13:2

And ['Amnôn](../../strongs/h/h550.md) was so [yāṣar](../../strongs/h/h3334.md), that he fell [ḥālâ](../../strongs/h/h2470.md) for his ['āḥôṯ](../../strongs/h/h269.md) [Tāmār](../../strongs/h/h8559.md); for she was a [bᵊṯûlâ](../../strongs/h/h1330.md); and ['Amnôn](../../strongs/h/h550.md) ['ayin](../../strongs/h/h5869.md) it [pala'](../../strongs/h/h6381.md) for him to ['asah](../../strongs/h/h6213.md) any [mᵊ'ûmâ](../../strongs/h/h3972.md) to her.

<a name="2samuel_13_3"></a>2Samuel 13:3

But ['Amnôn](../../strongs/h/h550.md) had a [rea'](../../strongs/h/h7453.md), whose [shem](../../strongs/h/h8034.md) was [Yônāḏāḇ](../../strongs/h/h3122.md), the [ben](../../strongs/h/h1121.md) of [ŠimʿÂ](../../strongs/h/h8093.md) [Dāviḏ](../../strongs/h/h1732.md) ['ach](../../strongs/h/h251.md): and [Yônāḏāḇ](../../strongs/h/h3122.md) was a [me'od](../../strongs/h/h3966.md) [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md).

<a name="2samuel_13_4"></a>2Samuel 13:4

And he ['āmar](../../strongs/h/h559.md) unto him, Why art thou, being the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), [dal](../../strongs/h/h1800.md) from [boqer](../../strongs/h/h1242.md) to [boqer](../../strongs/h/h1242.md)? wilt thou not [nāḡaḏ](../../strongs/h/h5046.md) me? And ['Amnôn](../../strongs/h/h550.md) ['āmar](../../strongs/h/h559.md) unto him, I ['ahab](../../strongs/h/h157.md) [Tāmār](../../strongs/h/h8559.md), my ['ach](../../strongs/h/h251.md) ['Ăbyšālôm](../../strongs/h/h53.md) ['āḥôṯ](../../strongs/h/h269.md).

<a name="2samuel_13_5"></a>2Samuel 13:5

And [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) ['āmar](../../strongs/h/h559.md) unto him, Lay thee [shakab](../../strongs/h/h7901.md) on thy [miškāḇ](../../strongs/h/h4904.md), and make thyself [ḥālâ](../../strongs/h/h2470.md): and when thy ['ab](../../strongs/h/h1.md) [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) thee, ['āmar](../../strongs/h/h559.md) unto him, I pray thee, let my ['āḥôṯ](../../strongs/h/h269.md) [Tāmār](../../strongs/h/h8559.md) [bow'](../../strongs/h/h935.md), and [bārâ](../../strongs/h/h1262.md) me [lechem](../../strongs/h/h3899.md), and ['asah](../../strongs/h/h6213.md) the [biryâ](../../strongs/h/h1279.md) in my ['ayin](../../strongs/h/h5869.md), that I may [ra'ah](../../strongs/h/h7200.md) it, and ['akal](../../strongs/h/h398.md) it at her [yad](../../strongs/h/h3027.md).

<a name="2samuel_13_6"></a>2Samuel 13:6

So ['Amnôn](../../strongs/h/h550.md) lay [shakab](../../strongs/h/h7901.md), and made himself [ḥālâ](../../strongs/h/h2470.md): and when the [melek](../../strongs/h/h4428.md) was [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) him, ['Amnôn](../../strongs/h/h550.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), I pray thee, let [Tāmār](../../strongs/h/h8559.md) my ['āḥôṯ](../../strongs/h/h269.md) [bow'](../../strongs/h/h935.md), and [lāḇaḇ](../../strongs/h/h3823.md) me a couple of [lᵊḇîḇôṯ](../../strongs/h/h3834.md) in my ['ayin](../../strongs/h/h5869.md), that I may [bārâ](../../strongs/h/h1262.md) at her [yad](../../strongs/h/h3027.md).

<a name="2samuel_13_7"></a>2Samuel 13:7

Then [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [bayith](../../strongs/h/h1004.md) to [Tāmār](../../strongs/h/h8559.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) now to thy ['ach](../../strongs/h/h251.md) ['Amnôn](../../strongs/h/h550.md) [bayith](../../strongs/h/h1004.md), and ['asah](../../strongs/h/h6213.md) him [biryâ](../../strongs/h/h1279.md).

<a name="2samuel_13_8"></a>2Samuel 13:8

So [Tāmār](../../strongs/h/h8559.md) [yālaḵ](../../strongs/h/h3212.md) to her ['ach](../../strongs/h/h251.md) ['Amnôn](../../strongs/h/h550.md) [bayith](../../strongs/h/h1004.md); and he was [shakab](../../strongs/h/h7901.md). And she [laqach](../../strongs/h/h3947.md) [bāṣēq](../../strongs/h/h1217.md), and [lûš](../../strongs/h/h3888.md) it, and made [lāḇaḇ](../../strongs/h/h3823.md) in his ['ayin](../../strongs/h/h5869.md), and did [bāšal](../../strongs/h/h1310.md) the [lᵊḇîḇôṯ](../../strongs/h/h3834.md).

<a name="2samuel_13_9"></a>2Samuel 13:9

And she [laqach](../../strongs/h/h3947.md) a [maśrēṯ](../../strongs/h/h4958.md), and poured them [yāṣaq](../../strongs/h/h3332.md) [paniym](../../strongs/h/h6440.md) him; but he [mā'ēn](../../strongs/h/h3985.md) to ['akal](../../strongs/h/h398.md). And ['Amnôn](../../strongs/h/h550.md) ['āmar](../../strongs/h/h559.md), Have [yāṣā'](../../strongs/h/h3318.md) all ['iysh](../../strongs/h/h376.md) from me. And they [yāṣā'](../../strongs/h/h3318.md) every ['iysh](../../strongs/h/h376.md) from him.

<a name="2samuel_13_10"></a>2Samuel 13:10

And ['Amnôn](../../strongs/h/h550.md) ['āmar](../../strongs/h/h559.md) unto [Tāmār](../../strongs/h/h8559.md), [bow'](../../strongs/h/h935.md) the [biryâ](../../strongs/h/h1279.md) into the [ḥeḏer](../../strongs/h/h2315.md), that I may [bārâ](../../strongs/h/h1262.md) of thine [yad](../../strongs/h/h3027.md). And [Tāmār](../../strongs/h/h8559.md) [laqach](../../strongs/h/h3947.md) the [lᵊḇîḇôṯ](../../strongs/h/h3834.md) which she had ['asah](../../strongs/h/h6213.md), and [bow'](../../strongs/h/h935.md) them into the [ḥeḏer](../../strongs/h/h2315.md) to ['Amnôn](../../strongs/h/h550.md) her ['ach](../../strongs/h/h251.md).

<a name="2samuel_13_11"></a>2Samuel 13:11

And when she had [nāḡaš](../../strongs/h/h5066.md) them unto him to ['akal](../../strongs/h/h398.md), he [ḥāzaq](../../strongs/h/h2388.md) of her, and ['āmar](../../strongs/h/h559.md) unto her, [bow'](../../strongs/h/h935.md) [shakab](../../strongs/h/h7901.md) with me, my ['āḥôṯ](../../strongs/h/h269.md).

<a name="2samuel_13_12"></a>2Samuel 13:12

And she ['āmar](../../strongs/h/h559.md) him, Nay, my ['ach](../../strongs/h/h251.md), do not [ʿānâ](../../strongs/h/h6031.md) me; for no such thing ought to be ['asah](../../strongs/h/h6213.md) in [Yisra'el](../../strongs/h/h3478.md): ['asah](../../strongs/h/h6213.md) not thou this [nᵊḇālâ](../../strongs/h/h5039.md).

<a name="2samuel_13_13"></a>2Samuel 13:13

And I, whither shall I cause my [cherpah](../../strongs/h/h2781.md) to [yālaḵ](../../strongs/h/h3212.md)? and as for thee, thou shalt be as one of the [nabal](../../strongs/h/h5036.md) in [Yisra'el](../../strongs/h/h3478.md). Now therefore, I pray thee, [dabar](../../strongs/h/h1696.md) unto the [melek](../../strongs/h/h4428.md); for he will not [mānaʿ](../../strongs/h/h4513.md) me from thee.

<a name="2samuel_13_14"></a>2Samuel 13:14

Howbeit he ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto her [qowl](../../strongs/h/h6963.md): but, being [ḥāzaq](../../strongs/h/h2388.md) than she, [ʿānâ](../../strongs/h/h6031.md) her, and [shakab](../../strongs/h/h7901.md) with her.

<a name="2samuel_13_15"></a>2Samuel 13:15

Then ['Amnôn](../../strongs/h/h550.md) [sane'](../../strongs/h/h8130.md) her [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md); so that the [śin'â](../../strongs/h/h8135.md) wherewith he [sane'](../../strongs/h/h8130.md) her was [gadowl](../../strongs/h/h1419.md) than the ['ahăḇâ](../../strongs/h/h160.md) wherewith he had ['ahab](../../strongs/h/h157.md) her. And ['Amnôn](../../strongs/h/h550.md) ['āmar](../../strongs/h/h559.md) unto her, [quwm](../../strongs/h/h6965.md), be [yālaḵ](../../strongs/h/h3212.md).

<a name="2samuel_13_16"></a>2Samuel 13:16

And she ['āmar](../../strongs/h/h559.md) unto him, There is no cause: this [ra'](../../strongs/h/h7451.md) in [shalach](../../strongs/h/h7971.md) me is [gadowl](../../strongs/h/h1419.md) than the ['aḥēr](../../strongs/h/h312.md) that thou ['asah](../../strongs/h/h6213.md) unto me. But he ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto her.

<a name="2samuel_13_17"></a>2Samuel 13:17

Then he [qara'](../../strongs/h/h7121.md) his [naʿar](../../strongs/h/h5288.md) that [sharath](../../strongs/h/h8334.md) unto him, and ['āmar](../../strongs/h/h559.md), [shalach](../../strongs/h/h7971.md) now this [ḥûṣ](../../strongs/h/h2351.md) from me, and [nāʿal](../../strongs/h/h5274.md) the [deleṯ](../../strongs/h/h1817.md) ['aḥar](../../strongs/h/h310.md) her.

<a name="2samuel_13_18"></a>2Samuel 13:18

And she had a [kĕthoneth](../../strongs/h/h3801.md) of [pas](../../strongs/h/h6446.md) upon her: for with such [mᵊʿîl](../../strongs/h/h4598.md) were the [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md) that were [bᵊṯûlâ](../../strongs/h/h1330.md) [labash](../../strongs/h/h3847.md). Then his [sharath](../../strongs/h/h8334.md) [yāṣā'](../../strongs/h/h3318.md) her [ḥûṣ](../../strongs/h/h2351.md), and [nāʿal](../../strongs/h/h5274.md) the [deleṯ](../../strongs/h/h1817.md) ['aḥar](../../strongs/h/h310.md) her.

<a name="2samuel_13_19"></a>2Samuel 13:19

And [Tāmār](../../strongs/h/h8559.md) [laqach](../../strongs/h/h3947.md) ['ēp̄er](../../strongs/h/h665.md) on her [ro'sh](../../strongs/h/h7218.md), and [qāraʿ](../../strongs/h/h7167.md) her [kĕthoneth](../../strongs/h/h3801.md) of [pas](../../strongs/h/h6446.md) that was on her, and [śûm](../../strongs/h/h7760.md) her [yad](../../strongs/h/h3027.md) on her [ro'sh](../../strongs/h/h7218.md), and [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md) [zāʿaq](../../strongs/h/h2199.md).

<a name="2samuel_13_20"></a>2Samuel 13:20

And ['Ăbyšālôm](../../strongs/h/h53.md) her ['ach](../../strongs/h/h251.md) ['āmar](../../strongs/h/h559.md) unto her, Hath ['Amnôn](../../strongs/h/h550.md) thy ['ach](../../strongs/h/h251.md) been with thee? but hold now thy [ḥāraš](../../strongs/h/h2790.md), my ['āḥôṯ](../../strongs/h/h269.md): he is thy ['ach](../../strongs/h/h251.md); [shiyth](../../strongs/h/h7896.md) [leb](../../strongs/h/h3820.md) not this [dabar](../../strongs/h/h1697.md). So [Tāmār](../../strongs/h/h8559.md) [yashab](../../strongs/h/h3427.md) [šāmēm](../../strongs/h/h8074.md) in her ['ach](../../strongs/h/h251.md) ['Ăbyšālôm](../../strongs/h/h53.md) [bayith](../../strongs/h/h1004.md).

<a name="2samuel_13_21"></a>2Samuel 13:21

But when [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) of all these [dabar](../../strongs/h/h1697.md), he was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md).

<a name="2samuel_13_22"></a>2Samuel 13:22

And ['Ăbyšālôm](../../strongs/h/h53.md) [dabar](../../strongs/h/h1696.md) unto ['Amnôn](../../strongs/h/h550.md) neither [towb](../../strongs/h/h2896.md) nor [ra'](../../strongs/h/h7451.md): for ['Ăbyšālôm](../../strongs/h/h53.md) [sane'](../../strongs/h/h8130.md) ['Amnôn](../../strongs/h/h550.md), [dabar](../../strongs/h/h1697.md) he had [ʿānâ](../../strongs/h/h6031.md) his ['āḥôṯ](../../strongs/h/h269.md) [Tāmār](../../strongs/h/h8559.md).

<a name="2samuel_13_23"></a>2Samuel 13:23

And it came to pass after two [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md), that ['Ăbyšālôm](../../strongs/h/h53.md) had [gāzaz](../../strongs/h/h1494.md) in [BaʿAl Ḥāṣôr](../../strongs/h/h1178.md), which is beside ['Ep̄rayim](../../strongs/h/h669.md): and ['Ăbyšālôm](../../strongs/h/h53.md) [qara'](../../strongs/h/h7121.md) all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md).

<a name="2samuel_13_24"></a>2Samuel 13:24

And ['Ăbyšālôm](../../strongs/h/h53.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), Behold now, thy ['ebed](../../strongs/h/h5650.md) hath [gāzaz](../../strongs/h/h1494.md); [yālaḵ](../../strongs/h/h3212.md) the [melek](../../strongs/h/h4428.md), I beseech thee, and his ['ebed](../../strongs/h/h5650.md) [yālaḵ](../../strongs/h/h3212.md) with thy ['ebed](../../strongs/h/h5650.md).

<a name="2samuel_13_25"></a>2Samuel 13:25

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to ['Ăbyšālôm](../../strongs/h/h53.md), Nay, my [ben](../../strongs/h/h1121.md), let us not all now [yālaḵ](../../strongs/h/h3212.md), lest we be [kabad](../../strongs/h/h3513.md) unto thee. And he [pāraṣ](../../strongs/h/h6555.md) him: howbeit he ['āḇâ](../../strongs/h/h14.md) not [yālaḵ](../../strongs/h/h3212.md), but [barak](../../strongs/h/h1288.md) him.

<a name="2samuel_13_26"></a>2Samuel 13:26

Then ['āmar](../../strongs/h/h559.md) ['Ăbyšālôm](../../strongs/h/h53.md), If not, I pray thee, let my ['ach](../../strongs/h/h251.md) ['Amnôn](../../strongs/h/h550.md) [yālaḵ](../../strongs/h/h3212.md) with us. And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, Why should he [yālaḵ](../../strongs/h/h3212.md) with thee?

<a name="2samuel_13_27"></a>2Samuel 13:27

But ['Ăbyšālôm](../../strongs/h/h53.md) [pāraṣ](../../strongs/h/h6555.md) him, that he let ['Amnôn](../../strongs/h/h550.md) and all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) [shalach](../../strongs/h/h7971.md) with him.

<a name="2samuel_13_28"></a>2Samuel 13:28

Now ['Ăbyšālôm](../../strongs/h/h53.md) had [tsavah](../../strongs/h/h6680.md) his [naʿar](../../strongs/h/h5288.md), ['āmar](../../strongs/h/h559.md), [ra'ah](../../strongs/h/h7200.md) ye now when ['Amnôn](../../strongs/h/h550.md) [leb](../../strongs/h/h3820.md) is [towb](../../strongs/h/h2896.md) with [yayin](../../strongs/h/h3196.md), and when I ['āmar](../../strongs/h/h559.md) unto you, [nakah](../../strongs/h/h5221.md) ['Amnôn](../../strongs/h/h550.md); then [muwth](../../strongs/h/h4191.md) him, [yare'](../../strongs/h/h3372.md) not: have not I [tsavah](../../strongs/h/h6680.md) you? be [ḥāzaq](../../strongs/h/h2388.md), and be [ben](../../strongs/h/h1121.md) [ḥayil](../../strongs/h/h2428.md).

<a name="2samuel_13_29"></a>2Samuel 13:29

And the [naʿar](../../strongs/h/h5288.md) of ['Ăbyšālôm](../../strongs/h/h53.md) ['asah](../../strongs/h/h6213.md) unto ['Amnôn](../../strongs/h/h550.md) as ['Ăbyšālôm](../../strongs/h/h53.md) had [tsavah](../../strongs/h/h6680.md). Then all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) [quwm](../../strongs/h/h6965.md), and every ['iysh](../../strongs/h/h376.md) gat him [rāḵaḇ](../../strongs/h/h7392.md) upon his [pereḏ](../../strongs/h/h6505.md), and [nûs](../../strongs/h/h5127.md).

<a name="2samuel_13_30"></a>2Samuel 13:30

And it came to pass, while they were in the [derek](../../strongs/h/h1870.md), that [šᵊmûʿâ](../../strongs/h/h8052.md) [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), ['Ăbyšālôm](../../strongs/h/h53.md) hath [nakah](../../strongs/h/h5221.md) all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and there is not one of them [yāṯar](../../strongs/h/h3498.md).

<a name="2samuel_13_31"></a>2Samuel 13:31

Then the [melek](../../strongs/h/h4428.md) [quwm](../../strongs/h/h6965.md), and [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and [shakab](../../strongs/h/h7901.md) on the ['erets](../../strongs/h/h776.md); and all his ['ebed](../../strongs/h/h5650.md) [nāṣaḇ](../../strongs/h/h5324.md) by with their [beḡeḏ](../../strongs/h/h899.md) [qāraʿ](../../strongs/h/h7167.md).

<a name="2samuel_13_32"></a>2Samuel 13:32

And [Yônāḏāḇ](../../strongs/h/h3122.md), the [ben](../../strongs/h/h1121.md) of [ŠimʿÂ](../../strongs/h/h8093.md) [Dāviḏ](../../strongs/h/h1732.md) ['ach](../../strongs/h/h251.md), ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Let not my ['adown](../../strongs/h/h113.md) ['āmar](../../strongs/h/h559.md) that they have [muwth](../../strongs/h/h4191.md) all the [naʿar](../../strongs/h/h5288.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md); for ['Amnôn](../../strongs/h/h550.md) only is [muwth](../../strongs/h/h4191.md): for by the [peh](../../strongs/h/h6310.md) of ['Ăbyšālôm](../../strongs/h/h53.md) this hath been [śûm](../../strongs/h/h7760.md) from the [yowm](../../strongs/h/h3117.md) that he [ʿānâ](../../strongs/h/h6031.md) his ['āḥôṯ](../../strongs/h/h269.md) [Tāmār](../../strongs/h/h8559.md).

<a name="2samuel_13_33"></a>2Samuel 13:33

Now therefore let not my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [śûm](../../strongs/h/h7760.md) the [dabar](../../strongs/h/h1697.md) to his [leb](../../strongs/h/h3820.md), to ['āmar](../../strongs/h/h559.md) that all the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) are [muwth](../../strongs/h/h4191.md): for ['Amnôn](../../strongs/h/h550.md) only is [muwth](../../strongs/h/h4191.md).

<a name="2samuel_13_34"></a>2Samuel 13:34

But ['Ăbyšālôm](../../strongs/h/h53.md) [bāraḥ](../../strongs/h/h1272.md). And the [naʿar](../../strongs/h/h5288.md) that kept the [tsaphah](../../strongs/h/h6822.md) [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, there [halak](../../strongs/h/h1980.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) by the [derek](../../strongs/h/h1870.md) of the [har](../../strongs/h/h2022.md) [ṣaḏ](../../strongs/h/h6654.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="2samuel_13_35"></a>2Samuel 13:35

And [Yônāḏāḇ](../../strongs/h/h3122.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Behold, the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md): as thy ['ebed](../../strongs/h/h5650.md) [dabar](../../strongs/h/h1697.md), so it is.

<a name="2samuel_13_36"></a>2Samuel 13:36

And it came to pass, as soon as he had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md), that, behold, the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md), and [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md) and [bāḵâ](../../strongs/h/h1058.md): and the [melek](../../strongs/h/h4428.md) also and all his ['ebed](../../strongs/h/h5650.md) [bāḵâ](../../strongs/h/h1058.md) [me'od](../../strongs/h/h3966.md) [bĕkiy](../../strongs/h/h1065.md) [gadowl](../../strongs/h/h1419.md).

<a name="2samuel_13_37"></a>2Samuel 13:37

But ['Ăbyšālôm](../../strongs/h/h53.md) [bāraḥ](../../strongs/h/h1272.md), and [yālaḵ](../../strongs/h/h3212.md) to [Talmay](../../strongs/h/h8526.md), the [ben](../../strongs/h/h1121.md) of [ʿAmmîḥûr](../../strongs/h/h5991.md) [ʿAmmîhûḏ](../../strongs/h/h5989.md), [melek](../../strongs/h/h4428.md) of [Gᵊšûr](../../strongs/h/h1650.md). And ['āḇal](../../strongs/h/h56.md) for his [ben](../../strongs/h/h1121.md) every [yowm](../../strongs/h/h3117.md).

<a name="2samuel_13_38"></a>2Samuel 13:38

So ['Ăbyšālôm](../../strongs/h/h53.md) [bāraḥ](../../strongs/h/h1272.md), and [yālaḵ](../../strongs/h/h3212.md) to [Gᵊšûr](../../strongs/h/h1650.md), and was there three [šānâ](../../strongs/h/h8141.md).

<a name="2samuel_13_39"></a>2Samuel 13:39

And the [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [kalah](../../strongs/h/h3615.md) to [yāṣā'](../../strongs/h/h3318.md) unto ['Ăbyšālôm](../../strongs/h/h53.md): for he was [nacham](../../strongs/h/h5162.md) concerning ['Amnôn](../../strongs/h/h550.md), seeing he was [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 12](2samuel_12.md) - [2Samuel 14](2samuel_14.md)