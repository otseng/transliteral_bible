# [2Samuel 9](https://www.blueletterbible.org/kjv/2samuel/9)

<a name="2samuel_9_1"></a>2Samuel 9:1

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), Is there yet any that is [yāṯar](../../strongs/h/h3498.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), that I may ['asah](../../strongs/h/h6213.md) him [checed](../../strongs/h/h2617.md) for [Yᵊhônāṯān](../../strongs/h/h3083.md) sake?

<a name="2samuel_9_2"></a>2Samuel 9:2

And there was of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md) an ['ebed](../../strongs/h/h5650.md) whose [shem](../../strongs/h/h8034.md) was [Ṣîḇā'](../../strongs/h/h6717.md). And when they had [qara'](../../strongs/h/h7121.md) him unto [Dāviḏ](../../strongs/h/h1732.md), the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, Art thou [Ṣîḇā'](../../strongs/h/h6717.md)? And he ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) is he.

<a name="2samuel_9_3"></a>2Samuel 9:3

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Is there ['ep̄es](../../strongs/h/h657.md) yet ['iysh](../../strongs/h/h376.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), that I may ['asah](../../strongs/h/h6213.md) the [checed](../../strongs/h/h2617.md) of ['Elohiym](../../strongs/h/h430.md) unto him? And [Ṣîḇā'](../../strongs/h/h6717.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), [Yᵊhônāṯān](../../strongs/h/h3083.md) hath yet a [ben](../../strongs/h/h1121.md), which is [nāḵê](../../strongs/h/h5223.md) on his [regel](../../strongs/h/h7272.md).

<a name="2samuel_9_4"></a>2Samuel 9:4

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, ['êp̄ô](../../strongs/h/h375.md) is he? And [Ṣîḇā'](../../strongs/h/h6717.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Behold, he is in the [bayith](../../strongs/h/h1004.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [ʿAmmî'ēl](../../strongs/h/h5988.md), in [Lō' Ḏᵊḇār](../../strongs/h/h3810.md).

<a name="2samuel_9_5"></a>2Samuel 9:5

Then [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) him out of the [bayith](../../strongs/h/h1004.md) of [Māḵîr](../../strongs/h/h4353.md), the [ben](../../strongs/h/h1121.md) of [ʿAmmî'ēl](../../strongs/h/h5988.md), from [Lō' Ḏᵊḇār](../../strongs/h/h3810.md).

<a name="2samuel_9_6"></a>2Samuel 9:6

Now when [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md), the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md), was [bow'](../../strongs/h/h935.md) unto [Dāviḏ](../../strongs/h/h1732.md), he [naphal](../../strongs/h/h5307.md) on his [paniym](../../strongs/h/h6440.md), and did [shachah](../../strongs/h/h7812.md). And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md). And he ['āmar](../../strongs/h/h559.md), Behold thy ['ebed](../../strongs/h/h5650.md)!

<a name="2samuel_9_7"></a>2Samuel 9:7

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, [yare'](../../strongs/h/h3372.md) not: for I will ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) thee [checed](../../strongs/h/h2617.md) for [Yᵊhônāṯān](../../strongs/h/h3083.md) thy ['ab](../../strongs/h/h1.md) sake, and will [shuwb](../../strongs/h/h7725.md) thee all the [sadeh](../../strongs/h/h7704.md) of [Šā'ûl](../../strongs/h/h7586.md) thy ['ab](../../strongs/h/h1.md); and thou shalt ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) at my [šulḥān](../../strongs/h/h7979.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="2samuel_9_8"></a>2Samuel 9:8

And he [shachah](../../strongs/h/h7812.md) himself, and ['āmar](../../strongs/h/h559.md), What is thy ['ebed](../../strongs/h/h5650.md), that thou shouldest [panah](../../strongs/h/h6437.md) upon such a [muwth](../../strongs/h/h4191.md) [keleḇ](../../strongs/h/h3611.md) as I am?

<a name="2samuel_9_9"></a>2Samuel 9:9

Then the [melek](../../strongs/h/h4428.md) [qara'](../../strongs/h/h7121.md) to [Ṣîḇā'](../../strongs/h/h6717.md), [Šā'ûl](../../strongs/h/h7586.md) [naʿar](../../strongs/h/h5288.md), and ['āmar](../../strongs/h/h559.md) unto him, I have [nathan](../../strongs/h/h5414.md) unto thy ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md) all that pertained to [Šā'ûl](../../strongs/h/h7586.md) and to all his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_9_10"></a>2Samuel 9:10

Thou therefore, and thy [ben](../../strongs/h/h1121.md), and thy ['ebed](../../strongs/h/h5650.md), shall ['abad](../../strongs/h/h5647.md) the ['ăḏāmâ](../../strongs/h/h127.md) for him, and thou shalt [bow'](../../strongs/h/h935.md) in, that thy ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md) may have [lechem](../../strongs/h/h3899.md) to ['akal](../../strongs/h/h398.md): but [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) thy ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md) shall ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) [tāmîḏ](../../strongs/h/h8548.md) at my [šulḥān](../../strongs/h/h7979.md). Now [Ṣîḇā'](../../strongs/h/h6717.md) had fifteen [ben](../../strongs/h/h1121.md) and twenty ['ebed](../../strongs/h/h5650.md).

<a name="2samuel_9_11"></a>2Samuel 9:11

Then ['āmar](../../strongs/h/h559.md) [Ṣîḇā'](../../strongs/h/h6717.md) unto the [melek](../../strongs/h/h4428.md), According to all that my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) hath [tsavah](../../strongs/h/h6680.md) his ['ebed](../../strongs/h/h5650.md), so shall thy ['ebed](../../strongs/h/h5650.md) ['asah](../../strongs/h/h6213.md). As for [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md), said the king, he shall ['akal](../../strongs/h/h398.md) at my [šulḥān](../../strongs/h/h7979.md), as one of the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md).

<a name="2samuel_9_12"></a>2Samuel 9:12

And [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) had a [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) was [Mîḵā'](../../strongs/h/h4316.md). And all that [môšāḇ](../../strongs/h/h4186.md) in the [bayith](../../strongs/h/h1004.md) of [Ṣîḇā'](../../strongs/h/h6717.md) were ['ebed](../../strongs/h/h5650.md) unto [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md).

<a name="2samuel_9_13"></a>2Samuel 9:13

So [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): for he did ['akal](../../strongs/h/h398.md) [tāmîḏ](../../strongs/h/h8548.md) at the [melek](../../strongs/h/h4428.md) [šulḥān](../../strongs/h/h7979.md); and was [pissēaḥ](../../strongs/h/h6455.md) on both his [regel](../../strongs/h/h7272.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 8](2samuel_8.md) - [2Samuel 10](2samuel_10.md)