# [2Samuel 6](https://www.blueletterbible.org/kjv/2samuel/6)

<a name="2samuel_6_1"></a>2Samuel 6:1

Again, [Dāviḏ](../../strongs/h/h1732.md) ['āsap̄](../../strongs/h/h622.md) [yāsap̄](../../strongs/h/h3254.md) all the [bāḥar](../../strongs/h/h977.md) of [Yisra'el](../../strongs/h/h3478.md), thirty thousand.

<a name="2samuel_6_2"></a>2Samuel 6:2

And [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) with all the ['am](../../strongs/h/h5971.md) that were with him from [BaʿĂlê Yᵊhûḏâ](../../strongs/h/h1184.md), to [ʿālâ](../../strongs/h/h5927.md) from thence the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), whose [shem](../../strongs/h/h8034.md) is [qara'](../../strongs/h/h7121.md) by the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) that [yashab](../../strongs/h/h3427.md) between the [kĕruwb](../../strongs/h/h3742.md).

<a name="2samuel_6_3"></a>2Samuel 6:3

And they [rāḵaḇ](../../strongs/h/h7392.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) upon a [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḡālâ](../../strongs/h/h5699.md), and [nasa'](../../strongs/h/h5375.md) it out of the [bayith](../../strongs/h/h1004.md) of ['Ăḇînāḏāḇ](../../strongs/h/h41.md) that was in [giḇʿâ](../../strongs/h/h1390.md): and [ʿUzzā'](../../strongs/h/h5798.md) and ['Aḥyô](../../strongs/h/h283.md), the [ben](../../strongs/h/h1121.md) of ['Ăḇînāḏāḇ](../../strongs/h/h41.md), [nāhaḡ](../../strongs/h/h5090.md) the [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḡālâ](../../strongs/h/h5699.md).

<a name="2samuel_6_4"></a>2Samuel 6:4

And they [nasa'](../../strongs/h/h5375.md) it out of the [bayith](../../strongs/h/h1004.md) of ['Ăḇînāḏāḇ](../../strongs/h/h41.md) which was at [giḇʿâ](../../strongs/h/h1390.md), accompanying the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md): and ['Aḥyô](../../strongs/h/h283.md) [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md).

<a name="2samuel_6_5"></a>2Samuel 6:5

And [Dāviḏ](../../strongs/h/h1732.md) and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [śāḥaq](../../strongs/h/h7832.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) on [bᵊrôš](../../strongs/h/h1265.md) ['ets](../../strongs/h/h6086.md), even on [kinnôr](../../strongs/h/h3658.md), and on [neḇel](../../strongs/h/h5035.md), and on [tōp̄](../../strongs/h/h8596.md), and on [manʿanʿîm](../../strongs/h/h4517.md), and on [ṣᵊlāṣal](../../strongs/h/h6767.md).

<a name="2samuel_6_6"></a>2Samuel 6:6

And when they [bow'](../../strongs/h/h935.md) to [Nāḵôn](../../strongs/h/h5225.md) [gōren](../../strongs/h/h1637.md), [ʿUzzā'](../../strongs/h/h5798.md) [shalach](../../strongs/h/h7971.md) to the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md), and ['āḥaz](../../strongs/h/h270.md) of it; for the [bāqār](../../strongs/h/h1241.md) [šāmaṭ](../../strongs/h/h8058.md) it.

<a name="2samuel_6_7"></a>2Samuel 6:7

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [ʿUzzā'](../../strongs/h/h5798.md); and ['Elohiym](../../strongs/h/h430.md) [nakah](../../strongs/h/h5221.md) him there for his [šal](../../strongs/h/h7944.md); and there he [muwth](../../strongs/h/h4191.md) by the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2samuel_6_8"></a>2Samuel 6:8

And [Dāviḏ](../../strongs/h/h1732.md) was [ḥārâ](../../strongs/h/h2734.md), because [Yĕhovah](../../strongs/h/h3068.md) had [pāraṣ](../../strongs/h/h6555.md) a [pereṣ](../../strongs/h/h6556.md) upon [ʿUzzā'](../../strongs/h/h5798.md): and he [qara'](../../strongs/h/h7121.md) the name of the [maqowm](../../strongs/h/h4725.md) [Pereṣ](../../strongs/h/h6560.md) to this [yowm](../../strongs/h/h3117.md).

<a name="2samuel_6_9"></a>2Samuel 6:9

And [Dāviḏ](../../strongs/h/h1732.md) was [yare'](../../strongs/h/h3372.md) of [Yĕhovah](../../strongs/h/h3068.md) that [yowm](../../strongs/h/h3117.md), and ['āmar](../../strongs/h/h559.md), How shall the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) to me?

<a name="2samuel_6_10"></a>2Samuel 6:10

So [Dāviḏ](../../strongs/h/h1732.md) ['āḇâ](../../strongs/h/h14.md) not [cuwr](../../strongs/h/h5493.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) unto him into the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): but [Dāviḏ](../../strongs/h/h1732.md) [natah](../../strongs/h/h5186.md) it into the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) the [Gitî](../../strongs/h/h1663.md).

<a name="2samuel_6_11"></a>2Samuel 6:11

And the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) the [Gitî](../../strongs/h/h1663.md) three [ḥōḏeš](../../strongs/h/h2320.md): and [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and all his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_6_12"></a>2Samuel 6:12

And it was [nāḡaḏ](../../strongs/h/h5046.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and all that pertaineth unto him, because of the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md). So [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) and [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) from the [bayith](../../strongs/h/h1004.md) of [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md) into the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) with [simchah](../../strongs/h/h8057.md).

<a name="2samuel_6_13"></a>2Samuel 6:13

And it was so, that when they that [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) had [ṣāʿaḏ](../../strongs/h/h6805.md) six [ṣaʿaḏ](../../strongs/h/h6806.md), he [zabach](../../strongs/h/h2076.md) [showr](../../strongs/h/h7794.md) and [mᵊrî'](../../strongs/h/h4806.md).

<a name="2samuel_6_14"></a>2Samuel 6:14

And [Dāviḏ](../../strongs/h/h1732.md) [kārar](../../strongs/h/h3769.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) with all his ['oz](../../strongs/h/h5797.md); and [Dāviḏ](../../strongs/h/h1732.md) was [ḥāḡar](../../strongs/h/h2296.md) with a [baḏ](../../strongs/h/h906.md) ['ēp̄ôḏ](../../strongs/h/h646.md).

<a name="2samuel_6_15"></a>2Samuel 6:15

So [Dāviḏ](../../strongs/h/h1732.md) and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) with [tᵊrûʿâ](../../strongs/h/h8643.md), and with the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md).

<a name="2samuel_6_16"></a>2Samuel 6:16

And as the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), [Mîḵāl](../../strongs/h/h4324.md) [Šā'ûl](../../strongs/h/h7586.md) [bath](../../strongs/h/h1323.md) [šāqap̄](../../strongs/h/h8259.md) through a [ḥallôn](../../strongs/h/h2474.md), and [ra'ah](../../strongs/h/h7200.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [pāzaz](../../strongs/h/h6339.md) and [kārar](../../strongs/h/h3769.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and she [bazah](../../strongs/h/h959.md) him in her [leb](../../strongs/h/h3820.md).

<a name="2samuel_6_17"></a>2Samuel 6:17

And they [bow'](../../strongs/h/h935.md) in the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yāṣaḡ](../../strongs/h/h3322.md) it in his [maqowm](../../strongs/h/h4725.md), in the [tavek](../../strongs/h/h8432.md) of the ['ohel](../../strongs/h/h168.md) that [Dāviḏ](../../strongs/h/h1732.md) had [natah](../../strongs/h/h5186.md) for it: and [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2samuel_6_18"></a>2Samuel 6:18

And as soon as [Dāviḏ](../../strongs/h/h1732.md) had made a [kalah](../../strongs/h/h3615.md) of [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md), he [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="2samuel_6_19"></a>2Samuel 6:19

And he [chalaq](../../strongs/h/h2505.md) among all the ['am](../../strongs/h/h5971.md), even among the whole [hāmôn](../../strongs/h/h1995.md) of [Yisra'el](../../strongs/h/h3478.md), as well to the ['ishshah](../../strongs/h/h802.md) as ['iysh](../../strongs/h/h376.md), to every ['iysh](../../strongs/h/h376.md) a [ḥallâ](../../strongs/h/h2471.md) of [lechem](../../strongs/h/h3899.md), and an ['ešpār](../../strongs/h/h829.md), and a ['ăšîšâ](../../strongs/h/h809.md). So all the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_6_20"></a>2Samuel 6:20

Then [Dāviḏ](../../strongs/h/h1732.md) [shuwb](../../strongs/h/h7725.md) to [barak](../../strongs/h/h1288.md) his [bayith](../../strongs/h/h1004.md). And [Mîḵāl](../../strongs/h/h4324.md) the [bath](../../strongs/h/h1323.md) of [Šā'ûl](../../strongs/h/h7586.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md), How [kabad](../../strongs/h/h3513.md) was the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) to [yowm](../../strongs/h/h3117.md), who [gālâ](../../strongs/h/h1540.md) himself to [yowm](../../strongs/h/h3117.md) in the ['ayin](../../strongs/h/h5869.md) of the ['amah](../../strongs/h/h519.md) of his ['ebed](../../strongs/h/h5650.md), as one of the vain [reyq](../../strongs/h/h7386.md) [gālâ](../../strongs/h/h1540.md) [gālâ](../../strongs/h/h1540.md) himself!

<a name="2samuel_6_21"></a>2Samuel 6:21

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Mîḵāl](../../strongs/h/h4324.md), It was [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), which [bāḥar](../../strongs/h/h977.md) me before thy ['ab](../../strongs/h/h1.md), and before all his [bayith](../../strongs/h/h1004.md), to [tsavah](../../strongs/h/h6680.md) me [nāḡîḏ](../../strongs/h/h5057.md) over the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md), over [Yisra'el](../../strongs/h/h3478.md): therefore will I [śāḥaq](../../strongs/h/h7832.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2samuel_6_22"></a>2Samuel 6:22

And I will yet be more [qālal](../../strongs/h/h7043.md) than thus, and will be [šāp̄āl](../../strongs/h/h8217.md) in mine own ['ayin](../../strongs/h/h5869.md): and of the ['amah](../../strongs/h/h519.md) which thou hast ['āmar](../../strongs/h/h559.md) of, of them shall I be had in [kabad](../../strongs/h/h3513.md).

<a name="2samuel_6_23"></a>2Samuel 6:23

Therefore [Mîḵāl](../../strongs/h/h4324.md) the [bath](../../strongs/h/h1323.md) of [Šā'ûl](../../strongs/h/h7586.md) had no [vālāḏ](../../strongs/h/h2056.md) [yeleḏ](../../strongs/h/h3206.md) unto the [yowm](../../strongs/h/h3117.md) of her [maveth](../../strongs/h/h4194.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 5](2samuel_5.md) - [2Samuel 7](2samuel_7.md)