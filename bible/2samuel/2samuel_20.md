# [2Samuel 20](https://www.blueletterbible.org/kjv/2samuel/20)

<a name="2samuel_20_1"></a>2Samuel 20:1

And there [qārā'](../../strongs/h/h7122.md) to be there an ['iysh](../../strongs/h/h376.md) of [Beliya'al](../../strongs/h/h1100.md), whose [shem](../../strongs/h/h8034.md) was [Šeḇaʿ](../../strongs/h/h7652.md), the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md): and he [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md), and ['āmar](../../strongs/h/h559.md), We have no [cheleq](../../strongs/h/h2506.md) in [Dāviḏ](../../strongs/h/h1732.md), neither have we [nachalah](../../strongs/h/h5159.md) in the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md): ['iysh](../../strongs/h/h376.md) ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md), O [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_20_2"></a>2Samuel 20:2

So every ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) from ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md), and ['aḥar](../../strongs/h/h310.md) [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md): but the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) [dāḇaq](../../strongs/h/h1692.md) unto their [melek](../../strongs/h/h4428.md), from [Yardēn](../../strongs/h/h3383.md) even to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_20_3"></a>2Samuel 20:3

And [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to his [bayith](../../strongs/h/h1004.md) at [Yĕruwshalaim](../../strongs/h/h3389.md); and the [melek](../../strongs/h/h4428.md) [laqach](../../strongs/h/h3947.md) the ten ['ishshah](../../strongs/h/h802.md) his [pîleḡeš](../../strongs/h/h6370.md), whom he had [yānaḥ](../../strongs/h/h3240.md) to [shamar](../../strongs/h/h8104.md) the [bayith](../../strongs/h/h1004.md), and [nathan](../../strongs/h/h5414.md) them in [mišmereṯ](../../strongs/h/h4931.md), and [kûl](../../strongs/h/h3557.md) them, but went not [bow'](../../strongs/h/h935.md) unto them. So they were [tsarar](../../strongs/h/h6887.md) unto the [yowm](../../strongs/h/h3117.md) of their [muwth](../../strongs/h/h4191.md), [ḥayyûṯ](../../strongs/h/h2424.md) in ['almānûṯ](../../strongs/h/h491.md).

<a name="2samuel_20_4"></a>2Samuel 20:4

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) to [ʿĂmāśā'](../../strongs/h/h6021.md), [zāʿaq](../../strongs/h/h2199.md) me the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) within three [yowm](../../strongs/h/h3117.md), and be thou here ['amad](../../strongs/h/h5975.md).

<a name="2samuel_20_5"></a>2Samuel 20:5

So [ʿĂmāśā'](../../strongs/h/h6021.md) [yālaḵ](../../strongs/h/h3212.md) to [zāʿaq](../../strongs/h/h2199.md) [Yehuwdah](../../strongs/h/h3063.md): but he ['āḥar](../../strongs/h/h309.md) [yāḥar](../../strongs/h/h3186.md) than the set [môʿēḏ](../../strongs/h/h4150.md) which he had [yāʿaḏ](../../strongs/h/h3259.md) him.

<a name="2samuel_20_6"></a>2Samuel 20:6

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ăḇîšay](../../strongs/h/h52.md), Now shall [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md) do us more [yāraʿ](../../strongs/h/h3415.md) than did ['Ăbyšālôm](../../strongs/h/h53.md): [laqach](../../strongs/h/h3947.md) thou thy ['adown](../../strongs/h/h113.md) ['ebed](../../strongs/h/h5650.md), and [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) him, lest he [māṣā'](../../strongs/h/h4672.md) him [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md), and [natsal](../../strongs/h/h5337.md) ['ayin](../../strongs/h/h5869.md).

<a name="2samuel_20_7"></a>2Samuel 20:7

And there [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) him [Yô'āḇ](../../strongs/h/h3097.md) ['enowsh](../../strongs/h/h582.md), and the [kᵊrēṯî](../../strongs/h/h3774.md), and the [pᵊlēṯî](../../strongs/h/h6432.md), and all the [gibôr](../../strongs/h/h1368.md): and they [yāṣā'](../../strongs/h/h3318.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), to [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md).

<a name="2samuel_20_8"></a>2Samuel 20:8

When they were at the [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) which is in [Giḇʿôn](../../strongs/h/h1391.md), [ʿĂmāśā'](../../strongs/h/h6021.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) them. And [Yô'āḇ](../../strongs/h/h3097.md) [maḏ](../../strongs/h/h4055.md) that he had put [lᵊḇûš](../../strongs/h/h3830.md) was [ḥāḡar](../../strongs/h/h2296.md) unto him, and upon it a [ḥăḡôr](../../strongs/h/h2289.md) with a [chereb](../../strongs/h/h2719.md) [ṣāmaḏ](../../strongs/h/h6775.md) upon his [māṯnayim](../../strongs/h/h4975.md) in the [taʿar](../../strongs/h/h8593.md) thereof; and as he [yāṣā'](../../strongs/h/h3318.md) it [naphal](../../strongs/h/h5307.md).

<a name="2samuel_20_9"></a>2Samuel 20:9

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md) to [ʿĂmāśā'](../../strongs/h/h6021.md), Art thou in [shalowm](../../strongs/h/h7965.md), my ['ach](../../strongs/h/h251.md)? And [Yô'āḇ](../../strongs/h/h3097.md) ['āḥaz](../../strongs/h/h270.md) [ʿĂmāśā'](../../strongs/h/h6021.md) by the [zāqān](../../strongs/h/h2206.md) with the [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md) to [nashaq](../../strongs/h/h5401.md) him.

<a name="2samuel_20_10"></a>2Samuel 20:10

But [ʿĂmāśā'](../../strongs/h/h6021.md) took no [shamar](../../strongs/h/h8104.md) to the [chereb](../../strongs/h/h2719.md) that was in [Yô'āḇ](../../strongs/h/h3097.md) [yad](../../strongs/h/h3027.md): so he [nakah](../../strongs/h/h5221.md) him therewith in the [ḥōmeš](../../strongs/h/h2570.md) rib, and [šāp̄aḵ](../../strongs/h/h8210.md) his [me'ah](../../strongs/h/h4578.md) to the ['erets](../../strongs/h/h776.md), and struck him not [šānâ](../../strongs/h/h8138.md); and he [muwth](../../strongs/h/h4191.md). So [Yô'āḇ](../../strongs/h/h3097.md) and ['Ăḇîšay](../../strongs/h/h52.md) his ['ach](../../strongs/h/h251.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md).

<a name="2samuel_20_11"></a>2Samuel 20:11

And ['iysh](../../strongs/h/h376.md) of [Yô'āḇ](../../strongs/h/h3097.md) [naʿar](../../strongs/h/h5288.md) ['amad](../../strongs/h/h5975.md) by him, and ['āmar](../../strongs/h/h559.md), He that [ḥāp̄ēṣ](../../strongs/h/h2654.md) [Yô'āḇ](../../strongs/h/h3097.md), and he that is for [Dāviḏ](../../strongs/h/h1732.md), ['aḥar](../../strongs/h/h310.md) [Yô'āḇ](../../strongs/h/h3097.md).

<a name="2samuel_20_12"></a>2Samuel 20:12

And [ʿĂmāśā'](../../strongs/h/h6021.md) [gālal](../../strongs/h/h1556.md) in [dam](../../strongs/h/h1818.md) in the [tavek](../../strongs/h/h8432.md) of the [mĕcillah](../../strongs/h/h4546.md). And when the ['iysh](../../strongs/h/h376.md) [ra'ah](../../strongs/h/h7200.md) that all the ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md), he [cabab](../../strongs/h/h5437.md) [ʿĂmāśā'](../../strongs/h/h6021.md) out of the [mĕcillah](../../strongs/h/h4546.md) into the [sadeh](../../strongs/h/h7704.md), and [shalak](../../strongs/h/h7993.md) a [beḡeḏ](../../strongs/h/h899.md) upon him, when he [ra'ah](../../strongs/h/h7200.md) that every one that [bow'](../../strongs/h/h935.md) by him ['amad](../../strongs/h/h5975.md).

<a name="2samuel_20_13"></a>2Samuel 20:13

When he was [yāḡâ](../../strongs/h/h3014.md) out of the [mĕcillah](../../strongs/h/h4546.md), all the ['iysh](../../strongs/h/h376.md) ['abar](../../strongs/h/h5674.md) ['aḥar](../../strongs/h/h310.md) [Yô'āḇ](../../strongs/h/h3097.md), to [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md).

<a name="2samuel_20_14"></a>2Samuel 20:14

And he ['abar](../../strongs/h/h5674.md) through all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) unto ['Āḇēl](../../strongs/h/h59.md), and to [Bêṯ MaʿĂḵâ](../../strongs/h/h1038.md), and all the [bērîm](../../strongs/h/h1276.md): and they were [qāhal](../../strongs/h/h6950.md) [qālah](../../strongs/h/h7035.md) , and [bow'](../../strongs/h/h935.md) also ['aḥar](../../strongs/h/h310.md) him.

<a name="2samuel_20_15"></a>2Samuel 20:15

And they [bow'](../../strongs/h/h935.md) and [ṣûr](../../strongs/h/h6696.md) him in ['Āḇēl](../../strongs/h/h59.md) of [Bêṯ MaʿĂḵâ](../../strongs/h/h1038.md), and they [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md) against the [ʿîr](../../strongs/h/h5892.md), and it ['amad](../../strongs/h/h5975.md) in the [cheyl](../../strongs/h/h2426.md): and all the ['am](../../strongs/h/h5971.md) that were with [Yô'āḇ](../../strongs/h/h3097.md) [shachath](../../strongs/h/h7843.md) the [ḥômâ](../../strongs/h/h2346.md), to [naphal](../../strongs/h/h5307.md) it.

<a name="2samuel_20_16"></a>2Samuel 20:16

Then [qara'](../../strongs/h/h7121.md) a [ḥāḵām](../../strongs/h/h2450.md) ['ishshah](../../strongs/h/h802.md) out of the [ʿîr](../../strongs/h/h5892.md), [shama'](../../strongs/h/h8085.md), [shama'](../../strongs/h/h8085.md); ['āmar](../../strongs/h/h559.md), I pray you, unto [Yô'āḇ](../../strongs/h/h3097.md), [qāraḇ](../../strongs/h/h7126.md) hither, that I may [dabar](../../strongs/h/h1696.md) with thee.

<a name="2samuel_20_17"></a>2Samuel 20:17

And when he was [qāraḇ](../../strongs/h/h7126.md) unto her, the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), Art thou [Yô'āḇ](../../strongs/h/h3097.md)? And he ['āmar](../../strongs/h/h559.md), I am he. Then she ['āmar](../../strongs/h/h559.md) unto him, [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of thine ['amah](../../strongs/h/h519.md). And he ['āmar](../../strongs/h/h559.md), I do [shama'](../../strongs/h/h8085.md).

<a name="2samuel_20_18"></a>2Samuel 20:18

Then she ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), They were [dabar](../../strongs/h/h1696.md) to [dabar](../../strongs/h/h1696.md) in [ri'šôn](../../strongs/h/h7223.md), ['āmar](../../strongs/h/h559.md), They shall [sha'al](../../strongs/h/h7592.md) [sha'al](../../strongs/h/h7592.md) at ['Āḇēl](../../strongs/h/h59.md): and so they [tamam](../../strongs/h/h8552.md).

<a name="2samuel_20_19"></a>2Samuel 20:19

I am [shalam](../../strongs/h/h7999.md) and ['aman](../../strongs/h/h539.md) in [Yisra'el](../../strongs/h/h3478.md): thou [bāqaš](../../strongs/h/h1245.md) to [muwth](../../strongs/h/h4191.md) a [ʿîr](../../strongs/h/h5892.md) and an ['em](../../strongs/h/h517.md) in [Yisra'el](../../strongs/h/h3478.md): why wilt thou [bālaʿ](../../strongs/h/h1104.md) the [nachalah](../../strongs/h/h5159.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="2samuel_20_20"></a>2Samuel 20:20

And [Yô'āḇ](../../strongs/h/h3097.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), [ḥālîlâ](../../strongs/h/h2486.md), [ḥālîlâ](../../strongs/h/h2486.md) from me, that I should [bālaʿ](../../strongs/h/h1104.md) or [shachath](../../strongs/h/h7843.md).

<a name="2samuel_20_21"></a>2Samuel 20:21

The [dabar](../../strongs/h/h1697.md) is not so: but an ['iysh](../../strongs/h/h376.md) of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md) by [shem](../../strongs/h/h8034.md), hath [nasa'](../../strongs/h/h5375.md) his [yad](../../strongs/h/h3027.md) against the [melek](../../strongs/h/h4428.md), even against [Dāviḏ](../../strongs/h/h1732.md): [nathan](../../strongs/h/h5414.md) him only, and I will [yālaḵ](../../strongs/h/h3212.md) from the [ʿîr](../../strongs/h/h5892.md). And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto [Yô'āḇ](../../strongs/h/h3097.md), Behold, his [ro'sh](../../strongs/h/h7218.md) shall be [shalak](../../strongs/h/h7993.md) to thee over the [ḥômâ](../../strongs/h/h2346.md).

<a name="2samuel_20_22"></a>2Samuel 20:22

Then the ['ishshah](../../strongs/h/h802.md) [bow'](../../strongs/h/h935.md) unto all the ['am](../../strongs/h/h5971.md) in her [ḥāḵmâ](../../strongs/h/h2451.md). And they [karath](../../strongs/h/h3772.md) the [ro'sh](../../strongs/h/h7218.md) of [Šeḇaʿ](../../strongs/h/h7652.md) the [ben](../../strongs/h/h1121.md) of [Biḵrî](../../strongs/h/h1075.md), and [shalak](../../strongs/h/h7993.md) it out to [Yô'āḇ](../../strongs/h/h3097.md). And he [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md), and they [puwts](../../strongs/h/h6327.md) from the [ʿîr](../../strongs/h/h5892.md), every ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md). And [Yô'āḇ](../../strongs/h/h3097.md) [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [melek](../../strongs/h/h4428.md).

<a name="2samuel_20_23"></a>2Samuel 20:23

Now [Yô'āḇ](../../strongs/h/h3097.md) was over all the [tsaba'](../../strongs/h/h6635.md) of [Yisra'el](../../strongs/h/h3478.md): and [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) was over the [kᵊrēṯî](../../strongs/h/h3774.md) [kārî](../../strongs/h/h3746.md) and over the [pᵊlēṯî](../../strongs/h/h6432.md):

<a name="2samuel_20_24"></a>2Samuel 20:24

And ['Ăḏōrām](../../strongs/h/h151.md) was over the [mas](../../strongs/h/h4522.md): and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîlûḏ](../../strongs/h/h286.md) was [zakar](../../strongs/h/h2142.md):

<a name="2samuel_20_25"></a>2Samuel 20:25

And [Šᵊyā'](../../strongs/h/h7864.md) [Šᵊvā'](../../strongs/h/h7724.md) was [sāp̄ar](../../strongs/h/h5608.md): and [Ṣāḏôq](../../strongs/h/h6659.md) and ['Eḇyāṯār](../../strongs/h/h54.md) were the [kōhēn](../../strongs/h/h3548.md):

<a name="2samuel_20_26"></a>2Samuel 20:26

And [ʿÎrā'](../../strongs/h/h5896.md) also the [yā'irî](../../strongs/h/h2972.md) was a [kōhēn](../../strongs/h/h3548.md) about [Dāviḏ](../../strongs/h/h1732.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 19](2samuel_19.md) - [2Samuel 21](2samuel_21.md)