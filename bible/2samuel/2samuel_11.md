# [2Samuel 11](https://www.blueletterbible.org/kjv/2samuel/11)

<a name="2samuel_11_1"></a>2Samuel 11:1

And it came to pass, after the [šānâ](../../strongs/h/h8141.md) was [tᵊšûḇâ](../../strongs/h/h8666.md), at the [ʿēṯ](../../strongs/h/h6256.md) when [melek](../../strongs/h/h4428.md) [mal'ak](../../strongs/h/h4397.md) [yāṣā'](../../strongs/h/h3318.md), that [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [Yô'āḇ](../../strongs/h/h3097.md), and his ['ebed](../../strongs/h/h5650.md) with him, and all [Yisra'el](../../strongs/h/h3478.md); and they [shachath](../../strongs/h/h7843.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [ṣûr](../../strongs/h/h6696.md) [Rabâ](../../strongs/h/h7237.md). But [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_11_2"></a>2Samuel 11:2

And it came to pass in an ['ereb](../../strongs/h/h6153.md) [ʿēṯ](../../strongs/h/h6256.md), that [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md) from off his [miškāḇ](../../strongs/h/h4904.md), and [halak](../../strongs/h/h1980.md) upon the [gāḡ](../../strongs/h/h1406.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md): and from the [gāḡ](../../strongs/h/h1406.md) he [ra'ah](../../strongs/h/h7200.md) an ['ishshah](../../strongs/h/h802.md) [rāḥaṣ](../../strongs/h/h7364.md) herself; and the ['ishshah](../../strongs/h/h802.md) was [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) to [mar'ê](../../strongs/h/h4758.md).

<a name="2samuel_11_3"></a>2Samuel 11:3

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) and [darash](../../strongs/h/h1875.md) after the ['ishshah](../../strongs/h/h802.md). And one ['āmar](../../strongs/h/h559.md), Is not this [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md), the [bath](../../strongs/h/h1323.md) of ['ĔlîʿĀm](../../strongs/h/h463.md), the ['ishshah](../../strongs/h/h802.md) of ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md)?

<a name="2samuel_11_4"></a>2Samuel 11:4

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md), and [laqach](../../strongs/h/h3947.md) her; and she [bow'](../../strongs/h/h935.md) unto him, and he [shakab](../../strongs/h/h7901.md) with her; for she was [qadash](../../strongs/h/h6942.md) from her [ṭām'â](../../strongs/h/h2932.md): and she [shuwb](../../strongs/h/h7725.md) unto her [bayith](../../strongs/h/h1004.md).

<a name="2samuel_11_5"></a>2Samuel 11:5

And the ['ishshah](../../strongs/h/h802.md) [harah](../../strongs/h/h2029.md), and [shalach](../../strongs/h/h7971.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md), I am with [hārê](../../strongs/h/h2030.md).

<a name="2samuel_11_6"></a>2Samuel 11:6

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) to [Yô'āḇ](../../strongs/h/h3097.md), saying, [shalach](../../strongs/h/h7971.md) me ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md). And [Yô'āḇ](../../strongs/h/h3097.md) [shalach](../../strongs/h/h7971.md) ['Ûrîyâ](../../strongs/h/h223.md) to [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_11_7"></a>2Samuel 11:7

And when ['Ûrîyâ](../../strongs/h/h223.md) was [bow'](../../strongs/h/h935.md) unto him, [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of him how [Yô'āḇ](../../strongs/h/h3097.md) [shalowm](../../strongs/h/h7965.md), and how the ['am](../../strongs/h/h5971.md) [shalowm](../../strongs/h/h7965.md), and how the [milḥāmâ](../../strongs/h/h4421.md) [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_11_8"></a>2Samuel 11:8

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ûrîyâ](../../strongs/h/h223.md), Go [yarad](../../strongs/h/h3381.md) to thy [bayith](../../strongs/h/h1004.md), and [rāḥaṣ](../../strongs/h/h7364.md) thy [regel](../../strongs/h/h7272.md). And ['Ûrîyâ](../../strongs/h/h223.md) departed [yāṣā'](../../strongs/h/h3318.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and there ['aḥar](../../strongs/h/h310.md) him a [maśśᵊ'ēṯ](../../strongs/h/h4864.md) of meat from the [melek](../../strongs/h/h4428.md).

<a name="2samuel_11_9"></a>2Samuel 11:9

But ['Ûrîyâ](../../strongs/h/h223.md) [shakab](../../strongs/h/h7901.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) with all the ['ebed](../../strongs/h/h5650.md) of his ['adown](../../strongs/h/h113.md), and went not [yarad](../../strongs/h/h3381.md) to his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_11_10"></a>2Samuel 11:10

And when they had [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), ['Ûrîyâ](../../strongs/h/h223.md) [yarad](../../strongs/h/h3381.md) not unto his [bayith](../../strongs/h/h1004.md), [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto ['Ûrîyâ](../../strongs/h/h223.md), [bow'](../../strongs/h/h935.md) thou not from thy [derek](../../strongs/h/h1870.md)? why then didst thou not [yarad](../../strongs/h/h3381.md) unto thine [bayith](../../strongs/h/h1004.md)?

<a name="2samuel_11_11"></a>2Samuel 11:11

And ['Ûrîyâ](../../strongs/h/h223.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), The ['ārôn](../../strongs/h/h727.md), and [Yisra'el](../../strongs/h/h3478.md), and [Yehuwdah](../../strongs/h/h3063.md), [yashab](../../strongs/h/h3427.md) in [cukkah](../../strongs/h/h5521.md); and my ['adown](../../strongs/h/h113.md) [Yô'āḇ](../../strongs/h/h3097.md), and the ['ebed](../../strongs/h/h5650.md) of my ['adown](../../strongs/h/h113.md), are [ḥānâ](../../strongs/h/h2583.md) in the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md); shall I then [bow'](../../strongs/h/h935.md) into mine [bayith](../../strongs/h/h1004.md), to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md), and to [shakab](../../strongs/h/h7901.md) with my ['ishshah](../../strongs/h/h802.md)? as thou [chay](../../strongs/h/h2416.md), and as thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), I will not ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="2samuel_11_12"></a>2Samuel 11:12

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ûrîyâ](../../strongs/h/h223.md), [yashab](../../strongs/h/h3427.md) here to [yowm](../../strongs/h/h3117.md) also, and [māḥār](../../strongs/h/h4279.md) I will let thee [shalach](../../strongs/h/h7971.md). So ['Ûrîyâ](../../strongs/h/h223.md) [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) that [yowm](../../strongs/h/h3117.md), and the [māḥŏrāṯ](../../strongs/h/h4283.md).

<a name="2samuel_11_13"></a>2Samuel 11:13

And when [Dāviḏ](../../strongs/h/h1732.md) had [qara'](../../strongs/h/h7121.md) him, he did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [paniym](../../strongs/h/h6440.md) him; and he made him [šāḵar](../../strongs/h/h7937.md): and at ['ereb](../../strongs/h/h6153.md) he [yāṣā'](../../strongs/h/h3318.md) to [shakab](../../strongs/h/h7901.md) on his [miškāḇ](../../strongs/h/h4904.md) with the ['ebed](../../strongs/h/h5650.md) of his ['adown](../../strongs/h/h113.md), but went not [yarad](../../strongs/h/h3381.md) to his [bayith](../../strongs/h/h1004.md).

<a name="2samuel_11_14"></a>2Samuel 11:14

And it came to pass in the [boqer](../../strongs/h/h1242.md), that [Dāviḏ](../../strongs/h/h1732.md) [kāṯaḇ](../../strongs/h/h3789.md) a [sēp̄er](../../strongs/h/h5612.md) to [Yô'āḇ](../../strongs/h/h3097.md), and [shalach](../../strongs/h/h7971.md) it by the [yad](../../strongs/h/h3027.md) of ['Ûrîyâ](../../strongs/h/h223.md).

<a name="2samuel_11_15"></a>2Samuel 11:15

And he [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md), ['āmar](../../strongs/h/h559.md), [yāhaḇ](../../strongs/h/h3051.md) ye ['Ûrîyâ](../../strongs/h/h223.md) in the [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md) of the [ḥāzāq](../../strongs/h/h2389.md) [milḥāmâ](../../strongs/h/h4421.md), and [shuwb](../../strongs/h/h7725.md) ye from ['aḥar](../../strongs/h/h310.md), that he may be [nakah](../../strongs/h/h5221.md), and [muwth](../../strongs/h/h4191.md).

<a name="2samuel_11_16"></a>2Samuel 11:16

And it came to pass, when [Yô'āḇ](../../strongs/h/h3097.md) [shamar](../../strongs/h/h8104.md) the [ʿîr](../../strongs/h/h5892.md), that he [nathan](../../strongs/h/h5414.md) ['Ûrîyâ](../../strongs/h/h223.md) unto a [maqowm](../../strongs/h/h4725.md) where he [yada'](../../strongs/h/h3045.md) that [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md) were.

<a name="2samuel_11_17"></a>2Samuel 11:17

And the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) [yāṣā'](../../strongs/h/h3318.md), and [lāḥam](../../strongs/h/h3898.md) with [Yô'āḇ](../../strongs/h/h3097.md): and there [naphal](../../strongs/h/h5307.md) some of the ['am](../../strongs/h/h5971.md) of the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md); and ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md) [muwth](../../strongs/h/h4191.md) also.

<a name="2samuel_11_18"></a>2Samuel 11:18

Then [Yô'āḇ](../../strongs/h/h3097.md) [shalach](../../strongs/h/h7971.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) all the [dabar](../../strongs/h/h1697.md) concerning the [milḥāmâ](../../strongs/h/h4421.md);

<a name="2samuel_11_19"></a>2Samuel 11:19

And [tsavah](../../strongs/h/h6680.md) the [mal'ak](../../strongs/h/h4397.md), ['āmar](../../strongs/h/h559.md), When thou hast made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) the [dabar](../../strongs/h/h1697.md) of the [milḥāmâ](../../strongs/h/h4421.md) unto the [melek](../../strongs/h/h4428.md),

<a name="2samuel_11_20"></a>2Samuel 11:20

And if so be that the [melek](../../strongs/h/h4428.md) [chemah](../../strongs/h/h2534.md) [ʿālâ](../../strongs/h/h5927.md), and he ['āmar](../../strongs/h/h559.md) unto thee, Wherefore approached ye so [nāḡaš](../../strongs/h/h5066.md) unto the [ʿîr](../../strongs/h/h5892.md) when ye did [lāḥam](../../strongs/h/h3898.md)? [yada'](../../strongs/h/h3045.md) ye not that they would [yārâ](../../strongs/h/h3384.md) from the [ḥômâ](../../strongs/h/h2346.md)?

<a name="2samuel_11_21"></a>2Samuel 11:21

Who [nakah](../../strongs/h/h5221.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrubešeṯ](../../strongs/h/h3380.md)? did not an ['ishshah](../../strongs/h/h802.md) [shalak](../../strongs/h/h7993.md) a [pelaḥ](../../strongs/h/h6400.md) of a [reḵeḇ](../../strongs/h/h7393.md) upon him from the [ḥômâ](../../strongs/h/h2346.md), that he [muwth](../../strongs/h/h4191.md) in [Tēḇēṣ](../../strongs/h/h8405.md)? why went ye [nāḡaš](../../strongs/h/h5066.md) the [ḥômâ](../../strongs/h/h2346.md)? then ['āmar](../../strongs/h/h559.md) thou, Thy ['ebed](../../strongs/h/h5650.md) ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md) is [muwth](../../strongs/h/h4191.md) also.

<a name="2samuel_11_22"></a>2Samuel 11:22

So the [mal'ak](../../strongs/h/h4397.md) [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) all that [Yô'āḇ](../../strongs/h/h3097.md) had [shalach](../../strongs/h/h7971.md) him for.

<a name="2samuel_11_23"></a>2Samuel 11:23

And the [mal'ak](../../strongs/h/h4397.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), Surely the ['enowsh](../../strongs/h/h582.md) [gabar](../../strongs/h/h1396.md) against us, and [yāṣā'](../../strongs/h/h3318.md) unto us into the [sadeh](../../strongs/h/h7704.md), and we were upon them even unto the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md).

<a name="2samuel_11_24"></a>2Samuel 11:24

And the [yārâ](../../strongs/h/h3384.md) [yārâ](../../strongs/h/h3384.md) from off the [ḥômâ](../../strongs/h/h2346.md) upon thy ['ebed](../../strongs/h/h5650.md); and some of the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md) be [muwth](../../strongs/h/h4191.md), and thy ['ebed](../../strongs/h/h5650.md) ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md) is [muwth](../../strongs/h/h4191.md) also.

<a name="2samuel_11_25"></a>2Samuel 11:25

Then [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) unto [Yô'āḇ](../../strongs/h/h3097.md), Let not this [dabar](../../strongs/h/h1697.md) [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) thee, for the [chereb](../../strongs/h/h2719.md) ['akal](../../strongs/h/h398.md) one as well as [zô](../../strongs/h/h2090.md): make thy [milḥāmâ](../../strongs/h/h4421.md) more [ḥāzaq](../../strongs/h/h2388.md) against the [ʿîr](../../strongs/h/h5892.md), and [harac](../../strongs/h/h2040.md) it: and [ḥāzaq](../../strongs/h/h2388.md) thou him.

<a name="2samuel_11_26"></a>2Samuel 11:26

And when the ['ishshah](../../strongs/h/h802.md) of ['Ûrîyâ](../../strongs/h/h223.md) [shama'](../../strongs/h/h8085.md) that ['Ûrîyâ](../../strongs/h/h223.md) her ['iysh](../../strongs/h/h376.md) was [muwth](../../strongs/h/h4191.md), she [sāp̄aḏ](../../strongs/h/h5594.md) for her [baʿal](../../strongs/h/h1167.md).

<a name="2samuel_11_27"></a>2Samuel 11:27

And when the ['ēḇel](../../strongs/h/h60.md) was ['abar](../../strongs/h/h5674.md), [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) and ['āsap̄](../../strongs/h/h622.md) her to his [bayith](../../strongs/h/h1004.md), and she became his ['ishshah](../../strongs/h/h802.md), and [yalad](../../strongs/h/h3205.md) him a [ben](../../strongs/h/h1121.md). But the [dabar](../../strongs/h/h1697.md) that [Dāviḏ](../../strongs/h/h1732.md) had ['asah](../../strongs/h/h6213.md) [yāraʿ](../../strongs/h/h3415.md) ['ayin](../../strongs/h/h5869.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 10](2samuel_10.md) - [2Samuel 12](2samuel_12.md)