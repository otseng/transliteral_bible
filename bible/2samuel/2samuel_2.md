# [2Samuel 2](https://www.blueletterbible.org/kjv/2samuel/2)

<a name="2samuel_2_1"></a>2Samuel 2:1

And it came to pass ['aḥar](../../strongs/h/h310.md), that [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Shall I [ʿālâ](../../strongs/h/h5927.md) into any of the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [ʿālâ](../../strongs/h/h5927.md). And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), Whither shall I [ʿālâ](../../strongs/h/h5927.md)? And he ['āmar](../../strongs/h/h559.md), Unto [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_2_2"></a>2Samuel 2:2

So [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) thither, and his two ['ishshah](../../strongs/h/h802.md) also, ['ĂḥînōʿAm](../../strongs/h/h293.md) the [Yizrᵊʿē'lîṯ](../../strongs/h/h3159.md), and ['Ăḇîḡayil](../../strongs/h/h26.md) [Nāḇāl](../../strongs/h/h5037.md) ['ishshah](../../strongs/h/h802.md) the [Karmᵊlî](../../strongs/h/h3761.md).

<a name="2samuel_2_3"></a>2Samuel 2:3

And his ['enowsh](../../strongs/h/h582.md) that were with him did [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md), every ['iysh](../../strongs/h/h376.md) with his [bayith](../../strongs/h/h1004.md): and they [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_2_4"></a>2Samuel 2:4

And the ['enowsh](../../strongs/h/h582.md) of [Yehuwdah](../../strongs/h/h3063.md) [bow'](../../strongs/h/h935.md), and there they [māšaḥ](../../strongs/h/h4886.md) [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) over the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md). And they [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), That the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) were they that [qāḇar](../../strongs/h/h6912.md) [Šā'ûl](../../strongs/h/h7586.md).

<a name="2samuel_2_5"></a>2Samuel 2:5

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto the ['enowsh](../../strongs/h/h582.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md), and ['āmar](../../strongs/h/h559.md) unto them, [barak](../../strongs/h/h1288.md) be ye of [Yĕhovah](../../strongs/h/h3068.md), that ye have ['asah](../../strongs/h/h6213.md) this [checed](../../strongs/h/h2617.md) unto your ['adown](../../strongs/h/h113.md), even unto [Šā'ûl](../../strongs/h/h7586.md), and have [qāḇar](../../strongs/h/h6912.md) him.

<a name="2samuel_2_6"></a>2Samuel 2:6

And now [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) unto you: and I also will ['asah](../../strongs/h/h6213.md) you this [towb](../../strongs/h/h2896.md), because ye have ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="2samuel_2_7"></a>2Samuel 2:7

Therefore now let your [yad](../../strongs/h/h3027.md) be [ḥāzaq](../../strongs/h/h2388.md), and be ye [ben](../../strongs/h/h1121.md) [ḥayil](../../strongs/h/h2428.md): for your ['adown](../../strongs/h/h113.md) [Šā'ûl](../../strongs/h/h7586.md) is [muwth](../../strongs/h/h4191.md), and also the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) have [māšaḥ](../../strongs/h/h4886.md) me [melek](../../strongs/h/h4428.md) over them.

<a name="2samuel_2_8"></a>2Samuel 2:8

But ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), [śar](../../strongs/h/h8269.md) of [Šā'ûl](../../strongs/h/h7586.md) [tsaba'](../../strongs/h/h6635.md), [laqach](../../strongs/h/h3947.md) ['Îš-Bšeṯ](../../strongs/h/h378.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md), and ['abar](../../strongs/h/h5674.md) him to [Maḥănayim](../../strongs/h/h4266.md);

<a name="2samuel_2_9"></a>2Samuel 2:9

And made him [mālaḵ](../../strongs/h/h4427.md) over [Gilʿāḏ](../../strongs/h/h1568.md), and over the ['aššûrî](../../strongs/h/h805.md), and over [YizrᵊʿE'L](../../strongs/h/h3157.md), and over ['Ep̄rayim](../../strongs/h/h669.md), and over [Binyāmîn](../../strongs/h/h1144.md), and over all [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_2_10"></a>2Samuel 2:10

['Îš-Bšeṯ](../../strongs/h/h378.md) [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md) was forty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md), and [mālaḵ](../../strongs/h/h4427.md) two [šānâ](../../strongs/h/h8141.md). But the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_2_11"></a>2Samuel 2:11

And the [mispār](../../strongs/h/h4557.md) [yowm](../../strongs/h/h3117.md) that [Dāviḏ](../../strongs/h/h1732.md) was [melek](../../strongs/h/h4428.md) in [Ḥeḇrôn](../../strongs/h/h2275.md) over the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) was seven [šānâ](../../strongs/h/h8141.md) and six [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2samuel_2_12"></a>2Samuel 2:12

And ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), and the ['ebed](../../strongs/h/h5650.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md), [yāṣā'](../../strongs/h/h3318.md) from [Maḥănayim](../../strongs/h/h4266.md) to [Giḇʿôn](../../strongs/h/h1391.md).

<a name="2samuel_2_13"></a>2Samuel 2:13

And [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), and the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md), [yāṣā'](../../strongs/h/h3318.md), and [pāḡaš](../../strongs/h/h6298.md) [yaḥaḏ](../../strongs/h/h3162.md) by the [bᵊrēḵâ](../../strongs/h/h1295.md) of [Giḇʿôn](../../strongs/h/h1391.md): and they [yashab](../../strongs/h/h3427.md), the one on the one side of the [bᵊrēḵâ](../../strongs/h/h1295.md), and the other on the other side of the [bᵊrēḵâ](../../strongs/h/h1295.md).

<a name="2samuel_2_14"></a>2Samuel 2:14

And ['Aḇnēr](../../strongs/h/h74.md) ['āmar](../../strongs/h/h559.md) to [Yô'āḇ](../../strongs/h/h3097.md), Let the [naʿar](../../strongs/h/h5288.md) now [quwm](../../strongs/h/h6965.md), and [śāḥaq](../../strongs/h/h7832.md) [paniym](../../strongs/h/h6440.md) us. And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md), Let them [quwm](../../strongs/h/h6965.md).

<a name="2samuel_2_15"></a>2Samuel 2:15

Then there [quwm](../../strongs/h/h6965.md) and ['abar](../../strongs/h/h5674.md) by [mispār](../../strongs/h/h4557.md) twelve  of [Binyāmîn](../../strongs/h/h1144.md), which pertained to ['Îš-Bšeṯ](../../strongs/h/h378.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md), and twelve  of the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_2_16"></a>2Samuel 2:16

And they [ḥāzaq](../../strongs/h/h2388.md) every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md) by the [ro'sh](../../strongs/h/h7218.md), and thrust his [chereb](../../strongs/h/h2719.md) in his [rea'](../../strongs/h/h7453.md) [ṣaḏ](../../strongs/h/h6654.md); so they [naphal](../../strongs/h/h5307.md) [yaḥaḏ](../../strongs/h/h3162.md): wherefore that [maqowm](../../strongs/h/h4725.md) was [qara'](../../strongs/h/h7121.md) [Ḥelqaṯ Haṣṣurîm](../../strongs/h/h2521.md), which is in [Giḇʿôn](../../strongs/h/h1391.md).

<a name="2samuel_2_17"></a>2Samuel 2:17

And there was a [me'od](../../strongs/h/h3966.md) [qāšê](../../strongs/h/h7186.md) [milḥāmâ](../../strongs/h/h4421.md) that [yowm](../../strongs/h/h3117.md); and ['Aḇnēr](../../strongs/h/h74.md) was [nāḡap̄](../../strongs/h/h5062.md), and the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md), [paniym](../../strongs/h/h6440.md) the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_2_18"></a>2Samuel 2:18

And there were three [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) there, [Yô'āḇ](../../strongs/h/h3097.md), and ['Ăḇîšay](../../strongs/h/h52.md), and [ʿĂśâ'Ēl](../../strongs/h/h6214.md): and [ʿĂśâ'Ēl](../../strongs/h/h6214.md) was as [qal](../../strongs/h/h7031.md) of [regel](../../strongs/h/h7272.md) as a [sadeh](../../strongs/h/h7704.md) [ṣᵊḇî](../../strongs/h/h6643.md).

<a name="2samuel_2_19"></a>2Samuel 2:19

And [ʿĂśâ'Ēl](../../strongs/h/h6214.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md); and in [yālaḵ](../../strongs/h/h3212.md) he [natah](../../strongs/h/h5186.md) not to the [yamiyn](../../strongs/h/h3225.md) nor to the [śᵊmō'l](../../strongs/h/h8040.md) from ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md).

<a name="2samuel_2_20"></a>2Samuel 2:20

Then ['Aḇnēr](../../strongs/h/h74.md) [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md) him, and ['āmar](../../strongs/h/h559.md), Art thou [ʿĂśâ'Ēl](../../strongs/h/h6214.md)? And he ['āmar](../../strongs/h/h559.md), I am.

<a name="2samuel_2_21"></a>2Samuel 2:21

And ['Aḇnēr](../../strongs/h/h74.md) ['āmar](../../strongs/h/h559.md) to him, [natah](../../strongs/h/h5186.md) thee to thy [yamiyn](../../strongs/h/h3225.md) or to thy [śᵊmō'l](../../strongs/h/h8040.md), and ['āḥaz](../../strongs/h/h270.md) thee on one of the [naʿar](../../strongs/h/h5288.md), and [laqach](../../strongs/h/h3947.md) thee his [ḥălîṣâ](../../strongs/h/h2488.md). But [ʿĂśâ'Ēl](../../strongs/h/h6214.md) ['āḇâ](../../strongs/h/h14.md) not [cuwr](../../strongs/h/h5493.md) from ['aḥar](../../strongs/h/h310.md) of him.

<a name="2samuel_2_22"></a>2Samuel 2:22

And ['Aḇnēr](../../strongs/h/h74.md) ['āmar](../../strongs/h/h559.md) again to [ʿĂśâ'Ēl](../../strongs/h/h6214.md), [cuwr](../../strongs/h/h5493.md) thee from ['aḥar](../../strongs/h/h310.md) me: wherefore should I [nakah](../../strongs/h/h5221.md) thee to the ['erets](../../strongs/h/h776.md)? how then should I [nasa'](../../strongs/h/h5375.md) my [paniym](../../strongs/h/h6440.md) to [Yô'āḇ](../../strongs/h/h3097.md) thy ['ach](../../strongs/h/h251.md)?

<a name="2samuel_2_23"></a>2Samuel 2:23

Howbeit he [mā'ēn](../../strongs/h/h3985.md) to [cuwr](../../strongs/h/h5493.md): wherefore ['Aḇnēr](../../strongs/h/h74.md) with the hinder ['aḥar](../../strongs/h/h310.md) of the [ḥănîṯ](../../strongs/h/h2595.md) [nakah](../../strongs/h/h5221.md) him under the [ḥōmeš](../../strongs/h/h2570.md) rib, that the [ḥănîṯ](../../strongs/h/h2595.md) [yāṣā'](../../strongs/h/h3318.md) ['aḥar](../../strongs/h/h310.md) him; and he [naphal](../../strongs/h/h5307.md) there, and [muwth](../../strongs/h/h4191.md) in the same place: and it came to pass, that as many as [bow'](../../strongs/h/h935.md) to the [maqowm](../../strongs/h/h4725.md) where [ʿĂśâ'Ēl](../../strongs/h/h6214.md) [naphal](../../strongs/h/h5307.md) and [muwth](../../strongs/h/h4191.md) ['amad](../../strongs/h/h5975.md).

<a name="2samuel_2_24"></a>2Samuel 2:24

[Yô'āḇ](../../strongs/h/h3097.md) also and ['Ăḇîšay](../../strongs/h/h52.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md): and the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md) when they were [bow'](../../strongs/h/h935.md) to the [giḇʿâ](../../strongs/h/h1389.md) of ['Ammâ](../../strongs/h/h522.md), that lieth [paniym](../../strongs/h/h6440.md) [Gîaḥ](../../strongs/h/h1520.md) by the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md) of [Giḇʿôn](../../strongs/h/h1391.md).

<a name="2samuel_2_25"></a>2Samuel 2:25

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) gathered themselves [qāḇaṣ](../../strongs/h/h6908.md) ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md), and became one ['ăḡudâ](../../strongs/h/h92.md), and ['amad](../../strongs/h/h5975.md) on the [ro'sh](../../strongs/h/h7218.md) of a [giḇʿâ](../../strongs/h/h1389.md).

<a name="2samuel_2_26"></a>2Samuel 2:26

Then ['Aḇnēr](../../strongs/h/h74.md) [qara'](../../strongs/h/h7121.md) to [Yô'āḇ](../../strongs/h/h3097.md), and ['āmar](../../strongs/h/h559.md), Shall the [chereb](../../strongs/h/h2719.md) ['akal](../../strongs/h/h398.md) for [netsach](../../strongs/h/h5331.md)? [yada'](../../strongs/h/h3045.md) thou not that it will be [mar](../../strongs/h/h4751.md) in the latter ['aḥărôn](../../strongs/h/h314.md)? how long shall it be then, ere thou ['āmar](../../strongs/h/h559.md) the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) their ['ach](../../strongs/h/h251.md)?

<a name="2samuel_2_27"></a>2Samuel 2:27

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md), As ['Elohiym](../../strongs/h/h430.md) [chay](../../strongs/h/h2416.md), unless thou hadst [dabar](../../strongs/h/h1696.md), surely then in the [boqer](../../strongs/h/h1242.md) the ['am](../../strongs/h/h5971.md) had [ʿālâ](../../strongs/h/h5927.md) every ['iysh](../../strongs/h/h376.md) from ['aḥar](../../strongs/h/h310.md) his ['ach](../../strongs/h/h251.md).

<a name="2samuel_2_28"></a>2Samuel 2:28

So [Yô'āḇ](../../strongs/h/h3097.md) [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md), and all the ['am](../../strongs/h/h5971.md) ['amad](../../strongs/h/h5975.md), and [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Yisra'el](../../strongs/h/h3478.md) no more, neither [lāḥam](../../strongs/h/h3898.md) they any more.

<a name="2samuel_2_29"></a>2Samuel 2:29

And ['Aḇnēr](../../strongs/h/h74.md) and his ['enowsh](../../strongs/h/h582.md) [halak](../../strongs/h/h1980.md) all that [layil](../../strongs/h/h3915.md) through the ['arabah](../../strongs/h/h6160.md), and ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [yālaḵ](../../strongs/h/h3212.md) all [Biṯrôn](../../strongs/h/h1338.md), and they [bow'](../../strongs/h/h935.md) to [Maḥănayim](../../strongs/h/h4266.md).

<a name="2samuel_2_30"></a>2Samuel 2:30

And [Yô'āḇ](../../strongs/h/h3097.md) [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md): and when he had [qāḇaṣ](../../strongs/h/h6908.md) all the ['am](../../strongs/h/h5971.md) [qāḇaṣ](../../strongs/h/h6908.md), there [paqad](../../strongs/h/h6485.md) of [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md) nineteen  ['iysh](../../strongs/h/h376.md) and [ʿĂśâ'Ēl](../../strongs/h/h6214.md).

<a name="2samuel_2_31"></a>2Samuel 2:31

But the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md) had [nakah](../../strongs/h/h5221.md) of [Binyāmîn](../../strongs/h/h1144.md), and of ['Aḇnēr](../../strongs/h/h74.md) ['enowsh](../../strongs/h/h582.md), so that three hundred and threescore ['iysh](../../strongs/h/h376.md) [muwth](../../strongs/h/h4191.md).

<a name="2samuel_2_32"></a>2Samuel 2:32

And they [nasa'](../../strongs/h/h5375.md) [ʿĂśâ'Ēl](../../strongs/h/h6214.md), and [qāḇar](../../strongs/h/h6912.md) him in the [qeber](../../strongs/h/h6913.md) of his ['ab](../../strongs/h/h1.md), which was in [Bêṯ leḥem](../../strongs/h/h1035.md). And [Yô'āḇ](../../strongs/h/h3097.md) and his ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) all [layil](../../strongs/h/h3915.md), and they came to [Ḥeḇrôn](../../strongs/h/h2275.md) at break of ['owr](../../strongs/h/h215.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 1](2samuel_1.md) - [2Samuel 3](2samuel_3.md)