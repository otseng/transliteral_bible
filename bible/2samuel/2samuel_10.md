# [2Samuel 10](https://www.blueletterbible.org/kjv/2samuel/10)

<a name="2samuel_10_1"></a>2Samuel 10:1

And it came to pass ['aḥar](../../strongs/h/h310.md), that the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [muwth](../../strongs/h/h4191.md), and [Ḥānûn](../../strongs/h/h2586.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2samuel_10_2"></a>2Samuel 10:2

Then ['āmar](../../strongs/h/h559.md) [Dāviḏ](../../strongs/h/h1732.md), I will ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto [Ḥānûn](../../strongs/h/h2586.md) the [ben](../../strongs/h/h1121.md) of [Nāḥāš](../../strongs/h/h5176.md), as his ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto me. And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) to [nacham](../../strongs/h/h5162.md) him by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md) for his ['ab](../../strongs/h/h1.md). And [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="2samuel_10_3"></a>2Samuel 10:3

And the [śar](../../strongs/h/h8269.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) ['āmar](../../strongs/h/h559.md) unto [Ḥānûn](../../strongs/h/h2586.md) their ['adown](../../strongs/h/h113.md), ['ayin](../../strongs/h/h5869.md) thou that [Dāviḏ](../../strongs/h/h1732.md) doth [kabad](../../strongs/h/h3513.md) thy ['ab](../../strongs/h/h1.md), that he hath [shalach](../../strongs/h/h7971.md) [nacham](../../strongs/h/h5162.md) unto thee? hath not [Dāviḏ](../../strongs/h/h1732.md) rather [shalach](../../strongs/h/h7971.md) his ['ebed](../../strongs/h/h5650.md) unto thee, to [chaqar](../../strongs/h/h2713.md) the [ʿîr](../../strongs/h/h5892.md), and to [ragal](../../strongs/h/h7270.md) it, and to [hāp̄aḵ](../../strongs/h/h2015.md) it?

<a name="2samuel_10_4"></a>2Samuel 10:4

Wherefore [Ḥānûn](../../strongs/h/h2586.md) [laqach](../../strongs/h/h3947.md) [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and [gālaḥ](../../strongs/h/h1548.md) the one half of their [zāqān](../../strongs/h/h2206.md), and [karath](../../strongs/h/h3772.md) their [meḏev](../../strongs/h/h4063.md) in the middle, even to their [Šēṯ](../../strongs/h/h8357.md), and [shalach](../../strongs/h/h7971.md) them.

<a name="2samuel_10_5"></a>2Samuel 10:5

When they [nāḡaḏ](../../strongs/h/h5046.md) it unto [Dāviḏ](../../strongs/h/h1732.md), he [shalach](../../strongs/h/h7971.md) to [qārā'](../../strongs/h/h7125.md) them, because the ['enowsh](../../strongs/h/h582.md) were [me'od](../../strongs/h/h3966.md) [kālam](../../strongs/h/h3637.md): and the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), [yashab](../../strongs/h/h3427.md) at [Yᵊrēḥô](../../strongs/h/h3405.md) until your [zāqān](../../strongs/h/h2206.md) be [ṣāmaḥ](../../strongs/h/h6779.md), and then [shuwb](../../strongs/h/h7725.md).

<a name="2samuel_10_6"></a>2Samuel 10:6

And when the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [ra'ah](../../strongs/h/h7200.md) that they [bā'aš](../../strongs/h/h887.md) before [Dāviḏ](../../strongs/h/h1732.md), the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [shalach](../../strongs/h/h7971.md) and [śāḵar](../../strongs/h/h7936.md) the ['Ărām](../../strongs/h/h758.md) of [Bêṯ-Rᵊḥôḇ](../../strongs/h/h1050.md), and the ['Ărām](../../strongs/h/h758.md) of [Ṣôḇā'](../../strongs/h/h6678.md), twenty thousand [raḡlî](../../strongs/h/h7273.md), and of [melek](../../strongs/h/h4428.md) [Maʿăḵâ](../../strongs/h/h4601.md) a thousand ['iysh](../../strongs/h/h376.md), and of ['Îš-Ṭôḇ](../../strongs/h/h382.md) twelve thousand ['iysh](../../strongs/h/h376.md).

<a name="2samuel_10_7"></a>2Samuel 10:7

And when [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) of it, he [shalach](../../strongs/h/h7971.md) [Yô'āḇ](../../strongs/h/h3097.md), and all the [tsaba'](../../strongs/h/h6635.md) of the [gibôr](../../strongs/h/h1368.md).

<a name="2samuel_10_8"></a>2Samuel 10:8

And the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [yāṣā'](../../strongs/h/h3318.md), and put the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md): and the ['Ărām](../../strongs/h/h758.md) of [Ṣôḇā'](../../strongs/h/h6678.md), and of [rᵊḥōḇ](../../strongs/h/h7340.md), and ['Îš-Ṭôḇ](../../strongs/h/h382.md), and [Maʿăḵâ](../../strongs/h/h4601.md), were by [baḏ](../../strongs/h/h905.md) in the [sadeh](../../strongs/h/h7704.md).

<a name="2samuel_10_9"></a>2Samuel 10:9

When [Yô'āḇ](../../strongs/h/h3097.md) [ra'ah](../../strongs/h/h7200.md) that the [paniym](../../strongs/h/h6440.md) of the [milḥāmâ](../../strongs/h/h4421.md) was against him [paniym](../../strongs/h/h6440.md) and ['āḥôr](../../strongs/h/h268.md), he [bāḥar](../../strongs/h/h977.md) of all the [bāḥar](../../strongs/h/h977.md) men of [Yisra'el](../../strongs/h/h3478.md), and put them in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the ['Ărām](../../strongs/h/h758.md):

<a name="2samuel_10_10"></a>2Samuel 10:10

And the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) he [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of ['Ăḇîšay](../../strongs/h/h52.md) his ['ach](../../strongs/h/h251.md), that he might put them in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="2samuel_10_11"></a>2Samuel 10:11

And he ['āmar](../../strongs/h/h559.md), If the ['Ărām](../../strongs/h/h758.md) be too [ḥāzaq](../../strongs/h/h2388.md) for me, then thou shalt [yĕshuw'ah](../../strongs/h/h3444.md) me: but if the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) be too [ḥāzaq](../../strongs/h/h2388.md) for thee, then I will [halak](../../strongs/h/h1980.md) and [yasha'](../../strongs/h/h3467.md) thee.

<a name="2samuel_10_12"></a>2Samuel 10:12

Be [ḥāzaq](../../strongs/h/h2388.md), and let us [ḥāzaq](../../strongs/h/h2388.md) for our ['am](../../strongs/h/h5971.md), and for the [ʿîr](../../strongs/h/h5892.md) of our ['Elohiym](../../strongs/h/h430.md): and [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) that which ['ayin](../../strongs/h/h5869.md) him [towb](../../strongs/h/h2896.md).

<a name="2samuel_10_13"></a>2Samuel 10:13

And [Yô'āḇ](../../strongs/h/h3097.md) [nāḡaš](../../strongs/h/h5066.md), and the ['am](../../strongs/h/h5971.md) that were with him, unto the [milḥāmâ](../../strongs/h/h4421.md) against the ['Ărām](../../strongs/h/h758.md): and they [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) him.

<a name="2samuel_10_14"></a>2Samuel 10:14

And when the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [ra'ah](../../strongs/h/h7200.md) that the ['Ărām](../../strongs/h/h758.md) were [nûs](../../strongs/h/h5127.md), then [nûs](../../strongs/h/h5127.md) they also [paniym](../../strongs/h/h6440.md) ['Ăḇîšay](../../strongs/h/h52.md), and [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md). So [Yô'āḇ](../../strongs/h/h3097.md) [shuwb](../../strongs/h/h7725.md) from the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_10_15"></a>2Samuel 10:15

And when the ['Ărām](../../strongs/h/h758.md) [ra'ah](../../strongs/h/h7200.md) that they were [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), they ['āsap̄](../../strongs/h/h622.md) themselves [yaḥaḏ](../../strongs/h/h3162.md).

<a name="2samuel_10_16"></a>2Samuel 10:16

And [HăḏarʿEzer](../../strongs/h/h1928.md) [shalach](../../strongs/h/h7971.md), and [yāṣā'](../../strongs/h/h3318.md) the ['Ărām](../../strongs/h/h758.md) that were [ʿēḇer](../../strongs/h/h5676.md) the [nāhār](../../strongs/h/h5104.md): and they [bow'](../../strongs/h/h935.md) to [Ḥêlām](../../strongs/h/h2431.md); and [Šôḇāḵ](../../strongs/h/h7731.md) the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of [HăḏarʿEzer](../../strongs/h/h1928.md) went [paniym](../../strongs/h/h6440.md) them.

<a name="2samuel_10_17"></a>2Samuel 10:17

And when it was [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), he ['āsap̄](../../strongs/h/h622.md) all [Yisra'el](../../strongs/h/h3478.md) ['āsap̄](../../strongs/h/h622.md), and ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [bow'](../../strongs/h/h935.md) to [Ḥêlām](../../strongs/h/h2431.md). And the ['Ărām](../../strongs/h/h758.md) set themselves in ['arak](../../strongs/h/h6186.md) [qārā'](../../strongs/h/h7125.md) [Dāviḏ](../../strongs/h/h1732.md), and [lāḥam](../../strongs/h/h3898.md) with him.

<a name="2samuel_10_18"></a>2Samuel 10:18

And the ['Ărām](../../strongs/h/h758.md) [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md); and [Dāviḏ](../../strongs/h/h1732.md) [harag](../../strongs/h/h2026.md) the men of seven hundred [reḵeḇ](../../strongs/h/h7393.md) of the ['Ărām](../../strongs/h/h758.md), and forty thousand [pārāš](../../strongs/h/h6571.md), and [nakah](../../strongs/h/h5221.md) [Šôḇāḵ](../../strongs/h/h7731.md) the [śar](../../strongs/h/h8269.md) of their [tsaba'](../../strongs/h/h6635.md), who [muwth](../../strongs/h/h4191.md) there.

<a name="2samuel_10_19"></a>2Samuel 10:19

And when all the [melek](../../strongs/h/h4428.md) that were ['ebed](../../strongs/h/h5650.md) to [HăḏarʿEzer](../../strongs/h/h1928.md) [ra'ah](../../strongs/h/h7200.md) that they were [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), they made [shalam](../../strongs/h/h7999.md) with [Yisra'el](../../strongs/h/h3478.md), and ['abad](../../strongs/h/h5647.md) them. So the ['Ărām](../../strongs/h/h758.md) [yare'](../../strongs/h/h3372.md) to [yasha'](../../strongs/h/h3467.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) any more.

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 9](2samuel_9.md) - [2Samuel 11](2samuel_11.md)