# [2Samuel 3](https://www.blueletterbible.org/kjv/2samuel/3)

<a name="2samuel_3_1"></a>2Samuel 3:1

Now there was ['ārēḵ](../../strongs/h/h752.md) [milḥāmâ](../../strongs/h/h4421.md) between the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md) and the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md): but [Dāviḏ](../../strongs/h/h1732.md) [halak](../../strongs/h/h1980.md) [ḥāzēq](../../strongs/h/h2390.md) and [ḥāzēq](../../strongs/h/h2390.md), and the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md) [halak](../../strongs/h/h1980.md) [dal](../../strongs/h/h1800.md) and [dal](../../strongs/h/h1800.md).

<a name="2samuel_3_2"></a>2Samuel 3:2

And unto [Dāviḏ](../../strongs/h/h1732.md) were [ben](../../strongs/h/h1121.md) [yalad](../../strongs/h/h3205.md) [yalad](../../strongs/h/h3205.md) in [Ḥeḇrôn](../../strongs/h/h2275.md): and his [bᵊḵôr](../../strongs/h/h1060.md) was ['Amnôn](../../strongs/h/h550.md), of ['ĂḥînōʿAm](../../strongs/h/h293.md) the [Yizrᵊʿē'lîṯ](../../strongs/h/h3159.md);

<a name="2samuel_3_3"></a>2Samuel 3:3

And his [mišnê](../../strongs/h/h4932.md), [Kil'Āḇ](../../strongs/h/h3609.md), of ['Ăḇîḡayil](../../strongs/h/h26.md) the ['ishshah](../../strongs/h/h802.md) of [Nāḇāl](../../strongs/h/h5037.md) the [Karmᵊlî](../../strongs/h/h3761.md); and the third, ['Ăbyšālôm](../../strongs/h/h53.md) the [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md) the [bath](../../strongs/h/h1323.md) of [Talmay](../../strongs/h/h8526.md) [melek](../../strongs/h/h4428.md) of [Gᵊšûr](../../strongs/h/h1650.md);

<a name="2samuel_3_4"></a>2Samuel 3:4

And the fourth, ['Ăḏōnîyâ](../../strongs/h/h138.md) the [ben](../../strongs/h/h1121.md) of [Ḥagîṯ](../../strongs/h/h2294.md); and the fifth, [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md) the [ben](../../strongs/h/h1121.md) of ['Ăḇîṭal](../../strongs/h/h37.md);

<a name="2samuel_3_5"></a>2Samuel 3:5

And the sixth, [YiṯrᵊʿĀm](../../strongs/h/h3507.md), by [ʿEḡlâ](../../strongs/h/h5698.md) [Dāviḏ](../../strongs/h/h1732.md) ['ishshah](../../strongs/h/h802.md). These were [yalad](../../strongs/h/h3205.md) to [Dāviḏ](../../strongs/h/h1732.md) in [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_3_6"></a>2Samuel 3:6

And it came to pass, while there was [milḥāmâ](../../strongs/h/h4421.md) between the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md) and the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), that ['Aḇnēr](../../strongs/h/h74.md) made himself [ḥāzaq](../../strongs/h/h2388.md) for the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="2samuel_3_7"></a>2Samuel 3:7

And [Šā'ûl](../../strongs/h/h7586.md) had a [pîleḡeš](../../strongs/h/h6370.md), whose [shem](../../strongs/h/h8034.md) was [Riṣpâ](../../strongs/h/h7532.md), the [bath](../../strongs/h/h1323.md) of ['Ayyâ](../../strongs/h/h345.md): and ['āmar](../../strongs/h/h559.md) to ['Aḇnēr](../../strongs/h/h74.md), Wherefore hast thou gone [bow'](../../strongs/h/h935.md) unto my ['ab](../../strongs/h/h1.md) [pîleḡeš](../../strongs/h/h6370.md)?

<a name="2samuel_3_8"></a>2Samuel 3:8

Then was ['Aḇnēr](../../strongs/h/h74.md) [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md) for the [dabar](../../strongs/h/h1697.md) of ['Îš-Bšeṯ](../../strongs/h/h378.md), and ['āmar](../../strongs/h/h559.md), Am I a [keleḇ](../../strongs/h/h3611.md) [ro'sh](../../strongs/h/h7218.md), which against [Yehuwdah](../../strongs/h/h3063.md) do ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) this [yowm](../../strongs/h/h3117.md) unto the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md) thy ['ab](../../strongs/h/h1.md), to his ['ach](../../strongs/h/h251.md), and to his [mērēaʿ](../../strongs/h/h4828.md), and have not [māṣā'](../../strongs/h/h4672.md) thee into the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md), that thou [paqad](../../strongs/h/h6485.md) me to [yowm](../../strongs/h/h3117.md) with an ['avon](../../strongs/h/h5771.md) concerning this ['ishshah](../../strongs/h/h802.md)?

<a name="2samuel_3_9"></a>2Samuel 3:9

So ['asah](../../strongs/h/h6213.md) ['Elohiym](../../strongs/h/h430.md) to ['Aḇnēr](../../strongs/h/h74.md), and more also, except, as [Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md) to [Dāviḏ](../../strongs/h/h1732.md), even so I ['asah](../../strongs/h/h6213.md) to him;

<a name="2samuel_3_10"></a>2Samuel 3:10

To ['abar](../../strongs/h/h5674.md) the [mamlāḵâ](../../strongs/h/h4467.md) from the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), and to [quwm](../../strongs/h/h6965.md) the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md) over [Yisra'el](../../strongs/h/h3478.md) and over [Yehuwdah](../../strongs/h/h3063.md), from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="2samuel_3_11"></a>2Samuel 3:11

And he [yakol](../../strongs/h/h3201.md) not [shuwb](../../strongs/h/h7725.md) ['Aḇnēr](../../strongs/h/h74.md) a [dabar](../../strongs/h/h1697.md) again, because he [yare'](../../strongs/h/h3372.md) him.

<a name="2samuel_3_12"></a>2Samuel 3:12

And ['Aḇnēr](../../strongs/h/h74.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Dāviḏ](../../strongs/h/h1732.md) on his behalf, ['āmar](../../strongs/h/h559.md), Whose is the ['erets](../../strongs/h/h776.md)? ['āmar](../../strongs/h/h559.md) also, [karath](../../strongs/h/h3772.md) thy [bĕriyth](../../strongs/h/h1285.md) with me, and, behold, my [yad](../../strongs/h/h3027.md) shall be with thee, to [cabab](../../strongs/h/h5437.md) all [Yisra'el](../../strongs/h/h3478.md) unto thee.

<a name="2samuel_3_13"></a>2Samuel 3:13

And he ['āmar](../../strongs/h/h559.md), [towb](../../strongs/h/h2896.md); I will [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with thee: but one [dabar](../../strongs/h/h1697.md) I [sha'al](../../strongs/h/h7592.md) of thee, that ['āmar](../../strongs/h/h559.md), Thou shalt not [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md), except thou [paniym](../../strongs/h/h6440.md) [bow'](../../strongs/h/h935.md) [Mîḵāl](../../strongs/h/h4324.md) [Šā'ûl](../../strongs/h/h7586.md) [bath](../../strongs/h/h1323.md), when thou [bow'](../../strongs/h/h935.md) to [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md).

<a name="2samuel_3_14"></a>2Samuel 3:14

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to ['Îš-Bšeṯ](../../strongs/h/h378.md) [Šā'ûl](../../strongs/h/h7586.md) [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) me my ['ishshah](../../strongs/h/h802.md) [Mîḵāl](../../strongs/h/h4324.md), which I ['āraś](../../strongs/h/h781.md) to me for an hundred [ʿārlâ](../../strongs/h/h6190.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="2samuel_3_15"></a>2Samuel 3:15

And ['Îš-Bšeṯ](../../strongs/h/h378.md) [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) her from her ['iysh](../../strongs/h/h376.md), even from [Palṭî'Ēl](../../strongs/h/h6409.md) the [ben](../../strongs/h/h1121.md) of [Lûš](../../strongs/h/h3889.md) [layiš](../../strongs/h/h3919.md).

<a name="2samuel_3_16"></a>2Samuel 3:16

And her ['iysh](../../strongs/h/h376.md) [yālaḵ](../../strongs/h/h3212.md) with her along [halak](../../strongs/h/h1980.md) [bāḵâ](../../strongs/h/h1058.md) ['aḥar](../../strongs/h/h310.md) her to [Baḥurîm](../../strongs/h/h980.md). Then ['āmar](../../strongs/h/h559.md) ['Aḇnēr](../../strongs/h/h74.md) unto him, [yālaḵ](../../strongs/h/h3212.md), [shuwb](../../strongs/h/h7725.md). And he [shuwb](../../strongs/h/h7725.md).

<a name="2samuel_3_17"></a>2Samuel 3:17

And ['Aḇnēr](../../strongs/h/h74.md) had [dabar](../../strongs/h/h1697.md) with the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Ye [bāqaš](../../strongs/h/h1245.md) for [Dāviḏ](../../strongs/h/h1732.md) in [tᵊmôl](../../strongs/h/h8543.md) [šilšôm](../../strongs/h/h8032.md) to be [melek](../../strongs/h/h4428.md) over you:

<a name="2samuel_3_18"></a>2Samuel 3:18

Now then ['asah](../../strongs/h/h6213.md) it: for [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) of [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), By the [yad](../../strongs/h/h3027.md) of my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) I will [yasha'](../../strongs/h/h3467.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and out of the [yad](../../strongs/h/h3027.md) of all their ['oyeb](../../strongs/h/h341.md).

<a name="2samuel_3_19"></a>2Samuel 3:19

And ['Aḇnēr](../../strongs/h/h74.md) also [dabar](../../strongs/h/h1696.md) in the ['ozen](../../strongs/h/h241.md) of [Binyāmîn](../../strongs/h/h1144.md): and ['Aḇnēr](../../strongs/h/h74.md) [yālaḵ](../../strongs/h/h3212.md) also to [dabar](../../strongs/h/h1696.md) in the ['ozen](../../strongs/h/h241.md) of [Dāviḏ](../../strongs/h/h1732.md) in [Ḥeḇrôn](../../strongs/h/h2275.md) all that ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) to [Yisra'el](../../strongs/h/h3478.md), and that seemed ['ayin](../../strongs/h/h5869.md) to the [bayith](../../strongs/h/h1004.md) of [Binyāmîn](../../strongs/h/h1144.md).

<a name="2samuel_3_20"></a>2Samuel 3:20

So ['Aḇnēr](../../strongs/h/h74.md) [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), and twenty ['enowsh](../../strongs/h/h582.md) with him. And [Dāviḏ](../../strongs/h/h1732.md) ['asah](../../strongs/h/h6213.md) ['Aḇnēr](../../strongs/h/h74.md) and the ['enowsh](../../strongs/h/h582.md) that were with him a [mištê](../../strongs/h/h4960.md).

<a name="2samuel_3_21"></a>2Samuel 3:21

And ['Aḇnēr](../../strongs/h/h74.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), I will [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md), and will [qāḇaṣ](../../strongs/h/h6908.md) all [Yisra'el](../../strongs/h/h3478.md) unto my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), that they may [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with thee, and that thou mayest [mālaḵ](../../strongs/h/h4427.md) over all that thine [nephesh](../../strongs/h/h5315.md) ['āvâ](../../strongs/h/h183.md). And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) ['Aḇnēr](../../strongs/h/h74.md) [shalach](../../strongs/h/h7971.md); and he [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_3_22"></a>2Samuel 3:22

And, behold, the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md) and [Yô'āḇ](../../strongs/h/h3097.md) [bow'](../../strongs/h/h935.md) from pursuing a [gᵊḏûḏ](../../strongs/h/h1416.md), and [bow'](../../strongs/h/h935.md) a [rab](../../strongs/h/h7227.md) [šālāl](../../strongs/h/h7998.md) with them: but ['Aḇnēr](../../strongs/h/h74.md) was not with [Dāviḏ](../../strongs/h/h1732.md) in [Ḥeḇrôn](../../strongs/h/h2275.md); for he had [shalach](../../strongs/h/h7971.md) him, and he was [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_3_23"></a>2Samuel 3:23

When [Yô'āḇ](../../strongs/h/h3097.md) and all the [tsaba'](../../strongs/h/h6635.md) that was with him were [bow'](../../strongs/h/h935.md), they [nāḡaḏ](../../strongs/h/h5046.md) [Yô'āḇ](../../strongs/h/h3097.md), ['āmar](../../strongs/h/h559.md), ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and he hath [shalach](../../strongs/h/h7971.md) him, and he is [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_3_24"></a>2Samuel 3:24

Then [Yô'āḇ](../../strongs/h/h3097.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), What hast thou ['asah](../../strongs/h/h6213.md)? behold, ['Aḇnēr](../../strongs/h/h74.md) [bow'](../../strongs/h/h935.md) unto thee; why is it that thou hast [shalach](../../strongs/h/h7971.md) him, and he is [halak](../../strongs/h/h1980.md) [yālaḵ](../../strongs/h/h3212.md)?

<a name="2samuel_3_25"></a>2Samuel 3:25

Thou [yada'](../../strongs/h/h3045.md) ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md), that he [bow'](../../strongs/h/h935.md) to [pāṯâ](../../strongs/h/h6601.md) thee, and to [yada'](../../strongs/h/h3045.md) thy going [môṣā'](../../strongs/h/h4161.md) and thy [môḇā'](../../strongs/h/h4126.md) [māḇô'](../../strongs/h/h3996.md), and to [yada'](../../strongs/h/h3045.md) all that thou ['asah](../../strongs/h/h6213.md).

<a name="2samuel_3_26"></a>2Samuel 3:26

And when [Yô'āḇ](../../strongs/h/h3097.md) was [yāṣā'](../../strongs/h/h3318.md) from [Dāviḏ](../../strongs/h/h1732.md), he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) ['aḥar](../../strongs/h/h310.md) ['Aḇnēr](../../strongs/h/h74.md), which [shuwb](../../strongs/h/h7725.md) him from the [bowr](../../strongs/h/h953.md) of [Sērâ](../../strongs/h/h5626.md): but [Dāviḏ](../../strongs/h/h1732.md) [yada'](../../strongs/h/h3045.md) it not.

<a name="2samuel_3_27"></a>2Samuel 3:27

And when ['Aḇnēr](../../strongs/h/h74.md) was [shuwb](../../strongs/h/h7725.md) to [Ḥeḇrôn](../../strongs/h/h2275.md), [Yô'āḇ](../../strongs/h/h3097.md) [natah](../../strongs/h/h5186.md) him in [tavek](../../strongs/h/h8432.md) the [sha'ar](../../strongs/h/h8179.md) to [dabar](../../strongs/h/h1696.md) with him [šᵊlî](../../strongs/h/h7987.md), and [nakah](../../strongs/h/h5221.md) him there under the [ḥōmeš](../../strongs/h/h2570.md), that he [muwth](../../strongs/h/h4191.md), for the [dam](../../strongs/h/h1818.md) of [ʿĂśâ'Ēl](../../strongs/h/h6214.md) his ['ach](../../strongs/h/h251.md).

<a name="2samuel_3_28"></a>2Samuel 3:28

And ['aḥar](../../strongs/h/h310.md) when [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) it, he ['āmar](../../strongs/h/h559.md), I and my [mamlāḵâ](../../strongs/h/h4467.md) are [naqiy](../../strongs/h/h5355.md) before [Yĕhovah](../../strongs/h/h3068.md) ['owlam](../../strongs/h/h5769.md) from the [dam](../../strongs/h/h1818.md) of ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md):

<a name="2samuel_3_29"></a>2Samuel 3:29

Let it [chuwl](../../strongs/h/h2342.md) on the [ro'sh](../../strongs/h/h7218.md) of [Yô'āḇ](../../strongs/h/h3097.md), and on all his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md); and let there not [karath](../../strongs/h/h3772.md) from the [bayith](../../strongs/h/h1004.md) of [Yô'āḇ](../../strongs/h/h3097.md) one that hath a [zûḇ](../../strongs/h/h2100.md), or that is a [ṣāraʿ](../../strongs/h/h6879.md), or that [ḥāzaq](../../strongs/h/h2388.md) on a [peleḵ](../../strongs/h/h6418.md), or that [naphal](../../strongs/h/h5307.md) on the [chereb](../../strongs/h/h2719.md), or that [ḥāsēr](../../strongs/h/h2638.md) [lechem](../../strongs/h/h3899.md).

<a name="2samuel_3_30"></a>2Samuel 3:30

So [Yô'āḇ](../../strongs/h/h3097.md) and ['Ăḇîšay](../../strongs/h/h52.md) his ['ach](../../strongs/h/h251.md) [harag](../../strongs/h/h2026.md) ['Aḇnēr](../../strongs/h/h74.md), because he had [muwth](../../strongs/h/h4191.md) their ['ach](../../strongs/h/h251.md) [ʿĂśâ'Ēl](../../strongs/h/h6214.md) at [Giḇʿôn](../../strongs/h/h1391.md) in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="2samuel_3_31"></a>2Samuel 3:31

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to [Yô'āḇ](../../strongs/h/h3097.md), and to all the ['am](../../strongs/h/h5971.md) that were with him, [qāraʿ](../../strongs/h/h7167.md) your [beḡeḏ](../../strongs/h/h899.md), and [ḥāḡar](../../strongs/h/h2296.md) you with [śaq](../../strongs/h/h8242.md), and [sāp̄aḏ](../../strongs/h/h5594.md) [paniym](../../strongs/h/h6440.md) ['Aḇnēr](../../strongs/h/h74.md). And [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) himself [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) the [mittah](../../strongs/h/h4296.md).

<a name="2samuel_3_32"></a>2Samuel 3:32

And they [qāḇar](../../strongs/h/h6912.md) ['Aḇnēr](../../strongs/h/h74.md) in [Ḥeḇrôn](../../strongs/h/h2275.md): and the [melek](../../strongs/h/h4428.md) [nasa'](../../strongs/h/h5375.md) his [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md) at the [qeber](../../strongs/h/h6913.md) of ['Aḇnēr](../../strongs/h/h74.md); and all the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md).

<a name="2samuel_3_33"></a>2Samuel 3:33

And the [melek](../../strongs/h/h4428.md) [qônēn](../../strongs/h/h6969.md) over ['Aḇnēr](../../strongs/h/h74.md), and ['āmar](../../strongs/h/h559.md), [muwth](../../strongs/h/h4191.md) ['Aḇnēr](../../strongs/h/h74.md) as a [nabal](../../strongs/h/h5036.md) [maveth](../../strongs/h/h4194.md)?

<a name="2samuel_3_34"></a>2Samuel 3:34

Thy [yad](../../strongs/h/h3027.md) were not ['āsar](../../strongs/h/h631.md), nor thy [regel](../../strongs/h/h7272.md) [nāḡaš](../../strongs/h/h5066.md) into [nᵊḥšeṯ](../../strongs/h/h5178.md): as a man [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) ['evel](../../strongs/h/h5766.md) [ben](../../strongs/h/h1121.md), so [naphal](../../strongs/h/h5307.md) thou. And all the ['am](../../strongs/h/h5971.md) [bāḵâ](../../strongs/h/h1058.md) again over him.

<a name="2samuel_3_35"></a>2Samuel 3:35

And when all the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to [bārâ](../../strongs/h/h1262.md) [Dāviḏ](../../strongs/h/h1732.md) to [bārâ](../../strongs/h/h1262.md) [lechem](../../strongs/h/h3899.md) while it was yet [yowm](../../strongs/h/h3117.md), [Dāviḏ](../../strongs/h/h1732.md) [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), So ['asah](../../strongs/h/h6213.md) ['Elohiym](../../strongs/h/h430.md) to me, and more also, if I [ṭāʿam](../../strongs/h/h2938.md) [lechem](../../strongs/h/h3899.md), or [mᵊ'ûmâ](../../strongs/h/h3972.md) else, [paniym](../../strongs/h/h6440.md) the [šemeš](../../strongs/h/h8121.md) be [bow'](../../strongs/h/h935.md).

<a name="2samuel_3_36"></a>2Samuel 3:36

And all the ['am](../../strongs/h/h5971.md) took [nāḵar](../../strongs/h/h5234.md) of it, and it [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md) them: as whatsoever the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) all the ['am](../../strongs/h/h5971.md).

<a name="2samuel_3_37"></a>2Samuel 3:37

For all the ['am](../../strongs/h/h5971.md) and all [Yisra'el](../../strongs/h/h3478.md) [yada'](../../strongs/h/h3045.md) that [yowm](../../strongs/h/h3117.md) that it was not of the [melek](../../strongs/h/h4428.md) to [muwth](../../strongs/h/h4191.md) ['Aḇnēr](../../strongs/h/h74.md) the [ben](../../strongs/h/h1121.md) of [Nēr](../../strongs/h/h5369.md).

<a name="2samuel_3_38"></a>2Samuel 3:38

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), [yada'](../../strongs/h/h3045.md) ye not that there is a [śar](../../strongs/h/h8269.md) and a [gadowl](../../strongs/h/h1419.md) [naphal](../../strongs/h/h5307.md) this [yowm](../../strongs/h/h3117.md) in [Yisra'el](../../strongs/h/h3478.md)?

<a name="2samuel_3_39"></a>2Samuel 3:39

And I am this [yowm](../../strongs/h/h3117.md) [raḵ](../../strongs/h/h7390.md), though [māšaḥ](../../strongs/h/h4886.md) [melek](../../strongs/h/h4428.md); and these ['enowsh](../../strongs/h/h582.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) be too [qāšê](../../strongs/h/h7186.md) for me: [Yĕhovah](../../strongs/h/h3068.md) shall [shalam](../../strongs/h/h7999.md) the doer of ['asah](../../strongs/h/h6213.md) according to his [ra'](../../strongs/h/h7451.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 2](2samuel_2.md) - [2Samuel 4](2samuel_4.md)