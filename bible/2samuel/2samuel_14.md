# [2Samuel 14](https://www.blueletterbible.org/kjv/2samuel/14)

<a name="2samuel_14_1"></a>2Samuel 14:1

Now [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) [yada'](../../strongs/h/h3045.md) that the [melek](../../strongs/h/h4428.md) [leb](../../strongs/h/h3820.md) was toward ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_14_2"></a>2Samuel 14:2

And [Yô'āḇ](../../strongs/h/h3097.md) [shalach](../../strongs/h/h7971.md) to [Tᵊqôaʿ](../../strongs/h/h8620.md), and [laqach](../../strongs/h/h3947.md) thence a [ḥāḵām](../../strongs/h/h2450.md) ['ishshah](../../strongs/h/h802.md), and ['āmar](../../strongs/h/h559.md) unto her, I pray thee, feign thyself to be an ['āḇal](../../strongs/h/h56.md), and [labash](../../strongs/h/h3847.md) now ['ēḇel](../../strongs/h/h60.md) [beḡeḏ](../../strongs/h/h899.md), and [sûḵ](../../strongs/h/h5480.md) not thyself with [šemen](../../strongs/h/h8081.md), but be as an ['ishshah](../../strongs/h/h802.md) that had a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) ['āḇal](../../strongs/h/h56.md) for the [muwth](../../strongs/h/h4191.md):

<a name="2samuel_14_3"></a>2Samuel 14:3

And [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and [dabar](../../strongs/h/h1696.md) on this [dabar](../../strongs/h/h1697.md) unto him. So [Yô'āḇ](../../strongs/h/h3097.md) [śûm](../../strongs/h/h7760.md) the [dabar](../../strongs/h/h1697.md) in her [peh](../../strongs/h/h6310.md).

<a name="2samuel_14_4"></a>2Samuel 14:4

And when the ['ishshah](../../strongs/h/h802.md) of [Tᵊqôʿî](../../strongs/h/h8621.md) ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md), she [naphal](../../strongs/h/h5307.md) on her ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md), and did [shachah](../../strongs/h/h7812.md), and ['āmar](../../strongs/h/h559.md), [yasha'](../../strongs/h/h3467.md), O [melek](../../strongs/h/h4428.md).

<a name="2samuel_14_5"></a>2Samuel 14:5

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto her, What aileth thee? And she ['āmar](../../strongs/h/h559.md), I am ['ăḇāl](../../strongs/h/h61.md) an ['almānâ](../../strongs/h/h490.md) ['ishshah](../../strongs/h/h802.md), and mine ['iysh](../../strongs/h/h376.md) is [muwth](../../strongs/h/h4191.md).

<a name="2samuel_14_6"></a>2Samuel 14:6

And thy [šip̄ḥâ](../../strongs/h/h8198.md) had two [ben](../../strongs/h/h1121.md), and they two strove [nāṣâ](../../strongs/h/h5327.md) in the [sadeh](../../strongs/h/h7704.md), and there was none to [natsal](../../strongs/h/h5337.md) them, but the one [nakah](../../strongs/h/h5221.md) the other, and [muwth](../../strongs/h/h4191.md) him.

<a name="2samuel_14_7"></a>2Samuel 14:7

And, behold, the [mišpāḥâ](../../strongs/h/h4940.md) is [quwm](../../strongs/h/h6965.md) against thine [šip̄ḥâ](../../strongs/h/h8198.md), and they ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) him that [nakah](../../strongs/h/h5221.md) his ['ach](../../strongs/h/h251.md), that we may [muwth](../../strongs/h/h4191.md) him, for the [nephesh](../../strongs/h/h5315.md) of his ['ach](../../strongs/h/h251.md) whom he [harag](../../strongs/h/h2026.md); and we will [šāmaḏ](../../strongs/h/h8045.md) the [yarash](../../strongs/h/h3423.md) also: and so they shall [kāḇâ](../../strongs/h/h3518.md) my [gechel](../../strongs/h/h1513.md) which is [śûm](../../strongs/h/h7760.md), and shall not [šā'ar](../../strongs/h/h7604.md) to my ['iysh](../../strongs/h/h376.md) neither [shem](../../strongs/h/h8034.md) nor [šᵊ'ērîṯ](../../strongs/h/h7611.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="2samuel_14_8"></a>2Samuel 14:8

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), [yālaḵ](../../strongs/h/h3212.md) to thine [bayith](../../strongs/h/h1004.md), and I will [tsavah](../../strongs/h/h6680.md) concerning thee.

<a name="2samuel_14_9"></a>2Samuel 14:9

And the ['ishshah](../../strongs/h/h802.md) of [Tᵊqôʿî](../../strongs/h/h8621.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), My ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), the ['avon](../../strongs/h/h5771.md) be on me, and on my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): and the [melek](../../strongs/h/h4428.md) and his [kicce'](../../strongs/h/h3678.md) be [naqiy](../../strongs/h/h5355.md).

<a name="2samuel_14_10"></a>2Samuel 14:10

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Whosoever [dabar](../../strongs/h/h1696.md) ought unto thee, [bow'](../../strongs/h/h935.md) him to me, and he shall not [naga'](../../strongs/h/h5060.md) thee any more.

<a name="2samuel_14_11"></a>2Samuel 14:11

Then ['āmar](../../strongs/h/h559.md) she, I pray thee, let the [melek](../../strongs/h/h4428.md) [zakar](../../strongs/h/h2142.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), that thou wouldest not suffer the [gā'al](../../strongs/h/h1350.md) of [dam](../../strongs/h/h1818.md) to [shachath](../../strongs/h/h7843.md) any [rabah](../../strongs/h/h7235.md), lest they [šāmaḏ](../../strongs/h/h8045.md) my [ben](../../strongs/h/h1121.md). And he ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), there shall not one [śaʿărâ](../../strongs/h/h8185.md) of thy [ben](../../strongs/h/h1121.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md).

<a name="2samuel_14_12"></a>2Samuel 14:12

Then the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), Let thine [šip̄ḥâ](../../strongs/h/h8198.md), I pray thee, [dabar](../../strongs/h/h1696.md) one [dabar](../../strongs/h/h1697.md) unto my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md). And he ['āmar](../../strongs/h/h559.md), [dabar](../../strongs/h/h1696.md).

<a name="2samuel_14_13"></a>2Samuel 14:13

And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), Wherefore then hast thou [chashab](../../strongs/h/h2803.md) such a thing against the ['am](../../strongs/h/h5971.md) of ['Elohiym](../../strongs/h/h430.md)? for the [melek](../../strongs/h/h4428.md) doth [dabar](../../strongs/h/h1696.md) this [dabar](../../strongs/h/h1697.md) as one which is ['āšēm](../../strongs/h/h818.md), in that the [melek](../../strongs/h/h4428.md) doth not [shuwb](../../strongs/h/h7725.md) his [nāḏaḥ](../../strongs/h/h5080.md).

<a name="2samuel_14_14"></a>2Samuel 14:14

For we must [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), and are as [mayim](../../strongs/h/h4325.md) [nāḡar](../../strongs/h/h5064.md) on the ['erets](../../strongs/h/h776.md), which cannot be gathered up ['āsap̄](../../strongs/h/h622.md); neither doth ['Elohiym](../../strongs/h/h430.md) [nasa'](../../strongs/h/h5375.md) any [nephesh](../../strongs/h/h5315.md): yet doth he [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md), that his [nāḏaḥ](../../strongs/h/h5080.md) be not [nāḏaḥ](../../strongs/h/h5080.md) from him.

<a name="2samuel_14_15"></a>2Samuel 14:15

Now therefore that I am [bow'](../../strongs/h/h935.md) to [dabar](../../strongs/h/h1696.md) of this [dabar](../../strongs/h/h1697.md) unto my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), it is because the ['am](../../strongs/h/h5971.md) have made me [yare'](../../strongs/h/h3372.md): and thy [šip̄ḥâ](../../strongs/h/h8198.md) ['āmar](../../strongs/h/h559.md), I will now [dabar](../../strongs/h/h1696.md) unto the [melek](../../strongs/h/h4428.md); it may be that the [melek](../../strongs/h/h4428.md) will ['asah](../../strongs/h/h6213.md) the [dabar](../../strongs/h/h1697.md) of his ['amah](../../strongs/h/h519.md).

<a name="2samuel_14_16"></a>2Samuel 14:16

For the [melek](../../strongs/h/h4428.md) will [shama'](../../strongs/h/h8085.md), to [natsal](../../strongs/h/h5337.md) his ['amah](../../strongs/h/h519.md) out of the [kaph](../../strongs/h/h3709.md) of the ['iysh](../../strongs/h/h376.md) that would [šāmaḏ](../../strongs/h/h8045.md) me and my [ben](../../strongs/h/h1121.md) [yaḥaḏ](../../strongs/h/h3162.md) out of the [nachalah](../../strongs/h/h5159.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2samuel_14_17"></a>2Samuel 14:17

Then thine [šip̄ḥâ](../../strongs/h/h8198.md) ['āmar](../../strongs/h/h559.md), The [dabar](../../strongs/h/h1697.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) shall now be [mᵊnûḥâ](../../strongs/h/h4496.md): for as a [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md), so is my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) to [shama'](../../strongs/h/h8085.md) [towb](../../strongs/h/h2896.md) and [ra'](../../strongs/h/h7451.md): therefore [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) will be with thee.

<a name="2samuel_14_18"></a>2Samuel 14:18

Then the [melek](../../strongs/h/h4428.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md), [kāḥaḏ](../../strongs/h/h3582.md) not from me, I pray thee, the [dabar](../../strongs/h/h1697.md) that I shall [sha'al](../../strongs/h/h7592.md) thee. And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md), Let my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) now [dabar](../../strongs/h/h1696.md).

<a name="2samuel_14_19"></a>2Samuel 14:19

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Is not the [yad](../../strongs/h/h3027.md) of [Yô'āḇ](../../strongs/h/h3097.md) with thee in all this? And the ['ishshah](../../strongs/h/h802.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), As thy [nephesh](../../strongs/h/h5315.md) [chay](../../strongs/h/h2416.md), my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), ['iysh](../../strongs/h/h376.md) ['iš](../../strongs/h/h786.md) turn to the [yāman](../../strongs/h/h3231.md) or to the [śam'al](../../strongs/h/h8041.md) from ought that my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) hath [dabar](../../strongs/h/h1696.md): for thy ['ebed](../../strongs/h/h5650.md) [Yô'āḇ](../../strongs/h/h3097.md), he [tsavah](../../strongs/h/h6680.md) me, and he [śûm](../../strongs/h/h7760.md) all these [dabar](../../strongs/h/h1697.md) in the [peh](../../strongs/h/h6310.md) of thine [šip̄ḥâ](../../strongs/h/h8198.md):

<a name="2samuel_14_20"></a>2Samuel 14:20

To [cabab](../../strongs/h/h5437.md) this [paniym](../../strongs/h/h6440.md) of [dabar](../../strongs/h/h1697.md) hath thy ['ebed](../../strongs/h/h5650.md) [Yô'āḇ](../../strongs/h/h3097.md) ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md): and my ['adown](../../strongs/h/h113.md) is [ḥāḵām](../../strongs/h/h2450.md), according to the [ḥāḵmâ](../../strongs/h/h2451.md) of a [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md), to [yada'](../../strongs/h/h3045.md) all things that are in the ['erets](../../strongs/h/h776.md).

<a name="2samuel_14_21"></a>2Samuel 14:21

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Yô'āḇ](../../strongs/h/h3097.md), Behold now, I have ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md): [yālaḵ](../../strongs/h/h3212.md) therefore, [shuwb](../../strongs/h/h7725.md) the [naʿar](../../strongs/h/h5288.md) ['Ăbyšālôm](../../strongs/h/h53.md) [shuwb](../../strongs/h/h7725.md).

<a name="2samuel_14_22"></a>2Samuel 14:22

And [Yô'āḇ](../../strongs/h/h3097.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md) on his [paniym](../../strongs/h/h6440.md), and [shachah](../../strongs/h/h7812.md) himself, and [barak](../../strongs/h/h1288.md) the [melek](../../strongs/h/h4428.md): and [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md), To [yowm](../../strongs/h/h3117.md) thy ['ebed](../../strongs/h/h5650.md) [yada'](../../strongs/h/h3045.md) that I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md), in that the [melek](../../strongs/h/h4428.md) hath ['asah](../../strongs/h/h6213.md) the [dabar](../../strongs/h/h1697.md) of his ['ebed](../../strongs/h/h5650.md).

<a name="2samuel_14_23"></a>2Samuel 14:23

So [Yô'āḇ](../../strongs/h/h3097.md) [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md) to [Gᵊšûr](../../strongs/h/h1650.md), and [bow'](../../strongs/h/h935.md) ['Ăbyšālôm](../../strongs/h/h53.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_14_24"></a>2Samuel 14:24

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Let him [cabab](../../strongs/h/h5437.md) to his own [bayith](../../strongs/h/h1004.md), and let him not [ra'ah](../../strongs/h/h7200.md) my [paniym](../../strongs/h/h6440.md). So ['Ăbyšālôm](../../strongs/h/h53.md) [cabab](../../strongs/h/h5437.md) to his own [bayith](../../strongs/h/h1004.md), and [ra'ah](../../strongs/h/h7200.md) not the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md).

<a name="2samuel_14_25"></a>2Samuel 14:25

But in all [Yisra'el](../../strongs/h/h3478.md) there was ['iysh](../../strongs/h/h376.md) to be so [me'od](../../strongs/h/h3966.md) [halal](../../strongs/h/h1984.md) as ['Ăbyšālôm](../../strongs/h/h53.md) for his [yāp̄ê](../../strongs/h/h3303.md): from the [kaph](../../strongs/h/h3709.md) of his [regel](../../strongs/h/h7272.md) even to his [qodqod](../../strongs/h/h6936.md) there was no [mᵊ'ûm](../../strongs/h/h3971.md) in him.

<a name="2samuel_14_26"></a>2Samuel 14:26

And when he [gālaḥ](../../strongs/h/h1548.md) his [ro'sh](../../strongs/h/h7218.md), (for it was at every [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [qēṣ](../../strongs/h/h7093.md) that he [gālaḥ](../../strongs/h/h1548.md) it: because was [kabad](../../strongs/h/h3513.md) on him, therefore he [gālaḥ](../../strongs/h/h1548.md) it:) he [šāqal](../../strongs/h/h8254.md) the [śēʿār](../../strongs/h/h8181.md) of his [ro'sh](../../strongs/h/h7218.md) at two hundred [šeqel](../../strongs/h/h8255.md) after the [melek](../../strongs/h/h4428.md) ['eben](../../strongs/h/h68.md).

<a name="2samuel_14_27"></a>2Samuel 14:27

And unto ['Ăbyšālôm](../../strongs/h/h53.md) there were [yalad](../../strongs/h/h3205.md) three [ben](../../strongs/h/h1121.md), and one [bath](../../strongs/h/h1323.md), whose [shem](../../strongs/h/h8034.md) was [Tāmār](../../strongs/h/h8559.md): she was an ['ishshah](../../strongs/h/h802.md) of a [yāp̄ê](../../strongs/h/h3303.md) [mar'ê](../../strongs/h/h4758.md).

<a name="2samuel_14_28"></a>2Samuel 14:28

So ['Ăbyšālôm](../../strongs/h/h53.md) [yashab](../../strongs/h/h3427.md) two [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and [ra'ah](../../strongs/h/h7200.md) not the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md).

<a name="2samuel_14_29"></a>2Samuel 14:29

Therefore ['Ăbyšālôm](../../strongs/h/h53.md) [shalach](../../strongs/h/h7971.md) for [Yô'āḇ](../../strongs/h/h3097.md), to have [shalach](../../strongs/h/h7971.md) him to the [melek](../../strongs/h/h4428.md); but he ['āḇâ](../../strongs/h/h14.md) not [bow'](../../strongs/h/h935.md) to him: and when he [shalach](../../strongs/h/h7971.md) the second time, he ['āḇâ](../../strongs/h/h14.md) not [bow'](../../strongs/h/h935.md).

<a name="2samuel_14_30"></a>2Samuel 14:30

Therefore he ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), [ra'ah](../../strongs/h/h7200.md), [Yô'āḇ](../../strongs/h/h3097.md) [ḥelqâ](../../strongs/h/h2513.md) is near [yad](../../strongs/h/h3027.md), and he hath [śᵊʿōrâ](../../strongs/h/h8184.md) there; [yālaḵ](../../strongs/h/h3212.md) and [yāṣaṯ](../../strongs/h/h3341.md) it on ['esh](../../strongs/h/h784.md). And ['Ăbyšālôm](../../strongs/h/h53.md) ['ebed](../../strongs/h/h5650.md) [yāṣaṯ](../../strongs/h/h3341.md) the [ḥelqâ](../../strongs/h/h2513.md) on ['esh](../../strongs/h/h784.md).

<a name="2samuel_14_31"></a>2Samuel 14:31

Then [Yô'āḇ](../../strongs/h/h3097.md) [quwm](../../strongs/h/h6965.md), and [bow'](../../strongs/h/h935.md) to ['Ăbyšālôm](../../strongs/h/h53.md) unto his [bayith](../../strongs/h/h1004.md), and ['āmar](../../strongs/h/h559.md) unto him, Wherefore have thy ['ebed](../../strongs/h/h5650.md) [yāṣaṯ](../../strongs/h/h3341.md) my [ḥelqâ](../../strongs/h/h2513.md) on ['esh](../../strongs/h/h784.md)?

<a name="2samuel_14_32"></a>2Samuel 14:32

And ['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) [Yô'āḇ](../../strongs/h/h3097.md), Behold, I [shalach](../../strongs/h/h7971.md) unto thee, ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) hither, that I may [shalach](../../strongs/h/h7971.md) thee to the [melek](../../strongs/h/h4428.md), to ['āmar](../../strongs/h/h559.md), Wherefore am I [bow'](../../strongs/h/h935.md) from [Gᵊšûr](../../strongs/h/h1650.md)? it had been [towb](../../strongs/h/h2896.md) for me to have been there still: now therefore let me [ra'ah](../../strongs/h/h7200.md) the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md); and if there be any ['avon](../../strongs/h/h5771.md) in me, let him [muwth](../../strongs/h/h4191.md) me.

<a name="2samuel_14_33"></a>2Samuel 14:33

So [Yô'āḇ](../../strongs/h/h3097.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and [nāḡaḏ](../../strongs/h/h5046.md) him: and when he had [qara'](../../strongs/h/h7121.md) for ['Ăbyšālôm](../../strongs/h/h53.md), he [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), and [shachah](../../strongs/h/h7812.md) himself on his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md): and the [melek](../../strongs/h/h4428.md) [nashaq](../../strongs/h/h5401.md) ['Ăbyšālôm](../../strongs/h/h53.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 13](2samuel_13.md) - [2Samuel 15](2samuel_15.md)