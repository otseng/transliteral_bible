# [2Samuel 7](https://www.blueletterbible.org/kjv/2samuel/7)

<a name="2samuel_7_1"></a>2Samuel 7:1

And it came to pass, when the [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) in his [bayith](../../strongs/h/h1004.md), and [Yĕhovah](../../strongs/h/h3068.md) had given him [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md) from all his ['oyeb](../../strongs/h/h341.md);

<a name="2samuel_7_2"></a>2Samuel 7:2

That the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), [ra'ah](../../strongs/h/h7200.md) now, I [yashab](../../strongs/h/h3427.md) in a [bayith](../../strongs/h/h1004.md) of ['erez](../../strongs/h/h730.md), but the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) [yashab](../../strongs/h/h3427.md) [tavek](../../strongs/h/h8432.md) [yᵊrîʿâ](../../strongs/h/h3407.md).

<a name="2samuel_7_3"></a>2Samuel 7:3

And [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md), [yālaḵ](../../strongs/h/h3212.md), ['asah](../../strongs/h/h6213.md) all that is in thine [lebab](../../strongs/h/h3824.md); for [Yĕhovah](../../strongs/h/h3068.md) is with thee.

<a name="2samuel_7_4"></a>2Samuel 7:4

And it came to pass that [layil](../../strongs/h/h3915.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Nāṯān](../../strongs/h/h5416.md), ['āmar](../../strongs/h/h559.md),

<a name="2samuel_7_5"></a>2Samuel 7:5

[yālaḵ](../../strongs/h/h3212.md) and ['āmar](../../strongs/h/h559.md) my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Shalt thou [bānâ](../../strongs/h/h1129.md) me a [bayith](../../strongs/h/h1004.md) for me to [yashab](../../strongs/h/h3427.md)?

<a name="2samuel_7_6"></a>2Samuel 7:6

Whereas I have not [yashab](../../strongs/h/h3427.md) in any [bayith](../../strongs/h/h1004.md) since the [yowm](../../strongs/h/h3117.md) that I [ʿālâ](../../strongs/h/h5927.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of [Mitsrayim](../../strongs/h/h4714.md), even to this [yowm](../../strongs/h/h3117.md), but have [halak](../../strongs/h/h1980.md) in a ['ohel](../../strongs/h/h168.md) and in a [miškān](../../strongs/h/h4908.md).

<a name="2samuel_7_7"></a>2Samuel 7:7

In all wherein I have [halak](../../strongs/h/h1980.md) with all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [dabar](../../strongs/h/h1696.md) I a [dabar](../../strongs/h/h1697.md) with any of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), whom I [tsavah](../../strongs/h/h6680.md) to [ra'ah](../../strongs/h/h7462.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Why [bānâ](../../strongs/h/h1129.md) ye not me a [bayith](../../strongs/h/h1004.md) of ['erez](../../strongs/h/h730.md)?

<a name="2samuel_7_8"></a>2Samuel 7:8

Now therefore so shalt thou ['āmar](../../strongs/h/h559.md) unto my ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), I [laqach](../../strongs/h/h3947.md) thee from the [nāvê](../../strongs/h/h5116.md), from ['aḥar](../../strongs/h/h310.md) the [tso'n](../../strongs/h/h6629.md), to be [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md), over [Yisra'el](../../strongs/h/h3478.md):

<a name="2samuel_7_9"></a>2Samuel 7:9

And I was with thee whithersoever thou [halak](../../strongs/h/h1980.md), and have [karath](../../strongs/h/h3772.md) all thine ['oyeb](../../strongs/h/h341.md) out of thy [paniym](../../strongs/h/h6440.md), and have ['asah](../../strongs/h/h6213.md) thee a [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md), like unto the [shem](../../strongs/h/h8034.md) of the [gadowl](../../strongs/h/h1419.md) men that are in the ['erets](../../strongs/h/h776.md).

<a name="2samuel_7_10"></a>2Samuel 7:10

Moreover I will [śûm](../../strongs/h/h7760.md) a [maqowm](../../strongs/h/h4725.md) for my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and will [nāṭaʿ](../../strongs/h/h5193.md) them, that they may [shakan](../../strongs/h/h7931.md) in a place of their own, and [ragaz](../../strongs/h/h7264.md) no more; neither shall the [ben](../../strongs/h/h1121.md) of ['evel](../../strongs/h/h5766.md) [ʿānâ](../../strongs/h/h6031.md) them any more, as [ri'šôn](../../strongs/h/h7223.md),

<a name="2samuel_7_11"></a>2Samuel 7:11

And as since the [yowm](../../strongs/h/h3117.md) that I [tsavah](../../strongs/h/h6680.md) [shaphat](../../strongs/h/h8199.md) to be over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and have caused thee to [nuwach](../../strongs/h/h5117.md) from all thine ['oyeb](../../strongs/h/h341.md). Also [Yĕhovah](../../strongs/h/h3068.md) [nāḡaḏ](../../strongs/h/h5046.md) thee that he will ['asah](../../strongs/h/h6213.md) thee a [bayith](../../strongs/h/h1004.md).

<a name="2samuel_7_12"></a>2Samuel 7:12

And when thy [yowm](../../strongs/h/h3117.md) be [mālā'](../../strongs/h/h4390.md), and thou shalt [shakab](../../strongs/h/h7901.md) with thy ['ab](../../strongs/h/h1.md), I will [quwm](../../strongs/h/h6965.md) thy [zera'](../../strongs/h/h2233.md) ['aḥar](../../strongs/h/h310.md) thee, which shall [yāṣā'](../../strongs/h/h3318.md) out of thy [me'ah](../../strongs/h/h4578.md), and I will [kuwn](../../strongs/h/h3559.md) his [mamlāḵâ](../../strongs/h/h4467.md).

<a name="2samuel_7_13"></a>2Samuel 7:13

He shall [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for my [shem](../../strongs/h/h8034.md), and I will [kuwn](../../strongs/h/h3559.md) the [kicce'](../../strongs/h/h3678.md) of his [mamlāḵâ](../../strongs/h/h4467.md) ['owlam](../../strongs/h/h5769.md).

<a name="2samuel_7_14"></a>2Samuel 7:14

I will be his ['ab](../../strongs/h/h1.md), and he shall be my [ben](../../strongs/h/h1121.md). If he commit [ʿāvâ](../../strongs/h/h5753.md), I will [yakach](../../strongs/h/h3198.md) him with the [shebet](../../strongs/h/h7626.md) of ['enowsh](../../strongs/h/h582.md), and with the [neḡaʿ](../../strongs/h/h5061.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md):

<a name="2samuel_7_15"></a>2Samuel 7:15

But my [checed](../../strongs/h/h2617.md) shall not [cuwr](../../strongs/h/h5493.md) from him, as I [cuwr](../../strongs/h/h5493.md) it from [Šā'ûl](../../strongs/h/h7586.md), whom I put [cuwr](../../strongs/h/h5493.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="2samuel_7_16"></a>2Samuel 7:16

And thine [bayith](../../strongs/h/h1004.md) and thy [mamlāḵâ](../../strongs/h/h4467.md) shall be ['aman](../../strongs/h/h539.md) ['owlam](../../strongs/h/h5769.md) [paniym](../../strongs/h/h6440.md) thee: thy [kicce'](../../strongs/h/h3678.md) shall be [kuwn](../../strongs/h/h3559.md) ['owlam](../../strongs/h/h5769.md).

<a name="2samuel_7_17"></a>2Samuel 7:17

According to all these [dabar](../../strongs/h/h1697.md), and according to all this [ḥizzāyôn](../../strongs/h/h2384.md), so did [Nāṯān](../../strongs/h/h5416.md) [dabar](../../strongs/h/h1696.md) unto [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_7_18"></a>2Samuel 7:18

Then [bow'](../../strongs/h/h935.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) in, and [yashab](../../strongs/h/h3427.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and he ['āmar](../../strongs/h/h559.md), Who am I, ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)? and what is my [bayith](../../strongs/h/h1004.md), that thou hast [bow'](../../strongs/h/h935.md) me hitherto?

<a name="2samuel_7_19"></a>2Samuel 7:19

And this was yet a [qāṭōn](../../strongs/h/h6994.md) in thy ['ayin](../../strongs/h/h5869.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); but thou hast [dabar](../../strongs/h/h1696.md) also of thy ['ebed](../../strongs/h/h5650.md) [bayith](../../strongs/h/h1004.md) for a great while to [rachowq](../../strongs/h/h7350.md). And is this the [tôrâ](../../strongs/h/h8452.md) of ['āḏām](../../strongs/h/h120.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)?

<a name="2samuel_7_20"></a>2Samuel 7:20

And what can [Dāviḏ](../../strongs/h/h1732.md) [dabar](../../strongs/h/h1696.md) more unto thee? for thou, ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [yada'](../../strongs/h/h3045.md) thy ['ebed](../../strongs/h/h5650.md).

<a name="2samuel_7_21"></a>2Samuel 7:21

For thy [dabar](../../strongs/h/h1697.md) sake, and according to thine own [leb](../../strongs/h/h3820.md), hast thou ['asah](../../strongs/h/h6213.md) all these [gᵊḏûlâ](../../strongs/h/h1420.md), to make thy ['ebed](../../strongs/h/h5650.md) [yada'](../../strongs/h/h3045.md) them.

<a name="2samuel_7_22"></a>2Samuel 7:22

Wherefore thou art [gāḏal](../../strongs/h/h1431.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md): for there is none like thee, neither is there any ['Elohiym](../../strongs/h/h430.md) [zûlâ](../../strongs/h/h2108.md) thee, according to all that we have [shama'](../../strongs/h/h8085.md) with our ['ozen](../../strongs/h/h241.md).

<a name="2samuel_7_23"></a>2Samuel 7:23

And what one [gowy](../../strongs/h/h1471.md) in the ['erets](../../strongs/h/h776.md) is like thy ['am](../../strongs/h/h5971.md), even like [Yisra'el](../../strongs/h/h3478.md), whom ['Elohiym](../../strongs/h/h430.md) [halak](../../strongs/h/h1980.md) to [pāḏâ](../../strongs/h/h6299.md) for an ['am](../../strongs/h/h5971.md) to himself, and to [śûm](../../strongs/h/h7760.md) him a [shem](../../strongs/h/h8034.md), and to ['asah](../../strongs/h/h6213.md) for you great [gᵊḏûlâ](../../strongs/h/h1420.md) and [yare'](../../strongs/h/h3372.md), for thy ['erets](../../strongs/h/h776.md), [paniym](../../strongs/h/h6440.md) thy ['am](../../strongs/h/h5971.md), which thou [pāḏâ](../../strongs/h/h6299.md) to thee from [Mitsrayim](../../strongs/h/h4714.md), from the [gowy](../../strongs/h/h1471.md) and their ['Elohiym](../../strongs/h/h430.md)?

<a name="2samuel_7_24"></a>2Samuel 7:24

For thou hast [kuwn](../../strongs/h/h3559.md) to thyself thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) to be an ['am](../../strongs/h/h5971.md) unto thee ['owlam](../../strongs/h/h5769.md): and thou, [Yĕhovah](../../strongs/h/h3068.md), art become their ['Elohiym](../../strongs/h/h430.md).

<a name="2samuel_7_25"></a>2Samuel 7:25

And now, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), the [dabar](../../strongs/h/h1697.md) that thou hast [dabar](../../strongs/h/h1696.md) concerning thy ['ebed](../../strongs/h/h5650.md), and concerning his [bayith](../../strongs/h/h1004.md), [quwm](../../strongs/h/h6965.md) it ['owlam](../../strongs/h/h5769.md), and ['asah](../../strongs/h/h6213.md) as thou hast [dabar](../../strongs/h/h1696.md).

<a name="2samuel_7_26"></a>2Samuel 7:26

And let thy [shem](../../strongs/h/h8034.md) be [gāḏal](../../strongs/h/h1431.md) ['owlam](../../strongs/h/h5769.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is the ['Elohiym](../../strongs/h/h430.md) over [Yisra'el](../../strongs/h/h3478.md): and let the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) be [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="2samuel_7_27"></a>2Samuel 7:27

For thou, [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), hast [gālâ](../../strongs/h/h1540.md) ['ozen](../../strongs/h/h241.md) to thy ['ebed](../../strongs/h/h5650.md), ['āmar](../../strongs/h/h559.md), I will [bānâ](../../strongs/h/h1129.md) thee a [bayith](../../strongs/h/h1004.md): therefore hath thy ['ebed](../../strongs/h/h5650.md) [māṣā'](../../strongs/h/h4672.md) in his [leb](../../strongs/h/h3820.md) to [palal](../../strongs/h/h6419.md) this [tĕphillah](../../strongs/h/h8605.md) unto thee.

<a name="2samuel_7_28"></a>2Samuel 7:28

And now, ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), thou art that ['Elohiym](../../strongs/h/h430.md), and thy [dabar](../../strongs/h/h1697.md) be ['emeth](../../strongs/h/h571.md), and thou hast [dabar](../../strongs/h/h1696.md) this [towb](../../strongs/h/h2896.md) unto thy ['ebed](../../strongs/h/h5650.md):

<a name="2samuel_7_29"></a>2Samuel 7:29

Therefore now let it [yā'al](../../strongs/h/h2974.md) thee to [barak](../../strongs/h/h1288.md) the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md), that it may continue ['owlam](../../strongs/h/h5769.md) [paniym](../../strongs/h/h6440.md) thee: for thou, ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), hast [dabar](../../strongs/h/h1696.md) it: and with thy [bĕrakah](../../strongs/h/h1293.md) let the [bayith](../../strongs/h/h1004.md) of thy ['ebed](../../strongs/h/h5650.md) be [barak](../../strongs/h/h1288.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 6](2samuel_6.md) - [2Samuel 8](2samuel_8.md)