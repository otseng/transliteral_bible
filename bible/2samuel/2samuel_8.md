# [2Samuel 8](https://www.blueletterbible.org/kjv/2samuel/8)

<a name="2samuel_8_1"></a>2Samuel 8:1

And ['aḥar](../../strongs/h/h310.md) this it came to pass, that [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [kānaʿ](../../strongs/h/h3665.md) them: and [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) [meṯeḡ hā'ammâ](../../strongs/h/h4965.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="2samuel_8_2"></a>2Samuel 8:2

And he [nakah](../../strongs/h/h5221.md) [Mô'āḇ](../../strongs/h/h4124.md), and [māḏaḏ](../../strongs/h/h4058.md) them with a [chebel](../../strongs/h/h2256.md), casting them [shakab](../../strongs/h/h7901.md) to the ['erets](../../strongs/h/h776.md); even with two [chebel](../../strongs/h/h2256.md) [māḏaḏ](../../strongs/h/h4058.md) he to put to [muwth](../../strongs/h/h4191.md), and with one [mᵊlō'](../../strongs/h/h4393.md) [chebel](../../strongs/h/h2256.md) to keep [ḥāyâ](../../strongs/h/h2421.md). And so the [Mô'āḇ](../../strongs/h/h4124.md) became [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md), and [nasa'](../../strongs/h/h5375.md) [minchah](../../strongs/h/h4503.md).

<a name="2samuel_8_3"></a>2Samuel 8:3

[Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) also [HăḏaḏʿEzer](../../strongs/h/h1909.md), the [ben](../../strongs/h/h1121.md) of [rᵊḥōḇ](../../strongs/h/h7340.md), [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md), as he [yālaḵ](../../strongs/h/h3212.md) to [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) at the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="2samuel_8_4"></a>2Samuel 8:4

And [Dāviḏ](../../strongs/h/h1732.md) [lāḵaḏ](../../strongs/h/h3920.md) from him a thousand chariots, and seven hundred [pārāš](../../strongs/h/h6571.md), and twenty thousand ['iysh](../../strongs/h/h376.md) [raḡlî](../../strongs/h/h7273.md): and [Dāviḏ](../../strongs/h/h1732.md) [ʿāqar](../../strongs/h/h6131.md) all the [reḵeḇ](../../strongs/h/h7393.md) horses, but [yāṯar](../../strongs/h/h3498.md) of them for an hundred [reḵeḇ](../../strongs/h/h7393.md).

<a name="2samuel_8_5"></a>2Samuel 8:5

And when the ['Ărām](../../strongs/h/h758.md) of [Dammeśeq](../../strongs/h/h1834.md) [bow'](../../strongs/h/h935.md) to [ʿāzar](../../strongs/h/h5826.md) [HăḏaḏʿEzer](../../strongs/h/h1909.md) [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md), [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) of the ['Ărām](../../strongs/h/h758.md) two and twenty thousand ['iysh](../../strongs/h/h376.md).

<a name="2samuel_8_6"></a>2Samuel 8:6

Then [Dāviḏ](../../strongs/h/h1732.md) [śûm](../../strongs/h/h7760.md) [nᵊṣîḇ](../../strongs/h/h5333.md) in ['Ărām](../../strongs/h/h758.md) of [Dammeśeq](../../strongs/h/h1834.md): and the ['Ărām](../../strongs/h/h758.md) became ['ebed](../../strongs/h/h5650.md) to [Dāviḏ](../../strongs/h/h1732.md), and [nasa'](../../strongs/h/h5375.md) [minchah](../../strongs/h/h4503.md). And [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Dāviḏ](../../strongs/h/h1732.md) whithersoever he [halak](../../strongs/h/h1980.md).

<a name="2samuel_8_7"></a>2Samuel 8:7

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) the [šeleṭ](../../strongs/h/h7982.md) of [zāhāḇ](../../strongs/h/h2091.md) that were on the ['ebed](../../strongs/h/h5650.md) of [HăḏaḏʿEzer](../../strongs/h/h1909.md), and [bow'](../../strongs/h/h935.md) them to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_8_8"></a>2Samuel 8:8

And from [Beṭaḥ](../../strongs/h/h984.md), and from [Bērôṯâ](../../strongs/h/h1268.md), [ʿîr](../../strongs/h/h5892.md) of [HăḏaḏʿEzer](../../strongs/h/h1909.md), [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="2samuel_8_9"></a>2Samuel 8:9

When [TōʿÛ](../../strongs/h/h8583.md) [melek](../../strongs/h/h4428.md) of [Ḥămāṯ](../../strongs/h/h2574.md) [shama'](../../strongs/h/h8085.md) that [Dāviḏ](../../strongs/h/h1732.md) had [nakah](../../strongs/h/h5221.md) all the [ḥayil](../../strongs/h/h2428.md) of [HăḏaḏʿEzer](../../strongs/h/h1909.md),

<a name="2samuel_8_10"></a>2Samuel 8:10

Then [TōʿÛ](../../strongs/h/h8583.md) [shalach](../../strongs/h/h7971.md) [Yôrām](../../strongs/h/h3141.md) his [ben](../../strongs/h/h1121.md) unto [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), to [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) him, and to [barak](../../strongs/h/h1288.md) him, because he had [lāḥam](../../strongs/h/h3898.md) against [HăḏaḏʿEzer](../../strongs/h/h1909.md), and [nakah](../../strongs/h/h5221.md) him: for [HăḏaḏʿEzer](../../strongs/h/h1909.md) had ['iysh](../../strongs/h/h376.md) [milḥāmâ](../../strongs/h/h4421.md) with [TōʿÛ](../../strongs/h/h8583.md). And Joram brought with [yad](../../strongs/h/h3027.md) [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md):

<a name="2samuel_8_11"></a>2Samuel 8:11

Which also [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) did [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md), with the [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md) that he had [qadash](../../strongs/h/h6942.md) of all [gowy](../../strongs/h/h1471.md) which he [kāḇaš](../../strongs/h/h3533.md);

<a name="2samuel_8_12"></a>2Samuel 8:12

Of ['Ărām](../../strongs/h/h758.md), and of [Mô'āḇ](../../strongs/h/h4124.md), and of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and of the [Pᵊlištî](../../strongs/h/h6430.md), and of [ʿĂmālēq](../../strongs/h/h6002.md), and of the [šālāl](../../strongs/h/h7998.md) of [HăḏaḏʿEzer](../../strongs/h/h1909.md), [ben](../../strongs/h/h1121.md) of [rᵊḥōḇ](../../strongs/h/h7340.md), [melek](../../strongs/h/h4428.md) of [Ṣôḇā'](../../strongs/h/h6678.md).

<a name="2samuel_8_13"></a>2Samuel 8:13

And [Dāviḏ](../../strongs/h/h1732.md) ['asah](../../strongs/h/h6213.md) him a [shem](../../strongs/h/h8034.md) when he [shuwb](../../strongs/h/h7725.md) from [nakah](../../strongs/h/h5221.md) of the ['Ărām](../../strongs/h/h758.md) in the [gay'](../../strongs/h/h1516.md) of [melaḥ](../../strongs/h/h4417.md), being eighteen thousand men.

<a name="2samuel_8_14"></a>2Samuel 8:14

And he [śûm](../../strongs/h/h7760.md) [nᵊṣîḇ](../../strongs/h/h5333.md) in ['Ĕḏōm](../../strongs/h/h123.md); throughout all ['Ĕḏōm](../../strongs/h/h123.md) [śûm](../../strongs/h/h7760.md) he [nᵊṣîḇ](../../strongs/h/h5333.md), and all they of ['Ĕḏōm](../../strongs/h/h123.md) became [Dāviḏ](../../strongs/h/h1732.md) ['ebed](../../strongs/h/h5650.md). And [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Dāviḏ](../../strongs/h/h1732.md) whithersoever he [halak](../../strongs/h/h1980.md).

<a name="2samuel_8_15"></a>2Samuel 8:15

And [Dāviḏ](../../strongs/h/h1732.md) [mālaḵ](../../strongs/h/h4427.md) over all [Yisra'el](../../strongs/h/h3478.md); and [Dāviḏ](../../strongs/h/h1732.md) ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md) unto all his ['am](../../strongs/h/h5971.md).

<a name="2samuel_8_16"></a>2Samuel 8:16

And [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) was over the [tsaba'](../../strongs/h/h6635.md); and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîlûḏ](../../strongs/h/h286.md) was [zakar](../../strongs/h/h2142.md);

<a name="2samuel_8_17"></a>2Samuel 8:17

And [Ṣāḏôq](../../strongs/h/h6659.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîṭûḇ](../../strongs/h/h285.md), and ['Ăḥîmeleḵ](../../strongs/h/h288.md) the [ben](../../strongs/h/h1121.md) of ['Eḇyāṯār](../../strongs/h/h54.md), were the [kōhēn](../../strongs/h/h3548.md); and [Śᵊrāyâ](../../strongs/h/h8304.md) was the [sāp̄ar](../../strongs/h/h5608.md);

<a name="2samuel_8_18"></a>2Samuel 8:18

And [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) was over both the [kᵊrēṯî](../../strongs/h/h3774.md) and the [pᵊlēṯî](../../strongs/h/h6432.md); and [Dāviḏ](../../strongs/h/h1732.md) [ben](../../strongs/h/h1121.md) were [kōhēn](../../strongs/h/h3548.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 7](2samuel_7.md) - [2Samuel 9](2samuel_9.md)