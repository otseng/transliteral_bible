# [2Samuel 21](https://www.blueletterbible.org/kjv/2samuel/21)

<a name="2samuel_21_1"></a>2Samuel 21:1

Then there was a [rāʿāḇ](../../strongs/h/h7458.md) in the [yowm](../../strongs/h/h3117.md) of [Dāviḏ](../../strongs/h/h1732.md) three [šānâ](../../strongs/h/h8141.md), [šānâ](../../strongs/h/h8141.md) ['aḥar](../../strongs/h/h310.md) [šānâ](../../strongs/h/h8141.md); and [Dāviḏ](../../strongs/h/h1732.md) [bāqaš](../../strongs/h/h1245.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md). And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), It is for [Šā'ûl](../../strongs/h/h7586.md), and for his [dam](../../strongs/h/h1818.md) [bayith](../../strongs/h/h1004.md), because he [muwth](../../strongs/h/h4191.md) the [Giḇʿōnî](../../strongs/h/h1393.md).

<a name="2samuel_21_2"></a>2Samuel 21:2

And the [melek](../../strongs/h/h4428.md) [qara'](../../strongs/h/h7121.md) the [Giḇʿōnî](../../strongs/h/h1393.md), and ['āmar](../../strongs/h/h559.md) unto them; (now the [Giḇʿōnî](../../strongs/h/h1393.md) were not of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), but of the [yeṯer](../../strongs/h/h3499.md) of the ['Ĕmōrî](../../strongs/h/h567.md); and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) had [shaba'](../../strongs/h/h7650.md) unto them: and [Šā'ûl](../../strongs/h/h7586.md) [bāqaš](../../strongs/h/h1245.md) to [nakah](../../strongs/h/h5221.md) them in his [qānā'](../../strongs/h/h7065.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md).)

<a name="2samuel_21_3"></a>2Samuel 21:3

Wherefore [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto the [Giḇʿōnî](../../strongs/h/h1393.md), What shall I ['asah](../../strongs/h/h6213.md) for you? and wherewith shall I make the [kāp̄ar](../../strongs/h/h3722.md), that ye may [barak](../../strongs/h/h1288.md) the [nachalah](../../strongs/h/h5159.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="2samuel_21_4"></a>2Samuel 21:4

And the [Giḇʿōnî](../../strongs/h/h1393.md) ['āmar](../../strongs/h/h559.md) unto him, We will have no [keceph](../../strongs/h/h3701.md) nor [zāhāḇ](../../strongs/h/h2091.md) of [Šā'ûl](../../strongs/h/h7586.md), nor of his [bayith](../../strongs/h/h1004.md); neither for us shalt thou [muwth](../../strongs/h/h4191.md) any ['iysh](../../strongs/h/h376.md) in [Yisra'el](../../strongs/h/h3478.md). And he ['āmar](../../strongs/h/h559.md), What ye shall ['āmar](../../strongs/h/h559.md), that will I ['asah](../../strongs/h/h6213.md) for you.

<a name="2samuel_21_5"></a>2Samuel 21:5

And they ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), The ['iysh](../../strongs/h/h376.md) that [kalah](../../strongs/h/h3615.md) us, and that [dāmâ](../../strongs/h/h1819.md) against us that we should be [šāmaḏ](../../strongs/h/h8045.md) from [yatsab](../../strongs/h/h3320.md) in any of the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md),

<a name="2samuel_21_6"></a>2Samuel 21:6

Let seven ['enowsh](../../strongs/h/h582.md) of his [ben](../../strongs/h/h1121.md) be [nathan](../../strongs/h/h5414.md) unto us, and we will [yāqaʿ](../../strongs/h/h3363.md) them unto [Yĕhovah](../../strongs/h/h3068.md) in [giḇʿâ](../../strongs/h/h1390.md) of [Šā'ûl](../../strongs/h/h7586.md), whom [Yĕhovah](../../strongs/h/h3068.md) did [bāḥîr](../../strongs/h/h972.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), I will [nathan](../../strongs/h/h5414.md) them.

<a name="2samuel_21_7"></a>2Samuel 21:7

But the [melek](../../strongs/h/h4428.md) [ḥāmal](../../strongs/h/h2550.md) [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md), because of [Yĕhovah](../../strongs/h/h3068.md) [šᵊḇûʿâ](../../strongs/h/h7621.md) that was between them, between [Dāviḏ](../../strongs/h/h1732.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of [Šā'ûl](../../strongs/h/h7586.md).

<a name="2samuel_21_8"></a>2Samuel 21:8

But the [melek](../../strongs/h/h4428.md) [laqach](../../strongs/h/h3947.md) the two [ben](../../strongs/h/h1121.md) of [Riṣpâ](../../strongs/h/h7532.md) the [bath](../../strongs/h/h1323.md) of ['Ayyâ](../../strongs/h/h345.md), whom she [yalad](../../strongs/h/h3205.md) unto [Šā'ûl](../../strongs/h/h7586.md), ['Armōnî](../../strongs/h/h764.md) and [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md); and the five [ben](../../strongs/h/h1121.md) of [Mîḵāl](../../strongs/h/h4324.md) the [bath](../../strongs/h/h1323.md) of [Šā'ûl](../../strongs/h/h7586.md), whom she [yalad](../../strongs/h/h3205.md) for [ʿAḏrî'Ēl](../../strongs/h/h5741.md) the [ben](../../strongs/h/h1121.md) of [Barzillay](../../strongs/h/h1271.md) the [mᵊḥōlāṯî](../../strongs/h/h4259.md):

<a name="2samuel_21_9"></a>2Samuel 21:9

And he [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of the [Giḇʿōnî](../../strongs/h/h1393.md), and they [yāqaʿ](../../strongs/h/h3363.md) them in the [har](../../strongs/h/h2022.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and they [naphal](../../strongs/h/h5307.md) all seven [yaḥaḏ](../../strongs/h/h3162.md), and were put to [muwth](../../strongs/h/h4191.md) in the [yowm](../../strongs/h/h3117.md) of [qāṣîr](../../strongs/h/h7105.md), in the [ri'šôn](../../strongs/h/h7223.md) days, in the [tᵊḥillâ](../../strongs/h/h8462.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [qāṣîr](../../strongs/h/h7105.md).

<a name="2samuel_21_10"></a>2Samuel 21:10

And [Riṣpâ](../../strongs/h/h7532.md) the [bath](../../strongs/h/h1323.md) of ['Ayyâ](../../strongs/h/h345.md) [laqach](../../strongs/h/h3947.md) [śaq](../../strongs/h/h8242.md), and [natah](../../strongs/h/h5186.md) it for her upon the [tsuwr](../../strongs/h/h6697.md), from the [tᵊḥillâ](../../strongs/h/h8462.md) of [qāṣîr](../../strongs/h/h7105.md) until [mayim](../../strongs/h/h4325.md) [nāṯaḵ](../../strongs/h/h5413.md) upon them out of [shamayim](../../strongs/h/h8064.md), and [nathan](../../strongs/h/h5414.md) neither the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) to [nuwach](../../strongs/h/h5117.md) on them by [yômām](../../strongs/h/h3119.md), nor the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) by [layil](../../strongs/h/h3915.md).

<a name="2samuel_21_11"></a>2Samuel 21:11

And it was [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md) what [Riṣpâ](../../strongs/h/h7532.md) the [bath](../../strongs/h/h1323.md) of ['Ayyâ](../../strongs/h/h345.md), the [pîleḡeš](../../strongs/h/h6370.md) of [Šā'ûl](../../strongs/h/h7586.md), had ['asah](../../strongs/h/h6213.md).

<a name="2samuel_21_12"></a>2Samuel 21:12

And [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) and [laqach](../../strongs/h/h3947.md) the ['etsem](../../strongs/h/h6106.md) of [Šā'ûl](../../strongs/h/h7586.md) and the ['etsem](../../strongs/h/h6106.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md) from the [baʿal](../../strongs/h/h1167.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md), which had [ganab](../../strongs/h/h1589.md) them from the [rᵊḥōḇ](../../strongs/h/h7339.md) of [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md), where the [Pᵊlištî](../../strongs/h/h6430.md) had [tālā'](../../strongs/h/h8511.md) [tālâ](../../strongs/h/h8518.md) them, [yowm](../../strongs/h/h3117.md) the [Pᵊlištî](../../strongs/h/h6430.md) had [nakah](../../strongs/h/h5221.md) [Šā'ûl](../../strongs/h/h7586.md) in [Gilbōaʿ](../../strongs/h/h1533.md):

<a name="2samuel_21_13"></a>2Samuel 21:13

And he [ʿālâ](../../strongs/h/h5927.md) from thence the ['etsem](../../strongs/h/h6106.md) of [Šā'ûl](../../strongs/h/h7586.md) and the ['etsem](../../strongs/h/h6106.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md); and they ['āsap̄](../../strongs/h/h622.md) the ['etsem](../../strongs/h/h6106.md) of them that were [yāqaʿ](../../strongs/h/h3363.md).

<a name="2samuel_21_14"></a>2Samuel 21:14

And the ['etsem](../../strongs/h/h6106.md) of [Šā'ûl](../../strongs/h/h7586.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md) [qāḇar](../../strongs/h/h6912.md) they in the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md) in [Ṣēlāʿ](../../strongs/h/h6762.md), in the [qeber](../../strongs/h/h6913.md) of [Qîš](../../strongs/h/h7027.md) his ['ab](../../strongs/h/h1.md): and they ['asah](../../strongs/h/h6213.md) all that the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md). And ['aḥar](../../strongs/h/h310.md) that ['Elohiym](../../strongs/h/h430.md) was [ʿāṯar](../../strongs/h/h6279.md) for the ['erets](../../strongs/h/h776.md).

<a name="2samuel_21_15"></a>2Samuel 21:15

Moreover the [Pᵊlištî](../../strongs/h/h6430.md) had yet [milḥāmâ](../../strongs/h/h4421.md) again with [Yisra'el](../../strongs/h/h3478.md); and [Dāviḏ](../../strongs/h/h1732.md) [yarad](../../strongs/h/h3381.md), and his ['ebed](../../strongs/h/h5650.md) with him, and [lāḥam](../../strongs/h/h3898.md) against the [Pᵊlištî](../../strongs/h/h6430.md): and [Dāviḏ](../../strongs/h/h1732.md) waxed ['uwph](../../strongs/h/h5774.md).

<a name="2samuel_21_16"></a>2Samuel 21:16

And [Yišḇô Ḇᵊnōḇ](../../strongs/h/h3430.md), which was of the [yālîḏ](../../strongs/h/h3211.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md), the [mišqāl](../../strongs/h/h4948.md) of whose [qayin](../../strongs/h/h7013.md) three hundred of [nᵊḥšeṯ](../../strongs/h/h5178.md) in [mišqāl](../../strongs/h/h4948.md), he being [ḥāḡar](../../strongs/h/h2296.md) with a [ḥāḏāš](../../strongs/h/h2319.md), ['āmar](../../strongs/h/h559.md) to have [nakah](../../strongs/h/h5221.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_21_17"></a>2Samuel 21:17

But ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) [ʿāzar](../../strongs/h/h5826.md) him, and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md), and [muwth](../../strongs/h/h4191.md) him. Then the ['enowsh](../../strongs/h/h582.md) of [Dāviḏ](../../strongs/h/h1732.md) [shaba'](../../strongs/h/h7650.md) unto him, ['āmar](../../strongs/h/h559.md), Thou shalt [yāṣā'](../../strongs/h/h3318.md) no more out with us to [milḥāmâ](../../strongs/h/h4421.md), that thou [kāḇâ](../../strongs/h/h3518.md) not the [nîr](../../strongs/h/h5216.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_21_18"></a>2Samuel 21:18

And it came to pass ['aḥar](../../strongs/h/h310.md), that there was again a [milḥāmâ](../../strongs/h/h4421.md) with the [Pᵊlištî](../../strongs/h/h6430.md) at [Gōḇ](../../strongs/h/h1359.md): then [Sibḵay](../../strongs/h/h5444.md) the [Ḥušāṯî](../../strongs/h/h2843.md) [nakah](../../strongs/h/h5221.md) [Sap̄](../../strongs/h/h5593.md), which was of the [yālîḏ](../../strongs/h/h3211.md) of the [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="2samuel_21_19"></a>2Samuel 21:19

And there was again a [milḥāmâ](../../strongs/h/h4421.md) in [Gōḇ](../../strongs/h/h1359.md) with the [Pᵊlištî](../../strongs/h/h6430.md), where ['Elḥānān](../../strongs/h/h445.md) the [ben](../../strongs/h/h1121.md) of [YaʿĂrê 'Ōrḡîm](../../strongs/h/h3296.md), a [bêṯ-hallaḥmî](../../strongs/h/h1022.md), [nakah](../../strongs/h/h5221.md) of [Gālyaṯ](../../strongs/h/h1555.md) the [Gitî](../../strongs/h/h1663.md), the ['ets](../../strongs/h/h6086.md) of whose [ḥănîṯ](../../strongs/h/h2595.md) was like an ['āraḡ](../../strongs/h/h707.md) [mānôr](../../strongs/h/h4500.md).

<a name="2samuel_21_20"></a>2Samuel 21:20

And there was yet a [milḥāmâ](../../strongs/h/h4421.md) in [Gaṯ](../../strongs/h/h1661.md), where was an ['iysh](../../strongs/h/h376.md) of great [māḏôn](../../strongs/h/h4067.md) [maḏ](../../strongs/h/h4055.md), that had on every [yad](../../strongs/h/h3027.md) six ['etsba'](../../strongs/h/h676.md), and on every [regel](../../strongs/h/h7272.md) six ['etsba'](../../strongs/h/h676.md), four and twenty in [mispār](../../strongs/h/h4557.md); and he also was [yalad](../../strongs/h/h3205.md) to the [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="2samuel_21_21"></a>2Samuel 21:21

And when he [ḥārap̄](../../strongs/h/h2778.md) [Yisra'el](../../strongs/h/h3478.md), [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of [ŠimʿĀ'](../../strongs/h/h8092.md) the ['ach](../../strongs/h/h251.md) of [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) him.

<a name="2samuel_21_22"></a>2Samuel 21:22

These four were [yalad](../../strongs/h/h3205.md) to the [rᵊp̄ā'îm](../../strongs/h/h7497.md) in [Gaṯ](../../strongs/h/h1661.md), and [naphal](../../strongs/h/h5307.md) by the [yad](../../strongs/h/h3027.md) of [Dāviḏ](../../strongs/h/h1732.md), and by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 20](2samuel_20.md) - [2Samuel 22](2samuel_22.md)