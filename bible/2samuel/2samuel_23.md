# [2Samuel 23](https://www.blueletterbible.org/kjv/2samuel/23)

<a name="2samuel_23_1"></a>2Samuel 23:1

Now these be the ['aḥărôn](../../strongs/h/h314.md) [dabar](../../strongs/h/h1697.md) of [Dāviḏ](../../strongs/h/h1732.md). [Dāviḏ](../../strongs/h/h1732.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md) [nᵊ'um](../../strongs/h/h5002.md), and the [geḇer](../../strongs/h/h1397.md) who was [quwm](../../strongs/h/h6965.md) on [ʿal](../../strongs/h/h5920.md), the [mashiyach](../../strongs/h/h4899.md) of the ['Elohiym](../../strongs/h/h430.md) of [Ya'aqob](../../strongs/h/h3290.md), and the [na'iym](../../strongs/h/h5273.md) [zᵊmîr](../../strongs/h/h2158.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md),

<a name="2samuel_23_2"></a>2Samuel 23:2

The [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) by me, and his [millâ](../../strongs/h/h4405.md) was in my [lashown](../../strongs/h/h3956.md).

<a name="2samuel_23_3"></a>2Samuel 23:3

The ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), the [tsuwr](../../strongs/h/h6697.md) of [Yisra'el](../../strongs/h/h3478.md) [dabar](../../strongs/h/h1696.md) to me, He that [mashal](../../strongs/h/h4910.md) over ['āḏām](../../strongs/h/h120.md) must be [tsaddiyq](../../strongs/h/h6662.md), [mashal](../../strongs/h/h4910.md) in the [yir'ah](../../strongs/h/h3374.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2samuel_23_4"></a>2Samuel 23:4

And he shall be as the ['owr](../../strongs/h/h216.md) of the [boqer](../../strongs/h/h1242.md), when the [šemeš](../../strongs/h/h8121.md) [zāraḥ](../../strongs/h/h2224.md), even a [boqer](../../strongs/h/h1242.md) without ['ab](../../strongs/h/h5645.md); as the [deše'](../../strongs/h/h1877.md) of the ['erets](../../strongs/h/h776.md) by [nogahh](../../strongs/h/h5051.md) after [māṭār](../../strongs/h/h4306.md).

<a name="2samuel_23_5"></a>2Samuel 23:5

Although my [bayith](../../strongs/h/h1004.md) be not so with ['el](../../strongs/h/h410.md); yet he hath [śûm](../../strongs/h/h7760.md) with me an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md), ['arak](../../strongs/h/h6186.md) in all things, and [shamar](../../strongs/h/h8104.md): for this is all my [yesha'](../../strongs/h/h3468.md), and all my [chephets](../../strongs/h/h2656.md), although he make it not to [ṣāmaḥ](../../strongs/h/h6779.md).

<a name="2samuel_23_6"></a>2Samuel 23:6

But of [beliya'al](../../strongs/h/h1100.md) shall be all of them as [qowts](../../strongs/h/h6975.md) thrust [nāḏaḏ](../../strongs/h/h5074.md), because they cannot be [laqach](../../strongs/h/h3947.md) with [yad](../../strongs/h/h3027.md):

<a name="2samuel_23_7"></a>2Samuel 23:7

But the ['iysh](../../strongs/h/h376.md) that shall [naga'](../../strongs/h/h5060.md) them must be [mālā'](../../strongs/h/h4390.md) with [barzel](../../strongs/h/h1270.md) and the ['ets](../../strongs/h/h6086.md) of a [ḥănîṯ](../../strongs/h/h2595.md); and they shall be [śārap̄](../../strongs/h/h8313.md) [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md) in the [šeḇeṯ](../../strongs/h/h7675.md).

<a name="2samuel_23_8"></a>2Samuel 23:8

These be the [shem](../../strongs/h/h8034.md) of the [gibôr](../../strongs/h/h1368.md) whom [Dāviḏ](../../strongs/h/h1732.md) had: The [Taḥkᵊmōnî](../../strongs/h/h8461.md) that [yashab](../../strongs/h/h3427.md) in the [šeḇeṯ](../../strongs/h/h7675.md) [Yōšēḇ Baššeḇeṯ](../../strongs/h/h3429.md) , [ro'sh](../../strongs/h/h7218.md) among the [šālîš](../../strongs/h/h7991.md); the same was [ʿĂḏînô](../../strongs/h/h5722.md) the [ʿēṣen](../../strongs/h/h6112.md): against eight hundred, whom he [ḥālāl](../../strongs/h/h2491.md) at one time.

<a name="2samuel_23_9"></a>2Samuel 23:9

And ['aḥar](../../strongs/h/h310.md) him was ['Elʿāzār](../../strongs/h/h499.md) the [ben](../../strongs/h/h1121.md) of [Dôḏô](../../strongs/h/h1734.md) the ['Ăḥôḥî](../../strongs/h/h266.md), one of the three [gibôr](../../strongs/h/h1368.md) with [Dāviḏ](../../strongs/h/h1732.md), when they [ḥārap̄](../../strongs/h/h2778.md) the [Pᵊlištî](../../strongs/h/h6430.md) that were there ['āsap̄](../../strongs/h/h622.md) to [milḥāmâ](../../strongs/h/h4421.md), and the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) were [ʿālâ](../../strongs/h/h5927.md):

<a name="2samuel_23_10"></a>2Samuel 23:10

He [quwm](../../strongs/h/h6965.md), and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md) until his [yad](../../strongs/h/h3027.md) was [yaga'](../../strongs/h/h3021.md), and his [yad](../../strongs/h/h3027.md) [dāḇaq](../../strongs/h/h1692.md) unto the [chereb](../../strongs/h/h2719.md): and [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [tᵊšûʿâ](../../strongs/h/h8668.md) that [yowm](../../strongs/h/h3117.md); and the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) ['aḥar](../../strongs/h/h310.md) him only to [pāšaṭ](../../strongs/h/h6584.md).

<a name="2samuel_23_11"></a>2Samuel 23:11

And ['aḥar](../../strongs/h/h310.md) him was [Šammâ](../../strongs/h/h8048.md) the [ben](../../strongs/h/h1121.md) of ['Āḡē'](../../strongs/h/h89.md) the [Hărārî](../../strongs/h/h2043.md). And the [Pᵊlištî](../../strongs/h/h6430.md) were ['āsap̄](../../strongs/h/h622.md) into a [chay](../../strongs/h/h2416.md), where was a [ḥelqâ](../../strongs/h/h2513.md) of [sadeh](../../strongs/h/h7704.md) [mālē'](../../strongs/h/h4392.md) of [ʿāḏāš](../../strongs/h/h5742.md): and the ['am](../../strongs/h/h5971.md) [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="2samuel_23_12"></a>2Samuel 23:12

But he [yatsab](../../strongs/h/h3320.md) in the midst of the [ḥelqâ](../../strongs/h/h2513.md), and [natsal](../../strongs/h/h5337.md) it, and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md): and [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [tᵊšûʿâ](../../strongs/h/h8668.md).

<a name="2samuel_23_13"></a>2Samuel 23:13

And three of the thirty [ro'sh](../../strongs/h/h7218.md) [yarad](../../strongs/h/h3381.md), and [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md) in the [qāṣîr](../../strongs/h/h7105.md) unto the [mᵊʿārâ](../../strongs/h/h4631.md) of [ʿĂḏullām](../../strongs/h/h5725.md): and the [chay](../../strongs/h/h2416.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ḥānâ](../../strongs/h/h2583.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="2samuel_23_14"></a>2Samuel 23:14

And [Dāviḏ](../../strongs/h/h1732.md) was then in a [matsuwd](../../strongs/h/h4686.md), and the [maṣṣāḇ](../../strongs/h/h4673.md) of the [Pᵊlištî](../../strongs/h/h6430.md) was then in [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="2samuel_23_15"></a>2Samuel 23:15

And [Dāviḏ](../../strongs/h/h1732.md) ['āvâ](../../strongs/h/h183.md), and ['āmar](../../strongs/h/h559.md), Oh that one would [šāqâ](../../strongs/h/h8248.md) me of the [mayim](../../strongs/h/h4325.md) of the [bowr](../../strongs/h/h953.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), which is by the [sha'ar](../../strongs/h/h8179.md)!

<a name="2samuel_23_16"></a>2Samuel 23:16

And the three [gibôr](../../strongs/h/h1368.md) [bāqaʿ](../../strongs/h/h1234.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and [šā'aḇ](../../strongs/h/h7579.md) [mayim](../../strongs/h/h4325.md) out of the [bowr](../../strongs/h/h953.md) of [Bêṯ leḥem](../../strongs/h/h1035.md), that was by the [sha'ar](../../strongs/h/h8179.md), and [nasa'](../../strongs/h/h5375.md) it, and [bow'](../../strongs/h/h935.md) it to [Dāviḏ](../../strongs/h/h1732.md): nevertheless he ['āḇâ](../../strongs/h/h14.md) not [šāṯâ](../../strongs/h/h8354.md) thereof, but [nacak](../../strongs/h/h5258.md) it unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="2samuel_23_17"></a>2Samuel 23:17

And he ['āmar](../../strongs/h/h559.md), [ḥālîlâ](../../strongs/h/h2486.md) from me, [Yĕhovah](../../strongs/h/h3068.md), that I should ['asah](../../strongs/h/h6213.md) this: is not this the [dam](../../strongs/h/h1818.md) of the ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) of their [nephesh](../../strongs/h/h5315.md)? therefore he ['āḇâ](../../strongs/h/h14.md) not [šāṯâ](../../strongs/h/h8354.md) it. These things ['asah](../../strongs/h/h6213.md) these three [gibôr](../../strongs/h/h1368.md).

<a name="2samuel_23_18"></a>2Samuel 23:18

And ['Ăḇîšay](../../strongs/h/h52.md), the ['ach](../../strongs/h/h251.md) of [Yô'āḇ](../../strongs/h/h3097.md), the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), was [ro'sh](../../strongs/h/h7218.md) among three. And he [ʿûr](../../strongs/h/h5782.md) his [ḥănîṯ](../../strongs/h/h2595.md) against three hundred, and [ḥālāl](../../strongs/h/h2491.md) them, and had the [shem](../../strongs/h/h8034.md) among three.

<a name="2samuel_23_19"></a>2Samuel 23:19

Was he not most [kabad](../../strongs/h/h3513.md) of three? therefore he was their [śar](../../strongs/h/h8269.md): howbeit he [bow'](../../strongs/h/h935.md) not unto the first three.

<a name="2samuel_23_20"></a>2Samuel 23:20

And [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), the [ben](../../strongs/h/h1121.md) of a [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) ['îš ḥayil](../../strongs/h/h381.md) , of [Qaḇṣᵊ'Ēl](../../strongs/h/h6909.md), who had done [rab](../../strongs/h/h7227.md) [pōʿal](../../strongs/h/h6467.md), he [nakah](../../strongs/h/h5221.md) two ['ărî'ēl](../../strongs/h/h739.md) of [Mô'āḇ](../../strongs/h/h4124.md): he [yarad](../../strongs/h/h3381.md) also and [nakah](../../strongs/h/h5221.md) an ['ariy](../../strongs/h/h738.md) in the midst of a [bowr](../../strongs/h/h953.md) in [yowm](../../strongs/h/h3117.md) of [šeleḡ](../../strongs/h/h7950.md):

<a name="2samuel_23_21"></a>2Samuel 23:21

And he [harag](../../strongs/h/h2026.md) an [Miṣrî](../../strongs/h/h4713.md), a [mar'ê](../../strongs/h/h4758.md) ['iysh](../../strongs/h/h376.md): and the [Miṣrî](../../strongs/h/h4713.md) had a [ḥănîṯ](../../strongs/h/h2595.md) in his [yad](../../strongs/h/h3027.md); but he [yarad](../../strongs/h/h3381.md) to him with a [shebet](../../strongs/h/h7626.md), and [gāzal](../../strongs/h/h1497.md) the [ḥănîṯ](../../strongs/h/h2595.md) out of the [Miṣrî](../../strongs/h/h4713.md) [yad](../../strongs/h/h3027.md), and [nakah](../../strongs/h/h5221.md) him with his own [ḥănîṯ](../../strongs/h/h2595.md).

<a name="2samuel_23_22"></a>2Samuel 23:22

These things ['asah](../../strongs/h/h6213.md) [Bᵊnāyâ](../../strongs/h/h1141.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), and had the [shem](../../strongs/h/h8034.md) among three [gibôr](../../strongs/h/h1368.md).

<a name="2samuel_23_23"></a>2Samuel 23:23

He was more [kabad](../../strongs/h/h3513.md) than the thirty, but he [bow'](../../strongs/h/h935.md) not to the first three. And [Dāviḏ](../../strongs/h/h1732.md) [śûm](../../strongs/h/h7760.md) him over his [mišmaʿaṯ](../../strongs/h/h4928.md).

<a name="2samuel_23_24"></a>2Samuel 23:24

[ʿĂśâ'Ēl](../../strongs/h/h6214.md) the ['ach](../../strongs/h/h251.md) of [Yô'āḇ](../../strongs/h/h3097.md) was one of the thirty; ['Elḥānān](../../strongs/h/h445.md) the [ben](../../strongs/h/h1121.md) of [Dôḏô](../../strongs/h/h1734.md) of [Bêṯ leḥem](../../strongs/h/h1035.md),

<a name="2samuel_23_25"></a>2Samuel 23:25

[Šammâ](../../strongs/h/h8048.md) the [Ḥărōḏî](../../strongs/h/h2733.md), ['Ĕlîqā'](../../strongs/h/h470.md) the [Ḥărōḏî](../../strongs/h/h2733.md),

<a name="2samuel_23_26"></a>2Samuel 23:26

[Ḥeleṣ](../../strongs/h/h2503.md) the [p̄lṭy](../../strongs/h/h6407.md), [ʿÎrā'](../../strongs/h/h5896.md) the [ben](../../strongs/h/h1121.md) of [ʿIqqēš](../../strongs/h/h6142.md) the [Tᵊqôʿî](../../strongs/h/h8621.md),

<a name="2samuel_23_27"></a>2Samuel 23:27

['ĂḇîʿEzer](../../strongs/h/h44.md) the [ʿAnnᵊṯōṯî](../../strongs/h/h6069.md), [Mᵊḇunnay](../../strongs/h/h4012.md) the [Ḥušāṯî](../../strongs/h/h2843.md),

<a name="2samuel_23_28"></a>2Samuel 23:28

[Ṣalmôn](../../strongs/h/h6756.md) the ['Ăḥôḥî](../../strongs/h/h266.md), [Mahăray](../../strongs/h/h4121.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md),

<a name="2samuel_23_29"></a>2Samuel 23:29

[Ḥēleḇ](../../strongs/h/h2460.md) the [ben](../../strongs/h/h1121.md) of [BaʿĂnâ](../../strongs/h/h1196.md), a [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), ['Itay](../../strongs/h/h863.md) the [ben](../../strongs/h/h1121.md) of [Rîḇay](../../strongs/h/h7380.md) out of [giḇʿâ](../../strongs/h/h1390.md) of the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md),

<a name="2samuel_23_30"></a>2Samuel 23:30

[Bᵊnāyâ](../../strongs/h/h1141.md) the [Pirʿāṯônî](../../strongs/h/h6553.md), [Hiday](../../strongs/h/h1914.md) of the [nachal](../../strongs/h/h5158.md) of [GaʿAš](../../strongs/h/h1608.md),

<a name="2samuel_23_31"></a>2Samuel 23:31

['Ăḇî-ʿAlḇôn](../../strongs/h/h45.md) the [ʿArḇāṯî](../../strongs/h/h6164.md), [ʿAzmāveṯ](../../strongs/h/h5820.md) the [Barḥumî](../../strongs/h/h1273.md),

<a name="2samuel_23_32"></a>2Samuel 23:32

['Elyaḥbā'](../../strongs/h/h455.md) the [ŠaʿAlḇōnî](../../strongs/h/h8170.md), of the [ben](../../strongs/h/h1121.md) of [Yāšēn](../../strongs/h/h3464.md), [Yᵊhônāṯān](../../strongs/h/h3083.md),

<a name="2samuel_23_33"></a>2Samuel 23:33

[Šammâ](../../strongs/h/h8048.md) the [Hărārî](../../strongs/h/h2043.md), ['Ăḥî'Ām](../../strongs/h/h279.md) the [ben](../../strongs/h/h1121.md) of [Šārār](../../strongs/h/h8325.md) the [Hărārî](../../strongs/h/h2043.md),

<a name="2samuel_23_34"></a>2Samuel 23:34

['Ĕlîp̄Eleṭ](../../strongs/h/h467.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥasbay](../../strongs/h/h308.md), the [ben](../../strongs/h/h1121.md) of the [Maʿăḵāṯî](../../strongs/h/h4602.md), ['ĔlîʿĀm](../../strongs/h/h463.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md) the [Gîlōnî](../../strongs/h/h1526.md),

<a name="2samuel_23_35"></a>2Samuel 23:35

[Ḥeṣrô](../../strongs/h/h2695.md) the [Karmᵊlî](../../strongs/h/h3761.md), [PaʿĂray](../../strongs/h/h6474.md) the ['Arbî](../../strongs/h/h701.md),

<a name="2samuel_23_36"></a>2Samuel 23:36

[Yiḡ'āl](../../strongs/h/h3008.md) the [ben](../../strongs/h/h1121.md) of [Nāṯān](../../strongs/h/h5416.md) of [Ṣôḇā'](../../strongs/h/h6678.md), [Bānî](../../strongs/h/h1137.md) the [Gāḏî](../../strongs/h/h1425.md),

<a name="2samuel_23_37"></a>2Samuel 23:37

[Ṣeleq](../../strongs/h/h6768.md) the [ʿAmmôn](../../strongs/h/h5984.md), [Naḥăray](../../strongs/h/h5171.md) the [Bᵊ'ērōṯî](../../strongs/h/h886.md), [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md) to [Yô'āḇ](../../strongs/h/h3097.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md),

<a name="2samuel_23_38"></a>2Samuel 23:38

[ʿÎrā'](../../strongs/h/h5896.md) an [Yiṯrî](../../strongs/h/h3505.md), [Gārēḇ](../../strongs/h/h1619.md) an [Yiṯrî](../../strongs/h/h3505.md),

<a name="2samuel_23_39"></a>2Samuel 23:39

['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md): thirty and seven in all.

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 22](2samuel_22.md) - [2Samuel 24](2samuel_24.md)