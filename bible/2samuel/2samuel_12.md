# [2Samuel 12](https://www.blueletterbible.org/kjv/2samuel/12)

<a name="2samuel_12_1"></a>2Samuel 12:1

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) [Nāṯān](../../strongs/h/h5416.md) unto [Dāviḏ](../../strongs/h/h1732.md). And he [bow'](../../strongs/h/h935.md) unto him, and ['āmar](../../strongs/h/h559.md) unto him, There were two ['enowsh](../../strongs/h/h582.md) in one [ʿîr](../../strongs/h/h5892.md); the one [ʿāšîr](../../strongs/h/h6223.md), and the other [rûš](../../strongs/h/h7326.md).

<a name="2samuel_12_2"></a>2Samuel 12:2

The [ʿāšîr](../../strongs/h/h6223.md) had [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md):

<a name="2samuel_12_3"></a>2Samuel 12:3

But the [rûš](../../strongs/h/h7326.md) man had nothing, save one [qāṭān](../../strongs/h/h6996.md) [kiḇśâ](../../strongs/h/h3535.md), which he had [qānâ](../../strongs/h/h7069.md) and [ḥāyâ](../../strongs/h/h2421.md): and it [gāḏal](../../strongs/h/h1431.md) [yaḥaḏ](../../strongs/h/h3162.md) with him, and with his [ben](../../strongs/h/h1121.md); it did ['akal](../../strongs/h/h398.md) of his own [paṯ](../../strongs/h/h6595.md), and [šāṯâ](../../strongs/h/h8354.md) of his own [kowc](../../strongs/h/h3563.md), and [shakab](../../strongs/h/h7901.md) in his [ḥêq](../../strongs/h/h2436.md), and was unto him as a [bath](../../strongs/h/h1323.md).

<a name="2samuel_12_4"></a>2Samuel 12:4

And there [bow'](../../strongs/h/h935.md) a [hēleḵ](../../strongs/h/h1982.md) unto the [ʿāšîr](../../strongs/h/h6223.md) ['iysh](../../strongs/h/h376.md), and he [ḥāmal](../../strongs/h/h2550.md) to [laqach](../../strongs/h/h3947.md) of his own [tso'n](../../strongs/h/h6629.md) and of his own [bāqār](../../strongs/h/h1241.md), to ['asah](../../strongs/h/h6213.md) for the ['āraḥ](../../strongs/h/h732.md) that was [bow'](../../strongs/h/h935.md) unto him; but [laqach](../../strongs/h/h3947.md) the [rûš](../../strongs/h/h7326.md) ['iysh](../../strongs/h/h376.md) [kiḇśâ](../../strongs/h/h3535.md), and ['asah](../../strongs/h/h6213.md) it for the ['iysh](../../strongs/h/h376.md) that was [bow'](../../strongs/h/h935.md) to him.

<a name="2samuel_12_5"></a>2Samuel 12:5

And [Dāviḏ](../../strongs/h/h1732.md) ['aph](../../strongs/h/h639.md) was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md) against the ['iysh](../../strongs/h/h376.md); and he ['āmar](../../strongs/h/h559.md) to [Nāṯān](../../strongs/h/h5416.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), the ['iysh](../../strongs/h/h376.md) that hath ['asah](../../strongs/h/h6213.md) this thing shall [ben](../../strongs/h/h1121.md) [maveth](../../strongs/h/h4194.md):

<a name="2samuel_12_6"></a>2Samuel 12:6

And he shall [shalam](../../strongs/h/h7999.md) the [kiḇśâ](../../strongs/h/h3535.md) ['arbaʿtāyim](../../strongs/h/h706.md), because he ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md), and because he had no [ḥāmal](../../strongs/h/h2550.md).

<a name="2samuel_12_7"></a>2Samuel 12:7

And [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md), Thou art the ['iysh](../../strongs/h/h376.md). Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), I [māšaḥ](../../strongs/h/h4886.md) thee [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), and I [natsal](../../strongs/h/h5337.md) thee out of the [yad](../../strongs/h/h3027.md) of [Šā'ûl](../../strongs/h/h7586.md);

<a name="2samuel_12_8"></a>2Samuel 12:8

And I [nathan](../../strongs/h/h5414.md) thee thy ['adown](../../strongs/h/h113.md) [bayith](../../strongs/h/h1004.md), and thy ['adown](../../strongs/h/h113.md) ['ishshah](../../strongs/h/h802.md) into thy [ḥêq](../../strongs/h/h2436.md), and [nathan](../../strongs/h/h5414.md) thee the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and of [Yehuwdah](../../strongs/h/h3063.md); and if that had been too [mᵊʿaṭ](../../strongs/h/h4592.md), I would moreover have [yāsap̄](../../strongs/h/h3254.md) unto thee [hēnnâ](../../strongs/h/h2007.md) and [hēnnâ](../../strongs/h/h2007.md).

<a name="2samuel_12_9"></a>2Samuel 12:9

Wherefore hast thou [bazah](../../strongs/h/h959.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in his ['ayin](../../strongs/h/h5869.md)? thou hast [nakah](../../strongs/h/h5221.md) ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md) with the [chereb](../../strongs/h/h2719.md), and hast [laqach](../../strongs/h/h3947.md) his ['ishshah](../../strongs/h/h802.md) to be thy ['ishshah](../../strongs/h/h802.md), and hast [harag](../../strongs/h/h2026.md) him with the [chereb](../../strongs/h/h2719.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="2samuel_12_10"></a>2Samuel 12:10

Now therefore the [chereb](../../strongs/h/h2719.md) shall never ['owlam](../../strongs/h/h5769.md) [cuwr](../../strongs/h/h5493.md) from thine [bayith](../../strongs/h/h1004.md); because thou hast [bazah](../../strongs/h/h959.md) me, and hast [laqach](../../strongs/h/h3947.md) the ['ishshah](../../strongs/h/h802.md) of ['Ûrîyâ](../../strongs/h/h223.md) the [Ḥitî](../../strongs/h/h2850.md) to be thy ['ishshah](../../strongs/h/h802.md).

<a name="2samuel_12_11"></a>2Samuel 12:11

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [quwm](../../strongs/h/h6965.md) [ra'](../../strongs/h/h7451.md) against thee out of thine own [bayith](../../strongs/h/h1004.md), and I will [laqach](../../strongs/h/h3947.md) thy ['ishshah](../../strongs/h/h802.md) before thine ['ayin](../../strongs/h/h5869.md), and [nathan](../../strongs/h/h5414.md) them unto thy [rea'](../../strongs/h/h7453.md), and he shall [shakab](../../strongs/h/h7901.md) with thy ['ishshah](../../strongs/h/h802.md) in the ['ayin](../../strongs/h/h5869.md) of this [šemeš](../../strongs/h/h8121.md).

<a name="2samuel_12_12"></a>2Samuel 12:12

For thou ['asah](../../strongs/h/h6213.md) it [cether](../../strongs/h/h5643.md): but I will ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) before all [Yisra'el](../../strongs/h/h3478.md), and before the [šemeš](../../strongs/h/h8121.md).

<a name="2samuel_12_13"></a>2Samuel 12:13

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Nāṯān](../../strongs/h/h5416.md), I have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md). And [Nāṯān](../../strongs/h/h5416.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [Yĕhovah](../../strongs/h/h3068.md) also hath put ['abar](../../strongs/h/h5674.md) thy [chatta'ath](../../strongs/h/h2403.md); thou shalt not [muwth](../../strongs/h/h4191.md).

<a name="2samuel_12_14"></a>2Samuel 12:14

['ep̄es](../../strongs/h/h657.md), because by this [dabar](../../strongs/h/h1697.md) thou hast [na'ats](../../strongs/h/h5006.md) great [na'ats](../../strongs/h/h5006.md) to the ['oyeb](../../strongs/h/h341.md) of [Yĕhovah](../../strongs/h/h3068.md) to [na'ats](../../strongs/h/h5006.md), the [ben](../../strongs/h/h1121.md) also that is [yillôḏ](../../strongs/h/h3209.md) unto thee shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="2samuel_12_15"></a>2Samuel 12:15

And [Nāṯān](../../strongs/h/h5416.md) [yālaḵ](../../strongs/h/h3212.md) unto his [bayith](../../strongs/h/h1004.md). And [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) the [yeleḏ](../../strongs/h/h3206.md) that ['Ûrîyâ](../../strongs/h/h223.md) ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) unto [Dāviḏ](../../strongs/h/h1732.md), and it was very ['anash](../../strongs/h/h605.md).

<a name="2samuel_12_16"></a>2Samuel 12:16

[Dāviḏ](../../strongs/h/h1732.md) therefore [bāqaš](../../strongs/h/h1245.md) ['Elohiym](../../strongs/h/h430.md) for the [naʿar](../../strongs/h/h5288.md); and [Dāviḏ](../../strongs/h/h1732.md) [ṣûm](../../strongs/h/h6684.md) [ṣôm](../../strongs/h/h6685.md), and [bow'](../../strongs/h/h935.md), and [shakab](../../strongs/h/h7901.md) all [lûn](../../strongs/h/h3885.md) upon the ['erets](../../strongs/h/h776.md).

<a name="2samuel_12_17"></a>2Samuel 12:17

And the [zāqēn](../../strongs/h/h2205.md) of his [bayith](../../strongs/h/h1004.md) [quwm](../../strongs/h/h6965.md), and went to him, to [quwm](../../strongs/h/h6965.md) him from the ['erets](../../strongs/h/h776.md): but he ['āḇâ](../../strongs/h/h14.md) not, neither did he [bārâ](../../strongs/h/h1262.md) [lechem](../../strongs/h/h3899.md) with them.

<a name="2samuel_12_18"></a>2Samuel 12:18

And it came to pass on the seventh [yowm](../../strongs/h/h3117.md), that the [yeleḏ](../../strongs/h/h3206.md) [muwth](../../strongs/h/h4191.md). And the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md) [yare'](../../strongs/h/h3372.md) to [nāḡaḏ](../../strongs/h/h5046.md) him that the [yeleḏ](../../strongs/h/h3206.md) was [muwth](../../strongs/h/h4191.md): for they ['āmar](../../strongs/h/h559.md), Behold, while the [yeleḏ](../../strongs/h/h3206.md) was yet [chay](../../strongs/h/h2416.md), we [dabar](../../strongs/h/h1696.md) unto him, and he would not [shama'](../../strongs/h/h8085.md) unto our [qowl](../../strongs/h/h6963.md): how will he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) ['asah](../../strongs/h/h6213.md), if we ['āmar](../../strongs/h/h559.md) him that the [yeleḏ](../../strongs/h/h3206.md) is [muwth](../../strongs/h/h4191.md)?

<a name="2samuel_12_19"></a>2Samuel 12:19

But when [Dāviḏ](../../strongs/h/h1732.md) [ra'ah](../../strongs/h/h7200.md) that his ['ebed](../../strongs/h/h5650.md) [lāḥaš](../../strongs/h/h3907.md), [Dāviḏ](../../strongs/h/h1732.md) [bîn](../../strongs/h/h995.md) that the [yeleḏ](../../strongs/h/h3206.md) was [muwth](../../strongs/h/h4191.md): therefore [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto his ['ebed](../../strongs/h/h5650.md), Is the [yeleḏ](../../strongs/h/h3206.md) [muwth](../../strongs/h/h4191.md)? And they ['āmar](../../strongs/h/h559.md), He is [muwth](../../strongs/h/h4191.md).

<a name="2samuel_12_20"></a>2Samuel 12:20

Then [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md) from the ['erets](../../strongs/h/h776.md), and [rāḥaṣ](../../strongs/h/h7364.md), and [sûḵ](../../strongs/h/h5480.md) himself, and [ḥālap̄](../../strongs/h/h2498.md) his [śimlâ](../../strongs/h/h8071.md), and [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [shachah](../../strongs/h/h7812.md): then he [bow'](../../strongs/h/h935.md) to his own [bayith](../../strongs/h/h1004.md); and when he [sha'al](../../strongs/h/h7592.md), they [śûm](../../strongs/h/h7760.md) [lechem](../../strongs/h/h3899.md) before him, and he did ['akal](../../strongs/h/h398.md).

<a name="2samuel_12_21"></a>2Samuel 12:21

Then ['āmar](../../strongs/h/h559.md) his ['ebed](../../strongs/h/h5650.md) unto him, What [dabar](../../strongs/h/h1697.md) is this that thou hast ['asah](../../strongs/h/h6213.md)? thou didst [ṣûm](../../strongs/h/h6684.md) and [bāḵâ](../../strongs/h/h1058.md) for the [yeleḏ](../../strongs/h/h3206.md), while it was [chay](../../strongs/h/h2416.md); but when the [yeleḏ](../../strongs/h/h3206.md) was [muwth](../../strongs/h/h4191.md), thou didst [quwm](../../strongs/h/h6965.md) and ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md).

<a name="2samuel_12_22"></a>2Samuel 12:22

And he ['āmar](../../strongs/h/h559.md), While the [yeleḏ](../../strongs/h/h3206.md) was yet [chay](../../strongs/h/h2416.md), I [ṣûm](../../strongs/h/h6684.md) and [bāḵâ](../../strongs/h/h1058.md): for I ['āmar](../../strongs/h/h559.md), Who can [yada'](../../strongs/h/h3045.md) whether [Yĕhovah](../../strongs/h/h3068.md) will be [chanan](../../strongs/h/h2603.md) [chanan](../../strongs/h/h2603.md) to me, that the [yeleḏ](../../strongs/h/h3206.md) may [chay](../../strongs/h/h2416.md)?

<a name="2samuel_12_23"></a>2Samuel 12:23

But now he is [muwth](../../strongs/h/h4191.md), wherefore should I [ṣûm](../../strongs/h/h6684.md)? [yakol](../../strongs/h/h3201.md) I bring him back [shuwb](../../strongs/h/h7725.md)? I shall [halak](../../strongs/h/h1980.md) to him, but he shall not [shuwb](../../strongs/h/h7725.md) to me.

<a name="2samuel_12_24"></a>2Samuel 12:24

And [Dāviḏ](../../strongs/h/h1732.md) [nacham](../../strongs/h/h5162.md) [Baṯ-Šeḇaʿ](../../strongs/h/h1339.md) his ['ishshah](../../strongs/h/h802.md), and [bow'](../../strongs/h/h935.md) unto her, and [shakab](../../strongs/h/h7901.md) with her: and she [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šᵊlōmô](../../strongs/h/h8010.md): and [Yĕhovah](../../strongs/h/h3068.md) ['ahab](../../strongs/h/h157.md) him.

<a name="2samuel_12_25"></a>2Samuel 12:25

And he [shalach](../../strongs/h/h7971.md) by the [yad](../../strongs/h/h3027.md) of [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md); and he [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Yᵊḏîḏyâ](../../strongs/h/h3041.md), because of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2samuel_12_26"></a>2Samuel 12:26

And [Yô'āḇ](../../strongs/h/h3097.md) [lāḥam](../../strongs/h/h3898.md) against [Rabâ](../../strongs/h/h7237.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [lāḵaḏ](../../strongs/h/h3920.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) [ʿîr](../../strongs/h/h5892.md).

<a name="2samuel_12_27"></a>2Samuel 12:27

And [Yô'āḇ](../../strongs/h/h3097.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md), I have [lāḥam](../../strongs/h/h3898.md) against [Rabâ](../../strongs/h/h7237.md), and have [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md) of [mayim](../../strongs/h/h4325.md).

<a name="2samuel_12_28"></a>2Samuel 12:28

Now therefore ['āsap̄](../../strongs/h/h622.md) the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) ['āsap̄](../../strongs/h/h622.md), and [ḥānâ](../../strongs/h/h2583.md) against the [ʿîr](../../strongs/h/h5892.md), and [lāḵaḏ](../../strongs/h/h3920.md) it: lest I [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md), and it be [qara'](../../strongs/h/h7121.md) after my [shem](../../strongs/h/h8034.md).

<a name="2samuel_12_29"></a>2Samuel 12:29

And [Dāviḏ](../../strongs/h/h1732.md) ['āsap̄](../../strongs/h/h622.md) all the ['am](../../strongs/h/h5971.md) ['āsap̄](../../strongs/h/h622.md), and [yālaḵ](../../strongs/h/h3212.md) to [Rabâ](../../strongs/h/h7237.md), and [lāḥam](../../strongs/h/h3898.md) against it, and [lāḵaḏ](../../strongs/h/h3920.md) it.

<a name="2samuel_12_30"></a>2Samuel 12:30

And he [laqach](../../strongs/h/h3947.md) their [melek](../../strongs/h/h4428.md) [ʿăṭārâ](../../strongs/h/h5850.md) from off his [ro'sh](../../strongs/h/h7218.md), the [mišqāl](../../strongs/h/h4948.md) whereof was a [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md) with the [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md): and it was set on [Dāviḏ](../../strongs/h/h1732.md) [ro'sh](../../strongs/h/h7218.md). And he [yāṣā'](../../strongs/h/h3318.md) the [šālāl](../../strongs/h/h7998.md) of the [ʿîr](../../strongs/h/h5892.md) in [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md).

<a name="2samuel_12_31"></a>2Samuel 12:31

And he [yāṣā'](../../strongs/h/h3318.md) the ['am](../../strongs/h/h5971.md) that were therein, and [śûm](../../strongs/h/h7760.md) them under [mᵊḡērâ](../../strongs/h/h4050.md), and under [ḥārîṣ](../../strongs/h/h2757.md) of [barzel](../../strongs/h/h1270.md), and under [maḡzērâ](../../strongs/h/h4037.md) of [barzel](../../strongs/h/h1270.md), and made them ['abar](../../strongs/h/h5674.md) through the [malbēn](../../strongs/h/h4404.md): and thus ['asah](../../strongs/h/h6213.md) he unto all the [ʿîr](../../strongs/h/h5892.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md). So [Dāviḏ](../../strongs/h/h1732.md) and all the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 11](2samuel_11.md) - [2Samuel 13](2samuel_13.md)