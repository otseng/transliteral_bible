# [2Samuel 1](https://www.blueletterbible.org/kjv/2samuel/1)

<a name="2samuel_1_1"></a>2Samuel 1:1

Now it came to pass ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Šā'ûl](../../strongs/h/h7586.md), when [Dāviḏ](../../strongs/h/h1732.md) was [shuwb](../../strongs/h/h7725.md) from the [nakah](../../strongs/h/h5221.md) of the [ʿĂmālēq](../../strongs/h/h6002.md), and [Dāviḏ](../../strongs/h/h1732.md) had [yashab](../../strongs/h/h3427.md) two [yowm](../../strongs/h/h3117.md) in [Ṣiqlāḡ](../../strongs/h/h6860.md);

<a name="2samuel_1_2"></a>2Samuel 1:2

It came even to pass on the third [yowm](../../strongs/h/h3117.md), that, behold, an ['iysh](../../strongs/h/h376.md) [bow'](../../strongs/h/h935.md) out of the [maḥănê](../../strongs/h/h4264.md) from [Šā'ûl](../../strongs/h/h7586.md) with his [beḡeḏ](../../strongs/h/h899.md) [qāraʿ](../../strongs/h/h7167.md), and ['ăḏāmâ](../../strongs/h/h127.md) upon his [ro'sh](../../strongs/h/h7218.md): and so it was, when he [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md), that he [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md), and did [shachah](../../strongs/h/h7812.md).

<a name="2samuel_1_3"></a>2Samuel 1:3

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, From whence [bow'](../../strongs/h/h935.md) thou? And he ['āmar](../../strongs/h/h559.md) unto him, Out of the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md) am I [mālaṭ](../../strongs/h/h4422.md).

<a name="2samuel_1_4"></a>2Samuel 1:4

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, How went the [dabar](../../strongs/h/h1697.md)? I pray thee, [nāḡaḏ](../../strongs/h/h5046.md) me. And he ['āmar](../../strongs/h/h559.md), That the ['am](../../strongs/h/h5971.md) are [nûs](../../strongs/h/h5127.md) from the [milḥāmâ](../../strongs/h/h4421.md), and [rabah](../../strongs/h/h7235.md) of the ['am](../../strongs/h/h5971.md) also are [naphal](../../strongs/h/h5307.md) and [muwth](../../strongs/h/h4191.md); and [Šā'ûl](../../strongs/h/h7586.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md) are [muwth](../../strongs/h/h4191.md) also.

<a name="2samuel_1_5"></a>2Samuel 1:5

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md) that [nāḡaḏ](../../strongs/h/h5046.md) him, How [yada'](../../strongs/h/h3045.md) thou that [Šā'ûl](../../strongs/h/h7586.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md) be [muwth](../../strongs/h/h4191.md)?

<a name="2samuel_1_6"></a>2Samuel 1:6

And the [naʿar](../../strongs/h/h5288.md) that [nāḡaḏ](../../strongs/h/h5046.md) him ['āmar](../../strongs/h/h559.md), As I [qārâ](../../strongs/h/h7136.md) by [qārā'](../../strongs/h/h7122.md) upon [har](../../strongs/h/h2022.md) [Gilbōaʿ](../../strongs/h/h1533.md), behold, [Šā'ûl](../../strongs/h/h7586.md) [šāʿan](../../strongs/h/h8172.md) upon his [ḥănîṯ](../../strongs/h/h2595.md); and, lo, the [reḵeḇ](../../strongs/h/h7393.md) and [baʿal](../../strongs/h/h1167.md) [pārāš](../../strongs/h/h6571.md) followed [dāḇaq](../../strongs/h/h1692.md) after him.

<a name="2samuel_1_7"></a>2Samuel 1:7

And when he [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md) him, he [ra'ah](../../strongs/h/h7200.md) me, and [qara'](../../strongs/h/h7121.md) unto me. And I ['āmar](../../strongs/h/h559.md), Here am I.

<a name="2samuel_1_8"></a>2Samuel 1:8

And he ['āmar](../../strongs/h/h559.md) unto me, Who art thou? And I ['āmar](../../strongs/h/h559.md) him, I am an [ʿămālēqî](../../strongs/h/h6003.md).

<a name="2samuel_1_9"></a>2Samuel 1:9

He ['āmar](../../strongs/h/h559.md) unto me again, ['amad](../../strongs/h/h5975.md), I pray thee, upon me, and [muwth](../../strongs/h/h4191.md) me: for [šāḇāṣ](../../strongs/h/h7661.md) is ['āḥaz](../../strongs/h/h270.md) upon me, because my [nephesh](../../strongs/h/h5315.md) is yet whole in me.

<a name="2samuel_1_10"></a>2Samuel 1:10

So I ['amad](../../strongs/h/h5975.md) upon him, and [muwth](../../strongs/h/h4191.md) him, because I was [yada'](../../strongs/h/h3045.md) that he could not [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) that he was [naphal](../../strongs/h/h5307.md): and I [laqach](../../strongs/h/h3947.md) the [nēzer](../../strongs/h/h5145.md) that was upon his [ro'sh](../../strongs/h/h7218.md), and the ['eṣʿāḏâ](../../strongs/h/h685.md) that was on his [zerowa'](../../strongs/h/h2220.md), and have [bow'](../../strongs/h/h935.md) them hither unto my ['adown](../../strongs/h/h113.md).

<a name="2samuel_1_11"></a>2Samuel 1:11

Then [Dāviḏ](../../strongs/h/h1732.md) took [ḥāzaq](../../strongs/h/h2388.md) on his [beḡeḏ](../../strongs/h/h899.md), and [qāraʿ](../../strongs/h/h7167.md) them; and likewise all the ['enowsh](../../strongs/h/h582.md) that were with him:

<a name="2samuel_1_12"></a>2Samuel 1:12

And they [sāp̄aḏ](../../strongs/h/h5594.md), and [bāḵâ](../../strongs/h/h1058.md), and [ṣûm](../../strongs/h/h6684.md) until ['ereb](../../strongs/h/h6153.md), for [Šā'ûl](../../strongs/h/h7586.md), and for [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md), and for the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md), and for the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); because they were [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md).

<a name="2samuel_1_13"></a>2Samuel 1:13

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md) that [nāḡaḏ](../../strongs/h/h5046.md) him, Whence art thou? And he ['āmar](../../strongs/h/h559.md), I am the [ben](../../strongs/h/h1121.md) of an ['iysh](../../strongs/h/h376.md) [ger](../../strongs/h/h1616.md), an [ʿămālēqî](../../strongs/h/h6003.md).

<a name="2samuel_1_14"></a>2Samuel 1:14

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, How wast thou not [yare'](../../strongs/h/h3372.md) to [shalach](../../strongs/h/h7971.md) thine [yad](../../strongs/h/h3027.md) to [shachath](../../strongs/h/h7843.md) [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md)?

<a name="2samuel_1_15"></a>2Samuel 1:15

And [Dāviḏ](../../strongs/h/h1732.md) [qara'](../../strongs/h/h7121.md) one of the [naʿar](../../strongs/h/h5288.md), and ['āmar](../../strongs/h/h559.md), [nāḡaš](../../strongs/h/h5066.md), and [pāḡaʿ](../../strongs/h/h6293.md) upon him. And he [nakah](../../strongs/h/h5221.md) him that he [muwth](../../strongs/h/h4191.md).

<a name="2samuel_1_16"></a>2Samuel 1:16

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto him, Thy [dam](../../strongs/h/h1818.md) be upon thy [ro'sh](../../strongs/h/h7218.md); for thy [peh](../../strongs/h/h6310.md) hath ['anah](../../strongs/h/h6030.md) against thee, ['āmar](../../strongs/h/h559.md), I have [muwth](../../strongs/h/h4191.md) [Yĕhovah](../../strongs/h/h3068.md) [mashiyach](../../strongs/h/h4899.md).

<a name="2samuel_1_17"></a>2Samuel 1:17

And [Dāviḏ](../../strongs/h/h1732.md) [qônēn](../../strongs/h/h6969.md) with this [qînâ](../../strongs/h/h7015.md) over [Šā'ûl](../../strongs/h/h7586.md) and over [Yᵊhônāṯān](../../strongs/h/h3083.md) his [ben](../../strongs/h/h1121.md):

<a name="2samuel_1_18"></a>2Samuel 1:18

(Also he ['āmar](../../strongs/h/h559.md) them [lamad](../../strongs/h/h3925.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) the use of the [qesheth](../../strongs/h/h7198.md): behold, it is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of [yashar](../../strongs/h/h3477.md).)

<a name="2samuel_1_19"></a>2Samuel 1:19

The [ṣᵊḇî](../../strongs/h/h6643.md) of [Yisra'el](../../strongs/h/h3478.md) is [ḥālāl](../../strongs/h/h2491.md) upon thy [bāmâ](../../strongs/h/h1116.md): how are the [gibôr](../../strongs/h/h1368.md) [naphal](../../strongs/h/h5307.md)!

<a name="2samuel_1_20"></a>2Samuel 1:20

[nāḡaḏ](../../strongs/h/h5046.md) it not in [Gaṯ](../../strongs/h/h1661.md), [bāśar](../../strongs/h/h1319.md) it not in the [ḥûṣ](../../strongs/h/h2351.md) of ['Ašqᵊlôn](../../strongs/h/h831.md); lest the [bath](../../strongs/h/h1323.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [samach](../../strongs/h/h8055.md), lest the [bath](../../strongs/h/h1323.md) of the [ʿārēl](../../strongs/h/h6189.md) [ʿālaz](../../strongs/h/h5937.md).

<a name="2samuel_1_21"></a>2Samuel 1:21

Ye [har](../../strongs/h/h2022.md) of [Gilbōaʿ](../../strongs/h/h1533.md), let there be no [ṭal](../../strongs/h/h2919.md), neither let there be [māṭār](../../strongs/h/h4306.md), upon you, nor [sadeh](../../strongs/h/h7704.md) of [tᵊrûmâ](../../strongs/h/h8641.md): for there the [magen](../../strongs/h/h4043.md) of the [gibôr](../../strongs/h/h1368.md) is [gāʿal](../../strongs/h/h1602.md), the [magen](../../strongs/h/h4043.md) of [Šā'ûl](../../strongs/h/h7586.md), as though he had not been [mashiyach](../../strongs/h/h4899.md) with [šemen](../../strongs/h/h8081.md).

<a name="2samuel_1_22"></a>2Samuel 1:22

From the [dam](../../strongs/h/h1818.md) of the [ḥālāl](../../strongs/h/h2491.md), from the [cheleb](../../strongs/h/h2459.md) of the [gibôr](../../strongs/h/h1368.md), the [qesheth](../../strongs/h/h7198.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) [śûḡ](../../strongs/h/h7734.md) not ['āḥôr](../../strongs/h/h268.md), and the [chereb](../../strongs/h/h2719.md) of [Šā'ûl](../../strongs/h/h7586.md) [shuwb](../../strongs/h/h7725.md) not [rêqām](../../strongs/h/h7387.md).

<a name="2samuel_1_23"></a>2Samuel 1:23

[Šā'ûl](../../strongs/h/h7586.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md) were ['ahab](../../strongs/h/h157.md) and [na'iym](../../strongs/h/h5273.md) in their [chay](../../strongs/h/h2416.md), and in their [maveth](../../strongs/h/h4194.md) they were not [pāraḏ](../../strongs/h/h6504.md): they were [qālal](../../strongs/h/h7043.md) than [nesheׁr](../../strongs/h/h5404.md), they were [gabar](../../strongs/h/h1396.md) than ['ariy](../../strongs/h/h738.md).

<a name="2samuel_1_24"></a>2Samuel 1:24

Ye [bath](../../strongs/h/h1323.md) of [Yisra'el](../../strongs/h/h3478.md), [bāḵâ](../../strongs/h/h1058.md) [Šā'ûl](../../strongs/h/h7586.md), who [labash](../../strongs/h/h3847.md) you in [šānî](../../strongs/h/h8144.md), with other [ʿēḏen](../../strongs/h/h5730.md), who [ʿālâ](../../strongs/h/h5927.md) on [ʿădiy](../../strongs/h/h5716.md) of [zāhāḇ](../../strongs/h/h2091.md) upon your [lᵊḇûš](../../strongs/h/h3830.md).

<a name="2samuel_1_25"></a>2Samuel 1:25

How are the [gibôr](../../strongs/h/h1368.md) [naphal](../../strongs/h/h5307.md) in the [tavek](../../strongs/h/h8432.md) of the [milḥāmâ](../../strongs/h/h4421.md)! O [Yᵊhônāṯān](../../strongs/h/h3083.md), thou wast [ḥālāl](../../strongs/h/h2491.md) in thine [bāmâ](../../strongs/h/h1116.md).

<a name="2samuel_1_26"></a>2Samuel 1:26

I am [tsarar](../../strongs/h/h6887.md) for thee, my ['ach](../../strongs/h/h251.md) [Yᵊhônāṯān](../../strongs/h/h3083.md): [me'od](../../strongs/h/h3966.md) [nāʿēm](../../strongs/h/h5276.md) hast thou been unto me: thy ['ahăḇâ](../../strongs/h/h160.md) to me was [pala'](../../strongs/h/h6381.md), passing the ['ahăḇâ](../../strongs/h/h160.md) of ['ishshah](../../strongs/h/h802.md).

<a name="2samuel_1_27"></a>2Samuel 1:27

How are the [gibôr](../../strongs/h/h1368.md) [naphal](../../strongs/h/h5307.md), and the [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md) ['abad](../../strongs/h/h6.md)!

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 2](2samuel_2.md)