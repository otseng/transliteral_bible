# [2Samuel 16](https://www.blueletterbible.org/kjv/2samuel/16)

<a name="2samuel_16_1"></a>2Samuel 16:1

And when [Dāviḏ](../../strongs/h/h1732.md) was a [mᵊʿaṭ](../../strongs/h/h4592.md) ['abar](../../strongs/h/h5674.md) the [ro'sh](../../strongs/h/h7218.md), behold, [Ṣîḇā'](../../strongs/h/h6717.md) the [naʿar](../../strongs/h/h5288.md) of [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md) [qārā'](../../strongs/h/h7125.md) him, with a [ṣemeḏ](../../strongs/h/h6776.md) of [chamowr](../../strongs/h/h2543.md) [ḥāḇaš](../../strongs/h/h2280.md), and upon them two hundred loaves of [lechem](../../strongs/h/h3899.md), and an hundred bunches of [ṣmvqym](../../strongs/h/h6778.md), and an hundred of summer [qayiṣ](../../strongs/h/h7019.md), and a [neḇel](../../strongs/h/h5035.md) of [yayin](../../strongs/h/h3196.md).

<a name="2samuel_16_2"></a>2Samuel 16:2

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Ṣîḇā'](../../strongs/h/h6717.md), What meanest thou by these? And [Ṣîḇā'](../../strongs/h/h6717.md) ['āmar](../../strongs/h/h559.md), The [chamowr](../../strongs/h/h2543.md) be for the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) to [rāḵaḇ](../../strongs/h/h7392.md); and the [lechem](../../strongs/h/h3899.md) and [qayiṣ](../../strongs/h/h7019.md) for the [naʿar](../../strongs/h/h5288.md) to ['akal](../../strongs/h/h398.md); and the [yayin](../../strongs/h/h3196.md), that such as be [yāʿēp̄](../../strongs/h/h3287.md) in the [midbar](../../strongs/h/h4057.md) may [šāṯâ](../../strongs/h/h8354.md).

<a name="2samuel_16_3"></a>2Samuel 16:3

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), And where is thy ['adown](../../strongs/h/h113.md) [ben](../../strongs/h/h1121.md)? And [Ṣîḇā'](../../strongs/h/h6717.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Behold, he [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): for he ['āmar](../../strongs/h/h559.md), [yowm](../../strongs/h/h3117.md) shall the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md) me the [mamlāḵûṯ](../../strongs/h/h4468.md) of my ['ab](../../strongs/h/h1.md).

<a name="2samuel_16_4"></a>2Samuel 16:4

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) to [Ṣîḇā'](../../strongs/h/h6717.md), Behold, thine are all that pertained unto [Mᵊp̄Îḇōšeṯ](../../strongs/h/h4648.md). And [Ṣîḇā'](../../strongs/h/h6717.md) ['āmar](../../strongs/h/h559.md), I [shachah](../../strongs/h/h7812.md) beseech thee that I may [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), my ['adown](../../strongs/h/h113.md), O [melek](../../strongs/h/h4428.md).

<a name="2samuel_16_5"></a>2Samuel 16:5

And when [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to [Baḥurîm](../../strongs/h/h980.md), behold, thence [yāṣā'](../../strongs/h/h3318.md) an ['iysh](../../strongs/h/h376.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), whose [shem](../../strongs/h/h8034.md) was [Šimʿî](../../strongs/h/h8096.md), the [ben](../../strongs/h/h1121.md) of [Gērā'](../../strongs/h/h1617.md): he [yāṣā'](../../strongs/h/h3318.md), and [qālal](../../strongs/h/h7043.md) still as he [yāṣā'](../../strongs/h/h3318.md).

<a name="2samuel_16_6"></a>2Samuel 16:6

And he [sāqal](../../strongs/h/h5619.md) ['eben](../../strongs/h/h68.md) at [Dāviḏ](../../strongs/h/h1732.md), and at all the ['ebed](../../strongs/h/h5650.md) of [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md): and all the ['am](../../strongs/h/h5971.md) and all the [gibôr](../../strongs/h/h1368.md) were on his [yamiyn](../../strongs/h/h3225.md) and on his [śᵊmō'l](../../strongs/h/h8040.md).

<a name="2samuel_16_7"></a>2Samuel 16:7

And thus ['āmar](../../strongs/h/h559.md) [Šimʿî](../../strongs/h/h8096.md) when he [qālal](../../strongs/h/h7043.md), [yāṣā'](../../strongs/h/h3318.md), [yāṣā'](../../strongs/h/h3318.md), thou [dam](../../strongs/h/h1818.md) ['iysh](../../strongs/h/h376.md), and thou ['iysh](../../strongs/h/h376.md) of [beliya'al](../../strongs/h/h1100.md):

<a name="2samuel_16_8"></a>2Samuel 16:8

[Yĕhovah](../../strongs/h/h3068.md) hath [shuwb](../../strongs/h/h7725.md) upon thee all the [dam](../../strongs/h/h1818.md) of the [bayith](../../strongs/h/h1004.md) of [Šā'ûl](../../strongs/h/h7586.md), in whose stead thou hast [mālaḵ](../../strongs/h/h4427.md); and [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) the [mᵊlûḵâ](../../strongs/h/h4410.md) into the [yad](../../strongs/h/h3027.md) of ['Ăbyšālôm](../../strongs/h/h53.md) thy [ben](../../strongs/h/h1121.md): and, behold, thou art taken in thy [ra'](../../strongs/h/h7451.md), because thou art a [dam](../../strongs/h/h1818.md) ['iysh](../../strongs/h/h376.md).

<a name="2samuel_16_9"></a>2Samuel 16:9

Then ['āmar](../../strongs/h/h559.md) ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md) unto the [melek](../../strongs/h/h4428.md), Why should this [muwth](../../strongs/h/h4191.md) [keleḇ](../../strongs/h/h3611.md) [qālal](../../strongs/h/h7043.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md)? let me ['abar](../../strongs/h/h5674.md), I pray thee, and [cuwr](../../strongs/h/h5493.md) his [ro'sh](../../strongs/h/h7218.md).

<a name="2samuel_16_10"></a>2Samuel 16:10

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), What have I to do with you, ye [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md)? so let him [qālal](../../strongs/h/h7043.md), because [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) unto him, [qālal](../../strongs/h/h7043.md) [Dāviḏ](../../strongs/h/h1732.md). Who shall then ['āmar](../../strongs/h/h559.md), Wherefore hast thou done ['asah](../../strongs/h/h6213.md)?

<a name="2samuel_16_11"></a>2Samuel 16:11

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Ăḇîšay](../../strongs/h/h52.md), and to all his ['ebed](../../strongs/h/h5650.md), Behold, my [ben](../../strongs/h/h1121.md), which [yāṣā'](../../strongs/h/h3318.md) of my [me'ah](../../strongs/h/h4578.md), [bāqaš](../../strongs/h/h1245.md) my [nephesh](../../strongs/h/h5315.md): how much more now may this [Ben-yᵊmînî](../../strongs/h/h1145.md) do it? let him [yānaḥ](../../strongs/h/h3240.md), and let him [qālal](../../strongs/h/h7043.md); for [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) him.

<a name="2samuel_16_12"></a>2Samuel 16:12

It may be that [Yĕhovah](../../strongs/h/h3068.md) will [ra'ah](../../strongs/h/h7200.md) on mine ['ayin](../../strongs/h/h5869.md) ['oniy](../../strongs/h/h6040.md), and that [Yĕhovah](../../strongs/h/h3068.md) will [shuwb](../../strongs/h/h7725.md) me [towb](../../strongs/h/h2896.md) for his [qᵊlālâ](../../strongs/h/h7045.md) this [yowm](../../strongs/h/h3117.md).

<a name="2samuel_16_13"></a>2Samuel 16:13

And as [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md), [Šimʿî](../../strongs/h/h8096.md) [halak](../../strongs/h/h1980.md) on the [har](../../strongs/h/h2022.md) [tsela'](../../strongs/h/h6763.md) over [ʿummâ](../../strongs/h/h5980.md) him, and [qālal](../../strongs/h/h7043.md) as he [halak](../../strongs/h/h1980.md), and [sāqal](../../strongs/h/h5619.md) ['eben](../../strongs/h/h68.md) [ʿummâ](../../strongs/h/h5980.md) him, and [ʿāp̄ar](../../strongs/h/h6080.md) ['aphar](../../strongs/h/h6083.md).

<a name="2samuel_16_14"></a>2Samuel 16:14

And the [melek](../../strongs/h/h4428.md), and all the ['am](../../strongs/h/h5971.md) that were with him, [bow'](../../strongs/h/h935.md) [ʿāyēp̄](../../strongs/h/h5889.md), and [nāp̄aš](../../strongs/h/h5314.md) themselves there.

<a name="2samuel_16_15"></a>2Samuel 16:15

And ['Ăbyšālôm](../../strongs/h/h53.md), and all the ['am](../../strongs/h/h5971.md) the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and ['Ăḥîṯōp̄El](../../strongs/h/h302.md) with him.

<a name="2samuel_16_16"></a>2Samuel 16:16

And it came to pass, when [Ḥûšay](../../strongs/h/h2365.md) the ['arkî](../../strongs/h/h757.md), [Dāviḏ](../../strongs/h/h1732.md) [rēʿê](../../strongs/h/h7463.md), was [bow'](../../strongs/h/h935.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), that [Ḥûšay](../../strongs/h/h2365.md) ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), [ḥāyâ](../../strongs/h/h2421.md) the [melek](../../strongs/h/h4428.md), [ḥāyâ](../../strongs/h/h2421.md) the [melek](../../strongs/h/h4428.md).

<a name="2samuel_16_17"></a>2Samuel 16:17

And ['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) to [Ḥûšay](../../strongs/h/h2365.md), Is this thy [checed](../../strongs/h/h2617.md) to thy [rea'](../../strongs/h/h7453.md)? why [halak](../../strongs/h/h1980.md) thou not with thy [rea'](../../strongs/h/h7453.md)?

<a name="2samuel_16_18"></a>2Samuel 16:18

And [Ḥûšay](../../strongs/h/h2365.md) ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), Nay; but whom [Yĕhovah](../../strongs/h/h3068.md), and this ['am](../../strongs/h/h5971.md), and all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), [bāḥar](../../strongs/h/h977.md), his will I be, and with him will I [yashab](../../strongs/h/h3427.md).

<a name="2samuel_16_19"></a>2Samuel 16:19

And again, whom should I ['abad](../../strongs/h/h5647.md)? should I not serve in the [paniym](../../strongs/h/h6440.md) of his [ben](../../strongs/h/h1121.md)? as I have ['abad](../../strongs/h/h5647.md) in thy ['ab](../../strongs/h/h1.md) [paniym](../../strongs/h/h6440.md), so will I be in thy [paniym](../../strongs/h/h6440.md).

<a name="2samuel_16_20"></a>2Samuel 16:20

Then ['āmar](../../strongs/h/h559.md) ['Ăbyšālôm](../../strongs/h/h53.md) to ['Ăḥîṯōp̄El](../../strongs/h/h302.md), [yāhaḇ](../../strongs/h/h3051.md) ['etsah](../../strongs/h/h6098.md) among you what we shall ['asah](../../strongs/h/h6213.md).

<a name="2samuel_16_21"></a>2Samuel 16:21

And ['Ăḥîṯōp̄El](../../strongs/h/h302.md) ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), [bow'](../../strongs/h/h935.md) unto thy ['ab](../../strongs/h/h1.md) [pîleḡeš](../../strongs/h/h6370.md), which he hath [yānaḥ](../../strongs/h/h3240.md) to [shamar](../../strongs/h/h8104.md) the [bayith](../../strongs/h/h1004.md); and all [Yisra'el](../../strongs/h/h3478.md) shall [shama'](../../strongs/h/h8085.md) that thou art [bā'aš](../../strongs/h/h887.md) of thy ['ab](../../strongs/h/h1.md): then shall the [yad](../../strongs/h/h3027.md) of all that are with thee be [ḥāzaq](../../strongs/h/h2388.md).

<a name="2samuel_16_22"></a>2Samuel 16:22

So they [natah](../../strongs/h/h5186.md) ['Ăbyšālôm](../../strongs/h/h53.md) a ['ohel](../../strongs/h/h168.md) upon the top of the [gāḡ](../../strongs/h/h1406.md); and ['Ăbyšālôm](../../strongs/h/h53.md) [bow'](../../strongs/h/h935.md) unto his ['ab](../../strongs/h/h1.md) [pîleḡeš](../../strongs/h/h6370.md) in the ['ayin](../../strongs/h/h5869.md) of all [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_16_23"></a>2Samuel 16:23

And the ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md), which he [ya'ats](../../strongs/h/h3289.md) in those [yowm](../../strongs/h/h3117.md), was as if an ['iysh](../../strongs/h/h376.md) had [sha'al](../../strongs/h/h7592.md) at the [dabar](../../strongs/h/h1697.md) of ['Elohiym](../../strongs/h/h430.md): so was all the ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md) both with [Dāviḏ](../../strongs/h/h1732.md) and with ['Ăbyšālôm](../../strongs/h/h53.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 15](2samuel_15.md) - [2Samuel 17](2samuel_17.md)