# [2Samuel 15](https://www.blueletterbible.org/kjv/2samuel/15)

<a name="2samuel_15_1"></a>2Samuel 15:1

And it came to pass ['aḥar](../../strongs/h/h310.md), that ['Ăbyšālôm](../../strongs/h/h53.md) ['asah](../../strongs/h/h6213.md) him [merkāḇâ](../../strongs/h/h4818.md) and [sûs](../../strongs/h/h5483.md), and fifty ['iysh](../../strongs/h/h376.md) to [rûṣ](../../strongs/h/h7323.md) [paniym](../../strongs/h/h6440.md) him.

<a name="2samuel_15_2"></a>2Samuel 15:2

And ['Ăbyšālôm](../../strongs/h/h53.md) [šāḵam](../../strongs/h/h7925.md), and ['amad](../../strongs/h/h5975.md) [yad](../../strongs/h/h3027.md) the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md): and it was so, that when any ['iysh](../../strongs/h/h376.md) that had a [rîḇ](../../strongs/h/h7379.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) for [mishpat](../../strongs/h/h4941.md), then ['Ăbyšālôm](../../strongs/h/h53.md) [qara'](../../strongs/h/h7121.md) unto him, and ['āmar](../../strongs/h/h559.md), Of what [ʿîr](../../strongs/h/h5892.md) art thou? And he ['āmar](../../strongs/h/h559.md), Thy ['ebed](../../strongs/h/h5650.md) is of one of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_15_3"></a>2Samuel 15:3

And ['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) unto him, [ra'ah](../../strongs/h/h7200.md), thy [dabar](../../strongs/h/h1697.md) are [towb](../../strongs/h/h2896.md) and [nāḵōaḥ](../../strongs/h/h5228.md); but there is no man deputed of the [melek](../../strongs/h/h4428.md) to [shama'](../../strongs/h/h8085.md) thee.

<a name="2samuel_15_4"></a>2Samuel 15:4

['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) moreover, Oh that I were [śûm](../../strongs/h/h7760.md) [shaphat](../../strongs/h/h8199.md) in the ['erets](../../strongs/h/h776.md), that every ['iysh](../../strongs/h/h376.md) which hath any [rîḇ](../../strongs/h/h7379.md) or [mishpat](../../strongs/h/h4941.md) might [bow'](../../strongs/h/h935.md) unto me, and I would do him [ṣāḏaq](../../strongs/h/h6663.md)!

<a name="2samuel_15_5"></a>2Samuel 15:5

And it was so, that when any ['iysh](../../strongs/h/h376.md) [qāraḇ](../../strongs/h/h7126.md) to him to do him [shachah](../../strongs/h/h7812.md), he [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [ḥāzaq](../../strongs/h/h2388.md) him, and [nashaq](../../strongs/h/h5401.md) him.

<a name="2samuel_15_6"></a>2Samuel 15:6

And on this [dabar](../../strongs/h/h1697.md) ['asah](../../strongs/h/h6213.md) ['Ăbyšālôm](../../strongs/h/h53.md) to all [Yisra'el](../../strongs/h/h3478.md) that [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) for [mishpat](../../strongs/h/h4941.md): so ['Ăbyšālôm](../../strongs/h/h53.md) [ganab](../../strongs/h/h1589.md) the [leb](../../strongs/h/h3820.md) of the ['enowsh](../../strongs/h/h582.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_15_7"></a>2Samuel 15:7

And it came to pass [qēṣ](../../strongs/h/h7093.md) forty [šānâ](../../strongs/h/h8141.md), that ['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), I pray thee, let me [yālaḵ](../../strongs/h/h3212.md) and [shalam](../../strongs/h/h7999.md) my [neḏer](../../strongs/h/h5088.md), which I have [nāḏar](../../strongs/h/h5087.md) unto [Yĕhovah](../../strongs/h/h3068.md), in [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_15_8"></a>2Samuel 15:8

For thy ['ebed](../../strongs/h/h5650.md) [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) while I [yashab](../../strongs/h/h3427.md) at [Gᵊšûr](../../strongs/h/h1650.md) in ['Ărām](../../strongs/h/h758.md), ['āmar](../../strongs/h/h559.md), If [Yĕhovah](../../strongs/h/h3068.md) shall bring me [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) indeed to [Yĕruwshalaim](../../strongs/h/h3389.md), then I will ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2samuel_15_9"></a>2Samuel 15:9

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md). So he [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) to [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_15_10"></a>2Samuel 15:10

But ['Ăbyšālôm](../../strongs/h/h53.md) [shalach](../../strongs/h/h7971.md) [ragal](../../strongs/h/h7270.md) throughout all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), As soon as ye [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), then ye shall ['āmar](../../strongs/h/h559.md), ['Ăbyšālôm](../../strongs/h/h53.md) [mālaḵ](../../strongs/h/h4427.md) in [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="2samuel_15_11"></a>2Samuel 15:11

And with ['Ăbyšālôm](../../strongs/h/h53.md) [halak](../../strongs/h/h1980.md) two hundred ['iysh](../../strongs/h/h376.md) out of [Yĕruwshalaim](../../strongs/h/h3389.md), that were [qara'](../../strongs/h/h7121.md); and they [halak](../../strongs/h/h1980.md) in their [tom](../../strongs/h/h8537.md), and they [yada'](../../strongs/h/h3045.md) not any [dabar](../../strongs/h/h1697.md).

<a name="2samuel_15_12"></a>2Samuel 15:12

And ['Ăbyšālôm](../../strongs/h/h53.md) [shalach](../../strongs/h/h7971.md) for ['Ăḥîṯōp̄El](../../strongs/h/h302.md) the [Gîlōnî](../../strongs/h/h1526.md), [Dāviḏ](../../strongs/h/h1732.md) [ya'ats](../../strongs/h/h3289.md), from his [ʿîr](../../strongs/h/h5892.md), even from [Gilô](../../strongs/h/h1542.md), while he [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md). And the [qešer](../../strongs/h/h7195.md) was ['ammîṣ](../../strongs/h/h533.md); for the ['am](../../strongs/h/h5971.md) [rab](../../strongs/h/h7227.md) [halak](../../strongs/h/h1980.md) with ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_15_13"></a>2Samuel 15:13

And there [bow'](../../strongs/h/h935.md) a [nāḡaḏ](../../strongs/h/h5046.md) to [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), The [leb](../../strongs/h/h3820.md) of the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) are ['aḥar](../../strongs/h/h310.md) ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_15_14"></a>2Samuel 15:14

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto all his ['ebed](../../strongs/h/h5650.md) that were with him at [Yĕruwshalaim](../../strongs/h/h3389.md), [quwm](../../strongs/h/h6965.md), and let us [bāraḥ](../../strongs/h/h1272.md); for we shall not else [pᵊlêṭâ](../../strongs/h/h6413.md) [paniym](../../strongs/h/h6440.md) ['Ăbyšālôm](../../strongs/h/h53.md): make [māhar](../../strongs/h/h4116.md) to [yālaḵ](../../strongs/h/h3212.md), lest he [nāśaḡ](../../strongs/h/h5381.md) us [māhar](../../strongs/h/h4116.md), and [nāḏaḥ](../../strongs/h/h5080.md) [ra'](../../strongs/h/h7451.md) upon us, and [nakah](../../strongs/h/h5221.md) the [ʿîr](../../strongs/h/h5892.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="2samuel_15_15"></a>2Samuel 15:15

And the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Behold, thy ['ebed](../../strongs/h/h5650.md) are ready to do whatsoever my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) shall [bāḥar](../../strongs/h/h977.md).

<a name="2samuel_15_16"></a>2Samuel 15:16

And the [melek](../../strongs/h/h4428.md) [yāṣā'](../../strongs/h/h3318.md), and all his [bayith](../../strongs/h/h1004.md) [regel](../../strongs/h/h7272.md) him. And the [melek](../../strongs/h/h4428.md) ['azab](../../strongs/h/h5800.md) ten ['ishshah](../../strongs/h/h802.md), which were [pîleḡeš](../../strongs/h/h6370.md), to [shamar](../../strongs/h/h8104.md) the [bayith](../../strongs/h/h1004.md).

<a name="2samuel_15_17"></a>2Samuel 15:17

And the [melek](../../strongs/h/h4428.md) [yāṣā'](../../strongs/h/h3318.md), and all the ['am](../../strongs/h/h5971.md) [regel](../../strongs/h/h7272.md) him, and ['amad](../../strongs/h/h5975.md) in a [bayith](../../strongs/h/h1004.md) that was far [merḥāq](../../strongs/h/h4801.md) [Bêṯ Hammerḥāq](../../strongs/h/h1023.md).

<a name="2samuel_15_18"></a>2Samuel 15:18

And all his ['ebed](../../strongs/h/h5650.md) ['abar](../../strongs/h/h5674.md) [yad](../../strongs/h/h3027.md) him; and all the [kᵊrēṯî](../../strongs/h/h3774.md), and all the [pᵊlēṯî](../../strongs/h/h6432.md), and all the [Gitî](../../strongs/h/h1663.md), six hundred ['iysh](../../strongs/h/h376.md) which [bow'](../../strongs/h/h935.md) [regel](../../strongs/h/h7272.md) him from [Gaṯ](../../strongs/h/h1661.md), ['abar](../../strongs/h/h5674.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="2samuel_15_19"></a>2Samuel 15:19

Then ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md) to ['Itay](../../strongs/h/h863.md) the [Gitî](../../strongs/h/h1663.md), Wherefore [yālaḵ](../../strongs/h/h3212.md) thou also with us? [shuwb](../../strongs/h/h7725.md) to thy [maqowm](../../strongs/h/h4725.md), and [yashab](../../strongs/h/h3427.md) with the [melek](../../strongs/h/h4428.md): for thou art a [nāḵrî](../../strongs/h/h5237.md), and also a [gālâ](../../strongs/h/h1540.md).

<a name="2samuel_15_20"></a>2Samuel 15:20

Whereas thou [bow'](../../strongs/h/h935.md) but [tᵊmôl](../../strongs/h/h8543.md), should I this [yowm](../../strongs/h/h3117.md) make thee [yālaḵ](../../strongs/h/h3212.md) up and [nûaʿ](../../strongs/h/h5128.md) [nûaʿ](../../strongs/h/h5128.md) with us? seeing I [halak](../../strongs/h/h1980.md) whither I may, [shuwb](../../strongs/h/h7725.md) thou, and take [shuwb](../../strongs/h/h7725.md) thy ['ach](../../strongs/h/h251.md): [checed](../../strongs/h/h2617.md) and ['emeth](../../strongs/h/h571.md) be with thee.

<a name="2samuel_15_21"></a>2Samuel 15:21

And ['Itay](../../strongs/h/h863.md) ['anah](../../strongs/h/h6030.md) the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), and as my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [chay](../../strongs/h/h2416.md), surely in what [maqowm](../../strongs/h/h4725.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) shall be, whether in [maveth](../../strongs/h/h4194.md) or [chay](../../strongs/h/h2416.md), even there also will thy ['ebed](../../strongs/h/h5650.md) be.

<a name="2samuel_15_22"></a>2Samuel 15:22

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) to ['Itay](../../strongs/h/h863.md), [yālaḵ](../../strongs/h/h3212.md) and ['abar](../../strongs/h/h5674.md). And ['Itay](../../strongs/h/h863.md) the [Gitî](../../strongs/h/h1663.md) ['abar](../../strongs/h/h5674.md), and all his ['enowsh](../../strongs/h/h582.md), and all the [ṭap̄](../../strongs/h/h2945.md) that were with him.

<a name="2samuel_15_23"></a>2Samuel 15:23

And all the ['erets](../../strongs/h/h776.md) [bāḵâ](../../strongs/h/h1058.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), and all the ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md): the [melek](../../strongs/h/h4428.md) also himself ['abar](../../strongs/h/h5674.md) the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md), and all the ['am](../../strongs/h/h5971.md) ['abar](../../strongs/h/h5674.md), [paniym](../../strongs/h/h6440.md) the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md).

<a name="2samuel_15_24"></a>2Samuel 15:24

And lo [Ṣāḏôq](../../strongs/h/h6659.md) also, and all the [Lᵊvî](../../strongs/h/h3881.md) were with him, [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md): and they set [yāṣaq](../../strongs/h/h3332.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md); and ['Eḇyāṯār](../../strongs/h/h54.md) [ʿālâ](../../strongs/h/h5927.md), until all the ['am](../../strongs/h/h5971.md) had [tamam](../../strongs/h/h8552.md) ['abar](../../strongs/h/h5674.md) out of the [ʿîr](../../strongs/h/h5892.md).

<a name="2samuel_15_25"></a>2Samuel 15:25

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Ṣāḏôq](../../strongs/h/h6659.md), [shuwb](../../strongs/h/h7725.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) into the [ʿîr](../../strongs/h/h5892.md): if I shall [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), he will [shuwb](../../strongs/h/h7725.md) me, and [ra'ah](../../strongs/h/h7200.md) me both it, and his [nāvê](../../strongs/h/h5116.md):

<a name="2samuel_15_26"></a>2Samuel 15:26

But if he thus ['āmar](../../strongs/h/h559.md), I have no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in thee; behold, here am I, let him ['asah](../../strongs/h/h6213.md) to me as ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto him.

<a name="2samuel_15_27"></a>2Samuel 15:27

The [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) also unto [Ṣāḏôq](../../strongs/h/h6659.md) the [kōhēn](../../strongs/h/h3548.md), Art not thou a [ra'ah](../../strongs/h/h7200.md)? [shuwb](../../strongs/h/h7725.md) into the [ʿîr](../../strongs/h/h5892.md) in [shalowm](../../strongs/h/h7965.md), and your two [ben](../../strongs/h/h1121.md) with you, ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) thy [ben](../../strongs/h/h1121.md), and [Yᵊhônāṯān](../../strongs/h/h3083.md) the [ben](../../strongs/h/h1121.md) of ['Eḇyāṯār](../../strongs/h/h54.md).

<a name="2samuel_15_28"></a>2Samuel 15:28

[ra'ah](../../strongs/h/h7200.md), I will [māhah](../../strongs/h/h4102.md) in the ['arabah](../../strongs/h/h6160.md) [ʿăḇārâ](../../strongs/h/h5679.md) of the [midbar](../../strongs/h/h4057.md), until there [bow'](../../strongs/h/h935.md) [dabar](../../strongs/h/h1697.md) from you to [nāḡaḏ](../../strongs/h/h5046.md) me.

<a name="2samuel_15_29"></a>2Samuel 15:29

[Ṣāḏôq](../../strongs/h/h6659.md) therefore and ['Eḇyāṯār](../../strongs/h/h54.md) [shuwb](../../strongs/h/h7725.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md): and they [yashab](../../strongs/h/h3427.md) there.

<a name="2samuel_15_30"></a>2Samuel 15:30

And [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) by the [maʿălê](../../strongs/h/h4608.md) of [zayiṯ](../../strongs/h/h2132.md), and [bāḵâ](../../strongs/h/h1058.md) as he [ʿālâ](../../strongs/h/h5927.md), and had his [ro'sh](../../strongs/h/h7218.md) [ḥāp̄â](../../strongs/h/h2645.md), and he [halak](../../strongs/h/h1980.md) [yāḥēp̄](../../strongs/h/h3182.md): and all the ['am](../../strongs/h/h5971.md) that was with him [ḥāp̄â](../../strongs/h/h2645.md) every ['iysh](../../strongs/h/h376.md) his [ro'sh](../../strongs/h/h7218.md), and they [ʿālâ](../../strongs/h/h5927.md), [bāḵâ](../../strongs/h/h1058.md) as they [ʿālâ](../../strongs/h/h5927.md).

<a name="2samuel_15_31"></a>2Samuel 15:31

And one [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), ['Ăḥîṯōp̄El](../../strongs/h/h302.md) is among the [qāšar](../../strongs/h/h7194.md) with ['Ăbyšālôm](../../strongs/h/h53.md). And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), I pray thee, turn the ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md) into [sāḵal](../../strongs/h/h5528.md).

<a name="2samuel_15_32"></a>2Samuel 15:32

And it came to pass, that when [Dāviḏ](../../strongs/h/h1732.md) was [bow'](../../strongs/h/h935.md) to the [ro'sh](../../strongs/h/h7218.md), where he [shachah](../../strongs/h/h7812.md) ['Elohiym](../../strongs/h/h430.md), behold, [Ḥûšay](../../strongs/h/h2365.md) the ['arkî](../../strongs/h/h757.md) came to [qārā'](../../strongs/h/h7125.md) him with his [kĕthoneth](../../strongs/h/h3801.md) [qāraʿ](../../strongs/h/h7167.md), and ['ăḏāmâ](../../strongs/h/h127.md) upon his [ro'sh](../../strongs/h/h7218.md):

<a name="2samuel_15_33"></a>2Samuel 15:33

Unto whom [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), If thou ['abar](../../strongs/h/h5674.md) with me, then thou shalt be a [maśśā'](../../strongs/h/h4853.md) unto me:

<a name="2samuel_15_34"></a>2Samuel 15:34

But if thou [shuwb](../../strongs/h/h7725.md) to the [ʿîr](../../strongs/h/h5892.md), and ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), I will be thy ['ebed](../../strongs/h/h5650.md), O [melek](../../strongs/h/h4428.md); as I have been thy ['ab](../../strongs/h/h1.md) ['ebed](../../strongs/h/h5650.md) hitherto, so will I now also be thy ['ebed](../../strongs/h/h5650.md): then mayest thou for me [pārar](../../strongs/h/h6565.md) the ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md).

<a name="2samuel_15_35"></a>2Samuel 15:35

And hast thou not there with thee [Ṣāḏôq](../../strongs/h/h6659.md) and ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md)? therefore it shall be, that what [dabar](../../strongs/h/h1697.md) soever thou shalt [shama'](../../strongs/h/h8085.md) out of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), thou shalt [nāḡaḏ](../../strongs/h/h5046.md) it to [Ṣāḏôq](../../strongs/h/h6659.md) and ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="2samuel_15_36"></a>2Samuel 15:36

Behold, they have there with them their two [ben](../../strongs/h/h1121.md), ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) [Ṣāḏôq](../../strongs/h/h6659.md) son, and [Yᵊhônāṯān](../../strongs/h/h3083.md) ['Eḇyāṯār](../../strongs/h/h54.md) son; and by [yad](../../strongs/h/h3027.md) ye shall [shalach](../../strongs/h/h7971.md) unto me every [dabar](../../strongs/h/h1697.md) that ye can [shama'](../../strongs/h/h8085.md).

<a name="2samuel_15_37"></a>2Samuel 15:37

So [Ḥûšay](../../strongs/h/h2365.md) [Dāviḏ](../../strongs/h/h1732.md) [rēʿê](../../strongs/h/h7463.md) [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), and ['Ăbyšālôm](../../strongs/h/h53.md) [bow'](../../strongs/h/h935.md) into [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 14](2samuel_14.md) - [2Samuel 16](2samuel_16.md)