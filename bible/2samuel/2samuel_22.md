# [2Samuel 22](https://www.blueletterbible.org/kjv/2samuel/22)

<a name="2samuel_22_1"></a>2Samuel 22:1

And [Dāviḏ](../../strongs/h/h1732.md) [dabar](../../strongs/h/h1696.md) unto [Yĕhovah](../../strongs/h/h3068.md) the [dabar](../../strongs/h/h1697.md) of this [šîr](../../strongs/h/h7892.md) in the [yowm](../../strongs/h/h3117.md) that [Yĕhovah](../../strongs/h/h3068.md) had [natsal](../../strongs/h/h5337.md) him out of the [kaph](../../strongs/h/h3709.md) of all his ['oyeb](../../strongs/h/h341.md), and out of the [kaph](../../strongs/h/h3709.md) of [Šā'ûl](../../strongs/h/h7586.md):

<a name="2samuel_22_2"></a>2Samuel 22:2

And he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) is my [cela'](../../strongs/h/h5553.md), and my [matsuwd](../../strongs/h/h4686.md), and my [palat](../../strongs/h/h6403.md);

<a name="2samuel_22_3"></a>2Samuel 22:3

The ['Elohiym](../../strongs/h/h430.md) of my [tsuwr](../../strongs/h/h6697.md); in him will I [chacah](../../strongs/h/h2620.md): he is my [magen](../../strongs/h/h4043.md), and the [qeren](../../strongs/h/h7161.md) of my [yesha'](../../strongs/h/h3468.md), my [misgab](../../strongs/h/h4869.md), and my [mānôs](../../strongs/h/h4498.md), my [yasha'](../../strongs/h/h3467.md); thou [yasha'](../../strongs/h/h3467.md) me from [chamac](../../strongs/h/h2555.md).

<a name="2samuel_22_4"></a>2Samuel 22:4

I will [qara'](../../strongs/h/h7121.md) on [Yĕhovah](../../strongs/h/h3068.md), who is [halal](../../strongs/h/h1984.md): so shall I be [yasha'](../../strongs/h/h3467.md) from mine ['oyeb](../../strongs/h/h341.md).

<a name="2samuel_22_5"></a>2Samuel 22:5

When the [mišbār](../../strongs/h/h4867.md) of [maveth](../../strongs/h/h4194.md) ['āp̄ap̄](../../strongs/h/h661.md) me, the [nachal](../../strongs/h/h5158.md) of [beliya'al](../../strongs/h/h1100.md) made me [ba'ath](../../strongs/h/h1204.md);

<a name="2samuel_22_6"></a>2Samuel 22:6

The [chebel](../../strongs/h/h2256.md) of [shĕ'owl](../../strongs/h/h7585.md) [cabab](../../strongs/h/h5437.md) me; the [mowqesh](../../strongs/h/h4170.md) of [maveth](../../strongs/h/h4194.md) [qadam](../../strongs/h/h6923.md) me;

<a name="2samuel_22_7"></a>2Samuel 22:7

In my [tsar](../../strongs/h/h6862.md) I [qara'](../../strongs/h/h7121.md) upon [Yĕhovah](../../strongs/h/h3068.md), and [qara'](../../strongs/h/h7121.md) to my ['Elohiym](../../strongs/h/h430.md): and he did [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md) out of his [heykal](../../strongs/h/h1964.md), and my [shav'ah](../../strongs/h/h7775.md) did enter into his ['ozen](../../strongs/h/h241.md).

<a name="2samuel_22_8"></a>2Samuel 22:8

Then the ['erets](../../strongs/h/h776.md) [gāʿaš](../../strongs/h/h1607.md) [gāʿaš](../../strongs/h/h1607.md) and [rāʿaš](../../strongs/h/h7493.md); the [mowcadah](../../strongs/h/h4146.md) of [shamayim](../../strongs/h/h8064.md) [ragaz](../../strongs/h/h7264.md) and [gāʿaš](../../strongs/h/h1607.md), because he was [ḥārâ](../../strongs/h/h2734.md).

<a name="2samuel_22_9"></a>2Samuel 22:9

There [ʿālâ](../../strongs/h/h5927.md) an ['ashan](../../strongs/h/h6227.md) out of his ['aph](../../strongs/h/h639.md), and ['esh](../../strongs/h/h784.md) out of his [peh](../../strongs/h/h6310.md) ['akal](../../strongs/h/h398.md): [gechel](../../strongs/h/h1513.md) were [bāʿar](../../strongs/h/h1197.md) by it.

<a name="2samuel_22_10"></a>2Samuel 22:10

He [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) also, and [yarad](../../strongs/h/h3381.md); and ['araphel](../../strongs/h/h6205.md) was under his [regel](../../strongs/h/h7272.md).

<a name="2samuel_22_11"></a>2Samuel 22:11

And he [rāḵaḇ](../../strongs/h/h7392.md) upon a [kĕruwb](../../strongs/h/h3742.md), and did ['uwph](../../strongs/h/h5774.md): and he was [ra'ah](../../strongs/h/h7200.md) upon the [kanaph](../../strongs/h/h3671.md) of the [ruwach](../../strongs/h/h7307.md).

<a name="2samuel_22_12"></a>2Samuel 22:12

And he [shiyth](../../strongs/h/h7896.md) [choshek](../../strongs/h/h2822.md) [cukkah](../../strongs/h/h5521.md) [cabiyb](../../strongs/h/h5439.md) him, [ḥašrâ](../../strongs/h/h2841.md) [mayim](../../strongs/h/h4325.md), and thick ['ab](../../strongs/h/h5645.md) of the [shachaq](../../strongs/h/h7834.md).

<a name="2samuel_22_13"></a>2Samuel 22:13

Through the [nogahh](../../strongs/h/h5051.md) before him were [gechel](../../strongs/h/h1513.md) of ['esh](../../strongs/h/h784.md) [bāʿar](../../strongs/h/h1197.md).

<a name="2samuel_22_14"></a>2Samuel 22:14

[Yĕhovah](../../strongs/h/h3068.md) [ra'am](../../strongs/h/h7481.md) from [shamayim](../../strongs/h/h8064.md), and the ['elyown](../../strongs/h/h5945.md) [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md).

<a name="2samuel_22_15"></a>2Samuel 22:15

And he [shalach](../../strongs/h/h7971.md) [chets](../../strongs/h/h2671.md), and [puwts](../../strongs/h/h6327.md) them; [baraq](../../strongs/h/h1300.md), and [hāmam](../../strongs/h/h2000.md) them.

<a name="2samuel_22_16"></a>2Samuel 22:16

And the ['āp̄îq](../../strongs/h/h650.md) of the [yam](../../strongs/h/h3220.md) [ra'ah](../../strongs/h/h7200.md), the [mowcadah](../../strongs/h/h4146.md) of the [tebel](../../strongs/h/h8398.md) were [gālâ](../../strongs/h/h1540.md), at the [ge'arah](../../strongs/h/h1606.md) of [Yĕhovah](../../strongs/h/h3068.md), at the [neshamah](../../strongs/h/h5397.md) of the [ruwach](../../strongs/h/h7307.md) of his ['aph](../../strongs/h/h639.md).

<a name="2samuel_22_17"></a>2Samuel 22:17

He [shalach](../../strongs/h/h7971.md) from [marowm](../../strongs/h/h4791.md), he [laqach](../../strongs/h/h3947.md) me; he [mashah](../../strongs/h/h4871.md) me out of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md);

<a name="2samuel_22_18"></a>2Samuel 22:18

He [natsal](../../strongs/h/h5337.md) me from my ['az](../../strongs/h/h5794.md) ['oyeb](../../strongs/h/h341.md), and from them that [sane'](../../strongs/h/h8130.md) me: for they were too ['amats](../../strongs/h/h553.md) for me.

<a name="2samuel_22_19"></a>2Samuel 22:19

They [qadam](../../strongs/h/h6923.md) me in the [yowm](../../strongs/h/h3117.md) of my ['êḏ](../../strongs/h/h343.md): but [Yĕhovah](../../strongs/h/h3068.md) was my [mašʿēn](../../strongs/h/h4937.md).

<a name="2samuel_22_20"></a>2Samuel 22:20

He [yāṣā'](../../strongs/h/h3318.md) me also into a [merḥāḇ](../../strongs/h/h4800.md): he [chalats](../../strongs/h/h2502.md) me, because he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in me.

<a name="2samuel_22_21"></a>2Samuel 22:21

[Yĕhovah](../../strongs/h/h3068.md) [gamal](../../strongs/h/h1580.md) me according to my [tsedaqah](../../strongs/h/h6666.md): according to the [bōr](../../strongs/h/h1252.md) of my [yad](../../strongs/h/h3027.md) hath he [shuwb](../../strongs/h/h7725.md) me.

<a name="2samuel_22_22"></a>2Samuel 22:22

For I have [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), and have not [rāšaʿ](../../strongs/h/h7561.md) from my ['Elohiym](../../strongs/h/h430.md).

<a name="2samuel_22_23"></a>2Samuel 22:23

For all his [mishpat](../../strongs/h/h4941.md) were before me: and as for his [chuqqah](../../strongs/h/h2708.md), I did not [cuwr](../../strongs/h/h5493.md) from them.

<a name="2samuel_22_24"></a>2Samuel 22:24

I was also [tamiym](../../strongs/h/h8549.md) before him, and have [shamar](../../strongs/h/h8104.md) myself from mine ['avon](../../strongs/h/h5771.md).

<a name="2samuel_22_25"></a>2Samuel 22:25

Therefore [Yĕhovah](../../strongs/h/h3068.md) hath [shuwb](../../strongs/h/h7725.md) me according to my [tsedaqah](../../strongs/h/h6666.md); according to my [bōr](../../strongs/h/h1252.md) in his [neḡeḏ](../../strongs/h/h5048.md) ['ayin](../../strongs/h/h5869.md).

<a name="2samuel_22_26"></a>2Samuel 22:26

With the [chaciyd](../../strongs/h/h2623.md) thou wilt [ḥāsaḏ](../../strongs/h/h2616.md), and with the [tamiym](../../strongs/h/h8549.md) [gibôr](../../strongs/h/h1368.md) thou wilt [tamam](../../strongs/h/h8552.md).

<a name="2samuel_22_27"></a>2Samuel 22:27

With the [bārar](../../strongs/h/h1305.md) thou wilt [bārar](../../strongs/h/h1305.md); and with the [ʿiqqēš](../../strongs/h/h6141.md) thou wilt [pāṯal](../../strongs/h/h6617.md).

<a name="2samuel_22_28"></a>2Samuel 22:28

And the ['aniy](../../strongs/h/h6041.md) ['am](../../strongs/h/h5971.md) thou wilt [yasha'](../../strongs/h/h3467.md): but thine ['ayin](../../strongs/h/h5869.md) are upon the [ruwm](../../strongs/h/h7311.md), that thou mayest [šāp̄ēl](../../strongs/h/h8213.md) them.

<a name="2samuel_22_29"></a>2Samuel 22:29

For thou art my [nîr](../../strongs/h/h5216.md), [Yĕhovah](../../strongs/h/h3068.md): and [Yĕhovah](../../strongs/h/h3068.md) will [nāḡahh](../../strongs/h/h5050.md) my [choshek](../../strongs/h/h2822.md).

<a name="2samuel_22_30"></a>2Samuel 22:30

For by thee I have [rûṣ](../../strongs/h/h7323.md) through a [gᵊḏûḏ](../../strongs/h/h1416.md): by my ['Elohiym](../../strongs/h/h430.md) have I [dālaḡ](../../strongs/h/h1801.md) a [šûrâ](../../strongs/h/h7791.md).

<a name="2samuel_22_31"></a>2Samuel 22:31

As for ['el](../../strongs/h/h410.md), his [derek](../../strongs/h/h1870.md) is [tamiym](../../strongs/h/h8549.md); the ['imrah](../../strongs/h/h565.md) of [Yĕhovah](../../strongs/h/h3068.md) is [tsaraph](../../strongs/h/h6884.md): he is a [magen](../../strongs/h/h4043.md) to all them that [chacah](../../strongs/h/h2620.md) in him.

<a name="2samuel_22_32"></a>2Samuel 22:32

For who is ['el](../../strongs/h/h410.md), [bilʿăḏê](../../strongs/h/h1107.md) [Yĕhovah](../../strongs/h/h3068.md)? and who is a [tsuwr](../../strongs/h/h6697.md), [bilʿăḏê](../../strongs/h/h1107.md) our ['Elohiym](../../strongs/h/h430.md)?

<a name="2samuel_22_33"></a>2Samuel 22:33

['el](../../strongs/h/h410.md) is my [māʿôz](../../strongs/h/h4581.md) and [ḥayil](../../strongs/h/h2428.md): and he [nāṯar](../../strongs/h/h5425.md) my [derek](../../strongs/h/h1870.md) [tamiym](../../strongs/h/h8549.md).

<a name="2samuel_22_34"></a>2Samuel 22:34

He [šāvâ](../../strongs/h/h7737.md) my [regel](../../strongs/h/h7272.md) [šāvâ](../../strongs/h/h7737.md) ['ayyālâ](../../strongs/h/h355.md): and ['amad](../../strongs/h/h5975.md) me upon my [bāmâ](../../strongs/h/h1116.md).

<a name="2samuel_22_35"></a>2Samuel 22:35

He [lamad](../../strongs/h/h3925.md) my [yad](../../strongs/h/h3027.md) to [milḥāmâ](../../strongs/h/h4421.md); so that a [qesheth](../../strongs/h/h7198.md) of [nᵊḥûšâ](../../strongs/h/h5154.md) is [nāḥaṯ](../../strongs/h/h5181.md) by mine [zerowa'](../../strongs/h/h2220.md).

<a name="2samuel_22_36"></a>2Samuel 22:36

Thou hast also [nathan](../../strongs/h/h5414.md) me the [magen](../../strongs/h/h4043.md) of thy [yesha'](../../strongs/h/h3468.md): and thy [ʿānâ](../../strongs/h/h6031.md) [ʿănāvâ](../../strongs/h/h6038.md) hath made me [rabah](../../strongs/h/h7235.md).

<a name="2samuel_22_37"></a>2Samuel 22:37

Thou hast [rāḥaḇ](../../strongs/h/h7337.md) my [ṣaʿaḏ](../../strongs/h/h6806.md) under me; so that my [qarsōl](../../strongs/h/h7166.md) did not [māʿaḏ](../../strongs/h/h4571.md).

<a name="2samuel_22_38"></a>2Samuel 22:38

I have [radaph](../../strongs/h/h7291.md) mine ['oyeb](../../strongs/h/h341.md), and [šāmaḏ](../../strongs/h/h8045.md) them; and [shuwb](../../strongs/h/h7725.md) not until I had [kalah](../../strongs/h/h3615.md) them.

<a name="2samuel_22_39"></a>2Samuel 22:39

And I have [kalah](../../strongs/h/h3615.md) them, and [māḥaṣ](../../strongs/h/h4272.md) them, that they could not [quwm](../../strongs/h/h6965.md): yea, they are [naphal](../../strongs/h/h5307.md) under my [regel](../../strongs/h/h7272.md).

<a name="2samuel_22_40"></a>2Samuel 22:40

For thou hast ['āzar](../../strongs/h/h247.md) me with [ḥayil](../../strongs/h/h2428.md) to [milḥāmâ](../../strongs/h/h4421.md): them that [quwm](../../strongs/h/h6965.md) against me hast thou [kara'](../../strongs/h/h3766.md) under me.

<a name="2samuel_22_41"></a>2Samuel 22:41

Thou hast also [nathan](../../strongs/h/h5414.md) me the [ʿōrep̄](../../strongs/h/h6203.md) of mine ['oyeb](../../strongs/h/h341.md), that I might [ṣāmaṯ](../../strongs/h/h6789.md) them that [sane'](../../strongs/h/h8130.md) me.

<a name="2samuel_22_42"></a>2Samuel 22:42

They [šāʿâ](../../strongs/h/h8159.md), but there was none to [yasha'](../../strongs/h/h3467.md); even unto [Yĕhovah](../../strongs/h/h3068.md), but he ['anah](../../strongs/h/h6030.md) them not.

<a name="2samuel_22_43"></a>2Samuel 22:43

Then did I [šāḥaq](../../strongs/h/h7833.md) them as small as the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md), I did [dāqaq](../../strongs/h/h1854.md) them as the [ṭîṭ](../../strongs/h/h2916.md) of the [ḥûṣ](../../strongs/h/h2351.md), and did [rāqaʿ](../../strongs/h/h7554.md) them.

<a name="2samuel_22_44"></a>2Samuel 22:44

Thou also hast [palat](../../strongs/h/h6403.md) me from the [rîḇ](../../strongs/h/h7379.md) of my ['am](../../strongs/h/h5971.md), thou hast [shamar](../../strongs/h/h8104.md) me to be [ro'sh](../../strongs/h/h7218.md) of the [gowy](../../strongs/h/h1471.md): an ['am](../../strongs/h/h5971.md) which I [yada'](../../strongs/h/h3045.md) not shall ['abad](../../strongs/h/h5647.md) me.

<a name="2samuel_22_45"></a>2Samuel 22:45

[ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md) shall [kāḥaš](../../strongs/h/h3584.md) themselves unto me: as soon as they [shama'](../../strongs/h/h8085.md) ['ozen](../../strongs/h/h241.md), they shall be [shama'](../../strongs/h/h8085.md) unto me.

<a name="2samuel_22_46"></a>2Samuel 22:46

[ben](../../strongs/h/h1121.md) [nēḵār](../../strongs/h/h5236.md) shall [nabel](../../strongs/h/h5034.md), and they shall be [ḥāḡar](../../strongs/h/h2296.md) out of their [misgereṯ](../../strongs/h/h4526.md).

<a name="2samuel_22_47"></a>2Samuel 22:47

[Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md); and [barak](../../strongs/h/h1288.md) be my [tsuwr](../../strongs/h/h6697.md); and [ruwm](../../strongs/h/h7311.md) be the ['Elohiym](../../strongs/h/h430.md) of the [tsuwr](../../strongs/h/h6697.md) of my [yesha'](../../strongs/h/h3468.md).

<a name="2samuel_22_48"></a>2Samuel 22:48

It is ['el](../../strongs/h/h410.md) that [nathan](../../strongs/h/h5414.md) [nᵊqāmâ](../../strongs/h/h5360.md) me, and that bringeth [yarad](../../strongs/h/h3381.md) the ['am](../../strongs/h/h5971.md) under me,

<a name="2samuel_22_49"></a>2Samuel 22:49

And that [yāṣā'](../../strongs/h/h3318.md) me from mine ['oyeb](../../strongs/h/h341.md): thou also hast [ruwm](../../strongs/h/h7311.md) me above them that [quwm](../../strongs/h/h6965.md) against me: thou hast [natsal](../../strongs/h/h5337.md) me from the [chamac](../../strongs/h/h2555.md) ['iysh](../../strongs/h/h376.md).

<a name="2samuel_22_50"></a>2Samuel 22:50

Therefore I will [yadah](../../strongs/h/h3034.md) unto thee, [Yĕhovah](../../strongs/h/h3068.md), among the [gowy](../../strongs/h/h1471.md), and I will [zamar](../../strongs/h/h2167.md) unto thy [shem](../../strongs/h/h8034.md).

<a name="2samuel_22_51"></a>2Samuel 22:51

He is the [miḡdôl](../../strongs/h/h4024.md) [gāḏal](../../strongs/h/h1431.md) of [yĕshuw'ah](../../strongs/h/h3444.md) for his [melek](../../strongs/h/h4428.md): and ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) to his [mashiyach](../../strongs/h/h4899.md), unto [Dāviḏ](../../strongs/h/h1732.md), and to his [zera'](../../strongs/h/h2233.md) ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 21](2samuel_21.md) - [2Samuel 23](2samuel_23.md)