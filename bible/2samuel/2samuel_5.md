# [2Samuel 5](https://www.blueletterbible.org/kjv/2samuel/5)

<a name="2samuel_5_1"></a>2Samuel 5:1

Then [bow'](../../strongs/h/h935.md) all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [Dāviḏ](../../strongs/h/h1732.md) unto [Ḥeḇrôn](../../strongs/h/h2275.md), and ['āmar](../../strongs/h/h559.md), ['āmar](../../strongs/h/h559.md), Behold, we are thy ['etsem](../../strongs/h/h6106.md) and thy [basar](../../strongs/h/h1320.md).

<a name="2samuel_5_2"></a>2Samuel 5:2

Also in time ['eṯmôl](../../strongs/h/h865.md) [šilšôm](../../strongs/h/h8032.md), when [Šā'ûl](../../strongs/h/h7586.md) was [melek](../../strongs/h/h4428.md) over us, thou wast he that [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md) [Yisra'el](../../strongs/h/h3478.md): and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to thee, Thou shalt [ra'ah](../../strongs/h/h7462.md) my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and thou shalt be a [nāḡîḏ](../../strongs/h/h5057.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_5_3"></a>2Samuel 5:3

So all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) to [Ḥeḇrôn](../../strongs/h/h2275.md); and [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with them in [Ḥeḇrôn](../../strongs/h/h2275.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and they [māšaḥ](../../strongs/h/h4886.md) [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_5_4"></a>2Samuel 5:4

[Dāviḏ](../../strongs/h/h1732.md) was thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) forty [šānâ](../../strongs/h/h8141.md).

<a name="2samuel_5_5"></a>2Samuel 5:5

In [Ḥeḇrôn](../../strongs/h/h2275.md) he [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md) seven [šānâ](../../strongs/h/h8141.md) and six [ḥōḏeš](../../strongs/h/h2320.md): and in [Yĕruwshalaim](../../strongs/h/h3389.md) he [mālaḵ](../../strongs/h/h4427.md) thirty and three [šānâ](../../strongs/h/h8141.md) over all [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md).

<a name="2samuel_5_6"></a>2Samuel 5:6

And the [melek](../../strongs/h/h4428.md) and his ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [Yᵊḇûsî](../../strongs/h/h2983.md), the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md): which ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), Except thou [cuwr](../../strongs/h/h5493.md) the [ʿiûēr](../../strongs/h/h5787.md) and the [pissēaḥ](../../strongs/h/h6455.md), thou shalt not [bow'](../../strongs/h/h935.md) hither: ['āmar](../../strongs/h/h559.md), [Dāviḏ](../../strongs/h/h1732.md) cannot [bow'](../../strongs/h/h935.md) hither.

<a name="2samuel_5_7"></a>2Samuel 5:7

Nevertheless [Dāviḏ](../../strongs/h/h1732.md) [lāḵaḏ](../../strongs/h/h3920.md) the [matsuwd](../../strongs/h/h4686.md) of [Tsiyown](../../strongs/h/h6726.md): the same is the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_5_8"></a>2Samuel 5:8

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) on that [yowm](../../strongs/h/h3117.md), Whosoever getteth [naga'](../../strongs/h/h5060.md) to the [ṣinnôr](../../strongs/h/h6794.md), and [nakah](../../strongs/h/h5221.md) the [Yᵊḇûsî](../../strongs/h/h2983.md), and the [pissēaḥ](../../strongs/h/h6455.md) and the [ʿiûēr](../../strongs/h/h5787.md), that are [sane'](../../strongs/h/h8130.md) of [Dāviḏ](../../strongs/h/h1732.md) [nephesh](../../strongs/h/h5315.md). Wherefore they ['āmar](../../strongs/h/h559.md), The [ʿiûēr](../../strongs/h/h5787.md) and the [pissēaḥ](../../strongs/h/h6455.md) shall not [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md).

<a name="2samuel_5_9"></a>2Samuel 5:9

So [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) in the [matsuwd](../../strongs/h/h4686.md), and [qara'](../../strongs/h/h7121.md) it the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md). And [Dāviḏ](../../strongs/h/h1732.md) [bānâ](../../strongs/h/h1129.md) [cabiyb](../../strongs/h/h5439.md) from [Millô'](../../strongs/h/h4407.md) and [bayith](../../strongs/h/h1004.md).

<a name="2samuel_5_10"></a>2Samuel 5:10

And [Dāviḏ](../../strongs/h/h1732.md) [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md), and [gadowl](../../strongs/h/h1419.md), and [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md) was with him.

<a name="2samuel_5_11"></a>2Samuel 5:11

And [Ḥîrām](../../strongs/h/h2438.md) [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to [Dāviḏ](../../strongs/h/h1732.md), and ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and [ḥārāš](../../strongs/h/h2796.md), and [qîr](../../strongs/h/h7023.md) ['eben](../../strongs/h/h68.md): and they [bānâ](../../strongs/h/h1129.md) [Dāviḏ](../../strongs/h/h1732.md) a [bayith](../../strongs/h/h1004.md).

<a name="2samuel_5_12"></a>2Samuel 5:12

And [Dāviḏ](../../strongs/h/h1732.md) [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) had [kuwn](../../strongs/h/h3559.md) him [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), and that he had [nasa'](../../strongs/h/h5375.md) his [mamlāḵâ](../../strongs/h/h4467.md) for his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) sake.

<a name="2samuel_5_13"></a>2Samuel 5:13

And [Dāviḏ](../../strongs/h/h1732.md) [laqach](../../strongs/h/h3947.md) him more [pîleḡeš](../../strongs/h/h6370.md) and ['ishshah](../../strongs/h/h802.md) out of [Yĕruwshalaim](../../strongs/h/h3389.md), ['aḥar](../../strongs/h/h310.md) he was [bow'](../../strongs/h/h935.md) from [Ḥeḇrôn](../../strongs/h/h2275.md): and there were yet [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md) [yalad](../../strongs/h/h3205.md) to [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_5_14"></a>2Samuel 5:14

And these be the [shem](../../strongs/h/h8034.md) of those that were [yillôḏ](../../strongs/h/h3209.md) unto him in [Yĕruwshalaim](../../strongs/h/h3389.md); [Šammûaʿ](../../strongs/h/h8051.md), and [Šôḇāḇ](../../strongs/h/h7727.md), and [Nāṯān](../../strongs/h/h5416.md), and [Šᵊlōmô](../../strongs/h/h8010.md),

<a name="2samuel_5_15"></a>2Samuel 5:15

[Yiḇḥār](../../strongs/h/h2984.md) also, and ['Ĕlîšûaʿ](../../strongs/h/h474.md), and [Nep̄eḡ](../../strongs/h/h5298.md), and [Yāp̄îaʿ](../../strongs/h/h3309.md),

<a name="2samuel_5_16"></a>2Samuel 5:16

And ['Ĕlîšāmāʿ](../../strongs/h/h476.md), and ['Elyāḏāʿ](../../strongs/h/h450.md), and ['Ĕlîp̄Eleṭ](../../strongs/h/h467.md).

<a name="2samuel_5_17"></a>2Samuel 5:17

But when the [Pᵊlištî](../../strongs/h/h6430.md) [shama'](../../strongs/h/h8085.md) that they had [māšaḥ](../../strongs/h/h4886.md) [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) over [Yisra'el](../../strongs/h/h3478.md), all the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) to [bāqaš](../../strongs/h/h1245.md) [Dāviḏ](../../strongs/h/h1732.md); and [Dāviḏ](../../strongs/h/h1732.md) [shama'](../../strongs/h/h8085.md) of it, and [yarad](../../strongs/h/h3381.md) to the [matsuwd](../../strongs/h/h4686.md).

<a name="2samuel_5_18"></a>2Samuel 5:18

The [Pᵊlištî](../../strongs/h/h6430.md) also [bow'](../../strongs/h/h935.md) and [nāṭaš](../../strongs/h/h5203.md) themselves in the [ʿēmeq](../../strongs/h/h6010.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="2samuel_5_19"></a>2Samuel 5:19

And [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Shall I [ʿālâ](../../strongs/h/h5927.md) to the [Pᵊlištî](../../strongs/h/h6430.md)? wilt thou [nathan](../../strongs/h/h5414.md) them into mine [yad](../../strongs/h/h3027.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [ʿālâ](../../strongs/h/h5927.md): for I will [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) the [Pᵊlištî](../../strongs/h/h6430.md) into thine [yad](../../strongs/h/h3027.md).

<a name="2samuel_5_20"></a>2Samuel 5:20

And [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to [BaʿAl-Pᵊrāṣîm](../../strongs/h/h1188.md), and [Dāviḏ](../../strongs/h/h1732.md) [nakah](../../strongs/h/h5221.md) them there, and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath broken [pāraṣ](../../strongs/h/h6555.md) upon mine ['oyeb](../../strongs/h/h341.md) [paniym](../../strongs/h/h6440.md) me, as the [pereṣ](../../strongs/h/h6556.md) of [mayim](../../strongs/h/h4325.md). Therefore he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [BaʿAl-Pᵊrāṣîm](../../strongs/h/h1188.md).

<a name="2samuel_5_21"></a>2Samuel 5:21

And there they ['azab](../../strongs/h/h5800.md) their [ʿāṣāḇ](../../strongs/h/h6091.md), and [Dāviḏ](../../strongs/h/h1732.md) and his ['enowsh](../../strongs/h/h582.md) [nasa'](../../strongs/h/h5375.md) them.

<a name="2samuel_5_22"></a>2Samuel 5:22

And the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) yet again, and [nāṭaš](../../strongs/h/h5203.md) themselves in the [ʿēmeq](../../strongs/h/h6010.md) of [rᵊp̄ā'îm](../../strongs/h/h7497.md).

<a name="2samuel_5_23"></a>2Samuel 5:23

And when [Dāviḏ](../../strongs/h/h1732.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), he ['āmar](../../strongs/h/h559.md), Thou shalt not [ʿālâ](../../strongs/h/h5927.md); but fetch a [cabab](../../strongs/h/h5437.md) ['aḥar](../../strongs/h/h310.md) them, and [bow'](../../strongs/h/h935.md) upon them over [môl](../../strongs/h/h4136.md) the [bāḵā'](../../strongs/h/h1057.md).

<a name="2samuel_5_24"></a>2Samuel 5:24

And let it be, when thou [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of a [ṣᵊʿāḏâ](../../strongs/h/h6807.md) in the [ro'sh](../../strongs/h/h7218.md) of the [bāḵā'](../../strongs/h/h1057.md), that then thou shalt [ḥāraṣ](../../strongs/h/h2782.md) thyself: for then shall [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) thee, to [nakah](../../strongs/h/h5221.md) the [maḥănê](../../strongs/h/h4264.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="2samuel_5_25"></a>2Samuel 5:25

And [Dāviḏ](../../strongs/h/h1732.md) ['asah](../../strongs/h/h6213.md), as [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) him; and [nakah](../../strongs/h/h5221.md) the [Pᵊlištî](../../strongs/h/h6430.md) from [Geḇaʿ](../../strongs/h/h1387.md) until thou [bow'](../../strongs/h/h935.md) to [Gezer](../../strongs/h/h1507.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 4](2samuel_4.md) - [2Samuel 6](2samuel_6.md)