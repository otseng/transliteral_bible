# [2Samuel 24](https://www.blueletterbible.org/kjv/2samuel/24)

<a name="2samuel_24_1"></a>2Samuel 24:1

And again the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he [sûṯ](../../strongs/h/h5496.md) [Dāviḏ](../../strongs/h/h1732.md) against them to ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [mānâ](../../strongs/h/h4487.md) [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md).

<a name="2samuel_24_2"></a>2Samuel 24:2

For the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to [Yô'āḇ](../../strongs/h/h3097.md) the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), which was with him, [šûṭ](../../strongs/h/h7751.md) all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), and [paqad](../../strongs/h/h6485.md) ye the ['am](../../strongs/h/h5971.md), that I may [yada'](../../strongs/h/h3045.md) the [mispār](../../strongs/h/h4557.md) of the ['am](../../strongs/h/h5971.md).

<a name="2samuel_24_3"></a>2Samuel 24:3

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), Now [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) add unto the ['am](../../strongs/h/h5971.md), how many soever they be, an hundredfold [pa'am](../../strongs/h/h6471.md), and that the ['ayin](../../strongs/h/h5869.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) may [ra'ah](../../strongs/h/h7200.md) it: but why doth my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [ḥāp̄ēṣ](../../strongs/h/h2654.md) in this [dabar](../../strongs/h/h1697.md)?

<a name="2samuel_24_4"></a>2Samuel 24:4

Notwithstanding the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [ḥāzaq](../../strongs/h/h2388.md) against [Yô'āḇ](../../strongs/h/h3097.md), and against the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md). And [Yô'āḇ](../../strongs/h/h3097.md) and the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) [yāṣā'](../../strongs/h/h3318.md) from the [paniym](../../strongs/h/h6440.md) of the [melek](../../strongs/h/h4428.md), to [paqad](../../strongs/h/h6485.md) the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_24_5"></a>2Samuel 24:5

And they ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), and [ḥānâ](../../strongs/h/h2583.md) in [ʿĂrôʿēr](../../strongs/h/h6177.md), on the [yamiyn](../../strongs/h/h3225.md) of the [ʿîr](../../strongs/h/h5892.md) in the midst of the [nachal](../../strongs/h/h5158.md) of [Gāḏ](../../strongs/h/h1410.md), and toward [Yaʿzêr](../../strongs/h/h3270.md):

<a name="2samuel_24_6"></a>2Samuel 24:6

Then they [bow'](../../strongs/h/h935.md) to [Gilʿāḏ](../../strongs/h/h1568.md), and to the ['erets](../../strongs/h/h776.md) of [Taḥtîm Ḥāḏšî](../../strongs/h/h8483.md) ; and they [bow'](../../strongs/h/h935.md) to [Dānâ YaʿAn](../../strongs/h/h1842.md) , and [cabiyb](../../strongs/h/h5439.md) to [Ṣîḏôn](../../strongs/h/h6721.md),

<a name="2samuel_24_7"></a>2Samuel 24:7

And [bow'](../../strongs/h/h935.md) to the [miḇṣār](../../strongs/h/h4013.md) of [Ṣōr](../../strongs/h/h6865.md), and to all the [ʿîr](../../strongs/h/h5892.md) of the [Ḥiûî](../../strongs/h/h2340.md), and of the [Kᵊnaʿănî](../../strongs/h/h3669.md): and they [yāṣā'](../../strongs/h/h3318.md) to the [neḡeḇ](../../strongs/h/h5045.md) of [Yehuwdah](../../strongs/h/h3063.md), even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="2samuel_24_8"></a>2Samuel 24:8

So when they had [šûṭ](../../strongs/h/h7751.md) through all the ['erets](../../strongs/h/h776.md), they [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) at the [qāṣê](../../strongs/h/h7097.md) of nine [ḥōḏeš](../../strongs/h/h2320.md) and twenty [yowm](../../strongs/h/h3117.md).

<a name="2samuel_24_9"></a>2Samuel 24:9

And [Yô'āḇ](../../strongs/h/h3097.md) gave [nathan](../../strongs/h/h5414.md) the [mispār](../../strongs/h/h4557.md) of the [mip̄qāḏ](../../strongs/h/h4662.md) of the ['am](../../strongs/h/h5971.md) unto the [melek](../../strongs/h/h4428.md): and there were in [Yisra'el](../../strongs/h/h3478.md) eight hundred thousand [ḥayil](../../strongs/h/h2428.md) ['iysh](../../strongs/h/h376.md) ['îš ḥayil](../../strongs/h/h381.md) that [šālap̄](../../strongs/h/h8025.md) the [chereb](../../strongs/h/h2719.md); and the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) were five hundred thousand ['iysh](../../strongs/h/h376.md).

<a name="2samuel_24_10"></a>2Samuel 24:10

And [Dāviḏ](../../strongs/h/h1732.md) [leb](../../strongs/h/h3820.md) [nakah](../../strongs/h/h5221.md) him ['aḥar](../../strongs/h/h310.md) that he had [sāp̄ar](../../strongs/h/h5608.md) the ['am](../../strongs/h/h5971.md). And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), I have [chata'](../../strongs/h/h2398.md) [me'od](../../strongs/h/h3966.md) in that I have ['asah](../../strongs/h/h6213.md): and now, I beseech thee, [Yĕhovah](../../strongs/h/h3068.md), ['abar](../../strongs/h/h5674.md) the ['avon](../../strongs/h/h5771.md) of thy ['ebed](../../strongs/h/h5650.md); for I have done [me'od](../../strongs/h/h3966.md) [sāḵal](../../strongs/h/h5528.md).

<a name="2samuel_24_11"></a>2Samuel 24:11

For when [Dāviḏ](../../strongs/h/h1732.md) was [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto the [nāḇî'](../../strongs/h/h5030.md) [Gāḏ](../../strongs/h/h1410.md), [Dāviḏ](../../strongs/h/h1732.md) [ḥōzê](../../strongs/h/h2374.md), ['āmar](../../strongs/h/h559.md),

<a name="2samuel_24_12"></a>2Samuel 24:12

[halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), Thus [dabar](../../strongs/h/h1696.md) [Yĕhovah](../../strongs/h/h3068.md), I [nāṭal](../../strongs/h/h5190.md) thee three things; [bāḥar](../../strongs/h/h977.md) thee one of them, that I may do it unto ['asah](../../strongs/h/h6213.md).

<a name="2samuel_24_13"></a>2Samuel 24:13

So [Gāḏ](../../strongs/h/h1410.md) [bow'](../../strongs/h/h935.md) to [Dāviḏ](../../strongs/h/h1732.md), and [nāḡaḏ](../../strongs/h/h5046.md) him, and ['āmar](../../strongs/h/h559.md) unto him, Shall seven [šānâ](../../strongs/h/h8141.md) of [rāʿāḇ](../../strongs/h/h7458.md) [bow'](../../strongs/h/h935.md) unto thee in thy ['erets](../../strongs/h/h776.md)? or wilt thou [nûs](../../strongs/h/h5127.md) three [ḥōḏeš](../../strongs/h/h2320.md) [paniym](../../strongs/h/h6440.md) thine [tsar](../../strongs/h/h6862.md), while they [radaph](../../strongs/h/h7291.md) thee? or that there be three [yowm](../../strongs/h/h3117.md) [deḇer](../../strongs/h/h1698.md) in thy ['erets](../../strongs/h/h776.md)? now [yada'](../../strongs/h/h3045.md), and [ra'ah](../../strongs/h/h7200.md) what [dabar](../../strongs/h/h1697.md) I shall [shuwb](../../strongs/h/h7725.md) to him that [shalach](../../strongs/h/h7971.md) me.

<a name="2samuel_24_14"></a>2Samuel 24:14

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Gāḏ](../../strongs/h/h1410.md), I am in a [me'od](../../strongs/h/h3966.md) [tsarar](../../strongs/h/h6887.md): let us [naphal](../../strongs/h/h5307.md) now into the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md); for his [raḥam](../../strongs/h/h7356.md) are [rab](../../strongs/h/h7227.md): and let me not [naphal](../../strongs/h/h5307.md) into the [yad](../../strongs/h/h3027.md) of ['āḏām](../../strongs/h/h120.md).

<a name="2samuel_24_15"></a>2Samuel 24:15

So [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) a [deḇer](../../strongs/h/h1698.md) upon [Yisra'el](../../strongs/h/h3478.md) from the [boqer](../../strongs/h/h1242.md) even to the [ʿēṯ](../../strongs/h/h6256.md) [môʿēḏ](../../strongs/h/h4150.md): and there [muwth](../../strongs/h/h4191.md) of the ['am](../../strongs/h/h5971.md) from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) seventy thousand ['iysh](../../strongs/h/h376.md).

<a name="2samuel_24_16"></a>2Samuel 24:16

And when the [mal'ak](../../strongs/h/h4397.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md) to [shachath](../../strongs/h/h7843.md) it, [Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) him of the [ra'](../../strongs/h/h7451.md), and ['āmar](../../strongs/h/h559.md) to the [mal'ak](../../strongs/h/h4397.md) that [shachath](../../strongs/h/h7843.md) the ['am](../../strongs/h/h5971.md), It is [rab](../../strongs/h/h7227.md): [rāp̄â](../../strongs/h/h7503.md) now thine [yad](../../strongs/h/h3027.md). And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) was by the [gōren](../../strongs/h/h1637.md) of ['Ăravnâ](../../strongs/h/h728.md) the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="2samuel_24_17"></a>2Samuel 24:17

And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md) when he [ra'ah](../../strongs/h/h7200.md) the [mal'ak](../../strongs/h/h4397.md) that [nakah](../../strongs/h/h5221.md) the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md), Lo, I have [chata'](../../strongs/h/h2398.md), and I have done [ʿāvâ](../../strongs/h/h5753.md): but these [tso'n](../../strongs/h/h6629.md), what have they ['asah](../../strongs/h/h6213.md)? let thine [yad](../../strongs/h/h3027.md), I pray thee, be against me, and against my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="2samuel_24_18"></a>2Samuel 24:18

And [Gāḏ](../../strongs/h/h1410.md) [bow'](../../strongs/h/h935.md) that [yowm](../../strongs/h/h3117.md) to [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto him, [ʿālâ](../../strongs/h/h5927.md), [quwm](../../strongs/h/h6965.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md) in the [gōren](../../strongs/h/h1637.md) of ['Ăravnâ](../../strongs/h/h728.md) the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="2samuel_24_19"></a>2Samuel 24:19

And [Dāviḏ](../../strongs/h/h1732.md), according to the [dabar](../../strongs/h/h1697.md) of [Gāḏ](../../strongs/h/h1410.md), [ʿālâ](../../strongs/h/h5927.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md).

<a name="2samuel_24_20"></a>2Samuel 24:20

And ['Ăravnâ](../../strongs/h/h728.md) [šāqap̄](../../strongs/h/h8259.md), and [ra'ah](../../strongs/h/h7200.md) the [melek](../../strongs/h/h4428.md) and his ['ebed](../../strongs/h/h5650.md) ['abar](../../strongs/h/h5674.md) toward him: and ['Ăravnâ](../../strongs/h/h728.md) [yāṣā'](../../strongs/h/h3318.md), and [shachah](../../strongs/h/h7812.md) himself before the [melek](../../strongs/h/h4428.md) on his ['aph](../../strongs/h/h639.md) upon the ['erets](../../strongs/h/h776.md).

<a name="2samuel_24_21"></a>2Samuel 24:21

And ['Ăravnâ](../../strongs/h/h728.md) ['āmar](../../strongs/h/h559.md), Wherefore is my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) to his ['ebed](../../strongs/h/h5650.md)? And [Dāviḏ](../../strongs/h/h1732.md) ['āmar](../../strongs/h/h559.md), To [qānâ](../../strongs/h/h7069.md) the [gōren](../../strongs/h/h1637.md) of thee, to [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md), that the [magēp̄â](../../strongs/h/h4046.md) may be [ʿāṣar](../../strongs/h/h6113.md) from the ['am](../../strongs/h/h5971.md).

<a name="2samuel_24_22"></a>2Samuel 24:22

And ['Ăravnâ](../../strongs/h/h728.md) ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), Let my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md) [laqach](../../strongs/h/h3947.md) and [ʿālâ](../../strongs/h/h5927.md) what seemeth [towb](../../strongs/h/h2896.md) unto ['ayin](../../strongs/h/h5869.md): [ra'ah](../../strongs/h/h7200.md), here be [bāqār](../../strongs/h/h1241.md) for [ʿōlâ](../../strongs/h/h5930.md), and [môraḡ](../../strongs/h/h4173.md) and other [kĕliy](../../strongs/h/h3627.md) of the [bāqār](../../strongs/h/h1241.md) for ['ets](../../strongs/h/h6086.md).

<a name="2samuel_24_23"></a>2Samuel 24:23

All these things did ['Ăravnâ](../../strongs/h/h728.md), as a [melek](../../strongs/h/h4428.md), [nathan](../../strongs/h/h5414.md) unto the [melek](../../strongs/h/h4428.md). And ['Ăravnâ](../../strongs/h/h728.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) [ratsah](../../strongs/h/h7521.md) thee.

<a name="2samuel_24_24"></a>2Samuel 24:24

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto ['Ăravnâ](../../strongs/h/h728.md), Nay; but I will [qānâ](../../strongs/h/h7069.md) [qānâ](../../strongs/h/h7069.md) it of thee at a [mᵊḥîr](../../strongs/h/h4242.md): neither will I [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) of that which doth cost me [ḥinnām](../../strongs/h/h2600.md). So [Dāviḏ](../../strongs/h/h1732.md) [qānâ](../../strongs/h/h7069.md) the [gōren](../../strongs/h/h1637.md) and the [bāqār](../../strongs/h/h1241.md) for fifty [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md).

<a name="2samuel_24_25"></a>2Samuel 24:25

And [Dāviḏ](../../strongs/h/h1732.md) [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md). So [Yĕhovah](../../strongs/h/h3068.md) was [ʿāṯar](../../strongs/h/h6279.md) for the ['erets](../../strongs/h/h776.md), and the [magēp̄â](../../strongs/h/h4046.md) was [ʿāṣar](../../strongs/h/h6113.md) from [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 23](2samuel_23.md)