# [2Samuel 18](https://www.blueletterbible.org/kjv/2samuel/18)

<a name="2samuel_18_1"></a>2Samuel 18:1

And [Dāviḏ](../../strongs/h/h1732.md) [paqad](../../strongs/h/h6485.md) the ['am](../../strongs/h/h5971.md) that were with him, and [śûm](../../strongs/h/h7760.md) [śar](../../strongs/h/h8269.md) of thousands and [śar](../../strongs/h/h8269.md) of hundreds over them.

<a name="2samuel_18_2"></a>2Samuel 18:2

And [Dāviḏ](../../strongs/h/h1732.md) [shalach](../../strongs/h/h7971.md) a third part of the ['am](../../strongs/h/h5971.md) under the [yad](../../strongs/h/h3027.md) of [Yô'āḇ](../../strongs/h/h3097.md), and a third part under the [yad](../../strongs/h/h3027.md) of ['Ăḇîšay](../../strongs/h/h52.md) the [ben](../../strongs/h/h1121.md) of [Ṣᵊrûyâ](../../strongs/h/h6870.md), [Yô'āḇ](../../strongs/h/h3097.md) ['ach](../../strongs/h/h251.md), and a third part under the [yad](../../strongs/h/h3027.md) of ['Itay](../../strongs/h/h863.md) the [Gitî](../../strongs/h/h1663.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md), I will [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) with you myself also.

<a name="2samuel_18_3"></a>2Samuel 18:3

But the ['am](../../strongs/h/h5971.md) ['āmar](../../strongs/h/h559.md), Thou shalt not [yāṣā'](../../strongs/h/h3318.md): for if we [nûs](../../strongs/h/h5127.md) [nûs](../../strongs/h/h5127.md), they will not [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) for us; neither if half of us [muwth](../../strongs/h/h4191.md), will they [śûm](../../strongs/h/h7760.md) [leb](../../strongs/h/h3820.md) for us: but now thou art worth ten thousand of us: therefore now it is [towb](../../strongs/h/h2896.md) that thou [ʿāzar](../../strongs/h/h5826.md) [ʿāzar](../../strongs/h/h5826.md) us out of the [ʿîr](../../strongs/h/h5892.md).

<a name="2samuel_18_4"></a>2Samuel 18:4

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto them, What ['ayin](../../strongs/h/h5869.md) you [yatab](../../strongs/h/h3190.md) I will ['asah](../../strongs/h/h6213.md). And the [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) by the [sha'ar](../../strongs/h/h8179.md) [yad](../../strongs/h/h3027.md), and all the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) by hundreds and by thousands.

<a name="2samuel_18_5"></a>2Samuel 18:5

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Yô'āḇ](../../strongs/h/h3097.md) and ['Ăḇîšay](../../strongs/h/h52.md) and ['Itay](../../strongs/h/h863.md), ['āmar](../../strongs/h/h559.md), ['aṭ](../../strongs/h/h328.md) for my sake with the [naʿar](../../strongs/h/h5288.md), even with ['Ăbyšālôm](../../strongs/h/h53.md). And all the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) when the [melek](../../strongs/h/h4428.md) gave all the [śar](../../strongs/h/h8269.md) [tsavah](../../strongs/h/h6680.md) [dabar](../../strongs/h/h1697.md) ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_18_6"></a>2Samuel 18:6

So the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md) [qārā'](../../strongs/h/h7125.md) [Yisra'el](../../strongs/h/h3478.md): and the [milḥāmâ](../../strongs/h/h4421.md) was in the [yaʿar](../../strongs/h/h3293.md) of ['Ep̄rayim](../../strongs/h/h669.md);

<a name="2samuel_18_7"></a>2Samuel 18:7

Where the ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md) were [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) the ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md), and there was there a [gadowl](../../strongs/h/h1419.md) [magēp̄â](../../strongs/h/h4046.md) that [yowm](../../strongs/h/h3117.md) of twenty thousand men.

<a name="2samuel_18_8"></a>2Samuel 18:8

For the [milḥāmâ](../../strongs/h/h4421.md) was there [puwts](../../strongs/h/h6327.md) over the [paniym](../../strongs/h/h6440.md) of all the ['erets](../../strongs/h/h776.md): and the [yaʿar](../../strongs/h/h3293.md) ['akal](../../strongs/h/h398.md) [rabah](../../strongs/h/h7235.md) ['am](../../strongs/h/h5971.md) that [yowm](../../strongs/h/h3117.md) than the [chereb](../../strongs/h/h2719.md) ['akal](../../strongs/h/h398.md).

<a name="2samuel_18_9"></a>2Samuel 18:9

And ['Ăbyšālôm](../../strongs/h/h53.md) [qārā'](../../strongs/h/h7122.md) the [paniym](../../strongs/h/h6440.md) ['ebed](../../strongs/h/h5650.md) of [Dāviḏ](../../strongs/h/h1732.md). And ['Ăbyšālôm](../../strongs/h/h53.md) [rāḵaḇ](../../strongs/h/h7392.md) upon a [pereḏ](../../strongs/h/h6505.md), and the [pereḏ](../../strongs/h/h6505.md) [bow'](../../strongs/h/h935.md) under the [śôḇeḵ](../../strongs/h/h7730.md) of a [gadowl](../../strongs/h/h1419.md) ['ēlâ](../../strongs/h/h424.md), and his [ro'sh](../../strongs/h/h7218.md) [ḥāzaq](../../strongs/h/h2388.md) of the ['ēlâ](../../strongs/h/h424.md), and he was [nathan](../../strongs/h/h5414.md) between the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md); and the [pereḏ](../../strongs/h/h6505.md) that was under him ['abar](../../strongs/h/h5674.md).

<a name="2samuel_18_10"></a>2Samuel 18:10

And a certain ['iysh](../../strongs/h/h376.md) [ra'ah](../../strongs/h/h7200.md) it, and [nāḡaḏ](../../strongs/h/h5046.md) [Yô'āḇ](../../strongs/h/h3097.md), and ['āmar](../../strongs/h/h559.md), Behold, I [ra'ah](../../strongs/h/h7200.md) ['Ăbyšālôm](../../strongs/h/h53.md) [tālâ](../../strongs/h/h8518.md) in an ['ēlâ](../../strongs/h/h424.md).

<a name="2samuel_18_11"></a>2Samuel 18:11

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md) unto the ['iysh](../../strongs/h/h376.md) that [nāḡaḏ](../../strongs/h/h5046.md) him, And, behold, thou [ra'ah](../../strongs/h/h7200.md) him, and why didst thou not [nakah](../../strongs/h/h5221.md) him there to the ['erets](../../strongs/h/h776.md)? and I would have [nathan](../../strongs/h/h5414.md) thee ten shekels of [keceph](../../strongs/h/h3701.md), and a [chagowr](../../strongs/h/h2290.md).

<a name="2samuel_18_12"></a>2Samuel 18:12

And the ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md) unto [Yô'āḇ](../../strongs/h/h3097.md), Though I should [šāqal](../../strongs/h/h8254.md) a thousand shekels of [keceph](../../strongs/h/h3701.md) in mine [kaph](../../strongs/h/h3709.md), yet would I not [shalach](../../strongs/h/h7971.md) mine [yad](../../strongs/h/h3027.md) against the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md): for in our ['ozen](../../strongs/h/h241.md) the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) thee and ['Ăḇîšay](../../strongs/h/h52.md) and ['Itay](../../strongs/h/h863.md), ['āmar](../../strongs/h/h559.md), [shamar](../../strongs/h/h8104.md) that none the [naʿar](../../strongs/h/h5288.md) ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_18_13"></a>2Samuel 18:13

Otherwise I should have ['asah](../../strongs/h/h6213.md) [sheqer](../../strongs/h/h8267.md) against mine own [nephesh](../../strongs/h/h5315.md): for there is no [dabar](../../strongs/h/h1697.md) [kāḥaḏ](../../strongs/h/h3582.md) from the [melek](../../strongs/h/h4428.md), and thou thyself wouldest have [yatsab](../../strongs/h/h3320.md) thyself against me.

<a name="2samuel_18_14"></a>2Samuel 18:14

Then ['āmar](../../strongs/h/h559.md) [Yô'āḇ](../../strongs/h/h3097.md), I may not [yāḥal](../../strongs/h/h3176.md) thus with [paniym](../../strongs/h/h6440.md). And he [laqach](../../strongs/h/h3947.md) three [shebet](../../strongs/h/h7626.md) in his [kaph](../../strongs/h/h3709.md), and [tāqaʿ](../../strongs/h/h8628.md) them through the [leb](../../strongs/h/h3820.md) of ['Ăbyšālôm](../../strongs/h/h53.md), while he was yet [chay](../../strongs/h/h2416.md) in the midst of the ['ēlâ](../../strongs/h/h424.md).

<a name="2samuel_18_15"></a>2Samuel 18:15

And ten [naʿar](../../strongs/h/h5288.md) that [nasa'](../../strongs/h/h5375.md) [Yô'āḇ](../../strongs/h/h3097.md) [kĕliy](../../strongs/h/h3627.md) [cabab](../../strongs/h/h5437.md) and [nakah](../../strongs/h/h5221.md) ['Ăbyšālôm](../../strongs/h/h53.md), and [muwth](../../strongs/h/h4191.md) him.

<a name="2samuel_18_16"></a>2Samuel 18:16

And [Yô'āḇ](../../strongs/h/h3097.md) [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) from [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Yisra'el](../../strongs/h/h3478.md): for [Yô'āḇ](../../strongs/h/h3097.md) [ḥāśaḵ](../../strongs/h/h2820.md) the ['am](../../strongs/h/h5971.md).

<a name="2samuel_18_17"></a>2Samuel 18:17

And they [laqach](../../strongs/h/h3947.md) ['Ăbyšālôm](../../strongs/h/h53.md), and [shalak](../../strongs/h/h7993.md) him into a [gadowl](../../strongs/h/h1419.md) [paḥaṯ](../../strongs/h/h6354.md) in the [yaʿar](../../strongs/h/h3293.md), and [nāṣaḇ](../../strongs/h/h5324.md) a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [gal](../../strongs/h/h1530.md) of ['eben](../../strongs/h/h68.md) upon him: and all [Yisra'el](../../strongs/h/h3478.md) [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md).

<a name="2samuel_18_18"></a>2Samuel 18:18

Now ['Ăbyšālôm](../../strongs/h/h53.md) in his [chay](../../strongs/h/h2416.md) had [laqach](../../strongs/h/h3947.md) and [nāṣaḇ](../../strongs/h/h5324.md) for himself a [maṣṣeḇeṯ](../../strongs/h/h4678.md), which is in the [melek](../../strongs/h/h4428.md) [ʿēmeq](../../strongs/h/h6010.md): for he ['āmar](../../strongs/h/h559.md), I have no [ben](../../strongs/h/h1121.md) to keep my [shem](../../strongs/h/h8034.md) in [zakar](../../strongs/h/h2142.md): and he [qara'](../../strongs/h/h7121.md) the [maṣṣeḇeṯ](../../strongs/h/h4678.md) after his own [shem](../../strongs/h/h8034.md): and it is [qara'](../../strongs/h/h7121.md) unto this [yowm](../../strongs/h/h3117.md), ['Ăbyšālôm](../../strongs/h/h53.md) [yad](../../strongs/h/h3027.md).

<a name="2samuel_18_19"></a>2Samuel 18:19

Then ['āmar](../../strongs/h/h559.md) ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md), Let me now [rûṣ](../../strongs/h/h7323.md), and [bāśar](../../strongs/h/h1319.md) the [melek](../../strongs/h/h4428.md) [bāśar](../../strongs/h/h1319.md), how that [Yĕhovah](../../strongs/h/h3068.md) hath [shaphat](../../strongs/h/h8199.md) him of his [yad](../../strongs/h/h3027.md) ['oyeb](../../strongs/h/h341.md).

<a name="2samuel_18_20"></a>2Samuel 18:20

And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md) unto him, ['iysh](../../strongs/h/h376.md) shalt not bear [bᵊśôrâ](../../strongs/h/h1309.md) this [yowm](../../strongs/h/h3117.md), but thou shalt [bāśar](../../strongs/h/h1319.md) ['aḥēr](../../strongs/h/h312.md) [yowm](../../strongs/h/h3117.md): but this [yowm](../../strongs/h/h3117.md) thou shalt bear no [bāśar](../../strongs/h/h1319.md), because the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) is [muwth](../../strongs/h/h4191.md).

<a name="2samuel_18_21"></a>2Samuel 18:21

Then ['āmar](../../strongs/h/h559.md) [Yô'āḇ](../../strongs/h/h3097.md) to [Kûšî](../../strongs/h/h3569.md), [yālaḵ](../../strongs/h/h3212.md) [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) what thou hast [ra'ah](../../strongs/h/h7200.md). And [Kûšî](../../strongs/h/h3569.md) [shachah](../../strongs/h/h7812.md) himself unto [Yô'āḇ](../../strongs/h/h3097.md), and [rûṣ](../../strongs/h/h7323.md).

<a name="2samuel_18_22"></a>2Samuel 18:22

Then ['āmar](../../strongs/h/h559.md) ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md) yet again to [Yô'āḇ](../../strongs/h/h3097.md), But howsoever, let me, I pray thee, also [rûṣ](../../strongs/h/h7323.md) ['aḥar](../../strongs/h/h310.md) [Kûšî](../../strongs/h/h3569.md). And [Yô'āḇ](../../strongs/h/h3097.md) ['āmar](../../strongs/h/h559.md), Wherefore wilt thou [rûṣ](../../strongs/h/h7323.md), my [ben](../../strongs/h/h1121.md), seeing that thou hast no [bᵊśôrâ](../../strongs/h/h1309.md) [māṣā'](../../strongs/h/h4672.md)?

<a name="2samuel_18_23"></a>2Samuel 18:23

But howsoever, said he, let me [rûṣ](../../strongs/h/h7323.md). And he ['āmar](../../strongs/h/h559.md) unto him, [rûṣ](../../strongs/h/h7323.md). Then ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) [rûṣ](../../strongs/h/h7323.md) by the [derek](../../strongs/h/h1870.md) of the [kikār](../../strongs/h/h3603.md), and ['abar](../../strongs/h/h5674.md) [Kûšî](../../strongs/h/h3569.md).

<a name="2samuel_18_24"></a>2Samuel 18:24

And [Dāviḏ](../../strongs/h/h1732.md) [yashab](../../strongs/h/h3427.md) between the two [sha'ar](../../strongs/h/h8179.md): and the [tsaphah](../../strongs/h/h6822.md) [yālaḵ](../../strongs/h/h3212.md) to the [gāḡ](../../strongs/h/h1406.md) over the [sha'ar](../../strongs/h/h8179.md) unto the [ḥômâ](../../strongs/h/h2346.md), and [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and behold an ['iysh](../../strongs/h/h376.md) [rûṣ](../../strongs/h/h7323.md) alone.

<a name="2samuel_18_25"></a>2Samuel 18:25

And the [tsaphah](../../strongs/h/h6822.md) [qara'](../../strongs/h/h7121.md), and [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), If he be alone, there is [bᵊśôrâ](../../strongs/h/h1309.md) in his [peh](../../strongs/h/h6310.md). And he [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md), and [qārēḇ](../../strongs/h/h7131.md).

<a name="2samuel_18_26"></a>2Samuel 18:26

And the [tsaphah](../../strongs/h/h6822.md) [ra'ah](../../strongs/h/h7200.md) ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md) [rûṣ](../../strongs/h/h7323.md): and the [tsaphah](../../strongs/h/h6822.md) [qara'](../../strongs/h/h7121.md) unto the [šôʿēr](../../strongs/h/h7778.md), and ['āmar](../../strongs/h/h559.md), Behold another ['iysh](../../strongs/h/h376.md) [rûṣ](../../strongs/h/h7323.md) alone. And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), He also bringeth [bāśar](../../strongs/h/h1319.md).

<a name="2samuel_18_27"></a>2Samuel 18:27

And the [tsaphah](../../strongs/h/h6822.md) ['āmar](../../strongs/h/h559.md), Me [ra'ah](../../strongs/h/h7200.md) the [mᵊrûṣâ](../../strongs/h/h4794.md) of the [ri'šôn](../../strongs/h/h7223.md) is like the [mᵊrûṣâ](../../strongs/h/h4794.md) of ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) the [ben](../../strongs/h/h1121.md) of [Ṣāḏôq](../../strongs/h/h6659.md). And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), He is a [towb](../../strongs/h/h2896.md) ['iysh](../../strongs/h/h376.md), and [bow'](../../strongs/h/h935.md) with [towb](../../strongs/h/h2896.md) [bᵊśôrâ](../../strongs/h/h1309.md).

<a name="2samuel_18_28"></a>2Samuel 18:28

And ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) [qara'](../../strongs/h/h7121.md), and ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), All is [shalowm](../../strongs/h/h7965.md). And he [shachah](../../strongs/h/h7812.md) to the ['erets](../../strongs/h/h776.md) upon his ['aph](../../strongs/h/h639.md) before the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which hath delivered [cagar](../../strongs/h/h5462.md) the ['enowsh](../../strongs/h/h582.md) that [nasa'](../../strongs/h/h5375.md) their [yad](../../strongs/h/h3027.md) against my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md).

<a name="2samuel_18_29"></a>2Samuel 18:29

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Is the [naʿar](../../strongs/h/h5288.md) ['Ăbyšālôm](../../strongs/h/h53.md) [shalowm](../../strongs/h/h7965.md)? And ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) ['āmar](../../strongs/h/h559.md), When [Yô'āḇ](../../strongs/h/h3097.md) [shalach](../../strongs/h/h7971.md) the [melek](../../strongs/h/h4428.md) ['ebed](../../strongs/h/h5650.md), and me thy ['ebed](../../strongs/h/h5650.md), I [ra'ah](../../strongs/h/h7200.md) a [gadowl](../../strongs/h/h1419.md) [hāmôn](../../strongs/h/h1995.md), but I [yada'](../../strongs/h/h3045.md) not what it was.

<a name="2samuel_18_30"></a>2Samuel 18:30

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, Turn [cabab](../../strongs/h/h5437.md), and [yatsab](../../strongs/h/h3320.md) here. And he [cabab](../../strongs/h/h5437.md), and ['amad](../../strongs/h/h5975.md).

<a name="2samuel_18_31"></a>2Samuel 18:31

And, behold, [Kûšî](../../strongs/h/h3569.md) [bow'](../../strongs/h/h935.md); and [Kûšî](../../strongs/h/h3569.md) ['āmar](../../strongs/h/h559.md), [bāśar](../../strongs/h/h1319.md), my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [shaphat](../../strongs/h/h8199.md) thee this [yowm](../../strongs/h/h3117.md) of all [yad](../../strongs/h/h3027.md) that [quwm](../../strongs/h/h6965.md) against thee.

<a name="2samuel_18_32"></a>2Samuel 18:32

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Kûšî](../../strongs/h/h3569.md), Is the [naʿar](../../strongs/h/h5288.md) ['Ăbyšālôm](../../strongs/h/h53.md) [shalowm](../../strongs/h/h7965.md)? And [Kûšî](../../strongs/h/h3569.md) ['āmar](../../strongs/h/h559.md), The ['oyeb](../../strongs/h/h341.md) of my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), and all that [quwm](../../strongs/h/h6965.md) against thee to do thee [ra'](../../strongs/h/h7451.md), be as that [naʿar](../../strongs/h/h5288.md) is.

<a name="2samuel_18_33"></a>2Samuel 18:33

And the [melek](../../strongs/h/h4428.md) was much [ragaz](../../strongs/h/h7264.md), and [ʿālâ](../../strongs/h/h5927.md) to the [ʿălîyâ](../../strongs/h/h5944.md) over the [sha'ar](../../strongs/h/h8179.md), and [bāḵâ](../../strongs/h/h1058.md): and as he [yālaḵ](../../strongs/h/h3212.md), thus he ['āmar](../../strongs/h/h559.md), O my [ben](../../strongs/h/h1121.md) ['Ăbyšālôm](../../strongs/h/h53.md), my [ben](../../strongs/h/h1121.md), my [ben](../../strongs/h/h1121.md) ['Ăbyšālôm](../../strongs/h/h53.md)! would I had [muwth](../../strongs/h/h4191.md) for [nathan](../../strongs/h/h5414.md), O ['Ăbyšālôm](../../strongs/h/h53.md), my [ben](../../strongs/h/h1121.md), my [ben](../../strongs/h/h1121.md)!

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 17](2samuel_17.md) - [2Samuel 19](2samuel_19.md)