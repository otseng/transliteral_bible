# [2Samuel 17](https://www.blueletterbible.org/kjv/2samuel/17)

<a name="2samuel_17_1"></a>2Samuel 17:1

Moreover ['Ăḥîṯōp̄El](../../strongs/h/h302.md) ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), Let me now [bāḥar](../../strongs/h/h977.md) twelve thousand ['iysh](../../strongs/h/h376.md), and I will [quwm](../../strongs/h/h6965.md) and [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Dāviḏ](../../strongs/h/h1732.md) this [layil](../../strongs/h/h3915.md):

<a name="2samuel_17_2"></a>2Samuel 17:2

And I will [bow'](../../strongs/h/h935.md) upon him while he is [yāḡēaʿ](../../strongs/h/h3023.md) and [rāp̄ê](../../strongs/h/h7504.md) [yad](../../strongs/h/h3027.md), and will make him [ḥārēḏ](../../strongs/h/h2729.md): and all the ['am](../../strongs/h/h5971.md) that are with him shall [nûs](../../strongs/h/h5127.md); and I will [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) only:

<a name="2samuel_17_3"></a>2Samuel 17:3

And I will [shuwb](../../strongs/h/h7725.md) all the ['am](../../strongs/h/h5971.md) unto thee: the ['iysh](../../strongs/h/h376.md) whom thou [bāqaš](../../strongs/h/h1245.md) is as if all [shuwb](../../strongs/h/h7725.md): so all the ['am](../../strongs/h/h5971.md) shall be in [shalowm](../../strongs/h/h7965.md).

<a name="2samuel_17_4"></a>2Samuel 17:4

And the [dabar](../../strongs/h/h1697.md) [yashar](../../strongs/h/h3474.md) ['Ăbyšālôm](../../strongs/h/h53.md) ['ayin](../../strongs/h/h5869.md), and all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2samuel_17_5"></a>2Samuel 17:5

Then ['āmar](../../strongs/h/h559.md) ['Ăbyšālôm](../../strongs/h/h53.md), [qara'](../../strongs/h/h7121.md) now [Ḥûšay](../../strongs/h/h2365.md) the ['arkî](../../strongs/h/h757.md) also, and let us [shama'](../../strongs/h/h8085.md) likewise what he [peh](../../strongs/h/h6310.md).

<a name="2samuel_17_6"></a>2Samuel 17:6

And when [Ḥûšay](../../strongs/h/h2365.md) was [bow'](../../strongs/h/h935.md) to ['Ăbyšālôm](../../strongs/h/h53.md), ['Ăbyšālôm](../../strongs/h/h53.md) ['āmar](../../strongs/h/h559.md) unto him, ['āmar](../../strongs/h/h559.md), ['Ăḥîṯōp̄El](../../strongs/h/h302.md) hath [dabar](../../strongs/h/h1696.md) after this [dabar](../../strongs/h/h1697.md): shall we ['asah](../../strongs/h/h6213.md) after his [dabar](../../strongs/h/h1697.md)? if not; [dabar](../../strongs/h/h1696.md) thou.

<a name="2samuel_17_7"></a>2Samuel 17:7

And [Ḥûšay](../../strongs/h/h2365.md) ['āmar](../../strongs/h/h559.md) unto ['Ăbyšālôm](../../strongs/h/h53.md), The ['etsah](../../strongs/h/h6098.md) that ['Ăḥîṯōp̄El](../../strongs/h/h302.md) hath [ya'ats](../../strongs/h/h3289.md) is not [towb](../../strongs/h/h2896.md) at this time.

<a name="2samuel_17_8"></a>2Samuel 17:8

For, ['āmar](../../strongs/h/h559.md) [Ḥûšay](../../strongs/h/h2365.md), thou [yada'](../../strongs/h/h3045.md) thy ['ab](../../strongs/h/h1.md) and his ['enowsh](../../strongs/h/h582.md), that they be [gibôr](../../strongs/h/h1368.md), and they be [mar](../../strongs/h/h4751.md) in their [nephesh](../../strongs/h/h5315.md), as a [dōḇ](../../strongs/h/h1677.md) [šakûl](../../strongs/h/h7909.md) of her [sadeh](../../strongs/h/h7704.md): and thy ['ab](../../strongs/h/h1.md) is an ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md), and will not [lûn](../../strongs/h/h3885.md) with the ['am](../../strongs/h/h5971.md).

<a name="2samuel_17_9"></a>2Samuel 17:9

Behold, he is [chaba'](../../strongs/h/h2244.md) now in some [paḥaṯ](../../strongs/h/h6354.md), or in some other [maqowm](../../strongs/h/h4725.md): and it will come to pass, when some of them be [naphal](../../strongs/h/h5307.md) at the [tᵊḥillâ](../../strongs/h/h8462.md), that [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) it will ['āmar](../../strongs/h/h559.md), There is a [magēp̄â](../../strongs/h/h4046.md) among the ['am](../../strongs/h/h5971.md) that ['aḥar](../../strongs/h/h310.md) ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_17_10"></a>2Samuel 17:10

And he also that is [ben](../../strongs/h/h1121.md) [ḥayil](../../strongs/h/h2428.md), whose [leb](../../strongs/h/h3820.md) is as the [leb](../../strongs/h/h3820.md) of an ['ariy](../../strongs/h/h738.md), shall [māsas](../../strongs/h/h4549.md) [māsas](../../strongs/h/h4549.md): for all [Yisra'el](../../strongs/h/h3478.md) [yada'](../../strongs/h/h3045.md) that thy ['ab](../../strongs/h/h1.md) is a [gibôr](../../strongs/h/h1368.md), and they which be with him are [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md).

<a name="2samuel_17_11"></a>2Samuel 17:11

Therefore I [ya'ats](../../strongs/h/h3289.md) that all [Yisra'el](../../strongs/h/h3478.md) be ['āsap̄](../../strongs/h/h622.md) ['āsap̄](../../strongs/h/h622.md) unto thee, from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), as the [ḥôl](../../strongs/h/h2344.md) that is by the [yam](../../strongs/h/h3220.md) for [rōḇ](../../strongs/h/h7230.md); and that thou [halak](../../strongs/h/h1980.md) to [qᵊrāḇ](../../strongs/h/h7128.md) in thine own [paniym](../../strongs/h/h6440.md).

<a name="2samuel_17_12"></a>2Samuel 17:12

So shall we [bow'](../../strongs/h/h935.md) upon him in some [maqowm](../../strongs/h/h4725.md) where he shall be [māṣā'](../../strongs/h/h4672.md), and we will light upon him as the [ṭal](../../strongs/h/h2919.md) [naphal](../../strongs/h/h5307.md) on the ['ăḏāmâ](../../strongs/h/h127.md): and of him and of all the ['enowsh](../../strongs/h/h582.md) that are with him there shall not be [yāṯar](../../strongs/h/h3498.md) so much as one.

<a name="2samuel_17_13"></a>2Samuel 17:13

Moreover, if he be ['āsap̄](../../strongs/h/h622.md) into a [ʿîr](../../strongs/h/h5892.md), then shall all [Yisra'el](../../strongs/h/h3478.md) [nasa'](../../strongs/h/h5375.md) [chebel](../../strongs/h/h2256.md) to that [ʿîr](../../strongs/h/h5892.md), and we will [sāḥaḇ](../../strongs/h/h5498.md) it into the [nachal](../../strongs/h/h5158.md), until there be not one small [ṣᵊrôr](../../strongs/h/h6872.md) [māṣā'](../../strongs/h/h4672.md) there.

<a name="2samuel_17_14"></a>2Samuel 17:14

And ['Ăbyšālôm](../../strongs/h/h53.md) and all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), The ['etsah](../../strongs/h/h6098.md) of [Ḥûšay](../../strongs/h/h2365.md) the ['arkî](../../strongs/h/h757.md) is [towb](../../strongs/h/h2896.md) than the ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md). For [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) to [pārar](../../strongs/h/h6565.md) the [towb](../../strongs/h/h2896.md) ['etsah](../../strongs/h/h6098.md) of ['Ăḥîṯōp̄El](../../strongs/h/h302.md), to the intent that [Yĕhovah](../../strongs/h/h3068.md) might [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon ['Ăbyšālôm](../../strongs/h/h53.md).

<a name="2samuel_17_15"></a>2Samuel 17:15

Then ['āmar](../../strongs/h/h559.md) [Ḥûšay](../../strongs/h/h2365.md) unto [Ṣāḏôq](../../strongs/h/h6659.md) and to ['Eḇyāṯār](../../strongs/h/h54.md) the [kōhēn](../../strongs/h/h3548.md), Thus and thus did ['Ăḥîṯōp̄El](../../strongs/h/h302.md) [ya'ats](../../strongs/h/h3289.md) ['Ăbyšālôm](../../strongs/h/h53.md) and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md); and thus and thus have I [ya'ats](../../strongs/h/h3289.md).

<a name="2samuel_17_16"></a>2Samuel 17:16

Now therefore [shalach](../../strongs/h/h7971.md) [mᵊhērâ](../../strongs/h/h4120.md), and [nāḡaḏ](../../strongs/h/h5046.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md), [lûn](../../strongs/h/h3885.md) not this [layil](../../strongs/h/h3915.md) in the ['arabah](../../strongs/h/h6160.md) [ʿăḇārâ](../../strongs/h/h5679.md) of the [midbar](../../strongs/h/h4057.md), but ['abar](../../strongs/h/h5674.md) ['abar](../../strongs/h/h5674.md); lest the [melek](../../strongs/h/h4428.md) be [bālaʿ](../../strongs/h/h1104.md), and all the ['am](../../strongs/h/h5971.md) that are with him.

<a name="2samuel_17_17"></a>2Samuel 17:17

Now [Yᵊhônāṯān](../../strongs/h/h3083.md) and ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) ['amad](../../strongs/h/h5975.md) by [ʿÊn Rōḡēl](../../strongs/h/h5883.md); for they [yakol](../../strongs/h/h3201.md) not be [ra'ah](../../strongs/h/h7200.md) to [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md): and a [šip̄ḥâ](../../strongs/h/h8198.md) [halak](../../strongs/h/h1980.md) and [nāḡaḏ](../../strongs/h/h5046.md) them; and they [yālaḵ](../../strongs/h/h3212.md) and [nāḡaḏ](../../strongs/h/h5046.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2samuel_17_18"></a>2Samuel 17:18

Nevertheless a [naʿar](../../strongs/h/h5288.md) [ra'ah](../../strongs/h/h7200.md) them, and [nāḡaḏ](../../strongs/h/h5046.md) ['Ăbyšālôm](../../strongs/h/h53.md): but they [yālaḵ](../../strongs/h/h3212.md) both of them away [mᵊhērâ](../../strongs/h/h4120.md), and [bow'](../../strongs/h/h935.md) to an ['iysh](../../strongs/h/h376.md) [bayith](../../strongs/h/h1004.md) in [Baḥurîm](../../strongs/h/h980.md), which had a [bᵊ'ēr](../../strongs/h/h875.md) in his [ḥāṣēr](../../strongs/h/h2691.md); whither they [yarad](../../strongs/h/h3381.md).

<a name="2samuel_17_19"></a>2Samuel 17:19

And the ['ishshah](../../strongs/h/h802.md) [laqach](../../strongs/h/h3947.md) and [pāraś](../../strongs/h/h6566.md) a [māsāḵ](../../strongs/h/h4539.md) over the [bᵊ'ēr](../../strongs/h/h875.md) [paniym](../../strongs/h/h6440.md), and [šāṭaḥ](../../strongs/h/h7849.md) [rîp̄ôṯ](../../strongs/h/h7383.md) thereon; and the [dabar](../../strongs/h/h1697.md) was not [yada'](../../strongs/h/h3045.md).

<a name="2samuel_17_20"></a>2Samuel 17:20

And when ['Ăbyšālôm](../../strongs/h/h53.md) ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md) to the ['ishshah](../../strongs/h/h802.md) to the [bayith](../../strongs/h/h1004.md), they ['āmar](../../strongs/h/h559.md), Where is ['ĂḥîmaʿAṣ](../../strongs/h/h290.md) and [Yᵊhônāṯān](../../strongs/h/h3083.md)? And the ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto them, They be ['abar](../../strongs/h/h5674.md) the [mîḵāl](../../strongs/h/h4323.md) of [mayim](../../strongs/h/h4325.md). And when they had [bāqaš](../../strongs/h/h1245.md) and could not [māṣā'](../../strongs/h/h4672.md) them, they [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2samuel_17_21"></a>2Samuel 17:21

And it came to pass, ['aḥar](../../strongs/h/h310.md) they were [yālaḵ](../../strongs/h/h3212.md), that they [ʿālâ](../../strongs/h/h5927.md) out of the [bᵊ'ēr](../../strongs/h/h875.md), and [yālaḵ](../../strongs/h/h3212.md) and [nāḡaḏ](../../strongs/h/h5046.md) [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), and ['āmar](../../strongs/h/h559.md) unto [Dāviḏ](../../strongs/h/h1732.md), [quwm](../../strongs/h/h6965.md), and pass [mᵊhērâ](../../strongs/h/h4120.md) ['abar](../../strongs/h/h5674.md) the [mayim](../../strongs/h/h4325.md): for thus hath ['Ăḥîṯōp̄El](../../strongs/h/h302.md) [ya'ats](../../strongs/h/h3289.md) against you.

<a name="2samuel_17_22"></a>2Samuel 17:22

Then [Dāviḏ](../../strongs/h/h1732.md) [quwm](../../strongs/h/h6965.md), and all the ['am](../../strongs/h/h5971.md) that were with him, and they ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md): by the [boqer](../../strongs/h/h1242.md) ['owr](../../strongs/h/h216.md) there [ʿāḏar](../../strongs/h/h5737.md) not one of them that was not ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md).

<a name="2samuel_17_23"></a>2Samuel 17:23

And when ['Ăḥîṯōp̄El](../../strongs/h/h302.md) [ra'ah](../../strongs/h/h7200.md) that his ['etsah](../../strongs/h/h6098.md) was not ['asah](../../strongs/h/h6213.md), he [ḥāḇaš](../../strongs/h/h2280.md) his [chamowr](../../strongs/h/h2543.md), and [quwm](../../strongs/h/h6965.md), and gat him [yālaḵ](../../strongs/h/h3212.md) to his [bayith](../../strongs/h/h1004.md), to his [ʿîr](../../strongs/h/h5892.md), and put his [bayith](../../strongs/h/h1004.md) in [tsavah](../../strongs/h/h6680.md), and [ḥānaq](../../strongs/h/h2614.md) himself, and [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in the [qeber](../../strongs/h/h6913.md) of his ['ab](../../strongs/h/h1.md).

<a name="2samuel_17_24"></a>2Samuel 17:24

Then [Dāviḏ](../../strongs/h/h1732.md) [bow'](../../strongs/h/h935.md) to [Maḥănayim](../../strongs/h/h4266.md). And ['Ăbyšālôm](../../strongs/h/h53.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md), he and all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) with him.

<a name="2samuel_17_25"></a>2Samuel 17:25

And ['Ăbyšālôm](../../strongs/h/h53.md) [śûm](../../strongs/h/h7760.md) [ʿĂmāśā'](../../strongs/h/h6021.md) the [tsaba'](../../strongs/h/h6635.md) instead of [Yô'āḇ](../../strongs/h/h3097.md): which [ʿĂmāśā'](../../strongs/h/h6021.md) was an ['iysh](../../strongs/h/h376.md) [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) was [Yiṯrā'](../../strongs/h/h3501.md) a [yiśrᵊ'ēlî](../../strongs/h/h3481.md), that [bow'](../../strongs/h/h935.md) to ['Ăḇîḡayil](../../strongs/h/h26.md) the [bath](../../strongs/h/h1323.md) of [Nāḥāš](../../strongs/h/h5176.md), ['āḥôṯ](../../strongs/h/h269.md) to [Ṣᵊrûyâ](../../strongs/h/h6870.md) [Yô'āḇ](../../strongs/h/h3097.md) ['em](../../strongs/h/h517.md).

<a name="2samuel_17_26"></a>2Samuel 17:26

So [Yisra'el](../../strongs/h/h3478.md) and ['Ăbyšālôm](../../strongs/h/h53.md) [ḥānâ](../../strongs/h/h2583.md) in the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="2samuel_17_27"></a>2Samuel 17:27

And it came to pass, when [Dāviḏ](../../strongs/h/h1732.md) was [bow'](../../strongs/h/h935.md) to [Maḥănayim](../../strongs/h/h4266.md), that [Šōḇî](../../strongs/h/h7629.md) the [ben](../../strongs/h/h1121.md) of [Nāḥāš](../../strongs/h/h5176.md) of [Rabâ](../../strongs/h/h7237.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [Māḵîr](../../strongs/h/h4353.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmî'ēl](../../strongs/h/h5988.md) of [Lō' Ḏᵊḇār](../../strongs/h/h3810.md), and [Barzillay](../../strongs/h/h1271.md) the [Gilʿāḏî](../../strongs/h/h1569.md) of [Rōḡlîm](../../strongs/h/h7274.md),

<a name="2samuel_17_28"></a>2Samuel 17:28

[nāḡaš](../../strongs/h/h5066.md) [miškāḇ](../../strongs/h/h4904.md), and [caph](../../strongs/h/h5592.md), and [yāṣar](../../strongs/h/h3335.md) [kĕliy](../../strongs/h/h3627.md), and [ḥiṭṭâ](../../strongs/h/h2406.md), and [śᵊʿōrâ](../../strongs/h/h8184.md), and [qemaḥ](../../strongs/h/h7058.md), and [qālî](../../strongs/h/h7039.md) corn, and [pôl](../../strongs/h/h6321.md), and [ʿāḏāš](../../strongs/h/h5742.md), and [qālî](../../strongs/h/h7039.md) pulse,

<a name="2samuel_17_29"></a>2Samuel 17:29

And [dĕbash](../../strongs/h/h1706.md), and [ḥem'â](../../strongs/h/h2529.md), and [tso'n](../../strongs/h/h6629.md), and [šāp̄ôṯ](../../strongs/h/h8194.md) of [bāqār](../../strongs/h/h1241.md), for [Dāviḏ](../../strongs/h/h1732.md), and for the ['am](../../strongs/h/h5971.md) that were with him, to ['akal](../../strongs/h/h398.md): for they ['āmar](../../strongs/h/h559.md), The ['am](../../strongs/h/h5971.md) is [rāʿēḇ](../../strongs/h/h7457.md), and [ʿāyēp̄](../../strongs/h/h5889.md), and [ṣāmē'](../../strongs/h/h6771.md), in the [midbar](../../strongs/h/h4057.md).

---

[Transliteral Bible](../bible.md)

[2Samuel](2samuel.md)

[2Samuel 16](2samuel_16.md) - [2Samuel 18](2samuel_18.md)