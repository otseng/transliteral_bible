# [1 Peter 3](https://www.blueletterbible.org/kjv/1pe/3/1/s_1154001)

<a name="1peter_3_1"></a>1 Peter 3:1

[homoiōs](../../strongs/g/g3668.md), ye [gynē](../../strongs/g/g1135.md), in [hypotassō](../../strongs/g/g5293.md) to your own [anēr](../../strongs/g/g435.md); that, if any [apeitheō](../../strongs/g/g544.md) the [logos](../../strongs/g/g3056.md), they also may without the [logos](../../strongs/g/g3056.md) be [kerdainō](../../strongs/g/g2770.md) by the [anastrophē](../../strongs/g/g391.md) of the [gynē](../../strongs/g/g1135.md);

<a name="1peter_3_2"></a>1 Peter 3:2

While they [epopteuō](../../strongs/g/g2029.md) your [hagnos](../../strongs/g/g53.md) [anastrophē](../../strongs/g/g391.md) with [phobos](../../strongs/g/g5401.md).

<a name="1peter_3_3"></a>1 Peter 3:3

Whose [kosmos](../../strongs/g/g2889.md) let it not be that [exōthen](../../strongs/g/g1855.md) of [emplokē](../../strongs/g/g1708.md) the [thrix](../../strongs/g/g2359.md), and of [perithesis](../../strongs/g/g4025.md) of [chrysion](../../strongs/g/g5553.md), or of [endysis](../../strongs/g/g1745.md) of [himation](../../strongs/g/g2440.md);

<a name="1peter_3_4"></a>1 Peter 3:4

But the [kryptos](../../strongs/g/g2927.md) [anthrōpos](../../strongs/g/g444.md) of the [kardia](../../strongs/g/g2588.md), in that which is [aphthartos](../../strongs/g/g862.md), of a [praÿs](../../strongs/g/g4239.md) and [hēsychios](../../strongs/g/g2272.md) [pneuma](../../strongs/g/g4151.md), which is in the [enōpion](../../strongs/g/g1799.md) of [theos](../../strongs/g/g2316.md) of [polytelēs](../../strongs/g/g4185.md).

<a name="1peter_3_5"></a>1 Peter 3:5

For after this [houtō(s)](../../strongs/g/g3779.md) in the [pote](../../strongs/g/g4218.md) the [hagios](../../strongs/g/g40.md) [gynē](../../strongs/g/g1135.md) also, who [elpizō](../../strongs/g/g1679.md) in [theos](../../strongs/g/g2316.md), [kosmeō](../../strongs/g/g2885.md) themselves, being in [hypotassō](../../strongs/g/g5293.md) unto their own [anēr](../../strongs/g/g435.md):

<a name="1peter_3_6"></a>1 Peter 3:6

Even as [Sarra](../../strongs/g/g4564.md) [hypakouō](../../strongs/g/g5219.md) [Abraam](../../strongs/g/g11.md), [kaleō](../../strongs/g/g2564.md) him [kyrios](../../strongs/g/g2962.md): whose [teknon](../../strongs/g/g5043.md) ye are, as long as ye [agathopoieō](../../strongs/g/g15.md), and are not [phobeō](../../strongs/g/g5399.md) with [mēdeis](../../strongs/g/g3367.md) [ptoēsis](../../strongs/g/g4423.md).

<a name="1peter_3_7"></a>1 Peter 3:7

[homoiōs](../../strongs/g/g3668.md), ye [anēr](../../strongs/g/g435.md), [synoikeō](../../strongs/g/g4924.md) according to [gnōsis](../../strongs/g/g1108.md), [aponemō](../../strongs/g/g632.md) [timē](../../strongs/g/g5092.md) unto the [gynaikeios](../../strongs/g/g1134.md), as unto the [asthenēs](../../strongs/g/g772.md) [skeuos](../../strongs/g/g4632.md), and as being [sygklēronomos](../../strongs/g/g4789.md) of the [charis](../../strongs/g/g5485.md) of [zōē](../../strongs/g/g2222.md); that your [proseuchē](../../strongs/g/g4335.md) be not [ekkoptō](../../strongs/g/g1581.md).

<a name="1peter_3_8"></a>1 Peter 3:8

[de](../../strongs/g/g1161.md) [telos](../../strongs/g/g5056.md), all of [homophrōn](../../strongs/g/g3675.md), [sympathēs](../../strongs/g/g4835.md), [philadelphos](../../strongs/g/g5361.md), [eusplagchnos](../../strongs/g/g2155.md), [philophrōn](../../strongs/g/g5391.md):

<a name="1peter_3_9"></a>1 Peter 3:9

Not [apodidōmi](../../strongs/g/g591.md) [kakos](../../strongs/g/g2556.md) for [kakos](../../strongs/g/g2556.md), or [loidoria](../../strongs/g/g3059.md) for [loidoria](../../strongs/g/g3059.md): but [tounantion](../../strongs/g/g5121.md) [eulogia](../../strongs/g/g2129.md); [eidō](../../strongs/g/g1492.md) that ye are thereunto [kaleō](../../strongs/g/g2564.md), that ye should [klēronomeō](../../strongs/g/g2816.md) an [eulogeō](../../strongs/g/g2127.md).

<a name="1peter_3_10"></a>1 Peter 3:10

For he that will [agapaō](../../strongs/g/g25.md) [zōē](../../strongs/g/g2222.md), and [eidō](../../strongs/g/g1492.md) [agathos](../../strongs/g/g18.md) [hēmera](../../strongs/g/g2250.md), let him [pauō](../../strongs/g/g3973.md) his [glōssa](../../strongs/g/g1100.md) from [kakos](../../strongs/g/g2556.md), and his [cheilos](../../strongs/g/g5491.md) that they [laleō](../../strongs/g/g2980.md) no [dolos](../../strongs/g/g1388.md):

<a name="1peter_3_11"></a>1 Peter 3:11

Let him [ekklinō](../../strongs/g/g1578.md) [kakos](../../strongs/g/g2556.md), and [poieō](../../strongs/g/g4160.md) [agathos](../../strongs/g/g18.md); let him [zēteō](../../strongs/g/g2212.md) [eirēnē](../../strongs/g/g1515.md), and [diōkō](../../strongs/g/g1377.md) it.

<a name="1peter_3_12"></a>1 Peter 3:12

For the [ophthalmos](../../strongs/g/g3788.md) of the [kyrios](../../strongs/g/g2962.md) are over the [dikaios](../../strongs/g/g1342.md), and his [ous](../../strongs/g/g3775.md) unto their [deēsis](../../strongs/g/g1162.md): but the [prosōpon](../../strongs/g/g4383.md) of the [kyrios](../../strongs/g/g2962.md) against them that [poieō](../../strongs/g/g4160.md) [kakos](../../strongs/g/g2556.md).

<a name="1peter_3_13"></a>1 Peter 3:13

And who he that will [kakoō](../../strongs/g/g2559.md) you, if ye be [mimētēs](../../strongs/g/g3402.md) of that which is [agathos](../../strongs/g/g18.md)?

<a name="1peter_3_14"></a>1 Peter 3:14

But and if ye [paschō](../../strongs/g/g3958.md) for [dikaiosynē](../../strongs/g/g1343.md), [makarios](../../strongs/g/g3107.md) ye: and be not [phobeō](../../strongs/g/g5399.md) of their [phobos](../../strongs/g/g5401.md), neither be [tarassō](../../strongs/g/g5015.md);

<a name="1peter_3_15"></a>1 Peter 3:15

But [hagiazō](../../strongs/g/g37.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) in your [kardia](../../strongs/g/g2588.md): and be [hetoimos](../../strongs/g/g2092.md) always to an [apologia](../../strongs/g/g627.md) to [pas](../../strongs/g/g3956.md) that [aiteō](../../strongs/g/g154.md) you a [logos](../../strongs/g/g3056.md) of the [elpis](../../strongs/g/g1680.md) that is in you with [praÿtēs](../../strongs/g/g4240.md) and [phobos](../../strongs/g/g5401.md):

<a name="1peter_3_16"></a>1 Peter 3:16

Having an [agathos](../../strongs/g/g18.md) [syneidēsis](../../strongs/g/g4893.md); that, whereas they [katalaleō](../../strongs/g/g2635.md) of you, as of [kakopoios](../../strongs/g/g2555.md), they may be [kataischynō](../../strongs/g/g2617.md) that [epēreazō](../../strongs/g/g1908.md) your [agathos](../../strongs/g/g18.md) [anastrophē](../../strongs/g/g391.md) in [Christos](../../strongs/g/g5547.md).

<a name="1peter_3_17"></a>1 Peter 3:17

For [kreittōn](../../strongs/g/g2909.md), if the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) be so, that ye [paschō](../../strongs/g/g3958.md) for [agathopoieō](../../strongs/g/g15.md), than for [kakopoieō](../../strongs/g/g2554.md).

<a name="1peter_3_18"></a>1 Peter 3:18

For [Christos](../../strongs/g/g5547.md) also hath [hapax](../../strongs/g/g530.md) [paschō](../../strongs/g/g3958.md) for [hamartia](../../strongs/g/g266.md), the [dikaios](../../strongs/g/g1342.md) for the [adikos](../../strongs/g/g94.md), that he might [prosagō](../../strongs/g/g4317.md) us to [theos](../../strongs/g/g2316.md), being [thanatoō](../../strongs/g/g2289.md) in the [sarx](../../strongs/g/g4561.md), but [zōopoieō](../../strongs/g/g2227.md) by the [pneuma](../../strongs/g/g4151.md):

<a name="1peter_3_19"></a>1 Peter 3:19

By which also he [poreuō](../../strongs/g/g4198.md) and [kēryssō](../../strongs/g/g2784.md) unto the [pneuma](../../strongs/g/g4151.md) in [phylakē](../../strongs/g/g5438.md);

<a name="1peter_3_20"></a>1 Peter 3:20

Which sometime were [apeitheō](../../strongs/g/g544.md), when [hapax](../../strongs/g/g530.md) the [makrothymia](../../strongs/g/g3115.md) of [theos](../../strongs/g/g2316.md) [ekdechomai](../../strongs/g/g1551.md) in the days of [Nōe](../../strongs/g/g3575.md), while the [kibōtos](../../strongs/g/g2787.md) was a [kataskeuazō](../../strongs/g/g2680.md), wherein [oligos](../../strongs/g/g3641.md), that is, eight [psychē](../../strongs/g/g5590.md) were [diasōzō](../../strongs/g/g1295.md) by [hydōr](../../strongs/g/g5204.md).

<a name="1peter_3_21"></a>1 Peter 3:21

The [antitypos](../../strongs/g/g499.md) whereunto even [baptisma](../../strongs/g/g908.md) doth also now [sōzō](../../strongs/g/g4982.md) us (not the [apothesis](../../strongs/g/g595.md) of the [rhypos](../../strongs/g/g4509.md) of the [sarx](../../strongs/g/g4561.md), but the [eperōtēma](../../strongs/g/g1906.md) of an [agathos](../../strongs/g/g18.md) [syneidēsis](../../strongs/g/g4893.md) toward [theos](../../strongs/g/g2316.md),) by the [anastasis](../../strongs/g/g386.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="1peter_3_22"></a>1 Peter 3:22

Who is [poreuō](../../strongs/g/g4198.md) into [ouranos](../../strongs/g/g3772.md), and is on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md); [aggelos](../../strongs/g/g32.md) and [exousia](../../strongs/g/g1849.md) and [dynamis](../../strongs/g/g1411.md) being [hypotassō](../../strongs/g/g5293.md) unto him.

---

[Transliteral Bible](../bible.md)

[1Peter](1peter.md)

[1Peter 2](1peter_2.md) - [1Peter 4](1peter_4.md)