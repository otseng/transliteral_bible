# 1 Peter

[1 Peter Overview](../../commentary/1peter/1peter_overview.md)

[1 Peter 1](1peter_1.md)

[1 Peter 2](1peter_2.md)

[1 Peter 3](1peter_3.md)

[1 Peter 4](1peter_4.md)

[1 Peter 5](1peter_5.md)

---

[Transliteral Bible](../index.md)
