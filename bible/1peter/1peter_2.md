# [1 Peter 2](https://www.blueletterbible.org/kjv/1pe/2/9/s_1153001)

<a name="1peter_2_1"></a>1 Peter 2:1

Wherefore [apotithēmi](../../strongs/g/g659.md) all [kakia](../../strongs/g/g2549.md), and all [dolos](../../strongs/g/g1388.md), and [hypokrisis](../../strongs/g/g5272.md), and [phthonos](../../strongs/g/g5355.md), all [katalalia](../../strongs/g/g2636.md),

<a name="1peter_2_2"></a>1 Peter 2:2

As [artigennētos](../../strongs/g/g738.md) [brephos](../../strongs/g/g1025.md), [epipotheo](../../strongs/g/g1971.md) the [adolos](../../strongs/g/g97.md) [gala](../../strongs/g/g1051.md) [logikos](../../strongs/g/g3050.md), that ye may [auxanō](../../strongs/g/g837.md) thereby: [^1]

<a name="1peter_2_3"></a>1 Peter 2:3

If so be ye have [geuomai](../../strongs/g/g1089.md) that the [kyrios](../../strongs/g/g2962.md) [chrēstos](../../strongs/g/g5543.md).

<a name="1peter_2_4"></a>1 Peter 2:4

To whom [proserchomai](../../strongs/g/g4334.md), a [zaō](../../strongs/g/g2198.md) [lithos](../../strongs/g/g3037.md), [apodokimazō](../../strongs/g/g593.md) indeed of [anthrōpos](../../strongs/g/g444.md), but [eklektos](../../strongs/g/g1588.md) of [theos](../../strongs/g/g2316.md), [entimos](../../strongs/g/g1784.md),

<a name="1peter_2_5"></a>1 Peter 2:5

Ye also, as [zaō](../../strongs/g/g2198.md) [lithos](../../strongs/g/g3037.md), are [oikodomeō](../../strongs/g/g3618.md) a [pneumatikos](../../strongs/g/g4152.md) [oikos](../../strongs/g/g3624.md), an [hagios](../../strongs/g/g40.md) [hierateuma](../../strongs/g/g2406.md), to [anapherō](../../strongs/g/g399.md) [pneumatikos](../../strongs/g/g4152.md) [thysia](../../strongs/g/g2378.md), [euprosdektos](../../strongs/g/g2144.md) to [theos](../../strongs/g/g2316.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1peter_2_6"></a>1 Peter 2:6

Wherefore also it is [periechō](../../strongs/g/g4023.md) in the [graphē](../../strongs/g/g1124.md), [idou](../../strongs/g/g2400.md), I [tithēmi](../../strongs/g/g5087.md) in [Siōn](../../strongs/g/g4622.md) an [akrogōniaios](../../strongs/g/g204.md) [lithos](../../strongs/g/g3037.md), [eklektos](../../strongs/g/g1588.md), [entimos](../../strongs/g/g1784.md): and he that [pisteuō](../../strongs/g/g4100.md) on him shall not be [kataischynō](../../strongs/g/g2617.md).

<a name="1peter_2_7"></a>1 Peter 2:7

Unto you therefore which [pisteuō](../../strongs/g/g4100.md) [timē](../../strongs/g/g5092.md): but unto them which be [apeitheō](../../strongs/g/g544.md), the [lithos](../../strongs/g/g3037.md) which the [oikodomeō](../../strongs/g/g3618.md) [apodokimazō](../../strongs/g/g593.md), the same is made the [kephalē](../../strongs/g/g2776.md) of the [gōnia](../../strongs/g/g1137.md),

<a name="1peter_2_8"></a>1 Peter 2:8

And a [lithos](../../strongs/g/g3037.md) of [proskomma](../../strongs/g/g4348.md), and a [petra](../../strongs/g/g4073.md) of [skandalon](../../strongs/g/g4625.md), to them which [proskoptō](../../strongs/g/g4350.md) at the [logos](../../strongs/g/g3056.md), being [apeitheō](../../strongs/g/g544.md): whereunto also they were [tithēmi](../../strongs/g/g5087.md).

<a name="1peter_2_9"></a>1 Peter 2:9

But ye [eklektos](../../strongs/g/g1588.md) [genos](../../strongs/g/g1085.md), a [basileios](../../strongs/g/g934.md) [hierateuma](../../strongs/g/g2406.md), an [hagios](../../strongs/g/g40.md) [ethnos](../../strongs/g/g1484.md), an [eis](../../strongs/g/g1519.md) [peripoiēsis](../../strongs/g/g4047.md) [laos](../../strongs/g/g2992.md); that ye should [exaggellō](../../strongs/g/g1804.md) the [aretē](../../strongs/g/g703.md) of him who hath [kaleō](../../strongs/g/g2564.md) you out of [skotos](../../strongs/g/g4655.md) into his [thaumastos](../../strongs/g/g2298.md) [phōs](../../strongs/g/g5457.md); [^2]

<a name="1peter_2_10"></a>1 Peter 2:10

Which [pote](../../strongs/g/g4218.md) not a [laos](../../strongs/g/g2992.md), but are [nyn](../../strongs/g/g3568.md) the [laos](../../strongs/g/g2992.md) of [theos](../../strongs/g/g2316.md): which had not [eleeō](../../strongs/g/g1653.md), but now have [eleeō](../../strongs/g/g1653.md).

<a name="1peter_2_11"></a>1 Peter 2:11

[agapētos](../../strongs/g/g27.md), I [parakaleō](../../strongs/g/g3870.md) as [paroikos](../../strongs/g/g3941.md) and [parepidēmos](../../strongs/g/g3927.md), [apechomai](../../strongs/g/g567.md) from [sarkikos](../../strongs/g/g4559.md) [epithymia](../../strongs/g/g1939.md), which [strateuō](../../strongs/g/g4754.md) against the [psychē](../../strongs/g/g5590.md);

<a name="1peter_2_12"></a>1 Peter 2:12

Having your [anastrophē](../../strongs/g/g391.md) [kalos](../../strongs/g/g2570.md) among the [ethnos](../../strongs/g/g1484.md): that, whereas they [katalaleō](../../strongs/g/g2635.md) you as [kakopoios](../../strongs/g/g2555.md), they may by [kalos](../../strongs/g/g2570.md) [ergon](../../strongs/g/g2041.md), which they shall [epopteuō](../../strongs/g/g2029.md), [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) in the [hēmera](../../strongs/g/g2250.md) of [episkopē](../../strongs/g/g1984.md).

<a name="1peter_2_13"></a>1 Peter 2:13

[hypotassō](../../strongs/g/g5293.md) to every [ktisis](../../strongs/g/g2937.md) of [anthrōpinos](../../strongs/g/g442.md) for the [kyrios](../../strongs/g/g2962.md): whether it be to the [basileus](../../strongs/g/g935.md), as [hyperechō](../../strongs/g/g5242.md);

<a name="1peter_2_14"></a>1 Peter 2:14

Or unto [hēgemōn](../../strongs/g/g2232.md), as unto them that are [pempō](../../strongs/g/g3992.md) by him for the [ekdikēsis](../../strongs/g/g1557.md) of [kakopoios](../../strongs/g/g2555.md), and for the [epainos](../../strongs/g/g1868.md) of them that [agathopoios](../../strongs/g/g17.md).

<a name="1peter_2_15"></a>1 Peter 2:15

For so is the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), that with [agathopoieō](../../strongs/g/g15.md) ye may [phimoō](../../strongs/g/g5392.md) the [agnōsia](../../strongs/g/g56.md) of [aphrōn](../../strongs/g/g878.md) [anthrōpos](../../strongs/g/g444.md):

<a name="1peter_2_16"></a>1 Peter 2:16

As [eleutheros](../../strongs/g/g1658.md), and not using [eleutheria](../../strongs/g/g1657.md) for an [epikalymma](../../strongs/g/g1942.md) of [kakia](../../strongs/g/g2549.md), but as the [doulos](../../strongs/g/g1401.md) of [theos](../../strongs/g/g2316.md).

<a name="1peter_2_17"></a>1 Peter 2:17

[timaō](../../strongs/g/g5091.md) all. [agapaō](../../strongs/g/g25.md) the [adelphotēs](../../strongs/g/g81.md). [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md). [timaō](../../strongs/g/g5091.md) the [basileus](../../strongs/g/g935.md).

<a name="1peter_2_18"></a>1 Peter 2:18

[oiketēs](../../strongs/g/g3610.md), [hypotassō](../../strongs/g/g5293.md) to [despotēs](../../strongs/g/g1203.md) with all [phobos](../../strongs/g/g5401.md); not only to the [agathos](../../strongs/g/g18.md) and [epieikēs](../../strongs/g/g1933.md), but also to the [skolios](../../strongs/g/g4646.md).

<a name="1peter_2_19"></a>1 Peter 2:19

For this [charis](../../strongs/g/g5485.md), if a [tis](../../strongs/g/g5100.md) for [syneidēsis](../../strongs/g/g4893.md) toward [theos](../../strongs/g/g2316.md) [hypopherō](../../strongs/g/g5297.md) [lypē](../../strongs/g/g3077.md), [paschō](../../strongs/g/g3958.md) [adikōs](../../strongs/g/g95.md).

<a name="1peter_2_20"></a>1 Peter 2:20

For what [kleos](../../strongs/g/g2811.md), if, when ye be [kolaphizō](../../strongs/g/g2852.md) for your [hamartanō](../../strongs/g/g264.md), ye shall [hypomenō](../../strongs/g/g5278.md)? but if, when ye [agathopoieō](../../strongs/g/g15.md), and [paschō](../../strongs/g/g3958.md), ye [hypomenō](../../strongs/g/g5278.md), this [charis](../../strongs/g/g5485.md) with [theos](../../strongs/g/g2316.md).

<a name="1peter_2_21"></a>1 Peter 2:21

For even hereunto were ye [kaleō](../../strongs/g/g2564.md): because [Christos](../../strongs/g/g5547.md) also [paschō](../../strongs/g/g3958.md) for us, [hypolimpanō](../../strongs/g/g5277.md) us an [hypogrammos](../../strongs/g/g5261.md), that ye should [epakoloutheō](../../strongs/g/g1872.md) his [ichnos](../../strongs/g/g2487.md):

<a name="1peter_2_22"></a>1 Peter 2:22

Who [poieō](../../strongs/g/g4160.md) no [hamartia](../../strongs/g/g266.md), neither was [dolos](../../strongs/g/g1388.md) [heuriskō](../../strongs/g/g2147.md) in his [stoma](../../strongs/g/g4750.md):

<a name="1peter_2_23"></a>1 Peter 2:23

Who, when he was [loidoreō](../../strongs/g/g3058.md), [antiloidoreō](../../strongs/g/g486.md) not again; when he [paschō](../../strongs/g/g3958.md), he [apeileō](../../strongs/g/g546.md) not; but [paradidōmi](../../strongs/g/g3860.md) to him that [krinō](../../strongs/g/g2919.md) [dikaiōs](../../strongs/g/g1346.md):

<a name="1peter_2_24"></a>1 Peter 2:24

Who his own self [anapherō](../../strongs/g/g399.md) our [hamartia](../../strongs/g/g266.md) in his own [sōma](../../strongs/g/g4983.md) on the [xylon](../../strongs/g/g3586.md), that we, [apoginomai](../../strongs/g/g581.md) to [hamartia](../../strongs/g/g266.md), should [zaō](../../strongs/g/g2198.md) unto [dikaiosynē](../../strongs/g/g1343.md): by whose [mōlōps](../../strongs/g/g3468.md) ye were [iaomai](../../strongs/g/g2390.md).

<a name="1peter_2_25"></a>1 Peter 2:25

For ye were as [probaton](../../strongs/g/g4263.md) [planaō](../../strongs/g/g4105.md); but are now [epistrephō](../../strongs/g/g1994.md) unto the [poimēn](../../strongs/g/g4166.md) and [episkopos](../../strongs/g/g1985.md) of your [psychē](../../strongs/g/g5590.md).

---

[Transliteral Bible](../bible.md)

[1 Peter](1peter.md)

[1 Peter 1](1peter_1.md) - [1 Peter 3](1peter_3.md)

---

[^1]: [1 Peter 2:2 Commentary](../../commentary/1peter/1peter_2_commentary.md#1peter_2_2)

[^2]: [1 Peter 2:9 Commentary](../../commentary/1peter/1peter_2_commentary.md#1peter_2_9)
