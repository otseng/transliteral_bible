# [1 Peter 1](https://www.blueletterbible.org/kjv/1pe/1/1/s_1152001)

<a name="1peter_1_1"></a>1 Peter 1:1

[Petros](../../strongs/g/g4074.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), to the [parepidēmos](../../strongs/g/g3927.md) [diaspora](../../strongs/g/g1290.md) throughout [Pontos](../../strongs/g/g4195.md), [galatia](../../strongs/g/g1053.md), [Kappadokia](../../strongs/g/g2587.md), [Asia](../../strongs/g/g773.md), and [Bithynia](../../strongs/g/g978.md),

<a name="1peter_1_2"></a>1 Peter 1:2

[eklektos](../../strongs/g/g1588.md) according to the [prognōsis](../../strongs/g/g4268.md) of [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md), through [hagiasmos](../../strongs/g/g38.md) of the [pneuma](../../strongs/g/g4151.md), unto [hypakoē](../../strongs/g/g5218.md) and [rhantismos](../../strongs/g/g4473.md) of the [haima](../../strongs/g/g129.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md): [charis](../../strongs/g/g5485.md) unto you, and [eirēnē](../../strongs/g/g1515.md), [plēthynō](../../strongs/g/g4129.md).

<a name="1peter_1_3"></a>1 Peter 1:3

[eulogētos](../../strongs/g/g2128.md) the [theos](../../strongs/g/g2316.md) and [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), which according to his [polys](../../strongs/g/g4183.md) [eleos](../../strongs/g/g1656.md) hath [anagennaō](../../strongs/g/g313.md) us again unto a [zaō](../../strongs/g/g2198.md) [elpis](../../strongs/g/g1680.md) by the [anastasis](../../strongs/g/g386.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) from the [nekros](../../strongs/g/g3498.md),

<a name="1peter_1_4"></a>1 Peter 1:4

To a [klēronomia](../../strongs/g/g2817.md) [aphthartos](../../strongs/g/g862.md), and [amiantos](../../strongs/g/g283.md), and that [amarantos](../../strongs/g/g263.md), [tēreō](../../strongs/g/g5083.md) in [ouranos](../../strongs/g/g3772.md) for you,

<a name="1peter_1_5"></a>1 Peter 1:5

Who are [phroureō](../../strongs/g/g5432.md) by the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md) through [pistis](../../strongs/g/g4102.md) unto [sōtēria](../../strongs/g/g4991.md) [hetoimos](../../strongs/g/g2092.md) to be [apokalyptō](../../strongs/g/g601.md) in the [eschatos](../../strongs/g/g2078.md) [kairos](../../strongs/g/g2540.md).

<a name="1peter_1_6"></a>1 Peter 1:6

Wherein ye [agalliaō](../../strongs/g/g21.md), though now for a [oligos](../../strongs/g/g3641.md), if need be, ye are in [lypeō](../../strongs/g/g3076.md) through [poikilos](../../strongs/g/g4164.md) [peirasmos](../../strongs/g/g3986.md):

<a name="1peter_1_7"></a>1 Peter 1:7

That the [dokimion](../../strongs/g/g1383.md) of your [pistis](../../strongs/g/g4102.md), being [polys](../../strongs/g/g4183.md) more [timios](../../strongs/g/g5093.md) than of [chrysion](../../strongs/g/g5553.md) that [apollymi](../../strongs/g/g622.md), though it be [dokimazō](../../strongs/g/g1381.md) with [pyr](../../strongs/g/g4442.md), might be [heuriskō](../../strongs/g/g2147.md) unto [epainos](../../strongs/g/g1868.md) and [timē](../../strongs/g/g5092.md) and [doxa](../../strongs/g/g1391.md) at the [apokalypsis](../../strongs/g/g602.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="1peter_1_8"></a>1 Peter 1:8

Whom having not [eidō](../../strongs/g/g1492.md), ye [agapaō](../../strongs/g/g25.md); in whom, though now ye [horaō](../../strongs/g/g3708.md) not, yet [pisteuō](../../strongs/g/g4100.md), ye [agalliaō](../../strongs/g/g21.md) with [chara](../../strongs/g/g5479.md) [aneklalētos](../../strongs/g/g412.md) and [doxazō](../../strongs/g/g1392.md):

<a name="1peter_1_9"></a>1 Peter 1:9

[komizō](../../strongs/g/g2865.md) the [telos](../../strongs/g/g5056.md) of your [pistis](../../strongs/g/g4102.md), the [sōtēria](../../strongs/g/g4991.md) of [psychē](../../strongs/g/g5590.md).

<a name="1peter_1_10"></a>1 Peter 1:10

Of which [sōtēria](../../strongs/g/g4991.md) the [prophētēs](../../strongs/g/g4396.md) have [ekzēteō](../../strongs/g/g1567.md) and [exeraunaō](../../strongs/g/g1830.md), who [prophēteuō](../../strongs/g/g4395.md) of the [charis](../../strongs/g/g5485.md) unto you:

<a name="1peter_1_11"></a>1 Peter 1:11

[eraunaō](../../strongs/g/g2045.md) what, or [poios](../../strongs/g/g4169.md) of [kairos](../../strongs/g/g2540.md) the [pneuma](../../strongs/g/g4151.md) of [Christos](../../strongs/g/g5547.md) which was in them did [dēloō](../../strongs/g/g1213.md), when it [promartyromai](../../strongs/g/g4303.md) the [pathēma](../../strongs/g/g3804.md) of [Christos](../../strongs/g/g5547.md), and the [doxa](../../strongs/g/g1391.md) that should follow.

<a name="1peter_1_12"></a>1 Peter 1:12

Unto whom it was [apokalyptō](../../strongs/g/g601.md), that not unto themselves, but unto us they did [diakoneō](../../strongs/g/g1247.md) the things, which are now [anaggellō](../../strongs/g/g312.md) unto you by them that have [euaggelizō](../../strongs/g/g2097.md) unto you with the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [apostellō](../../strongs/g/g649.md) from [ouranos](../../strongs/g/g3772.md); which things the [aggelos](../../strongs/g/g32.md) [epithymeō](../../strongs/g/g1937.md) to [parakyptō](../../strongs/g/g3879.md) into.

<a name="1peter_1_13"></a>1 Peter 1:13

Wherefore [anazōnnymi](../../strongs/g/g328.md) the [osphys](../../strongs/g/g3751.md) of your [dianoia](../../strongs/g/g1271.md), be [nēphō](../../strongs/g/g3525.md), and [elpizō](../../strongs/g/g1679.md) [teleiōs](../../strongs/g/g5049.md) for the [charis](../../strongs/g/g5485.md) that is to be [pherō](../../strongs/g/g5342.md) unto you at the [apokalypsis](../../strongs/g/g602.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md);

<a name="1peter_1_14"></a>1 Peter 1:14

As [hypakoē](../../strongs/g/g5218.md) [teknon](../../strongs/g/g5043.md), not [syschēmatizō](../../strongs/g/g4964.md) to the [proteros](../../strongs/g/g4386.md) [epithymia](../../strongs/g/g1939.md) in your [agnoia](../../strongs/g/g52.md):

<a name="1peter_1_15"></a>1 Peter 1:15

But as he which hath [kaleō](../../strongs/g/g2564.md) you is [hagios](../../strongs/g/g40.md), so be ye [hagios](../../strongs/g/g40.md) in all [anastrophē](../../strongs/g/g391.md);

<a name="1peter_1_16"></a>1 Peter 1:16

Because it is [graphō](../../strongs/g/g1125.md), [ginomai](../../strongs/g/g1096.md) [hagios](../../strongs/g/g40.md); for I am [hagios](../../strongs/g/g40.md). [^1]

<a name="1peter_1_17"></a>1 Peter 1:17

And if ye [epikaleō](../../strongs/g/g1941.md) the [patēr](../../strongs/g/g3962.md), who [aprosōpolēmptōs](../../strongs/g/g678.md) [krinō](../../strongs/g/g2919.md) according to [hekastos](../../strongs/g/g1538.md) [ergon](../../strongs/g/g2041.md), [anastrephō](../../strongs/g/g390.md) the [chronos](../../strongs/g/g5550.md) of your [paroikia](../../strongs/g/g3940.md) here in [phobos](../../strongs/g/g5401.md):

<a name="1peter_1_18"></a>1 Peter 1:18

Forasmuch as ye [eidō](../../strongs/g/g1492.md) that ye were not [lytroō](../../strongs/g/g3084.md) with [phthartos](../../strongs/g/g5349.md), as [argyrion](../../strongs/g/g694.md) and [chrysion](../../strongs/g/g5553.md), from your [mataios](../../strongs/g/g3152.md) [anastrophē](../../strongs/g/g391.md) by [patroparadotos](../../strongs/g/g3970.md);

<a name="1peter_1_19"></a>1 Peter 1:19

But with the [timios](../../strongs/g/g5093.md) [haima](../../strongs/g/g129.md) of [Christos](../../strongs/g/g5547.md), as of an [amnos](../../strongs/g/g286.md) [amōmos](../../strongs/g/g299.md) and [aspilos](../../strongs/g/g784.md):

<a name="1peter_1_20"></a>1 Peter 1:20

Who [men](../../strongs/g/g3303.md) was [proginōskō](../../strongs/g/g4267.md) before the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md), but was [phaneroō](../../strongs/g/g5319.md) in these [eschatos](../../strongs/g/g2078.md) [chronos](../../strongs/g/g5550.md) for you,

<a name="1peter_1_21"></a>1 Peter 1:21

Who by him [pisteuō](../../strongs/g/g4100.md) in [theos](../../strongs/g/g2316.md), that [egeirō](../../strongs/g/g1453.md) him up from the [nekros](../../strongs/g/g3498.md), and [didōmi](../../strongs/g/g1325.md) him [doxa](../../strongs/g/g1391.md); that your [pistis](../../strongs/g/g4102.md) and [elpis](../../strongs/g/g1680.md) might be in [theos](../../strongs/g/g2316.md).

<a name="1peter_1_22"></a>1 Peter 1:22

Seeing ye have [hagnizō](../../strongs/g/g48.md) your [psychē](../../strongs/g/g5590.md) in [hypakoē](../../strongs/g/g5218.md) the [alētheia](../../strongs/g/g225.md) through the [pneuma](../../strongs/g/g4151.md) unto [anypokritos](../../strongs/g/g505.md) [philadelphia](../../strongs/g/g5360.md), see that ye [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md) with a [katharos](../../strongs/g/g2513.md) [kardia](../../strongs/g/g2588.md) [ektenōs](../../strongs/g/g1619.md):

<a name="1peter_1_23"></a>1 Peter 1:23

[anagennaō](../../strongs/g/g313.md), not of [phthartos](../../strongs/g/g5349.md) [spora](../../strongs/g/g4701.md), but of [aphthartos](../../strongs/g/g862.md), by the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), which [zaō](../../strongs/g/g2198.md) and [menō](../../strongs/g/g3306.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md).

<a name="1peter_1_24"></a>1 Peter 1:24

For all [sarx](../../strongs/g/g4561.md) as [chortos](../../strongs/g/g5528.md), and all the [doxa](../../strongs/g/g1391.md) of [anthrōpos](../../strongs/g/g444.md) as the [anthos](../../strongs/g/g438.md) of [chortos](../../strongs/g/g5528.md). The [chortos](../../strongs/g/g5528.md) [xērainō](../../strongs/g/g3583.md), and the [anthos](../../strongs/g/g438.md) thereof [ekpiptō](../../strongs/g/g1601.md):

<a name="1peter_1_25"></a>1 Peter 1:25

But the [rhēma](../../strongs/g/g4487.md) of the [kyrios](../../strongs/g/g2962.md) [menō](../../strongs/g/g3306.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). And this is the [rhēma](../../strongs/g/g4487.md) which [euaggelizō](../../strongs/g/g2097.md) unto you.

---

[Transliteral Bible](../bible.md)

[1 Peter](1peter.md)

[1 Peter 2](1peter_2.md)

---

[^1]: [1 Peter 1:16 Commentary](../../commentary/1peter/1peter_1_commentary.md#1peter_1_16)
