# [1 Peter 5](https://www.blueletterbible.org/kjv/1pe/5/1/s_1156001)

<a name="1peter_5_1"></a>1 Peter 5:1

The [presbyteros](../../strongs/g/g4245.md) which are among you I [parakaleō](../../strongs/g/g3870.md), who am [sympresbyteros](../../strongs/g/g4850.md), and a [martys](../../strongs/g/g3144.md) of the [pathēma](../../strongs/g/g3804.md) of [Christos](../../strongs/g/g5547.md), and also a [koinōnos](../../strongs/g/g2844.md) of the [doxa](../../strongs/g/g1391.md) that shall be [apokalyptō](../../strongs/g/g601.md):

<a name="1peter_5_2"></a>1 Peter 5:2

[poimainō](../../strongs/g/g4165.md) the [poimnion](../../strongs/g/g4168.md) of [theos](../../strongs/g/g2316.md) which is among you, [episkopeō](../../strongs/g/g1983.md), not by [anagkastōs](../../strongs/g/g317.md), but [hekousiōs](../../strongs/g/g1596.md); not for [aischrokerdōs](../../strongs/g/g147.md), but [prothymōs](../../strongs/g/g4290.md);

<a name="1peter_5_3"></a>1 Peter 5:3

Neither [katakyrieuō](../../strongs/g/g2634.md) over [klēros](../../strongs/g/g2819.md), but being [typos](../../strongs/g/g5179.md) to the [poimnion](../../strongs/g/g4168.md).

<a name="1peter_5_4"></a>1 Peter 5:4

And when the [archipoimēn](../../strongs/g/g750.md) shall [phaneroō](../../strongs/g/g5319.md), ye shall [komizō](../../strongs/g/g2865.md) a [stephanos](../../strongs/g/g4735.md) of [doxa](../../strongs/g/g1391.md) that [amarantinos](../../strongs/g/g262.md).

<a name="1peter_5_5"></a>1 Peter 5:5

[homoiōs](../../strongs/g/g3668.md), ye [neos](../../strongs/g/g3501.md), [hypotassō](../../strongs/g/g5293.md) unto the [presbyteros](../../strongs/g/g4245.md). [de](../../strongs/g/g1161.md), [pas](../../strongs/g/g3956.md) [hypotassō](../../strongs/g/g5293.md) to [allēlōn](../../strongs/g/g240.md), and be [egkomboomai](../../strongs/g/g1463.md) with [tapeinophrosynē](../../strongs/g/g5012.md): for [theos](../../strongs/g/g2316.md) [antitassō](../../strongs/g/g498.md) the [hyperēphanos](../../strongs/g/g5244.md), and [didōmi](../../strongs/g/g1325.md) [charis](../../strongs/g/g5485.md) to the [tapeinos](../../strongs/g/g5011.md).

<a name="1peter_5_6"></a>1 Peter 5:6

[tapeinoō](../../strongs/g/g5013.md) yourselves therefore under the [krataios](../../strongs/g/g2900.md) [cheir](../../strongs/g/g5495.md) of [theos](../../strongs/g/g2316.md), that he may [hypsoō](../../strongs/g/g5312.md) you in [kairos](../../strongs/g/g2540.md):

<a name="1peter_5_7"></a>1 Peter 5:7

[epiriptō](../../strongs/g/g1977.md) all your [merimna](../../strongs/g/g3308.md) upon him; for he [melei](../../strongs/g/g3199.md) for you.

<a name="1peter_5_8"></a>1 Peter 5:8

Be [nēphō](../../strongs/g/g3525.md), be [grēgoreō](../../strongs/g/g1127.md); because your [antidikos](../../strongs/g/g476.md) the [diabolos](../../strongs/g/g1228.md), as an [ōryomai](../../strongs/g/g5612.md) [leōn](../../strongs/g/g3023.md), [peripateō](../../strongs/g/g4043.md), [zēteō](../../strongs/g/g2212.md) whom he may [katapinō](../../strongs/g/g2666.md):

<a name="1peter_5_9"></a>1 Peter 5:9

Whom [anthistēmi](../../strongs/g/g436.md) [stereos](../../strongs/g/g4731.md) in the [pistis](../../strongs/g/g4102.md), [eidō](../../strongs/g/g1492.md) that the same [pathēma](../../strongs/g/g3804.md) are [epiteleō](../../strongs/g/g2005.md) in your [adelphotēs](../../strongs/g/g81.md) that are in the [kosmos](../../strongs/g/g2889.md).

<a name="1peter_5_10"></a>1 Peter 5:10

But the [theos](../../strongs/g/g2316.md) of all [charis](../../strongs/g/g5485.md), who hath [kaleō](../../strongs/g/g2564.md) us unto his [aiōnios](../../strongs/g/g166.md) [doxa](../../strongs/g/g1391.md) by [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), after that ye have [paschō](../../strongs/g/g3958.md) [oligos](../../strongs/g/g3641.md), make you [katartizō](../../strongs/g/g2675.md), [stērizō](../../strongs/g/g4741.md), [sthenoō](../../strongs/g/g4599.md), [themelioō](../../strongs/g/g2311.md) you.

<a name="1peter_5_11"></a>1 Peter 5:11

To him be [doxa](../../strongs/g/g1391.md) and [kratos](../../strongs/g/g2904.md) for [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="1peter_5_12"></a>1 Peter 5:12

By [silouanos](../../strongs/g/g4610.md), a [pistos](../../strongs/g/g4103.md) [adelphos](../../strongs/g/g80.md) unto you, as I [logizomai](../../strongs/g/g3049.md), I have [graphō](../../strongs/g/g1125.md) [dia](../../strongs/g/g1223.md) [oligos](../../strongs/g/g3641.md), [parakaleō](../../strongs/g/g3870.md), and [epimartyreō](../../strongs/g/g1957.md) that this is the [alēthēs](../../strongs/g/g227.md) [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) wherein ye [histēmi](../../strongs/g/g2476.md).

<a name="1peter_5_13"></a>1 Peter 5:13

at [Babylōn](../../strongs/g/g897.md), [syneklektos](../../strongs/g/g4899.md), [aspazomai](../../strongs/g/g782.md) you; and [Markos](../../strongs/g/g3138.md) my [huios](../../strongs/g/g5207.md).

<a name="1peter_5_14"></a>1 Peter 5:14

[aspazomai](../../strongs/g/g782.md) ye [allēlōn](../../strongs/g/g240.md) with a [philēma](../../strongs/g/g5370.md) of [agapē](../../strongs/g/g26.md). [eirēnē](../../strongs/g/g1515.md) with you all that are in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[1 Peter](1peter.md)

[1 Peter 4](1peter_4.md)