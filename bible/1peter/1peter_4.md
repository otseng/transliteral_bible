# [1 Peter 4](https://www.blueletterbible.org/kjv/1pe/4/1/s_1155001)

<a name="1peter_4_1"></a>1 Peter 4:1

Forasmuch then as [Christos](../../strongs/g/g5547.md) hath [paschō](../../strongs/g/g3958.md) for us in the [sarx](../../strongs/g/g4561.md), [hoplizō](../../strongs/g/g3695.md) yourselves likewise with the same [ennoia](../../strongs/g/g1771.md): for he that hath [paschō](../../strongs/g/g3958.md) in the [sarx](../../strongs/g/g4561.md) hath [pauō](../../strongs/g/g3973.md) from [hamartia](../../strongs/g/g266.md);

<a name="1peter_4_2"></a>1 Peter 4:2

That he no longer should [bioō](../../strongs/g/g980.md) the [epiloipos](../../strongs/g/g1954.md) of [chronos](../../strongs/g/g5550.md) in the [sarx](../../strongs/g/g4561.md) to the [epithymia](../../strongs/g/g1939.md) of [anthrōpos](../../strongs/g/g444.md), but to the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md).

<a name="1peter_4_3"></a>1 Peter 4:3

For the [chronos](../../strongs/g/g5550.md) [parerchomai](../../strongs/g/g3928.md) of [bios](../../strongs/g/g979.md) may [arketos](../../strongs/g/g713.md) us to have [katergazomai](../../strongs/g/g2716.md) the [thelēma](../../strongs/g/g2307.md) of the [ethnos](../../strongs/g/g1484.md), when we [poreuō](../../strongs/g/g4198.md) in [aselgeia](../../strongs/g/g766.md), [epithymia](../../strongs/g/g1939.md), [oinophlygia](../../strongs/g/g3632.md), [kōmos](../../strongs/g/g2970.md), [potos](../../strongs/g/g4224.md), and [athemitos](../../strongs/g/g111.md) [eidōlolatria](../../strongs/g/g1495.md):

<a name="1peter_4_4"></a>1 Peter 4:4

Wherein they [xenizō](../../strongs/g/g3579.md) that ye [syntrechō](../../strongs/g/g4936.md) not with to the same [anachysis](../../strongs/g/g401.md) of [asōtia](../../strongs/g/g810.md), [blasphēmeō](../../strongs/g/g987.md):

<a name="1peter_4_5"></a>1 Peter 4:5

Who shall [apodidōmi](../../strongs/g/g591.md) [logos](../../strongs/g/g3056.md) to him that is [hetoimōs](../../strongs/g/g2093.md) to [krinō](../../strongs/g/g2919.md) the [zaō](../../strongs/g/g2198.md) and the [nekros](../../strongs/g/g3498.md).

<a name="1peter_4_6"></a>1 Peter 4:6

For for this cause was [euaggelizō](../../strongs/g/g2097.md) also to them that are [nekros](../../strongs/g/g3498.md), that they might be [krinō](../../strongs/g/g2919.md) according to [anthrōpos](../../strongs/g/g444.md) in the [sarx](../../strongs/g/g4561.md), but [zaō](../../strongs/g/g2198.md) according to [theos](../../strongs/g/g2316.md) in the [pneuma](../../strongs/g/g4151.md).

<a name="1peter_4_7"></a>1 Peter 4:7

But the [telos](../../strongs/g/g5056.md) of all things is [eggizō](../../strongs/g/g1448.md): be ye therefore [sōphroneō](../../strongs/g/g4993.md), and [nēphō](../../strongs/g/g3525.md) unto [proseuchē](../../strongs/g/g4335.md).

<a name="1peter_4_8"></a>1 Peter 4:8

And above all things have [ektenēs](../../strongs/g/g1618.md) [agapē](../../strongs/g/g26.md) among yourselves: for [agapē](../../strongs/g/g26.md) shall [kalyptō](../../strongs/g/g2572.md) the [plēthos](../../strongs/g/g4128.md) of [hamartia](../../strongs/g/g266.md).

<a name="1peter_4_9"></a>1 Peter 4:9

Use [philoxenos](../../strongs/g/g5382.md) to [allēlōn](../../strongs/g/g240.md) without [goggysmos](../../strongs/g/g1112.md).

<a name="1peter_4_10"></a>1 Peter 4:10

As [hekastos](../../strongs/g/g1538.md) hath [lambanō](../../strongs/g/g2983.md) the [charisma](../../strongs/g/g5486.md), [diakoneō](../../strongs/g/g1247.md) the same one to another, as [kalos](../../strongs/g/g2570.md) [oikonomos](../../strongs/g/g3623.md) of the [poikilos](../../strongs/g/g4164.md) [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md).

<a name="1peter_4_11"></a>1 Peter 4:11

[ei tis](../../strongs/g/g1536.md) [laleō](../../strongs/g/g2980.md), as the [logion](../../strongs/g/g3051.md) of [theos](../../strongs/g/g2316.md); [ei tis](../../strongs/g/g1536.md) [diakoneō](../../strongs/g/g1247.md), it as of the [ischys](../../strongs/g/g2479.md) which [theos](../../strongs/g/g2316.md) [chorēgeō](../../strongs/g/g5524.md): that [theos](../../strongs/g/g2316.md) in all things may be [doxazō](../../strongs/g/g1392.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), to whom be [doxa](../../strongs/g/g1391.md) and [kratos](../../strongs/g/g2904.md) for [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="1peter_4_12"></a>1 Peter 4:12

[agapētos](../../strongs/g/g27.md), think it not [xenizō](../../strongs/g/g3579.md) concerning the [pyrōsis](../../strongs/g/g4451.md) which is to [peirasmos](../../strongs/g/g3986.md) you, as though some [xenos](../../strongs/g/g3581.md) [symbainō](../../strongs/g/g4819.md) unto you:

<a name="1peter_4_13"></a>1 Peter 4:13

But [chairō](../../strongs/g/g5463.md), inasmuch as ye are [koinōneō](../../strongs/g/g2841.md) of [Christos](../../strongs/g/g5547.md) [pathēma](../../strongs/g/g3804.md); that, when his [doxa](../../strongs/g/g1391.md) shall be [apokalypsis](../../strongs/g/g602.md), ye may be [chairō](../../strongs/g/g5463.md) also with [agalliaō](../../strongs/g/g21.md).

<a name="1peter_4_14"></a>1 Peter 4:14

If ye be [oneidizō](../../strongs/g/g3679.md) for the [onoma](../../strongs/g/g3686.md) of [Christos](../../strongs/g/g5547.md), [makarios](../../strongs/g/g3107.md); for the [pneuma](../../strongs/g/g4151.md) of [doxa](../../strongs/g/g1391.md) and of [theos](../../strongs/g/g2316.md) [anapauō](../../strongs/g/g373.md) upon you: on their part he is [blasphēmeō](../../strongs/g/g987.md), but on your part he is [doxazō](../../strongs/g/g1392.md). [^1]

<a name="1peter_4_15"></a>1 Peter 4:15

But let none of you [paschō](../../strongs/g/g3958.md) as a [phoneus](../../strongs/g/g5406.md), or as a [kleptēs](../../strongs/g/g2812.md), or as a [kakopoios](../../strongs/g/g2555.md), or as an [allotriepiskopos](../../strongs/g/g244.md).

<a name="1peter_4_16"></a>1 Peter 4:16

Yet if as a [Christianos](../../strongs/g/g5546.md), let him not be [aischynō](../../strongs/g/g153.md); but let him [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) on this [meros](../../strongs/g/g3313.md).

<a name="1peter_4_17"></a>1 Peter 4:17

For the [kairos](../../strongs/g/g2540.md) that [krima](../../strongs/g/g2917.md) must [archomai](../../strongs/g/g756.md) at the [oikos](../../strongs/g/g3624.md) of [theos](../../strongs/g/g2316.md): and if [prōton](../../strongs/g/g4412.md) at us, what shall the [telos](../../strongs/g/g5056.md) of them that [apeitheō](../../strongs/g/g544.md) not the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md)?

<a name="1peter_4_18"></a>1 Peter 4:18

And if the [dikaios](../../strongs/g/g1342.md) [molis](../../strongs/g/g3433.md) be [sōzō](../../strongs/g/g4982.md), where shall the [asebēs](../../strongs/g/g765.md) and the [hamartōlos](../../strongs/g/g268.md) [phainō](../../strongs/g/g5316.md)?

<a name="1peter_4_19"></a>1 Peter 4:19

Wherefore let them that [paschō](../../strongs/g/g3958.md) according to the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) [paratithēmi](../../strongs/g/g3908.md) of their [psychē](../../strongs/g/g5590.md) in [agathopoiia](../../strongs/g/g16.md), as unto a [pistos](../../strongs/g/g4103.md) [ktistēs](../../strongs/g/g2939.md).

---

[Transliteral Bible](../bible.md)

[1 Peter](1peter.md)

[1 Peter 3](1peter_3.md) - [1 Peter 5](1peter_5.md)

---

[^1]: [1 Peter 4:14 Commentary](../../commentary/1peter/1peter_4_commentary.md#1peter_4_14)
