# [Romans 12](https://www.blueletterbible.org/kjv/rom/12/1/rl1/s_1050001)

<a name="romans_12_1"></a>Romans 12:1

I [parakaleō](../../strongs/g/g3870.md) you therefore, [adelphos](../../strongs/g/g80.md), by the [oiktirmos](../../strongs/g/g3628.md) of [theos](../../strongs/g/g2316.md), that ye [paristēmi](../../strongs/g/g3936.md) your [sōma](../../strongs/g/g4983.md) a [zaō](../../strongs/g/g2198.md) [thysia](../../strongs/g/g2378.md), [hagios](../../strongs/g/g40.md), [euarestos](../../strongs/g/g2101.md) unto [theos](../../strongs/g/g2316.md), which is your [logikos](../../strongs/g/g3050.md) [latreia](../../strongs/g/g2999.md).

<a name="romans_12_2"></a>Romans 12:2

And be not [syschēmatizō](../../strongs/g/g4964.md) to this [aiōn](../../strongs/g/g165.md): but be ye [metamorphoō](../../strongs/g/g3339.md) by the [anakainōsis](../../strongs/g/g342.md) of your [nous](../../strongs/g/g3563.md), that ye may [dokimazō](../../strongs/g/g1381.md) what is that [agathos](../../strongs/g/g18.md), and [euarestos](../../strongs/g/g2101.md), and [teleios](../../strongs/g/g5046.md), [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_12_3"></a>Romans 12:3

For I [legō](../../strongs/g/g3004.md), through the [charis](../../strongs/g/g5485.md) [didōmi](../../strongs/g/g1325.md) unto me, to every man that is among you, not to [hyperphroneō](../../strongs/g/g5252.md) than he ought to [phroneō](../../strongs/g/g5426.md); but to [phroneō](../../strongs/g/g5426.md) [sōphroneō](../../strongs/g/g4993.md), according as [theos](../../strongs/g/g2316.md) hath [merizō](../../strongs/g/g3307.md) to every man the [metron](../../strongs/g/g3358.md) of [pistis](../../strongs/g/g4102.md).

<a name="romans_12_4"></a>Romans 12:4

For as we have [polys](../../strongs/g/g4183.md) [melos](../../strongs/g/g3196.md) in one [sōma](../../strongs/g/g4983.md), and all [melos](../../strongs/g/g3196.md) have not the same [praxis](../../strongs/g/g4234.md):

<a name="romans_12_5"></a>Romans 12:5

So we, [polys](../../strongs/g/g4183.md), are one [sōma](../../strongs/g/g4983.md) in [Christos](../../strongs/g/g5547.md), and every one [melos](../../strongs/g/g3196.md) of [allēlōn](../../strongs/g/g240.md).

<a name="romans_12_6"></a>Romans 12:6

Having then [charisma](../../strongs/g/g5486.md) [diaphoros](../../strongs/g/g1313.md) according to the [charis](../../strongs/g/g5485.md) that is [didōmi](../../strongs/g/g1325.md) to us, whether [prophēteia](../../strongs/g/g4394.md), according to the [analogia](../../strongs/g/g356.md) of [pistis](../../strongs/g/g4102.md);

<a name="romans_12_7"></a>Romans 12:7

Or [diakonia](../../strongs/g/g1248.md), on [diakonia](../../strongs/g/g1248.md): or he that [didaskō](../../strongs/g/g1321.md), on [didaskalia](../../strongs/g/g1319.md);

<a name="romans_12_8"></a>Romans 12:8

Or he that [parakaleō](../../strongs/g/g3870.md), on [paraklēsis](../../strongs/g/g3874.md): he that [metadidōmi](../../strongs/g/g3330.md), let him do it with [haplotēs](../../strongs/g/g572.md); he that [proistēmi](../../strongs/g/g4291.md), with [spoudē](../../strongs/g/g4710.md); he that [eleeō](../../strongs/g/g1653.md), with [hilarotēs](../../strongs/g/g2432.md).

<a name="romans_12_9"></a>Romans 12:9

[agapē](../../strongs/g/g26.md) be [anypokritos](../../strongs/g/g505.md). [apostygeō](../../strongs/g/g655.md) that which is [ponēros](../../strongs/g/g4190.md); [kollaō](../../strongs/g/g2853.md) to that which is [agathos](../../strongs/g/g18.md).

<a name="romans_12_10"></a>Romans 12:10

[philostorgos](../../strongs/g/g5387.md) [allēlōn](../../strongs/g/g240.md) with [philadelphia](../../strongs/g/g5360.md); in [timē](../../strongs/g/g5092.md) [proēgeomai](../../strongs/g/g4285.md) [allēlōn](../../strongs/g/g240.md);

<a name="romans_12_11"></a>Romans 12:11

Not [oknēros](../../strongs/g/g3636.md) in [spoudē](../../strongs/g/g4710.md); [zeō](../../strongs/g/g2204.md) in [pneuma](../../strongs/g/g4151.md); [douleuō](../../strongs/g/g1398.md) the [kyrios](../../strongs/g/g2962.md);

<a name="romans_12_12"></a>Romans 12:12

[chairō](../../strongs/g/g5463.md) in [elpis](../../strongs/g/g1680.md); [hypomenō](../../strongs/g/g5278.md) in [thlipsis](../../strongs/g/g2347.md); [proskartereō](../../strongs/g/g4342.md) in [proseuchē](../../strongs/g/g4335.md);

<a name="romans_12_13"></a>Romans 12:13

[koinōneō](../../strongs/g/g2841.md) to the [chreia](../../strongs/g/g5532.md) of [hagios](../../strongs/g/g40.md); [diōkō](../../strongs/g/g1377.md) to [philoxenia](../../strongs/g/g5381.md).

<a name="romans_12_14"></a>Romans 12:14

[eulogeō](../../strongs/g/g2127.md) them which [diōkō](../../strongs/g/g1377.md) you: [eulogeō](../../strongs/g/g2127.md), and [kataraomai](../../strongs/g/g2672.md) not.

<a name="romans_12_15"></a>Romans 12:15

[chairō](../../strongs/g/g5463.md) with them that do [chairō](../../strongs/g/g5463.md), and [klaiō](../../strongs/g/g2799.md) with them that [klaiō](../../strongs/g/g2799.md).

<a name="romans_12_16"></a>Romans 12:16

Be of the same [phroneō](../../strongs/g/g5426.md) [allēlōn](../../strongs/g/g240.md). [phroneō](../../strongs/g/g5426.md) not [hypsēlos](../../strongs/g/g5308.md), but [synapagō](../../strongs/g/g4879.md) to [tapeinos](../../strongs/g/g5011.md). Be not [phronimos](../../strongs/g/g5429.md) in [heautou](../../strongs/g/g1438.md).

<a name="romans_12_17"></a>Romans 12:17

[apodidōmi](../../strongs/g/g591.md) to [mēdeis](../../strongs/g/g3367.md) [kakos](../../strongs/g/g2556.md) for [kakos](../../strongs/g/g2556.md). [pronoeō](../../strongs/g/g4306.md) [kalos](../../strongs/g/g2570.md) in the [enōpion](../../strongs/g/g1799.md) of all [anthrōpos](../../strongs/g/g444.md).

<a name="romans_12_18"></a>Romans 12:18

If it [dynatos](../../strongs/g/g1415.md), as much as lieth in you, [eirēneuō](../../strongs/g/g1514.md) with all [anthrōpos](../../strongs/g/g444.md).

<a name="romans_12_19"></a>Romans 12:19

[agapētos](../../strongs/g/g27.md), [ekdikeō](../../strongs/g/g1556.md) not yourselves, but rather [didōmi](../../strongs/g/g1325.md) [topos](../../strongs/g/g5117.md) unto [orgē](../../strongs/g/g3709.md): for it is [graphō](../../strongs/g/g1125.md), [ekdikēsis](../../strongs/g/g1557.md) is mine; I will [antapodidōmi](../../strongs/g/g467.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md).

<a name="romans_12_20"></a>Romans 12:20

Therefore if thine [echthros](../../strongs/g/g2190.md) [peinaō](../../strongs/g/g3983.md), [psōmizō](../../strongs/g/g5595.md) him; if he [dipsaō](../../strongs/g/g1372.md), [potizō](../../strongs/g/g4222.md) him: for in so [poieō](../../strongs/g/g4160.md) thou shalt [sōreuō](../../strongs/g/g4987.md) [anthrax](../../strongs/g/g440.md) of [pyr](../../strongs/g/g4442.md) on his [kephalē](../../strongs/g/g2776.md).

<a name="romans_12_21"></a>Romans 12:21

Be not [nikaō](../../strongs/g/g3528.md) of [kakos](../../strongs/g/g2556.md), but [nikaō](../../strongs/g/g3528.md) [kakos](../../strongs/g/g2556.md) with [agathos](../../strongs/g/g18.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 11](romans_11.md) - [Romans 13](romans_13.md)
