# [Romans 10](https://www.blueletterbible.org/kjv/rom/10/1/s_1056001)

<a name="romans_10_1"></a>Romans 10:1

[adelphos](../../strongs/g/g80.md), my [kardia](../../strongs/g/g2588.md) [eudokia](../../strongs/g/g2107.md) and [deēsis](../../strongs/g/g1162.md) to [theos](../../strongs/g/g2316.md) for [Israēl](../../strongs/g/g2474.md) is, that they might be [sōtēria](../../strongs/g/g4991.md).

<a name="romans_10_2"></a>Romans 10:2

For I [martyreō](../../strongs/g/g3140.md) them that they have a [zēlos](../../strongs/g/g2205.md) of [theos](../../strongs/g/g2316.md), but not according to [epignōsis](../../strongs/g/g1922.md).

<a name="romans_10_3"></a>Romans 10:3

For they being [agnoeō](../../strongs/g/g50.md) of [theos](../../strongs/g/g2316.md) [dikaiosynē](../../strongs/g/g1343.md), and [zēteō](../../strongs/g/g2212.md) to [histēmi](../../strongs/g/g2476.md) their own [dikaiosynē](../../strongs/g/g1343.md), have not [hypotassō](../../strongs/g/g5293.md) themselves unto the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_10_4"></a>Romans 10:4

For [Christos](../../strongs/g/g5547.md) the [telos](../../strongs/g/g5056.md) of the [nomos](../../strongs/g/g3551.md) for [dikaiosynē](../../strongs/g/g1343.md) to every one that [pisteuō](../../strongs/g/g4100.md).

<a name="romans_10_5"></a>Romans 10:5

For [Mōÿsēs](../../strongs/g/g3475.md) [graphō](../../strongs/g/g1125.md) the [dikaiosynē](../../strongs/g/g1343.md) which is of the [nomos](../../strongs/g/g3551.md), That the [anthrōpos](../../strongs/g/g444.md) which [poieō](../../strongs/g/g4160.md) those things shall [zaō](../../strongs/g/g2198.md) by them.

<a name="romans_10_6"></a>Romans 10:6

But the [dikaiosynē](../../strongs/g/g1343.md) which is of [pistis](../../strongs/g/g4102.md) [legō](../../strongs/g/g3004.md) [houtō(s)](../../strongs/g/g3779.md), [eipon](../../strongs/g/g2036.md) not in thine [kardia](../../strongs/g/g2588.md), Who shall [anabainō](../../strongs/g/g305.md) into [ouranos](../../strongs/g/g3772.md)? (that is, to [katagō](../../strongs/g/g2609.md) [Christos](../../strongs/g/g5547.md) from above:)

<a name="romans_10_7"></a>Romans 10:7

Or, Who shall [katabainō](../../strongs/g/g2597.md) into the [abyssos](../../strongs/g/g12.md)? (that is, to [anagō](../../strongs/g/g321.md) [Christos](../../strongs/g/g5547.md) again from the [nekros](../../strongs/g/g3498.md).)

<a name="romans_10_8"></a>Romans 10:8

But what [legō](../../strongs/g/g3004.md) it? The [rhēma](../../strongs/g/g4487.md) is [eggys](../../strongs/g/g1451.md) thee, even in thy [stoma](../../strongs/g/g4750.md), and in thy [kardia](../../strongs/g/g2588.md): that is, the [rhēma](../../strongs/g/g4487.md) of [pistis](../../strongs/g/g4102.md), which we [kēryssō](../../strongs/g/g2784.md);

<a name="romans_10_9"></a>Romans 10:9

That if thou shalt [homologeō](../../strongs/g/g3670.md) with thy [stoma](../../strongs/g/g4750.md) the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and shalt [pisteuō](../../strongs/g/g4100.md) in thine [kardia](../../strongs/g/g2588.md) that [theos](../../strongs/g/g2316.md) hath [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md), thou shalt be [sōzō](../../strongs/g/g4982.md).

<a name="romans_10_10"></a>Romans 10:10

For with the [kardia](../../strongs/g/g2588.md) [pisteuō](../../strongs/g/g4100.md) unto [dikaiosynē](../../strongs/g/g1343.md); and with the [stoma](../../strongs/g/g4750.md) [homologeō](../../strongs/g/g3670.md) unto [sōtēria](../../strongs/g/g4991.md).

<a name="romans_10_11"></a>Romans 10:11

For the [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md), Whosoever [pisteuō](../../strongs/g/g4100.md) on him shall not be [kataischynō](../../strongs/g/g2617.md).

<a name="romans_10_12"></a>Romans 10:12

For there is no [diastolē](../../strongs/g/g1293.md) between the [Ioudaios](../../strongs/g/g2453.md) and the [Hellēn](../../strongs/g/g1672.md): for the same [kyrios](../../strongs/g/g2962.md) over all is [plouteō](../../strongs/g/g4147.md) unto all that [epikaleō](../../strongs/g/g1941.md) him.

<a name="romans_10_13"></a>Romans 10:13

For whosoever shall [epikaleō](../../strongs/g/g1941.md) upon the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) shall be [sōzō](../../strongs/g/g4982.md).

<a name="romans_10_14"></a>Romans 10:14

How then shall they [epikaleō](../../strongs/g/g1941.md) him in whom they have not [pisteuō](../../strongs/g/g4100.md)? and how shall they [pisteuō](../../strongs/g/g4100.md) in him of whom they have not [akouō](../../strongs/g/g191.md)? and how shall they [akouō](../../strongs/g/g191.md) without a [kēryssō](../../strongs/g/g2784.md)?

<a name="romans_10_15"></a>Romans 10:15

And how shall they [kēryssō](../../strongs/g/g2784.md), except they be [apostellō](../../strongs/g/g649.md)? as it is [graphō](../../strongs/g/g1125.md), How [hōraios](../../strongs/g/g5611.md) are the [pous](../../strongs/g/g4228.md) of them that [euaggelizō](../../strongs/g/g2097.md) [eirēnē](../../strongs/g/g1515.md), and [euaggelizō](../../strongs/g/g2097.md) [agathos](../../strongs/g/g18.md)! [^1]

<a name="romans_10_16"></a>Romans 10:16

But they have not all [hypakouō](../../strongs/g/g5219.md) the [euaggelion](../../strongs/g/g2098.md). For [Ēsaïas](../../strongs/g/g2268.md) [legō](../../strongs/g/g3004.md), [kyrios](../../strongs/g/g2962.md), who hath [pisteuō](../../strongs/g/g4100.md) our [akoē](../../strongs/g/g189.md)?

<a name="romans_10_17"></a>Romans 10:17

So then [pistis](../../strongs/g/g4102.md) by [akoē](../../strongs/g/g189.md), and [akoē](../../strongs/g/g189.md) by the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_10_18"></a>Romans 10:18

But I [legō](../../strongs/g/g3004.md), Have they not [akouō](../../strongs/g/g191.md)? [menoun](../../strongs/g/g3304.md), their [phthongos](../../strongs/g/g5353.md) [exerchomai](../../strongs/g/g1831.md) into all [gē](../../strongs/g/g1093.md), and their [rhēma](../../strongs/g/g4487.md) unto the [peras](../../strongs/g/g4009.md) of [oikoumenē](../../strongs/g/g3625.md).

<a name="romans_10_19"></a>Romans 10:19

But I [legō](../../strongs/g/g3004.md), Did not [Israēl](../../strongs/g/g2474.md) [ginōskō](../../strongs/g/g1097.md)? First [Mōÿsēs](../../strongs/g/g3475.md) [legō](../../strongs/g/g3004.md), I will [parazēloō](../../strongs/g/g3863.md) you by them that are no [ethnos](../../strongs/g/g1484.md), and by an [asynetos](../../strongs/g/g801.md) [ethnos](../../strongs/g/g1484.md) I will [parorgizō](../../strongs/g/g3949.md) you.

<a name="romans_10_20"></a>Romans 10:20

But [Ēsaïas](../../strongs/g/g2268.md) is [apotolmaō](../../strongs/g/g662.md), and [legō](../../strongs/g/g3004.md), I was [heuriskō](../../strongs/g/g2147.md) of them that [zēteō](../../strongs/g/g2212.md) me not; I was [ginomai](../../strongs/g/g1096.md) [emphanēs](../../strongs/g/g1717.md) unto them that [eperōtaō](../../strongs/g/g1905.md) not after me.

<a name="romans_10_21"></a>Romans 10:21

But to [Israēl](../../strongs/g/g2474.md) he [legō](../../strongs/g/g3004.md), All [hēmera](../../strongs/g/g2250.md) I have [ekpetannymi](../../strongs/g/g1600.md) my [cheir](../../strongs/g/g5495.md) unto an [apeitheō](../../strongs/g/g544.md) and [antilegō](../../strongs/g/g483.md) [laos](../../strongs/g/g2992.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 9](romans_9.md) - [Romans 11](romans_11.md)

---

[^1]: [Romans 10:15 Commentary](../../commentary/romans/romans_10_commentary.md#romans_10_15)
