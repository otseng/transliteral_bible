# [Romans 11](https://www.blueletterbible.org/kjv/rom/11/1/s_1057001)

<a name="romans_11_1"></a>Romans 11:1

I [legō](../../strongs/g/g3004.md) then, Hath [theos](../../strongs/g/g2316.md) [apōtheō](../../strongs/g/g683.md) his [laos](../../strongs/g/g2992.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md). For I also am an [Israēlitēs](../../strongs/g/g2475.md), of the [sperma](../../strongs/g/g4690.md) of [Abraam](../../strongs/g/g11.md), of the [phylē](../../strongs/g/g5443.md) of [Beniam(e)in](../../strongs/g/g958.md).

<a name="romans_11_2"></a>Romans 11:2

[theos](../../strongs/g/g2316.md) hath not [apōtheō](../../strongs/g/g683.md) his [laos](../../strongs/g/g2992.md) which he [proginōskō](../../strongs/g/g4267.md). [eidō](../../strongs/g/g1492.md) ye not what the [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md) of [Ēlias](../../strongs/g/g2243.md)? how he [entygchanō](../../strongs/g/g1793.md) to [theos](../../strongs/g/g2316.md) against [Israēl](../../strongs/g/g2474.md) [legō](../../strongs/g/g3004.md),

<a name="romans_11_3"></a>Romans 11:3

[kyrios](../../strongs/g/g2962.md), they have [apokteinō](../../strongs/g/g615.md) thy [prophētēs](../../strongs/g/g4396.md), and [kataskaptō](../../strongs/g/g2679.md) thine [thysiastērion](../../strongs/g/g2379.md); and I am [hypoleipō](../../strongs/g/g5275.md) [monos](../../strongs/g/g3441.md), and they [zēteō](../../strongs/g/g2212.md) my [psychē](../../strongs/g/g5590.md).

<a name="romans_11_4"></a>Romans 11:4

But what [legō](../../strongs/g/g3004.md) the [chrēmatismos](../../strongs/g/g5538.md) unto him? I have [kataleipō](../../strongs/g/g2641.md) to myself seven thousand [anēr](../../strongs/g/g435.md), who have not [kamptō](../../strongs/g/g2578.md) the [gony](../../strongs/g/g1119.md) to [baal](../../strongs/g/g896.md).

<a name="romans_11_5"></a>Romans 11:5

Even so then at this present [kairos](../../strongs/g/g2540.md) also there is a [leimma](../../strongs/g/g3005.md) according to the [eklogē](../../strongs/g/g1589.md) of [charis](../../strongs/g/g5485.md).

<a name="romans_11_6"></a>Romans 11:6

And if by [charis](../../strongs/g/g5485.md), then no more of [ergon](../../strongs/g/g2041.md): otherwise [charis](../../strongs/g/g5485.md) is no more [charis](../../strongs/g/g5485.md). But if it be of [ergon](../../strongs/g/g2041.md), then it is no more [charis](../../strongs/g/g5485.md): otherwise [ergon](../../strongs/g/g2041.md) is no more [ergon](../../strongs/g/g2041.md). [^1]

<a name="romans_11_7"></a>Romans 11:7

What then? [Israēl](../../strongs/g/g2474.md) hath not [epitygchanō](../../strongs/g/g2013.md) that which he [epizēteō](../../strongs/g/g1934.md); but the [eklogē](../../strongs/g/g1589.md) hath [epitygchanō](../../strongs/g/g2013.md) it, and the [loipos](../../strongs/g/g3062.md) were [pōroō](../../strongs/g/g4456.md).

<a name="romans_11_8"></a>Romans 11:8

(According as it is [graphō](../../strongs/g/g1125.md), [theos](../../strongs/g/g2316.md) hath [didōmi](../../strongs/g/g1325.md) them the [pneuma](../../strongs/g/g4151.md) of [katanyxis](../../strongs/g/g2659.md), [ophthalmos](../../strongs/g/g3788.md) that they should not [blepō](../../strongs/g/g991.md), and [ous](../../strongs/g/g3775.md) that they should not [akouō](../../strongs/g/g191.md);) unto [sēmeron](../../strongs/g/g4594.md) [hēmera](../../strongs/g/g2250.md).

<a name="romans_11_9"></a>Romans 11:9

And [Dabid](../../strongs/g/g1138.md) [legō](../../strongs/g/g3004.md), Let their [trapeza](../../strongs/g/g5132.md) be made [eis](../../strongs/g/g1519.md) [pagis](../../strongs/g/g3803.md), and [eis](../../strongs/g/g1519.md) [thēra](../../strongs/g/g2339.md), and [eis](../../strongs/g/g1519.md) [skandalon](../../strongs/g/g4625.md), and [eis](../../strongs/g/g1519.md) [antapodoma](../../strongs/g/g468.md) unto them:

<a name="romans_11_10"></a>Romans 11:10

Let their [ophthalmos](../../strongs/g/g3788.md) be [skotizō](../../strongs/g/g4654.md), that they may not [blepō](../../strongs/g/g991.md), and [sygkamptō](../../strongs/g/g4781.md) their [nōtos](../../strongs/g/g3577.md) [diapantos](../../strongs/g/g1275.md).

<a name="romans_11_11"></a>Romans 11:11

I [legō](../../strongs/g/g3004.md) then, Have they [ptaiō](../../strongs/g/g4417.md) that they should [piptō](../../strongs/g/g4098.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md): but rather through their [paraptōma](../../strongs/g/g3900.md) [sōtēria](../../strongs/g/g4991.md) unto the [ethnos](../../strongs/g/g1484.md), for to [parazēloō](../../strongs/g/g3863.md) them.

<a name="romans_11_12"></a>Romans 11:12

Now if the [paraptōma](../../strongs/g/g3900.md) of them be the [ploutos](../../strongs/g/g4149.md) of the [kosmos](../../strongs/g/g2889.md), and the [hēttēma](../../strongs/g/g2275.md) of them the [ploutos](../../strongs/g/g4149.md) of the [ethnos](../../strongs/g/g1484.md); how much more their [plērōma](../../strongs/g/g4138.md)?

<a name="romans_11_13"></a>Romans 11:13

For I [legō](../../strongs/g/g3004.md) to you [ethnos](../../strongs/g/g1484.md), inasmuch as I am the [apostolos](../../strongs/g/g652.md) of the [ethnos](../../strongs/g/g1484.md), I [doxazō](../../strongs/g/g1392.md) mine [diakonia](../../strongs/g/g1248.md):

<a name="romans_11_14"></a>Romans 11:14

[ei pōs](../../strongs/g/g1513.md) I may [parazēloō](../../strongs/g/g3863.md) them which are my [sarx](../../strongs/g/g4561.md), and might [sōzō](../../strongs/g/g4982.md) some of them.

<a name="romans_11_15"></a>Romans 11:15

For if the [apobolē](../../strongs/g/g580.md) of them the [katallagē](../../strongs/g/g2643.md) of the [kosmos](../../strongs/g/g2889.md), what the [proslēmpsis](../../strongs/g/g4356.md), but [zōē](../../strongs/g/g2222.md) from the [nekros](../../strongs/g/g3498.md)?

<a name="romans_11_16"></a>Romans 11:16

For if the [aparchē](../../strongs/g/g536.md) be [hagios](../../strongs/g/g40.md), the [phyrama](../../strongs/g/g5445.md) is also: and if the [rhiza](../../strongs/g/g4491.md) be [hagios](../../strongs/g/g40.md), so are the [klados](../../strongs/g/g2798.md).

<a name="romans_11_17"></a>Romans 11:17

And if some of the [klados](../../strongs/g/g2798.md) be [ekklaō](../../strongs/g/g1575.md), and thou, being an [agrielaios](../../strongs/g/g65.md), wert [egkentrizō](../../strongs/g/g1461.md) among them, and with them [sygkoinōnos](../../strongs/g/g4791.md) of the [rhiza](../../strongs/g/g4491.md) and [piotēs](../../strongs/g/g4096.md) of the [elaia](../../strongs/g/g1636.md);

<a name="romans_11_18"></a>Romans 11:18

[katakauchaomai](../../strongs/g/g2620.md) not the [klados](../../strongs/g/g2798.md). But if thou [katakauchaomai](../../strongs/g/g2620.md), thou [bastazō](../../strongs/g/g941.md) not the [rhiza](../../strongs/g/g4491.md), but the [rhiza](../../strongs/g/g4491.md) thee.

<a name="romans_11_19"></a>Romans 11:19

Thou wilt [eipon](../../strongs/g/g2046.md) then, The [klados](../../strongs/g/g2798.md) were [ekklaō](../../strongs/g/g1575.md), that I might be [egkentrizō](../../strongs/g/g1461.md).

<a name="romans_11_20"></a>Romans 11:20

[kalōs](../../strongs/g/g2573.md); because of [apistia](../../strongs/g/g570.md) they were [ekklaō](../../strongs/g/g1575.md), and thou [histēmi](../../strongs/g/g2476.md) by [pistis](../../strongs/g/g4102.md). Be not [hypsēlophroneō](../../strongs/g/g5309.md), but [phobeō](../../strongs/g/g5399.md):

<a name="romans_11_21"></a>Romans 11:21

For if [theos](../../strongs/g/g2316.md) [pheidomai](../../strongs/g/g5339.md) not the [physis](../../strongs/g/g5449.md) [klados](../../strongs/g/g2798.md), take heed lest he also [pheidomai](../../strongs/g/g5339.md) not thee.

<a name="romans_11_22"></a>Romans 11:22

[eidō](../../strongs/g/g1492.md) therefore the [chrēstotēs](../../strongs/g/g5544.md) and [apotomia](../../strongs/g/g663.md) of [theos](../../strongs/g/g2316.md): on them which [piptō](../../strongs/g/g4098.md), [apotomia](../../strongs/g/g663.md); but toward thee, [chrēstotēs](../../strongs/g/g5544.md), if thou [epimenō](../../strongs/g/g1961.md) in his [chrēstotēs](../../strongs/g/g5544.md): otherwise thou also shalt be [ekkoptō](../../strongs/g/g1581.md).

<a name="romans_11_23"></a>Romans 11:23

And they also, if they [epimenō](../../strongs/g/g1961.md) not still in [apistia](../../strongs/g/g570.md), shall be [egkentrizō](../../strongs/g/g1461.md): for [theos](../../strongs/g/g2316.md) is [dynatos](../../strongs/g/g1415.md) to [egkentrizō](../../strongs/g/g1461.md) them again.

<a name="romans_11_24"></a>Romans 11:24

For if thou wert [ekkoptō](../../strongs/g/g1581.md) out of the [agrielaios](../../strongs/g/g65.md) by [physis](../../strongs/g/g5449.md), and wert [egkentrizō](../../strongs/g/g1461.md) contrary to [physis](../../strongs/g/g5449.md) into a [kallielaios](../../strongs/g/g2565.md): how much more shall these, which be the [physis](../../strongs/g/g5449.md), be [egkentrizō](../../strongs/g/g1461.md) into their own [elaia](../../strongs/g/g1636.md)?

<a name="romans_11_25"></a>Romans 11:25

For I would not, [adelphos](../../strongs/g/g80.md), that ye should be [agnoeō](../../strongs/g/g50.md) of this [mystērion](../../strongs/g/g3466.md), lest ye should be [phronimos](../../strongs/g/g5429.md) in [heautou](../../strongs/g/g1438.md); that [pōrōsis](../../strongs/g/g4457.md) in [meros](../../strongs/g/g3313.md) is [ginomai](../../strongs/g/g1096.md) to [Israēl](../../strongs/g/g2474.md), until the [plērōma](../../strongs/g/g4138.md) of the [ethnos](../../strongs/g/g1484.md) [eiserchomai](../../strongs/g/g1525.md).

<a name="romans_11_26"></a>Romans 11:26

And so all [Israēl](../../strongs/g/g2474.md) shall be [sōzō](../../strongs/g/g4982.md): as it is [graphō](../../strongs/g/g1125.md), There shall [hēkō](../../strongs/g/g2240.md) out of [Siōn](../../strongs/g/g4622.md) the [rhyomai](../../strongs/g/g4506.md), and shall [apostrephō](../../strongs/g/g654.md) [asebeia](../../strongs/g/g763.md) from [Iakōb](../../strongs/g/g2384.md):

<a name="romans_11_27"></a>Romans 11:27

For this my [diathēkē](../../strongs/g/g1242.md) unto them, when I shall [aphaireō](../../strongs/g/g851.md) their [hamartia](../../strongs/g/g266.md).

<a name="romans_11_28"></a>Romans 11:28

As concerning the [euaggelion](../../strongs/g/g2098.md), they are [echthros](../../strongs/g/g2190.md) for your sakes: but as touching the [eklogē](../../strongs/g/g1589.md), they are [agapētos](../../strongs/g/g27.md) for the [patēr](../../strongs/g/g3962.md) sakes.

<a name="romans_11_29"></a>Romans 11:29

For the [charisma](../../strongs/g/g5486.md) and [klēsis](../../strongs/g/g2821.md) of [theos](../../strongs/g/g2316.md) are [ametamelētos](../../strongs/g/g278.md).

<a name="romans_11_30"></a>Romans 11:30

For as ye in [pote](../../strongs/g/g4218.md) have [apeitheō](../../strongs/g/g544.md) [theos](../../strongs/g/g2316.md), yet have now [eleeō](../../strongs/g/g1653.md) through their [apeitheia](../../strongs/g/g543.md):

<a name="romans_11_31"></a>Romans 11:31

Even so have these also now [apeitheō](../../strongs/g/g544.md), that through your [eleos](../../strongs/g/g1656.md) they also may obtain [eleeō](../../strongs/g/g1653.md).

<a name="romans_11_32"></a>Romans 11:32

For [theos](../../strongs/g/g2316.md) hath [sygkleiō](../../strongs/g/g4788.md) them all in [apeitheia](../../strongs/g/g543.md), that he might have [eleeō](../../strongs/g/g1653.md) upon all.

<a name="romans_11_33"></a>Romans 11:33

[ō](../../strongs/g/g5599.md) the [bathos](../../strongs/g/g899.md) of the [ploutos](../../strongs/g/g4149.md) both of the [sophia](../../strongs/g/g4678.md) and [gnōsis](../../strongs/g/g1108.md) of [theos](../../strongs/g/g2316.md)!  how [anexeraunētos](../../strongs/g/g419.md) are his [krima](../../strongs/g/g2917.md), and his [hodos](../../strongs/g/g3598.md) [anexichniastos](../../strongs/g/g421.md)!

<a name="romans_11_34"></a>Romans 11:34

For who hath [ginōskō](../../strongs/g/g1097.md) the [nous](../../strongs/g/g3563.md) of the [kyrios](../../strongs/g/g2962.md)? or who hath been his [symboulos](../../strongs/g/g4825.md)?

<a name="romans_11_35"></a>Romans 11:35

Or who hath [prodidōmi](../../strongs/g/g4272.md) to him, and it shall be [antapodidōmi](../../strongs/g/g467.md) unto him?

<a name="romans_11_36"></a>Romans 11:36

For of him, and through him, and to him, all things: to whom [doxa](../../strongs/g/g1391.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 10](romans_10.md) - [Romans 12](romans_12.md)

---

[^1]: [Romans 11:6 Commentary](../../commentary/romans/romans_11_commentary.md#romans_11_6)
