# [Romans 15](https://www.blueletterbible.org/kjv/rom/15/1/s_1061001)

<a name="romans_15_1"></a>Romans 15:1

We then that are [dynatos](../../strongs/g/g1415.md) [opheilō](../../strongs/g/g3784.md) to [bastazō](../../strongs/g/g941.md) the [asthenēma](../../strongs/g/g771.md) of the [adynatos](../../strongs/g/g102.md), and not to [areskō](../../strongs/g/g700.md) ourselves.

<a name="romans_15_2"></a>Romans 15:2

Let [hekastos](../../strongs/g/g1538.md) of us [areskō](../../strongs/g/g700.md) his [plēsion](../../strongs/g/g4139.md) for his [agathos](../../strongs/g/g18.md) to [oikodomē](../../strongs/g/g3619.md).

<a name="romans_15_3"></a>Romans 15:3

For even [Christos](../../strongs/g/g5547.md) [areskō](../../strongs/g/g700.md) not himself; but, as it is [graphō](../../strongs/g/g1125.md), The [oneidismos](../../strongs/g/g3680.md) of them that [oneidizō](../../strongs/g/g3679.md) thee [epipiptō](../../strongs/g/g1968.md) on me.

<a name="romans_15_4"></a>Romans 15:4

For whatsoever things were [prographō](../../strongs/g/g4270.md) were [prographō](../../strongs/g/g4270.md) for our [didaskalia](../../strongs/g/g1319.md), that we through [hypomonē](../../strongs/g/g5281.md) and [paraklēsis](../../strongs/g/g3874.md) of the [graphē](../../strongs/g/g1124.md) might have [elpis](../../strongs/g/g1680.md).

<a name="romans_15_5"></a>Romans 15:5

Now the [theos](../../strongs/g/g2316.md) of [hypomonē](../../strongs/g/g5281.md) and [paraklēsis](../../strongs/g/g3874.md) [didōmi](../../strongs/g/g1325.md) you to be [phroneō](../../strongs/g/g5426.md) toward [allēlōn](../../strongs/g/g240.md) according to [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="romans_15_6"></a>Romans 15:6

That ye may [homothymadon](../../strongs/g/g3661.md) and one [stoma](../../strongs/g/g4750.md) [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md), even the [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="romans_15_7"></a>Romans 15:7

Wherefore [proslambanō](../../strongs/g/g4355.md) ye [allēlōn](../../strongs/g/g240.md), as [Christos](../../strongs/g/g5547.md) also [proslambanō](../../strongs/g/g4355.md) us to the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_15_8"></a>Romans 15:8

Now I [legō](../../strongs/g/g3004.md) that [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) was a [diakonos](../../strongs/g/g1249.md) of the [peritomē](../../strongs/g/g4061.md) for the [alētheia](../../strongs/g/g225.md) of [theos](../../strongs/g/g2316.md), to [bebaioō](../../strongs/g/g950.md) the [epaggelia](../../strongs/g/g1860.md) unto the [patēr](../../strongs/g/g3962.md):

<a name="romans_15_9"></a>Romans 15:9

And that the [ethnos](../../strongs/g/g1484.md) might [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) for his [eleos](../../strongs/g/g1656.md); as it is [graphō](../../strongs/g/g1125.md), For this cause I will [exomologeō](../../strongs/g/g1843.md) to thee among the [ethnos](../../strongs/g/g1484.md), and [psallō](../../strongs/g/g5567.md) unto thy [onoma](../../strongs/g/g3686.md).

<a name="romans_15_10"></a>Romans 15:10

And again he [legō](../../strongs/g/g3004.md), [euphrainō](../../strongs/g/g2165.md), ye [ethnos](../../strongs/g/g1484.md), with his [laos](../../strongs/g/g2992.md).

<a name="romans_15_11"></a>Romans 15:11

And again, [aineō](../../strongs/g/g134.md) the [kyrios](../../strongs/g/g2962.md), all ye [ethnos](../../strongs/g/g1484.md); and [epaineō](../../strongs/g/g1867.md) him, all ye [laos](../../strongs/g/g2992.md).

<a name="romans_15_12"></a>Romans 15:12

And again, [Ēsaïas](../../strongs/g/g2268.md) [legō](../../strongs/g/g3004.md), There shall be a [rhiza](../../strongs/g/g4491.md) of [Iessai](../../strongs/g/g2421.md), and he that shall [anistēmi](../../strongs/g/g450.md) to [archō](../../strongs/g/g757.md) the [ethnos](../../strongs/g/g1484.md); in him shall the [ethnos](../../strongs/g/g1484.md) [elpizō](../../strongs/g/g1679.md).

<a name="romans_15_13"></a>Romans 15:13

Now the [theos](../../strongs/g/g2316.md) of [elpis](../../strongs/g/g1680.md) [plēroō](../../strongs/g/g4137.md) you with all [chara](../../strongs/g/g5479.md) and [eirēnē](../../strongs/g/g1515.md) in [pisteuō](../../strongs/g/g4100.md), that ye may [perisseuō](../../strongs/g/g4052.md) in [elpis](../../strongs/g/g1680.md), through the [dynamis](../../strongs/g/g1411.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="romans_15_14"></a>Romans 15:14

And I myself also am [peithō](../../strongs/g/g3982.md) of you, my [adelphos](../../strongs/g/g80.md), that ye also are [mestos](../../strongs/g/g3324.md) of [agathōsynē](../../strongs/g/g19.md), [plēroō](../../strongs/g/g4137.md) with all [gnōsis](../../strongs/g/g1108.md), able also to [noutheteō](../../strongs/g/g3560.md) [allēlōn](../../strongs/g/g240.md).

<a name="romans_15_15"></a>Romans 15:15

Nevertheless, [adelphos](../../strongs/g/g80.md), I have [graphō](../../strongs/g/g1125.md) [tolmēros](../../strongs/g/g5112.md) unto you in [meros](../../strongs/g/g3313.md), as [epanamimnēskō](../../strongs/g/g1878.md) you, because of the [charis](../../strongs/g/g5485.md) that is [didōmi](../../strongs/g/g1325.md) to me of [theos](../../strongs/g/g2316.md),

<a name="romans_15_16"></a>Romans 15:16

That I should be the [leitourgos](../../strongs/g/g3011.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) to the [ethnos](../../strongs/g/g1484.md), [hierourgeō](../../strongs/g/g2418.md) the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md), that the [prosphora](../../strongs/g/g4376.md) of the [ethnos](../../strongs/g/g1484.md) might be [euprosdektos](../../strongs/g/g2144.md), being [hagiazō](../../strongs/g/g37.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="romans_15_17"></a>Romans 15:17

I have therefore whereof I may [kauchēsis](../../strongs/g/g2746.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) [pros](../../strongs/g/g4314.md) [theos](../../strongs/g/g2316.md).

<a name="romans_15_18"></a>Romans 15:18

For I will not [tolmaō](../../strongs/g/g5111.md) to [laleō](../../strongs/g/g2980.md) of any of those things which [Christos](../../strongs/g/g5547.md) hath not [katergazomai](../../strongs/g/g2716.md) by me, to make the [ethnos](../../strongs/g/g1484.md) [hypakoē](../../strongs/g/g5218.md), by [logos](../../strongs/g/g3056.md) and [ergon](../../strongs/g/g2041.md),

<a name="romans_15_19"></a>Romans 15:19

Through [dynamis](../../strongs/g/g1411.md) [sēmeion](../../strongs/g/g4592.md) and [teras](../../strongs/g/g5059.md), by the [dynamis](../../strongs/g/g1411.md) of the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md); so that from [Ierousalēm](../../strongs/g/g2419.md), and [kyklō](../../strongs/g/g2945.md) unto [illyrikon](../../strongs/g/g2437.md), I have [plēroō](../../strongs/g/g4137.md) [euaggelion](../../strongs/g/g2098.md) [Christos](../../strongs/g/g5547.md).

<a name="romans_15_20"></a>Romans 15:20

Yea, so have I [philotimeomai](../../strongs/g/g5389.md) to [euaggelizō](../../strongs/g/g2097.md), not where [Christos](../../strongs/g/g5547.md) was [onomazō](../../strongs/g/g3687.md), lest I should [oikodomeō](../../strongs/g/g3618.md) upon [allotrios](../../strongs/g/g245.md) [themelios](../../strongs/g/g2310.md):

<a name="romans_15_21"></a>Romans 15:21

But as it is [graphō](../../strongs/g/g1125.md), To whom he was not [anaggellō](../../strongs/g/g312.md) of, they shall [optanomai](../../strongs/g/g3700.md): and they that have not [akouō](../../strongs/g/g191.md) shall [syniēmi](../../strongs/g/g4920.md).

<a name="romans_15_22"></a>Romans 15:22

For which cause also I have been [polys](../../strongs/g/g4183.md) [egkoptō](../../strongs/g/g1465.md) from [erchomai](../../strongs/g/g2064.md) to you.

<a name="romans_15_23"></a>Romans 15:23

But [nyni](../../strongs/g/g3570.md) having no more [topos](../../strongs/g/g5117.md) in these [klima](../../strongs/g/g2824.md), and having [epipothia](../../strongs/g/g1974.md) these [polys](../../strongs/g/g4183.md) [etos](../../strongs/g/g2094.md) to [erchomai](../../strongs/g/g2064.md) unto you;

<a name="romans_15_24"></a>Romans 15:24

Whensoever I [poreuō](../../strongs/g/g4198.md) into [spania](../../strongs/g/g4681.md), I will [erchomai](../../strongs/g/g2064.md) to you: for I [elpizō](../../strongs/g/g1679.md) to [theaomai](../../strongs/g/g2300.md) you in my [diaporeuomai](../../strongs/g/g1279.md), and to [propempō](../../strongs/g/g4311.md) thitherward by you, if first I be [meros](../../strongs/g/g3313.md) [empi(m)plēmi](../../strongs/g/g1705.md) with your.

<a name="romans_15_25"></a>Romans 15:25

But [nyni](../../strongs/g/g3570.md) I [poreuō](../../strongs/g/g4198.md) unto [Ierousalēm](../../strongs/g/g2419.md) to [diakoneō](../../strongs/g/g1247.md) unto the [hagios](../../strongs/g/g40.md).

<a name="romans_15_26"></a>Romans 15:26

For it hath [eudokeō](../../strongs/g/g2106.md) them of [Makedonia](../../strongs/g/g3109.md) and [Achaïa](../../strongs/g/g882.md) to [poieō](../../strongs/g/g4160.md) a certain [koinōnia](../../strongs/g/g2842.md) for the [ptōchos](../../strongs/g/g4434.md) [hagios](../../strongs/g/g40.md) which are at [Ierousalēm](../../strongs/g/g2419.md).

<a name="romans_15_27"></a>Romans 15:27

It hath [eudokeō](../../strongs/g/g2106.md) them verily; and their [opheiletēs](../../strongs/g/g3781.md) they are. For if the [ethnos](../../strongs/g/g1484.md) have been [koinōneō](../../strongs/g/g2841.md) of their [pneumatikos](../../strongs/g/g4152.md), their [opheilō](../../strongs/g/g3784.md) also to [leitourgeō](../../strongs/g/g3008.md) unto them in [sarkikos](../../strongs/g/g4559.md).

<a name="romans_15_28"></a>Romans 15:28

When therefore I have [epiteleō](../../strongs/g/g2005.md) this, and have [sphragizō](../../strongs/g/g4972.md) to them this [karpos](../../strongs/g/g2590.md), I will [aperchomai](../../strongs/g/g565.md) by you into [spania](../../strongs/g/g4681.md).

<a name="romans_15_29"></a>Romans 15:29

And I [eidō](../../strongs/g/g1492.md) that, when I [erchomai](../../strongs/g/g2064.md) unto you, I shall [erchomai](../../strongs/g/g2064.md) in the [plērōma](../../strongs/g/g4138.md) of the [eulogia](../../strongs/g/g2129.md) of the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md).

<a name="romans_15_30"></a>Romans 15:30

Now I [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), for the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) sake, and for the [agapē](../../strongs/g/g26.md) of the [pneuma](../../strongs/g/g4151.md), that ye [synagōnizomai](../../strongs/g/g4865.md) with me in your [proseuchē](../../strongs/g/g4335.md) to [theos](../../strongs/g/g2316.md) for me;

<a name="romans_15_31"></a>Romans 15:31

That I may be [rhyomai](../../strongs/g/g4506.md) from them that [apeitheō](../../strongs/g/g544.md) in [Ioudaia](../../strongs/g/g2449.md); and that my [diakonia](../../strongs/g/g1248.md) which I have for [Ierousalēm](../../strongs/g/g2419.md) may be [euprosdektos](../../strongs/g/g2144.md) of the [hagios](../../strongs/g/g40.md);

<a name="romans_15_32"></a>Romans 15:32

That I may [erchomai](../../strongs/g/g2064.md) unto you with [chara](../../strongs/g/g5479.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), and may with you be [synanapauomai](../../strongs/g/g4875.md).

<a name="romans_15_33"></a>Romans 15:33

Now the [theos](../../strongs/g/g2316.md) of [eirēnē](../../strongs/g/g1515.md) be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 14](romans_14.md) - [Romans 16](romans_16.md)