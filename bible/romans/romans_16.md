# [Romans 16](https://www.blueletterbible.org/kjv/rom/16/1/s_1062001)

<a name="romans_16_1"></a>Romans 16:1

I [synistēmi](../../strongs/g/g4921.md) unto you [phoibē](../../strongs/g/g5402.md) our [adelphē](../../strongs/g/g79.md), which is a [diakonos](../../strongs/g/g1249.md) of the [ekklēsia](../../strongs/g/g1577.md) which is at [Kegchreai](../../strongs/g/g2747.md):

<a name="romans_16_2"></a>Romans 16:2

That ye [prosdechomai](../../strongs/g/g4327.md) her in the [kyrios](../../strongs/g/g2962.md), as [axiōs](../../strongs/g/g516.md) [hagios](../../strongs/g/g40.md), and that ye [paristēmi](../../strongs/g/g3936.md) her in whatsoever [pragma](../../strongs/g/g4229.md) she hath [chrēzō](../../strongs/g/g5535.md) of you: for she hath been a [prostatis](../../strongs/g/g4368.md) of [polys](../../strongs/g/g4183.md), and of myself also.

<a name="romans_16_3"></a>Romans 16:3

[aspazomai](../../strongs/g/g782.md) [Priskilla](../../strongs/g/g4252.md) and [Akylas](../../strongs/g/g207.md) my [synergos](../../strongs/g/g4904.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="romans_16_4"></a>Romans 16:4

Who have for my [psychē](../../strongs/g/g5590.md) [hypotithēmi](../../strongs/g/g5294.md) their own [trachēlos](../../strongs/g/g5137.md): unto whom not only I [eucharisteō](../../strongs/g/g2168.md), but also all the [ekklēsia](../../strongs/g/g1577.md) of the [ethnos](../../strongs/g/g1484.md).

<a name="romans_16_5"></a>Romans 16:5

Likewise the [ekklēsia](../../strongs/g/g1577.md) that is in their [oikos](../../strongs/g/g3624.md). [aspazomai](../../strongs/g/g782.md) my [agapētos](../../strongs/g/g27.md) [epainetos](../../strongs/g/g1866.md), who is the [aparchē](../../strongs/g/g536.md) of [Achaïa](../../strongs/g/g882.md) unto [Christos](../../strongs/g/g5547.md).

<a name="romans_16_6"></a>Romans 16:6

[aspazomai](../../strongs/g/g782.md) [Maria](../../strongs/g/g3137.md), who [kopiaō](../../strongs/g/g2872.md) [polys](../../strongs/g/g4183.md) on us.

<a name="romans_16_7"></a>Romans 16:7

[aspazomai](../../strongs/g/g782.md) [andronikos](../../strongs/g/g408.md) and [iounias](../../strongs/g/g2458.md), my [syggenēs](../../strongs/g/g4773.md), and my [synaichmalōtos](../../strongs/g/g4869.md), who are [episēmos](../../strongs/g/g1978.md) among the [apostolos](../../strongs/g/g652.md), who also were in [Christos](../../strongs/g/g5547.md) before me.

<a name="romans_16_8"></a>Romans 16:8

[aspazomai](../../strongs/g/g782.md) [amplias](../../strongs/g/g291.md) my [agapētos](../../strongs/g/g27.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="romans_16_9"></a>Romans 16:9

[aspazomai](../../strongs/g/g782.md) [ourbanos](../../strongs/g/g3773.md), our [synergos](../../strongs/g/g4904.md) in [Christos](../../strongs/g/g5547.md), and [stachys](../../strongs/g/g4720.md) my [agapētos](../../strongs/g/g27.md).

<a name="romans_16_10"></a>Romans 16:10

[aspazomai](../../strongs/g/g782.md) [apellēs](../../strongs/g/g559.md) [dokimos](../../strongs/g/g1384.md) in [Christos](../../strongs/g/g5547.md). [aspazomai](../../strongs/g/g782.md) them which are of [aristoboulos](../../strongs/g/g711.md).

<a name="romans_16_11"></a>Romans 16:11

[aspazomai](../../strongs/g/g782.md) [hērōdiōn](../../strongs/g/g2267.md) my [syggenēs](../../strongs/g/g4773.md). [aspazomai](../../strongs/g/g782.md) them that be of [narkissos](../../strongs/g/g3488.md), which are in the [kyrios](../../strongs/g/g2962.md).

<a name="romans_16_12"></a>Romans 16:12

[aspazomai](../../strongs/g/g782.md) [tryphaina](../../strongs/g/g5170.md) and [tryphōsa](../../strongs/g/g5173.md), who [kopiaō](../../strongs/g/g2872.md) in the [kyrios](../../strongs/g/g2962.md). [aspazomai](../../strongs/g/g782.md) the [agapētos](../../strongs/g/g27.md) [persis](../../strongs/g/g4069.md), which [kopiaō](../../strongs/g/g2872.md) [polys](../../strongs/g/g4183.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="romans_16_13"></a>Romans 16:13

[aspazomai](../../strongs/g/g782.md) [Rhouphos](../../strongs/g/g4504.md) [eklektos](../../strongs/g/g1588.md) in the [kyrios](../../strongs/g/g2962.md), and his [mētēr](../../strongs/g/g3384.md) and mine.

<a name="romans_16_14"></a>Romans 16:14

[aspazomai](../../strongs/g/g782.md) [asygkritos](../../strongs/g/g799.md), [phlegōn](../../strongs/g/g5393.md), [hermas](../../strongs/g/g2057.md), [patrobas](../../strongs/g/g3969.md), [Hermēs](../../strongs/g/g2060.md), and the [adelphos](../../strongs/g/g80.md) which are with them.

<a name="romans_16_15"></a>Romans 16:15

[aspazomai](../../strongs/g/g782.md) [philologos](../../strongs/g/g5378.md), and [ioulia](../../strongs/g/g2456.md), [nēreus](../../strongs/g/g3517.md), and his [adelphē](../../strongs/g/g79.md), and [olympas](../../strongs/g/g3652.md), and all the [hagios](../../strongs/g/g40.md) which are with them.

<a name="romans_16_16"></a>Romans 16:16

[aspazomai](../../strongs/g/g782.md) [allēlōn](../../strongs/g/g240.md) with an [hagios](../../strongs/g/g40.md) [philēma](../../strongs/g/g5370.md). The [ekklēsia](../../strongs/g/g1577.md) of [Christos](../../strongs/g/g5547.md) [aspazomai](../../strongs/g/g782.md) you.

<a name="romans_16_17"></a>Romans 16:17

Now I [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), [skopeō](../../strongs/g/g4648.md) them which [poieō](../../strongs/g/g4160.md) [dichostasia](../../strongs/g/g1370.md) and [skandalon](../../strongs/g/g4625.md) contrary to the [didachē](../../strongs/g/g1322.md) which ye have [manthanō](../../strongs/g/g3129.md); and [ekklinō](../../strongs/g/g1578.md) them.

<a name="romans_16_18"></a>Romans 16:18

For they that are such [douleuō](../../strongs/g/g1398.md) not our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), but their own [koilia](../../strongs/g/g2836.md); and by [chrēstologia](../../strongs/g/g5542.md) and [eulogia](../../strongs/g/g2129.md) [exapataō](../../strongs/g/g1818.md) the [kardia](../../strongs/g/g2588.md) of the [akakos](../../strongs/g/g172.md).

<a name="romans_16_19"></a>Romans 16:19

For your [hypakoē](../../strongs/g/g5218.md) is [aphikneomai](../../strongs/g/g864.md) unto all. I am [chairō](../../strongs/g/g5463.md) therefore on your behalf: but yet I would have you [sophos](../../strongs/g/g4680.md) unto that which is [agathos](../../strongs/g/g18.md), and [akeraios](../../strongs/g/g185.md) concerning [kakos](../../strongs/g/g2556.md).

<a name="romans_16_20"></a>Romans 16:20

And the [theos](../../strongs/g/g2316.md) of [eirēnē](../../strongs/g/g1515.md) shall [syntribō](../../strongs/g/g4937.md) [Satanas](../../strongs/g/g4567.md) under your [pous](../../strongs/g/g4228.md) [tachos](../../strongs/g/g5034.md). The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you. [amēn](../../strongs/g/g281.md).

<a name="romans_16_21"></a>Romans 16:21

[Timotheos](../../strongs/g/g5095.md) my [synergos](../../strongs/g/g4904.md), and [Loukios](../../strongs/g/g3066.md), and [Iasōn](../../strongs/g/g2394.md), and [sōsipatros](../../strongs/g/g4989.md), my [syggenēs](../../strongs/g/g4773.md), [aspazomai](../../strongs/g/g782.md) you.

<a name="romans_16_22"></a>Romans 16:22

I [Tertios](../../strongs/g/g5060.md), who [graphō](../../strongs/g/g1125.md) [epistolē](../../strongs/g/g1992.md), [aspazomai](../../strongs/g/g782.md) you in the [kyrios](../../strongs/g/g2962.md).

<a name="romans_16_23"></a>Romans 16:23

[Gaïos](../../strongs/g/g1050.md) mine [xenos](../../strongs/g/g3581.md), and of the whole [ekklēsia](../../strongs/g/g1577.md), [aspazomai](../../strongs/g/g782.md) you. [Erastos](../../strongs/g/g2037.md) the [oikonomos](../../strongs/g/g3623.md) of the [polis](../../strongs/g/g4172.md) [aspazomai](../../strongs/g/g782.md) you, and [kouartos](../../strongs/g/g2890.md) an [adelphos](../../strongs/g/g80.md).

<a name="romans_16_24"></a>Romans 16:24

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you all. [amēn](../../strongs/g/g281.md). [^1]

<a name="romans_16_25"></a>Romans 16:25

Now to him that is of [dynamai](../../strongs/g/g1410.md) to [stērizō](../../strongs/g/g4741.md) you according to my [euaggelion](../../strongs/g/g2098.md), and the [kērygma](../../strongs/g/g2782.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), according to the [apokalypsis](../../strongs/g/g602.md) of the [mystērion](../../strongs/g/g3466.md), which was [sigaō](../../strongs/g/g4601.md) since the [chronos](../../strongs/g/g5550.md) [aiōnios](../../strongs/g/g166.md), [^2]

<a name="romans_16_26"></a>Romans 16:26

But now is [phaneroō](../../strongs/g/g5319.md), and by the [graphē](../../strongs/g/g1124.md) of the [prophētikos](../../strongs/g/g4397.md), according to the [epitagē](../../strongs/g/g2003.md) of the [aiōnios](../../strongs/g/g166.md) [theos](../../strongs/g/g2316.md), [gnōrizō](../../strongs/g/g1107.md) to all [ethnos](../../strongs/g/g1484.md) for the [hypakoē](../../strongs/g/g5218.md) of [pistis](../../strongs/g/g4102.md):

<a name="romans_16_27"></a>Romans 16:27

To [theos](../../strongs/g/g2316.md) only [sophos](../../strongs/g/g4680.md), [doxa](../../strongs/g/g1391.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 15](romans_15.md)

---

[^1]: [Romans 16:24 Commentary](../../commentary/romans/romans_16_commentary.md#romans_16_24)

[^2]: [Romans 16:25 Commentary](../../commentary/romans/romans_16_commentary.md#romans_16_25)
