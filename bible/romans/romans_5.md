# [Romans 5](https://www.blueletterbible.org/kjv/rom/5/1/s_1051001)

<a name="romans_5_1"></a>Romans 5:1

Therefore being [dikaioō](../../strongs/g/g1344.md) by [pistis](../../strongs/g/g4102.md), we have [eirēnē](../../strongs/g/g1515.md) with [theos](../../strongs/g/g2316.md) through our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="romans_5_2"></a>Romans 5:2

By whom also we have [prosagōgē](../../strongs/g/g4318.md) by [pistis](../../strongs/g/g4102.md) into this [charis](../../strongs/g/g5485.md) wherein we [histēmi](../../strongs/g/g2476.md), and [kauchaomai](../../strongs/g/g2744.md) in [elpis](../../strongs/g/g1680.md) of the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_5_3"></a>Romans 5:3

And not only, but we [kauchaomai](../../strongs/g/g2744.md) in [thlipsis](../../strongs/g/g2347.md) also: [eidō](../../strongs/g/g1492.md) that [thlipsis](../../strongs/g/g2347.md) [katergazomai](../../strongs/g/g2716.md) [hypomonē](../../strongs/g/g5281.md);

<a name="romans_5_4"></a>Romans 5:4

And [hypomonē](../../strongs/g/g5281.md), [dokimē](../../strongs/g/g1382.md); and [dokimē](../../strongs/g/g1382.md), [elpis](../../strongs/g/g1680.md):

<a name="romans_5_5"></a>Romans 5:5

And [elpis](../../strongs/g/g1680.md) [kataischynō](../../strongs/g/g2617.md) not; because the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md) is [ekcheō](../../strongs/g/g1632.md) in our [kardia](../../strongs/g/g2588.md) by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) which is [didōmi](../../strongs/g/g1325.md) unto us.

<a name="romans_5_6"></a>Romans 5:6

For when we were yet [asthenēs](../../strongs/g/g772.md), in [kairos](../../strongs/g/g2540.md) [Christos](../../strongs/g/g5547.md) [apothnēskō](../../strongs/g/g599.md) for the [asebēs](../../strongs/g/g765.md).

<a name="romans_5_7"></a>Romans 5:7

For [molis](../../strongs/g/g3433.md) for a [dikaios](../../strongs/g/g1342.md) will one [apothnēskō](../../strongs/g/g599.md): yet [tacha](../../strongs/g/g5029.md) for an [agathos](../../strongs/g/g18.md) some would even [tolmaō](../../strongs/g/g5111.md) to [apothnēskō](../../strongs/g/g599.md).

<a name="romans_5_8"></a>Romans 5:8

But [theos](../../strongs/g/g2316.md) [synistēmi](../../strongs/g/g4921.md) his [agapē](../../strongs/g/g26.md) toward us, in that, while we were yet [hamartōlos](../../strongs/g/g268.md), [Christos](../../strongs/g/g5547.md) [apothnēskō](../../strongs/g/g599.md) for us.

<a name="romans_5_9"></a>Romans 5:9

[polys](../../strongs/g/g4183.md) more then, being now [dikaioō](../../strongs/g/g1344.md) by his [haima](../../strongs/g/g129.md), we shall be [sōzō](../../strongs/g/g4982.md) from [orgē](../../strongs/g/g3709.md) through him.

<a name="romans_5_10"></a>Romans 5:10

For if, when we were [echthros](../../strongs/g/g2190.md), we were [katallassō](../../strongs/g/g2644.md) to [theos](../../strongs/g/g2316.md) by the [thanatos](../../strongs/g/g2288.md) of his [huios](../../strongs/g/g5207.md), [polys](../../strongs/g/g4183.md) more, being [katallassō](../../strongs/g/g2644.md), we shall be [sōzō](../../strongs/g/g4982.md) by his [zōē](../../strongs/g/g2222.md).

<a name="romans_5_11"></a>Romans 5:11

And not only so, but we also [kauchaomai](../../strongs/g/g2744.md) in [theos](../../strongs/g/g2316.md) through our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), by whom we have now [lambanō](../../strongs/g/g2983.md) [katallagē](../../strongs/g/g2643.md).

<a name="romans_5_12"></a>Romans 5:12

Wherefore, as by one [anthrōpos](../../strongs/g/g444.md) [hamartia](../../strongs/g/g266.md) [eiserchomai](../../strongs/g/g1525.md) into the [kosmos](../../strongs/g/g2889.md), and [thanatos](../../strongs/g/g2288.md) by [hamartia](../../strongs/g/g266.md); and so [thanatos](../../strongs/g/g2288.md) [dierchomai](../../strongs/g/g1330.md) upon all [anthrōpos](../../strongs/g/g444.md), for that all have [hamartanō](../../strongs/g/g264.md):

<a name="romans_5_13"></a>Romans 5:13

(For until the [nomos](../../strongs/g/g3551.md) [hamartia](../../strongs/g/g266.md) was in the [kosmos](../../strongs/g/g2889.md): but [hamartia](../../strongs/g/g266.md) is not [ellogeō](../../strongs/g/g1677.md) when there is no [nomos](../../strongs/g/g3551.md).

<a name="romans_5_14"></a>Romans 5:14

Nevertheless [thanatos](../../strongs/g/g2288.md) [basileuō](../../strongs/g/g936.md) from [Adam](../../strongs/g/g76.md) to [Mōÿsēs](../../strongs/g/g3475.md), even over them that had not [hamartanō](../../strongs/g/g264.md) after the [homoiōma](../../strongs/g/g3667.md) of [Adam](../../strongs/g/g76.md) [parabasis](../../strongs/g/g3847.md), who is the [typos](../../strongs/g/g5179.md) [mellō](../../strongs/g/g3195.md).

<a name="romans_5_15"></a>Romans 5:15

But not as the [paraptōma](../../strongs/g/g3900.md), so also is the [charisma](../../strongs/g/g5486.md). For if through the [paraptōma](../../strongs/g/g3900.md) of one [polys](../../strongs/g/g4183.md) be [apothnēskō](../../strongs/g/g599.md), [polys](../../strongs/g/g4183.md) more the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md), and the [dōrea](../../strongs/g/g1431.md) by [charis](../../strongs/g/g5485.md), which is by one [anthrōpos](../../strongs/g/g444.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), hath [perisseuō](../../strongs/g/g4052.md) unto [polys](../../strongs/g/g4183.md).

<a name="romans_5_16"></a>Romans 5:16

And not as by one that [hamartanō](../../strongs/g/g264.md), the [dōrēma](../../strongs/g/g1434.md): for the [krima](../../strongs/g/g2917.md) by one to [katakrima](../../strongs/g/g2631.md), but the [charisma](../../strongs/g/g5486.md) of [polys](../../strongs/g/g4183.md) [paraptōma](../../strongs/g/g3900.md) unto [dikaiōma](../../strongs/g/g1345.md).

<a name="romans_5_17"></a>Romans 5:17

For if by [heis](../../strongs/g/g1520.md) [paraptōma](../../strongs/g/g3900.md) [thanatos](../../strongs/g/g2288.md) [basileuō](../../strongs/g/g936.md) by [heis](../../strongs/g/g1520.md); [polys](../../strongs/g/g4183.md) more they which [lambanō](../../strongs/g/g2983.md) [perisseia](../../strongs/g/g4050.md) of [charis](../../strongs/g/g5485.md) and of the [dōrea](../../strongs/g/g1431.md) of [dikaiosynē](../../strongs/g/g1343.md) shall [basileuō](../../strongs/g/g936.md) in [zōē](../../strongs/g/g2222.md) by [heis](../../strongs/g/g1520.md), [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).)

<a name="romans_5_18"></a>Romans 5:18

Therefore as by the [paraptōma](../../strongs/g/g3900.md) of one upon all [anthrōpos](../../strongs/g/g444.md) to [katakrima](../../strongs/g/g2631.md); even so by the [dikaiōma](../../strongs/g/g1345.md) of one upon all [anthrōpos](../../strongs/g/g444.md) unto [dikaiōsis](../../strongs/g/g1347.md) of [zōē](../../strongs/g/g2222.md).

<a name="romans_5_19"></a>Romans 5:19

For as by one [anthrōpos](../../strongs/g/g444.md) [parakoē](../../strongs/g/g3876.md) [polys](../../strongs/g/g4183.md) were [kathistēmi](../../strongs/g/g2525.md) [hamartōlos](../../strongs/g/g268.md), so by the [hypakoē](../../strongs/g/g5218.md) of one shall [polys](../../strongs/g/g4183.md) be [kathistēmi](../../strongs/g/g2525.md) [dikaios](../../strongs/g/g1342.md).

<a name="romans_5_20"></a>Romans 5:20

Moreover the [nomos](../../strongs/g/g3551.md) [pareiserchomai](../../strongs/g/g3922.md), that the [paraptōma](../../strongs/g/g3900.md) might [pleonazō](../../strongs/g/g4121.md). But where [hamartia](../../strongs/g/g266.md) [pleonazō](../../strongs/g/g4121.md), [charis](../../strongs/g/g5485.md) did [hyperperisseuō](../../strongs/g/g5248.md):

<a name="romans_5_21"></a>Romans 5:21

That as [hamartia](../../strongs/g/g266.md) hath [basileuō](../../strongs/g/g936.md) unto [thanatos](../../strongs/g/g2288.md), even so might [charis](../../strongs/g/g5485.md) [basileuō](../../strongs/g/g936.md) through [dikaiosynē](../../strongs/g/g1343.md) unto [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 4](romans_4.md) - [Romans 6](romans_6.md)