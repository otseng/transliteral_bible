# [Romans 9](https://www.blueletterbible.org/kjv/rom/9/1/s_1055001)

<a name="romans_9_1"></a>Romans 9:1

I [legō](../../strongs/g/g3004.md) the [alētheia](../../strongs/g/g225.md) in [Christos](../../strongs/g/g5547.md), I [pseudomai](../../strongs/g/g5574.md) not, my [syneidēsis](../../strongs/g/g4893.md) also [symmartyreō](../../strongs/g/g4828.md) me in the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md),

<a name="romans_9_2"></a>Romans 9:2

That I have [megas](../../strongs/g/g3173.md) [lypē](../../strongs/g/g3077.md) and [adialeiptos](../../strongs/g/g88.md) [odynē](../../strongs/g/g3601.md) in my [kardia](../../strongs/g/g2588.md).

<a name="romans_9_3"></a>Romans 9:3

For I could [euchomai](../../strongs/g/g2172.md) that myself were [anathema](../../strongs/g/g331.md) from [Christos](../../strongs/g/g5547.md) for my [adelphos](../../strongs/g/g80.md), my [syggenēs](../../strongs/g/g4773.md) according to the [sarx](../../strongs/g/g4561.md):

<a name="romans_9_4"></a>Romans 9:4

Who are [Israēlitēs](../../strongs/g/g2475.md); to whom [huiothesia](../../strongs/g/g5206.md), and the [doxa](../../strongs/g/g1391.md), and the [diathēkē](../../strongs/g/g1242.md), and the [nomothesia](../../strongs/g/g3548.md), and the [latreia](../../strongs/g/g2999.md), and the [epaggelia](../../strongs/g/g1860.md);

<a name="romans_9_5"></a>Romans 9:5

Whose the [patēr](../../strongs/g/g3962.md), and of whom as concerning the [sarx](../../strongs/g/g4561.md) [Christos](../../strongs/g/g5547.md), who is over all, [theos](../../strongs/g/g2316.md) [eulogētos](../../strongs/g/g2128.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="romans_9_6"></a>Romans 9:6

Not as though the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) hath [ekpiptō](../../strongs/g/g1601.md). For they not all [Israēl](../../strongs/g/g2474.md), which are of [Israēl](../../strongs/g/g2474.md):

<a name="romans_9_7"></a>Romans 9:7

Neither, because they are the [sperma](../../strongs/g/g4690.md) of [Abraam](../../strongs/g/g11.md), are they all [teknon](../../strongs/g/g5043.md): but, In [Isaak](../../strongs/g/g2464.md) shall thy [sperma](../../strongs/g/g4690.md) be [kaleō](../../strongs/g/g2564.md).

<a name="romans_9_8"></a>Romans 9:8

That is, They which are the [teknon](../../strongs/g/g5043.md) of the [sarx](../../strongs/g/g4561.md), these are not the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md): but the [teknon](../../strongs/g/g5043.md) of the [epaggelia](../../strongs/g/g1860.md) are [logizomai](../../strongs/g/g3049.md) for the [sperma](../../strongs/g/g4690.md).

<a name="romans_9_9"></a>Romans 9:9

For this the [logos](../../strongs/g/g3056.md) of [epaggelia](../../strongs/g/g1860.md), At this [kairos](../../strongs/g/g2540.md) will I [erchomai](../../strongs/g/g2064.md), and [Sarra](../../strongs/g/g4564.md) shall have a [huios](../../strongs/g/g5207.md).

<a name="romans_9_10"></a>Romans 9:10

And not only; but when [rhebekka](../../strongs/g/g4479.md) also had [koitē](../../strongs/g/g2845.md) by one, even by our [patēr](../../strongs/g/g3962.md) [Isaak](../../strongs/g/g2464.md);

<a name="romans_9_11"></a>Romans 9:11

(For being not yet [gennaō](../../strongs/g/g1080.md), neither having [prassō](../../strongs/g/g4238.md) any [agathos](../../strongs/g/g18.md) or [kakos](../../strongs/g/g2556.md), that the [prothesis](../../strongs/g/g4286.md) of [theos](../../strongs/g/g2316.md) according to [eklogē](../../strongs/g/g1589.md) might [menō](../../strongs/g/g3306.md), not of [ergon](../../strongs/g/g2041.md), but of him that [kaleō](../../strongs/g/g2564.md);)

<a name="romans_9_12"></a>Romans 9:12

It was [rheō](../../strongs/g/g4483.md) unto her, The [meizōn](../../strongs/g/g3187.md) shall [douleuō](../../strongs/g/g1398.md) the [elassōn](../../strongs/g/g1640.md).

<a name="romans_9_13"></a>Romans 9:13

As it is [graphō](../../strongs/g/g1125.md), [Iakōb](../../strongs/g/g2384.md) have I [agapaō](../../strongs/g/g25.md), but [Ēsau](../../strongs/g/g2269.md) have I [miseō](../../strongs/g/g3404.md).

<a name="romans_9_14"></a>Romans 9:14

What shall we [eipon](../../strongs/g/g2046.md) then? Is there [adikia](../../strongs/g/g93.md) with [theos](../../strongs/g/g2316.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md).

<a name="romans_9_15"></a>Romans 9:15

For he [legō](../../strongs/g/g3004.md) to [Mōÿsēs](../../strongs/g/g3475.md), I will have [eleeō](../../strongs/g/g1653.md) on whom I will have [eleeō](../../strongs/g/g1653.md), and I will have [oiktirō](../../strongs/g/g3627.md) on whom I will have [oiktirō](../../strongs/g/g3627.md).

<a name="romans_9_16"></a>Romans 9:16

So then not of him that [thelō](../../strongs/g/g2309.md), nor of him that [trechō](../../strongs/g/g5143.md), but of [theos](../../strongs/g/g2316.md) that [eleeō](../../strongs/g/g1653.md).

<a name="romans_9_17"></a>Romans 9:17

For the [graphē](../../strongs/g/g1124.md) [legō](../../strongs/g/g3004.md) unto [Pharaō](../../strongs/g/g5328.md), Even for this same purpose have I [exegeirō](../../strongs/g/g1825.md) thee, that I might [endeiknymi](../../strongs/g/g1731.md) my [dynamis](../../strongs/g/g1411.md) in thee, and that my [onoma](../../strongs/g/g3686.md) might be [diaggellō](../../strongs/g/g1229.md) throughout all [gē](../../strongs/g/g1093.md).

<a name="romans_9_18"></a>Romans 9:18

Therefore hath he [eleeō](../../strongs/g/g1653.md) on whom he [thelō](../../strongs/g/g2309.md), and whom he [thelō](../../strongs/g/g2309.md) he [sklērynō](../../strongs/g/g4645.md).

<a name="romans_9_19"></a>Romans 9:19

Thou wilt [eipon](../../strongs/g/g2046.md) then unto me, Why doth he yet [memphomai](../../strongs/g/g3201.md)? For who hath [anthistēmi](../../strongs/g/g436.md) his [boulēma](../../strongs/g/g1013.md)?

<a name="romans_9_20"></a>Romans 9:20

[menoun](../../strongs/g/g3304.md), [ō](../../strongs/g/g5599.md) [anthrōpos](../../strongs/g/g444.md), who art thou that [antapokrinomai](../../strongs/g/g470.md) [theos](../../strongs/g/g2316.md)? Shall the [plasma](../../strongs/g/g4110.md) [eipon](../../strongs/g/g2046.md) to [plassō](../../strongs/g/g4111.md), Why hast thou [poieō](../../strongs/g/g4160.md) me thus?

<a name="romans_9_21"></a>Romans 9:21

Hath not the [kerameus](../../strongs/g/g2763.md) [exousia](../../strongs/g/g1849.md) over the [pēlos](../../strongs/g/g4081.md), of the same [phyrama](../../strongs/g/g5445.md) to [poieō](../../strongs/g/g4160.md) [skeuos](../../strongs/g/g4632.md) unto [timē](../../strongs/g/g5092.md), and another unto [atimia](../../strongs/g/g819.md)?

<a name="romans_9_22"></a>Romans 9:22

What if [theos](../../strongs/g/g2316.md), willing to [endeiknymi](../../strongs/g/g1731.md) his [orgē](../../strongs/g/g3709.md), and to make his [dynatos](../../strongs/g/g1415.md) [gnōrizō](../../strongs/g/g1107.md), [pherō](../../strongs/g/g5342.md) with [polys](../../strongs/g/g4183.md) [makrothymia](../../strongs/g/g3115.md) the [skeuos](../../strongs/g/g4632.md) of [orgē](../../strongs/g/g3709.md) [katartizō](../../strongs/g/g2675.md) to [apōleia](../../strongs/g/g684.md):

<a name="romans_9_23"></a>Romans 9:23

And that he might [gnōrizō](../../strongs/g/g1107.md) the [ploutos](../../strongs/g/g4149.md) of his [doxa](../../strongs/g/g1391.md) on the [skeuos](../../strongs/g/g4632.md) of [eleos](../../strongs/g/g1656.md), which he had [proetoimazō](../../strongs/g/g4282.md) unto [doxa](../../strongs/g/g1391.md),

<a name="romans_9_24"></a>Romans 9:24

Even us, whom he hath [kaleō](../../strongs/g/g2564.md), not of the [Ioudaios](../../strongs/g/g2453.md) only, but also of the [ethnos](../../strongs/g/g1484.md)?

<a name="romans_9_25"></a>Romans 9:25

As he [legō](../../strongs/g/g3004.md) also in [hōsēe](../../strongs/g/g5617.md), I will [kaleō](../../strongs/g/g2564.md) them my [laos](../../strongs/g/g2992.md), which were not my [laos](../../strongs/g/g2992.md); and her [agapaō](../../strongs/g/g25.md), which was not [agapaō](../../strongs/g/g25.md).

<a name="romans_9_26"></a>Romans 9:26

And [esomai](../../strongs/g/g2071.md), that in the [topos](../../strongs/g/g5117.md) where it was [rheō](../../strongs/g/g4483.md) unto them, Ye are not my [laos](../../strongs/g/g2992.md); there shall they be [kaleō](../../strongs/g/g2564.md) the [huios](../../strongs/g/g5207.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md).

<a name="romans_9_27"></a>Romans 9:27

[Ēsaïas](../../strongs/g/g2268.md) also [krazō](../../strongs/g/g2896.md) concerning [Israēl](../../strongs/g/g2474.md), Though the [arithmos](../../strongs/g/g706.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md) be as the [ammos](../../strongs/g/g285.md) of the [thalassa](../../strongs/g/g2281.md), a [hypoleimma](../../strongs/g/g2640.md) shall be [sōzō](../../strongs/g/g4982.md):

<a name="romans_9_28"></a>Romans 9:28

For he will [synteleō](../../strongs/g/g4931.md) the [logos](../../strongs/g/g3056.md), and [syntemnō](../../strongs/g/g4932.md) in [dikaiosynē](../../strongs/g/g1343.md): because a [syntemnō](../../strongs/g/g4932.md) [logos](../../strongs/g/g3056.md) will the [kyrios](../../strongs/g/g2962.md) [poieō](../../strongs/g/g4160.md) upon [gē](../../strongs/g/g1093.md).

<a name="romans_9_29"></a>Romans 9:29

And as [Ēsaïas](../../strongs/g/g2268.md) [proereō](../../strongs/g/g4280.md), Except the [kyrios](../../strongs/g/g2962.md) of [Sabaōth](../../strongs/g/g4519.md) had [egkataleipō](../../strongs/g/g1459.md) us a [sperma](../../strongs/g/g4690.md), we had been as [Sodoma](../../strongs/g/g4670.md), and been [homoioō](../../strongs/g/g3666.md) unto [Gomorra](../../strongs/g/g1116.md).

<a name="romans_9_30"></a>Romans 9:30

What shall we [eipon](../../strongs/g/g2046.md) then? That the [ethnos](../../strongs/g/g1484.md), which [diōkō](../../strongs/g/g1377.md) not after [dikaiosynē](../../strongs/g/g1343.md), have [katalambanō](../../strongs/g/g2638.md) to [dikaiosynē](../../strongs/g/g1343.md), even the [dikaiosynē](../../strongs/g/g1343.md) which is of [pistis](../../strongs/g/g4102.md).

<a name="romans_9_31"></a>Romans 9:31

But [Israēl](../../strongs/g/g2474.md), which [diōkō](../../strongs/g/g1377.md) after the [nomos](../../strongs/g/g3551.md) of [dikaiosynē](../../strongs/g/g1343.md), hath not [phthanō](../../strongs/g/g5348.md) to the [nomos](../../strongs/g/g3551.md) of [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_9_32"></a>Romans 9:32

Wherefore? Because not by [pistis](../../strongs/g/g4102.md), but as it were by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md). For they [proskoptō](../../strongs/g/g4350.md) at that [lithos](../../strongs/g/g3037.md) [proskomma](../../strongs/g/g4348.md);

<a name="romans_9_33"></a>Romans 9:33

As it is [graphō](../../strongs/g/g1125.md), [idou](../../strongs/g/g2400.md), I [tithēmi](../../strongs/g/g5087.md) in [Siōn](../../strongs/g/g4622.md) a [lithos](../../strongs/g/g3037.md) [proskomma](../../strongs/g/g4348.md) and [petra](../../strongs/g/g4073.md) of [skandalon](../../strongs/g/g4625.md): and whosoever [pisteuō](../../strongs/g/g4100.md) on him shall not be [kataischynō](../../strongs/g/g2617.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 8](romans_8.md) - [Romans 10](romans_10.md)