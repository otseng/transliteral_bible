# [Romans 2](https://www.blueletterbible.org/kjv/rom/2/1/s_1048001)

<a name="romans_2_1"></a>Romans 2:1

Therefore thou art [anapologetos](../../strongs/g/g379.md), [ō](../../strongs/g/g5599.md) [anthrōpos](../../strongs/g/g444.md), whosoever thou art that [krinō](../../strongs/g/g2919.md): for wherein thou [krinō](../../strongs/g/g2919.md) another, thou [katakrinō](../../strongs/g/g2632.md) thyself; for thou that [krinō](../../strongs/g/g2919.md) [prassō](../../strongs/g/g4238.md) the same things.

<a name="romans_2_2"></a>Romans 2:2

But we are [eidō](../../strongs/g/g1492.md) that the [krima](../../strongs/g/g2917.md) of [theos](../../strongs/g/g2316.md) is according to [alētheia](../../strongs/g/g225.md) against them which [prassō](../../strongs/g/g4238.md) [toioutos](../../strongs/g/g5108.md).

<a name="romans_2_3"></a>Romans 2:3

And [logizomai](../../strongs/g/g3049.md) thou this, [ō](../../strongs/g/g5599.md) [anthrōpos](../../strongs/g/g444.md), that [krinō](../../strongs/g/g2919.md) them which [prassō](../../strongs/g/g4238.md) such things, and [poieō](../../strongs/g/g4160.md) the same, that thou shalt [ekpheugō](../../strongs/g/g1628.md) the [krima](../../strongs/g/g2917.md) of [theos](../../strongs/g/g2316.md)?

<a name="romans_2_4"></a>Romans 2:4

Or [kataphroneō](../../strongs/g/g2706.md) thou the [ploutos](../../strongs/g/g4149.md) of his [chrēstotēs](../../strongs/g/g5544.md) and [anochē](../../strongs/g/g463.md) and [makrothymia](../../strongs/g/g3115.md); not [agnoeō](../../strongs/g/g50.md) that the [chrēstos](../../strongs/g/g5543.md) of [theos](../../strongs/g/g2316.md) [agō](../../strongs/g/g71.md) thee to [metanoia](../../strongs/g/g3341.md)?

<a name="romans_2_5"></a>Romans 2:5

But after thy [sklerotes](../../strongs/g/g4643.md) and [ametanoetos](../../strongs/g/g279.md) [kardia](../../strongs/g/g2588.md) [thēsaurizō](../../strongs/g/g2343.md) unto thyself [orgē](../../strongs/g/g3709.md) against the [hēmera](../../strongs/g/g2250.md) of [orgē](../../strongs/g/g3709.md) and [apokalypsis](../../strongs/g/g602.md) of the [dikaiokrisia](../../strongs/g/g1341.md) of [theos](../../strongs/g/g2316.md);

<a name="romans_2_6"></a>Romans 2:6

Who will [apodidōmi](../../strongs/g/g591.md) to [hekastos](../../strongs/g/g1538.md) according to his [ergon](../../strongs/g/g2041.md):

<a name="romans_2_7"></a>Romans 2:7

To them who by [hypomonē](../../strongs/g/g5281.md) in [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md) [zēteō](../../strongs/g/g2212.md) for [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md) and [aphtharsia](../../strongs/g/g861.md), [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md):

<a name="romans_2_8"></a>Romans 2:8

But unto them that are [eritheia](../../strongs/g/g2052.md), and [apeitheō](../../strongs/g/g544.md) [men](../../strongs/g/g3303.md) the [alētheia](../../strongs/g/g225.md)h, but [peithō](../../strongs/g/g3982.md) [adikia](../../strongs/g/g93.md), [thymos](../../strongs/g/g2372.md) and [orgē](../../strongs/g/g3709.md),

<a name="romans_2_9"></a>Romans 2:9

[thlipsis](../../strongs/g/g2347.md) and [stenochoria](../../strongs/g/g4730.md), upon every [psychē](../../strongs/g/g5590.md) of [anthrōpos](../../strongs/g/g444.md) that [katergazomai](../../strongs/g/g2716.md) [kakos](../../strongs/g/g2556.md), of the [Ioudaios](../../strongs/g/g2453.md) first, and also of the [Hellēn](../../strongs/g/g1672.md);

<a name="romans_2_10"></a>Romans 2:10

But [doxa](../../strongs/g/g1391.md), [timē](../../strongs/g/g5092.md), and [eirēnē](../../strongs/g/g1515.md), to [pas](../../strongs/g/g3956.md) that [ergazomai](../../strongs/g/g2038.md) [agathos](../../strongs/g/g18.md), to the [Ioudaios](../../strongs/g/g2453.md) first, and also to the [Hellēn](../../strongs/g/g1672.md):

<a name="romans_2_11"></a>Romans 2:11

For there is no [prosōpolēmpsia](../../strongs/g/g4382.md) with [theos](../../strongs/g/g2316.md).

<a name="romans_2_12"></a>Romans 2:12

For as many as have [hamartanō](../../strongs/g/g264.md) [anomōs](../../strongs/g/g460.md) shall also [apollymi](../../strongs/g/g622.md) [anomōs](../../strongs/g/g460.md): and as many as have [hamartanō](../../strongs/g/g264.md) in the [nomos](../../strongs/g/g3551.md) shall be [krinō](../../strongs/g/g2919.md) by the [nomos](../../strongs/g/g3551.md);

<a name="romans_2_13"></a>Romans 2:13

(For not the [akroatēs](../../strongs/g/g202.md) of the [nomos](../../strongs/g/g3551.md) are [dikaios](../../strongs/g/g1342.md) before [theos](../../strongs/g/g2316.md), but the [poiētēs](../../strongs/g/g4163.md) of the [nomos](../../strongs/g/g3551.md) shall be [dikaioō](../../strongs/g/g1344.md).

<a name="romans_2_14"></a>Romans 2:14

For when the [ethnos](../../strongs/g/g1484.md), which have not the [nomos](../../strongs/g/g3551.md), [poieō](../../strongs/g/g4160.md) by [physis](../../strongs/g/g5449.md) the things in the [nomos](../../strongs/g/g3551.md), these, having not the [nomos](../../strongs/g/g3551.md), are a [nomos](../../strongs/g/g3551.md) unto themselves:

<a name="romans_2_15"></a>Romans 2:15

Which [endeiknymi](../../strongs/g/g1731.md) the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md) [graptos](../../strongs/g/g1123.md) in their [kardia](../../strongs/g/g2588.md), their [syneidēsis](../../strongs/g/g4893.md) also [symmartyreō](../../strongs/g/g4828.md), and [logismos](../../strongs/g/g3053.md) the [katēgoreō](../../strongs/g/g2723.md) or else [apologeomai](../../strongs/g/g626.md) [allēlōn](../../strongs/g/g240.md);) [^1]

<a name="romans_2_16"></a>Romans 2:16

In the [hēmera](../../strongs/g/g2250.md) when [theos](../../strongs/g/g2316.md) shall [krinō](../../strongs/g/g2919.md) the [kryptos](../../strongs/g/g2927.md) of [anthrōpos](../../strongs/g/g444.md) by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) according to my [euaggelion](../../strongs/g/g2098.md).

<a name="romans_2_17"></a>Romans 2:17

[ide](../../strongs/g/g2396.md), thou art [eponomazō](../../strongs/g/g2028.md) an [Ioudaios](../../strongs/g/g2453.md), and [epanapauomai](../../strongs/g/g1879.md) in the [nomos](../../strongs/g/g3551.md), and thy [kauchaomai](../../strongs/g/g2744.md) of [theos](../../strongs/g/g2316.md),

<a name="romans_2_18"></a>Romans 2:18

And [ginōskō](../../strongs/g/g1097.md) his [thelēma](../../strongs/g/g2307.md), and [dokimazō](../../strongs/g/g1381.md) the [diapherō](../../strongs/g/g1308.md), being [katēcheō](../../strongs/g/g2727.md) out of the [nomos](../../strongs/g/g3551.md);

<a name="romans_2_19"></a>Romans 2:19

And art [peithō](../../strongs/g/g3982.md) that thou thyself art a [hodēgos](../../strongs/g/g3595.md) of the [typhlos](../../strongs/g/g5185.md), a [phōs](../../strongs/g/g5457.md) of them which are in [skotos](../../strongs/g/g4655.md),

<a name="romans_2_20"></a>Romans 2:20

A [paideutēs](../../strongs/g/g3810.md) of the [aphrōn](../../strongs/g/g878.md), a [didaskalos](../../strongs/g/g1320.md) of [nēpios](../../strongs/g/g3516.md), which hast the [morphōsis](../../strongs/g/g3446.md) of [gnōsis](../../strongs/g/g1108.md) and of the [alētheia](../../strongs/g/g225.md) in the [nomos](../../strongs/g/g3551.md).

<a name="romans_2_21"></a>Romans 2:21

Thou therefore which [didaskō](../../strongs/g/g1321.md) another, [didaskō](../../strongs/g/g1321.md) thou not thyself? thou that [kēryssō](../../strongs/g/g2784.md) should not [kleptō](../../strongs/g/g2813.md), dost thou [kleptō](../../strongs/g/g2813.md)?

<a name="romans_2_22"></a>Romans 2:22

Thou that [legō](../../strongs/g/g3004.md) should not [moicheuō](../../strongs/g/g3431.md), dost thou [moicheuō](../../strongs/g/g3431.md)? thou that [bdelyssō](../../strongs/g/g948.md) [eidōlon](../../strongs/g/g1497.md), dost thou [hierosyleō](../../strongs/g/g2416.md)?

<a name="romans_2_23"></a>Romans 2:23

Thou that [kauchaomai](../../strongs/g/g2744.md) of the [nomos](../../strongs/g/g3551.md), through [parabasis](../../strongs/g/g3847.md) the [nomos](../../strongs/g/g3551.md) [atimazō](../../strongs/g/g818.md) thou [theos](../../strongs/g/g2316.md)?

<a name="romans_2_24"></a>Romans 2:24

For the [onoma](../../strongs/g/g3686.md) of [theos](../../strongs/g/g2316.md) is [blasphēmeō](../../strongs/g/g987.md) among the [ethnos](../../strongs/g/g1484.md) through you, as it is [graphō](../../strongs/g/g1125.md).

<a name="romans_2_25"></a>Romans 2:25

For [peritomē](../../strongs/g/g4061.md) verily [ōpheleō](../../strongs/g/g5623.md), if thou [prassō](../../strongs/g/g4238.md) the [nomos](../../strongs/g/g3551.md): but if thou be a [parabatēs](../../strongs/g/g3848.md) of the [nomos](../../strongs/g/g3551.md), thy [peritomē](../../strongs/g/g4061.md) is made [akrobystia](../../strongs/g/g203.md).

<a name="romans_2_26"></a>Romans 2:26

Therefore if the [akrobystia](../../strongs/g/g203.md) [phylassō](../../strongs/g/g5442.md) the [dikaiōma](../../strongs/g/g1345.md) of the [nomos](../../strongs/g/g3551.md), shall not his [akrobystia](../../strongs/g/g203.md) be [logizomai](../../strongs/g/g3049.md) for [peritomē](../../strongs/g/g4061.md)?

<a name="romans_2_27"></a>Romans 2:27

And shall not [akrobystia](../../strongs/g/g203.md) which is by [physis](../../strongs/g/g5449.md), if it [teleō](../../strongs/g/g5055.md) the [nomos](../../strongs/g/g3551.md), [krinō](../../strongs/g/g2919.md) thee, who by the [gramma](../../strongs/g/g1121.md) and [peritomē](../../strongs/g/g4061.md) dost [parabatēs](../../strongs/g/g3848.md) the [nomos](../../strongs/g/g3551.md)?

<a name="romans_2_28"></a>Romans 2:28

For he is not an [Ioudaios](../../strongs/g/g2453.md), [en](../../strongs/g/g1722.md) [phaneros](../../strongs/g/g5318.md); neither is that [peritomē](../../strongs/g/g4061.md), [en](../../strongs/g/g1722.md) [phaneros](../../strongs/g/g5318.md) in the [sarx](../../strongs/g/g4561.md):

<a name="romans_2_29"></a>Romans 2:29

But he is an [Ioudaios](../../strongs/g/g2453.md), [en](../../strongs/g/g1722.md) [kryptos](../../strongs/g/g2927.md); and [peritomē](../../strongs/g/g4061.md) is that of the [kardia](../../strongs/g/g2588.md), in the [pneuma](../../strongs/g/g4151.md), and not in the [gramma](../../strongs/g/g1121.md); whose [epainos](../../strongs/g/g1868.md) is not of [anthrōpos](../../strongs/g/g444.md), but of [theos](../../strongs/g/g2316.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 1](romans_1.md) - [Romans 3](romans_3.md)

---

[^1]: [Romans 2:15 Commentary](../../commentary/romans/romans_2_commentary.md#romans_2_15)
