# [Romans 8](https://www.blueletterbible.org/kjv/rom/8/1/rl1/s_1050001)

<a name="romans_8_1"></a>Romans 8:1

Therefore now no [katakrima](../../strongs/g/g2631.md) to them which are in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), who [peripateō](../../strongs/g/g4043.md) not after the [sarx](../../strongs/g/g4561.md), but after the [pneuma](../../strongs/g/g4151.md). [^1]

<a name="romans_8_2"></a>Romans 8:2

For the [nomos](../../strongs/g/g3551.md) of the [pneuma](../../strongs/g/g4151.md) of [zōē](../../strongs/g/g2222.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) hath [eleutheroō](../../strongs/g/g1659.md) me from the [nomos](../../strongs/g/g3551.md) of [hamartia](../../strongs/g/g266.md) and [thanatos](../../strongs/g/g2288.md).

<a name="romans_8_3"></a>Romans 8:3

For what the [nomos](../../strongs/g/g3551.md) [adynatos](../../strongs/g/g102.md), in that it was [astheneō](../../strongs/g/g770.md) through the [sarx](../../strongs/g/g4561.md), [theos](../../strongs/g/g2316.md) [pempō](../../strongs/g/g3992.md) his own [huios](../../strongs/g/g5207.md) in the [homoiōma](../../strongs/g/g3667.md) of [hamartia](../../strongs/g/g266.md) [sarx](../../strongs/g/g4561.md), and for [hamartia](../../strongs/g/g266.md), [katakrinō](../../strongs/g/g2632.md) [hamartia](../../strongs/g/g266.md) in the [sarx](../../strongs/g/g4561.md):

<a name="romans_8_4"></a>Romans 8:4

That the [dikaiōma](../../strongs/g/g1345.md) of the [nomos](../../strongs/g/g3551.md) might be [plēroō](../../strongs/g/g4137.md) in us, who [peripateō](../../strongs/g/g4043.md) not after the [sarx](../../strongs/g/g4561.md), but after the [pneuma](../../strongs/g/g4151.md).

<a name="romans_8_5"></a>Romans 8:5

For they that are after the [sarx](../../strongs/g/g4561.md) [phroneō](../../strongs/g/g5426.md) the things of the [sarx](../../strongs/g/g4561.md); but they that are after the [pneuma](../../strongs/g/g4151.md) the things of the [pneuma](../../strongs/g/g4151.md).

<a name="romans_8_6"></a>Romans 8:6

For to be [sarx](../../strongs/g/g4561.md) [phronēma](../../strongs/g/g5427.md) [thanatos](../../strongs/g/g2288.md); but to be [pneuma](../../strongs/g/g4151.md) [phronēma](../../strongs/g/g5427.md) [zōē](../../strongs/g/g2222.md) and [eirēnē](../../strongs/g/g1515.md).

<a name="romans_8_7"></a>Romans 8:7

Because the [sarx](../../strongs/g/g4561.md) [phronēma](../../strongs/g/g5427.md) is [echthra](../../strongs/g/g2189.md) against [theos](../../strongs/g/g2316.md): for it is not [hypotassō](../../strongs/g/g5293.md) to the [nomos](../../strongs/g/g3551.md) of [theos](../../strongs/g/g2316.md), neither indeed can be.

<a name="romans_8_8"></a>Romans 8:8

So then they that are in the [sarx](../../strongs/g/g4561.md) cannot [areskō](../../strongs/g/g700.md) [theos](../../strongs/g/g2316.md).

<a name="romans_8_9"></a>Romans 8:9

But ye are not in the [sarx](../../strongs/g/g4561.md), but in the [pneuma](../../strongs/g/g4151.md), [ei per](../../strongs/g/g1512.md) that the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [oikeō](../../strongs/g/g3611.md) in you. Now [ei tis](../../strongs/g/g1536.md) have not the [pneuma](../../strongs/g/g4151.md) of [Christos](../../strongs/g/g5547.md), he is none of his.

<a name="romans_8_10"></a>Romans 8:10

And if [Christos](../../strongs/g/g5547.md) be in you, the [sōma](../../strongs/g/g4983.md) is [nekros](../../strongs/g/g3498.md) because of [hamartia](../../strongs/g/g266.md); but the [pneuma](../../strongs/g/g4151.md) is [zōē](../../strongs/g/g2222.md) because of [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_8_11"></a>Romans 8:11

But if the [pneuma](../../strongs/g/g4151.md) of him that [egeirō](../../strongs/g/g1453.md) [Iēsous](../../strongs/g/g2424.md) from the [nekros](../../strongs/g/g3498.md) [oikeō](../../strongs/g/g3611.md) in you, he that [egeirō](../../strongs/g/g1453.md) [Christos](../../strongs/g/g5547.md) from the [nekros](../../strongs/g/g3498.md) shall also [zōopoieō](../../strongs/g/g2227.md) your [thnētos](../../strongs/g/g2349.md) [sōma](../../strongs/g/g4983.md) by his [pneuma](../../strongs/g/g4151.md) that [enoikeō](../../strongs/g/g1774.md) in you.

<a name="romans_8_12"></a>Romans 8:12

Therefore, [adelphos](../../strongs/g/g80.md), we are [opheiletēs](../../strongs/g/g3781.md), not to the [sarx](../../strongs/g/g4561.md), to [zaō](../../strongs/g/g2198.md) after the [sarx](../../strongs/g/g4561.md).

<a name="romans_8_13"></a>Romans 8:13

For if ye [zaō](../../strongs/g/g2198.md) after the [sarx](../../strongs/g/g4561.md), ye shall [apothnēskō](../../strongs/g/g599.md): but if ye through the [pneuma](../../strongs/g/g4151.md) do [thanatoō](../../strongs/g/g2289.md) the [praxis](../../strongs/g/g4234.md) of the [sōma](../../strongs/g/g4983.md), ye shall [zaō](../../strongs/g/g2198.md).

<a name="romans_8_14"></a>Romans 8:14

For as many as are [agō](../../strongs/g/g71.md) by the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md), they are the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_8_15"></a>Romans 8:15

For ye have not [lambanō](../../strongs/g/g2983.md) the [pneuma](../../strongs/g/g4151.md) of [douleia](../../strongs/g/g1397.md) again to [phobos](../../strongs/g/g5401.md); but ye have [lambanō](../../strongs/g/g2983.md) the [pneuma](../../strongs/g/g4151.md) of [huiothesia](../../strongs/g/g5206.md), whereby we [krazō](../../strongs/g/g2896.md), [abba](../../strongs/g/g5.md), [patēr](../../strongs/g/g3962.md).

<a name="romans_8_16"></a>Romans 8:16

The [pneuma](../../strongs/g/g4151.md) itself [symmartyreō](../../strongs/g/g4828.md) with our [pneuma](../../strongs/g/g4151.md), that we are the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md):

<a name="romans_8_17"></a>Romans 8:17

And if [teknon](../../strongs/g/g5043.md), then [klēronomos](../../strongs/g/g2818.md); [klēronomos](../../strongs/g/g2818.md) of [theos](../../strongs/g/g2316.md), and [sygklēronomos](../../strongs/g/g4789.md) with [Christos](../../strongs/g/g5547.md); [ei per](../../strongs/g/g1512.md) that we [sympaschō](../../strongs/g/g4841.md) him, that we may be also [syndoxazō](../../strongs/g/g4888.md).

<a name="romans_8_18"></a>Romans 8:18

For I [logizomai](../../strongs/g/g3049.md) that the [pathēma](../../strongs/g/g3804.md) of this [nyn](../../strongs/g/g3568.md) [kairos](../../strongs/g/g2540.md) are not [axios](../../strongs/g/g514.md) with the [doxa](../../strongs/g/g1391.md) which shall be [apokalyptō](../../strongs/g/g601.md) in us.

<a name="romans_8_19"></a>Romans 8:19

For the [apokaradokia](../../strongs/g/g603.md) of the [ktisis](../../strongs/g/g2937.md) [apekdechomai](../../strongs/g/g553.md) for the [apokalypsis](../../strongs/g/g602.md) of the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_8_20"></a>Romans 8:20

For the [ktisis](../../strongs/g/g2937.md) was [hypotassō](../../strongs/g/g5293.md) to [mataiotēs](../../strongs/g/g3153.md), not [hekōn](../../strongs/g/g1635.md), but [dia](../../strongs/g/g1223.md) him who hath [hypotassō](../../strongs/g/g5293.md) the same in [elpis](../../strongs/g/g1680.md),

<a name="romans_8_21"></a>Romans 8:21

Because the [ktisis](../../strongs/g/g2937.md) itself also shall be [eleutheroō](../../strongs/g/g1659.md) from the [douleia](../../strongs/g/g1397.md) of [phthora](../../strongs/g/g5356.md) into the [doxa](../../strongs/g/g1391.md) [eleutheria](../../strongs/g/g1657.md) of the [teknon](../../strongs/g/g5043.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_8_22"></a>Romans 8:22

For we [eidō](../../strongs/g/g1492.md) that the [pas](../../strongs/g/g3956.md) [ktisis](../../strongs/g/g2937.md) [systenazō](../../strongs/g/g4959.md) and [synōdinō](../../strongs/g/g4944.md) until now.

<a name="romans_8_23"></a>Romans 8:23

And not only, but ourselves also, which have the [aparchē](../../strongs/g/g536.md) of the [pneuma](../../strongs/g/g4151.md), even we ourselves [stenazō](../../strongs/g/g4727.md) within ourselves, [apekdechomai](../../strongs/g/g553.md) for the [huiothesia](../../strongs/g/g5206.md), the [apolytrōsis](../../strongs/g/g629.md) of our [sōma](../../strongs/g/g4983.md).

<a name="romans_8_24"></a>Romans 8:24

For we are [sōzō](../../strongs/g/g4982.md) by [elpis](../../strongs/g/g1680.md): but [elpis](../../strongs/g/g1680.md) that is [blepō](../../strongs/g/g991.md) is not [elpis](../../strongs/g/g1680.md): for what a man [blepō](../../strongs/g/g991.md), why doth he yet [elpizō](../../strongs/g/g1679.md)?

<a name="romans_8_25"></a>Romans 8:25

But if we [elpizō](../../strongs/g/g1679.md) for that we [blepō](../../strongs/g/g991.md) not, do we with [hypomonē](../../strongs/g/g5281.md) [apekdechomai](../../strongs/g/g553.md).

<a name="romans_8_26"></a>Romans 8:26

[hōsautōs](../../strongs/g/g5615.md) the [pneuma](../../strongs/g/g4151.md) also [synantilambanomai](../../strongs/g/g4878.md) our [astheneia](../../strongs/g/g769.md): for we [eidō](../../strongs/g/g1492.md) not what we should [proseuchomai](../../strongs/g/g4336.md) for as we [dei](../../strongs/g/g1163.md): but the [pneuma](../../strongs/g/g4151.md) itself [hyperentygchanō](../../strongs/g/g5241.md) for us with [stenagmos](../../strongs/g/g4726.md) [alalētos](../../strongs/g/g215.md).

<a name="romans_8_27"></a>Romans 8:27

And he that [eraunaō](../../strongs/g/g2045.md) the [kardia](../../strongs/g/g2588.md) [eidō](../../strongs/g/g1492.md) what is the [phronēma](../../strongs/g/g5427.md) of the [pneuma](../../strongs/g/g4151.md), because he [entygchanō](../../strongs/g/g1793.md) for the [hagios](../../strongs/g/g40.md) according to of [theos](../../strongs/g/g2316.md).

<a name="romans_8_28"></a>Romans 8:28

And we [eidō](../../strongs/g/g1492.md) that all things [synergeō](../../strongs/g/g4903.md) for [agathos](../../strongs/g/g18.md) to them that [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md), to them who are the [klētos](../../strongs/g/g2822.md) according to his [prothesis](../../strongs/g/g4286.md).

<a name="romans_8_29"></a>Romans 8:29

For whom he did [proginōskō](../../strongs/g/g4267.md), he also did [proorizō](../../strongs/g/g4309.md) to be [symmorphos](../../strongs/g/g4832.md) to the [eikōn](../../strongs/g/g1504.md) of his [huios](../../strongs/g/g5207.md), that he might be the [prōtotokos](../../strongs/g/g4416.md) among [polys](../../strongs/g/g4183.md) [adelphos](../../strongs/g/g80.md).

<a name="romans_8_30"></a>Romans 8:30

Moreover whom he did [proorizō](../../strongs/g/g4309.md), them he also [kaleō](../../strongs/g/g2564.md): and whom he [kaleō](../../strongs/g/g2564.md), them he also [dikaioō](../../strongs/g/g1344.md): and whom he [dikaioō](../../strongs/g/g1344.md), them he also [doxazō](../../strongs/g/g1392.md).

<a name="romans_8_31"></a>Romans 8:31

What shall we then [eipon](../../strongs/g/g2046.md) to these things? If [theos](../../strongs/g/g2316.md) be for us, who can be against us?

<a name="romans_8_32"></a>Romans 8:32

He that [pheidomai](../../strongs/g/g5339.md) not his own [huios](../../strongs/g/g5207.md), but [paradidōmi](../../strongs/g/g3860.md) him up for us all, how shall he not with him also [charizomai](../../strongs/g/g5483.md) us all things?

<a name="romans_8_33"></a>Romans 8:33

Who shall [egkaleō](../../strongs/g/g1458.md) to the [kata](../../strongs/g/g2596.md) of [theos](../../strongs/g/g2316.md) [eklektos](../../strongs/g/g1588.md)? [theos](../../strongs/g/g2316.md) that [dikaioō](../../strongs/g/g1344.md).

<a name="romans_8_34"></a>Romans 8:34

Who he that [katakrinō](../../strongs/g/g2632.md)? [Christos](../../strongs/g/g5547.md) that [apothnēskō](../../strongs/g/g599.md), yea rather, that is [egeirō](../../strongs/g/g1453.md), who is even at the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md), who also [entygchanō](../../strongs/g/g1793.md) for us.

<a name="romans_8_35"></a>Romans 8:35

Who shall [chōrizō](../../strongs/g/g5563.md) us from the [agapē](../../strongs/g/g26.md) of [Christos](../../strongs/g/g5547.md)? shall [thlipsis](../../strongs/g/g2347.md), or [stenochoria](../../strongs/g/g4730.md), or [diōgmos](../../strongs/g/g1375.md), or [limos](../../strongs/g/g3042.md), or [gymnotēs](../../strongs/g/g1132.md), or [kindynos](../../strongs/g/g2794.md), or [machaira](../../strongs/g/g3162.md)?

<a name="romans_8_36"></a>Romans 8:36

As it is [graphō](../../strongs/g/g1125.md), For thy sake we are [thanatoō](../../strongs/g/g2289.md) all the [hēmera](../../strongs/g/g2250.md); we are [logizomai](../../strongs/g/g3049.md) as [probaton](../../strongs/g/g4263.md) for the [sphagē](../../strongs/g/g4967.md).

<a name="romans_8_37"></a>Romans 8:37

Nay, in all these things we are [hypernikaō](../../strongs/g/g5245.md) through him that [agapaō](../../strongs/g/g25.md) us.

<a name="romans_8_38"></a>Romans 8:38

For I am [peithō](../../strongs/g/g3982.md), that neither [thanatos](../../strongs/g/g2288.md), nor [zōē](../../strongs/g/g2222.md), nor [aggelos](../../strongs/g/g32.md), nor [archē](../../strongs/g/g746.md), nor [dynamis](../../strongs/g/g1411.md), nor [enistēmi](../../strongs/g/g1764.md), nor [mellō](../../strongs/g/g3195.md),

<a name="romans_8_39"></a>Romans 8:39

Nor [hypsōma](../../strongs/g/g5313.md), nor [bathos](../../strongs/g/g899.md), nor any other [ktisis](../../strongs/g/g2937.md), shall be able to [chōrizō](../../strongs/g/g5563.md) us from the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md), which is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 7](romans_7.md) - [Romans 9](romans_9.md)

---

[^1]: [Romans 8:1 Commentary](../../commentary/romans/romans_8_commentary.md#romans_8_1)
