# [Romans 6](https://www.blueletterbible.org/kjv/rom/6/1/rl1/s_1050001)

<a name="romans_6_1"></a>Romans 6:1

What shall we [eipon](../../strongs/g/g2046.md) then? Shall we [epimenō](../../strongs/g/g1961.md) in [hamartia](../../strongs/g/g266.md), that [charis](../../strongs/g/g5485.md) may [pleonazō](../../strongs/g/g4121.md)?

<a name="romans_6_2"></a>Romans 6:2

[mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md). How shall we, that are [apothnēskō](../../strongs/g/g599.md) to [hamartia](../../strongs/g/g266.md), [zaō](../../strongs/g/g2198.md) any longer therein?

<a name="romans_6_3"></a>Romans 6:3

[agnoeō](../../strongs/g/g50.md) ye, that so many of us as were [baptizō](../../strongs/g/g907.md) into [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) were [baptizō](../../strongs/g/g907.md) into his [thanatos](../../strongs/g/g2288.md)?

<a name="romans_6_4"></a>Romans 6:4

Therefore we are [synthaptō](../../strongs/g/g4916.md) him by [baptisma](../../strongs/g/g908.md) into [thanatos](../../strongs/g/g2288.md): that like as [Christos](../../strongs/g/g5547.md) was [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md) by the [doxa](../../strongs/g/g1391.md) of the [patēr](../../strongs/g/g3962.md), even so we also should [peripateō](../../strongs/g/g4043.md) in [kainotēs](../../strongs/g/g2538.md) of [zōē](../../strongs/g/g2222.md).

<a name="romans_6_5"></a>Romans 6:5

For if we have been [symphytos](../../strongs/g/g4854.md) in the [homoiōma](../../strongs/g/g3667.md) of his [thanatos](../../strongs/g/g2288.md), we shall be also of [anastasis](../../strongs/g/g386.md):

<a name="romans_6_6"></a>Romans 6:6

[ginōskō](../../strongs/g/g1097.md) this, that our [palaios](../../strongs/g/g3820.md) [anthrōpos](../../strongs/g/g444.md) is [systauroō](../../strongs/g/g4957.md) him, that the [sōma](../../strongs/g/g4983.md) of [hamartia](../../strongs/g/g266.md) might be [katargeō](../../strongs/g/g2673.md), that henceforth we should not [douleuō](../../strongs/g/g1398.md) [hamartia](../../strongs/g/g266.md).

<a name="romans_6_7"></a>Romans 6:7

For he that is [apothnēskō](../../strongs/g/g599.md) is [dikaioō](../../strongs/g/g1344.md) from [hamartia](../../strongs/g/g266.md).

<a name="romans_6_8"></a>Romans 6:8

Now if we be [apothnēskō](../../strongs/g/g599.md) with [Christos](../../strongs/g/g5547.md), we [pisteuō](../../strongs/g/g4100.md) that we shall also [syzaō](../../strongs/g/g4800.md) him:

<a name="romans_6_9"></a>Romans 6:9

[eidō](../../strongs/g/g1492.md) that [Christos](../../strongs/g/g5547.md) being [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md) [apothnēskō](../../strongs/g/g599.md) no more; [thanatos](../../strongs/g/g2288.md) hath no more [kyrieuō](../../strongs/g/g2961.md) him.

<a name="romans_6_10"></a>Romans 6:10

For in that he [apothnēskō](../../strongs/g/g599.md), he [apothnēskō](../../strongs/g/g599.md) unto [hamartia](../../strongs/g/g266.md) [ephapax](../../strongs/g/g2178.md): but in that he [zaō](../../strongs/g/g2198.md), he [zaō](../../strongs/g/g2198.md) unto [theos](../../strongs/g/g2316.md).

<a name="romans_6_11"></a>Romans 6:11

Likewise [logizomai](../../strongs/g/g3049.md) ye also yourselves to be [nekros](../../strongs/g/g3498.md) indeed unto [hamartia](../../strongs/g/g266.md), but [zaō](../../strongs/g/g2198.md) unto [theos](../../strongs/g/g2316.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md).

<a name="romans_6_12"></a>Romans 6:12

Let not [hamartia](../../strongs/g/g266.md) therefore [basileuō](../../strongs/g/g936.md) in your [thnētos](../../strongs/g/g2349.md) [sōma](../../strongs/g/g4983.md), that ye should [hypakouō](../../strongs/g/g5219.md) it in the [epithymia](../../strongs/g/g1939.md) thereof.

<a name="romans_6_13"></a>Romans 6:13

Neither [paristēmi](../../strongs/g/g3936.md) your [melos](../../strongs/g/g3196.md) as [hoplon](../../strongs/g/g3696.md) of [adikia](../../strongs/g/g93.md) unto [hamartia](../../strongs/g/g266.md): but [paristēmi](../../strongs/g/g3936.md) yourselves unto [theos](../../strongs/g/g2316.md), as those that are [zaō](../../strongs/g/g2198.md) from the [nekros](../../strongs/g/g3498.md), and your [melos](../../strongs/g/g3196.md) as [hoplon](../../strongs/g/g3696.md) of [dikaiosynē](../../strongs/g/g1343.md) unto [theos](../../strongs/g/g2316.md).

<a name="romans_6_14"></a>Romans 6:14

For [hamartia](../../strongs/g/g266.md) shall not have [kyrieuō](../../strongs/g/g2961.md) over you: for ye are not under the [nomos](../../strongs/g/g3551.md), but under [charis](../../strongs/g/g5485.md).

<a name="romans_6_15"></a>Romans 6:15

What then? shall we [hamartanō](../../strongs/g/g264.md), because we are not under the [nomos](../../strongs/g/g3551.md), but under [charis](../../strongs/g/g5485.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md).

<a name="romans_6_16"></a>Romans 6:16

[eidō](../../strongs/g/g1492.md) ye not, that to whom ye [paristēmi](../../strongs/g/g3936.md) yourselves [doulos](../../strongs/g/g1401.md) to [hypakoē](../../strongs/g/g5218.md), his [doulos](../../strongs/g/g1401.md) ye are to whom ye [hypakouō](../../strongs/g/g5219.md); whether of [hamartia](../../strongs/g/g266.md) unto [thanatos](../../strongs/g/g2288.md), or of [hypakoē](../../strongs/g/g5218.md) unto [dikaiosynē](../../strongs/g/g1343.md)?

<a name="romans_6_17"></a>Romans 6:17

But [theos](../../strongs/g/g2316.md) be [charis](../../strongs/g/g5485.md), that ye were the [doulos](../../strongs/g/g1401.md) of [hamartia](../../strongs/g/g266.md), but ye have [hypakouō](../../strongs/g/g5219.md) from the [kardia](../../strongs/g/g2588.md) that [typos](../../strongs/g/g5179.md) of [didachē](../../strongs/g/g1322.md) which was [paradidōmi](../../strongs/g/g3860.md) you.

<a name="romans_6_18"></a>Romans 6:18

Being then [eleutheroō](../../strongs/g/g1659.md) from [hamartia](../../strongs/g/g266.md), ye became the [douloō](../../strongs/g/g1402.md) of [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_6_19"></a>Romans 6:19

I [legō](../../strongs/g/g3004.md) [anthrōpinos](../../strongs/g/g442.md) because of the [astheneia](../../strongs/g/g769.md) of your [sarx](../../strongs/g/g4561.md): for as ye have [paristēmi](../../strongs/g/g3936.md) your [melos](../../strongs/g/g3196.md) [doulos](../../strongs/g/g1401.md) to [akatharsia](../../strongs/g/g167.md) and to [anomia](../../strongs/g/g458.md) unto [anomia](../../strongs/g/g458.md); even so now [paristēmi](../../strongs/g/g3936.md) your [melos](../../strongs/g/g3196.md) [doulos](../../strongs/g/g1401.md) to [dikaiosynē](../../strongs/g/g1343.md) unto [hagiasmos](../../strongs/g/g38.md).

<a name="romans_6_20"></a>Romans 6:20

For when ye were the [doulos](../../strongs/g/g1401.md) of [hamartia](../../strongs/g/g266.md), ye were [eleutheros](../../strongs/g/g1658.md) from [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_6_21"></a>Romans 6:21

What [karpos](../../strongs/g/g2590.md) had ye then in those things whereof ye are now [epaischynomai](../../strongs/g/g1870.md)? for the [telos](../../strongs/g/g5056.md) of those things is [thanatos](../../strongs/g/g2288.md).

<a name="romans_6_22"></a>Romans 6:22

But [nyni](../../strongs/g/g3570.md) being [eleutheroō](../../strongs/g/g1659.md) from [hamartia](../../strongs/g/g266.md), and [douloō](../../strongs/g/g1402.md) to [theos](../../strongs/g/g2316.md), ye have your [karpos](../../strongs/g/g2590.md) unto [hagiasmos](../../strongs/g/g38.md), and the [telos](../../strongs/g/g5056.md) [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md).

<a name="romans_6_23"></a>Romans 6:23

For the [opsōnion](../../strongs/g/g3800.md) of [hamartia](../../strongs/g/g266.md) [thanatos](../../strongs/g/g2288.md); but the [charisma](../../strongs/g/g5486.md) of [theos](../../strongs/g/g2316.md) is [aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 5](romans_5.md) - [Romans 7](romans_7.md)