# [Romans 7](https://www.blueletterbible.org/kjv/rom/7/1/s_1053001)

<a name="romans_7_1"></a>Romans 7:1

[agnoeō](../../strongs/g/g50.md) ye, [adelphos](../../strongs/g/g80.md), (for I [laleō](../../strongs/g/g2980.md) to them that [ginōskō](../../strongs/g/g1097.md) the [nomos](../../strongs/g/g3551.md),) how that the [nomos](../../strongs/g/g3551.md) hath [kyrieuō](../../strongs/g/g2961.md) an [anthrōpos](../../strongs/g/g444.md) [epi](../../strongs/g/g1909.md) [hosos](../../strongs/g/g3745.md)[chronos](../../strongs/g/g5550.md) he [zaō](../../strongs/g/g2198.md)?

<a name="romans_7_2"></a>Romans 7:2

For the [gynē](../../strongs/g/g1135.md) [hypandros](../../strongs/g/g5220.md) is [deō](../../strongs/g/g1210.md) by the [nomos](../../strongs/g/g3551.md) to her [anēr](../../strongs/g/g435.md) [zaō](../../strongs/g/g2198.md); but if the [anēr](../../strongs/g/g435.md) be [apothnēskō](../../strongs/g/g599.md), she is [katargeō](../../strongs/g/g2673.md) from the [nomos](../../strongs/g/g3551.md) of her [anēr](../../strongs/g/g435.md).

<a name="romans_7_3"></a>Romans 7:3

So then if, while her [anēr](../../strongs/g/g435.md) [zaō](../../strongs/g/g2198.md)h, she [ginomai](../../strongs/g/g1096.md) to another [anēr](../../strongs/g/g435.md), she shall be [chrēmatizō](../../strongs/g/g5537.md) [moichalis](../../strongs/g/g3428.md): but if her [anēr](../../strongs/g/g435.md) be [apothnēskō](../../strongs/g/g599.md), she is [eleutheros](../../strongs/g/g1658.md) from that [nomos](../../strongs/g/g3551.md); so that she is no [moichalis](../../strongs/g/g3428.md), though she [ginomai](../../strongs/g/g1096.md) to another [anēr](../../strongs/g/g435.md).

<a name="romans_7_4"></a>Romans 7:4

Wherefore, my [adelphos](../../strongs/g/g80.md), ye also are [thanatoō](../../strongs/g/g2289.md) to the [nomos](../../strongs/g/g3551.md) by the [sōma](../../strongs/g/g4983.md) of [Christos](../../strongs/g/g5547.md); that ye should be [ginomai](../../strongs/g/g1096.md) to another, even to him who is [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), that we should [karpophoreō](../../strongs/g/g2592.md) unto [theos](../../strongs/g/g2316.md).

<a name="romans_7_5"></a>Romans 7:5

For when we were in the [sarx](../../strongs/g/g4561.md), the [pathēma](../../strongs/g/g3804.md) of [hamartia](../../strongs/g/g266.md), which were by the [nomos](../../strongs/g/g3551.md), did [energeō](../../strongs/g/g1754.md) in our [melos](../../strongs/g/g3196.md) to bring [karpophoreō](../../strongs/g/g2592.md) unto [thanatos](../../strongs/g/g2288.md).

<a name="romans_7_6"></a>Romans 7:6

But [nyni](../../strongs/g/g3570.md) we are [katargeō](../../strongs/g/g2673.md) from the [nomos](../../strongs/g/g3551.md), that being [apothnēskō](../../strongs/g/g599.md) wherein we were [katechō](../../strongs/g/g2722.md); that we should [douleuō](../../strongs/g/g1398.md) in [kainotēs](../../strongs/g/g2538.md) of [pneuma](../../strongs/g/g4151.md), and not in the [palaiotēs](../../strongs/g/g3821.md) of the [gramma](../../strongs/g/g1121.md).

<a name="romans_7_7"></a>Romans 7:7

What shall we [eipon](../../strongs/g/g2046.md) then? Is the [nomos](../../strongs/g/g3551.md) [hamartia](../../strongs/g/g266.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md). [alla](../../strongs/g/g235.md), I had not [ginōskō](../../strongs/g/g1097.md) [hamartia](../../strongs/g/g266.md), but by the [nomos](../../strongs/g/g3551.md): for I had not [eidō](../../strongs/g/g1492.md) [epithymia](../../strongs/g/g1939.md), except the [nomos](../../strongs/g/g3551.md) had [legō](../../strongs/g/g3004.md), Thou shalt not [epithymeō](../../strongs/g/g1937.md).

<a name="romans_7_8"></a>Romans 7:8

But [hamartia](../../strongs/g/g266.md), [lambanō](../../strongs/g/g2983.md) [aphormē](../../strongs/g/g874.md) by the [entolē](../../strongs/g/g1785.md), [katergazomai](../../strongs/g/g2716.md) in me all manner of [epithymia](../../strongs/g/g1939.md). For without the [nomos](../../strongs/g/g3551.md) [hamartia](../../strongs/g/g266.md) was [nekros](../../strongs/g/g3498.md).

<a name="romans_7_9"></a>Romans 7:9

For I was [zaō](../../strongs/g/g2198.md) without the [nomos](../../strongs/g/g3551.md) [pote](../../strongs/g/g4218.md): but when the [entolē](../../strongs/g/g1785.md) [erchomai](../../strongs/g/g2064.md), [hamartia](../../strongs/g/g266.md) [anazaō](../../strongs/g/g326.md), and I [apothnēskō](../../strongs/g/g599.md).

<a name="romans_7_10"></a>Romans 7:10

And the [entolē](../../strongs/g/g1785.md), which to [zōē](../../strongs/g/g2222.md), I [heuriskō](../../strongs/g/g2147.md) to be unto [thanatos](../../strongs/g/g2288.md).

<a name="romans_7_11"></a>Romans 7:11

For [hamartia](../../strongs/g/g266.md), [lambanō](../../strongs/g/g2983.md) [aphormē](../../strongs/g/g874.md) by the [entolē](../../strongs/g/g1785.md), [exapataō](../../strongs/g/g1818.md) me, and by it [apokteinō](../../strongs/g/g615.md) me.

<a name="romans_7_12"></a>Romans 7:12

Wherefore the [nomos](../../strongs/g/g3551.md) is [hagios](../../strongs/g/g40.md), and the [entolē](../../strongs/g/g1785.md) [hagios](../../strongs/g/g40.md), and [dikaios](../../strongs/g/g1342.md), and [agathos](../../strongs/g/g18.md).

<a name="romans_7_13"></a>Romans 7:13

Was then that which is [agathos](../../strongs/g/g18.md) [ginomai](../../strongs/g/g1096.md) [thanatos](../../strongs/g/g2288.md) unto me? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md). But [hamartia](../../strongs/g/g266.md), that it might [phainō](../../strongs/g/g5316.md) [hamartia](../../strongs/g/g266.md), [katergazomai](../../strongs/g/g2716.md) [thanatos](../../strongs/g/g2288.md) in me by that which is [agathos](../../strongs/g/g18.md); that [hamartia](../../strongs/g/g266.md) by the [entolē](../../strongs/g/g1785.md) might [ginomai](../../strongs/g/g1096.md) [hyperbolē](../../strongs/g/g5236.md) [hamartōlos](../../strongs/g/g268.md).

<a name="romans_7_14"></a>Romans 7:14

For we [eidō](../../strongs/g/g1492.md) that the [nomos](../../strongs/g/g3551.md) is [pneumatikos](../../strongs/g/g4152.md): but I am [sarkikos](../../strongs/g/g4559.md), [pipraskō](../../strongs/g/g4097.md) under [hamartia](../../strongs/g/g266.md).

<a name="romans_7_15"></a>Romans 7:15

For that which I [katergazomai](../../strongs/g/g2716.md) I [ginōskō](../../strongs/g/g1097.md) not: for what I would, that [prassō](../../strongs/g/g4238.md) I not; but what I [miseō](../../strongs/g/g3404.md), that [poieō](../../strongs/g/g4160.md) I.

<a name="romans_7_16"></a>Romans 7:16

If then I [poieō](../../strongs/g/g4160.md) that which I would not, I [symphēmi](../../strongs/g/g4852.md) unto the [nomos](../../strongs/g/g3551.md) that [kalos](../../strongs/g/g2570.md).

<a name="romans_7_17"></a>Romans 7:17

[nyni](../../strongs/g/g3570.md) then it is no more I that [katergazomai](../../strongs/g/g2716.md) it, but [hamartia](../../strongs/g/g266.md) that [oikeō](../../strongs/g/g3611.md) in me.

<a name="romans_7_18"></a>Romans 7:18

For I [eidō](../../strongs/g/g1492.md) that in me (that is, in my [sarx](../../strongs/g/g4561.md),) [oikeō](../../strongs/g/g3611.md) no [agathos](../../strongs/g/g18.md): for [thelō](../../strongs/g/g2309.md) is [parakeimai](../../strongs/g/g3873.md) with me; but [katergazomai](../../strongs/g/g2716.md) [kalos](../../strongs/g/g2570.md) I [heuriskō](../../strongs/g/g2147.md) not.

<a name="romans_7_19"></a>Romans 7:19

For the [agathos](../../strongs/g/g18.md) that I would I [poieō](../../strongs/g/g4160.md) not: but the [kakos](../../strongs/g/g2556.md) which I would not, that I [prassō](../../strongs/g/g4238.md).

<a name="romans_7_20"></a>Romans 7:20

Now if I [poieō](../../strongs/g/g4160.md) that I would not, it is no more I that [katergazomai](../../strongs/g/g2716.md) it, but [hamartia](../../strongs/g/g266.md) that [oikeō](../../strongs/g/g3611.md) in me.

<a name="romans_7_21"></a>Romans 7:21

I [heuriskō](../../strongs/g/g2147.md) then a [nomos](../../strongs/g/g3551.md), that, when I would [poieō](../../strongs/g/g4160.md) [kalos](../../strongs/g/g2570.md), [kakos](../../strongs/g/g2556.md) is [parakeimai](../../strongs/g/g3873.md) with me.

<a name="romans_7_22"></a>Romans 7:22

For I [synēdomai](../../strongs/g/g4913.md) in the [nomos](../../strongs/g/g3551.md) of [theos](../../strongs/g/g2316.md) after the [esō](../../strongs/g/g2080.md) [anthrōpos](../../strongs/g/g444.md):

<a name="romans_7_23"></a>Romans 7:23

But I [blepō](../../strongs/g/g991.md) another [nomos](../../strongs/g/g3551.md) in my [melos](../../strongs/g/g3196.md), [antistrateuomai](../../strongs/g/g497.md) the [nomos](../../strongs/g/g3551.md) of my [nous](../../strongs/g/g3563.md), and [aichmalōtizō](../../strongs/g/g163.md) me to the [nomos](../../strongs/g/g3551.md) of [hamartia](../../strongs/g/g266.md) which is in my [melos](../../strongs/g/g3196.md).

<a name="romans_7_24"></a>Romans 7:24

[talaipōros](../../strongs/g/g5005.md) [anthrōpos](../../strongs/g/g444.md) that I am! who shall [rhyomai](../../strongs/g/g4506.md) me from the [sōma](../../strongs/g/g4983.md) of this [thanatos](../../strongs/g/g2288.md)?

<a name="romans_7_25"></a>Romans 7:25

I [eucharisteō](../../strongs/g/g2168.md) [theos](../../strongs/g/g2316.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md). So then with the [nous](../../strongs/g/g3563.md) I myself [men](../../strongs/g/g3303.md) [douleuō](../../strongs/g/g1398.md) the [nomos](../../strongs/g/g3551.md) of [theos](../../strongs/g/g2316.md); but with the [sarx](../../strongs/g/g4561.md) the [nomos](../../strongs/g/g3551.md) of [hamartia](../../strongs/g/g266.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 6](romans_6.md) - [Romans 8](romans_8.md)