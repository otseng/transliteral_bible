# [Romans 14](https://www.blueletterbible.org/kjv/rom/14/1/s_1060001)

<a name="romans_14_1"></a>Romans 14:1

Him that is [astheneō](../../strongs/g/g770.md) in the [pistis](../../strongs/g/g4102.md) [proslambanō](../../strongs/g/g4355.md) ye, but not to [dialogismos](../../strongs/g/g1261.md) [diakrisis](../../strongs/g/g1253.md).

<a name="romans_14_2"></a>Romans 14:2

For one [pisteuō](../../strongs/g/g4100.md) that he may [phago](../../strongs/g/g5315.md) all things: another, who is [astheneō](../../strongs/g/g770.md), [esthiō](../../strongs/g/g2068.md) [lachanon](../../strongs/g/g3001.md).

<a name="romans_14_3"></a>Romans 14:3

Let not him that [esthiō](../../strongs/g/g2068.md) [exoutheneō](../../strongs/g/g1848.md) him that [esthiō](../../strongs/g/g2068.md) not; and let not him which [esthiō](../../strongs/g/g2068.md) not [krinō](../../strongs/g/g2919.md) him that [esthiō](../../strongs/g/g2068.md): for [theos](../../strongs/g/g2316.md) hath [proslambanō](../../strongs/g/g4355.md) him.

<a name="romans_14_4"></a>Romans 14:4

Who art thou that [krinō](../../strongs/g/g2919.md) [allotrios](../../strongs/g/g245.md) [oiketēs](../../strongs/g/g3610.md)? to his own [kyrios](../../strongs/g/g2962.md) he [stēkō](../../strongs/g/g4739.md) or [piptō](../../strongs/g/g4098.md). Yea, he shall be [histēmi](../../strongs/g/g2476.md): for [theos](../../strongs/g/g2316.md) is [dynatos](../../strongs/g/g1415.md) to make him [histēmi](../../strongs/g/g2476.md).

<a name="romans_14_5"></a>Romans 14:5

One man [krinō](../../strongs/g/g2919.md) one day above another: another [krinō](../../strongs/g/g2919.md) every day alike. Let every man [plērophoreō](../../strongs/g/g4135.md) in his own [nous](../../strongs/g/g3563.md).

<a name="romans_14_6"></a>Romans 14:6

He that [phroneō](../../strongs/g/g5426.md) the day, [phroneō](../../strongs/g/g5426.md) it unto the [kyrios](../../strongs/g/g2962.md); and he that [phroneō](../../strongs/g/g5426.md) not the day, to the [kyrios](../../strongs/g/g2962.md) he doth not regard it. He that [esthiō](../../strongs/g/g2068.md), [esthiō](../../strongs/g/g2068.md) to the [kyrios](../../strongs/g/g2962.md), for he giveth [theos](../../strongs/g/g2316.md) [eucharisteō](../../strongs/g/g2168.md); and he that [esthiō](../../strongs/g/g2068.md) not, to the [kyrios](../../strongs/g/g2962.md) he [esthiō](../../strongs/g/g2068.md) not, and giveth [theos](../../strongs/g/g2316.md) [eucharisteō](../../strongs/g/g2168.md). [^1]

<a name="romans_14_7"></a>Romans 14:7

For none of us [zaō](../../strongs/g/g2198.md) to himself, and no man [apothnēskō](../../strongs/g/g599.md) to himself.

<a name="romans_14_8"></a>Romans 14:8

For whether we [zaō](../../strongs/g/g2198.md), we [zaō](../../strongs/g/g2198.md) unto the [kyrios](../../strongs/g/g2962.md); and whether we [apothnēskō](../../strongs/g/g599.md), we [apothnēskō](../../strongs/g/g599.md) unto the [kyrios](../../strongs/g/g2962.md): whether we [zaō](../../strongs/g/g2198.md) therefore, or [apothnēskō](../../strongs/g/g599.md), we are the [kyrios](../../strongs/g/g2962.md).

<a name="romans_14_9"></a>Romans 14:9

For to this [Christos](../../strongs/g/g5547.md) both [apothnēskō](../../strongs/g/g599.md), and [anistēmi](../../strongs/g/g450.md), and [anazaō](../../strongs/g/g326.md), that he might be [kyrieuō](../../strongs/g/g2961.md) both of the [nekros](../../strongs/g/g3498.md) and [zaō](../../strongs/g/g2198.md).

<a name="romans_14_10"></a>Romans 14:10

But why dost thou [krinō](../../strongs/g/g2919.md) thy [adelphos](../../strongs/g/g80.md)? or why dost thou [exoutheneō](../../strongs/g/g1848.md) thy [adelphos](../../strongs/g/g80.md)? for we shall all [paristēmi](../../strongs/g/g3936.md) the [bēma](../../strongs/g/g968.md) of [Christos](../../strongs/g/g5547.md).

<a name="romans_14_11"></a>Romans 14:11

For it is [graphō](../../strongs/g/g1125.md), As I [zaō](../../strongs/g/g2198.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), every [gony](../../strongs/g/g1119.md) shall [kamptō](../../strongs/g/g2578.md) to me, and every [glōssa](../../strongs/g/g1100.md) shall [exomologeō](../../strongs/g/g1843.md) to [theos](../../strongs/g/g2316.md).

<a name="romans_14_12"></a>Romans 14:12

So then every one of us shall [didōmi](../../strongs/g/g1325.md) [logos](../../strongs/g/g3056.md) of himself to [theos](../../strongs/g/g2316.md).

<a name="romans_14_13"></a>Romans 14:13

Let us not therefore [krinō](../../strongs/g/g2919.md) [allēlōn](../../strongs/g/g240.md) any more: but [krinō](../../strongs/g/g2919.md) this rather, that [mē](../../strongs/g/g3361.md) [tithēmi](../../strongs/g/g5087.md) a [proskomma](../../strongs/g/g4348.md) or [skandalon](../../strongs/g/g4625.md) in his [adelphos](../../strongs/g/g80.md).

<a name="romans_14_14"></a>Romans 14:14

I [eidō](../../strongs/g/g1492.md), and am [peithō](../../strongs/g/g3982.md) by the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), that there is nothing [koinos](../../strongs/g/g2839.md) of itself: but to him that [logizomai](../../strongs/g/g3049.md) any thing to be [koinos](../../strongs/g/g2839.md), to him it is [koinos](../../strongs/g/g2839.md).

<a name="romans_14_15"></a>Romans 14:15

But if thy [adelphos](../../strongs/g/g80.md) be [lypeō](../../strongs/g/g3076.md) with thy [brōma](../../strongs/g/g1033.md), now [peripateō](../../strongs/g/g4043.md) thou not [agapē](../../strongs/g/g26.md) [kata](../../strongs/g/g2596.md). [apollymi](../../strongs/g/g622.md) not him with thy [brōma](../../strongs/g/g1033.md), for whom [Christos](../../strongs/g/g5547.md) [apothnēskō](../../strongs/g/g599.md).

<a name="romans_14_16"></a>Romans 14:16

Let not then your [agathos](../../strongs/g/g18.md) be [blasphēmeō](../../strongs/g/g987.md):

<a name="romans_14_17"></a>Romans 14:17

For the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is not [brōsis](../../strongs/g/g1035.md) and [posis](../../strongs/g/g4213.md); but [dikaiosynē](../../strongs/g/g1343.md), and [eirēnē](../../strongs/g/g1515.md), and [chara](../../strongs/g/g5479.md) in the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="romans_14_18"></a>Romans 14:18

For he that in these things [douleuō](../../strongs/g/g1398.md) [Christos](../../strongs/g/g5547.md) is [euarestos](../../strongs/g/g2101.md) to [theos](../../strongs/g/g2316.md), and [dokimos](../../strongs/g/g1384.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="romans_14_19"></a>Romans 14:19

Let us therefore [diōkō](../../strongs/g/g1377.md) the things which make for [eirēnē](../../strongs/g/g1515.md), and things wherewith may [oikodomē](../../strongs/g/g3619.md) [allēlōn](../../strongs/g/g240.md).

<a name="romans_14_20"></a>Romans 14:20

For [brōma](../../strongs/g/g1033.md) [katalyō](../../strongs/g/g2647.md) not the [ergon](../../strongs/g/g2041.md) of [theos](../../strongs/g/g2316.md). All things indeed are [katharos](../../strongs/g/g2513.md); but [kakos](../../strongs/g/g2556.md) for that [anthrōpos](../../strongs/g/g444.md) who [esthiō](../../strongs/g/g2068.md) with [proskomma](../../strongs/g/g4348.md).

<a name="romans_14_21"></a>Romans 14:21

[kalos](../../strongs/g/g2570.md) neither to [phago](../../strongs/g/g5315.md) [kreas](../../strongs/g/g2907.md), nor to [pinō](../../strongs/g/g4095.md) [oinos](../../strongs/g/g3631.md), nor whereby thy [adelphos](../../strongs/g/g80.md) [proskoptō](../../strongs/g/g4350.md), or is [skandalizō](../../strongs/g/g4624.md), or is [astheneō](../../strongs/g/g770.md).

<a name="romans_14_22"></a>Romans 14:22

Hast thou [pistis](../../strongs/g/g4102.md)? have it to thyself before [theos](../../strongs/g/g2316.md). [makarios](../../strongs/g/g3107.md) is he that [krinō](../../strongs/g/g2919.md) not himself in that thing which he [dokimazō](../../strongs/g/g1381.md).

<a name="romans_14_23"></a>Romans 14:23

And he that [diakrinō](../../strongs/g/g1252.md) is [katakrinō](../../strongs/g/g2632.md) if he [phago](../../strongs/g/g5315.md), because not of [pistis](../../strongs/g/g4102.md): for whatsoever is not of [pistis](../../strongs/g/g4102.md) is [hamartia](../../strongs/g/g266.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 13](romans_13.md) - [Romans 15](romans_15.md)

---

[^1]: [Romans 14:6 Commentary](../../commentary/romans/romans_14_commentary.md#romans_14_6)
