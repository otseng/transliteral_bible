# [Romans 4](https://www.blueletterbible.org/kjv/rom/4/1/rl1/s_1050001)

<a name="romans_4_1"></a>Romans 4:1

What shall we [eipon](../../strongs/g/g2046.md) then that [Abraam](../../strongs/g/g11.md) our [patēr](../../strongs/g/g3962.md), as pertaining to the [sarx](../../strongs/g/g4561.md), hath [heuriskō](../../strongs/g/g2147.md)?

<a name="romans_4_2"></a>Romans 4:2

For if [Abraam](../../strongs/g/g11.md) were [dikaioō](../../strongs/g/g1344.md) by [ergon](../../strongs/g/g2041.md), he hath [kauchēma](../../strongs/g/g2745.md); but not before [theos](../../strongs/g/g2316.md).

<a name="romans_4_3"></a>Romans 4:3

For what [legō](../../strongs/g/g3004.md) the [graphē](../../strongs/g/g1124.md)? [Abraam](../../strongs/g/g11.md) [pisteuō](../../strongs/g/g4100.md) [theos](../../strongs/g/g2316.md), and it was [logizomai](../../strongs/g/g3049.md) unto him for [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_4_4"></a>Romans 4:4

Now [ergazomai](../../strongs/g/g2038.md) is the [misthos](../../strongs/g/g3408.md) not [logizomai](../../strongs/g/g3049.md) of [charis](../../strongs/g/g5485.md), but of [opheilēma](../../strongs/g/g3783.md).

<a name="romans_4_5"></a>Romans 4:5

But [ergazomai](../../strongs/g/g2038.md) not, but [pisteuō](../../strongs/g/g4100.md) on him that [dikaioō](../../strongs/g/g1344.md) the [asebēs](../../strongs/g/g765.md), his [pistis](../../strongs/g/g4102.md) is [logizomai](../../strongs/g/g3049.md) for [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_4_6"></a>Romans 4:6

[kathaper](../../strongs/g/g2509.md) [Dabid](../../strongs/g/g1138.md) also [legō](../../strongs/g/g3004.md) the [makarismos](../../strongs/g/g3108.md) of the [anthrōpos](../../strongs/g/g444.md), unto whom [theos](../../strongs/g/g2316.md) [logizomai](../../strongs/g/g3049.md) [dikaiosynē](../../strongs/g/g1343.md) without [ergon](../../strongs/g/g2041.md),

<a name="romans_4_7"></a>Romans 4:7

[makarios](../../strongs/g/g3107.md) are they whose [anomia](../../strongs/g/g458.md) are [aphiēmi](../../strongs/g/g863.md), and whose [hamartia](../../strongs/g/g266.md) are [epikalyptō](../../strongs/g/g1943.md).

<a name="romans_4_8"></a>Romans 4:8

[makarios](../../strongs/g/g3107.md) the [anēr](../../strongs/g/g435.md) to whom the [kyrios](../../strongs/g/g2962.md) will not [logizomai](../../strongs/g/g3049.md) [hamartia](../../strongs/g/g266.md).

<a name="romans_4_9"></a>Romans 4:9

this [makarismos](../../strongs/g/g3108.md) then upon the [peritomē](../../strongs/g/g4061.md) only, or upon the [akrobystia](../../strongs/g/g203.md) also? for we [legō](../../strongs/g/g3004.md) that [pistis](../../strongs/g/g4102.md) was [logizomai](../../strongs/g/g3049.md) to [Abraam](../../strongs/g/g11.md) for [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_4_10"></a>Romans 4:10

How was it then [logizomai](../../strongs/g/g3049.md)? when he was in [peritomē](../../strongs/g/g4061.md), or in [akrobystia](../../strongs/g/g203.md)? Not in [peritomē](../../strongs/g/g4061.md), but in [akrobystia](../../strongs/g/g203.md).

<a name="romans_4_11"></a>Romans 4:11

And he [lambanō](../../strongs/g/g2983.md) the [sēmeion](../../strongs/g/g4592.md) of [peritomē](../../strongs/g/g4061.md), a [sphragis](../../strongs/g/g4973.md) of the [dikaiosynē](../../strongs/g/g1343.md) of the [pistis](../../strongs/g/g4102.md) which he had yet being [akrobystia](../../strongs/g/g203.md): that he might be the [patēr](../../strongs/g/g3962.md) of all them that [pisteuō](../../strongs/g/g4100.md), though they be [akrobystia](../../strongs/g/g203.md); that [dikaiosynē](../../strongs/g/g1343.md) might be [logizomai](../../strongs/g/g3049.md) unto them also:

<a name="romans_4_12"></a>Romans 4:12

And the [patēr](../../strongs/g/g3962.md) of [peritomē](../../strongs/g/g4061.md) to them who are not of the [peritomē](../../strongs/g/g4061.md) only, but who also [stoicheō](../../strongs/g/g4748.md) in the [ichnos](../../strongs/g/g2487.md) of that [pistis](../../strongs/g/g4102.md) of our [patēr](../../strongs/g/g3962.md) [Abraam](../../strongs/g/g11.md), which he had being yet [akrobystia](../../strongs/g/g203.md).

<a name="romans_4_13"></a>Romans 4:13

For the [epaggelia](../../strongs/g/g1860.md), that he should be the [klēronomos](../../strongs/g/g2818.md) of the [kosmos](../../strongs/g/g2889.md), was not to [Abraam](../../strongs/g/g11.md), or to his [sperma](../../strongs/g/g4690.md), through the [nomos](../../strongs/g/g3551.md), but through the [dikaiosynē](../../strongs/g/g1343.md) of [pistis](../../strongs/g/g4102.md).

<a name="romans_4_14"></a>Romans 4:14

For if they which are of the [nomos](../../strongs/g/g3551.md) be [klēronomos](../../strongs/g/g2818.md), [pistis](../../strongs/g/g4102.md) is [kenoō](../../strongs/g/g2758.md), and the [epaggelia](../../strongs/g/g1860.md) [katargeō](../../strongs/g/g2673.md):

<a name="romans_4_15"></a>Romans 4:15

Because the [nomos](../../strongs/g/g3551.md) [katergazomai](../../strongs/g/g2716.md) [orgē](../../strongs/g/g3709.md): for where no [nomos](../../strongs/g/g3551.md) is, no [parabasis](../../strongs/g/g3847.md).

<a name="romans_4_16"></a>Romans 4:16

Therefore of [pistis](../../strongs/g/g4102.md), that by [charis](../../strongs/g/g5485.md); to the end the [epaggelia](../../strongs/g/g1860.md) might be [bebaios](../../strongs/g/g949.md) to all the [sperma](../../strongs/g/g4690.md); not to that only which is of the [nomos](../../strongs/g/g3551.md), but to that also which is of the [pistis](../../strongs/g/g4102.md) of [Abraam](../../strongs/g/g11.md); who is the [patēr](../../strongs/g/g3962.md) of us all,

<a name="romans_4_17"></a>Romans 4:17

(As it is [graphō](../../strongs/g/g1125.md), I have [tithēmi](../../strongs/g/g5087.md) thee a [patēr](../../strongs/g/g3962.md) of [polys](../../strongs/g/g4183.md) [ethnos](../../strongs/g/g1484.md),) [katenanti](../../strongs/g/g2713.md) him whom he [pisteuō](../../strongs/g/g4100.md), [theos](../../strongs/g/g2316.md), who [zōopoieō](../../strongs/g/g2227.md) the [nekros](../../strongs/g/g3498.md), and [kaleō](../../strongs/g/g2564.md) those things which be not as though they were.

<a name="romans_4_18"></a>Romans 4:18

Who against [elpis](../../strongs/g/g1680.md) [pisteuō](../../strongs/g/g4100.md) in [elpis](../../strongs/g/g1680.md), that he might become the [patēr](../../strongs/g/g3962.md) of [polys](../../strongs/g/g4183.md) [ethnos](../../strongs/g/g1484.md), according to that which was [eipon](../../strongs/g/g2046.md), So shall thy [sperma](../../strongs/g/g4690.md) be.

<a name="romans_4_19"></a>Romans 4:19

And being not [astheneō](../../strongs/g/g770.md) in [pistis](../../strongs/g/g4102.md), he [katanoeō](../../strongs/g/g2657.md) not his own [sōma](../../strongs/g/g4983.md) now [nekroō](../../strongs/g/g3499.md), when he was about an hundred years old, neither yet the [nekrōsis](../../strongs/g/g3500.md) of [Sarra](../../strongs/g/g4564.md) [mētra](../../strongs/g/g3388.md):

<a name="romans_4_20"></a>Romans 4:20

He [diakrinō](../../strongs/g/g1252.md) not at the [epaggelia](../../strongs/g/g1860.md) of [theos](../../strongs/g/g2316.md) through [apistia](../../strongs/g/g570.md); but was [endynamoō](../../strongs/g/g1743.md) in [pistis](../../strongs/g/g4102.md), [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) to [theos](../../strongs/g/g2316.md);

<a name="romans_4_21"></a>Romans 4:21

And being [plērophoreō](../../strongs/g/g4135.md) that, what he had [epaggellomai](../../strongs/g/g1861.md), he was [dynatos](../../strongs/g/g1415.md) also to [poieō](../../strongs/g/g4160.md).

<a name="romans_4_22"></a>Romans 4:22

And therefore it was [logizomai](../../strongs/g/g3049.md) to him for [dikaiosynē](../../strongs/g/g1343.md).

<a name="romans_4_23"></a>Romans 4:23

Now it was not [graphō](../../strongs/g/g1125.md) for his sake alone, that it was [logizomai](../../strongs/g/g3049.md) to him;

<a name="romans_4_24"></a>Romans 4:24

But for us also, to whom it shall be [logizomai](../../strongs/g/g3049.md), if we [pisteuō](../../strongs/g/g4100.md) on him that [egeirō](../../strongs/g/g1453.md) up [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md) from the [nekros](../../strongs/g/g3498.md);

<a name="romans_4_25"></a>Romans 4:25

Who was [paradidōmi](../../strongs/g/g3860.md) for our [paraptōma](../../strongs/g/g3900.md), and was [egeirō](../../strongs/g/g1453.md) for our [dikaiōsis](../../strongs/g/g1347.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 3](romans_3.md) - [Romans 5](romans_5.md)