# [Romans 1](https://www.blueletterbible.org/kjv/rom/1/1/s_1047001)

<a name="romans_1_1"></a>Romans 1:1

[Paulos](../../strongs/g/g3972.md), a [doulos](../../strongs/g/g1401.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [klētos](../../strongs/g/g2822.md) to be an [apostolos](../../strongs/g/g652.md), [aphorizō](../../strongs/g/g873.md) unto the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md),

<a name="romans_1_2"></a>Romans 1:2

(Which he had [proepaggello](../../strongs/g/g4279.md) by his [prophētēs](../../strongs/g/g4396.md) in the [hagios](../../strongs/g/g40.md) [graphē](../../strongs/g/g1124.md),)

<a name="romans_1_3"></a>Romans 1:3

Concerning his [huios](../../strongs/g/g5207.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md), which was [ginomai](../../strongs/g/g1096.md) of the [sperma](../../strongs/g/g4690.md) of [Dabid](../../strongs/g/g1138.md) according to the [sarx](../../strongs/g/g4561.md);

<a name="romans_1_4"></a>Romans 1:4

And [horizō](../../strongs/g/g3724.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md) with [dynamis](../../strongs/g/g1411.md), according to the [pneuma](../../strongs/g/g4151.md) of [hagiosyne](../../strongs/g/g42.md), by the [anastasis](../../strongs/g/g386.md) from the [nekros](../../strongs/g/g3498.md):

<a name="romans_1_5"></a>Romans 1:5

By whom we have [lambanō](../../strongs/g/g2983.md) [charis](../../strongs/g/g5485.md) and [apostolē](../../strongs/g/g651.md), for [hypakoē](../../strongs/g/g5218.md) to the [pistis](../../strongs/g/g4102.md) among all [ethnos](../../strongs/g/g1484.md), for his [onoma](../../strongs/g/g3686.md):

<a name="romans_1_6"></a>Romans 1:6

Among whom are ye also the [klētos](../../strongs/g/g2822.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="romans_1_7"></a>Romans 1:7

To all that be in [Rhōmē](../../strongs/g/g4516.md), [agapētos](../../strongs/g/g27.md) of [theos](../../strongs/g/g2316.md), [klētos](../../strongs/g/g2822.md) to be [hagios](../../strongs/g/g40.md): [charis](../../strongs/g/g5485.md) to you and [eirēnē](../../strongs/g/g1515.md) from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="romans_1_8"></a>Romans 1:8

First, I [eucharisteō](../../strongs/g/g2168.md) my [theos](../../strongs/g/g2316.md) through [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) for you all, that your [pistis](../../strongs/g/g4102.md) is [kataggellō](../../strongs/g/g2605.md) of throughout the [holos](../../strongs/g/g3650.md) [kosmos](../../strongs/g/g2889.md).

<a name="romans_1_9"></a>Romans 1:9

For [theos](../../strongs/g/g2316.md) is my [martys](../../strongs/g/g3144.md), whom I [latreuō](../../strongs/g/g3000.md) with my [pneuma](../../strongs/g/g4151.md) in the [euaggelion](../../strongs/g/g2098.md) of his [huios](../../strongs/g/g5207.md), that [adialeiptos](../../strongs/g/g89.md) I [poieō](../../strongs/g/g4160.md) [mneia](../../strongs/g/g3417.md) of you [pantote](../../strongs/g/g3842.md) in my [proseuchē](../../strongs/g/g4335.md);

<a name="romans_1_10"></a>Romans 1:10

[deomai](../../strongs/g/g1189.md), [ei pōs](../../strongs/g/g1513.md) [ēdē](../../strongs/g/g2235.md) at [pote](../../strongs/g/g4218.md) I might [euodoo](../../strongs/g/g2137.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) to [erchomai](../../strongs/g/g2064.md) unto you.

<a name="romans_1_11"></a>Romans 1:11

For I [epipotheo](../../strongs/g/g1971.md) to [eidō](../../strongs/g/g1492.md) you, that I may [metadidōmi](../../strongs/g/g3330.md) unto you some [pneumatikos](../../strongs/g/g4152.md) [charisma](../../strongs/g/g5486.md), to the end ye may be [stērizō](../../strongs/g/g4741.md);

<a name="romans_1_12"></a>Romans 1:12

That is, that I may be [symparakaleo](../../strongs/g/g4837.md) with you by the [allēlōn](../../strongs/g/g240.md) [pistis](../../strongs/g/g4102.md) both of you and me.

<a name="romans_1_13"></a>Romans 1:13

Now I would not have you [agnoeō](../../strongs/g/g50.md), [adelphos](../../strongs/g/g80.md), that [pollakis](../../strongs/g/g4178.md) I [protithēmi](../../strongs/g/g4388.md) to [erchomai](../../strongs/g/g2064.md) unto you, (but was [kōlyō](../../strongs/g/g2967.md) [achri](../../strongs/g/g891.md) [deuro](../../strongs/g/g1204.md),) that I might have some [karpos](../../strongs/g/g2590.md) among you also, even as among [loipos](../../strongs/g/g3062.md) [ethnos](../../strongs/g/g1484.md).

<a name="romans_1_14"></a>Romans 1:14

I am [opheiletēs](../../strongs/g/g3781.md) both to the [Hellēn](../../strongs/g/g1672.md), and to the [barbaros](../../strongs/g/g915.md); both to the [sophos](../../strongs/g/g4680.md), and to the [anoētos](../../strongs/g/g453.md).

<a name="romans_1_15"></a>Romans 1:15

So, as much as in me is, I am [prothymos](../../strongs/g/g4289.md) to [euaggelizō](../../strongs/g/g2097.md) to you that are at [Rhōmē](../../strongs/g/g4516.md) also.

<a name="romans_1_16"></a>Romans 1:16

For I am not [epaischynomai](../../strongs/g/g1870.md) of the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md): for it is the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md) unto [sōtēria](../../strongs/g/g4991.md) to every one that [pisteuō](../../strongs/g/g4100.md); to the [Ioudaios](../../strongs/g/g2453.md) first, and also to the [Hellēn](../../strongs/g/g1672.md).

<a name="romans_1_17"></a>Romans 1:17

For therein is the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md) [apokalyptō](../../strongs/g/g601.md) from [pistis](../../strongs/g/g4102.md) to [pistis](../../strongs/g/g4102.md): as it is [graphō](../../strongs/g/g1125.md), The [dikaios](../../strongs/g/g1342.md) shall [zaō](../../strongs/g/g2198.md) by [pistis](../../strongs/g/g4102.md).

<a name="romans_1_18"></a>Romans 1:18

For the [orgē](../../strongs/g/g3709.md) of [theos](../../strongs/g/g2316.md) is [apokalyptō](../../strongs/g/g601.md) from [ouranos](../../strongs/g/g3772.md) against all [asebeia](../../strongs/g/g763.md) and [adikia](../../strongs/g/g93.md) of [anthrōpos](../../strongs/g/g444.md), who [katechō](../../strongs/g/g2722.md) the [alētheia](../../strongs/g/g225.md) in [adikia](../../strongs/g/g93.md);

<a name="romans_1_19"></a>Romans 1:19

Because that which may be [gnōstos](../../strongs/g/g1110.md) of [theos](../../strongs/g/g2316.md) is [phaneros](../../strongs/g/g5318.md) in them; for [theos](../../strongs/g/g2316.md) hath [phaneroō](../../strongs/g/g5319.md) it unto them.

<a name="romans_1_20"></a>Romans 1:20

For the [aoratos](../../strongs/g/g517.md) of him from the [ktisis](../../strongs/g/g2937.md) of the [kosmos](../../strongs/g/g2889.md) are [kathorao](../../strongs/g/g2529.md), being [noeō](../../strongs/g/g3539.md) by [poiēma](../../strongs/g/g4161.md), even his [aïdios](../../strongs/g/g126.md) [dynamis](../../strongs/g/g1411.md) and [theiotes](../../strongs/g/g2305.md); so that they are [anapologetos](../../strongs/g/g379.md):

<a name="romans_1_21"></a>Romans 1:21

Because that, when they [ginōskō](../../strongs/g/g1097.md) [theos](../../strongs/g/g2316.md), they [doxazō](../../strongs/g/g1392.md) him not as [theos](../../strongs/g/g2316.md), neither were [eucharisteō](../../strongs/g/g2168.md); but became [mataioo](../../strongs/g/g3154.md) in their [dialogismos](../../strongs/g/g1261.md), and their [asynetos](../../strongs/g/g801.md) [kardia](../../strongs/g/g2588.md) was [skotizō](../../strongs/g/g4654.md).

<a name="romans_1_22"></a>Romans 1:22

[phasko](../../strongs/g/g5335.md) themselves to be [sophos](../../strongs/g/g4680.md), they became [mōrainō](../../strongs/g/g3471.md),

<a name="romans_1_23"></a>Romans 1:23

And [allassō](../../strongs/g/g236.md) the [doxa](../../strongs/g/g1391.md) of the [aphthartos](../../strongs/g/g862.md) [theos](../../strongs/g/g2316.md) into an [eikōn](../../strongs/g/g1504.md) [homoiōma](../../strongs/g/g3667.md) to [phthartos](../../strongs/g/g5349.md) [anthrōpos](../../strongs/g/g444.md), and to [peteinon](../../strongs/g/g4071.md), and [tetrapous](../../strongs/g/g5074.md), and [herpeton](../../strongs/g/g2062.md).

<a name="romans_1_24"></a>Romans 1:24

Wherefore [theos](../../strongs/g/g2316.md) also [paradidōmi](../../strongs/g/g3860.md) them to [akatharsia](../../strongs/g/g167.md) through the [epithymia](../../strongs/g/g1939.md) of their own [kardia](../../strongs/g/g2588.md), to [atimazō](../../strongs/g/g818.md) their own [sōma](../../strongs/g/g4983.md) between themselves:

<a name="romans_1_25"></a>Romans 1:25

Who [metallasso](../../strongs/g/g3337.md) the [alētheia](../../strongs/g/g225.md) of [theos](../../strongs/g/g2316.md) into a [pseudos](../../strongs/g/g5579.md), and [sebazomai](../../strongs/g/g4573.md) and [latreuō](../../strongs/g/g3000.md) the [ktisis](../../strongs/g/g2937.md) more than the [ktizō](../../strongs/g/g2936.md), who is [eulogētos](../../strongs/g/g2128.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="romans_1_26"></a>Romans 1:26

For this cause [theos](../../strongs/g/g2316.md) [paradidōmi](../../strongs/g/g3860.md) them up unto [atimia](../../strongs/g/g819.md) [pathos](../../strongs/g/g3806.md): for even their [thēlys](../../strongs/g/g2338.md) did [metallasso](../../strongs/g/g3337.md) the [physikos](../../strongs/g/g5446.md) [chresis](../../strongs/g/g5540.md) into that which is against [physis](../../strongs/g/g5449.md):

<a name="romans_1_27"></a>Romans 1:27

And [homoiōs](../../strongs/g/g3668.md) also the [arrēn](../../strongs/g/g730.md), [aphiēmi](../../strongs/g/g863.md) the [physikos](../../strongs/g/g5446.md) [chresis](../../strongs/g/g5540.md) of the [thēlys](../../strongs/g/g2338.md), [ekkaio](../../strongs/g/g1572.md) in their [orexis](../../strongs/g/g3715.md) [allēlōn](../../strongs/g/g240.md); [arrēn](../../strongs/g/g730.md) with [arrēn](../../strongs/g/g730.md) [katergazomai](../../strongs/g/g2716.md) [aschemosyne](../../strongs/g/g808.md), and [apolambanō](../../strongs/g/g618.md) in themselves that [antimisthia](../../strongs/g/g489.md) of their [planē](../../strongs/g/g4106.md) which was [dei](../../strongs/g/g1163.md).

<a name="romans_1_28"></a>Romans 1:28

And even as they did not [dokimazō](../../strongs/g/g1381.md) to retain [theos](../../strongs/g/g2316.md) in [epignōsis](../../strongs/g/g1922.md), [theos](../../strongs/g/g2316.md) [paradidōmi](../../strongs/g/g3860.md) them over to an [adokimos](../../strongs/g/g96.md) [nous](../../strongs/g/g3563.md), to [poieō](../../strongs/g/g4160.md) those things which are not [kathēkō](../../strongs/g/g2520.md);

<a name="romans_1_29"></a>Romans 1:29

Being [plēroō](../../strongs/g/g4137.md) with all [adikia](../../strongs/g/g93.md), [porneia](../../strongs/g/g4202.md), [ponēria](../../strongs/g/g4189.md), [pleonexia](../../strongs/g/g4124.md), [kakia](../../strongs/g/g2549.md); [mestos](../../strongs/g/g3324.md) of [phthonos](../../strongs/g/g5355.md), [phonos](../../strongs/g/g5408.md), [eris](../../strongs/g/g2054.md), [dolos](../../strongs/g/g1388.md), [kakoetheia](../../strongs/g/g2550.md); [psithyristes](../../strongs/g/g5588.md),

<a name="romans_1_30"></a>Romans 1:30

[katalalos](../../strongs/g/g2637.md), [theostyges](../../strongs/g/g2319.md), [hybristes](../../strongs/g/g5197.md), [hyperēphanos](../../strongs/g/g5244.md), [alazon](../../strongs/g/g213.md), [epheuretes](../../strongs/g/g2182.md) of [kakos](../../strongs/g/g2556.md), [apeithēs](../../strongs/g/g545.md) to [goneus](../../strongs/g/g1118.md),

<a name="romans_1_31"></a>Romans 1:31

[asynetos](../../strongs/g/g801.md), [asynthetos](../../strongs/g/g802.md), [astorgos](../../strongs/g/g794.md), [aspondos](../../strongs/g/g786.md), [aneleemon](../../strongs/g/g415.md):

<a name="romans_1_32"></a>Romans 1:32

Who [epiginōskō](../../strongs/g/g1921.md) the [dikaiōma](../../strongs/g/g1345.md) of [theos](../../strongs/g/g2316.md), that they which [prassō](../../strongs/g/g4238.md) such things are [axios](../../strongs/g/g514.md) of [thanatos](../../strongs/g/g2288.md), not only [poieō](../../strongs/g/g4160.md) the same, but have [syneudokeō](../../strongs/g/g4909.md) in them that [prassō](../../strongs/g/g4238.md) them.

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 0](romans_0.md) - [Romans 2](romans_2.md)