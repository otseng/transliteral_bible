# [Romans 3](https://www.blueletterbible.org/kjv/rom/3/1/s_1049001)

<a name="romans_3_1"></a>Romans 3:1

What [perissos](../../strongs/g/g4053.md) then hath the [Ioudaios](../../strongs/g/g2453.md)? or what [ōpheleia](../../strongs/g/g5622.md) is there of [peritomē](../../strongs/g/g4061.md)?

<a name="romans_3_2"></a>Romans 3:2

[polys](../../strongs/g/g4183.md) every way: [prōton](../../strongs/g/g4412.md), because that [pisteuō](../../strongs/g/g4100.md) the [logion](../../strongs/g/g3051.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_3_3"></a>Romans 3:3

For what if some [apisteō](../../strongs/g/g569.md)? shall their [apistia](../../strongs/g/g570.md) make the [pistis](../../strongs/g/g4102.md) of [theos](../../strongs/g/g2316.md) [katargeō](../../strongs/g/g2673.md)?

<a name="romans_3_4"></a>Romans 3:4

[mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md): [de](../../strongs/g/g1161.md), let [theos](../../strongs/g/g2316.md) be [alēthēs](../../strongs/g/g227.md), but every [anthrōpos](../../strongs/g/g444.md) a [pseustēs](../../strongs/g/g5583.md); as it is [graphō](../../strongs/g/g1125.md), That thou mightest be [dikaioō](../../strongs/g/g1344.md) in thy [logos](../../strongs/g/g3056.md), and [nikaō](../../strongs/g/g3528.md) when thou art [krinō](../../strongs/g/g2919.md).

<a name="romans_3_5"></a>Romans 3:5

But if our [adikia](../../strongs/g/g93.md) [synistēmi](../../strongs/g/g4921.md) the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md), what shall we [eipon](../../strongs/g/g2046.md)? Is [theos](../../strongs/g/g2316.md) [adikos](../../strongs/g/g94.md) who [epipherō](../../strongs/g/g2018.md) [orgē](../../strongs/g/g3709.md)? (I [legō](../../strongs/g/g3004.md) as [anthrōpos](../../strongs/g/g444.md))

<a name="romans_3_6"></a>Romans 3:6

[mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md): for then how shall [theos](../../strongs/g/g2316.md) [krinō](../../strongs/g/g2919.md) the [kosmos](../../strongs/g/g2889.md)?

<a name="romans_3_7"></a>Romans 3:7

For if the [alētheia](../../strongs/g/g225.md) of [theos](../../strongs/g/g2316.md) hath [perisseuō](../../strongs/g/g4052.md) through my [pseusma](../../strongs/g/g5582.md) unto his [doxa](../../strongs/g/g1391.md); why yet am I also [krinō](../../strongs/g/g2919.md) as a [hamartōlos](../../strongs/g/g268.md)?

<a name="romans_3_8"></a>Romans 3:8

And not, (as [blasphēmeō](../../strongs/g/g987.md), and as some [phēmi](../../strongs/g/g5346.md) that we [legō](../../strongs/g/g3004.md),) Let us [poieō](../../strongs/g/g4160.md) [kakos](../../strongs/g/g2556.md), that [agathos](../../strongs/g/g18.md) may [erchomai](../../strongs/g/g2064.md)? whose [krima](../../strongs/g/g2917.md) is [endikos](../../strongs/g/g1738.md).

<a name="romans_3_9"></a>Romans 3:9

What then? are we [proechō](../../strongs/g/g4284.md) than they? [ou](../../strongs/g/g3756.md), [pantōs](../../strongs/g/g3843.md): for we have [proaitiaomai](../../strongs/g/g4256.md) both [Ioudaios](../../strongs/g/g2453.md) and [Hellēn](../../strongs/g/g1672.md), that they are all under [hamartia](../../strongs/g/g266.md);

<a name="romans_3_10"></a>Romans 3:10

As it is [graphō](../../strongs/g/g1125.md), There is none [dikaios](../../strongs/g/g1342.md), no, not one:

<a name="romans_3_11"></a>Romans 3:11

There is none [syniēmi](../../strongs/g/g4920.md), there is none [ekzēteō](../../strongs/g/g1567.md) [theos](../../strongs/g/g2316.md).

<a name="romans_3_12"></a>Romans 3:12

They are all [ekklinō](../../strongs/g/g1578.md), they are together [achreioō](../../strongs/g/g889.md); there is none that [poieō](../../strongs/g/g4160.md) [chrēstotēs](../../strongs/g/g5544.md), no, not one.

<a name="romans_3_13"></a>Romans 3:13

Their [larygx](../../strongs/g/g2995.md) an [anoigō](../../strongs/g/g455.md) [taphos](../../strongs/g/g5028.md); with their [glōssa](../../strongs/g/g1100.md) they have [dolioō](../../strongs/g/g1387.md); the [ios](../../strongs/g/g2447.md) of [aspis](../../strongs/g/g785.md) under their [cheilos](../../strongs/g/g5491.md):

<a name="romans_3_14"></a>Romans 3:14

Whose [stoma](../../strongs/g/g4750.md) [gemō](../../strongs/g/g1073.md) of [ara](../../strongs/g/g685.md) and [pikria](../../strongs/g/g4088.md):

<a name="romans_3_15"></a>Romans 3:15

Their [pous](../../strongs/g/g4228.md) are [oxys](../../strongs/g/g3691.md) to [ekcheō](../../strongs/g/g1632.md) [haima](../../strongs/g/g129.md):

<a name="romans_3_16"></a>Romans 3:16

[syntrimma](../../strongs/g/g4938.md) and [talaipōria](../../strongs/g/g5004.md) in their [hodos](../../strongs/g/g3598.md):

<a name="romans_3_17"></a>Romans 3:17

And the [hodos](../../strongs/g/g3598.md) of [eirēnē](../../strongs/g/g1515.md) have they [ginōskō](../../strongs/g/g1097.md) not:

<a name="romans_3_18"></a>Romans 3:18

There is no [phobos](../../strongs/g/g5401.md) of [theos](../../strongs/g/g2316.md) [apenanti](../../strongs/g/g561.md) their [ophthalmos](../../strongs/g/g3788.md).

<a name="romans_3_19"></a>Romans 3:19

Now we [eidō](../../strongs/g/g1492.md) that what things soever the [nomos](../../strongs/g/g3551.md) [laleō](../../strongs/g/g2980.md), it [legō](../../strongs/g/g3004.md) to them who are under the [nomos](../../strongs/g/g3551.md): that every [stoma](../../strongs/g/g4750.md) may be [phrassō](../../strongs/g/g5420.md), and all the [kosmos](../../strongs/g/g2889.md) may become [hypodikos](../../strongs/g/g5267.md) before [theos](../../strongs/g/g2316.md).

<a name="romans_3_20"></a>Romans 3:20

Therefore by the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md) there shall no [sarx](../../strongs/g/g4561.md) be [dikaioō](../../strongs/g/g1344.md) in his [enōpion](../../strongs/g/g1799.md): for by the [nomos](../../strongs/g/g3551.md) is the [epignōsis](../../strongs/g/g1922.md) of [hamartia](../../strongs/g/g266.md).

<a name="romans_3_21"></a>Romans 3:21

But [nyni](../../strongs/g/g3570.md) the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md) without the [nomos](../../strongs/g/g3551.md) is [phaneroō](../../strongs/g/g5319.md), [martyreō](../../strongs/g/g3140.md) by the [nomos](../../strongs/g/g3551.md) and the [prophētēs](../../strongs/g/g4396.md);

<a name="romans_3_22"></a>Romans 3:22

Even the [dikaiosynē](../../strongs/g/g1343.md) of [theos](../../strongs/g/g2316.md) which is by [pistis](../../strongs/g/g4102.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) unto all and upon all them that [pisteuō](../../strongs/g/g4100.md): for there is no [diastolē](../../strongs/g/g1293.md):

<a name="romans_3_23"></a>Romans 3:23

For all have [hamartanō](../../strongs/g/g264.md), and [hystereō](../../strongs/g/g5302.md) of the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md);

<a name="romans_3_24"></a>Romans 3:24

Being [dikaioō](../../strongs/g/g1344.md) [dōrean](../../strongs/g/g1432.md) by his [charis](../../strongs/g/g5485.md) through the [apolytrōsis](../../strongs/g/g629.md) that is in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="romans_3_25"></a>Romans 3:25

Whom [theos](../../strongs/g/g2316.md) hath [protithēmi](../../strongs/g/g4388.md) a [hilastērion](../../strongs/g/g2435.md) through [pistis](../../strongs/g/g4102.md) in his [haima](../../strongs/g/g129.md), to [endeixis](../../strongs/g/g1732.md) his [dikaiosynē](../../strongs/g/g1343.md) for the [paresis](../../strongs/g/g3929.md) of [hamartēma](../../strongs/g/g265.md) that are [proginomai](../../strongs/g/g4266.md), through the [anochē](../../strongs/g/g463.md) of [theos](../../strongs/g/g2316.md);

<a name="romans_3_26"></a>Romans 3:26

To [endeixis](../../strongs/g/g1732.md), at this [kairos](../../strongs/g/g2540.md) his [dikaiosynē](../../strongs/g/g1343.md): that he might be [dikaios](../../strongs/g/g1342.md), and the [dikaioō](../../strongs/g/g1344.md) of him which [pistis](../../strongs/g/g4102.md) in [Iēsous](../../strongs/g/g2424.md).

<a name="romans_3_27"></a>Romans 3:27

Where [kauchēsis](../../strongs/g/g2746.md) then? It is [ekkleiō](../../strongs/g/g1576.md). By what [nomos](../../strongs/g/g3551.md)? of [ergon](../../strongs/g/g2041.md)? [ouchi](../../strongs/g/g3780.md): but by the [nomos](../../strongs/g/g3551.md) of [pistis](../../strongs/g/g4102.md).

<a name="romans_3_28"></a>Romans 3:28

Therefore we [logizomai](../../strongs/g/g3049.md) that [anthrōpos](../../strongs/g/g444.md) is [dikaioō](../../strongs/g/g1344.md) by [pistis](../../strongs/g/g4102.md) without the [ergon](../../strongs/g/g2041.md) of the [nomos](../../strongs/g/g3551.md).

<a name="romans_3_29"></a>Romans 3:29

the [theos](../../strongs/g/g2316.md) of the [Ioudaios](../../strongs/g/g2453.md) only? is he not also of the [ethnos](../../strongs/g/g1484.md)? [nai](../../strongs/g/g3483.md), of the [ethnos](../../strongs/g/g1484.md) also:

<a name="romans_3_30"></a>Romans 3:30

[epeiper](../../strongs/g/g1897.md) one [theos](../../strongs/g/g2316.md), which shall [dikaioō](../../strongs/g/g1344.md) the [peritomē](../../strongs/g/g4061.md) by [pistis](../../strongs/g/g4102.md), and [akrobystia](../../strongs/g/g203.md) through [pistis](../../strongs/g/g4102.md).

<a name="romans_3_31"></a>Romans 3:31

Do we then [katargeō](../../strongs/g/g2673.md) the [nomos](../../strongs/g/g3551.md) through [pistis](../../strongs/g/g4102.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md): [alla](../../strongs/g/g235.md), we [histēmi](../../strongs/g/g2476.md) the [nomos](../../strongs/g/g3551.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 2](romans_2.md) - [Romans 4](romans_4.md)