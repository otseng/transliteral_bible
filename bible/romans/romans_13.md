# [Romans 13](https://www.blueletterbible.org/kjv/rom/13/1/s_1059001)

<a name="romans_13_1"></a>Romans 13:1

Let every [psychē](../../strongs/g/g5590.md) [hypotassō](../../strongs/g/g5293.md) unto the [hyperechō](../../strongs/g/g5242.md) [exousia](../../strongs/g/g1849.md). For there is no [exousia](../../strongs/g/g1849.md) but of [theos](../../strongs/g/g2316.md): the [exousia](../../strongs/g/g1849.md) that be are [tassō](../../strongs/g/g5021.md) of [theos](../../strongs/g/g2316.md).

<a name="romans_13_2"></a>Romans 13:2

Whosoever therefore [antitassō](../../strongs/g/g498.md) the [exousia](../../strongs/g/g1849.md), [anthistēmi](../../strongs/g/g436.md) the [diatagē](../../strongs/g/g1296.md) of [theos](../../strongs/g/g2316.md): and they that [anthistēmi](../../strongs/g/g436.md) shall [lambanō](../../strongs/g/g2983.md) to themselves [krima](../../strongs/g/g2917.md).

<a name="romans_13_3"></a>Romans 13:3

For [archōn](../../strongs/g/g758.md) are not a [phobos](../../strongs/g/g5401.md) to [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md), but to the [kakos](../../strongs/g/g2556.md). Wilt thou then not be [phobeō](../../strongs/g/g5399.md) of the [exousia](../../strongs/g/g1849.md)? [poieō](../../strongs/g/g4160.md) that which is [agathos](../../strongs/g/g18.md), and thou shalt have [epainos](../../strongs/g/g1868.md) of the same:

<a name="romans_13_4"></a>Romans 13:4

For he is the [diakonos](../../strongs/g/g1249.md) of [theos](../../strongs/g/g2316.md) to thee for [agathos](../../strongs/g/g18.md). But if thou [poieō](../../strongs/g/g4160.md) that which is [kakos](../../strongs/g/g2556.md), be [phobeō](../../strongs/g/g5399.md); for he [phoreō](../../strongs/g/g5409.md) not the [machaira](../../strongs/g/g3162.md) [eikē](../../strongs/g/g1500.md): for he is the [diakonos](../../strongs/g/g1249.md) of [theos](../../strongs/g/g2316.md), an [ekdikos](../../strongs/g/g1558.md) to [orgē](../../strongs/g/g3709.md) upon him that [prassō](../../strongs/g/g4238.md) [kakos](../../strongs/g/g2556.md).

<a name="romans_13_5"></a>Romans 13:5

Wherefore ye must [anagkē](../../strongs/g/g318.md) be [hypotassō](../../strongs/g/g5293.md), not only for [orgē](../../strongs/g/g3709.md), but also for [syneidēsis](../../strongs/g/g4893.md) sake.

<a name="romans_13_6"></a>Romans 13:6

For for this cause [teleō](../../strongs/g/g5055.md) ye [phoros](../../strongs/g/g5411.md) also: for they are [theos](../../strongs/g/g2316.md) [leitourgos](../../strongs/g/g3011.md), [proskartereō](../../strongs/g/g4342.md) upon this very thing.

<a name="romans_13_7"></a>Romans 13:7

[apodidōmi](../../strongs/g/g591.md) therefore to all their [opheilē](../../strongs/g/g3782.md): [phoros](../../strongs/g/g5411.md) to whom [phoros](../../strongs/g/g5411.md) is due; [telos](../../strongs/g/g5056.md) to whom [telos](../../strongs/g/g5056.md); [phobos](../../strongs/g/g5401.md) to whom [phobos](../../strongs/g/g5401.md); [timē](../../strongs/g/g5092.md) to whom [timē](../../strongs/g/g5092.md).

<a name="romans_13_8"></a>Romans 13:8

[opheilō](../../strongs/g/g3784.md) [mēdeis](../../strongs/g/g3367.md) [mēdeis](../../strongs/g/g3367.md), but to [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md): for he that [agapaō](../../strongs/g/g25.md) another hath [plēroō](../../strongs/g/g4137.md) the [nomos](../../strongs/g/g3551.md).

<a name="romans_13_9"></a>Romans 13:9

For this, Thou shalt not [moicheuō](../../strongs/g/g3431.md), Thou shalt not [phoneuō](../../strongs/g/g5407.md), Thou shalt not [kleptō](../../strongs/g/g2813.md), Thou shalt not [pseudomartyreō](../../strongs/g/g5576.md), Thou shalt not [epithymeō](../../strongs/g/g1937.md); and if there be any other [entolē](../../strongs/g/g1785.md), it is [anakephalaioō](../../strongs/g/g346.md) in this [logos](../../strongs/g/g3056.md), namely, Thou shalt [agapaō](../../strongs/g/g25.md) thy [plēsion](../../strongs/g/g4139.md) as thyself.

<a name="romans_13_10"></a>Romans 13:10

[agapē](../../strongs/g/g26.md) [ergazomai](../../strongs/g/g2038.md) no [kakos](../../strongs/g/g2556.md) to his [plēsion](../../strongs/g/g4139.md): therefore [agapē](../../strongs/g/g26.md) is the [plērōma](../../strongs/g/g4138.md) of the [nomos](../../strongs/g/g3551.md).

<a name="romans_13_11"></a>Romans 13:11

And that, [eidō](../../strongs/g/g1492.md) the [kairos](../../strongs/g/g2540.md), that now [hōra](../../strongs/g/g5610.md) to [hēmas](../../strongs/g/g2248.md) [egeirō](../../strongs/g/g1453.md) out of [hypnos](../../strongs/g/g5258.md): for now our [sōtēria](../../strongs/g/g4991.md) [engyteron](../../strongs/g/g1452.md) than when we [pisteuō](../../strongs/g/g4100.md).

<a name="romans_13_12"></a>Romans 13:12

The [nyx](../../strongs/g/g3571.md) is [prokoptō](../../strongs/g/g4298.md), the [hēmera](../../strongs/g/g2250.md) is [eggizō](../../strongs/g/g1448.md): let us therefore [apotithēmi](../../strongs/g/g659.md) the [ergon](../../strongs/g/g2041.md) of [skotos](../../strongs/g/g4655.md), and let us [endyō](../../strongs/g/g1746.md) the [hoplon](../../strongs/g/g3696.md) of [phōs](../../strongs/g/g5457.md).

<a name="romans_13_13"></a>Romans 13:13

Let us [peripateō](../../strongs/g/g4043.md) [euschēmonōs](../../strongs/g/g2156.md), as in the [hēmera](../../strongs/g/g2250.md); not in [kōmos](../../strongs/g/g2970.md) and [methē](../../strongs/g/g3178.md), not in [koitē](../../strongs/g/g2845.md) and [aselgeia](../../strongs/g/g766.md), not in [eris](../../strongs/g/g2054.md) and [zēlos](../../strongs/g/g2205.md).

<a name="romans_13_14"></a>Romans 13:14

But [endyō](../../strongs/g/g1746.md) ye the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and [poieō](../../strongs/g/g4160.md) not [pronoia](../../strongs/g/g4307.md) for the [sarx](../../strongs/g/g4561.md), to the [epithymia](../../strongs/g/g1939.md).

---

[Transliteral Bible](../bible.md)

[Romans](romans.md)

[Romans 12](romans_12.md) - [Romans 14](romans_14.md)