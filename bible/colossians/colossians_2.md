# [Colossians 2](https://www.blueletterbible.org/kjv/col/2/1/s_1109001)

<a name="colossians_2_1"></a>Colossians 2:1

For I would that ye [eidō](../../strongs/g/g1492.md) [hēlikos](../../strongs/g/g2245.md) [agōn](../../strongs/g/g73.md) I have for you, and them at [laodikeia](../../strongs/g/g2993.md), and as many as have not [horaō](../../strongs/g/g3708.md) my [prosōpon](../../strongs/g/g4383.md) in the [sarx](../../strongs/g/g4561.md);

<a name="colossians_2_2"></a>Colossians 2:2

That their [kardia](../../strongs/g/g2588.md) might be [parakaleō](../../strongs/g/g3870.md), being [symbibazō](../../strongs/g/g4822.md) together in [agapē](../../strongs/g/g26.md), and unto all [ploutos](../../strongs/g/g4149.md) of the [plērophoria](../../strongs/g/g4136.md) of [synesis](../../strongs/g/g4907.md), to the [epignōsis](../../strongs/g/g1922.md) of the [mystērion](../../strongs/g/g3466.md) of [theos](../../strongs/g/g2316.md), and of the [patēr](../../strongs/g/g3962.md), and of [Christos](../../strongs/g/g5547.md);

<a name="colossians_2_3"></a>Colossians 2:3

In whom are [apokryphos](../../strongs/g/g614.md) all the [thēsauros](../../strongs/g/g2344.md) of [sophia](../../strongs/g/g4678.md) and [gnōsis](../../strongs/g/g1108.md).

<a name="colossians_2_4"></a>Colossians 2:4

And this I [legō](../../strongs/g/g3004.md), lest [tis](../../strongs/g/g5100.md) should [paralogizomai](../../strongs/g/g3884.md) you with [pithanologia](../../strongs/g/g4086.md).

<a name="colossians_2_5"></a>Colossians 2:5

For though I be [apeimi](../../strongs/g/g548.md) in the [sarx](../../strongs/g/g4561.md), yet am I with you in the [pneuma](../../strongs/g/g4151.md), [chairō](../../strongs/g/g5463.md) and [blepō](../../strongs/g/g991.md) your [taxis](../../strongs/g/g5010.md), and the [stereōma](../../strongs/g/g4733.md) of your [pistis](../../strongs/g/g4102.md) in [Christos](../../strongs/g/g5547.md).

<a name="colossians_2_6"></a>Colossians 2:6

As ye have therefore [paralambanō](../../strongs/g/g3880.md) [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) the [kyrios](../../strongs/g/g2962.md), so [peripateō](../../strongs/g/g4043.md) ye in him:

<a name="colossians_2_7"></a>Colossians 2:7

[rhizoō](../../strongs/g/g4492.md) and [epoikodomeō](../../strongs/g/g2026.md) in him, and [bebaioō](../../strongs/g/g950.md) in the [pistis](../../strongs/g/g4102.md), as ye have been [didaskō](../../strongs/g/g1321.md), [perisseuō](../../strongs/g/g4052.md) therein with [eucharistia](../../strongs/g/g2169.md).

<a name="colossians_2_8"></a>Colossians 2:8

[blepō](../../strongs/g/g991.md) lest [tis](../../strongs/g/g5100.md) [esomai](../../strongs/g/g2071.md) [sylagōgeō](../../strongs/g/g4812.md) you through [philosophia](../../strongs/g/g5385.md) and [kenos](../../strongs/g/g2756.md) [apatē](../../strongs/g/g539.md), after the [paradosis](../../strongs/g/g3862.md) of [anthrōpos](../../strongs/g/g444.md), after the [stoicheion](../../strongs/g/g4747.md) of the [kosmos](../../strongs/g/g2889.md), and not after [Christos](../../strongs/g/g5547.md).

<a name="colossians_2_9"></a>Colossians 2:9

For in him [katoikeō](../../strongs/g/g2730.md) all the [plērōma](../../strongs/g/g4138.md) of the [theotēs](../../strongs/g/g2320.md) [sōmatikōs](../../strongs/g/g4985.md).

<a name="colossians_2_10"></a>Colossians 2:10

And ye are [plēroō](../../strongs/g/g4137.md) in him, which is the [kephalē](../../strongs/g/g2776.md) of all [archē](../../strongs/g/g746.md) and [exousia](../../strongs/g/g1849.md):

<a name="colossians_2_11"></a>Colossians 2:11

In whom also ye are [peritemnō](../../strongs/g/g4059.md) with the [peritomē](../../strongs/g/g4061.md) [acheiropoiētos](../../strongs/g/g886.md), in [apekdysis](../../strongs/g/g555.md) the [sōma](../../strongs/g/g4983.md) of the [hamartia](../../strongs/g/g266.md) of the [sarx](../../strongs/g/g4561.md) by the [peritomē](../../strongs/g/g4061.md) of [Christos](../../strongs/g/g5547.md):

<a name="colossians_2_12"></a>Colossians 2:12

[synthaptō](../../strongs/g/g4916.md) with him in [baptisma](../../strongs/g/g908.md), wherein also ye are [synegeirō](../../strongs/g/g4891.md) with through the [pistis](../../strongs/g/g4102.md) of the [energeia](../../strongs/g/g1753.md) of [theos](../../strongs/g/g2316.md), who hath [egeirō](../../strongs/g/g1453.md) him from the [nekros](../../strongs/g/g3498.md).

<a name="colossians_2_13"></a>Colossians 2:13

And you, being [nekros](../../strongs/g/g3498.md) in your [paraptōma](../../strongs/g/g3900.md) and the [akrobystia](../../strongs/g/g203.md) of your [sarx](../../strongs/g/g4561.md), hath he [syzōopoieō](../../strongs/g/g4806.md) with him, having [charizomai](../../strongs/g/g5483.md) you all [paraptōma](../../strongs/g/g3900.md);

<a name="colossians_2_14"></a>Colossians 2:14

[exaleiphō](../../strongs/g/g1813.md) out the [cheirographon](../../strongs/g/g5498.md) of [dogma](../../strongs/g/g1378.md) that was against us, which was [hypenantios](../../strongs/g/g5227.md) to us, and [airō](../../strongs/g/g142.md) it out of the [mesos](../../strongs/g/g3319.md), [prosēloō](../../strongs/g/g4338.md) it to his [stauros](../../strongs/g/g4716.md);

<a name="colossians_2_15"></a>Colossians 2:15

having [apekdyomai](../../strongs/g/g554.md) [archē](../../strongs/g/g746.md) and [exousia](../../strongs/g/g1849.md), he [deigmatizō](../../strongs/g/g1165.md) of them [parrēsia](../../strongs/g/g3954.md), [thriambeuō](../../strongs/g/g2358.md) over them in it.

<a name="colossians_2_16"></a>Colossians 2:16

Let no [tis](../../strongs/g/g5100.md) therefore [krinō](../../strongs/g/g2919.md) you in [brōsis](../../strongs/g/g1035.md), or in [posis](../../strongs/g/g4213.md), or in [meros](../../strongs/g/g3313.md) of an [heortē](../../strongs/g/g1859.md), or of the [neomēnia](../../strongs/g/g3561.md), or of the [sabbaton](../../strongs/g/g4521.md):

<a name="colossians_2_17"></a>Colossians 2:17

Which are a [skia](../../strongs/g/g4639.md) of things to [mellō](../../strongs/g/g3195.md); but the [sōma](../../strongs/g/g4983.md) of [Christos](../../strongs/g/g5547.md).

<a name="colossians_2_18"></a>Colossians 2:18

Let [mēdeis](../../strongs/g/g3367.md) [katabrabeuō](../../strongs/g/g2603.md) you in a [thelō](../../strongs/g/g2309.md) [tapeinophrosynē](../../strongs/g/g5012.md) and [thrēskeia](../../strongs/g/g2356.md) of [aggelos](../../strongs/g/g32.md), [embateuō](../../strongs/g/g1687.md) those things which he hath not [horaō](../../strongs/g/g3708.md), [eikē](../../strongs/g/g1500.md) [physioō](../../strongs/g/g5448.md) by his [sarx](../../strongs/g/g4561.md) [nous](../../strongs/g/g3563.md),

<a name="colossians_2_19"></a>Colossians 2:19

And not [krateō](../../strongs/g/g2902.md) the [kephalē](../../strongs/g/g2776.md), from which all the [sōma](../../strongs/g/g4983.md) by [haphē](../../strongs/g/g860.md) and [syndesmos](../../strongs/g/g4886.md) having [epichorēgeō](../../strongs/g/g2023.md), and [symbibazō](../../strongs/g/g4822.md), [auxanō](../../strongs/g/g837.md) with the [auxēsis](../../strongs/g/g838.md) of [theos](../../strongs/g/g2316.md).

<a name="colossians_2_20"></a>Colossians 2:20

Wherefore if ye be [apothnēskō](../../strongs/g/g599.md) with [Christos](../../strongs/g/g5547.md) from the [stoicheion](../../strongs/g/g4747.md) of the [kosmos](../../strongs/g/g2889.md), why, as though [zaō](../../strongs/g/g2198.md) in the [kosmos](../../strongs/g/g2889.md), are ye [dogmatizō](../../strongs/g/g1379.md),

<a name="colossians_2_21"></a>Colossians 2:21

([haptomai](../../strongs/g/g680.md) not; [geuomai](../../strongs/g/g1089.md) not; [thinganō](../../strongs/g/g2345.md) not;

<a name="colossians_2_22"></a>Colossians 2:22

Which all are to [phthora](../../strongs/g/g5356.md) with the [apochrēsis](../../strongs/g/g671.md);) after the [entalma](../../strongs/g/g1778.md) and [didaskalia](../../strongs/g/g1319.md) of [anthrōpos](../../strongs/g/g444.md)?

<a name="colossians_2_23"></a>Colossians 2:23

Which things have indeed a [logos](../../strongs/g/g3056.md) of [sophia](../../strongs/g/g4678.md) in [ethelothrēskia](../../strongs/g/g1479.md), and [tapeinophrosynē](../../strongs/g/g5012.md), and [apheidia](../../strongs/g/g857.md) of the [sōma](../../strongs/g/g4983.md): not in any [timē](../../strongs/g/g5092.md) to the [plēsmonē](../../strongs/g/g4140.md) of the [sarx](../../strongs/g/g4561.md).

---

[Transliteral Bible](../bible.md)

[Colossians](colossians.md)

[Colossians 1](colossians_1.md) - [Colossians 3](colossians_3.md)