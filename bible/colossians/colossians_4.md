# [Colossians 4](https://www.blueletterbible.org/kjv/col/4/1/s_1111001)

<a name="colossians_4_1"></a>Colossians 4:1

[kyrios](../../strongs/g/g2962.md), [parechō](../../strongs/g/g3930.md) unto [doulos](../../strongs/g/g1401.md) that which is [dikaios](../../strongs/g/g1342.md) and [isotēs](../../strongs/g/g2471.md); [eidō](../../strongs/g/g1492.md) that ye also have a [kyrios](../../strongs/g/g2962.md) in [ouranos](../../strongs/g/g3772.md).

<a name="colossians_4_2"></a>Colossians 4:2

[proskartereō](../../strongs/g/g4342.md) in [proseuchē](../../strongs/g/g4335.md), and [grēgoreō](../../strongs/g/g1127.md) in the same with [eucharistia](../../strongs/g/g2169.md);

<a name="colossians_4_3"></a>Colossians 4:3

Withal [proseuchomai](../../strongs/g/g4336.md) also for us, that [theos](../../strongs/g/g2316.md) would [anoigō](../../strongs/g/g455.md) unto us a [thyra](../../strongs/g/g2374.md) of [logos](../../strongs/g/g3056.md), to [laleō](../../strongs/g/g2980.md) the [mystērion](../../strongs/g/g3466.md) of [Christos](../../strongs/g/g5547.md), for which I am also [deō](../../strongs/g/g1210.md):

<a name="colossians_4_4"></a>Colossians 4:4

That I may [phaneroō](../../strongs/g/g5319.md) it, as I ought to [laleō](../../strongs/g/g2980.md).

<a name="colossians_4_5"></a>Colossians 4:5

[peripateō](../../strongs/g/g4043.md) in [sophia](../../strongs/g/g4678.md) toward them that are without, [exagorazō](../../strongs/g/g1805.md) the [kairos](../../strongs/g/g2540.md).

<a name="colossians_4_6"></a>Colossians 4:6

Let your [logos](../../strongs/g/g3056.md) [pantote](../../strongs/g/g3842.md) with [charis](../../strongs/g/g5485.md), [artyō](../../strongs/g/g741.md) with [halas](../../strongs/g/g217.md), that ye may [eidō](../../strongs/g/g1492.md) how ye ought to [apokrinomai](../../strongs/g/g611.md) every man.

<a name="colossians_4_7"></a>Colossians 4:7

All my state shall [Tychikos](../../strongs/g/g5190.md) [gnōrizō](../../strongs/g/g1107.md) unto you, an [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md), and a [pistos](../../strongs/g/g4103.md) [diakonos](../../strongs/g/g1249.md) and [syndoulos](../../strongs/g/g4889.md) in the [kyrios](../../strongs/g/g2962.md):

<a name="colossians_4_8"></a>Colossians 4:8

Whom I have [pempō](../../strongs/g/g3992.md) unto you for the same [touto](../../strongs/g/g5124.md), that he might [ginōskō](../../strongs/g/g1097.md) your [peri](../../strongs/g/g4012.md), and [parakaleō](../../strongs/g/g3870.md) your [kardia](../../strongs/g/g2588.md);

<a name="colossians_4_9"></a>Colossians 4:9

With [Onēsimos](../../strongs/g/g3682.md), a [pistos](../../strongs/g/g4103.md) and [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md), who is one of you. They shall [gnōrizō](../../strongs/g/g1107.md) unto you all things which here.

<a name="colossians_4_10"></a>Colossians 4:10

[Aristarchos](../../strongs/g/g708.md) my [synaichmalōtos](../../strongs/g/g4869.md) [aspazomai](../../strongs/g/g782.md) you, and [Markos](../../strongs/g/g3138.md), [anepsios](../../strongs/g/g431.md) to [Barnabas](../../strongs/g/g921.md), (touching whom ye [lambanō](../../strongs/g/g2983.md) [entolē](../../strongs/g/g1785.md): if he [erchomai](../../strongs/g/g2064.md) unto you, [dechomai](../../strongs/g/g1209.md) him;)

<a name="colossians_4_11"></a>Colossians 4:11

And [Iēsous](../../strongs/g/g2424.md), which is [legō](../../strongs/g/g3004.md) [Ioustos](../../strongs/g/g2459.md), who are of the [peritomē](../../strongs/g/g4061.md). These only [synergos](../../strongs/g/g4904.md) unto the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), which have been a [parēgoria](../../strongs/g/g3931.md) unto me.

<a name="colossians_4_12"></a>Colossians 4:12

[epaphras](../../strongs/g/g1889.md), who is one of you, a [doulos](../../strongs/g/g1401.md) of [Christos](../../strongs/g/g5547.md), [aspazomai](../../strongs/g/g782.md) you, [pantote](../../strongs/g/g3842.md) [agōnizomai](../../strongs/g/g75.md) for you in [proseuchē](../../strongs/g/g4335.md), that ye may [histēmi](../../strongs/g/g2476.md) [teleios](../../strongs/g/g5046.md) and [plēroō](../../strongs/g/g4137.md) in all the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md).

<a name="colossians_4_13"></a>Colossians 4:13

For I [martyreō](../../strongs/g/g3140.md) him, that he hath a [polys](../../strongs/g/g4183.md) [zēlos](../../strongs/g/g2205.md) for you, and them in [laodikeia](../../strongs/g/g2993.md), and them in [hierapolis](../../strongs/g/g2404.md).

<a name="colossians_4_14"></a>Colossians 4:14

[loukas](../../strongs/g/g3065.md), the [agapētos](../../strongs/g/g27.md) [iatros](../../strongs/g/g2395.md), and [dēmas](../../strongs/g/g1214.md), [aspazomai](../../strongs/g/g782.md) you.

<a name="colossians_4_15"></a>Colossians 4:15

[aspazomai](../../strongs/g/g782.md) the [adelphos](../../strongs/g/g80.md) which are in [laodikeia](../../strongs/g/g2993.md), and [nympha](../../strongs/g/g3564.md), and the [ekklēsia](../../strongs/g/g1577.md) which is in his [oikos](../../strongs/g/g3624.md).

<a name="colossians_4_16"></a>Colossians 4:16

And when this [epistolē](../../strongs/g/g1992.md) is [anaginōskō](../../strongs/g/g314.md) among you, [poieō](../../strongs/g/g4160.md) that it be [anaginōskō](../../strongs/g/g314.md) also in the [ekklēsia](../../strongs/g/g1577.md) of the [laodikeus](../../strongs/g/g2994.md); and that ye likewise [anaginōskō](../../strongs/g/g314.md) from [laodikeia](../../strongs/g/g2993.md).

<a name="colossians_4_17"></a>Colossians 4:17

And [eipon](../../strongs/g/g2036.md) to [archippos](../../strongs/g/g751.md), [blepō](../../strongs/g/g991.md) to the [diakonia](../../strongs/g/g1248.md) which thou hast [paralambanō](../../strongs/g/g3880.md) in the [kyrios](../../strongs/g/g2962.md), that thou [plēroō](../../strongs/g/g4137.md) it.

<a name="colossians_4_18"></a>Colossians 4:18

The [aspasmos](../../strongs/g/g783.md) by the [cheir](../../strongs/g/g5495.md) of me [Paulos](../../strongs/g/g3972.md). [mnēmoneuō](../../strongs/g/g3421.md) my [desmos](../../strongs/g/g1199.md). [charis](../../strongs/g/g5485.md) be with you. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Colossians](colossians.md)

[Colossians 3](colossians_3.md)