# [Colossians 3](https://www.blueletterbible.org/kjv/col/3/1/s_1110001i)

<a name="colossians_3_1"></a>Colossians 3:1

If ye then be [synegeirō](../../strongs/g/g4891.md) with [Christos](../../strongs/g/g5547.md), [zēteō](../../strongs/g/g2212.md) those things which are [anō](../../strongs/g/g507.md), where [Christos](../../strongs/g/g5547.md) [kathēmai](../../strongs/g/g2521.md) on the [dexios](../../strongs/g/g1188.md) of [theos](../../strongs/g/g2316.md).

<a name="colossians_3_2"></a>Colossians 3:2

[phroneō](../../strongs/g/g5426.md) on things [anō](../../strongs/g/g507.md), not on things on [gē](../../strongs/g/g1093.md).

<a name="colossians_3_3"></a>Colossians 3:3

For ye are [apothnēskō](../../strongs/g/g599.md), and your [zōē](../../strongs/g/g2222.md) is [kryptō](../../strongs/g/g2928.md) with [Christos](../../strongs/g/g5547.md) in [theos](../../strongs/g/g2316.md).

<a name="colossians_3_4"></a>Colossians 3:4

When [Christos](../../strongs/g/g5547.md), our [zōē](../../strongs/g/g2222.md), shall [phaneroō](../../strongs/g/g5319.md), then shall ye also [phaneroō](../../strongs/g/g5319.md) with him in [doxa](../../strongs/g/g1391.md).

<a name="colossians_3_5"></a>Colossians 3:5

[nekroō](../../strongs/g/g3499.md) therefore your [melos](../../strongs/g/g3196.md) which are upon [gē](../../strongs/g/g1093.md); [porneia](../../strongs/g/g4202.md), [akatharsia](../../strongs/g/g167.md), [pathos](../../strongs/g/g3806.md), [kakos](../../strongs/g/g2556.md) [epithymia](../../strongs/g/g1939.md), and [pleonexia](../../strongs/g/g4124.md), which is [eidōlolatria](../../strongs/g/g1495.md):

<a name="colossians_3_6"></a>Colossians 3:6

For which things' sake the [orgē](../../strongs/g/g3709.md) of [theos](../../strongs/g/g2316.md) [erchomai](../../strongs/g/g2064.md) on the [huios](../../strongs/g/g5207.md) of [apeitheia](../../strongs/g/g543.md):

<a name="colossians_3_7"></a>Colossians 3:7

In the which ye also [peripateō](../../strongs/g/g4043.md) some time, when ye [zaō](../../strongs/g/g2198.md) in them.

<a name="colossians_3_8"></a>Colossians 3:8

But [nyni](../../strongs/g/g3570.md) ye also [apotithēmi](../../strongs/g/g659.md) all these; [orgē](../../strongs/g/g3709.md), [thymos](../../strongs/g/g2372.md), [kakia](../../strongs/g/g2549.md), [blasphēmia](../../strongs/g/g988.md), [aischrologia](../../strongs/g/g148.md) out of your [stoma](../../strongs/g/g4750.md).

<a name="colossians_3_9"></a>Colossians 3:9

[pseudomai](../../strongs/g/g5574.md) not to [allēlōn](../../strongs/g/g240.md), seeing that ye have [apekdyomai](../../strongs/g/g554.md) the [palaios](../../strongs/g/g3820.md) [anthrōpos](../../strongs/g/g444.md) with his [praxis](../../strongs/g/g4234.md);

<a name="colossians_3_10"></a>Colossians 3:10

And have [endyō](../../strongs/g/g1746.md) the [neos](../../strongs/g/g3501.md), which is [anakainoō](../../strongs/g/g341.md) in [epignōsis](../../strongs/g/g1922.md) after the [eikōn](../../strongs/g/g1504.md) of him that [ktizō](../../strongs/g/g2936.md) him:

<a name="colossians_3_11"></a>Colossians 3:11

Where there is neither [Hellēn](../../strongs/g/g1672.md) nor [Ioudaios](../../strongs/g/g2453.md), [peritomē](../../strongs/g/g4061.md) nor [akrobystia](../../strongs/g/g203.md), [barbaros](../../strongs/g/g915.md), [skythēs](../../strongs/g/g4658.md), [doulos](../../strongs/g/g1401.md) nor [eleutheros](../../strongs/g/g1658.md): but [Christos](../../strongs/g/g5547.md) is all, and in all.

<a name="colossians_3_12"></a>Colossians 3:12

[endyō](../../strongs/g/g1746.md) therefore, as the [eklektos](../../strongs/g/g1588.md) of [theos](../../strongs/g/g2316.md), [hagios](../../strongs/g/g40.md) and [agapaō](../../strongs/g/g25.md), [splagchnon](../../strongs/g/g4698.md) of [oiktirmos](../../strongs/g/g3628.md), [chrēstotēs](../../strongs/g/g5544.md), [tapeinophrosynē](../../strongs/g/g5012.md), [praotēs](../../strongs/g/g4236.md), [makrothymia](../../strongs/g/g3115.md);

<a name="colossians_3_13"></a>Colossians 3:13

[anechō](../../strongs/g/g430.md) [allēlōn](../../strongs/g/g240.md), and [charizomai](../../strongs/g/g5483.md) [heautou](../../strongs/g/g1438.md), if any man have a [momphē](../../strongs/g/g3437.md) against any: even as [Christos](../../strongs/g/g5547.md) [charizomai](../../strongs/g/g5483.md) you, so also do ye.

<a name="colossians_3_14"></a>Colossians 3:14

And above all these things put on [agapē](../../strongs/g/g26.md), which is the [syndesmos](../../strongs/g/g4886.md) of [teleiotēs](../../strongs/g/g5047.md).

<a name="colossians_3_15"></a>Colossians 3:15

And let the [eirēnē](../../strongs/g/g1515.md) of [theos](../../strongs/g/g2316.md) [brabeuō](../../strongs/g/g1018.md) in your [kardia](../../strongs/g/g2588.md), to the which also ye are [kaleō](../../strongs/g/g2564.md) in one [sōma](../../strongs/g/g4983.md); and be ye [eucharistos](../../strongs/g/g2170.md).

<a name="colossians_3_16"></a>Colossians 3:16

Let the [logos](../../strongs/g/g3056.md) of [Christos](../../strongs/g/g5547.md) [enoikeō](../../strongs/g/g1774.md) in you [plousiōs](../../strongs/g/g4146.md) in all [sophia](../../strongs/g/g4678.md); [didaskō](../../strongs/g/g1321.md) and [noutheteō](../../strongs/g/g3560.md) one another in [psalmos](../../strongs/g/g5568.md) and [hymnos](../../strongs/g/g5215.md) and [pneumatikos](../../strongs/g/g4152.md) [ōdē](../../strongs/g/g5603.md), [adō](../../strongs/g/g103.md) with [charis](../../strongs/g/g5485.md) in your [kardia](../../strongs/g/g2588.md) to the [kyrios](../../strongs/g/g2962.md).

<a name="colossians_3_17"></a>Colossians 3:17

And whatsoever ye do in [logos](../../strongs/g/g3056.md) or [ergon](../../strongs/g/g2041.md), [poieō](../../strongs/g/g4160.md) all in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), [eucharisteō](../../strongs/g/g2168.md) to [theos](../../strongs/g/g2316.md) and the [patēr](../../strongs/g/g3962.md) by him.

<a name="colossians_3_18"></a>Colossians 3:18

[gynē](../../strongs/g/g1135.md), [hypotassō](../../strongs/g/g5293.md) yourselves unto your own [anēr](../../strongs/g/g435.md), as it is [anēkō](../../strongs/g/g433.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="colossians_3_19"></a>Colossians 3:19

[anēr](../../strongs/g/g435.md), [agapaō](../../strongs/g/g25.md) your [gynē](../../strongs/g/g1135.md), and be not [pikrainō](../../strongs/g/g4087.md) against them.

<a name="colossians_3_20"></a>Colossians 3:20

[teknon](../../strongs/g/g5043.md), [hypakouō](../../strongs/g/g5219.md) your [goneus](../../strongs/g/g1118.md) in all things: for this is [euarestos](../../strongs/g/g2101.md) unto the [kyrios](../../strongs/g/g2962.md).

<a name="colossians_3_21"></a>Colossians 3:21

[patēr](../../strongs/g/g3962.md), [erethizō](../../strongs/g/g2042.md) not your [teknon](../../strongs/g/g5043.md), lest they be [athymeō](../../strongs/g/g120.md).

<a name="colossians_3_22"></a>Colossians 3:22

[doulos](../../strongs/g/g1401.md), [hypakouō](../../strongs/g/g5219.md) in all things your [kyrios](../../strongs/g/g2962.md) according to the [sarx](../../strongs/g/g4561.md); not with [ophthalmodoulia](../../strongs/g/g3787.md), as [anthrōpareskos](../../strongs/g/g441.md); but in [haplotēs](../../strongs/g/g572.md) of [kardia](../../strongs/g/g2588.md), [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md);

<a name="colossians_3_23"></a>Colossians 3:23

And whatsoever ye [poieō](../../strongs/g/g4160.md), [ergazomai](../../strongs/g/g2038.md) it [ek](../../strongs/g/g1537.md) [psychē](../../strongs/g/g5590.md), as to the [kyrios](../../strongs/g/g2962.md), and not unto [anthrōpos](../../strongs/g/g444.md);

<a name="colossians_3_24"></a>Colossians 3:24

[eidō](../../strongs/g/g1492.md) that of the [kyrios](../../strongs/g/g2962.md) ye shall [apolambanō](../../strongs/g/g618.md) the [antapodosis](../../strongs/g/g469.md) of the [klēronomia](../../strongs/g/g2817.md): for ye [douleuō](../../strongs/g/g1398.md) the [kyrios](../../strongs/g/g2962.md) [Christos](../../strongs/g/g5547.md).

<a name="colossians_3_25"></a>Colossians 3:25

But he that [adikeō](../../strongs/g/g91.md) shall [komizō](../../strongs/g/g2865.md) for the [adikeō](../../strongs/g/g91.md) which he hath [adikeō](../../strongs/g/g91.md): and there is no [prosōpolēmpsia](../../strongs/g/g4382.md).

---

[Transliteral Bible](../bible.md)

[Colossians](colossians.md)

[Colossians 2](colossians_2.md) - [Colossians 4](colossians_4.md)