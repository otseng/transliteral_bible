# [Colossians 1](https://www.blueletterbible.org/kjv/col/1/1/s_1108001)

<a name="colossians_1_1"></a>Colossians 1:1

[Paulos](../../strongs/g/g3972.md), an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) by the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), and [Timotheos](../../strongs/g/g5095.md) [adelphos](../../strongs/g/g80.md),

<a name="colossians_1_2"></a>Colossians 1:2

To the [hagios](../../strongs/g/g40.md) and [pistos](../../strongs/g/g4103.md) [adelphos](../../strongs/g/g80.md) in [Christos](../../strongs/g/g5547.md) which are at [Kolossai](../../strongs/g/g2857.md): [charis](../../strongs/g/g5485.md) be unto you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="colossians_1_3"></a>Colossians 1:3

We [eucharisteō](../../strongs/g/g2168.md) to [theos](../../strongs/g/g2316.md) and the [patēr](../../strongs/g/g3962.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [proseuchomai](../../strongs/g/g4336.md) [pantote](../../strongs/g/g3842.md) for you,

<a name="colossians_1_4"></a>Colossians 1:4

Since we [akouō](../../strongs/g/g191.md) of your [pistis](../../strongs/g/g4102.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), and of the [agapē](../../strongs/g/g26.md) to all the [hagios](../../strongs/g/g40.md),

<a name="colossians_1_5"></a>Colossians 1:5

For the [elpis](../../strongs/g/g1680.md) which is [apokeimai](../../strongs/g/g606.md) for you in [ouranos](../../strongs/g/g3772.md), whereof ye [proakouō](../../strongs/g/g4257.md) in the [logos](../../strongs/g/g3056.md) of the [alētheia](../../strongs/g/g225.md) of the [euaggelion](../../strongs/g/g2098.md);

<a name="colossians_1_6"></a>Colossians 1:6

Which [pareimi](../../strongs/g/g3918.md) unto you, as it is in all the [kosmos](../../strongs/g/g2889.md); and [karpophoreō](../../strongs/g/g2592.md), as it doth also in you, since the [hēmera](../../strongs/g/g2250.md) ye [akouō](../../strongs/g/g191.md) of it, and [epiginōskō](../../strongs/g/g1921.md) the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) in [alētheia](../../strongs/g/g225.md):

<a name="colossians_1_7"></a>Colossians 1:7

As ye also [manthanō](../../strongs/g/g3129.md) of [epaphras](../../strongs/g/g1889.md) our [agapētos](../../strongs/g/g27.md) [syndoulos](../../strongs/g/g4889.md), who is for you a [pistos](../../strongs/g/g4103.md) [diakonos](../../strongs/g/g1249.md) of [Christos](../../strongs/g/g5547.md);

<a name="colossians_1_8"></a>Colossians 1:8

Who also [dēloō](../../strongs/g/g1213.md) unto us your [agapē](../../strongs/g/g26.md) in the [pneuma](../../strongs/g/g4151.md).

<a name="colossians_1_9"></a>Colossians 1:9

For this cause we also, since the [hēmera](../../strongs/g/g2250.md) we [akouō](../../strongs/g/g191.md) it, do not [pauō](../../strongs/g/g3973.md) to [proseuchomai](../../strongs/g/g4336.md) for you, and to [aiteō](../../strongs/g/g154.md) that ye might be [plēroō](../../strongs/g/g4137.md) with the [epignōsis](../../strongs/g/g1922.md) of his [thelēma](../../strongs/g/g2307.md) in all [sophia](../../strongs/g/g4678.md) and [pneumatikos](../../strongs/g/g4152.md) [synesis](../../strongs/g/g4907.md);

<a name="colossians_1_10"></a>Colossians 1:10

That ye might [peripateō](../../strongs/g/g4043.md) [axiōs](../../strongs/g/g516.md) of the [kyrios](../../strongs/g/g2962.md) unto all [areskeia](../../strongs/g/g699.md), being [karpophoreō](../../strongs/g/g2592.md) in every [agathos](../../strongs/g/g18.md) [ergon](../../strongs/g/g2041.md), and [auxanō](../../strongs/g/g837.md) in the [epignōsis](../../strongs/g/g1922.md) of [theos](../../strongs/g/g2316.md);

<a name="colossians_1_11"></a>Colossians 1:11

[dynamoō](../../strongs/g/g1412.md) with all [dynamis](../../strongs/g/g1411.md), according to his [doxa](../../strongs/g/g1391.md) [kratos](../../strongs/g/g2904.md), unto all [hypomonē](../../strongs/g/g5281.md) and [makrothymia](../../strongs/g/g3115.md) with [chara](../../strongs/g/g5479.md);

<a name="colossians_1_12"></a>Colossians 1:12

[eucharisteō](../../strongs/g/g2168.md) unto the [patēr](../../strongs/g/g3962.md), which hath [hikanoō](../../strongs/g/g2427.md) us to be [meris](../../strongs/g/g3310.md) of the [klēros](../../strongs/g/g2819.md) of the [hagios](../../strongs/g/g40.md) in [phōs](../../strongs/g/g5457.md):

<a name="colossians_1_13"></a>Colossians 1:13

Who hath [rhyomai](../../strongs/g/g4506.md) us from the [exousia](../../strongs/g/g1849.md) of [skotos](../../strongs/g/g4655.md), and hath [methistēmi](../../strongs/g/g3179.md) into the [basileia](../../strongs/g/g932.md) of his [agapē](../../strongs/g/g26.md) [huios](../../strongs/g/g5207.md):

<a name="colossians_1_14"></a>Colossians 1:14

In whom we have [apolytrōsis](../../strongs/g/g629.md) through his [haima](../../strongs/g/g129.md), the [aphesis](../../strongs/g/g859.md) of [hamartia](../../strongs/g/g266.md):

<a name="colossians_1_15"></a>Colossians 1:15

Who is the [eikōn](../../strongs/g/g1504.md) of the [aoratos](../../strongs/g/g517.md) [theos](../../strongs/g/g2316.md), the [prōtotokos](../../strongs/g/g4416.md) of every [ktisis](../../strongs/g/g2937.md):

<a name="colossians_1_16"></a>Colossians 1:16

For by him were all things [ktizō](../../strongs/g/g2936.md), that are in [ouranos](../../strongs/g/g3772.md), and that are in [gē](../../strongs/g/g1093.md), [horatos](../../strongs/g/g3707.md) and [aoratos](../../strongs/g/g517.md), be [thronos](../../strongs/g/g2362.md), or [kyriotēs](../../strongs/g/g2963.md), or [archē](../../strongs/g/g746.md), or [exousia](../../strongs/g/g1849.md): all things were [ktizō](../../strongs/g/g2936.md) by him, and for him:

<a name="colossians_1_17"></a>Colossians 1:17

And he is [pro](../../strongs/g/g4253.md) [pas](../../strongs/g/g3956.md), and by him all things [synistēmi](../../strongs/g/g4921.md).

<a name="colossians_1_18"></a>Colossians 1:18

And he is the [kephalē](../../strongs/g/g2776.md) of the [sōma](../../strongs/g/g4983.md), the [ekklēsia](../../strongs/g/g1577.md): who is the [archē](../../strongs/g/g746.md), the [prōtotokos](../../strongs/g/g4416.md) from the [nekros](../../strongs/g/g3498.md); that in all he might have the [prōteuō](../../strongs/g/g4409.md).

<a name="colossians_1_19"></a>Colossians 1:19

For it [eudokeō](../../strongs/g/g2106.md) that in him should all [plērōma](../../strongs/g/g4138.md) [katoikeō](../../strongs/g/g2730.md);

<a name="colossians_1_20"></a>Colossians 1:20

And, having [eirēnopoieō](../../strongs/g/g1517.md) through the [haima](../../strongs/g/g129.md) of his [stauros](../../strongs/g/g4716.md), by him to [apokatallassō](../../strongs/g/g604.md) all things unto himself; by him, whether things in [gē](../../strongs/g/g1093.md), or things in [ouranos](../../strongs/g/g3772.md).

<a name="colossians_1_21"></a>Colossians 1:21

And you, that were sometime [apallotrioō](../../strongs/g/g526.md) and [echthros](../../strongs/g/g2190.md) in [dianoia](../../strongs/g/g1271.md) by [ponēros](../../strongs/g/g4190.md) [ergon](../../strongs/g/g2041.md), yet [nyni](../../strongs/g/g3570.md) hath he [apokatallassō](../../strongs/g/g604.md)

<a name="colossians_1_22"></a>Colossians 1:22

In the [sōma](../../strongs/g/g4983.md) of his [sarx](../../strongs/g/g4561.md) through [thanatos](../../strongs/g/g2288.md), to [paristēmi](../../strongs/g/g3936.md) you [hagios](../../strongs/g/g40.md) and [amōmos](../../strongs/g/g299.md) and [anegklētos](../../strongs/g/g410.md) in his [katenōpion](../../strongs/g/g2714.md):

<a name="colossians_1_23"></a>Colossians 1:23

If ye [epimenō](../../strongs/g/g1961.md) in the [pistis](../../strongs/g/g4102.md) [themelioō](../../strongs/g/g2311.md) and [hedraios](../../strongs/g/g1476.md), and not [metakineō](../../strongs/g/g3334.md) from the [elpis](../../strongs/g/g1680.md) of the [euaggelion](../../strongs/g/g2098.md), which ye have [akouō](../../strongs/g/g191.md), which was [kēryssō](../../strongs/g/g2784.md) to every [ktisis](../../strongs/g/g2937.md) which is under [ouranos](../../strongs/g/g3772.md); whereof I [Paulos](../../strongs/g/g3972.md) am made a [diakonos](../../strongs/g/g1249.md);

<a name="colossians_1_24"></a>Colossians 1:24

Who now [chairō](../../strongs/g/g5463.md) in my [pathēma](../../strongs/g/g3804.md) for you, and [antanaplēroō](../../strongs/g/g466.md) that which is [hysterēma](../../strongs/g/g5303.md) of the [thlipsis](../../strongs/g/g2347.md) of [Christos](../../strongs/g/g5547.md) in my [sarx](../../strongs/g/g4561.md) for his [sōma](../../strongs/g/g4983.md) sake, which is the [ekklēsia](../../strongs/g/g1577.md):

<a name="colossians_1_25"></a>Colossians 1:25

Whereof I am made a [diakonos](../../strongs/g/g1249.md), according to the [oikonomia](../../strongs/g/g3622.md) of [theos](../../strongs/g/g2316.md) which is [didōmi](../../strongs/g/g1325.md) to me for you, to [plēroō](../../strongs/g/g4137.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md);

<a name="colossians_1_26"></a>Colossians 1:26

the [mystērion](../../strongs/g/g3466.md) which hath been [apokryptō](../../strongs/g/g613.md) from [aiōn](../../strongs/g/g165.md) and from [genea](../../strongs/g/g1074.md), but [nyni](../../strongs/g/g3570.md) is made [phaneroō](../../strongs/g/g5319.md) to his [hagios](../../strongs/g/g40.md):

<a name="colossians_1_27"></a>Colossians 1:27

To whom [theos](../../strongs/g/g2316.md) would [gnōrizō](../../strongs/g/g1107.md) what the [ploutos](../../strongs/g/g4149.md) of the [doxa](../../strongs/g/g1391.md) of this [mystērion](../../strongs/g/g3466.md) among the [ethnos](../../strongs/g/g1484.md); which is [Christos](../../strongs/g/g5547.md) in you, the [elpis](../../strongs/g/g1680.md) of [doxa](../../strongs/g/g1391.md):

<a name="colossians_1_28"></a>Colossians 1:28

Whom we [kataggellō](../../strongs/g/g2605.md), [noutheteō](../../strongs/g/g3560.md) every [anthrōpos](../../strongs/g/g444.md), and [didaskō](../../strongs/g/g1321.md) every [anthrōpos](../../strongs/g/g444.md) in all [sophia](../../strongs/g/g4678.md); that we may [paristēmi](../../strongs/g/g3936.md) every [anthrōpos](../../strongs/g/g444.md) [teleios](../../strongs/g/g5046.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md):

<a name="colossians_1_29"></a>Colossians 1:29

Whereunto I also [kopiaō](../../strongs/g/g2872.md), [agōnizomai](../../strongs/g/g75.md) according to his [energeia](../../strongs/g/g1753.md), which [energeō](../../strongs/g/g1754.md) in me [dynamis](../../strongs/g/g1411.md).

---

[Transliteral Bible](../bible.md)

[Colossians](colossians.md)

[Colossians 2](colossians_2.md)