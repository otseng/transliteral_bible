# [Habakkuk 3](https://www.blueletterbible.org/kjv/habakkuk/3)

<a name="habakkuk_3_1"></a>Habakkuk 3:1

A [tĕphillah](../../strongs/h/h8605.md) of [Ḥăḇaqqûq](../../strongs/h/h2265.md) the [nāḇî'](../../strongs/h/h5030.md) upon [Šigāyôn](../../strongs/h/h7692.md).

<a name="habakkuk_3_2"></a>Habakkuk 3:2

[Yĕhovah](../../strongs/h/h3068.md), I have [shama'](../../strongs/h/h8085.md) thy [šēmaʿ](../../strongs/h/h8088.md), and was [yare'](../../strongs/h/h3372.md): [Yĕhovah](../../strongs/h/h3068.md), [ḥāyâ](../../strongs/h/h2421.md) thy [pōʿal](../../strongs/h/h6467.md) in the [qereḇ](../../strongs/h/h7130.md) of the [šānâ](../../strongs/h/h8141.md), in the [qereḇ](../../strongs/h/h7130.md) of the [šānâ](../../strongs/h/h8141.md) [yada'](../../strongs/h/h3045.md); in [rōḡez](../../strongs/h/h7267.md) [zakar](../../strongs/h/h2142.md) [racham](../../strongs/h/h7355.md).

<a name="habakkuk_3_3"></a>Habakkuk 3:3

['ĕlvôha](../../strongs/h/h433.md) [bow'](../../strongs/h/h935.md) from [Têmān](../../strongs/h/h8487.md), and the [qadowsh](../../strongs/h/h6918.md) from [har](../../strongs/h/h2022.md) [Pā'rān](../../strongs/h/h6290.md). [Celah](../../strongs/h/h5542.md). His [howd](../../strongs/h/h1935.md) [kāsâ](../../strongs/h/h3680.md) the [shamayim](../../strongs/h/h8064.md), and the ['erets](../../strongs/h/h776.md) was [mālā'](../../strongs/h/h4390.md) of his [tehillah](../../strongs/h/h8416.md).

<a name="habakkuk_3_4"></a>Habakkuk 3:4

And his [nogahh](../../strongs/h/h5051.md) was as the ['owr](../../strongs/h/h216.md); he had [qeren](../../strongs/h/h7161.md) coming out of his [yad](../../strongs/h/h3027.md): and there was the [ḥeḇyôn](../../strongs/h/h2253.md) of his ['oz](../../strongs/h/h5797.md).

<a name="habakkuk_3_5"></a>Habakkuk 3:5

[paniym](../../strongs/h/h6440.md) him [yālaḵ](../../strongs/h/h3212.md) the [deḇer](../../strongs/h/h1698.md), and [rešep̄](../../strongs/h/h7565.md) [yāṣā'](../../strongs/h/h3318.md) at his [regel](../../strongs/h/h7272.md).

<a name="habakkuk_3_6"></a>Habakkuk 3:6

He ['amad](../../strongs/h/h5975.md), and [mûḏ](../../strongs/h/h4128.md) the ['erets](../../strongs/h/h776.md): he [ra'ah](../../strongs/h/h7200.md), and [nāṯar](../../strongs/h/h5425.md) the [gowy](../../strongs/h/h1471.md); and the [ʿaḏ](../../strongs/h/h5703.md) [har](../../strongs/h/h2042.md) were [puwts](../../strongs/h/h6327.md), the ['owlam](../../strongs/h/h5769.md) [giḇʿâ](../../strongs/h/h1389.md) did [shachach](../../strongs/h/h7817.md): his [hălîḵâ](../../strongs/h/h1979.md) are ['owlam](../../strongs/h/h5769.md).

<a name="habakkuk_3_7"></a>Habakkuk 3:7

I [ra'ah](../../strongs/h/h7200.md) the ['ohel](../../strongs/h/h168.md) of [Kûšān](../../strongs/h/h3572.md) in ['aven](../../strongs/h/h205.md): and the [yᵊrîʿâ](../../strongs/h/h3407.md) of the ['erets](../../strongs/h/h776.md) of [Miḏyān](../../strongs/h/h4080.md) did [ragaz](../../strongs/h/h7264.md).

<a name="habakkuk_3_8"></a>Habakkuk 3:8

Was [Yĕhovah](../../strongs/h/h3068.md) [ḥārâ](../../strongs/h/h2734.md) against the [nāhār](../../strongs/h/h5104.md)? was thine ['aph](../../strongs/h/h639.md) against the [nāhār](../../strongs/h/h5104.md)? was thy ['ebrah](../../strongs/h/h5678.md) against the [yam](../../strongs/h/h3220.md), that thou didst [rāḵaḇ](../../strongs/h/h7392.md) upon thine [sûs](../../strongs/h/h5483.md) and thy [merkāḇâ](../../strongs/h/h4818.md) of [yĕshuw'ah](../../strongs/h/h3444.md)?

<a name="habakkuk_3_9"></a>Habakkuk 3:9

Thy [qesheth](../../strongs/h/h7198.md) was made [ʿeryâ](../../strongs/h/h6181.md) [ʿûr](../../strongs/h/h5783.md), according to the [šᵊḇûʿâ](../../strongs/h/h7621.md) of the [maṭṭê](../../strongs/h/h4294.md), even thy ['omer](../../strongs/h/h562.md). [Celah](../../strongs/h/h5542.md). Thou didst [bāqaʿ](../../strongs/h/h1234.md) the ['erets](../../strongs/h/h776.md) with [nāhār](../../strongs/h/h5104.md).

<a name="habakkuk_3_10"></a>Habakkuk 3:10

The [har](../../strongs/h/h2022.md) [ra'ah](../../strongs/h/h7200.md) thee, and they [chuwl](../../strongs/h/h2342.md): the [zerem](../../strongs/h/h2230.md) of the [mayim](../../strongs/h/h4325.md) ['abar](../../strongs/h/h5674.md): the [tĕhowm](../../strongs/h/h8415.md) [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md), and [nasa'](../../strongs/h/h5375.md) his [yad](../../strongs/h/h3027.md) on [rôm](../../strongs/h/h7315.md).

<a name="habakkuk_3_11"></a>Habakkuk 3:11

The [šemeš](../../strongs/h/h8121.md) and [yareach](../../strongs/h/h3394.md) ['amad](../../strongs/h/h5975.md) in their [zᵊḇûl](../../strongs/h/h2073.md): at the ['owr](../../strongs/h/h216.md) of thine [chets](../../strongs/h/h2671.md) they [halak](../../strongs/h/h1980.md), and at the [nogahh](../../strongs/h/h5051.md) of thy [baraq](../../strongs/h/h1300.md) [ḥănîṯ](../../strongs/h/h2595.md).

<a name="habakkuk_3_12"></a>Habakkuk 3:12

Thou didst [ṣāʿaḏ](../../strongs/h/h6805.md) the ['erets](../../strongs/h/h776.md) in [zaʿam](../../strongs/h/h2195.md), thou didst [dûš](../../strongs/h/h1758.md) the [gowy](../../strongs/h/h1471.md) in ['aph](../../strongs/h/h639.md).

<a name="habakkuk_3_13"></a>Habakkuk 3:13

Thou wentest [yāṣā'](../../strongs/h/h3318.md) for the [yesha'](../../strongs/h/h3468.md) of thy ['am](../../strongs/h/h5971.md), even for [yesha'](../../strongs/h/h3468.md) with thine [mashiyach](../../strongs/h/h4899.md); thou [māḥaṣ](../../strongs/h/h4272.md) the [ro'sh](../../strongs/h/h7218.md) out of the [bayith](../../strongs/h/h1004.md) of the [rasha'](../../strongs/h/h7563.md), by [ʿārâ](../../strongs/h/h6168.md) the [yᵊsôḏ](../../strongs/h/h3247.md) unto the [ṣaûā'r](../../strongs/h/h6677.md). [Celah](../../strongs/h/h5542.md).

<a name="habakkuk_3_14"></a>Habakkuk 3:14

Thou didst [nāqaḇ](../../strongs/h/h5344.md) with his [maṭṭê](../../strongs/h/h4294.md) the [ro'sh](../../strongs/h/h7218.md) of his [pārāz](../../strongs/h/h6518.md) : they came out as a [sāʿar](../../strongs/h/h5590.md) to [puwts](../../strongs/h/h6327.md) me: their [ʿălîṣûṯ](../../strongs/h/h5951.md) was as to ['akal](../../strongs/h/h398.md) the ['aniy](../../strongs/h/h6041.md) [mictar](../../strongs/h/h4565.md).

<a name="habakkuk_3_15"></a>Habakkuk 3:15

Thou didst [dāraḵ](../../strongs/h/h1869.md) through the [yam](../../strongs/h/h3220.md) with thine [sûs](../../strongs/h/h5483.md), through the [ḥōmer](../../strongs/h/h2563.md) of [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md).

<a name="habakkuk_3_16"></a>Habakkuk 3:16

When I [shama'](../../strongs/h/h8085.md), my [beten](../../strongs/h/h990.md) [ragaz](../../strongs/h/h7264.md); my [saphah](../../strongs/h/h8193.md) [ṣālal](../../strongs/h/h6750.md) at the [qowl](../../strongs/h/h6963.md): [rāqāḇ](../../strongs/h/h7538.md) [bow'](../../strongs/h/h935.md) into my ['etsem](../../strongs/h/h6106.md), and I [ragaz](../../strongs/h/h7264.md) in myself, that I might [nuwach](../../strongs/h/h5117.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md): when he [ʿālâ](../../strongs/h/h5927.md) unto the ['am](../../strongs/h/h5971.md), he will invade them with his [gûḏ](../../strongs/h/h1464.md).

<a name="habakkuk_3_17"></a>Habakkuk 3:17

Although the [tĕ'en](../../strongs/h/h8384.md) shall not [pāraḥ](../../strongs/h/h6524.md), neither shall [yᵊḇûl](../../strongs/h/h2981.md) be in the [gep̄en](../../strongs/h/h1612.md); the [ma'aseh](../../strongs/h/h4639.md) of the [zayiṯ](../../strongs/h/h2132.md) shall [kāḥaš](../../strongs/h/h3584.md), and the [šᵊḏēmâ](../../strongs/h/h7709.md) shall ['asah](../../strongs/h/h6213.md) no ['ōḵel](../../strongs/h/h400.md); the [tso'n](../../strongs/h/h6629.md) shall be [gāzar](../../strongs/h/h1504.md) from the [miḵlâ](../../strongs/h/h4356.md), and there shall be no [bāqār](../../strongs/h/h1241.md) in the [rep̄eṯ](../../strongs/h/h7517.md) :

<a name="habakkuk_3_18"></a>Habakkuk 3:18

Yet I will [ʿālaz](../../strongs/h/h5937.md) in [Yĕhovah](../../strongs/h/h3068.md), I will [giyl](../../strongs/h/h1523.md) in the ['Elohiym](../../strongs/h/h430.md) of my [yesha'](../../strongs/h/h3468.md).

<a name="habakkuk_3_19"></a>Habakkuk 3:19

The [Yᵊhōvâ](../../strongs/h/h3069.md) ['adonay](../../strongs/h/h136.md) is my [ḥayil](../../strongs/h/h2428.md), and he will [śûm](../../strongs/h/h7760.md) my [regel](../../strongs/h/h7272.md) like ['ayyālâ](../../strongs/h/h355.md) feet, and he will make me to [dāraḵ](../../strongs/h/h1869.md) upon mine [bāmâ](../../strongs/h/h1116.md). To the [nāṣaḥ](../../strongs/h/h5329.md) on my [Nᵊḡînâ](../../strongs/h/h5058.md).

---

[Transliteral Bible](../bible.md)

[Habakkuk](habakkuk.md)

[Habakkuk 2](habakkuk_2.md)