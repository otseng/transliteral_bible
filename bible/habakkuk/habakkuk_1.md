# [Habakkuk 1](https://www.blueletterbible.org/kjv/habakkuk/1)

<a name="habakkuk_1_1"></a>Habakkuk 1:1

The [maśśā'](../../strongs/h/h4853.md) which [Ḥăḇaqqûq](../../strongs/h/h2265.md) the [nāḇî'](../../strongs/h/h5030.md) did [chazah](../../strongs/h/h2372.md).

<a name="habakkuk_1_2"></a>Habakkuk 1:2

[Yĕhovah](../../strongs/h/h3068.md), how long shall I [šāvaʿ](../../strongs/h/h7768.md), and thou wilt not [shama'](../../strongs/h/h8085.md)! even [zāʿaq](../../strongs/h/h2199.md) unto thee of [chamac](../../strongs/h/h2555.md), and thou wilt not [yasha'](../../strongs/h/h3467.md)!

<a name="habakkuk_1_3"></a>Habakkuk 1:3

Why dost thou [ra'ah](../../strongs/h/h7200.md) me ['aven](../../strongs/h/h205.md), and cause me to [nabat](../../strongs/h/h5027.md) ['amal](../../strongs/h/h5999.md)? for [shod](../../strongs/h/h7701.md) and [chamac](../../strongs/h/h2555.md) are before me: and there are that [nasa'](../../strongs/h/h5375.md) [rîḇ](../../strongs/h/h7379.md) and [māḏôn](../../strongs/h/h4066.md).

<a name="habakkuk_1_4"></a>Habakkuk 1:4

Therefore the [towrah](../../strongs/h/h8451.md) is [pûḡ](../../strongs/h/h6313.md), and [mishpat](../../strongs/h/h4941.md) doth [netsach](../../strongs/h/h5331.md) [yāṣā'](../../strongs/h/h3318.md): for the [rasha'](../../strongs/h/h7563.md) doth [kāṯar](../../strongs/h/h3803.md) about the [tsaddiyq](../../strongs/h/h6662.md); therefore [ʿāqal](../../strongs/h/h6127.md) [mishpat](../../strongs/h/h4941.md) [yāṣā'](../../strongs/h/h3318.md).

<a name="habakkuk_1_5"></a>Habakkuk 1:5

[ra'ah](../../strongs/h/h7200.md) ye among the [gowy](../../strongs/h/h1471.md), and [nabat](../../strongs/h/h5027.md), and [tāmah](../../strongs/h/h8539.md) [tāmah](../../strongs/h/h8539.md): for I will [pa'al](../../strongs/h/h6466.md) a [pōʿal](../../strongs/h/h6467.md) in your [yowm](../../strongs/h/h3117.md), which ye will not ['aman](../../strongs/h/h539.md), though it be [sāp̄ar](../../strongs/h/h5608.md) you.

<a name="habakkuk_1_6"></a>Habakkuk 1:6

For, lo, I [quwm](../../strongs/h/h6965.md) the [Kaśdîmâ](../../strongs/h/h3778.md), that [mar](../../strongs/h/h4751.md) and [māhar](../../strongs/h/h4116.md) [gowy](../../strongs/h/h1471.md), which shall [halak](../../strongs/h/h1980.md) through the [merḥāḇ](../../strongs/h/h4800.md) of the ['erets](../../strongs/h/h776.md), to [yarash](../../strongs/h/h3423.md) the [miškān](../../strongs/h/h4908.md) that are not theirs.

<a name="habakkuk_1_7"></a>Habakkuk 1:7

They are ['āyōm](../../strongs/h/h366.md) and [yare'](../../strongs/h/h3372.md): their [mishpat](../../strongs/h/h4941.md) and their [śᵊ'ēṯ](../../strongs/h/h7613.md) shall [yāṣā'](../../strongs/h/h3318.md) of themselves.

<a name="habakkuk_1_8"></a>Habakkuk 1:8

Their [sûs](../../strongs/h/h5483.md) also are [qālal](../../strongs/h/h7043.md) than the [nāmēr](../../strongs/h/h5246.md), and are more [ḥāḏaḏ](../../strongs/h/h2300.md) than the ['ereb](../../strongs/h/h6153.md) [zᵊ'ēḇ](../../strongs/h/h2061.md): and their [pārāš](../../strongs/h/h6571.md) shall [pûš](../../strongs/h/h6335.md) themselves, and their [pārāš](../../strongs/h/h6571.md) shall [bow'](../../strongs/h/h935.md) from [rachowq](../../strongs/h/h7350.md); they shall ['uwph](../../strongs/h/h5774.md) as the [nesheׁr](../../strongs/h/h5404.md) that [ḥûš](../../strongs/h/h2363.md) to ['akal](../../strongs/h/h398.md).

<a name="habakkuk_1_9"></a>Habakkuk 1:9

They shall [bow'](../../strongs/h/h935.md) all for [chamac](../../strongs/h/h2555.md): their [paniym](../../strongs/h/h6440.md) shall [mᵊḡammâ](../../strongs/h/h4041.md) as the [qāḏîm](../../strongs/h/h6921.md), and they shall ['āsap̄](../../strongs/h/h622.md) the [šᵊḇî](../../strongs/h/h7628.md) as the [ḥôl](../../strongs/h/h2344.md).

<a name="habakkuk_1_10"></a>Habakkuk 1:10

And they shall [qālas](../../strongs/h/h7046.md) at the [melek](../../strongs/h/h4428.md), and the [razan](../../strongs/h/h7336.md) shall be a [miśḥāq](../../strongs/h/h4890.md) unto them: they shall [śāḥaq](../../strongs/h/h7832.md) every [miḇṣār](../../strongs/h/h4013.md); for they shall [ṣāḇar](../../strongs/h/h6651.md) ['aphar](../../strongs/h/h6083.md), and [lāḵaḏ](../../strongs/h/h3920.md) it.

<a name="habakkuk_1_11"></a>Habakkuk 1:11

Then shall his [ruwach](../../strongs/h/h7307.md) [ḥālap̄](../../strongs/h/h2498.md), and he shall ['abar](../../strongs/h/h5674.md), and ['asham](../../strongs/h/h816.md), [zû](../../strongs/h/h2098.md) his [koach](../../strongs/h/h3581.md) unto his ['ĕlvôha](../../strongs/h/h433.md).

<a name="habakkuk_1_12"></a>Habakkuk 1:12

Art thou not from [qeḏem](../../strongs/h/h6924.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), mine [qadowsh](../../strongs/h/h6918.md)? we shall not [muwth](../../strongs/h/h4191.md). [Yĕhovah](../../strongs/h/h3068.md), thou hast [śûm](../../strongs/h/h7760.md) them for [mishpat](../../strongs/h/h4941.md); and, O [tsuwr](../../strongs/h/h6697.md), thou hast [yacad](../../strongs/h/h3245.md) them for [yakach](../../strongs/h/h3198.md).

<a name="habakkuk_1_13"></a>Habakkuk 1:13

Thou art of [tahowr](../../strongs/h/h2889.md) ['ayin](../../strongs/h/h5869.md) than to [ra'ah](../../strongs/h/h7200.md) [ra'](../../strongs/h/h7451.md), and [yakol](../../strongs/h/h3201.md) not [nabat](../../strongs/h/h5027.md) on ['amal](../../strongs/h/h5999.md): wherefore [nabat](../../strongs/h/h5027.md) thou upon them that [bāḡaḏ](../../strongs/h/h898.md), and holdest thy [ḥāraš](../../strongs/h/h2790.md) when the [rasha'](../../strongs/h/h7563.md) [bālaʿ](../../strongs/h/h1104.md) the man that is more [tsaddiyq](../../strongs/h/h6662.md) than he?

<a name="habakkuk_1_14"></a>Habakkuk 1:14

And ['asah](../../strongs/h/h6213.md) ['āḏām](../../strongs/h/h120.md) as the [dag](../../strongs/h/h1709.md) of the [yam](../../strongs/h/h3220.md), as the creeping [remeś](../../strongs/h/h7431.md), that have no [mashal](../../strongs/h/h4910.md) over them?

<a name="habakkuk_1_15"></a>Habakkuk 1:15

They [ʿālâ](../../strongs/h/h5927.md) all of them with the [ḥakâ](../../strongs/h/h2443.md), they [gārar](../../strongs/h/h1641.md) them in their [ḥērem](../../strongs/h/h2764.md), and ['āsap̄](../../strongs/h/h622.md) them in their [miḵmōreṯ](../../strongs/h/h4365.md): therefore they [samach](../../strongs/h/h8055.md) and are [giyl](../../strongs/h/h1523.md).

<a name="habakkuk_1_16"></a>Habakkuk 1:16

Therefore they [zabach](../../strongs/h/h2076.md) unto their [ḥērem](../../strongs/h/h2764.md), and [qāṭar](../../strongs/h/h6999.md) unto their [miḵmōreṯ](../../strongs/h/h4365.md); because by them their [cheleq](../../strongs/h/h2506.md) is [šāmēn](../../strongs/h/h8082.md), and their [ma'akal](../../strongs/h/h3978.md) [bārî'](../../strongs/h/h1277.md).

<a name="habakkuk_1_17"></a>Habakkuk 1:17

Shall they therefore [rîq](../../strongs/h/h7324.md) their [ḥērem](../../strongs/h/h2764.md), and not [ḥāmal](../../strongs/h/h2550.md) [tāmîḏ](../../strongs/h/h8548.md) to [harag](../../strongs/h/h2026.md) the [gowy](../../strongs/h/h1471.md)?

---

[Transliteral Bible](../bible.md)

[Habakkuk](habakkuk.md)

[Habakkuk 2](habakkuk_2.md)