# [Habakkuk 2](https://www.blueletterbible.org/kjv/habakkuk/2)

<a name="habakkuk_2_1"></a>Habakkuk 2:1

I will ['amad](../../strongs/h/h5975.md) upon my [mišmereṯ](../../strongs/h/h4931.md), and [yatsab](../../strongs/h/h3320.md) me upon the [māṣôr](../../strongs/h/h4692.md), and will [tsaphah](../../strongs/h/h6822.md) to [ra'ah](../../strongs/h/h7200.md) what he will [dabar](../../strongs/h/h1696.md) unto me, and what I shall [shuwb](../../strongs/h/h7725.md) when I am [tôḵēḥâ](../../strongs/h/h8433.md).

<a name="habakkuk_2_2"></a>Habakkuk 2:2

And [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) me, and ['āmar](../../strongs/h/h559.md), [kāṯaḇ](../../strongs/h/h3789.md) the [ḥāzôn](../../strongs/h/h2377.md), and make it [bā'ar](../../strongs/h/h874.md) upon [lûaḥ](../../strongs/h/h3871.md), that he may [rûṣ](../../strongs/h/h7323.md) that [qara'](../../strongs/h/h7121.md) it.

<a name="habakkuk_2_3"></a>Habakkuk 2:3

For the [ḥāzôn](../../strongs/h/h2377.md) is yet for a [môʿēḏ](../../strongs/h/h4150.md), but at the [qēṣ](../../strongs/h/h7093.md) it shall [puwach](../../strongs/h/h6315.md), and not [kāzaḇ](../../strongs/h/h3576.md): though it [māhah](../../strongs/h/h4102.md), [ḥāḵâ](../../strongs/h/h2442.md) for it; because it will [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md), it will not ['āḥar](../../strongs/h/h309.md).

<a name="habakkuk_2_4"></a>Habakkuk 2:4

Behold, his [nephesh](../../strongs/h/h5315.md) which is [ʿāp̄al](../../strongs/h/h6075.md) is not [yashar](../../strongs/h/h3474.md) in him: but the [tsaddiyq](../../strongs/h/h6662.md) shall [ḥāyâ](../../strongs/h/h2421.md) by his ['ĕmûnâ](../../strongs/h/h530.md).

<a name="habakkuk_2_5"></a>Habakkuk 2:5

Yea also, because he [bāḡaḏ](../../strongs/h/h898.md) by [yayin](../../strongs/h/h3196.md), he is a [yāhîr](../../strongs/h/h3093.md) [geḇer](../../strongs/h/h1397.md), neither [nāvâ](../../strongs/h/h5115.md), who [rāḥaḇ](../../strongs/h/h7337.md) his [nephesh](../../strongs/h/h5315.md) as [shĕ'owl](../../strongs/h/h7585.md), and is as [maveth](../../strongs/h/h4194.md), and cannot be [sāׂbaʿ](../../strongs/h/h7646.md), but ['āsap̄](../../strongs/h/h622.md) unto him all [gowy](../../strongs/h/h1471.md), and [qāḇaṣ](../../strongs/h/h6908.md) unto him all ['am](../../strongs/h/h5971.md):

<a name="habakkuk_2_6"></a>Habakkuk 2:6

Shall not all these [nasa'](../../strongs/h/h5375.md) a [māšāl](../../strongs/h/h4912.md) against him, and a [mᵊlîṣâ](../../strongs/h/h4426.md) [ḥîḏâ](../../strongs/h/h2420.md) against him, and ['āmar](../../strongs/h/h559.md), [hôy](../../strongs/h/h1945.md) to him that [rabah](../../strongs/h/h7235.md) that which is not his! how long? and to him that [kabad](../../strongs/h/h3513.md) himself with [ʿaḇṭîṭ](../../strongs/h/h5671.md)!

<a name="habakkuk_2_7"></a>Habakkuk 2:7

Shall they not [quwm](../../strongs/h/h6965.md) [peṯaʿ](../../strongs/h/h6621.md) that shall [nāšaḵ](../../strongs/h/h5391.md) thee, and [yāqaṣ](../../strongs/h/h3364.md) that shall [zûaʿ](../../strongs/h/h2111.md) thee, and thou shalt be for [mᵊšissâ](../../strongs/h/h4933.md) unto them?

<a name="habakkuk_2_8"></a>Habakkuk 2:8

Because thou hast [šālal](../../strongs/h/h7997.md) [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md), all the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) shall [šālal](../../strongs/h/h7997.md) thee; because of ['āḏām](../../strongs/h/h120.md) [dam](../../strongs/h/h1818.md), and for the [chamac](../../strongs/h/h2555.md) of the ['erets](../../strongs/h/h776.md), of the [qiryâ](../../strongs/h/h7151.md), and of all that [yashab](../../strongs/h/h3427.md) therein.

<a name="habakkuk_2_9"></a>Habakkuk 2:9

[hôy](../../strongs/h/h1945.md) to him that [batsa'](../../strongs/h/h1214.md) a [ra'](../../strongs/h/h7451.md) [beṣaʿ](../../strongs/h/h1215.md) to his [bayith](../../strongs/h/h1004.md), that he may [śûm](../../strongs/h/h7760.md) his [qēn](../../strongs/h/h7064.md) on [marowm](../../strongs/h/h4791.md), that he may be [natsal](../../strongs/h/h5337.md) from the [kaph](../../strongs/h/h3709.md) of [ra'](../../strongs/h/h7451.md)!

<a name="habakkuk_2_10"></a>Habakkuk 2:10

Thou hast [ya'ats](../../strongs/h/h3289.md) [bšeṯ](../../strongs/h/h1322.md) to thy [bayith](../../strongs/h/h1004.md) by [qāṣâ](../../strongs/h/h7096.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md), and hast [chata'](../../strongs/h/h2398.md) against thy [nephesh](../../strongs/h/h5315.md).

<a name="habakkuk_2_11"></a>Habakkuk 2:11

For the ['eben](../../strongs/h/h68.md) shall [zāʿaq](../../strongs/h/h2199.md) of the [qîr](../../strongs/h/h7023.md), and the [kāp̄îs](../../strongs/h/h3714.md) of the ['ets](../../strongs/h/h6086.md) shall ['anah](../../strongs/h/h6030.md) it.

<a name="habakkuk_2_12"></a>Habakkuk 2:12

[hôy](../../strongs/h/h1945.md) to him that [bānâ](../../strongs/h/h1129.md) a [ʿîr](../../strongs/h/h5892.md) with [dam](../../strongs/h/h1818.md), and [kuwn](../../strongs/h/h3559.md) a [qiryâ](../../strongs/h/h7151.md) by ['evel](../../strongs/h/h5766.md)!

<a name="habakkuk_2_13"></a>Habakkuk 2:13

Behold, is it not of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) that the ['am](../../strongs/h/h5971.md) shall [yaga'](../../strongs/h/h3021.md) in the [day](../../strongs/h/h1767.md) ['esh](../../strongs/h/h784.md), and the [lĕom](../../strongs/h/h3816.md) shall [yāʿap̄](../../strongs/h/h3286.md) themselves for [day](../../strongs/h/h1767.md) [riyq](../../strongs/h/h7385.md)?

<a name="habakkuk_2_14"></a>Habakkuk 2:14

For the ['erets](../../strongs/h/h776.md) shall be [mālā'](../../strongs/h/h4390.md) with the [yada'](../../strongs/h/h3045.md) of the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md), as the [mayim](../../strongs/h/h4325.md) [kāsâ](../../strongs/h/h3680.md) the [yam](../../strongs/h/h3220.md).

<a name="habakkuk_2_15"></a>Habakkuk 2:15

[hôy](../../strongs/h/h1945.md) unto him that giveth his [rea'](../../strongs/h/h7453.md) [šāqâ](../../strongs/h/h8248.md), that [sāp̄aḥ](../../strongs/h/h5596.md) thy [ḥēmeṯ](../../strongs/h/h2573.md) to him, and makest him [šāḵar](../../strongs/h/h7937.md) also, that thou mayest [nabat](../../strongs/h/h5027.md) on their [māʿôr](../../strongs/h/h4589.md)!

<a name="habakkuk_2_16"></a>Habakkuk 2:16

Thou art [sāׂbaʿ](../../strongs/h/h7646.md) with [qālôn](../../strongs/h/h7036.md) for [kabowd](../../strongs/h/h3519.md): [šāṯâ](../../strongs/h/h8354.md) thou also, and let thy [ʿārēl](../../strongs/h/h6188.md): the [kowc](../../strongs/h/h3563.md) of [Yĕhovah](../../strongs/h/h3068.md) [yamiyn](../../strongs/h/h3225.md) shall be [cabab](../../strongs/h/h5437.md) unto thee, and [qîqālôn](../../strongs/h/h7022.md) shall be on thy [kabowd](../../strongs/h/h3519.md).

<a name="habakkuk_2_17"></a>Habakkuk 2:17

For the [chamac](../../strongs/h/h2555.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) shall [kāsâ](../../strongs/h/h3680.md) thee, and the [shod](../../strongs/h/h7701.md) of [bĕhemah](../../strongs/h/h929.md), which made them [ḥāṯaṯ](../../strongs/h/h2865.md), because of ['āḏām](../../strongs/h/h120.md) [dam](../../strongs/h/h1818.md), and for the [chamac](../../strongs/h/h2555.md) of the ['erets](../../strongs/h/h776.md), of the [qiryâ](../../strongs/h/h7151.md), and of all that [yashab](../../strongs/h/h3427.md) therein.

<a name="habakkuk_2_18"></a>Habakkuk 2:18

What [yāʿal](../../strongs/h/h3276.md) the [pecel](../../strongs/h/h6459.md) that the [yāṣar](../../strongs/h/h3335.md) thereof hath [pāsal](../../strongs/h/h6458.md) it; the [massēḵâ](../../strongs/h/h4541.md), and a [yārâ](../../strongs/h/h3384.md) of [sheqer](../../strongs/h/h8267.md), that the [yāṣar](../../strongs/h/h3335.md) of his [yetser](../../strongs/h/h3336.md) [batach](../../strongs/h/h982.md) therein, to ['asah](../../strongs/h/h6213.md) ['illēm](../../strongs/h/h483.md) ['ĕlîl](../../strongs/h/h457.md)?

<a name="habakkuk_2_19"></a>Habakkuk 2:19

[hôy](../../strongs/h/h1945.md) unto him that ['āmar](../../strongs/h/h559.md) to the ['ets](../../strongs/h/h6086.md), [quwts](../../strongs/h/h6974.md); to the [dûmām](../../strongs/h/h1748.md) ['eben](../../strongs/h/h68.md), [ʿûr](../../strongs/h/h5782.md), it shall [yārâ](../../strongs/h/h3384.md)! Behold, it is [tāp̄aś](../../strongs/h/h8610.md) over with [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md), and there is no [ruwach](../../strongs/h/h7307.md) at all in the [qereḇ](../../strongs/h/h7130.md) of it.

<a name="habakkuk_2_20"></a>Habakkuk 2:20

But [Yĕhovah](../../strongs/h/h3068.md) is in his [qodesh](../../strongs/h/h6944.md) [heykal](../../strongs/h/h1964.md): let all the ['erets](../../strongs/h/h776.md) [hāsâ](../../strongs/h/h2013.md) [paniym](../../strongs/h/h6440.md) him.

---

[Transliteral Bible](../bible.md)

[Habakkuk](habakkuk.md)

[Habakkuk 1](habakkuk_1.md) - [Habakkuk 3](habakkuk_3.md)