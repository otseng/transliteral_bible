# [Amos 7](https://www.blueletterbible.org/kjv/amos/7)

<a name="amos_7_1"></a>Amos 7:1

Thus hath the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [ra'ah](../../strongs/h/h7200.md) unto me; and, behold, he [yāṣar](../../strongs/h/h3335.md) [gôḇ](../../strongs/h/h1462.md) in the [tᵊḥillâ](../../strongs/h/h8462.md) of the [ʿālâ](../../strongs/h/h5927.md) of the [leqeš](../../strongs/h/h3954.md); and, lo, it was the [leqeš](../../strongs/h/h3954.md) ['aḥar](../../strongs/h/h310.md) the [melek](../../strongs/h/h4428.md) [gēz](../../strongs/h/h1488.md).

<a name="amos_7_2"></a>Amos 7:2

And it came to pass, that when they had made a [kalah](../../strongs/h/h3615.md) of ['akal](../../strongs/h/h398.md) the ['eseb](../../strongs/h/h6212.md) of the ['erets](../../strongs/h/h776.md), then I ['āmar](../../strongs/h/h559.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [sālaḥ](../../strongs/h/h5545.md), I beseech thee: by whom shall [Ya'aqob](../../strongs/h/h3290.md) [quwm](../../strongs/h/h6965.md)? for he is [qāṭān](../../strongs/h/h6996.md).

<a name="amos_7_3"></a>Amos 7:3

[Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) for this: It shall not be, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_7_4"></a>Amos 7:4

Thus hath the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [ra'ah](../../strongs/h/h7200.md) unto me: and, behold, the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [qara'](../../strongs/h/h7121.md) to [riyb](../../strongs/h/h7378.md) by ['esh](../../strongs/h/h784.md), and it ['akal](../../strongs/h/h398.md) the [rab](../../strongs/h/h7227.md) [tĕhowm](../../strongs/h/h8415.md), and did eat ['akal](../../strongs/h/h398.md) a [cheleq](../../strongs/h/h2506.md).

<a name="amos_7_5"></a>Amos 7:5

Then ['āmar](../../strongs/h/h559.md) I, ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [ḥāḏal](../../strongs/h/h2308.md), I beseech thee: by whom shall [Ya'aqob](../../strongs/h/h3290.md) [quwm](../../strongs/h/h6965.md)? for he is [qāṭān](../../strongs/h/h6996.md).

<a name="amos_7_6"></a>Amos 7:6

[Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) for this: This also shall not be, ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="amos_7_7"></a>Amos 7:7

Thus he [ra'ah](../../strongs/h/h7200.md) me: and, behold, the ['adonay](../../strongs/h/h136.md) [nāṣaḇ](../../strongs/h/h5324.md) upon a [ḥômâ](../../strongs/h/h2346.md) made by a ['ănāḵ](../../strongs/h/h594.md), with a ['ănāḵ](../../strongs/h/h594.md) in his [yad](../../strongs/h/h3027.md).

<a name="amos_7_8"></a>Amos 7:8

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [ʿĀmôs](../../strongs/h/h5986.md), what [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), A ['ănāḵ](../../strongs/h/h594.md). Then ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md), Behold, I will [śûm](../../strongs/h/h7760.md) a ['ănāḵ](../../strongs/h/h594.md) in the [qereḇ](../../strongs/h/h7130.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md): I will not again ['abar](../../strongs/h/h5674.md) them any more:

<a name="amos_7_9"></a>Amos 7:9

And the [bāmâ](../../strongs/h/h1116.md) of [Yišḥāq](../../strongs/h/h3446.md) shall be [šāmēm](../../strongs/h/h8074.md), and the [miqdash](../../strongs/h/h4720.md) of [Yisra'el](../../strongs/h/h3478.md) shall be [ḥāraḇ](../../strongs/h/h2717.md); and I will [quwm](../../strongs/h/h6965.md) against the [bayith](../../strongs/h/h1004.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) with the [chereb](../../strongs/h/h2719.md).

<a name="amos_7_10"></a>Amos 7:10

Then ['Ămaṣyâ](../../strongs/h/h558.md) the [kōhēn](../../strongs/h/h3548.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md) [shalach](../../strongs/h/h7971.md) to [YārāḇʿĀm](../../strongs/h/h3379.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [ʿĀmôs](../../strongs/h/h5986.md) hath [qāšar](../../strongs/h/h7194.md) against thee in the [qereḇ](../../strongs/h/h7130.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md): the ['erets](../../strongs/h/h776.md) is not [yakol](../../strongs/h/h3201.md) to [kûl](../../strongs/h/h3557.md) all his [dabar](../../strongs/h/h1697.md).

<a name="amos_7_11"></a>Amos 7:11

For thus [ʿĀmôs](../../strongs/h/h5986.md) ['āmar](../../strongs/h/h559.md), [YārāḇʿĀm](../../strongs/h/h3379.md) shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), and [Yisra'el](../../strongs/h/h3478.md) shall [gālâ](../../strongs/h/h1540.md) [gālâ](../../strongs/h/h1540.md) out of their own ['ăḏāmâ](../../strongs/h/h127.md).

<a name="amos_7_12"></a>Amos 7:12

Also ['Ămaṣyâ](../../strongs/h/h558.md) ['āmar](../../strongs/h/h559.md) unto [ʿĀmôs](../../strongs/h/h5986.md), O thou [ḥōzê](../../strongs/h/h2374.md), [yālaḵ](../../strongs/h/h3212.md), flee thee [bāraḥ](../../strongs/h/h1272.md) into the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and there ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md), and [nāḇā'](../../strongs/h/h5012.md) there:

<a name="amos_7_13"></a>Amos 7:13

But [nāḇā'](../../strongs/h/h5012.md) not again any more at [Bêṯ-'ēl](../../strongs/h/h1008.md): for it is the [melek](../../strongs/h/h4428.md) [miqdash](../../strongs/h/h4720.md), and it is the [mamlāḵâ](../../strongs/h/h4467.md) [bayith](../../strongs/h/h1004.md).

<a name="amos_7_14"></a>Amos 7:14

Then ['anah](../../strongs/h/h6030.md) [ʿĀmôs](../../strongs/h/h5986.md), and ['āmar](../../strongs/h/h559.md) to ['Ămaṣyâ](../../strongs/h/h558.md), I was no [nāḇî'](../../strongs/h/h5030.md), neither was I a [nāḇî'](../../strongs/h/h5030.md) [ben](../../strongs/h/h1121.md); but I was a [bôqēr](../../strongs/h/h951.md), and a [bālas](../../strongs/h/h1103.md) of [šiqmâ](../../strongs/h/h8256.md):

<a name="amos_7_15"></a>Amos 7:15

And [Yĕhovah](../../strongs/h/h3068.md) [laqach](../../strongs/h/h3947.md) me as I ['aḥar](../../strongs/h/h310.md) the [tso'n](../../strongs/h/h6629.md), and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [yālaḵ](../../strongs/h/h3212.md), [nāḇā'](../../strongs/h/h5012.md) unto my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="amos_7_16"></a>Amos 7:16

Now therefore [shama'](../../strongs/h/h8085.md) thou the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md): Thou ['āmar](../../strongs/h/h559.md), [nāḇā'](../../strongs/h/h5012.md) not against [Yisra'el](../../strongs/h/h3478.md), and [nāṭap̄](../../strongs/h/h5197.md) not thy word against the [bayith](../../strongs/h/h1004.md) of [Yišḥāq](../../strongs/h/h3446.md).

<a name="amos_7_17"></a>Amos 7:17

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Thy ['ishshah](../../strongs/h/h802.md) shall be a [zānâ](../../strongs/h/h2181.md) in the [ʿîr](../../strongs/h/h5892.md), and thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md) shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), and thy ['ăḏāmâ](../../strongs/h/h127.md) shall be [chalaq](../../strongs/h/h2505.md) by [chebel](../../strongs/h/h2256.md); and thou shalt [muwth](../../strongs/h/h4191.md) in a [tame'](../../strongs/h/h2931.md) ['ăḏāmâ](../../strongs/h/h127.md): and [Yisra'el](../../strongs/h/h3478.md) shall [gālâ](../../strongs/h/h1540.md) go into [gālâ](../../strongs/h/h1540.md) forth of his ['ăḏāmâ](../../strongs/h/h127.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 6](amos_6.md) - [Amos 8](amos_8.md)