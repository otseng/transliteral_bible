# [Amos 6](https://www.blueletterbible.org/kjv/amos/6)

<a name="amos_6_1"></a>Amos 6:1

[hôy](../../strongs/h/h1945.md) to them that are at [ša'ănān](../../strongs/h/h7600.md) in [Tsiyown](../../strongs/h/h6726.md), and [batach](../../strongs/h/h982.md) in the [har](../../strongs/h/h2022.md) of [Šōmrôn](../../strongs/h/h8111.md), which are [nāqaḇ](../../strongs/h/h5344.md) [re'shiyth](../../strongs/h/h7225.md) of the [gowy](../../strongs/h/h1471.md), to whom the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md)!

<a name="amos_6_2"></a>Amos 6:2

['abar](../../strongs/h/h5674.md) ye unto [kalnê](../../strongs/h/h3641.md), and [ra'ah](../../strongs/h/h7200.md); and from thence [yālaḵ](../../strongs/h/h3212.md) ye to [Ḥămāṯ](../../strongs/h/h2574.md) the [rab](../../strongs/h/h7227.md) [Ḥămaṯ Rabâ](../../strongs/h/h2579.md) : then [yarad](../../strongs/h/h3381.md) to [Gaṯ](../../strongs/h/h1661.md) of the [Pᵊlištî](../../strongs/h/h6430.md): be they [towb](../../strongs/h/h2896.md) than these [mamlāḵâ](../../strongs/h/h4467.md)? or their [gᵊḇûl](../../strongs/h/h1366.md) [rab](../../strongs/h/h7227.md) than your [gᵊḇûl](../../strongs/h/h1366.md)?

<a name="amos_6_3"></a>Amos 6:3

Ye that put far [nāḏâ](../../strongs/h/h5077.md) the [ra'](../../strongs/h/h7451.md) [yowm](../../strongs/h/h3117.md), and cause the [šeḇeṯ](../../strongs/h/h7675.md) of [chamac](../../strongs/h/h2555.md) to come [nāḡaš](../../strongs/h/h5066.md);

<a name="amos_6_4"></a>Amos 6:4

That [shakab](../../strongs/h/h7901.md) upon [mittah](../../strongs/h/h4296.md) of [šēn](../../strongs/h/h8127.md), and [sāraḥ](../../strongs/h/h5628.md) themselves upon their ['eres](../../strongs/h/h6210.md), and ['akal](../../strongs/h/h398.md) the [kar](../../strongs/h/h3733.md) out of the [tso'n](../../strongs/h/h6629.md), and the [ʿēḡel](../../strongs/h/h5695.md) out of the [tavek](../../strongs/h/h8432.md) of the [marbēq](../../strongs/h/h4770.md);

<a name="amos_6_5"></a>Amos 6:5

That [pāraṭ](../../strongs/h/h6527.md) to the [peh](../../strongs/h/h6310.md) of the [neḇel](../../strongs/h/h5035.md), and [chashab](../../strongs/h/h2803.md) to themselves [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md), like [Dāviḏ](../../strongs/h/h1732.md);

<a name="amos_6_6"></a>Amos 6:6

That [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) in [mizrāq](../../strongs/h/h4219.md), and [māšaḥ](../../strongs/h/h4886.md) themselves with the [re'shiyth](../../strongs/h/h7225.md) [šemen](../../strongs/h/h8081.md): but they are not [ḥālâ](../../strongs/h/h2470.md) for the [šeḇar](../../strongs/h/h7667.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="amos_6_7"></a>Amos 6:7

Therefore now shall they go [gālâ](../../strongs/h/h1540.md) with the [ro'sh](../../strongs/h/h7218.md) that go [gālâ](../../strongs/h/h1540.md), and the [mirzaḥ](../../strongs/h/h4797.md) of them that [sāraḥ](../../strongs/h/h5628.md) themselves shall be [cuwr](../../strongs/h/h5493.md).

<a name="amos_6_8"></a>Amos 6:8

The ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) hath [shaba'](../../strongs/h/h7650.md) by [nephesh](../../strongs/h/h5315.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), I [tā'aḇ](../../strongs/h/h8374.md) the [gā'ôn](../../strongs/h/h1347.md) of [Ya'aqob](../../strongs/h/h3290.md), and [sane'](../../strongs/h/h8130.md) his ['armôn](../../strongs/h/h759.md): therefore will I deliver [cagar](../../strongs/h/h5462.md) the [ʿîr](../../strongs/h/h5892.md) with all that is [mᵊlō'](../../strongs/h/h4393.md).

<a name="amos_6_9"></a>Amos 6:9

And it shall come to pass, if there [yāṯar](../../strongs/h/h3498.md) ten ['enowsh](../../strongs/h/h582.md) in one [bayith](../../strongs/h/h1004.md), that they shall [muwth](../../strongs/h/h4191.md).

<a name="amos_6_10"></a>Amos 6:10

And a man's [dôḏ](../../strongs/h/h1730.md) shall take him [nasa'](../../strongs/h/h5375.md), and he that [sārap̄](../../strongs/h/h5635.md) him, to [yāṣā'](../../strongs/h/h3318.md) the ['etsem](../../strongs/h/h6106.md) out of the [bayith](../../strongs/h/h1004.md), and shall ['āmar](../../strongs/h/h559.md) unto him that is by the [yᵊrēḵâ](../../strongs/h/h3411.md) of the [bayith](../../strongs/h/h1004.md), Is there yet any with thee? and he shall ['āmar](../../strongs/h/h559.md), ['ep̄es](../../strongs/h/h657.md). Then shall he ['āmar](../../strongs/h/h559.md), Hold thy [hāsâ](../../strongs/h/h2013.md): for we may not make [zakar](../../strongs/h/h2142.md) of the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_6_11"></a>Amos 6:11

For, behold, [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), and he will [nakah](../../strongs/h/h5221.md) the [gadowl](../../strongs/h/h1419.md) [bayith](../../strongs/h/h1004.md) with [rāsîs](../../strongs/h/h7447.md), and the [qāṭān](../../strongs/h/h6996.md) [bayith](../../strongs/h/h1004.md) with [bᵊqîaʿ](../../strongs/h/h1233.md).

<a name="amos_6_12"></a>Amos 6:12

Shall [sûs](../../strongs/h/h5483.md) [rûṣ](../../strongs/h/h7323.md) upon the [cela'](../../strongs/h/h5553.md)? will one [ḥāraš](../../strongs/h/h2790.md) there with [bāqār](../../strongs/h/h1241.md)? for ye have [hāp̄aḵ](../../strongs/h/h2015.md) [mishpat](../../strongs/h/h4941.md) into [rō'š](../../strongs/h/h7219.md), and the [pĕriy](../../strongs/h/h6529.md) of [tsedaqah](../../strongs/h/h6666.md) into [laʿănâ](../../strongs/h/h3939.md):

<a name="amos_6_13"></a>Amos 6:13

Ye which [śāmēaḥ](../../strongs/h/h8056.md) in a [dabar](../../strongs/h/h1697.md) of nought, which ['āmar](../../strongs/h/h559.md), Have we not [laqach](../../strongs/h/h3947.md) to us [qeren](../../strongs/h/h7161.md) by our own [ḥōzeq](../../strongs/h/h2392.md)?

<a name="amos_6_14"></a>Amos 6:14

But, behold, I will [quwm](../../strongs/h/h6965.md) against you a [gowy](../../strongs/h/h1471.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md); and they shall [lāḥaṣ](../../strongs/h/h3905.md) you from the entering [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md) unto the [nachal](../../strongs/h/h5158.md) of the ['arabah](../../strongs/h/h6160.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 5](amos_5.md) - [Amos 7](amos_7.md)