# [Amos 3](https://www.blueletterbible.org/kjv/amos/3)

<a name="amos_3_1"></a>Amos 3:1

[shama'](../../strongs/h/h8085.md) this [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) against you, O [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), against the [mišpāḥâ](../../strongs/h/h4940.md) which I [ʿālâ](../../strongs/h/h5927.md) from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), ['āmar](../../strongs/h/h559.md),

<a name="amos_3_2"></a>Amos 3:2

You only have I [yada'](../../strongs/h/h3045.md) of all the [mišpāḥâ](../../strongs/h/h4940.md) of the ['ăḏāmâ](../../strongs/h/h127.md): therefore I will [paqad](../../strongs/h/h6485.md) you for all your ['avon](../../strongs/h/h5771.md).

<a name="amos_3_3"></a>Amos 3:3

Can two [yālaḵ](../../strongs/h/h3212.md) [yaḥaḏ](../../strongs/h/h3162.md), except they be [yāʿaḏ](../../strongs/h/h3259.md)?

<a name="amos_3_4"></a>Amos 3:4

Will an ['ariy](../../strongs/h/h738.md) [šā'aḡ](../../strongs/h/h7580.md) in the [yaʿar](../../strongs/h/h3293.md), when he hath no [ṭerep̄](../../strongs/h/h2964.md)? will a [kephiyr](../../strongs/h/h3715.md) [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) of his [mᵊʿônâ](../../strongs/h/h4585.md), if he have [lāḵaḏ](../../strongs/h/h3920.md) nothing?

<a name="amos_3_5"></a>Amos 3:5

Can a [tsippowr](../../strongs/h/h6833.md) [naphal](../../strongs/h/h5307.md) in a [paḥ](../../strongs/h/h6341.md) upon the ['erets](../../strongs/h/h776.md), where no [mowqesh](../../strongs/h/h4170.md) is for him? shall one [ʿālâ](../../strongs/h/h5927.md) a [paḥ](../../strongs/h/h6341.md) from the ['ăḏāmâ](../../strongs/h/h127.md), and have [lāḵaḏ](../../strongs/h/h3920.md) nothing at [lāḵaḏ](../../strongs/h/h3920.md)?

<a name="amos_3_6"></a>Amos 3:6

Shall a [šôp̄ār](../../strongs/h/h7782.md) be [tāqaʿ](../../strongs/h/h8628.md) in the [ʿîr](../../strongs/h/h5892.md), and the ['am](../../strongs/h/h5971.md) not be [ḥārēḏ](../../strongs/h/h2729.md)? shall there be [ra'](../../strongs/h/h7451.md) in a [ʿîr](../../strongs/h/h5892.md), and [Yĕhovah](../../strongs/h/h3068.md) hath not ['asah](../../strongs/h/h6213.md) it?

<a name="amos_3_7"></a>Amos 3:7

Surely the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) will ['asah](../../strongs/h/h6213.md) [dabar](../../strongs/h/h1697.md), but he [gālâ](../../strongs/h/h1540.md) his [sôḏ](../../strongs/h/h5475.md) unto his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="amos_3_8"></a>Amos 3:8

The ['ariy](../../strongs/h/h738.md) hath [šā'aḡ](../../strongs/h/h7580.md), who will not [yare'](../../strongs/h/h3372.md)? the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) hath [dabar](../../strongs/h/h1696.md), who can but [nāḇā'](../../strongs/h/h5012.md)?

<a name="amos_3_9"></a>Amos 3:9

[shama'](../../strongs/h/h8085.md) in the ['armôn](../../strongs/h/h759.md) at ['Ašdôḏ](../../strongs/h/h795.md), and in the ['armôn](../../strongs/h/h759.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and ['āmar](../../strongs/h/h559.md), ['āsap̄](../../strongs/h/h622.md) yourselves upon the [har](../../strongs/h/h2022.md) of [Šōmrôn](../../strongs/h/h8111.md), and [ra'ah](../../strongs/h/h7200.md) the [rab](../../strongs/h/h7227.md) [mᵊhûmâ](../../strongs/h/h4103.md) in the [qereḇ](../../strongs/h/h7130.md) thereof, and the [ʿăšûqîm](../../strongs/h/h6217.md) in the [tavek](../../strongs/h/h8432.md) thereof.

<a name="amos_3_10"></a>Amos 3:10

For they [yada'](../../strongs/h/h3045.md) not to ['asah](../../strongs/h/h6213.md) [nᵊḵōḥâ](../../strongs/h/h5229.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), who ['āṣar](../../strongs/h/h686.md) up [chamac](../../strongs/h/h2555.md) and [shod](../../strongs/h/h7701.md) in their ['armôn](../../strongs/h/h759.md).

<a name="amos_3_11"></a>Amos 3:11

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); A [tsar](../../strongs/h/h6862.md) there shall be even [cabiyb](../../strongs/h/h5439.md) the ['erets](../../strongs/h/h776.md); and he shall bring [yarad](../../strongs/h/h3381.md) thy ['oz](../../strongs/h/h5797.md) from thee, and thy ['armôn](../../strongs/h/h759.md) shall be [bāzaz](../../strongs/h/h962.md).

<a name="amos_3_12"></a>Amos 3:12

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); As the [ra'ah](../../strongs/h/h7462.md) taketh [natsal](../../strongs/h/h5337.md) of the [peh](../../strongs/h/h6310.md) of the ['ariy](../../strongs/h/h738.md) two [keraʿ](../../strongs/h/h3767.md), or a [bāḏāl](../../strongs/h/h915.md) of an ['ozen](../../strongs/h/h241.md); so shall the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) be taken [natsal](../../strongs/h/h5337.md) that [yashab](../../strongs/h/h3427.md) in [Šōmrôn](../../strongs/h/h8111.md) in the [pē'â](../../strongs/h/h6285.md) of a [mittah](../../strongs/h/h4296.md), and in [dammeśeq](../../strongs/h/h1833.md) in an ['eres](../../strongs/h/h6210.md).

<a name="amos_3_13"></a>Amos 3:13

[shama'](../../strongs/h/h8085.md) ye, and [ʿûḏ](../../strongs/h/h5749.md) in the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md),

<a name="amos_3_14"></a>Amos 3:14

That in the [yowm](../../strongs/h/h3117.md) that I shall [paqad](../../strongs/h/h6485.md) the [pesha'](../../strongs/h/h6588.md) of [Yisra'el](../../strongs/h/h3478.md) upon him I will also [paqad](../../strongs/h/h6485.md) the [mizbeach](../../strongs/h/h4196.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md): and the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md) shall be [gāḏaʿ](../../strongs/h/h1438.md), and [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md).

<a name="amos_3_15"></a>Amos 3:15

And I will [nakah](../../strongs/h/h5221.md) the [ḥōrep̄](../../strongs/h/h2779.md) [bayith](../../strongs/h/h1004.md) with the [qayiṣ](../../strongs/h/h7019.md) [bayith](../../strongs/h/h1004.md); and the [bayith](../../strongs/h/h1004.md) of [šēn](../../strongs/h/h8127.md) shall ['abad](../../strongs/h/h6.md), and the [rab](../../strongs/h/h7227.md) [bayith](../../strongs/h/h1004.md) shall have a [sûp̄](../../strongs/h/h5486.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 2](amos_2.md) - [Amos 4](amos_4.md)