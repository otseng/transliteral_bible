# [Amos 5](https://www.blueletterbible.org/kjv/amos/5)

<a name="amos_5_1"></a>Amos 5:1

[shama'](../../strongs/h/h8085.md) ye this [dabar](../../strongs/h/h1697.md) which I [nasa'](../../strongs/h/h5375.md) against you, even a [qînâ](../../strongs/h/h7015.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="amos_5_2"></a>Amos 5:2

The [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yisra'el](../../strongs/h/h3478.md) is [naphal](../../strongs/h/h5307.md); she shall no more [quwm](../../strongs/h/h6965.md): she is [nāṭaš](../../strongs/h/h5203.md) upon her ['ăḏāmâ](../../strongs/h/h127.md); there is none to [quwm](../../strongs/h/h6965.md) her.

<a name="amos_5_3"></a>Amos 5:3

For thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); The [ʿîr](../../strongs/h/h5892.md) that [yāṣā'](../../strongs/h/h3318.md) by a thousand shall [šā'ar](../../strongs/h/h7604.md) an hundred, and that which [yāṣā'](../../strongs/h/h3318.md) by an hundred shall [šā'ar](../../strongs/h/h7604.md) ten, to the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="amos_5_4"></a>Amos 5:4

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [darash](../../strongs/h/h1875.md) ye me, and ye shall [ḥāyâ](../../strongs/h/h2421.md):

<a name="amos_5_5"></a>Amos 5:5

But [darash](../../strongs/h/h1875.md) not [Bêṯ-'ēl](../../strongs/h/h1008.md), nor [bow'](../../strongs/h/h935.md) into [Gilgāl](../../strongs/h/h1537.md), and ['abar](../../strongs/h/h5674.md) not to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md): for [Gilgāl](../../strongs/h/h1537.md) shall [gālâ](../../strongs/h/h1540.md) go into [gālâ](../../strongs/h/h1540.md), and [Bêṯ-'ēl](../../strongs/h/h1008.md) shall come to ['aven](../../strongs/h/h205.md).

<a name="amos_5_6"></a>Amos 5:6

[darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), and ye shall [ḥāyâ](../../strongs/h/h2421.md); lest he [tsalach](../../strongs/h/h6743.md) like ['esh](../../strongs/h/h784.md) in the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md), and ['akal](../../strongs/h/h398.md) it, and there be none to [kāḇâ](../../strongs/h/h3518.md) it in [Bêṯ-'ēl](../../strongs/h/h1008.md).

<a name="amos_5_7"></a>Amos 5:7

Ye who [hāp̄aḵ](../../strongs/h/h2015.md) [mishpat](../../strongs/h/h4941.md) to [laʿănâ](../../strongs/h/h3939.md), and [yānaḥ](../../strongs/h/h3240.md) [tsedaqah](../../strongs/h/h6666.md) in the ['erets](../../strongs/h/h776.md),

<a name="amos_5_8"></a>Amos 5:8

['asah](../../strongs/h/h6213.md) the seven [kîmâ](../../strongs/h/h3598.md) and [kᵊsîl](../../strongs/h/h3685.md), and [hāp̄aḵ](../../strongs/h/h2015.md) the [ṣalmāveṯ](../../strongs/h/h6757.md) into the [boqer](../../strongs/h/h1242.md), and maketh the [yowm](../../strongs/h/h3117.md) [ḥāšaḵ](../../strongs/h/h2821.md) with [layil](../../strongs/h/h3915.md): that [qara'](../../strongs/h/h7121.md) for the [mayim](../../strongs/h/h4325.md) of the [yam](../../strongs/h/h3220.md), and [šāp̄aḵ](../../strongs/h/h8210.md) them upon the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md): [Yĕhovah](../../strongs/h/h3068.md) is his [shem](../../strongs/h/h8034.md):

<a name="amos_5_9"></a>Amos 5:9

That [bālaḡ](../../strongs/h/h1082.md) the [shod](../../strongs/h/h7701.md) against the ['az](../../strongs/h/h5794.md), so that the [shod](../../strongs/h/h7701.md) shall [bow'](../../strongs/h/h935.md) against the [miḇṣār](../../strongs/h/h4013.md).

<a name="amos_5_10"></a>Amos 5:10

They [sane'](../../strongs/h/h8130.md) him that [yakach](../../strongs/h/h3198.md) in the [sha'ar](../../strongs/h/h8179.md), and they [ta'ab](../../strongs/h/h8581.md) him that [dabar](../../strongs/h/h1696.md) [tamiym](../../strongs/h/h8549.md).

<a name="amos_5_11"></a>Amos 5:11

Forasmuch therefore as your [bāšas](../../strongs/h/h1318.md) is upon the [dal](../../strongs/h/h1800.md), and ye [laqach](../../strongs/h/h3947.md) from him [maśśᵊ'ēṯ](../../strongs/h/h4864.md) of [bar](../../strongs/h/h1250.md): ye have [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md) of [gāzîṯ](../../strongs/h/h1496.md), but ye shall not [yashab](../../strongs/h/h3427.md) in them; ye have [nāṭaʿ](../../strongs/h/h5193.md) [ḥemeḏ](../../strongs/h/h2531.md) [kerem](../../strongs/h/h3754.md), but ye shall not [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) of them.

<a name="amos_5_12"></a>Amos 5:12

For I [yada'](../../strongs/h/h3045.md) your [rab](../../strongs/h/h7227.md) [pesha'](../../strongs/h/h6588.md) and your ['atsuwm](../../strongs/h/h6099.md) [chatta'ath](../../strongs/h/h2403.md): they [tsarar](../../strongs/h/h6887.md) the [tsaddiyq](../../strongs/h/h6662.md), they [laqach](../../strongs/h/h3947.md) a [kōp̄er](../../strongs/h/h3724.md), and they [natah](../../strongs/h/h5186.md) the ['ebyown](../../strongs/h/h34.md) in the [sha'ar](../../strongs/h/h8179.md).

<a name="amos_5_13"></a>Amos 5:13

Therefore the [sakal](../../strongs/h/h7919.md) shall keep [damam](../../strongs/h/h1826.md) in that [ʿēṯ](../../strongs/h/h6256.md); for it is a [ra'](../../strongs/h/h7451.md) [ʿēṯ](../../strongs/h/h6256.md).

<a name="amos_5_14"></a>Amos 5:14

[darash](../../strongs/h/h1875.md) [towb](../../strongs/h/h2896.md), and not [ra'](../../strongs/h/h7451.md), that ye may [ḥāyâ](../../strongs/h/h2421.md): and so [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), shall be with you, as ye have ['āmar](../../strongs/h/h559.md).

<a name="amos_5_15"></a>Amos 5:15

[sane'](../../strongs/h/h8130.md) the [ra'](../../strongs/h/h7451.md), and ['ahab](../../strongs/h/h157.md) the [towb](../../strongs/h/h2896.md), and [yāṣaḡ](../../strongs/h/h3322.md) [mishpat](../../strongs/h/h4941.md) in the [sha'ar](../../strongs/h/h8179.md): it may be that [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md) will be [chanan](../../strongs/h/h2603.md) unto the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yôsēp̄](../../strongs/h/h3130.md).

<a name="amos_5_16"></a>Amos 5:16

Therefore [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), the ['adonay](../../strongs/h/h136.md), ['āmar](../../strongs/h/h559.md) thus; [mispēḏ](../../strongs/h/h4553.md) shall be in all [rᵊḥōḇ](../../strongs/h/h7339.md); and they shall ['āmar](../../strongs/h/h559.md) in all the [ḥûṣ](../../strongs/h/h2351.md), [hô](../../strongs/h/h1930.md)! [hô](../../strongs/h/h1930.md)! and they shall [qara'](../../strongs/h/h7121.md) the ['ikār](../../strongs/h/h406.md) to ['ēḇel](../../strongs/h/h60.md), and such as are [yada'](../../strongs/h/h3045.md) of [nᵊhî](../../strongs/h/h5092.md) to [mispēḏ](../../strongs/h/h4553.md).

<a name="amos_5_17"></a>Amos 5:17

And in all [kerem](../../strongs/h/h3754.md) shall be [mispēḏ](../../strongs/h/h4553.md): for I will ['abar](../../strongs/h/h5674.md) [qereḇ](../../strongs/h/h7130.md) thee, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_5_18"></a>Amos 5:18

[hôy](../../strongs/h/h1945.md) unto you that ['āvâ](../../strongs/h/h183.md) the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md)! to what end is it for you? the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [choshek](../../strongs/h/h2822.md), and not ['owr](../../strongs/h/h216.md).

<a name="amos_5_19"></a>Amos 5:19

As if an ['iysh](../../strongs/h/h376.md) did [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) an ['ariy](../../strongs/h/h738.md), and a [dōḇ](../../strongs/h/h1677.md) [pāḡaʿ](../../strongs/h/h6293.md) him; or [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md), and [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) on the [qîr](../../strongs/h/h7023.md), and a [nachash](../../strongs/h/h5175.md) [nāšaḵ](../../strongs/h/h5391.md) him.

<a name="amos_5_20"></a>Amos 5:20

Shall not the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) be [choshek](../../strongs/h/h2822.md), and not ['owr](../../strongs/h/h216.md)? even very ['āp̄ēl](../../strongs/h/h651.md), and no [nogahh](../../strongs/h/h5051.md) in it?

<a name="amos_5_21"></a>Amos 5:21

I [sane'](../../strongs/h/h8130.md), I [mā'as](../../strongs/h/h3988.md) your [ḥāḡ](../../strongs/h/h2282.md), and I will not [rîaḥ](../../strongs/h/h7306.md) in your [ʿăṣārâ](../../strongs/h/h6116.md).

<a name="amos_5_22"></a>Amos 5:22

Though ye [ʿālâ](../../strongs/h/h5927.md) me [ʿōlâ](../../strongs/h/h5930.md) and your [minchah](../../strongs/h/h4503.md), I will not [ratsah](../../strongs/h/h7521.md) them: neither will I [nabat](../../strongs/h/h5027.md) the [šelem](../../strongs/h/h8002.md) of your [mᵊrî'](../../strongs/h/h4806.md).

<a name="amos_5_23"></a>Amos 5:23

Take thou [cuwr](../../strongs/h/h5493.md) from me the [hāmôn](../../strongs/h/h1995.md) of thy [šîr](../../strongs/h/h7892.md); for I will not [shama'](../../strongs/h/h8085.md) the [zimrâ](../../strongs/h/h2172.md) of thy [neḇel](../../strongs/h/h5035.md).

<a name="amos_5_24"></a>Amos 5:24

But let [mishpat](../../strongs/h/h4941.md) [gālal](../../strongs/h/h1556.md) as [mayim](../../strongs/h/h4325.md), and [tsedaqah](../../strongs/h/h6666.md) as a ['êṯān](../../strongs/h/h386.md) [nachal](../../strongs/h/h5158.md).

<a name="amos_5_25"></a>Amos 5:25

Have ye [nāḡaš](../../strongs/h/h5066.md) unto me [zebach](../../strongs/h/h2077.md) and [minchah](../../strongs/h/h4503.md) in the [midbar](../../strongs/h/h4057.md) forty [šānâ](../../strongs/h/h8141.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md)?

<a name="amos_5_26"></a>Amos 5:26

But ye have [nasa'](../../strongs/h/h5375.md) the [Sikûṯ](../../strongs/h/h5522.md) of your [melek](../../strongs/h/h4428.md) and [kîyûn](../../strongs/h/h3594.md) your [tselem](../../strongs/h/h6754.md), the [kowkab](../../strongs/h/h3556.md) of your ['Elohiym](../../strongs/h/h430.md), which ye ['asah](../../strongs/h/h6213.md) to yourselves.

<a name="amos_5_27"></a>Amos 5:27

Therefore will I cause you to go into [gālâ](../../strongs/h/h1540.md) [hāl'â](../../strongs/h/h1973.md) [Dammeśeq](../../strongs/h/h1834.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), whose [shem](../../strongs/h/h8034.md) is The ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 4](amos_4.md) - [Amos 6](amos_6.md)