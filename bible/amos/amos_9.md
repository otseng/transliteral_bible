# [Amos 9](https://www.blueletterbible.org/kjv/amos/9)

<a name="amos_9_1"></a>Amos 9:1

I [ra'ah](../../strongs/h/h7200.md) the ['adonay](../../strongs/h/h136.md) [nāṣaḇ](../../strongs/h/h5324.md) upon the [mizbeach](../../strongs/h/h4196.md): and he ['āmar](../../strongs/h/h559.md), [nakah](../../strongs/h/h5221.md) the [kap̄tôr](../../strongs/h/h3730.md), that the [caph](../../strongs/h/h5592.md) may [rāʿaš](../../strongs/h/h7493.md): and [batsa'](../../strongs/h/h1214.md) them in the [ro'sh](../../strongs/h/h7218.md), all of them; and I will [harag](../../strongs/h/h2026.md) the ['aḥărîṯ](../../strongs/h/h319.md) of them with the [chereb](../../strongs/h/h2719.md): he that [nûs](../../strongs/h/h5127.md) of them shall not flee [nûs](../../strongs/h/h5127.md), and he that [pālîṭ](../../strongs/h/h6412.md) of them shall not be [mālaṭ](../../strongs/h/h4422.md).

<a name="amos_9_2"></a>Amos 9:2

Though they [ḥāṯar](../../strongs/h/h2864.md) into [shĕ'owl](../../strongs/h/h7585.md), thence shall mine [yad](../../strongs/h/h3027.md) [laqach](../../strongs/h/h3947.md) them; though they [ʿālâ](../../strongs/h/h5927.md) to [shamayim](../../strongs/h/h8064.md), thence will I [yarad](../../strongs/h/h3381.md) them:

<a name="amos_9_3"></a>Amos 9:3

And though they [chaba'](../../strongs/h/h2244.md) themselves in the [ro'sh](../../strongs/h/h7218.md) of [Karmel](../../strongs/h/h3760.md), I will [ḥāp̄aś](../../strongs/h/h2664.md) and take them [laqach](../../strongs/h/h3947.md) thence; and though they be [cathar](../../strongs/h/h5641.md) from my ['ayin](../../strongs/h/h5869.md) in the [qarqaʿ](../../strongs/h/h7172.md) of the [yam](../../strongs/h/h3220.md), thence will I [tsavah](../../strongs/h/h6680.md) the [nachash](../../strongs/h/h5175.md), and he shall [nāšaḵ](../../strongs/h/h5391.md) them:

<a name="amos_9_4"></a>Amos 9:4

And though they [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), thence will I [tsavah](../../strongs/h/h6680.md) the [chereb](../../strongs/h/h2719.md), and it shall [harag](../../strongs/h/h2026.md) them: and I will [śûm](../../strongs/h/h7760.md) mine ['ayin](../../strongs/h/h5869.md) upon them for [ra'](../../strongs/h/h7451.md), and not for [towb](../../strongs/h/h2896.md).

<a name="amos_9_5"></a>Amos 9:5

And the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) is he that [naga'](../../strongs/h/h5060.md) the ['erets](../../strongs/h/h776.md), and it shall [mûḡ](../../strongs/h/h4127.md), and all that [yashab](../../strongs/h/h3427.md) therein shall ['āḇal](../../strongs/h/h56.md): and it shall [ʿālâ](../../strongs/h/h5927.md) wholly like a [yᵊ'ōr](../../strongs/h/h2975.md); and shall be [šāqaʿ](../../strongs/h/h8257.md), as by the [yᵊ'ōr](../../strongs/h/h2975.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="amos_9_6"></a>Amos 9:6

It is he that [bānâ](../../strongs/h/h1129.md) his [maʿălâ](../../strongs/h/h4609.md) in the [shamayim](../../strongs/h/h8064.md), and hath [yacad](../../strongs/h/h3245.md) his ['ăḡudâ](../../strongs/h/h92.md) in the ['erets](../../strongs/h/h776.md); he that [qara'](../../strongs/h/h7121.md) for the [mayim](../../strongs/h/h4325.md) of the [yam](../../strongs/h/h3220.md), and [šāp̄aḵ](../../strongs/h/h8210.md) them upon the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md): [Yĕhovah](../../strongs/h/h3068.md) is his [shem](../../strongs/h/h8034.md).

<a name="amos_9_7"></a>Amos 9:7

Are ye not as [ben](../../strongs/h/h1121.md) of the [Kûšî](../../strongs/h/h3569.md) unto me, O [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Have not I [ʿālâ](../../strongs/h/h5927.md) [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md)? and the [Pᵊlištî](../../strongs/h/h6430.md) from [Kap̄Tôr](../../strongs/h/h3731.md), and the ['Ărām](../../strongs/h/h758.md) from [qîr](../../strongs/h/h7024.md)?

<a name="amos_9_8"></a>Amos 9:8

Behold, the ['ayin](../../strongs/h/h5869.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) are upon the [chatta'ath](../../strongs/h/h2403.md) [mamlāḵâ](../../strongs/h/h4467.md), and I will [šāmaḏ](../../strongs/h/h8045.md) it from off the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md); ['ep̄es](../../strongs/h/h657.md) that I will not [šāmaḏ](../../strongs/h/h8045.md) [šāmaḏ](../../strongs/h/h8045.md) the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_9_9"></a>Amos 9:9

For, lo, I will [tsavah](../../strongs/h/h6680.md), and I will [nûaʿ](../../strongs/h/h5128.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) among all [gowy](../../strongs/h/h1471.md), like is [nûaʿ](../../strongs/h/h5128.md) in a [kᵊḇārâ](../../strongs/h/h3531.md) , yet shall not the [ṣᵊrôr](../../strongs/h/h6872.md) [naphal](../../strongs/h/h5307.md) upon the ['erets](../../strongs/h/h776.md).

<a name="amos_9_10"></a>Amos 9:10

All the [chatta'](../../strongs/h/h2400.md) of my ['am](../../strongs/h/h5971.md) shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), which ['āmar](../../strongs/h/h559.md), The [ra'](../../strongs/h/h7451.md) shall not [nāḡaš](../../strongs/h/h5066.md) nor [qadam](../../strongs/h/h6923.md) us.

<a name="amos_9_11"></a>Amos 9:11

In that [yowm](../../strongs/h/h3117.md) will I [quwm](../../strongs/h/h6965.md) the [cukkah](../../strongs/h/h5521.md) of [Dāviḏ](../../strongs/h/h1732.md) that is [naphal](../../strongs/h/h5307.md), and [gāḏar](../../strongs/h/h1443.md) the [pereṣ](../../strongs/h/h6556.md) thereof; and I will [quwm](../../strongs/h/h6965.md) his [hărîsâ](../../strongs/h/h2034.md) , and I will [bānâ](../../strongs/h/h1129.md) it as in the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md):

<a name="amos_9_12"></a>Amos 9:12

That they may [yarash](../../strongs/h/h3423.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of ['Ĕḏōm](../../strongs/h/h123.md), and of all the [gowy](../../strongs/h/h1471.md), which are [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) that ['asah](../../strongs/h/h6213.md) this.

<a name="amos_9_13"></a>Amos 9:13

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that the [ḥāraš](../../strongs/h/h2790.md) shall [nāḡaš](../../strongs/h/h5066.md) the [qāṣar](../../strongs/h/h7114.md), and the [dāraḵ](../../strongs/h/h1869.md) of [ʿēnāḇ](../../strongs/h/h6025.md) him that [mashak](../../strongs/h/h4900.md) [zera'](../../strongs/h/h2233.md); and the [har](../../strongs/h/h2022.md) shall [nāṭap̄](../../strongs/h/h5197.md) [ʿāsîs](../../strongs/h/h6071.md), and all the [giḇʿâ](../../strongs/h/h1389.md) shall [mûḡ](../../strongs/h/h4127.md).

<a name="amos_9_14"></a>Amos 9:14

And I will [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of my ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md), and they shall [bānâ](../../strongs/h/h1129.md) the [šāmēm](../../strongs/h/h8074.md) [ʿîr](../../strongs/h/h5892.md), and [yashab](../../strongs/h/h3427.md) them; and they shall [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), and [šāṯâ](../../strongs/h/h8354.md) the [yayin](../../strongs/h/h3196.md) thereof; they shall also ['asah](../../strongs/h/h6213.md) [gannâ](../../strongs/h/h1593.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of them.

<a name="amos_9_15"></a>Amos 9:15

And I will [nāṭaʿ](../../strongs/h/h5193.md) them upon their ['ăḏāmâ](../../strongs/h/h127.md), and they shall no more be pulled [nathash](../../strongs/h/h5428.md) out of their ['ăḏāmâ](../../strongs/h/h127.md) which I have [nathan](../../strongs/h/h5414.md) them, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 8](amos_8.md)