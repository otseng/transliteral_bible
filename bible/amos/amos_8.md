# [Amos 8](https://www.blueletterbible.org/kjv/amos/8)

<a name="amos_8_1"></a>Amos 8:1

Thus hath the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [ra'ah](../../strongs/h/h7200.md) unto me: and behold a [kᵊlûḇ](../../strongs/h/h3619.md) of [qayiṣ](../../strongs/h/h7019.md).

<a name="amos_8_2"></a>Amos 8:2

And he ['āmar](../../strongs/h/h559.md), [ʿĀmôs](../../strongs/h/h5986.md), what [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), A [kᵊlûḇ](../../strongs/h/h3619.md) of [qayiṣ](../../strongs/h/h7019.md). Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, The [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md) upon my ['am](../../strongs/h/h5971.md) of [Yisra'el](../../strongs/h/h3478.md); I will not again ['abar](../../strongs/h/h5674.md) by them any more.

<a name="amos_8_3"></a>Amos 8:3

And the [šîr](../../strongs/h/h7892.md) of the [heykal](../../strongs/h/h1964.md) shall be [yālal](../../strongs/h/h3213.md) in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md): there shall be [rab](../../strongs/h/h7227.md) [peḡer](../../strongs/h/h6297.md) in every [maqowm](../../strongs/h/h4725.md); they shall [shalak](../../strongs/h/h7993.md) them with [hāsâ](../../strongs/h/h2013.md).

<a name="amos_8_4"></a>Amos 8:4

[shama'](../../strongs/h/h8085.md) this, O ye that [šā'ap̄](../../strongs/h/h7602.md) the ['ebyown](../../strongs/h/h34.md), even to [shabath](../../strongs/h/h7673.md) the ['aniy](../../strongs/h/h6041.md) ['anav](../../strongs/h/h6035.md) of the ['erets](../../strongs/h/h776.md) to [shabath](../../strongs/h/h7673.md),

<a name="amos_8_5"></a>Amos 8:5

['āmar](../../strongs/h/h559.md), When will the [ḥōḏeš](../../strongs/h/h2320.md) be ['abar](../../strongs/h/h5674.md), that we may [šāḇar](../../strongs/h/h7666.md) [šēḇer](../../strongs/h/h7668.md)? and the [shabbath](../../strongs/h/h7676.md), that we may set [pāṯaḥ](../../strongs/h/h6605.md) [bar](../../strongs/h/h1250.md), making the ['êp̄â](../../strongs/h/h374.md) [qāṭōn](../../strongs/h/h6994.md), and the [šeqel](../../strongs/h/h8255.md) [gāḏal](../../strongs/h/h1431.md), and [ʿāvaṯ](../../strongs/h/h5791.md) the [mō'znayim](../../strongs/h/h3976.md) by [mirmah](../../strongs/h/h4820.md)?

<a name="amos_8_6"></a>Amos 8:6

That we may [qānâ](../../strongs/h/h7069.md) the [dal](../../strongs/h/h1800.md) for [keceph](../../strongs/h/h3701.md), and the ['ebyown](../../strongs/h/h34.md) for a pair of [naʿal](../../strongs/h/h5275.md); yea, and [šāḇar](../../strongs/h/h7666.md) the [mapāl](../../strongs/h/h4651.md) of the [bar](../../strongs/h/h1250.md)?

<a name="amos_8_7"></a>Amos 8:7

[Yĕhovah](../../strongs/h/h3068.md) hath [shaba'](../../strongs/h/h7650.md) by the [gā'ôn](../../strongs/h/h1347.md) of [Ya'aqob](../../strongs/h/h3290.md), Surely I will [netsach](../../strongs/h/h5331.md) [shakach](../../strongs/h/h7911.md) any of their [ma'aseh](../../strongs/h/h4639.md).

<a name="amos_8_8"></a>Amos 8:8

Shall not the ['erets](../../strongs/h/h776.md) [ragaz](../../strongs/h/h7264.md) for this, and every one ['āḇal](../../strongs/h/h56.md) that [yashab](../../strongs/h/h3427.md) therein? and it shall [ʿālâ](../../strongs/h/h5927.md) wholly as a [yᵊ'ōr](../../strongs/h/h2975.md); and it shall be [gāraš](../../strongs/h/h1644.md) and [šāqaʿ](../../strongs/h/h8257.md) [šāqâ](../../strongs/h/h8248.md), as by the ['owr](../../strongs/h/h216.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="amos_8_9"></a>Amos 8:9

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), that I will cause the [šemeš](../../strongs/h/h8121.md) to [bow'](../../strongs/h/h935.md) at [ṣōhar](../../strongs/h/h6672.md), and I will [ḥāšaḵ](../../strongs/h/h2821.md) the ['erets](../../strongs/h/h776.md) in the ['owr](../../strongs/h/h216.md) [yowm](../../strongs/h/h3117.md):

<a name="amos_8_10"></a>Amos 8:10

And I will [hāp̄aḵ](../../strongs/h/h2015.md) your [ḥāḡ](../../strongs/h/h2282.md) into ['ēḇel](../../strongs/h/h60.md), and all your [šîr](../../strongs/h/h7892.md) into [qînâ](../../strongs/h/h7015.md); and I will [ʿālâ](../../strongs/h/h5927.md) [śaq](../../strongs/h/h8242.md) upon all [māṯnayim](../../strongs/h/h4975.md), and [qrḥ](../../strongs/h/h7144.md) upon every [ro'sh](../../strongs/h/h7218.md); and I will [śûm](../../strongs/h/h7760.md) it as the ['ēḇel](../../strongs/h/h60.md) of a [yāḥîḏ](../../strongs/h/h3173.md) son, and the ['aḥărîṯ](../../strongs/h/h319.md) thereof as a [mar](../../strongs/h/h4751.md) [yowm](../../strongs/h/h3117.md).

<a name="amos_8_11"></a>Amos 8:11

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), that I will [shalach](../../strongs/h/h7971.md) a [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md), not a [rāʿāḇ](../../strongs/h/h7458.md) of [lechem](../../strongs/h/h3899.md), nor a [ṣāmā'](../../strongs/h/h6772.md) for [mayim](../../strongs/h/h4325.md), but of [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="amos_8_12"></a>Amos 8:12

And they shall [nûaʿ](../../strongs/h/h5128.md) from [yam](../../strongs/h/h3220.md) to [yam](../../strongs/h/h3220.md), and from the [ṣāp̄ôn](../../strongs/h/h6828.md) even to the [mizrach](../../strongs/h/h4217.md), they shall [šûṭ](../../strongs/h/h7751.md) to [bāqaš](../../strongs/h/h1245.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and shall not [māṣā'](../../strongs/h/h4672.md) it.

<a name="amos_8_13"></a>Amos 8:13

In that [yowm](../../strongs/h/h3117.md) shall the [yāp̄ê](../../strongs/h/h3303.md) [bᵊṯûlâ](../../strongs/h/h1330.md) and [bāḥûr](../../strongs/h/h970.md) [ʿālap̄](../../strongs/h/h5968.md) for [ṣāmā'](../../strongs/h/h6772.md).

<a name="amos_8_14"></a>Amos 8:14

They that [shaba'](../../strongs/h/h7650.md) by the ['ašmâ](../../strongs/h/h819.md) of [Šōmrôn](../../strongs/h/h8111.md), and ['āmar](../../strongs/h/h559.md), Thy ['Elohiym](../../strongs/h/h430.md), O [Dān](../../strongs/h/h1835.md), [chay](../../strongs/h/h2416.md); and, The [derek](../../strongs/h/h1870.md) of [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) [chay](../../strongs/h/h2416.md); even they shall [naphal](../../strongs/h/h5307.md), and never [quwm](../../strongs/h/h6965.md) again.

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 7](amos_7.md) - [Amos 9](amos_9.md)