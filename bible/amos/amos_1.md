# [Amos 1](https://www.blueletterbible.org/kjv/amos/1)

<a name="amos_1_1"></a>Amos 1:1

The [dabar](../../strongs/h/h1697.md) of [ʿĀmôs](../../strongs/h/h5986.md), who was among the [nōqēḏ](../../strongs/h/h5349.md) of [Tᵊqôaʿ](../../strongs/h/h8620.md), which he [chazah](../../strongs/h/h2372.md) concerning [Yisra'el](../../strongs/h/h3478.md) in the [yowm](../../strongs/h/h3117.md) of ['Uzziyah](../../strongs/h/h5818.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [yowm](../../strongs/h/h3117.md) of [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), two [šānâ](../../strongs/h/h8141.md) [paniym](../../strongs/h/h6440.md) the [raʿaš](../../strongs/h/h7494.md).

<a name="amos_1_2"></a>Amos 1:2

And he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) will [šā'aḡ](../../strongs/h/h7580.md) from [Tsiyown](../../strongs/h/h6726.md), and [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md) from [Yĕruwshalaim](../../strongs/h/h3389.md); and the [nā'â](../../strongs/h/h4999.md) of the [ra'ah](../../strongs/h/h7462.md) shall ['āḇal](../../strongs/h/h56.md), and the [ro'sh](../../strongs/h/h7218.md) of [Karmel](../../strongs/h/h3760.md) shall [yāḇēš](../../strongs/h/h3001.md).

<a name="amos_1_3"></a>Amos 1:3

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [Dammeśeq](../../strongs/h/h1834.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because they have [dûš](../../strongs/h/h1758.md) [Gilʿāḏ](../../strongs/h/h1568.md) with [ḥārûṣ](../../strongs/h/h2742.md) of [barzel](../../strongs/h/h1270.md):

<a name="amos_1_4"></a>Amos 1:4

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) into the [bayith](../../strongs/h/h1004.md) of [Ḥăzā'Ēl](../../strongs/h/h2371.md), which shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Ben-Hăḏaḏ](../../strongs/h/h1130.md).

<a name="amos_1_5"></a>Amos 1:5

I will [shabar](../../strongs/h/h7665.md) also the [bᵊrîaḥ](../../strongs/h/h1280.md) of [Dammeśeq](../../strongs/h/h1834.md), and [karath](../../strongs/h/h3772.md) the [yashab](../../strongs/h/h3427.md) from the [biqʿâ](../../strongs/h/h1237.md) of ['Āven](../../strongs/h/h206.md), and him that [tamak](../../strongs/h/h8551.md) the [shebet](../../strongs/h/h7626.md) from the [bayith](../../strongs/h/h1004.md) of ['Eden](../../strongs/h/h5731.md) [Bêṯ ʿĒḏen](../../strongs/h/h1040.md) : and the ['am](../../strongs/h/h5971.md) of ['Ărām](../../strongs/h/h758.md) shall go into [gālâ](../../strongs/h/h1540.md) unto [qîr](../../strongs/h/h7024.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_1_6"></a>Amos 1:6

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [ʿAzzâ](../../strongs/h/h5804.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because they [gālâ](../../strongs/h/h1540.md) the [šālēm](../../strongs/h/h8003.md) [gālûṯ](../../strongs/h/h1546.md), to [cagar](../../strongs/h/h5462.md) them to ['Ĕḏōm](../../strongs/h/h123.md):

<a name="amos_1_7"></a>Amos 1:7

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) on the [ḥômâ](../../strongs/h/h2346.md) of [ʿAzzâ](../../strongs/h/h5804.md), which shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) thereof:

<a name="amos_1_8"></a>Amos 1:8

And I will [karath](../../strongs/h/h3772.md) the [yashab](../../strongs/h/h3427.md) from ['Ašdôḏ](../../strongs/h/h795.md), and him that [tamak](../../strongs/h/h8551.md) the [shebet](../../strongs/h/h7626.md) from ['Ašqᵊlôn](../../strongs/h/h831.md), and I will [shuwb](../../strongs/h/h7725.md) mine [yad](../../strongs/h/h3027.md) against [ʿEqrôn](../../strongs/h/h6138.md): and the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [Pᵊlištî](../../strongs/h/h6430.md) shall ['abad](../../strongs/h/h6.md), ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="amos_1_9"></a>Amos 1:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [Ṣōr](../../strongs/h/h6865.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because they [cagar](../../strongs/h/h5462.md) the [šālēm](../../strongs/h/h8003.md) [gālûṯ](../../strongs/h/h1546.md) to ['Ĕḏōm](../../strongs/h/h123.md), and [zakar](../../strongs/h/h2142.md) not the ['ach](../../strongs/h/h251.md) [bĕriyth](../../strongs/h/h1285.md):

<a name="amos_1_10"></a>Amos 1:10

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) on the [ḥômâ](../../strongs/h/h2346.md) of [Ṣōr](../../strongs/h/h6865.md), which shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) thereof.

<a name="amos_1_11"></a>Amos 1:11

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of ['Ĕḏōm](../../strongs/h/h123.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because he did [radaph](../../strongs/h/h7291.md) his ['ach](../../strongs/h/h251.md) with the [chereb](../../strongs/h/h2719.md), and did [shachath](../../strongs/h/h7843.md) all [raḥam](../../strongs/h/h7356.md), and his ['aph](../../strongs/h/h639.md) did [taraph](../../strongs/h/h2963.md) [ʿaḏ](../../strongs/h/h5703.md), and he [shamar](../../strongs/h/h8104.md) his ['ebrah](../../strongs/h/h5678.md) for [netsach](../../strongs/h/h5331.md):

<a name="amos_1_12"></a>Amos 1:12

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) upon [têmān](../../strongs/h/h8487.md), which shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Bāṣrâ](../../strongs/h/h1224.md).

<a name="amos_1_13"></a>Amos 1:13

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because they have [bāqaʿ](../../strongs/h/h1234.md) with [hārê](../../strongs/h/h2030.md) of [Gilʿāḏ](../../strongs/h/h1568.md), that they might [rāḥaḇ](../../strongs/h/h7337.md) their [gᵊḇûl](../../strongs/h/h1366.md):

<a name="amos_1_14"></a>Amos 1:14

But I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in the [ḥômâ](../../strongs/h/h2346.md) of [Rabâ](../../strongs/h/h7237.md), and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) thereof, with [tᵊrûʿâ](../../strongs/h/h8643.md) in the [yowm](../../strongs/h/h3117.md) of [milḥāmâ](../../strongs/h/h4421.md), with a [saʿar](../../strongs/h/h5591.md) in the [yowm](../../strongs/h/h3117.md) of the [sûp̄â](../../strongs/h/h5492.md):

<a name="amos_1_15"></a>Amos 1:15

And their [melek](../../strongs/h/h4428.md) shall [halak](../../strongs/h/h1980.md) into [gôlâ](../../strongs/h/h1473.md), he and his [śar](../../strongs/h/h8269.md) [yaḥaḏ](../../strongs/h/h3162.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 2](amos_2.md)