# [Amos 4](https://www.blueletterbible.org/kjv/amos/4)

<a name="amos_4_1"></a>Amos 4:1

[shama'](../../strongs/h/h8085.md) this [dabar](../../strongs/h/h1697.md), ye [pārâ](../../strongs/h/h6510.md) of [Bāšān](../../strongs/h/h1316.md), that are in the [har](../../strongs/h/h2022.md) of [Šōmrôn](../../strongs/h/h8111.md), which [ʿāšaq](../../strongs/h/h6231.md) the [dal](../../strongs/h/h1800.md), which [rāṣaṣ](../../strongs/h/h7533.md) the ['ebyown](../../strongs/h/h34.md), which ['āmar](../../strongs/h/h559.md) to their ['adown](../../strongs/h/h113.md), [bow'](../../strongs/h/h935.md), and let us [šāṯâ](../../strongs/h/h8354.md).

<a name="amos_4_2"></a>Amos 4:2

The ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) hath [shaba'](../../strongs/h/h7650.md) by his [qodesh](../../strongs/h/h6944.md), that, lo, the [yowm](../../strongs/h/h3117.md) shall [bow'](../../strongs/h/h935.md) upon you, that he will take you [nasa'](../../strongs/h/h5375.md) with [tsinnah](../../strongs/h/h6793.md), and your ['aḥărîṯ](../../strongs/h/h319.md) with [dûḡâ](../../strongs/h/h1729.md) [sîr](../../strongs/h/h5518.md).

<a name="amos_4_3"></a>Amos 4:3

And ye shall [yāṣā'](../../strongs/h/h3318.md) at the [pereṣ](../../strongs/h/h6556.md), ['ishshah](../../strongs/h/h802.md) before her; and ye shall [shalak](../../strongs/h/h7993.md) them into the [Harmôn](../../strongs/h/h2038.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_4"></a>Amos 4:4

[bow'](../../strongs/h/h935.md) to [Bêṯ-'ēl](../../strongs/h/h1008.md), and [pāšaʿ](../../strongs/h/h6586.md); at [Gilgāl](../../strongs/h/h1537.md) [rabah](../../strongs/h/h7235.md) [pāšaʿ](../../strongs/h/h6586.md); and [bow'](../../strongs/h/h935.md) your [zebach](../../strongs/h/h2077.md) every [boqer](../../strongs/h/h1242.md), and your [maʿăśēr](../../strongs/h/h4643.md) after three [yowm](../../strongs/h/h3117.md):

<a name="amos_4_5"></a>Amos 4:5

And [qāṭar](../../strongs/h/h6999.md) a [tôḏâ](../../strongs/h/h8426.md) with [ḥāmēṣ](../../strongs/h/h2557.md), and [qara'](../../strongs/h/h7121.md) and [shama'](../../strongs/h/h8085.md) the [nᵊḏāḇâ](../../strongs/h/h5071.md): for this ['ahab](../../strongs/h/h157.md) you, O ye [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="amos_4_6"></a>Amos 4:6

And I also have [nathan](../../strongs/h/h5414.md) you [niqqāyôn](../../strongs/h/h5356.md) of [šēn](../../strongs/h/h8127.md) in all your [ʿîr](../../strongs/h/h5892.md), and [ḥōser](../../strongs/h/h2640.md) of [lechem](../../strongs/h/h3899.md) in all your [maqowm](../../strongs/h/h4725.md): yet have ye not [shuwb](../../strongs/h/h7725.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_7"></a>Amos 4:7

And also I have [mānaʿ](../../strongs/h/h4513.md) the [gešem](../../strongs/h/h1653.md) from you, when there were yet three [ḥōḏeš](../../strongs/h/h2320.md) to the [qāṣîr](../../strongs/h/h7105.md): and I caused it to [matar](../../strongs/h/h4305.md) upon one [ʿîr](../../strongs/h/h5892.md), and caused it not to [matar](../../strongs/h/h4305.md) upon another [ʿîr](../../strongs/h/h5892.md): one [ḥelqâ](../../strongs/h/h2513.md) was [matar](../../strongs/h/h4305.md) upon, and the [ḥelqâ](../../strongs/h/h2513.md) whereupon it [matar](../../strongs/h/h4305.md) not [yāḇēš](../../strongs/h/h3001.md).

<a name="amos_4_8"></a>Amos 4:8

So two or three [ʿîr](../../strongs/h/h5892.md) [nûaʿ](../../strongs/h/h5128.md) unto one [ʿîr](../../strongs/h/h5892.md), to [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md); but they were not [sāׂbaʿ](../../strongs/h/h7646.md): yet have ye not [shuwb](../../strongs/h/h7725.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_9"></a>Amos 4:9

I have [nakah](../../strongs/h/h5221.md) you with [šᵊḏēp̄â](../../strongs/h/h7711.md) and [yērāqôn](../../strongs/h/h3420.md): when your [gannâ](../../strongs/h/h1593.md) and your [kerem](../../strongs/h/h3754.md) and your [tĕ'en](../../strongs/h/h8384.md) and your [zayiṯ](../../strongs/h/h2132.md) [rabah](../../strongs/h/h7235.md), the [gāzām](../../strongs/h/h1501.md) ['akal](../../strongs/h/h398.md) them: yet have ye not [shuwb](../../strongs/h/h7725.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_10"></a>Amos 4:10

I have [shalach](../../strongs/h/h7971.md) among you the [deḇer](../../strongs/h/h1698.md) after the [derek](../../strongs/h/h1870.md) of [Mitsrayim](../../strongs/h/h4714.md): your [bāḥûr](../../strongs/h/h970.md) have I [harag](../../strongs/h/h2026.md) with the [chereb](../../strongs/h/h2719.md), and have taken [šᵊḇî](../../strongs/h/h7628.md) your [sûs](../../strongs/h/h5483.md); and I have made the [bᵊ'š](../../strongs/h/h889.md) of your [maḥănê](../../strongs/h/h4264.md) to [ʿālâ](../../strongs/h/h5927.md) unto your ['aph](../../strongs/h/h639.md): yet have ye not [shuwb](../../strongs/h/h7725.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_11"></a>Amos 4:11

I have [hāp̄aḵ](../../strongs/h/h2015.md) some of you, as ['Elohiym](../../strongs/h/h430.md) [mahpēḵâ](../../strongs/h/h4114.md) [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md), and ye were as a ['ûḏ](../../strongs/h/h181.md) [natsal](../../strongs/h/h5337.md) of the [śᵊrēp̄â](../../strongs/h/h8316.md): yet have ye not [shuwb](../../strongs/h/h7725.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_4_12"></a>Amos 4:12

Therefore thus will I ['asah](../../strongs/h/h6213.md) unto thee, O [Yisra'el](../../strongs/h/h3478.md): and because I will ['asah](../../strongs/h/h6213.md) this unto thee, [kuwn](../../strongs/h/h3559.md) to [qārā'](../../strongs/h/h7125.md) thy ['Elohiym](../../strongs/h/h430.md), O [Yisra'el](../../strongs/h/h3478.md).

<a name="amos_4_13"></a>Amos 4:13

For, lo, he that [yāṣar](../../strongs/h/h3335.md) the [har](../../strongs/h/h2022.md), and [bara'](../../strongs/h/h1254.md) the [ruwach](../../strongs/h/h7307.md), and [nāḡaḏ](../../strongs/h/h5046.md) unto ['āḏām](../../strongs/h/h120.md) what is his [śēaḥ](../../strongs/h/h7808.md), that ['asah](../../strongs/h/h6213.md) the [šaḥar](../../strongs/h/h7837.md) [ʿêp̄â](../../strongs/h/h5890.md), and [dāraḵ](../../strongs/h/h1869.md) upon the [bāmâ](../../strongs/h/h1116.md) of the ['erets](../../strongs/h/h776.md), [Yĕhovah](../../strongs/h/h3068.md), The ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), is his [shem](../../strongs/h/h8034.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 3](amos_3.md) - [Amos 5](amos_5.md)