# [Amos 2](https://www.blueletterbible.org/kjv/amos/2)

<a name="amos_2_1"></a>Amos 2:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [Mô'āḇ](../../strongs/h/h4124.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because he [śārap̄](../../strongs/h/h8313.md) the ['etsem](../../strongs/h/h6106.md) of the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md) into [śîḏ](../../strongs/h/h7875.md):

<a name="amos_2_2"></a>Amos 2:2

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) upon [Mô'āḇ](../../strongs/h/h4124.md), and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Qᵊrîyôṯ](../../strongs/h/h7152.md): and [Mô'āḇ](../../strongs/h/h4124.md) shall [muwth](../../strongs/h/h4191.md) with [shā'ôn](../../strongs/h/h7588.md), with [tᵊrûʿâ](../../strongs/h/h8643.md), and with the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md):

<a name="amos_2_3"></a>Amos 2:3

And I will [karath](../../strongs/h/h3772.md) the [shaphat](../../strongs/h/h8199.md) from the [qereḇ](../../strongs/h/h7130.md) thereof, and will [harag](../../strongs/h/h2026.md) all the [śar](../../strongs/h/h8269.md) thereof with him, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_2_4"></a>Amos 2:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [Yehuwdah](../../strongs/h/h3063.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) thereof; because they have [mā'as](../../strongs/h/h3988.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md), and have not [shamar](../../strongs/h/h8104.md) his [choq](../../strongs/h/h2706.md), and their [kazab](../../strongs/h/h3577.md) caused them to [tāʿâ](../../strongs/h/h8582.md), ['aḥar](../../strongs/h/h310.md) the which their ['ab](../../strongs/h/h1.md) have [halak](../../strongs/h/h1980.md):

<a name="amos_2_5"></a>Amos 2:5

But I will [shalach](../../strongs/h/h7971.md) an ['esh](../../strongs/h/h784.md) upon [Yehuwdah](../../strongs/h/h3063.md), and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="amos_2_6"></a>Amos 2:6

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); For three [pesha'](../../strongs/h/h6588.md) of [Yisra'el](../../strongs/h/h3478.md), and for four, I will not [shuwb](../../strongs/h/h7725.md) the punishment thereof; because they [māḵar](../../strongs/h/h4376.md) the [tsaddiyq](../../strongs/h/h6662.md) for [keceph](../../strongs/h/h3701.md), and the ['ebyown](../../strongs/h/h34.md) for a pair of [naʿal](../../strongs/h/h5275.md);

<a name="amos_2_7"></a>Amos 2:7

That [šā'ap̄](../../strongs/h/h7602.md) after the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md) on the [ro'sh](../../strongs/h/h7218.md) of the [dal](../../strongs/h/h1800.md), and turn [natah](../../strongs/h/h5186.md) the [derek](../../strongs/h/h1870.md) of the ['anav](../../strongs/h/h6035.md): and an ['iysh](../../strongs/h/h376.md) and his ['ab](../../strongs/h/h1.md) will [yālaḵ](../../strongs/h/h3212.md) in unto the same [naʿărâ](../../strongs/h/h5291.md), to [ḥālal](../../strongs/h/h2490.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md):

<a name="amos_2_8"></a>Amos 2:8

And they lay themselves [natah](../../strongs/h/h5186.md) upon [beḡeḏ](../../strongs/h/h899.md) laid to [chabal](../../strongs/h/h2254.md) by every [mizbeach](../../strongs/h/h4196.md), and they [šāṯâ](../../strongs/h/h8354.md) the [yayin](../../strongs/h/h3196.md) of the [ʿānaš](../../strongs/h/h6064.md) in the [bayith](../../strongs/h/h1004.md) of their ['Elohiym](../../strongs/h/h430.md).

<a name="amos_2_9"></a>Amos 2:9

Yet [šāmaḏ](../../strongs/h/h8045.md) I the ['Ĕmōrî](../../strongs/h/h567.md) [paniym](../../strongs/h/h6440.md) them, whose [gobahh](../../strongs/h/h1363.md) was like the [gobahh](../../strongs/h/h1363.md) of the ['erez](../../strongs/h/h730.md), and he was [ḥāsōn](../../strongs/h/h2634.md) as the ['allôn](../../strongs/h/h437.md); yet I [šāmaḏ](../../strongs/h/h8045.md) his [pĕriy](../../strongs/h/h6529.md) from [maʿal](../../strongs/h/h4605.md), and his [šereš](../../strongs/h/h8328.md) from beneath.

<a name="amos_2_10"></a>Amos 2:10

Also I [ʿālâ](../../strongs/h/h5927.md) you from the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [yālaḵ](../../strongs/h/h3212.md) you forty [šānâ](../../strongs/h/h8141.md) through the [midbar](../../strongs/h/h4057.md), to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md) of the ['Ĕmōrî](../../strongs/h/h567.md).

<a name="amos_2_11"></a>Amos 2:11

And I [quwm](../../strongs/h/h6965.md) of your [ben](../../strongs/h/h1121.md) for [nāḇî'](../../strongs/h/h5030.md), and of your [bāḥûr](../../strongs/h/h970.md) for [nāzîr](../../strongs/h/h5139.md). Is it not even thus, O ye [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="amos_2_12"></a>Amos 2:12

But ye gave the [nāzîr](../../strongs/h/h5139.md) [yayin](../../strongs/h/h3196.md) to [šāqâ](../../strongs/h/h8248.md); and [tsavah](../../strongs/h/h6680.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md), [nāḇā'](../../strongs/h/h5012.md) not.

<a name="amos_2_13"></a>Amos 2:13

Behold, I am [ʿûq](../../strongs/h/h5781.md) under you, as a [ʿăḡālâ](../../strongs/h/h5699.md) is [ʿûq](../../strongs/h/h5781.md) that is [mālē'](../../strongs/h/h4392.md) of [ʿāmîr](../../strongs/h/h5995.md).

<a name="amos_2_14"></a>Amos 2:14

Therefore the [mānôs](../../strongs/h/h4498.md) shall ['abad](../../strongs/h/h6.md) from the [qal](../../strongs/h/h7031.md), and the [ḥāzāq](../../strongs/h/h2389.md) shall not ['amats](../../strongs/h/h553.md) his [koach](../../strongs/h/h3581.md), neither shall the [gibôr](../../strongs/h/h1368.md) [mālaṭ](../../strongs/h/h4422.md) [nephesh](../../strongs/h/h5315.md):

<a name="amos_2_15"></a>Amos 2:15

Neither shall he ['amad](../../strongs/h/h5975.md) that [tāp̄aś](../../strongs/h/h8610.md) the [qesheth](../../strongs/h/h7198.md); and he that is [qal](../../strongs/h/h7031.md) of [regel](../../strongs/h/h7272.md) shall not [mālaṭ](../../strongs/h/h4422.md) himself: neither shall he that [rāḵaḇ](../../strongs/h/h7392.md) the [sûs](../../strongs/h/h5483.md) [mālaṭ](../../strongs/h/h4422.md) [nephesh](../../strongs/h/h5315.md).

<a name="amos_2_16"></a>Amos 2:16

And he that is ['ammîṣ](../../strongs/h/h533.md) [leb](../../strongs/h/h3820.md) among the [gibôr](../../strongs/h/h1368.md) shall [nûs](../../strongs/h/h5127.md) ['arowm](../../strongs/h/h6174.md) in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Amos](amos.md)

[Amos 1](amos_1.md) - [Amos 3](amos_3.md)