# [3 John 1](https://www.blueletterbible.org/kjv/3jo/1/1/s_1166001)

<a name="3john_1_1"></a>3 John 1:1

The [presbyteros](../../strongs/g/g4245.md) unto [agapētos](../../strongs/g/g27.md) [Gaïos](../../strongs/g/g1050.md), whom I [agapaō](../../strongs/g/g25.md) in the [alētheia](../../strongs/g/g225.md).

<a name="3john_1_2"></a>3 John 1:2

[agapētos](../../strongs/g/g27.md), I [euchomai](../../strongs/g/g2172.md) above all things that thou mayest [euodoo](../../strongs/g/g2137.md) and be in [hygiainō](../../strongs/g/g5198.md), even as thy [psychē](../../strongs/g/g5590.md) [euodoo](../../strongs/g/g2137.md).

<a name="3john_1_3"></a>3 John 1:3

For I [chairō](../../strongs/g/g5463.md) [lian](../../strongs/g/g3029.md), when the [adelphos](../../strongs/g/g80.md) [erchomai](../../strongs/g/g2064.md) and [martyreō](../../strongs/g/g3140.md) of the [alētheia](../../strongs/g/g225.md) that is in thee, even as thou [peripateō](../../strongs/g/g4043.md) in the [alētheia](../../strongs/g/g225.md).

<a name="3john_1_4"></a>3 John 1:4

I have no [meizoteros](../../strongs/g/g3186.md) [chara](../../strongs/g/g5479.md) than to [akouō](../../strongs/g/g191.md) that my [teknon](../../strongs/g/g5043.md) [peripateō](../../strongs/g/g4043.md) in [alētheia](../../strongs/g/g225.md).

<a name="3john_1_5"></a>3 John 1:5

[agapētos](../../strongs/g/g27.md), thou [poieō](../../strongs/g/g4160.md) [pistos](../../strongs/g/g4103.md) whatsoever thou [ergazomai](../../strongs/g/g2038.md) to the [adelphos](../../strongs/g/g80.md), and to [xenos](../../strongs/g/g3581.md);

<a name="3john_1_6"></a>3 John 1:6

Which have [martyreō](../../strongs/g/g3140.md) of thy [agapē](../../strongs/g/g26.md) before the [ekklēsia](../../strongs/g/g1577.md): whom if thou [propempō](../../strongs/g/g4311.md) after a [theos](../../strongs/g/g2316.md) [axiōs](../../strongs/g/g516.md), thou shalt [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md):

<a name="3john_1_7"></a>3 John 1:7

Because that for his [onoma](../../strongs/g/g3686.md) sake they [exerchomai](../../strongs/g/g1831.md), [lambanō](../../strongs/g/g2983.md) [mēdeis](../../strongs/g/g3367.md) of the [ethnos](../../strongs/g/g1484.md).

<a name="3john_1_8"></a>3 John 1:8

We therefore [opheilō](../../strongs/g/g3784.md) to [apolambanō](../../strongs/g/g618.md) such, that we might be [synergos](../../strongs/g/g4904.md) to the [alētheia](../../strongs/g/g225.md).

<a name="3john_1_9"></a>3 John 1:9

I [graphō](../../strongs/g/g1125.md) unto the [ekklēsia](../../strongs/g/g1577.md): but [diotrephēs](../../strongs/g/g1361.md), who [philoprōteuō](../../strongs/g/g5383.md) among them, [epidechomai](../../strongs/g/g1926.md) us not.

<a name="3john_1_10"></a>3 John 1:10

Wherefore, if I [erchomai](../../strongs/g/g2064.md), I will [hypomimnēskō](../../strongs/g/g5279.md) his [ergon](../../strongs/g/g2041.md) which he [poieō](../../strongs/g/g4160.md), [phlyareō](../../strongs/g/g5396.md) us with [ponēros](../../strongs/g/g4190.md) [logos](../../strongs/g/g3056.md): and not [arkeō](../../strongs/g/g714.md) therewith, neither doth he himself [epidechomai](../../strongs/g/g1926.md) the [adelphos](../../strongs/g/g80.md), and [kōlyō](../../strongs/g/g2967.md) them that [boulomai](../../strongs/g/g1014.md), and [ekballō](../../strongs/g/g1544.md) out of the [ekklēsia](../../strongs/g/g1577.md).

<a name="3john_1_11"></a>3 John 1:11

[agapētos](../../strongs/g/g27.md), [mimeomai](../../strongs/g/g3401.md) not that which is [kakos](../../strongs/g/g2556.md), but that which is [agathos](../../strongs/g/g18.md). He that [agathopoieō](../../strongs/g/g15.md) is of [theos](../../strongs/g/g2316.md): but he that [kakopoieō](../../strongs/g/g2554.md) hath not [horaō](../../strongs/g/g3708.md) [theos](../../strongs/g/g2316.md).

<a name="3john_1_12"></a>3 John 1:12

[Dēmētrios](../../strongs/g/g1216.md) hath [martyreō](../../strongs/g/g3140.md) of all, and of the [alētheia](../../strongs/g/g225.md) itself: yea, and we also [martyreō](../../strongs/g/g3140.md); and ye [eidō](../../strongs/g/g1492.md) that our [martyria](../../strongs/g/g3141.md) is [alēthēs](../../strongs/g/g227.md).

<a name="3john_1_13"></a>3 John 1:13

I had [polys](../../strongs/g/g4183.md) to [graphō](../../strongs/g/g1125.md), but I will not with [melan](../../strongs/g/g3188.md) and [kalamos](../../strongs/g/g2563.md) [graphō](../../strongs/g/g1125.md) unto thee:

<a name="3john_1_14"></a>3 John 1:14

But I [elpizō](../../strongs/g/g1679.md) I shall [eutheōs](../../strongs/g/g2112.md) [eidō](../../strongs/g/g1492.md) thee, and we shall [laleō](../../strongs/g/g2980.md) [stoma](../../strongs/g/g4750.md) to [stoma](../../strongs/g/g4750.md). [eirēnē](../../strongs/g/g1515.md) be to thee. [philos](../../strongs/g/g5384.md) [aspazomai](../../strongs/g/g782.md) thee. [aspazomai](../../strongs/g/g782.md) the [philos](../../strongs/g/g5384.md) by [onoma](../../strongs/g/g3686.md).

---

[Transliteral Bible](../bible.md)

[3 John](3john.md)