# [Judges 2](https://www.blueletterbible.org/kjv/judges/2)

<a name="judges_2_1"></a>Judges 2:1

And a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) from [Gilgāl](../../strongs/h/h1537.md) to [Bōḵîm](../../strongs/h/h1066.md), and ['āmar](../../strongs/h/h559.md), I made you to [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), and have [bow'](../../strongs/h/h935.md) you unto the ['erets](../../strongs/h/h776.md) which I [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md); and I ['āmar](../../strongs/h/h559.md), I will ['owlam](../../strongs/h/h5769.md) [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) with you.

<a name="judges_2_2"></a>Judges 2:2

And ye shall [karath](../../strongs/h/h3772.md) no [bĕriyth](../../strongs/h/h1285.md) with the [yashab](../../strongs/h/h3427.md) of this ['erets](../../strongs/h/h776.md); ye shall [nāṯaṣ](../../strongs/h/h5422.md) their [mizbeach](../../strongs/h/h4196.md): but ye have not [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md): why have ye ['asah](../../strongs/h/h6213.md) this?

<a name="judges_2_3"></a>Judges 2:3

Wherefore I also ['āmar](../../strongs/h/h559.md), I will not [gāraš](../../strongs/h/h1644.md) them from [paniym](../../strongs/h/h6440.md) you; but they shall be as thorns in your [ṣaḏ](../../strongs/h/h6654.md), and their ['Elohiym](../../strongs/h/h430.md) shall be a [mowqesh](../../strongs/h/h4170.md) unto you.

<a name="judges_2_4"></a>Judges 2:4

And it came to pass, when the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that the ['am](../../strongs/h/h5971.md) [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md).

<a name="judges_2_5"></a>Judges 2:5

And they [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that [maqowm](../../strongs/h/h4725.md) [Bōḵîm](../../strongs/h/h1066.md): and they [zabach](../../strongs/h/h2076.md) there unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_2_6"></a>Judges 2:6

And when [Yᵊhôšûaʿ](../../strongs/h/h3091.md) had let the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) unto his [nachalah](../../strongs/h/h5159.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md).

<a name="judges_2_7"></a>Judges 2:7

And the ['am](../../strongs/h/h5971.md) ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md), and all the [yowm](../../strongs/h/h3117.md) of the [zāqēn](../../strongs/h/h2205.md) that ['arak](../../strongs/h/h748.md) [yowm](../../strongs/h/h3117.md) ['aḥar](../../strongs/h/h310.md) [Yᵊhôšûaʿ](../../strongs/h/h3091.md), who had [ra'ah](../../strongs/h/h7200.md) all the [gadowl](../../strongs/h/h1419.md) [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md), that he ['asah](../../strongs/h/h6213.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_2_8"></a>Judges 2:8

And [Yᵊhôšûaʿ](../../strongs/h/h3091.md) the [ben](../../strongs/h/h1121.md) of [Nûn](../../strongs/h/h5126.md), the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), [muwth](../../strongs/h/h4191.md), being a [mē'â](../../strongs/h/h3967.md) and [ʿeśer](../../strongs/h/h6235.md) [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md).

<a name="judges_2_9"></a>Judges 2:9

And they [qāḇar](../../strongs/h/h6912.md) him in the [gᵊḇûl](../../strongs/h/h1366.md) of his [nachalah](../../strongs/h/h5159.md) in [Timnaṯ-Ḥeres](../../strongs/h/h8556.md), in the [har](../../strongs/h/h2022.md) of ['Ep̄rayim](../../strongs/h/h669.md), on the [ṣāp̄ôn](../../strongs/h/h6828.md) of the [har](../../strongs/h/h2022.md) [GaʿAš](../../strongs/h/h1608.md).

<a name="judges_2_10"></a>Judges 2:10

And also all that [dôr](../../strongs/h/h1755.md) were ['āsap̄](../../strongs/h/h622.md) unto their ['ab](../../strongs/h/h1.md): and there [quwm](../../strongs/h/h6965.md) ['aḥēr](../../strongs/h/h312.md) [dôr](../../strongs/h/h1755.md) ['aḥar](../../strongs/h/h310.md) them, which [yada'](../../strongs/h/h3045.md) not [Yĕhovah](../../strongs/h/h3068.md), nor yet the [ma'aseh](../../strongs/h/h4639.md) which he had ['asah](../../strongs/h/h6213.md) for [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_2_11"></a>Judges 2:11

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md):

<a name="judges_2_12"></a>Judges 2:12

And they ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), which [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), of the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) that were [cabiyb](../../strongs/h/h5439.md) them, and [shachah](../../strongs/h/h7812.md) themselves unto them, and provoked [Yĕhovah](../../strongs/h/h3068.md) to [kāʿas](../../strongs/h/h3707.md).

<a name="judges_2_13"></a>Judges 2:13

And they ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md) and [ʿAštārōṯ](../../strongs/h/h6252.md).

<a name="judges_2_14"></a>Judges 2:14

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [šāsâ](../../strongs/h/h8154.md) that [šāsas](../../strongs/h/h8155.md) them, and he [māḵar](../../strongs/h/h4376.md) them into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md) [cabiyb](../../strongs/h/h5439.md), so that they [yakol](../../strongs/h/h3201.md) not any [ʿôḏ](../../strongs/h/h5750.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md).

<a name="judges_2_15"></a>Judges 2:15

Whithersoever they [yāṣā'](../../strongs/h/h3318.md), the [yad](../../strongs/h/h3027.md) of [Yĕhovah](../../strongs/h/h3068.md) was against them for [ra'](../../strongs/h/h7451.md), as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md), and as [Yĕhovah](../../strongs/h/h3068.md) had [shaba'](../../strongs/h/h7650.md) unto them: and they were [me'od](../../strongs/h/h3966.md) [yāṣar](../../strongs/h/h3334.md).

<a name="judges_2_16"></a>Judges 2:16

Nevertheless [Yĕhovah](../../strongs/h/h3068.md) [quwm](../../strongs/h/h6965.md) [shaphat](../../strongs/h/h8199.md), which [yasha'](../../strongs/h/h3467.md) them out of the [yad](../../strongs/h/h3027.md) of those that [šāsâ](../../strongs/h/h8154.md) them.

<a name="judges_2_17"></a>Judges 2:17

And yet they would not [shama'](../../strongs/h/h8085.md) unto their [shaphat](../../strongs/h/h8199.md), but they went a [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) themselves unto them: they [cuwr](../../strongs/h/h5493.md) [mahēr](../../strongs/h/h4118.md) out of the [derek](../../strongs/h/h1870.md) which their ['ab](../../strongs/h/h1.md) [halak](../../strongs/h/h1980.md), [shama'](../../strongs/h/h8085.md) the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md); but they ['asah](../../strongs/h/h6213.md) not so.

<a name="judges_2_18"></a>Judges 2:18

And when [Yĕhovah](../../strongs/h/h3068.md) [quwm](../../strongs/h/h6965.md) them up [shaphat](../../strongs/h/h8199.md), then [Yĕhovah](../../strongs/h/h3068.md) was with the [shaphat](../../strongs/h/h8199.md), and [yasha'](../../strongs/h/h3467.md) them out of the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md) all the [yowm](../../strongs/h/h3117.md) of the [shaphat](../../strongs/h/h8199.md): for it [nacham](../../strongs/h/h5162.md) [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) of their [nᵊ'āqâ](../../strongs/h/h5009.md) by reason of them that [lāḥaṣ](../../strongs/h/h3905.md) them and [dāḥaq](../../strongs/h/h1766.md) them.

<a name="judges_2_19"></a>Judges 2:19

And it came to pass, when the [shaphat](../../strongs/h/h8199.md) was [maveth](../../strongs/h/h4194.md), that they [shuwb](../../strongs/h/h7725.md), and [shachath](../../strongs/h/h7843.md) themselves more than their ['ab](../../strongs/h/h1.md), in [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to ['abad](../../strongs/h/h5647.md) them, and to [shachah](../../strongs/h/h7812.md) unto them; they [naphal](../../strongs/h/h5307.md) not from their own [maʿălāl](../../strongs/h/h4611.md), nor from their [qāšê](../../strongs/h/h7186.md) [derek](../../strongs/h/h1870.md).

<a name="judges_2_20"></a>Judges 2:20

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md); and he ['āmar](../../strongs/h/h559.md), Because that this [gowy](../../strongs/h/h1471.md) hath ['abar](../../strongs/h/h5674.md) my [bĕriyth](../../strongs/h/h1285.md) which I [tsavah](../../strongs/h/h6680.md) their ['ab](../../strongs/h/h1.md), and have not [shama'](../../strongs/h/h8085.md) unto my [qowl](../../strongs/h/h6963.md);

<a name="judges_2_21"></a>Judges 2:21

I also will not [yāsap̄](../../strongs/h/h3254.md) [yarash](../../strongs/h/h3423.md) ['iysh](../../strongs/h/h376.md) from [paniym](../../strongs/h/h6440.md) them of the [gowy](../../strongs/h/h1471.md) which [Yᵊhôšûaʿ](../../strongs/h/h3091.md) ['azab](../../strongs/h/h5800.md) when he [muwth](../../strongs/h/h4191.md):

<a name="judges_2_22"></a>Judges 2:22

That through them I may [nāsâ](../../strongs/h/h5254.md) [Yisra'el](../../strongs/h/h3478.md), whether they will [shamar](../../strongs/h/h8104.md) the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md) to [yālaḵ](../../strongs/h/h3212.md) therein, as their ['ab](../../strongs/h/h1.md) did [shamar](../../strongs/h/h8104.md) it, or not.

<a name="judges_2_23"></a>Judges 2:23

Therefore [Yĕhovah](../../strongs/h/h3068.md) [yānaḥ](../../strongs/h/h3240.md) those [gowy](../../strongs/h/h1471.md), [biltî](../../strongs/h/h1115.md) driving them [yarash](../../strongs/h/h3423.md) [mahēr](../../strongs/h/h4118.md); neither [nathan](../../strongs/h/h5414.md) he them into the [yad](../../strongs/h/h3027.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 1](judges_1.md) - [Judges 3](judges_3.md)