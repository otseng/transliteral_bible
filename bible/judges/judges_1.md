# [Judges 1](https://www.blueletterbible.org/kjv/judges/1)

<a name="judges_1_1"></a>Judges 1:1

Now ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Yᵊhôšûaʿ](../../strongs/h/h3091.md) it came to pass, that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [sha'al](../../strongs/h/h7592.md) [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Who shall [ʿālâ](../../strongs/h/h5927.md) for us against the [Kᵊnaʿănî](../../strongs/h/h3669.md) [tᵊḥillâ](../../strongs/h/h8462.md), to [lāḥam](../../strongs/h/h3898.md) against them?

<a name="judges_1_2"></a>Judges 1:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [Yehuwdah](../../strongs/h/h3063.md) shall [ʿālâ](../../strongs/h/h5927.md): behold, I have [nathan](../../strongs/h/h5414.md) the ['erets](../../strongs/h/h776.md) into his [yad](../../strongs/h/h3027.md).

<a name="judges_1_3"></a>Judges 1:3

And [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md) unto [Šimʿôn](../../strongs/h/h8095.md) his ['ach](../../strongs/h/h251.md), [ʿālâ](../../strongs/h/h5927.md) with me into my [gôrāl](../../strongs/h/h1486.md), that we may [lāḥam](../../strongs/h/h3898.md) against the [Kᵊnaʿănî](../../strongs/h/h3669.md); and I likewise will [halak](../../strongs/h/h1980.md) with thee into thy [gôrāl](../../strongs/h/h1486.md). So [Šimʿôn](../../strongs/h/h8095.md) [yālaḵ](../../strongs/h/h3212.md) with him.

<a name="judges_1_4"></a>Judges 1:4

And [Yehuwdah](../../strongs/h/h3063.md) [ʿālâ](../../strongs/h/h5927.md); and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) and the [Pᵊrizzî](../../strongs/h/h6522.md) into their [yad](../../strongs/h/h3027.md): and they [nakah](../../strongs/h/h5221.md) of them in [Bezeq](../../strongs/h/h966.md) [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_1_5"></a>Judges 1:5

And they [māṣā'](../../strongs/h/h4672.md) ['Ăḏōnî-Ḇezeq](../../strongs/h/h137.md) in [Bezeq](../../strongs/h/h966.md): and they [lāḥam](../../strongs/h/h3898.md) against him, and they [nakah](../../strongs/h/h5221.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) and the [Pᵊrizzî](../../strongs/h/h6522.md).

<a name="judges_1_6"></a>Judges 1:6

But ['Ăḏōnî-Ḇezeq](../../strongs/h/h137.md) [nûs](../../strongs/h/h5127.md); and they [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) him, and ['āḥaz](../../strongs/h/h270.md) him, and [qāṣaṣ](../../strongs/h/h7112.md) his [bōhen](../../strongs/h/h931.md) [yad](../../strongs/h/h3027.md) and his [regel](../../strongs/h/h7272.md).

<a name="judges_1_7"></a>Judges 1:7

And ['Ăḏōnî-Ḇezeq](../../strongs/h/h137.md) ['āmar](../../strongs/h/h559.md), Threescore and [šiḇʿîm](../../strongs/h/h7657.md) [melek](../../strongs/h/h4428.md), having their [bōhen](../../strongs/h/h931.md) [yad](../../strongs/h/h3027.md) and their great [regel](../../strongs/h/h7272.md) cut [qāṣaṣ](../../strongs/h/h7112.md), [lāqaṭ](../../strongs/h/h3950.md) their meat under my [šulḥān](../../strongs/h/h7979.md): as I have ['asah](../../strongs/h/h6213.md), so ['Elohiym](../../strongs/h/h430.md) hath [shalam](../../strongs/h/h7999.md) me. And they [bow'](../../strongs/h/h935.md) him to [Yĕruwshalaim](../../strongs/h/h3389.md), and there he [muwth](../../strongs/h/h4191.md).

<a name="judges_1_8"></a>Judges 1:8

Now the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) had [lāḥam](../../strongs/h/h3898.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and had [lāḵaḏ](../../strongs/h/h3920.md) it, and [nakah](../../strongs/h/h5221.md) it with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [shalach](../../strongs/h/h7971.md) the [ʿîr](../../strongs/h/h5892.md) on ['esh](../../strongs/h/h784.md).

<a name="judges_1_9"></a>Judges 1:9

And ['aḥar](../../strongs/h/h310.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [yarad](../../strongs/h/h3381.md) to [lāḥam](../../strongs/h/h3898.md) against the [Kᵊnaʿănî](../../strongs/h/h3669.md), that [yashab](../../strongs/h/h3427.md) in the [har](../../strongs/h/h2022.md), and in the [neḡeḇ](../../strongs/h/h5045.md), and in the [šᵊp̄ēlâ](../../strongs/h/h8219.md).

<a name="judges_1_10"></a>Judges 1:10

And [Yehuwdah](../../strongs/h/h3063.md) [yālaḵ](../../strongs/h/h3212.md) against the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) in [Ḥeḇrôn](../../strongs/h/h2275.md): (now the [shem](../../strongs/h/h8034.md) of [Ḥeḇrôn](../../strongs/h/h2275.md) [paniym](../../strongs/h/h6440.md) was [qiryaṯ 'arbaʿ](../../strongs/h/h7153.md):) and they [nakah](../../strongs/h/h5221.md) [šēšay](../../strongs/h/h8344.md), and ['Ăḥîmān](../../strongs/h/h289.md), and [Talmay](../../strongs/h/h8526.md).

<a name="judges_1_11"></a>Judges 1:11

And from thence he [yālaḵ](../../strongs/h/h3212.md) against the [yashab](../../strongs/h/h3427.md) of [dᵊḇîr](../../strongs/h/h1688.md): and the [shem](../../strongs/h/h8034.md) of [dᵊḇîr](../../strongs/h/h1688.md) [paniym](../../strongs/h/h6440.md) was [Qiryaṯ-Sēnê](../../strongs/h/h7158.md):

<a name="judges_1_12"></a>Judges 1:12

And [Kālēḇ](../../strongs/h/h3612.md) ['āmar](../../strongs/h/h559.md), He that [nakah](../../strongs/h/h5221.md) [Qiryaṯ-Sēnê](../../strongs/h/h7158.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, to him will I [nathan](../../strongs/h/h5414.md) [ʿAḵsâ](../../strongs/h/h5915.md) my [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md).

<a name="judges_1_13"></a>Judges 1:13

And [ʿĀṯnî'Ēl](../../strongs/h/h6274.md) the [ben](../../strongs/h/h1121.md) of [Qᵊnaz](../../strongs/h/h7073.md), [Kālēḇ](../../strongs/h/h3612.md) [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md), [lāḵaḏ](../../strongs/h/h3920.md) it: and he [nathan](../../strongs/h/h5414.md) him [ʿAḵsâ](../../strongs/h/h5915.md) his [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md).

<a name="judges_1_14"></a>Judges 1:14

And it came to pass, when she [bow'](../../strongs/h/h935.md) to him, that she [sûṯ](../../strongs/h/h5496.md) him to [sha'al](../../strongs/h/h7592.md) of her ['ab](../../strongs/h/h1.md) a [sadeh](../../strongs/h/h7704.md): and she [ṣānaḥ](../../strongs/h/h6795.md) from off her [chamowr](../../strongs/h/h2543.md); and [Kālēḇ](../../strongs/h/h3612.md) ['āmar](../../strongs/h/h559.md) unto her, What wilt thou?

<a name="judges_1_15"></a>Judges 1:15

And she ['āmar](../../strongs/h/h559.md) unto him, [yāhaḇ](../../strongs/h/h3051.md) me a [bĕrakah](../../strongs/h/h1293.md): for thou hast [nathan](../../strongs/h/h5414.md) me a [neḡeḇ](../../strongs/h/h5045.md) ['erets](../../strongs/h/h776.md); [nathan](../../strongs/h/h5414.md) me also [gullâ](../../strongs/h/h1543.md) of [mayim](../../strongs/h/h4325.md). And [Kālēḇ](../../strongs/h/h3612.md) [nathan](../../strongs/h/h5414.md) her the [ʿillî](../../strongs/h/h5942.md) [gullâ](../../strongs/h/h1543.md) and the [taḥtî](../../strongs/h/h8482.md) [gullâ](../../strongs/h/h1543.md).

<a name="judges_1_16"></a>Judges 1:16

And the [ben](../../strongs/h/h1121.md) of the [Qênî](../../strongs/h/h7017.md), [Mōshe](../../strongs/h/h4872.md) father in [ḥāṯan](../../strongs/h/h2859.md), [ʿālâ](../../strongs/h/h5927.md) out of the [ʿîr](../../strongs/h/h5892.md) of palm [tāmār](../../strongs/h/h8558.md) [ʿîr hatmārîm](../../strongs/h/h5899.md) with the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) into the [midbar](../../strongs/h/h4057.md) of [Yehuwdah](../../strongs/h/h3063.md), which lieth in the [neḡeḇ](../../strongs/h/h5045.md) of [ʿĂrāḏ](../../strongs/h/h6166.md); and they [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) ['ēṯ](../../strongs/h/h854.md) the ['am](../../strongs/h/h5971.md).

<a name="judges_1_17"></a>Judges 1:17

And [Yehuwdah](../../strongs/h/h3063.md) [yālaḵ](../../strongs/h/h3212.md) with [Šimʿôn](../../strongs/h/h8095.md) his ['ach](../../strongs/h/h251.md), and they [nakah](../../strongs/h/h5221.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) [Ṣᵊp̄Āṯ](../../strongs/h/h6857.md), and [ḥāram](../../strongs/h/h2763.md) it. And the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) was [qara'](../../strongs/h/h7121.md) [Ḥārmâ](../../strongs/h/h2767.md).

<a name="judges_1_18"></a>Judges 1:18

Also [Yehuwdah](../../strongs/h/h3063.md) [lāḵaḏ](../../strongs/h/h3920.md) [ʿAzzâ](../../strongs/h/h5804.md) with the [gᵊḇûl](../../strongs/h/h1366.md) thereof, and ['Ašqᵊlôn](../../strongs/h/h831.md) with the [gᵊḇûl](../../strongs/h/h1366.md) thereof, and [ʿEqrôn](../../strongs/h/h6138.md) with the [gᵊḇûl](../../strongs/h/h1366.md) thereof.

<a name="judges_1_19"></a>Judges 1:19

And [Yĕhovah](../../strongs/h/h3068.md) was with [Yehuwdah](../../strongs/h/h3063.md); and he [yarash](../../strongs/h/h3423.md) the inhabitants of the [har](../../strongs/h/h2022.md); but could [lō'](../../strongs/h/h3808.md) [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of the [ʿēmeq](../../strongs/h/h6010.md), because they had [reḵeḇ](../../strongs/h/h7393.md) of [barzel](../../strongs/h/h1270.md).

<a name="judges_1_20"></a>Judges 1:20

And they [nathan](../../strongs/h/h5414.md) [Ḥeḇrôn](../../strongs/h/h2275.md) unto [Kālēḇ](../../strongs/h/h3612.md), as [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md): and he [yarash](../../strongs/h/h3423.md) thence the [šālôš](../../strongs/h/h7969.md) [ben](../../strongs/h/h1121.md) of [ʿĂnāq](../../strongs/h/h6061.md).

<a name="judges_1_21"></a>Judges 1:21

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) did not [yarash](../../strongs/h/h3423.md) the [Yᵊḇûsî](../../strongs/h/h2983.md) that [yashab](../../strongs/h/h3427.md) [Yĕruwshalaim](../../strongs/h/h3389.md); but the [Yᵊḇûsî](../../strongs/h/h2983.md) [yashab](../../strongs/h/h3427.md) with the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="judges_1_22"></a>Judges 1:22

And the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md), they also [ʿālâ](../../strongs/h/h5927.md) against [Bêṯ-'ēl](../../strongs/h/h1008.md): and [Yĕhovah](../../strongs/h/h3068.md) was with them.

<a name="judges_1_23"></a>Judges 1:23

And the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md) sent to [tûr](../../strongs/h/h8446.md) [Bêṯ-'ēl](../../strongs/h/h1008.md). (Now the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) [paniym](../../strongs/h/h6440.md) was [Lûz](../../strongs/h/h3870.md).)

<a name="judges_1_24"></a>Judges 1:24

And the [shamar](../../strongs/h/h8104.md) [ra'ah](../../strongs/h/h7200.md) an ['iysh](../../strongs/h/h376.md) [yāṣā'](../../strongs/h/h3318.md) out of the [ʿîr](../../strongs/h/h5892.md), and they ['āmar](../../strongs/h/h559.md) unto him, [ra'ah](../../strongs/h/h7200.md) us, we pray thee, the [māḇô'](../../strongs/h/h3996.md) into the [ʿîr](../../strongs/h/h5892.md), and we will ['asah](../../strongs/h/h6213.md) thee [checed](../../strongs/h/h2617.md).

<a name="judges_1_25"></a>Judges 1:25

And when he [ra'ah](../../strongs/h/h7200.md) them the [māḇô'](../../strongs/h/h3996.md) into the [ʿîr](../../strongs/h/h5892.md), they [nakah](../../strongs/h/h5221.md) the [ʿîr](../../strongs/h/h5892.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); but they let [shalach](../../strongs/h/h7971.md) the ['iysh](../../strongs/h/h376.md) and all his [mišpāḥâ](../../strongs/h/h4940.md).

<a name="judges_1_26"></a>Judges 1:26

And the ['iysh](../../strongs/h/h376.md) [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of the [Ḥitî](../../strongs/h/h2850.md), and [bānâ](../../strongs/h/h1129.md) a [ʿîr](../../strongs/h/h5892.md), and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) thereof [Lûz](../../strongs/h/h3870.md): which is the [shem](../../strongs/h/h8034.md) thereof unto this [yowm](../../strongs/h/h3117.md).

<a name="judges_1_27"></a>Judges 1:27

Neither did [Mᵊnaššê](../../strongs/h/h4519.md) [yarash](../../strongs/h/h3423.md) the inhabitants of [Bêṯ Šᵊ'Ān](../../strongs/h/h1052.md) and her [bath](../../strongs/h/h1323.md), nor [TaʿNāḵ](../../strongs/h/h8590.md) and her [bath](../../strongs/h/h1323.md), nor the [yashab](../../strongs/h/h3427.md) of [Dôr](../../strongs/h/h1756.md) and her [bath](../../strongs/h/h1323.md), nor the [yashab](../../strongs/h/h3427.md) of [YiḇlᵊʿĀm](../../strongs/h/h2991.md) and her [bath](../../strongs/h/h1323.md), nor the [yashab](../../strongs/h/h3427.md) of [Mᵊḡidôn](../../strongs/h/h4023.md) and her [bath](../../strongs/h/h1323.md): but the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yā'al](../../strongs/h/h2974.md) [yashab](../../strongs/h/h3427.md) in that ['erets](../../strongs/h/h776.md).

<a name="judges_1_28"></a>Judges 1:28

And it came to pass, when [Yisra'el](../../strongs/h/h3478.md) was [ḥāzaq](../../strongs/h/h2388.md), that they [śûm](../../strongs/h/h7760.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) to [mas](../../strongs/h/h4522.md), and did not [yarash](../../strongs/h/h3423.md) drive them [yarash](../../strongs/h/h3423.md).

<a name="judges_1_29"></a>Judges 1:29

Neither did ['Ep̄rayim](../../strongs/h/h669.md) [yarash](../../strongs/h/h3423.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md) that [yashab](../../strongs/h/h3427.md) in [Gezer](../../strongs/h/h1507.md); but the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yashab](../../strongs/h/h3427.md) in [Gezer](../../strongs/h/h1507.md) [qereḇ](../../strongs/h/h7130.md) them.

<a name="judges_1_30"></a>Judges 1:30

Neither did [Zᵊḇûlûn](../../strongs/h/h2074.md) [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of [Qiṭrôn](../../strongs/h/h7003.md), nor the [yashab](../../strongs/h/h3427.md) of [Nahălāl](../../strongs/h/h5096.md); but the [Kᵊnaʿănî](../../strongs/h/h3669.md) [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) them, and became [mas](../../strongs/h/h4522.md).

<a name="judges_1_31"></a>Judges 1:31

Neither did ['Āšēr](../../strongs/h/h836.md) [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of [ʿAkô](../../strongs/h/h5910.md), nor the [yashab](../../strongs/h/h3427.md) of [Ṣîḏôn](../../strongs/h/h6721.md), nor of ['Aḥlāḇ](../../strongs/h/h303.md), nor of ['Aḵzîḇ](../../strongs/h/h392.md), nor of [Ḥelbâ](../../strongs/h/h2462.md), nor of ['Ăp̄Ēq](../../strongs/h/h663.md), nor of [rᵊḥōḇ](../../strongs/h/h7340.md):

<a name="judges_1_32"></a>Judges 1:32

But the ['āšērî](../../strongs/h/h843.md) [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md): for they did not [yarash](../../strongs/h/h3423.md) them.

<a name="judges_1_33"></a>Judges 1:33

Neither did [Nap̄tālî](../../strongs/h/h5321.md) [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md), nor the [yashab](../../strongs/h/h3427.md) of [Bêṯ-ʿĂnāṯ](../../strongs/h/h1043.md); but he [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md): nevertheless the [yashab](../../strongs/h/h3427.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md) and of [Bêṯ-ʿĂnāṯ](../../strongs/h/h1043.md) became [mas](../../strongs/h/h4522.md) unto them.

<a name="judges_1_34"></a>Judges 1:34

And the ['Ĕmōrî](../../strongs/h/h567.md) [lāḥaṣ](../../strongs/h/h3905.md) the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) into the [har](../../strongs/h/h2022.md): for they would not [nathan](../../strongs/h/h5414.md) them to [yarad](../../strongs/h/h3381.md) to the [ʿēmeq](../../strongs/h/h6010.md):

<a name="judges_1_35"></a>Judges 1:35

But the ['Ĕmōrî](../../strongs/h/h567.md) [yā'al](../../strongs/h/h2974.md) [yashab](../../strongs/h/h3427.md) in [har](../../strongs/h/h2022.md) [Ḥeres](../../strongs/h/h2776.md) in ['Ayyālôn](../../strongs/h/h357.md), and in [ŠaʿAlḇîm](../../strongs/h/h8169.md): yet the [yad](../../strongs/h/h3027.md) of the [bayith](../../strongs/h/h1004.md) of [Yôsēp̄](../../strongs/h/h3130.md) [kabad](../../strongs/h/h3513.md), so that they became [mas](../../strongs/h/h4522.md).

<a name="judges_1_36"></a>Judges 1:36

And the [gᵊḇûl](../../strongs/h/h1366.md) of the ['Ĕmōrî](../../strongs/h/h567.md) was from the [maʿălê](../../strongs/h/h4608.md) to [MaʿĂlê ʿAqrabîm](../../strongs/h/h4610.md), from the [cela'](../../strongs/h/h5553.md), and [maʿal](../../strongs/h/h4605.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 2](judges_2.md)