# [Judges 11](https://www.blueletterbible.org/kjv/judges/11)

<a name="judges_11_1"></a>Judges 11:1

Now [Yip̄tāḥ](../../strongs/h/h3316.md) the [Gilʿāḏî](../../strongs/h/h1569.md) was a [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and he was the [ben](../../strongs/h/h1121.md) of an ['ishshah](../../strongs/h/h802.md) [zānâ](../../strongs/h/h2181.md): and [Gilʿāḏ](../../strongs/h/h1568.md) [yalad](../../strongs/h/h3205.md) [Yip̄tāḥ](../../strongs/h/h3316.md).

<a name="judges_11_2"></a>Judges 11:2

And [Gilʿāḏ](../../strongs/h/h1568.md) ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) him [ben](../../strongs/h/h1121.md); and his ['ishshah](../../strongs/h/h802.md) [ben](../../strongs/h/h1121.md) [gāḏal](../../strongs/h/h1431.md), and they [gāraš](../../strongs/h/h1644.md) [Yip̄tāḥ](../../strongs/h/h3316.md), and ['āmar](../../strongs/h/h559.md) unto him, Thou shalt not [nāḥal](../../strongs/h/h5157.md) in our ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md); for thou art the [ben](../../strongs/h/h1121.md) of an ['aḥēr](../../strongs/h/h312.md) ['ishshah](../../strongs/h/h802.md).

<a name="judges_11_3"></a>Judges 11:3

Then [Yip̄tāḥ](../../strongs/h/h3316.md) [bāraḥ](../../strongs/h/h1272.md) [paniym](../../strongs/h/h6440.md) his ['ach](../../strongs/h/h251.md), and [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Ṭôḇ](../../strongs/h/h2897.md): and there were [lāqaṭ](../../strongs/h/h3950.md) [reyq](../../strongs/h/h7386.md) ['enowsh](../../strongs/h/h582.md) to [Yip̄tāḥ](../../strongs/h/h3316.md), and [yāṣā'](../../strongs/h/h3318.md) with him.

<a name="judges_11_4"></a>Judges 11:4

And it came to pass in process of [yowm](../../strongs/h/h3117.md), that the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_11_5"></a>Judges 11:5

And it was so, that when the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md), the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md) [yālaḵ](../../strongs/h/h3212.md) to [laqach](../../strongs/h/h3947.md) [Yip̄tāḥ](../../strongs/h/h3316.md) out of the ['erets](../../strongs/h/h776.md) of [Ṭôḇ](../../strongs/h/h2897.md):

<a name="judges_11_6"></a>Judges 11:6

And they ['āmar](../../strongs/h/h559.md) unto [Yip̄tāḥ](../../strongs/h/h3316.md), [yālaḵ](../../strongs/h/h3212.md), and be our [qāṣîn](../../strongs/h/h7101.md), that we may [lāḥam](../../strongs/h/h3898.md) with the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="judges_11_7"></a>Judges 11:7

And [Yip̄tāḥ](../../strongs/h/h3316.md) ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md), Did not ye [sane'](../../strongs/h/h8130.md) me, and [gāraš](../../strongs/h/h1644.md) me out of my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md)? and why are ye [bow'](../../strongs/h/h935.md) unto me now when ye are in [tsarar](../../strongs/h/h6887.md)?

<a name="judges_11_8"></a>Judges 11:8

And the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md) ['āmar](../../strongs/h/h559.md) unto [Yip̄tāḥ](../../strongs/h/h3316.md), Therefore we [shuwb](../../strongs/h/h7725.md) to thee now, that thou mayest [halak](../../strongs/h/h1980.md) with us, and [lāḥam](../../strongs/h/h3898.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and be our [ro'sh](../../strongs/h/h7218.md) over all the [yashab](../../strongs/h/h3427.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="judges_11_9"></a>Judges 11:9

And [Yip̄tāḥ](../../strongs/h/h3316.md) ['āmar](../../strongs/h/h559.md) unto the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md), If ye [shuwb](../../strongs/h/h7725.md) me to [lāḥam](../../strongs/h/h3898.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them [paniym](../../strongs/h/h6440.md) me, shall I be your [ro'sh](../../strongs/h/h7218.md)?

<a name="judges_11_10"></a>Judges 11:10

And the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md) ['āmar](../../strongs/h/h559.md) unto [Yip̄tāḥ](../../strongs/h/h3316.md), [Yĕhovah](../../strongs/h/h3068.md) be [shama'](../../strongs/h/h8085.md) between us, if we ['asah](../../strongs/h/h6213.md) not so according to thy [dabar](../../strongs/h/h1697.md).

<a name="judges_11_11"></a>Judges 11:11

Then [Yip̄tāḥ](../../strongs/h/h3316.md) [yālaḵ](../../strongs/h/h3212.md) with the [zāqēn](../../strongs/h/h2205.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and the ['am](../../strongs/h/h5971.md) [śûm](../../strongs/h/h7760.md) him [ro'sh](../../strongs/h/h7218.md) and [qāṣîn](../../strongs/h/h7101.md) over them: and [Yip̄tāḥ](../../strongs/h/h3316.md) [dabar](../../strongs/h/h1696.md) all his [dabar](../../strongs/h/h1697.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) in [Miṣpâ](../../strongs/h/h4709.md).

<a name="judges_11_12"></a>Judges 11:12

And [Yip̄tāḥ](../../strongs/h/h3316.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), ['āmar](../../strongs/h/h559.md), What hast thou to do with me, that thou art [bow'](../../strongs/h/h935.md) against me to [lāḥam](../../strongs/h/h3898.md) in my ['erets](../../strongs/h/h776.md)?

<a name="judges_11_13"></a>Judges 11:13

And the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) of [Yip̄tāḥ](../../strongs/h/h3316.md), Because [Yisra'el](../../strongs/h/h3478.md) [laqach](../../strongs/h/h3947.md) my ['erets](../../strongs/h/h776.md), when they [ʿālâ](../../strongs/h/h5927.md) out of [Mitsrayim](../../strongs/h/h4714.md), from ['arnôn](../../strongs/h/h769.md) even unto [Yabōq](../../strongs/h/h2999.md), and unto [Yardēn](../../strongs/h/h3383.md): now therefore [shuwb](../../strongs/h/h7725.md) those again [shalowm](../../strongs/h/h7965.md).

<a name="judges_11_14"></a>Judges 11:14

And [Yip̄tāḥ](../../strongs/h/h3316.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) [yāsap̄](../../strongs/h/h3254.md) unto the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md):

<a name="judges_11_15"></a>Judges 11:15

And ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yip̄tāḥ](../../strongs/h/h3316.md), [Yisra'el](../../strongs/h/h3478.md) [laqach](../../strongs/h/h3947.md) not the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), nor the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md):

<a name="judges_11_16"></a>Judges 11:16

But when [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) from [Mitsrayim](../../strongs/h/h4714.md), and [yālaḵ](../../strongs/h/h3212.md) through the [midbar](../../strongs/h/h4057.md) unto the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md), and [bow'](../../strongs/h/h935.md) to [Qāḏēš](../../strongs/h/h6946.md);

<a name="judges_11_17"></a>Judges 11:17

Then [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md), ['āmar](../../strongs/h/h559.md), Let me, I pray thee, ['abar](../../strongs/h/h5674.md) thy ['erets](../../strongs/h/h776.md): but the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md) would not [shama'](../../strongs/h/h8085.md) thereto. And in like manner they [shalach](../../strongs/h/h7971.md) unto the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md): but he ['āḇâ](../../strongs/h/h14.md) not: and [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in [Qāḏēš](../../strongs/h/h6946.md).

<a name="judges_11_18"></a>Judges 11:18

Then they [yālaḵ](../../strongs/h/h3212.md) through the [midbar](../../strongs/h/h4057.md), and [cabab](../../strongs/h/h5437.md) the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md), and the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), and [bow'](../../strongs/h/h935.md) by the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md) of the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), and [ḥānâ](../../strongs/h/h2583.md) on the other [ʿēḇer](../../strongs/h/h5676.md) of ['arnôn](../../strongs/h/h769.md), but [bow'](../../strongs/h/h935.md) not within the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md): for ['arnôn](../../strongs/h/h769.md) was the [gᵊḇûl](../../strongs/h/h1366.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="judges_11_19"></a>Judges 11:19

And [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto [Sîḥôn](../../strongs/h/h5511.md) [melek](../../strongs/h/h4428.md) of the ['Ĕmōrî](../../strongs/h/h567.md), the [melek](../../strongs/h/h4428.md) of [Hešbôn](../../strongs/h/h2809.md); and [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto him, Let us ['abar](../../strongs/h/h5674.md), we pray thee, through thy ['erets](../../strongs/h/h776.md) [ʿaḏ](../../strongs/h/h5704.md) my [maqowm](../../strongs/h/h4725.md).

<a name="judges_11_20"></a>Judges 11:20

But [Sîḥôn](../../strongs/h/h5511.md) ['aman](../../strongs/h/h539.md) not [Yisra'el](../../strongs/h/h3478.md) to ['abar](../../strongs/h/h5674.md) through his [gᵊḇûl](../../strongs/h/h1366.md): but [Sîḥôn](../../strongs/h/h5511.md) ['āsap̄](../../strongs/h/h622.md) all his ['am](../../strongs/h/h5971.md) ['āsap̄](../../strongs/h/h622.md), and [ḥānâ](../../strongs/h/h2583.md) in [Yahaṣ](../../strongs/h/h3096.md), and [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_11_21"></a>Judges 11:21

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) [Sîḥôn](../../strongs/h/h5511.md) and all his ['am](../../strongs/h/h5971.md) into the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md), and they [nakah](../../strongs/h/h5221.md) them: so [Yisra'el](../../strongs/h/h3478.md) [yarash](../../strongs/h/h3423.md) all the ['erets](../../strongs/h/h776.md) of the ['Ĕmōrî](../../strongs/h/h567.md), the [yashab](../../strongs/h/h3427.md) of that ['erets](../../strongs/h/h776.md).

<a name="judges_11_22"></a>Judges 11:22

And they [yarash](../../strongs/h/h3423.md) all the [gᵊḇûl](../../strongs/h/h1366.md) of the ['Ĕmōrî](../../strongs/h/h567.md), from ['arnôn](../../strongs/h/h769.md) even unto [Yabōq](../../strongs/h/h2999.md), and from the [midbar](../../strongs/h/h4057.md) even unto [Yardēn](../../strongs/h/h3383.md).

<a name="judges_11_23"></a>Judges 11:23

So now [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) hath [yarash](../../strongs/h/h3423.md) the ['Ĕmōrî](../../strongs/h/h567.md) from [paniym](../../strongs/h/h6440.md) his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and shouldest thou [yarash](../../strongs/h/h3423.md) it?

<a name="judges_11_24"></a>Judges 11:24

Wilt not thou [yarash](../../strongs/h/h3423.md) that which [kᵊmôš](../../strongs/h/h3645.md) thy ['Elohiym](../../strongs/h/h430.md) giveth thee to [yarash](../../strongs/h/h3423.md)? So whomsoever [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall [yarash](../../strongs/h/h3423.md) from [paniym](../../strongs/h/h6440.md) us, them will we [yarash](../../strongs/h/h3423.md).

<a name="judges_11_25"></a>Judges 11:25

And now art thou any thing [towb](../../strongs/h/h2896.md) than [Bālāq](../../strongs/h/h1111.md) the [ben](../../strongs/h/h1121.md) of [Tṣipôr](../../strongs/h/h6834.md), [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md)? did he [riyb](../../strongs/h/h7378.md) [riyb](../../strongs/h/h7378.md) against [Yisra'el](../../strongs/h/h3478.md), or did he [lāḥam](../../strongs/h/h3898.md) [lāḥam](../../strongs/h/h3898.md) against them,

<a name="judges_11_26"></a>Judges 11:26

While [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) in [Hešbôn](../../strongs/h/h2809.md) and her [bath](../../strongs/h/h1323.md), and in [ʿĂrôʿēr](../../strongs/h/h6177.md) and her [bath](../../strongs/h/h1323.md), and in all the [ʿîr](../../strongs/h/h5892.md) that be along by the [yad](../../strongs/h/h3027.md) of ['arnôn](../../strongs/h/h769.md), [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [šānâ](../../strongs/h/h8141.md)? why therefore did ye not [natsal](../../strongs/h/h5337.md) them within that [ʿēṯ](../../strongs/h/h6256.md)?

<a name="judges_11_27"></a>Judges 11:27

Wherefore I have not [chata'](../../strongs/h/h2398.md) against thee, but thou ['asah](../../strongs/h/h6213.md) ['ēṯ](../../strongs/h/h853.md) me [ra'](../../strongs/h/h7451.md) to [lāḥam](../../strongs/h/h3898.md) against me: [Yĕhovah](../../strongs/h/h3068.md) the [shaphat](../../strongs/h/h8199.md) be [shaphat](../../strongs/h/h8199.md) this [yowm](../../strongs/h/h3117.md) between the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="judges_11_28"></a>Judges 11:28

Howbeit the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [shama'](../../strongs/h/h8085.md) not unto the [dabar](../../strongs/h/h1697.md) of [Yip̄tāḥ](../../strongs/h/h3316.md) which he [shalach](../../strongs/h/h7971.md) him.

<a name="judges_11_29"></a>Judges 11:29

Then the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) came upon [Yip̄tāḥ](../../strongs/h/h3316.md), and he ['abar](../../strongs/h/h5674.md) [Gilʿāḏ](../../strongs/h/h1568.md), and [Mᵊnaššê](../../strongs/h/h4519.md), and ['abar](../../strongs/h/h5674.md) [Miṣpê](../../strongs/h/h4708.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and from [Miṣpê](../../strongs/h/h4708.md) of [Gilʿāḏ](../../strongs/h/h1568.md) he ['abar](../../strongs/h/h5674.md) unto the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="judges_11_30"></a>Judges 11:30

And [Yip̄tāḥ](../../strongs/h/h3316.md) [nāḏar](../../strongs/h/h5087.md) a [neḏer](../../strongs/h/h5088.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), If thou shalt without [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) into mine [yad](../../strongs/h/h3027.md),

<a name="judges_11_31"></a>Judges 11:31

Then it shall be, that [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) of the [deleṯ](../../strongs/h/h1817.md) of my [bayith](../../strongs/h/h1004.md) to [qārā'](../../strongs/h/h7125.md) me, when I [shuwb](../../strongs/h/h7725.md) in [shalowm](../../strongs/h/h7965.md) from the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), shall surely be [Yĕhovah](../../strongs/h/h3068.md), and I will [ʿālâ](../../strongs/h/h5927.md) it for a [ʿōlâ](../../strongs/h/h5930.md).

<a name="judges_11_32"></a>Judges 11:32

So [Yip̄tāḥ](../../strongs/h/h3316.md) ['abar](../../strongs/h/h5674.md) unto the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) to [lāḥam](../../strongs/h/h3898.md) against them; and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them into his [yad](../../strongs/h/h3027.md).

<a name="judges_11_33"></a>Judges 11:33

And he [nakah](../../strongs/h/h5221.md) them from [ʿĂrôʿēr](../../strongs/h/h6177.md), even till thou [bow'](../../strongs/h/h935.md) to [Minnîṯ](../../strongs/h/h4511.md), even [ʿeśrîm](../../strongs/h/h6242.md) [ʿîr](../../strongs/h/h5892.md), and unto the ['āḇēl](../../strongs/h/h58.md) of the [kerem](../../strongs/h/h3754.md) ['Āḇēl Kᵊrāmîm](../../strongs/h/h64.md), with a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md). Thus the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) were [kānaʿ](../../strongs/h/h3665.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_11_34"></a>Judges 11:34

And [Yip̄tāḥ](../../strongs/h/h3316.md) [bow'](../../strongs/h/h935.md) to [Miṣpâ](../../strongs/h/h4709.md) unto his [bayith](../../strongs/h/h1004.md), and, behold, his [bath](../../strongs/h/h1323.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him with [tōp̄](../../strongs/h/h8596.md) and with [mᵊḥōlâ](../../strongs/h/h4246.md): and she was his only [yāḥîḏ](../../strongs/h/h3173.md); beside her he had neither [ben](../../strongs/h/h1121.md) ['av](../../strongs/h/h176.md) [bath](../../strongs/h/h1323.md).

<a name="judges_11_35"></a>Judges 11:35

And it came to pass, when he [ra'ah](../../strongs/h/h7200.md) her, that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md), and ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md), my [bath](../../strongs/h/h1323.md)! thou hast [kara'](../../strongs/h/h3766.md) [kara'](../../strongs/h/h3766.md) [kara'](../../strongs/h/h3766.md) me, and thou art one of them that [ʿāḵar](../../strongs/h/h5916.md) me: for I have [pāṣâ](../../strongs/h/h6475.md) my [peh](../../strongs/h/h6310.md) unto [Yĕhovah](../../strongs/h/h3068.md), and I [yakol](../../strongs/h/h3201.md) [shuwb](../../strongs/h/h7725.md).

<a name="judges_11_36"></a>Judges 11:36

And she ['āmar](../../strongs/h/h559.md) unto him, My ['ab](../../strongs/h/h1.md), if thou hast [pāṣâ](../../strongs/h/h6475.md) thy [peh](../../strongs/h/h6310.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['asah](../../strongs/h/h6213.md) to me according to that ['ăšer](../../strongs/h/h834.md) hath [yāṣā'](../../strongs/h/h3318.md) out of thy [peh](../../strongs/h/h6310.md); ['aḥar](../../strongs/h/h310.md) as [Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) [nᵊqāmâ](../../strongs/h/h5360.md) for thee of thine ['oyeb](../../strongs/h/h341.md), even of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="judges_11_37"></a>Judges 11:37

And she ['āmar](../../strongs/h/h559.md) unto her ['ab](../../strongs/h/h1.md), Let this [dabar](../../strongs/h/h1697.md) be ['asah](../../strongs/h/h6213.md) for me: let me [rāp̄â](../../strongs/h/h7503.md) [šᵊnayim](../../strongs/h/h8147.md) [ḥōḏeš](../../strongs/h/h2320.md), that I may go [yālaḵ](../../strongs/h/h3212.md) and [yarad](../../strongs/h/h3381.md) upon the [har](../../strongs/h/h2022.md), and [bāḵâ](../../strongs/h/h1058.md) my [bᵊṯûlîm](../../strongs/h/h1331.md), I and my [rēʿâ](../../strongs/h/h7464.md) [raʿyâ](../../strongs/h/h7474.md).

<a name="judges_11_38"></a>Judges 11:38

And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md). And he [shalach](../../strongs/h/h7971.md) her for [šᵊnayim](../../strongs/h/h8147.md) [ḥōḏeš](../../strongs/h/h2320.md): and she [yālaḵ](../../strongs/h/h3212.md) with her [rēʿâ](../../strongs/h/h7464.md), and [bāḵâ](../../strongs/h/h1058.md) her [bᵊṯûlîm](../../strongs/h/h1331.md) upon the [har](../../strongs/h/h2022.md).

<a name="judges_11_39"></a>Judges 11:39

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of [šᵊnayim](../../strongs/h/h8147.md) [ḥōḏeš](../../strongs/h/h2320.md), that she [shuwb](../../strongs/h/h7725.md) unto her ['ab](../../strongs/h/h1.md), who ['asah](../../strongs/h/h6213.md) with her according to his [neḏer](../../strongs/h/h5088.md) which he had [nāḏar](../../strongs/h/h5087.md): and she [yada'](../../strongs/h/h3045.md) no ['iysh](../../strongs/h/h376.md). And it was a [choq](../../strongs/h/h2706.md) in [Yisra'el](../../strongs/h/h3478.md),

<a name="judges_11_40"></a>Judges 11:40

That the [bath](../../strongs/h/h1323.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) to [tānâ](../../strongs/h/h8567.md) the [bath](../../strongs/h/h1323.md) of [Yip̄tāḥ](../../strongs/h/h3316.md) the [Gilʿāḏî](../../strongs/h/h1569.md) ['arbaʿ](../../strongs/h/h702.md) [yowm](../../strongs/h/h3117.md) in a [šānâ](../../strongs/h/h8141.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 10](judges_10.md) - [Judges 12](judges_12.md)