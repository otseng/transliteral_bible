# [Judges 9](https://www.blueletterbible.org/kjv/judges/9)

<a name="judges_9_1"></a>Judges 9:1

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) the [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md) [yālaḵ](../../strongs/h/h3212.md) to [Šᵊḵem](../../strongs/h/h7927.md) unto his ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md), and [dabar](../../strongs/h/h1696.md) with them, and with all the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of his ['em](../../strongs/h/h517.md) ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md),

<a name="judges_9_2"></a>Judges 9:2

[dabar](../../strongs/h/h1696.md), I pray you, in the ['ozen](../../strongs/h/h241.md) of all the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), Whether is [towb](../../strongs/h/h2896.md) for you, either that all the [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md), which are [šiḇʿîm](../../strongs/h/h7657.md) ['iysh](../../strongs/h/h376.md), [mashal](../../strongs/h/h4910.md) over you, or that ['echad](../../strongs/h/h259.md) [mashal](../../strongs/h/h4910.md) over you? [zakar](../../strongs/h/h2142.md) also that I am your ['etsem](../../strongs/h/h6106.md) and your [basar](../../strongs/h/h1320.md).

<a name="judges_9_3"></a>Judges 9:3

And his ['em](../../strongs/h/h517.md) ['ach](../../strongs/h/h251.md) [dabar](../../strongs/h/h1696.md) of him in the ['ozen](../../strongs/h/h241.md) of all the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md) all these [dabar](../../strongs/h/h1697.md): and their [leb](../../strongs/h/h3820.md) [natah](../../strongs/h/h5186.md) to ['aḥar](../../strongs/h/h310.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md); for they ['āmar](../../strongs/h/h559.md), He is our ['ach](../../strongs/h/h251.md).

<a name="judges_9_4"></a>Judges 9:4

And they [nathan](../../strongs/h/h5414.md) him [šiḇʿîm](../../strongs/h/h7657.md) pieces of [keceph](../../strongs/h/h3701.md) out of the [bayith](../../strongs/h/h1004.md) of [BaʿAl Bᵊrîṯ](../../strongs/h/h1170.md), wherewith ['Ăḇîmeleḵ](../../strongs/h/h40.md) [śāḵar](../../strongs/h/h7936.md) [reyq](../../strongs/h/h7386.md) and [pāḥaz](../../strongs/h/h6348.md) ['enowsh](../../strongs/h/h582.md), which [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md).

<a name="judges_9_5"></a>Judges 9:5

And he [bow'](../../strongs/h/h935.md) unto his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) at [ʿĀp̄Râ](../../strongs/h/h6084.md), and [harag](../../strongs/h/h2026.md) his ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md), being threescore and [šiḇʿîm](../../strongs/h/h7657.md) ['iysh](../../strongs/h/h376.md), upon ['echad](../../strongs/h/h259.md) ['eben](../../strongs/h/h68.md): notwithstanding yet [Yôṯām](../../strongs/h/h3147.md) the [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md) was [yāṯar](../../strongs/h/h3498.md); for he [chaba'](../../strongs/h/h2244.md) himself.

<a name="judges_9_6"></a>Judges 9:6

And all the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md) ['āsap̄](../../strongs/h/h622.md), and all the [bayith](../../strongs/h/h1004.md) of [Millô'](../../strongs/h/h4407.md) [Bêṯ Millô'](../../strongs/h/h1037.md), and [yālaḵ](../../strongs/h/h3212.md), and [mālaḵ](../../strongs/h/h4427.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md) [melek](../../strongs/h/h4428.md), by the ['ēlôn](../../strongs/h/h436.md) of the [nāṣaḇ](../../strongs/h/h5324.md) that was in [Šᵊḵem](../../strongs/h/h7927.md).

<a name="judges_9_7"></a>Judges 9:7

And when they [nāḡaḏ](../../strongs/h/h5046.md) it to [Yôṯām](../../strongs/h/h3147.md), he [yālaḵ](../../strongs/h/h3212.md) and ['amad](../../strongs/h/h5975.md) in the [ro'sh](../../strongs/h/h7218.md) of [har](../../strongs/h/h2022.md) [Gᵊrizzîm](../../strongs/h/h1630.md), and [nasa'](../../strongs/h/h5375.md) his [qowl](../../strongs/h/h6963.md), and [qara'](../../strongs/h/h7121.md), and ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md) unto me, ye [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), that ['Elohiym](../../strongs/h/h430.md) may [shama'](../../strongs/h/h8085.md) unto you.

<a name="judges_9_8"></a>Judges 9:8

The ['ets](../../strongs/h/h6086.md) [halak](../../strongs/h/h1980.md) [halak](../../strongs/h/h1980.md) on a time to [māšaḥ](../../strongs/h/h4886.md) a [melek](../../strongs/h/h4428.md) over them; and they ['āmar](../../strongs/h/h559.md) unto the [zayiṯ](../../strongs/h/h2132.md), [mālaḵ](../../strongs/h/h4427.md) thou over us.

<a name="judges_9_9"></a>Judges 9:9

But the [zayiṯ](../../strongs/h/h2132.md) ['āmar](../../strongs/h/h559.md) unto them, Should I [ḥāḏal](../../strongs/h/h2308.md) my [dešen](../../strongs/h/h1880.md), wherewith by me they [kabad](../../strongs/h/h3513.md) ['Elohiym](../../strongs/h/h430.md) and ['enowsh](../../strongs/h/h582.md), and [halak](../../strongs/h/h1980.md) to be [nûaʿ](../../strongs/h/h5128.md) over the ['ets](../../strongs/h/h6086.md)?

<a name="judges_9_10"></a>Judges 9:10

And the ['ets](../../strongs/h/h6086.md) ['āmar](../../strongs/h/h559.md) to the [tĕ'en](../../strongs/h/h8384.md), [yālaḵ](../../strongs/h/h3212.md) thou, and [mālaḵ](../../strongs/h/h4427.md) over us.

<a name="judges_9_11"></a>Judges 9:11

But the [tĕ'en](../../strongs/h/h8384.md) ['āmar](../../strongs/h/h559.md) unto them, Should I [ḥāḏal](../../strongs/h/h2308.md) my [mōṯēq](../../strongs/h/h4987.md), and my [towb](../../strongs/h/h2896.md) [tᵊnûḇâ](../../strongs/h/h8570.md), and [halak](../../strongs/h/h1980.md) to be [nûaʿ](../../strongs/h/h5128.md) over the ['ets](../../strongs/h/h6086.md)?

<a name="judges_9_12"></a>Judges 9:12

Then ['āmar](../../strongs/h/h559.md) the ['ets](../../strongs/h/h6086.md) unto the [gep̄en](../../strongs/h/h1612.md), [yālaḵ](../../strongs/h/h3212.md) thou, and [mālaḵ](../../strongs/h/h4427.md) over us.

<a name="judges_9_13"></a>Judges 9:13

And the [gep̄en](../../strongs/h/h1612.md) ['āmar](../../strongs/h/h559.md) unto them, Should I [ḥāḏal](../../strongs/h/h2308.md) my [tiyrowsh](../../strongs/h/h8492.md), which [samach](../../strongs/h/h8055.md) ['Elohiym](../../strongs/h/h430.md) and ['enowsh](../../strongs/h/h582.md), and [halak](../../strongs/h/h1980.md) to be [nûaʿ](../../strongs/h/h5128.md) over the ['ets](../../strongs/h/h6086.md)?

<a name="judges_9_14"></a>Judges 9:14

Then ['āmar](../../strongs/h/h559.md) all the ['ets](../../strongs/h/h6086.md) unto the ['āṭāḏ](../../strongs/h/h329.md), [yālaḵ](../../strongs/h/h3212.md) thou, and [mālaḵ](../../strongs/h/h4427.md) over us.

<a name="judges_9_15"></a>Judges 9:15

And the ['āṭāḏ](../../strongs/h/h329.md) ['āmar](../../strongs/h/h559.md) unto the ['ets](../../strongs/h/h6086.md), If in ['emeth](../../strongs/h/h571.md) ye [māšaḥ](../../strongs/h/h4886.md) me [melek](../../strongs/h/h4428.md) over you, then [bow'](../../strongs/h/h935.md) and put your [chacah](../../strongs/h/h2620.md) in my [ṣēl](../../strongs/h/h6738.md): and if not, let ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) of the ['āṭāḏ](../../strongs/h/h329.md), and ['akal](../../strongs/h/h398.md) the ['erez](../../strongs/h/h730.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="judges_9_16"></a>Judges 9:16

Now therefore, if ye have ['asah](../../strongs/h/h6213.md) ['emeth](../../strongs/h/h571.md) and [tamiym](../../strongs/h/h8549.md), in that ye have made ['Ăḇîmeleḵ](../../strongs/h/h40.md) [mālaḵ](../../strongs/h/h4427.md), and if ye have ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md) with [YᵊrubaʿAl](../../strongs/h/h3378.md) and his [bayith](../../strongs/h/h1004.md), and have ['asah](../../strongs/h/h6213.md) unto him according to the [gĕmwl](../../strongs/h/h1576.md) of his [yad](../../strongs/h/h3027.md);

<a name="judges_9_17"></a>Judges 9:17

(For my ['ab](../../strongs/h/h1.md) [lāḥam](../../strongs/h/h3898.md) for you, and [shalak](../../strongs/h/h7993.md) his [nephesh](../../strongs/h/h5315.md) [neḡeḏ](../../strongs/h/h5048.md), and [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of [Miḏyān](../../strongs/h/h4080.md):

<a name="judges_9_18"></a>Judges 9:18

And ye are [quwm](../../strongs/h/h6965.md) against my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) this [yowm](../../strongs/h/h3117.md), and have [harag](../../strongs/h/h2026.md) his [ben](../../strongs/h/h1121.md), threescore and [šiḇʿîm](../../strongs/h/h7657.md) ['iysh](../../strongs/h/h376.md), upon ['echad](../../strongs/h/h259.md) ['eben](../../strongs/h/h68.md), and have made ['Ăḇîmeleḵ](../../strongs/h/h40.md), the [ben](../../strongs/h/h1121.md) of his ['amah](../../strongs/h/h519.md), [mālaḵ](../../strongs/h/h4427.md) over the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), because he is your ['ach](../../strongs/h/h251.md);)

<a name="judges_9_19"></a>Judges 9:19

If ye then have ['asah](../../strongs/h/h6213.md) ['emeth](../../strongs/h/h571.md) and [tamiym](../../strongs/h/h8549.md) with [YᵊrubaʿAl](../../strongs/h/h3378.md) and with his [bayith](../../strongs/h/h1004.md) this [yowm](../../strongs/h/h3117.md), then [samach](../../strongs/h/h8055.md) ye in ['Ăḇîmeleḵ](../../strongs/h/h40.md), and let him also [samach](../../strongs/h/h8055.md) in you:

<a name="judges_9_20"></a>Judges 9:20

But if not, let ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) from ['Ăḇîmeleḵ](../../strongs/h/h40.md), and ['akal](../../strongs/h/h398.md) the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), and the [bayith](../../strongs/h/h1004.md) of [Millô'](../../strongs/h/h4407.md) H1037; and let ['esh](../../strongs/h/h784.md) [yāṣā'](../../strongs/h/h3318.md) from the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), and from the [bayith](../../strongs/h/h1004.md) of [Millô'](../../strongs/h/h4407.md) H1037, and ['akal](../../strongs/h/h398.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_9_21"></a>Judges 9:21

And [Yôṯām](../../strongs/h/h3147.md) [bāraḥ](../../strongs/h/h1272.md), and [nûs](../../strongs/h/h5127.md), and [yālaḵ](../../strongs/h/h3212.md) to [bᵊ'ēr](../../strongs/h/h876.md), and [yashab](../../strongs/h/h3427.md) there, for [paniym](../../strongs/h/h6440.md) of ['Ăḇîmeleḵ](../../strongs/h/h40.md) his ['ach](../../strongs/h/h251.md).

<a name="judges_9_22"></a>Judges 9:22

When ['Ăḇîmeleḵ](../../strongs/h/h40.md) had [śûr](../../strongs/h/h7786.md) [šālôš](../../strongs/h/h7969.md) [šānâ](../../strongs/h/h8141.md) over [Yisra'el](../../strongs/h/h3478.md),

<a name="judges_9_23"></a>Judges 9:23

Then ['Elohiym](../../strongs/h/h430.md) [shalach](../../strongs/h/h7971.md) a [ra'](../../strongs/h/h7451.md) [ruwach](../../strongs/h/h7307.md) between ['Ăḇîmeleḵ](../../strongs/h/h40.md) and the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md); and the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md) [bāḡaḏ](../../strongs/h/h898.md) with ['Ăḇîmeleḵ](../../strongs/h/h40.md):

<a name="judges_9_24"></a>Judges 9:24

That the [chamac](../../strongs/h/h2555.md) done to the [šiḇʿîm](../../strongs/h/h7657.md) [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md) might [bow'](../../strongs/h/h935.md), and their [dam](../../strongs/h/h1818.md) be [śûm](../../strongs/h/h7760.md) upon ['Ăḇîmeleḵ](../../strongs/h/h40.md) their ['ach](../../strongs/h/h251.md), which [harag](../../strongs/h/h2026.md) them; and upon the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), which [ḥāzaq](../../strongs/h/h2388.md) [yad](../../strongs/h/h3027.md) him in the [harag](../../strongs/h/h2026.md) of his ['ach](../../strongs/h/h251.md).

<a name="judges_9_25"></a>Judges 9:25

And the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md) [śûm](../../strongs/h/h7760.md) in ['arab](../../strongs/h/h693.md) for him in the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md), and they [gāzal](../../strongs/h/h1497.md) all that ['abar](../../strongs/h/h5674.md) along that [derek](../../strongs/h/h1870.md) by them: and it was [nāḡaḏ](../../strongs/h/h5046.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_9_26"></a>Judges 9:26

And [GaʿAl](../../strongs/h/h1603.md) the [ben](../../strongs/h/h1121.md) of [ʿEḇeḏ](../../strongs/h/h5651.md) [bow'](../../strongs/h/h935.md) with his ['ach](../../strongs/h/h251.md), and ['abar](../../strongs/h/h5674.md) to [Šᵊḵem](../../strongs/h/h7927.md): and the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md) put their [batach](../../strongs/h/h982.md) in him.

<a name="judges_9_27"></a>Judges 9:27

And they [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md), and [bāṣar](../../strongs/h/h1219.md) their [kerem](../../strongs/h/h3754.md), and [dāraḵ](../../strongs/h/h1869.md), and ['asah](../../strongs/h/h6213.md) [hillûl](../../strongs/h/h1974.md), and [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of their ['Elohiym](../../strongs/h/h430.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [qālal](../../strongs/h/h7043.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_9_28"></a>Judges 9:28

And [GaʿAl](../../strongs/h/h1603.md) the [ben](../../strongs/h/h1121.md) of [ʿEḇeḏ](../../strongs/h/h5651.md) ['āmar](../../strongs/h/h559.md), Who is ['Ăḇîmeleḵ](../../strongs/h/h40.md), and who is [Šᵊḵem](../../strongs/h/h7927.md), that we should ['abad](../../strongs/h/h5647.md) him? is not he the [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md)? and [Zᵊḇul](../../strongs/h/h2083.md) his [pāqîḏ](../../strongs/h/h6496.md)? ['abad](../../strongs/h/h5647.md) the ['enowsh](../../strongs/h/h582.md) of [ḥămôr](../../strongs/h/h2544.md) the ['ab](../../strongs/h/h1.md) of [Šᵊḵem](../../strongs/h/h7927.md): for why should we ['abad](../../strongs/h/h5647.md) him?

<a name="judges_9_29"></a>Judges 9:29

And this ['am](../../strongs/h/h5971.md) were [nathan](../../strongs/h/h5414.md) my [yad](../../strongs/h/h3027.md)! then would I [cuwr](../../strongs/h/h5493.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md). And he ['āmar](../../strongs/h/h559.md) to ['Ăḇîmeleḵ](../../strongs/h/h40.md), [rabah](../../strongs/h/h7235.md) thine [tsaba'](../../strongs/h/h6635.md), and [yāṣā'](../../strongs/h/h3318.md).

<a name="judges_9_30"></a>Judges 9:30

And when [Zᵊḇul](../../strongs/h/h2083.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md) [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [GaʿAl](../../strongs/h/h1603.md) the [ben](../../strongs/h/h1121.md) of [ʿEḇeḏ](../../strongs/h/h5651.md), his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md).

<a name="judges_9_31"></a>Judges 9:31

And he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto ['Ăḇîmeleḵ](../../strongs/h/h40.md) [tārmâ](../../strongs/h/h8649.md), ['āmar](../../strongs/h/h559.md), Behold, [GaʿAl](../../strongs/h/h1603.md) the [ben](../../strongs/h/h1121.md) of [ʿEḇeḏ](../../strongs/h/h5651.md) and his ['ach](../../strongs/h/h251.md) be [bow'](../../strongs/h/h935.md) to [Šᵊḵem](../../strongs/h/h7927.md); and, behold, they [ṣûr](../../strongs/h/h6696.md) the [ʿîr](../../strongs/h/h5892.md) against thee.

<a name="judges_9_32"></a>Judges 9:32

Now therefore [quwm](../../strongs/h/h6965.md) by [layil](../../strongs/h/h3915.md), thou and the ['am](../../strongs/h/h5971.md) that is with thee, and ['arab](../../strongs/h/h693.md) in the [sadeh](../../strongs/h/h7704.md):

<a name="judges_9_33"></a>Judges 9:33

And it shall be, that in the [boqer](../../strongs/h/h1242.md), as soon as the [šemeš](../../strongs/h/h8121.md) is [zāraḥ](../../strongs/h/h2224.md), thou shalt rise [šāḵam](../../strongs/h/h7925.md), and [pāšaṭ](../../strongs/h/h6584.md) upon the [ʿîr](../../strongs/h/h5892.md): and, behold, when he and the ['am](../../strongs/h/h5971.md) that is with him [yāṣā'](../../strongs/h/h3318.md) against thee, then mayest thou ['asah](../../strongs/h/h6213.md) to them as [yad](../../strongs/h/h3027.md) shalt find [māṣā'](../../strongs/h/h4672.md).

<a name="judges_9_34"></a>Judges 9:34

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [quwm](../../strongs/h/h6965.md), and all the ['am](../../strongs/h/h5971.md) that were with him, by [layil](../../strongs/h/h3915.md), and they ['arab](../../strongs/h/h693.md) against [Šᵊḵem](../../strongs/h/h7927.md) in ['arbaʿ](../../strongs/h/h702.md) [ro'sh](../../strongs/h/h7218.md).

<a name="judges_9_35"></a>Judges 9:35

And [GaʿAl](../../strongs/h/h1603.md) the [ben](../../strongs/h/h1121.md) of [ʿEḇeḏ](../../strongs/h/h5651.md) [yāṣā'](../../strongs/h/h3318.md), and ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md): and ['Ăḇîmeleḵ](../../strongs/h/h40.md) [quwm](../../strongs/h/h6965.md), and the ['am](../../strongs/h/h5971.md) that were with him, from lying in [ma'ărāḇ](../../strongs/h/h3993.md).

<a name="judges_9_36"></a>Judges 9:36

And when [GaʿAl](../../strongs/h/h1603.md) [ra'ah](../../strongs/h/h7200.md) the ['am](../../strongs/h/h5971.md), he ['āmar](../../strongs/h/h559.md) to [Zᵊḇul](../../strongs/h/h2083.md), Behold, there [yarad](../../strongs/h/h3381.md) ['am](../../strongs/h/h5971.md) [yarad](../../strongs/h/h3381.md) from the [ro'sh](../../strongs/h/h7218.md) of the [har](../../strongs/h/h2022.md). And [Zᵊḇul](../../strongs/h/h2083.md) ['āmar](../../strongs/h/h559.md) unto him, Thou [ra'ah](../../strongs/h/h7200.md) the [ṣēl](../../strongs/h/h6738.md) of the [har](../../strongs/h/h2022.md) as if they were ['enowsh](../../strongs/h/h582.md).

<a name="judges_9_37"></a>Judges 9:37

And [GaʿAl](../../strongs/h/h1603.md) [dabar](../../strongs/h/h1696.md) [yāsap̄](../../strongs/h/h3254.md) and ['āmar](../../strongs/h/h559.md), See there [yarad](../../strongs/h/h3381.md) ['am](../../strongs/h/h5971.md) [yarad](../../strongs/h/h3381.md) by the [ṭabûr](../../strongs/h/h2872.md) of the ['erets](../../strongs/h/h776.md), and ['echad](../../strongs/h/h259.md) [ro'sh](../../strongs/h/h7218.md) [bow'](../../strongs/h/h935.md) along by the ['ēlôn](../../strongs/h/h436.md) [derek](../../strongs/h/h1870.md) [ʿānan](../../strongs/h/h6049.md).

<a name="judges_9_38"></a>Judges 9:38

Then ['āmar](../../strongs/h/h559.md) [Zᵊḇul](../../strongs/h/h2083.md) unto him, Where is ['ēp̄ô](../../strongs/h/h645.md) thy [peh](../../strongs/h/h6310.md), wherewith thou ['āmar](../../strongs/h/h559.md), Who is ['Ăḇîmeleḵ](../../strongs/h/h40.md), that we should ['abad](../../strongs/h/h5647.md) him? is not this the ['am](../../strongs/h/h5971.md) that thou hast [mā'as](../../strongs/h/h3988.md)? [yāṣā'](../../strongs/h/h3318.md), I pray [na'](../../strongs/h/h4994.md), and [lāḥam](../../strongs/h/h3898.md) with them.

<a name="judges_9_39"></a>Judges 9:39

And [GaʿAl](../../strongs/h/h1603.md) [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) the [baʿal](../../strongs/h/h1167.md) of [Šᵊḵem](../../strongs/h/h7927.md), and [lāḥam](../../strongs/h/h3898.md) with ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_9_40"></a>Judges 9:40

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [radaph](../../strongs/h/h7291.md) him, and he [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) him, and [rab](../../strongs/h/h7227.md) were [naphal](../../strongs/h/h5307.md) and [ḥālāl](../../strongs/h/h2491.md), even unto the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md).

<a name="judges_9_41"></a>Judges 9:41

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [yashab](../../strongs/h/h3427.md) at ['Ărûmâ](../../strongs/h/h725.md): and [Zᵊḇul](../../strongs/h/h2083.md) [gāraš](../../strongs/h/h1644.md) [GaʿAl](../../strongs/h/h1603.md) and his ['ach](../../strongs/h/h251.md), that they should not [yashab](../../strongs/h/h3427.md) in [Šᵊḵem](../../strongs/h/h7927.md).

<a name="judges_9_42"></a>Judges 9:42

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that the ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md); and they [nāḡaḏ](../../strongs/h/h5046.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_9_43"></a>Judges 9:43

And he [laqach](../../strongs/h/h3947.md) the ['am](../../strongs/h/h5971.md), and [ḥāṣâ](../../strongs/h/h2673.md) them into [šālôš](../../strongs/h/h7969.md) [ro'sh](../../strongs/h/h7218.md), and laid ['arab](../../strongs/h/h693.md) in the [sadeh](../../strongs/h/h7704.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, the ['am](../../strongs/h/h5971.md) were [yāṣā'](../../strongs/h/h3318.md) out of the [ʿîr](../../strongs/h/h5892.md); and he [quwm](../../strongs/h/h6965.md) against them, and [nakah](../../strongs/h/h5221.md) them.

<a name="judges_9_44"></a>Judges 9:44

And ['Ăḇîmeleḵ](../../strongs/h/h40.md), and the [ro'sh](../../strongs/h/h7218.md) that was with him, [pāšaṭ](../../strongs/h/h6584.md), and ['amad](../../strongs/h/h5975.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md): and the [šᵊnayim](../../strongs/h/h8147.md) other [ro'sh](../../strongs/h/h7218.md) ran [pāšaṭ](../../strongs/h/h6584.md) all the people that were in the [sadeh](../../strongs/h/h7704.md), and [nakah](../../strongs/h/h5221.md) them.

<a name="judges_9_45"></a>Judges 9:45

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [lāḥam](../../strongs/h/h3898.md) against the [ʿîr](../../strongs/h/h5892.md) all that [yowm](../../strongs/h/h3117.md); and he [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md), and [harag](../../strongs/h/h2026.md) the ['am](../../strongs/h/h5971.md) that was therein, and [nāṯaṣ](../../strongs/h/h5422.md) the [ʿîr](../../strongs/h/h5892.md), and [zāraʿ](../../strongs/h/h2232.md) it with [melaḥ](../../strongs/h/h4417.md).

<a name="judges_9_46"></a>Judges 9:46

And when all the [baʿal](../../strongs/h/h1167.md) of the [miḡdāl](../../strongs/h/h4026.md) of [Šᵊḵem](../../strongs/h/h7927.md) [shama'](../../strongs/h/h8085.md) that, they [bow'](../../strongs/h/h935.md) into an [ṣārîaḥ](../../strongs/h/h6877.md) of the [bayith](../../strongs/h/h1004.md) of the ['el](../../strongs/h/h410.md) [Bᵊrîṯ](../../strongs/h/h1286.md).

<a name="judges_9_47"></a>Judges 9:47

And it was [nāḡaḏ](../../strongs/h/h5046.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md), that all the [baʿal](../../strongs/h/h1167.md) of the [miḡdāl](../../strongs/h/h4026.md) of [Šᵊḵem](../../strongs/h/h7927.md) were [qāḇaṣ](../../strongs/h/h6908.md).

<a name="judges_9_48"></a>Judges 9:48

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [ʿālâ](../../strongs/h/h5927.md) him to [har](../../strongs/h/h2022.md) [Ṣalmôn](../../strongs/h/h6756.md), he and all the ['am](../../strongs/h/h5971.md) that were with him; and ['Ăḇîmeleḵ](../../strongs/h/h40.md) [laqach](../../strongs/h/h3947.md) a [qardōm](../../strongs/h/h7134.md) in his [yad](../../strongs/h/h3027.md), and [karath](../../strongs/h/h3772.md) a [śôḵ](../../strongs/h/h7754.md) from the ['ets](../../strongs/h/h6086.md), and [nasa'](../../strongs/h/h5375.md) it, and [śûm](../../strongs/h/h7760.md) it on his [šᵊḵem](../../strongs/h/h7926.md), and ['āmar](../../strongs/h/h559.md) unto the ['am](../../strongs/h/h5971.md) that were with him, What ye have [ra'ah](../../strongs/h/h7200.md) me ['asah](../../strongs/h/h6213.md), make [māhar](../../strongs/h/h4116.md), and ['asah](../../strongs/h/h6213.md) as [kᵊmô](../../strongs/h/h3644.md) have done.

<a name="judges_9_49"></a>Judges 9:49

And all the ['am](../../strongs/h/h5971.md) likewise [karath](../../strongs/h/h3772.md) every ['iysh](../../strongs/h/h376.md) his [śôḵ](../../strongs/h/h7754.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md), and [śûm](../../strongs/h/h7760.md) them to the [ṣārîaḥ](../../strongs/h/h6877.md), and [yāṣaṯ](../../strongs/h/h3341.md) the [ṣārîaḥ](../../strongs/h/h6877.md) on ['esh](../../strongs/h/h784.md) upon them; so that all the ['enowsh](../../strongs/h/h582.md) of the [miḡdāl](../../strongs/h/h4026.md) of [Šᵊḵem](../../strongs/h/h7927.md) [muwth](../../strongs/h/h4191.md) also, about an ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md).

<a name="judges_9_50"></a>Judges 9:50

Then [yālaḵ](../../strongs/h/h3212.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md) to [Tēḇēṣ](../../strongs/h/h8405.md), and [ḥānâ](../../strongs/h/h2583.md) against [Tēḇēṣ](../../strongs/h/h8405.md), and [lāḵaḏ](../../strongs/h/h3920.md) it.

<a name="judges_9_51"></a>Judges 9:51

But there was a ['oz](../../strongs/h/h5797.md) [miḡdāl](../../strongs/h/h4026.md) [tavek](../../strongs/h/h8432.md) the [ʿîr](../../strongs/h/h5892.md), and thither [nûs](../../strongs/h/h5127.md) all the ['enowsh](../../strongs/h/h582.md) and ['ishshah](../../strongs/h/h802.md), and all [baʿal](../../strongs/h/h1167.md) of the [ʿîr](../../strongs/h/h5892.md), and [cagar](../../strongs/h/h5462.md) it to them, and [ʿālâ](../../strongs/h/h5927.md) them to the [gāḡ](../../strongs/h/h1406.md) of the [miḡdāl](../../strongs/h/h4026.md).

<a name="judges_9_52"></a>Judges 9:52

And ['Ăḇîmeleḵ](../../strongs/h/h40.md) [bow'](../../strongs/h/h935.md) unto the [miḡdāl](../../strongs/h/h4026.md), and [lāḥam](../../strongs/h/h3898.md) against it, and went [nāḡaš](../../strongs/h/h5066.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the [miḡdāl](../../strongs/h/h4026.md) to [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md).

<a name="judges_9_53"></a>Judges 9:53

And an ['echad](../../strongs/h/h259.md) ['ishshah](../../strongs/h/h802.md) [shalak](../../strongs/h/h7993.md) a [pelaḥ](../../strongs/h/h6400.md) of a [reḵeḇ](../../strongs/h/h7393.md) upon ['Ăḇîmeleḵ](../../strongs/h/h40.md) [ro'sh](../../strongs/h/h7218.md), and all to [rāṣaṣ](../../strongs/h/h7533.md) his [gulgōleṯ](../../strongs/h/h1538.md).

<a name="judges_9_54"></a>Judges 9:54

Then he [qara'](../../strongs/h/h7121.md) [mᵊhērâ](../../strongs/h/h4120.md) unto the [naʿar](../../strongs/h/h5288.md) his [nasa'](../../strongs/h/h5375.md) [kĕliy](../../strongs/h/h3627.md), and ['āmar](../../strongs/h/h559.md) unto him, [šālap̄](../../strongs/h/h8025.md) thy [chereb](../../strongs/h/h2719.md), and [muwth](../../strongs/h/h4191.md) me, that ['āmar](../../strongs/h/h559.md) not of me, An ['ishshah](../../strongs/h/h802.md) [harag](../../strongs/h/h2026.md) him. And his [naʿar](../../strongs/h/h5288.md) [dāqar](../../strongs/h/h1856.md) him, and he [muwth](../../strongs/h/h4191.md).

<a name="judges_9_55"></a>Judges 9:55

And when the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that ['Ăḇîmeleḵ](../../strongs/h/h40.md) was [muwth](../../strongs/h/h4191.md), they [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) unto his [maqowm](../../strongs/h/h4725.md).

<a name="judges_9_56"></a>Judges 9:56

Thus ['Elohiym](../../strongs/h/h430.md) [shuwb](../../strongs/h/h7725.md) the [ra'](../../strongs/h/h7451.md) of ['Ăḇîmeleḵ](../../strongs/h/h40.md), which he ['asah](../../strongs/h/h6213.md) unto his ['ab](../../strongs/h/h1.md), in [harag](../../strongs/h/h2026.md) his [šiḇʿîm](../../strongs/h/h7657.md) ['ach](../../strongs/h/h251.md):

<a name="judges_9_57"></a>Judges 9:57

And all the [ra'](../../strongs/h/h7451.md) of the ['enowsh](../../strongs/h/h582.md) of [Šᵊḵem](../../strongs/h/h7927.md) did ['Elohiym](../../strongs/h/h430.md) [shuwb](../../strongs/h/h7725.md) upon their [ro'sh](../../strongs/h/h7218.md): and upon them [bow'](../../strongs/h/h935.md) the [qᵊlālâ](../../strongs/h/h7045.md) of [Yôṯām](../../strongs/h/h3147.md) the [ben](../../strongs/h/h1121.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 8](judges_8.md) - [Judges 10](judges_10.md)