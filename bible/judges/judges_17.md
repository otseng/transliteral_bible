# [Judges 17](https://www.blueletterbible.org/kjv/judges/17)

<a name="judges_17_1"></a>Judges 17:1

And there was an ['iysh](../../strongs/h/h376.md) of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), whose [shem](../../strongs/h/h8034.md) was [Mîḵāyhû](../../strongs/h/h4321.md).

<a name="judges_17_2"></a>Judges 17:2

And he ['āmar](../../strongs/h/h559.md) unto his ['em](../../strongs/h/h517.md), The ['elep̄](../../strongs/h/h505.md) [mē'â](../../strongs/h/h3967.md) shekels of [keceph](../../strongs/h/h3701.md) that were [laqach](../../strongs/h/h3947.md) from thee, about which thou ['ālâ](../../strongs/h/h422.md), and ['āmar](../../strongs/h/h559.md) of also in mine ['ozen](../../strongs/h/h241.md), behold, the [keceph](../../strongs/h/h3701.md) is with me; I [laqach](../../strongs/h/h3947.md) it. And his ['em](../../strongs/h/h517.md) ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be thou of [Yĕhovah](../../strongs/h/h3068.md), my [ben](../../strongs/h/h1121.md).

<a name="judges_17_3"></a>Judges 17:3

And when he had [shuwb](../../strongs/h/h7725.md) the ['elep̄](../../strongs/h/h505.md) [mē'â](../../strongs/h/h3967.md) shekels of [keceph](../../strongs/h/h3701.md) to his ['em](../../strongs/h/h517.md), his ['em](../../strongs/h/h517.md) ['āmar](../../strongs/h/h559.md), I had [qadash](../../strongs/h/h6942.md) [qadash](../../strongs/h/h6942.md) the [keceph](../../strongs/h/h3701.md) unto [Yĕhovah](../../strongs/h/h3068.md) from my [yad](../../strongs/h/h3027.md) for my [ben](../../strongs/h/h1121.md), to ['asah](../../strongs/h/h6213.md) a [pecel](../../strongs/h/h6459.md) and a [massēḵâ](../../strongs/h/h4541.md): now therefore I will [shuwb](../../strongs/h/h7725.md) it unto thee.

<a name="judges_17_4"></a>Judges 17:4

Yet he [shuwb](../../strongs/h/h7725.md) the [keceph](../../strongs/h/h3701.md) unto his ['em](../../strongs/h/h517.md); and his ['em](../../strongs/h/h517.md) [laqach](../../strongs/h/h3947.md) two [mē'â](../../strongs/h/h3967.md) shekels of [keceph](../../strongs/h/h3701.md), and [nathan](../../strongs/h/h5414.md) them to the [tsaraph](../../strongs/h/h6884.md), who ['asah](../../strongs/h/h6213.md) thereof a [pecel](../../strongs/h/h6459.md) and a [massēḵâ](../../strongs/h/h4541.md): and they were in the [bayith](../../strongs/h/h1004.md) of [Mîḵāyhû](../../strongs/h/h4321.md).

<a name="judges_17_5"></a>Judges 17:5

And the ['iysh](../../strongs/h/h376.md) [Mîḵâ](../../strongs/h/h4318.md) had a [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and ['asah](../../strongs/h/h6213.md) an ['ēp̄ôḏ](../../strongs/h/h646.md), and [tᵊrāp̄îm](../../strongs/h/h8655.md), and [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) ['echad](../../strongs/h/h259.md) of his [ben](../../strongs/h/h1121.md), who became his [kōhēn](../../strongs/h/h3548.md).

<a name="judges_17_6"></a>Judges 17:6

In those [yowm](../../strongs/h/h3117.md) there was no [melek](../../strongs/h/h4428.md) in [Yisra'el](../../strongs/h/h3478.md), but every ['iysh](../../strongs/h/h376.md) ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in his own ['ayin](../../strongs/h/h5869.md).

<a name="judges_17_7"></a>Judges 17:7

And there was a [naʿar](../../strongs/h/h5288.md) out of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md) of the [mišpāḥâ](../../strongs/h/h4940.md) of [Yehuwdah](../../strongs/h/h3063.md), who was a [Lᵊvî](../../strongs/h/h3881.md), and he [guwr](../../strongs/h/h1481.md) there.

<a name="judges_17_8"></a>Judges 17:8

And the ['iysh](../../strongs/h/h376.md) [yālaḵ](../../strongs/h/h3212.md) out of the [ʿîr](../../strongs/h/h5892.md) from [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md) to [guwr](../../strongs/h/h1481.md) where he could [māṣā'](../../strongs/h/h4672.md) a place: and he [bow'](../../strongs/h/h935.md) to [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) to the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md), as he ['asah](../../strongs/h/h6213.md) [derek](../../strongs/h/h1870.md).

<a name="judges_17_9"></a>Judges 17:9

And [Mîḵâ](../../strongs/h/h4318.md) ['āmar](../../strongs/h/h559.md) unto him, ['ayin](../../strongs/h/h370.md) [bow'](../../strongs/h/h935.md) thou? And he ['āmar](../../strongs/h/h559.md) unto him, I am a [Lᵊvî](../../strongs/h/h3881.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md), and I [halak](../../strongs/h/h1980.md) to [guwr](../../strongs/h/h1481.md) where I may [māṣā'](../../strongs/h/h4672.md) a place.

<a name="judges_17_10"></a>Judges 17:10

And [Mîḵâ](../../strongs/h/h4318.md) ['āmar](../../strongs/h/h559.md) unto him, [yashab](../../strongs/h/h3427.md) with me, and be unto me an ['ab](../../strongs/h/h1.md) and a [kōhēn](../../strongs/h/h3548.md), and I will [nathan](../../strongs/h/h5414.md) thee [ʿeśer](../../strongs/h/h6235.md) shekels of [keceph](../../strongs/h/h3701.md) by the [yowm](../../strongs/h/h3117.md), and a [ʿēreḵ](../../strongs/h/h6187.md) of [beḡeḏ](../../strongs/h/h899.md), and thy [miḥyâ](../../strongs/h/h4241.md). So the [Lᵊvî](../../strongs/h/h3881.md) [yālaḵ](../../strongs/h/h3212.md).

<a name="judges_17_11"></a>Judges 17:11

And the [Lᵊvî](../../strongs/h/h3881.md) was [yā'al](../../strongs/h/h2974.md) to [yashab](../../strongs/h/h3427.md) with the ['iysh](../../strongs/h/h376.md); and the [naʿar](../../strongs/h/h5288.md) was unto him as ['echad](../../strongs/h/h259.md) of his [ben](../../strongs/h/h1121.md).

<a name="judges_17_12"></a>Judges 17:12

And [Mîḵâ](../../strongs/h/h4318.md) [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) the [Lᵊvî](../../strongs/h/h3881.md); and the [naʿar](../../strongs/h/h5288.md) became his [kōhēn](../../strongs/h/h3548.md), and was in the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md).

<a name="judges_17_13"></a>Judges 17:13

Then ['āmar](../../strongs/h/h559.md) [Mîḵâ](../../strongs/h/h4318.md), Now [yada'](../../strongs/h/h3045.md) I that [Yĕhovah](../../strongs/h/h3068.md) will do me [yatab](../../strongs/h/h3190.md), seeing I have a [Lᵊvî](../../strongs/h/h3881.md) to my [kōhēn](../../strongs/h/h3548.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 16](judges_16.md) - [Judges 18](judges_18.md)