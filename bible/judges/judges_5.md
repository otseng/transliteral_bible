# [Judges 5](https://www.blueletterbible.org/kjv/judges/5)

<a name="judges_5_1"></a>Judges 5:1

Then [shiyr](../../strongs/h/h7891.md) [Dᵊḇôrâ](../../strongs/h/h1683.md) and [Bārāq](../../strongs/h/h1301.md) the [ben](../../strongs/h/h1121.md) of ['ĂḇînōʿAm](../../strongs/h/h42.md) on that [yowm](../../strongs/h/h3117.md), ['āmar](../../strongs/h/h559.md),

<a name="judges_5_2"></a>Judges 5:2

[barak](../../strongs/h/h1288.md) ye [Yĕhovah](../../strongs/h/h3068.md) for the [pāraʿ](../../strongs/h/h6544.md) [parʿâ](../../strongs/h/h6546.md) of [Yisra'el](../../strongs/h/h3478.md), when the ['am](../../strongs/h/h5971.md) willingly [nāḏaḇ](../../strongs/h/h5068.md) themselves.

<a name="judges_5_3"></a>Judges 5:3

[shama'](../../strongs/h/h8085.md), O ye [melek](../../strongs/h/h4428.md); ['azan](../../strongs/h/h238.md), O ye [razan](../../strongs/h/h7336.md); I, even I, will [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md); I will [zamar](../../strongs/h/h2167.md) to [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_5_4"></a>Judges 5:4

[Yĕhovah](../../strongs/h/h3068.md), when thou [yāṣā'](../../strongs/h/h3318.md) of [Śēʿîr](../../strongs/h/h8165.md), when thou [ṣāʿaḏ](../../strongs/h/h6805.md) of the [sadeh](../../strongs/h/h7704.md) of ['Ĕḏōm](../../strongs/h/h123.md), the ['erets](../../strongs/h/h776.md) [rāʿaš](../../strongs/h/h7493.md), and the [shamayim](../../strongs/h/h8064.md) [nāṭap̄](../../strongs/h/h5197.md), the ['ab](../../strongs/h/h5645.md) also [nāṭap̄](../../strongs/h/h5197.md) [mayim](../../strongs/h/h4325.md).

<a name="judges_5_5"></a>Judges 5:5

The [har](../../strongs/h/h2022.md) [nāzal](../../strongs/h/h5140.md) from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), even that [Sînay](../../strongs/h/h5514.md) from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_5_6"></a>Judges 5:6

In the [yowm](../../strongs/h/h3117.md) of [Šamgar](../../strongs/h/h8044.md) the [ben](../../strongs/h/h1121.md) of [ʿĂnāṯ](../../strongs/h/h6067.md), in the [yowm](../../strongs/h/h3117.md) of [YāʿĒl](../../strongs/h/h3278.md), the ['orach](../../strongs/h/h734.md) were [ḥāḏal](../../strongs/h/h2308.md), and the [halak](../../strongs/h/h1980.md) [yālaḵ](../../strongs/h/h3212.md) through [ʿăqalqāl](../../strongs/h/h6128.md) [nāṯîḇ](../../strongs/h/h5410.md).

<a name="judges_5_7"></a>Judges 5:7

The inhabitants of the [pᵊrāzôn](../../strongs/h/h6520.md) [ḥāḏal](../../strongs/h/h2308.md), they [ḥāḏal](../../strongs/h/h2308.md) in [Yisra'el](../../strongs/h/h3478.md), until that I [Dᵊḇôrâ](../../strongs/h/h1683.md) [quwm](../../strongs/h/h6965.md), that I [quwm](../../strongs/h/h6965.md) an ['em](../../strongs/h/h517.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_5_8"></a>Judges 5:8

They [bāḥar](../../strongs/h/h977.md) [ḥāḏāš](../../strongs/h/h2319.md) ['Elohiym](../../strongs/h/h430.md); then was [lāḥēm](../../strongs/h/h3901.md) in the [sha'ar](../../strongs/h/h8179.md): was there a [magen](../../strongs/h/h4043.md) or [rōmaḥ](../../strongs/h/h7420.md) [ra'ah](../../strongs/h/h7200.md) among ['arbāʿîm](../../strongs/h/h705.md) ['elep̄](../../strongs/h/h505.md) in [Yisra'el](../../strongs/h/h3478.md)?

<a name="judges_5_9"></a>Judges 5:9

My [leb](../../strongs/h/h3820.md) is toward the [ḥāqaq](../../strongs/h/h2710.md) of [Yisra'el](../../strongs/h/h3478.md), that offered themselves [nāḏaḇ](../../strongs/h/h5068.md) among the ['am](../../strongs/h/h5971.md). [barak](../../strongs/h/h1288.md) ye [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_5_10"></a>Judges 5:10

[śîaḥ](../../strongs/h/h7878.md), ye that [rāḵaḇ](../../strongs/h/h7392.md) on [ṣāḥōr](../../strongs/h/h6715.md) ['āṯôn](../../strongs/h/h860.md), ye that [yashab](../../strongs/h/h3427.md) in [maḏ](../../strongs/h/h4055.md), and [halak](../../strongs/h/h1980.md) by the [derek](../../strongs/h/h1870.md).

<a name="judges_5_11"></a>Judges 5:11

They that are delivered from the [qowl](../../strongs/h/h6963.md) of [ḥāṣaṣ](../../strongs/h/h2686.md) in the places of drawing [maš'āḇ](../../strongs/h/h4857.md), there shall they [tānâ](../../strongs/h/h8567.md) the righteous [tsedaqah](../../strongs/h/h6666.md) of [Yĕhovah](../../strongs/h/h3068.md), even the [tsedaqah](../../strongs/h/h6666.md) toward the inhabitants of his [pᵊrāzôn](../../strongs/h/h6520.md) in [Yisra'el](../../strongs/h/h3478.md): then shall the ['am](../../strongs/h/h5971.md) of [Yĕhovah](../../strongs/h/h3068.md) [yarad](../../strongs/h/h3381.md) to the [sha'ar](../../strongs/h/h8179.md).

<a name="judges_5_12"></a>Judges 5:12

[ʿûr](../../strongs/h/h5782.md), [ʿûr](../../strongs/h/h5782.md), [Dᵊḇôrâ](../../strongs/h/h1683.md): [ʿûr](../../strongs/h/h5782.md), [ʿûr](../../strongs/h/h5782.md), [dabar](../../strongs/h/h1696.md) a [šîr](../../strongs/h/h7892.md): [quwm](../../strongs/h/h6965.md), [Bārāq](../../strongs/h/h1301.md), and lead thy [šᵊḇî](../../strongs/h/h7628.md) [šāḇâ](../../strongs/h/h7617.md), thou [ben](../../strongs/h/h1121.md) of ['ĂḇînōʿAm](../../strongs/h/h42.md).

<a name="judges_5_13"></a>Judges 5:13

Then he made him that [śārîḏ](../../strongs/h/h8300.md) have [radah](../../strongs/h/h7287.md) over the ['addiyr](../../strongs/h/h117.md) among the ['am](../../strongs/h/h5971.md): [Yĕhovah](../../strongs/h/h3068.md) made me have [radah](../../strongs/h/h7287.md) over the [gibôr](../../strongs/h/h1368.md).

<a name="judges_5_14"></a>Judges 5:14

Out of ['Ep̄rayim](../../strongs/h/h669.md) was there a [šereš](../../strongs/h/h8328.md) of them against [ʿĂmālēq](../../strongs/h/h6002.md); ['aḥar](../../strongs/h/h310.md) thee, [Binyāmîn](../../strongs/h/h1144.md), among thy ['am](../../strongs/h/h5971.md); out of [Māḵîr](../../strongs/h/h4353.md) [yarad](../../strongs/h/h3381.md) [ḥāqaq](../../strongs/h/h2710.md), and out of [Zᵊḇûlûn](../../strongs/h/h2074.md) they that [mashak](../../strongs/h/h4900.md) the [shebet](../../strongs/h/h7626.md) of the [sāp̄ar](../../strongs/h/h5608.md).

<a name="judges_5_15"></a>Judges 5:15

And the [śar](../../strongs/h/h8269.md) of [Yiśśāśḵār](../../strongs/h/h3485.md) were with [Dᵊḇôrâ](../../strongs/h/h1683.md); even [Yiśśāśḵār](../../strongs/h/h3485.md), and also [Bārāq](../../strongs/h/h1301.md): he was [shalach](../../strongs/h/h7971.md) on [regel](../../strongs/h/h7272.md) into the [ʿēmeq](../../strongs/h/h6010.md). For the [pᵊlagâ](../../strongs/h/h6390.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) there were [gadowl](../../strongs/h/h1419.md) [ḥēqeq](../../strongs/h/h2711.md) of [leb](../../strongs/h/h3820.md).

<a name="judges_5_16"></a>Judges 5:16

Why [yashab](../../strongs/h/h3427.md) thou [bayin](../../strongs/h/h996.md) the [mišpᵊṯayim](../../strongs/h/h4942.md), to [shama'](../../strongs/h/h8085.md) the [šᵊrûqâ](../../strongs/h/h8292.md) of the [ʿēḏer](../../strongs/h/h5739.md)? For the [pᵊlagâ](../../strongs/h/h6390.md) of [Rᵊ'ûḇēn](../../strongs/h/h7205.md) there were [gadowl](../../strongs/h/h1419.md) [ḥēqer](../../strongs/h/h2714.md) of [leb](../../strongs/h/h3820.md).

<a name="judges_5_17"></a>Judges 5:17

[Gilʿāḏ](../../strongs/h/h1568.md) [shakan](../../strongs/h/h7931.md) [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md): and why did [Dān](../../strongs/h/h1835.md) [guwr](../../strongs/h/h1481.md) in ['ŏnîyâ](../../strongs/h/h591.md)? ['Āšēr](../../strongs/h/h836.md) [yashab](../../strongs/h/h3427.md) on the [yam](../../strongs/h/h3220.md) [ḥôp̄](../../strongs/h/h2348.md), and [shakan](../../strongs/h/h7931.md) in his [mip̄rāṣ](../../strongs/h/h4664.md).

<a name="judges_5_18"></a>Judges 5:18

[Zᵊḇûlûn](../../strongs/h/h2074.md) and [Nap̄tālî](../../strongs/h/h5321.md) were an ['am](../../strongs/h/h5971.md) that [ḥārap̄](../../strongs/h/h2778.md) their [nephesh](../../strongs/h/h5315.md) unto the [muwth](../../strongs/h/h4191.md) in the [marowm](../../strongs/h/h4791.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="judges_5_19"></a>Judges 5:19

The [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) and [lāḥam](../../strongs/h/h3898.md), then [lāḥam](../../strongs/h/h3898.md) the [melek](../../strongs/h/h4428.md) of [Kĕna'an](../../strongs/h/h3667.md) in [TaʿNāḵ](../../strongs/h/h8590.md) by the [mayim](../../strongs/h/h4325.md) of [Mᵊḡidôn](../../strongs/h/h4023.md); they [laqach](../../strongs/h/h3947.md) no [beṣaʿ](../../strongs/h/h1215.md) of [keceph](../../strongs/h/h3701.md).

<a name="judges_5_20"></a>Judges 5:20

They [lāḥam](../../strongs/h/h3898.md) from [shamayim](../../strongs/h/h8064.md); the [kowkab](../../strongs/h/h3556.md) in their [mĕcillah](../../strongs/h/h4546.md) [lāḥam](../../strongs/h/h3898.md) against [Sîsrā'](../../strongs/h/h5516.md).

<a name="judges_5_21"></a>Judges 5:21

The [nachal](../../strongs/h/h5158.md) of [Qîšôn](../../strongs/h/h7028.md) swept them [gārap̄](../../strongs/h/h1640.md), that [qᵊḏûmîm](../../strongs/h/h6917.md) [nachal](../../strongs/h/h5158.md), the [nachal](../../strongs/h/h5158.md) [Qîšôn](../../strongs/h/h7028.md). O my [nephesh](../../strongs/h/h5315.md), thou hast [dāraḵ](../../strongs/h/h1869.md) ['oz](../../strongs/h/h5797.md).

<a name="judges_5_22"></a>Judges 5:22

Then were the ['aqeb](../../strongs/h/h6119.md) [sûs](../../strongs/h/h5483.md) [hālam](../../strongs/h/h1986.md) by the means of the [dahărâ](../../strongs/h/h1726.md), the [dahărâ](../../strongs/h/h1726.md) of their ['abîr](../../strongs/h/h47.md).

<a name="judges_5_23"></a>Judges 5:23

['arar](../../strongs/h/h779.md) ye [Mērôz](../../strongs/h/h4789.md), ['āmar](../../strongs/h/h559.md) the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), ['arar](../../strongs/h/h779.md) ye ['arar](../../strongs/h/h779.md) the [yashab](../../strongs/h/h3427.md) thereof; because they [bow'](../../strongs/h/h935.md) not to the [ʿezrâ](../../strongs/h/h5833.md) of [Yĕhovah](../../strongs/h/h3068.md), to the [ʿezrâ](../../strongs/h/h5833.md) of [Yĕhovah](../../strongs/h/h3068.md) against the [gibôr](../../strongs/h/h1368.md).

<a name="judges_5_24"></a>Judges 5:24

[barak](../../strongs/h/h1288.md) above ['ishshah](../../strongs/h/h802.md) shall [YāʿĒl](../../strongs/h/h3278.md) the ['ishshah](../../strongs/h/h802.md) of [Ḥeḇer](../../strongs/h/h2268.md) the [Qênî](../../strongs/h/h7017.md) be, [barak](../../strongs/h/h1288.md) shall she be above ['ishshah](../../strongs/h/h802.md) in the ['ohel](../../strongs/h/h168.md).

<a name="judges_5_25"></a>Judges 5:25

He [sha'al](../../strongs/h/h7592.md) [mayim](../../strongs/h/h4325.md), and she [nathan](../../strongs/h/h5414.md) him [chalab](../../strongs/h/h2461.md); she [qāraḇ](../../strongs/h/h7126.md) [ḥem'â](../../strongs/h/h2529.md) in an ['addiyr](../../strongs/h/h117.md) [sēp̄el](../../strongs/h/h5602.md).

<a name="judges_5_26"></a>Judges 5:26

She [shalach](../../strongs/h/h7971.md) her [yad](../../strongs/h/h3027.md) to the [yāṯēḏ](../../strongs/h/h3489.md), and her [yamiyn](../../strongs/h/h3225.md) to the [ʿāmēl](../../strongs/h/h6001.md) [halmûṯ](../../strongs/h/h1989.md); and with the hammer she [hālam](../../strongs/h/h1986.md) [Sîsrā'](../../strongs/h/h5516.md), she smote [māḥaq](../../strongs/h/h4277.md) his [ro'sh](../../strongs/h/h7218.md), when she had [māḥaṣ](../../strongs/h/h4272.md) and [ḥālap̄](../../strongs/h/h2498.md) his [raqqâ](../../strongs/h/h7541.md).

<a name="judges_5_27"></a>Judges 5:27

At her [regel](../../strongs/h/h7272.md) he [kara'](../../strongs/h/h3766.md), he [naphal](../../strongs/h/h5307.md), he [shakab](../../strongs/h/h7901.md): at her [regel](../../strongs/h/h7272.md) he [kara'](../../strongs/h/h3766.md), he [naphal](../../strongs/h/h5307.md): ['ăšer](../../strongs/h/h834.md) he [kara'](../../strongs/h/h3766.md), there he [naphal](../../strongs/h/h5307.md) [shadad](../../strongs/h/h7703.md).

<a name="judges_5_28"></a>Judges 5:28

The ['em](../../strongs/h/h517.md) of [Sîsrā'](../../strongs/h/h5516.md) [šāqap̄](../../strongs/h/h8259.md) out at a [ḥallôn](../../strongs/h/h2474.md), and [yāḇaḇ](../../strongs/h/h2980.md) through the ['ešnāḇ](../../strongs/h/h822.md), Why is his [reḵeḇ](../../strongs/h/h7393.md) so [buwsh](../../strongs/h/h954.md) in [bow'](../../strongs/h/h935.md)? why ['āḥar](../../strongs/h/h309.md) the [pa'am](../../strongs/h/h6471.md) of his [merkāḇâ](../../strongs/h/h4818.md)?

<a name="judges_5_29"></a>Judges 5:29

Her [ḥāḵām](../../strongs/h/h2450.md) [Śārâ](../../strongs/h/h8282.md) ['anah](../../strongs/h/h6030.md) her, yea, she [shuwb](../../strongs/h/h7725.md) ['emer](../../strongs/h/h561.md) to herself,

<a name="judges_5_30"></a>Judges 5:30

Have they not [māṣā'](../../strongs/h/h4672.md)? have they not [chalaq](../../strongs/h/h2505.md) the [šālāl](../../strongs/h/h7998.md); to [ro'sh](../../strongs/h/h7218.md) [geḇer](../../strongs/h/h1397.md) a [raḥam](../../strongs/h/h7356.md) or [raḥămâ](../../strongs/h/h7361.md); to [Sîsrā'](../../strongs/h/h5516.md) a [šālāl](../../strongs/h/h7998.md) of [ṣeḇaʿ](../../strongs/h/h6648.md), a [šālāl](../../strongs/h/h7998.md) of [ṣeḇaʿ](../../strongs/h/h6648.md) of [riqmâ](../../strongs/h/h7553.md), of [ṣeḇaʿ](../../strongs/h/h6648.md) of needlework on both [riqmâ](../../strongs/h/h7553.md), meet for the [ṣaûā'r](../../strongs/h/h6677.md) of them that take the [šālāl](../../strongs/h/h7998.md)?

<a name="judges_5_31"></a>Judges 5:31

So let all thine ['oyeb](../../strongs/h/h341.md) ['abad](../../strongs/h/h6.md), [Yĕhovah](../../strongs/h/h3068.md): but let them that ['ahab](../../strongs/h/h157.md) him be as the [šemeš](../../strongs/h/h8121.md) when he [yāṣā'](../../strongs/h/h3318.md) in his [gᵊḇûrâ](../../strongs/h/h1369.md). And the ['erets](../../strongs/h/h776.md) had [šāqaṭ](../../strongs/h/h8252.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 4](judges_4.md) - [Judges 6](judges_6.md)