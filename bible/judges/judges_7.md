# [Judges 7](https://www.blueletterbible.org/kjv/judges/7)

<a name="judges_7_1"></a>Judges 7:1

Then [YᵊrubaʿAl](../../strongs/h/h3378.md), who is [GiḏʿÔn](../../strongs/h/h1439.md), and all the ['am](../../strongs/h/h5971.md) that were with him, [šāḵam](../../strongs/h/h7925.md), and [ḥānâ](../../strongs/h/h2583.md) beside the well of [ʿÊn Ḥarōḏ](../../strongs/h/h5878.md): so that the [maḥănê](../../strongs/h/h4264.md) of the [Miḏyān](../../strongs/h/h4080.md) were on the [ṣāp̄ôn](../../strongs/h/h6828.md) of them, by the [giḇʿâ](../../strongs/h/h1389.md) of [Môrê](../../strongs/h/h4176.md), in the [ʿēmeq](../../strongs/h/h6010.md).

<a name="judges_7_2"></a>Judges 7:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), The ['am](../../strongs/h/h5971.md) that are with thee are too [rab](../../strongs/h/h7227.md) for me to [nathan](../../strongs/h/h5414.md) the [Miḏyān](../../strongs/h/h4080.md) into their [yad](../../strongs/h/h3027.md), lest [Yisra'el](../../strongs/h/h3478.md) [pā'ar](../../strongs/h/h6286.md) themselves against me, ['āmar](../../strongs/h/h559.md), Mine own [yad](../../strongs/h/h3027.md) hath [yasha'](../../strongs/h/h3467.md) me.

<a name="judges_7_3"></a>Judges 7:3

Now therefore go [na'](../../strongs/h/h4994.md), [qara'](../../strongs/h/h7121.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Whosoever is [yārē'](../../strongs/h/h3373.md) and [ḥārēḏ](../../strongs/h/h2730.md), let him [shuwb](../../strongs/h/h7725.md) and depart [ṣāp̄ar](../../strongs/h/h6852.md) from [har](../../strongs/h/h2022.md) [Gilʿāḏ](../../strongs/h/h1568.md). And there [shuwb](../../strongs/h/h7725.md) of the ['am](../../strongs/h/h5971.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md) ['elep̄](../../strongs/h/h505.md); and there [šā'ar](../../strongs/h/h7604.md) [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md).

<a name="judges_7_4"></a>Judges 7:4

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), The ['am](../../strongs/h/h5971.md) are yet too [rab](../../strongs/h/h7227.md); bring them [yarad](../../strongs/h/h3381.md) unto the [mayim](../../strongs/h/h4325.md), and I will [tsaraph](../../strongs/h/h6884.md) them for thee there: and it shall be, that of whom I ['āmar](../../strongs/h/h559.md) unto thee, This shall [yālaḵ](../../strongs/h/h3212.md) with thee, the same shall [yālaḵ](../../strongs/h/h3212.md) with thee; and of whomsoever I ['āmar](../../strongs/h/h559.md) unto thee, This shall not [yālaḵ](../../strongs/h/h3212.md) with thee, the same shall not [yālaḵ](../../strongs/h/h3212.md).

<a name="judges_7_5"></a>Judges 7:5

So he [yarad](../../strongs/h/h3381.md) the ['am](../../strongs/h/h5971.md) unto the [mayim](../../strongs/h/h4325.md): and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), Every one that [lāqaq](../../strongs/h/h3952.md) of the [mayim](../../strongs/h/h4325.md) with his [lashown](../../strongs/h/h3956.md), as a [keleḇ](../../strongs/h/h3611.md) [lāqaq](../../strongs/h/h3952.md), him shalt thou [yāṣaḡ](../../strongs/h/h3322.md) by himself; likewise every one that boweth [kara'](../../strongs/h/h3766.md) upon his [bereḵ](../../strongs/h/h1290.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="judges_7_6"></a>Judges 7:6

And the [mispār](../../strongs/h/h4557.md) of them that [lāqaq](../../strongs/h/h3952.md), putting their [yad](../../strongs/h/h3027.md) to their [peh](../../strongs/h/h6310.md), were [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md): but all the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) bowed [kara'](../../strongs/h/h3766.md) upon their [bereḵ](../../strongs/h/h1290.md) to [šāṯâ](../../strongs/h/h8354.md) [mayim](../../strongs/h/h4325.md).

<a name="judges_7_7"></a>Judges 7:7

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), By the [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) that [lāqaq](../../strongs/h/h3952.md) will I [yasha'](../../strongs/h/h3467.md) you, and [nathan](../../strongs/h/h5414.md) the [Miḏyān](../../strongs/h/h4080.md) into thine [yad](../../strongs/h/h3027.md): and let all the other ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) unto his [maqowm](../../strongs/h/h4725.md).

<a name="judges_7_8"></a>Judges 7:8

So the ['am](../../strongs/h/h5971.md) [laqach](../../strongs/h/h3947.md) [ṣêḏâ](../../strongs/h/h6720.md) in their [yad](../../strongs/h/h3027.md), and their [šôp̄ār](../../strongs/h/h7782.md): and he [shalach](../../strongs/h/h7971.md) ['iysh](../../strongs/h/h376.md) the rest of [Yisra'el](../../strongs/h/h3478.md) every ['iysh](../../strongs/h/h376.md) unto his ['ohel](../../strongs/h/h168.md), and [ḥāzaq](../../strongs/h/h2388.md) those [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md): and the [maḥănê](../../strongs/h/h4264.md) of [Miḏyān](../../strongs/h/h4080.md) was beneath him in the [ʿēmeq](../../strongs/h/h6010.md).

<a name="judges_7_9"></a>Judges 7:9

And it came to pass the same [layil](../../strongs/h/h3915.md), that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [quwm](../../strongs/h/h6965.md), get thee [yarad](../../strongs/h/h3381.md) unto the [maḥănê](../../strongs/h/h4264.md); for I have [nathan](../../strongs/h/h5414.md) it into thine [yad](../../strongs/h/h3027.md).

<a name="judges_7_10"></a>Judges 7:10

But if thou [yārē'](../../strongs/h/h3373.md) to [yarad](../../strongs/h/h3381.md), [yarad](../../strongs/h/h3381.md) thou with [Purâ](../../strongs/h/h6513.md) thy [naʿar](../../strongs/h/h5288.md) [yarad](../../strongs/h/h3381.md) to the [maḥănê](../../strongs/h/h4264.md):

<a name="judges_7_11"></a>Judges 7:11

And thou shalt [shama'](../../strongs/h/h8085.md) what they [dabar](../../strongs/h/h1696.md); and ['aḥar](../../strongs/h/h310.md) shall thine [yad](../../strongs/h/h3027.md) be [ḥāzaq](../../strongs/h/h2388.md) to [yarad](../../strongs/h/h3381.md) unto the [maḥănê](../../strongs/h/h4264.md). Then went he [yarad](../../strongs/h/h3381.md) with [Purâ](../../strongs/h/h6513.md) his [naʿar](../../strongs/h/h5288.md) unto the [qāṣê](../../strongs/h/h7097.md) of the [ḥāmaš](../../strongs/h/h2571.md) that were in the [maḥănê](../../strongs/h/h4264.md).

<a name="judges_7_12"></a>Judges 7:12

And the [Miḏyān](../../strongs/h/h4080.md) and the [ʿĂmālēq](../../strongs/h/h6002.md) and all the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md) [naphal](../../strongs/h/h5307.md) along in the [ʿēmeq](../../strongs/h/h6010.md) like ['arbê](../../strongs/h/h697.md) for [rōḇ](../../strongs/h/h7230.md); and their [gāmāl](../../strongs/h/h1581.md) were without [mispār](../../strongs/h/h4557.md), as the [ḥôl](../../strongs/h/h2344.md) by the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="judges_7_13"></a>Judges 7:13

And when [GiḏʿÔn](../../strongs/h/h1439.md) was [bow'](../../strongs/h/h935.md), behold, there was an ['iysh](../../strongs/h/h376.md) that [sāp̄ar](../../strongs/h/h5608.md) a [ḥălôm](../../strongs/h/h2472.md) unto his [rea'](../../strongs/h/h7453.md), and ['āmar](../../strongs/h/h559.md), Behold, I [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md), and, lo, a [ṣᵊlûl](../../strongs/h/h6742.md) [ṣᵊlûl](../../strongs/h/h6742.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [lechem](../../strongs/h/h3899.md) [hāp̄aḵ](../../strongs/h/h2015.md) into the [maḥănê](../../strongs/h/h4264.md) of [Miḏyān](../../strongs/h/h4080.md), and [bow'](../../strongs/h/h935.md) unto a ['ohel](../../strongs/h/h168.md), and [nakah](../../strongs/h/h5221.md) it that it [naphal](../../strongs/h/h5307.md), and [hāp̄aḵ](../../strongs/h/h2015.md) [maʿal](../../strongs/h/h4605.md) it, that the ['ohel](../../strongs/h/h168.md) lay [naphal](../../strongs/h/h5307.md).

<a name="judges_7_14"></a>Judges 7:14

And his [rea'](../../strongs/h/h7453.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), This is nothing else save the [chereb](../../strongs/h/h2719.md) of [GiḏʿÔn](../../strongs/h/h1439.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md), an ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md): for into his [yad](../../strongs/h/h3027.md) hath ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) [Miḏyān](../../strongs/h/h4080.md), and all the [maḥănê](../../strongs/h/h4264.md).

<a name="judges_7_15"></a>Judges 7:15

And it was so, when [GiḏʿÔn](../../strongs/h/h1439.md) [shama'](../../strongs/h/h8085.md) the [mispār](../../strongs/h/h4557.md) of the [ḥălôm](../../strongs/h/h2472.md), and the [šeḇar](../../strongs/h/h7667.md) thereof, that he [shachah](../../strongs/h/h7812.md), and [shuwb](../../strongs/h/h7725.md) into the [maḥănê](../../strongs/h/h4264.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) into your [yad](../../strongs/h/h3027.md) the [maḥănê](../../strongs/h/h4264.md) of [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_7_16"></a>Judges 7:16

And he [ḥāṣâ](../../strongs/h/h2673.md) the [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) into [šālôš](../../strongs/h/h7969.md) [ro'sh](../../strongs/h/h7218.md), and he [nathan](../../strongs/h/h5414.md) a [šôp̄ār](../../strongs/h/h7782.md) in every man's [yad](../../strongs/h/h3027.md), with [reyq](../../strongs/h/h7386.md) [kaḏ](../../strongs/h/h3537.md), and [lapîḏ](../../strongs/h/h3940.md) [tavek](../../strongs/h/h8432.md) the [kaḏ](../../strongs/h/h3537.md).

<a name="judges_7_17"></a>Judges 7:17

And he ['āmar](../../strongs/h/h559.md) unto them, [ra'ah](../../strongs/h/h7200.md) on me, and ['asah](../../strongs/h/h6213.md) likewise: and, behold, when I [bow'](../../strongs/h/h935.md) to the [qāṣê](../../strongs/h/h7097.md) of the [maḥănê](../../strongs/h/h4264.md), it shall be that, as I ['asah](../../strongs/h/h6213.md), so shall ye ['asah](../../strongs/h/h6213.md).

<a name="judges_7_18"></a>Judges 7:18

When I [tāqaʿ](../../strongs/h/h8628.md) with a [šôp̄ār](../../strongs/h/h7782.md), I and all that are with me, then [tāqaʿ](../../strongs/h/h8628.md) ye the [šôp̄ār](../../strongs/h/h7782.md) also on every [cabiyb](../../strongs/h/h5439.md) of all the [maḥănê](../../strongs/h/h4264.md), and ['āmar](../../strongs/h/h559.md), The sword of [Yĕhovah](../../strongs/h/h3068.md), and of [GiḏʿÔn](../../strongs/h/h1439.md).

<a name="judges_7_19"></a>Judges 7:19

So [GiḏʿÔn](../../strongs/h/h1439.md), and the [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) that were with him, [bow'](../../strongs/h/h935.md) unto the [qāṣê](../../strongs/h/h7097.md) of the [maḥănê](../../strongs/h/h4264.md) in the [ro'sh](../../strongs/h/h7218.md) of the [tîḵôn](../../strongs/h/h8484.md) ['ašmurâ](../../strongs/h/h821.md); and they had ['aḵ](../../strongs/h/h389.md) [quwm](../../strongs/h/h6965.md) [quwm](../../strongs/h/h6965.md) the [shamar](../../strongs/h/h8104.md): and they [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and [naphats](../../strongs/h/h5310.md) the [kaḏ](../../strongs/h/h3537.md) that were in their [yad](../../strongs/h/h3027.md).

<a name="judges_7_20"></a>Judges 7:20

And the [šālôš](../../strongs/h/h7969.md) [ro'sh](../../strongs/h/h7218.md) [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and [shabar](../../strongs/h/h7665.md) the [kaḏ](../../strongs/h/h3537.md), and [ḥāzaq](../../strongs/h/h2388.md) the [lapîḏ](../../strongs/h/h3940.md) in their [śᵊmō'l](../../strongs/h/h8040.md) [yad](../../strongs/h/h3027.md), and the [šôp̄ār](../../strongs/h/h7782.md) in their [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md) to [tāqaʿ](../../strongs/h/h8628.md) withal: and they [qara'](../../strongs/h/h7121.md), The [chereb](../../strongs/h/h2719.md) of [Yĕhovah](../../strongs/h/h3068.md), and of [GiḏʿÔn](../../strongs/h/h1439.md).

<a name="judges_7_21"></a>Judges 7:21

And they ['amad](../../strongs/h/h5975.md) every ['iysh](../../strongs/h/h376.md) in his place [cabiyb](../../strongs/h/h5439.md) the [maḥănê](../../strongs/h/h4264.md): and all the [maḥănê](../../strongs/h/h4264.md) [rûṣ](../../strongs/h/h7323.md), and [rûaʿ](../../strongs/h/h7321.md), and [nûs](../../strongs/h/h5127.md) [nûs](../../strongs/h/h5127.md).

<a name="judges_7_22"></a>Judges 7:22

And the [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md), and [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) every ['iysh](../../strongs/h/h376.md) [chereb](../../strongs/h/h2719.md) against his [rea'](../../strongs/h/h7453.md), even throughout all the [maḥănê](../../strongs/h/h4264.md): and the [maḥănê](../../strongs/h/h4264.md) [nûs](../../strongs/h/h5127.md) to [Bêṯ Hašiṭṭâ](../../strongs/h/h1029.md) in [Ṣᵊrērâ](../../strongs/h/h6888.md), and to the [saphah](../../strongs/h/h8193.md) of ['Āḇēl Mᵊḥôlâ](../../strongs/h/h65.md), unto [Ṭabāṯ](../../strongs/h/h2888.md).

<a name="judges_7_23"></a>Judges 7:23

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) gathered themselves [ṣāʿaq](../../strongs/h/h6817.md) out of [Nap̄tālî](../../strongs/h/h5321.md), and out of ['Āšēr](../../strongs/h/h836.md), and out of all [Mᵊnaššê](../../strongs/h/h4519.md), and [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_7_24"></a>Judges 7:24

And [GiḏʿÔn](../../strongs/h/h1439.md) [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) throughout all [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), ['āmar](../../strongs/h/h559.md), [yarad](../../strongs/h/h3381.md) [qārā'](../../strongs/h/h7125.md) the [Miḏyān](../../strongs/h/h4080.md), and [lāḵaḏ](../../strongs/h/h3920.md) before them the [mayim](../../strongs/h/h4325.md) unto [Bêṯ Bārâ](../../strongs/h/h1012.md) and [Yardēn](../../strongs/h/h3383.md). Then all the ['iysh](../../strongs/h/h376.md) of ['Ep̄rayim](../../strongs/h/h669.md) gathered themselves [ṣāʿaq](../../strongs/h/h6817.md), and [lāḵaḏ](../../strongs/h/h3920.md) the [mayim](../../strongs/h/h4325.md) unto [Bêṯ Bārâ](../../strongs/h/h1012.md) and [Yardēn](../../strongs/h/h3383.md).

<a name="judges_7_25"></a>Judges 7:25

And they [lāḵaḏ](../../strongs/h/h3920.md) [šᵊnayim](../../strongs/h/h8147.md) [śar](../../strongs/h/h8269.md) of the [Miḏyān](../../strongs/h/h4080.md), [ʿōrēḇ](../../strongs/h/h6159.md) and [Zᵊ'Ēḇ](../../strongs/h/h2062.md); and they [harag](../../strongs/h/h2026.md) [ʿōrēḇ](../../strongs/h/h6159.md) upon the [tsuwr](../../strongs/h/h6697.md) [ʿōrēḇ](../../strongs/h/h6159.md), and [Zᵊ'Ēḇ](../../strongs/h/h2062.md) they [harag](../../strongs/h/h2026.md) at the [yeqeḇ](../../strongs/h/h3342.md) of [Zᵊ'Ēḇ](../../strongs/h/h2062.md), and [radaph](../../strongs/h/h7291.md) [Miḏyān](../../strongs/h/h4080.md), and [bow'](../../strongs/h/h935.md) the [ro'sh](../../strongs/h/h7218.md) of [ʿōrēḇ](../../strongs/h/h6159.md) and [Zᵊ'Ēḇ](../../strongs/h/h2062.md) to [GiḏʿÔn](../../strongs/h/h1439.md) on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 6](judges_6.md) - [Judges 8](judges_8.md)