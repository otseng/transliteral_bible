# [Judges 15](https://www.blueletterbible.org/kjv/judges/15)

<a name="judges_15_1"></a>Judges 15:1

But it came to pass within a while [yowm](../../strongs/h/h3117.md), in the [yowm](../../strongs/h/h3117.md) of [ḥiṭṭâ](../../strongs/h/h2406.md) [qāṣîr](../../strongs/h/h7105.md), that [Šimšôn](../../strongs/h/h8123.md) [paqad](../../strongs/h/h6485.md) his ['ishshah](../../strongs/h/h802.md) with a [gᵊḏî](../../strongs/h/h1423.md) [ʿēz](../../strongs/h/h5795.md); and he ['āmar](../../strongs/h/h559.md), I will go [bow'](../../strongs/h/h935.md) to my ['ishshah](../../strongs/h/h802.md) into the [ḥeḏer](../../strongs/h/h2315.md). But her ['ab](../../strongs/h/h1.md) would not [nathan](../../strongs/h/h5414.md) him to go [bow'](../../strongs/h/h935.md).

<a name="judges_15_2"></a>Judges 15:2

And her ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md), I ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) that thou hadst [sane'](../../strongs/h/h8130.md) [sane'](../../strongs/h/h8130.md) her; therefore I [nathan](../../strongs/h/h5414.md) her to thy [mērēaʿ](../../strongs/h/h4828.md): is not her [qāṭān](../../strongs/h/h6996.md) ['āḥôṯ](../../strongs/h/h269.md) [towb](../../strongs/h/h2896.md) than she? take her, I pray thee, instead of her.

<a name="judges_15_3"></a>Judges 15:3

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) concerning them, [pa'am](../../strongs/h/h6471.md) shall I be more [naqah](../../strongs/h/h5352.md) than the [Pᵊlištî](../../strongs/h/h6430.md), though I ['asah](../../strongs/h/h6213.md) them a [ra'](../../strongs/h/h7451.md).

<a name="judges_15_4"></a>Judges 15:4

And [Šimšôn](../../strongs/h/h8123.md) [yālaḵ](../../strongs/h/h3212.md) and [lāḵaḏ](../../strongs/h/h3920.md) [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) [šûʿāl](../../strongs/h/h7776.md), and [laqach](../../strongs/h/h3947.md) [lapîḏ](../../strongs/h/h3940.md), and [panah](../../strongs/h/h6437.md) [zānāḇ](../../strongs/h/h2180.md) to [zānāḇ](../../strongs/h/h2180.md), and [śûm](../../strongs/h/h7760.md) ['echad](../../strongs/h/h259.md) [lapîḏ](../../strongs/h/h3940.md) in the [tavek](../../strongs/h/h8432.md) between [šᵊnayim](../../strongs/h/h8147.md) [zānāḇ](../../strongs/h/h2180.md).

<a name="judges_15_5"></a>Judges 15:5

And when he had [bāʿar](../../strongs/h/h1197.md) the [lapîḏ](../../strongs/h/h3940.md) on ['esh](../../strongs/h/h784.md), he let them [shalach](../../strongs/h/h7971.md) into the [qāmâ](../../strongs/h/h7054.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and [bāʿar](../../strongs/h/h1197.md) both the [gāḏîš](../../strongs/h/h1430.md), and also the [qāmâ](../../strongs/h/h7054.md), with the [kerem](../../strongs/h/h3754.md) and [zayiṯ](../../strongs/h/h2132.md).

<a name="judges_15_6"></a>Judges 15:6

Then the [Pᵊlištî](../../strongs/h/h6430.md) ['āmar](../../strongs/h/h559.md), Who hath ['asah](../../strongs/h/h6213.md) this? And they ['āmar](../../strongs/h/h559.md), [Šimšôn](../../strongs/h/h8123.md), the son in [ḥāṯān](../../strongs/h/h2860.md) of the [timnāy](../../strongs/h/h8554.md), because he had [laqach](../../strongs/h/h3947.md) his ['ishshah](../../strongs/h/h802.md), and [nathan](../../strongs/h/h5414.md) her to his [mērēaʿ](../../strongs/h/h4828.md). And the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md), and [śārap̄](../../strongs/h/h8313.md) her and her ['ab](../../strongs/h/h1.md) with ['esh](../../strongs/h/h784.md).

<a name="judges_15_7"></a>Judges 15:7

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto them, ['im](../../strongs/h/h518.md) ye have ['asah](../../strongs/h/h6213.md) [zō'ṯ](../../strongs/h/h2063.md), yet will I be [naqam](../../strongs/h/h5358.md) of you, and ['aḥar](../../strongs/h/h310.md) that I will [ḥāḏal](../../strongs/h/h2308.md).

<a name="judges_15_8"></a>Judges 15:8

And he [nakah](../../strongs/h/h5221.md) them [šôq](../../strongs/h/h7785.md) [ʿal](../../strongs/h/h5921.md) [yārēḵ](../../strongs/h/h3409.md) with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md): and he [yarad](../../strongs/h/h3381.md) and [yashab](../../strongs/h/h3427.md) in the [sᵊʿîp̄](../../strongs/h/h5585.md) of the [cela'](../../strongs/h/h5553.md) [ʿÊṭām](../../strongs/h/h5862.md).

<a name="judges_15_9"></a>Judges 15:9

Then the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md), and [ḥānâ](../../strongs/h/h2583.md) in [Yehuwdah](../../strongs/h/h3063.md), and [nāṭaš](../../strongs/h/h5203.md) themselves in [Lᵊḥî](../../strongs/h/h3896.md).

<a name="judges_15_10"></a>Judges 15:10

And the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) ['āmar](../../strongs/h/h559.md), Why are ye [ʿālâ](../../strongs/h/h5927.md) against us? And they ['āmar](../../strongs/h/h559.md), To ['āsar](../../strongs/h/h631.md) [Šimšôn](../../strongs/h/h8123.md) are we [ʿālâ](../../strongs/h/h5927.md), to ['asah](../../strongs/h/h6213.md) to him as he hath ['asah](../../strongs/h/h6213.md) to us.

<a name="judges_15_11"></a>Judges 15:11

Then [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) [yarad](../../strongs/h/h3381.md) to the [sᵊʿîp̄](../../strongs/h/h5585.md) of the [cela'](../../strongs/h/h5553.md) [ʿÊṭām](../../strongs/h/h5862.md), and ['āmar](../../strongs/h/h559.md) to [Šimšôn](../../strongs/h/h8123.md), [yada'](../../strongs/h/h3045.md) thou not that the [Pᵊlištî](../../strongs/h/h6430.md) are [mashal](../../strongs/h/h4910.md) over us? what is this that thou hast ['asah](../../strongs/h/h6213.md) unto us? And he ['āmar](../../strongs/h/h559.md) unto them, As they ['asah](../../strongs/h/h6213.md) unto me, so have I ['asah](../../strongs/h/h6213.md) unto them.

<a name="judges_15_12"></a>Judges 15:12

And they ['āmar](../../strongs/h/h559.md) unto him, We are [yarad](../../strongs/h/h3381.md) to ['āsar](../../strongs/h/h631.md) thee, that we may [nathan](../../strongs/h/h5414.md) thee into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md). And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto them, [shaba'](../../strongs/h/h7650.md) unto me, that ye will not [pāḡaʿ](../../strongs/h/h6293.md) me yourselves.

<a name="judges_15_13"></a>Judges 15:13

And they ['āmar](../../strongs/h/h559.md) unto him, ['āmar](../../strongs/h/h559.md), No; but we will ['āsar](../../strongs/h/h631.md) thee ['āsar](../../strongs/h/h631.md), and [nathan](../../strongs/h/h5414.md) thee into their [yad](../../strongs/h/h3027.md): but [muwth](../../strongs/h/h4191.md) we will not [muwth](../../strongs/h/h4191.md) thee. And they ['āsar](../../strongs/h/h631.md) him with [šᵊnayim](../../strongs/h/h8147.md) [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḇōṯ](../../strongs/h/h5688.md), and [ʿālâ](../../strongs/h/h5927.md) him from the [cela'](../../strongs/h/h5553.md).

<a name="judges_15_14"></a>Judges 15:14

And when he [bow'](../../strongs/h/h935.md) unto [Lᵊḥî](../../strongs/h/h3896.md), the [Pᵊlištî](../../strongs/h/h6430.md) [rûaʿ](../../strongs/h/h7321.md) [qārā'](../../strongs/h/h7125.md) him: and the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsalach](../../strongs/h/h6743.md) upon him, and the [ʿăḇōṯ](../../strongs/h/h5688.md) that were upon his [zerowa'](../../strongs/h/h2220.md) became as [pēšeṯ](../../strongs/h/h6593.md) that was [bāʿar](../../strongs/h/h1197.md) with ['esh](../../strongs/h/h784.md), and his ['ēsûr](../../strongs/h/h612.md) [māsas](../../strongs/h/h4549.md) from off his [yad](../../strongs/h/h3027.md).

<a name="judges_15_15"></a>Judges 15:15

And he [māṣā'](../../strongs/h/h4672.md) a [ṭārî](../../strongs/h/h2961.md) [lᵊḥî](../../strongs/h/h3895.md) of a [chamowr](../../strongs/h/h2543.md), and [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) it, and [nakah](../../strongs/h/h5221.md) an ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) therewith.

<a name="judges_15_16"></a>Judges 15:16

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md), With the [lᵊḥî](../../strongs/h/h3895.md) of a [chamowr](../../strongs/h/h2543.md), [ḥămōrâ](../../strongs/h/h2565.md) upon [ḥămōrâ](../../strongs/h/h2565.md), with the [lᵊḥî](../../strongs/h/h3895.md) of a [chamowr](../../strongs/h/h2543.md) have I [nakah](../../strongs/h/h5221.md) an ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_15_17"></a>Judges 15:17

And it came to pass, when he had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md), that he [shalak](../../strongs/h/h7993.md) the [lᵊḥî](../../strongs/h/h3895.md) out of his [yad](../../strongs/h/h3027.md), and [qara'](../../strongs/h/h7121.md) that [maqowm](../../strongs/h/h4725.md) [Rāmaṯ Leḥî](../../strongs/h/h7437.md).

<a name="judges_15_18"></a>Judges 15:18

And he was [me'od](../../strongs/h/h3966.md) [ṣāmē'](../../strongs/h/h6770.md), and [qara'](../../strongs/h/h7121.md) on [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), Thou hast [nathan](../../strongs/h/h5414.md) this [gadowl](../../strongs/h/h1419.md) [tᵊšûʿâ](../../strongs/h/h8668.md) into the [yad](../../strongs/h/h3027.md) of thy ['ebed](../../strongs/h/h5650.md): and now shall I [muwth](../../strongs/h/h4191.md) for [ṣāmā'](../../strongs/h/h6772.md), and [naphal](../../strongs/h/h5307.md) into the [yad](../../strongs/h/h3027.md) of the [ʿārēl](../../strongs/h/h6189.md)?

<a name="judges_15_19"></a>Judges 15:19

But ['Elohiym](../../strongs/h/h430.md) [bāqaʿ](../../strongs/h/h1234.md) an hollow [maḵtēš](../../strongs/h/h4388.md) that was in the [lᵊḥî](../../strongs/h/h3895.md), and there [yāṣā'](../../strongs/h/h3318.md) [mayim](../../strongs/h/h4325.md) thereout; and when he had [šāṯâ](../../strongs/h/h8354.md), his [ruwach](../../strongs/h/h7307.md) came [shuwb](../../strongs/h/h7725.md), and he [ḥāyâ](../../strongs/h/h2421.md): wherefore he [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) thereof [ʿÊn Haqqôrē'](../../strongs/h/h5875.md), which is in [Lᵊḥî](../../strongs/h/h3896.md) unto this [yowm](../../strongs/h/h3117.md).

<a name="judges_15_20"></a>Judges 15:20

And he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) in the [yowm](../../strongs/h/h3117.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 14](judges_14.md) - [Judges 16](judges_16.md)