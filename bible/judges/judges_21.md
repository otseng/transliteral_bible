# [Judges 21](https://www.blueletterbible.org/kjv/judges/21)

<a name="judges_21_1"></a>Judges 21:1

Now the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) had [shaba'](../../strongs/h/h7650.md) in [Miṣpâ](../../strongs/h/h4709.md), ['āmar](../../strongs/h/h559.md), There shall not ['iysh](../../strongs/h/h376.md) of us [nathan](../../strongs/h/h5414.md) his [bath](../../strongs/h/h1323.md) unto [Binyāmîn](../../strongs/h/h1144.md) to ['ishshah](../../strongs/h/h802.md).

<a name="judges_21_2"></a>Judges 21:2

And the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), and [yashab](../../strongs/h/h3427.md) there till ['ereb](../../strongs/h/h6153.md) [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md), and [nasa'](../../strongs/h/h5375.md) their [qowl](../../strongs/h/h6963.md), and [bāḵâ](../../strongs/h/h1058.md) [bĕkiy](../../strongs/h/h1065.md) [gadowl](../../strongs/h/h1419.md);

<a name="judges_21_3"></a>Judges 21:3

And ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), why is this come to pass in [Yisra'el](../../strongs/h/h3478.md), that there should be to [yowm](../../strongs/h/h3117.md) ['echad](../../strongs/h/h259.md) [shebet](../../strongs/h/h7626.md) [paqad](../../strongs/h/h6485.md) in [Yisra'el](../../strongs/h/h3478.md)?

<a name="judges_21_4"></a>Judges 21:4

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that the ['am](../../strongs/h/h5971.md) [šāḵam](../../strongs/h/h7925.md), and [bānâ](../../strongs/h/h1129.md) there a [mizbeach](../../strongs/h/h4196.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md).

<a name="judges_21_5"></a>Judges 21:5

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), Who is there among all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) ['ăšer](../../strongs/h/h834.md) [ʿālâ](../../strongs/h/h5927.md) not up with the [qāhēl](../../strongs/h/h6951.md) unto [Yĕhovah](../../strongs/h/h3068.md)? For they had made a [gadowl](../../strongs/h/h1419.md) [šᵊḇûʿâ](../../strongs/h/h7621.md) concerning him that [ʿālâ](../../strongs/h/h5927.md) not to [Yĕhovah](../../strongs/h/h3068.md) to [Miṣpâ](../../strongs/h/h4709.md), ['āmar](../../strongs/h/h559.md), He shall [muwth](../../strongs/h/h4191.md) be put to [muwth](../../strongs/h/h4191.md).

<a name="judges_21_6"></a>Judges 21:6

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nacham](../../strongs/h/h5162.md) them for [Binyāmîn](../../strongs/h/h1144.md) their ['ach](../../strongs/h/h251.md), and ['āmar](../../strongs/h/h559.md), There is ['echad](../../strongs/h/h259.md) [shebet](../../strongs/h/h7626.md) [gāḏaʿ](../../strongs/h/h1438.md) from [Yisra'el](../../strongs/h/h3478.md) this [yowm](../../strongs/h/h3117.md).

<a name="judges_21_7"></a>Judges 21:7

How shall we ['asah](../../strongs/h/h6213.md) for ['ishshah](../../strongs/h/h802.md) for them that [yāṯar](../../strongs/h/h3498.md), seeing we have [shaba'](../../strongs/h/h7650.md) by [Yĕhovah](../../strongs/h/h3068.md) that we will not [nathan](../../strongs/h/h5414.md) them of our [bath](../../strongs/h/h1323.md) to ['ishshah](../../strongs/h/h802.md)?

<a name="judges_21_8"></a>Judges 21:8

And they ['āmar](../../strongs/h/h559.md), What ['echad](../../strongs/h/h259.md) is there of the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) that [ʿālâ](../../strongs/h/h5927.md) not to [Miṣpâ](../../strongs/h/h4709.md) to [Yĕhovah](../../strongs/h/h3068.md)? And, behold, there [bow'](../../strongs/h/h935.md) ['iysh](../../strongs/h/h376.md) to the [maḥănê](../../strongs/h/h4264.md) from [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) to the [qāhēl](../../strongs/h/h6951.md).

<a name="judges_21_9"></a>Judges 21:9

For the ['am](../../strongs/h/h5971.md) were [paqad](../../strongs/h/h6485.md), and, behold, there were ['iysh](../../strongs/h/h376.md) of the [yashab](../../strongs/h/h3427.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) there.

<a name="judges_21_10"></a>Judges 21:10

And the ['edah](../../strongs/h/h5712.md) [shalach](../../strongs/h/h7971.md) thither [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) [ḥayil](../../strongs/h/h2428.md), and [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and [nakah](../../strongs/h/h5221.md) the [yashab](../../strongs/h/h3427.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), with the ['ishshah](../../strongs/h/h802.md) and the [ṭap̄](../../strongs/h/h2945.md).

<a name="judges_21_11"></a>Judges 21:11

And this is the [dabar](../../strongs/h/h1697.md) that ye shall ['asah](../../strongs/h/h6213.md), Ye shall [ḥāram](../../strongs/h/h2763.md) every [zāḵār](../../strongs/h/h2145.md), and every ['ishshah](../../strongs/h/h802.md) that hath [miškāḇ](../../strongs/h/h4904.md) by [yada'](../../strongs/h/h3045.md).

<a name="judges_21_12"></a>Judges 21:12

And they [māṣā'](../../strongs/h/h4672.md) among the [yashab](../../strongs/h/h3427.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md) ['arbaʿ](../../strongs/h/h702.md) [mē'â](../../strongs/h/h3967.md) [naʿărâ](../../strongs/h/h5291.md) [bᵊṯûlâ](../../strongs/h/h1330.md), that had [yada'](../../strongs/h/h3045.md) no ['iysh](../../strongs/h/h376.md) by [miškāḇ](../../strongs/h/h4904.md) with any [zāḵār](../../strongs/h/h2145.md): and they [bow'](../../strongs/h/h935.md) them unto the [maḥănê](../../strongs/h/h4264.md) to [Šîlô](../../strongs/h/h7887.md), which is in the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md).

<a name="judges_21_13"></a>Judges 21:13

And the ['edah](../../strongs/h/h5712.md) [shalach](../../strongs/h/h7971.md) some to [dabar](../../strongs/h/h1696.md) to the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) that were in the [cela'](../../strongs/h/h5553.md) [Rimmôn](../../strongs/h/h7417.md), and to [qara'](../../strongs/h/h7121.md) [shalowm](../../strongs/h/h7965.md) unto them.

<a name="judges_21_14"></a>Judges 21:14

And [Binyāmîn](../../strongs/h/h1144.md) [shuwb](../../strongs/h/h7725.md) at that [ʿēṯ](../../strongs/h/h6256.md); and they [nathan](../../strongs/h/h5414.md) them ['ishshah](../../strongs/h/h802.md) which they had [ḥāyâ](../../strongs/h/h2421.md) of the ['ishshah](../../strongs/h/h802.md) of [Yāḇēš](../../strongs/h/h3003.md) [Gilʿāḏ](../../strongs/h/h1568.md): and yet so they [māṣā'](../../strongs/h/h4672.md) them not.

<a name="judges_21_15"></a>Judges 21:15

And the ['am](../../strongs/h/h5971.md) [nacham](../../strongs/h/h5162.md) them for [Binyāmîn](../../strongs/h/h1144.md), because that [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) a [pereṣ](../../strongs/h/h6556.md) in the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_21_16"></a>Judges 21:16

Then the [zāqēn](../../strongs/h/h2205.md) of the ['edah](../../strongs/h/h5712.md) ['āmar](../../strongs/h/h559.md), How shall we ['asah](../../strongs/h/h6213.md) for ['ishshah](../../strongs/h/h802.md) for them that [yāṯar](../../strongs/h/h3498.md), seeing the ['ishshah](../../strongs/h/h802.md) are [šāmaḏ](../../strongs/h/h8045.md) out of [Binyāmîn](../../strongs/h/h1144.md)?

<a name="judges_21_17"></a>Judges 21:17

And they ['āmar](../../strongs/h/h559.md), There must be a [yᵊruššâ](../../strongs/h/h3425.md) for them that be [pᵊlêṭâ](../../strongs/h/h6413.md) of [Binyāmîn](../../strongs/h/h1144.md), that a [shebet](../../strongs/h/h7626.md) be not [māḥâ](../../strongs/h/h4229.md) out of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_21_18"></a>Judges 21:18

Howbeit we [yakol](../../strongs/h/h3201.md) not [nathan](../../strongs/h/h5414.md) them ['ishshah](../../strongs/h/h802.md) of our [bath](../../strongs/h/h1323.md): for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have [shaba'](../../strongs/h/h7650.md), ['āmar](../../strongs/h/h559.md), ['arar](../../strongs/h/h779.md) be he that [nathan](../../strongs/h/h5414.md) an ['ishshah](../../strongs/h/h802.md) to [Binyāmîn](../../strongs/h/h1144.md).

<a name="judges_21_19"></a>Judges 21:19

Then they ['āmar](../../strongs/h/h559.md), Behold, there is a [ḥāḡ](../../strongs/h/h2282.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Šîlô](../../strongs/h/h7887.md) [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) in a place which is on the [ṣāp̄ôn](../../strongs/h/h6828.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md), on the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md) of the [mĕcillah](../../strongs/h/h4546.md) that [ʿālâ](../../strongs/h/h5927.md) from [Bêṯ-'ēl](../../strongs/h/h1008.md) to [Šᵊḵem](../../strongs/h/h7927.md), and on the [neḡeḇ](../../strongs/h/h5045.md) of [Lᵊḇônâ](../../strongs/h/h3829.md).

<a name="judges_21_20"></a>Judges 21:20

Therefore they [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) and ['arab](../../strongs/h/h693.md) in the [kerem](../../strongs/h/h3754.md);

<a name="judges_21_21"></a>Judges 21:21

And [ra'ah](../../strongs/h/h7200.md), and, behold, if the [bath](../../strongs/h/h1323.md) of [Šîlô](../../strongs/h/h7887.md) [yāṣā'](../../strongs/h/h3318.md) to [chuwl](../../strongs/h/h2342.md) in [mᵊḥōlâ](../../strongs/h/h4246.md), then come ye [yāṣā'](../../strongs/h/h3318.md) of the [kerem](../../strongs/h/h3754.md), and [ḥāṭap̄](../../strongs/h/h2414.md) you every ['iysh](../../strongs/h/h376.md) his ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Šîlô](../../strongs/h/h7887.md), and [halak](../../strongs/h/h1980.md) to the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md).

<a name="judges_21_22"></a>Judges 21:22

And it shall be, when their ['ab](../../strongs/h/h1.md) or their ['ach](../../strongs/h/h251.md) [bow'](../../strongs/h/h935.md) unto us to [riyb](../../strongs/h/h7378.md), that we will ['āmar](../../strongs/h/h559.md) unto them, Be [chanan](../../strongs/h/h2603.md) unto them for our sakes: because we [laqach](../../strongs/h/h3947.md) not to each ['iysh](../../strongs/h/h376.md) his ['ishshah](../../strongs/h/h802.md) in the [milḥāmâ](../../strongs/h/h4421.md): for ye did not [nathan](../../strongs/h/h5414.md) unto them at this [ʿēṯ](../../strongs/h/h6256.md), that ye should be ['asham](../../strongs/h/h816.md).

<a name="judges_21_23"></a>Judges 21:23

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) did ['asah](../../strongs/h/h6213.md), and [nasa'](../../strongs/h/h5375.md) them ['ishshah](../../strongs/h/h802.md), according to their [mispār](../../strongs/h/h4557.md), of them that [chuwl](../../strongs/h/h2342.md), whom they [gāzal](../../strongs/h/h1497.md): and they [yālaḵ](../../strongs/h/h3212.md) and [shuwb](../../strongs/h/h7725.md) unto their [nachalah](../../strongs/h/h5159.md), and [bānâ](../../strongs/h/h1129.md) the [ʿîr](../../strongs/h/h5892.md), and [yashab](../../strongs/h/h3427.md) in them.

<a name="judges_21_24"></a>Judges 21:24

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [halak](../../strongs/h/h1980.md) thence at that [ʿēṯ](../../strongs/h/h6256.md), every ['iysh](../../strongs/h/h376.md) to his [shebet](../../strongs/h/h7626.md) and to his [mišpāḥâ](../../strongs/h/h4940.md), and they [yāṣā'](../../strongs/h/h3318.md) from thence every ['iysh](../../strongs/h/h376.md) to his [nachalah](../../strongs/h/h5159.md).

<a name="judges_21_25"></a>Judges 21:25

In those [yowm](../../strongs/h/h3117.md) there was no [melek](../../strongs/h/h4428.md) in [Yisra'el](../../strongs/h/h3478.md): every ['iysh](../../strongs/h/h376.md) ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in his own ['ayin](../../strongs/h/h5869.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 20](judges_20.md)