# [Judges 10](https://www.blueletterbible.org/kjv/judges/10)

<a name="judges_10_1"></a>Judges 10:1

And ['aḥar](../../strongs/h/h310.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md) there [quwm](../../strongs/h/h6965.md) to [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) [Tôlāʿ](../../strongs/h/h8439.md) the [ben](../../strongs/h/h1121.md) of [pû'â](../../strongs/h/h6312.md), the [ben](../../strongs/h/h1121.md) of [Dôḏô](../../strongs/h/h1734.md), an ['iysh](../../strongs/h/h376.md) of [Yiśśāśḵār](../../strongs/h/h3485.md); and he [yashab](../../strongs/h/h3427.md) in [Šāmîr](../../strongs/h/h8069.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md).

<a name="judges_10_2"></a>Judges 10:2

And he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šālôš](../../strongs/h/h7969.md) [šānâ](../../strongs/h/h8141.md), and [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in [Šāmîr](../../strongs/h/h8069.md).

<a name="judges_10_3"></a>Judges 10:3

And ['aḥar](../../strongs/h/h310.md) him [quwm](../../strongs/h/h6965.md) [Yā'Îr](../../strongs/h/h2971.md), a [Gilʿāḏî](../../strongs/h/h1569.md), and [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_10_4"></a>Judges 10:4

And he had [šᵊlōšîm](../../strongs/h/h7970.md) [ben](../../strongs/h/h1121.md) that [rāḵaḇ](../../strongs/h/h7392.md) on [šᵊlōšîm](../../strongs/h/h7970.md) ass [ʿayir](../../strongs/h/h5895.md), and they had [šᵊlōšîm](../../strongs/h/h7970.md) [ʿîr](../../strongs/h/h5892.md), which are [qara'](../../strongs/h/h7121.md) [Ḥaûôṯ YāʿÎr](../../strongs/h/h2334.md) unto this [yowm](../../strongs/h/h3117.md), which are in the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="judges_10_5"></a>Judges 10:5

And [Yā'Îr](../../strongs/h/h2971.md) [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in [Qāmôn](../../strongs/h/h7056.md).

<a name="judges_10_6"></a>Judges 10:6

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) [yāsap̄](../../strongs/h/h3254.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md), and [ʿAštārōṯ](../../strongs/h/h6252.md), and the ['Elohiym](../../strongs/h/h430.md) of ['Ărām](../../strongs/h/h758.md), and the ['Elohiym](../../strongs/h/h430.md) of [Ṣîḏôn](../../strongs/h/h6721.md), and the ['Elohiym](../../strongs/h/h430.md) of [Mô'āḇ](../../strongs/h/h4124.md), and the ['Elohiym](../../strongs/h/h430.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and the ['Elohiym](../../strongs/h/h430.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), and ['abad](../../strongs/h/h5647.md) not him.

<a name="judges_10_7"></a>Judges 10:7

And the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he [māḵar](../../strongs/h/h4376.md) them into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and into the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="judges_10_8"></a>Judges 10:8

And that [šānâ](../../strongs/h/h8141.md) they [rāʿaṣ](../../strongs/h/h7492.md) and [rāṣaṣ](../../strongs/h/h7533.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md), all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that were on the other [ʿēḇer](../../strongs/h/h5676.md) [Yardēn](../../strongs/h/h3383.md) in the ['erets](../../strongs/h/h776.md) of the ['Ĕmōrî](../../strongs/h/h567.md), which is in [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="judges_10_9"></a>Judges 10:9

Moreover the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) ['abar](../../strongs/h/h5674.md) [Yardēn](../../strongs/h/h3383.md) to [lāḥam](../../strongs/h/h3898.md) also against [Yehuwdah](../../strongs/h/h3063.md), and against [Binyāmîn](../../strongs/h/h1144.md), and against the [bayith](../../strongs/h/h1004.md) of ['Ep̄rayim](../../strongs/h/h669.md); so that [Yisra'el](../../strongs/h/h3478.md) was [me'od](../../strongs/h/h3966.md) [yāṣar](../../strongs/h/h3334.md).

<a name="judges_10_10"></a>Judges 10:10

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), We have [chata'](../../strongs/h/h2398.md) against thee, both because we have ['azab](../../strongs/h/h5800.md) our ['Elohiym](../../strongs/h/h430.md), and also ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md).

<a name="judges_10_11"></a>Judges 10:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), not from the [Mitsrayim](../../strongs/h/h4714.md), and from the ['Ĕmōrî](../../strongs/h/h567.md), from the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and from the [Pᵊlištî](../../strongs/h/h6430.md)?

<a name="judges_10_12"></a>Judges 10:12

The [Ṣîḏōnî](../../strongs/h/h6722.md) also, and the [ʿĂmālēq](../../strongs/h/h6002.md), and the [MāʿÔn](../../strongs/h/h4584.md), did [lāḥaṣ](../../strongs/h/h3905.md) you; and ye [ṣāʿaq](../../strongs/h/h6817.md) to me, and I [yasha'](../../strongs/h/h3467.md) you out of their [yad](../../strongs/h/h3027.md).

<a name="judges_10_13"></a>Judges 10:13

Yet ye have ['azab](../../strongs/h/h5800.md) me, and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md): wherefore I will [yasha'](../../strongs/h/h3467.md) you no [yāsap̄](../../strongs/h/h3254.md).

<a name="judges_10_14"></a>Judges 10:14

[yālaḵ](../../strongs/h/h3212.md) and [zāʿaq](../../strongs/h/h2199.md) unto the ['Elohiym](../../strongs/h/h430.md) which ye have [bāḥar](../../strongs/h/h977.md); let [hēm](../../strongs/h/h1992.md) [yasha'](../../strongs/h/h3467.md) you in the [ʿēṯ](../../strongs/h/h6256.md) of your [tsarah](../../strongs/h/h6869.md).

<a name="judges_10_15"></a>Judges 10:15

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yĕhovah](../../strongs/h/h3068.md), We have [chata'](../../strongs/h/h2398.md): ['asah](../../strongs/h/h6213.md) thou unto us whatsoever ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto thee; [natsal](../../strongs/h/h5337.md) us only, we pray thee, this [yowm](../../strongs/h/h3117.md).

<a name="judges_10_16"></a>Judges 10:16

And they put [cuwr](../../strongs/h/h5493.md) the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) from [qereḇ](../../strongs/h/h7130.md) them, and ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md): and his [nephesh](../../strongs/h/h5315.md) was [qāṣar](../../strongs/h/h7114.md) for the ['amal](../../strongs/h/h5999.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_10_17"></a>Judges 10:17

Then the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) were gathered [ṣāʿaq](../../strongs/h/h6817.md), and [ḥānâ](../../strongs/h/h2583.md) in [Gilʿāḏ](../../strongs/h/h1568.md). And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) assembled themselves ['āsap̄](../../strongs/h/h622.md), and [ḥānâ](../../strongs/h/h2583.md) in [Miṣpâ](../../strongs/h/h4709.md).

<a name="judges_10_18"></a>Judges 10:18

And the ['am](../../strongs/h/h5971.md) and [śar](../../strongs/h/h8269.md) of [Gilʿāḏ](../../strongs/h/h1568.md) ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), What ['iysh](../../strongs/h/h376.md) is he that will [ḥālal](../../strongs/h/h2490.md) to [lāḥam](../../strongs/h/h3898.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md)? he shall be [ro'sh](../../strongs/h/h7218.md) over all the [yashab](../../strongs/h/h3427.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 9](judges_9.md) - [Judges 11](judges_11.md)