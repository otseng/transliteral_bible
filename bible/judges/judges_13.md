# [Judges 13](https://www.blueletterbible.org/kjv/judges/13)

<a name="judges_13_1"></a>Judges 13:1

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) [yāsap̄](../../strongs/h/h3254.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md); and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_13_2"></a>Judges 13:2

And there was an ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md) of [ṢārʿÂ](../../strongs/h/h6881.md), of the [mišpāḥâ](../../strongs/h/h4940.md) of the [dānî](../../strongs/h/h1839.md), whose [shem](../../strongs/h/h8034.md) was [Mānôaḥ](../../strongs/h/h4495.md); and his ['ishshah](../../strongs/h/h802.md) was [ʿāqār](../../strongs/h/h6135.md), and [yalad](../../strongs/h/h3205.md) not.

<a name="judges_13_3"></a>Judges 13:3

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto the ['ishshah](../../strongs/h/h802.md), and ['āmar](../../strongs/h/h559.md) unto her, Behold now, thou art [ʿāqār](../../strongs/h/h6135.md), and [yalad](../../strongs/h/h3205.md) not: but thou shalt [harah](../../strongs/h/h2029.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md).

<a name="judges_13_4"></a>Judges 13:4

Now therefore [shamar](../../strongs/h/h8104.md), I pray thee, and [šāṯâ](../../strongs/h/h8354.md) not [yayin](../../strongs/h/h3196.md) nor [šēḵār](../../strongs/h/h7941.md), and ['akal](../../strongs/h/h398.md) not any [tame'](../../strongs/h/h2931.md) thing:

<a name="judges_13_5"></a>Judges 13:5

For, lo, thou shalt [hārê](../../strongs/h/h2030.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and no [môrâ](../../strongs/h/h4177.md) shall [ʿālâ](../../strongs/h/h5927.md) on his [ro'sh](../../strongs/h/h7218.md): for the [naʿar](../../strongs/h/h5288.md) shall be a [nāzîr](../../strongs/h/h5139.md) unto ['Elohiym](../../strongs/h/h430.md) from the [beten](../../strongs/h/h990.md): and he shall [ḥālal](../../strongs/h/h2490.md) to [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) out of the [yad](../../strongs/h/h3027.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="judges_13_6"></a>Judges 13:6

Then the ['ishshah](../../strongs/h/h802.md) [bow'](../../strongs/h/h935.md) and ['āmar](../../strongs/h/h559.md) her ['iysh](../../strongs/h/h376.md), ['āmar](../../strongs/h/h559.md), An ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) unto me, and his [mar'ê](../../strongs/h/h4758.md) was like the [mar'ê](../../strongs/h/h4758.md) of a [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md), [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md): but I [sha'al](../../strongs/h/h7592.md) him not whence he was, neither [nāḡaḏ](../../strongs/h/h5046.md) he me his [shem](../../strongs/h/h8034.md):

<a name="judges_13_7"></a>Judges 13:7

But he ['āmar](../../strongs/h/h559.md) unto me, Behold, thou shalt [hārê](../../strongs/h/h2030.md), and [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md); and now [šēḵār](../../strongs/h/h7941.md) no [yayin](../../strongs/h/h3196.md) nor [šāṯâ](../../strongs/h/h8354.md), neither ['akal](../../strongs/h/h398.md) any [ṭām'â](../../strongs/h/h2932.md) thing: for the [naʿar](../../strongs/h/h5288.md) shall be a [nāzîr](../../strongs/h/h5139.md) to ['Elohiym](../../strongs/h/h430.md) from the [beten](../../strongs/h/h990.md) to the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md).

<a name="judges_13_8"></a>Judges 13:8

Then [Mānôaḥ](../../strongs/h/h4495.md) [ʿāṯar](../../strongs/h/h6279.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), [bî](../../strongs/h/h994.md) my ['adonay](../../strongs/h/h136.md), [na'](../../strongs/h/h4994.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) which thou didst [shalach](../../strongs/h/h7971.md) [bow'](../../strongs/h/h935.md) unto us, and [yārâ](../../strongs/h/h3384.md) us what we shall ['asah](../../strongs/h/h6213.md) unto the [naʿar](../../strongs/h/h5288.md) that shall be [yalad](../../strongs/h/h3205.md).

<a name="judges_13_9"></a>Judges 13:9

And ['Elohiym](../../strongs/h/h430.md) [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of [Mānôaḥ](../../strongs/h/h4495.md); and the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) [bow'](../../strongs/h/h935.md) unto the ['ishshah](../../strongs/h/h802.md) as she [yashab](../../strongs/h/h3427.md) in the [sadeh](../../strongs/h/h7704.md): but [Mānôaḥ](../../strongs/h/h4495.md) her ['iysh](../../strongs/h/h376.md) was not with her.

<a name="judges_13_10"></a>Judges 13:10

And the ['ishshah](../../strongs/h/h802.md) made [māhar](../../strongs/h/h4116.md), and [rûṣ](../../strongs/h/h7323.md), and [nāḡaḏ](../../strongs/h/h5046.md) her ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md) unto him, Behold, the ['iysh](../../strongs/h/h376.md) hath [ra'ah](../../strongs/h/h7200.md) unto me, that [bow'](../../strongs/h/h935.md) unto me the other [yowm](../../strongs/h/h3117.md).

<a name="judges_13_11"></a>Judges 13:11

And [Mānôaḥ](../../strongs/h/h4495.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) his ['ishshah](../../strongs/h/h802.md), and [bow'](../../strongs/h/h935.md) to the ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md) unto him, Art thou the ['iysh](../../strongs/h/h376.md) that [dabar](../../strongs/h/h1696.md) unto the ['ishshah](../../strongs/h/h802.md)? And he ['āmar](../../strongs/h/h559.md), I am.

<a name="judges_13_12"></a>Judges 13:12

And [Mānôaḥ](../../strongs/h/h4495.md) ['āmar](../../strongs/h/h559.md), Now let thy [dabar](../../strongs/h/h1697.md) [bow'](../../strongs/h/h935.md). How shall we [mishpat](../../strongs/h/h4941.md) the [naʿar](../../strongs/h/h5288.md), and how shall we [ma'aseh](../../strongs/h/h4639.md) unto him?

<a name="judges_13_13"></a>Judges 13:13

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mānôaḥ](../../strongs/h/h4495.md), Of all that I ['āmar](../../strongs/h/h559.md) unto the ['ishshah](../../strongs/h/h802.md) let her [shamar](../../strongs/h/h8104.md).

<a name="judges_13_14"></a>Judges 13:14

She may not ['akal](../../strongs/h/h398.md) of any thing that [yāṣā'](../../strongs/h/h3318.md) of the [gep̄en](../../strongs/h/h1612.md), neither let her [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) or [šēḵār](../../strongs/h/h7941.md), nor ['akal](../../strongs/h/h398.md) any [ṭām'â](../../strongs/h/h2932.md) thing: all that I [tsavah](../../strongs/h/h6680.md) her let her [shamar](../../strongs/h/h8104.md).

<a name="judges_13_15"></a>Judges 13:15

And [Mānôaḥ](../../strongs/h/h4495.md) ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), I pray thee, let us [ʿāṣar](../../strongs/h/h6113.md) thee, until we shall have made ['asah](../../strongs/h/h6213.md) a [gᵊḏî](../../strongs/h/h1423.md) [ʿēz](../../strongs/h/h5795.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="judges_13_16"></a>Judges 13:16

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mānôaḥ](../../strongs/h/h4495.md), Though thou [ʿāṣar](../../strongs/h/h6113.md) me, I will not ['akal](../../strongs/h/h398.md) of thy [lechem](../../strongs/h/h3899.md): and ['im](../../strongs/h/h518.md) thou wilt ['asah](../../strongs/h/h6213.md) a [ʿōlâ](../../strongs/h/h5930.md), thou must [ʿālâ](../../strongs/h/h5927.md) it unto [Yĕhovah](../../strongs/h/h3068.md). For [Mānôaḥ](../../strongs/h/h4495.md) [yada'](../../strongs/h/h3045.md) not that he was a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_13_17"></a>Judges 13:17

And [Mānôaḥ](../../strongs/h/h4495.md) ['āmar](../../strongs/h/h559.md) unto the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), What is thy [shem](../../strongs/h/h8034.md), that when thy [dabar](../../strongs/h/h1697.md) [bow'](../../strongs/h/h935.md) we may do thee [kabad](../../strongs/h/h3513.md)?

<a name="judges_13_18"></a>Judges 13:18

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Why [sha'al](../../strongs/h/h7592.md) thou thus after my [shem](../../strongs/h/h8034.md), seeing it is [pil'î](../../strongs/h/h6383.md) [pil'î](../../strongs/h/h6383.md)?

<a name="judges_13_19"></a>Judges 13:19

So [Mānôaḥ](../../strongs/h/h4495.md) [laqach](../../strongs/h/h3947.md) a [gᵊḏî](../../strongs/h/h1423.md) [ʿēz](../../strongs/h/h5795.md) with a [minchah](../../strongs/h/h4503.md), and [ʿālâ](../../strongs/h/h5927.md) it upon a [tsuwr](../../strongs/h/h6697.md) unto [Yĕhovah](../../strongs/h/h3068.md): and ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md); and [Mānôaḥ](../../strongs/h/h4495.md) and his ['ishshah](../../strongs/h/h802.md) [ra'ah](../../strongs/h/h7200.md).

<a name="judges_13_20"></a>Judges 13:20

For it came to pass, when the [lahaḇ](../../strongs/h/h3851.md) [ʿālâ](../../strongs/h/h5927.md) toward [shamayim](../../strongs/h/h8064.md) from off the [mizbeach](../../strongs/h/h4196.md), that the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) in the [lahaḇ](../../strongs/h/h3851.md) of the [mizbeach](../../strongs/h/h4196.md). And [Mānôaḥ](../../strongs/h/h4495.md) and his ['ishshah](../../strongs/h/h802.md) [ra'ah](../../strongs/h/h7200.md) it, and [naphal](../../strongs/h/h5307.md) their [paniym](../../strongs/h/h6440.md) to the ['erets](../../strongs/h/h776.md).

<a name="judges_13_21"></a>Judges 13:21

But the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) did no [yāsap̄](../../strongs/h/h3254.md) [ra'ah](../../strongs/h/h7200.md) to [Mānôaḥ](../../strongs/h/h4495.md) and to his ['ishshah](../../strongs/h/h802.md). Then [Mānôaḥ](../../strongs/h/h4495.md) [yada'](../../strongs/h/h3045.md) that he was a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_13_22"></a>Judges 13:22

And [Mānôaḥ](../../strongs/h/h4495.md) ['āmar](../../strongs/h/h559.md) unto his ['ishshah](../../strongs/h/h802.md), We shall [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), because we have [ra'ah](../../strongs/h/h7200.md) ['Elohiym](../../strongs/h/h430.md).

<a name="judges_13_23"></a>Judges 13:23

But his ['ishshah](../../strongs/h/h802.md) ['āmar](../../strongs/h/h559.md) unto him, [lû'](../../strongs/h/h3863.md) [Yĕhovah](../../strongs/h/h3068.md) were [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [muwth](../../strongs/h/h4191.md) us, he would not have [laqach](../../strongs/h/h3947.md) a [ʿōlâ](../../strongs/h/h5930.md) and a [minchah](../../strongs/h/h4503.md) at our [yad](../../strongs/h/h3027.md), neither would he have [ra'ah](../../strongs/h/h7200.md) us all these things, nor would as at this [ʿēṯ](../../strongs/h/h6256.md) have [shama'](../../strongs/h/h8085.md) us such things as [zō'ṯ](../../strongs/h/h2063.md).

<a name="judges_13_24"></a>Judges 13:24

And the ['ishshah](../../strongs/h/h802.md) [yalad](../../strongs/h/h3205.md) a [ben](../../strongs/h/h1121.md), and [qara'](../../strongs/h/h7121.md) his [shem](../../strongs/h/h8034.md) [Šimšôn](../../strongs/h/h8123.md): and the [naʿar](../../strongs/h/h5288.md) [gāḏal](../../strongs/h/h1431.md), and [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) him.

<a name="judges_13_25"></a>Judges 13:25

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥālal](../../strongs/h/h2490.md) to move him at [pāʿam](../../strongs/h/h6470.md) in the [maḥănê](../../strongs/h/h4264.md) of [Dān](../../strongs/h/h1835.md) between [ṢārʿÂ](../../strongs/h/h6881.md) and ['Eštā'Ōl](../../strongs/h/h847.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 12](judges_12.md) - [Judges 14](judges_14.md)