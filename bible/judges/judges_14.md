# [Judges 14](https://www.blueletterbible.org/kjv/judges/14)

<a name="judges_14_1"></a>Judges 14:1

And [Šimšôn](../../strongs/h/h8123.md) [yarad](../../strongs/h/h3381.md) to [Timnâ](../../strongs/h/h8553.md), and [ra'ah](../../strongs/h/h7200.md) an ['ishshah](../../strongs/h/h802.md) in [Timnâ](../../strongs/h/h8553.md) of the [bath](../../strongs/h/h1323.md) of the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="judges_14_2"></a>Judges 14:2

And he [ʿālâ](../../strongs/h/h5927.md), and [nāḡaḏ](../../strongs/h/h5046.md) his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md), and ['āmar](../../strongs/h/h559.md), I have [ra'ah](../../strongs/h/h7200.md) an ['ishshah](../../strongs/h/h802.md) in [Timnâ](../../strongs/h/h8553.md) of the [bath](../../strongs/h/h1323.md) of the [Pᵊlištî](../../strongs/h/h6430.md): now therefore [laqach](../../strongs/h/h3947.md) her for me to ['ishshah](../../strongs/h/h802.md).

<a name="judges_14_3"></a>Judges 14:3

Then his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md) ['āmar](../../strongs/h/h559.md) unto him, Is there ['în](../../strongs/h/h369.md) an ['ishshah](../../strongs/h/h802.md) among the [bath](../../strongs/h/h1323.md) of thy ['ach](../../strongs/h/h251.md), or among all my ['am](../../strongs/h/h5971.md), that thou [halak](../../strongs/h/h1980.md) to [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) of the [ʿārēl](../../strongs/h/h6189.md) [Pᵊlištî](../../strongs/h/h6430.md)? And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto his ['ab](../../strongs/h/h1.md), [laqach](../../strongs/h/h3947.md) her for me; for she [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md).

<a name="judges_14_4"></a>Judges 14:4

But his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md) [yada'](../../strongs/h/h3045.md) not that it was of [Yĕhovah](../../strongs/h/h3068.md), that he [bāqaš](../../strongs/h/h1245.md) a [ta'ănâ](../../strongs/h/h8385.md) against the [Pᵊlištî](../../strongs/h/h6430.md): for at that [ʿēṯ](../../strongs/h/h6256.md) the [Pᵊlištî](../../strongs/h/h6430.md) had [mashal](../../strongs/h/h4910.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_14_5"></a>Judges 14:5

Then [yarad](../../strongs/h/h3381.md) [Šimšôn](../../strongs/h/h8123.md) [yarad](../../strongs/h/h3381.md), and his ['ab](../../strongs/h/h1.md) and his ['em](../../strongs/h/h517.md), to [Timnâ](../../strongs/h/h8553.md), and [bow'](../../strongs/h/h935.md) to the [kerem](../../strongs/h/h3754.md) of [Timnâ](../../strongs/h/h8553.md): and, behold, a [kephiyr](../../strongs/h/h3715.md) ['ariy](../../strongs/h/h738.md) [šā'aḡ](../../strongs/h/h7580.md) [qārā'](../../strongs/h/h7125.md) him.

<a name="judges_14_6"></a>Judges 14:6

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsalach](../../strongs/h/h6743.md) upon him, and he [šāsaʿ](../../strongs/h/h8156.md) him as he would have [šāsaʿ](../../strongs/h/h8156.md) a [gᵊḏî](../../strongs/h/h1423.md), and he had [mᵊ'ûmâ](../../strongs/h/h3972.md) in his [yad](../../strongs/h/h3027.md): but he [nāḡaḏ](../../strongs/h/h5046.md) not his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md) what he had ['asah](../../strongs/h/h6213.md).

<a name="judges_14_7"></a>Judges 14:7

And he [yarad](../../strongs/h/h3381.md), and [dabar](../../strongs/h/h1696.md) with the ['ishshah](../../strongs/h/h802.md); and she [yashar](../../strongs/h/h3474.md) [Šimšôn](../../strongs/h/h8123.md) [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md).

<a name="judges_14_8"></a>Judges 14:8

And after a [yowm](../../strongs/h/h3117.md) he [shuwb](../../strongs/h/h7725.md) to [laqach](../../strongs/h/h3947.md) her, and he [cuwr](../../strongs/h/h5493.md) to [ra'ah](../../strongs/h/h7200.md) the [mapeleṯ](../../strongs/h/h4658.md) of the ['ariy](../../strongs/h/h738.md): and, behold, there was an ['edah](../../strongs/h/h5712.md) of [dᵊḇôrâ](../../strongs/h/h1682.md) and [dĕbash](../../strongs/h/h1706.md) in the [gᵊvîyâ](../../strongs/h/h1472.md) of the ['ariy](../../strongs/h/h738.md).

<a name="judges_14_9"></a>Judges 14:9

And he [radah](../../strongs/h/h7287.md) thereof in his [kaph](../../strongs/h/h3709.md), and [yālaḵ](../../strongs/h/h3212.md) ['akal](../../strongs/h/h398.md), and [halak](../../strongs/h/h1980.md) to his ['ab](../../strongs/h/h1.md) and ['em](../../strongs/h/h517.md), and he [nathan](../../strongs/h/h5414.md) them, and they did ['akal](../../strongs/h/h398.md): but he [nāḡaḏ](../../strongs/h/h5046.md) not them that he had [radah](../../strongs/h/h7287.md) the [dĕbash](../../strongs/h/h1706.md) out of the [gᵊvîyâ](../../strongs/h/h1472.md) of the ['ariy](../../strongs/h/h738.md).

<a name="judges_14_10"></a>Judges 14:10

So his ['ab](../../strongs/h/h1.md) [yarad](../../strongs/h/h3381.md) unto the ['ishshah](../../strongs/h/h802.md): and [Šimšôn](../../strongs/h/h8123.md) ['asah](../../strongs/h/h6213.md) there a [mištê](../../strongs/h/h4960.md); for so used the [bāḥûr](../../strongs/h/h970.md) to ['asah](../../strongs/h/h6213.md).

<a name="judges_14_11"></a>Judges 14:11

And it came to pass, when they [ra'ah](../../strongs/h/h7200.md) him, that they [laqach](../../strongs/h/h3947.md) [šᵊlōšîm](../../strongs/h/h7970.md) [mērēaʿ](../../strongs/h/h4828.md) to be with him.

<a name="judges_14_12"></a>Judges 14:12

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto them, I will now [ḥûḏ](../../strongs/h/h2330.md) a [ḥîḏâ](../../strongs/h/h2420.md) unto you: if ye can [nāḡaḏ](../../strongs/h/h5046.md) [nāḡaḏ](../../strongs/h/h5046.md) it me within the [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md) of the [mištê](../../strongs/h/h4960.md), and [māṣā'](../../strongs/h/h4672.md) it, then I will [nathan](../../strongs/h/h5414.md) you [šᵊlōšîm](../../strongs/h/h7970.md) [sāḏîn](../../strongs/h/h5466.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [ḥălîp̄â](../../strongs/h/h2487.md) of [beḡeḏ](../../strongs/h/h899.md):

<a name="judges_14_13"></a>Judges 14:13

But if ye [yakol](../../strongs/h/h3201.md) [nāḡaḏ](../../strongs/h/h5046.md) it me, then shall ye [nathan](../../strongs/h/h5414.md) me [šᵊlōšîm](../../strongs/h/h7970.md) [sāḏîn](../../strongs/h/h5466.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [ḥălîp̄â](../../strongs/h/h2487.md) of [beḡeḏ](../../strongs/h/h899.md). And they ['āmar](../../strongs/h/h559.md) unto him, [ḥûḏ](../../strongs/h/h2330.md) thy [ḥîḏâ](../../strongs/h/h2420.md), that we may [shama'](../../strongs/h/h8085.md) it.

<a name="judges_14_14"></a>Judges 14:14

And he ['āmar](../../strongs/h/h559.md) unto them, Out of the ['akal](../../strongs/h/h398.md) [yāṣā'](../../strongs/h/h3318.md) [ma'akal](../../strongs/h/h3978.md), and out of the ['az](../../strongs/h/h5794.md) [yāṣā'](../../strongs/h/h3318.md) [māṯôq](../../strongs/h/h4966.md). And they [yakol](../../strongs/h/h3201.md) not in [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md) [nāḡaḏ](../../strongs/h/h5046.md) the [ḥîḏâ](../../strongs/h/h2420.md).

<a name="judges_14_15"></a>Judges 14:15

And it came to pass on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md), that they ['āmar](../../strongs/h/h559.md) unto [Šimšôn](../../strongs/h/h8123.md) ['ishshah](../../strongs/h/h802.md), [pāṯâ](../../strongs/h/h6601.md) thy ['iysh](../../strongs/h/h376.md), that he may [nāḡaḏ](../../strongs/h/h5046.md) unto us the [ḥîḏâ](../../strongs/h/h2420.md), lest we [śārap̄](../../strongs/h/h8313.md) thee and thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) with ['esh](../../strongs/h/h784.md): have ye [qara'](../../strongs/h/h7121.md) us to take that we [yarash](../../strongs/h/h3423.md)? is it not so?

<a name="judges_14_16"></a>Judges 14:16

And [Šimšôn](../../strongs/h/h8123.md) ['ishshah](../../strongs/h/h802.md) [bāḵâ](../../strongs/h/h1058.md) before him, and ['āmar](../../strongs/h/h559.md), Thou dost but [sane'](../../strongs/h/h8130.md) me, and ['ahab](../../strongs/h/h157.md) me not: thou hast put [ḥûḏ](../../strongs/h/h2330.md) a [ḥîḏâ](../../strongs/h/h2420.md) unto the [ben](../../strongs/h/h1121.md) of my ['am](../../strongs/h/h5971.md), and hast not [nāḡaḏ](../../strongs/h/h5046.md) it me. And he ['āmar](../../strongs/h/h559.md) unto her, Behold, I have not [nāḡaḏ](../../strongs/h/h5046.md) it my ['ab](../../strongs/h/h1.md) nor my ['em](../../strongs/h/h517.md), and shall I [nāḡaḏ](../../strongs/h/h5046.md) it thee?

<a name="judges_14_17"></a>Judges 14:17

And she [bāḵâ](../../strongs/h/h1058.md) before him the [šeḇaʿ](../../strongs/h/h7651.md) [yowm](../../strongs/h/h3117.md), while their [mištê](../../strongs/h/h4960.md) [hayah](../../strongs/h/h1961.md): and it came to pass on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md), that he [nāḡaḏ](../../strongs/h/h5046.md) her, because she [ṣûq](../../strongs/h/h6693.md) upon him: and she [nāḡaḏ](../../strongs/h/h5046.md) the [ḥîḏâ](../../strongs/h/h2420.md) to the [ben](../../strongs/h/h1121.md) of her ['am](../../strongs/h/h5971.md).

<a name="judges_14_18"></a>Judges 14:18

And the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) ['āmar](../../strongs/h/h559.md) unto him on the [šᵊḇîʿî](../../strongs/h/h7637.md) [yowm](../../strongs/h/h3117.md) before the [ḥeres](../../strongs/h/h2775.md) [bow'](../../strongs/h/h935.md), What is [māṯôq](../../strongs/h/h4966.md) than [dĕbash](../../strongs/h/h1706.md)? and what is ['az](../../strongs/h/h5794.md) than an ['ariy](../../strongs/h/h738.md)? And he ['āmar](../../strongs/h/h559.md) unto them, [lûlē'](../../strongs/h/h3884.md) ye had not [ḥāraš](../../strongs/h/h2790.md) with my [ʿeḡlâ](../../strongs/h/h5697.md), ye had not [māṣā'](../../strongs/h/h4672.md) my [ḥîḏâ](../../strongs/h/h2420.md).

<a name="judges_14_19"></a>Judges 14:19

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [tsalach](../../strongs/h/h6743.md) upon him, and he [yarad](../../strongs/h/h3381.md) to ['Ašqᵊlôn](../../strongs/h/h831.md), and [nakah](../../strongs/h/h5221.md) [šᵊlōšîm](../../strongs/h/h7970.md) ['iysh](../../strongs/h/h376.md) of them, and [laqach](../../strongs/h/h3947.md) their [ḥălîṣâ](../../strongs/h/h2488.md), and [nathan](../../strongs/h/h5414.md) [ḥălîp̄â](../../strongs/h/h2487.md) of garments unto them which [nāḡaḏ](../../strongs/h/h5046.md) the [ḥîḏâ](../../strongs/h/h2420.md). And his ['aph](../../strongs/h/h639.md) was [ḥārâ](../../strongs/h/h2734.md), and he [ʿālâ](../../strongs/h/h5927.md) to his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="judges_14_20"></a>Judges 14:20

But [Šimšôn](../../strongs/h/h8123.md) ['ishshah](../../strongs/h/h802.md) was given to his [mērēaʿ](../../strongs/h/h4828.md), whom he had used as his [ra'ah](../../strongs/h/h7462.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 13](judges_13.md) - [Judges 15](judges_15.md)