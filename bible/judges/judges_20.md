# [Judges 20](https://www.blueletterbible.org/kjv/judges/20)

<a name="judges_20_1"></a>Judges 20:1

Then all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md), and the ['edah](../../strongs/h/h5712.md) was [qāhal](../../strongs/h/h6950.md) as ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md), from [Dān](../../strongs/h/h1835.md) even to [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md), with the ['erets](../../strongs/h/h776.md) of [Gilʿāḏ](../../strongs/h/h1568.md), unto [Yĕhovah](../../strongs/h/h3068.md) in [Miṣpâ](../../strongs/h/h4709.md).

<a name="judges_20_2"></a>Judges 20:2

And the [pinnâ](../../strongs/h/h6438.md) of all the ['am](../../strongs/h/h5971.md), even of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), [yatsab](../../strongs/h/h3320.md) themselves in the [qāhēl](../../strongs/h/h6951.md) of the ['am](../../strongs/h/h5971.md) of ['Elohiym](../../strongs/h/h430.md), ['arbaʿ](../../strongs/h/h702.md) [mē'â](../../strongs/h/h3967.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) [raḡlî](../../strongs/h/h7273.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md).

<a name="judges_20_3"></a>Judges 20:3

(Now the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) [shama'](../../strongs/h/h8085.md) that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [ʿālâ](../../strongs/h/h5927.md) to [Miṣpâ](../../strongs/h/h4709.md).) Then ['āmar](../../strongs/h/h559.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [dabar](../../strongs/h/h1696.md) us, how [hayah](../../strongs/h/h1961.md) this [ra'](../../strongs/h/h7451.md)?

<a name="judges_20_4"></a>Judges 20:4

And the [Lᵊvî](../../strongs/h/h3881.md) ['iysh](../../strongs/h/h376.md), the ['iysh](../../strongs/h/h376.md) of the ['ishshah](../../strongs/h/h802.md) that was [ratsach](../../strongs/h/h7523.md), ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), I [bow'](../../strongs/h/h935.md) into [giḇʿâ](../../strongs/h/h1390.md) that belongeth to [Binyāmîn](../../strongs/h/h1144.md), I and my [pîleḡeš](../../strongs/h/h6370.md), to [lûn](../../strongs/h/h3885.md).

<a name="judges_20_5"></a>Judges 20:5

And the [baʿal](../../strongs/h/h1167.md) of [giḇʿâ](../../strongs/h/h1390.md) [quwm](../../strongs/h/h6965.md) against me, and [cabab](../../strongs/h/h5437.md) the [bayith](../../strongs/h/h1004.md) [cabab](../../strongs/h/h5437.md) upon me by [layil](../../strongs/h/h3915.md), and [dāmâ](../../strongs/h/h1819.md) to have [harag](../../strongs/h/h2026.md) me: and my [pîleḡeš](../../strongs/h/h6370.md) have they [ʿānâ](../../strongs/h/h6031.md), that she is [muwth](../../strongs/h/h4191.md).

<a name="judges_20_6"></a>Judges 20:6

And I ['āḥaz](../../strongs/h/h270.md) my [pîleḡeš](../../strongs/h/h6370.md), and [nāṯaḥ](../../strongs/h/h5408.md) her, and [shalach](../../strongs/h/h7971.md) her throughout all the [sadeh](../../strongs/h/h7704.md) of the [nachalah](../../strongs/h/h5159.md) of [Yisra'el](../../strongs/h/h3478.md): for they have ['asah](../../strongs/h/h6213.md) [zimmâ](../../strongs/h/h2154.md) and [nᵊḇālâ](../../strongs/h/h5039.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_20_7"></a>Judges 20:7

Behold, ye are all [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); [yāhaḇ](../../strongs/h/h3051.md) [hălōm](../../strongs/h/h1988.md) your [dabar](../../strongs/h/h1697.md) and ['etsah](../../strongs/h/h6098.md).

<a name="judges_20_8"></a>Judges 20:8

And all the ['am](../../strongs/h/h5971.md) [quwm](../../strongs/h/h6965.md) as ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md), ['āmar](../../strongs/h/h559.md), We will not ['iysh](../../strongs/h/h376.md) of us [yālaḵ](../../strongs/h/h3212.md) to his ['ohel](../../strongs/h/h168.md), neither will we ['iysh](../../strongs/h/h376.md) of us [cuwr](../../strongs/h/h5493.md) into his [bayith](../../strongs/h/h1004.md).

<a name="judges_20_9"></a>Judges 20:9

But now this shall be the [dabar](../../strongs/h/h1697.md) which we will ['asah](../../strongs/h/h6213.md) to [giḇʿâ](../../strongs/h/h1390.md); we will go up by [gôrāl](../../strongs/h/h1486.md) against it;

<a name="judges_20_10"></a>Judges 20:10

And we will [laqach](../../strongs/h/h3947.md) [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) of a [mē'â](../../strongs/h/h3967.md) throughout all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), and a [mē'â](../../strongs/h/h3967.md) of an ['elep̄](../../strongs/h/h505.md), and an ['elep̄](../../strongs/h/h505.md) out of ten [rᵊḇāḇâ](../../strongs/h/h7233.md), to [laqach](../../strongs/h/h3947.md) [ṣêḏâ](../../strongs/h/h6720.md) for the ['am](../../strongs/h/h5971.md), that they may ['asah](../../strongs/h/h6213.md), when they [bow'](../../strongs/h/h935.md) to [Geḇaʿ](../../strongs/h/h1387.md) of [Binyāmîn](../../strongs/h/h1144.md), according to all the [nᵊḇālâ](../../strongs/h/h5039.md) that they have ['asah](../../strongs/h/h6213.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_20_11"></a>Judges 20:11

So all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) were ['āsap̄](../../strongs/h/h622.md) against the [ʿîr](../../strongs/h/h5892.md), [ḥāḇēr](../../strongs/h/h2270.md) as ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_20_12"></a>Judges 20:12

And the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) ['enowsh](../../strongs/h/h582.md) through all the [shebet](../../strongs/h/h7626.md) of [Binyāmîn](../../strongs/h/h1144.md), ['āmar](../../strongs/h/h559.md), What [ra'](../../strongs/h/h7451.md) is this that is [hayah](../../strongs/h/h1961.md) among you?

<a name="judges_20_13"></a>Judges 20:13

Now therefore [nathan](../../strongs/h/h5414.md) us the ['enowsh](../../strongs/h/h582.md), the [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), which are in [giḇʿâ](../../strongs/h/h1390.md), that we may put them to [muwth](../../strongs/h/h4191.md), and [bāʿar](../../strongs/h/h1197.md) [ra'](../../strongs/h/h7451.md) from [Yisra'el](../../strongs/h/h3478.md). But the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of their ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="judges_20_14"></a>Judges 20:14

But the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) gathered themselves ['āsap̄](../../strongs/h/h622.md) out of the [ʿîr](../../strongs/h/h5892.md) unto [giḇʿâ](../../strongs/h/h1390.md), to [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_20_15"></a>Judges 20:15

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) were [paqad](../../strongs/h/h6485.md) at that [yowm](../../strongs/h/h3117.md) out of the [ʿîr](../../strongs/h/h5892.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šēš](../../strongs/h/h8337.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md), beside the [yashab](../../strongs/h/h3427.md) of [giḇʿâ](../../strongs/h/h1390.md), which were [paqad](../../strongs/h/h6485.md) [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_20_16"></a>Judges 20:16

Among all this ['am](../../strongs/h/h5971.md) there were [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md) ['iṭṭēr](../../strongs/h/h334.md) [yad](../../strongs/h/h3027.md) [yamiyn](../../strongs/h/h3225.md); every one could [qālaʿ](../../strongs/h/h7049.md) ['eben](../../strongs/h/h68.md) at an [śaʿărâ](../../strongs/h/h8185.md) breadth, and not [chata'](../../strongs/h/h2398.md).

<a name="judges_20_17"></a>Judges 20:17

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md), beside [Binyāmîn](../../strongs/h/h1144.md), were [paqad](../../strongs/h/h6485.md) ['arbaʿ](../../strongs/h/h702.md) [mē'â](../../strongs/h/h3967.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md): all these were ['iysh](../../strongs/h/h376.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="judges_20_18"></a>Judges 20:18

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [quwm](../../strongs/h/h6965.md), and [ʿālâ](../../strongs/h/h5927.md) to the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), and [sha'al](../../strongs/h/h7592.md) counsel of ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md), [mî](../../strongs/h/h4310.md) of us shall [ʿālâ](../../strongs/h/h5927.md) [tᵊḥillâ](../../strongs/h/h8462.md) to the [milḥāmâ](../../strongs/h/h4421.md) against the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [Yehuwdah](../../strongs/h/h3063.md) shall go up [tᵊḥillâ](../../strongs/h/h8462.md).

<a name="judges_20_19"></a>Judges 20:19

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), and [ḥānâ](../../strongs/h/h2583.md) against [giḇʿâ](../../strongs/h/h1390.md).

<a name="judges_20_20"></a>Judges 20:20

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against [Binyāmîn](../../strongs/h/h1144.md); and the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) put themselves in ['arak](../../strongs/h/h6186.md) to [milḥāmâ](../../strongs/h/h4421.md) against them at [giḇʿâ](../../strongs/h/h1390.md).

<a name="judges_20_21"></a>Judges 20:21

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) [yāṣā'](../../strongs/h/h3318.md) out of [giḇʿâ](../../strongs/h/h1390.md), and [shachath](../../strongs/h/h7843.md) to the ['erets](../../strongs/h/h776.md) of the [Yisra'el](../../strongs/h/h3478.md) that [yowm](../../strongs/h/h3117.md) [ʿeśrîm](../../strongs/h/h6242.md) and [šᵊnayim](../../strongs/h/h8147.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_20_22"></a>Judges 20:22

And the ['am](../../strongs/h/h5971.md) the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥāzaq](../../strongs/h/h2388.md) themselves, and set their [milḥāmâ](../../strongs/h/h4421.md) [yāsap̄](../../strongs/h/h3254.md) in ['arak](../../strongs/h/h6186.md) in the [maqowm](../../strongs/h/h4725.md) where they put themselves in ['arak](../../strongs/h/h6186.md) the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md).

<a name="judges_20_23"></a>Judges 20:23

(And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) and [bāḵâ](../../strongs/h/h1058.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) until ['ereb](../../strongs/h/h6153.md), and [sha'al](../../strongs/h/h7592.md) counsel of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), Shall I [nāḡaš](../../strongs/h/h5066.md) [yāsap̄](../../strongs/h/h3254.md) to [milḥāmâ](../../strongs/h/h4421.md) against the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) my ['ach](../../strongs/h/h251.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) against him.)

<a name="judges_20_24"></a>Judges 20:24

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [qāraḇ](../../strongs/h/h7126.md) against the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md).

<a name="judges_20_25"></a>Judges 20:25

And [Binyāmîn](../../strongs/h/h1144.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) them out of [giḇʿâ](../../strongs/h/h1390.md) the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md), and [shachath](../../strongs/h/h7843.md) to the ['erets](../../strongs/h/h776.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) again [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md); all these [šālap̄](../../strongs/h/h8025.md) the [chereb](../../strongs/h/h2719.md).

<a name="judges_20_26"></a>Judges 20:26

Then all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and all the ['am](../../strongs/h/h5971.md), [ʿālâ](../../strongs/h/h5927.md), and [bow'](../../strongs/h/h935.md) unto the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), and [bāḵâ](../../strongs/h/h1058.md), and [yashab](../../strongs/h/h3427.md) there [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [ṣûm](../../strongs/h/h6684.md) that [yowm](../../strongs/h/h3117.md) until ['ereb](../../strongs/h/h6153.md), and [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and [šelem](../../strongs/h/h8002.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_20_27"></a>Judges 20:27

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [sha'al](../../strongs/h/h7592.md) of [Yĕhovah](../../strongs/h/h3068.md), (for the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md) was there in those [yowm](../../strongs/h/h3117.md),

<a name="judges_20_28"></a>Judges 20:28

And [Pînḥās](../../strongs/h/h6372.md), the [ben](../../strongs/h/h1121.md) of ['Elʿāzār](../../strongs/h/h499.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) it in those [yowm](../../strongs/h/h3117.md),) ['āmar](../../strongs/h/h559.md), Shall I yet [yāsap̄](../../strongs/h/h3254.md) [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) my ['ach](../../strongs/h/h251.md), or shall I [ḥāḏal](../../strongs/h/h2308.md)? And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md); for [māḥār](../../strongs/h/h4279.md) I will [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md).

<a name="judges_20_29"></a>Judges 20:29

And [Yisra'el](../../strongs/h/h3478.md) [śûm](../../strongs/h/h7760.md) ['arab](../../strongs/h/h693.md) [cabiyb](../../strongs/h/h5439.md) [giḇʿâ](../../strongs/h/h1390.md).

<a name="judges_20_30"></a>Judges 20:30

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) against the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) on the [šᵊlîšî](../../strongs/h/h7992.md) [yowm](../../strongs/h/h3117.md), and put themselves in ['arak](../../strongs/h/h6186.md) against [giḇʿâ](../../strongs/h/h1390.md), as at other [pa'am](../../strongs/h/h6471.md).

<a name="judges_20_31"></a>Judges 20:31

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) the ['am](../../strongs/h/h5971.md), and were [nathaq](../../strongs/h/h5423.md) from the [ʿîr](../../strongs/h/h5892.md); and they [ḥālal](../../strongs/h/h2490.md) to [nakah](../../strongs/h/h5221.md) of the ['am](../../strongs/h/h5971.md), and [ḥālāl](../../strongs/h/h2491.md), as at other [pa'am](../../strongs/h/h6471.md), in the [mĕcillah](../../strongs/h/h4546.md), of which ['echad](../../strongs/h/h259.md) [ʿālâ](../../strongs/h/h5927.md) to the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) [Bêṯ-'ēl](../../strongs/h/h1008.md), and the ['echad](../../strongs/h/h259.md) to [giḇʿâ](../../strongs/h/h1390.md) in the [sadeh](../../strongs/h/h7704.md), about [šᵊlōšîm](../../strongs/h/h7970.md) ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_20_32"></a>Judges 20:32

And the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) ['āmar](../../strongs/h/h559.md), They are [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) us, as at the [ri'šôn](../../strongs/h/h7223.md). But the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), Let us [nûs](../../strongs/h/h5127.md), and [nathaq](../../strongs/h/h5423.md) them from the [ʿîr](../../strongs/h/h5892.md) unto the [mĕcillah](../../strongs/h/h4546.md).

<a name="judges_20_33"></a>Judges 20:33

And all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [quwm](../../strongs/h/h6965.md) out of their [maqowm](../../strongs/h/h4725.md), and put themselves in ['arak](../../strongs/h/h6186.md) at [BaʿAl Tāmār](../../strongs/h/h1193.md): and the ['arab](../../strongs/h/h693.md) of [Yisra'el](../../strongs/h/h3478.md) came [gîaḥ](../../strongs/h/h1518.md) out of their [maqowm](../../strongs/h/h4725.md), even out of the [maʿărê](../../strongs/h/h4629.md) of [Geḇaʿ](../../strongs/h/h1387.md).

<a name="judges_20_34"></a>Judges 20:34

And there [bow'](../../strongs/h/h935.md) [neḡeḏ](../../strongs/h/h5048.md) [giḇʿâ](../../strongs/h/h1390.md) [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md) out of all [Yisra'el](../../strongs/h/h3478.md), and the [milḥāmâ](../../strongs/h/h4421.md) was [kabad](../../strongs/h/h3513.md): but they [yada'](../../strongs/h/h3045.md) not that [ra'](../../strongs/h/h7451.md) was [naga'](../../strongs/h/h5060.md) them.

<a name="judges_20_35"></a>Judges 20:35

And [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) [Binyāmîn](../../strongs/h/h1144.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shachath](../../strongs/h/h7843.md) of the [Ben-yᵊmînî](../../strongs/h/h1145.md) that [yowm](../../strongs/h/h3117.md) [ʿeśrîm](../../strongs/h/h6242.md) and [ḥāmēš](../../strongs/h/h2568.md) ['elep̄](../../strongs/h/h505.md) and a [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md): all these [šālap̄](../../strongs/h/h8025.md) the [chereb](../../strongs/h/h2719.md).

<a name="judges_20_36"></a>Judges 20:36

So the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md) [ra'ah](../../strongs/h/h7200.md) that they were [nāḡap̄](../../strongs/h/h5062.md): for the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) [maqowm](../../strongs/h/h4725.md) to the [Ben-yᵊmînî](../../strongs/h/h1145.md), because they [batach](../../strongs/h/h982.md) unto the ['arab](../../strongs/h/h693.md) which they had [śûm](../../strongs/h/h7760.md) beside [giḇʿâ](../../strongs/h/h1390.md).

<a name="judges_20_37"></a>Judges 20:37

And the ['arab](../../strongs/h/h693.md) [ḥûš](../../strongs/h/h2363.md), and [pāšaṭ](../../strongs/h/h6584.md) upon [giḇʿâ](../../strongs/h/h1390.md); and the ['arab](../../strongs/h/h693.md) drew themselves [mashak](../../strongs/h/h4900.md), and [nakah](../../strongs/h/h5221.md) all the [ʿîr](../../strongs/h/h5892.md) with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md).

<a name="judges_20_38"></a>Judges 20:38

Now there was an appointed [môʿēḏ](../../strongs/h/h4150.md) between the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿim](../../strongs/h/h5973.md) the ['arab](../../strongs/h/h693.md), that they should make a [rabah](../../strongs/h/h7235.md) [maśśᵊ'ēṯ](../../strongs/h/h4864.md) with ['ashan](../../strongs/h/h6227.md) [ʿālâ](../../strongs/h/h5927.md) out of the [ʿîr](../../strongs/h/h5892.md).

<a name="judges_20_39"></a>Judges 20:39

And when the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [hāp̄aḵ](../../strongs/h/h2015.md) in the [milḥāmâ](../../strongs/h/h4421.md), [Binyāmîn](../../strongs/h/h1144.md) [ḥālal](../../strongs/h/h2490.md) to [nakah](../../strongs/h/h5221.md) and [ḥālāl](../../strongs/h/h2491.md) of the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) about [šᵊlōšîm](../../strongs/h/h7970.md) ['iysh](../../strongs/h/h376.md): for they ['āmar](../../strongs/h/h559.md), [nāḡap̄](../../strongs/h/h5062.md) they are smitten [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) us, as in the [ri'šôn](../../strongs/h/h7223.md) [milḥāmâ](../../strongs/h/h4421.md).

<a name="judges_20_40"></a>Judges 20:40

But when the [maśśᵊ'ēṯ](../../strongs/h/h4864.md) [ḥālal](../../strongs/h/h2490.md) to a[ʿālâ](../../strongs/h/h5927.md) out of the [ʿîr](../../strongs/h/h5892.md) with a [ʿammûḏ](../../strongs/h/h5982.md) of ['ashan](../../strongs/h/h6227.md), the [Ben-yᵊmînî](../../strongs/h/h1145.md) [panah](../../strongs/h/h6437.md) ['aḥar](../../strongs/h/h310.md) them, and, behold, the [kālîl](../../strongs/h/h3632.md) of the [ʿîr](../../strongs/h/h5892.md) [ʿālâ](../../strongs/h/h5927.md) to [shamayim](../../strongs/h/h8064.md).

<a name="judges_20_41"></a>Judges 20:41

And when the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [hāp̄aḵ](../../strongs/h/h2015.md), the ['iysh](../../strongs/h/h376.md) of [Binyāmîn](../../strongs/h/h1144.md) were [bahal](../../strongs/h/h926.md): for they [ra'ah](../../strongs/h/h7200.md) that [ra'](../../strongs/h/h7451.md) was [naga'](../../strongs/h/h5060.md) upon them.

<a name="judges_20_42"></a>Judges 20:42

Therefore they [panah](../../strongs/h/h6437.md) their backs [paniym](../../strongs/h/h6440.md) the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) unto the [derek](../../strongs/h/h1870.md) of the [midbar](../../strongs/h/h4057.md); but the [milḥāmâ](../../strongs/h/h4421.md) [dāḇaq](../../strongs/h/h1692.md) them; and them which came out of the [ʿîr](../../strongs/h/h5892.md) they [shachath](../../strongs/h/h7843.md) in the [tavek](../../strongs/h/h8432.md) of them.

<a name="judges_20_43"></a>Judges 20:43

Thus they [kāṯar](../../strongs/h/h3803.md) the [Ben-yᵊmînî](../../strongs/h/h1145.md) round [kāṯar](../../strongs/h/h3803.md), and [radaph](../../strongs/h/h7291.md) them, and [dāraḵ](../../strongs/h/h1869.md) them with [mᵊnûḥâ](../../strongs/h/h4496.md) over [nōḵaḥ](../../strongs/h/h5227.md) [giḇʿâ](../../strongs/h/h1390.md) toward the [mizrach](../../strongs/h/h4217.md) [šemeš](../../strongs/h/h8121.md).

<a name="judges_20_44"></a>Judges 20:44

And there [naphal](../../strongs/h/h5307.md) of [Binyāmîn](../../strongs/h/h1144.md) [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md); all these were ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="judges_20_45"></a>Judges 20:45

And they [panah](../../strongs/h/h6437.md) and [nûs](../../strongs/h/h5127.md) toward the [midbar](../../strongs/h/h4057.md) unto the [cela'](../../strongs/h/h5553.md) of [Rimmôn](../../strongs/h/h7417.md): and they [ʿālal](../../strongs/h/h5953.md) of them in the [mĕcillah](../../strongs/h/h4546.md) [ḥāmēš](../../strongs/h/h2568.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md); and [dāḇaq](../../strongs/h/h1692.md) hard ['aḥar](../../strongs/h/h310.md) them unto [GiḏʿŌm](../../strongs/h/h1440.md), and [nakah](../../strongs/h/h5221.md) two ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) of them.

<a name="judges_20_46"></a>Judges 20:46

So that all which [naphal](../../strongs/h/h5307.md) that [yowm](../../strongs/h/h3117.md) of [Binyāmîn](../../strongs/h/h1144.md) were [ʿeśrîm](../../strongs/h/h6242.md) and [ḥāmēš](../../strongs/h/h2568.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) the [chereb](../../strongs/h/h2719.md); all these were ['enowsh](../../strongs/h/h582.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="judges_20_47"></a>Judges 20:47

But [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) [panah](../../strongs/h/h6437.md) and [nûs](../../strongs/h/h5127.md) to the [midbar](../../strongs/h/h4057.md) unto the [cela'](../../strongs/h/h5553.md) [Rimmôn](../../strongs/h/h7417.md), and [yashab](../../strongs/h/h3427.md) in the [cela'](../../strongs/h/h5553.md) [Rimmôn](../../strongs/h/h7417.md) ['arbaʿ](../../strongs/h/h702.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="judges_20_48"></a>Judges 20:48

And the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md) upon the [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), and [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), as well the [mᵊṯōm](../../strongs/h/h4974.md) of every [ʿîr](../../strongs/h/h5892.md), [ʿaḏ](../../strongs/h/h5704.md) the [bĕhemah](../../strongs/h/h929.md), and all that [māṣā'](../../strongs/h/h4672.md): also they [shalach](../../strongs/h/h7971.md) on ['esh](../../strongs/h/h784.md) all the [ʿîr](../../strongs/h/h5892.md) that they [māṣā'](../../strongs/h/h4672.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 19](judges_19.md) - [Judges 21](judges_21.md)