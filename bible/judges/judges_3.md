# [Judges 3](https://www.blueletterbible.org/kjv/judges/3)

<a name="judges_3_1"></a>Judges 3:1

Now these are the [gowy](../../strongs/h/h1471.md) which [Yĕhovah](../../strongs/h/h3068.md) [yānaḥ](../../strongs/h/h3240.md), to [nāsâ](../../strongs/h/h5254.md) [Yisra'el](../../strongs/h/h3478.md) by them, as many as had not [yada'](../../strongs/h/h3045.md) all the [milḥāmâ](../../strongs/h/h4421.md) of [Kĕna'an](../../strongs/h/h3667.md);

<a name="judges_3_2"></a>Judges 3:2

Only [raq](../../strongs/h/h7535.md) the [dôr](../../strongs/h/h1755.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) might [yada'](../../strongs/h/h3045.md), to [lamad](../../strongs/h/h3925.md) them [milḥāmâ](../../strongs/h/h4421.md), at the least such as [paniym](../../strongs/h/h6440.md) [yada'](../../strongs/h/h3045.md) nothing thereof;

<a name="judges_3_3"></a>Judges 3:3

Namely, [ḥāmēš](../../strongs/h/h2568.md) [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and all the [Kᵊnaʿănî](../../strongs/h/h3669.md), and the [Ṣîḏōnî](../../strongs/h/h6722.md), and the [Ḥiûî](../../strongs/h/h2340.md) that [yashab](../../strongs/h/h3427.md) in [har](../../strongs/h/h2022.md) [Lᵊḇānôn](../../strongs/h/h3844.md), from [har](../../strongs/h/h2022.md) [BaʿAl Ḥermôn](../../strongs/h/h1179.md) unto the entering [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md).

<a name="judges_3_4"></a>Judges 3:4

And they were to [nāsâ](../../strongs/h/h5254.md) [Yisra'el](../../strongs/h/h3478.md) by them, to [yada'](../../strongs/h/h3045.md) whether they would [shama'](../../strongs/h/h8085.md) unto the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [tsavah](../../strongs/h/h6680.md) their ['ab](../../strongs/h/h1.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="judges_3_5"></a>Judges 3:5

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yashab](../../strongs/h/h3427.md) [qereḇ](../../strongs/h/h7130.md) the [Kᵊnaʿănî](../../strongs/h/h3669.md), [Ḥitî](../../strongs/h/h2850.md), and ['Ĕmōrî](../../strongs/h/h567.md), and [Pᵊrizzî](../../strongs/h/h6522.md), and [Ḥiûî](../../strongs/h/h2340.md), and [Yᵊḇûsî](../../strongs/h/h2983.md):

<a name="judges_3_6"></a>Judges 3:6

And they [laqach](../../strongs/h/h3947.md) their [bath](../../strongs/h/h1323.md) to be their ['ishshah](../../strongs/h/h802.md), and [nathan](../../strongs/h/h5414.md) their [bath](../../strongs/h/h1323.md) to their [ben](../../strongs/h/h1121.md), and ['abad](../../strongs/h/h5647.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="judges_3_7"></a>Judges 3:7

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) [BaʿAl](../../strongs/h/h1168.md) and the ['ăšērâ](../../strongs/h/h842.md).

<a name="judges_3_8"></a>Judges 3:8

Therefore the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against [Yisra'el](../../strongs/h/h3478.md), and he [māḵar](../../strongs/h/h4376.md) them into the [yad](../../strongs/h/h3027.md) of [Kûšan RišʿĀṯayim](../../strongs/h/h3573.md) [melek](../../strongs/h/h4428.md) of ['Ăram nahărayim](../../strongs/h/h763.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['abad](../../strongs/h/h5647.md) [Kûšan RišʿĀṯayim](../../strongs/h/h3573.md) [šᵊmōnê](../../strongs/h/h8083.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_3_9"></a>Judges 3:9

And when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), [Yĕhovah](../../strongs/h/h3068.md) [quwm](../../strongs/h/h6965.md) a [yasha'](../../strongs/h/h3467.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), who [yasha'](../../strongs/h/h3467.md) them, even [ʿĀṯnî'Ēl](../../strongs/h/h6274.md) the [ben](../../strongs/h/h1121.md) of [Qᵊnaz](../../strongs/h/h7073.md), [Kālēḇ](../../strongs/h/h3612.md) [qāṭān](../../strongs/h/h6996.md) ['ach](../../strongs/h/h251.md).

<a name="judges_3_10"></a>Judges 3:10

And the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) came upon him, and he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md), and [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md): and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) [Kûšan RišʿĀṯayim](../../strongs/h/h3573.md) [melek](../../strongs/h/h4428.md) of ['Ăram nahărayim](../../strongs/h/h763.md) into his [yad](../../strongs/h/h3027.md); and his [yad](../../strongs/h/h3027.md) ['azaz](../../strongs/h/h5810.md) against [Kûšan RišʿĀṯayim](../../strongs/h/h3573.md).

<a name="judges_3_11"></a>Judges 3:11

And the ['erets](../../strongs/h/h776.md) had [šāqaṭ](../../strongs/h/h8252.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md). And [ʿĀṯnî'Ēl](../../strongs/h/h6274.md) the [ben](../../strongs/h/h1121.md) of [Qᵊnaz](../../strongs/h/h7073.md) [muwth](../../strongs/h/h4191.md).

<a name="judges_3_12"></a>Judges 3:12

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) [yāsap̄](../../strongs/h/h3254.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): and [Yĕhovah](../../strongs/h/h3068.md) [ḥāzaq](../../strongs/h/h2388.md) [ʿEḡlôn](../../strongs/h/h5700.md) the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) against [Yisra'el](../../strongs/h/h3478.md), because they had ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_3_13"></a>Judges 3:13

And he ['āsap̄](../../strongs/h/h622.md) unto him the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) and [ʿĂmālēq](../../strongs/h/h6002.md), and [yālaḵ](../../strongs/h/h3212.md) and [nakah](../../strongs/h/h5221.md) [Yisra'el](../../strongs/h/h3478.md), and [yarash](../../strongs/h/h3423.md) the [ʿîr](../../strongs/h/h5892.md) of palm [Tāmār](../../strongs/h/h8558.md) [ʿîr hatmārîm](../../strongs/h/h5899.md).

<a name="judges_3_14"></a>Judges 3:14

So the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['abad](../../strongs/h/h5647.md) [ʿEḡlôn](../../strongs/h/h5700.md) the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md) [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_3_15"></a>Judges 3:15

But when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md), [Yĕhovah](../../strongs/h/h3068.md) raised them [quwm](../../strongs/h/h6965.md) a [yasha'](../../strongs/h/h3467.md), ['Êûḏ](../../strongs/h/h164.md) the [ben](../../strongs/h/h1121.md) of [Gērā'](../../strongs/h/h1617.md), a [Ben-yᵊmînî](../../strongs/h/h1145.md), an ['iysh](../../strongs/h/h376.md) ['iṭṭēr](../../strongs/h/h334.md) [yamiyn](../../strongs/h/h3225.md): and by [yad](../../strongs/h/h3027.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) a [minchah](../../strongs/h/h4503.md) unto [ʿEḡlôn](../../strongs/h/h5700.md) the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md).

<a name="judges_3_16"></a>Judges 3:16

But ['Êûḏ](../../strongs/h/h164.md) ['asah](../../strongs/h/h6213.md) him a [chereb](../../strongs/h/h2719.md) which had [šᵊnayim](../../strongs/h/h8147.md) [pêâ](../../strongs/h/h6366.md), of a [gōmeḏ](../../strongs/h/h1574.md) ['ōreḵ](../../strongs/h/h753.md); and he did [ḥāḡar](../../strongs/h/h2296.md) it under his [maḏ](../../strongs/h/h4055.md) upon his [yamiyn](../../strongs/h/h3225.md) [yārēḵ](../../strongs/h/h3409.md).

<a name="judges_3_17"></a>Judges 3:17

And he [qāraḇ](../../strongs/h/h7126.md) the [minchah](../../strongs/h/h4503.md) unto [ʿEḡlôn](../../strongs/h/h5700.md) [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md): and [ʿEḡlôn](../../strongs/h/h5700.md) was a [me'od](../../strongs/h/h3966.md) [bārî'](../../strongs/h/h1277.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_3_18"></a>Judges 3:18

And when he had made a [kalah](../../strongs/h/h3615.md) to [qāraḇ](../../strongs/h/h7126.md) the [minchah](../../strongs/h/h4503.md), he [shalach](../../strongs/h/h7971.md) the ['am](../../strongs/h/h5971.md) that [nasa'](../../strongs/h/h5375.md) the [minchah](../../strongs/h/h4503.md).

<a name="judges_3_19"></a>Judges 3:19

But he himself [shuwb](../../strongs/h/h7725.md) from the [pāsîl](../../strongs/h/h6456.md) that were by [Gilgāl](../../strongs/h/h1537.md), and ['āmar](../../strongs/h/h559.md), I have a [cether](../../strongs/h/h5643.md) [dabar](../../strongs/h/h1697.md) unto thee, O [melek](../../strongs/h/h4428.md): who ['āmar](../../strongs/h/h559.md), Keep [hāsâ](../../strongs/h/h2013.md). And all that ['amad](../../strongs/h/h5975.md) by him [yāṣā'](../../strongs/h/h3318.md) from him.

<a name="judges_3_20"></a>Judges 3:20

And ['Êûḏ](../../strongs/h/h164.md) [bow'](../../strongs/h/h935.md) unto him; and he was [yashab](../../strongs/h/h3427.md) in a [mᵊqērâ](../../strongs/h/h4747.md) [ʿălîyâ](../../strongs/h/h5944.md), which he had for himself alone. And ['Êûḏ](../../strongs/h/h164.md) ['āmar](../../strongs/h/h559.md), I have a [dabar](../../strongs/h/h1697.md) from ['Elohiym](../../strongs/h/h430.md) unto thee. And he [quwm](../../strongs/h/h6965.md) out of his [kicce'](../../strongs/h/h3678.md).

<a name="judges_3_21"></a>Judges 3:21

And ['Êûḏ](../../strongs/h/h164.md) [shalach](../../strongs/h/h7971.md) his [śᵊmō'l](../../strongs/h/h8040.md) [yad](../../strongs/h/h3027.md), and [laqach](../../strongs/h/h3947.md) the [chereb](../../strongs/h/h2719.md) from his [yamiyn](../../strongs/h/h3225.md) [yārēḵ](../../strongs/h/h3409.md), and [tāqaʿ](../../strongs/h/h8628.md) it into his [beten](../../strongs/h/h990.md):

<a name="judges_3_22"></a>Judges 3:22

And the [niṣṣāḇ](../../strongs/h/h5325.md) also [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) the [lahaḇ](../../strongs/h/h3851.md); and the [cheleb](../../strongs/h/h2459.md) [cagar](../../strongs/h/h5462.md) [bᵊʿaḏ](../../strongs/h/h1157.md) the [lahaḇ](../../strongs/h/h3851.md), so that he could not [šālap̄](../../strongs/h/h8025.md) the [chereb](../../strongs/h/h2719.md) out of his [beten](../../strongs/h/h990.md); and the [p̄aršᵊḏōn](../../strongs/h/h6574.md) [yāṣā'](../../strongs/h/h3318.md).

<a name="judges_3_23"></a>Judges 3:23

Then ['Êûḏ](../../strongs/h/h164.md) [yāṣā'](../../strongs/h/h3318.md) through the [misdᵊrôn](../../strongs/h/h4528.md), and [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) of the [ʿălîyâ](../../strongs/h/h5944.md) upon him, and [nāʿal](../../strongs/h/h5274.md) them.

<a name="judges_3_24"></a>Judges 3:24

When he was [yāṣā'](../../strongs/h/h3318.md), his ['ebed](../../strongs/h/h5650.md) [bow'](../../strongs/h/h935.md); and when they [ra'ah](../../strongs/h/h7200.md) that, behold, the [deleṯ](../../strongs/h/h1817.md) of the [ʿălîyâ](../../strongs/h/h5944.md) were [nāʿal](../../strongs/h/h5274.md), they ['āmar](../../strongs/h/h559.md), Surely he [cakak](../../strongs/h/h5526.md) his [regel](../../strongs/h/h7272.md) in his [mᵊqērâ](../../strongs/h/h4747.md) [ḥeḏer](../../strongs/h/h2315.md).

<a name="judges_3_25"></a>Judges 3:25

And they [chuwl](../../strongs/h/h2342.md) till they were [buwsh](../../strongs/h/h954.md): and, behold, he [pāṯaḥ](../../strongs/h/h6605.md) not the [deleṯ](../../strongs/h/h1817.md) of the [ʿălîyâ](../../strongs/h/h5944.md); therefore they [laqach](../../strongs/h/h3947.md) a [map̄tēaḥ](../../strongs/h/h4668.md), and [pāṯaḥ](../../strongs/h/h6605.md) them: and, behold, their ['adown](../../strongs/h/h113.md) was fallen [naphal](../../strongs/h/h5307.md) [muwth](../../strongs/h/h4191.md) on the ['erets](../../strongs/h/h776.md).

<a name="judges_3_26"></a>Judges 3:26

And ['Êûḏ](../../strongs/h/h164.md) [mālaṭ](../../strongs/h/h4422.md) while they [māhah](../../strongs/h/h4102.md), and ['abar](../../strongs/h/h5674.md) the [pāsîl](../../strongs/h/h6456.md), and [mālaṭ](../../strongs/h/h4422.md) unto [ŚᵊʿÎrâ](../../strongs/h/h8167.md).

<a name="judges_3_27"></a>Judges 3:27

And it came to pass, when he was [bow'](../../strongs/h/h935.md), that he [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md) in the [har](../../strongs/h/h2022.md) of ['Ep̄rayim](../../strongs/h/h669.md), and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yarad](../../strongs/h/h3381.md) with him from the [har](../../strongs/h/h2022.md), and he [paniym](../../strongs/h/h6440.md) them.

<a name="judges_3_28"></a>Judges 3:28

And he ['āmar](../../strongs/h/h559.md) unto them, [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) me: for [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) your ['oyeb](../../strongs/h/h341.md) the [Mô'āḇ](../../strongs/h/h4124.md) into your [yad](../../strongs/h/h3027.md). And they [yarad](../../strongs/h/h3381.md) ['aḥar](../../strongs/h/h310.md) him, and [lāḵaḏ](../../strongs/h/h3920.md) the [maʿăḇār](../../strongs/h/h4569.md) of [Yardēn](../../strongs/h/h3383.md) toward [Mô'āḇ](../../strongs/h/h4124.md), and [nathan](../../strongs/h/h5414.md) not an ['iysh](../../strongs/h/h376.md) to ['abar](../../strongs/h/h5674.md).

<a name="judges_3_29"></a>Judges 3:29

And they [nakah](../../strongs/h/h5221.md) of [Mô'āḇ](../../strongs/h/h4124.md) at that [ʿēṯ](../../strongs/h/h6256.md) about [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md), all [šāmēn](../../strongs/h/h8082.md), and all ['iysh](../../strongs/h/h376.md) of [ḥayil](../../strongs/h/h2428.md); and there [mālaṭ](../../strongs/h/h4422.md) not an ['iysh](../../strongs/h/h376.md).

<a name="judges_3_30"></a>Judges 3:30

So [Mô'āḇ](../../strongs/h/h4124.md) was [kānaʿ](../../strongs/h/h3665.md) that [yowm](../../strongs/h/h3117.md) under the [yad](../../strongs/h/h3027.md) of [Yisra'el](../../strongs/h/h3478.md). And the ['erets](../../strongs/h/h776.md) had [šāqaṭ](../../strongs/h/h8252.md) [šᵊmōnîm](../../strongs/h/h8084.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_3_31"></a>Judges 3:31

And ['aḥar](../../strongs/h/h310.md) him was [Šamgar](../../strongs/h/h8044.md) the [ben](../../strongs/h/h1121.md) of [ʿĂnāṯ](../../strongs/h/h6067.md), which [nakah](../../strongs/h/h5221.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) with a [bāqār](../../strongs/h/h1241.md) [malmāḏ](../../strongs/h/h4451.md): and he also [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 2](judges_2.md) - [Judges 4](judges_4.md)