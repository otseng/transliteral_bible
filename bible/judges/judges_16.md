# [Judges 16](https://www.blueletterbible.org/kjv/judges/16)

<a name="judges_16_1"></a>Judges 16:1

Then [yālaḵ](../../strongs/h/h3212.md) [Šimšôn](../../strongs/h/h8123.md) to [ʿAzzâ](../../strongs/h/h5804.md), and [ra'ah](../../strongs/h/h7200.md) there an ['ishshah](../../strongs/h/h802.md) [zānâ](../../strongs/h/h2181.md), and [bow'](../../strongs/h/h935.md) unto her.

<a name="judges_16_2"></a>Judges 16:2

And it was told the [ʿĂzzāṯî](../../strongs/h/h5841.md), ['āmar](../../strongs/h/h559.md), [Šimšôn](../../strongs/h/h8123.md) is [bow'](../../strongs/h/h935.md) hither. And they [cabab](../../strongs/h/h5437.md) him, and ['arab](../../strongs/h/h693.md) for him all [layil](../../strongs/h/h3915.md) in the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md), and were [ḥāraš](../../strongs/h/h2790.md) all the [layil](../../strongs/h/h3915.md), ['āmar](../../strongs/h/h559.md), In the [boqer](../../strongs/h/h1242.md), when it is ['owr](../../strongs/h/h216.md), we shall [harag](../../strongs/h/h2026.md) him.

<a name="judges_16_3"></a>Judges 16:3

And [Šimšôn](../../strongs/h/h8123.md) [shakab](../../strongs/h/h7901.md) till [ḥēṣî](../../strongs/h/h2677.md) [layil](../../strongs/h/h3915.md), and [quwm](../../strongs/h/h6965.md) at [ḥēṣî](../../strongs/h/h2677.md) [layil](../../strongs/h/h3915.md), and ['āḥaz](../../strongs/h/h270.md) the [deleṯ](../../strongs/h/h1817.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md), and the [šᵊnayim](../../strongs/h/h8147.md) [mᵊzûzâ](../../strongs/h/h4201.md), and went [nāsaʿ](../../strongs/h/h5265.md) with them, [bᵊrîaḥ](../../strongs/h/h1280.md) and all, and [śûm](../../strongs/h/h7760.md) them upon his [kāṯēp̄](../../strongs/h/h3802.md), and carried them [ʿālâ](../../strongs/h/h5927.md) to the [ro'sh](../../strongs/h/h7218.md) of an [har](../../strongs/h/h2022.md) that is [paniym](../../strongs/h/h6440.md) [Ḥeḇrôn](../../strongs/h/h2275.md).

<a name="judges_16_4"></a>Judges 16:4

And it came to pass ['aḥar](../../strongs/h/h310.md), that he ['ahab](../../strongs/h/h157.md) an ['ishshah](../../strongs/h/h802.md) in the [nachal](../../strongs/h/h5158.md) of [Śrēq](../../strongs/h/h7796.md), whose [shem](../../strongs/h/h8034.md) was [Dᵊlîlâ](../../strongs/h/h1807.md).

<a name="judges_16_5"></a>Judges 16:5

And the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) unto her, and ['āmar](../../strongs/h/h559.md) unto her, [pāṯâ](../../strongs/h/h6601.md) him, and [ra'ah](../../strongs/h/h7200.md) wherein his [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) lieth, and by what means we may [yakol](../../strongs/h/h3201.md) against him, that we may ['āsar](../../strongs/h/h631.md) him to [ʿānâ](../../strongs/h/h6031.md) him: and we will [nathan](../../strongs/h/h5414.md) thee every ['iysh](../../strongs/h/h376.md) of us ['elep̄](../../strongs/h/h505.md) [mē'â](../../strongs/h/h3967.md) of [keceph](../../strongs/h/h3701.md).

<a name="judges_16_6"></a>Judges 16:6

And [Dᵊlîlâ](../../strongs/h/h1807.md) ['āmar](../../strongs/h/h559.md) to [Šimšôn](../../strongs/h/h8123.md), [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, wherein thy [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) lieth, and wherewith thou mightest be ['āsar](../../strongs/h/h631.md) to [ʿānâ](../../strongs/h/h6031.md) thee.

<a name="judges_16_7"></a>Judges 16:7

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto her, If they ['āsar](../../strongs/h/h631.md) me with [šeḇaʿ](../../strongs/h/h7651.md) [laḥ](../../strongs/h/h3892.md) [yeṯer](../../strongs/h/h3499.md) that were never [ḥāraḇ](../../strongs/h/h2717.md), then shall I be [ḥālâ](../../strongs/h/h2470.md), and be as ['echad](../../strongs/h/h259.md) ['āḏām](../../strongs/h/h120.md).

<a name="judges_16_8"></a>Judges 16:8

Then the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) to her [šeḇaʿ](../../strongs/h/h7651.md) [laḥ](../../strongs/h/h3892.md) [yeṯer](../../strongs/h/h3499.md) which had not been [ḥāraḇ](../../strongs/h/h2717.md), and she ['āsar](../../strongs/h/h631.md) him with them.

<a name="judges_16_9"></a>Judges 16:9

Now there were men ['arab](../../strongs/h/h693.md), [yashab](../../strongs/h/h3427.md) with her in the [ḥeḏer](../../strongs/h/h2315.md). And she ['āmar](../../strongs/h/h559.md) unto him, The [Pᵊlištî](../../strongs/h/h6430.md) be upon thee, [Šimšôn](../../strongs/h/h8123.md). And he [nathaq](../../strongs/h/h5423.md) the [yeṯer](../../strongs/h/h3499.md), as a [pāṯîl](../../strongs/h/h6616.md) of [nᵊʿōreṯ](../../strongs/h/h5296.md) is [nathaq](../../strongs/h/h5423.md) when it [rîaḥ](../../strongs/h/h7306.md) the ['esh](../../strongs/h/h784.md). So his [koach](../../strongs/h/h3581.md) was not [yada'](../../strongs/h/h3045.md).

<a name="judges_16_10"></a>Judges 16:10

And [Dᵊlîlâ](../../strongs/h/h1807.md) ['āmar](../../strongs/h/h559.md) unto [Šimšôn](../../strongs/h/h8123.md), Behold, thou hast [hāṯal](../../strongs/h/h2048.md) me, and [dabar](../../strongs/h/h1696.md) me [kazab](../../strongs/h/h3577.md): now [nāḡaḏ](../../strongs/h/h5046.md) me, I pray thee, wherewith thou mightest be ['āsar](../../strongs/h/h631.md).

<a name="judges_16_11"></a>Judges 16:11

And he ['āmar](../../strongs/h/h559.md) unto her, If they ['āsar](../../strongs/h/h631.md) me ['āsar](../../strongs/h/h631.md) with [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḇōṯ](../../strongs/h/h5688.md) that never were [mĕla'kah](../../strongs/h/h4399.md) ['asah](../../strongs/h/h6213.md), then shall I be [ḥālâ](../../strongs/h/h2470.md), and be as ['echad](../../strongs/h/h259.md) ['āḏām](../../strongs/h/h120.md).

<a name="judges_16_12"></a>Judges 16:12

[Dᵊlîlâ](../../strongs/h/h1807.md) therefore [laqach](../../strongs/h/h3947.md) [ḥāḏāš](../../strongs/h/h2319.md) [ʿăḇōṯ](../../strongs/h/h5688.md), and ['āsar](../../strongs/h/h631.md) him therewith, and ['āmar](../../strongs/h/h559.md) unto him, The [Pᵊlištî](../../strongs/h/h6430.md) be upon thee, [Šimšôn](../../strongs/h/h8123.md). And there were ['arab](../../strongs/h/h693.md) [yashab](../../strongs/h/h3427.md) in the [ḥeḏer](../../strongs/h/h2315.md). And he [nathaq](../../strongs/h/h5423.md) them from off his [zerowa'](../../strongs/h/h2220.md) like a [ḥûṭ](../../strongs/h/h2339.md).

<a name="judges_16_13"></a>Judges 16:13

And [Dᵊlîlâ](../../strongs/h/h1807.md) ['āmar](../../strongs/h/h559.md) unto [Šimšôn](../../strongs/h/h8123.md), [hēnnâ](../../strongs/h/h2008.md) thou hast [hāṯal](../../strongs/h/h2048.md) me, and [dabar](../../strongs/h/h1696.md) me [kazab](../../strongs/h/h3577.md): [nāḡaḏ](../../strongs/h/h5046.md) me wherewith thou mightest be ['āsar](../../strongs/h/h631.md). And he ['āmar](../../strongs/h/h559.md) unto her, If thou ['āraḡ](../../strongs/h/h707.md) the [šeḇaʿ](../../strongs/h/h7651.md) [maḥlāp̄â](../../strongs/h/h4253.md) of my [ro'sh](../../strongs/h/h7218.md) with the [masseḵeṯ](../../strongs/h/h4545.md).

<a name="judges_16_14"></a>Judges 16:14

And she [tāqaʿ](../../strongs/h/h8628.md) it with the [yāṯēḏ](../../strongs/h/h3489.md), and ['āmar](../../strongs/h/h559.md) unto him, The [Pᵊlištî](../../strongs/h/h6430.md) be upon thee, [Šimšôn](../../strongs/h/h8123.md). And he [yāqaṣ](../../strongs/h/h3364.md) out of his [šēnā'](../../strongs/h/h8142.md), and went [nāsaʿ](../../strongs/h/h5265.md) with the [yāṯēḏ](../../strongs/h/h3489.md) of the ['ereḡ](../../strongs/h/h708.md), and with the [masseḵeṯ](../../strongs/h/h4545.md).

<a name="judges_16_15"></a>Judges 16:15

And she ['āmar](../../strongs/h/h559.md) unto him, ['êḵ](../../strongs/h/h349.md) canst thou ['āmar](../../strongs/h/h559.md), I ['ahab](../../strongs/h/h157.md) thee, when thine [leb](../../strongs/h/h3820.md) is not with me? thou hast [hāṯal](../../strongs/h/h2048.md) me these [šālôš](../../strongs/h/h7969.md) [pa'am](../../strongs/h/h6471.md), and hast not [nāḡaḏ](../../strongs/h/h5046.md) me wherein thy [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) lieth.

<a name="judges_16_16"></a>Judges 16:16

And it came to pass, when she [ṣûq](../../strongs/h/h6693.md) him [yowm](../../strongs/h/h3117.md) with her [dabar](../../strongs/h/h1697.md), and ['ālaṣ](../../strongs/h/h509.md) him, so that his [nephesh](../../strongs/h/h5315.md) was [qāṣar](../../strongs/h/h7114.md) unto [muwth](../../strongs/h/h4191.md);

<a name="judges_16_17"></a>Judges 16:17

That he [nāḡaḏ](../../strongs/h/h5046.md) her all his [leb](../../strongs/h/h3820.md), and ['āmar](../../strongs/h/h559.md) unto her, There hath not [ʿālâ](../../strongs/h/h5927.md) a [môrâ](../../strongs/h/h4177.md) upon mine [ro'sh](../../strongs/h/h7218.md); for I have been a [nāzîr](../../strongs/h/h5139.md) unto ['Elohiym](../../strongs/h/h430.md) from my ['em](../../strongs/h/h517.md) [beten](../../strongs/h/h990.md): if I be [gālaḥ](../../strongs/h/h1548.md), then my [koach](../../strongs/h/h3581.md) will [cuwr](../../strongs/h/h5493.md) from me, and I shall become [ḥālâ](../../strongs/h/h2470.md), and be like any other ['āḏām](../../strongs/h/h120.md).

<a name="judges_16_18"></a>Judges 16:18

And when [Dᵊlîlâ](../../strongs/h/h1807.md) [ra'ah](../../strongs/h/h7200.md) that he had [nāḡaḏ](../../strongs/h/h5046.md) her all his [leb](../../strongs/h/h3820.md), she [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) for the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md), ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) this [pa'am](../../strongs/h/h6471.md), for he hath [nāḡaḏ](../../strongs/h/h5046.md) me all his [leb](../../strongs/h/h3820.md). Then the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) [ʿālâ](../../strongs/h/h5927.md) unto her, and [ʿālâ](../../strongs/h/h5927.md) [keceph](../../strongs/h/h3701.md) in their [yad](../../strongs/h/h3027.md).

<a name="judges_16_19"></a>Judges 16:19

And she made him [yashen](../../strongs/h/h3462.md) upon her [bereḵ](../../strongs/h/h1290.md); and she [qara'](../../strongs/h/h7121.md) for an ['iysh](../../strongs/h/h376.md), and she caused him to [gālaḥ](../../strongs/h/h1548.md) the [šeḇaʿ](../../strongs/h/h7651.md) [maḥlāp̄â](../../strongs/h/h4253.md) of his [ro'sh](../../strongs/h/h7218.md); and she [ḥālal](../../strongs/h/h2490.md) to [ʿānâ](../../strongs/h/h6031.md) him, and his [koach](../../strongs/h/h3581.md) [cuwr](../../strongs/h/h5493.md) from him.

<a name="judges_16_20"></a>Judges 16:20

And she ['āmar](../../strongs/h/h559.md), The [Pᵊlištî](../../strongs/h/h6430.md) be upon thee, [Šimšôn](../../strongs/h/h8123.md). And he [yāqaṣ](../../strongs/h/h3364.md) out of his [šēnā'](../../strongs/h/h8142.md), and ['āmar](../../strongs/h/h559.md), I will [yāṣā'](../../strongs/h/h3318.md) as at other times [pa'am](../../strongs/h/h6471.md), and [nāʿar](../../strongs/h/h5287.md) myself. And he [yada'](../../strongs/h/h3045.md) not that [Yĕhovah](../../strongs/h/h3068.md) was [cuwr](../../strongs/h/h5493.md) from him.

<a name="judges_16_21"></a>Judges 16:21

But the [Pᵊlištî](../../strongs/h/h6430.md) ['āḥaz](../../strongs/h/h270.md) him, and put [nāqar](../../strongs/h/h5365.md) his ['ayin](../../strongs/h/h5869.md), and [yarad](../../strongs/h/h3381.md) him to [ʿAzzâ](../../strongs/h/h5804.md), and ['āsar](../../strongs/h/h631.md) him with [nᵊḥšeṯ](../../strongs/h/h5178.md); and he did [ṭāḥan](../../strongs/h/h2912.md) in the ['āsar](../../strongs/h/h631.md) [bayith](../../strongs/h/h1004.md).

<a name="judges_16_22"></a>Judges 16:22

Howbeit the [śēʿār](../../strongs/h/h8181.md) of his [ro'sh](../../strongs/h/h7218.md) [ḥālal](../../strongs/h/h2490.md) to [ṣāmaḥ](../../strongs/h/h6779.md) ['ăšer](../../strongs/h/h834.md) he was [gālaḥ](../../strongs/h/h1548.md).

<a name="judges_16_23"></a>Judges 16:23

Then the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) ['āsap̄](../../strongs/h/h622.md) them for to [zabach](../../strongs/h/h2076.md) a [gadowl](../../strongs/h/h1419.md) [zebach](../../strongs/h/h2077.md) unto [Dāḡôn](../../strongs/h/h1712.md) their ['Elohiym](../../strongs/h/h430.md), and to [simchah](../../strongs/h/h8057.md): for they ['āmar](../../strongs/h/h559.md), Our ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) [Šimšôn](../../strongs/h/h8123.md) our ['oyeb](../../strongs/h/h341.md) into our [yad](../../strongs/h/h3027.md).

<a name="judges_16_24"></a>Judges 16:24

And when the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md) him, they [halal](../../strongs/h/h1984.md) their ['Elohiym](../../strongs/h/h430.md): for they ['āmar](../../strongs/h/h559.md), Our ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) into our [yad](../../strongs/h/h3027.md) our ['oyeb](../../strongs/h/h341.md), and the [ḥāraḇ](../../strongs/h/h2717.md) of our ['erets](../../strongs/h/h776.md), which [ḥālāl](../../strongs/h/h2491.md) [rabah](../../strongs/h/h7235.md) of us.

<a name="judges_16_25"></a>Judges 16:25

And it came to pass, when their [leb](../../strongs/h/h3820.md) were [towb](../../strongs/h/h2896.md), that they ['āmar](../../strongs/h/h559.md), [qara'](../../strongs/h/h7121.md) for [Šimšôn](../../strongs/h/h8123.md), that he may make us [śāḥaq](../../strongs/h/h7832.md). And they [qara'](../../strongs/h/h7121.md) for [Šimšôn](../../strongs/h/h8123.md) out of the ['āsar](../../strongs/h/h631.md) [bayith](../../strongs/h/h1004.md); and he made [paniym](../../strongs/h/h6440.md) [ṣāḥaq](../../strongs/h/h6711.md): and they ['amad](../../strongs/h/h5975.md) him between the [ʿammûḏ](../../strongs/h/h5982.md).

<a name="judges_16_26"></a>Judges 16:26

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md) unto the [naʿar](../../strongs/h/h5288.md) that [ḥāzaq](../../strongs/h/h2388.md) him by the [yad](../../strongs/h/h3027.md), [yānaḥ](../../strongs/h/h3240.md) me that I may [mûš](../../strongs/h/h4184.md) H3237 the [ʿammûḏ](../../strongs/h/h5982.md) whereupon the [bayith](../../strongs/h/h1004.md) [kuwn](../../strongs/h/h3559.md), that I may [šāʿan](../../strongs/h/h8172.md) upon them.

<a name="judges_16_27"></a>Judges 16:27

Now the [bayith](../../strongs/h/h1004.md) was [mālā'](../../strongs/h/h4390.md) of ['enowsh](../../strongs/h/h582.md) and ['ishshah](../../strongs/h/h802.md); and all the [seren](../../strongs/h/h5633.md) of the [Pᵊlištî](../../strongs/h/h6430.md) were there; and there were upon the [gāḡ](../../strongs/h/h1406.md) about [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), that [ra'ah](../../strongs/h/h7200.md) while [Šimšôn](../../strongs/h/h8123.md) made [śāḥaq](../../strongs/h/h7832.md).

<a name="judges_16_28"></a>Judges 16:28

And [Šimšôn](../../strongs/h/h8123.md) [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [zakar](../../strongs/h/h2142.md) me, I pray thee, and [ḥāzaq](../../strongs/h/h2388.md) me, I pray thee, only this [pa'am](../../strongs/h/h6471.md), ['Elohiym](../../strongs/h/h430.md), that I may be at ['echad](../../strongs/h/h259.md) [naqam](../../strongs/h/h5358.md) [nāqām](../../strongs/h/h5359.md) of the [Pᵊlištî](../../strongs/h/h6430.md) for my [šᵊnayim](../../strongs/h/h8147.md) ['ayin](../../strongs/h/h5869.md).

<a name="judges_16_29"></a>Judges 16:29

And [Šimšôn](../../strongs/h/h8123.md) [lāp̄aṯ](../../strongs/h/h3943.md) of the [šᵊnayim](../../strongs/h/h8147.md) [tavek](../../strongs/h/h8432.md) [ʿammûḏ](../../strongs/h/h5982.md) upon which the [bayith](../../strongs/h/h1004.md) [kuwn](../../strongs/h/h3559.md), and on which it was [camak](../../strongs/h/h5564.md), of the ['echad](../../strongs/h/h259.md) with his [yamiyn](../../strongs/h/h3225.md), and of the ['echad](../../strongs/h/h259.md) with his [śᵊmō'l](../../strongs/h/h8040.md).

<a name="judges_16_30"></a>Judges 16:30

And [Šimšôn](../../strongs/h/h8123.md) ['āmar](../../strongs/h/h559.md), Let me [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md) with the [Pᵊlištî](../../strongs/h/h6430.md). And he [natah](../../strongs/h/h5186.md) himself with all his [koach](../../strongs/h/h3581.md); and the [bayith](../../strongs/h/h1004.md) [naphal](../../strongs/h/h5307.md) upon the [seren](../../strongs/h/h5633.md), and upon all the ['am](../../strongs/h/h5971.md) that were therein. So the [muwth](../../strongs/h/h4191.md) which he [muwth](../../strongs/h/h4191.md) at his [maveth](../../strongs/h/h4194.md) were [rab](../../strongs/h/h7227.md) than they which he [muwth](../../strongs/h/h4191.md) in his [chay](../../strongs/h/h2416.md).

<a name="judges_16_31"></a>Judges 16:31

Then his ['ach](../../strongs/h/h251.md) and all the [bayith](../../strongs/h/h1004.md) of his ['ab](../../strongs/h/h1.md) [yarad](../../strongs/h/h3381.md), and [nasa'](../../strongs/h/h5375.md) him, and brought him [ʿālâ](../../strongs/h/h5927.md), and [qāḇar](../../strongs/h/h6912.md) him between [ṢārʿÂ](../../strongs/h/h6881.md) and ['Eštā'Ōl](../../strongs/h/h847.md) in the [qeber](../../strongs/h/h6913.md) of [Mānôaḥ](../../strongs/h/h4495.md) his ['ab](../../strongs/h/h1.md). And he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 15](judges_15.md) - [Judges 17](judges_17.md)