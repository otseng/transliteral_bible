# [Judges 4](https://www.blueletterbible.org/kjv/judges/4)

<a name="judges_4_1"></a>Judges 4:1

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yāsap̄](../../strongs/h/h3254.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), when ['Êûḏ](../../strongs/h/h164.md) was [muwth](../../strongs/h/h4191.md).

<a name="judges_4_2"></a>Judges 4:2

And [Yĕhovah](../../strongs/h/h3068.md) [māḵar](../../strongs/h/h4376.md) them into the [yad](../../strongs/h/h3027.md) of [Yāḇîn](../../strongs/h/h2985.md) [melek](../../strongs/h/h4428.md) of [Kĕna'an](../../strongs/h/h3667.md), that [mālaḵ](../../strongs/h/h4427.md) in [Ḥāṣôr](../../strongs/h/h2674.md); the [śar](../../strongs/h/h8269.md) of whose [tsaba'](../../strongs/h/h6635.md) was [Sîsrā'](../../strongs/h/h5516.md), which [yashab](../../strongs/h/h3427.md) in [Ḥărōšeṯ](../../strongs/h/h2800.md) of the [gowy](../../strongs/h/h1471.md).

<a name="judges_4_3"></a>Judges 4:3

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md): for he had [tēšaʿ](../../strongs/h/h8672.md) [mē'â](../../strongs/h/h3967.md) [reḵeḇ](../../strongs/h/h7393.md) of [barzel](../../strongs/h/h1270.md); and [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md) he [ḥāzqâ](../../strongs/h/h2394.md) [lāḥaṣ](../../strongs/h/h3905.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_4_4"></a>Judges 4:4

And [Dᵊḇôrâ](../../strongs/h/h1683.md), a [nᵊḇî'â](../../strongs/h/h5031.md), the ['ishshah](../../strongs/h/h802.md) of [Lapîḏôṯ](../../strongs/h/h3941.md), she [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) at that [ʿēṯ](../../strongs/h/h6256.md).

<a name="judges_4_5"></a>Judges 4:5

And she [yashab](../../strongs/h/h3427.md) under the [tōmer](../../strongs/h/h8560.md) of [Dᵊḇôrâ](../../strongs/h/h1683.md) between [rāmâ](../../strongs/h/h7414.md) and [Bêṯ-'ēl](../../strongs/h/h1008.md) in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md): and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) to her for [mishpat](../../strongs/h/h4941.md).

<a name="judges_4_6"></a>Judges 4:6

And she [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) [Bārāq](../../strongs/h/h1301.md) the [ben](../../strongs/h/h1121.md) of ['ĂḇînōʿAm](../../strongs/h/h42.md) out of [Qeḏeš](../../strongs/h/h6943.md), and ['āmar](../../strongs/h/h559.md) unto him, Hath not [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [tsavah](../../strongs/h/h6680.md), saying, [yālaḵ](../../strongs/h/h3212.md) and [mashak](../../strongs/h/h4900.md) toward [har](../../strongs/h/h2022.md) [Tāḇôr](../../strongs/h/h8396.md), and [laqach](../../strongs/h/h3947.md) with thee [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) of the [ben](../../strongs/h/h1121.md) of [Nap̄tālî](../../strongs/h/h5321.md) and of the [ben](../../strongs/h/h1121.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md)?

<a name="judges_4_7"></a>Judges 4:7

And I will [mashak](../../strongs/h/h4900.md) unto thee to the [nachal](../../strongs/h/h5158.md) [Qîšôn](../../strongs/h/h7028.md) [Sîsrā'](../../strongs/h/h5516.md), the [śar](../../strongs/h/h8269.md) of [Yāḇîn](../../strongs/h/h2985.md) [tsaba'](../../strongs/h/h6635.md), with his [reḵeḇ](../../strongs/h/h7393.md) and his [hāmôn](../../strongs/h/h1995.md); and I will [nathan](../../strongs/h/h5414.md) him into thine [yad](../../strongs/h/h3027.md).

<a name="judges_4_8"></a>Judges 4:8

And [Bārāq](../../strongs/h/h1301.md) ['āmar](../../strongs/h/h559.md) unto her, If thou wilt [yālaḵ](../../strongs/h/h3212.md) with me, then I will [halak](../../strongs/h/h1980.md): but if thou wilt not [yālaḵ](../../strongs/h/h3212.md) with me, then I will not [yālaḵ](../../strongs/h/h3212.md).

<a name="judges_4_9"></a>Judges 4:9

And she ['āmar](../../strongs/h/h559.md), I will [halak](../../strongs/h/h1980.md) [yālaḵ](../../strongs/h/h3212.md) with thee: ['ep̄es](../../strongs/h/h657.md) the [derek](../../strongs/h/h1870.md) that thou [halak](../../strongs/h/h1980.md) shall not be for thine [tip̄'ārâ](../../strongs/h/h8597.md); for [Yĕhovah](../../strongs/h/h3068.md) shall [māḵar](../../strongs/h/h4376.md) [Sîsrā'](../../strongs/h/h5516.md) into the [yad](../../strongs/h/h3027.md) of an ['ishshah](../../strongs/h/h802.md). And [Dᵊḇôrâ](../../strongs/h/h1683.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) with [Bārāq](../../strongs/h/h1301.md) to [Qeḏeš](../../strongs/h/h6943.md).

<a name="judges_4_10"></a>Judges 4:10

And [Bārāq](../../strongs/h/h1301.md) [zāʿaq](../../strongs/h/h2199.md) [Zᵊḇûlûn](../../strongs/h/h2074.md) and [Nap̄tālî](../../strongs/h/h5321.md) to [Qeḏeš](../../strongs/h/h6943.md); and he [ʿālâ](../../strongs/h/h5927.md) with [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) at his [regel](../../strongs/h/h7272.md): and [Dᵊḇôrâ](../../strongs/h/h1683.md) [ʿālâ](../../strongs/h/h5927.md) with him.

<a name="judges_4_11"></a>Judges 4:11

Now [Ḥeḇer](../../strongs/h/h2268.md) the [Qênî](../../strongs/h/h7017.md), which was of the [ben](../../strongs/h/h1121.md) of [Ḥōḇāḇ](../../strongs/h/h2246.md) the [ḥāṯan](../../strongs/h/h2859.md) of [Mōshe](../../strongs/h/h4872.md), had [pāraḏ](../../strongs/h/h6504.md) himself from the [Qênî](../../strongs/h/h7017.md), and [natah](../../strongs/h/h5186.md) his ['ohel](../../strongs/h/h168.md) unto the ['ēlôn](../../strongs/h/h436.md) of [ṢaʿĂnannîm](../../strongs/h/h6815.md), which is by [Qeḏeš](../../strongs/h/h6943.md).

<a name="judges_4_12"></a>Judges 4:12

And they [nāḡaḏ](../../strongs/h/h5046.md) [Sîsrā'](../../strongs/h/h5516.md) that [Bārāq](../../strongs/h/h1301.md) the [ben](../../strongs/h/h1121.md) of ['ĂḇînōʿAm](../../strongs/h/h42.md) was [ʿālâ](../../strongs/h/h5927.md) to [har](../../strongs/h/h2022.md) [Tāḇôr](../../strongs/h/h8396.md).

<a name="judges_4_13"></a>Judges 4:13

And [Sîsrā'](../../strongs/h/h5516.md) [zāʿaq](../../strongs/h/h2199.md) all his [reḵeḇ](../../strongs/h/h7393.md), even [tēšaʿ](../../strongs/h/h8672.md) [mē'â](../../strongs/h/h3967.md) [reḵeḇ](../../strongs/h/h7393.md) of [barzel](../../strongs/h/h1270.md), and all the ['am](../../strongs/h/h5971.md) that were with him, from [Ḥărōšeṯ](../../strongs/h/h2800.md) of the [gowy](../../strongs/h/h1471.md) unto the [nachal](../../strongs/h/h5158.md) of [Qîšôn](../../strongs/h/h7028.md).

<a name="judges_4_14"></a>Judges 4:14

And [Dᵊḇôrâ](../../strongs/h/h1683.md) ['āmar](../../strongs/h/h559.md) unto [Bārāq](../../strongs/h/h1301.md), [quwm](../../strongs/h/h6965.md); for this is the [yowm](../../strongs/h/h3117.md) in which [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) [Sîsrā'](../../strongs/h/h5516.md) into thine [yad](../../strongs/h/h3027.md): is not [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) thee? So [Bārāq](../../strongs/h/h1301.md) [yarad](../../strongs/h/h3381.md) from [har](../../strongs/h/h2022.md) [Tāḇôr](../../strongs/h/h8396.md), and [ʿeśer](../../strongs/h/h6235.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="judges_4_15"></a>Judges 4:15

And [Yĕhovah](../../strongs/h/h3068.md) [hāmam](../../strongs/h/h2000.md) [Sîsrā'](../../strongs/h/h5516.md), and all his [reḵeḇ](../../strongs/h/h7393.md), and all his [maḥănê](../../strongs/h/h4264.md), with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md) [paniym](../../strongs/h/h6440.md) [Bārāq](../../strongs/h/h1301.md); so that [Sîsrā'](../../strongs/h/h5516.md) [yarad](../../strongs/h/h3381.md) off his [merkāḇâ](../../strongs/h/h4818.md), and [nûs](../../strongs/h/h5127.md) on his [regel](../../strongs/h/h7272.md).

<a name="judges_4_16"></a>Judges 4:16

But [Bārāq](../../strongs/h/h1301.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [reḵeḇ](../../strongs/h/h7393.md), and after the [maḥănê](../../strongs/h/h4264.md), unto [Ḥărōšeṯ](../../strongs/h/h2800.md) of the [gowy](../../strongs/h/h1471.md): and all the [maḥănê](../../strongs/h/h4264.md) of [Sîsrā'](../../strongs/h/h5516.md) [naphal](../../strongs/h/h5307.md) upon the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); and there was not an ['echad](../../strongs/h/h259.md) [šā'ar](../../strongs/h/h7604.md).

<a name="judges_4_17"></a>Judges 4:17

Howbeit [Sîsrā'](../../strongs/h/h5516.md) [nûs](../../strongs/h/h5127.md) on his [regel](../../strongs/h/h7272.md) to the ['ohel](../../strongs/h/h168.md) of [YāʿĒl](../../strongs/h/h3278.md) the ['ishshah](../../strongs/h/h802.md) of [Ḥeḇer](../../strongs/h/h2268.md) the [Qênî](../../strongs/h/h7017.md): for there was [shalowm](../../strongs/h/h7965.md) between [Yāḇîn](../../strongs/h/h2985.md) the [melek](../../strongs/h/h4428.md) of [Ḥāṣôr](../../strongs/h/h2674.md) and the [bayith](../../strongs/h/h1004.md) of [Ḥeḇer](../../strongs/h/h2268.md) the [Qênî](../../strongs/h/h7017.md).

<a name="judges_4_18"></a>Judges 4:18

And [YāʿĒl](../../strongs/h/h3278.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) [Sîsrā'](../../strongs/h/h5516.md), and ['āmar](../../strongs/h/h559.md) unto him, [cuwr](../../strongs/h/h5493.md), my ['adown](../../strongs/h/h113.md), [cuwr](../../strongs/h/h5493.md) to me; [yare'](../../strongs/h/h3372.md) not. And when he had [cuwr](../../strongs/h/h5493.md) unto her into the ['ohel](../../strongs/h/h168.md), she [kāsâ](../../strongs/h/h3680.md) him with a [śᵊmîḵâ](../../strongs/h/h8063.md).

<a name="judges_4_19"></a>Judges 4:19

And he ['āmar](../../strongs/h/h559.md) unto her, Give me, I pray thee, a [mᵊʿaṭ](../../strongs/h/h4592.md) [mayim](../../strongs/h/h4325.md) to [šāqâ](../../strongs/h/h8248.md); for I am [ṣāmē'](../../strongs/h/h6770.md). And she [pāṯaḥ](../../strongs/h/h6605.md) a [nō'ḏ](../../strongs/h/h4997.md) of [chalab](../../strongs/h/h2461.md), and gave him [šāqâ](../../strongs/h/h8248.md), and [kāsâ](../../strongs/h/h3680.md) him.

<a name="judges_4_20"></a>Judges 4:20

Again he ['āmar](../../strongs/h/h559.md) unto her, Stand ['amad](../../strongs/h/h5975.md) the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md), and it shall be, when any ['iysh](../../strongs/h/h376.md) doth [bow'](../../strongs/h/h935.md) and [sha'al](../../strongs/h/h7592.md) of thee, and ['āmar](../../strongs/h/h559.md), Is [yēš](../../strongs/h/h3426.md) any ['iysh](../../strongs/h/h376.md) [yēš](../../strongs/h/h3426.md)? that thou shalt ['āmar](../../strongs/h/h559.md), No.

<a name="judges_4_21"></a>Judges 4:21

Then [YāʿĒl](../../strongs/h/h3278.md) [Ḥeḇer](../../strongs/h/h2268.md) ['ishshah](../../strongs/h/h802.md) [laqach](../../strongs/h/h3947.md) a [yāṯēḏ](../../strongs/h/h3489.md) of the ['ohel](../../strongs/h/h168.md), and [śûm](../../strongs/h/h7760.md) a [maqqeḇeṯ](../../strongs/h/h4718.md) in her [yad](../../strongs/h/h3027.md), and [bow'](../../strongs/h/h935.md) [lā'ṭ](../../strongs/h/h3814.md) unto him, and [tāqaʿ](../../strongs/h/h8628.md) the [yāṯēḏ](../../strongs/h/h3489.md) into his [raqqâ](../../strongs/h/h7541.md), and [ṣānaḥ](../../strongs/h/h6795.md) it into the ['erets](../../strongs/h/h776.md): for he was fast [rāḏam](../../strongs/h/h7290.md) and ['uwph](../../strongs/h/h5774.md). So he [muwth](../../strongs/h/h4191.md).

<a name="judges_4_22"></a>Judges 4:22

And, behold, as [Bārāq](../../strongs/h/h1301.md) [radaph](../../strongs/h/h7291.md) [Sîsrā'](../../strongs/h/h5516.md), [YāʿĒl](../../strongs/h/h3278.md) [yāṣā'](../../strongs/h/h3318.md) to [qārā'](../../strongs/h/h7125.md) him, and ['āmar](../../strongs/h/h559.md) unto him, [yālaḵ](../../strongs/h/h3212.md), and I will [ra'ah](../../strongs/h/h7200.md) thee the ['iysh](../../strongs/h/h376.md) whom thou [bāqaš](../../strongs/h/h1245.md). And when he [bow'](../../strongs/h/h935.md) into her, behold, [Sîsrā'](../../strongs/h/h5516.md) [naphal](../../strongs/h/h5307.md) [muwth](../../strongs/h/h4191.md), and the [yāṯēḏ](../../strongs/h/h3489.md) was in his [raqqâ](../../strongs/h/h7541.md).

<a name="judges_4_23"></a>Judges 4:23

So ['Elohiym](../../strongs/h/h430.md) [kānaʿ](../../strongs/h/h3665.md) on that [yowm](../../strongs/h/h3117.md) [Yāḇîn](../../strongs/h/h2985.md) the [melek](../../strongs/h/h4428.md) of [Kĕna'an](../../strongs/h/h3667.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_4_24"></a>Judges 4:24

And the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) [halak](../../strongs/h/h1980.md), and [qāšê](../../strongs/h/h7186.md) against [Yāḇîn](../../strongs/h/h2985.md) the [melek](../../strongs/h/h4428.md) of [Kĕna'an](../../strongs/h/h3667.md), until they had [karath](../../strongs/h/h3772.md) [Yāḇîn](../../strongs/h/h2985.md) [melek](../../strongs/h/h4428.md) of [Kĕna'an](../../strongs/h/h3667.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 3](judges_3.md) - [Judges 5](judges_5.md)