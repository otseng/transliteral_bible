# [Judges 12](https://www.blueletterbible.org/kjv/judges/12)

<a name="judges_12_1"></a>Judges 12:1

And the ['iysh](../../strongs/h/h376.md) of ['Ep̄rayim](../../strongs/h/h669.md) [ṣāʿaq](../../strongs/h/h6817.md), and ['abar](../../strongs/h/h5674.md) [ṣāp̄ôn](../../strongs/h/h6828.md), and ['āmar](../../strongs/h/h559.md) unto [Yip̄tāḥ](../../strongs/h/h3316.md), Wherefore ['abar](../../strongs/h/h5674.md) thou to [lāḥam](../../strongs/h/h3898.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and didst not [qara'](../../strongs/h/h7121.md) us to [yālaḵ](../../strongs/h/h3212.md) with thee? we will [śārap̄](../../strongs/h/h8313.md) thine [bayith](../../strongs/h/h1004.md) upon thee with ['esh](../../strongs/h/h784.md).

<a name="judges_12_2"></a>Judges 12:2

And [Yip̄tāḥ](../../strongs/h/h3316.md) ['āmar](../../strongs/h/h559.md) unto them, I and my ['am](../../strongs/h/h5971.md) [hayah](../../strongs/h/h1961.md) at [me'od](../../strongs/h/h3966.md) ['iysh](../../strongs/h/h376.md) [rîḇ](../../strongs/h/h7379.md) with the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md); and when I [zāʿaq](../../strongs/h/h2199.md) you, ye [yasha'](../../strongs/h/h3467.md) me not out of their [yad](../../strongs/h/h3027.md).

<a name="judges_12_3"></a>Judges 12:3

And when I [ra'ah](../../strongs/h/h7200.md) that ye [yasha'](../../strongs/h/h3467.md) me not, I [śûm](../../strongs/h/h7760.md) my [nephesh](../../strongs/h/h5315.md) in my [kaph](../../strongs/h/h3709.md), and ['abar](../../strongs/h/h5674.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them into my [yad](../../strongs/h/h3027.md): wherefore then are ye [ʿālâ](../../strongs/h/h5927.md) unto me this [yowm](../../strongs/h/h3117.md), to [lāḥam](../../strongs/h/h3898.md) against me?

<a name="judges_12_4"></a>Judges 12:4

Then [Yip̄tāḥ](../../strongs/h/h3316.md) [qāḇaṣ](../../strongs/h/h6908.md) all the ['enowsh](../../strongs/h/h582.md) of [Gilʿāḏ](../../strongs/h/h1568.md), and [lāḥam](../../strongs/h/h3898.md) with ['Ep̄rayim](../../strongs/h/h669.md): and the ['enowsh](../../strongs/h/h582.md) of [Gilʿāḏ](../../strongs/h/h1568.md) [nakah](../../strongs/h/h5221.md) ['Ep̄rayim](../../strongs/h/h669.md), because they ['āmar](../../strongs/h/h559.md), Ye [Gilʿāḏ](../../strongs/h/h1568.md) are [pālîṭ](../../strongs/h/h6412.md) of ['Ep̄rayim](../../strongs/h/h669.md) [tavek](../../strongs/h/h8432.md) the ['Ep̄rayim](../../strongs/h/h669.md), and [tavek](../../strongs/h/h8432.md) the [Mᵊnaššê](../../strongs/h/h4519.md).

<a name="judges_12_5"></a>Judges 12:5

And the [Gilʿāḏ](../../strongs/h/h1568.md) [lāḵaḏ](../../strongs/h/h3920.md) the [maʿăḇār](../../strongs/h/h4569.md) of [Yardēn](../../strongs/h/h3383.md) before the ['Ep̄rayim](../../strongs/h/h669.md): and it was so, that when those ['Ep̄rayim](../../strongs/h/h669.md) which were [pālîṭ](../../strongs/h/h6412.md) ['āmar](../../strongs/h/h559.md), Let me ['abar](../../strongs/h/h5674.md); that the ['enowsh](../../strongs/h/h582.md) of [Gilʿāḏ](../../strongs/h/h1568.md) ['āmar](../../strongs/h/h559.md) unto him, Art thou an ['ep̄rāṯî](../../strongs/h/h673.md)? If he ['āmar](../../strongs/h/h559.md), Nay;

<a name="judges_12_6"></a>Judges 12:6

Then ['āmar](../../strongs/h/h559.md) they unto him, ['āmar](../../strongs/h/h559.md) now [šibōleṯ](../../strongs/h/h7641.md): and he ['āmar](../../strongs/h/h559.md) [sibōleṯ](../../strongs/h/h5451.md): for he could not [kuwn](../../strongs/h/h3559.md) to [dabar](../../strongs/h/h1696.md) it [kuwn](../../strongs/h/h3559.md). Then they ['āḥaz](../../strongs/h/h270.md) him, and [šāḥaṭ](../../strongs/h/h7819.md) him at the [maʿăḇār](../../strongs/h/h4569.md) of [Yardēn](../../strongs/h/h3383.md): and there [naphal](../../strongs/h/h5307.md) at that [ʿēṯ](../../strongs/h/h6256.md) of the ['Ep̄rayim](../../strongs/h/h669.md) ['arbāʿîm](../../strongs/h/h705.md) and [šᵊnayim](../../strongs/h/h8147.md) ['elep̄](../../strongs/h/h505.md).

<a name="judges_12_7"></a>Judges 12:7

And [Yip̄tāḥ](../../strongs/h/h3316.md) [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [šēš](../../strongs/h/h8337.md) [šānâ](../../strongs/h/h8141.md). Then [muwth](../../strongs/h/h4191.md) [Yip̄tāḥ](../../strongs/h/h3316.md) the [Gilʿāḏî](../../strongs/h/h1569.md), and was [qāḇar](../../strongs/h/h6912.md) in one of the [ʿîr](../../strongs/h/h5892.md) of [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="judges_12_8"></a>Judges 12:8

And ['aḥar](../../strongs/h/h310.md) him ['Iḇṣān](../../strongs/h/h78.md) of [Bêṯ leḥem](../../strongs/h/h1035.md) [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_12_9"></a>Judges 12:9

And he had [šᵊlōšîm](../../strongs/h/h7970.md) [ben](../../strongs/h/h1121.md), and [šᵊlōšîm](../../strongs/h/h7970.md) [bath](../../strongs/h/h1323.md), whom he [shalach](../../strongs/h/h7971.md) [ḥûṣ](../../strongs/h/h2351.md), and [bow'](../../strongs/h/h935.md) in [šᵊlōšîm](../../strongs/h/h7970.md) [bath](../../strongs/h/h1323.md) from [ḥûṣ](../../strongs/h/h2351.md) for his [ben](../../strongs/h/h1121.md). And he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [šeḇaʿ](../../strongs/h/h7651.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_12_10"></a>Judges 12:10

Then [muwth](../../strongs/h/h4191.md) ['Iḇṣān](../../strongs/h/h78.md), and was [qāḇar](../../strongs/h/h6912.md) at [Bêṯ leḥem](../../strongs/h/h1035.md).

<a name="judges_12_11"></a>Judges 12:11

And ['aḥar](../../strongs/h/h310.md) him ['êlôn](../../strongs/h/h356.md), a [zᵊḇûlōnî](../../strongs/h/h2075.md), [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md); and he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [ʿeśer](../../strongs/h/h6235.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_12_12"></a>Judges 12:12

And ['êlôn](../../strongs/h/h356.md) the [zᵊḇûlōnî](../../strongs/h/h2075.md) [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in ['Ayyālôn](../../strongs/h/h357.md) in the ['erets](../../strongs/h/h776.md) of [Zᵊḇûlûn](../../strongs/h/h2074.md).

<a name="judges_12_13"></a>Judges 12:13

And ['aḥar](../../strongs/h/h310.md) him [ʿAḇdôn](../../strongs/h/h5658.md) the [ben](../../strongs/h/h1121.md) of [Hillēl](../../strongs/h/h1985.md), a [Pirʿāṯônî](../../strongs/h/h6553.md), [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_12_14"></a>Judges 12:14

And he had ['arbāʿîm](../../strongs/h/h705.md) [ben](../../strongs/h/h1121.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), that [rāḵaḇ](../../strongs/h/h7392.md) on [šiḇʿîm](../../strongs/h/h7657.md) ass [ʿayir](../../strongs/h/h5895.md): and he [shaphat](../../strongs/h/h8199.md) [Yisra'el](../../strongs/h/h3478.md) [šᵊmōnê](../../strongs/h/h8083.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_12_15"></a>Judges 12:15

And [ʿAḇdôn](../../strongs/h/h5658.md) the [ben](../../strongs/h/h1121.md) of [Hillēl](../../strongs/h/h1985.md) the [Pirʿāṯônî](../../strongs/h/h6553.md) [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in [PirʿĀṯôn](../../strongs/h/h6552.md) in the ['erets](../../strongs/h/h776.md) of ['Ep̄rayim](../../strongs/h/h669.md), in the [har](../../strongs/h/h2022.md) of the [ʿămālēqî](../../strongs/h/h6003.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 11](judges_11.md) - [Judges 13](judges_13.md)