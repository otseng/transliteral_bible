# [Judges 19](https://www.blueletterbible.org/kjv/judges/19)

<a name="judges_19_1"></a>Judges 19:1

And it came to pass in those [yowm](../../strongs/h/h3117.md), when there was no [melek](../../strongs/h/h4428.md) in [Yisra'el](../../strongs/h/h3478.md), that there was an ['iysh](../../strongs/h/h376.md) [Lᵊvî](../../strongs/h/h3881.md) [guwr](../../strongs/h/h1481.md) on the [yᵊrēḵâ](../../strongs/h/h3411.md) of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), who [laqach](../../strongs/h/h3947.md) to him an ['ishshah](../../strongs/h/h802.md) [pîleḡeš](../../strongs/h/h6370.md) out of [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md).

<a name="judges_19_2"></a>Judges 19:2

And his [pîleḡeš](../../strongs/h/h6370.md) played the [zānâ](../../strongs/h/h2181.md) against him, and [yālaḵ](../../strongs/h/h3212.md) from him unto her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) to [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md), and was there ['arbaʿ](../../strongs/h/h702.md) whole [ḥōḏeš](../../strongs/h/h2320.md) [yowm](../../strongs/h/h3117.md).

<a name="judges_19_3"></a>Judges 19:3

And her ['iysh](../../strongs/h/h376.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) her, to [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto her, and to [shuwb](../../strongs/h/h7725.md) her, having his [naʿar](../../strongs/h/h5288.md) with him, and a [ṣemeḏ](../../strongs/h/h6776.md) of [chamowr](../../strongs/h/h2543.md): and she [bow'](../../strongs/h/h935.md) him into her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md): and when the ['ab](../../strongs/h/h1.md) of the [naʿărâ](../../strongs/h/h5291.md) [ra'ah](../../strongs/h/h7200.md) him, he [samach](../../strongs/h/h8055.md) to [qārā'](../../strongs/h/h7125.md) him.

<a name="judges_19_4"></a>Judges 19:4

And his [ḥāṯan](../../strongs/h/h2859.md), the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md), [ḥāzaq](../../strongs/h/h2388.md) him; and he [yashab](../../strongs/h/h3427.md) with him [šālôš](../../strongs/h/h7969.md) [yowm](../../strongs/h/h3117.md): so they did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and [lûn](../../strongs/h/h3885.md) there.

<a name="judges_19_5"></a>Judges 19:5

And it came to pass on the [rᵊḇîʿî](../../strongs/h/h7243.md) [yowm](../../strongs/h/h3117.md), when they a[šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), that he [quwm](../../strongs/h/h6965.md) to [yālaḵ](../../strongs/h/h3212.md): and the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md) unto his [ḥāṯān](../../strongs/h/h2860.md), [sāʿaḏ](../../strongs/h/h5582.md) thine [leb](../../strongs/h/h3820.md) with a [paṯ](../../strongs/h/h6595.md) of [lechem](../../strongs/h/h3899.md), and ['aḥar](../../strongs/h/h310.md) go your [yālaḵ](../../strongs/h/h3212.md).

<a name="judges_19_6"></a>Judges 19:6

And they [yashab](../../strongs/h/h3427.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md) [šᵊnayim](../../strongs/h/h8147.md) of them [yaḥaḏ](../../strongs/h/h3162.md): for the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md) had ['āmar](../../strongs/h/h559.md) unto the ['iysh](../../strongs/h/h376.md), Be [yā'al](../../strongs/h/h2974.md), I pray thee, and tarry all [lûn](../../strongs/h/h3885.md), and let thine [leb](../../strongs/h/h3820.md) be [yatab](../../strongs/h/h3190.md).

<a name="judges_19_7"></a>Judges 19:7

And when the ['iysh](../../strongs/h/h376.md) [quwm](../../strongs/h/h6965.md) to [yālaḵ](../../strongs/h/h3212.md), his [ḥāṯan](../../strongs/h/h2859.md) [pāṣar](../../strongs/h/h6484.md) him: therefore he [lûn](../../strongs/h/h3885.md) there [shuwb](../../strongs/h/h7725.md).

<a name="judges_19_8"></a>Judges 19:8

And he a[šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md) on the [ḥămîšî](../../strongs/h/h2549.md) [yowm](../../strongs/h/h3117.md) to [yālaḵ](../../strongs/h/h3212.md): and the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md) ['āmar](../../strongs/h/h559.md), [sāʿaḏ](../../strongs/h/h5582.md) thine [lebab](../../strongs/h/h3824.md), I pray thee. And they [māhah](../../strongs/h/h4102.md) until [yowm](../../strongs/h/h3117.md) [natah](../../strongs/h/h5186.md), and they did ['akal](../../strongs/h/h398.md) [šᵊnayim](../../strongs/h/h8147.md) of them.

<a name="judges_19_9"></a>Judges 19:9

And when the ['iysh](../../strongs/h/h376.md) [quwm](../../strongs/h/h6965.md) to [yālaḵ](../../strongs/h/h3212.md), he, and his [pîleḡeš](../../strongs/h/h6370.md), and his [naʿar](../../strongs/h/h5288.md), his [ḥāṯan](../../strongs/h/h2859.md), the [naʿărâ](../../strongs/h/h5291.md) ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md) unto him, Behold, now the [yowm](../../strongs/h/h3117.md) [rāp̄â](../../strongs/h/h7503.md) toward [ʿāraḇ](../../strongs/h/h6150.md), I pray you [lûn](../../strongs/h/h3885.md): behold, the [yowm](../../strongs/h/h3117.md) [ḥānâ](../../strongs/h/h2583.md), [lûn](../../strongs/h/h3885.md) here, that thine [lebab](../../strongs/h/h3824.md) may be [yatab](../../strongs/h/h3190.md); and [māḥār](../../strongs/h/h4279.md) get you [šāḵam](../../strongs/h/h7925.md) on your [derek](../../strongs/h/h1870.md), that thou mayest [halak](../../strongs/h/h1980.md) ['ohel](../../strongs/h/h168.md).

<a name="judges_19_10"></a>Judges 19:10

But the ['iysh](../../strongs/h/h376.md) ['āḇâ](../../strongs/h/h14.md) not [lûn](../../strongs/h/h3885.md), but he [quwm](../../strongs/h/h6965.md) and [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) over [nōḵaḥ](../../strongs/h/h5227.md) [Yᵊḇûs](../../strongs/h/h2982.md), which is [Yĕruwshalaim](../../strongs/h/h3389.md); and there were with him [ṣemeḏ](../../strongs/h/h6776.md) [chamowr](../../strongs/h/h2543.md) [ḥāḇaš](../../strongs/h/h2280.md), his [pîleḡeš](../../strongs/h/h6370.md) also was with him.

<a name="judges_19_11"></a>Judges 19:11

And when they were by [Yᵊḇûs](../../strongs/h/h2982.md), the [yowm](../../strongs/h/h3117.md) was [me'od](../../strongs/h/h3966.md) [rāḏaḏ](../../strongs/h/h7286.md); and the [naʿar](../../strongs/h/h5288.md) ['āmar](../../strongs/h/h559.md) unto his ['adown](../../strongs/h/h113.md), [yālaḵ](../../strongs/h/h3212.md), I pray thee, and let us [cuwr](../../strongs/h/h5493.md) into this [ʿîr](../../strongs/h/h5892.md) of the [Yᵊḇûsî](../../strongs/h/h2983.md), and [lûn](../../strongs/h/h3885.md) in it.

<a name="judges_19_12"></a>Judges 19:12

And his ['adown](../../strongs/h/h113.md) ['āmar](../../strongs/h/h559.md) unto him, We will not [cuwr](../../strongs/h/h5493.md) hither into the [ʿîr](../../strongs/h/h5892.md) of a [nāḵrî](../../strongs/h/h5237.md), that is not of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md); we will ['abar](../../strongs/h/h5674.md) to [giḇʿâ](../../strongs/h/h1390.md).

<a name="judges_19_13"></a>Judges 19:13

And he ['āmar](../../strongs/h/h559.md) unto his [naʿar](../../strongs/h/h5288.md), [yālaḵ](../../strongs/h/h3212.md), and let us draw [qāraḇ](../../strongs/h/h7126.md) to ['echad](../../strongs/h/h259.md) of these [maqowm](../../strongs/h/h4725.md) to [lûn](../../strongs/h/h3885.md), in [giḇʿâ](../../strongs/h/h1390.md), or in [rāmâ](../../strongs/h/h7414.md).

<a name="judges_19_14"></a>Judges 19:14

And they ['abar](../../strongs/h/h5674.md) and [yālaḵ](../../strongs/h/h3212.md); and the [šemeš](../../strongs/h/h8121.md) [bow'](../../strongs/h/h935.md) upon them when they were ['ēṣel](../../strongs/h/h681.md) [giḇʿâ](../../strongs/h/h1390.md), which belongeth to [Binyāmîn](../../strongs/h/h1144.md).

<a name="judges_19_15"></a>Judges 19:15

And they [cuwr](../../strongs/h/h5493.md) thither, to [bow'](../../strongs/h/h935.md) and to [lûn](../../strongs/h/h3885.md) in [giḇʿâ](../../strongs/h/h1390.md): and when he [bow'](../../strongs/h/h935.md), he [yashab](../../strongs/h/h3427.md) him in a [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md): for there was no ['iysh](../../strongs/h/h376.md) that ['āsap̄](../../strongs/h/h622.md) them into his [bayith](../../strongs/h/h1004.md) to [lûn](../../strongs/h/h3885.md).

<a name="judges_19_16"></a>Judges 19:16

And, behold, there [bow'](../../strongs/h/h935.md) a [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md) from his [ma'aseh](../../strongs/h/h4639.md) out of the [sadeh](../../strongs/h/h7704.md) at ['ereb](../../strongs/h/h6153.md), which was ['iysh](../../strongs/h/h376.md) of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md); and he [guwr](../../strongs/h/h1481.md) in [giḇʿâ](../../strongs/h/h1390.md): but the ['enowsh](../../strongs/h/h582.md) of the [maqowm](../../strongs/h/h4725.md) were [Ben-yᵊmînî](../../strongs/h/h1145.md).

<a name="judges_19_17"></a>Judges 19:17

And when he had [nasa'](../../strongs/h/h5375.md) his ['ayin](../../strongs/h/h5869.md), he [ra'ah](../../strongs/h/h7200.md) an ['āraḥ](../../strongs/h/h732.md) ['iysh](../../strongs/h/h376.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md): and the [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md), Whither [yālaḵ](../../strongs/h/h3212.md) thou? and ['ayin](../../strongs/h/h370.md) [bow'](../../strongs/h/h935.md) thou?

<a name="judges_19_18"></a>Judges 19:18

And he ['āmar](../../strongs/h/h559.md) unto him, We are ['abar](../../strongs/h/h5674.md) from [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md) toward the [yᵊrēḵâ](../../strongs/h/h3411.md) of [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md); from thence am I: and I [yālaḵ](../../strongs/h/h3212.md) to [Bêṯ leḥem](../../strongs/h/h1035.md) [Yehuwdah](../../strongs/h/h3063.md), but I am now [halak](../../strongs/h/h1980.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); and there is no ['iysh](../../strongs/h/h376.md) that ['āsap̄](../../strongs/h/h622.md) me to [bayith](../../strongs/h/h1004.md).

<a name="judges_19_19"></a>Judges 19:19

Yet there [yēš](../../strongs/h/h3426.md) both [teḇen](../../strongs/h/h8401.md) and [mispô'](../../strongs/h/h4554.md) for our [chamowr](../../strongs/h/h2543.md); and there is [lechem](../../strongs/h/h3899.md) and [yayin](../../strongs/h/h3196.md) also for me, and for thy ['amah](../../strongs/h/h519.md), and for the [naʿar](../../strongs/h/h5288.md) which is with thy ['ebed](../../strongs/h/h5650.md): there is no [maḥsôr](../../strongs/h/h4270.md) of any [dabar](../../strongs/h/h1697.md).

<a name="judges_19_20"></a>Judges 19:20

And the [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md) ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md) be with thee; [raq](../../strongs/h/h7535.md) let all thy [maḥsôr](../../strongs/h/h4270.md) lie upon me; only [lûn](../../strongs/h/h3885.md) not in the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="judges_19_21"></a>Judges 19:21

So he [bow'](../../strongs/h/h935.md) him into his [bayith](../../strongs/h/h1004.md), and [bālal](../../strongs/h/h1101.md) unto the [chamowr](../../strongs/h/h2543.md): and they [rāḥaṣ](../../strongs/h/h7364.md) their [regel](../../strongs/h/h7272.md), and did ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md).

<a name="judges_19_22"></a>Judges 19:22

Now as they were making their [leb](../../strongs/h/h3820.md) [yatab](../../strongs/h/h3190.md), behold, the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md), ['enowsh](../../strongs/h/h582.md) [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), [cabab](../../strongs/h/h5437.md) the [bayith](../../strongs/h/h1004.md) [cabab](../../strongs/h/h5437.md), and [dāp̄aq](../../strongs/h/h1849.md) at the [deleṯ](../../strongs/h/h1817.md), and ['āmar](../../strongs/h/h559.md) to the [baʿal](../../strongs/h/h1167.md) of the [bayith](../../strongs/h/h1004.md), the [zāqēn](../../strongs/h/h2205.md) ['iysh](../../strongs/h/h376.md), ['āmar](../../strongs/h/h559.md), [yāṣā'](../../strongs/h/h3318.md) the ['iysh](../../strongs/h/h376.md) that [bow'](../../strongs/h/h935.md) into thine [bayith](../../strongs/h/h1004.md), that we may [yada'](../../strongs/h/h3045.md) him.

<a name="judges_19_23"></a>Judges 19:23

And the ['iysh](../../strongs/h/h376.md), the [baʿal](../../strongs/h/h1167.md) of the [bayith](../../strongs/h/h1004.md), [yāṣā'](../../strongs/h/h3318.md) unto them, and ['āmar](../../strongs/h/h559.md) unto them, ['al](../../strongs/h/h408.md), my ['ach](../../strongs/h/h251.md), nay, I pray you, do not so [ra'a'](../../strongs/h/h7489.md); ['aḥar](../../strongs/h/h310.md) that this ['iysh](../../strongs/h/h376.md) is [bow'](../../strongs/h/h935.md) into mine [bayith](../../strongs/h/h1004.md), ['asah](../../strongs/h/h6213.md) not this [nᵊḇālâ](../../strongs/h/h5039.md).

<a name="judges_19_24"></a>Judges 19:24

Behold, here is my [bath](../../strongs/h/h1323.md) a [bᵊṯûlâ](../../strongs/h/h1330.md), and his [pîleḡeš](../../strongs/h/h6370.md); them I will [yāṣā'](../../strongs/h/h3318.md) now, and [ʿānâ](../../strongs/h/h6031.md) ye them, and ['asah](../../strongs/h/h6213.md) with them what ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) unto you: but unto this ['iysh](../../strongs/h/h376.md) ['asah](../../strongs/h/h6213.md) not [zō'ṯ](../../strongs/h/h2063.md) [nᵊḇālâ](../../strongs/h/h5039.md) a [dabar](../../strongs/h/h1697.md).

<a name="judges_19_25"></a>Judges 19:25

But the ['enowsh](../../strongs/h/h582.md) ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) to him: so the ['iysh](../../strongs/h/h376.md) [ḥāzaq](../../strongs/h/h2388.md) his [pîleḡeš](../../strongs/h/h6370.md), and [yāṣā'](../../strongs/h/h3318.md) her [ḥûṣ](../../strongs/h/h2351.md) unto them; and they [yada'](../../strongs/h/h3045.md) her, and [ʿālal](../../strongs/h/h5953.md) her all the [layil](../../strongs/h/h3915.md) until the [boqer](../../strongs/h/h1242.md): and when the [šaḥar](../../strongs/h/h7837.md) began to [ʿālâ](../../strongs/h/h5927.md), they let her [shalach](../../strongs/h/h7971.md).

<a name="judges_19_26"></a>Judges 19:26

Then [bow'](../../strongs/h/h935.md) the ['ishshah](../../strongs/h/h802.md) in the [panah](../../strongs/h/h6437.md) of the [boqer](../../strongs/h/h1242.md), and [naphal](../../strongs/h/h5307.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['iysh](../../strongs/h/h376.md) [bayith](../../strongs/h/h1004.md) where her ['adown](../../strongs/h/h113.md) was, till it was ['owr](../../strongs/h/h216.md).

<a name="judges_19_27"></a>Judges 19:27

And her ['adown](../../strongs/h/h113.md) [quwm](../../strongs/h/h6965.md) in the [boqer](../../strongs/h/h1242.md), and [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md), and [yāṣā'](../../strongs/h/h3318.md) to [yālaḵ](../../strongs/h/h3212.md) his [derek](../../strongs/h/h1870.md): and, behold, the ['ishshah](../../strongs/h/h802.md) his [pîleḡeš](../../strongs/h/h6370.md) was [naphal](../../strongs/h/h5307.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md), and her [yad](../../strongs/h/h3027.md) were upon the [caph](../../strongs/h/h5592.md).

<a name="judges_19_28"></a>Judges 19:28

And he ['āmar](../../strongs/h/h559.md) unto her, [quwm](../../strongs/h/h6965.md), and let us be [yālaḵ](../../strongs/h/h3212.md). But none ['anah](../../strongs/h/h6030.md). Then the ['iysh](../../strongs/h/h376.md) [laqach](../../strongs/h/h3947.md) her up upon a [chamowr](../../strongs/h/h2543.md), and the ['iysh](../../strongs/h/h376.md) [quwm](../../strongs/h/h6965.md), and [yālaḵ](../../strongs/h/h3212.md) him unto his [maqowm](../../strongs/h/h4725.md).

<a name="judges_19_29"></a>Judges 19:29

And when he was [bow'](../../strongs/h/h935.md) into his [bayith](../../strongs/h/h1004.md), he [laqach](../../strongs/h/h3947.md) a [ma'ăḵeleṯ](../../strongs/h/h3979.md), and [ḥāzaq](../../strongs/h/h2388.md) on his [pîleḡeš](../../strongs/h/h6370.md), and [nāṯaḥ](../../strongs/h/h5408.md) her, together with her ['etsem](../../strongs/h/h6106.md), into [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [nēṯaḥ](../../strongs/h/h5409.md), and [shalach](../../strongs/h/h7971.md) her into all the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_19_30"></a>Judges 19:30

And it was [hayah](../../strongs/h/h1961.md), that all that [ra'ah](../../strongs/h/h7200.md) it ['āmar](../../strongs/h/h559.md), There was no such [zō'ṯ](../../strongs/h/h2063.md) done nor [ra'ah](../../strongs/h/h7200.md) from the [yowm](../../strongs/h/h3117.md) that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) unto this [yowm](../../strongs/h/h3117.md): [śûm](../../strongs/h/h7760.md) of it, take [ʿÛṣ](../../strongs/h/h5779.md), and [dabar](../../strongs/h/h1696.md) your minds.

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 18](judges_18.md) - [Judges 20](judges_20.md)