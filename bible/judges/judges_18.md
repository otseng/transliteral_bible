# [Judges 18](https://www.blueletterbible.org/kjv/judges/18)

<a name="judges_18_1"></a>Judges 18:1

In those [yowm](../../strongs/h/h3117.md) there was no [melek](../../strongs/h/h4428.md) in [Yisra'el](../../strongs/h/h3478.md): and in those [yowm](../../strongs/h/h3117.md) the [shebet](../../strongs/h/h7626.md) of the [dānî](../../strongs/h/h1839.md) [bāqaš](../../strongs/h/h1245.md) them a [nachalah](../../strongs/h/h5159.md) to [yashab](../../strongs/h/h3427.md); for unto that [yowm](../../strongs/h/h3117.md) all their [nachalah](../../strongs/h/h5159.md) had not [naphal](../../strongs/h/h5307.md) unto them [tavek](../../strongs/h/h8432.md) the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="judges_18_2"></a>Judges 18:2

And the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [shalach](../../strongs/h/h7971.md) of their [mišpāḥâ](../../strongs/h/h4940.md) [ḥāmēš](../../strongs/h/h2568.md) ['enowsh](../../strongs/h/h582.md) from their [qāṣâ](../../strongs/h/h7098.md), [ben](../../strongs/h/h1121.md) of [ḥayil](../../strongs/h/h2428.md), from [ṢārʿÂ](../../strongs/h/h6881.md), and from ['Eštā'Ōl](../../strongs/h/h847.md), to [ragal](../../strongs/h/h7270.md) out the ['erets](../../strongs/h/h776.md), and to [chaqar](../../strongs/h/h2713.md) it; and they ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md), [chaqar](../../strongs/h/h2713.md) the ['erets](../../strongs/h/h776.md): who when they [bow'](../../strongs/h/h935.md) to [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), to the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md), they [lûn](../../strongs/h/h3885.md) there.

<a name="judges_18_3"></a>Judges 18:3

When they were by the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md), they [nāḵar](../../strongs/h/h5234.md) the [qowl](../../strongs/h/h6963.md) of the [naʿar](../../strongs/h/h5288.md) the [Lᵊvî](../../strongs/h/h3881.md): and they turned [cuwr](../../strongs/h/h5493.md) thither, and ['āmar](../../strongs/h/h559.md) unto him, Who [bow'](../../strongs/h/h935.md) thee [hălōm](../../strongs/h/h1988.md)? and what ['asah](../../strongs/h/h6213.md) thou in this place? and what hast thou here?

<a name="judges_18_4"></a>Judges 18:4

And he ['āmar](../../strongs/h/h559.md) unto them, [zô](../../strongs/h/h2090.md) and [zô](../../strongs/h/h2090.md) ['asah](../../strongs/h/h6213.md) [Mîḵâ](../../strongs/h/h4318.md) with me, and hath [śāḵar](../../strongs/h/h7936.md) me, and I am his [kōhēn](../../strongs/h/h3548.md).

<a name="judges_18_5"></a>Judges 18:5

And they ['āmar](../../strongs/h/h559.md) unto him, Ask [sha'al](../../strongs/h/h7592.md), we pray thee, of ['Elohiym](../../strongs/h/h430.md), that we may [yada'](../../strongs/h/h3045.md) whether our [derek](../../strongs/h/h1870.md) which we [halak](../../strongs/h/h1980.md) shall be [tsalach](../../strongs/h/h6743.md).

<a name="judges_18_6"></a>Judges 18:6

And the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) unto them, [yālaḵ](../../strongs/h/h3212.md) in [shalowm](../../strongs/h/h7965.md): [nōḵaḥ](../../strongs/h/h5227.md) [Yĕhovah](../../strongs/h/h3068.md) is your [derek](../../strongs/h/h1870.md) wherein ye [yālaḵ](../../strongs/h/h3212.md).

<a name="judges_18_7"></a>Judges 18:7

Then the [ḥāmēš](../../strongs/h/h2568.md) ['enowsh](../../strongs/h/h582.md) [yālaḵ](../../strongs/h/h3212.md), and [bow'](../../strongs/h/h935.md) to [layiš](../../strongs/h/h3919.md), and [ra'ah](../../strongs/h/h7200.md) the ['am](../../strongs/h/h5971.md) that were [qereḇ](../../strongs/h/h7130.md), how they [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md), after the [mishpat](../../strongs/h/h4941.md) of the [Ṣîḏōnî](../../strongs/h/h6722.md), [šāqaṭ](../../strongs/h/h8252.md) and [batach](../../strongs/h/h982.md); and there was no [yarash](../../strongs/h/h3423.md) [ʿeṣer](../../strongs/h/h6114.md) in the ['erets](../../strongs/h/h776.md), that might put them to [kālam](../../strongs/h/h3637.md) in any [dabar](../../strongs/h/h1697.md); and they were [rachowq](../../strongs/h/h7350.md) from the [Ṣîḏōnî](../../strongs/h/h6722.md), and had no [dabar](../../strongs/h/h1697.md) with any ['āḏām](../../strongs/h/h120.md).

<a name="judges_18_8"></a>Judges 18:8

And they [bow'](../../strongs/h/h935.md) unto their ['ach](../../strongs/h/h251.md) to [ṢārʿÂ](../../strongs/h/h6881.md) and ['Eštā'Ōl](../../strongs/h/h847.md): and their ['ach](../../strongs/h/h251.md) ['āmar](../../strongs/h/h559.md) unto them, What say ye?

<a name="judges_18_9"></a>Judges 18:9

And they ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), that we may [ʿālâ](../../strongs/h/h5927.md) against them: for we have [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md), and, behold, it is [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md): and are ye [ḥāšâ](../../strongs/h/h2814.md)? be not [ʿāṣal](../../strongs/h/h6101.md) to [yālaḵ](../../strongs/h/h3212.md), and to [bow'](../../strongs/h/h935.md) to [yarash](../../strongs/h/h3423.md) the ['erets](../../strongs/h/h776.md).

<a name="judges_18_10"></a>Judges 18:10

When ye [bow'](../../strongs/h/h935.md), ye shall [bow'](../../strongs/h/h935.md) unto an ['am](../../strongs/h/h5971.md) [batach](../../strongs/h/h982.md), and to a [rāḥāḇ](../../strongs/h/h7342.md) ['erets](../../strongs/h/h776.md): for ['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) it into your [yad](../../strongs/h/h3027.md); a [maqowm](../../strongs/h/h4725.md) where there is no [maḥsôr](../../strongs/h/h4270.md) of any [dabar](../../strongs/h/h1697.md) that is in the ['erets](../../strongs/h/h776.md).

<a name="judges_18_11"></a>Judges 18:11

And there [nāsaʿ](../../strongs/h/h5265.md) from thence of the [mišpāḥâ](../../strongs/h/h4940.md) of the [dānî](../../strongs/h/h1839.md), out of [ṢārʿÂ](../../strongs/h/h6881.md) and out of ['Eštā'Ōl](../../strongs/h/h847.md), [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) [ḥāḡar](../../strongs/h/h2296.md) with [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="judges_18_12"></a>Judges 18:12

And they [ʿālâ](../../strongs/h/h5927.md), and [ḥānâ](../../strongs/h/h2583.md) in [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), in [Yehuwdah](../../strongs/h/h3063.md): wherefore they [qara'](../../strongs/h/h7121.md) that [maqowm](../../strongs/h/h4725.md) [Maḥănê-Ḏān](../../strongs/h/h4265.md) unto this [yowm](../../strongs/h/h3117.md): behold, it is ['aḥar](../../strongs/h/h310.md) [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md).

<a name="judges_18_13"></a>Judges 18:13

And they ['abar](../../strongs/h/h5674.md) thence unto [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and [bow'](../../strongs/h/h935.md) unto the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md).

<a name="judges_18_14"></a>Judges 18:14

Then ['anah](../../strongs/h/h6030.md) the [ḥāmēš](../../strongs/h/h2568.md) ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) to spy [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md) of [layiš](../../strongs/h/h3919.md), and ['āmar](../../strongs/h/h559.md) unto their ['ach](../../strongs/h/h251.md), Do ye [yada'](../../strongs/h/h3045.md) that there [yēš](../../strongs/h/h3426.md) in these [bayith](../../strongs/h/h1004.md) an ['ēp̄ôḏ](../../strongs/h/h646.md), and [tᵊrāp̄îm](../../strongs/h/h8655.md), and a [pecel](../../strongs/h/h6459.md), and a [massēḵâ](../../strongs/h/h4541.md)? now therefore [yada'](../../strongs/h/h3045.md) what ye have to ['asah](../../strongs/h/h6213.md).

<a name="judges_18_15"></a>Judges 18:15

And they [cuwr](../../strongs/h/h5493.md) thitherward, and [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of the [naʿar](../../strongs/h/h5288.md) the [Lᵊvî](../../strongs/h/h3881.md), even unto the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md), and [sha'al](../../strongs/h/h7592.md) [shalowm](../../strongs/h/h7965.md) him.

<a name="judges_18_16"></a>Judges 18:16

And the [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) [ḥāḡar](../../strongs/h/h2296.md) with their [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md), which were of the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md), [nāṣaḇ](../../strongs/h/h5324.md) by the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md).

<a name="judges_18_17"></a>Judges 18:17

And the [ḥāmēš](../../strongs/h/h2568.md) ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) to spy [ragal](../../strongs/h/h7270.md) the ['erets](../../strongs/h/h776.md) [ʿālâ](../../strongs/h/h5927.md), and [bow'](../../strongs/h/h935.md) thither, and [laqach](../../strongs/h/h3947.md) the [pecel](../../strongs/h/h6459.md), and the ['ēp̄ôḏ](../../strongs/h/h646.md), and the [tᵊrāp̄îm](../../strongs/h/h8655.md), and the [massēḵâ](../../strongs/h/h4541.md): and the [kōhēn](../../strongs/h/h3548.md) [nāṣaḇ](../../strongs/h/h5324.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) with the [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) that were [ḥāḡar](../../strongs/h/h2296.md) with [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="judges_18_18"></a>Judges 18:18

And these [bow'](../../strongs/h/h935.md) into [Mîḵâ](../../strongs/h/h4318.md) [bayith](../../strongs/h/h1004.md), and [laqach](../../strongs/h/h3947.md) the [pecel](../../strongs/h/h6459.md), the ['ēp̄ôḏ](../../strongs/h/h646.md), and the [tᵊrāp̄îm](../../strongs/h/h8655.md), and the [massēḵâ](../../strongs/h/h4541.md). Then ['āmar](../../strongs/h/h559.md) the [kōhēn](../../strongs/h/h3548.md) unto them, What ['asah](../../strongs/h/h6213.md) ye?

<a name="judges_18_19"></a>Judges 18:19

And they ['āmar](../../strongs/h/h559.md) unto him, Hold thy [ḥāraš](../../strongs/h/h2790.md), [śûm](../../strongs/h/h7760.md) thine [yad](../../strongs/h/h3027.md) upon thy [peh](../../strongs/h/h6310.md), and [yālaḵ](../../strongs/h/h3212.md) with us, and be to [hayah](../../strongs/h/h1961.md) an ['ab](../../strongs/h/h1.md) and a [kōhēn](../../strongs/h/h3548.md): is it [towb](../../strongs/h/h2896.md) for thee to be a [kōhēn](../../strongs/h/h3548.md) unto the [bayith](../../strongs/h/h1004.md) of ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md), or that thou be a [kōhēn](../../strongs/h/h3548.md) unto a [shebet](../../strongs/h/h7626.md) and a [mišpāḥâ](../../strongs/h/h4940.md) in [Yisra'el](../../strongs/h/h3478.md)?

<a name="judges_18_20"></a>Judges 18:20

And the [kōhēn](../../strongs/h/h3548.md) [leb](../../strongs/h/h3820.md) was [yatab](../../strongs/h/h3190.md), and he [laqach](../../strongs/h/h3947.md) the ['ēp̄ôḏ](../../strongs/h/h646.md), and the [tᵊrāp̄îm](../../strongs/h/h8655.md), and the [pecel](../../strongs/h/h6459.md), and [bow'](../../strongs/h/h935.md) the [qereḇ](../../strongs/h/h7130.md) of the ['am](../../strongs/h/h5971.md).

<a name="judges_18_21"></a>Judges 18:21

So they [panah](../../strongs/h/h6437.md) and [yālaḵ](../../strongs/h/h3212.md), and [śûm](../../strongs/h/h7760.md) the [ṭap̄](../../strongs/h/h2945.md) and the [miqnê](../../strongs/h/h4735.md) and the [kᵊḇûdâ](../../strongs/h/h3520.md) [paniym](../../strongs/h/h6440.md) them.

<a name="judges_18_22"></a>Judges 18:22

And when they were a [rachaq](../../strongs/h/h7368.md) from the [bayith](../../strongs/h/h1004.md) of [Mîḵâ](../../strongs/h/h4318.md), the ['enowsh](../../strongs/h/h582.md) that were in the [bayith](../../strongs/h/h1004.md) near to [Mîḵâ](../../strongs/h/h4318.md) [bayith](../../strongs/h/h1004.md) were [zāʿaq](../../strongs/h/h2199.md), and [dāḇaq](../../strongs/h/h1692.md) the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md).

<a name="judges_18_23"></a>Judges 18:23

And they [qara'](../../strongs/h/h7121.md) unto the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md). And they [cabab](../../strongs/h/h5437.md) their [paniym](../../strongs/h/h6440.md), and ['āmar](../../strongs/h/h559.md) unto [Mîḵâ](../../strongs/h/h4318.md), What aileth thee, that thou comest with such a [zāʿaq](../../strongs/h/h2199.md)?

<a name="judges_18_24"></a>Judges 18:24

And he ['āmar](../../strongs/h/h559.md), Ye have [laqach](../../strongs/h/h3947.md) my ['Elohiym](../../strongs/h/h430.md) which I ['asah](../../strongs/h/h6213.md), and the [kōhēn](../../strongs/h/h3548.md), and ye are [yālaḵ](../../strongs/h/h3212.md): and what have I more? and what is this that ye ['āmar](../../strongs/h/h559.md) unto me, What aileth thee?

<a name="judges_18_25"></a>Judges 18:25

And the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) ['āmar](../../strongs/h/h559.md) unto him, Let not thy [qowl](../../strongs/h/h6963.md) be [shama'](../../strongs/h/h8085.md) among us, lest [mar](../../strongs/h/h4751.md) [nephesh](../../strongs/h/h5315.md) ['enowsh](../../strongs/h/h582.md) [pāḡaʿ](../../strongs/h/h6293.md) upon thee, and thou ['āsap̄](../../strongs/h/h622.md) thy [nephesh](../../strongs/h/h5315.md), with the [nephesh](../../strongs/h/h5315.md) of thy [bayith](../../strongs/h/h1004.md).

<a name="judges_18_26"></a>Judges 18:26

And the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [yālaḵ](../../strongs/h/h3212.md) their [derek](../../strongs/h/h1870.md): and when [Mîḵâ](../../strongs/h/h4318.md) [ra'ah](../../strongs/h/h7200.md) that they were too [ḥāzāq](../../strongs/h/h2389.md) for him, he [panah](../../strongs/h/h6437.md) and went [shuwb](../../strongs/h/h7725.md) unto his [bayith](../../strongs/h/h1004.md).

<a name="judges_18_27"></a>Judges 18:27

And they [laqach](../../strongs/h/h3947.md) the things which [Mîḵâ](../../strongs/h/h4318.md) had ['asah](../../strongs/h/h6213.md), and the [kōhēn](../../strongs/h/h3548.md) which he had, and [bow'](../../strongs/h/h935.md) unto [layiš](../../strongs/h/h3919.md), unto an ['am](../../strongs/h/h5971.md) that were at [šāqaṭ](../../strongs/h/h8252.md) and [batach](../../strongs/h/h982.md): and they [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md), and [śārap̄](../../strongs/h/h8313.md) the [ʿîr](../../strongs/h/h5892.md) with ['esh](../../strongs/h/h784.md).

<a name="judges_18_28"></a>Judges 18:28

And there was no [natsal](../../strongs/h/h5337.md), because it was [rachowq](../../strongs/h/h7350.md) from [Ṣîḏôn](../../strongs/h/h6721.md), and they had no [dabar](../../strongs/h/h1697.md) with any ['āḏām](../../strongs/h/h120.md); and it was in the [ʿēmeq](../../strongs/h/h6010.md) that lieth by [Bêṯ-Rᵊḥôḇ](../../strongs/h/h1050.md). And they [bānâ](../../strongs/h/h1129.md) a [ʿîr](../../strongs/h/h5892.md), and [yashab](../../strongs/h/h3427.md) therein.

<a name="judges_18_29"></a>Judges 18:29

And they [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) [Dān](../../strongs/h/h1835.md), after the [shem](../../strongs/h/h8034.md) of [Dān](../../strongs/h/h1835.md) their ['ab](../../strongs/h/h1.md), who was [yalad](../../strongs/h/h3205.md) unto [Yisra'el](../../strongs/h/h3478.md): ['ûlām](../../strongs/h/h199.md) the [shem](../../strongs/h/h8034.md) of the [ʿîr](../../strongs/h/h5892.md) was [layiš](../../strongs/h/h3919.md) at the [ri'šôn](../../strongs/h/h7223.md).

<a name="judges_18_30"></a>Judges 18:30

And the [ben](../../strongs/h/h1121.md) of [Dān](../../strongs/h/h1835.md) [quwm](../../strongs/h/h6965.md) the [pecel](../../strongs/h/h6459.md): and [Yᵊhônāṯān](../../strongs/h/h3083.md), the [ben](../../strongs/h/h1121.md) of [Gēršōm](../../strongs/h/h1647.md), the [ben](../../strongs/h/h1121.md) of [Mᵊnaššê](../../strongs/h/h4519.md), he and his [ben](../../strongs/h/h1121.md) were [kōhēn](../../strongs/h/h3548.md) to the [shebet](../../strongs/h/h7626.md) of [dānî](../../strongs/h/h1839.md) until the [yowm](../../strongs/h/h3117.md) of the [gālâ](../../strongs/h/h1540.md) of the ['erets](../../strongs/h/h776.md).

<a name="judges_18_31"></a>Judges 18:31

And they set them [śûm](../../strongs/h/h7760.md) [Mîḵâ](../../strongs/h/h4318.md) [pecel](../../strongs/h/h6459.md), which he ['asah](../../strongs/h/h6213.md), all the [yowm](../../strongs/h/h3117.md) that the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) was in [Šîlô](../../strongs/h/h7887.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 17](judges_17.md) - [Judges 19](judges_19.md)