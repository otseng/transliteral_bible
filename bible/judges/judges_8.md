x# [Judges 8](https://www.blueletterbible.org/kjv/judges/8)

<a name="judges_8_1"></a>Judges 8:1

And the ['iysh](../../strongs/h/h376.md) of ['Ep̄rayim](../../strongs/h/h669.md) ['āmar](../../strongs/h/h559.md) unto him, Why hast [mah](../../strongs/h/h4100.md) ['asah](../../strongs/h/h6213.md) us [dabar](../../strongs/h/h1697.md), that thou [qara'](../../strongs/h/h7121.md) us not, when thou [halak](../../strongs/h/h1980.md) to [lāḥam](../../strongs/h/h3898.md) with the [Miḏyān](../../strongs/h/h4080.md)? And they did [riyb](../../strongs/h/h7378.md) with him [ḥāzqâ](../../strongs/h/h2394.md).

<a name="judges_8_2"></a>Judges 8:2

And he ['āmar](../../strongs/h/h559.md) unto them, What have I ['asah](../../strongs/h/h6213.md) now in comparison of you? Is not the [ʿōlēlôṯ](../../strongs/h/h5955.md) of ['Ep̄rayim](../../strongs/h/h669.md) [towb](../../strongs/h/h2896.md) than the [bāṣîr](../../strongs/h/h1210.md) of ['ĂḇîʿEzer](../../strongs/h/h44.md)?

<a name="judges_8_3"></a>Judges 8:3

['Elohiym](../../strongs/h/h430.md) hath [nathan](../../strongs/h/h5414.md) into your [yad](../../strongs/h/h3027.md) the [śar](../../strongs/h/h8269.md) of [Miḏyān](../../strongs/h/h4080.md), [ʿōrēḇ](../../strongs/h/h6159.md) and [Zᵊ'Ēḇ](../../strongs/h/h2062.md): and what was I [yakol](../../strongs/h/h3201.md) to ['asah](../../strongs/h/h6213.md) in comparison of you? Then their [ruwach](../../strongs/h/h7307.md) was [rāp̄â](../../strongs/h/h7503.md) toward him, when he had [dabar](../../strongs/h/h1697.md) [dabar](../../strongs/h/h1696.md).

<a name="judges_8_4"></a>Judges 8:4

And [GiḏʿÔn](../../strongs/h/h1439.md) [bow'](../../strongs/h/h935.md) to [Yardēn](../../strongs/h/h3383.md), and ['abar](../../strongs/h/h5674.md), he, and the [šālôš](../../strongs/h/h7969.md) [mē'â](../../strongs/h/h3967.md) ['iysh](../../strongs/h/h376.md) that were with him, [ʿāyēp̄](../../strongs/h/h5889.md), yet [radaph](../../strongs/h/h7291.md) them.

<a name="judges_8_5"></a>Judges 8:5

And he ['āmar](../../strongs/h/h559.md) unto the ['enowsh](../../strongs/h/h582.md) of [Sukôṯ](../../strongs/h/h5523.md), [nathan](../../strongs/h/h5414.md), I pray you, [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md) unto the ['am](../../strongs/h/h5971.md) that [regel](../../strongs/h/h7272.md) me; for they be [ʿāyēp̄](../../strongs/h/h5889.md), and I am [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md), [melek](../../strongs/h/h4428.md) of [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_8_6"></a>Judges 8:6

And the [śar](../../strongs/h/h8269.md) of [Sukôṯ](../../strongs/h/h5523.md) ['āmar](../../strongs/h/h559.md), Are the [kaph](../../strongs/h/h3709.md) of [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) now in thine [yad](../../strongs/h/h3027.md), that we should [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) unto thine [tsaba'](../../strongs/h/h6635.md)?

<a name="judges_8_7"></a>Judges 8:7

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md), Therefore when [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) into mine [yad](../../strongs/h/h3027.md), then I will [dûš](../../strongs/h/h1758.md) your [basar](../../strongs/h/h1320.md) with the [qowts](../../strongs/h/h6975.md) of the [midbar](../../strongs/h/h4057.md) and with [barqānîm](../../strongs/h/h1303.md).

<a name="judges_8_8"></a>Judges 8:8

And he [ʿālâ](../../strongs/h/h5927.md) thence to [Pᵊnû'ēl](../../strongs/h/h6439.md), and [dabar](../../strongs/h/h1696.md) unto them [zō'ṯ](../../strongs/h/h2063.md): and the ['enowsh](../../strongs/h/h582.md) of [Pᵊnû'ēl](../../strongs/h/h6439.md) ['anah](../../strongs/h/h6030.md) him as the ['enowsh](../../strongs/h/h582.md) of [Sukôṯ](../../strongs/h/h5523.md) had ['anah](../../strongs/h/h6030.md) him.

<a name="judges_8_9"></a>Judges 8:9

And he ['āmar](../../strongs/h/h559.md) also unto the ['enowsh](../../strongs/h/h582.md) of [Pᵊnû'ēl](../../strongs/h/h6439.md), ['āmar](../../strongs/h/h559.md), When I [shuwb](../../strongs/h/h7725.md) in [shalowm](../../strongs/h/h7965.md), I will [nāṯaṣ](../../strongs/h/h5422.md) this [miḡdāl](../../strongs/h/h4026.md).

<a name="judges_8_10"></a>Judges 8:10

Now [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) were in [Qarqōr](../../strongs/h/h7174.md), and their [maḥănê](../../strongs/h/h4264.md) with them, about [ḥāmēš](../../strongs/h/h2568.md) [ʿeśer](../../strongs/h/h6240.md) ['elep̄](../../strongs/h/h505.md) men, all that were [yāṯar](../../strongs/h/h3498.md) of all the [maḥănê](../../strongs/h/h4264.md) of the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md): for there [naphal](../../strongs/h/h5307.md) a [mē'â](../../strongs/h/h3967.md) and [ʿeśrîm](../../strongs/h/h6242.md) ['elep̄](../../strongs/h/h505.md) ['iysh](../../strongs/h/h376.md) that [šālap̄](../../strongs/h/h8025.md) [chereb](../../strongs/h/h2719.md).

<a name="judges_8_11"></a>Judges 8:11

And [GiḏʿÔn](../../strongs/h/h1439.md) [ʿālâ](../../strongs/h/h5927.md) by the [derek](../../strongs/h/h1870.md) of them that [shakan](../../strongs/h/h7931.md) in ['ohel](../../strongs/h/h168.md) on the [qeḏem](../../strongs/h/h6924.md) of [Nōḇaḥ](../../strongs/h/h5025.md) and [Yāḡbahâ](../../strongs/h/h3011.md), and [nakah](../../strongs/h/h5221.md) the [maḥănê](../../strongs/h/h4264.md): for the [maḥănê](../../strongs/h/h4264.md) was [betach](../../strongs/h/h983.md).

<a name="judges_8_12"></a>Judges 8:12

And when [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) [nûs](../../strongs/h/h5127.md), he [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them, and [lāḵaḏ](../../strongs/h/h3920.md) the [šᵊnayim](../../strongs/h/h8147.md) [melek](../../strongs/h/h4428.md) of [Miḏyān](../../strongs/h/h4080.md), [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md), and [ḥārēḏ](../../strongs/h/h2729.md) all the [maḥănê](../../strongs/h/h4264.md).

<a name="judges_8_13"></a>Judges 8:13

And [GiḏʿÔn](../../strongs/h/h1439.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [shuwb](../../strongs/h/h7725.md) from [milḥāmâ](../../strongs/h/h4421.md) [maʿălê](../../strongs/h/h4608.md) the [ḥeres](../../strongs/h/h2775.md) was up,

<a name="judges_8_14"></a>Judges 8:14

And [lāḵaḏ](../../strongs/h/h3920.md) a [naʿar](../../strongs/h/h5288.md) of the ['enowsh](../../strongs/h/h582.md) of [Sukôṯ](../../strongs/h/h5523.md), and [sha'al](../../strongs/h/h7592.md) of him: and he [kāṯaḇ](../../strongs/h/h3789.md) unto him the [śar](../../strongs/h/h8269.md) of [Sukôṯ](../../strongs/h/h5523.md), and the [zāqēn](../../strongs/h/h2205.md) thereof, even [šiḇʿîm](../../strongs/h/h7657.md) [šeḇaʿ](../../strongs/h/h7651.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_8_15"></a>Judges 8:15

And he [bow'](../../strongs/h/h935.md) unto the ['enowsh](../../strongs/h/h582.md) of [Sukôṯ](../../strongs/h/h5523.md), and ['āmar](../../strongs/h/h559.md), Behold [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md), with whom ye did [ḥārap̄](../../strongs/h/h2778.md) me, ['āmar](../../strongs/h/h559.md), Are the [kaph](../../strongs/h/h3709.md) of [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) now in thine [yad](../../strongs/h/h3027.md), that we should [nathan](../../strongs/h/h5414.md) [lechem](../../strongs/h/h3899.md) unto thy ['enowsh](../../strongs/h/h582.md) that are [yāʿēp̄](../../strongs/h/h3287.md)?

<a name="judges_8_16"></a>Judges 8:16

And he [laqach](../../strongs/h/h3947.md) the [zāqēn](../../strongs/h/h2205.md) of the [ʿîr](../../strongs/h/h5892.md), and [qowts](../../strongs/h/h6975.md) of the [midbar](../../strongs/h/h4057.md) and [barqānîm](../../strongs/h/h1303.md), and with them he [yada'](../../strongs/h/h3045.md) the ['enowsh](../../strongs/h/h582.md) of [Sukôṯ](../../strongs/h/h5523.md).

<a name="judges_8_17"></a>Judges 8:17

And he [nāṯaṣ](../../strongs/h/h5422.md) the [miḡdāl](../../strongs/h/h4026.md) of [Pᵊnû'ēl](../../strongs/h/h6439.md), and [harag](../../strongs/h/h2026.md) the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="judges_8_18"></a>Judges 8:18

Then ['āmar](../../strongs/h/h559.md) he unto [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md), ['êp̄ô](../../strongs/h/h375.md) manner of ['enowsh](../../strongs/h/h582.md) were they whom ye [harag](../../strongs/h/h2026.md) at [Tāḇôr](../../strongs/h/h8396.md)? And they ['āmar](../../strongs/h/h559.md), As [kᵊmô](../../strongs/h/h3644.md) art, so were they; each ['echad](../../strongs/h/h259.md) [tō'ar](../../strongs/h/h8389.md) the [ben](../../strongs/h/h1121.md) of a [melek](../../strongs/h/h4428.md).

<a name="judges_8_19"></a>Judges 8:19

And he ['āmar](../../strongs/h/h559.md), They were my ['ach](../../strongs/h/h251.md), even the [ben](../../strongs/h/h1121.md) of my ['em](../../strongs/h/h517.md): as [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), [lû'](../../strongs/h/h3863.md) ye had saved them [ḥāyâ](../../strongs/h/h2421.md), I would not [harag](../../strongs/h/h2026.md) you.

<a name="judges_8_20"></a>Judges 8:20

And he ['āmar](../../strongs/h/h559.md) unto [Yeṯer](../../strongs/h/h3500.md) his [bᵊḵôr](../../strongs/h/h1060.md), [quwm](../../strongs/h/h6965.md), and [harag](../../strongs/h/h2026.md) them. But the [naʿar](../../strongs/h/h5288.md) [šālap̄](../../strongs/h/h8025.md) not his [chereb](../../strongs/h/h2719.md): for he [yare'](../../strongs/h/h3372.md), because he was yet a [naʿar](../../strongs/h/h5288.md).

<a name="judges_8_21"></a>Judges 8:21

Then [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md) ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md) thou, and [pāḡaʿ](../../strongs/h/h6293.md) upon us: for as the ['iysh](../../strongs/h/h376.md) is, so is his [gᵊḇûrâ](../../strongs/h/h1369.md). And [GiḏʿÔn](../../strongs/h/h1439.md) [quwm](../../strongs/h/h6965.md), and [harag](../../strongs/h/h2026.md) [Zeḇaḥ](../../strongs/h/h2078.md) and [Ṣalmunnāʿ](../../strongs/h/h6759.md), and took [laqach](../../strongs/h/h3947.md) the [śahărōnîm](../../strongs/h/h7720.md) that were on their [gāmāl](../../strongs/h/h1581.md) [ṣaûā'r](../../strongs/h/h6677.md).

<a name="judges_8_22"></a>Judges 8:22

Then the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), [mashal](../../strongs/h/h4910.md) thou over us, both thou, and thy [ben](../../strongs/h/h1121.md), and thy [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) also: for thou hast [yasha'](../../strongs/h/h3467.md) us from the [yad](../../strongs/h/h3027.md) of [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_8_23"></a>Judges 8:23

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md) unto them, I will not [mashal](../../strongs/h/h4910.md) over you, neither shall my [ben](../../strongs/h/h1121.md) [mashal](../../strongs/h/h4910.md) over you: [Yĕhovah](../../strongs/h/h3068.md) shall [mashal](../../strongs/h/h4910.md) over you.

<a name="judges_8_24"></a>Judges 8:24

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md) unto them, I would [sha'al](../../strongs/h/h7592.md) a [šᵊ'ēlâ](../../strongs/h/h7596.md) of you, that ye would [nathan](../../strongs/h/h5414.md) me every ['iysh](../../strongs/h/h376.md) the [nezem](../../strongs/h/h5141.md) of his [šālāl](../../strongs/h/h7998.md). (For they had [zāhāḇ](../../strongs/h/h2091.md) [nezem](../../strongs/h/h5141.md), because they were [Yišmᵊʿē'lî](../../strongs/h/h3459.md).)

<a name="judges_8_25"></a>Judges 8:25

And they ['āmar](../../strongs/h/h559.md), We will [nathan](../../strongs/h/h5414.md) [nathan](../../strongs/h/h5414.md) them. And they [pāraś](../../strongs/h/h6566.md) a [śimlâ](../../strongs/h/h8071.md), and did [shalak](../../strongs/h/h7993.md) therein every ['iysh](../../strongs/h/h376.md) the [nezem](../../strongs/h/h5141.md) of his [šālāl](../../strongs/h/h7998.md).

<a name="judges_8_26"></a>Judges 8:26

And the [mišqāl](../../strongs/h/h4948.md) of the [zāhāḇ](../../strongs/h/h2091.md) [nezem](../../strongs/h/h5141.md) that he [sha'al](../../strongs/h/h7592.md) was an ['elep̄](../../strongs/h/h505.md) and [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) of [zāhāḇ](../../strongs/h/h2091.md); beside [śahărōnîm](../../strongs/h/h7720.md), and [nᵊṭîp̄â](../../strongs/h/h5188.md), and ['argāmān](../../strongs/h/h713.md) [beḡeḏ](../../strongs/h/h899.md) that was on the [melek](../../strongs/h/h4428.md) of [Miḏyān](../../strongs/h/h4080.md), and beside the [ʿĂnāq](../../strongs/h/h6060.md) that were about their [gāmāl](../../strongs/h/h1581.md) [ṣaûā'r](../../strongs/h/h6677.md).

<a name="judges_8_27"></a>Judges 8:27

And [GiḏʿÔn](../../strongs/h/h1439.md) ['asah](../../strongs/h/h6213.md) an ['ēp̄ôḏ](../../strongs/h/h646.md) thereof, and [yāṣaḡ](../../strongs/h/h3322.md) it in his [ʿîr](../../strongs/h/h5892.md), even in [ʿĀp̄Râ](../../strongs/h/h6084.md): and all [Yisra'el](../../strongs/h/h3478.md) went thither a [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) it: which thing became a [mowqesh](../../strongs/h/h4170.md) unto [GiḏʿÔn](../../strongs/h/h1439.md), and to his [bayith](../../strongs/h/h1004.md).

<a name="judges_8_28"></a>Judges 8:28

Thus was [Miḏyān](../../strongs/h/h4080.md) [kānaʿ](../../strongs/h/h3665.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), so that they [nasa'](../../strongs/h/h5375.md) their [ro'sh](../../strongs/h/h7218.md) no [yāsap̄](../../strongs/h/h3254.md). And the ['erets](../../strongs/h/h776.md) was in [šāqaṭ](../../strongs/h/h8252.md) ['arbāʿîm](../../strongs/h/h705.md) [šānâ](../../strongs/h/h8141.md) in the [yowm](../../strongs/h/h3117.md) of [GiḏʿÔn](../../strongs/h/h1439.md).

<a name="judges_8_29"></a>Judges 8:29

And [YᵊrubaʿAl](../../strongs/h/h3378.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [yālaḵ](../../strongs/h/h3212.md) and [yashab](../../strongs/h/h3427.md) in his own [bayith](../../strongs/h/h1004.md).

<a name="judges_8_30"></a>Judges 8:30

And [GiḏʿÔn](../../strongs/h/h1439.md) had [šiḇʿîm](../../strongs/h/h7657.md) [ben](../../strongs/h/h1121.md) of his [yārēḵ](../../strongs/h/h3409.md) [yāṣā'](../../strongs/h/h3318.md): for he had [rab](../../strongs/h/h7227.md) ['ishshah](../../strongs/h/h802.md).

<a name="judges_8_31"></a>Judges 8:31

And his [pîleḡeš](../../strongs/h/h6370.md) that was in [Šᵊḵem](../../strongs/h/h7927.md), she also [yalad](../../strongs/h/h3205.md) him a [ben](../../strongs/h/h1121.md), whose [shem](../../strongs/h/h8034.md) he [śûm](../../strongs/h/h7760.md) ['Ăḇîmeleḵ](../../strongs/h/h40.md).

<a name="judges_8_32"></a>Judges 8:32

And [GiḏʿÔn](../../strongs/h/h1439.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [muwth](../../strongs/h/h4191.md) in a [towb](../../strongs/h/h2896.md) old [śêḇâ](../../strongs/h/h7872.md), and was [qāḇar](../../strongs/h/h6912.md) in the [qeber](../../strongs/h/h6913.md) of [Yô'Āš](../../strongs/h/h3101.md) his ['ab](../../strongs/h/h1.md), in [ʿĀp̄Râ](../../strongs/h/h6084.md) of the ['Ăḇî HāʿEzrî](../../strongs/h/h33.md).

<a name="judges_8_33"></a>Judges 8:33

And it came to pass, as soon as [GiḏʿÔn](../../strongs/h/h1439.md) was [muwth](../../strongs/h/h4191.md), that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md), and went a [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) [BaʿAl](../../strongs/h/h1168.md), and [śûm](../../strongs/h/h7760.md) [BaʿAl Bᵊrîṯ](../../strongs/h/h1170.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="judges_8_34"></a>Judges 8:34

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zakar](../../strongs/h/h2142.md) not [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), who had [natsal](../../strongs/h/h5337.md) them out of the [yad](../../strongs/h/h3027.md) of all their ['oyeb](../../strongs/h/h341.md) on every [cabiyb](../../strongs/h/h5439.md):

<a name="judges_8_35"></a>Judges 8:35

Neither ['asah](../../strongs/h/h6213.md) they [checed](../../strongs/h/h2617.md) to the [bayith](../../strongs/h/h1004.md) of [YᵊrubaʿAl](../../strongs/h/h3378.md), namely, [GiḏʿÔn](../../strongs/h/h1439.md), according to all the [towb](../../strongs/h/h2896.md) which he had ['asah](../../strongs/h/h6213.md) unto [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 7](judges_7.md) - [Judges 9](judges_9.md)