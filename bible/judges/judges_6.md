# [Judges 6](https://www.blueletterbible.org/kjv/judges/6)

<a name="judges_6_1"></a>Judges 6:1

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md): and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Miḏyān](../../strongs/h/h4080.md) [šeḇaʿ](../../strongs/h/h7651.md) [šānâ](../../strongs/h/h8141.md).

<a name="judges_6_2"></a>Judges 6:2

And the [yad](../../strongs/h/h3027.md) of [Miḏyān](../../strongs/h/h4080.md) ['azaz](../../strongs/h/h5810.md) against [Yisra'el](../../strongs/h/h3478.md): and [paniym](../../strongs/h/h6440.md) of the [Miḏyān](../../strongs/h/h4080.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) them the [minhārâ](../../strongs/h/h4492.md) which are in the [har](../../strongs/h/h2022.md), and [mᵊʿārâ](../../strongs/h/h4631.md), and [mᵊṣāḏ](../../strongs/h/h4679.md).

<a name="judges_6_3"></a>Judges 6:3

And so it was, when [Yisra'el](../../strongs/h/h3478.md) had [zāraʿ](../../strongs/h/h2232.md), that the [Miḏyān](../../strongs/h/h4080.md) [ʿālâ](../../strongs/h/h5927.md), and the [ʿĂmālēq](../../strongs/h/h6002.md), and the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md), even they [ʿālâ](../../strongs/h/h5927.md) against them;

<a name="judges_6_4"></a>Judges 6:4

And they [ḥānâ](../../strongs/h/h2583.md) against them, and [shachath](../../strongs/h/h7843.md) the [yᵊḇûl](../../strongs/h/h2981.md) of the ['erets](../../strongs/h/h776.md), till thou [bow'](../../strongs/h/h935.md) unto [ʿAzzâ](../../strongs/h/h5804.md), and [šā'ar](../../strongs/h/h7604.md) no [miḥyâ](../../strongs/h/h4241.md) for [Yisra'el](../../strongs/h/h3478.md), neither [śê](../../strongs/h/h7716.md), nor [showr](../../strongs/h/h7794.md), nor [chamowr](../../strongs/h/h2543.md).

<a name="judges_6_5"></a>Judges 6:5

For they [ʿālâ](../../strongs/h/h5927.md) with their [miqnê](../../strongs/h/h4735.md) and their ['ohel](../../strongs/h/h168.md), and they [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md) [day](../../strongs/h/h1767.md) ['arbê](../../strongs/h/h697.md) for [rōḇ](../../strongs/h/h7230.md); for both they and their [gāmāl](../../strongs/h/h1581.md) were without [mispār](../../strongs/h/h4557.md): and they [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) to [shachath](../../strongs/h/h7843.md) it.

<a name="judges_6_6"></a>Judges 6:6

And [Yisra'el](../../strongs/h/h3478.md) was [me'od](../../strongs/h/h3966.md) [dālal](../../strongs/h/h1809.md) [paniym](../../strongs/h/h6440.md) of the [Miḏyān](../../strongs/h/h4080.md); and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="judges_6_7"></a>Judges 6:7

And it came to pass, when the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['ôḏôṯ](../../strongs/h/h182.md) of the [Miḏyān](../../strongs/h/h4080.md),

<a name="judges_6_8"></a>Judges 6:8

That [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) an ['iysh](../../strongs/h/h376.md) [nāḇî'](../../strongs/h/h5030.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), I brought you [ʿālâ](../../strongs/h/h5927.md) from [Mitsrayim](../../strongs/h/h4714.md), and brought you [yāṣā'](../../strongs/h/h3318.md) out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md);

<a name="judges_6_9"></a>Judges 6:9

And I [natsal](../../strongs/h/h5337.md) you out of the [yad](../../strongs/h/h3027.md) of the [Mitsrayim](../../strongs/h/h4714.md), and out of the [yad](../../strongs/h/h3027.md) of all that [lāḥaṣ](../../strongs/h/h3905.md) you, and [gāraš](../../strongs/h/h1644.md) them from [paniym](../../strongs/h/h6440.md) you, and [nathan](../../strongs/h/h5414.md) you their ['erets](../../strongs/h/h776.md);

<a name="judges_6_10"></a>Judges 6:10

And I ['āmar](../../strongs/h/h559.md) unto you, I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); [yare'](../../strongs/h/h3372.md) not the ['Elohiym](../../strongs/h/h430.md) of the ['Ĕmōrî](../../strongs/h/h567.md), in whose ['erets](../../strongs/h/h776.md) ye [yashab](../../strongs/h/h3427.md): but ye have not [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md).

<a name="judges_6_11"></a>Judges 6:11

And there [bow'](../../strongs/h/h935.md) a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yashab](../../strongs/h/h3427.md) under an ['ēlâ](../../strongs/h/h424.md) which was in [ʿĀp̄Râ](../../strongs/h/h6084.md), that pertained unto [Yô'Āš](../../strongs/h/h3101.md) the ['Ăḇî HāʿEzrî](../../strongs/h/h33.md): and his [ben](../../strongs/h/h1121.md) [GiḏʿÔn](../../strongs/h/h1439.md) [ḥāḇaṭ](../../strongs/h/h2251.md) [ḥiṭṭâ](../../strongs/h/h2406.md) by the [gaṯ](../../strongs/h/h1660.md), to [nûs](../../strongs/h/h5127.md) it [paniym](../../strongs/h/h6440.md) the [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_6_12"></a>Judges 6:12

And the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto him, and ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) is with thee, thou [gibôr](../../strongs/h/h1368.md) man of [ḥayil](../../strongs/h/h2428.md).

<a name="judges_6_13"></a>Judges 6:13

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md) unto him, [bî](../../strongs/h/h994.md) my ['adown](../../strongs/h/h113.md), [yēš](../../strongs/h/h3426.md) [Yĕhovah](../../strongs/h/h3068.md) be with us, why then is all this [māṣā'](../../strongs/h/h4672.md) us? and where be all his [pala'](../../strongs/h/h6381.md) which our ['ab](../../strongs/h/h1.md) [sāp̄ar](../../strongs/h/h5608.md) us of, ['āmar](../../strongs/h/h559.md), Did not [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) us from [Mitsrayim](../../strongs/h/h4714.md)? but now [Yĕhovah](../../strongs/h/h3068.md) hath [nāṭaš](../../strongs/h/h5203.md) us, and [nathan](../../strongs/h/h5414.md) us into the [kaph](../../strongs/h/h3709.md) of the [Miḏyān](../../strongs/h/h4080.md).

<a name="judges_6_14"></a>Judges 6:14

And [Yĕhovah](../../strongs/h/h3068.md) [panah](../../strongs/h/h6437.md) upon him, and ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md) in this thy [koach](../../strongs/h/h3581.md), and thou shalt [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) from the [kaph](../../strongs/h/h3709.md) of the [Miḏyān](../../strongs/h/h4080.md): have not I [shalach](../../strongs/h/h7971.md) thee?

<a name="judges_6_15"></a>Judges 6:15

And he ['āmar](../../strongs/h/h559.md) unto him, [bî](../../strongs/h/h994.md) my ['adonay](../../strongs/h/h136.md), [mah](../../strongs/h/h4100.md) shall I [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md)? behold, my ['eleph](../../strongs/h/h504.md) ['elep̄](../../strongs/h/h505.md) is [dal](../../strongs/h/h1800.md) in [Mᵊnaššê](../../strongs/h/h4519.md), and I am the [ṣāʿîr](../../strongs/h/h6810.md) in my ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md).

<a name="judges_6_16"></a>Judges 6:16

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Surely I will be with thee, and thou shalt [nakah](../../strongs/h/h5221.md) the [Miḏyān](../../strongs/h/h4080.md) as ['echad](../../strongs/h/h259.md) ['iysh](../../strongs/h/h376.md).

<a name="judges_6_17"></a>Judges 6:17

And he ['āmar](../../strongs/h/h559.md) unto him, If now I have [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in thy ['ayin](../../strongs/h/h5869.md), then ['asah](../../strongs/h/h6213.md) me a ['ôṯ](../../strongs/h/h226.md) that thou [dabar](../../strongs/h/h1696.md) with me.

<a name="judges_6_18"></a>Judges 6:18

[mûš](../../strongs/h/h4185.md) not hence, I pray thee, until I [bow'](../../strongs/h/h935.md) unto thee, and [yāṣā'](../../strongs/h/h3318.md) my [minchah](../../strongs/h/h4503.md), and [yānaḥ](../../strongs/h/h3240.md) it [paniym](../../strongs/h/h6440.md) thee. And he ['āmar](../../strongs/h/h559.md), I will [yashab](../../strongs/h/h3427.md) until thou [shuwb](../../strongs/h/h7725.md).

<a name="judges_6_19"></a>Judges 6:19

And [GiḏʿÔn](../../strongs/h/h1439.md) [bow'](../../strongs/h/h935.md), and ['asah](../../strongs/h/h6213.md) a [ʿēz](../../strongs/h/h5795.md) [gᵊḏî](../../strongs/h/h1423.md), and [maṣṣâ](../../strongs/h/h4682.md) of an ['êp̄â](../../strongs/h/h374.md) of [qemaḥ](../../strongs/h/h7058.md): the [basar](../../strongs/h/h1320.md) he [śûm](../../strongs/h/h7760.md) in a [sal](../../strongs/h/h5536.md), and he [śûm](../../strongs/h/h7760.md) the [mārāq](../../strongs/h/h4839.md) in a [pārûr](../../strongs/h/h6517.md), and [yāṣā'](../../strongs/h/h3318.md) it unto him under the ['ēlâ](../../strongs/h/h424.md), and [nāḡaš](../../strongs/h/h5066.md) it.

<a name="judges_6_20"></a>Judges 6:20

And the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) unto him, [laqach](../../strongs/h/h3947.md) the [basar](../../strongs/h/h1320.md) and the [maṣṣâ](../../strongs/h/h4682.md), and [yānaḥ](../../strongs/h/h3240.md) them upon [hallāz](../../strongs/h/h1975.md) [cela'](../../strongs/h/h5553.md), and [šāp̄aḵ](../../strongs/h/h8210.md) the [mārāq](../../strongs/h/h4839.md). And he ['asah](../../strongs/h/h6213.md) so.

<a name="judges_6_21"></a>Judges 6:21

Then the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) the [qāṣê](../../strongs/h/h7097.md) of the [mašʿēnâ](../../strongs/h/h4938.md) that was in his [yad](../../strongs/h/h3027.md), and [naga'](../../strongs/h/h5060.md) the [basar](../../strongs/h/h1320.md) and the [maṣṣâ](../../strongs/h/h4682.md); and there [ʿālâ](../../strongs/h/h5927.md) ['esh](../../strongs/h/h784.md) out of the [tsuwr](../../strongs/h/h6697.md), and ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) and the [maṣṣâ](../../strongs/h/h4682.md). Then the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [halak](../../strongs/h/h1980.md) out of his ['ayin](../../strongs/h/h5869.md).

<a name="judges_6_22"></a>Judges 6:22

And when [GiḏʿÔn](../../strongs/h/h1439.md) [ra'ah](../../strongs/h/h7200.md) that he was a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md), [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md), ['ăhâ](../../strongs/h/h162.md), ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! for [kēn](../../strongs/h/h3651.md) I have [ra'ah](../../strongs/h/h7200.md) a [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) to [paniym](../../strongs/h/h6440.md).

<a name="judges_6_23"></a>Judges 6:23

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [shalowm](../../strongs/h/h7965.md) be unto thee; [yare'](../../strongs/h/h3372.md) not: thou shalt not [muwth](../../strongs/h/h4191.md).

<a name="judges_6_24"></a>Judges 6:24

Then [GiḏʿÔn](../../strongs/h/h1439.md) [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) there unto [Yĕhovah](../../strongs/h/h3068.md), and [qara'](../../strongs/h/h7121.md) it [Yᵊhōvâ Šālôm](../../strongs/h/h3073.md): unto this [yowm](../../strongs/h/h3117.md) it is yet in [ʿĀp̄Râ](../../strongs/h/h6084.md) of the ['Ăḇî HāʿEzrî](../../strongs/h/h33.md).

<a name="judges_6_25"></a>Judges 6:25

And it came to pass the same [layil](../../strongs/h/h3915.md), that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, [laqach](../../strongs/h/h3947.md) thy ['ab](../../strongs/h/h1.md) [par](../../strongs/h/h6499.md) [showr](../../strongs/h/h7794.md), even the [šēnî](../../strongs/h/h8145.md) [par](../../strongs/h/h6499.md) of [šeḇaʿ](../../strongs/h/h7651.md) [šānâ](../../strongs/h/h8141.md), and [harac](../../strongs/h/h2040.md) the [mizbeach](../../strongs/h/h4196.md) of [BaʿAl](../../strongs/h/h1168.md) that thy ['ab](../../strongs/h/h1.md) hath, and [karath](../../strongs/h/h3772.md) the ['ăšērâ](../../strongs/h/h842.md) that is by it:

<a name="judges_6_26"></a>Judges 6:26

And [bānâ](../../strongs/h/h1129.md) a [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) upon the [ro'sh](../../strongs/h/h7218.md) of this [māʿôz](../../strongs/h/h4581.md), in the ordered [maʿărāḵâ](../../strongs/h/h4634.md), and [laqach](../../strongs/h/h3947.md) the [šēnî](../../strongs/h/h8145.md) [par](../../strongs/h/h6499.md), and [ʿālâ](../../strongs/h/h5927.md) a [ʿōlâ](../../strongs/h/h5930.md) with the ['ets](../../strongs/h/h6086.md) of the ['ăšērâ](../../strongs/h/h842.md) which thou shalt [karath](../../strongs/h/h3772.md).

<a name="judges_6_27"></a>Judges 6:27

Then [GiḏʿÔn](../../strongs/h/h1439.md) [laqach](../../strongs/h/h3947.md) [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) of his ['ebed](../../strongs/h/h5650.md), and ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) had [dabar](../../strongs/h/h1696.md) unto him: and so it was, because he [yare'](../../strongs/h/h3372.md) his ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), and the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md), that he could not ['asah](../../strongs/h/h6213.md) it by [yômām](../../strongs/h/h3119.md), that he ['asah](../../strongs/h/h6213.md) it by [layil](../../strongs/h/h3915.md).

<a name="judges_6_28"></a>Judges 6:28

And when the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) a[šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), behold, the [mizbeach](../../strongs/h/h4196.md) of [BaʿAl](../../strongs/h/h1168.md) was cast [nāṯaṣ](../../strongs/h/h5422.md), and the ['ăšērâ](../../strongs/h/h842.md) was [karath](../../strongs/h/h3772.md) that was by it, and the [šēnî](../../strongs/h/h8145.md) [par](../../strongs/h/h6499.md) was [ʿālâ](../../strongs/h/h5927.md) upon the [mizbeach](../../strongs/h/h4196.md) that was [bānâ](../../strongs/h/h1129.md).

<a name="judges_6_29"></a>Judges 6:29

And they ['āmar](../../strongs/h/h559.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md), Who hath ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md)? And when they [darash](../../strongs/h/h1875.md) and [bāqaš](../../strongs/h/h1245.md), they ['āmar](../../strongs/h/h559.md), [GiḏʿÔn](../../strongs/h/h1439.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) hath ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md).

<a name="judges_6_30"></a>Judges 6:30

Then the ['enowsh](../../strongs/h/h582.md) of the [ʿîr](../../strongs/h/h5892.md) ['āmar](../../strongs/h/h559.md) unto [Yô'Āš](../../strongs/h/h3101.md), Bring [yāṣā'](../../strongs/h/h3318.md) thy [ben](../../strongs/h/h1121.md), that he may [muwth](../../strongs/h/h4191.md): because he hath [nāṯaṣ](../../strongs/h/h5422.md) the [mizbeach](../../strongs/h/h4196.md) of [BaʿAl](../../strongs/h/h1168.md), and because he hath [karath](../../strongs/h/h3772.md) the ['ăšērâ](../../strongs/h/h842.md) that was by it.

<a name="judges_6_31"></a>Judges 6:31

And [Yô'Āš](../../strongs/h/h3101.md) ['āmar](../../strongs/h/h559.md) unto all that ['amad](../../strongs/h/h5975.md) against him, Will ye [riyb](../../strongs/h/h7378.md) for [BaʿAl](../../strongs/h/h1168.md)? will ye [yasha'](../../strongs/h/h3467.md) him? he that will [riyb](../../strongs/h/h7378.md) for him, let him be put to [muwth](../../strongs/h/h4191.md) whilst it is yet [boqer](../../strongs/h/h1242.md): if he be an ['Elohiym](../../strongs/h/h430.md), let him [riyb](../../strongs/h/h7378.md) for himself, because one hath [nāṯaṣ](../../strongs/h/h5422.md) his [mizbeach](../../strongs/h/h4196.md).

<a name="judges_6_32"></a>Judges 6:32

Therefore on that [yowm](../../strongs/h/h3117.md) he [qara'](../../strongs/h/h7121.md) him [YᵊrubaʿAl](../../strongs/h/h3378.md), ['āmar](../../strongs/h/h559.md), Let [BaʿAl](../../strongs/h/h1168.md) [riyb](../../strongs/h/h7378.md) against him, because he hath [nāṯaṣ](../../strongs/h/h5422.md) his [mizbeach](../../strongs/h/h4196.md).

<a name="judges_6_33"></a>Judges 6:33

Then all the [Miḏyān](../../strongs/h/h4080.md) and the [ʿĂmālēq](../../strongs/h/h6002.md) and the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md) were ['āsap̄](../../strongs/h/h622.md) [yaḥaḏ](../../strongs/h/h3162.md), and ['abar](../../strongs/h/h5674.md), and [ḥānâ](../../strongs/h/h2583.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [YizrᵊʿE'L](../../strongs/h/h3157.md).

<a name="judges_6_34"></a>Judges 6:34

But the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) [labash](../../strongs/h/h3847.md) upon [GiḏʿÔn](../../strongs/h/h1439.md), and he [tāqaʿ](../../strongs/h/h8628.md) a [šôp̄ār](../../strongs/h/h7782.md); and ['ĂḇîʿEzer](../../strongs/h/h44.md) was [zāʿaq](../../strongs/h/h2199.md) ['aḥar](../../strongs/h/h310.md) him.

<a name="judges_6_35"></a>Judges 6:35

And he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) throughout all [Mᵊnaššê](../../strongs/h/h4519.md); who also was [zāʿaq](../../strongs/h/h2199.md) ['aḥar](../../strongs/h/h310.md) him: and he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) unto ['Āšēr](../../strongs/h/h836.md), and unto [Zᵊḇûlûn](../../strongs/h/h2074.md), and unto [Nap̄tālî](../../strongs/h/h5321.md); and they [ʿālâ](../../strongs/h/h5927.md) to [qārā'](../../strongs/h/h7125.md) them.

<a name="judges_6_36"></a>Judges 6:36

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), If thou [yēš](../../strongs/h/h3426.md) [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) by mine [yad](../../strongs/h/h3027.md), as thou hast [dabar](../../strongs/h/h1696.md),

<a name="judges_6_37"></a>Judges 6:37

Behold, I will [yāṣaḡ](../../strongs/h/h3322.md) a [gizzâ](../../strongs/h/h1492.md) of [ṣemer](../../strongs/h/h6785.md) in the [gōren](../../strongs/h/h1637.md); and if the [ṭal](../../strongs/h/h2919.md) be on the [gizzâ](../../strongs/h/h1492.md) only, and it be [ḥōreḇ](../../strongs/h/h2721.md) upon all the ['erets](../../strongs/h/h776.md) beside, then shall I [yada'](../../strongs/h/h3045.md) that thou wilt [yasha'](../../strongs/h/h3467.md) [Yisra'el](../../strongs/h/h3478.md) by mine [yad](../../strongs/h/h3027.md), as thou hast [dabar](../../strongs/h/h1696.md).

<a name="judges_6_38"></a>Judges 6:38

And it was so: for he [šāḵam](../../strongs/h/h7925.md) on the [māḥŏrāṯ](../../strongs/h/h4283.md), and [zûr](../../strongs/h/h2115.md) the [gizzâ](../../strongs/h/h1492.md) [zûr](../../strongs/h/h2115.md), and [māṣâ](../../strongs/h/h4680.md) the [ṭal](../../strongs/h/h2919.md) out of the [gizzâ](../../strongs/h/h1492.md), a [sēp̄el](../../strongs/h/h5602.md) [mᵊlō'](../../strongs/h/h4393.md) of [mayim](../../strongs/h/h4325.md).

<a name="judges_6_39"></a>Judges 6:39

And [GiḏʿÔn](../../strongs/h/h1439.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), Let not thine ['aph](../../strongs/h/h639.md) be [ḥārâ](../../strongs/h/h2734.md) against me, and I will [dabar](../../strongs/h/h1696.md) but this [pa'am](../../strongs/h/h6471.md): let me [nāsâ](../../strongs/h/h5254.md), I pray thee, but this [pa'am](../../strongs/h/h6471.md) with the [gizzâ](../../strongs/h/h1492.md); let it now be [ḥōreḇ](../../strongs/h/h2721.md) only upon the [gizzâ](../../strongs/h/h1492.md), and upon all the ['erets](../../strongs/h/h776.md) let there be [ṭal](../../strongs/h/h2919.md).

<a name="judges_6_40"></a>Judges 6:40

And ['Elohiym](../../strongs/h/h430.md) ['asah](../../strongs/h/h6213.md) so that [layil](../../strongs/h/h3915.md): for it was [ḥōreḇ](../../strongs/h/h2721.md) upon the [gizzâ](../../strongs/h/h1492.md) only, and there was [ṭal](../../strongs/h/h2919.md) on all the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Judges](judges.md)

[Judges 5](judges_5.md) - [Judges 7](judges_7.md)