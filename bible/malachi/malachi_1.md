# [Malachi 1](https://www.blueletterbible.org/kjv/malachi/1)

<a name="malachi_1_1"></a>Malachi 1:1

The [maśśā'](../../strongs/h/h4853.md) of the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to [Yisra'el](../../strongs/h/h3478.md) [yad](../../strongs/h/h3027.md) [Mal'Āḵî](../../strongs/h/h4401.md).

<a name="malachi_1_2"></a>Malachi 1:2

I have ['ahab](../../strongs/h/h157.md) you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Yet ye ['āmar](../../strongs/h/h559.md), Wherein hast thou ['ahab](../../strongs/h/h157.md) us? Was not [ʿĒśāv](../../strongs/h/h6215.md) [Ya'aqob](../../strongs/h/h3290.md) ['ach](../../strongs/h/h251.md)? ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md): yet I ['ahab](../../strongs/h/h157.md) [Ya'aqob](../../strongs/h/h3290.md),

<a name="malachi_1_3"></a>Malachi 1:3

And I [sane'](../../strongs/h/h8130.md) [ʿĒśāv](../../strongs/h/h6215.md), and [śûm](../../strongs/h/h7760.md) his [har](../../strongs/h/h2022.md) and his [nachalah](../../strongs/h/h5159.md) [šᵊmāmâ](../../strongs/h/h8077.md) for the [ṯn](../../strongs/h/h8568.md) of the [midbar](../../strongs/h/h4057.md).

<a name="malachi_1_4"></a>Malachi 1:4

Whereas ['Ĕḏōm](../../strongs/h/h123.md) ['āmar](../../strongs/h/h559.md), We are [rāšaš](../../strongs/h/h7567.md), but we will [shuwb](../../strongs/h/h7725.md) and [bānâ](../../strongs/h/h1129.md) the [chorbah](../../strongs/h/h2723.md); thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), They shall [bānâ](../../strongs/h/h1129.md), but I will [harac](../../strongs/h/h2040.md); and they shall [qara'](../../strongs/h/h7121.md) them, The [gᵊḇûl](../../strongs/h/h1366.md) of [rišʿâ](../../strongs/h/h7564.md), and, The ['am](../../strongs/h/h5971.md) against whom [Yĕhovah](../../strongs/h/h3068.md) hath [za'am](../../strongs/h/h2194.md) ['owlam](../../strongs/h/h5769.md).

<a name="malachi_1_5"></a>Malachi 1:5

And your ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md), and ye shall ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) will be [gāḏal](../../strongs/h/h1431.md) from the [gᵊḇûl](../../strongs/h/h1366.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="malachi_1_6"></a>Malachi 1:6

A [ben](../../strongs/h/h1121.md) [kabad](../../strongs/h/h3513.md) his ['ab](../../strongs/h/h1.md), and an ['ebed](../../strongs/h/h5650.md) his ['adown](../../strongs/h/h113.md): if then I be an ['ab](../../strongs/h/h1.md), where is mine [kabowd](../../strongs/h/h3519.md)? and if I be an ['adown](../../strongs/h/h113.md), where is my [mowra'](../../strongs/h/h4172.md)? ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) unto you, O [kōhēn](../../strongs/h/h3548.md), that [bazah](../../strongs/h/h959.md) my [shem](../../strongs/h/h8034.md). And ye ['āmar](../../strongs/h/h559.md), Wherein have we [bazah](../../strongs/h/h959.md) thy [shem](../../strongs/h/h8034.md)?

<a name="malachi_1_7"></a>Malachi 1:7

Ye [nāḡaš](../../strongs/h/h5066.md) [gā'al](../../strongs/h/h1351.md) [lechem](../../strongs/h/h3899.md) upon mine [mizbeach](../../strongs/h/h4196.md); and ye ['āmar](../../strongs/h/h559.md), Wherein have we [gā'al](../../strongs/h/h1351.md) thee? In that ye ['āmar](../../strongs/h/h559.md), The [šulḥān](../../strongs/h/h7979.md) of [Yĕhovah](../../strongs/h/h3068.md) is [bazah](../../strongs/h/h959.md).

<a name="malachi_1_8"></a>Malachi 1:8

And if ye [nāḡaš](../../strongs/h/h5066.md) the [ʿiûēr](../../strongs/h/h5787.md) for [zabach](../../strongs/h/h2076.md), is it not [ra'](../../strongs/h/h7451.md)? and if ye [nāḡaš](../../strongs/h/h5066.md) the [pissēaḥ](../../strongs/h/h6455.md) and [ḥālâ](../../strongs/h/h2470.md), is it not [ra'](../../strongs/h/h7451.md)? [qāraḇ](../../strongs/h/h7126.md) it now unto thy [peḥâ](../../strongs/h/h6346.md); will he be [ratsah](../../strongs/h/h7521.md) with thee, or [nasa'](../../strongs/h/h5375.md) thy [paniym](../../strongs/h/h6440.md)? ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_1_9"></a>Malachi 1:9

And now, I pray you, [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) ['el](../../strongs/h/h410.md) that he will be [chanan](../../strongs/h/h2603.md) unto us: this hath been by your [yad](../../strongs/h/h3027.md): will he [nasa'](../../strongs/h/h5375.md) your [paniym](../../strongs/h/h6440.md)? ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_1_10"></a>Malachi 1:10

Who is there even among you that would [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) for nought? neither do ye ['owr](../../strongs/h/h215.md) on mine [mizbeach](../../strongs/h/h4196.md) for [ḥinnām](../../strongs/h/h2600.md). I have no [chephets](../../strongs/h/h2656.md) in you, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), neither will I [ratsah](../../strongs/h/h7521.md) a [minchah](../../strongs/h/h4503.md) at your [yad](../../strongs/h/h3027.md).

<a name="malachi_1_11"></a>Malachi 1:11

For from the [mizrach](../../strongs/h/h4217.md) of the [šemeš](../../strongs/h/h8121.md) even unto the [māḇô'](../../strongs/h/h3996.md) of the same my [shem](../../strongs/h/h8034.md) shall be [gadowl](../../strongs/h/h1419.md) among the [gowy](../../strongs/h/h1471.md); and in every [maqowm](../../strongs/h/h4725.md) [qāṭar](../../strongs/h/h6999.md) shall be [nāḡaš](../../strongs/h/h5066.md) unto my [shem](../../strongs/h/h8034.md), and a [tahowr](../../strongs/h/h2889.md) [minchah](../../strongs/h/h4503.md): for my [shem](../../strongs/h/h8034.md) shall be [gadowl](../../strongs/h/h1419.md) among the [gowy](../../strongs/h/h1471.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_1_12"></a>Malachi 1:12

But ye have [ḥālal](../../strongs/h/h2490.md) it, in that ye ['āmar](../../strongs/h/h559.md), The [šulḥān](../../strongs/h/h7979.md) of [Yĕhovah](../../strongs/h/h3068.md) is [gā'al](../../strongs/h/h1351.md); and the [nîḇ](../../strongs/h/h5108.md) thereof, even his ['ōḵel](../../strongs/h/h400.md), is [bazah](../../strongs/h/h959.md).

<a name="malachi_1_13"></a>Malachi 1:13

Ye ['āmar](../../strongs/h/h559.md) also, Behold, what a [matlā'â](../../strongs/h/h4972.md) is it! and ye have [nāp̄aḥ](../../strongs/h/h5301.md) at it, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); and ye [bow'](../../strongs/h/h935.md) that which was [gāzal](../../strongs/h/h1497.md), and the [pissēaḥ](../../strongs/h/h6455.md), and the [ḥālâ](../../strongs/h/h2470.md); thus ye [bow'](../../strongs/h/h935.md) a [minchah](../../strongs/h/h4503.md): should I [ratsah](../../strongs/h/h7521.md) this of your [yad](../../strongs/h/h3027.md)? ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="malachi_1_14"></a>Malachi 1:14

But ['arar](../../strongs/h/h779.md) be the [nāḵal](../../strongs/h/h5230.md), which hath in his [ʿēḏer](../../strongs/h/h5739.md) a [zāḵār](../../strongs/h/h2145.md), and [nāḏar](../../strongs/h/h5087.md), and [zabach](../../strongs/h/h2076.md) unto the ['adonay](../../strongs/h/h136.md) a corrupt [shachath](../../strongs/h/h7843.md): for I am a [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), and my [shem](../../strongs/h/h8034.md) is [yare'](../../strongs/h/h3372.md) among the [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[Malachi](malachi.md)

[Malachi 2](malachi_2.md)