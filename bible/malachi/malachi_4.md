# [Malachi 4](https://www.blueletterbible.org/kjv/malachi/4)

<a name="malachi_4_1"></a>Malachi 4:1

For, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), that shall [bāʿar](../../strongs/h/h1197.md) as a [tannûr](../../strongs/h/h8574.md); and all the [zed](../../strongs/h/h2086.md), yea, and all that ['asah](../../strongs/h/h6213.md) [rišʿâ](../../strongs/h/h7564.md), shall be [qaš](../../strongs/h/h7179.md): and the [yowm](../../strongs/h/h3117.md) that [bow'](../../strongs/h/h935.md) shall [lāhaṭ](../../strongs/h/h3857.md) them, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that it shall ['azab](../../strongs/h/h5800.md) them neither [šereš](../../strongs/h/h8328.md) nor [ʿānāp̄](../../strongs/h/h6057.md).

<a name="malachi_4_2"></a>Malachi 4:2

But unto you that [yārē'](../../strongs/h/h3373.md) my [shem](../../strongs/h/h8034.md) shall the [šemeš](../../strongs/h/h8121.md) of [tsedaqah](../../strongs/h/h6666.md) [zāraḥ](../../strongs/h/h2224.md) with [marpē'](../../strongs/h/h4832.md) in his [kanaph](../../strongs/h/h3671.md); and ye shall [yāṣā'](../../strongs/h/h3318.md), and grow [pûš](../../strongs/h/h6335.md) as [ʿēḡel](../../strongs/h/h5695.md) of the [marbēq](../../strongs/h/h4770.md).

<a name="malachi_4_3"></a>Malachi 4:3

And ye shall tread [ʿāsas](../../strongs/h/h6072.md) the [rasha'](../../strongs/h/h7563.md); for they shall be ['ēp̄er](../../strongs/h/h665.md) under the [kaph](../../strongs/h/h3709.md) of your [regel](../../strongs/h/h7272.md) in the [yowm](../../strongs/h/h3117.md) that I shall ['asah](../../strongs/h/h6213.md) this, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_4_4"></a>Malachi 4:4

[zakar](../../strongs/h/h2142.md) ye the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md) my ['ebed](../../strongs/h/h5650.md), which I [tsavah](../../strongs/h/h6680.md) unto him in [ḥōrēḇ](../../strongs/h/h2722.md) for all [Yisra'el](../../strongs/h/h3478.md), with the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md).

<a name="malachi_4_5"></a>Malachi 4:5

Behold, I will [shalach](../../strongs/h/h7971.md) you ['Ēlîyâ](../../strongs/h/h452.md) the [nāḇî'](../../strongs/h/h5030.md) [paniym](../../strongs/h/h6440.md) the [bow'](../../strongs/h/h935.md) of the [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="malachi_4_6"></a>Malachi 4:6

And he shall [shuwb](../../strongs/h/h7725.md) the [leb](../../strongs/h/h3820.md) of the ['ab](../../strongs/h/h1.md) to the [ben](../../strongs/h/h1121.md), and the [leb](../../strongs/h/h3820.md) of the [ben](../../strongs/h/h1121.md) to their ['ab](../../strongs/h/h1.md), lest I [bow'](../../strongs/h/h935.md) and [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md) with a [ḥērem](../../strongs/h/h2764.md).

---

[Transliteral Bible](../bible.md)

[Malachi](malachi.md)

[Malachi 3](malachi_3.md)