# [Malachi 3](https://www.blueletterbible.org/kjv/malachi/3)

<a name="malachi_3_1"></a>Malachi 3:1

Behold, I will [shalach](../../strongs/h/h7971.md) my [mal'ak](../../strongs/h/h4397.md), and he shall [panah](../../strongs/h/h6437.md) the [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) me: and the ['adown](../../strongs/h/h113.md), whom ye [bāqaš](../../strongs/h/h1245.md), shall [piṯ'ōm](../../strongs/h/h6597.md) [bow'](../../strongs/h/h935.md) to his [heykal](../../strongs/h/h1964.md), even the [mal'ak](../../strongs/h/h4397.md) of the [bĕriyth](../../strongs/h/h1285.md), whom ye [chaphets](../../strongs/h/h2655.md) in: behold, he shall [bow'](../../strongs/h/h935.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_3_2"></a>Malachi 3:2

But who may [kûl](../../strongs/h/h3557.md) the [yowm](../../strongs/h/h3117.md) of his [bow'](../../strongs/h/h935.md)? and who shall ['amad](../../strongs/h/h5975.md) when he [ra'ah](../../strongs/h/h7200.md)? for he is like a [tsaraph](../../strongs/h/h6884.md) ['esh](../../strongs/h/h784.md), and like [kāḇas](../../strongs/h/h3526.md) [bōrîṯ](../../strongs/h/h1287.md):

<a name="malachi_3_3"></a>Malachi 3:3

And he shall [yashab](../../strongs/h/h3427.md) as a [tsaraph](../../strongs/h/h6884.md) and [ṭāhēr](../../strongs/h/h2891.md) of [keceph](../../strongs/h/h3701.md): and he shall [ṭāhēr](../../strongs/h/h2891.md) the [ben](../../strongs/h/h1121.md) of [Lēvî](../../strongs/h/h3878.md), and [zaqaq](../../strongs/h/h2212.md) them as [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md), that they may [nāḡaš](../../strongs/h/h5066.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [minchah](../../strongs/h/h4503.md) in [tsedaqah](../../strongs/h/h6666.md).

<a name="malachi_3_4"></a>Malachi 3:4

Then shall the [minchah](../../strongs/h/h4503.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) be [ʿāraḇ](../../strongs/h/h6149.md) unto [Yĕhovah](../../strongs/h/h3068.md), as in the [yowm](../../strongs/h/h3117.md) of ['owlam](../../strongs/h/h5769.md), and as in [qaḏmōnî](../../strongs/h/h6931.md) [šānâ](../../strongs/h/h8141.md).

<a name="malachi_3_5"></a>Malachi 3:5

And I will [qāraḇ](../../strongs/h/h7126.md) to you to [mishpat](../../strongs/h/h4941.md); and I will be a [māhar](../../strongs/h/h4116.md) ['ed](../../strongs/h/h5707.md) against the [kāšap̄](../../strongs/h/h3784.md), and against the [na'aph](../../strongs/h/h5003.md), and against [sheqer](../../strongs/h/h8267.md) [shaba'](../../strongs/h/h7650.md), and against those that [ʿāšaq](../../strongs/h/h6231.md) the [śāḵîr](../../strongs/h/h7916.md) in his [śāḵār](../../strongs/h/h7939.md), the ['almānâ](../../strongs/h/h490.md), and the [yathowm](../../strongs/h/h3490.md), and that [natah](../../strongs/h/h5186.md) the [ger](../../strongs/h/h1616.md) from his right, and [yare'](../../strongs/h/h3372.md) not me, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_3_6"></a>Malachi 3:6

For I am [Yĕhovah](../../strongs/h/h3068.md), I [šānâ](../../strongs/h/h8138.md) not; therefore ye [ben](../../strongs/h/h1121.md) of [Ya'aqob](../../strongs/h/h3290.md) are not [kalah](../../strongs/h/h3615.md).

<a name="malachi_3_7"></a>Malachi 3:7

Even from the [yowm](../../strongs/h/h3117.md) of your ['ab](../../strongs/h/h1.md) ye are gone [cuwr](../../strongs/h/h5493.md) from mine [choq](../../strongs/h/h2706.md), and have not [shamar](../../strongs/h/h8104.md) them. [shuwb](../../strongs/h/h7725.md) unto me, and I will [shuwb](../../strongs/h/h7725.md) unto you, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md). But ye ['āmar](../../strongs/h/h559.md), Wherein shall we [shuwb](../../strongs/h/h7725.md)?

<a name="malachi_3_8"></a>Malachi 3:8

Will an ['āḏām](../../strongs/h/h120.md) [qāḇaʿ](../../strongs/h/h6906.md) ['Elohiym](../../strongs/h/h430.md)? Yet ye have [qāḇaʿ](../../strongs/h/h6906.md) me. But ye ['āmar](../../strongs/h/h559.md), Wherein have we [qāḇaʿ](../../strongs/h/h6906.md) thee? In [maʿăśēr](../../strongs/h/h4643.md) and [tᵊrûmâ](../../strongs/h/h8641.md).

<a name="malachi_3_9"></a>Malachi 3:9

Ye are ['arar](../../strongs/h/h779.md) with a [mᵊ'ērâ](../../strongs/h/h3994.md): for ye have [qāḇaʿ](../../strongs/h/h6906.md) me, even this [gowy](../../strongs/h/h1471.md).

<a name="malachi_3_10"></a>Malachi 3:10

[bow'](../../strongs/h/h935.md) ye all the [maʿăśēr](../../strongs/h/h4643.md) into the ['ôṣār](../../strongs/h/h214.md), that there may be [ṭerep̄](../../strongs/h/h2964.md) in mine [bayith](../../strongs/h/h1004.md), and [bachan](../../strongs/h/h974.md) me now herewith, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), if I will not [pāṯaḥ](../../strongs/h/h6605.md) you the ['ărubâ](../../strongs/h/h699.md) of [shamayim](../../strongs/h/h8064.md), and [rîq](../../strongs/h/h7324.md) you a [bĕrakah](../../strongs/h/h1293.md), that there shall not be [day](../../strongs/h/h1767.md).

<a name="malachi_3_11"></a>Malachi 3:11

And I will [gāʿar](../../strongs/h/h1605.md) the ['akal](../../strongs/h/h398.md) for your sakes, and he shall not [shachath](../../strongs/h/h7843.md) the [pĕriy](../../strongs/h/h6529.md) of your ['ăḏāmâ](../../strongs/h/h127.md); neither shall your [gep̄en](../../strongs/h/h1612.md) cast her [šāḵōl](../../strongs/h/h7921.md) before the time in the [sadeh](../../strongs/h/h7704.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_3_12"></a>Malachi 3:12

And all [gowy](../../strongs/h/h1471.md) shall call you ['āšar](../../strongs/h/h833.md): for ye shall be a [chephets](../../strongs/h/h2656.md) ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_3_13"></a>Malachi 3:13

Your [dabar](../../strongs/h/h1697.md) have been [ḥāzaq](../../strongs/h/h2388.md) against me, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md). Yet ye ['āmar](../../strongs/h/h559.md), What have we [dabar](../../strongs/h/h1696.md) so much against thee?

<a name="malachi_3_14"></a>Malachi 3:14

Ye have ['āmar](../../strongs/h/h559.md), It is [shav'](../../strongs/h/h7723.md) to ['abad](../../strongs/h/h5647.md) ['Elohiym](../../strongs/h/h430.md): and what [beṣaʿ](../../strongs/h/h1215.md) is it that we have [shamar](../../strongs/h/h8104.md) his [mišmereṯ](../../strongs/h/h4931.md), and that we have [halak](../../strongs/h/h1980.md) [qᵊḏōrannîṯ](../../strongs/h/h6941.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md)?

<a name="malachi_3_15"></a>Malachi 3:15

And now we ['āšar](../../strongs/h/h833.md) the [zed](../../strongs/h/h2086.md) ['āšar](../../strongs/h/h833.md); yea, they that ['asah](../../strongs/h/h6213.md) [rišʿâ](../../strongs/h/h7564.md) are set [bānâ](../../strongs/h/h1129.md); yea, they that [bachan](../../strongs/h/h974.md) ['Elohiym](../../strongs/h/h430.md) are even [mālaṭ](../../strongs/h/h4422.md).

<a name="malachi_3_16"></a>Malachi 3:16

Then they that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) ['iysh](../../strongs/h/h376.md) to [rea'](../../strongs/h/h7453.md): and [Yĕhovah](../../strongs/h/h3068.md) [qashab](../../strongs/h/h7181.md), and [shama'](../../strongs/h/h8085.md) it, and a [sēp̄er](../../strongs/h/h5612.md) of [zikārôn](../../strongs/h/h2146.md) was [kāṯaḇ](../../strongs/h/h3789.md) [paniym](../../strongs/h/h6440.md) him for them that [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), and that [chashab](../../strongs/h/h2803.md) upon his [shem](../../strongs/h/h8034.md).

<a name="malachi_3_17"></a>Malachi 3:17

And they shall be mine, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), in that [yowm](../../strongs/h/h3117.md) when I make ['asah](../../strongs/h/h6213.md) my [sᵊḡullâ](../../strongs/h/h5459.md); and I will [ḥāmal](../../strongs/h/h2550.md) them, as an ['iysh](../../strongs/h/h376.md) [ḥāmal](../../strongs/h/h2550.md) his own [ben](../../strongs/h/h1121.md) that ['abad](../../strongs/h/h5647.md) him.

<a name="malachi_3_18"></a>Malachi 3:18

Then shall ye [shuwb](../../strongs/h/h7725.md), and [ra'ah](../../strongs/h/h7200.md) between the [tsaddiyq](../../strongs/h/h6662.md) and the [rasha'](../../strongs/h/h7563.md), between him that ['abad](../../strongs/h/h5647.md) ['Elohiym](../../strongs/h/h430.md) and him that ['abad](../../strongs/h/h5647.md) him not.

---

[Transliteral Bible](../bible.md)

[Malachi](malachi.md)

[Malachi 2](malachi_2.md) - [Malachi 4](malachi_4.md)