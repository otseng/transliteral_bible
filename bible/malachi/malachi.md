# Malachi

[Malachi Overview](../../commentary/malachi/malachi_overview.md)

[Malachi 1](malachi_1.md)

[Malachi 2](malachi_2.md)

[Malachi 3](malachi_3.md)

[Malachi 4](malachi_4.md)

---

[Transliteral Bible](../index.md)