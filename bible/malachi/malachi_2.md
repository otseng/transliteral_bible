# [Malachi 2](https://www.blueletterbible.org/kjv/malachi/2)

<a name="malachi_2_1"></a>Malachi 2:1

And now, O ye [kōhēn](../../strongs/h/h3548.md), this [mitsvah](../../strongs/h/h4687.md) is for you.

<a name="malachi_2_2"></a>Malachi 2:2

If ye will not [shama'](../../strongs/h/h8085.md), and if ye will not [śûm](../../strongs/h/h7760.md) it to [leb](../../strongs/h/h3820.md), to [nathan](../../strongs/h/h5414.md) [kabowd](../../strongs/h/h3519.md) unto my [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), I will even [shalach](../../strongs/h/h7971.md) a [mᵊ'ērâ](../../strongs/h/h3994.md) upon you, and I will ['arar](../../strongs/h/h779.md) your [bĕrakah](../../strongs/h/h1293.md): yea, I have ['arar](../../strongs/h/h779.md) them already, because ye do not [śûm](../../strongs/h/h7760.md) it to [leb](../../strongs/h/h3820.md).

<a name="malachi_2_3"></a>Malachi 2:3

Behold, I will [gāʿar](../../strongs/h/h1605.md) your [zera'](../../strongs/h/h2233.md), and [zārâ](../../strongs/h/h2219.md) [pereš](../../strongs/h/h6569.md) upon your [paniym](../../strongs/h/h6440.md), even the [pereš](../../strongs/h/h6569.md) of your [ḥāḡ](../../strongs/h/h2282.md); and one shall take you [nasa'](../../strongs/h/h5375.md) with it.

<a name="malachi_2_4"></a>Malachi 2:4

And ye shall [yada'](../../strongs/h/h3045.md) that I have [shalach](../../strongs/h/h7971.md) this [mitsvah](../../strongs/h/h4687.md) unto you, that my [bĕriyth](../../strongs/h/h1285.md) might be with [Lēvî](../../strongs/h/h3878.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_2_5"></a>Malachi 2:5

My [bĕriyth](../../strongs/h/h1285.md) was with him of [chay](../../strongs/h/h2416.md) and [shalowm](../../strongs/h/h7965.md); and I [nathan](../../strongs/h/h5414.md) them to him for the [mowra'](../../strongs/h/h4172.md) wherewith he [yare'](../../strongs/h/h3372.md) me, and was [ḥāṯaṯ](../../strongs/h/h2865.md) [paniym](../../strongs/h/h6440.md) my [shem](../../strongs/h/h8034.md).

<a name="malachi_2_6"></a>Malachi 2:6

The [towrah](../../strongs/h/h8451.md) of ['emeth](../../strongs/h/h571.md) was in his [peh](../../strongs/h/h6310.md), and ['evel](../../strongs/h/h5766.md) was not [māṣā'](../../strongs/h/h4672.md) in his [saphah](../../strongs/h/h8193.md): he [halak](../../strongs/h/h1980.md) with me in [shalowm](../../strongs/h/h7965.md) and [mîšôr](../../strongs/h/h4334.md), and did [shuwb](../../strongs/h/h7725.md) [rab](../../strongs/h/h7227.md) [shuwb](../../strongs/h/h7725.md) from ['avon](../../strongs/h/h5771.md).

<a name="malachi_2_7"></a>Malachi 2:7

For the [kōhēn](../../strongs/h/h3548.md) [saphah](../../strongs/h/h8193.md) should [shamar](../../strongs/h/h8104.md) [da'ath](../../strongs/h/h1847.md), and they should [bāqaš](../../strongs/h/h1245.md) the [towrah](../../strongs/h/h8451.md) at his [peh](../../strongs/h/h6310.md): for he is the [mal'ak](../../strongs/h/h4397.md) of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_2_8"></a>Malachi 2:8

But ye are [cuwr](../../strongs/h/h5493.md) out of the [derek](../../strongs/h/h1870.md); ye have caused [rab](../../strongs/h/h7227.md) to [kashal](../../strongs/h/h3782.md) at the [towrah](../../strongs/h/h8451.md); ye have [shachath](../../strongs/h/h7843.md) the [bĕriyth](../../strongs/h/h1285.md) of [Lēvî](../../strongs/h/h3878.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_2_9"></a>Malachi 2:9

Therefore have I also [nathan](../../strongs/h/h5414.md) you [bazah](../../strongs/h/h959.md) and [šāp̄āl](../../strongs/h/h8217.md) before all the ['am](../../strongs/h/h5971.md), [peh](../../strongs/h/h6310.md) as ye have not [shamar](../../strongs/h/h8104.md) my [derek](../../strongs/h/h1870.md), but have been [nasa'](../../strongs/h/h5375.md) [paniym](../../strongs/h/h6440.md) in the [towrah](../../strongs/h/h8451.md).

<a name="malachi_2_10"></a>Malachi 2:10

Have we not all one ['ab](../../strongs/h/h1.md)? hath not one ['el](../../strongs/h/h410.md) [bara'](../../strongs/h/h1254.md) us? why do we deal [bāḡaḏ](../../strongs/h/h898.md) every ['iysh](../../strongs/h/h376.md) against his ['ach](../../strongs/h/h251.md), by [ḥālal](../../strongs/h/h2490.md) the [bĕriyth](../../strongs/h/h1285.md) of our ['ab](../../strongs/h/h1.md)?

<a name="malachi_2_11"></a>Malachi 2:11

[Yehuwdah](../../strongs/h/h3063.md) hath [bāḡaḏ](../../strongs/h/h898.md), and a [tôʿēḇâ](../../strongs/h/h8441.md) is ['asah](../../strongs/h/h6213.md) in [Yisra'el](../../strongs/h/h3478.md) and in [Yĕruwshalaim](../../strongs/h/h3389.md); for [Yehuwdah](../../strongs/h/h3063.md) hath [ḥālal](../../strongs/h/h2490.md) the [qodesh](../../strongs/h/h6944.md) of [Yĕhovah](../../strongs/h/h3068.md) which he ['ahab](../../strongs/h/h157.md), and hath [bāʿal](../../strongs/h/h1166.md) the [bath](../../strongs/h/h1323.md) of a [nēḵār](../../strongs/h/h5236.md) ['el](../../strongs/h/h410.md).

<a name="malachi_2_12"></a>Malachi 2:12

[Yĕhovah](../../strongs/h/h3068.md) will [karath](../../strongs/h/h3772.md) the ['iysh](../../strongs/h/h376.md) that ['asah](../../strongs/h/h6213.md) this, the [ʿûr](../../strongs/h/h5782.md) and the ['anah](../../strongs/h/h6030.md), out of the ['ohel](../../strongs/h/h168.md) of [Ya'aqob](../../strongs/h/h3290.md), and him that [nāḡaš](../../strongs/h/h5066.md) a [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="malachi_2_13"></a>Malachi 2:13

And this have ye ['asah](../../strongs/h/h6213.md) again, [kāsâ](../../strongs/h/h3680.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) with [dim'ah](../../strongs/h/h1832.md), with [bĕkiy](../../strongs/h/h1065.md), and with crying ['ănāqâ](../../strongs/h/h603.md), insomuch that he [panah](../../strongs/h/h6437.md) not the [minchah](../../strongs/h/h4503.md) any more, or [laqach](../../strongs/h/h3947.md) it with [ratsown](../../strongs/h/h7522.md) at your [yad](../../strongs/h/h3027.md).

<a name="malachi_2_14"></a>Malachi 2:14

Yet ye ['āmar](../../strongs/h/h559.md), Wherefore? Because [Yĕhovah](../../strongs/h/h3068.md) hath been [ʿûḏ](../../strongs/h/h5749.md) between thee and the ['ishshah](../../strongs/h/h802.md) of thy [nāʿur](../../strongs/h/h5271.md), against whom thou hast [bāḡaḏ](../../strongs/h/h898.md): yet is she thy [ḥăḇereṯ](../../strongs/h/h2278.md), and the ['ishshah](../../strongs/h/h802.md) of thy [bĕriyth](../../strongs/h/h1285.md).

<a name="malachi_2_15"></a>Malachi 2:15

And did not he ['asah](../../strongs/h/h6213.md) one? Yet had he the [šᵊ'ār](../../strongs/h/h7605.md) of the [ruwach](../../strongs/h/h7307.md). And wherefore one? That he might [bāqaš](../../strongs/h/h1245.md) an ['Elohiym](../../strongs/h/h430.md) [zera'](../../strongs/h/h2233.md). Therefore take [shamar](../../strongs/h/h8104.md) to your [ruwach](../../strongs/h/h7307.md), and let none [bāḡaḏ](../../strongs/h/h898.md) against the ['ishshah](../../strongs/h/h802.md) of his [nāʿur](../../strongs/h/h5271.md).

<a name="malachi_2_16"></a>Malachi 2:16

For [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md) that he [sane'](../../strongs/h/h8130.md) putting [shalach](../../strongs/h/h7971.md): for one [kāsâ](../../strongs/h/h3680.md) [chamac](../../strongs/h/h2555.md) with his [lᵊḇûš](../../strongs/h/h3830.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): therefore take [shamar](../../strongs/h/h8104.md) to your [ruwach](../../strongs/h/h7307.md), that ye deal not [bāḡaḏ](../../strongs/h/h898.md).

<a name="malachi_2_17"></a>Malachi 2:17

Ye have [yaga'](../../strongs/h/h3021.md) [Yĕhovah](../../strongs/h/h3068.md) with your [dabar](../../strongs/h/h1697.md). Yet ye ['āmar](../../strongs/h/h559.md), Wherein have we [yaga'](../../strongs/h/h3021.md) him? When ye ['āmar](../../strongs/h/h559.md), Every one that ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) is [towb](../../strongs/h/h2896.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and he [ḥāp̄ēṣ](../../strongs/h/h2654.md) in them; or, Where is the ['Elohiym](../../strongs/h/h430.md) of [mishpat](../../strongs/h/h4941.md)?

---

[Transliteral Bible](../bible.md)

[Malachi](malachi.md)

[Malachi 1](malachi_1.md) - [Malachi 3](malachi_3.md)