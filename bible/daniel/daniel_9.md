# [Daniel 9](https://www.blueletterbible.org/kjv/daniel/9)

<a name="daniel_9_1"></a>Daniel 9:1

In the first [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥašvērôš](../../strongs/h/h325.md), of the [zera'](../../strongs/h/h2233.md) of the [Māḏay](../../strongs/h/h4074.md), which was made [mālaḵ](../../strongs/h/h4427.md) over the [malkuwth](../../strongs/h/h4438.md) of the [Kaśdîmâ](../../strongs/h/h3778.md);

<a name="daniel_9_2"></a>Daniel 9:2

In the first [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md) I [Dinîyē'L](../../strongs/h/h1840.md) [bîn](../../strongs/h/h995.md) by [sēp̄er](../../strongs/h/h5612.md) the [mispār](../../strongs/h/h4557.md) of the [šānâ](../../strongs/h/h8141.md), whereof the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), that he would [mālā'](../../strongs/h/h4390.md) seventy [šānâ](../../strongs/h/h8141.md) in the [chorbah](../../strongs/h/h2723.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="daniel_9_3"></a>Daniel 9:3

And I [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) unto the ['adonay](../../strongs/h/h136.md) ['Elohiym](../../strongs/h/h430.md), to [bāqaš](../../strongs/h/h1245.md) by [tĕphillah](../../strongs/h/h8605.md) and [taḥănûn](../../strongs/h/h8469.md), with [ṣôm](../../strongs/h/h6685.md), and [śaq](../../strongs/h/h8242.md), and ['ēp̄er](../../strongs/h/h665.md):

<a name="daniel_9_4"></a>Daniel 9:4

And I [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), and made my [yadah](../../strongs/h/h3034.md), and ['āmar](../../strongs/h/h559.md), ['ānnā'](../../strongs/h/h577.md) ['adonay](../../strongs/h/h136.md), the [gadowl](../../strongs/h/h1419.md) and [yare'](../../strongs/h/h3372.md) ['el](../../strongs/h/h410.md), [shamar](../../strongs/h/h8104.md) the [bĕriyth](../../strongs/h/h1285.md) and [checed](../../strongs/h/h2617.md) to them that ['ahab](../../strongs/h/h157.md) him, and to them that [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md);

<a name="daniel_9_5"></a>Daniel 9:5

We have [chata'](../../strongs/h/h2398.md), and have [ʿāvâ](../../strongs/h/h5753.md), and have done [rāšaʿ](../../strongs/h/h7561.md), and have [māraḏ](../../strongs/h/h4775.md), even by [cuwr](../../strongs/h/h5493.md) from thy [mitsvah](../../strongs/h/h4687.md) and from thy [mishpat](../../strongs/h/h4941.md):

<a name="daniel_9_6"></a>Daniel 9:6

Neither have we [shama'](../../strongs/h/h8085.md) unto thy ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), which [dabar](../../strongs/h/h1696.md) in thy [shem](../../strongs/h/h8034.md) to our [melek](../../strongs/h/h4428.md), our [śar](../../strongs/h/h8269.md), and our ['ab](../../strongs/h/h1.md), and to all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="daniel_9_7"></a>Daniel 9:7

['Adonay](../../strongs/h/h136.md), [tsedaqah](../../strongs/h/h6666.md) belongeth unto thee, but unto us [bšeṯ](../../strongs/h/h1322.md) of [paniym](../../strongs/h/h6440.md), as at this [yowm](../../strongs/h/h3117.md); to the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and unto all [Yisra'el](../../strongs/h/h3478.md), that are [qarowb](../../strongs/h/h7138.md), and that are far [rachowq](../../strongs/h/h7350.md), through all the ['erets](../../strongs/h/h776.md) whither thou hast [nāḏaḥ](../../strongs/h/h5080.md) them, because of their [maʿal](../../strongs/h/h4604.md) that they have [māʿal](../../strongs/h/h4603.md) against thee.

<a name="daniel_9_8"></a>Daniel 9:8

['Adonay](../../strongs/h/h136.md), to us [bšeṯ](../../strongs/h/h1322.md) of [paniym](../../strongs/h/h6440.md), to our [melek](../../strongs/h/h4428.md), to our [śar](../../strongs/h/h8269.md), and to our ['ab](../../strongs/h/h1.md), because we have [chata'](../../strongs/h/h2398.md) against thee.

<a name="daniel_9_9"></a>Daniel 9:9

To the ['adonay](../../strongs/h/h136.md) our ['Elohiym](../../strongs/h/h430.md) belong [raḥam](../../strongs/h/h7356.md) and [sᵊlîḥâ](../../strongs/h/h5547.md), though we have [māraḏ](../../strongs/h/h4775.md) against him;

<a name="daniel_9_10"></a>Daniel 9:10

Neither have we [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), to [yālaḵ](../../strongs/h/h3212.md) in his [towrah](../../strongs/h/h8451.md), which he [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) us [yad](../../strongs/h/h3027.md) his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="daniel_9_11"></a>Daniel 9:11

Yea, all [Yisra'el](../../strongs/h/h3478.md) have ['abar](../../strongs/h/h5674.md) thy [towrah](../../strongs/h/h8451.md), even by [cuwr](../../strongs/h/h5493.md), that they might not [shama'](../../strongs/h/h8085.md) thy [qowl](../../strongs/h/h6963.md); therefore the ['alah](../../strongs/h/h423.md) is [nāṯaḵ](../../strongs/h/h5413.md) upon us, and the [šᵊḇûʿâ](../../strongs/h/h7621.md) that is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of ['Elohiym](../../strongs/h/h430.md), because we have [chata'](../../strongs/h/h2398.md) against him.

<a name="daniel_9_12"></a>Daniel 9:12

And he hath [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md), which he [dabar](../../strongs/h/h1696.md) against us, and against our [shaphat](../../strongs/h/h8199.md) that [shaphat](../../strongs/h/h8199.md) us, by [bow'](../../strongs/h/h935.md) upon us a [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md): for under the [shamayim](../../strongs/h/h8064.md) hath not been ['asah](../../strongs/h/h6213.md) as hath been ['asah](../../strongs/h/h6213.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="daniel_9_13"></a>Daniel 9:13

As it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), all this [ra'](../../strongs/h/h7451.md) is [bow'](../../strongs/h/h935.md) upon us: yet made we not our [ḥālâ](../../strongs/h/h2470.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), that we might [shuwb](../../strongs/h/h7725.md) from our ['avon](../../strongs/h/h5771.md), and [sakal](../../strongs/h/h7919.md) thy ['emeth](../../strongs/h/h571.md).

<a name="daniel_9_14"></a>Daniel 9:14

Therefore hath [Yĕhovah](../../strongs/h/h3068.md) [šāqaḏ](../../strongs/h/h8245.md) upon the [ra'](../../strongs/h/h7451.md), and [bow'](../../strongs/h/h935.md) it upon us: for [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) is [tsaddiyq](../../strongs/h/h6662.md) in all his [ma'aseh](../../strongs/h/h4639.md) which he ['asah](../../strongs/h/h6213.md): for we [shama'](../../strongs/h/h8085.md) not his [qowl](../../strongs/h/h6963.md).

<a name="daniel_9_15"></a>Daniel 9:15

And now, ['Adonay](../../strongs/h/h136.md) our ['Elohiym](../../strongs/h/h430.md), that hast [yāṣā'](../../strongs/h/h3318.md) thy ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and hast ['asah](../../strongs/h/h6213.md) thee [shem](../../strongs/h/h8034.md), as at this [yowm](../../strongs/h/h3117.md); we have [chata'](../../strongs/h/h2398.md), we have done [rāšaʿ](../../strongs/h/h7561.md).

<a name="daniel_9_16"></a>Daniel 9:16

['Adonay](../../strongs/h/h136.md), according to all thy [tsedaqah](../../strongs/h/h6666.md), I beseech thee, let thine ['aph](../../strongs/h/h639.md) and thy [chemah](../../strongs/h/h2534.md) be [shuwb](../../strongs/h/h7725.md) from thy [ʿîr](../../strongs/h/h5892.md) [Yĕruwshalaim](../../strongs/h/h3389.md), thy [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md): because for our [ḥēṭĕ'](../../strongs/h/h2399.md), and for the ['avon](../../strongs/h/h5771.md) of our ['ab](../../strongs/h/h1.md), [Yĕruwshalaim](../../strongs/h/h3389.md) and thy ['am](../../strongs/h/h5971.md) are become a [cherpah](../../strongs/h/h2781.md) to all that are [cabiyb](../../strongs/h/h5439.md) us.

<a name="daniel_9_17"></a>Daniel 9:17

Now therefore, O our ['Elohiym](../../strongs/h/h430.md), [shama'](../../strongs/h/h8085.md) the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), and his [taḥănûn](../../strongs/h/h8469.md), and cause thy [paniym](../../strongs/h/h6440.md) to ['owr](../../strongs/h/h215.md) upon thy [miqdash](../../strongs/h/h4720.md) that is [šāmēm](../../strongs/h/h8076.md), for the ['adonay](../../strongs/h/h136.md) sake.

<a name="daniel_9_18"></a>Daniel 9:18

O my ['Elohiym](../../strongs/h/h430.md), [natah](../../strongs/h/h5186.md) thine ['ozen](../../strongs/h/h241.md), and [shama'](../../strongs/h/h8085.md); [paqach](../../strongs/h/h6491.md) thine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) our [šāmēm](../../strongs/h/h8074.md), and the [ʿîr](../../strongs/h/h5892.md) which is [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md): for we do not [naphal](../../strongs/h/h5307.md) our [taḥănûn](../../strongs/h/h8469.md) [paniym](../../strongs/h/h6440.md) thee for our [tsedaqah](../../strongs/h/h6666.md), but for thy [rab](../../strongs/h/h7227.md) [raḥam](../../strongs/h/h7356.md).

<a name="daniel_9_19"></a>Daniel 9:19

['Adonay](../../strongs/h/h136.md), [shama'](../../strongs/h/h8085.md); ['Adonay](../../strongs/h/h136.md), [sālaḥ](../../strongs/h/h5545.md); ['Adonay](../../strongs/h/h136.md), [qashab](../../strongs/h/h7181.md) and ['asah](../../strongs/h/h6213.md); ['āḥar](../../strongs/h/h309.md) not, for thine own sake, O my ['Elohiym](../../strongs/h/h430.md): for thy [ʿîr](../../strongs/h/h5892.md) and thy ['am](../../strongs/h/h5971.md) are [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md).

<a name="daniel_9_20"></a>Daniel 9:20

And whiles I was [dabar](../../strongs/h/h1696.md), and [palal](../../strongs/h/h6419.md), and [yadah](../../strongs/h/h3034.md) my [chatta'ath](../../strongs/h/h2403.md) and the [chatta'ath](../../strongs/h/h2403.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and [naphal](../../strongs/h/h5307.md) my [tĕchinnah](../../strongs/h/h8467.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md) for the [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md) of my ['Elohiym](../../strongs/h/h430.md);

<a name="daniel_9_21"></a>Daniel 9:21

Yea, whiles I was [dabar](../../strongs/h/h1696.md) in [tĕphillah](../../strongs/h/h8605.md), even the ['iysh](../../strongs/h/h376.md) [Gaḇrî'Ēl](../../strongs/h/h1403.md), whom I had [ra'ah](../../strongs/h/h7200.md) in the [ḥāzôn](../../strongs/h/h2377.md) at the [tᵊḥillâ](../../strongs/h/h8462.md), being caused to [yāʿap̄](../../strongs/h/h3286.md) [yᵊʿāp̄](../../strongs/h/h3288.md), [naga'](../../strongs/h/h5060.md) me about the [ʿēṯ](../../strongs/h/h6256.md) of the ['ereb](../../strongs/h/h6153.md) [minchah](../../strongs/h/h4503.md).

<a name="daniel_9_22"></a>Daniel 9:22

And he [bîn](../../strongs/h/h995.md) me, and [dabar](../../strongs/h/h1696.md) with me, and ['āmar](../../strongs/h/h559.md), O [Dinîyē'L](../../strongs/h/h1840.md), I am now [yāṣā'](../../strongs/h/h3318.md) to give thee [sakal](../../strongs/h/h7919.md) and [bînâ](../../strongs/h/h998.md).

<a name="daniel_9_23"></a>Daniel 9:23

At the [tᵊḥillâ](../../strongs/h/h8462.md) of thy [taḥănûn](../../strongs/h/h8469.md) the [dabar](../../strongs/h/h1697.md) [yāṣā'](../../strongs/h/h3318.md), and I am [bow'](../../strongs/h/h935.md) to [nāḡaḏ](../../strongs/h/h5046.md) thee; for thou art greatly [ḥemdâ](../../strongs/h/h2532.md): therefore [bîn](../../strongs/h/h995.md) the [dabar](../../strongs/h/h1697.md), and [bîn](../../strongs/h/h995.md) the [mar'ê](../../strongs/h/h4758.md).

<a name="daniel_9_24"></a>Daniel 9:24

Seventy [šāḇûaʿ](../../strongs/h/h7620.md) are [ḥāṯaḵ](../../strongs/h/h2852.md) upon thy ['am](../../strongs/h/h5971.md) and upon thy [qodesh](../../strongs/h/h6944.md) [ʿîr](../../strongs/h/h5892.md), to [kālā'](../../strongs/h/h3607.md) the [pesha'](../../strongs/h/h6588.md), and to make a [tamam](../../strongs/h/h8552.md) [ḥāṯam](../../strongs/h/h2856.md) of [chatta'ath](../../strongs/h/h2403.md), and to make [kāp̄ar](../../strongs/h/h3722.md) for ['avon](../../strongs/h/h5771.md), and to [bow'](../../strongs/h/h935.md) ['owlam](../../strongs/h/h5769.md) [tsedeq](../../strongs/h/h6664.md), and to [ḥāṯam](../../strongs/h/h2856.md) the [ḥāzôn](../../strongs/h/h2377.md) and [nāḇî'](../../strongs/h/h5030.md), and to [māšaḥ](../../strongs/h/h4886.md) the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="daniel_9_25"></a>Daniel 9:25

[yada'](../../strongs/h/h3045.md) therefore and [sakal](../../strongs/h/h7919.md), that from the going [môṣā'](../../strongs/h/h4161.md) of the [dabar](../../strongs/h/h1697.md) to [shuwb](../../strongs/h/h7725.md) and to [bānâ](../../strongs/h/h1129.md) [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [mashiyach](../../strongs/h/h4899.md) the [nāḡîḏ](../../strongs/h/h5057.md) shall be seven [šāḇûaʿ](../../strongs/h/h7620.md), and threescore and two [šāḇûaʿ](../../strongs/h/h7620.md): the [rᵊḥōḇ](../../strongs/h/h7339.md) shall be [bānâ](../../strongs/h/h1129.md) [shuwb](../../strongs/h/h7725.md), and the [ḥārûṣ](../../strongs/h/h2742.md), even in [ṣôq](../../strongs/h/h6695.md) [ʿēṯ](../../strongs/h/h6256.md).

<a name="daniel_9_26"></a>Daniel 9:26

And ['aḥar](../../strongs/h/h310.md) threescore and two [šāḇûaʿ](../../strongs/h/h7620.md) shall [mashiyach](../../strongs/h/h4899.md) be [karath](../../strongs/h/h3772.md), but not for himself: and the ['am](../../strongs/h/h5971.md) of the [nāḡîḏ](../../strongs/h/h5057.md) that shall [bow'](../../strongs/h/h935.md) shall [shachath](../../strongs/h/h7843.md) the [ʿîr](../../strongs/h/h5892.md) and the [qodesh](../../strongs/h/h6944.md); and the [qēṣ](../../strongs/h/h7093.md) thereof shall be with a [šeṭep̄](../../strongs/h/h7858.md), and unto the [qēṣ](../../strongs/h/h7093.md) of the [milḥāmâ](../../strongs/h/h4421.md) [šāmēm](../../strongs/h/h8074.md) are [ḥāraṣ](../../strongs/h/h2782.md).

<a name="daniel_9_27"></a>Daniel 9:27

And he shall [gabar](../../strongs/h/h1396.md) the [bĕriyth](../../strongs/h/h1285.md) with [rab](../../strongs/h/h7227.md) for one [šāḇûaʿ](../../strongs/h/h7620.md): and in the [ḥēṣî](../../strongs/h/h2677.md) of the [šāḇûaʿ](../../strongs/h/h7620.md) he shall cause the [zebach](../../strongs/h/h2077.md) and the [minchah](../../strongs/h/h4503.md) to [shabath](../../strongs/h/h7673.md), and for the [kanaph](../../strongs/h/h3671.md) of [šiqqûṣ](../../strongs/h/h8251.md) he shall make it [šāmēm](../../strongs/h/h8074.md), even until the [kālâ](../../strongs/h/h3617.md), and that [ḥāraṣ](../../strongs/h/h2782.md) shall be [nāṯaḵ](../../strongs/h/h5413.md) upon the [šāmēm](../../strongs/h/h8074.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 8](daniel_8.md) - [Daniel 10](daniel_10.md)