# [Daniel 5](https://www.blueletterbible.org/kjv/daniel/5)

<a name="daniel_5_1"></a>Daniel 5:1

[Bēlša'Ṣṣar](../../strongs/h/h1113.md) the [meleḵ](../../strongs/h/h4430.md) [ʿăḇaḏ](../../strongs/h/h5648.md) a [raḇ](../../strongs/h/h7229.md) [lᵊḥem](../../strongs/h/h3900.md) to an ['elep̄](../../strongs/h/h506.md) of his [raḇrᵊḇānîn](../../strongs/h/h7261.md), and [šᵊṯâ](../../strongs/h/h8355.md) [ḥămar](../../strongs/h/h2562.md) [qᵊḇēl](../../strongs/h/h6903.md) the ['elep̄](../../strongs/h/h506.md).

<a name="daniel_5_2"></a>Daniel 5:2

[Bēlša'Ṣṣar](../../strongs/h/h1113.md), whiles he [ṭᵊʿēm](../../strongs/h/h2942.md) the [ḥămar](../../strongs/h/h2562.md), ['ămar](../../strongs/h/h560.md) to ['ăṯâ](../../strongs/h/h858.md) the [dᵊhaḇ](../../strongs/h/h1722.md) and [kᵊsap̄](../../strongs/h/h3702.md) [mā'n](../../strongs/h/h3984.md) which his ['ab](../../strongs/h/h2.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) had [nᵊp̄aq](../../strongs/h/h5312.md) out [min](../../strongs/h/h4481.md) the [hêḵal](../../strongs/h/h1965.md) which was in [Yᵊrûšlēm](../../strongs/h/h3390.md); that the [meleḵ](../../strongs/h/h4430.md), and his [raḇrᵊḇānîn](../../strongs/h/h7261.md), his [šēḡāl](../../strongs/h/h7695.md), and his [lᵊḥēnâ](../../strongs/h/h3904.md), might [šᵊṯâ](../../strongs/h/h8355.md) therein.

<a name="daniel_5_3"></a>Daniel 5:3

['ĕḏayin](../../strongs/h/h116.md) they ['ăṯâ](../../strongs/h/h858.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [mā'n](../../strongs/h/h3984.md) that were [nᵊp̄aq](../../strongs/h/h5312.md) out [min](../../strongs/h/h4481.md) the [hêḵal](../../strongs/h/h1965.md) of the [bayiṯ](../../strongs/h/h1005.md) of ['ĕlâ](../../strongs/h/h426.md) which was at [Yᵊrûšlēm](../../strongs/h/h3390.md); and the [meleḵ](../../strongs/h/h4430.md), and his [raḇrᵊḇānîn](../../strongs/h/h7261.md), his [šēḡāl](../../strongs/h/h7695.md), and his [lᵊḥēnâ](../../strongs/h/h3904.md), [šᵊṯâ](../../strongs/h/h8355.md) in them.

<a name="daniel_5_4"></a>Daniel 5:4

They [šᵊṯâ](../../strongs/h/h8355.md) [ḥămar](../../strongs/h/h2562.md), and [šᵊḇaḥ](../../strongs/h/h7624.md) the ['ĕlâ](../../strongs/h/h426.md) of [dᵊhaḇ](../../strongs/h/h1722.md), and of [kᵊsap̄](../../strongs/h/h3702.md), of [nᵊḥāš](../../strongs/h/h5174.md), of [parzel](../../strongs/h/h6523.md), of ['āʿ](../../strongs/h/h636.md), and of ['eḇen](../../strongs/h/h69.md).

<a name="daniel_5_5"></a>Daniel 5:5

In the same [šāʿâ](../../strongs/h/h8160.md) [nᵊp̄aq](../../strongs/h/h5312.md) ['eṣbʿ](../../strongs/h/h677.md) of an ['ēneš](../../strongs/h/h606.md) [yaḏ](../../strongs/h/h3028.md), and [kᵊṯaḇ](../../strongs/h/h3790.md) over [qᵊḇēl](../../strongs/h/h6903.md) the [neḇraštā'](../../strongs/h/h5043.md) [ʿal](../../strongs/h/h5922.md) the [gîr](../../strongs/h/h1528.md) of the [kᵊṯal](../../strongs/h/h3797.md) of the [meleḵ](../../strongs/h/h4430.md) [hêḵal](../../strongs/h/h1965.md): and the [meleḵ](../../strongs/h/h4430.md) [ḥăzā'](../../strongs/h/h2370.md) the [pas](../../strongs/h/h6447.md) of the [yaḏ](../../strongs/h/h3028.md) that [kᵊṯaḇ](../../strongs/h/h3790.md).

<a name="daniel_5_6"></a>Daniel 5:6

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) [zîv](../../strongs/h/h2122.md) was [šᵊnâ](../../strongs/h/h8133.md), and his [raʿyôn](../../strongs/h/h7476.md) [bᵊhal](../../strongs/h/h927.md) him, so that the [qᵊṭar](../../strongs/h/h7001.md) of his [ḥăraṣ](../../strongs/h/h2783.md) were [śᵊrâ](../../strongs/h/h8271.md), and his ['arḵubâ](../../strongs/h/h755.md) [nᵊqaש](../../strongs/h/h5368.md) [dā'](../../strongs/h/h1668.md) against [dā'](../../strongs/h/h1668.md).

<a name="daniel_5_7"></a>Daniel 5:7

The [meleḵ](../../strongs/h/h4430.md) [qᵊrā'](../../strongs/h/h7123.md) [ḥayil](../../strongs/h/h2429.md) to [ʿălal](../../strongs/h/h5954.md) in the ['aššāp̄](../../strongs/h/h826.md), the [kaśday](../../strongs/h/h3779.md), and the [gᵊzar](../../strongs/h/h1505.md). And the [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md) to the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md), ['ēneš](../../strongs/h/h606.md) [kōl](../../strongs/h/h3606.md) shall [qᵊrā'](../../strongs/h/h7123.md) [dēn](../../strongs/h/h1836.md) [kᵊṯāḇ](../../strongs/h/h3792.md), and [ḥăvā'](../../strongs/h/h2324.md) me the [pᵊšar](../../strongs/h/h6591.md) thereof, shall be [lᵊḇaש](../../strongs/h/h3848.md) with ['arḡᵊvān](../../strongs/h/h711.md), and have a [hamnîḵ](../../strongs/h/h2002.md) of [dᵊhaḇ](../../strongs/h/h1722.md) [ʿal](../../strongs/h/h5922.md) his [ṣaûā'r](../../strongs/h/h6676.md), and shall be the [tᵊlîṯay](../../strongs/h/h8523.md) [šᵊlēṭ](../../strongs/h/h7981.md) in the [malkû](../../strongs/h/h4437.md).

<a name="daniel_5_8"></a>Daniel 5:8

['ĕḏayin](../../strongs/h/h116.md) [ʿălal](../../strongs/h/h5954.md) in [kōl](../../strongs/h/h3606.md) the [meleḵ](../../strongs/h/h4430.md) [ḥakîm](../../strongs/h/h2445.md) men: but they [kᵊhal](../../strongs/h/h3546.md) [lā'](../../strongs/h/h3809.md) [qᵊrā'](../../strongs/h/h7123.md) the [kᵊṯāḇ](../../strongs/h/h3792.md), nor make [yᵊḏaʿ](../../strongs/h/h3046.md) to the [meleḵ](../../strongs/h/h4430.md) the [pᵊšar](../../strongs/h/h6591.md) thereof.

<a name="daniel_5_9"></a>Daniel 5:9

['ĕḏayin](../../strongs/h/h116.md) was [meleḵ](../../strongs/h/h4430.md) [Bēlša'Ṣṣar](../../strongs/h/h1113.md) [śagî'](../../strongs/h/h7690.md) [bᵊhal](../../strongs/h/h927.md), and his [zîv](../../strongs/h/h2122.md) was [šᵊnâ](../../strongs/h/h8133.md) in [ʿal](../../strongs/h/h5922.md), and his [raḇrᵊḇānîn](../../strongs/h/h7261.md) were [šᵊḇaš](../../strongs/h/h7672.md).

<a name="daniel_5_10"></a>Daniel 5:10

Now the [malkā'](../../strongs/h/h4433.md), by [qᵊḇēl](../../strongs/h/h6903.md) of the [millâ](../../strongs/h/h4406.md) of the [meleḵ](../../strongs/h/h4430.md) and his [raḇrᵊḇānîn](../../strongs/h/h7261.md), [ʿălal](../../strongs/h/h5954.md) into the [mištê](../../strongs/h/h4961.md) [bayiṯ](../../strongs/h/h1005.md): and the [malkā'](../../strongs/h/h4433.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), O [meleḵ](../../strongs/h/h4430.md), [ḥăyā'](../../strongs/h/h2418.md) for [ʿālam](../../strongs/h/h5957.md): let ['al](../../strongs/h/h409.md) thy [raʿyôn](../../strongs/h/h7476.md) [bᵊhal](../../strongs/h/h927.md) thee, nor let thy [zîv](../../strongs/h/h2122.md) be [šᵊnâ](../../strongs/h/h8133.md):

<a name="daniel_5_11"></a>Daniel 5:11

There ['îṯay](../../strongs/h/h383.md) a [gᵊḇar](../../strongs/h/h1400.md) in thy [malkû](../../strongs/h/h4437.md), in whom is the [rûaḥ](../../strongs/h/h7308.md) of the [qadîš](../../strongs/h/h6922.md) ['ĕlâ](../../strongs/h/h426.md); and in the [yôm](../../strongs/h/h3118.md) of thy ['ab](../../strongs/h/h2.md) [nāhîrû](../../strongs/h/h5094.md) and [śāḵlᵊṯānû](../../strongs/h/h7924.md) and [ḥāḵmâ](../../strongs/h/h2452.md), like the [ḥāḵmâ](../../strongs/h/h2452.md) of the ['ĕlâ](../../strongs/h/h426.md), was [šᵊḵaḥ](../../strongs/h/h7912.md) in him; whom the [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) thy ['ab](../../strongs/h/h2.md), the [meleḵ](../../strongs/h/h4430.md), I say, thy ['ab](../../strongs/h/h2.md), [qûm](../../strongs/h/h6966.md) [raḇ](../../strongs/h/h7229.md) of the [ḥarṭṭōm](../../strongs/h/h2749.md), ['aššāp̄](../../strongs/h/h826.md), [kaśday](../../strongs/h/h3779.md), and [gᵊzar](../../strongs/h/h1505.md);

<a name="daniel_5_12"></a>Daniel 5:12

Forasmuch [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) a [yatîr](../../strongs/h/h3493.md) [rûaḥ](../../strongs/h/h7308.md), and [mandaʿ](../../strongs/h/h4486.md), and [śāḵlᵊṯānû](../../strongs/h/h7924.md), [pᵊšar](../../strongs/h/h6590.md) of [ḥēlem](../../strongs/h/h2493.md), and ['aḥăvāyâ](../../strongs/h/h263.md) of hard ['ăḥîḏâ](../../strongs/h/h280.md), and [śᵊrâ](../../strongs/h/h8271.md) of [qᵊṭar](../../strongs/h/h7001.md), were [šᵊḵaḥ](../../strongs/h/h7912.md) in the same [Dānîyē'L](../../strongs/h/h1841.md), whom the [meleḵ](../../strongs/h/h4430.md) [śûm](../../strongs/h/h7761.md) [šum](../../strongs/h/h8036.md) [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md): [kᵊʿan](../../strongs/h/h3705.md) let [Dānîyē'L](../../strongs/h/h1841.md) be [qᵊrā'](../../strongs/h/h7123.md), and he will [ḥăvā'](../../strongs/h/h2324.md) the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_5_13"></a>Daniel 5:13

['ĕḏayin](../../strongs/h/h116.md) was [Dānîyē'L](../../strongs/h/h1841.md) [ʿălal](../../strongs/h/h5954.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md). And the [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) unto [Dānîyē'L](../../strongs/h/h1841.md), Art ['antâ](../../strongs/h/h607.md) that [Dānîyē'L](../../strongs/h/h1841.md), which art [min](../../strongs/h/h4481.md) the [bēn](../../strongs/h/h1123.md) of the [gālûṯ](../../strongs/h/h1547.md) of [Yᵊhûḏ](../../strongs/h/h3061.md), whom the [meleḵ](../../strongs/h/h4430.md) my ['ab](../../strongs/h/h2.md) ['ăṯâ](../../strongs/h/h858.md) out [min](../../strongs/h/h4481.md) [Yᵊhûḏ](../../strongs/h/h3061.md)?

<a name="daniel_5_14"></a>Daniel 5:14

I have even [šᵊmaʿ](../../strongs/h/h8086.md) of thee, that the [rûaḥ](../../strongs/h/h7308.md) of the ['ĕlâ](../../strongs/h/h426.md) is in [ʿal](../../strongs/h/h5922.md), and that [nāhîrû](../../strongs/h/h5094.md) and [śāḵlᵊṯānû](../../strongs/h/h7924.md) and [yatîr](../../strongs/h/h3493.md) [ḥāḵmâ](../../strongs/h/h2452.md) is [šᵊḵaḥ](../../strongs/h/h7912.md) in thee.

<a name="daniel_5_15"></a>Daniel 5:15

And [kᵊʿan](../../strongs/h/h3705.md) the [ḥakîm](../../strongs/h/h2445.md) men, the ['aššāp̄](../../strongs/h/h826.md), have been [ʿălal](../../strongs/h/h5954.md) in [qŏḏām](../../strongs/h/h6925.md) me, that they should [qᵊrā'](../../strongs/h/h7123.md) [dēn](../../strongs/h/h1836.md) [kᵊṯāḇ](../../strongs/h/h3792.md), and make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [pᵊšar](../../strongs/h/h6591.md) thereof: but they [kᵊhal](../../strongs/h/h3546.md) [lā'](../../strongs/h/h3809.md) [ḥăvā'](../../strongs/h/h2324.md) the [pᵊšar](../../strongs/h/h6591.md) of the [millâ](../../strongs/h/h4406.md):

<a name="daniel_5_16"></a>Daniel 5:16

And ['ănā'](../../strongs/h/h576.md) have [šᵊmaʿ](../../strongs/h/h8086.md) of [ʿal](../../strongs/h/h5922.md), that thou [yᵊḵēl](../../strongs/h/h3202.md) [pᵊšar](../../strongs/h/h6590.md) [pᵊšar](../../strongs/h/h6591.md), and [śᵊrâ](../../strongs/h/h8271.md) [qᵊṭar](../../strongs/h/h7001.md): [kᵊʿan](../../strongs/h/h3705.md) [hēn](../../strongs/h/h2006.md) thou [yᵊḵēl](../../strongs/h/h3202.md) [qᵊrā'](../../strongs/h/h7123.md) the [kᵊṯāḇ](../../strongs/h/h3792.md), and make [yᵊḏaʿ](../../strongs/h/h3046.md) to me the [pᵊšar](../../strongs/h/h6591.md) thereof, thou shalt be [lᵊḇaש](../../strongs/h/h3848.md) with ['arḡᵊvān](../../strongs/h/h711.md), and have a [hamnîḵ](../../strongs/h/h2002.md) of [dᵊhaḇ](../../strongs/h/h1722.md) [ʿal](../../strongs/h/h5922.md) thy [ṣaûā'r](../../strongs/h/h6676.md), and shalt be the [tᵊlaṯ](../../strongs/h/h8531.md) [šᵊlēṭ](../../strongs/h/h7981.md) in the [malkû](../../strongs/h/h4437.md).

<a name="daniel_5_17"></a>Daniel 5:17

['ĕḏayin](../../strongs/h/h116.md) [Dānîyē'L](../../strongs/h/h1841.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), Let thy [matnā'](../../strongs/h/h4978.md) [hăvâ](../../strongs/h/h1934.md) to thyself, and [yᵊhaḇ](../../strongs/h/h3052.md) thy [nᵊḇizbâ](../../strongs/h/h5023.md) to ['āḥŏrān](../../strongs/h/h321.md); [bᵊram](../../strongs/h/h1297.md) I will [qᵊrā'](../../strongs/h/h7123.md) the [kᵊṯāḇ](../../strongs/h/h3792.md) unto the [meleḵ](../../strongs/h/h4430.md), and make [yᵊḏaʿ](../../strongs/h/h3046.md) to him the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_5_18"></a>Daniel 5:18

O ['antâ](../../strongs/h/h607.md) [meleḵ](../../strongs/h/h4430.md), the most [ʿillay](../../strongs/h/h5943.md) ['ĕlâ](../../strongs/h/h426.md) [yᵊhaḇ](../../strongs/h/h3052.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) thy ['ab](../../strongs/h/h2.md) a [malkû](../../strongs/h/h4437.md), and [rᵊḇû](../../strongs/h/h7238.md), and [yᵊqār](../../strongs/h/h3367.md), and [hăḏar](../../strongs/h/h1923.md):

<a name="daniel_5_19"></a>Daniel 5:19

And [min](../../strongs/h/h4481.md) the [rᵊḇû](../../strongs/h/h7238.md) that he [yᵊhaḇ](../../strongs/h/h3052.md) him, [kōl](../../strongs/h/h3606.md) [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md), [hăvâ](../../strongs/h/h1934.md) [zûaʿ](../../strongs/h/h2112.md) and [dᵊḥal](../../strongs/h/h1763.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md): whom he [hăvâ](../../strongs/h/h1934.md) [ṣᵊḇâ](../../strongs/h/h6634.md) he [hăvâ](../../strongs/h/h1934.md) [qᵊṭal](../../strongs/h/h6992.md); and whom he [hăvâ](../../strongs/h/h1934.md) [ṣᵊḇâ](../../strongs/h/h6634.md) he [hăvâ](../../strongs/h/h1934.md) [ḥăyā'](../../strongs/h/h2418.md); and whom he [hăvâ](../../strongs/h/h1934.md) [ṣᵊḇâ](../../strongs/h/h6634.md) he set [hăvâ](../../strongs/h/h1934.md) [rûm](../../strongs/h/h7313.md); and whom he [hăvâ](../../strongs/h/h1934.md) [ṣᵊḇâ](../../strongs/h/h6634.md) he put [hăvâ](../../strongs/h/h1934.md) [šᵊp̄al](../../strongs/h/h8214.md).

<a name="daniel_5_20"></a>Daniel 5:20

But when his [lᵊḇaḇ](../../strongs/h/h3825.md) was [rûm](../../strongs/h/h7313.md), and his [rûaḥ](../../strongs/h/h7308.md) [tᵊqēp̄](../../strongs/h/h8631.md) in [zûḏ](../../strongs/h/h2103.md), he was [nᵊḥaṯ](../../strongs/h/h5182.md) [min](../../strongs/h/h4481.md) his [malkû](../../strongs/h/h4437.md) [kārsē'](../../strongs/h/h3764.md), and they [ʿăḏā'](../../strongs/h/h5709.md) his [yᵊqār](../../strongs/h/h3367.md) from [min](../../strongs/h/h4481.md):

<a name="daniel_5_21"></a>Daniel 5:21

And he was [ṭᵊraḏ](../../strongs/h/h2957.md) [min](../../strongs/h/h4481.md) the [bēn](../../strongs/h/h1123.md) of ['ēneš](../../strongs/h/h606.md); and his [lᵊḇaḇ](../../strongs/h/h3825.md) was [šᵊvâ](../../strongs/h/h7739.md) [ʿim](../../strongs/h/h5974.md) the [ḥêvā'](../../strongs/h/h2423.md), and his [mᵊḏôr](../../strongs/h/h4070.md) was with the [ʿărāḏ](../../strongs/h/h6167.md): they [ṭᵊʿam](../../strongs/h/h2939.md) him with [ʿāš](../../strongs/h/h6211.md) like [tôr](../../strongs/h/h8450.md), and his [gešem](../../strongs/h/h1655.md) was [ṣᵊḇaʿ](../../strongs/h/h6647.md) with the [ṭal](../../strongs/h/h2920.md) of [šᵊmayin](../../strongs/h/h8065.md); [ʿaḏ](../../strongs/h/h5705.md) he [yᵊḏaʿ](../../strongs/h/h3046.md) that the [ʿillay](../../strongs/h/h5943.md) ['ĕlâ](../../strongs/h/h426.md) [šallîṭ](../../strongs/h/h7990.md) in the [malkû](../../strongs/h/h4437.md) of ['ēneš](../../strongs/h/h606.md), and that he [qûm](../../strongs/h/h6966.md) [ʿal](../../strongs/h/h5922.md) it [mān](../../strongs/h/h4479.md) he [ṣᵊḇâ](../../strongs/h/h6634.md).

<a name="daniel_5_22"></a>Daniel 5:22

And ['antâ](../../strongs/h/h607.md) his [bar](../../strongs/h/h1247.md), O [Bēlša'Ṣṣar](../../strongs/h/h1113.md), hast [lā'](../../strongs/h/h3809.md) [šᵊp̄al](../../strongs/h/h8214.md) thine [lᵊḇaḇ](../../strongs/h/h3825.md), [qᵊḇēl](../../strongs/h/h6903.md) thou [yᵊḏaʿ](../../strongs/h/h3046.md) [kōl](../../strongs/h/h3606.md) [dēn](../../strongs/h/h1836.md);

<a name="daniel_5_23"></a>Daniel 5:23

But hast [rûm](../../strongs/h/h7313.md) thyself [ʿal](../../strongs/h/h5922.md) the [mārē'](../../strongs/h/h4756.md) of [šᵊmayin](../../strongs/h/h8065.md); and they have ['ăṯâ](../../strongs/h/h858.md) the [mā'n](../../strongs/h/h3984.md) of his [bayiṯ](../../strongs/h/h1005.md) [qŏḏām](../../strongs/h/h6925.md) thee, and ['antâ](../../strongs/h/h607.md), and thy [raḇrᵊḇānîn](../../strongs/h/h7261.md), thy [šēḡāl](../../strongs/h/h7695.md), and thy [lᵊḥēnâ](../../strongs/h/h3904.md), have [šᵊṯâ](../../strongs/h/h8355.md) [ḥămar](../../strongs/h/h2562.md) in them; and thou hast [šᵊḇaḥ](../../strongs/h/h7624.md) the ['ĕlâ](../../strongs/h/h426.md) of [kᵊsap̄](../../strongs/h/h3702.md), and [dᵊhaḇ](../../strongs/h/h1722.md), of [nᵊḥāš](../../strongs/h/h5174.md), [parzel](../../strongs/h/h6523.md), ['āʿ](../../strongs/h/h636.md), and ['eḇen](../../strongs/h/h69.md), which [ḥăzā'](../../strongs/h/h2370.md) [lā'](../../strongs/h/h3809.md), [lā'](../../strongs/h/h3809.md) [šᵊmaʿ](../../strongs/h/h8086.md), [lā'](../../strongs/h/h3809.md) [yᵊḏaʿ](../../strongs/h/h3046.md): and the ['ĕlâ](../../strongs/h/h426.md) in whose [yaḏ](../../strongs/h/h3028.md) thy [nišmā'](../../strongs/h/h5396.md) is, and whose are [kōl](../../strongs/h/h3606.md) thy ['ōraḥ](../../strongs/h/h735.md), hast thou [lā'](../../strongs/h/h3809.md) [hăḏar](../../strongs/h/h1922.md):

<a name="daniel_5_24"></a>Daniel 5:24

['ĕḏayin](../../strongs/h/h116.md) was the [pas](../../strongs/h/h6447.md) of the [yaḏ](../../strongs/h/h3028.md) [šᵊlaḥ](../../strongs/h/h7972.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md); and [dēn](../../strongs/h/h1836.md) [kᵊṯāḇ](../../strongs/h/h3792.md) was [rᵊšam](../../strongs/h/h7560.md).

<a name="daniel_5_25"></a>Daniel 5:25

And [dēn](../../strongs/h/h1836.md) is the [kᵊṯāḇ](../../strongs/h/h3792.md) that was [rᵊšam](../../strongs/h/h7560.md), [mᵊnē'](../../strongs/h/h4484.md), [mᵊnē'](../../strongs/h/h4484.md), [tᵊqal](../../strongs/h/h8625.md), [pᵊras](../../strongs/h/h6537.md).

<a name="daniel_5_26"></a>Daniel 5:26

[dēn](../../strongs/h/h1836.md) is the [pᵊšar](../../strongs/h/h6591.md) of the [millâ](../../strongs/h/h4406.md): [mᵊnē'](../../strongs/h/h4484.md); ['ĕlâ](../../strongs/h/h426.md) hath [mᵊnā'](../../strongs/h/h4483.md) thy [malkû](../../strongs/h/h4437.md), and [šᵊlam](../../strongs/h/h8000.md) it.

<a name="daniel_5_27"></a>Daniel 5:27

[tᵊqal](../../strongs/h/h8625.md); Thou art [tᵊqal](../../strongs/h/h8625.md) in the [mō'znayin](../../strongs/h/h3977.md), and art [šᵊḵaḥ](../../strongs/h/h7912.md) [ḥassîr](../../strongs/h/h2627.md).

<a name="daniel_5_28"></a>Daniel 5:28

[pᵊras](../../strongs/h/h6537.md); Thy [malkû](../../strongs/h/h4437.md) is [pᵊras](../../strongs/h/h6537.md), and [yᵊhaḇ](../../strongs/h/h3052.md) to the [Māḏay](../../strongs/h/h4076.md) and [Pāras](../../strongs/h/h6540.md).

<a name="daniel_5_29"></a>Daniel 5:29

['ĕḏayin](../../strongs/h/h116.md) ['ămar](../../strongs/h/h560.md) [Bēlša'Ṣṣar](../../strongs/h/h1113.md), and they [lᵊḇaש](../../strongs/h/h3848.md) [Dānîyē'L](../../strongs/h/h1841.md) with ['arḡᵊvān](../../strongs/h/h711.md), and put a [hamnîḵ](../../strongs/h/h2002.md) of [dᵊhaḇ](../../strongs/h/h1722.md) [ʿal](../../strongs/h/h5922.md) his [ṣaûā'r](../../strongs/h/h6676.md), and made a [kᵊraz](../../strongs/h/h3745.md) [ʿal](../../strongs/h/h5922.md) him, that he should [hăvâ](../../strongs/h/h1934.md) the [tᵊlaṯ](../../strongs/h/h8531.md) [šallîṭ](../../strongs/h/h7990.md) in the [malkû](../../strongs/h/h4437.md).

<a name="daniel_5_30"></a>Daniel 5:30

In that [lêlyā'](../../strongs/h/h3916.md) was [Bēlša'Ṣṣar](../../strongs/h/h1113.md) the [meleḵ](../../strongs/h/h4430.md) of the [kaśday](../../strongs/h/h3779.md) [qᵊṭal](../../strongs/h/h6992.md).

<a name="daniel_5_31"></a>Daniel 5:31

And [Daryāveš](../../strongs/h/h1868.md) the [Māḏay](../../strongs/h/h4077.md) [qᵊḇal](../../strongs/h/h6902.md) the [malkû](../../strongs/h/h4437.md), being about [šitîn](../../strongs/h/h8361.md) and [tᵊrên](../../strongs/h/h8648.md) [šᵊnâ](../../strongs/h/h8140.md) [bar](../../strongs/h/h1247.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 4](daniel_4.md) - [Daniel 6](daniel_6.md)