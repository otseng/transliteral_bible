# [Daniel 3](https://www.blueletterbible.org/kjv/daniel/3)

<a name="daniel_3_1"></a>Daniel 3:1

[Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) [ʿăḇaḏ](../../strongs/h/h5648.md) an [ṣelem](../../strongs/h/h6755.md) of [dᵊhaḇ](../../strongs/h/h1722.md), whose [rûm](../../strongs/h/h7314.md) was [šitîn](../../strongs/h/h8361.md) ['ammâ](../../strongs/h/h521.md), and the [pᵊṯay](../../strongs/h/h6613.md) thereof [šēṯ](../../strongs/h/h8353.md) ['ammâ](../../strongs/h/h521.md): he set it [qûm](../../strongs/h/h6966.md) in the [biqʿā'](../../strongs/h/h1236.md) of [Dûrā'](../../strongs/h/h1757.md), in the [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md).

<a name="daniel_3_2"></a>Daniel 3:2

Then [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) [šᵊlaḥ](../../strongs/h/h7972.md) to [kᵊnaš](../../strongs/h/h3673.md) the ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), the [sᵊḡan](../../strongs/h/h5460.md), and the [peḥâ](../../strongs/h/h6347.md), the ['ăḏargāzar](../../strongs/h/h148.md), the [gᵊḏāḇrayyā'](../../strongs/h/h1411.md), the [dᵊṯāḇār](../../strongs/h/h1884.md), the [tip̄tāyē'](../../strongs/h/h8614.md), and [kōl](../../strongs/h/h3606.md) the [šilṭōn](../../strongs/h/h7984.md) of the [mᵊḏînâ](../../strongs/h/h4083.md), to ['ăṯâ](../../strongs/h/h858.md) to the [ḥănukā'](../../strongs/h/h2597.md) of the [ṣelem](../../strongs/h/h6755.md) which [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) had [qûm](../../strongs/h/h6966.md).

<a name="daniel_3_3"></a>Daniel 3:3

['ĕḏayin](../../strongs/h/h116.md) the ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), the [sᵊḡan](../../strongs/h/h5460.md), and [peḥâ](../../strongs/h/h6347.md), the ['ăḏargāzar](../../strongs/h/h148.md), the [gᵊḏāḇrayyā'](../../strongs/h/h1411.md), the [dᵊṯāḇār](../../strongs/h/h1884.md), the [tip̄tāyē'](../../strongs/h/h8614.md), and [kōl](../../strongs/h/h3606.md) the [šilṭōn](../../strongs/h/h7984.md) of the [mᵊḏînâ](../../strongs/h/h4083.md), were [kᵊnaš](../../strongs/h/h3673.md) unto the [ḥănukā'](../../strongs/h/h2597.md) of the [ṣelem](../../strongs/h/h6755.md) that [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) had set [qûm](../../strongs/h/h6966.md); and they [qûm](../../strongs/h/h6966.md) [qᵊḇēl](../../strongs/h/h6903.md) the [ṣelem](../../strongs/h/h6755.md) that [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) had [qûm](../../strongs/h/h6966.md).

<a name="daniel_3_4"></a>Daniel 3:4

Then a [kārôz](../../strongs/h/h3744.md) [qᵊrā'](../../strongs/h/h7123.md) [ḥayil](../../strongs/h/h2429.md), To you it is ['ămar](../../strongs/h/h560.md), O [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md),

<a name="daniel_3_5"></a>Daniel 3:5

That at what [ʿidān](../../strongs/h/h5732.md) ye [šᵊmaʿ](../../strongs/h/h8086.md) the [qāl](../../strongs/h/h7032.md) of the [qeren](../../strongs/h/h7162.md), [mašrôqî](../../strongs/h/h4953.md), [qîṯārōs](../../strongs/h/h7030.md) [qîṯārōs](../../strongs/h/h7030.md), [sabḵā'](../../strongs/h/h5443.md), [pᵊsanṯērîn](../../strongs/h/h6460.md), [sûmpōnyâ](../../strongs/h/h5481.md), and [kōl](../../strongs/h/h3606.md) [zan](../../strongs/h/h2178.md) of [zᵊmār](../../strongs/h/h2170.md), ye [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md) that [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) hath [qûm](../../strongs/h/h6966.md):

<a name="daniel_3_6"></a>Daniel 3:6

And [mān](../../strongs/h/h4479.md) [nᵊp̄al](../../strongs/h/h5308.md) [lā'](../../strongs/h/h3809.md) [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md) shall the same [šāʿâ](../../strongs/h/h8160.md) be [rᵊmâ](../../strongs/h/h7412.md) into the [gav](../../strongs/h/h1459.md) of a [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md).

<a name="daniel_3_7"></a>Daniel 3:7

[qᵊḇēl](../../strongs/h/h6903.md) [dēn](../../strongs/h/h1836.md) at that [zᵊmān](../../strongs/h/h2166.md), [dî](../../strongs/h/h1768.md) [kōl](../../strongs/h/h3606.md) the [ʿam](../../strongs/h/h5972.md) [šᵊmaʿ](../../strongs/h/h8086.md) the [qāl](../../strongs/h/h7032.md) of the [qeren](../../strongs/h/h7162.md), [mašrôqî](../../strongs/h/h4953.md), [qîṯārōs](../../strongs/h/h7030.md) [qîṯārōs](../../strongs/h/h7030.md), [sabḵā'](../../strongs/h/h5443.md), [pᵊsanṯērîn](../../strongs/h/h6460.md), and [kōl](../../strongs/h/h3606.md) [zan](../../strongs/h/h2178.md) of [zᵊmār](../../strongs/h/h2170.md), [kōl](../../strongs/h/h3606.md) the [ʿam](../../strongs/h/h5972.md), the ['ummâ](../../strongs/h/h524.md), and the [liššān](../../strongs/h/h3961.md), [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md) that [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) had [qûm](../../strongs/h/h6966.md).

<a name="daniel_3_8"></a>Daniel 3:8

[qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) at [dēn](../../strongs/h/h1836.md) [zᵊmān](../../strongs/h/h2166.md) [gᵊḇar](../../strongs/h/h1400.md) [kaśday](../../strongs/h/h3779.md) came [qᵊrēḇ](../../strongs/h/h7127.md), and [qᵊraṣ](../../strongs/h/h7170.md) ['ăḵal](../../strongs/h/h399.md) the [yᵊhûḏay](../../strongs/h/h3062.md).

<a name="daniel_3_9"></a>Daniel 3:9

They [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to the [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md), O [meleḵ](../../strongs/h/h4430.md), [ḥăyā'](../../strongs/h/h2418.md) for [ʿālam](../../strongs/h/h5957.md).

<a name="daniel_3_10"></a>Daniel 3:10

['antâ](../../strongs/h/h607.md), O [meleḵ](../../strongs/h/h4430.md), hast [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), that [kōl](../../strongs/h/h3606.md) ['ēneš](../../strongs/h/h606.md) that shall [šᵊmaʿ](../../strongs/h/h8086.md) the [qāl](../../strongs/h/h7032.md) of the [qeren](../../strongs/h/h7162.md), [mašrôqî](../../strongs/h/h4953.md), [qîṯārōs](../../strongs/h/h7030.md) [qîṯārōs](../../strongs/h/h7030.md), [sabḵā'](../../strongs/h/h5443.md), [pᵊsanṯērîn](../../strongs/h/h6460.md), and [sûmpōnyâ](../../strongs/h/h5481.md) [sûmpōnyâ](../../strongs/h/h5481.md), and [kōl](../../strongs/h/h3606.md) [zan](../../strongs/h/h2178.md) of [zᵊmār](../../strongs/h/h2170.md), shall [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md):

<a name="daniel_3_11"></a>Daniel 3:11

And [mān](../../strongs/h/h4479.md) [nᵊp̄al](../../strongs/h/h5308.md) [lā'](../../strongs/h/h3809.md) [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md), that he should be [rᵊmâ](../../strongs/h/h7412.md) into the [gav](../../strongs/h/h1459.md) of a [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md).

<a name="daniel_3_12"></a>Daniel 3:12

There ['îṯay](../../strongs/h/h383.md) [gᵊḇar](../../strongs/h/h1400.md) [yᵊhûḏay](../../strongs/h/h3062.md) [yaṯ](../../strongs/h/h3487.md) thou hast [mᵊnā'](../../strongs/h/h4483.md) [ʿal](../../strongs/h/h5922.md) the [ʿăḇîḏâ](../../strongs/h/h5673.md) of the [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md), [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md); ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md), O [meleḵ](../../strongs/h/h4430.md), [śûm](../../strongs/h/h7761.md) [lā'](../../strongs/h/h3809.md) [ṭᵊʿēm](../../strongs/h/h2942.md) [ʿal](../../strongs/h/h5922.md): they [pᵊlaḥ](../../strongs/h/h6399.md) [lā'](../../strongs/h/h3809.md) thy ['ĕlâ](../../strongs/h/h426.md), [lā'](../../strongs/h/h3809.md) [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md) which thou hast [qûm](../../strongs/h/h6966.md).

<a name="daniel_3_13"></a>Daniel 3:13

['ĕḏayin](../../strongs/h/h116.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) in his [rᵊḡaz](../../strongs/h/h7266.md) and [ḥĕmā'](../../strongs/h/h2528.md) ['ămar](../../strongs/h/h560.md) to ['ăṯâ](../../strongs/h/h858.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md). ['ĕḏayin](../../strongs/h/h116.md) they ['ăṯâ](../../strongs/h/h858.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md).

<a name="daniel_3_14"></a>Daniel 3:14

[Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) unto them, Is it [ṣᵊḏā'](../../strongs/h/h6656.md), O [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), do [lā'](../../strongs/h/h3809.md) ['îṯay](../../strongs/h/h383.md) [pᵊlaḥ](../../strongs/h/h6399.md) my ['ĕlâ](../../strongs/h/h426.md), [lā'](../../strongs/h/h3809.md) [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md) which I have [qûm](../../strongs/h/h6966.md)?

<a name="daniel_3_15"></a>Daniel 3:15

[kᵊʿan](../../strongs/h/h3705.md) [hēn](../../strongs/h/h2006.md) ye ['îṯay](../../strongs/h/h383.md) [ʿăṯîḏ](../../strongs/h/h6263.md) that at what [ʿidān](../../strongs/h/h5732.md) ye [šᵊmaʿ](../../strongs/h/h8086.md) the [qāl](../../strongs/h/h7032.md) of the [qeren](../../strongs/h/h7162.md), [mašrôqî](../../strongs/h/h4953.md), [qîṯārōs](../../strongs/h/h7030.md) [qîṯārōs](../../strongs/h/h7030.md), [sabḵā'](../../strongs/h/h5443.md), [pᵊsanṯērîn](../../strongs/h/h6460.md), and [sûmpōnyâ](../../strongs/h/h5481.md), and [kōl](../../strongs/h/h3606.md) [zan](../../strongs/h/h2178.md) of [zᵊmār](../../strongs/h/h2170.md), ye fall [nᵊp̄al](../../strongs/h/h5308.md) and [sᵊḡiḏ](../../strongs/h/h5457.md) the [ṣelem](../../strongs/h/h6755.md) which I have [ʿăḇaḏ](../../strongs/h/h5648.md); well: but [hēn](../../strongs/h/h2006.md) ye [sᵊḡiḏ](../../strongs/h/h5457.md) [lā'](../../strongs/h/h3809.md), ye shall be [rᵊmâ](../../strongs/h/h7412.md) the same [šāʿâ](../../strongs/h/h8160.md) into the [gav](../../strongs/h/h1459.md) of a [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md); and [mān](../../strongs/h/h4479.md) is that ['ĕlâ](../../strongs/h/h426.md) that shall [šêziḇ](../../strongs/h/h7804.md) you out [min](../../strongs/h/h4481.md) my [yaḏ](../../strongs/h/h3028.md)?

<a name="daniel_3_16"></a>Daniel 3:16

[Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to the [meleḵ](../../strongs/h/h4430.md), O [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md), ['ănaḥnā'](../../strongs/h/h586.md) are [lā'](../../strongs/h/h3809.md) [ḥăšaḥ](../../strongs/h/h2818.md) to [tûḇ](../../strongs/h/h8421.md) thee [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) [piṯgām](../../strongs/h/h6600.md).

<a name="daniel_3_17"></a>Daniel 3:17

[hēn](../../strongs/h/h2006.md) it be so, our ['ĕlâ](../../strongs/h/h426.md) whom ['ănaḥnā'](../../strongs/h/h586.md) [pᵊlaḥ](../../strongs/h/h6399.md) ['îṯay](../../strongs/h/h383.md) [yᵊḵēl](../../strongs/h/h3202.md) to [šêziḇ](../../strongs/h/h7804.md) us [min](../../strongs/h/h4481.md) the [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md), [min](../../strongs/h/h4481.md) he will [šêziḇ](../../strongs/h/h7804.md) us out [min](../../strongs/h/h4481.md) thine [yaḏ](../../strongs/h/h3028.md), O [meleḵ](../../strongs/h/h4430.md).

<a name="daniel_3_18"></a>Daniel 3:18

But [hēn](../../strongs/h/h2006.md) [lā'](../../strongs/h/h3809.md), be [hăvâ](../../strongs/h/h1934.md) [yᵊḏaʿ](../../strongs/h/h3046.md) unto thee, O [meleḵ](../../strongs/h/h4430.md), that we ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) [pᵊlaḥ](../../strongs/h/h6399.md) thy ['ĕlâ](../../strongs/h/h426.md), [lā'](../../strongs/h/h3809.md) [sᵊḡiḏ](../../strongs/h/h5457.md) the [dᵊhaḇ](../../strongs/h/h1722.md) [ṣelem](../../strongs/h/h6755.md) which thou hast [qûm](../../strongs/h/h6966.md).

<a name="daniel_3_19"></a>Daniel 3:19

['ĕḏayin](../../strongs/h/h116.md) was [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [mᵊlā'](../../strongs/h/h4391.md) of [ḥĕmā'](../../strongs/h/h2528.md), and the [ṣelem](../../strongs/h/h6755.md) of his ['ănap̄](../../strongs/h/h600.md) was [šᵊnâ](../../strongs/h/h8133.md) [ʿal](../../strongs/h/h5922.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md): therefore he [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md) that they should ['ăzā'](../../strongs/h/h228.md) the ['atûn](../../strongs/h/h861.md) [ḥaḏ](../../strongs/h/h2298.md) [šᵊḇaʿ](../../strongs/h/h7655.md) [ʿal](../../strongs/h/h5922.md) [dî](../../strongs/h/h1768.md) it was [ḥăzā'](../../strongs/h/h2370.md) to be ['ăzā'](../../strongs/h/h228.md).

<a name="daniel_3_20"></a>Daniel 3:20

And he ['ămar](../../strongs/h/h560.md) the [ḥayil](../../strongs/h/h2429.md) [gibār](../../strongs/h/h1401.md) [gᵊḇar](../../strongs/h/h1400.md) that were in his [ḥayil](../../strongs/h/h2429.md) to [kᵊp̄aṯ](../../strongs/h/h3729.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), and to [rᵊmâ](../../strongs/h/h7412.md) them into the [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md).

<a name="daniel_3_21"></a>Daniel 3:21

['ĕḏayin](../../strongs/h/h116.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) were [kᵊp̄aṯ](../../strongs/h/h3729.md) in their [sarḇal](../../strongs/h/h5622.md), their [paṭṭîš](../../strongs/h/h6361.md) [paṭṭîš](../../strongs/h/h6361.md), and their [karbᵊlā'](../../strongs/h/h3737.md), and their other [lᵊḇûš](../../strongs/h/h3831.md), and were [rᵊmâ](../../strongs/h/h7412.md) into the [gav](../../strongs/h/h1459.md) of the [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md).

<a name="daniel_3_22"></a>Daniel 3:22

[kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) [min](../../strongs/h/h4481.md) [dēn](../../strongs/h/h1836.md) the [meleḵ](../../strongs/h/h4430.md) [millâ](../../strongs/h/h4406.md) was [ḥăṣap̄](../../strongs/h/h2685.md), and the ['atûn](../../strongs/h/h861.md) [yatîr](../../strongs/h/h3493.md) ['ăzā'](../../strongs/h/h228.md), the [šᵊḇîḇ](../../strongs/h/h7631.md) of the [nûr](../../strongs/h/h5135.md) [qᵊṭal](../../strongs/h/h6992.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) [himmô](../../strongs/h/h1994.md) that [nᵊsaq](../../strongs/h/h5267.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md).

<a name="daniel_3_23"></a>Daniel 3:23

And ['illēḵ](../../strongs/h/h479.md) [tᵊlāṯ](../../strongs/h/h8532.md) [gᵊḇar](../../strongs/h/h1400.md), [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), fell [nᵊp̄al](../../strongs/h/h5308.md) [kᵊp̄aṯ](../../strongs/h/h3729.md) into the [gav](../../strongs/h/h1459.md) of the [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md).

<a name="daniel_3_24"></a>Daniel 3:24

['ĕḏayin](../../strongs/h/h116.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md) was [tᵊvah](../../strongs/h/h8429.md), and rose [qûm](../../strongs/h/h6966.md) in [bᵊhal](../../strongs/h/h927.md), and [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md) unto his [hadāḇar](../../strongs/h/h1907.md), Did [lā'](../../strongs/h/h3809.md) we [rᵊmâ](../../strongs/h/h7412.md) [tᵊlāṯ](../../strongs/h/h8532.md) [gᵊḇar](../../strongs/h/h1400.md) [kᵊp̄aṯ](../../strongs/h/h3729.md) into the [gav](../../strongs/h/h1459.md) of the [nûr](../../strongs/h/h5135.md)? They [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) unto the [meleḵ](../../strongs/h/h4430.md), [yaṣṣîḇ](../../strongs/h/h3330.md), O [meleḵ](../../strongs/h/h4430.md).

<a name="daniel_3_25"></a>Daniel 3:25

He [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), [hā'](../../strongs/h/h1888.md), ['ănā'](../../strongs/h/h576.md) [ḥăzā'](../../strongs/h/h2370.md) ['arbaʿ](../../strongs/h/h703.md) [gᵊḇar](../../strongs/h/h1400.md) [śᵊrâ](../../strongs/h/h8271.md), [hălaḵ](../../strongs/h/h1981.md) in the [gav](../../strongs/h/h1459.md) of the [nûr](../../strongs/h/h5135.md), and they ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) [ḥăḇal](../../strongs/h/h2257.md); and the [rēv](../../strongs/h/h7299.md) of the [rᵊḇîʿay](../../strongs/h/h7244.md) is [dᵊmâ](../../strongs/h/h1821.md) the [bar](../../strongs/h/h1247.md) of ['ĕlâ](../../strongs/h/h426.md).

<a name="daniel_3_26"></a>Daniel 3:26

['ĕḏayin](../../strongs/h/h116.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [qᵊrēḇ](../../strongs/h/h7127.md) to the [tᵊraʿ](../../strongs/h/h8651.md) of the [yᵊqaḏ](../../strongs/h/h3345.md) [nûr](../../strongs/h/h5135.md) ['atûn](../../strongs/h/h861.md), and [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md), [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), ye [ʿăḇaḏ](../../strongs/h/h5649.md) of the [ʿillay](../../strongs/h/h5943.md) ['ĕlâ](../../strongs/h/h426.md), [nᵊp̄aq](../../strongs/h/h5312.md), and ['ăṯâ](../../strongs/h/h858.md). ['ĕḏayin](../../strongs/h/h116.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), [nᵊp̄aq](../../strongs/h/h5312.md) [min](../../strongs/h/h4481.md) the [gav](../../strongs/h/h1459.md) of the [nûr](../../strongs/h/h5135.md).

<a name="daniel_3_27"></a>Daniel 3:27

And the ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), [sᵊḡan](../../strongs/h/h5460.md), and [peḥâ](../../strongs/h/h6347.md), and the [meleḵ](../../strongs/h/h4430.md) [hadāḇar](../../strongs/h/h1907.md), being [kᵊnaš](../../strongs/h/h3673.md), [ḥăzā'](../../strongs/h/h2370.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md), upon whose [gešem](../../strongs/h/h1655.md) the [nûr](../../strongs/h/h5135.md) had [lā'](../../strongs/h/h3809.md) [šᵊlēṭ](../../strongs/h/h7981.md), [lā'](../../strongs/h/h3809.md) was an [śᵊʿar](../../strongs/h/h8177.md) of their [rē'š](../../strongs/h/h7217.md) [ḥăraḵ](../../strongs/h/h2761.md), [lā'](../../strongs/h/h3809.md) were their [sarḇal](../../strongs/h/h5622.md) [šᵊnâ](../../strongs/h/h8133.md), [lā'](../../strongs/h/h3809.md) the [rêaḥ](../../strongs/h/h7382.md) of [nûr](../../strongs/h/h5135.md) had [ʿăḏā'](../../strongs/h/h5709.md) on them.

<a name="daniel_3_28"></a>Daniel 3:28

Then [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md), [bᵊraḵ](../../strongs/h/h1289.md) be the ['ĕlâ](../../strongs/h/h426.md) of [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), who hath [šᵊlaḥ](../../strongs/h/h7972.md) his [mal'aḵ](../../strongs/h/h4398.md), and [šêziḇ](../../strongs/h/h7804.md) his [ʿăḇaḏ](../../strongs/h/h5649.md) that [rᵊḥaṣ](../../strongs/h/h7365.md) in [ʿal](../../strongs/h/h5922.md), and have [šᵊnâ](../../strongs/h/h8133.md) the [meleḵ](../../strongs/h/h4430.md) [millâ](../../strongs/h/h4406.md), and [yᵊhaḇ](../../strongs/h/h3052.md) their [gešem](../../strongs/h/h1655.md), that they might [lā'](../../strongs/h/h3809.md) [pᵊlaḥ](../../strongs/h/h6399.md) [lā'](../../strongs/h/h3809.md) [sᵊḡiḏ](../../strongs/h/h5457.md) [kōl](../../strongs/h/h3606.md) ['ĕlâ](../../strongs/h/h426.md), [lāhēn](../../strongs/h/h3861.md) their own ['ĕlâ](../../strongs/h/h426.md).

<a name="daniel_3_29"></a>Daniel 3:29

Therefore [min](../../strongs/h/h4481.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), That [kōl](../../strongs/h/h3606.md) [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md), which ['ămar](../../strongs/h/h560.md) any thing [šālû](../../strongs/h/h7960.md) [šālâ](../../strongs/h/h7955.md) [ʿal](../../strongs/h/h5922.md) the ['ĕlâ](../../strongs/h/h426.md) of [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), shall be [ʿăḇaḏ](../../strongs/h/h5648.md) in [hadām](../../strongs/h/h1917.md), and their [bayiṯ](../../strongs/h/h1005.md) shall be [šᵊvâ](../../strongs/h/h7739.md) a [nᵊvālû](../../strongs/h/h5122.md): [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) there ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) ['āḥŏrān](../../strongs/h/h321.md) ['ĕlâ](../../strongs/h/h426.md) that [yᵊḵēl](../../strongs/h/h3202.md) [nᵊṣal](../../strongs/h/h5338.md) after [dēn](../../strongs/h/h1836.md) sort.

<a name="daniel_3_30"></a>Daniel 3:30

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) [ṣᵊlaḥ](../../strongs/h/h6744.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), in the [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 2](daniel_2.md) - [Daniel 4](daniel_4.md)