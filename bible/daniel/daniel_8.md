# [Daniel 8](https://www.blueletterbible.org/kjv/daniel/8)

<a name="daniel_8_1"></a>Daniel 8:1

In the third [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of [melek](../../strongs/h/h4428.md) [Bēlša'Ṣṣar](../../strongs/h/h1112.md) a [ḥāzôn](../../strongs/h/h2377.md) [ra'ah](../../strongs/h/h7200.md) unto me, even unto me [Dinîyē'L](../../strongs/h/h1840.md), ['aḥar](../../strongs/h/h310.md) that which [ra'ah](../../strongs/h/h7200.md) unto me at the [tᵊḥillâ](../../strongs/h/h8462.md).

<a name="daniel_8_2"></a>Daniel 8:2

And I [ra'ah](../../strongs/h/h7200.md) in a [ḥāzôn](../../strongs/h/h2377.md); and it came to pass, when I [ra'ah](../../strongs/h/h7200.md), that I was at [Šûšan](../../strongs/h/h7800.md) in the [bîrâ](../../strongs/h/h1002.md), which is in the [mᵊḏînâ](../../strongs/h/h4082.md) of [ʿÊlām](../../strongs/h/h5867.md); and I [ra'ah](../../strongs/h/h7200.md) in a [ḥāzôn](../../strongs/h/h2377.md), and I was by the ['ûḇāl](../../strongs/h/h180.md) of ['Ûlay](../../strongs/h/h195.md).

<a name="daniel_8_3"></a>Daniel 8:3

Then I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and, behold, there ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['ûḇāl](../../strongs/h/h180.md) an ['ayil](../../strongs/h/h352.md) which had two [qeren](../../strongs/h/h7161.md): and the two [qeren](../../strongs/h/h7161.md) were [gāḇōha](../../strongs/h/h1364.md); but one was [gāḇōha](../../strongs/h/h1364.md) than the other, and the [gāḇōha](../../strongs/h/h1364.md) [ʿālâ](../../strongs/h/h5927.md) ['aḥărôn](../../strongs/h/h314.md).

<a name="daniel_8_4"></a>Daniel 8:4

I [ra'ah](../../strongs/h/h7200.md) the ['ayil](../../strongs/h/h352.md) [nāḡaḥ](../../strongs/h/h5055.md) [yam](../../strongs/h/h3220.md), and [ṣāp̄ôn](../../strongs/h/h6828.md), and [neḡeḇ](../../strongs/h/h5045.md); so that no [chay](../../strongs/h/h2416.md) might ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him, neither was there any that could [natsal](../../strongs/h/h5337.md) out of his [yad](../../strongs/h/h3027.md); but he ['asah](../../strongs/h/h6213.md) according to his [ratsown](../../strongs/h/h7522.md), and became [gāḏal](../../strongs/h/h1431.md).

<a name="daniel_8_5"></a>Daniel 8:5

And as I was [bîn](../../strongs/h/h995.md), behold, an [ʿēz](../../strongs/h/h5795.md) [ṣāp̄îr](../../strongs/h/h6842.md) [bow'](../../strongs/h/h935.md) from the [ma'arab](../../strongs/h/h4628.md) on the [paniym](../../strongs/h/h6440.md) of the ['erets](../../strongs/h/h776.md), and [naga'](../../strongs/h/h5060.md) not the ['erets](../../strongs/h/h776.md): and the [ṣāp̄îr](../../strongs/h/h6842.md) had a [ḥāzûṯ](../../strongs/h/h2380.md) [qeren](../../strongs/h/h7161.md) between his ['ayin](../../strongs/h/h5869.md).

<a name="daniel_8_6"></a>Daniel 8:6

And he [bow'](../../strongs/h/h935.md) to the ['ayil](../../strongs/h/h352.md) that [baʿal](../../strongs/h/h1167.md) two [qeren](../../strongs/h/h7161.md), which I had [ra'ah](../../strongs/h/h7200.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the ['ûḇāl](../../strongs/h/h180.md), and [rûṣ](../../strongs/h/h7323.md) unto him in the [chemah](../../strongs/h/h2534.md) of his [koach](../../strongs/h/h3581.md).

<a name="daniel_8_7"></a>Daniel 8:7

And I [ra'ah](../../strongs/h/h7200.md) him [naga'](../../strongs/h/h5060.md) close unto the ['ayil](../../strongs/h/h352.md), and he was moved with [mārar](../../strongs/h/h4843.md) against him, and [nakah](../../strongs/h/h5221.md) the ['ayil](../../strongs/h/h352.md), and [shabar](../../strongs/h/h7665.md) his two [qeren](../../strongs/h/h7161.md): and there was no [koach](../../strongs/h/h3581.md) in the ['ayil](../../strongs/h/h352.md) to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him, but he [shalak](../../strongs/h/h7993.md) him to the ['erets](../../strongs/h/h776.md), and [rāmas](../../strongs/h/h7429.md) upon him: and there was none that could [natsal](../../strongs/h/h5337.md) the ['ayil](../../strongs/h/h352.md) out of his [yad](../../strongs/h/h3027.md).

<a name="daniel_8_8"></a>Daniel 8:8

Therefore the [ṣāp̄îr](../../strongs/h/h6842.md) [ʿēz](../../strongs/h/h5795.md) waxed [me'od](../../strongs/h/h3966.md) [gāḏal](../../strongs/h/h1431.md): and when he was [ʿāṣam](../../strongs/h/h6105.md), the [gadowl](../../strongs/h/h1419.md) [qeren](../../strongs/h/h7161.md) was [shabar](../../strongs/h/h7665.md); and for it [ʿālâ](../../strongs/h/h5927.md) four notable [ḥāzûṯ](../../strongs/h/h2380.md) toward the four [ruwach](../../strongs/h/h7307.md) of [shamayim](../../strongs/h/h8064.md).

<a name="daniel_8_9"></a>Daniel 8:9

And out of one of them [yāṣā'](../../strongs/h/h3318.md) a [miṣṣᵊʿîrâ](../../strongs/h/h4704.md) [qeren](../../strongs/h/h7161.md), which [yeṯer](../../strongs/h/h3499.md) [gāḏal](../../strongs/h/h1431.md), toward the [neḡeḇ](../../strongs/h/h5045.md), and toward the [mizrach](../../strongs/h/h4217.md), and toward the [ṣᵊḇî](../../strongs/h/h6643.md) land.

<a name="daniel_8_10"></a>Daniel 8:10

And it [gāḏal](../../strongs/h/h1431.md), even to the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md); and it [naphal](../../strongs/h/h5307.md) some of the [tsaba'](../../strongs/h/h6635.md) and of the [kowkab](../../strongs/h/h3556.md) to the ['erets](../../strongs/h/h776.md), and [rāmas](../../strongs/h/h7429.md) upon them.

<a name="daniel_8_11"></a>Daniel 8:11

Yea, he [gāḏal](../../strongs/h/h1431.md) himself even to the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md), and by him the [tāmîḏ](../../strongs/h/h8548.md) was [ruwm](../../strongs/h/h7311.md) [ruwm](../../strongs/h/h7311.md), and the [māḵôn](../../strongs/h/h4349.md) of his [miqdash](../../strongs/h/h4720.md) was [shalak](../../strongs/h/h7993.md).

<a name="daniel_8_12"></a>Daniel 8:12

And a [tsaba'](../../strongs/h/h6635.md) was [nathan](../../strongs/h/h5414.md) him against the [tāmîḏ](../../strongs/h/h8548.md) by reason of [pesha'](../../strongs/h/h6588.md), and it [shalak](../../strongs/h/h7993.md) the ['emeth](../../strongs/h/h571.md) to the ['erets](../../strongs/h/h776.md); and it ['asah](../../strongs/h/h6213.md), and [tsalach](../../strongs/h/h6743.md).

<a name="daniel_8_13"></a>Daniel 8:13

Then I [shama'](../../strongs/h/h8085.md) one [qadowsh](../../strongs/h/h6918.md) [dabar](../../strongs/h/h1696.md), and another [qadowsh](../../strongs/h/h6918.md) ['āmar](../../strongs/h/h559.md) unto that [palmônî](../../strongs/h/h6422.md) which [dabar](../../strongs/h/h1696.md), How long shall be the [ḥāzôn](../../strongs/h/h2377.md) concerning the [tāmîḏ](../../strongs/h/h8548.md) sacrifice, and the [pesha'](../../strongs/h/h6588.md) of [šāmēm](../../strongs/h/h8074.md), to [nathan](../../strongs/h/h5414.md) both the [qodesh](../../strongs/h/h6944.md) and the [tsaba'](../../strongs/h/h6635.md) to be trodden under [mirmās](../../strongs/h/h4823.md)?

<a name="daniel_8_14"></a>Daniel 8:14

And he ['āmar](../../strongs/h/h559.md) unto me, Unto two thousand and three hundred ['ereb](../../strongs/h/h6153.md) [boqer](../../strongs/h/h1242.md); then shall the [qodesh](../../strongs/h/h6944.md) be [ṣāḏaq](../../strongs/h/h6663.md).

<a name="daniel_8_15"></a>Daniel 8:15

And it came to pass, when I, even I [Dinîyē'L](../../strongs/h/h1840.md), had [ra'ah](../../strongs/h/h7200.md) the [ḥāzôn](../../strongs/h/h2377.md), and [bāqaš](../../strongs/h/h1245.md) for the [bînâ](../../strongs/h/h998.md), then, behold, there ['amad](../../strongs/h/h5975.md) before me as the [mar'ê](../../strongs/h/h4758.md) of a [geḇer](../../strongs/h/h1397.md).

<a name="daniel_8_16"></a>Daniel 8:16

And I [shama'](../../strongs/h/h8085.md) an ['āḏām](../../strongs/h/h120.md) [qowl](../../strongs/h/h6963.md) between ['Ûlay](../../strongs/h/h195.md), which [qara'](../../strongs/h/h7121.md), and ['āmar](../../strongs/h/h559.md), [Gaḇrî'Ēl](../../strongs/h/h1403.md), make [hallāz](../../strongs/h/h1975.md) to [bîn](../../strongs/h/h995.md) the [mar'ê](../../strongs/h/h4758.md).

<a name="daniel_8_17"></a>Daniel 8:17

So he [bow'](../../strongs/h/h935.md) near where I [ʿōmeḏ](../../strongs/h/h5977.md): and when he [bow'](../../strongs/h/h935.md), I was [ba'ath](../../strongs/h/h1204.md), and [naphal](../../strongs/h/h5307.md) upon my [paniym](../../strongs/h/h6440.md): but he ['āmar](../../strongs/h/h559.md) unto me, [bîn](../../strongs/h/h995.md), O [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md): for at the [ʿēṯ](../../strongs/h/h6256.md) of the [qēṣ](../../strongs/h/h7093.md) shall be the [ḥāzôn](../../strongs/h/h2377.md).

<a name="daniel_8_18"></a>Daniel 8:18

Now as he was [dabar](../../strongs/h/h1696.md) with me, I was in a [rāḏam](../../strongs/h/h7290.md) on my [paniym](../../strongs/h/h6440.md) toward the ['erets](../../strongs/h/h776.md): but he [naga'](../../strongs/h/h5060.md) me, and ['amad](../../strongs/h/h5975.md) me [ʿōmeḏ](../../strongs/h/h5977.md).

<a name="daniel_8_19"></a>Daniel 8:19

And he ['āmar](../../strongs/h/h559.md), Behold, I will make thee [yada'](../../strongs/h/h3045.md) what shall be in the last ['aḥărîṯ](../../strongs/h/h319.md) of the [zaʿam](../../strongs/h/h2195.md): for at the [môʿēḏ](../../strongs/h/h4150.md) the [qēṣ](../../strongs/h/h7093.md) shall be.

<a name="daniel_8_20"></a>Daniel 8:20

The ['ayil](../../strongs/h/h352.md) which thou [ra'ah](../../strongs/h/h7200.md) [baʿal](../../strongs/h/h1167.md) two [qeren](../../strongs/h/h7161.md) are the [melek](../../strongs/h/h4428.md) of [Māḏay](../../strongs/h/h4074.md) and [Pāras](../../strongs/h/h6539.md).

<a name="daniel_8_21"></a>Daniel 8:21

And the [śāʿîr](../../strongs/h/h8163.md) [ṣāp̄îr](../../strongs/h/h6842.md) is the [melek](../../strongs/h/h4428.md) of [Yāvān](../../strongs/h/h3120.md): and the [gadowl](../../strongs/h/h1419.md) [qeren](../../strongs/h/h7161.md) that is between his ['ayin](../../strongs/h/h5869.md) is the [ri'šôn](../../strongs/h/h7223.md) [melek](../../strongs/h/h4428.md).

<a name="daniel_8_22"></a>Daniel 8:22

Now that being [shabar](../../strongs/h/h7665.md), whereas four ['amad](../../strongs/h/h5975.md) for it, four [malkuwth](../../strongs/h/h4438.md) shall ['amad](../../strongs/h/h5975.md) out of the [gowy](../../strongs/h/h1471.md), but not in his [koach](../../strongs/h/h3581.md).

<a name="daniel_8_23"></a>Daniel 8:23

And in the ['aḥărîṯ](../../strongs/h/h319.md) of their [malkuwth](../../strongs/h/h4438.md), when the [pāšaʿ](../../strongs/h/h6586.md) are come to the [tamam](../../strongs/h/h8552.md), a [melek](../../strongs/h/h4428.md) of ['az](../../strongs/h/h5794.md) [paniym](../../strongs/h/h6440.md), and [bîn](../../strongs/h/h995.md) [ḥîḏâ](../../strongs/h/h2420.md), shall ['amad](../../strongs/h/h5975.md).

<a name="daniel_8_24"></a>Daniel 8:24

And his [koach](../../strongs/h/h3581.md) shall be [ʿāṣam](../../strongs/h/h6105.md), but not by his own [koach](../../strongs/h/h3581.md): and he shall [shachath](../../strongs/h/h7843.md) [pala'](../../strongs/h/h6381.md), and shall [tsalach](../../strongs/h/h6743.md), and ['asah](../../strongs/h/h6213.md), and shall [shachath](../../strongs/h/h7843.md) the ['atsuwm](../../strongs/h/h6099.md) and the [qadowsh](../../strongs/h/h6918.md) ['am](../../strongs/h/h5971.md).

<a name="daniel_8_25"></a>Daniel 8:25

And through his [śēḵel](../../strongs/h/h7922.md) also he shall cause [mirmah](../../strongs/h/h4820.md) to [tsalach](../../strongs/h/h6743.md) in his [yad](../../strongs/h/h3027.md); and he shall [gāḏal](../../strongs/h/h1431.md) himself in his [lebab](../../strongs/h/h3824.md), and by [šalvâ](../../strongs/h/h7962.md) shall [shachath](../../strongs/h/h7843.md) [rab](../../strongs/h/h7227.md): he shall also ['amad](../../strongs/h/h5975.md) against the [śar](../../strongs/h/h8269.md) of [śar](../../strongs/h/h8269.md); but he shall be [shabar](../../strongs/h/h7665.md) ['ep̄es](../../strongs/h/h657.md) [yad](../../strongs/h/h3027.md).

<a name="daniel_8_26"></a>Daniel 8:26

And the [mar'ê](../../strongs/h/h4758.md) of the ['ereb](../../strongs/h/h6153.md) and the [boqer](../../strongs/h/h1242.md) which was ['āmar](../../strongs/h/h559.md) is ['emeth](../../strongs/h/h571.md): wherefore shut thou [sāṯam](../../strongs/h/h5640.md) the [ḥāzôn](../../strongs/h/h2377.md); for it shall be for [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="daniel_8_27"></a>Daniel 8:27

And I [Dinîyē'L](../../strongs/h/h1840.md) [hayah](../../strongs/h/h1961.md), and was [ḥālâ](../../strongs/h/h2470.md) certain [yowm](../../strongs/h/h3117.md); afterward I [quwm](../../strongs/h/h6965.md), and ['asah](../../strongs/h/h6213.md) the [melek](../../strongs/h/h4428.md) [mĕla'kah](../../strongs/h/h4399.md); and I was [šāmēm](../../strongs/h/h8074.md) at the [mar'ê](../../strongs/h/h4758.md), but none [bîn](../../strongs/h/h995.md) it.

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 7](daniel_7.md) - [Daniel 9](daniel_9.md)