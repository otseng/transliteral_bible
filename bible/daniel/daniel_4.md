# [Daniel 4](https://www.blueletterbible.org/kjv/daniel/4)

<a name="daniel_4_1"></a>Daniel 4:1

[Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) the [meleḵ](../../strongs/h/h4430.md), unto [kōl](../../strongs/h/h3606.md) [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md), that [dûr](../../strongs/h/h1753.md) in [kōl](../../strongs/h/h3606.md) the ['ăraʿ](../../strongs/h/h772.md); [šᵊlām](../../strongs/h/h8001.md) be [śᵊḡā'](../../strongs/h/h7680.md) unto you.

<a name="daniel_4_2"></a>Daniel 4:2

I [qŏḏām](../../strongs/h/h6925.md) it [šᵊp̄ar](../../strongs/h/h8232.md) to [ḥăvā'](../../strongs/h/h2324.md) the ['āṯ](../../strongs/h/h852.md) and [tᵊmah](../../strongs/h/h8540.md) that the [ʿillay](../../strongs/h/h5943.md) ['ĕlâ](../../strongs/h/h426.md) hath [ʿăḇaḏ](../../strongs/h/h5648.md) [ʿim](../../strongs/h/h5974.md) me.

<a name="daniel_4_3"></a>Daniel 4:3

[mâ](../../strongs/h/h4101.md) [raḇraḇ](../../strongs/h/h7260.md) are his ['āṯ](../../strongs/h/h852.md)! and [mâ](../../strongs/h/h4101.md) [taqqîp̄](../../strongs/h/h8624.md) are his [tᵊmah](../../strongs/h/h8540.md)! his [malkû](../../strongs/h/h4437.md) is an [ʿālam](../../strongs/h/h5957.md) [malkû](../../strongs/h/h4437.md), and his [šālṭān](../../strongs/h/h7985.md) is [ʿim](../../strongs/h/h5974.md) [dār](../../strongs/h/h1859.md) to [dār](../../strongs/h/h1859.md).

<a name="daniel_4_4"></a>Daniel 4:4

['ănā'](../../strongs/h/h576.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [hăvâ](../../strongs/h/h1934.md) at [šᵊlâ](../../strongs/h/h7954.md) in mine [bayiṯ](../../strongs/h/h1005.md), and [raʿănan](../../strongs/h/h7487.md) in my [hêḵal](../../strongs/h/h1965.md):

<a name="daniel_4_5"></a>Daniel 4:5

I [ḥăzā'](../../strongs/h/h2370.md) a [ḥēlem](../../strongs/h/h2493.md) which made me [dᵊḥal](../../strongs/h/h1763.md), and the [harhōr](../../strongs/h/h2031.md) [ʿal](../../strongs/h/h5922.md) my [miškāḇ](../../strongs/h/h4903.md) and the [ḥēzev](../../strongs/h/h2376.md) of my [rē'š](../../strongs/h/h7217.md) [bᵊhal](../../strongs/h/h927.md) me.

<a name="daniel_4_6"></a>Daniel 4:6

[min](../../strongs/h/h4481.md) [śûm](../../strongs/h/h7761.md) [min](../../strongs/h/h4481.md) a [ṭᵊʿēm](../../strongs/h/h2942.md) to [ʿălal](../../strongs/h/h5954.md) in [kōl](../../strongs/h/h3606.md) the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md) [qŏḏām](../../strongs/h/h6925.md) me, that they might make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [pᵊšar](../../strongs/h/h6591.md) of the [ḥēlem](../../strongs/h/h2493.md).

<a name="daniel_4_7"></a>Daniel 4:7

['ĕḏayin](../../strongs/h/h116.md) [ʿălal](../../strongs/h/h5954.md) in the [ḥarṭṭōm](../../strongs/h/h2749.md), the ['aššāp̄](../../strongs/h/h826.md), the [kaśday](../../strongs/h/h3779.md), and the [gᵊzar](../../strongs/h/h1505.md): and ['ănā'](../../strongs/h/h576.md) ['ămar](../../strongs/h/h560.md) the [ḥēlem](../../strongs/h/h2493.md) [qŏḏām](../../strongs/h/h6925.md) them; but they did [lā'](../../strongs/h/h3809.md) make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [pᵊšar](../../strongs/h/h6591.md) thereof.

<a name="daniel_4_8"></a>Daniel 4:8

But [ʿaḏ](../../strongs/h/h5705.md) the ['āḥŏrên](../../strongs/h/h318.md) [Dānîyē'L](../../strongs/h/h1841.md) came [ʿal](../../strongs/h/h5922.md) before [qŏḏām](../../strongs/h/h6925.md), whose [šum](../../strongs/h/h8036.md) was [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), according to the [šum](../../strongs/h/h8036.md) of my ['ĕlâ](../../strongs/h/h426.md), and in whom is the [rûaḥ](../../strongs/h/h7308.md) of the [qadîš](../../strongs/h/h6922.md) ['ĕlâ](../../strongs/h/h426.md): and [qŏḏām](../../strongs/h/h6925.md) him I ['ămar](../../strongs/h/h560.md) the [ḥēlem](../../strongs/h/h2493.md), saying,

<a name="daniel_4_9"></a>Daniel 4:9

O [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), [raḇ](../../strongs/h/h7229.md) of the [ḥarṭṭōm](../../strongs/h/h2749.md), because ['ănā'](../../strongs/h/h576.md) [yᵊḏaʿ](../../strongs/h/h3046.md) that the [rûaḥ](../../strongs/h/h7308.md) of the [qadîš](../../strongs/h/h6922.md) ['ĕlâ](../../strongs/h/h426.md) is in thee, and [kōl](../../strongs/h/h3606.md) [lā'](../../strongs/h/h3809.md) [rāz](../../strongs/h/h7328.md) ['ănas](../../strongs/h/h598.md) thee, ['ămar](../../strongs/h/h560.md) me the [ḥēzev](../../strongs/h/h2376.md) of my [ḥēlem](../../strongs/h/h2493.md) that I have [ḥăzā'](../../strongs/h/h2370.md), and the [pᵊšar](../../strongs/h/h6591.md) thereof.

<a name="daniel_4_10"></a>Daniel 4:10

Thus were the [ḥēzev](../../strongs/h/h2376.md) of mine [rē'š](../../strongs/h/h7217.md) [ʿal](../../strongs/h/h5922.md) my [miškāḇ](../../strongs/h/h4903.md); I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md), and ['ălû](../../strongs/h/h431.md) a ['îlān](../../strongs/h/h363.md) in the [gav](../../strongs/h/h1459.md) of the ['ăraʿ](../../strongs/h/h772.md), and the [rûm](../../strongs/h/h7314.md) thereof was [śagî'](../../strongs/h/h7690.md).

<a name="daniel_4_11"></a>Daniel 4:11

The ['îlān](../../strongs/h/h363.md) [rᵊḇâ](../../strongs/h/h7236.md), and was [tᵊqēp̄](../../strongs/h/h8631.md), and the [rûm](../../strongs/h/h7314.md) thereof [mᵊṭā'](../../strongs/h/h4291.md) unto [šᵊmayin](../../strongs/h/h8065.md), and the [ḥăzôṯ](../../strongs/h/h2379.md) thereof to the [sôp̄](../../strongs/h/h5491.md) of [kōl](../../strongs/h/h3606.md) the ['ăraʿ](../../strongs/h/h772.md):

<a name="daniel_4_12"></a>Daniel 4:12

The [ʿŏp̄î](../../strongs/h/h6074.md) thereof were [šapîr](../../strongs/h/h8209.md), and the ['ēḇ](../../strongs/h/h4.md) thereof [śagî'](../../strongs/h/h7690.md), and in it was [māzôn](../../strongs/h/h4203.md) for [kōl](../../strongs/h/h3606.md): the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md) had [ṭᵊlal](../../strongs/h/h2927.md) [tᵊḥôṯ](../../strongs/h/h8460.md) it, and the [ṣipar](../../strongs/h/h6853.md) of the [šᵊmayin](../../strongs/h/h8065.md) [dûr](../../strongs/h/h1753.md) in the [ʿănap̄](../../strongs/h/h6056.md) thereof, and [kōl](../../strongs/h/h3606.md) [bᵊśar](../../strongs/h/h1321.md) was [zûn](../../strongs/h/h2110.md) of [min](../../strongs/h/h4481.md).

<a name="daniel_4_13"></a>Daniel 4:13

I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) in the [ḥēzev](../../strongs/h/h2376.md) of my [rē'š](../../strongs/h/h7217.md) [ʿal](../../strongs/h/h5922.md) my [miškāḇ](../../strongs/h/h4903.md), and, ['ălû](../../strongs/h/h431.md), a [ʿîr](../../strongs/h/h5894.md) and a [qadîš](../../strongs/h/h6922.md) came [nᵊḥaṯ](../../strongs/h/h5182.md) [min](../../strongs/h/h4481.md) [šᵊmayin](../../strongs/h/h8065.md);

<a name="daniel_4_14"></a>Daniel 4:14

He [qᵊrā'](../../strongs/h/h7123.md) [ḥayil](../../strongs/h/h2429.md), and ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md), [gᵊḏaḏ](../../strongs/h/h1414.md) the ['îlān](../../strongs/h/h363.md), and [qᵊṣaṣ](../../strongs/h/h7113.md) his [ʿănap̄](../../strongs/h/h6056.md), [nᵊṯar](../../strongs/h/h5426.md) his [ʿŏp̄î](../../strongs/h/h6074.md), and [bᵊḏar](../../strongs/h/h921.md) his ['ēḇ](../../strongs/h/h4.md): let the [ḥêvā'](../../strongs/h/h2423.md) get [nûḏ](../../strongs/h/h5111.md) [min](../../strongs/h/h4481.md) under [tᵊḥôṯ](../../strongs/h/h8479.md), and the [ṣipar](../../strongs/h/h6853.md) [min](../../strongs/h/h4481.md) his [ʿănap̄](../../strongs/h/h6056.md):

<a name="daniel_4_15"></a>Daniel 4:15

[bᵊram](../../strongs/h/h1297.md) [šᵊḇaq](../../strongs/h/h7662.md) the [ʿiqqār](../../strongs/h/h6136.md) of his [šōreš](../../strongs/h/h8330.md) in the ['ăraʿ](../../strongs/h/h772.md), even with an ['ĕsûr](../../strongs/h/h613.md) of [parzel](../../strongs/h/h6523.md) and [nᵊḥāš](../../strongs/h/h5174.md), in the [deṯe'](../../strongs/h/h1883.md) of the [bar](../../strongs/h/h1251.md); and let it be [ṣᵊḇaʿ](../../strongs/h/h6647.md) with the [ṭal](../../strongs/h/h2920.md) of [šᵊmayin](../../strongs/h/h8065.md), and let his [ḥălāq](../../strongs/h/h2508.md) be [ʿim](../../strongs/h/h5974.md) the [ḥêvā'](../../strongs/h/h2423.md) in the [ʿāš](../../strongs/h/h6211.md) of the ['ăraʿ](../../strongs/h/h772.md):

<a name="daniel_4_16"></a>Daniel 4:16

Let his [lᵊḇaḇ](../../strongs/h/h3825.md) be [šᵊnâ](../../strongs/h/h8133.md) [min](../../strongs/h/h4481.md) ['ēneš](../../strongs/h/h606.md), and let a [ḥêvā'](../../strongs/h/h2423.md) [lᵊḇaḇ](../../strongs/h/h3825.md) be [yᵊhaḇ](../../strongs/h/h3052.md) unto him; and let [šᵊḇaʿ](../../strongs/h/h7655.md) [ʿidān](../../strongs/h/h5732.md) [ḥălap̄](../../strongs/h/h2499.md) [ʿal](../../strongs/h/h5922.md) him.

<a name="daniel_4_17"></a>Daniel 4:17

This [piṯgām](../../strongs/h/h6600.md) is by the [gᵊzērâ](../../strongs/h/h1510.md) of the [ʿîr](../../strongs/h/h5894.md), and the [šᵊ'ēlâ](../../strongs/h/h7595.md) by the [mē'mar](../../strongs/h/h3983.md) of the [qadîš](../../strongs/h/h6922.md): [ʿaḏ](../../strongs/h/h5705.md) the [diḇrâ](../../strongs/h/h1701.md) that the [ḥay](../../strongs/h/h2417.md) may [yᵊḏaʿ](../../strongs/h/h3046.md) that the most [ʿillay](../../strongs/h/h5943.md) [šallîṭ](../../strongs/h/h7990.md) in the [malkû](../../strongs/h/h4437.md) of ['ēneš](../../strongs/h/h606.md), and [nᵊṯan](../../strongs/h/h5415.md) it to [mān](../../strongs/h/h4479.md) he [ṣᵊḇâ](../../strongs/h/h6634.md), and [qûm](../../strongs/h/h6966.md) [ʿal](../../strongs/h/h5922.md) it the [šᵊp̄al](../../strongs/h/h8215.md) of ['ēneš](../../strongs/h/h606.md).

<a name="daniel_4_18"></a>Daniel 4:18

[dēn](../../strongs/h/h1836.md) [ḥēlem](../../strongs/h/h2493.md) ['ănā'](../../strongs/h/h576.md) [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) have [ḥăzā'](../../strongs/h/h2370.md). Now ['antâ](../../strongs/h/h607.md), O [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), ['ămar](../../strongs/h/h560.md) the [pᵊšar](../../strongs/h/h6591.md) thereof, forasmuch [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) the [ḥakîm](../../strongs/h/h2445.md) men of my [malkû](../../strongs/h/h4437.md) are [lā'](../../strongs/h/h3809.md) [yᵊḵēl](../../strongs/h/h3202.md) to make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [pᵊšar](../../strongs/h/h6591.md): but ['antâ](../../strongs/h/h607.md) art [kᵊhal](../../strongs/h/h3546.md); for the [rûaḥ](../../strongs/h/h7308.md) of the [qadîš](../../strongs/h/h6922.md) ['ĕlâ](../../strongs/h/h426.md) is in thee.

<a name="daniel_4_19"></a>Daniel 4:19

['ĕḏayin](../../strongs/h/h116.md) [Dānîyē'L](../../strongs/h/h1841.md), whose [šum](../../strongs/h/h8036.md) was [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), was [šᵊmam](../../strongs/h/h8075.md) for [ḥaḏ](../../strongs/h/h2298.md) [šāʿâ](../../strongs/h/h8160.md), and his [raʿyôn](../../strongs/h/h7476.md) [bᵊhal](../../strongs/h/h927.md) him. The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md), [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), let ['al](../../strongs/h/h409.md) the [ḥēlem](../../strongs/h/h2493.md), or the [pᵊšar](../../strongs/h/h6591.md) thereof, [bᵊhal](../../strongs/h/h927.md) thee. [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), My [mārē'](../../strongs/h/h4756.md), the [ḥēlem](../../strongs/h/h2493.md) be to them that [śᵊnē'](../../strongs/h/h8131.md) thee, and the [pᵊšar](../../strongs/h/h6591.md) thereof to thine [ʿār](../../strongs/h/h6146.md).

<a name="daniel_4_20"></a>Daniel 4:20

The ['îlān](../../strongs/h/h363.md) that thou [ḥăzā'](../../strongs/h/h2370.md), which [rᵊḇâ](../../strongs/h/h7236.md), and was [tᵊqēp̄](../../strongs/h/h8631.md), whose [rûm](../../strongs/h/h7314.md) [mᵊṭā'](../../strongs/h/h4291.md) unto the [šᵊmayin](../../strongs/h/h8065.md), and the [ḥăzôṯ](../../strongs/h/h2379.md) thereof to [kōl](../../strongs/h/h3606.md) the ['ăraʿ](../../strongs/h/h772.md);

<a name="daniel_4_21"></a>Daniel 4:21

Whose [ʿŏp̄î](../../strongs/h/h6074.md) were [šapîr](../../strongs/h/h8209.md), and the ['ēḇ](../../strongs/h/h4.md) thereof [śagî'](../../strongs/h/h7690.md), and in it was [māzôn](../../strongs/h/h4203.md) for [kōl](../../strongs/h/h3606.md); [tᵊḥôṯ](../../strongs/h/h8460.md) which the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md) [dûr](../../strongs/h/h1753.md), and upon whose [ʿănap̄](../../strongs/h/h6056.md) the [ṣipar](../../strongs/h/h6853.md) of the [šᵊmayin](../../strongs/h/h8065.md) had their [šᵊḵan](../../strongs/h/h7932.md):

<a name="daniel_4_22"></a>Daniel 4:22

It is ['antâ](../../strongs/h/h607.md), O [meleḵ](../../strongs/h/h4430.md), that art [rᵊḇâ](../../strongs/h/h7236.md) and become [tᵊqēp̄](../../strongs/h/h8631.md): for thy [rᵊḇû](../../strongs/h/h7238.md) is [rᵊḇâ](../../strongs/h/h7236.md), and [mᵊṭā'](../../strongs/h/h4291.md) unto [šᵊmayin](../../strongs/h/h8065.md), and thy [šālṭān](../../strongs/h/h7985.md) to the [sôp̄](../../strongs/h/h5491.md) of the ['ăraʿ](../../strongs/h/h772.md).

<a name="daniel_4_23"></a>Daniel 4:23

And whereas the [meleḵ](../../strongs/h/h4430.md) [ḥăzā'](../../strongs/h/h2370.md) a [ʿîr](../../strongs/h/h5894.md) and a [qadîš](../../strongs/h/h6922.md) coming [nᵊḥaṯ](../../strongs/h/h5182.md) [min](../../strongs/h/h4481.md) [šᵊmayin](../../strongs/h/h8065.md), and ['ămar](../../strongs/h/h560.md), [gᵊḏaḏ](../../strongs/h/h1414.md) the ['îlān](../../strongs/h/h363.md) [gᵊḏaḏ](../../strongs/h/h1414.md), and [ḥăḇal](../../strongs/h/h2255.md) it; [bᵊram](../../strongs/h/h1297.md) [šᵊḇaq](../../strongs/h/h7662.md) the [ʿiqqār](../../strongs/h/h6136.md) of the [šōreš](../../strongs/h/h8330.md) thereof in the ['ăraʿ](../../strongs/h/h772.md), even with an ['ĕsûr](../../strongs/h/h613.md) of [parzel](../../strongs/h/h6523.md) and [nᵊḥāš](../../strongs/h/h5174.md), in the [deṯe'](../../strongs/h/h1883.md) of the [bar](../../strongs/h/h1251.md); and let it be [ṣᵊḇaʿ](../../strongs/h/h6647.md) with the [ṭal](../../strongs/h/h2920.md) of [šᵊmayin](../../strongs/h/h8065.md), and let his [ḥălāq](../../strongs/h/h2508.md) be [ʿim](../../strongs/h/h5974.md) the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md), [ʿaḏ](../../strongs/h/h5705.md) [šᵊḇaʿ](../../strongs/h/h7655.md) [ʿidān](../../strongs/h/h5732.md) [ḥălap̄](../../strongs/h/h2499.md) [ʿal](../../strongs/h/h5922.md) him;

<a name="daniel_4_24"></a>Daniel 4:24

[dēn](../../strongs/h/h1836.md) is the [pᵊšar](../../strongs/h/h6591.md), O [meleḵ](../../strongs/h/h4430.md), and [hû'](../../strongs/h/h1932.md) is the [gᵊzērâ](../../strongs/h/h1510.md) of the most [ʿillay](../../strongs/h/h5943.md), which is [mᵊṭā'](../../strongs/h/h4291.md) [ʿal](../../strongs/h/h5922.md) my [mārē'](../../strongs/h/h4756.md) the [meleḵ](../../strongs/h/h4430.md):

<a name="daniel_4_25"></a>Daniel 4:25

That they shall [ṭᵊraḏ](../../strongs/h/h2957.md) thee [min](../../strongs/h/h4481.md) ['ēneš](../../strongs/h/h606.md), and thy [mᵊḏôr](../../strongs/h/h4070.md) shall [hăvâ](../../strongs/h/h1934.md) [ʿim](../../strongs/h/h5974.md) the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md), and they shall make thee to [ṭᵊʿam](../../strongs/h/h2939.md) [ʿāš](../../strongs/h/h6211.md) as [tôr](../../strongs/h/h8450.md), and they shall [ṣᵊḇaʿ](../../strongs/h/h6647.md) thee with the [ṭal](../../strongs/h/h2920.md) of [šᵊmayin](../../strongs/h/h8065.md), and [šᵊḇaʿ](../../strongs/h/h7655.md) [ʿidān](../../strongs/h/h5732.md) shall [ḥălap̄](../../strongs/h/h2499.md) [ʿal](../../strongs/h/h5922.md) thee, [ʿaḏ](../../strongs/h/h5705.md) thou [yᵊḏaʿ](../../strongs/h/h3046.md) that the most [ʿillay](../../strongs/h/h5943.md) [šallîṭ](../../strongs/h/h7990.md) in the [malkû](../../strongs/h/h4437.md) of ['ēneš](../../strongs/h/h606.md), and [nᵊṯan](../../strongs/h/h5415.md) it to [mān](../../strongs/h/h4479.md) he [ṣᵊḇâ](../../strongs/h/h6634.md).

<a name="daniel_4_26"></a>Daniel 4:26

And whereas they ['ămar](../../strongs/h/h560.md) to [šᵊḇaq](../../strongs/h/h7662.md) the [ʿiqqār](../../strongs/h/h6136.md) of the ['îlān](../../strongs/h/h363.md) [šōreš](../../strongs/h/h8330.md); thy [malkû](../../strongs/h/h4437.md) shall be [qayyām](../../strongs/h/h7011.md) unto thee, [min](../../strongs/h/h4481.md) that thou shalt have [yᵊḏaʿ](../../strongs/h/h3046.md) that the [šᵊmayin](../../strongs/h/h8065.md) do [šallîṭ](../../strongs/h/h7990.md).

<a name="daniel_4_27"></a>Daniel 4:27

[lāhēn](../../strongs/h/h3861.md), O [meleḵ](../../strongs/h/h4430.md), let my [mᵊlaḵ](../../strongs/h/h4431.md) be [šᵊp̄ar](../../strongs/h/h8232.md) unto [ʿal](../../strongs/h/h5922.md), and [pᵊraq](../../strongs/h/h6562.md) thy [ḥăṭāy](../../strongs/h/h2408.md) by [ṣiḏqâ](../../strongs/h/h6665.md), and thine [ʿivyā'](../../strongs/h/h5758.md) by shewing [ḥănan](../../strongs/h/h2604.md) to the [ʿănâ](../../strongs/h/h6033.md); [hēn](../../strongs/h/h2006.md) it may [hăvâ](../../strongs/h/h1934.md) an ['arkâ](../../strongs/h/h754.md) of thy [šᵊlēvâ](../../strongs/h/h7963.md).

<a name="daniel_4_28"></a>Daniel 4:28

[kōl](../../strongs/h/h3606.md) this [mᵊṭā'](../../strongs/h/h4291.md) [ʿal](../../strongs/h/h5922.md) the [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md).

<a name="daniel_4_29"></a>Daniel 4:29

At the [qᵊṣāṯ](../../strongs/h/h7118.md) of [tᵊrên](../../strongs/h/h8648.md) [ʿăśar](../../strongs/h/h6236.md) [yᵊraḥ](../../strongs/h/h3393.md) he [hăvâ](../../strongs/h/h1934.md) [hălaḵ](../../strongs/h/h1981.md) [ʿal](../../strongs/h/h5922.md) the [hêḵal](../../strongs/h/h1965.md) of the [malkû](../../strongs/h/h4437.md) of [Bāḇel](../../strongs/h/h895.md).

<a name="daniel_4_30"></a>Daniel 4:30

The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md), and ['ămar](../../strongs/h/h560.md), Is [lā'](../../strongs/h/h3809.md) [hû'](../../strongs/h/h1932.md) [dā'](../../strongs/h/h1668.md) [raḇ](../../strongs/h/h7229.md) [Bāḇel](../../strongs/h/h895.md), that ['ănā'](../../strongs/h/h576.md) have [bᵊnā'](../../strongs/h/h1124.md) for the [bayiṯ](../../strongs/h/h1005.md) of the [malkû](../../strongs/h/h4437.md) by the [tᵊqōp̄](../../strongs/h/h8632.md) of my [ḥēsen](../../strongs/h/h2632.md), and for the [yᵊqār](../../strongs/h/h3367.md) of my [hăḏar](../../strongs/h/h1923.md)?

<a name="daniel_4_31"></a>Daniel 4:31

[ʿôḏ](../../strongs/h/h5751.md) the [millâ](../../strongs/h/h4406.md) was in the [meleḵ](../../strongs/h/h4430.md) [pum](../../strongs/h/h6433.md), there [nᵊp̄al](../../strongs/h/h5308.md) a [qāl](../../strongs/h/h7032.md) [min](../../strongs/h/h4481.md) [šᵊmayin](../../strongs/h/h8065.md), saying, O [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md), to thee it is ['ămar](../../strongs/h/h560.md); The [malkû](../../strongs/h/h4437.md) is [ʿăḏā'](../../strongs/h/h5709.md) [min](../../strongs/h/h4481.md) thee.

<a name="daniel_4_32"></a>Daniel 4:32

And they shall [ṭᵊraḏ](../../strongs/h/h2957.md) thee [min](../../strongs/h/h4481.md) ['ēneš](../../strongs/h/h606.md), and thy [mᵊḏôr](../../strongs/h/h4070.md) shall be [ʿim](../../strongs/h/h5974.md) the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md): they shall make thee to [ṭᵊʿam](../../strongs/h/h2939.md) [ʿāš](../../strongs/h/h6211.md) as [tôr](../../strongs/h/h8450.md), and [šᵊḇaʿ](../../strongs/h/h7655.md) [ʿidān](../../strongs/h/h5732.md) shall [ḥălap̄](../../strongs/h/h2499.md) [ʿal](../../strongs/h/h5922.md) thee, [ʿaḏ](../../strongs/h/h5705.md) thou [yᵊḏaʿ](../../strongs/h/h3046.md) that the most [ʿillay](../../strongs/h/h5943.md) [šallîṭ](../../strongs/h/h7990.md) in the [malkû](../../strongs/h/h4437.md) of ['ēneš](../../strongs/h/h606.md), and [nᵊṯan](../../strongs/h/h5415.md) it to [mān](../../strongs/h/h4479.md) he [ṣᵊḇâ](../../strongs/h/h6634.md).

<a name="daniel_4_33"></a>Daniel 4:33

The same [šāʿâ](../../strongs/h/h8160.md) was the [millâ](../../strongs/h/h4406.md) [sûp̄](../../strongs/h/h5487.md) [ʿal](../../strongs/h/h5922.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md): and he was [ṭᵊraḏ](../../strongs/h/h2957.md) [min](../../strongs/h/h4481.md) ['ēneš](../../strongs/h/h606.md), and did ['ăḵal](../../strongs/h/h399.md) [ʿāš](../../strongs/h/h6211.md) as [tôr](../../strongs/h/h8450.md), and his [gešem](../../strongs/h/h1655.md) was [ṣᵊḇaʿ](../../strongs/h/h6647.md) with the [ṭal](../../strongs/h/h2920.md) of [šᵊmayin](../../strongs/h/h8065.md), [ʿaḏ](../../strongs/h/h5705.md) his [śᵊʿar](../../strongs/h/h8177.md) were [rᵊḇâ](../../strongs/h/h7236.md) like [nᵊšar](../../strongs/h/h5403.md) feathers, and his [ṭᵊp̄ar](../../strongs/h/h2953.md) like [ṣipar](../../strongs/h/h6853.md).

<a name="daniel_4_34"></a>Daniel 4:34

And at the [qᵊṣāṯ](../../strongs/h/h7118.md) of the [yôm](../../strongs/h/h3118.md) ['ănā'](../../strongs/h/h576.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) lifted [nᵊṭal](../../strongs/h/h5191.md) mine [ʿayin](../../strongs/h/h5870.md) unto [šᵊmayin](../../strongs/h/h8065.md), and mine [mandaʿ](../../strongs/h/h4486.md) [tûḇ](../../strongs/h/h8421.md) unto [ʿal](../../strongs/h/h5922.md), and I [bᵊraḵ](../../strongs/h/h1289.md) the most [ʿillay](../../strongs/h/h5943.md), and I [šᵊḇaḥ](../../strongs/h/h7624.md) and [hăḏar](../../strongs/h/h1922.md) him that [ḥay](../../strongs/h/h2417.md) for [ʿālam](../../strongs/h/h5957.md), whose [šālṭān](../../strongs/h/h7985.md) is an [ʿālam](../../strongs/h/h5957.md) [šālṭān](../../strongs/h/h7985.md), and his [malkû](../../strongs/h/h4437.md) is [ʿim](../../strongs/h/h5974.md) [dār](../../strongs/h/h1859.md) to [dār](../../strongs/h/h1859.md):

<a name="daniel_4_35"></a>Daniel 4:35

And [kōl](../../strongs/h/h3606.md) the [dûr](../../strongs/h/h1753.md) of the ['ăraʿ](../../strongs/h/h772.md) are [ḥăšaḇ](../../strongs/h/h2804.md) as [lā'](../../strongs/h/h3809.md): and he [ʿăḇaḏ](../../strongs/h/h5648.md) according to his [ṣᵊḇâ](../../strongs/h/h6634.md) in the [ḥayil](../../strongs/h/h2429.md) of [šᵊmayin](../../strongs/h/h8065.md), and among the [dûr](../../strongs/h/h1753.md) of the ['ăraʿ](../../strongs/h/h772.md): and [lā'](../../strongs/h/h3809.md) ['îṯay](../../strongs/h/h383.md) [mᵊḥā'](../../strongs/h/h4223.md) his [yaḏ](../../strongs/h/h3028.md), or ['ămar](../../strongs/h/h560.md) unto him, [mâ](../../strongs/h/h4101.md) [ʿăḇaḏ](../../strongs/h/h5648.md) thou?

<a name="daniel_4_36"></a>Daniel 4:36

At the same [zᵊmān](../../strongs/h/h2166.md) my [mandaʿ](../../strongs/h/h4486.md) [tûḇ](../../strongs/h/h8421.md) unto [ʿal](../../strongs/h/h5922.md); and for the [yᵊqār](../../strongs/h/h3367.md) of my [malkû](../../strongs/h/h4437.md), mine [hăḏar](../../strongs/h/h1923.md) and [zîv](../../strongs/h/h2122.md) [tûḇ](../../strongs/h/h8421.md) unto [ʿal](../../strongs/h/h5922.md); and my [hadāḇar](../../strongs/h/h1907.md) and my [raḇrᵊḇānîn](../../strongs/h/h7261.md) [bᵊʿā'](../../strongs/h/h1156.md) unto [ʿal](../../strongs/h/h5922.md); and I was [tᵊqan](../../strongs/h/h8627.md) in my [malkû](../../strongs/h/h4437.md), and [yatîr](../../strongs/h/h3493.md) [rᵊḇû](../../strongs/h/h7238.md) was [yᵊsap̄](../../strongs/h/h3255.md) unto me.

<a name="daniel_4_37"></a>Daniel 4:37

[kᵊʿan](../../strongs/h/h3705.md) ['ănā'](../../strongs/h/h576.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [šᵊḇaḥ](../../strongs/h/h7624.md) and [rûm](../../strongs/h/h7313.md) and [hăḏar](../../strongs/h/h1922.md) the [meleḵ](../../strongs/h/h4430.md) of [šᵊmayin](../../strongs/h/h8065.md), [kōl](../../strongs/h/h3606.md) whose [maʿbāḏ](../../strongs/h/h4567.md) are [qᵊšōṭ](../../strongs/h/h7187.md), and his ['ōraḥ](../../strongs/h/h735.md) [dîn](../../strongs/h/h1780.md): and those that [hălaḵ](../../strongs/h/h1981.md) in [gēvâ](../../strongs/h/h1467.md) he is [yᵊḵēl](../../strongs/h/h3202.md) to [šᵊp̄al](../../strongs/h/h8214.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 3](daniel_3.md) - [Daniel 5](daniel_5.md)