# [Daniel 12](https://www.blueletterbible.org/kjv/daniel/12)

<a name="daniel_12_1"></a>Daniel 12:1

And at that [ʿēṯ](../../strongs/h/h6256.md) shall [Mîḵā'ēl](../../strongs/h/h4317.md) ['amad](../../strongs/h/h5975.md), the [gadowl](../../strongs/h/h1419.md) [śar](../../strongs/h/h8269.md) which ['amad](../../strongs/h/h5975.md) for the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md): and there shall [hayah](../../strongs/h/h1961.md) a [ʿēṯ](../../strongs/h/h6256.md) of [tsarah](../../strongs/h/h6869.md), such as never was since there was a [gowy](../../strongs/h/h1471.md) even to that same [ʿēṯ](../../strongs/h/h6256.md): and at that [ʿēṯ](../../strongs/h/h6256.md) thy ['am](../../strongs/h/h5971.md) shall be [mālaṭ](../../strongs/h/h4422.md), every one that shall be [māṣā'](../../strongs/h/h4672.md) [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md).

<a name="daniel_12_2"></a>Daniel 12:2

And [rab](../../strongs/h/h7227.md) of them that [yāšēn](../../strongs/h/h3463.md) in the ['aphar](../../strongs/h/h6083.md) of the ['ăḏāmâ](../../strongs/h/h127.md) shall [quwts](../../strongs/h/h6974.md), some to ['owlam](../../strongs/h/h5769.md) [chay](../../strongs/h/h2416.md), and some to [cherpah](../../strongs/h/h2781.md) and ['owlam](../../strongs/h/h5769.md) [dᵊrā'ôn](../../strongs/h/h1860.md).

<a name="daniel_12_3"></a>Daniel 12:3

And they that be [sakal](../../strongs/h/h7919.md) shall [zāhar](../../strongs/h/h2094.md) as the [zōhar](../../strongs/h/h2096.md) of the [raqiya'](../../strongs/h/h7549.md); and they that [rab](../../strongs/h/h7227.md) to [ṣāḏaq](../../strongs/h/h6663.md) as the [kowkab](../../strongs/h/h3556.md) ['owlam](../../strongs/h/h5769.md) and [ʿaḏ](../../strongs/h/h5703.md).

<a name="daniel_12_4"></a>Daniel 12:4

But thou, O [Dinîyē'L](../../strongs/h/h1840.md), [sāṯam](../../strongs/h/h5640.md) the [dabar](../../strongs/h/h1697.md), and [ḥāṯam](../../strongs/h/h2856.md) the [sēp̄er](../../strongs/h/h5612.md), even to the [ʿēṯ](../../strongs/h/h6256.md) of the [qēṣ](../../strongs/h/h7093.md): [rab](../../strongs/h/h7227.md) shall [šûṭ](../../strongs/h/h7751.md), and [da'ath](../../strongs/h/h1847.md) shall be [rabah](../../strongs/h/h7235.md).

<a name="daniel_12_5"></a>Daniel 12:5

Then I [Dinîyē'L](../../strongs/h/h1840.md) [ra'ah](../../strongs/h/h7200.md), and, behold, there ['amad](../../strongs/h/h5975.md) ['aḥēr](../../strongs/h/h312.md) two, the one on this side of the [saphah](../../strongs/h/h8193.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), and the other on that side of the [saphah](../../strongs/h/h8193.md) of the [yᵊ'ōr](../../strongs/h/h2975.md).

<a name="daniel_12_6"></a>Daniel 12:6

And one ['āmar](../../strongs/h/h559.md) to the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) in [baḏ](../../strongs/h/h906.md), which was [maʿal](../../strongs/h/h4605.md) the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), How long shall it be to the [qēṣ](../../strongs/h/h7093.md) of these [pele'](../../strongs/h/h6382.md)?

<a name="daniel_12_7"></a>Daniel 12:7

And I [shama'](../../strongs/h/h8085.md) the ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) in [baḏ](../../strongs/h/h906.md), which was [maʿal](../../strongs/h/h4605.md) the [mayim](../../strongs/h/h4325.md) of the [yᵊ'ōr](../../strongs/h/h2975.md), when he held [ruwm](../../strongs/h/h7311.md) his [yamiyn](../../strongs/h/h3225.md) and his [śᵊmō'l](../../strongs/h/h8040.md) unto [shamayim](../../strongs/h/h8064.md), and [shaba'](../../strongs/h/h7650.md) by him that [chay](../../strongs/h/h2416.md) ['owlam](../../strongs/h/h5769.md) that it shall be for a [môʿēḏ](../../strongs/h/h4150.md), [môʿēḏ](../../strongs/h/h4150.md), and an [ḥēṣî](../../strongs/h/h2677.md); and when he shall have [kalah](../../strongs/h/h3615.md) to [naphats](../../strongs/h/h5310.md) the [yad](../../strongs/h/h3027.md) of the [qodesh](../../strongs/h/h6944.md) ['am](../../strongs/h/h5971.md), all these things shall be [kalah](../../strongs/h/h3615.md).

<a name="daniel_12_8"></a>Daniel 12:8

And I [shama'](../../strongs/h/h8085.md), but I [bîn](../../strongs/h/h995.md) not: then ['āmar](../../strongs/h/h559.md) I, O my ['adown](../../strongs/h/h113.md), what shall be the ['aḥărîṯ](../../strongs/h/h319.md) of these things?

<a name="daniel_12_9"></a>Daniel 12:9

And he ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), [Dinîyē'L](../../strongs/h/h1840.md): for the [dabar](../../strongs/h/h1697.md) are [sāṯam](../../strongs/h/h5640.md) and [ḥāṯam](../../strongs/h/h2856.md) till the [ʿēṯ](../../strongs/h/h6256.md) of the [qēṣ](../../strongs/h/h7093.md).

<a name="daniel_12_10"></a>Daniel 12:10

[rab](../../strongs/h/h7227.md) shall be [bārar](../../strongs/h/h1305.md), and made [lāḇan](../../strongs/h/h3835.md), and [tsaraph](../../strongs/h/h6884.md); but the [rasha'](../../strongs/h/h7563.md) shall do [rāšaʿ](../../strongs/h/h7561.md): and none of the [rasha'](../../strongs/h/h7563.md) shall [bîn](../../strongs/h/h995.md); but the [sakal](../../strongs/h/h7919.md) shall [bîn](../../strongs/h/h995.md).

<a name="daniel_12_11"></a>Daniel 12:11

And from the [ʿēṯ](../../strongs/h/h6256.md) that the [tāmîḏ](../../strongs/h/h8548.md) shall be [cuwr](../../strongs/h/h5493.md), and the [šiqqûṣ](../../strongs/h/h8251.md) that maketh [šāmēm](../../strongs/h/h8074.md) [nathan](../../strongs/h/h5414.md), there shall be a thousand two hundred and ninety [yowm](../../strongs/h/h3117.md).

<a name="daniel_12_12"></a>Daniel 12:12

['esher](../../strongs/h/h835.md) is he that [ḥāḵâ](../../strongs/h/h2442.md), and [naga'](../../strongs/h/h5060.md) to the thousand three hundred and five and thirty [yowm](../../strongs/h/h3117.md).

<a name="daniel_12_13"></a>Daniel 12:13

But go thou thy [yālaḵ](../../strongs/h/h3212.md) till the [qēṣ](../../strongs/h/h7093.md) be: for thou shalt [nuwach](../../strongs/h/h5117.md), and ['amad](../../strongs/h/h5975.md) in thy [gôrāl](../../strongs/h/h1486.md) at the [qēṣ](../../strongs/h/h7093.md) of the [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 11](daniel_11.md)