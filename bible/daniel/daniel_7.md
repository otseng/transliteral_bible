# [Daniel 7](https://www.blueletterbible.org/kjv/daniel/7)

<a name="daniel_7_1"></a>Daniel 7:1

In the [ḥaḏ](../../strongs/h/h2298.md) [šᵊnâ](../../strongs/h/h8140.md) of [Bēlša'Ṣṣar](../../strongs/h/h1113.md) [meleḵ](../../strongs/h/h4430.md) of [Bāḇel](../../strongs/h/h895.md) [Dānîyē'L](../../strongs/h/h1841.md) [ḥăzā'](../../strongs/h/h2370.md) a [ḥēlem](../../strongs/h/h2493.md) and [ḥēzev](../../strongs/h/h2376.md) of his [rē'š](../../strongs/h/h7217.md) [ʿal](../../strongs/h/h5922.md) his [miškāḇ](../../strongs/h/h4903.md): ['ĕḏayin](../../strongs/h/h116.md) he [kᵊṯaḇ](../../strongs/h/h3790.md) the [ḥēlem](../../strongs/h/h2493.md), and ['ămar](../../strongs/h/h560.md) the [rē'š](../../strongs/h/h7217.md) of the [millâ](../../strongs/h/h4406.md).

<a name="daniel_7_2"></a>Daniel 7:2

[Dānîyē'L](../../strongs/h/h1841.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), I [ḥăzā'](../../strongs/h/h2370.md) [hăvâ](../../strongs/h/h1934.md) in my [ḥēzev](../../strongs/h/h2376.md) [ʿim](../../strongs/h/h5974.md) [lêlyā'](../../strongs/h/h3916.md), and, ['ărû](../../strongs/h/h718.md), the ['arbaʿ](../../strongs/h/h703.md) [rûaḥ](../../strongs/h/h7308.md) of the [šᵊmayin](../../strongs/h/h8065.md) [gîaḥ](../../strongs/h/h1519.md) upon the [raḇ](../../strongs/h/h7229.md) [yām](../../strongs/h/h3221.md).

<a name="daniel_7_3"></a>Daniel 7:3

And ['arbaʿ](../../strongs/h/h703.md) [raḇraḇ](../../strongs/h/h7260.md) [ḥêvā'](../../strongs/h/h2423.md) [sᵊlaq](../../strongs/h/h5559.md) [min](../../strongs/h/h4481.md) the [yām](../../strongs/h/h3221.md), [šᵊnâ](../../strongs/h/h8133.md) [dā'](../../strongs/h/h1668.md) [min](../../strongs/h/h4481.md) [dā'](../../strongs/h/h1668.md).

<a name="daniel_7_4"></a>Daniel 7:4

The [qaḏmay](../../strongs/h/h6933.md) was like an ['aryê](../../strongs/h/h744.md), and had [nᵊšar](../../strongs/h/h5403.md) [gap̄](../../strongs/h/h1611.md): I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) [ʿaḏ](../../strongs/h/h5705.md) the [gap̄](../../strongs/h/h1611.md) thereof were [mᵊrāṭ](../../strongs/h/h4804.md), and it was [nᵊṭal](../../strongs/h/h5191.md) [min](../../strongs/h/h4481.md) the ['ăraʿ](../../strongs/h/h772.md), and [qûm](../../strongs/h/h6966.md) [ʿal](../../strongs/h/h5922.md) the [reḡel](../../strongs/h/h7271.md) as an ['ēneš](../../strongs/h/h606.md), and an ['ēneš](../../strongs/h/h606.md) [lᵊḇaḇ](../../strongs/h/h3825.md) was [yᵊhaḇ](../../strongs/h/h3052.md) to it.

<a name="daniel_7_5"></a>Daniel 7:5

And ['ărû](../../strongs/h/h718.md) ['āḥŏrî](../../strongs/h/h317.md) [ḥêvā'](../../strongs/h/h2423.md), a [tinyān](../../strongs/h/h8578.md), [dᵊmâ](../../strongs/h/h1821.md) to a [dōḇ](../../strongs/h/h1678.md), and it [qûm](../../strongs/h/h6966.md) itself on [ḥaḏ](../../strongs/h/h2298.md) [šᵊṭar](../../strongs/h/h7859.md), and it had [tᵊlāṯ](../../strongs/h/h8532.md) [ʿălaʿ](../../strongs/h/h5967.md) in the [pum](../../strongs/h/h6433.md) of it [bên](../../strongs/h/h997.md) the [šēn](../../strongs/h/h8128.md) of it: and they ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md) unto it, [qûm](../../strongs/h/h6966.md), ['ăḵal](../../strongs/h/h399.md) [śagî'](../../strongs/h/h7690.md) [bᵊśar](../../strongs/h/h1321.md).

<a name="daniel_7_6"></a>Daniel 7:6

['ăṯar](../../strongs/h/h870.md) [dēn](../../strongs/h/h1836.md) I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md), and ['ărû](../../strongs/h/h718.md) ['āḥŏrî](../../strongs/h/h317.md), like a [nᵊmar](../../strongs/h/h5245.md), which had [ʿal](../../strongs/h/h5922.md) the [gaḇ](../../strongs/h/h1355.md) of it ['arbaʿ](../../strongs/h/h703.md) [gap̄](../../strongs/h/h1611.md) of a [ʿôp̄](../../strongs/h/h5776.md); the [ḥêvā'](../../strongs/h/h2423.md) had also ['arbaʿ](../../strongs/h/h703.md) [rē'š](../../strongs/h/h7217.md); and [šālṭān](../../strongs/h/h7985.md) was [yᵊhaḇ](../../strongs/h/h3052.md) to it.

<a name="daniel_7_7"></a>Daniel 7:7

['ăṯar](../../strongs/h/h870.md) [dēn](../../strongs/h/h1836.md) I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) in the [lêlyā'](../../strongs/h/h3916.md) [ḥēzev](../../strongs/h/h2376.md), and ['ărû](../../strongs/h/h718.md) a [rᵊḇîʿay](../../strongs/h/h7244.md) [ḥêvā'](../../strongs/h/h2423.md), [dᵊḥal](../../strongs/h/h1763.md) and ['emtānî](../../strongs/h/h574.md), and [taqqîp̄](../../strongs/h/h8624.md) [yatîr](../../strongs/h/h3493.md); and it had [raḇraḇ](../../strongs/h/h7260.md) [parzel](../../strongs/h/h6523.md) [šēn](../../strongs/h/h8128.md): it ['ăḵal](../../strongs/h/h399.md) and [dᵊqaq](../../strongs/h/h1855.md), and [rᵊp̄as](../../strongs/h/h7512.md) the [šᵊ'ār](../../strongs/h/h7606.md) with the [reḡel](../../strongs/h/h7271.md) of it: and [hû'](../../strongs/h/h1932.md) was [šᵊnâ](../../strongs/h/h8133.md) [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) the [ḥêvā'](../../strongs/h/h2423.md) that were [qŏḏām](../../strongs/h/h6925.md) it; and it had [ʿăśar](../../strongs/h/h6236.md) [qeren](../../strongs/h/h7162.md).

<a name="daniel_7_8"></a>Daniel 7:8

I [hăvâ](../../strongs/h/h1934.md) [śᵊḵal](../../strongs/h/h7920.md) the [qeren](../../strongs/h/h7162.md), and, ['ălû](../../strongs/h/h431.md), there came [sᵊlaq](../../strongs/h/h5559.md) [bên](../../strongs/h/h997.md) them ['āḥŏrî](../../strongs/h/h317.md) [zᵊʿêr](../../strongs/h/h2192.md) [qeren](../../strongs/h/h7162.md), [qŏḏām](../../strongs/h/h6925.md) [min](../../strongs/h/h4481.md) whom there were [tᵊlāṯ](../../strongs/h/h8532.md) [min](../../strongs/h/h4481.md) the [qaḏmay](../../strongs/h/h6933.md) [qeren](../../strongs/h/h7162.md) [ʿăqar](../../strongs/h/h6132.md): and, ['ălû](../../strongs/h/h431.md), in [dā'](../../strongs/h/h1668.md) [qeren](../../strongs/h/h7162.md) were [ʿayin](../../strongs/h/h5870.md) like the [ʿayin](../../strongs/h/h5870.md) of ['ēneš](../../strongs/h/h606.md), and a [pum](../../strongs/h/h6433.md) [mᵊlal](../../strongs/h/h4449.md) [raḇraḇ](../../strongs/h/h7260.md).

<a name="daniel_7_9"></a>Daniel 7:9

I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) [ʿaḏ](../../strongs/h/h5705.md) the [kārsē'](../../strongs/h/h3764.md) were [rᵊmâ](../../strongs/h/h7412.md), and the [ʿatîq](../../strongs/h/h6268.md) of [yôm](../../strongs/h/h3118.md) did [yᵊṯiḇ](../../strongs/h/h3488.md), whose [lᵊḇûš](../../strongs/h/h3831.md) was [ḥiûār](../../strongs/h/h2358.md) as [tᵊlaḡ](../../strongs/h/h8517.md), and the [śᵊʿar](../../strongs/h/h8177.md) of his [rē'š](../../strongs/h/h7217.md) like the [nᵊqē'](../../strongs/h/h5343.md) [ʿămar](../../strongs/h/h6015.md): his [kārsē'](../../strongs/h/h3764.md) was like the [nûr](../../strongs/h/h5135.md) [šᵊḇîḇ](../../strongs/h/h7631.md), and his [galgal](../../strongs/h/h1535.md) as [dᵊlaq](../../strongs/h/h1815.md) [nûr](../../strongs/h/h5135.md).

<a name="daniel_7_10"></a>Daniel 7:10

A [nûr](../../strongs/h/h5135.md) [nᵊhar](../../strongs/h/h5103.md) [nᵊḡaḏ](../../strongs/h/h5047.md) and [nᵊp̄aq](../../strongs/h/h5312.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) him: ['elep̄](../../strongs/h/h506.md) ['elep̄](../../strongs/h/h506.md) [šᵊmaš](../../strongs/h/h8120.md) unto him, and ten [ribô](../../strongs/h/h7240.md) times ten [ribô](../../strongs/h/h7240.md) [qûm](../../strongs/h/h6966.md) [qŏḏām](../../strongs/h/h6925.md) him: the [dîn](../../strongs/h/h1780.md) was [yᵊṯiḇ](../../strongs/h/h3488.md), and the [sᵊp̄ar](../../strongs/h/h5609.md) were [pĕthach](../../strongs/h/h6606.md).

<a name="daniel_7_11"></a>Daniel 7:11

I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) ['ĕḏayin](../../strongs/h/h116.md) [min](../../strongs/h/h4481.md) of the [qāl](../../strongs/h/h7032.md) of the [raḇraḇ](../../strongs/h/h7260.md) [millâ](../../strongs/h/h4406.md) which the [qeren](../../strongs/h/h7162.md) [mᵊlal](../../strongs/h/h4449.md): I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) even [ʿaḏ](../../strongs/h/h5705.md) the [ḥêvā'](../../strongs/h/h2423.md) was [qᵊṭal](../../strongs/h/h6992.md), and his [gešem](../../strongs/h/h1655.md) ['ăḇaḏ](../../strongs/h/h7.md), and [yᵊhaḇ](../../strongs/h/h3052.md) to the [yᵊqēḏā'](../../strongs/h/h3346.md) ['ēš](../../strongs/h/h785.md).

<a name="daniel_7_12"></a>Daniel 7:12

As concerning the [šᵊ'ār](../../strongs/h/h7606.md) of the [ḥêvā'](../../strongs/h/h2423.md), they had their [šālṭān](../../strongs/h/h7985.md) [ʿăḏā'](../../strongs/h/h5709.md): yet their [ḥay](../../strongs/h/h2417.md) were [yᵊhaḇ](../../strongs/h/h3052.md) [ʿaḏ](../../strongs/h/h5705.md) a [zᵊmān](../../strongs/h/h2166.md) and [ʿidān](../../strongs/h/h5732.md).

<a name="daniel_7_13"></a>Daniel 7:13

I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md) in the [lêlyā'](../../strongs/h/h3916.md) [ḥēzev](../../strongs/h/h2376.md), and, ['ărû](../../strongs/h/h718.md), one like the [bar](../../strongs/h/h1247.md) of ['ēneš](../../strongs/h/h606.md) ['ăṯâ](../../strongs/h/h858.md) [ʿim](../../strongs/h/h5974.md) the [ʿănān](../../strongs/h/h6050.md) of [šᵊmayin](../../strongs/h/h8065.md), and [mᵊṭā'](../../strongs/h/h4291.md) [ʿaḏ](../../strongs/h/h5705.md) the [ʿatîq](../../strongs/h/h6268.md) of [yôm](../../strongs/h/h3118.md), and they [qᵊrēḇ](../../strongs/h/h7127.md) [qŏḏām](../../strongs/h/h6925.md) him.

<a name="daniel_7_14"></a>Daniel 7:14

And there was [yᵊhaḇ](../../strongs/h/h3052.md) him [šālṭān](../../strongs/h/h7985.md), and [yᵊqār](../../strongs/h/h3367.md), and a [malkû](../../strongs/h/h4437.md), that [kōl](../../strongs/h/h3606.md) [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md), should [pᵊlaḥ](../../strongs/h/h6399.md) him: his [šālṭān](../../strongs/h/h7985.md) is an [ʿālam](../../strongs/h/h5957.md) [šālṭān](../../strongs/h/h7985.md), which shall [lā'](../../strongs/h/h3809.md) pass [ʿăḏā'](../../strongs/h/h5709.md), and his [malkû](../../strongs/h/h4437.md) that which shall [lā'](../../strongs/h/h3809.md) be [ḥăḇal](../../strongs/h/h2255.md).

<a name="daniel_7_15"></a>Daniel 7:15

['ănā'](../../strongs/h/h576.md) [Dānîyē'L](../../strongs/h/h1841.md) was [kᵊrā'](../../strongs/h/h3735.md) in my [rûaḥ](../../strongs/h/h7308.md) in the [gav](../../strongs/h/h1459.md) of my [niḏnê](../../strongs/h/h5085.md), and the [ḥēzev](../../strongs/h/h2376.md) of my [rē'š](../../strongs/h/h7217.md) [bᵊhal](../../strongs/h/h927.md) me.

<a name="daniel_7_16"></a>Daniel 7:16

I came [qᵊrēḇ](../../strongs/h/h7127.md) [ʿal](../../strongs/h/h5922.md) [ḥaḏ](../../strongs/h/h2298.md) [min](../../strongs/h/h4481.md) them that [qûm](../../strongs/h/h6966.md), and [bᵊʿā'](../../strongs/h/h1156.md) [min](../../strongs/h/h4481.md) the [yaṣṣîḇ](../../strongs/h/h3330.md) of [kōl](../../strongs/h/h3606.md) [dēn](../../strongs/h/h1836.md). So he ['ămar](../../strongs/h/h560.md) me, and made me [yᵊḏaʿ](../../strongs/h/h3046.md) the [pᵊšar](../../strongs/h/h6591.md) of the [millâ](../../strongs/h/h4406.md).

<a name="daniel_7_17"></a>Daniel 7:17

['illên](../../strongs/h/h459.md) [raḇraḇ](../../strongs/h/h7260.md) [ḥêvā'](../../strongs/h/h2423.md), ['innûn](../../strongs/h/h581.md) are ['arbaʿ](../../strongs/h/h703.md), are ['arbaʿ](../../strongs/h/h703.md) [meleḵ](../../strongs/h/h4430.md), which shall [qûm](../../strongs/h/h6966.md) out [min](../../strongs/h/h4481.md) the ['ăraʿ](../../strongs/h/h772.md).

<a name="daniel_7_18"></a>Daniel 7:18

But the [qadîš](../../strongs/h/h6922.md) of the [ʿelyôn](../../strongs/h/h5946.md) shall [qᵊḇal](../../strongs/h/h6902.md) the [malkû](../../strongs/h/h4437.md), and [ḥăsan](../../strongs/h/h2631.md) the [malkû](../../strongs/h/h4437.md) [ʿaḏ](../../strongs/h/h5705.md) [ʿālam](../../strongs/h/h5957.md), even [ʿaḏ](../../strongs/h/h5705.md) [ʿālam](../../strongs/h/h5957.md) and [ʿālam](../../strongs/h/h5957.md).

<a name="daniel_7_19"></a>Daniel 7:19

['ĕḏayin](../../strongs/h/h116.md) I [ṣᵊḇâ](../../strongs/h/h6634.md) know the [yᵊṣēḇ](../../strongs/h/h3321.md) [ʿal](../../strongs/h/h5922.md) the [rᵊḇîʿay](../../strongs/h/h7244.md) [ḥêvā'](../../strongs/h/h2423.md), which [hăvâ](../../strongs/h/h1934.md) [šᵊnâ](../../strongs/h/h8133.md) [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) the others, [yatîr](../../strongs/h/h3493.md) [dᵊḥal](../../strongs/h/h1763.md), whose [šēn](../../strongs/h/h8128.md) were of [parzel](../../strongs/h/h6523.md), and his [ṭᵊp̄ar](../../strongs/h/h2953.md) of [nᵊḥāš](../../strongs/h/h5174.md); which ['ăḵal](../../strongs/h/h399.md), [dᵊqaq](../../strongs/h/h1855.md), and [rᵊp̄as](../../strongs/h/h7512.md) the [šᵊ'ār](../../strongs/h/h7606.md) with his [reḡel](../../strongs/h/h7271.md);

<a name="daniel_7_20"></a>Daniel 7:20

And [ʿal](../../strongs/h/h5922.md) the [ʿăśar](../../strongs/h/h6236.md) [qeren](../../strongs/h/h7162.md) that were in his [rē'š](../../strongs/h/h7217.md), and of the ['āḥŏrî](../../strongs/h/h317.md) which [sᵊlaq](../../strongs/h/h5559.md), and [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) whom [tᵊlāṯ](../../strongs/h/h8532.md) [nᵊp̄al](../../strongs/h/h5308.md); even of that [qeren](../../strongs/h/h7162.md) [dikēn](../../strongs/h/h1797.md) had [ʿayin](../../strongs/h/h5870.md), and a [pum](../../strongs/h/h6433.md) that [mᵊlal](../../strongs/h/h4449.md) [raḇraḇ](../../strongs/h/h7260.md), whose [ḥēzev](../../strongs/h/h2376.md) was [min](../../strongs/h/h4481.md) [raḇ](../../strongs/h/h7229.md) [min](../../strongs/h/h4481.md) his [ḥaḇrâ](../../strongs/h/h2273.md).

<a name="daniel_7_21"></a>Daniel 7:21

I [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md), and the [dikēn](../../strongs/h/h1797.md) [qeren](../../strongs/h/h7162.md) [ʿăḇaḏ](../../strongs/h/h5648.md) [qᵊrāḇ](../../strongs/h/h7129.md) [ʿim](../../strongs/h/h5974.md) the [qadîš](../../strongs/h/h6922.md), and [yᵊḵēl](../../strongs/h/h3202.md) against them;

<a name="daniel_7_22"></a>Daniel 7:22

[ʿaḏ](../../strongs/h/h5705.md) the [ʿatîq](../../strongs/h/h6268.md) of [yôm](../../strongs/h/h3118.md) ['ăṯâ](../../strongs/h/h858.md) [dî](../../strongs/h/h1768.md), and [dîn](../../strongs/h/h1780.md) was [yᵊhaḇ](../../strongs/h/h3052.md) to the [qadîš](../../strongs/h/h6922.md) of the most [ʿelyôn](../../strongs/h/h5946.md); and the [zᵊmān](../../strongs/h/h2166.md) [mᵊṭā'](../../strongs/h/h4291.md) that the [qadîš](../../strongs/h/h6922.md) [ḥăsan](../../strongs/h/h2631.md) the [malkû](../../strongs/h/h4437.md).

<a name="daniel_7_23"></a>Daniel 7:23

[kēn](../../strongs/h/h3652.md) he ['ămar](../../strongs/h/h560.md), The [rᵊḇîʿay](../../strongs/h/h7244.md) [ḥêvā'](../../strongs/h/h2423.md) shall [hăvâ](../../strongs/h/h1934.md) the [rᵊḇîʿay](../../strongs/h/h7244.md) [malkû](../../strongs/h/h4437.md) upon ['ăraʿ](../../strongs/h/h772.md), which shall be [šᵊnâ](../../strongs/h/h8133.md) [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) [malkû](../../strongs/h/h4437.md), and shall ['ăḵal](../../strongs/h/h399.md) the [kōl](../../strongs/h/h3606.md) ['ăraʿ](../../strongs/h/h772.md), and shall tread it [dûš](../../strongs/h/h1759.md), and break it in [dᵊqaq](../../strongs/h/h1855.md).

<a name="daniel_7_24"></a>Daniel 7:24

And the [ʿăśar](../../strongs/h/h6236.md) [qeren](../../strongs/h/h7162.md) out [min](../../strongs/h/h4481.md) this [malkû](../../strongs/h/h4437.md) are [ʿăśar](../../strongs/h/h6236.md) [meleḵ](../../strongs/h/h4430.md) that shall [qûm](../../strongs/h/h6966.md): and ['āḥŏrān](../../strongs/h/h321.md) shall [qûm](../../strongs/h/h6966.md) ['aḥar](../../strongs/h/h311.md) them; and he shall be [šᵊnâ](../../strongs/h/h8133.md) [min](../../strongs/h/h4481.md) the [qaḏmay](../../strongs/h/h6933.md), and he shall [šᵊp̄al](../../strongs/h/h8214.md) [tᵊlāṯ](../../strongs/h/h8532.md) [meleḵ](../../strongs/h/h4430.md).

<a name="daniel_7_25"></a>Daniel 7:25

And he shall [mᵊlal](../../strongs/h/h4449.md) great [millâ](../../strongs/h/h4406.md) [ṣaḏ](../../strongs/h/h6655.md) the most [ʿillay](../../strongs/h/h5943.md), and shall [bᵊlā'](../../strongs/h/h1080.md) the [qadîš](../../strongs/h/h6922.md) of the most [ʿelyôn](../../strongs/h/h5946.md), and [sᵊḇar](../../strongs/h/h5452.md) to [šᵊnâ](../../strongs/h/h8133.md) [zᵊmān](../../strongs/h/h2166.md) and [dāṯ](../../strongs/h/h1882.md): and they shall be [yᵊhaḇ](../../strongs/h/h3052.md) into his [yaḏ](../../strongs/h/h3028.md) [ʿaḏ](../../strongs/h/h5705.md) a [ʿidān](../../strongs/h/h5732.md) and [ʿidān](../../strongs/h/h5732.md) and the [pᵊlaḡ](../../strongs/h/h6387.md) of [ʿidān](../../strongs/h/h5732.md).

<a name="daniel_7_26"></a>Daniel 7:26

But the [dîn](../../strongs/h/h1780.md) shall [yᵊṯiḇ](../../strongs/h/h3488.md), and they shall take [ʿăḏā'](../../strongs/h/h5709.md) his [šālṭān](../../strongs/h/h7985.md), to [šᵊmaḏ](../../strongs/h/h8046.md) and to ['ăḇaḏ](../../strongs/h/h7.md) it [ʿaḏ](../../strongs/h/h5705.md) the [sôp̄](../../strongs/h/h5491.md).

<a name="daniel_7_27"></a>Daniel 7:27

And the [malkû](../../strongs/h/h4437.md) and [šālṭān](../../strongs/h/h7985.md), and the [rᵊḇû](../../strongs/h/h7238.md) of the [malkû](../../strongs/h/h4437.md) [tᵊḥôṯ](../../strongs/h/h8460.md) the [kōl](../../strongs/h/h3606.md) [šᵊmayin](../../strongs/h/h8065.md), shall be [yᵊhaḇ](../../strongs/h/h3052.md) to the [ʿam](../../strongs/h/h5972.md) of the [qadîš](../../strongs/h/h6922.md) of the [ʿelyôn](../../strongs/h/h5946.md), whose [malkû](../../strongs/h/h4437.md) is an [ʿālam](../../strongs/h/h5957.md) [malkû](../../strongs/h/h4437.md), and [kōl](../../strongs/h/h3606.md) [šālṭān](../../strongs/h/h7985.md) shall [pᵊlaḥ](../../strongs/h/h6399.md) and [šᵊmaʿ](../../strongs/h/h8086.md) him.

<a name="daniel_7_28"></a>Daniel 7:28

[ʿaḏ](../../strongs/h/h5705.md) [kô](../../strongs/h/h3542.md) is the [sôp̄](../../strongs/h/h5491.md) of the [millâ](../../strongs/h/h4406.md). As for ['ănā'](../../strongs/h/h576.md) [Dānîyē'L](../../strongs/h/h1841.md), my [raʿyôn](../../strongs/h/h7476.md) [śagî'](../../strongs/h/h7690.md) [bᵊhal](../../strongs/h/h927.md) me, and my [zîv](../../strongs/h/h2122.md) [šᵊnâ](../../strongs/h/h8133.md) in [ʿal](../../strongs/h/h5922.md): but I [nᵊṭar](../../strongs/h/h5202.md) the [millâ](../../strongs/h/h4406.md) in my [lēḇ](../../strongs/h/h3821.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 6](daniel_6.md) - [Daniel 8](daniel_8.md)