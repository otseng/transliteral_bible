# [Daniel 1](https://www.blueletterbible.org/kjv/daniel/1)

<a name="daniel_1_1"></a>Daniel 1:1

In the third [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [bow'](../../strongs/h/h935.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md), and [ṣûr](../../strongs/h/h6696.md) it.

<a name="daniel_1_2"></a>Daniel 1:2

And the ['adonay](../../strongs/h/h136.md) [nathan](../../strongs/h/h5414.md) [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) into his [yad](../../strongs/h/h3027.md), with [qᵊṣāṯ](../../strongs/h/h7117.md) of the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md): which he [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Šinʿār](../../strongs/h/h8152.md) to the [bayith](../../strongs/h/h1004.md) of his ['Elohiym](../../strongs/h/h430.md); and he [bow'](../../strongs/h/h935.md) the [kĕliy](../../strongs/h/h3627.md) into the ['ôṣār](../../strongs/h/h214.md) [bayith](../../strongs/h/h1004.md) of his ['Elohiym](../../strongs/h/h430.md).

<a name="daniel_1_3"></a>Daniel 1:3

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto ['Ašpᵊnaz](../../strongs/h/h828.md) the [rab](../../strongs/h/h7227.md) of his [sārîs](../../strongs/h/h5631.md), that he should [bow'](../../strongs/h/h935.md) certain of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and of the [mᵊlûḵâ](../../strongs/h/h4410.md) [zera'](../../strongs/h/h2233.md), and of the [partᵊmîm](../../strongs/h/h6579.md);

<a name="daniel_1_4"></a>Daniel 1:4

[yeleḏ](../../strongs/h/h3206.md) in whom was no [mᵊ'ûm](../../strongs/h/h3971.md) [mᵊ'ûm](../../strongs/h/h3971.md), but [towb](../../strongs/h/h2896.md) [mar'ê](../../strongs/h/h4758.md), and [sakal](../../strongs/h/h7919.md) in all [ḥāḵmâ](../../strongs/h/h2451.md), and [yada'](../../strongs/h/h3045.md) in [da'ath](../../strongs/h/h1847.md), and [bîn](../../strongs/h/h995.md) [madāʿ](../../strongs/h/h4093.md), and such as had [koach](../../strongs/h/h3581.md) in them to ['amad](../../strongs/h/h5975.md) in the [melek](../../strongs/h/h4428.md) [heykal](../../strongs/h/h1964.md), and whom they might [lamad](../../strongs/h/h3925.md) the [sēp̄er](../../strongs/h/h5612.md) and the [lashown](../../strongs/h/h3956.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="daniel_1_5"></a>Daniel 1:5

And the [melek](../../strongs/h/h4428.md) [mānâ](../../strongs/h/h4487.md) them a [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md) of the [melek](../../strongs/h/h4428.md) [p̄ṯ-ḇḡ](../../strongs/h/h6598.md), and of the [yayin](../../strongs/h/h3196.md) which he [mištê](../../strongs/h/h4960.md): so [gāḏal](../../strongs/h/h1431.md) them three [šānâ](../../strongs/h/h8141.md), that at the [qᵊṣāṯ](../../strongs/h/h7117.md) thereof they might ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="daniel_1_6"></a>Daniel 1:6

Now among these were of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), [Dinîyē'L](../../strongs/h/h1840.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Mîšā'ēl](../../strongs/h/h4332.md), and [ʿĂzaryâ](../../strongs/h/h5838.md):

<a name="daniel_1_7"></a>Daniel 1:7

Unto whom the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md) [śûm](../../strongs/h/h7760.md) [shem](../../strongs/h/h8034.md): for he [śûm](../../strongs/h/h7760.md) unto [Dinîyē'L](../../strongs/h/h1840.md) the name of [Bēlṭᵊša'Ṣṣar](../../strongs/h/h1095.md); and to [Ḥănanyâ](../../strongs/h/h2608.md), of [Šaḏraḵ](../../strongs/h/h7714.md); and to [Mîšā'ēl](../../strongs/h/h4332.md), of [Mêšaḵ](../../strongs/h/h4335.md); and to [ʿĂzaryâ](../../strongs/h/h5838.md), of [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5664.md).

<a name="daniel_1_8"></a>Daniel 1:8

But [Dinîyē'L](../../strongs/h/h1840.md) [śûm](../../strongs/h/h7760.md) in his [leb](../../strongs/h/h3820.md) that he would not [gā'al](../../strongs/h/h1351.md) himself with the [p̄ṯ-ḇḡ](../../strongs/h/h6598.md) of the [melek](../../strongs/h/h4428.md) [p̄ṯ-ḇḡ](../../strongs/h/h6598.md), nor with the [yayin](../../strongs/h/h3196.md) which he [mištê](../../strongs/h/h4960.md): therefore he [bāqaš](../../strongs/h/h1245.md) of the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md) that he might not [gā'al](../../strongs/h/h1351.md) himself.

<a name="daniel_1_9"></a>Daniel 1:9

Now ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) [Dinîyē'L](../../strongs/h/h1840.md) into [checed](../../strongs/h/h2617.md) and [raḥam](../../strongs/h/h7356.md) [paniym](../../strongs/h/h6440.md) the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md).

<a name="daniel_1_10"></a>Daniel 1:10

And the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md) ['āmar](../../strongs/h/h559.md) unto [Dinîyē'L](../../strongs/h/h1840.md), I [yārē'](../../strongs/h/h3373.md) my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), who hath [mānâ](../../strongs/h/h4487.md) your [ma'akal](../../strongs/h/h3978.md) and your [mištê](../../strongs/h/h4960.md): for why should he [ra'ah](../../strongs/h/h7200.md) your [paniym](../../strongs/h/h6440.md) worse [zāʿap̄](../../strongs/h/h2196.md) than the [yeleḏ](../../strongs/h/h3206.md) which are of your [gîl](../../strongs/h/h1524.md)? then shall ye make me [ḥûḇ](../../strongs/h/h2325.md) my [ro'sh](../../strongs/h/h7218.md) to the [melek](../../strongs/h/h4428.md).

<a name="daniel_1_11"></a>Daniel 1:11

Then ['āmar](../../strongs/h/h559.md) [Dinîyē'L](../../strongs/h/h1840.md) to [melṣar](../../strongs/h/h4453.md), whom the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md) had [mānâ](../../strongs/h/h4487.md) over [Dinîyē'L](../../strongs/h/h1840.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Mîšā'ēl](../../strongs/h/h4332.md), and [ʿĂzaryâ](../../strongs/h/h5838.md),

<a name="daniel_1_12"></a>Daniel 1:12

[nāsâ](../../strongs/h/h5254.md) thy ['ebed](../../strongs/h/h5650.md), I beseech thee, ten [yowm](../../strongs/h/h3117.md); and let them [nathan](../../strongs/h/h5414.md) us [zērʿōn](../../strongs/h/h2235.md) to ['akal](../../strongs/h/h398.md), and [mayim](../../strongs/h/h4325.md) to [šāṯâ](../../strongs/h/h8354.md).

<a name="daniel_1_13"></a>Daniel 1:13

Then let our [mar'ê](../../strongs/h/h4758.md) be [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) thee, and the [mar'ê](../../strongs/h/h4758.md) of the [yeleḏ](../../strongs/h/h3206.md) that ['akal](../../strongs/h/h398.md) of the [p̄ṯ-ḇḡ](../../strongs/h/h6598.md) of the [melek](../../strongs/h/h4428.md) [p̄ṯ-ḇḡ](../../strongs/h/h6598.md): and as thou [ra'ah](../../strongs/h/h7200.md), ['asah](../../strongs/h/h6213.md) with thy ['ebed](../../strongs/h/h5650.md).

<a name="daniel_1_14"></a>Daniel 1:14

So he [shama'](../../strongs/h/h8085.md) to them in this [dabar](../../strongs/h/h1697.md), and [nāsâ](../../strongs/h/h5254.md) them ten [yowm](../../strongs/h/h3117.md).

<a name="daniel_1_15"></a>Daniel 1:15

And at the [qᵊṣāṯ](../../strongs/h/h7117.md) of ten [yowm](../../strongs/h/h3117.md) their [mar'ê](../../strongs/h/h4758.md) [ra'ah](../../strongs/h/h7200.md) [towb](../../strongs/h/h2896.md) and [bārî'](../../strongs/h/h1277.md) in [basar](../../strongs/h/h1320.md) than all the [yeleḏ](../../strongs/h/h3206.md) which did ['akal](../../strongs/h/h398.md) the [p̄ṯ-ḇḡ](../../strongs/h/h6598.md) of the [melek](../../strongs/h/h4428.md) [p̄ṯ-ḇḡ](../../strongs/h/h6598.md).

<a name="daniel_1_16"></a>Daniel 1:16

Thus [melṣar](../../strongs/h/h4453.md) [nasa'](../../strongs/h/h5375.md) the portion of their [p̄ṯ-ḇḡ](../../strongs/h/h6598.md), and the [yayin](../../strongs/h/h3196.md) that they should [mištê](../../strongs/h/h4960.md); and [nathan](../../strongs/h/h5414.md) them [zērʿōn](../../strongs/h/h2235.md).

<a name="daniel_1_17"></a>Daniel 1:17

As for these four [yeleḏ](../../strongs/h/h3206.md), ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) them [madāʿ](../../strongs/h/h4093.md) and [sakal](../../strongs/h/h7919.md) in all [sēp̄er](../../strongs/h/h5612.md) and [ḥāḵmâ](../../strongs/h/h2451.md): and [Dinîyē'L](../../strongs/h/h1840.md) had [bîn](../../strongs/h/h995.md) in all [ḥāzôn](../../strongs/h/h2377.md) and [ḥălôm](../../strongs/h/h2472.md).

<a name="daniel_1_18"></a>Daniel 1:18

Now at the [qᵊṣāṯ](../../strongs/h/h7117.md) of the [yowm](../../strongs/h/h3117.md) that the [melek](../../strongs/h/h4428.md) had ['āmar](../../strongs/h/h559.md) he should [bow'](../../strongs/h/h935.md) them in, then the [śar](../../strongs/h/h8269.md) of the [sārîs](../../strongs/h/h5631.md) [bow'](../../strongs/h/h935.md) them in [paniym](../../strongs/h/h6440.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md).

<a name="daniel_1_19"></a>Daniel 1:19

And the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1696.md) with them; and among them all was [māṣā'](../../strongs/h/h4672.md) none like [Dinîyē'L](../../strongs/h/h1840.md), [Ḥănanyâ](../../strongs/h/h2608.md), [Mîšā'ēl](../../strongs/h/h4332.md), and [ʿĂzaryâ](../../strongs/h/h5838.md): therefore ['amad](../../strongs/h/h5975.md) they [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="daniel_1_20"></a>Daniel 1:20

And in all [dabar](../../strongs/h/h1697.md) of [ḥāḵmâ](../../strongs/h/h2451.md) and [bînâ](../../strongs/h/h998.md), that the [melek](../../strongs/h/h4428.md) [bāqaš](../../strongs/h/h1245.md) of them, he [māṣā'](../../strongs/h/h4672.md) them ten times [yad](../../strongs/h/h3027.md) than all the [ḥarṭōm](../../strongs/h/h2748.md) and ['aššāp̄](../../strongs/h/h825.md) that were in all his [malkuwth](../../strongs/h/h4438.md).

<a name="daniel_1_21"></a>Daniel 1:21

And [Dinîyē'L](../../strongs/h/h1840.md) continued even unto the first [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Kôreš](../../strongs/h/h3566.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 2](daniel_2.md)