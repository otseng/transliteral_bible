# [Daniel 11](https://www.blueletterbible.org/kjv/daniel/11)

<a name="daniel_11_1"></a>Daniel 11:1

Also I in the first [šānâ](../../strongs/h/h8141.md) of [Daryāveš](../../strongs/h/h1867.md) the [Māḏay](../../strongs/h/h4075.md), even I, [ʿōmeḏ](../../strongs/h/h5977.md) to [ḥāzaq](../../strongs/h/h2388.md) and to [māʿôz](../../strongs/h/h4581.md) him.

<a name="daniel_11_2"></a>Daniel 11:2

And now will I [nāḡaḏ](../../strongs/h/h5046.md) thee the ['emeth](../../strongs/h/h571.md). Behold, there shall ['amad](../../strongs/h/h5975.md) yet three [melek](../../strongs/h/h4428.md) in [Pāras](../../strongs/h/h6539.md); and the fourth shall be [ʿōšer](../../strongs/h/h6239.md) [ʿāšar](../../strongs/h/h6238.md) than they [gadowl](../../strongs/h/h1419.md): and by his [ḥezqâ](../../strongs/h/h2393.md) through his [ʿōšer](../../strongs/h/h6239.md) he shall stir [ʿûr](../../strongs/h/h5782.md) all against the [malkuwth](../../strongs/h/h4438.md) of [Yāvān](../../strongs/h/h3120.md).

<a name="daniel_11_3"></a>Daniel 11:3

And a [gibôr](../../strongs/h/h1368.md) [melek](../../strongs/h/h4428.md) shall ['amad](../../strongs/h/h5975.md), that shall [mashal](../../strongs/h/h4910.md) with [rab](../../strongs/h/h7227.md) [mimšāl](../../strongs/h/h4474.md), and ['asah](../../strongs/h/h6213.md) according to his [ratsown](../../strongs/h/h7522.md).

<a name="daniel_11_4"></a>Daniel 11:4

And when he shall ['amad](../../strongs/h/h5975.md), his [malkuwth](../../strongs/h/h4438.md) shall be [shabar](../../strongs/h/h7665.md), and shall be [ḥāṣâ](../../strongs/h/h2673.md) toward the four [ruwach](../../strongs/h/h7307.md) of [shamayim](../../strongs/h/h8064.md); and not to his ['aḥărîṯ](../../strongs/h/h319.md), nor according to his [mōšel](../../strongs/h/h4915.md) which he [mashal](../../strongs/h/h4910.md): for his [malkuwth](../../strongs/h/h4438.md) shall be [nathash](../../strongs/h/h5428.md), even for ['aḥēr](../../strongs/h/h312.md) beside those.

<a name="daniel_11_5"></a>Daniel 11:5

And the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall be [ḥāzaq](../../strongs/h/h2388.md), and one of his [śar](../../strongs/h/h8269.md); and he shall be [ḥāzaq](../../strongs/h/h2388.md) above him, and have [mashal](../../strongs/h/h4910.md); his [memshalah](../../strongs/h/h4475.md) shall be a [rab](../../strongs/h/h7227.md) [mimšāl](../../strongs/h/h4474.md).

<a name="daniel_11_6"></a>Daniel 11:6

And in the [qēṣ](../../strongs/h/h7093.md) of [šānâ](../../strongs/h/h8141.md) they shall [ḥāḇar](../../strongs/h/h2266.md) themselves; for the [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) to ['asah](../../strongs/h/h6213.md) a [meyshar](../../strongs/h/h4339.md): but she shall not [ʿāṣar](../../strongs/h/h6113.md) the [koach](../../strongs/h/h3581.md) of the [zerowa'](../../strongs/h/h2220.md); neither shall he ['amad](../../strongs/h/h5975.md), nor his [zerowa'](../../strongs/h/h2220.md): but she shall be [nathan](../../strongs/h/h5414.md), and they that [bow'](../../strongs/h/h935.md) her, and he that [yalad](../../strongs/h/h3205.md) her, and he that [ḥāzaq](../../strongs/h/h2388.md) her in these [ʿēṯ](../../strongs/h/h6256.md).

<a name="daniel_11_7"></a>Daniel 11:7

But out of a [nēṣer](../../strongs/h/h5342.md) of her [šereš](../../strongs/h/h8328.md) shall one ['amad](../../strongs/h/h5975.md) in his [kēn](../../strongs/h/h3653.md), which shall [bow'](../../strongs/h/h935.md) with an [ḥayil](../../strongs/h/h2428.md), and shall [bow'](../../strongs/h/h935.md) into the [māʿôz](../../strongs/h/h4581.md) of the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), and shall ['asah](../../strongs/h/h6213.md) against them, and shall [ḥāzaq](../../strongs/h/h2388.md):

<a name="daniel_11_8"></a>Daniel 11:8

And shall also [bow'](../../strongs/h/h935.md) [šᵊḇî](../../strongs/h/h7628.md) into [Mitsrayim](../../strongs/h/h4714.md) their ['Elohiym](../../strongs/h/h430.md), with their [nāsîḵ](../../strongs/h/h5257.md), and with their [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md) and of [zāhāḇ](../../strongs/h/h2091.md); and he shall ['amad](../../strongs/h/h5975.md) more [šānâ](../../strongs/h/h8141.md) than the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="daniel_11_9"></a>Daniel 11:9

So the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall [bow'](../../strongs/h/h935.md) into his [malkuwth](../../strongs/h/h4438.md), and shall [shuwb](../../strongs/h/h7725.md) into his own ['ăḏāmâ](../../strongs/h/h127.md).

<a name="daniel_11_10"></a>Daniel 11:10

But his [ben](../../strongs/h/h1121.md) shall be [gārâ](../../strongs/h/h1624.md), and shall ['āsap̄](../../strongs/h/h622.md) a [hāmôn](../../strongs/h/h1995.md) of [rab](../../strongs/h/h7227.md) [ḥayil](../../strongs/h/h2428.md): and one shall [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md), and [šāṭap̄](../../strongs/h/h7857.md), and ['abar](../../strongs/h/h5674.md): then shall he [shuwb](../../strongs/h/h7725.md), and be [gārâ](../../strongs/h/h1624.md), even to his [māʿôz](../../strongs/h/h4581.md).

<a name="daniel_11_11"></a>Daniel 11:11

And the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall be [mārar](../../strongs/h/h4843.md), and shall [yāṣā'](../../strongs/h/h3318.md) and [lāḥam](../../strongs/h/h3898.md) with him, even with the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md): and he shall set ['amad](../../strongs/h/h5975.md) a [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md); but the [hāmôn](../../strongs/h/h1995.md) shall be [nathan](../../strongs/h/h5414.md) into his [yad](../../strongs/h/h3027.md).

<a name="daniel_11_12"></a>Daniel 11:12

And when he hath [nasa'](../../strongs/h/h5375.md) the [hāmôn](../../strongs/h/h1995.md), his [lebab](../../strongs/h/h3824.md) shall be lifted [ruwm](../../strongs/h/h7311.md) [ruwm](../../strongs/h/h7311.md); and he shall [naphal](../../strongs/h/h5307.md) many ten [ribô'](../../strongs/h/h7239.md): but he shall not be ['azaz](../../strongs/h/h5810.md) by it.

<a name="daniel_11_13"></a>Daniel 11:13

For the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) shall [shuwb](../../strongs/h/h7725.md), and shall set ['amad](../../strongs/h/h5975.md) a [hāmôn](../../strongs/h/h1995.md) [rab](../../strongs/h/h7227.md) than the [ri'šôn](../../strongs/h/h7223.md), and shall [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md) [qēṣ](../../strongs/h/h7093.md) [ʿēṯ](../../strongs/h/h6256.md) [šānâ](../../strongs/h/h8141.md) with a [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md) and with [rab](../../strongs/h/h7227.md) [rᵊḵûš](../../strongs/h/h7399.md).

<a name="daniel_11_14"></a>Daniel 11:14

And in those [ʿēṯ](../../strongs/h/h6256.md) there shall [rab](../../strongs/h/h7227.md) ['amad](../../strongs/h/h5975.md) against the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md): also the [ben](../../strongs/h/h1121.md) [periyts](../../strongs/h/h6530.md) of thy ['am](../../strongs/h/h5971.md) shall [nasa'](../../strongs/h/h5375.md) themselves to ['amad](../../strongs/h/h5975.md) the [ḥāzôn](../../strongs/h/h2377.md); but they shall [kashal](../../strongs/h/h3782.md).

<a name="daniel_11_15"></a>Daniel 11:15

So the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) shall [bow'](../../strongs/h/h935.md), and [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md), and [lāḵaḏ](../../strongs/h/h3920.md) the most [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md): and the [zerowa'](../../strongs/h/h2220.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall not ['amad](../../strongs/h/h5975.md), neither his [miḇḥār](../../strongs/h/h4005.md) ['am](../../strongs/h/h5971.md), neither shall there be any [koach](../../strongs/h/h3581.md) to ['amad](../../strongs/h/h5975.md).

<a name="daniel_11_16"></a>Daniel 11:16

But he that [bow'](../../strongs/h/h935.md) against him shall ['asah](../../strongs/h/h6213.md) according to his own [ratsown](../../strongs/h/h7522.md), and none shall ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him: and he shall ['amad](../../strongs/h/h5975.md) in the [ṣᵊḇî](../../strongs/h/h6643.md) ['erets](../../strongs/h/h776.md), which by his [yad](../../strongs/h/h3027.md) shall be [kālâ](../../strongs/h/h3617.md).

<a name="daniel_11_17"></a>Daniel 11:17

He shall also [śûm](../../strongs/h/h7760.md) his [paniym](../../strongs/h/h6440.md) to [bow'](../../strongs/h/h935.md) with the [tōqep̄](../../strongs/h/h8633.md) of his whole [malkuwth](../../strongs/h/h4438.md), and upright [yashar](../../strongs/h/h3477.md) with him; thus shall he ['asah](../../strongs/h/h6213.md): and he shall [nathan](../../strongs/h/h5414.md) him the [bath](../../strongs/h/h1323.md) of ['ishshah](../../strongs/h/h802.md), [shachath](../../strongs/h/h7843.md) her: but she shall not ['amad](../../strongs/h/h5975.md) on his side, neither be for him.

<a name="daniel_11_18"></a>Daniel 11:18

After this shall he [śûm](../../strongs/h/h7760.md) [shuwb](../../strongs/h/h7725.md) his [paniym](../../strongs/h/h6440.md) unto the ['î](../../strongs/h/h339.md), and shall [lāḵaḏ](../../strongs/h/h3920.md) [rab](../../strongs/h/h7227.md): but a [qāṣîn](../../strongs/h/h7101.md) for his own behalf shall cause the [cherpah](../../strongs/h/h2781.md) offered by him to [shabath](../../strongs/h/h7673.md); without his own [cherpah](../../strongs/h/h2781.md) he shall cause it to [shuwb](../../strongs/h/h7725.md) upon him.

<a name="daniel_11_19"></a>Daniel 11:19

Then he shall [shuwb](../../strongs/h/h7725.md) his [paniym](../../strongs/h/h6440.md) toward the [māʿôz](../../strongs/h/h4581.md) of his own ['erets](../../strongs/h/h776.md): but he shall [kashal](../../strongs/h/h3782.md) and [naphal](../../strongs/h/h5307.md), and not be [māṣā'](../../strongs/h/h4672.md).

<a name="daniel_11_20"></a>Daniel 11:20

Then shall ['amad](../../strongs/h/h5975.md) in his [kēn](../../strongs/h/h3653.md) an ['abar](../../strongs/h/h5674.md) of [nāḡaś](../../strongs/h/h5065.md) in the [heḏer](../../strongs/h/h1925.md) of the [malkuwth](../../strongs/h/h4438.md): but within few [yowm](../../strongs/h/h3117.md) he shall be [shabar](../../strongs/h/h7665.md), neither in ['aph](../../strongs/h/h639.md), nor in [milḥāmâ](../../strongs/h/h4421.md).

<a name="daniel_11_21"></a>Daniel 11:21

And in his [kēn](../../strongs/h/h3653.md) shall ['amad](../../strongs/h/h5975.md) a [bazah](../../strongs/h/h959.md), to whom they shall not [nathan](../../strongs/h/h5414.md) the [howd](../../strongs/h/h1935.md) of the [malkuwth](../../strongs/h/h4438.md): but he shall [bow'](../../strongs/h/h935.md) in [šalvâ](../../strongs/h/h7962.md), and [ḥāzaq](../../strongs/h/h2388.md) the [malkuwth](../../strongs/h/h4438.md) by [ḥălaqlaqqôṯ](../../strongs/h/h2519.md).

<a name="daniel_11_22"></a>Daniel 11:22

And with the [zerowa'](../../strongs/h/h2220.md) of a [šeṭep̄](../../strongs/h/h7858.md) shall they be [šāṭap̄](../../strongs/h/h7857.md) from [paniym](../../strongs/h/h6440.md) him, and shall be [shabar](../../strongs/h/h7665.md); yea, also the [nāḡîḏ](../../strongs/h/h5057.md) of the [bĕriyth](../../strongs/h/h1285.md).

<a name="daniel_11_23"></a>Daniel 11:23

And after the [ḥāḇar](../../strongs/h/h2266.md) made with him he shall ['asah](../../strongs/h/h6213.md) [mirmah](../../strongs/h/h4820.md): for he shall [ʿālâ](../../strongs/h/h5927.md), and shall become [ʿāṣam](../../strongs/h/h6105.md) with a [mᵊʿaṭ](../../strongs/h/h4592.md) [gowy](../../strongs/h/h1471.md).

<a name="daniel_11_24"></a>Daniel 11:24

He shall [bow'](../../strongs/h/h935.md) [šalvâ](../../strongs/h/h7962.md) even upon the [mišmān](../../strongs/h/h4924.md) of the [mᵊḏînâ](../../strongs/h/h4082.md); and he shall ['asah](../../strongs/h/h6213.md) that which his ['ab](../../strongs/h/h1.md) have not ['asah](../../strongs/h/h6213.md), nor his ['ab](../../strongs/h/h1.md) ['ab](../../strongs/h/h1.md); he shall [bāzar](../../strongs/h/h967.md) among them the [bizzâ](../../strongs/h/h961.md), and [šālāl](../../strongs/h/h7998.md), and [rᵊḵûš](../../strongs/h/h7399.md): yea, and he shall [chashab](../../strongs/h/h2803.md) his [maḥăšāḇâ](../../strongs/h/h4284.md) against the [miḇṣār](../../strongs/h/h4013.md), even for a [ʿēṯ](../../strongs/h/h6256.md).

<a name="daniel_11_25"></a>Daniel 11:25

And he shall stir [ʿûr](../../strongs/h/h5782.md) his [koach](../../strongs/h/h3581.md) and his [lebab](../../strongs/h/h3824.md) against the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) with a [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md); and the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall be stirred [gārâ](../../strongs/h/h1624.md) to [milḥāmâ](../../strongs/h/h4421.md) with a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) and ['atsuwm](../../strongs/h/h6099.md) [ḥayil](../../strongs/h/h2428.md); but he shall not ['amad](../../strongs/h/h5975.md): for they shall [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md) against him.

<a name="daniel_11_26"></a>Daniel 11:26

Yea, they that ['akal](../../strongs/h/h398.md) of the portion of his [p̄ṯ-ḇḡ](../../strongs/h/h6598.md) shall [shabar](../../strongs/h/h7665.md) him, and his [ḥayil](../../strongs/h/h2428.md) shall [šāṭap̄](../../strongs/h/h7857.md): and [rab](../../strongs/h/h7227.md) shall [naphal](../../strongs/h/h5307.md) down [ḥālāl](../../strongs/h/h2491.md).

<a name="daniel_11_27"></a>Daniel 11:27

And both these [melek](../../strongs/h/h4428.md) [lebab](../../strongs/h/h3824.md) shall be to do [mēraʿ](../../strongs/h/h4827.md) [ra'](../../strongs/h/h7451.md), and they shall [dabar](../../strongs/h/h1696.md) [kazab](../../strongs/h/h3577.md) at one [šulḥān](../../strongs/h/h7979.md); but it shall not [tsalach](../../strongs/h/h6743.md): for yet the [qēṣ](../../strongs/h/h7093.md) shall be at the time [môʿēḏ](../../strongs/h/h4150.md).

<a name="daniel_11_28"></a>Daniel 11:28

Then shall he [shuwb](../../strongs/h/h7725.md) into his ['erets](../../strongs/h/h776.md) with [gadowl](../../strongs/h/h1419.md) [rᵊḵûš](../../strongs/h/h7399.md); and his [lebab](../../strongs/h/h3824.md) shall be against the [qodesh](../../strongs/h/h6944.md) [bĕriyth](../../strongs/h/h1285.md); and he shall ['asah](../../strongs/h/h6213.md), and [shuwb](../../strongs/h/h7725.md) to his own ['erets](../../strongs/h/h776.md).

<a name="daniel_11_29"></a>Daniel 11:29

At the time [môʿēḏ](../../strongs/h/h4150.md) he shall [shuwb](../../strongs/h/h7725.md), and [bow'](../../strongs/h/h935.md) toward the [neḡeḇ](../../strongs/h/h5045.md); but it shall not be as the [ri'šôn](../../strongs/h/h7223.md), or as the ['aḥărôn](../../strongs/h/h314.md).

<a name="daniel_11_30"></a>Daniel 11:30

For the [ṣî](../../strongs/h/h6716.md) of [Kitîm](../../strongs/h/h3794.md) shall [bow'](../../strongs/h/h935.md) against him: therefore he shall be [kā'â](../../strongs/h/h3512.md), and [shuwb](../../strongs/h/h7725.md), and have [za'am](../../strongs/h/h2194.md) against the [qodesh](../../strongs/h/h6944.md) [bĕriyth](../../strongs/h/h1285.md): so shall he ['asah](../../strongs/h/h6213.md); he shall even [shuwb](../../strongs/h/h7725.md), and have [bîn](../../strongs/h/h995.md) with them that ['azab](../../strongs/h/h5800.md) the [qodesh](../../strongs/h/h6944.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="daniel_11_31"></a>Daniel 11:31

And [zerowa'](../../strongs/h/h2220.md) shall ['amad](../../strongs/h/h5975.md) on his part, and they shall [ḥālal](../../strongs/h/h2490.md) the [miqdash](../../strongs/h/h4720.md) of [māʿôz](../../strongs/h/h4581.md), and shall [cuwr](../../strongs/h/h5493.md) the [tāmîḏ](../../strongs/h/h8548.md), and they shall [nathan](../../strongs/h/h5414.md) the [šiqqûṣ](../../strongs/h/h8251.md) that maketh [šāmēm](../../strongs/h/h8074.md).

<a name="daniel_11_32"></a>Daniel 11:32

And such as do [rāšaʿ](../../strongs/h/h7561.md) against the [bĕriyth](../../strongs/h/h1285.md) shall he [ḥānēp̄](../../strongs/h/h2610.md) by [ḥălaqqâ](../../strongs/h/h2514.md): but the ['am](../../strongs/h/h5971.md) that do [yada'](../../strongs/h/h3045.md) their ['Elohiym](../../strongs/h/h430.md) shall be [ḥāzaq](../../strongs/h/h2388.md), and ['asah](../../strongs/h/h6213.md).

<a name="daniel_11_33"></a>Daniel 11:33

And they that [sakal](../../strongs/h/h7919.md) among the ['am](../../strongs/h/h5971.md) shall [bîn](../../strongs/h/h995.md) [rab](../../strongs/h/h7227.md): yet they shall [kashal](../../strongs/h/h3782.md) by the [chereb](../../strongs/h/h2719.md), and by [lehāḇâ](../../strongs/h/h3852.md), by [šᵊḇî](../../strongs/h/h7628.md), and by [bizzâ](../../strongs/h/h961.md), many [yowm](../../strongs/h/h3117.md).

<a name="daniel_11_34"></a>Daniel 11:34

Now when they shall [kashal](../../strongs/h/h3782.md), they shall be [ʿāzar](../../strongs/h/h5826.md) with a [mᵊʿaṭ](../../strongs/h/h4592.md) ['ezer](../../strongs/h/h5828.md): but [rab](../../strongs/h/h7227.md) shall [lāvâ](../../strongs/h/h3867.md) to them with [ḥălaqlaqqôṯ](../../strongs/h/h2519.md).

<a name="daniel_11_35"></a>Daniel 11:35

And some of them of [sakal](../../strongs/h/h7919.md) shall [kashal](../../strongs/h/h3782.md), to [tsaraph](../../strongs/h/h6884.md) them, and to [bārar](../../strongs/h/h1305.md), and to make them [lāḇan](../../strongs/h/h3835.md), even to the [ʿēṯ](../../strongs/h/h6256.md) of the [qēṣ](../../strongs/h/h7093.md): because it is yet for a time [môʿēḏ](../../strongs/h/h4150.md).

<a name="daniel_11_36"></a>Daniel 11:36

And the [melek](../../strongs/h/h4428.md) shall ['asah](../../strongs/h/h6213.md) according to his [ratsown](../../strongs/h/h7522.md); and he shall [ruwm](../../strongs/h/h7311.md) himself, and [gāḏal](../../strongs/h/h1431.md) himself above every ['el](../../strongs/h/h410.md), and shall [dabar](../../strongs/h/h1696.md) [pala'](../../strongs/h/h6381.md) against the ['el](../../strongs/h/h410.md) of ['el](../../strongs/h/h410.md), and shall [tsalach](../../strongs/h/h6743.md) till the [zaʿam](../../strongs/h/h2195.md) be [kalah](../../strongs/h/h3615.md): for that that is [ḥāraṣ](../../strongs/h/h2782.md) shall be ['asah](../../strongs/h/h6213.md).

<a name="daniel_11_37"></a>Daniel 11:37

Neither shall he [bîn](../../strongs/h/h995.md) the ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md), nor the [ḥemdâ](../../strongs/h/h2532.md) of ['ishshah](../../strongs/h/h802.md), nor [bîn](../../strongs/h/h995.md) any ['ĕlvôha](../../strongs/h/h433.md): for he shall [gāḏal](../../strongs/h/h1431.md) himself above all.

<a name="daniel_11_38"></a>Daniel 11:38

But in his [kēn](../../strongs/h/h3653.md) shall he [kabad](../../strongs/h/h3513.md) the ['ĕlvôha](../../strongs/h/h433.md) of [māʿôz](../../strongs/h/h4581.md): and an ['ĕlvôha](../../strongs/h/h433.md) whom his ['ab](../../strongs/h/h1.md) [yada'](../../strongs/h/h3045.md) not shall he [kabad](../../strongs/h/h3513.md) with [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), and with [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), and [ḥemdâ](../../strongs/h/h2532.md).

<a name="daniel_11_39"></a>Daniel 11:39

Thus shall he ['asah](../../strongs/h/h6213.md) in the most [māʿôz](../../strongs/h/h4581.md) [miḇṣār](../../strongs/h/h4013.md) with a [nēḵār](../../strongs/h/h5236.md) ['ĕlvôha](../../strongs/h/h433.md), whom he shall [nāḵar](../../strongs/h/h5234.md) [nāḵar](../../strongs/h/h5234.md) and [rabah](../../strongs/h/h7235.md) with [kabowd](../../strongs/h/h3519.md): and he shall cause them to [mashal](../../strongs/h/h4910.md) over [rab](../../strongs/h/h7227.md), and shall [chalaq](../../strongs/h/h2505.md) the ['ăḏāmâ](../../strongs/h/h127.md) for [mᵊḥîr](../../strongs/h/h4242.md).

<a name="daniel_11_40"></a>Daniel 11:40

And at the [ʿēṯ](../../strongs/h/h6256.md) of the [qēṣ](../../strongs/h/h7093.md) shall the [melek](../../strongs/h/h4428.md) of the [neḡeḇ](../../strongs/h/h5045.md) [nāḡaḥ](../../strongs/h/h5055.md) at him: and the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) shall come against him like a [śāʿar](../../strongs/h/h8175.md), with [reḵeḇ](../../strongs/h/h7393.md), and with [pārāš](../../strongs/h/h6571.md), and with [rab](../../strongs/h/h7227.md) ['ŏnîyâ](../../strongs/h/h591.md); and he shall [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md), and shall [šāṭap̄](../../strongs/h/h7857.md) and ['abar](../../strongs/h/h5674.md).

<a name="daniel_11_41"></a>Daniel 11:41

He shall [bow'](../../strongs/h/h935.md) also into the [ṣᵊḇî](../../strongs/h/h6643.md) ['erets](../../strongs/h/h776.md), and [rab](../../strongs/h/h7227.md) countries shall be [kashal](../../strongs/h/h3782.md): but these shall [mālaṭ](../../strongs/h/h4422.md) out of his [yad](../../strongs/h/h3027.md), even ['Ĕḏōm](../../strongs/h/h123.md), and [Mô'āḇ](../../strongs/h/h4124.md), and the [re'shiyth](../../strongs/h/h7225.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md).

<a name="daniel_11_42"></a>Daniel 11:42

He shall [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md) also upon the ['erets](../../strongs/h/h776.md): and the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) shall not [pᵊlêṭâ](../../strongs/h/h6413.md).

<a name="daniel_11_43"></a>Daniel 11:43

But he shall have [mashal](../../strongs/h/h4910.md) over the [miḵman](../../strongs/h/h4362.md) of [zāhāḇ](../../strongs/h/h2091.md) and of [keceph](../../strongs/h/h3701.md), and over all the [ḥemdâ](../../strongs/h/h2532.md) things of [Mitsrayim](../../strongs/h/h4714.md): and the [Luḇî](../../strongs/h/h3864.md) and the [Kûšî](../../strongs/h/h3569.md) shall be at his [miṣʿāḏ](../../strongs/h/h4703.md).

<a name="daniel_11_44"></a>Daniel 11:44

But [šᵊmûʿâ](../../strongs/h/h8052.md) out of the [mizrach](../../strongs/h/h4217.md) and out of the [ṣāp̄ôn](../../strongs/h/h6828.md) shall [bahal](../../strongs/h/h926.md) him: therefore he shall [yāṣā'](../../strongs/h/h3318.md) with [gadowl](../../strongs/h/h1419.md) [chemah](../../strongs/h/h2534.md) to [šāmaḏ](../../strongs/h/h8045.md), and utterly to make [ḥāram](../../strongs/h/h2763.md) [rab](../../strongs/h/h7227.md).

<a name="daniel_11_45"></a>Daniel 11:45

And he shall [nāṭaʿ](../../strongs/h/h5193.md) the ['ohel](../../strongs/h/h168.md) of his ['apeḏen](../../strongs/h/h643.md) between the [yam](../../strongs/h/h3220.md) in the [ṣᵊḇî](../../strongs/h/h6643.md) [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md); yet he shall [bow'](../../strongs/h/h935.md) to his [qēṣ](../../strongs/h/h7093.md), and none shall [ʿāzar](../../strongs/h/h5826.md) him.

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 10](daniel_10.md) - [Daniel 12](daniel_12.md)