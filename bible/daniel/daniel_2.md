# [Daniel 2](https://www.blueletterbible.org/kjv/daniel/2)

<a name="daniel_2_1"></a>Daniel 2:1

And in the second [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [ḥālam](../../strongs/h/h2492.md) [ḥălôm](../../strongs/h/h2472.md), wherewith his [ruwach](../../strongs/h/h7307.md) was [pāʿam](../../strongs/h/h6470.md), and his [šēnā'](../../strongs/h/h8142.md) [hayah](../../strongs/h/h1961.md) from him.

<a name="daniel_2_2"></a>Daniel 2:2

Then the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to [qara'](../../strongs/h/h7121.md) the [ḥarṭōm](../../strongs/h/h2748.md), and the ['aššāp̄](../../strongs/h/h825.md), and the [kāšap̄](../../strongs/h/h3784.md), and the [Kaśdîmâ](../../strongs/h/h3778.md), for to [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) his [ḥălôm](../../strongs/h/h2472.md). So they [bow'](../../strongs/h/h935.md) and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="daniel_2_3"></a>Daniel 2:3

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto them, I have [ḥālam](../../strongs/h/h2492.md) a [ḥălôm](../../strongs/h/h2472.md), and my [ruwach](../../strongs/h/h7307.md) was [pāʿam](../../strongs/h/h6470.md) to [yada'](../../strongs/h/h3045.md) the [ḥălôm](../../strongs/h/h2472.md).

<a name="daniel_2_4"></a>Daniel 2:4

Then [dabar](../../strongs/h/h1696.md) the [Kaśdîmâ](../../strongs/h/h3778.md) to the [melek](../../strongs/h/h4428.md) in ['ărāmy](../../strongs/h/h762.md), O [meleḵ](../../strongs/h/h4430.md), [ḥăyā'](../../strongs/h/h2418.md) for [ʿālam](../../strongs/h/h5957.md): ['ămar](../../strongs/h/h560.md) thy [ʿăḇaḏ](../../strongs/h/h5649.md) the [ḥēlem](../../strongs/h/h2493.md), and we will [ḥăvā'](../../strongs/h/h2324.md) the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_2_5"></a>Daniel 2:5

The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to the [kaśday](../../strongs/h/h3779.md), The [millâ](../../strongs/h/h4406.md) is ['ăzaḏ](../../strongs/h/h230.md) from [min](../../strongs/h/h4481.md): [hēn](../../strongs/h/h2006.md) ye will [lā'](../../strongs/h/h3809.md) make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [ḥēlem](../../strongs/h/h2493.md), with the [pᵊšar](../../strongs/h/h6591.md) thereof, ye shall be [ʿăḇaḏ](../../strongs/h/h5648.md) in [hadām](../../strongs/h/h1917.md), and your [bayiṯ](../../strongs/h/h1005.md) shall be [śûm](../../strongs/h/h7761.md) a [nᵊvālû](../../strongs/h/h5122.md).

<a name="daniel_2_6"></a>Daniel 2:6

But [hēn](../../strongs/h/h2006.md) ye [ḥăvā'](../../strongs/h/h2324.md) the [ḥēlem](../../strongs/h/h2493.md), and the [pᵊšar](../../strongs/h/h6591.md) thereof, ye shall [qᵊḇal](../../strongs/h/h6902.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) [matnā'](../../strongs/h/h4978.md) and [nᵊḇizbâ](../../strongs/h/h5023.md) and [śagî'](../../strongs/h/h7690.md) [yᵊqār](../../strongs/h/h3367.md): [lāhēn](../../strongs/h/h3861.md) [ḥăvā'](../../strongs/h/h2324.md) me the [ḥēlem](../../strongs/h/h2493.md), and the [pᵊšar](../../strongs/h/h6591.md) thereof.

<a name="daniel_2_7"></a>Daniel 2:7

They [ʿănâ](../../strongs/h/h6032.md) [tinyānûṯ](../../strongs/h/h8579.md) and ['ămar](../../strongs/h/h560.md), Let the [meleḵ](../../strongs/h/h4430.md) ['ămar](../../strongs/h/h560.md) his [ʿăḇaḏ](../../strongs/h/h5649.md) the [ḥēlem](../../strongs/h/h2493.md), and we will [ḥăvā'](../../strongs/h/h2324.md) the [pᵊšar](../../strongs/h/h6591.md) of it.

<a name="daniel_2_8"></a>Daniel 2:8

The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), ['ănā'](../../strongs/h/h576.md) [yᵊḏaʿ](../../strongs/h/h3046.md) [min](../../strongs/h/h4481.md) [yaṣṣîḇ](../../strongs/h/h3330.md) that ['antûn](../../strongs/h/h608.md) would [zᵊḇan](../../strongs/h/h2084.md) the [ʿidān](../../strongs/h/h5732.md), [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) ye [ḥăzā'](../../strongs/h/h2370.md) the [millâ](../../strongs/h/h4406.md) is ['ăzaḏ](../../strongs/h/h230.md) [min](../../strongs/h/h4481.md) me.

<a name="daniel_2_9"></a>Daniel 2:9

But [hēn](../../strongs/h/h2006.md) ye will [lā'](../../strongs/h/h3809.md) [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [ḥēlem](../../strongs/h/h2493.md), there is but [hû'](../../strongs/h/h1932.md) [ḥaḏ](../../strongs/h/h2298.md) [dāṯ](../../strongs/h/h1882.md) for you: for ye have [zᵊman](../../strongs/h/h2164.md) [kᵊḏaḇ](../../strongs/h/h3538.md) and [šᵊḥaṯ](../../strongs/h/h7844.md) [millâ](../../strongs/h/h4406.md) to ['ămar](../../strongs/h/h560.md) [qŏḏām](../../strongs/h/h6925.md) me, [ʿaḏ](../../strongs/h/h5705.md) the [ʿidān](../../strongs/h/h5732.md) be [šᵊnâ](../../strongs/h/h8133.md): [lāhēn](../../strongs/h/h3861.md) ['ămar](../../strongs/h/h560.md) me the [ḥēlem](../../strongs/h/h2493.md), and I shall [yᵊḏaʿ](../../strongs/h/h3046.md) [dî](../../strongs/h/h1768.md) ye can [ḥăvā'](../../strongs/h/h2324.md) me the [pᵊšar](../../strongs/h/h6591.md) thereof.

<a name="daniel_2_10"></a>Daniel 2:10

The [kaśday](../../strongs/h/h3779.md) [ʿănâ](../../strongs/h/h6032.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), and ['ămar](../../strongs/h/h560.md), There ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) an ['ēneš](../../strongs/h/h606.md) [ʿal](../../strongs/h/h5922.md) the [yabešeṯ](../../strongs/h/h3007.md) that [yᵊḵēl](../../strongs/h/h3202.md) [ḥăvā'](../../strongs/h/h2324.md) the [meleḵ](../../strongs/h/h4430.md) [millâ](../../strongs/h/h4406.md): [qᵊḇēl](../../strongs/h/h6903.md) [dî](../../strongs/h/h1768.md) there is [lā'](../../strongs/h/h3809.md) [meleḵ](../../strongs/h/h4430.md), [raḇ](../../strongs/h/h7229.md), nor [šallîṭ](../../strongs/h/h7990.md), that [šᵊ'ēl](../../strongs/h/h7593.md) [dēn](../../strongs/h/h1836.md) [millâ](../../strongs/h/h4406.md) at [kōl](../../strongs/h/h3606.md) [ḥarṭṭōm](../../strongs/h/h2749.md), or ['aššāp̄](../../strongs/h/h826.md), or [kaśday](../../strongs/h/h3779.md).

<a name="daniel_2_11"></a>Daniel 2:11

And it is a [yaqqîr](../../strongs/h/h3358.md) [millâ](../../strongs/h/h4406.md) that the [meleḵ](../../strongs/h/h4430.md) [šᵊ'ēl](../../strongs/h/h7593.md), and there ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) ['āḥŏrān](../../strongs/h/h321.md) that can [ḥăvā'](../../strongs/h/h2324.md) it [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), [lāhēn](../../strongs/h/h3861.md) the ['ĕlâ](../../strongs/h/h426.md), whose [mᵊḏôr](../../strongs/h/h4070.md) ['îṯay](../../strongs/h/h383.md) [lā'](../../strongs/h/h3809.md) [ʿim](../../strongs/h/h5974.md) [bᵊśar](../../strongs/h/h1321.md).

<a name="daniel_2_12"></a>Daniel 2:12

[qᵊḇēl](../../strongs/h/h6903.md) [dēn](../../strongs/h/h1836.md) [kōl](../../strongs/h/h3606.md) the [meleḵ](../../strongs/h/h4430.md) was [bᵊnas](../../strongs/h/h1149.md) and [śagî'](../../strongs/h/h7690.md) [qᵊṣap̄](../../strongs/h/h7108.md), and ['ămar](../../strongs/h/h560.md) to ['ăḇaḏ](../../strongs/h/h7.md) [kōl](../../strongs/h/h3606.md) the [ḥakîm](../../strongs/h/h2445.md) of [Bāḇel](../../strongs/h/h895.md).

<a name="daniel_2_13"></a>Daniel 2:13

And the [dāṯ](../../strongs/h/h1882.md) went [nᵊp̄aq](../../strongs/h/h5312.md) that the [ḥakîm](../../strongs/h/h2445.md) men should be [qᵊṭal](../../strongs/h/h6992.md); and they [bᵊʿā'](../../strongs/h/h1156.md) [Dānîyē'L](../../strongs/h/h1841.md) and his [ḥăḇar](../../strongs/h/h2269.md) to be [qᵊṭal](../../strongs/h/h6992.md).

<a name="daniel_2_14"></a>Daniel 2:14

['ĕḏayin](../../strongs/h/h116.md) [Dānîyē'L](../../strongs/h/h1841.md) [tûḇ](../../strongs/h/h8421.md) with [ʿēṭā'](../../strongs/h/h5843.md) and [ṭᵊʿēm](../../strongs/h/h2942.md) to ['aryôḵ](../../strongs/h/h746.md) the [raḇ](../../strongs/h/h7229.md) [dî](../../strongs/h/h1768.md) the [meleḵ](../../strongs/h/h4430.md) [ṭabāḥ](../../strongs/h/h2877.md), which was gone [nᵊp̄aq](../../strongs/h/h5312.md) to [qᵊṭal](../../strongs/h/h6992.md) the [ḥakîm](../../strongs/h/h2445.md) of [Bāḇel](../../strongs/h/h895.md):

<a name="daniel_2_15"></a>Daniel 2:15

He [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to ['aryôḵ](../../strongs/h/h746.md) the [meleḵ](../../strongs/h/h4430.md) [šallîṭ](../../strongs/h/h7990.md), [mâ](../../strongs/h/h4101.md) [ʿal](../../strongs/h/h5922.md) is the [dāṯ](../../strongs/h/h1882.md) so [ḥăṣap̄](../../strongs/h/h2685.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md)? ['ĕḏayin](../../strongs/h/h116.md) ['aryôḵ](../../strongs/h/h746.md) made the [millâ](../../strongs/h/h4406.md) [yᵊḏaʿ](../../strongs/h/h3046.md) to [Dānîyē'L](../../strongs/h/h1841.md).

<a name="daniel_2_16"></a>Daniel 2:16

Then [Dānîyē'L](../../strongs/h/h1841.md) went [ʿălal](../../strongs/h/h5954.md), and [bᵊʿā'](../../strongs/h/h1156.md) [min](../../strongs/h/h4481.md) the [meleḵ](../../strongs/h/h4430.md) that he would [nᵊṯan](../../strongs/h/h5415.md) him [zᵊmān](../../strongs/h/h2166.md), and that he would [ḥăvā'](../../strongs/h/h2324.md) the [meleḵ](../../strongs/h/h4430.md) the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_2_17"></a>Daniel 2:17

['ĕḏayin](../../strongs/h/h116.md) [Dānîyē'L](../../strongs/h/h1841.md) ['ăzal](../../strongs/h/h236.md) to his [bayiṯ](../../strongs/h/h1005.md), and made the [millâ](../../strongs/h/h4406.md) [yᵊḏaʿ](../../strongs/h/h3046.md) to [Ḥănanyâ](../../strongs/h/h2608.md), [Mîšā'ēl](../../strongs/h/h4333.md), and [ʿĂzaryâ](../../strongs/h/h5839.md), his [ḥăḇar](../../strongs/h/h2269.md):

<a name="daniel_2_18"></a>Daniel 2:18

That they would [bᵊʿā'](../../strongs/h/h1156.md) [raḥămîn](../../strongs/h/h7359.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md) [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) [rāz](../../strongs/h/h7328.md); that [Dānîyē'L](../../strongs/h/h1841.md) and his [ḥăḇar](../../strongs/h/h2269.md) should [lā'](../../strongs/h/h3809.md) ['ăḇaḏ](../../strongs/h/h7.md) [ʿim](../../strongs/h/h5974.md) the [šᵊ'ār](../../strongs/h/h7606.md) of the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md).

<a name="daniel_2_19"></a>Daniel 2:19

['ĕḏayin](../../strongs/h/h116.md) was the [rāz](../../strongs/h/h7328.md) [gᵊlâ](../../strongs/h/h1541.md) unto [Dānîyē'L](../../strongs/h/h1841.md) in a [lêlyā'](../../strongs/h/h3916.md) [ḥēzev](../../strongs/h/h2376.md). ['ĕḏayin](../../strongs/h/h116.md) [Dānîyē'L](../../strongs/h/h1841.md) [bᵊraḵ](../../strongs/h/h1289.md) the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md).

<a name="daniel_2_20"></a>Daniel 2:20

[Dānîyē'L](../../strongs/h/h1841.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), [bᵊraḵ](../../strongs/h/h1289.md) [hăvâ](../../strongs/h/h1934.md) the [šum](../../strongs/h/h8036.md) of ['ĕlâ](../../strongs/h/h426.md) [min](../../strongs/h/h4481.md) [ʿālam](../../strongs/h/h5957.md) and [ʿaḏ](../../strongs/h/h5705.md) [ʿālam](../../strongs/h/h5957.md): for [ḥāḵmâ](../../strongs/h/h2452.md) and [gᵊḇûrâ](../../strongs/h/h1370.md) [hû'](../../strongs/h/h1932.md) his:

<a name="daniel_2_21"></a>Daniel 2:21

And he [šᵊnâ](../../strongs/h/h8133.md) the [ʿidān](../../strongs/h/h5732.md) and the [zᵊmān](../../strongs/h/h2166.md): he [ʿăḏā'](../../strongs/h/h5709.md) [meleḵ](../../strongs/h/h4430.md), and [qûm](../../strongs/h/h6966.md) [meleḵ](../../strongs/h/h4430.md): he [yᵊhaḇ](../../strongs/h/h3052.md) [ḥāḵmâ](../../strongs/h/h2452.md) unto the [ḥakîm](../../strongs/h/h2445.md), and [mandaʿ](../../strongs/h/h4486.md) to them that [yᵊḏaʿ](../../strongs/h/h3046.md) [bînâ](../../strongs/h/h999.md):

<a name="daniel_2_22"></a>Daniel 2:22

He [gᵊlâ](../../strongs/h/h1541.md) the [ʿămîq](../../strongs/h/h5994.md) and [sᵊṯar](../../strongs/h/h5642.md): he [yᵊḏaʿ](../../strongs/h/h3046.md) [mâ](../../strongs/h/h4101.md) is in the [ḥăšôḵ](../../strongs/h/h2816.md), and the [nāhîrû](../../strongs/h/h5094.md) [śᵊrâ](../../strongs/h/h8271.md) with [ʿim](../../strongs/h/h5974.md).

<a name="daniel_2_23"></a>Daniel 2:23

['ănā'](../../strongs/h/h576.md) [yeḏā'](../../strongs/h/h3029.md) thee, and [šᵊḇaḥ](../../strongs/h/h7624.md) thee, O thou ['ĕlâ](../../strongs/h/h426.md) of my ['ab](../../strongs/h/h2.md), who hast [yᵊhaḇ](../../strongs/h/h3052.md) me [ḥāḵmâ](../../strongs/h/h2452.md) and [gᵊḇûrâ](../../strongs/h/h1370.md), and hast made [yᵊḏaʿ](../../strongs/h/h3046.md) unto me [kᵊʿan](../../strongs/h/h3705.md) what we [bᵊʿā'](../../strongs/h/h1156.md) of [min](../../strongs/h/h4481.md): for thou hast now made [yᵊḏaʿ](../../strongs/h/h3046.md) unto us the [meleḵ](../../strongs/h/h4430.md) [millâ](../../strongs/h/h4406.md).

<a name="daniel_2_24"></a>Daniel 2:24

[kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) [dēn](../../strongs/h/h1836.md) [Dānîyē'L](../../strongs/h/h1841.md) went [ʿălal](../../strongs/h/h5954.md) [ʿal](../../strongs/h/h5922.md) ['aryôḵ](../../strongs/h/h746.md), whom the [meleḵ](../../strongs/h/h4430.md) had [mᵊnā'](../../strongs/h/h4483.md) to ['ăḇaḏ](../../strongs/h/h7.md) the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md): he ['ăzal](../../strongs/h/h236.md) and ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md) unto him; ['ăḇaḏ](../../strongs/h/h7.md) ['al](../../strongs/h/h409.md) the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md): bring me [ʿălal](../../strongs/h/h5954.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), and I will [ḥăvā'](../../strongs/h/h2324.md) unto the [meleḵ](../../strongs/h/h4430.md) the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_2_25"></a>Daniel 2:25

['ĕḏayin](../../strongs/h/h116.md) ['aryôḵ](../../strongs/h/h746.md) brought [ʿălal](../../strongs/h/h5954.md) [Dānîyē'L](../../strongs/h/h1841.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md) in [bᵊhal](../../strongs/h/h927.md), and ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md) unto him, I have [šᵊḵaḥ](../../strongs/h/h7912.md) a [gᵊḇar](../../strongs/h/h1400.md) of the [bēn](../../strongs/h/h1123.md) [gālûṯ](../../strongs/h/h1547.md) [min](../../strongs/h/h4481.md) [Yᵊhûḏ](../../strongs/h/h3061.md), that will make [yᵊḏaʿ](../../strongs/h/h3046.md) unto the [meleḵ](../../strongs/h/h4430.md) the [pᵊšar](../../strongs/h/h6591.md).

<a name="daniel_2_26"></a>Daniel 2:26

The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to [Dānîyē'L](../../strongs/h/h1841.md), whose [šum](../../strongs/h/h8036.md) was [Ḇēlṭᵊša'Ṣṣar](../../strongs/h/h1096.md), ['îṯay](../../strongs/h/h383.md) thou [kᵊhal](../../strongs/h/h3546.md) to make [yᵊḏaʿ](../../strongs/h/h3046.md) unto me the [ḥēlem](../../strongs/h/h2493.md) which I have [ḥăzā'](../../strongs/h/h2370.md), and the [pᵊšar](../../strongs/h/h6591.md) thereof?

<a name="daniel_2_27"></a>Daniel 2:27

[Dānîyē'L](../../strongs/h/h1841.md) [ʿănâ](../../strongs/h/h6032.md) in the [qŏḏām](../../strongs/h/h6925.md) of the [meleḵ](../../strongs/h/h4430.md), and ['ămar](../../strongs/h/h560.md), The [rāz](../../strongs/h/h7328.md) which the [meleḵ](../../strongs/h/h4430.md) hath [šᵊ'ēl](../../strongs/h/h7593.md) [yᵊḵēl](../../strongs/h/h3202.md) [lā'](../../strongs/h/h3809.md) the [ḥakîm](../../strongs/h/h2445.md) men, the ['aššāp̄](../../strongs/h/h826.md), the [ḥarṭṭōm](../../strongs/h/h2749.md), the [gᵊzar](../../strongs/h/h1505.md), [ḥăvā'](../../strongs/h/h2324.md) unto the [meleḵ](../../strongs/h/h4430.md);

<a name="daniel_2_28"></a>Daniel 2:28

[bᵊram](../../strongs/h/h1297.md) there ['îṯay](../../strongs/h/h383.md) an ['ĕlâ](../../strongs/h/h426.md) in [šᵊmayin](../../strongs/h/h8065.md) that [gᵊlâ](../../strongs/h/h1541.md) [rāz](../../strongs/h/h7328.md), and [yᵊḏaʿ](../../strongs/h/h3046.md) to the [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [mâ](../../strongs/h/h4101.md) shall [hăvâ](../../strongs/h/h1934.md) in the ['aḥărîṯ](../../strongs/h/h320.md) [yôm](../../strongs/h/h3118.md). Thy [ḥēlem](../../strongs/h/h2493.md), and the [ḥēzev](../../strongs/h/h2376.md) of thy [rē'š](../../strongs/h/h7217.md) [ʿal](../../strongs/h/h5922.md) thy [miškāḇ](../../strongs/h/h4903.md), are [dēn](../../strongs/h/h1836.md);

<a name="daniel_2_29"></a>Daniel 2:29

As for ['antâ](../../strongs/h/h607.md), O [meleḵ](../../strongs/h/h4430.md), thy [raʿyôn](../../strongs/h/h7476.md) [sᵊlaq](../../strongs/h/h5559.md) into thy mind [ʿal](../../strongs/h/h5922.md) thy [miškāḇ](../../strongs/h/h4903.md), [mâ](../../strongs/h/h4101.md) should come to [hăvâ](../../strongs/h/h1934.md) ['aḥar](../../strongs/h/h311.md) [dēn](../../strongs/h/h1836.md): and he that [gᵊlâ](../../strongs/h/h1541.md) [rāz](../../strongs/h/h7328.md) maketh [yᵊḏaʿ](../../strongs/h/h3046.md) to thee [mâ](../../strongs/h/h4101.md) shall come to [hăvâ](../../strongs/h/h1934.md).

<a name="daniel_2_30"></a>Daniel 2:30

But as for ['ănā'](../../strongs/h/h576.md), [dēn](../../strongs/h/h1836.md) [rāz](../../strongs/h/h7328.md) is [lā'](../../strongs/h/h3809.md) [gᵊlâ](../../strongs/h/h1541.md) to me for any [ḥāḵmâ](../../strongs/h/h2452.md) that I ['îṯay](../../strongs/h/h383.md) more [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) [ḥay](../../strongs/h/h2417.md), [lāhēn](../../strongs/h/h3861.md) for their [diḇrâ](../../strongs/h/h1701.md) [ʿal](../../strongs/h/h5922.md) shall make [yᵊḏaʿ](../../strongs/h/h3046.md) the [pᵊšar](../../strongs/h/h6591.md) to the [meleḵ](../../strongs/h/h4430.md), and that thou mightest [yᵊḏaʿ](../../strongs/h/h3046.md) the [raʿyôn](../../strongs/h/h7476.md) of thy [lᵊḇaḇ](../../strongs/h/h3825.md).

<a name="daniel_2_31"></a>Daniel 2:31

['antâ](../../strongs/h/h607.md), O [meleḵ](../../strongs/h/h4430.md), [hăvâ](../../strongs/h/h1934.md) [ḥăzā'](../../strongs/h/h2370.md), and ['ălû](../../strongs/h/h431.md) a [ḥaḏ](../../strongs/h/h2298.md) [śagî'](../../strongs/h/h7690.md) [ṣelem](../../strongs/h/h6755.md). [dikēn](../../strongs/h/h1797.md) [raḇ](../../strongs/h/h7229.md) [ṣelem](../../strongs/h/h6755.md), whose [zîv](../../strongs/h/h2122.md) was [yatîr](../../strongs/h/h3493.md), [qûm](../../strongs/h/h6966.md) [qᵊḇēl](../../strongs/h/h6903.md) thee; and the [rēv](../../strongs/h/h7299.md) thereof was [dᵊḥal](../../strongs/h/h1763.md).

<a name="daniel_2_32"></a>Daniel 2:32

This [ṣelem](../../strongs/h/h6755.md) [rē'š](../../strongs/h/h7217.md) was of [ṭāḇ](../../strongs/h/h2869.md) [dᵊhaḇ](../../strongs/h/h1722.md), his [ḥăḏîn](../../strongs/h/h2306.md) and his [dᵊrāʿ](../../strongs/h/h1872.md) of [kᵊsap̄](../../strongs/h/h3702.md), his [mᵊʿâ](../../strongs/h/h4577.md) and his [yarkâ](../../strongs/h/h3410.md) of [nᵊḥāš](../../strongs/h/h5174.md),

<a name="daniel_2_33"></a>Daniel 2:33

His [šāq](../../strongs/h/h8243.md) of [parzel](../../strongs/h/h6523.md), his [reḡel](../../strongs/h/h7271.md) [min](../../strongs/h/h4481.md) of [parzel](../../strongs/h/h6523.md) and [min](../../strongs/h/h4481.md) of [ḥăsap̄](../../strongs/h/h2635.md).

<a name="daniel_2_34"></a>Daniel 2:34

Thou [ḥăzā'](../../strongs/h/h2370.md) [hăvâ](../../strongs/h/h1934.md) [ʿaḏ](../../strongs/h/h5705.md) that an ['eḇen](../../strongs/h/h69.md) was [gᵊzar](../../strongs/h/h1505.md) [lā'](../../strongs/h/h3809.md) [yaḏ](../../strongs/h/h3028.md), which [mᵊḥā'](../../strongs/h/h4223.md) the [ṣelem](../../strongs/h/h6755.md) [ʿal](../../strongs/h/h5922.md) his [reḡel](../../strongs/h/h7271.md) that were of [parzel](../../strongs/h/h6523.md) and [ḥăsap̄](../../strongs/h/h2635.md), and [dᵊqaq](../../strongs/h/h1855.md) [himmô](../../strongs/h/h1994.md) to [dᵊqaq](../../strongs/h/h1855.md).

<a name="daniel_2_35"></a>Daniel 2:35

['ĕḏayin](../../strongs/h/h116.md) was the [parzel](../../strongs/h/h6523.md), the [ḥăsap̄](../../strongs/h/h2635.md), the [nᵊḥāš](../../strongs/h/h5174.md), the [kᵊsap̄](../../strongs/h/h3702.md), and the [dᵊhaḇ](../../strongs/h/h1722.md), [dᵊqaq](../../strongs/h/h1855.md) [dûq](../../strongs/h/h1751.md) [ḥaḏ](../../strongs/h/h2298.md), and [hăvâ](../../strongs/h/h1934.md) like the [ʿûr](../../strongs/h/h5784.md) [min](../../strongs/h/h4481.md) the [qayaṭ](../../strongs/h/h7007.md) ['idar](../../strongs/h/h147.md); and the [rûaḥ](../../strongs/h/h7308.md) [nᵊśā'](../../strongs/h/h5376.md) [himmô](../../strongs/h/h1994.md) [nᵊśā'](../../strongs/h/h5376.md), [kōl](../../strongs/h/h3606.md) [lā'](../../strongs/h/h3809.md) ['ăṯar](../../strongs/h/h870.md) was [šᵊḵaḥ](../../strongs/h/h7912.md) for them: and the ['eḇen](../../strongs/h/h69.md) that [mᵊḥā'](../../strongs/h/h4223.md) the [ṣelem](../../strongs/h/h6755.md) [hăvâ](../../strongs/h/h1934.md) a [raḇ](../../strongs/h/h7229.md) [ṭûr](../../strongs/h/h2906.md), and [mᵊlā'](../../strongs/h/h4391.md) the [kōl](../../strongs/h/h3606.md) ['ăraʿ](../../strongs/h/h772.md).

<a name="daniel_2_36"></a>Daniel 2:36

[dēn](../../strongs/h/h1836.md) is the [ḥēlem](../../strongs/h/h2493.md); and we will ['ămar](../../strongs/h/h560.md) the [pᵊšar](../../strongs/h/h6591.md) thereof [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md).

<a name="daniel_2_37"></a>Daniel 2:37

['antâ](../../strongs/h/h607.md), O [meleḵ](../../strongs/h/h4430.md), art a [meleḵ](../../strongs/h/h4430.md) of [meleḵ](../../strongs/h/h4430.md): for the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md) hath [yᵊhaḇ](../../strongs/h/h3052.md) thee a [malkû](../../strongs/h/h4437.md), [ḥēsen](../../strongs/h/h2632.md), and [tᵊqōp̄](../../strongs/h/h8632.md), and [yᵊqār](../../strongs/h/h3367.md).

<a name="daniel_2_38"></a>Daniel 2:38

And [kōl](../../strongs/h/h3606.md) the [bēn](../../strongs/h/h1123.md) of ['ēneš](../../strongs/h/h606.md) [dûr](../../strongs/h/h1753.md), the [ḥêvā'](../../strongs/h/h2423.md) of the [bar](../../strongs/h/h1251.md) and the [ʿôp̄](../../strongs/h/h5776.md) of the [šᵊmayin](../../strongs/h/h8065.md) hath he [yᵊhaḇ](../../strongs/h/h3052.md) into thine [yaḏ](../../strongs/h/h3028.md), and hath made thee [šᵊlēṭ](../../strongs/h/h7981.md) over them [kōl](../../strongs/h/h3606.md). ['antâ](../../strongs/h/h607.md) art this [rē'š](../../strongs/h/h7217.md) of [dᵊhaḇ](../../strongs/h/h1722.md).

<a name="daniel_2_39"></a>Daniel 2:39

And ['ăṯar](../../strongs/h/h870.md) thee shall [qûm](../../strongs/h/h6966.md) ['āḥŏrî](../../strongs/h/h317.md) [malkû](../../strongs/h/h4437.md) ['ăraʿ](../../strongs/h/h772.md) to [min](../../strongs/h/h4481.md), and ['āḥŏrî](../../strongs/h/h317.md) [tᵊlîṯay](../../strongs/h/h8523.md) [malkû](../../strongs/h/h4437.md) of [nᵊḥāš](../../strongs/h/h5174.md), which shall bear [šᵊlēṭ](../../strongs/h/h7981.md) over [kōl](../../strongs/h/h3606.md) the ['ăraʿ](../../strongs/h/h772.md).

<a name="daniel_2_40"></a>Daniel 2:40

And the [rᵊḇîʿay](../../strongs/h/h7244.md) [malkû](../../strongs/h/h4437.md) shall [hăvâ](../../strongs/h/h1934.md) [taqqîp̄](../../strongs/h/h8624.md) as [parzel](../../strongs/h/h6523.md): forasmuch as [parzel](../../strongs/h/h6523.md) breaketh in [dᵊqaq](../../strongs/h/h1855.md) and [ḥăšal](../../strongs/h/h2827.md) [kōl](../../strongs/h/h3606.md) things: and [qᵊḇēl](../../strongs/h/h6903.md) [parzel](../../strongs/h/h6523.md) that [rᵊʿaʿ](../../strongs/h/h7490.md) [kōl](../../strongs/h/h3606.md) ['illên](../../strongs/h/h459.md), shall it break in [dᵊqaq](../../strongs/h/h1855.md) and [rᵊʿaʿ](../../strongs/h/h7490.md).

<a name="daniel_2_41"></a>Daniel 2:41

And whereas thou [ḥăzā'](../../strongs/h/h2370.md) the [reḡel](../../strongs/h/h7271.md) and ['eṣbʿ](../../strongs/h/h677.md), [min](../../strongs/h/h4481.md) of [peḥār](../../strongs/h/h6353.md) [ḥăsap̄](../../strongs/h/h2635.md), and [min](../../strongs/h/h4481.md) of [parzel](../../strongs/h/h6523.md), the [malkû](../../strongs/h/h4437.md) shall [hăvâ](../../strongs/h/h1934.md) [pᵊlaḡ](../../strongs/h/h6386.md); [min](../../strongs/h/h4481.md) there shall [hăvâ](../../strongs/h/h1934.md) in it [min](../../strongs/h/h4481.md) the [niṣbâ](../../strongs/h/h5326.md) of the [parzel](../../strongs/h/h6523.md), forasmuch [kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) thou [ḥăzā'](../../strongs/h/h2370.md) the [parzel](../../strongs/h/h6523.md) [ʿăraḇ](../../strongs/h/h6151.md) with [ṭîn](../../strongs/h/h2917.md) [ḥăsap̄](../../strongs/h/h2635.md).

<a name="daniel_2_42"></a>Daniel 2:42

And as the ['eṣbʿ](../../strongs/h/h677.md) of the [reḡel](../../strongs/h/h7271.md) were [min](../../strongs/h/h4481.md) of [parzel](../../strongs/h/h6523.md), and [min](../../strongs/h/h4481.md) of [ḥăsap̄](../../strongs/h/h2635.md), so the [malkû](../../strongs/h/h4437.md) shall [hăvâ](../../strongs/h/h1934.md) [min](../../strongs/h/h4481.md) [qᵊṣāṯ](../../strongs/h/h7118.md) [taqqîp̄](../../strongs/h/h8624.md), and [min](../../strongs/h/h4481.md) [qᵊṣāṯ](../../strongs/h/h7118.md) [tᵊḇar](../../strongs/h/h8406.md).

<a name="daniel_2_43"></a>Daniel 2:43

And [dî](../../strongs/h/h1768.md) thou [ḥăzā'](../../strongs/h/h2370.md) [parzel](../../strongs/h/h6523.md) [ʿăraḇ](../../strongs/h/h6151.md) with [ṭîn](../../strongs/h/h2917.md) [ḥăsap̄](../../strongs/h/h2635.md), they shall [hăvâ](../../strongs/h/h1934.md) [ʿăraḇ](../../strongs/h/h6151.md) with the [zᵊraʿ](../../strongs/h/h2234.md) of ['ēneš](../../strongs/h/h606.md): but they [hăvâ](../../strongs/h/h1934.md) [lā'](../../strongs/h/h3809.md) [dᵊḇaq](../../strongs/h/h1693.md) [dēn](../../strongs/h/h1836.md) [ʿim](../../strongs/h/h5974.md) [dēn](../../strongs/h/h1836.md), [hā'](../../strongs/h/h1888.md) as [parzel](../../strongs/h/h6523.md) is [lā'](../../strongs/h/h3809.md) [ʿăraḇ](../../strongs/h/h6151.md) with [ḥăsap̄](../../strongs/h/h2635.md).

<a name="daniel_2_44"></a>Daniel 2:44

And in the [yôm](../../strongs/h/h3118.md) of ['innûn](../../strongs/h/h581.md) [meleḵ](../../strongs/h/h4430.md) shall the ['ĕlâ](../../strongs/h/h426.md) of [šᵊmayin](../../strongs/h/h8065.md) set [qûm](../../strongs/h/h6966.md) a [malkû](../../strongs/h/h4437.md), which shall [ʿālam](../../strongs/h/h5957.md) [lā'](../../strongs/h/h3809.md) be [ḥăḇal](../../strongs/h/h2255.md): and the [malkû](../../strongs/h/h4437.md) shall [lā'](../../strongs/h/h3809.md) be [šᵊḇaq](../../strongs/h/h7662.md) to ['āḥŏrān](../../strongs/h/h321.md) [ʿam](../../strongs/h/h5972.md), but it shall break in [dᵊqaq](../../strongs/h/h1855.md) and [sûp̄](../../strongs/h/h5487.md) [kōl](../../strongs/h/h3606.md) ['illên](../../strongs/h/h459.md) [malkû](../../strongs/h/h4437.md), and [hû'](../../strongs/h/h1932.md) shall [qûm](../../strongs/h/h6966.md) for [ʿālam](../../strongs/h/h5957.md).

<a name="daniel_2_45"></a>Daniel 2:45

Forasmuch [kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) thou [ḥăzā'](../../strongs/h/h2370.md) that the ['eḇen](../../strongs/h/h69.md) was [gᵊzar](../../strongs/h/h1505.md) of the [ṭûr](../../strongs/h/h2906.md) [lā'](../../strongs/h/h3809.md) [yaḏ](../../strongs/h/h3028.md), and that it [dᵊqaq](../../strongs/h/h1855.md) the [parzel](../../strongs/h/h6523.md), the [nᵊḥāš](../../strongs/h/h5174.md), the [ḥăsap̄](../../strongs/h/h2635.md), the [kᵊsap̄](../../strongs/h/h3702.md), and the [dᵊhaḇ](../../strongs/h/h1722.md); the [raḇ](../../strongs/h/h7229.md) ['ĕlâ](../../strongs/h/h426.md) hath made [yᵊḏaʿ](../../strongs/h/h3046.md) to the [meleḵ](../../strongs/h/h4430.md) [mâ](../../strongs/h/h4101.md) shall come to [hăvâ](../../strongs/h/h1934.md) ['aḥar](../../strongs/h/h311.md) [dēn](../../strongs/h/h1836.md): and the [ḥēlem](../../strongs/h/h2493.md) is [yaṣṣîḇ](../../strongs/h/h3330.md), and the [pᵊšar](../../strongs/h/h6591.md) thereof ['ăman](../../strongs/h/h540.md).

<a name="daniel_2_46"></a>Daniel 2:46

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) [Nᵊḇûḵaḏneṣṣar](../../strongs/h/h5020.md) [nᵊp̄al](../../strongs/h/h5308.md) [ʿal](../../strongs/h/h5922.md) his ['ănap̄](../../strongs/h/h600.md), and [sᵊḡiḏ](../../strongs/h/h5457.md) [Dānîyē'L](../../strongs/h/h1841.md), and ['ămar](../../strongs/h/h560.md) that they should [nᵊsaḵ](../../strongs/h/h5260.md) a [minḥâ](../../strongs/h/h4504.md) and sweet [nîḥôaḥ](../../strongs/h/h5208.md) unto him.

<a name="daniel_2_47"></a>Daniel 2:47

The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) unto [Dānîyē'L](../../strongs/h/h1841.md), and ['ămar](../../strongs/h/h560.md), [min](../../strongs/h/h4481.md) a [qᵊšōṭ](../../strongs/h/h7187.md) it is, [dî](../../strongs/h/h1768.md) your ['ĕlâ](../../strongs/h/h426.md) is an ['ĕlâ](../../strongs/h/h426.md) of ['ĕlâ](../../strongs/h/h426.md), and a [mārē'](../../strongs/h/h4756.md) of [meleḵ](../../strongs/h/h4430.md), and a [gᵊlâ](../../strongs/h/h1541.md) of [rāz](../../strongs/h/h7328.md), seeing thou [yᵊḵēl](../../strongs/h/h3202.md) [gᵊlâ](../../strongs/h/h1541.md) [dēn](../../strongs/h/h1836.md) [rāz](../../strongs/h/h7328.md).

<a name="daniel_2_48"></a>Daniel 2:48

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) [rᵊḇâ](../../strongs/h/h7236.md) [Dānîyē'L](../../strongs/h/h1841.md) a great [rᵊḇâ](../../strongs/h/h7236.md), and [yᵊhaḇ](../../strongs/h/h3052.md) him [śagî'](../../strongs/h/h7690.md) [raḇraḇ](../../strongs/h/h7260.md) [matnā'](../../strongs/h/h4978.md), and made him [šᵊlēṭ](../../strongs/h/h7981.md) [ʿal](../../strongs/h/h5922.md) the [kōl](../../strongs/h/h3606.md) [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md), and [raḇ](../../strongs/h/h7229.md) of the [sᵊḡan](../../strongs/h/h5460.md) [ʿal](../../strongs/h/h5922.md) [kōl](../../strongs/h/h3606.md) the [ḥakîm](../../strongs/h/h2445.md) men of [Bāḇel](../../strongs/h/h895.md).

<a name="daniel_2_49"></a>Daniel 2:49

Then [Dānîyē'L](../../strongs/h/h1841.md) [bᵊʿā'](../../strongs/h/h1156.md) [min](../../strongs/h/h4481.md) the [meleḵ](../../strongs/h/h4430.md), and he [mᵊnā'](../../strongs/h/h4483.md) [Šaḏraḵ](../../strongs/h/h7715.md), [Mêšaḵ](../../strongs/h/h4336.md), and [ʿĂḇēḏ Nᵊḡô](../../strongs/h/h5665.md), [ʿal](../../strongs/h/h5922.md) the [ʿăḇîḏâ](../../strongs/h/h5673.md) of the [mᵊḏînâ](../../strongs/h/h4083.md) of [Bāḇel](../../strongs/h/h895.md): but [Dānîyē'L](../../strongs/h/h1841.md) sat in the [tᵊraʿ](../../strongs/h/h8651.md) of the [meleḵ](../../strongs/h/h4430.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 1](daniel_1.md) - [Daniel 3](daniel_3.md)