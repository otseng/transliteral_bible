# [Daniel 10](https://www.blueletterbible.org/kjv/daniel/10)

<a name="daniel_10_1"></a>Daniel 10:1

In the third [šānâ](../../strongs/h/h8141.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md) a [dabar](../../strongs/h/h1697.md) was [gālâ](../../strongs/h/h1540.md) unto [Dinîyē'L](../../strongs/h/h1840.md), whose [shem](../../strongs/h/h8034.md) was [qara'](../../strongs/h/h7121.md) [Bēlṭᵊša'Ṣṣar](../../strongs/h/h1095.md); and the [dabar](../../strongs/h/h1697.md) was ['emeth](../../strongs/h/h571.md), but the time [tsaba'](../../strongs/h/h6635.md) was [gadowl](../../strongs/h/h1419.md): and he [bîn](../../strongs/h/h995.md) the [dabar](../../strongs/h/h1697.md), and had [bînâ](../../strongs/h/h998.md) of the [mar'ê](../../strongs/h/h4758.md).

<a name="daniel_10_2"></a>Daniel 10:2

In those [yowm](../../strongs/h/h3117.md) I [Dinîyē'L](../../strongs/h/h1840.md) was ['āḇal](../../strongs/h/h56.md) three [yowm](../../strongs/h/h3117.md) [šāḇûaʿ](../../strongs/h/h7620.md).

<a name="daniel_10_3"></a>Daniel 10:3

I ['akal](../../strongs/h/h398.md) no [ḥemdâ](../../strongs/h/h2532.md) [lechem](../../strongs/h/h3899.md), neither [bow'](../../strongs/h/h935.md) [basar](../../strongs/h/h1320.md) nor [yayin](../../strongs/h/h3196.md) in my [peh](../../strongs/h/h6310.md), neither did I [sûḵ](../../strongs/h/h5480.md) myself at [sûḵ](../../strongs/h/h5480.md), till three [yowm](../../strongs/h/h3117.md) [šāḇûaʿ](../../strongs/h/h7620.md) were [mālā'](../../strongs/h/h4390.md).

<a name="daniel_10_4"></a>Daniel 10:4

And in the four and twentieth [yowm](../../strongs/h/h3117.md) of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), as I was by the [yad](../../strongs/h/h3027.md) of the [gadowl](../../strongs/h/h1419.md) [nāhār](../../strongs/h/h5104.md), which is [Ḥideqel](../../strongs/h/h2313.md);

<a name="daniel_10_5"></a>Daniel 10:5

Then I [nasa'](../../strongs/h/h5375.md) mine ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md), and behold a certain ['iysh](../../strongs/h/h376.md) [labash](../../strongs/h/h3847.md) in [baḏ](../../strongs/h/h906.md), whose [māṯnayim](../../strongs/h/h4975.md) were [ḥāḡar](../../strongs/h/h2296.md) with [keṯem](../../strongs/h/h3800.md) of ['Ûp̄Āz](../../strongs/h/h210.md):

<a name="daniel_10_6"></a>Daniel 10:6

His [gᵊvîyâ](../../strongs/h/h1472.md) also was like the [taršîš](../../strongs/h/h8658.md), and his [paniym](../../strongs/h/h6440.md) as the [mar'ê](../../strongs/h/h4758.md) of [baraq](../../strongs/h/h1300.md), and his ['ayin](../../strongs/h/h5869.md) as [lapîḏ](../../strongs/h/h3940.md) of ['esh](../../strongs/h/h784.md), and his [zerowa'](../../strongs/h/h2220.md) and his [margᵊlôṯ](../../strongs/h/h4772.md) like in ['ayin](../../strongs/h/h5869.md) to [qālāl](../../strongs/h/h7044.md) [nᵊḥšeṯ](../../strongs/h/h5178.md), and the [qowl](../../strongs/h/h6963.md) of his [dabar](../../strongs/h/h1697.md) like the [qowl](../../strongs/h/h6963.md) of a [hāmôn](../../strongs/h/h1995.md).

<a name="daniel_10_7"></a>Daniel 10:7

And I [Dinîyē'L](../../strongs/h/h1840.md) alone [ra'ah](../../strongs/h/h7200.md) the [mar'â](../../strongs/h/h4759.md): for the ['enowsh](../../strongs/h/h582.md) that were with me [ra'ah](../../strongs/h/h7200.md) not the [mar'â](../../strongs/h/h4759.md); ['ăḇāl](../../strongs/h/h61.md) a [gadowl](../../strongs/h/h1419.md) [ḥărāḏâ](../../strongs/h/h2731.md) [naphal](../../strongs/h/h5307.md) upon them, so that they [bāraḥ](../../strongs/h/h1272.md) to [chaba'](../../strongs/h/h2244.md) themselves.

<a name="daniel_10_8"></a>Daniel 10:8

Therefore I was [šā'ar](../../strongs/h/h7604.md), and [ra'ah](../../strongs/h/h7200.md) this [gadowl](../../strongs/h/h1419.md) [mar'â](../../strongs/h/h4759.md), and there [šā'ar](../../strongs/h/h7604.md) no [koach](../../strongs/h/h3581.md) in me: for my [howd](../../strongs/h/h1935.md) was [hāp̄aḵ](../../strongs/h/h2015.md) in me into [mašḥîṯ](../../strongs/h/h4889.md), and I [ʿāṣar](../../strongs/h/h6113.md) no [koach](../../strongs/h/h3581.md).

<a name="daniel_10_9"></a>Daniel 10:9

Yet [shama'](../../strongs/h/h8085.md) I the [qowl](../../strongs/h/h6963.md) of his [dabar](../../strongs/h/h1697.md): and when I [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of his [dabar](../../strongs/h/h1697.md), then was I in a [rāḏam](../../strongs/h/h7290.md) on my [paniym](../../strongs/h/h6440.md), and my [paniym](../../strongs/h/h6440.md) toward the ['erets](../../strongs/h/h776.md).

<a name="daniel_10_10"></a>Daniel 10:10

And, behold, a [yad](../../strongs/h/h3027.md) [naga'](../../strongs/h/h5060.md) me, which [nûaʿ](../../strongs/h/h5128.md) me upon my [bereḵ](../../strongs/h/h1290.md) and upon the [kaph](../../strongs/h/h3709.md) of my [yad](../../strongs/h/h3027.md).

<a name="daniel_10_11"></a>Daniel 10:11

And he ['āmar](../../strongs/h/h559.md) unto me, O [Dinîyē'L](../../strongs/h/h1840.md), an ['iysh](../../strongs/h/h376.md) [ḥemdâ](../../strongs/h/h2532.md), [bîn](../../strongs/h/h995.md) the [dabar](../../strongs/h/h1697.md) that I [dabar](../../strongs/h/h1696.md) unto thee, and ['amad](../../strongs/h/h5975.md) [ʿōmeḏ](../../strongs/h/h5977.md): for unto thee am I now [shalach](../../strongs/h/h7971.md). And when he had [dabar](../../strongs/h/h1696.md) this [dabar](../../strongs/h/h1697.md) unto me, I ['amad](../../strongs/h/h5975.md) [rāʿaḏ](../../strongs/h/h7460.md).

<a name="daniel_10_12"></a>Daniel 10:12

Then ['āmar](../../strongs/h/h559.md) he unto me, [yare'](../../strongs/h/h3372.md) not, [Dinîyē'L](../../strongs/h/h1840.md): for from the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) that thou didst [nathan](../../strongs/h/h5414.md) thine [leb](../../strongs/h/h3820.md) to [bîn](../../strongs/h/h995.md), and to [ʿānâ](../../strongs/h/h6031.md) thyself [paniym](../../strongs/h/h6440.md) thy ['Elohiym](../../strongs/h/h430.md), thy [dabar](../../strongs/h/h1697.md) were [shama'](../../strongs/h/h8085.md), and I am [bow'](../../strongs/h/h935.md) for thy [dabar](../../strongs/h/h1697.md).

<a name="daniel_10_13"></a>Daniel 10:13

But the [śar](../../strongs/h/h8269.md) of the [malkuwth](../../strongs/h/h4438.md) of [Pāras](../../strongs/h/h6539.md) ['amad](../../strongs/h/h5975.md) me one and twenty [yowm](../../strongs/h/h3117.md): but, lo, [Mîḵā'ēl](../../strongs/h/h4317.md), one of the [ri'šôn](../../strongs/h/h7223.md) [śar](../../strongs/h/h8269.md), [bow'](../../strongs/h/h935.md) to [ʿāzar](../../strongs/h/h5826.md) me; and I [yāṯar](../../strongs/h/h3498.md) there with the [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md).

<a name="daniel_10_14"></a>Daniel 10:14

Now I am [bow'](../../strongs/h/h935.md) to make thee [bîn](../../strongs/h/h995.md) what shall [qārâ](../../strongs/h/h7136.md) thy ['am](../../strongs/h/h5971.md) in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md): for yet the [ḥāzôn](../../strongs/h/h2377.md) is for many [yowm](../../strongs/h/h3117.md).

<a name="daniel_10_15"></a>Daniel 10:15

And when he had [dabar](../../strongs/h/h1696.md) such [dabar](../../strongs/h/h1697.md) unto me, I [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) toward the ['erets](../../strongs/h/h776.md), and I became ['ālam](../../strongs/h/h481.md).

<a name="daniel_10_16"></a>Daniel 10:16

And, behold, one like the [dĕmuwth](../../strongs/h/h1823.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [naga'](../../strongs/h/h5060.md) my [saphah](../../strongs/h/h8193.md): then I [pāṯaḥ](../../strongs/h/h6605.md) my [peh](../../strongs/h/h6310.md), and [dabar](../../strongs/h/h1696.md), and ['āmar](../../strongs/h/h559.md) unto him that ['amad](../../strongs/h/h5975.md) before me, O my ['adown](../../strongs/h/h113.md), by the [mar'â](../../strongs/h/h4759.md) my [ṣîr](../../strongs/h/h6735.md) are [hāp̄aḵ](../../strongs/h/h2015.md) upon me, and I have [ʿāṣar](../../strongs/h/h6113.md) no [koach](../../strongs/h/h3581.md).

<a name="daniel_10_17"></a>Daniel 10:17

For [hêḵ](../../strongs/h/h1963.md) [yakol](../../strongs/h/h3201.md) the ['ebed](../../strongs/h/h5650.md) of this my ['adown](../../strongs/h/h113.md) [dabar](../../strongs/h/h1696.md) with this my ['adown](../../strongs/h/h113.md)? for as for me, straightway there ['amad](../../strongs/h/h5975.md) no [koach](../../strongs/h/h3581.md) in me, neither is there [neshamah](../../strongs/h/h5397.md) [šā'ar](../../strongs/h/h7604.md) in me.

<a name="daniel_10_18"></a>Daniel 10:18

Then there came again and [naga'](../../strongs/h/h5060.md) me one like the [mar'ê](../../strongs/h/h4758.md) of an ['āḏām](../../strongs/h/h120.md), and he [ḥāzaq](../../strongs/h/h2388.md) me,

<a name="daniel_10_19"></a>Daniel 10:19

And ['āmar](../../strongs/h/h559.md), O ['iysh](../../strongs/h/h376.md) [ḥemdâ](../../strongs/h/h2532.md), [yare'](../../strongs/h/h3372.md) not: [shalowm](../../strongs/h/h7965.md) be unto thee, be [ḥāzaq](../../strongs/h/h2388.md), yea, be [ḥāzaq](../../strongs/h/h2388.md). And when he had [dabar](../../strongs/h/h1696.md) unto me, I was [ḥāzaq](../../strongs/h/h2388.md), and ['āmar](../../strongs/h/h559.md), Let my ['adown](../../strongs/h/h113.md) [dabar](../../strongs/h/h1696.md); for thou hast [ḥāzaq](../../strongs/h/h2388.md) me.

<a name="daniel_10_20"></a>Daniel 10:20

Then ['āmar](../../strongs/h/h559.md) he, [yada'](../../strongs/h/h3045.md) thou wherefore I [bow'](../../strongs/h/h935.md) unto thee? and now will I [shuwb](../../strongs/h/h7725.md) to [lāḥam](../../strongs/h/h3898.md) with the [śar](../../strongs/h/h8269.md) of [Pāras](../../strongs/h/h6539.md): and when I am [yāṣā'](../../strongs/h/h3318.md), lo, the [śar](../../strongs/h/h8269.md) of [Yāvān](../../strongs/h/h3120.md) shall [bow'](../../strongs/h/h935.md).

<a name="daniel_10_21"></a>Daniel 10:21

['ăḇāl](../../strongs/h/h61.md) I will [nāḡaḏ](../../strongs/h/h5046.md) thee that which is [rāšam](../../strongs/h/h7559.md) in the [kᵊṯāḇ](../../strongs/h/h3791.md) of ['emeth](../../strongs/h/h571.md): and there is none that [ḥāzaq](../../strongs/h/h2388.md) with me in these things, but [Mîḵā'ēl](../../strongs/h/h4317.md) your [śar](../../strongs/h/h8269.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 9](daniel_9.md) - [Daniel 11](daniel_11.md)