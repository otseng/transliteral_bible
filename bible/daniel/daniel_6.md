# [Daniel 6](https://www.blueletterbible.org/kjv/daniel/6)

<a name="daniel_6_1"></a>Daniel 6:1

It [qŏḏām](../../strongs/h/h6925.md) [šᵊp̄ar](../../strongs/h/h8232.md) [Daryāveš](../../strongs/h/h1868.md) to [qûm](../../strongs/h/h6966.md) [ʿal](../../strongs/h/h5922.md) the [malkû](../../strongs/h/h4437.md) a [mᵊ'â](../../strongs/h/h3969.md) and [ʿשryn](../../strongs/h/h6243.md) ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), which should [hăvâ](../../strongs/h/h1934.md) over the [kōl](../../strongs/h/h3606.md) [malkû](../../strongs/h/h4437.md);

<a name="daniel_6_2"></a>Daniel 6:2

And [ʿēllā'](../../strongs/h/h5924.md) [min](../../strongs/h/h4481.md) [tᵊlāṯ](../../strongs/h/h8532.md) [sārēḵ](../../strongs/h/h5632.md); of [min](../../strongs/h/h4481.md) [Dānîyē'L](../../strongs/h/h1841.md) was [ḥaḏ](../../strongs/h/h2298.md): ['illên](../../strongs/h/h459.md) the ['ăḥašdarpᵊnîn](../../strongs/h/h324.md) [hăvâ](../../strongs/h/h1934.md) [yᵊhaḇ](../../strongs/h/h3052.md) [ṭaʿam](../../strongs/h/h2941.md) unto them, and the [meleḵ](../../strongs/h/h4430.md) should [hăvâ](../../strongs/h/h1934.md) [lā'](../../strongs/h/h3809.md) [nᵊzaq](../../strongs/h/h5142.md).

<a name="daniel_6_3"></a>Daniel 6:3

['ĕḏayin](../../strongs/h/h116.md) [dēn](../../strongs/h/h1836.md) [Dānîyē'L](../../strongs/h/h1841.md) [hăvâ](../../strongs/h/h1934.md) [nᵊṣaḥ](../../strongs/h/h5330.md) [ʿal](../../strongs/h/h5922.md) the [sārēḵ](../../strongs/h/h5632.md) and ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), [kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) a [yatîr](../../strongs/h/h3493.md) [rûaḥ](../../strongs/h/h7308.md) was in him; and the [meleḵ](../../strongs/h/h4430.md) [ʿăšiṯ](../../strongs/h/h6246.md) to [qûm](../../strongs/h/h6966.md) him [ʿal](../../strongs/h/h5922.md) the [kōl](../../strongs/h/h3606.md) [malkû](../../strongs/h/h4437.md).

<a name="daniel_6_4"></a>Daniel 6:4

['ĕḏayin](../../strongs/h/h116.md) the [sārēḵ](../../strongs/h/h5632.md) and ['ăḥašdarpᵊnîn](../../strongs/h/h324.md) [hăvâ](../../strongs/h/h1934.md) [bᵊʿā'](../../strongs/h/h1156.md) to [šᵊḵaḥ](../../strongs/h/h7912.md) [ʿillâ](../../strongs/h/h5931.md) against [Dānîyē'L](../../strongs/h/h1841.md) [ṣaḏ](../../strongs/h/h6655.md) the [malkû](../../strongs/h/h4437.md); [kōl](../../strongs/h/h3606.md) they [yᵊḵēl](../../strongs/h/h3202.md) [šᵊḵaḥ](../../strongs/h/h7912.md) [lā'](../../strongs/h/h3809.md) [ʿillâ](../../strongs/h/h5931.md) [lā'](../../strongs/h/h3809.md) [šᵊḥaṯ](../../strongs/h/h7844.md); [qᵊḇēl](../../strongs/h/h6903.md) as he was ['ăman](../../strongs/h/h540.md), [lā'](../../strongs/h/h3809.md) was there [kōl](../../strongs/h/h3606.md) [šālû](../../strongs/h/h7960.md) or [šᵊḥaṯ](../../strongs/h/h7844.md) [šᵊḵaḥ](../../strongs/h/h7912.md) in [ʿal](../../strongs/h/h5922.md).

<a name="daniel_6_5"></a>Daniel 6:5

['ĕḏayin](../../strongs/h/h116.md) ['ămar](../../strongs/h/h560.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md), We shall [lā'](../../strongs/h/h3809.md) [šᵊḵaḥ](../../strongs/h/h7912.md) [kōl](../../strongs/h/h3606.md) [ʿillâ](../../strongs/h/h5931.md) [ʿal](../../strongs/h/h5922.md) [dēn](../../strongs/h/h1836.md) [Dānîyē'L](../../strongs/h/h1841.md), [lāhēn](../../strongs/h/h3861.md) we [šᵊḵaḥ](../../strongs/h/h7912.md) it against him concerning the [dāṯ](../../strongs/h/h1882.md) of his ['ĕlâ](../../strongs/h/h426.md).

<a name="daniel_6_6"></a>Daniel 6:6

['ĕḏayin](../../strongs/h/h116.md) ['illên](../../strongs/h/h459.md) [sārēḵ](../../strongs/h/h5632.md) and ['ăḥašdarpᵊnîn](../../strongs/h/h324.md) [rᵊḡaš](../../strongs/h/h7284.md) to the [meleḵ](../../strongs/h/h4430.md), and ['ămar](../../strongs/h/h560.md) [kēn](../../strongs/h/h3652.md) unto [ʿal](../../strongs/h/h5922.md), [meleḵ](../../strongs/h/h4430.md) [Daryāveš](../../strongs/h/h1868.md), [ḥăyā'](../../strongs/h/h2418.md) for [ʿālam](../../strongs/h/h5957.md).

<a name="daniel_6_7"></a>Daniel 6:7

[kōl](../../strongs/h/h3606.md) the [sārēḵ](../../strongs/h/h5632.md) of the [malkû](../../strongs/h/h4437.md), the [sᵊḡan](../../strongs/h/h5460.md), and the ['ăḥašdarpᵊnîn](../../strongs/h/h324.md), the [hadāḇar](../../strongs/h/h1907.md), and the [peḥâ](../../strongs/h/h6347.md), have [yᵊʿaṭ](../../strongs/h/h3272.md) to [qûm](../../strongs/h/h6966.md) a [meleḵ](../../strongs/h/h4430.md) [qᵊyām](../../strongs/h/h7010.md), and to make a [tᵊqēp̄](../../strongs/h/h8631.md) ['ĕsār](../../strongs/h/h633.md), that whosoever shall [bᵊʿā'](../../strongs/h/h1156.md) a [bāʿû](../../strongs/h/h1159.md) [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) ['ĕlâ](../../strongs/h/h426.md) or ['ēneš](../../strongs/h/h606.md) [ʿaḏ](../../strongs/h/h5705.md) [tᵊlāṯîn](../../strongs/h/h8533.md) [yôm](../../strongs/h/h3118.md), [lāhēn](../../strongs/h/h3861.md) of [min](../../strongs/h/h4481.md), O [meleḵ](../../strongs/h/h4430.md), he shall be [rᵊmâ](../../strongs/h/h7412.md) into the [gōḇ](../../strongs/h/h1358.md) of ['aryê](../../strongs/h/h744.md).

<a name="daniel_6_8"></a>Daniel 6:8

[kᵊʿan](../../strongs/h/h3705.md), O [meleḵ](../../strongs/h/h4430.md), [qûm](../../strongs/h/h6966.md) the ['ĕsār](../../strongs/h/h633.md), and [rᵊšam](../../strongs/h/h7560.md) the [kᵊṯāḇ](../../strongs/h/h3792.md), that it be [lā'](../../strongs/h/h3809.md) [šᵊnâ](../../strongs/h/h8133.md), according to the [dāṯ](../../strongs/h/h1882.md) of the [Māḏay](../../strongs/h/h4076.md) and [Pāras](../../strongs/h/h6540.md), which [ʿăḏā'](../../strongs/h/h5709.md) [lā'](../../strongs/h/h3809.md).

<a name="daniel_6_9"></a>Daniel 6:9

[kōl](../../strongs/h/h3606.md) [dēn](../../strongs/h/h1836.md) [qᵊḇēl](../../strongs/h/h6903.md) [meleḵ](../../strongs/h/h4430.md) [Daryāveš](../../strongs/h/h1868.md) [rᵊšam](../../strongs/h/h7560.md) the [kᵊṯāḇ](../../strongs/h/h3792.md) and the ['ĕsār](../../strongs/h/h633.md).

<a name="daniel_6_10"></a>Daniel 6:10

Now when [Dānîyē'L](../../strongs/h/h1841.md) [yᵊḏaʿ](../../strongs/h/h3046.md) that the [kᵊṯāḇ](../../strongs/h/h3792.md) was [rᵊšam](../../strongs/h/h7560.md), he [ʿălal](../../strongs/h/h5954.md) into his [bayiṯ](../../strongs/h/h1005.md); and his [kav](../../strongs/h/h3551.md) being [pĕthach](../../strongs/h/h6606.md) in his [ʿillîṯ](../../strongs/h/h5952.md) [neḡeḏ](../../strongs/h/h5049.md) [Yᵊrûšlēm](../../strongs/h/h3390.md), he [bᵊraḵ](../../strongs/h/h1289.md) [ʿal](../../strongs/h/h5922.md) his [bereḵ](../../strongs/h/h1291.md) [tᵊlāṯ](../../strongs/h/h8532.md) [zᵊmān](../../strongs/h/h2166.md) a [yôm](../../strongs/h/h3118.md), and [ṣᵊlâ](../../strongs/h/h6739.md), and [yeḏā'](../../strongs/h/h3029.md) [qŏḏām](../../strongs/h/h6925.md) his ['ĕlâ](../../strongs/h/h426.md), [qᵊḇēl](../../strongs/h/h6903.md) [kōl](../../strongs/h/h3606.md) he [hăvâ](../../strongs/h/h1934.md) [ʿăḇaḏ](../../strongs/h/h5648.md) [min](../../strongs/h/h4481.md) [dēn](../../strongs/h/h1836.md) [qaḏmâ](../../strongs/h/h6928.md).

<a name="daniel_6_11"></a>Daniel 6:11

['ĕḏayin](../../strongs/h/h116.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) [rᵊḡaš](../../strongs/h/h7284.md), and [šᵊḵaḥ](../../strongs/h/h7912.md) [Dānîyē'L](../../strongs/h/h1841.md) [bᵊʿā'](../../strongs/h/h1156.md) and making [ḥănan](../../strongs/h/h2604.md) [qŏḏām](../../strongs/h/h6925.md) his ['ĕlâ](../../strongs/h/h426.md).

<a name="daniel_6_12"></a>Daniel 6:12

['ĕḏayin](../../strongs/h/h116.md) they [qᵊrēḇ](../../strongs/h/h7127.md), and ['ămar](../../strongs/h/h560.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md) [ʿal](../../strongs/h/h5922.md) the [meleḵ](../../strongs/h/h4430.md) ['ĕsār](../../strongs/h/h633.md); Hast thou [lā'](../../strongs/h/h3809.md) [rᵊšam](../../strongs/h/h7560.md) an ['ĕsār](../../strongs/h/h633.md), that [kōl](../../strongs/h/h3606.md) ['ēneš](../../strongs/h/h606.md) that shall [bᵊʿā'](../../strongs/h/h1156.md) a [min](../../strongs/h/h4481.md) [kōl](../../strongs/h/h3606.md) ['ĕlâ](../../strongs/h/h426.md) or ['ēneš](../../strongs/h/h606.md) [ʿaḏ](../../strongs/h/h5705.md) [tᵊlāṯîn](../../strongs/h/h8533.md) [yôm](../../strongs/h/h3118.md), [lāhēn](../../strongs/h/h3861.md) of [min](../../strongs/h/h4481.md), O [meleḵ](../../strongs/h/h4430.md), shall be [rᵊmâ](../../strongs/h/h7412.md) into the [gōḇ](../../strongs/h/h1358.md) of ['aryê](../../strongs/h/h744.md)? The [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md), The [millâ](../../strongs/h/h4406.md) is [yaṣṣîḇ](../../strongs/h/h3330.md), according to the [dāṯ](../../strongs/h/h1882.md) of the [Māḏay](../../strongs/h/h4076.md) and [Pāras](../../strongs/h/h6540.md), which [ʿăḏā'](../../strongs/h/h5709.md) [lā'](../../strongs/h/h3809.md).

<a name="daniel_6_13"></a>Daniel 6:13

['ĕḏayin](../../strongs/h/h116.md) [ʿănâ](../../strongs/h/h6032.md) they and ['ămar](../../strongs/h/h560.md) [qŏḏām](../../strongs/h/h6925.md) the [meleḵ](../../strongs/h/h4430.md), That [Dānîyē'L](../../strongs/h/h1841.md), which is [min](../../strongs/h/h4481.md) the [bēn](../../strongs/h/h1123.md) of the [gālûṯ](../../strongs/h/h1547.md) of [Yᵊhûḏ](../../strongs/h/h3061.md), [śûm](../../strongs/h/h7761.md) [ṭᵊʿēm](../../strongs/h/h2942.md) [lā'](../../strongs/h/h3809.md) [ʿal](../../strongs/h/h5922.md), O [meleḵ](../../strongs/h/h4430.md), nor the ['ĕsār](../../strongs/h/h633.md) that thou hast [rᵊšam](../../strongs/h/h7560.md), but [bᵊʿā'](../../strongs/h/h1156.md) his [bāʿû](../../strongs/h/h1159.md) [tᵊlāṯ](../../strongs/h/h8532.md) [zᵊmān](../../strongs/h/h2166.md) a [yôm](../../strongs/h/h3118.md).

<a name="daniel_6_14"></a>Daniel 6:14

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md), when he [šᵊmaʿ](../../strongs/h/h8086.md) these [millâ](../../strongs/h/h4406.md), was [śagî'](../../strongs/h/h7690.md) [bᵊ'ēš](../../strongs/h/h888.md) with himself, and [śûm](../../strongs/h/h7761.md) his [bāl](../../strongs/h/h1079.md) [ʿal](../../strongs/h/h5922.md) [Dānîyē'L](../../strongs/h/h1841.md) to [šêziḇ](../../strongs/h/h7804.md) him: and he [hăvâ](../../strongs/h/h1934.md) [šᵊḏar](../../strongs/h/h7712.md) [ʿaḏ](../../strongs/h/h5705.md) the going [mēʿāl](../../strongs/h/h4606.md) of the [šemeš](../../strongs/h/h8122.md) to [nᵊṣal](../../strongs/h/h5338.md) him.

<a name="daniel_6_15"></a>Daniel 6:15

['ĕḏayin](../../strongs/h/h116.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) [rᵊḡaš](../../strongs/h/h7284.md) [ʿal](../../strongs/h/h5922.md) the [meleḵ](../../strongs/h/h4430.md), and ['ămar](../../strongs/h/h560.md) unto the [meleḵ](../../strongs/h/h4430.md), [yᵊḏaʿ](../../strongs/h/h3046.md), O [meleḵ](../../strongs/h/h4430.md), that the [dāṯ](../../strongs/h/h1882.md) of the [Māḏay](../../strongs/h/h4076.md) and [Pāras](../../strongs/h/h6540.md) is, That [kōl](../../strongs/h/h3606.md) ['ĕsār](../../strongs/h/h633.md) nor [qᵊyām](../../strongs/h/h7010.md) which the [meleḵ](../../strongs/h/h4430.md) [qûm](../../strongs/h/h6966.md) may be [šᵊnâ](../../strongs/h/h8133.md).

<a name="daniel_6_16"></a>Daniel 6:16

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) ['ămar](../../strongs/h/h560.md), and they ['ăṯâ](../../strongs/h/h858.md) [Dānîyē'L](../../strongs/h/h1841.md), and [rᵊmâ](../../strongs/h/h7412.md) him into the [gōḇ](../../strongs/h/h1358.md) of ['aryê](../../strongs/h/h744.md). Now the [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) unto [Dānîyē'L](../../strongs/h/h1841.md), Thy ['ĕlâ](../../strongs/h/h426.md) whom ['antâ](../../strongs/h/h607.md) [pᵊlaḥ](../../strongs/h/h6399.md) [tᵊḏîrā'](../../strongs/h/h8411.md), he will [šêziḇ](../../strongs/h/h7804.md) thee.

<a name="daniel_6_17"></a>Daniel 6:17

And [ḥaḏ](../../strongs/h/h2298.md) ['eḇen](../../strongs/h/h69.md) was ['ăṯâ](../../strongs/h/h858.md), and [śûm](../../strongs/h/h7761.md) [ʿal](../../strongs/h/h5922.md) the [pum](../../strongs/h/h6433.md) of the [gōḇ](../../strongs/h/h1358.md); and the [meleḵ](../../strongs/h/h4430.md) [ḥăṯam](../../strongs/h/h2857.md) it with his own [ʿizqā'](../../strongs/h/h5824.md), and with the [ʿizqā'](../../strongs/h/h5824.md) of his [raḇrᵊḇānîn](../../strongs/h/h7261.md); that the [ṣᵊḇû](../../strongs/h/h6640.md) might [lā'](../../strongs/h/h3809.md) be [šᵊnâ](../../strongs/h/h8133.md) concerning [Dānîyē'L](../../strongs/h/h1841.md).

<a name="daniel_6_18"></a>Daniel 6:18

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) ['ăzal](../../strongs/h/h236.md) to his [hêḵal](../../strongs/h/h1965.md), and passed the [bûṯ](../../strongs/h/h956.md) [ṭᵊvāṯ](../../strongs/h/h2908.md): [lā'](../../strongs/h/h3809.md) were [daḥăvâ](../../strongs/h/h1761.md) of musick [ʿălal](../../strongs/h/h5954.md) [qŏḏām](../../strongs/h/h6925.md) him: and his [šᵊnâ](../../strongs/h/h8139.md) [nᵊḏaḏ](../../strongs/h/h5075.md) from [ʿal](../../strongs/h/h5922.md).

<a name="daniel_6_19"></a>Daniel 6:19

['ĕḏayin](../../strongs/h/h116.md) the [meleḵ](../../strongs/h/h4430.md) [qûm](../../strongs/h/h6966.md) very [šᵊp̄arpār](../../strongs/h/h8238.md) in the [nōḡah](../../strongs/h/h5053.md), and ['ăzal](../../strongs/h/h236.md) in [bᵊhal](../../strongs/h/h927.md) unto the [gōḇ](../../strongs/h/h1358.md) of ['aryê](../../strongs/h/h744.md).

<a name="daniel_6_20"></a>Daniel 6:20

And when he [qᵊrēḇ](../../strongs/h/h7127.md) to the [gōḇ](../../strongs/h/h1358.md), he [zᵊʿiq](../../strongs/h/h2200.md) with a [ʿăṣaḇ](../../strongs/h/h6088.md) [qāl](../../strongs/h/h7032.md) unto [Dānîyē'L](../../strongs/h/h1841.md): and the [meleḵ](../../strongs/h/h4430.md) [ʿănâ](../../strongs/h/h6032.md) and ['ămar](../../strongs/h/h560.md) to [Dānîyē'L](../../strongs/h/h1841.md), O [Dānîyē'L](../../strongs/h/h1841.md), [ʿăḇaḏ](../../strongs/h/h5649.md) of the [ḥay](../../strongs/h/h2417.md) ['ĕlâ](../../strongs/h/h426.md), is thy ['ĕlâ](../../strongs/h/h426.md), whom thou [pᵊlaḥ](../../strongs/h/h6399.md) [tᵊḏîrā'](../../strongs/h/h8411.md), [yᵊḵēl](../../strongs/h/h3202.md) to [šêziḇ](../../strongs/h/h7804.md) thee [min](../../strongs/h/h4481.md) the ['aryê](../../strongs/h/h744.md)?

<a name="daniel_6_21"></a>Daniel 6:21

['ĕḏayin](../../strongs/h/h116.md) [mᵊlal](../../strongs/h/h4449.md) [Dānîyē'L](../../strongs/h/h1841.md) [ʿim](../../strongs/h/h5974.md) the [meleḵ](../../strongs/h/h4430.md), O [meleḵ](../../strongs/h/h4430.md), [ḥăyā'](../../strongs/h/h2418.md) for [ʿālam](../../strongs/h/h5957.md).

<a name="daniel_6_22"></a>Daniel 6:22

My ['ĕlâ](../../strongs/h/h426.md) hath [šᵊlaḥ](../../strongs/h/h7972.md) his [mal'aḵ](../../strongs/h/h4398.md), and hath [sᵊḡar](../../strongs/h/h5463.md) the ['aryê](../../strongs/h/h744.md) [pum](../../strongs/h/h6433.md), that they have [lā'](../../strongs/h/h3809.md) [ḥăḇal](../../strongs/h/h2255.md) me: forasmuch [kōl](../../strongs/h/h3606.md) [qᵊḇēl](../../strongs/h/h6903.md) [qŏḏām](../../strongs/h/h6925.md) him [zāḵû](../../strongs/h/h2136.md) was [šᵊḵaḥ](../../strongs/h/h7912.md) in me; and ['ap̄](../../strongs/h/h638.md) [qŏḏām](../../strongs/h/h6925.md) thee, O [meleḵ](../../strongs/h/h4430.md), have I [ʿăḇaḏ](../../strongs/h/h5648.md) [lā'](../../strongs/h/h3809.md) [ḥăḇûlâ](../../strongs/h/h2248.md).

<a name="daniel_6_23"></a>Daniel 6:23

['ĕḏayin](../../strongs/h/h116.md) was the [meleḵ](../../strongs/h/h4430.md) [śagî'](../../strongs/h/h7690.md) [ṭᵊ'ēḇ](../../strongs/h/h2868.md) for [ʿal](../../strongs/h/h5922.md), and ['ămar](../../strongs/h/h560.md) that they should [nᵊsaq](../../strongs/h/h5267.md) [Dānîyē'L](../../strongs/h/h1841.md) [nᵊsaq](../../strongs/h/h5267.md) out [min](../../strongs/h/h4481.md) the [gōḇ](../../strongs/h/h1358.md). So [Dānîyē'L](../../strongs/h/h1841.md) was taken [nᵊsaq](../../strongs/h/h5267.md) out [min](../../strongs/h/h4481.md) the [gōḇ](../../strongs/h/h1358.md), and [lā'](../../strongs/h/h3809.md) [kōl](../../strongs/h/h3606.md) of [ḥăḇal](../../strongs/h/h2257.md) was [šᵊḵaḥ](../../strongs/h/h7912.md) upon him, because he ['ăman](../../strongs/h/h540.md) in his ['ĕlâ](../../strongs/h/h426.md).

<a name="daniel_6_24"></a>Daniel 6:24

And the [meleḵ](../../strongs/h/h4430.md) ['ămar](../../strongs/h/h560.md), and they ['ăṯâ](../../strongs/h/h858.md) ['illēḵ](../../strongs/h/h479.md) [gᵊḇar](../../strongs/h/h1400.md) which had ['ăḵal](../../strongs/h/h399.md) [qᵊraṣ](../../strongs/h/h7170.md) [Dānîyē'L](../../strongs/h/h1841.md), and they [rᵊmâ](../../strongs/h/h7412.md) them into the [gōḇ](../../strongs/h/h1358.md) of ['aryê](../../strongs/h/h744.md), ['innûn](../../strongs/h/h581.md), their [bēn](../../strongs/h/h1123.md), and their [nāšîn](../../strongs/h/h5389.md); and the ['aryê](../../strongs/h/h744.md) had the [ʿaḏ](../../strongs/h/h5705.md) [šᵊlēṭ](../../strongs/h/h7981.md) of them, and [dᵊqaq](../../strongs/h/h1855.md) [kōl](../../strongs/h/h3606.md) their [gerem](../../strongs/h/h1635.md) in [dᵊqaq](../../strongs/h/h1855.md) or [lā'](../../strongs/h/h3809.md) they [mᵊṭā'](../../strongs/h/h4291.md) at the ['arʿîṯ](../../strongs/h/h773.md) of the [gōḇ](../../strongs/h/h1358.md).

<a name="daniel_6_25"></a>Daniel 6:25

['ĕḏayin](../../strongs/h/h116.md) [meleḵ](../../strongs/h/h4430.md) [Daryāveš](../../strongs/h/h1868.md) [kᵊṯaḇ](../../strongs/h/h3790.md) unto [kōl](../../strongs/h/h3606.md) [ʿam](../../strongs/h/h5972.md), ['ummâ](../../strongs/h/h524.md), and [liššān](../../strongs/h/h3961.md), that [dûr](../../strongs/h/h1753.md) in [kōl](../../strongs/h/h3606.md) the ['ăraʿ](../../strongs/h/h772.md); [šᵊlām](../../strongs/h/h8001.md) be [śᵊḡā'](../../strongs/h/h7680.md) unto you.

<a name="daniel_6_26"></a>Daniel 6:26

[min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) [śûm](../../strongs/h/h7761.md) a [ṭᵊʿēm](../../strongs/h/h2942.md), That in [kōl](../../strongs/h/h3606.md) [šālṭān](../../strongs/h/h7985.md) of my [malkû](../../strongs/h/h4437.md) men [hăvâ](../../strongs/h/h1934.md) [zûaʿ](../../strongs/h/h2112.md) and [dᵊḥal](../../strongs/h/h1763.md) [min](../../strongs/h/h4481.md) [qŏḏām](../../strongs/h/h6925.md) the ['ĕlâ](../../strongs/h/h426.md) of [Dānîyē'L](../../strongs/h/h1841.md): for he is the [ḥay](../../strongs/h/h2417.md) ['ĕlâ](../../strongs/h/h426.md), and [qayyām](../../strongs/h/h7011.md) for [ʿālam](../../strongs/h/h5957.md), and his [malkû](../../strongs/h/h4437.md) that which shall [lā'](../../strongs/h/h3809.md) be [ḥăḇal](../../strongs/h/h2255.md), and his [šālṭān](../../strongs/h/h7985.md) shall be even [ʿaḏ](../../strongs/h/h5705.md) the [sôp̄](../../strongs/h/h5491.md).

<a name="daniel_6_27"></a>Daniel 6:27

He [šêziḇ](../../strongs/h/h7804.md) and [nᵊṣal](../../strongs/h/h5338.md), and he [ʿăḇaḏ](../../strongs/h/h5648.md) ['āṯ](../../strongs/h/h852.md) and [tᵊmah](../../strongs/h/h8540.md) in [šᵊmayin](../../strongs/h/h8065.md) and in ['ăraʿ](../../strongs/h/h772.md), who hath [šêziḇ](../../strongs/h/h7804.md) [Dānîyē'L](../../strongs/h/h1841.md) [min](../../strongs/h/h4481.md) the [yaḏ](../../strongs/h/h3028.md) of the ['aryê](../../strongs/h/h744.md).

<a name="daniel_6_28"></a>Daniel 6:28

So [dēn](../../strongs/h/h1836.md) [Dānîyē'L](../../strongs/h/h1841.md) [ṣᵊlaḥ](../../strongs/h/h6744.md) in the [malkû](../../strongs/h/h4437.md) of [Daryāveš](../../strongs/h/h1868.md), and in the [malkû](../../strongs/h/h4437.md) of [Kôreš](../../strongs/h/h3567.md) the [parsî](../../strongs/h/h6543.md).

---

[Transliteral Bible](../bible.md)

[Daniel](daniel.md)

[Daniel 5](daniel_5.md) - [Daniel 7](daniel_7.md)