# [2 Thessalonians 1](https://www.blueletterbible.org/kjv/2th/1/1/s_1117001)

<a name="2thessalonians_1_1"></a>2 Thessalonians 1:1

[Paulos](../../strongs/g/g3972.md), and [silouanos](../../strongs/g/g4610.md), and [Timotheos](../../strongs/g/g5095.md), unto the [ekklēsia](../../strongs/g/g1577.md) of the [Thessalonikeus](../../strongs/g/g2331.md) in [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="2thessalonians_1_2"></a>2 Thessalonians 1:2

[charis](../../strongs/g/g5485.md) unto you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2thessalonians_1_3"></a>2 Thessalonians 1:3

We are [opheilō](../../strongs/g/g3784.md) to [eucharisteō](../../strongs/g/g2168.md) [theos](../../strongs/g/g2316.md) [pantote](../../strongs/g/g3842.md) for you, [adelphos](../../strongs/g/g80.md), as it is [axios](../../strongs/g/g514.md), because that your [pistis](../../strongs/g/g4102.md) [hyperauxanō](../../strongs/g/g5232.md), and the [agapē](../../strongs/g/g26.md) of every one of you all toward [allēlōn](../../strongs/g/g240.md) [pleonazō](../../strongs/g/g4121.md);

<a name="2thessalonians_1_4"></a>2 Thessalonians 1:4

So that we ourselves [kauchaomai](../../strongs/g/g2744.md) in you in the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md) for your [hypomonē](../../strongs/g/g5281.md) and [pistis](../../strongs/g/g4102.md) in all your [diōgmos](../../strongs/g/g1375.md) and [thlipsis](../../strongs/g/g2347.md) that ye [anechō](../../strongs/g/g430.md):

<a name="2thessalonians_1_5"></a>2 Thessalonians 1:5

an [endeigma](../../strongs/g/g1730.md) of the [dikaios](../../strongs/g/g1342.md) [krisis](../../strongs/g/g2920.md) of [theos](../../strongs/g/g2316.md), that ye may be [kataxioō](../../strongs/g/g2661.md) of the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md), for which ye also [paschō](../../strongs/g/g3958.md):

<a name="2thessalonians_1_6"></a>2 Thessalonians 1:6

[ei per](../../strongs/g/g1512.md) a [dikaios](../../strongs/g/g1342.md) thing with [theos](../../strongs/g/g2316.md) to [antapodidōmi](../../strongs/g/g467.md) [thlipsis](../../strongs/g/g2347.md) to them that [thlibō](../../strongs/g/g2346.md) you;

<a name="2thessalonians_1_7"></a>2 Thessalonians 1:7

And to you who are [thlibō](../../strongs/g/g2346.md) [anesis](../../strongs/g/g425.md) with us, when the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) shall be [apokalypsis](../../strongs/g/g602.md) from [ouranos](../../strongs/g/g3772.md) with his [dynamis](../../strongs/g/g1411.md) [aggelos](../../strongs/g/g32.md),

<a name="2thessalonians_1_8"></a>2 Thessalonians 1:8

In [phlox](../../strongs/g/g5395.md) [pyr](../../strongs/g/g4442.md) [didōmi](../../strongs/g/g1325.md) [ekdikēsis](../../strongs/g/g1557.md) on them that [eidō](../../strongs/g/g1492.md) not [theos](../../strongs/g/g2316.md), and that [hypakouō](../../strongs/g/g5219.md) not the [euaggelion](../../strongs/g/g2098.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="2thessalonians_1_9"></a>2 Thessalonians 1:9

Who shall be [dikē](../../strongs/g/g1349.md) [tinō](../../strongs/g/g5099.md) with [aiōnios](../../strongs/g/g166.md) [olethros](../../strongs/g/g3639.md) from the [prosōpon](../../strongs/g/g4383.md) of the [kyrios](../../strongs/g/g2962.md), and from the [doxa](../../strongs/g/g1391.md) of his [ischys](../../strongs/g/g2479.md);

<a name="2thessalonians_1_10"></a>2 Thessalonians 1:10

When he shall [erchomai](../../strongs/g/g2064.md) to be [endoxazomai](../../strongs/g/g1740.md) in his [hagios](../../strongs/g/g40.md), and to be [thaumazō](../../strongs/g/g2296.md) in all them that [pisteuō](../../strongs/g/g4100.md) (because our [martyrion](../../strongs/g/g3142.md) among you was [pisteuō](../../strongs/g/g4100.md)) in that day.

<a name="2thessalonians_1_11"></a>2 Thessalonians 1:11

Wherefore also we [proseuchomai](../../strongs/g/g4336.md) [pantote](../../strongs/g/g3842.md) for you, that our [theos](../../strongs/g/g2316.md) would [axioō](../../strongs/g/g515.md) you of [klēsis](../../strongs/g/g2821.md), and [plēroō](../../strongs/g/g4137.md) all the [eudokia](../../strongs/g/g2107.md) of [agathōsynē](../../strongs/g/g19.md), and the [ergon](../../strongs/g/g2041.md) of [pistis](../../strongs/g/g4102.md) with [dynamis](../../strongs/g/g1411.md):

<a name="2thessalonians_1_12"></a>2 Thessalonians 1:12

That the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) may be [endoxazomai](../../strongs/g/g1740.md) in you, and ye in him, according to the [charis](../../strongs/g/g5485.md) of our [theos](../../strongs/g/g2316.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[2 Thessalonians](2thessalonians.md)

[2 Thessalonians 2](2thessalonians_2.md)