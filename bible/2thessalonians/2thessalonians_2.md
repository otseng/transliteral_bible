# [2 Thessalonians 2](https://www.blueletterbible.org/kjv/2th/2/1/s_1118001)

<a name="2thessalonians_2_1"></a>2 Thessalonians 2:1

Now we [erōtaō](../../strongs/g/g2065.md) you, [adelphos](../../strongs/g/g80.md), by the [parousia](../../strongs/g/g3952.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and our [episynagōgē](../../strongs/g/g1997.md) unto him,

<a name="2thessalonians_2_2"></a>2 Thessalonians 2:2

That ye be not [tacheōs](../../strongs/g/g5030.md) [saleuō](../../strongs/g/g4531.md) in [nous](../../strongs/g/g3563.md), or be [throeō](../../strongs/g/g2360.md), neither by [pneuma](../../strongs/g/g4151.md), nor by [logos](../../strongs/g/g3056.md), nor by [epistolē](../../strongs/g/g1992.md) as from us, as that the [hēmera](../../strongs/g/g2250.md) of [Christos](../../strongs/g/g5547.md) is [enistēmi](../../strongs/g/g1764.md).

<a name="2thessalonians_2_3"></a>2 Thessalonians 2:3

Let no [tis](../../strongs/g/g5100.md) [exapataō](../../strongs/g/g1818.md) you by [mēdeis](../../strongs/g/g3367.md) [tropos](../../strongs/g/g5158.md): for, except there [erchomai](../../strongs/g/g2064.md) an [apostasia](../../strongs/g/g646.md) first, and that [anthrōpos](../../strongs/g/g444.md) of [hamartia](../../strongs/g/g266.md) be [apokalyptō](../../strongs/g/g601.md), the [huios](../../strongs/g/g5207.md) of [apōleia](../../strongs/g/g684.md);

<a name="2thessalonians_2_4"></a>2 Thessalonians 2:4

Who [antikeimai](../../strongs/g/g480.md) and [hyperairō](../../strongs/g/g5229.md) himself above all that is [legō](../../strongs/g/g3004.md) [theos](../../strongs/g/g2316.md), or that is [sebasma](../../strongs/g/g4574.md); so that he as [theos](../../strongs/g/g2316.md) [kathizō](../../strongs/g/g2523.md) in the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md), [apodeiknymi](../../strongs/g/g584.md) himself that he is [theos](../../strongs/g/g2316.md).

<a name="2thessalonians_2_5"></a>2 Thessalonians 2:5

[mnēmoneuō](../../strongs/g/g3421.md) ye not, that, when I was yet with you, I [legō](../../strongs/g/g3004.md) you these things?

<a name="2thessalonians_2_6"></a>2 Thessalonians 2:6

And now ye [eidō](../../strongs/g/g1492.md) what [katechō](../../strongs/g/g2722.md) that he might be [apokalyptō](../../strongs/g/g601.md) in his [kairos](../../strongs/g/g2540.md).

<a name="2thessalonians_2_7"></a>2 Thessalonians 2:7

For the [mystērion](../../strongs/g/g3466.md) of [anomia](../../strongs/g/g458.md) doth already [energeō](../../strongs/g/g1754.md): only he who now [katechō](../../strongs/g/g2722.md), until he be [ginomai](../../strongs/g/g1096.md) out of the [mesos](../../strongs/g/g3319.md).

<a name="2thessalonians_2_8"></a>2 Thessalonians 2:8

And then shall that [anomos](../../strongs/g/g459.md) be [apokalyptō](../../strongs/g/g601.md), whom the [kyrios](../../strongs/g/g2962.md) shall [analiskō](../../strongs/g/g355.md) with the [pneuma](../../strongs/g/g4151.md) of his [stoma](../../strongs/g/g4750.md), and shall [katargeō](../../strongs/g/g2673.md) with the [epiphaneia](../../strongs/g/g2015.md) of his [parousia](../../strongs/g/g3952.md):

<a name="2thessalonians_2_9"></a>2 Thessalonians 2:9

whose [parousia](../../strongs/g/g3952.md) is after the [energeia](../../strongs/g/g1753.md) of [Satanas](../../strongs/g/g4567.md) with all [dynamis](../../strongs/g/g1411.md) and [sēmeion](../../strongs/g/g4592.md) and [pseudos](../../strongs/g/g5579.md) [teras](../../strongs/g/g5059.md),

<a name="2thessalonians_2_10"></a>2 Thessalonians 2:10

And with all [apatē](../../strongs/g/g539.md) of [adikia](../../strongs/g/g93.md) in them that [apollymi](../../strongs/g/g622.md); because they [dechomai](../../strongs/g/g1209.md) not the [agapē](../../strongs/g/g26.md) of the [alētheia](../../strongs/g/g225.md), that they might be [sōzō](../../strongs/g/g4982.md).

<a name="2thessalonians_2_11"></a>2 Thessalonians 2:11

And for this cause [theos](../../strongs/g/g2316.md) shall [pempō](../../strongs/g/g3992.md) them [energeia](../../strongs/g/g1753.md) [planē](../../strongs/g/g4106.md), that they should [pisteuō](../../strongs/g/g4100.md) a [pseudos](../../strongs/g/g5579.md):

<a name="2thessalonians_2_12"></a>2 Thessalonians 2:12

That they all might be [krinō](../../strongs/g/g2919.md) who [pisteuō](../../strongs/g/g4100.md) not the [alētheia](../../strongs/g/g225.md), but had [eudokeō](../../strongs/g/g2106.md) in [adikia](../../strongs/g/g93.md).

<a name="2thessalonians_2_13"></a>2 Thessalonians 2:13

But we are [opheilō](../../strongs/g/g3784.md) to [eucharisteō](../../strongs/g/g2168.md) [pantote](../../strongs/g/g3842.md) to [theos](../../strongs/g/g2316.md) for you, [adelphos](../../strongs/g/g80.md) [agapaō](../../strongs/g/g25.md) of the [kyrios](../../strongs/g/g2962.md), because [theos](../../strongs/g/g2316.md) hath from the [archē](../../strongs/g/g746.md) [haireō](../../strongs/g/g138.md) you to [sōtēria](../../strongs/g/g4991.md) through [hagiasmos](../../strongs/g/g38.md) of the [pneuma](../../strongs/g/g4151.md) and [pistis](../../strongs/g/g4102.md) of the [alētheia](../../strongs/g/g225.md):

<a name="2thessalonians_2_14"></a>2 Thessalonians 2:14

Whereunto he [kaleō](../../strongs/g/g2564.md) you by our [euaggelion](../../strongs/g/g2098.md), to the [peripoiēsis](../../strongs/g/g4047.md) of the [doxa](../../strongs/g/g1391.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="2thessalonians_2_15"></a>2 Thessalonians 2:15

Therefore, [adelphos](../../strongs/g/g80.md), [stēkō](../../strongs/g/g4739.md), and [krateō](../../strongs/g/g2902.md) the [paradosis](../../strongs/g/g3862.md) which ye have been [didaskō](../../strongs/g/g1321.md), whether by [logos](../../strongs/g/g3056.md), or our [epistolē](../../strongs/g/g1992.md).

<a name="2thessalonians_2_16"></a>2 Thessalonians 2:16

Now our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) himself, and [theos](../../strongs/g/g2316.md), even our [patēr](../../strongs/g/g3962.md), which hath [agapaō](../../strongs/g/g25.md) us, and hath [didōmi](../../strongs/g/g1325.md) us [aiōnios](../../strongs/g/g166.md) [paraklēsis](../../strongs/g/g3874.md) and [agathos](../../strongs/g/g18.md) [elpis](../../strongs/g/g1680.md) through [charis](../../strongs/g/g5485.md),

<a name="2thessalonians_2_17"></a>2 Thessalonians 2:17

[parakaleō](../../strongs/g/g3870.md) your [kardia](../../strongs/g/g2588.md), and [stērizō](../../strongs/g/g4741.md) you in every [agathos](../../strongs/g/g18.md) [logos](../../strongs/g/g3056.md) and [ergon](../../strongs/g/g2041.md).

---

[Transliteral Bible](../bible.md)

[2 Thessalonians](2thessalonians.md)

[2 Thessalonians 1](2thessalonians_1.md) - [2 Thessalonians 3](2thessalonians_3.md)