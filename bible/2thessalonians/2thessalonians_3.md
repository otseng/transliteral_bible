# [2 Thessalonians 3](https://www.blueletterbible.org/kjv/2th/3/1/s_1119001)

<a name="2thessalonians_3_1"></a>2 Thessalonians 3:1

Finally, [adelphos](../../strongs/g/g80.md), [proseuchomai](../../strongs/g/g4336.md) for us, that the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md) may [trechō](../../strongs/g/g5143.md), and be [doxazō](../../strongs/g/g1392.md), even as with you:

<a name="2thessalonians_3_2"></a>2 Thessalonians 3:2

And that we may be [rhyomai](../../strongs/g/g4506.md) from [atopos](../../strongs/g/g824.md) and [ponēros](../../strongs/g/g4190.md) [anthrōpos](../../strongs/g/g444.md): for all have not [pistis](../../strongs/g/g4102.md).

<a name="2thessalonians_3_3"></a>2 Thessalonians 3:3

But the [kyrios](../../strongs/g/g2962.md) is [pistos](../../strongs/g/g4103.md), who shall [stērizō](../../strongs/g/g4741.md) you, and [phylassō](../../strongs/g/g5442.md) from [ponēros](../../strongs/g/g4190.md).

<a name="2thessalonians_3_4"></a>2 Thessalonians 3:4

And we have [peithō](../../strongs/g/g3982.md) in the [kyrios](../../strongs/g/g2962.md) touching you, that ye both [poieō](../../strongs/g/g4160.md) and will [poieō](../../strongs/g/g4160.md) the things which we [paraggellō](../../strongs/g/g3853.md) you.

<a name="2thessalonians_3_5"></a>2 Thessalonians 3:5

And the [kyrios](../../strongs/g/g2962.md) [kateuthynō](../../strongs/g/g2720.md) your [kardia](../../strongs/g/g2588.md) into the [agapē](../../strongs/g/g26.md) of [theos](../../strongs/g/g2316.md), and into the [hypomonē](../../strongs/g/g5281.md) waiting for [Christos](../../strongs/g/g5547.md).

<a name="2thessalonians_3_6"></a>2 Thessalonians 3:6

Now we [paraggellō](../../strongs/g/g3853.md) you, [adelphos](../../strongs/g/g80.md), in the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), that ye [stellō](../../strongs/g/g4724.md) yourselves from every [adelphos](../../strongs/g/g80.md) that [peripateō](../../strongs/g/g4043.md) [ataktōs](../../strongs/g/g814.md), and not after the [paradosis](../../strongs/g/g3862.md) which he [paralambanō](../../strongs/g/g3880.md) of us.

<a name="2thessalonians_3_7"></a>2 Thessalonians 3:7

For yourselves [eidō](../../strongs/g/g1492.md) how ye ought to [mimeomai](../../strongs/g/g3401.md) us: for we [atakteō](../../strongs/g/g812.md) not ourselves among you;

<a name="2thessalonians_3_8"></a>2 Thessalonians 3:8

Neither did we [phago](../../strongs/g/g5315.md) any man's [artos](../../strongs/g/g740.md) [dōrean](../../strongs/g/g1432.md); but [ergazomai](../../strongs/g/g2038.md) with [kopos](../../strongs/g/g2873.md) and [mochthos](../../strongs/g/g3449.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md), that we might not be [epibareō](../../strongs/g/g1912.md) to any of you:

<a name="2thessalonians_3_9"></a>2 Thessalonians 3:9

Not because we have not [exousia](../../strongs/g/g1849.md), but to [didōmi](../../strongs/g/g1325.md) ourselves a [typos](../../strongs/g/g5179.md) unto you to [mimeomai](../../strongs/g/g3401.md) us.

<a name="2thessalonians_3_10"></a>2 Thessalonians 3:10

For even when we were with you, this we [paraggellō](../../strongs/g/g3853.md) you, that if any would not [ergazomai](../../strongs/g/g2038.md), neither should he [esthiō](../../strongs/g/g2068.md).

<a name="2thessalonians_3_11"></a>2 Thessalonians 3:11

For we [akouō](../../strongs/g/g191.md) that there are some which [peripateō](../../strongs/g/g4043.md) among you [ataktōs](../../strongs/g/g814.md), [ergazomai](../../strongs/g/g2038.md) [mēdeis](../../strongs/g/g3367.md), but are [periergazomai](../../strongs/g/g4020.md).

<a name="2thessalonians_3_12"></a>2 Thessalonians 3:12

Now them that are such we [paraggellō](../../strongs/g/g3853.md) and [parakaleō](../../strongs/g/g3870.md) by our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), that with [hēsychia](../../strongs/g/g2271.md) they [ergazomai](../../strongs/g/g2038.md), and [esthiō](../../strongs/g/g2068.md) their own [artos](../../strongs/g/g740.md).

<a name="2thessalonians_3_13"></a>2 Thessalonians 3:13

But ye, [adelphos](../../strongs/g/g80.md), be not [ekkakeō](../../strongs/g/g1573.md) in [kalopoieō](../../strongs/g/g2569.md).

<a name="2thessalonians_3_14"></a>2 Thessalonians 3:14

And if any man [hypakouō](../../strongs/g/g5219.md) not our [logos](../../strongs/g/g3056.md) by this [epistolē](../../strongs/g/g1992.md), [sēmeioō](../../strongs/g/g4593.md) that man, and have no [synanamignymi](../../strongs/g/g4874.md) with him, that he may be [entrepō](../../strongs/g/g1788.md).

<a name="2thessalonians_3_15"></a>2 Thessalonians 3:15

Yet [hēgeomai](../../strongs/g/g2233.md) not as an [echthros](../../strongs/g/g2190.md), but [noutheteō](../../strongs/g/g3560.md) as an [adelphos](../../strongs/g/g80.md).

<a name="2thessalonians_3_16"></a>2 Thessalonians 3:16

Now the [kyrios](../../strongs/g/g2962.md) of [eirēnē](../../strongs/g/g1515.md) himself [didōmi](../../strongs/g/g1325.md) you [eirēnē](../../strongs/g/g1515.md) always by all means. The [kyrios](../../strongs/g/g2962.md) with you all.

<a name="2thessalonians_3_17"></a>2 Thessalonians 3:17

The [aspasmos](../../strongs/g/g783.md) of [Paulos](../../strongs/g/g3972.md) with mine own [cheir](../../strongs/g/g5495.md), which is the [sēmeion](../../strongs/g/g4592.md) in every [epistolē](../../strongs/g/g1992.md): so I [graphō](../../strongs/g/g1125.md).

<a name="2thessalonians_3_18"></a>2 Thessalonians 3:18

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[2 Thessalonians](2thessalonians.md)

[2 Thessalonians 2](2thessalonians_2.md)