# [2Chronicles 9](https://www.blueletterbible.org/kjv/2chronicles/9)

<a name="2chronicles_9_1"></a>2Chronicles 9:1

And when the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) [shama'](../../strongs/h/h8085.md) of the [šēmaʿ](../../strongs/h/h8088.md) of [Šᵊlōmô](../../strongs/h/h8010.md), she [bow'](../../strongs/h/h935.md) to [nāsâ](../../strongs/h/h5254.md) [Šᵊlōmô](../../strongs/h/h8010.md) with [ḥîḏâ](../../strongs/h/h2420.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), with a [me'od](../../strongs/h/h3966.md) [kāḇēḏ](../../strongs/h/h3515.md) [ḥayil](../../strongs/h/h2428.md), and [gāmāl](../../strongs/h/h1581.md) that [nasa'](../../strongs/h/h5375.md) [beśem](../../strongs/h/h1314.md), and [zāhāḇ](../../strongs/h/h2091.md) in [rōḇ](../../strongs/h/h7230.md), and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md): and when she was [bow'](../../strongs/h/h935.md) to [Šᵊlōmô](../../strongs/h/h8010.md), she [dabar](../../strongs/h/h1696.md) with him of all that was in her [lebab](../../strongs/h/h3824.md).

<a name="2chronicles_9_2"></a>2Chronicles 9:2

And [Šᵊlōmô](../../strongs/h/h8010.md) [nāḡaḏ](../../strongs/h/h5046.md) her all her [dabar](../../strongs/h/h1697.md): and there was nothing ['alam](../../strongs/h/h5956.md) from [Šᵊlōmô](../../strongs/h/h8010.md) which he [nāḡaḏ](../../strongs/h/h5046.md) her not.

<a name="2chronicles_9_3"></a>2Chronicles 9:3

And when the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) had [ra'ah](../../strongs/h/h7200.md) the [ḥāḵmâ](../../strongs/h/h2451.md) of [Šᵊlōmô](../../strongs/h/h8010.md), and the [bayith](../../strongs/h/h1004.md) that he had [bānâ](../../strongs/h/h1129.md),

<a name="2chronicles_9_4"></a>2Chronicles 9:4

And the [ma'akal](../../strongs/h/h3978.md) of his [šulḥān](../../strongs/h/h7979.md), and the [môšāḇ](../../strongs/h/h4186.md) of his ['ebed](../../strongs/h/h5650.md), and the [maʿămāḏ](../../strongs/h/h4612.md) of his [sharath](../../strongs/h/h8334.md), and their [malbûš](../../strongs/h/h4403.md); his [šāqâ](../../strongs/h/h8248.md) also, and their [malbûš](../../strongs/h/h4403.md); and his [ʿălîyâ](../../strongs/h/h5944.md) by which he [ʿālâ](../../strongs/h/h5927.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); there was no more [ruwach](../../strongs/h/h7307.md) in her.

<a name="2chronicles_9_5"></a>2Chronicles 9:5

And she ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md), It was an ['emeth](../../strongs/h/h571.md) [dabar](../../strongs/h/h1697.md) which I [shama'](../../strongs/h/h8085.md) in mine own ['erets](../../strongs/h/h776.md) of thine [dabar](../../strongs/h/h1697.md), and of thy [ḥāḵmâ](../../strongs/h/h2451.md):

<a name="2chronicles_9_6"></a>2Chronicles 9:6

Howbeit I ['aman](../../strongs/h/h539.md) not their [dabar](../../strongs/h/h1697.md), until I [bow'](../../strongs/h/h935.md), and mine ['ayin](../../strongs/h/h5869.md) had [ra'ah](../../strongs/h/h7200.md) it: and, behold, the one [ḥēṣî](../../strongs/h/h2677.md) of the [marbîṯ](../../strongs/h/h4768.md) of thy [ḥāḵmâ](../../strongs/h/h2451.md) was not [nāḡaḏ](../../strongs/h/h5046.md) me: for thou exceedest the [šᵊmûʿâ](../../strongs/h/h8052.md) that I [shama'](../../strongs/h/h8085.md).

<a name="2chronicles_9_7"></a>2Chronicles 9:7

['esher](../../strongs/h/h835.md) are thy ['enowsh](../../strongs/h/h582.md), and ['esher](../../strongs/h/h835.md) are these thy ['ebed](../../strongs/h/h5650.md), which ['amad](../../strongs/h/h5975.md) [tāmîḏ](../../strongs/h/h8548.md) [paniym](../../strongs/h/h6440.md) thee, and [shama'](../../strongs/h/h8085.md) thy [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="2chronicles_9_8"></a>2Chronicles 9:8

[barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), which [ḥāp̄ēṣ](../../strongs/h/h2654.md) in thee to [nathan](../../strongs/h/h5414.md) thee on his [kicce'](../../strongs/h/h3678.md), to be [melek](../../strongs/h/h4428.md) for [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md): because thy ['Elohiym](../../strongs/h/h430.md) ['ahăḇâ](../../strongs/h/h160.md) [Yisra'el](../../strongs/h/h3478.md), to ['amad](../../strongs/h/h5975.md) them ['owlam](../../strongs/h/h5769.md), therefore [nathan](../../strongs/h/h5414.md) he thee [melek](../../strongs/h/h4428.md) over them, to ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md).

<a name="2chronicles_9_9"></a>2Chronicles 9:9

And she [nathan](../../strongs/h/h5414.md) the [melek](../../strongs/h/h4428.md) an hundred and twenty [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), and of [beśem](../../strongs/h/h1314.md) [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md), and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md): neither was there any such [beśem](../../strongs/h/h1314.md) as the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) [nathan](../../strongs/h/h5414.md) [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="2chronicles_9_10"></a>2Chronicles 9:10

And the ['ebed](../../strongs/h/h5650.md) also of [Ḥûrām](../../strongs/h/h2361.md), and the ['ebed](../../strongs/h/h5650.md) of [Šᵊlōmô](../../strongs/h/h8010.md), which [bow'](../../strongs/h/h935.md) [zāhāḇ](../../strongs/h/h2091.md) from ['Ôp̄îr](../../strongs/h/h211.md), [bow'](../../strongs/h/h935.md) ['algûmmîm](../../strongs/h/h418.md) ['ets](../../strongs/h/h6086.md) and [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md).

<a name="2chronicles_9_11"></a>2Chronicles 9:11

And the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) of the ['algûmmîm](../../strongs/h/h418.md) ['ets](../../strongs/h/h6086.md) [mĕcillah](../../strongs/h/h4546.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and to the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [kinnôr](../../strongs/h/h3658.md) and [neḇel](../../strongs/h/h5035.md) for [shiyr](../../strongs/h/h7891.md): and there were none such [ra'ah](../../strongs/h/h7200.md) [paniym](../../strongs/h/h6440.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_9_12"></a>2Chronicles 9:12

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) to the [malkâ](../../strongs/h/h4436.md) of [Šᵊḇā'](../../strongs/h/h7614.md) all her [chephets](../../strongs/h/h2656.md), whatsoever she [sha'al](../../strongs/h/h7592.md), beside that which she had [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md). So she [hāp̄aḵ](../../strongs/h/h2015.md), and [yālaḵ](../../strongs/h/h3212.md) to her own ['erets](../../strongs/h/h776.md), she and her ['ebed](../../strongs/h/h5650.md).

<a name="2chronicles_9_13"></a>2Chronicles 9:13

Now the [mišqāl](../../strongs/h/h4948.md) of [zāhāḇ](../../strongs/h/h2091.md) that [bow'](../../strongs/h/h935.md) to [Šᵊlōmô](../../strongs/h/h8010.md) in one [šānâ](../../strongs/h/h8141.md) was six hundred and threescore and six [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md);

<a name="2chronicles_9_14"></a>2Chronicles 9:14

Beside that which ['enowsh](../../strongs/h/h582.md) [tûr](../../strongs/h/h8446.md) and [sāḥar](../../strongs/h/h5503.md) [bow'](../../strongs/h/h935.md). And all the [melek](../../strongs/h/h4428.md) of [ʿĂrāḇ](../../strongs/h/h6152.md) and [peḥâ](../../strongs/h/h6346.md) of the ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md) [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md) to [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="2chronicles_9_15"></a>2Chronicles 9:15

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) two hundred [tsinnah](../../strongs/h/h6793.md) of [šāḥaṭ](../../strongs/h/h7820.md) [zāhāḇ](../../strongs/h/h2091.md): six hundred shekels of [šāḥaṭ](../../strongs/h/h7820.md) [zāhāḇ](../../strongs/h/h2091.md) [ʿālâ](../../strongs/h/h5927.md) to one [tsinnah](../../strongs/h/h6793.md).

<a name="2chronicles_9_16"></a>2Chronicles 9:16

And three hundred [magen](../../strongs/h/h4043.md) made he of [šāḥaṭ](../../strongs/h/h7820.md) [zāhāḇ](../../strongs/h/h2091.md): three hundred shekels of [zāhāḇ](../../strongs/h/h2091.md) [ʿālâ](../../strongs/h/h5927.md) to one [magen](../../strongs/h/h4043.md). And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) them in the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md) of [Lᵊḇānôn](../../strongs/h/h3844.md).

<a name="2chronicles_9_17"></a>2Chronicles 9:17

Moreover the [melek](../../strongs/h/h4428.md) ['asah](../../strongs/h/h6213.md) a [gadowl](../../strongs/h/h1419.md) [kicce'](../../strongs/h/h3678.md) of [šēn](../../strongs/h/h8127.md), and [ṣāp̄â](../../strongs/h/h6823.md) it with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_9_18"></a>2Chronicles 9:18

And there were six [maʿălâ](../../strongs/h/h4609.md) to the [kicce'](../../strongs/h/h3678.md), with a [keḇeš](../../strongs/h/h3534.md) of [zāhāḇ](../../strongs/h/h2091.md), which were ['āḥaz](../../strongs/h/h270.md) to the [kicce'](../../strongs/h/h3678.md), and [yad](../../strongs/h/h3027.md) on each side of the [yashab](../../strongs/h/h3427.md) [maqowm](../../strongs/h/h4725.md), and two ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) by the [yad](../../strongs/h/h3027.md):

<a name="2chronicles_9_19"></a>2Chronicles 9:19

And twelve ['ariy](../../strongs/h/h738.md) ['amad](../../strongs/h/h5975.md) there on the one side and on the other upon the six [maʿălâ](../../strongs/h/h4609.md). There was not the like ['asah](../../strongs/h/h6213.md) in any [mamlāḵâ](../../strongs/h/h4467.md).

<a name="2chronicles_9_20"></a>2Chronicles 9:20

And all the [mašqê](../../strongs/h/h4945.md) [kĕliy](../../strongs/h/h3627.md) of [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) were of [zāhāḇ](../../strongs/h/h2091.md), and all the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of the [yaʿar](../../strongs/h/h3293.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) were of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md): none were of [keceph](../../strongs/h/h3701.md); it was not [mᵊ'ûmâ](../../strongs/h/h3972.md) thing [chashab](../../strongs/h/h2803.md) of in the [yowm](../../strongs/h/h3117.md) of [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="2chronicles_9_21"></a>2Chronicles 9:21

For the [melek](../../strongs/h/h4428.md) ['ŏnîyâ](../../strongs/h/h591.md) [halak](../../strongs/h/h1980.md) to [Taršîš](../../strongs/h/h8659.md) with the ['ebed](../../strongs/h/h5650.md) of [Ḥûrām](../../strongs/h/h2361.md): every three [šānâ](../../strongs/h/h8141.md) once [bow'](../../strongs/h/h935.md) the ['ŏnîyâ](../../strongs/h/h591.md) of [Taršîš](../../strongs/h/h8659.md) [nasa'](../../strongs/h/h5375.md) [zāhāḇ](../../strongs/h/h2091.md), and [keceph](../../strongs/h/h3701.md), [šenhabîm](../../strongs/h/h8143.md), and [qôp̄](../../strongs/h/h6971.md), and [tukîyîm](../../strongs/h/h8500.md).

<a name="2chronicles_9_22"></a>2Chronicles 9:22

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [gāḏal](../../strongs/h/h1431.md) all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) in [ʿōšer](../../strongs/h/h6239.md) and [ḥāḵmâ](../../strongs/h/h2451.md).

<a name="2chronicles_9_23"></a>2Chronicles 9:23

And all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) [bāqaš](../../strongs/h/h1245.md) the [paniym](../../strongs/h/h6440.md) of [Šᵊlōmô](../../strongs/h/h8010.md), to [shama'](../../strongs/h/h8085.md) his [ḥāḵmâ](../../strongs/h/h2451.md), that ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) in his [leb](../../strongs/h/h3820.md).

<a name="2chronicles_9_24"></a>2Chronicles 9:24

And they [bow'](../../strongs/h/h935.md) every ['iysh](../../strongs/h/h376.md) his [minchah](../../strongs/h/h4503.md), [kĕliy](../../strongs/h/h3627.md) of [keceph](../../strongs/h/h3701.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md), and [śalmâ](../../strongs/h/h8008.md), [nešeq](../../strongs/h/h5402.md), and [beśem](../../strongs/h/h1314.md), [sûs](../../strongs/h/h5483.md), and [pereḏ](../../strongs/h/h6505.md), a [dabar](../../strongs/h/h1697.md) [šānâ](../../strongs/h/h8141.md) by [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_9_25"></a>2Chronicles 9:25

And [Šᵊlōmô](../../strongs/h/h8010.md) had four thousand ['urvâ](../../strongs/h/h723.md) for [sûs](../../strongs/h/h5483.md) and [merkāḇâ](../../strongs/h/h4818.md), and twelve thousand [pārāš](../../strongs/h/h6571.md); whom he [yānaḥ](../../strongs/h/h3240.md) in the [reḵeḇ](../../strongs/h/h7393.md) [ʿîr](../../strongs/h/h5892.md), and with the [melek](../../strongs/h/h4428.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_9_26"></a>2Chronicles 9:26

And he [mashal](../../strongs/h/h4910.md) over all the [melek](../../strongs/h/h4428.md) from the [nāhār](../../strongs/h/h5104.md) even unto the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and to the [gᵊḇûl](../../strongs/h/h1366.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="2chronicles_9_27"></a>2Chronicles 9:27

And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) [keceph](../../strongs/h/h3701.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) as ['eben](../../strongs/h/h68.md), and ['erez](../../strongs/h/h730.md) [nathan](../../strongs/h/h5414.md) he as the [šiqmâ](../../strongs/h/h8256.md) that are in the low [šᵊp̄ēlâ](../../strongs/h/h8219.md) in [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_9_28"></a>2Chronicles 9:28

And they [yāṣā'](../../strongs/h/h3318.md) unto [Šᵊlōmô](../../strongs/h/h8010.md) [sûs](../../strongs/h/h5483.md) out of [Mitsrayim](../../strongs/h/h4714.md), and out of all ['erets](../../strongs/h/h776.md).

<a name="2chronicles_9_29"></a>2Chronicles 9:29

Now the [šᵊ'ār](../../strongs/h/h7605.md) of the [dabar](../../strongs/h/h1697.md) of [Šᵊlōmô](../../strongs/h/h8010.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [dabar](../../strongs/h/h1697.md) of [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md), and in the [nᵊḇû'â](../../strongs/h/h5016.md) of ['Ăḥîyâ](../../strongs/h/h281.md) the [Šîlōnî](../../strongs/h/h7888.md), and in the [ḥāzôṯ](../../strongs/h/h2378.md) of [YeʿDay](../../strongs/h/h3260.md) the [ḥōzê](../../strongs/h/h2374.md) against [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md)?

<a name="2chronicles_9_30"></a>2Chronicles 9:30

And [Šᵊlōmô](../../strongs/h/h8010.md) [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) over all [Yisra'el](../../strongs/h/h3478.md) forty [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_9_31"></a>2Chronicles 9:31

And [Šᵊlōmô](../../strongs/h/h8010.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and he was [qāḇar](../../strongs/h/h6912.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 8](2chronicles_8.md) - [2Chronicles 10](2chronicles_10.md)