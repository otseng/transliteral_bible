# [2Chronicles 20](https://www.blueletterbible.org/kjv/2chronicles/20)

<a name="2chronicles_20_1"></a>2Chronicles 20:1

It came to pass ['aḥar](../../strongs/h/h310.md) also, that the [ben](../../strongs/h/h1121.md) of [Mô'āḇ](../../strongs/h/h4124.md), and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and with them other beside the [ʿAmmôn](../../strongs/h/h5984.md), [bow'](../../strongs/h/h935.md) against [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) to [milḥāmâ](../../strongs/h/h4421.md).

<a name="2chronicles_20_2"></a>2Chronicles 20:2

Then there [bow'](../../strongs/h/h935.md) some that [nāḡaḏ](../../strongs/h/h5046.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), ['āmar](../../strongs/h/h559.md), There [bow'](../../strongs/h/h935.md) a [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md) against thee from [ʿēḇer](../../strongs/h/h5676.md) the [yam](../../strongs/h/h3220.md) on this side ['Ărām](../../strongs/h/h758.md); and, behold, they be in [Ḥaṣṣôn Tāmār](../../strongs/h/h2688.md), which is [ʿÊn Ḡeḏî](../../strongs/h/h5872.md).

<a name="2chronicles_20_3"></a>2Chronicles 20:3

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [yare'](../../strongs/h/h3372.md), and [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), and [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md) throughout all [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_20_4"></a>2Chronicles 20:4

And [Yehuwdah](../../strongs/h/h3063.md) gathered themselves [qāḇaṣ](../../strongs/h/h6908.md), to [bāqaš](../../strongs/h/h1245.md) help of [Yĕhovah](../../strongs/h/h3068.md): even out of all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) they [bow'](../../strongs/h/h935.md) to [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_20_5"></a>2Chronicles 20:5

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['amad](../../strongs/h/h5975.md) in the [qāhēl](../../strongs/h/h6951.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), [paniym](../../strongs/h/h6440.md) the [ḥāḏāš](../../strongs/h/h2319.md) [ḥāṣēr](../../strongs/h/h2691.md),

<a name="2chronicles_20_6"></a>2Chronicles 20:6

And ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of our ['ab](../../strongs/h/h1.md), art not thou ['Elohiym](../../strongs/h/h430.md) in [shamayim](../../strongs/h/h8064.md)? and [mashal](../../strongs/h/h4910.md) not thou over all the [mamlāḵâ](../../strongs/h/h4467.md) of the [gowy](../../strongs/h/h1471.md)? and in thine [yad](../../strongs/h/h3027.md) is there not [koach](../../strongs/h/h3581.md) and [gᵊḇûrâ](../../strongs/h/h1369.md), so that none is able to [yatsab](../../strongs/h/h3320.md) thee?

<a name="2chronicles_20_7"></a>2Chronicles 20:7

Art not thou our ['Elohiym](../../strongs/h/h430.md), who didst [yarash](../../strongs/h/h3423.md) the [yashab](../../strongs/h/h3427.md) of this ['erets](../../strongs/h/h776.md) [paniym](../../strongs/h/h6440.md) thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and [nathan](../../strongs/h/h5414.md) it to the [zera'](../../strongs/h/h2233.md) of ['Abraham](../../strongs/h/h85.md) thy ['ahab](../../strongs/h/h157.md) ['owlam](../../strongs/h/h5769.md)?

<a name="2chronicles_20_8"></a>2Chronicles 20:8

And they [yashab](../../strongs/h/h3427.md) therein, and have [bānâ](../../strongs/h/h1129.md) thee a [miqdash](../../strongs/h/h4720.md) therein for thy [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_20_9"></a>2Chronicles 20:9

If, when [ra'](../../strongs/h/h7451.md) [bow'](../../strongs/h/h935.md) upon us, as the [chereb](../../strongs/h/h2719.md), [šᵊp̄ôṭ](../../strongs/h/h8196.md), or [deḇer](../../strongs/h/h1698.md), or [rāʿāḇ](../../strongs/h/h7458.md), we ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) this [bayith](../../strongs/h/h1004.md), and in thy [paniym](../../strongs/h/h6440.md), (for thy [shem](../../strongs/h/h8034.md) is in this [bayith](../../strongs/h/h1004.md),) and [zāʿaq](../../strongs/h/h2199.md) unto thee in our [tsarah](../../strongs/h/h6869.md), then thou wilt [shama'](../../strongs/h/h8085.md) and [yasha'](../../strongs/h/h3467.md).

<a name="2chronicles_20_10"></a>2Chronicles 20:10

And now, behold, the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) and [Mô'āḇ](../../strongs/h/h4124.md) and [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), whom thou wouldest not [nathan](../../strongs/h/h5414.md) [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md), when they [bow'](../../strongs/h/h935.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), but they [cuwr](../../strongs/h/h5493.md) from them, and [šāmaḏ](../../strongs/h/h8045.md) them not;

<a name="2chronicles_20_11"></a>2Chronicles 20:11

Behold, how they [gamal](../../strongs/h/h1580.md) us, to [bow'](../../strongs/h/h935.md) to [gāraš](../../strongs/h/h1644.md) us of thy [yᵊruššâ](../../strongs/h/h3425.md), which thou hast given us to [yarash](../../strongs/h/h3423.md).

<a name="2chronicles_20_12"></a>2Chronicles 20:12

O our ['Elohiym](../../strongs/h/h430.md), wilt thou not [shaphat](../../strongs/h/h8199.md) them? for we have no [koach](../../strongs/h/h3581.md) [paniym](../../strongs/h/h6440.md) this [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md) that [bow'](../../strongs/h/h935.md) against us; neither [yada'](../../strongs/h/h3045.md) we what to ['asah](../../strongs/h/h6213.md): but our ['ayin](../../strongs/h/h5869.md) are upon thee.

<a name="2chronicles_20_13"></a>2Chronicles 20:13

And all [Yehuwdah](../../strongs/h/h3063.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), with their [ṭap̄](../../strongs/h/h2945.md), their ['ishshah](../../strongs/h/h802.md), and their [ben](../../strongs/h/h1121.md).

<a name="2chronicles_20_14"></a>2Chronicles 20:14

Then upon [Yaḥăzî'Ēl](../../strongs/h/h3166.md) the [ben](../../strongs/h/h1121.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), the [ben](../../strongs/h/h1121.md) of [Bᵊnāyâ](../../strongs/h/h1141.md), the [ben](../../strongs/h/h1121.md) of [YᵊʿÎ'Ēl](../../strongs/h/h3273.md), the [ben](../../strongs/h/h1121.md) of [Matanyâ](../../strongs/h/h4983.md), a [Lᵊvî](../../strongs/h/h3881.md) of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md), came the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) in the [tavek](../../strongs/h/h8432.md) of the [qāhēl](../../strongs/h/h6951.md);

<a name="2chronicles_20_15"></a>2Chronicles 20:15

And he ['āmar](../../strongs/h/h559.md), [qashab](../../strongs/h/h7181.md) ye, all [Yehuwdah](../../strongs/h/h3063.md), and ye [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and thou [melek](../../strongs/h/h4428.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto you, Be not [yare'](../../strongs/h/h3372.md) nor [ḥāṯaṯ](../../strongs/h/h2865.md) by [paniym](../../strongs/h/h6440.md) of this [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md); for the [milḥāmâ](../../strongs/h/h4421.md) is not yours, but ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_20_16"></a>2Chronicles 20:16

[māḥār](../../strongs/h/h4279.md) go ye [yarad](../../strongs/h/h3381.md) against them: behold, they [ʿālâ](../../strongs/h/h5927.md) by the [maʿălê](../../strongs/h/h4608.md) of [Ṣîṣ](../../strongs/h/h6732.md); and ye shall [māṣā'](../../strongs/h/h4672.md) them at the [sôp̄](../../strongs/h/h5490.md) of the [nachal](../../strongs/h/h5158.md), [paniym](../../strongs/h/h6440.md) the [midbar](../../strongs/h/h4057.md) of [Yᵊrû'Ēl](../../strongs/h/h3385.md).

<a name="2chronicles_20_17"></a>2Chronicles 20:17

Ye shall not need to [lāḥam](../../strongs/h/h3898.md) in [zō'ṯ](../../strongs/h/h2063.md): [yatsab](../../strongs/h/h3320.md) yourselves, ['amad](../../strongs/h/h5975.md) ye still, and [ra'ah](../../strongs/h/h7200.md) the [yĕshuw'ah](../../strongs/h/h3444.md) of [Yĕhovah](../../strongs/h/h3068.md) with you, O [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md): [yare'](../../strongs/h/h3372.md) not, nor be [ḥāṯaṯ](../../strongs/h/h2865.md); [māḥār](../../strongs/h/h4279.md) [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) them: for [Yĕhovah](../../strongs/h/h3068.md) will be with you.

<a name="2chronicles_20_18"></a>2Chronicles 20:18

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [qāḏaḏ](../../strongs/h/h6915.md) his head with his ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md): and all [Yehuwdah](../../strongs/h/h3063.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_20_19"></a>2Chronicles 20:19

And the [Lᵊvî](../../strongs/h/h3881.md), of the [ben](../../strongs/h/h1121.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md), and of the [ben](../../strongs/h/h1121.md) of the [Qārḥî](../../strongs/h/h7145.md), [quwm](../../strongs/h/h6965.md) to [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) on [maʿal](../../strongs/h/h4605.md).

<a name="2chronicles_20_20"></a>2Chronicles 20:20

And they [šāḵam](../../strongs/h/h7925.md) in the [boqer](../../strongs/h/h1242.md), and [yāṣā'](../../strongs/h/h3318.md) into the [midbar](../../strongs/h/h4057.md) of [Tᵊqôaʿ](../../strongs/h/h8620.md): and as they [yāṣā'](../../strongs/h/h3318.md), [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['amad](../../strongs/h/h5975.md) and ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) me, O [Yehuwdah](../../strongs/h/h3063.md), and ye [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); ['aman](../../strongs/h/h539.md) in [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), so shall ye be ['aman](../../strongs/h/h539.md); ['aman](../../strongs/h/h539.md) his [nāḇî'](../../strongs/h/h5030.md), so shall ye [tsalach](../../strongs/h/h6743.md).

<a name="2chronicles_20_21"></a>2Chronicles 20:21

And when he had [ya'ats](../../strongs/h/h3289.md) with the ['am](../../strongs/h/h5971.md), he ['amad](../../strongs/h/h5975.md) [shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), and that should [halal](../../strongs/h/h1984.md) the [hăḏārâ](../../strongs/h/h1927.md) of [qodesh](../../strongs/h/h6944.md), as they [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) the [chalats](../../strongs/h/h2502.md), and to ['āmar](../../strongs/h/h559.md), [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md); for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md).

<a name="2chronicles_20_22"></a>2Chronicles 20:22

And [ʿēṯ](../../strongs/h/h6256.md) they [ḥālal](../../strongs/h/h2490.md) to [rinnah](../../strongs/h/h7440.md) and to [tehillah](../../strongs/h/h8416.md), [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) ['arab](../../strongs/h/h693.md) against the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), [Mô'āḇ](../../strongs/h/h4124.md), and [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), which were [bow'](../../strongs/h/h935.md) against [Yehuwdah](../../strongs/h/h3063.md); and they were [nāḡap̄](../../strongs/h/h5062.md).

<a name="2chronicles_20_23"></a>2Chronicles 20:23

For the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) and [Mô'āḇ](../../strongs/h/h4124.md) ['amad](../../strongs/h/h5975.md) against the [yashab](../../strongs/h/h3427.md) of [har](../../strongs/h/h2022.md) [Śēʿîr](../../strongs/h/h8165.md), utterly to [ḥāram](../../strongs/h/h2763.md) and [šāmaḏ](../../strongs/h/h8045.md) them: and when they had made a [kalah](../../strongs/h/h3615.md) of the [yashab](../../strongs/h/h3427.md) of [Śēʿîr](../../strongs/h/h8165.md), every ['iysh](../../strongs/h/h376.md) [ʿāzar](../../strongs/h/h5826.md) to [mašḥîṯ](../../strongs/h/h4889.md) [rea'](../../strongs/h/h7453.md).

<a name="2chronicles_20_24"></a>2Chronicles 20:24

And when [Yehuwdah](../../strongs/h/h3063.md) [bow'](../../strongs/h/h935.md) toward the [miṣpê](../../strongs/h/h4707.md) [Miṣpê](../../strongs/h/h4708.md) in the [midbar](../../strongs/h/h4057.md), they [panah](../../strongs/h/h6437.md) unto the [hāmôn](../../strongs/h/h1995.md), and, behold, they were [peḡer](../../strongs/h/h6297.md) [naphal](../../strongs/h/h5307.md) to the ['erets](../../strongs/h/h776.md), and none [pᵊlêṭâ](../../strongs/h/h6413.md).

<a name="2chronicles_20_25"></a>2Chronicles 20:25

And when [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) and his ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to take [bāzaz](../../strongs/h/h962.md) the [šālāl](../../strongs/h/h7998.md) of them, they [māṣā'](../../strongs/h/h4672.md) among them in [rōḇ](../../strongs/h/h7230.md) both [rᵊḵûš](../../strongs/h/h7399.md) with the [peḡer](../../strongs/h/h6297.md), and [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md), which they stripped [natsal](../../strongs/h/h5337.md) for themselves, more than they could [maśśā'](../../strongs/h/h4853.md): and they were three [yowm](../../strongs/h/h3117.md) in [bāzaz](../../strongs/h/h962.md) of the [šālāl](../../strongs/h/h7998.md), it was so [rab](../../strongs/h/h7227.md).

<a name="2chronicles_20_26"></a>2Chronicles 20:26

And on the fourth [yowm](../../strongs/h/h3117.md) they [qāhal](../../strongs/h/h6950.md) themselves in the [ʿēmeq](../../strongs/h/h6010.md) of [Bᵊrāḵâ](../../strongs/h/h1294.md); for there they [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md): therefore the [shem](../../strongs/h/h8034.md) of the same [maqowm](../../strongs/h/h4725.md) was [qara'](../../strongs/h/h7121.md), The [ʿēmeq](../../strongs/h/h6010.md) of [Bᵊrāḵâ](../../strongs/h/h1294.md), unto this [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_20_27"></a>2Chronicles 20:27

Then they [shuwb](../../strongs/h/h7725.md), every ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) in the [ro'sh](../../strongs/h/h7218.md) of them, to [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) with [simchah](../../strongs/h/h8057.md); for [Yĕhovah](../../strongs/h/h3068.md) had made them to [samach](../../strongs/h/h8055.md) over their ['oyeb](../../strongs/h/h341.md).

<a name="2chronicles_20_28"></a>2Chronicles 20:28

And they [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) with [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md) and [ḥăṣōṣrâ](../../strongs/h/h2689.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_20_29"></a>2Chronicles 20:29

And the [paḥaḏ](../../strongs/h/h6343.md) of ['Elohiym](../../strongs/h/h430.md) was on all the [mamlāḵâ](../../strongs/h/h4467.md) of those ['erets](../../strongs/h/h776.md), when they had [shama'](../../strongs/h/h8085.md) that [Yĕhovah](../../strongs/h/h3068.md) [lāḥam](../../strongs/h/h3898.md) against the ['oyeb](../../strongs/h/h341.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_20_30"></a>2Chronicles 20:30

So the [malkuwth](../../strongs/h/h4438.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) was [šāqaṭ](../../strongs/h/h8252.md): for his ['Elohiym](../../strongs/h/h430.md) gave him [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_20_31"></a>2Chronicles 20:31

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md): he was thirty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) twenty and five [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [ʿĂzûḇâ](../../strongs/h/h5806.md) the [bath](../../strongs/h/h1323.md) of [Šilḥî](../../strongs/h/h7977.md).

<a name="2chronicles_20_32"></a>2Chronicles 20:32

And he [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of ['Āsā'](../../strongs/h/h609.md) his ['ab](../../strongs/h/h1.md), and [cuwr](../../strongs/h/h5493.md) not from it, ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_20_33"></a>2Chronicles 20:33

Howbeit the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md): for as yet the ['am](../../strongs/h/h5971.md) had not [kuwn](../../strongs/h/h3559.md) their [lebab](../../strongs/h/h3824.md) unto the ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_20_34"></a>2Chronicles 20:34

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [dabar](../../strongs/h/h1697.md) of [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Ḥănānî](../../strongs/h/h2607.md), who is [ʿālâ](../../strongs/h/h5927.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_20_35"></a>2Chronicles 20:35

And ['aḥar](../../strongs/h/h310.md) this did [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ḥāḇar](../../strongs/h/h2266.md) himself with ['Ăḥazyâ](../../strongs/h/h274.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), who ['asah](../../strongs/h/h6213.md) very [rāšaʿ](../../strongs/h/h7561.md):

<a name="2chronicles_20_36"></a>2Chronicles 20:36

And he [ḥāḇar](../../strongs/h/h2266.md) himself with him to ['asah](../../strongs/h/h6213.md) ['ŏnîyâ](../../strongs/h/h591.md) to [yālaḵ](../../strongs/h/h3212.md) to [Taršîš](../../strongs/h/h8659.md): and they ['asah](../../strongs/h/h6213.md) the ['ŏnîyâ](../../strongs/h/h591.md) in [ʿEṣyôn Geḇer](../../strongs/h/h6100.md).

<a name="2chronicles_20_37"></a>2Chronicles 20:37

Then ['Ĕlîʿezer](../../strongs/h/h461.md) the [ben](../../strongs/h/h1121.md) of [Dôḏāvâû](../../strongs/h/h1735.md) of [Mārē'Šâ](../../strongs/h/h4762.md) [nāḇā'](../../strongs/h/h5012.md) against [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), ['āmar](../../strongs/h/h559.md), Because thou hast [ḥāḇar](../../strongs/h/h2266.md) thyself with ['Ăḥazyâ](../../strongs/h/h274.md), [Yĕhovah](../../strongs/h/h3068.md) hath [pāraṣ](../../strongs/h/h6555.md) thy [ma'aseh](../../strongs/h/h4639.md). And the ['ŏnîyâ](../../strongs/h/h591.md) were [shabar](../../strongs/h/h7665.md), that they were not [ʿāṣar](../../strongs/h/h6113.md) to [yālaḵ](../../strongs/h/h3212.md) to [Taršîš](../../strongs/h/h8659.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 19](2chronicles_19.md) - [2Chronicles 21](2chronicles_21.md)