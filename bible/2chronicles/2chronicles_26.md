# [2Chronicles 26](https://www.blueletterbible.org/kjv/2chronicles/26)

<a name="2chronicles_26_1"></a>2Chronicles 26:1

Then all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md) [laqach](../../strongs/h/h3947.md) ['Uzziyah](../../strongs/h/h5818.md), who was sixteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), and made him [mālaḵ](../../strongs/h/h4427.md) in the room of his ['ab](../../strongs/h/h1.md) ['Ămaṣyâ](../../strongs/h/h558.md).

<a name="2chronicles_26_2"></a>2Chronicles 26:2

He [bānâ](../../strongs/h/h1129.md) ['Êlôṯ](../../strongs/h/h359.md), and [shuwb](../../strongs/h/h7725.md) it to [Yehuwdah](../../strongs/h/h3063.md), ['aḥar](../../strongs/h/h310.md) that the [melek](../../strongs/h/h4428.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md).

<a name="2chronicles_26_3"></a>2Chronicles 26:3

Sixteen [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was ['Uzziyah](../../strongs/h/h5818.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) fifty and two [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was [Yᵊḵālyâ](../../strongs/h/h3203.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_26_4"></a>2Chronicles 26:4

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) ['Ămaṣyâ](../../strongs/h/h558.md) ['asah](../../strongs/h/h6213.md).

<a name="2chronicles_26_5"></a>2Chronicles 26:5

And he [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md) in the [yowm](../../strongs/h/h3117.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md), who had [bîn](../../strongs/h/h995.md) in the [ra'ah](../../strongs/h/h7200.md) of ['Elohiym](../../strongs/h/h430.md): and as [yowm](../../strongs/h/h3117.md) he [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md), ['Elohiym](../../strongs/h/h430.md) made him to [tsalach](../../strongs/h/h6743.md).

<a name="2chronicles_26_6"></a>2Chronicles 26:6

And he [yāṣā'](../../strongs/h/h3318.md) and [lāḥam](../../strongs/h/h3898.md) against the [Pᵊlištî](../../strongs/h/h6430.md), and [pāraṣ](../../strongs/h/h6555.md) the [ḥômâ](../../strongs/h/h2346.md) of [Gaṯ](../../strongs/h/h1661.md), and the [ḥômâ](../../strongs/h/h2346.md) of [Yaḇnê](../../strongs/h/h2996.md), and the [ḥômâ](../../strongs/h/h2346.md) of ['Ašdôḏ](../../strongs/h/h795.md), and [bānâ](../../strongs/h/h1129.md) [ʿîr](../../strongs/h/h5892.md) about ['Ašdôḏ](../../strongs/h/h795.md), and among the [Pᵊlištî](../../strongs/h/h6430.md).

<a name="2chronicles_26_7"></a>2Chronicles 26:7

And ['Elohiym](../../strongs/h/h430.md) [ʿāzar](../../strongs/h/h5826.md) him against the [Pᵊlištî](../../strongs/h/h6430.md), and against the [ʿĂrāḇî](../../strongs/h/h6163.md) that [yashab](../../strongs/h/h3427.md) in [Gûr-BaʿAl](../../strongs/h/h1485.md), and the [MᵊʿYny](../../strongs/h/h4586.md).

<a name="2chronicles_26_8"></a>2Chronicles 26:8

And the [ʿAmmôn](../../strongs/h/h5984.md) [nathan](../../strongs/h/h5414.md) [minchah](../../strongs/h/h4503.md) to ['Uzziyah](../../strongs/h/h5818.md): and his [shem](../../strongs/h/h8034.md) [yālaḵ](../../strongs/h/h3212.md) even to the [bow'](../../strongs/h/h935.md) in of [Mitsrayim](../../strongs/h/h4714.md); for he [ḥāzaq](../../strongs/h/h2388.md) himself [maʿal](../../strongs/h/h4605.md).

<a name="2chronicles_26_9"></a>2Chronicles 26:9

Moreover ['Uzziyah](../../strongs/h/h5818.md) [bānâ](../../strongs/h/h1129.md) [miḡdāl](../../strongs/h/h4026.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) at the [pinnâ](../../strongs/h/h6438.md) [sha'ar](../../strongs/h/h8179.md), and at the [gay'](../../strongs/h/h1516.md) [sha'ar](../../strongs/h/h8179.md), and at the [miqṣôaʿ](../../strongs/h/h4740.md) of the wall, and [ḥāzaq](../../strongs/h/h2388.md) them.

<a name="2chronicles_26_10"></a>2Chronicles 26:10

Also he [bānâ](../../strongs/h/h1129.md) [miḡdāl](../../strongs/h/h4026.md) in the [midbar](../../strongs/h/h4057.md), and [ḥāṣaḇ](../../strongs/h/h2672.md) [rab](../../strongs/h/h7227.md) [bowr](../../strongs/h/h953.md): for he had [rab](../../strongs/h/h7227.md) [miqnê](../../strongs/h/h4735.md), both in the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the [mîšôr](../../strongs/h/h4334.md): ['ikār](../../strongs/h/h406.md) also, and [kōrēm](../../strongs/h/h3755.md) in the [har](../../strongs/h/h2022.md), and in [Karmel](../../strongs/h/h3760.md): for he ['ahab](../../strongs/h/h157.md) ['ăḏāmâ](../../strongs/h/h127.md).

<a name="2chronicles_26_11"></a>2Chronicles 26:11

Moreover ['Uzziyah](../../strongs/h/h5818.md) had an [ḥayil](../../strongs/h/h2428.md) of [milḥāmâ](../../strongs/h/h4421.md) ['asah](../../strongs/h/h6213.md), that [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md) by [gᵊḏûḏ](../../strongs/h/h1416.md), according to the [mispār](../../strongs/h/h4557.md) of their [pᵊqudâ](../../strongs/h/h6486.md) by the [yad](../../strongs/h/h3027.md) of [YᵊʿÎ'Ēl](../../strongs/h/h3273.md) the [sāp̄ar](../../strongs/h/h5608.md) and [MaʿĂśêâ](../../strongs/h/h4641.md) the [šāṭar](../../strongs/h/h7860.md), under the [yad](../../strongs/h/h3027.md) of [Ḥănanyâ](../../strongs/h/h2608.md), one of the [melek](../../strongs/h/h4428.md) [śar](../../strongs/h/h8269.md).

<a name="2chronicles_26_12"></a>2Chronicles 26:12

The [mispār](../../strongs/h/h4557.md) of the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of the [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md) were two thousand and six hundred.

<a name="2chronicles_26_13"></a>2Chronicles 26:13

And under their [yad](../../strongs/h/h3027.md) was an [ḥayil](../../strongs/h/h2428.md) [tsaba'](../../strongs/h/h6635.md), three hundred thousand and seven thousand and five hundred, that ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md) with [ḥayil](../../strongs/h/h2428.md) [koach](../../strongs/h/h3581.md), to [ʿāzar](../../strongs/h/h5826.md) the [melek](../../strongs/h/h4428.md) against the ['oyeb](../../strongs/h/h341.md).

<a name="2chronicles_26_14"></a>2Chronicles 26:14

And ['Uzziyah](../../strongs/h/h5818.md) [kuwn](../../strongs/h/h3559.md) for them throughout all the [tsaba'](../../strongs/h/h6635.md) [magen](../../strongs/h/h4043.md), and [rōmaḥ](../../strongs/h/h7420.md), and [kôḇaʿ](../../strongs/h/h3553.md), and [širyôn](../../strongs/h/h8302.md), and [qesheth](../../strongs/h/h7198.md), and [qelaʿ](../../strongs/h/h7050.md) to ['eben](../../strongs/h/h68.md).

<a name="2chronicles_26_15"></a>2Chronicles 26:15

And he ['asah](../../strongs/h/h6213.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) [ḥiššāḇôn](../../strongs/h/h2810.md), [maḥăšāḇâ](../../strongs/h/h4284.md) by [chashab](../../strongs/h/h2803.md), to be on the [miḡdāl](../../strongs/h/h4026.md) and upon the [pinnâ](../../strongs/h/h6438.md), to [yārâ](../../strongs/h/h3384.md) [chets](../../strongs/h/h2671.md) and [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) withal. And his [shem](../../strongs/h/h8034.md) [yāṣā'](../../strongs/h/h3318.md) far [rachowq](../../strongs/h/h7350.md); for he was [pala'](../../strongs/h/h6381.md) [ʿāzar](../../strongs/h/h5826.md), till he was [ḥāzaq](../../strongs/h/h2388.md).

<a name="2chronicles_26_16"></a>2Chronicles 26:16

But when he was [ḥezqâ](../../strongs/h/h2393.md), his [leb](../../strongs/h/h3820.md) was [gāḇah](../../strongs/h/h1361.md) to his [shachath](../../strongs/h/h7843.md): for he [māʿal](../../strongs/h/h4603.md) against [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and [bow'](../../strongs/h/h935.md) into the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md) to [qāṭar](../../strongs/h/h6999.md) upon the [mizbeach](../../strongs/h/h4196.md) of [qᵊṭōreṯ](../../strongs/h/h7004.md).

<a name="2chronicles_26_17"></a>2Chronicles 26:17

And [ʿĂzaryâ](../../strongs/h/h5838.md) the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) him, and with him fourscore [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md), that were [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md):

<a name="2chronicles_26_18"></a>2Chronicles 26:18

And they ['amad](../../strongs/h/h5975.md) ['Uzziyah](../../strongs/h/h5818.md) the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md) unto him, not unto thee, ['Uzziyah](../../strongs/h/h5818.md), to [qāṭar](../../strongs/h/h6999.md) unto [Yĕhovah](../../strongs/h/h3068.md), but to the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), that are [qadash](../../strongs/h/h6942.md) to [qāṭar](../../strongs/h/h6999.md): [yāṣā'](../../strongs/h/h3318.md) of the [miqdash](../../strongs/h/h4720.md); for thou hast [māʿal](../../strongs/h/h4603.md); neither shall it be for thine [kabowd](../../strongs/h/h3519.md) from [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_26_19"></a>2Chronicles 26:19

Then ['Uzziyah](../../strongs/h/h5818.md) was [zāʿap̄](../../strongs/h/h2196.md), and had a [miqṭereṯ](../../strongs/h/h4730.md) in his [yad](../../strongs/h/h3027.md) to [qāṭar](../../strongs/h/h6999.md): and while he was [zāʿap̄](../../strongs/h/h2196.md) with the [kōhēn](../../strongs/h/h3548.md), the [ṣāraʿaṯ](../../strongs/h/h6883.md) even [zāraḥ](../../strongs/h/h2224.md) in his [mēṣaḥ](../../strongs/h/h4696.md) [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), from beside the [qᵊṭōreṯ](../../strongs/h/h7004.md) [mizbeach](../../strongs/h/h4196.md).

<a name="2chronicles_26_20"></a>2Chronicles 26:20

And [ʿĂzaryâ](../../strongs/h/h5838.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md), and all the [kōhēn](../../strongs/h/h3548.md), [panah](../../strongs/h/h6437.md) upon him, and, behold, he was [ṣāraʿ](../../strongs/h/h6879.md) in his [mēṣaḥ](../../strongs/h/h4696.md), and they [bahal](../../strongs/h/h926.md) him from thence; yea, himself [dāḥap̄](../../strongs/h/h1765.md) also to [yāṣā'](../../strongs/h/h3318.md), because [Yĕhovah](../../strongs/h/h3068.md) had [naga'](../../strongs/h/h5060.md) him.

<a name="2chronicles_26_21"></a>2Chronicles 26:21

And ['Uzziyah](../../strongs/h/h5818.md) the [melek](../../strongs/h/h4428.md) was a [ṣāraʿ](../../strongs/h/h6879.md) unto the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md), and [yashab](../../strongs/h/h3427.md) a [ḥāp̄šûṯ](../../strongs/h/h2669.md) [ḥāp̄šûṯ](../../strongs/h/h2669.md) [bayith](../../strongs/h/h1004.md), being a [ṣāraʿ](../../strongs/h/h6879.md); for he was cut [gāzar](../../strongs/h/h1504.md) from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and [Yôṯām](../../strongs/h/h3147.md) his [ben](../../strongs/h/h1121.md) was over the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), [shaphat](../../strongs/h/h8199.md) the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="2chronicles_26_22"></a>2Chronicles 26:22

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Uzziyah](../../strongs/h/h5818.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), did [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md), the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md), [kāṯaḇ](../../strongs/h/h3789.md).

<a name="2chronicles_26_23"></a>2Chronicles 26:23

So ['Uzziyah](../../strongs/h/h5818.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him with his ['ab](../../strongs/h/h1.md) in the [sadeh](../../strongs/h/h7704.md) of the [qᵊḇûrâ](../../strongs/h/h6900.md) which belonged to the [melek](../../strongs/h/h4428.md); for they ['āmar](../../strongs/h/h559.md), He is a [ṣāraʿ](../../strongs/h/h6879.md): and [Yôṯām](../../strongs/h/h3147.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 25](2chronicles_25.md) - [2Chronicles 27](2chronicles_27.md)