# [2Chronicles 8](https://www.blueletterbible.org/kjv/2chronicles/8)

<a name="2chronicles_8_1"></a>2Chronicles 8:1

And it came to pass at the [qēṣ](../../strongs/h/h7093.md) of twenty [šānâ](../../strongs/h/h8141.md), wherein [Šᵊlōmô](../../strongs/h/h8010.md) had [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and his own [bayith](../../strongs/h/h1004.md),

<a name="2chronicles_8_2"></a>2Chronicles 8:2

That the [ʿîr](../../strongs/h/h5892.md) which [Ḥûrām](../../strongs/h/h2361.md) had [nathan](../../strongs/h/h5414.md) to [Šᵊlōmô](../../strongs/h/h8010.md), [Šᵊlōmô](../../strongs/h/h8010.md) [bānâ](../../strongs/h/h1129.md) them, and caused the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) to [yashab](../../strongs/h/h3427.md) there.

<a name="2chronicles_8_3"></a>2Chronicles 8:3

And [Šᵊlōmô](../../strongs/h/h8010.md) [yālaḵ](../../strongs/h/h3212.md) to [Ḥămāṯ Ṣôḇâ](../../strongs/h/h2578.md), and [ḥāzaq](../../strongs/h/h2388.md) against it.

<a name="2chronicles_8_4"></a>2Chronicles 8:4

And he [bānâ](../../strongs/h/h1129.md) [Taḏmōr](../../strongs/h/h8412.md) in the [midbar](../../strongs/h/h4057.md), and all the [miskᵊnôṯ](../../strongs/h/h4543.md) [ʿîr](../../strongs/h/h5892.md), which he [bānâ](../../strongs/h/h1129.md) in [Ḥămāṯ](../../strongs/h/h2574.md).

<a name="2chronicles_8_5"></a>2Chronicles 8:5

Also he [bānâ](../../strongs/h/h1129.md) [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the ['elyown](../../strongs/h/h5945.md), and [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md) the [taḥtôn](../../strongs/h/h8481.md), [māṣôr](../../strongs/h/h4692.md) [ʿîr](../../strongs/h/h5892.md), with [ḥômâ](../../strongs/h/h2346.md), [deleṯ](../../strongs/h/h1817.md), and [bᵊrîaḥ](../../strongs/h/h1280.md);

<a name="2chronicles_8_6"></a>2Chronicles 8:6

And [BaʿĂlāṯ](../../strongs/h/h1191.md), and all the [miskᵊnôṯ](../../strongs/h/h4543.md) [ʿîr](../../strongs/h/h5892.md) that [Šᵊlōmô](../../strongs/h/h8010.md) had, and all the [reḵeḇ](../../strongs/h/h7393.md) [ʿîr](../../strongs/h/h5892.md), and the [ʿîr](../../strongs/h/h5892.md) of the [pārāš](../../strongs/h/h6571.md), and all that [Šᵊlōmô](../../strongs/h/h8010.md) [ḥāšaq](../../strongs/h/h2836.md) [ḥēšeq](../../strongs/h/h2837.md) to [bānâ](../../strongs/h/h1129.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and in [Lᵊḇānôn](../../strongs/h/h3844.md), and throughout all the ['erets](../../strongs/h/h776.md) of his [memshalah](../../strongs/h/h4475.md).

<a name="2chronicles_8_7"></a>2Chronicles 8:7

As for all the ['am](../../strongs/h/h5971.md) that were [yāṯar](../../strongs/h/h3498.md) of the [Ḥitî](../../strongs/h/h2850.md), and the ['Ĕmōrî](../../strongs/h/h567.md), and the [Pᵊrizzî](../../strongs/h/h6522.md), and the [Ḥiûî](../../strongs/h/h2340.md), and the [Yᵊḇûsî](../../strongs/h/h2983.md), which were not of [Yisra'el](../../strongs/h/h3478.md),

<a name="2chronicles_8_8"></a>2Chronicles 8:8

But of their [ben](../../strongs/h/h1121.md), who were [yāṯar](../../strongs/h/h3498.md) ['aḥar](../../strongs/h/h310.md) them in the ['erets](../../strongs/h/h776.md), whom the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [kalah](../../strongs/h/h3615.md) not, them did [Šᵊlōmô](../../strongs/h/h8010.md) make to [ʿālâ](../../strongs/h/h5927.md) [mas](../../strongs/h/h4522.md) until this [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_8_9"></a>2Chronicles 8:9

But of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) did [Šᵊlōmô](../../strongs/h/h8010.md) [nathan](../../strongs/h/h5414.md) no ['ebed](../../strongs/h/h5650.md) for his [mĕla'kah](../../strongs/h/h4399.md); but they were ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and [śar](../../strongs/h/h8269.md) of his [šālîš](../../strongs/h/h7991.md), and [śar](../../strongs/h/h8269.md) of his [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md).

<a name="2chronicles_8_10"></a>2Chronicles 8:10

And these were the [śar](../../strongs/h/h8269.md) of [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [nāṣaḇ](../../strongs/h/h5324.md) [nᵊṣîḇ](../../strongs/h/h5333.md), even two hundred and fifty, that [radah](../../strongs/h/h7287.md) over the ['am](../../strongs/h/h5971.md).

<a name="2chronicles_8_11"></a>2Chronicles 8:11

And [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) the [bath](../../strongs/h/h1323.md) of [Parʿô](../../strongs/h/h6547.md) out of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) unto the [bayith](../../strongs/h/h1004.md) that he had [bānâ](../../strongs/h/h1129.md) for her: for he ['āmar](../../strongs/h/h559.md), My ['ishshah](../../strongs/h/h802.md) shall not [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), because the places are [qodesh](../../strongs/h/h6944.md), whereunto the ['ārôn](../../strongs/h/h727.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [bow'](../../strongs/h/h935.md).

<a name="2chronicles_8_12"></a>2Chronicles 8:12

Then [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md) on the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), which he had [bānâ](../../strongs/h/h1129.md) [paniym](../../strongs/h/h6440.md) the ['ûlām](../../strongs/h/h197.md),

<a name="2chronicles_8_13"></a>2Chronicles 8:13

Even after a certain [dabar](../../strongs/h/h1697.md) every [yowm](../../strongs/h/h3117.md), [ʿālâ](../../strongs/h/h5927.md) according to the [mitsvah](../../strongs/h/h4687.md) of [Mōshe](../../strongs/h/h4872.md), on the [shabbath](../../strongs/h/h7676.md), and on the [ḥōḏeš](../../strongs/h/h2320.md), and on the [môʿēḏ](../../strongs/h/h4150.md), three [pa'am](../../strongs/h/h6471.md) in the [šānâ](../../strongs/h/h8141.md), even in the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md), and in the [ḥāḡ](../../strongs/h/h2282.md) of [šāḇûaʿ](../../strongs/h/h7620.md), and in the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md).

<a name="2chronicles_8_14"></a>2Chronicles 8:14

And he ['amad](../../strongs/h/h5975.md), according to the [mishpat](../../strongs/h/h4941.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md), the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [kōhēn](../../strongs/h/h3548.md) to their [ʿăḇōḏâ](../../strongs/h/h5656.md), and the [Lᵊvî](../../strongs/h/h3881.md) to their [mišmereṯ](../../strongs/h/h4931.md), to [halal](../../strongs/h/h1984.md) and [sharath](../../strongs/h/h8334.md) before the [kōhēn](../../strongs/h/h3548.md), as the [dabar](../../strongs/h/h1697.md) of every [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md): the [šôʿēr](../../strongs/h/h7778.md) also by their [maḥălōqeṯ](../../strongs/h/h4256.md) at every [sha'ar](../../strongs/h/h8179.md): for so had [Dāviḏ](../../strongs/h/h1732.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) [mitsvah](../../strongs/h/h4687.md).

<a name="2chronicles_8_15"></a>2Chronicles 8:15

And they [cuwr](../../strongs/h/h5493.md) not from the [mitsvah](../../strongs/h/h4687.md) of the [melek](../../strongs/h/h4428.md) unto the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md) concerning any [dabar](../../strongs/h/h1697.md), or concerning the ['ôṣār](../../strongs/h/h214.md).

<a name="2chronicles_8_16"></a>2Chronicles 8:16

Now all the [mĕla'kah](../../strongs/h/h4399.md) of [Šᵊlōmô](../../strongs/h/h8010.md) was [kuwn](../../strongs/h/h3559.md) unto the [yowm](../../strongs/h/h3117.md) of the [mûsāḏ](../../strongs/h/h4143.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and until it was [kalah](../../strongs/h/h3615.md). So the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) was [šālēm](../../strongs/h/h8003.md).

<a name="2chronicles_8_17"></a>2Chronicles 8:17

Then [halak](../../strongs/h/h1980.md) [Šᵊlōmô](../../strongs/h/h8010.md) to [ʿEṣyôn Geḇer](../../strongs/h/h6100.md), and to ['Êlôṯ](../../strongs/h/h359.md), at the [yam](../../strongs/h/h3220.md) [saphah](../../strongs/h/h8193.md) in the ['erets](../../strongs/h/h776.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="2chronicles_8_18"></a>2Chronicles 8:18

And [Ḥûrām](../../strongs/h/h2361.md) [shalach](../../strongs/h/h7971.md) him by the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md) ['ŏnîyâ](../../strongs/h/h591.md) ['ŏnîyâ](../../strongs/h/h591.md), and ['ebed](../../strongs/h/h5650.md) that had [yada'](../../strongs/h/h3045.md) of the [yam](../../strongs/h/h3220.md); and they [bow'](../../strongs/h/h935.md) with the ['ebed](../../strongs/h/h5650.md) of [Šᵊlōmô](../../strongs/h/h8010.md) to ['Ôp̄îr](../../strongs/h/h211.md), and [laqach](../../strongs/h/h3947.md) thence four hundred and fifty [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md), and [bow'](../../strongs/h/h935.md) them to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 7](2chronicles_7.md) - [2Chronicles 9](2chronicles_9.md)