# [2Chronicles 16](https://www.blueletterbible.org/kjv/2chronicles/16)

<a name="2chronicles_16_1"></a>2Chronicles 16:1

In the six and thirtieth [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of ['Āsā'](../../strongs/h/h609.md) [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md) against [Yehuwdah](../../strongs/h/h3063.md), and [bānâ](../../strongs/h/h1129.md) [rāmâ](../../strongs/h/h7414.md), to the intent that he might [nathan](../../strongs/h/h5414.md) none [yāṣā'](../../strongs/h/h3318.md) or [bow'](../../strongs/h/h935.md) to ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_16_2"></a>2Chronicles 16:2

Then ['Āsā'](../../strongs/h/h609.md) [yāṣā'](../../strongs/h/h3318.md) [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md) out of the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) and of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [shalach](../../strongs/h/h7971.md) to [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), that [yashab](../../strongs/h/h3427.md) at [Dammeśeq](../../strongs/h/h1834.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_16_3"></a>2Chronicles 16:3

There is a [bĕriyth](../../strongs/h/h1285.md) between me and thee, as there was between my ['ab](../../strongs/h/h1.md) and thy ['ab](../../strongs/h/h1.md): behold, I have [shalach](../../strongs/h/h7971.md) thee [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md); [yālaḵ](../../strongs/h/h3212.md), [pārar](../../strongs/h/h6565.md) thy [bĕriyth](../../strongs/h/h1285.md) with [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that he may [ʿālâ](../../strongs/h/h5927.md) from me.

<a name="2chronicles_16_4"></a>2Chronicles 16:4

And [Ben-Hăḏaḏ](../../strongs/h/h1130.md) [shama'](../../strongs/h/h8085.md) unto [melek](../../strongs/h/h4428.md) ['Āsā'](../../strongs/h/h609.md), and [shalach](../../strongs/h/h7971.md) the [śar](../../strongs/h/h8269.md) of his [ḥayil](../../strongs/h/h2428.md) against the [ʿîr](../../strongs/h/h5892.md) of [Yisra'el](../../strongs/h/h3478.md); and they [nakah](../../strongs/h/h5221.md) [ʿÎyôn](../../strongs/h/h5859.md), and [Dān](../../strongs/h/h1835.md), and ['Āḇēl Mayim](../../strongs/h/h66.md), and all the [miskᵊnôṯ](../../strongs/h/h4543.md) [ʿîr](../../strongs/h/h5892.md) of [Nap̄tālî](../../strongs/h/h5321.md).

<a name="2chronicles_16_5"></a>2Chronicles 16:5

And it came to pass, when [BaʿŠā'](../../strongs/h/h1201.md) [shama'](../../strongs/h/h8085.md) it, that he [ḥāḏal](../../strongs/h/h2308.md) [bānâ](../../strongs/h/h1129.md) of [rāmâ](../../strongs/h/h7414.md), and let his [mĕla'kah](../../strongs/h/h4399.md) [shabath](../../strongs/h/h7673.md).

<a name="2chronicles_16_6"></a>2Chronicles 16:6

Then ['Āsā'](../../strongs/h/h609.md) the [melek](../../strongs/h/h4428.md) [laqach](../../strongs/h/h3947.md) all [Yehuwdah](../../strongs/h/h3063.md); and they carried [nasa'](../../strongs/h/h5375.md) the ['eben](../../strongs/h/h68.md) of [rāmâ](../../strongs/h/h7414.md), and the ['ets](../../strongs/h/h6086.md) thereof, wherewith [BaʿŠā'](../../strongs/h/h1201.md) was [bānâ](../../strongs/h/h1129.md); and he [bānâ](../../strongs/h/h1129.md) therewith [Geḇaʿ](../../strongs/h/h1387.md) and [Miṣpâ](../../strongs/h/h4709.md).

<a name="2chronicles_16_7"></a>2Chronicles 16:7

And at that [ʿēṯ](../../strongs/h/h6256.md) [Ḥănānî](../../strongs/h/h2607.md) the [ra'ah](../../strongs/h/h7200.md) [bow'](../../strongs/h/h935.md) to ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md) unto him, Because thou hast [šāʿan](../../strongs/h/h8172.md) on the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), and not [šāʿan](../../strongs/h/h8172.md) on [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), therefore is the [ḥayil](../../strongs/h/h2428.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [mālaṭ](../../strongs/h/h4422.md) out of thine [yad](../../strongs/h/h3027.md).

<a name="2chronicles_16_8"></a>2Chronicles 16:8

Were not the [Kûšî](../../strongs/h/h3569.md) and the [Luḇî](../../strongs/h/h3864.md) a [rōḇ](../../strongs/h/h7230.md) [ḥayil](../../strongs/h/h2428.md), with [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md)? yet, because thou didst [šāʿan](../../strongs/h/h8172.md) on [Yĕhovah](../../strongs/h/h3068.md), he [nathan](../../strongs/h/h5414.md) them into thine [yad](../../strongs/h/h3027.md).

<a name="2chronicles_16_9"></a>2Chronicles 16:9

For the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) [šûṭ](../../strongs/h/h7751.md) throughout the ['erets](../../strongs/h/h776.md), to shew himself [ḥāzaq](../../strongs/h/h2388.md) in the behalf of them whose [lebab](../../strongs/h/h3824.md) is [šālēm](../../strongs/h/h8003.md) toward him. Herein thou hast done [sāḵal](../../strongs/h/h5528.md): therefore from henceforth thou shalt have [milḥāmâ](../../strongs/h/h4421.md).

<a name="2chronicles_16_10"></a>2Chronicles 16:10

Then ['Āsā'](../../strongs/h/h609.md) was [kāʿas](../../strongs/h/h3707.md) with the [ra'ah](../../strongs/h/h7200.md), and [nathan](../../strongs/h/h5414.md) him in a [mahpeḵeṯ](../../strongs/h/h4115.md) [bayith](../../strongs/h/h1004.md); for he was in a [zaʿap̄](../../strongs/h/h2197.md) with him because of this thing. And ['Āsā'](../../strongs/h/h609.md) [rāṣaṣ](../../strongs/h/h7533.md) some of the ['am](../../strongs/h/h5971.md) the same [ʿēṯ](../../strongs/h/h6256.md).

<a name="2chronicles_16_11"></a>2Chronicles 16:11

And, behold, the [dabar](../../strongs/h/h1697.md) of ['Āsā'](../../strongs/h/h609.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), lo, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_16_12"></a>2Chronicles 16:12

And ['Āsā'](../../strongs/h/h609.md) in the thirty and ninth [šānâ](../../strongs/h/h8141.md) of his [malkuwth](../../strongs/h/h4438.md) was [ḥālā'](../../strongs/h/h2456.md) in his [regel](../../strongs/h/h7272.md), until his [ḥŏlî](../../strongs/h/h2483.md) was [maʿal](../../strongs/h/h4605.md) great: yet in his [ḥŏlî](../../strongs/h/h2483.md) he [darash](../../strongs/h/h1875.md) not to [Yĕhovah](../../strongs/h/h3068.md), but to the [rapha'](../../strongs/h/h7495.md).

<a name="2chronicles_16_13"></a>2Chronicles 16:13

And ['Āsā'](../../strongs/h/h609.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and [muwth](../../strongs/h/h4191.md) in the one and fortieth [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md).

<a name="2chronicles_16_14"></a>2Chronicles 16:14

And they [qāḇar](../../strongs/h/h6912.md) him in his own [qeber](../../strongs/h/h6913.md), which he had [karah](../../strongs/h/h3738.md) for himself in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), and [shakab](../../strongs/h/h7901.md) him in the [miškāḇ](../../strongs/h/h4904.md) which was [mālā'](../../strongs/h/h4390.md) with [beśem](../../strongs/h/h1314.md) and [zan](../../strongs/h/h2177.md) of [rāqaḥ](../../strongs/h/h7543.md) by the [mirqaḥaṯ](../../strongs/h/h4842.md) [ma'aseh](../../strongs/h/h4639.md): and they [śārap̄](../../strongs/h/h8313.md) a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [śᵊrēp̄â](../../strongs/h/h8316.md) for him.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 15](2chronicles_15.md) - [2Chronicles 17](2chronicles_17.md)