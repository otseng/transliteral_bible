# [2Chronicles 7](https://www.blueletterbible.org/kjv/2chronicles/7)

<a name="2chronicles_7_1"></a>2Chronicles 7:1

Now when [Šᵊlōmô](../../strongs/h/h8010.md) had made a [kalah](../../strongs/h/h3615.md) of [palal](../../strongs/h/h6419.md), the ['esh](../../strongs/h/h784.md) [yarad](../../strongs/h/h3381.md) from [shamayim](../../strongs/h/h8064.md), and ['akal](../../strongs/h/h398.md) the [ʿōlâ](../../strongs/h/h5930.md) and the [zebach](../../strongs/h/h2077.md); and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_7_2"></a>2Chronicles 7:2

And the [kōhēn](../../strongs/h/h3548.md) [yakol](../../strongs/h/h3201.md) not [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), because the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) had [mālā'](../../strongs/h/h4390.md) [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_7_3"></a>2Chronicles 7:3

And when all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) how the ['esh](../../strongs/h/h784.md) [yarad](../../strongs/h/h3381.md), and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) upon the [bayith](../../strongs/h/h1004.md), they [kara'](../../strongs/h/h3766.md) themselves with their ['aph](../../strongs/h/h639.md) to the ['erets](../../strongs/h/h776.md) upon the [ritspah](../../strongs/h/h7531.md), and [shachah](../../strongs/h/h7812.md), and [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md), saying, For he is [towb](../../strongs/h/h2896.md); for his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md).

<a name="2chronicles_7_4"></a>2Chronicles 7:4

Then the [melek](../../strongs/h/h4428.md) and all the ['am](../../strongs/h/h5971.md) [zabach](../../strongs/h/h2076.md) [zebach](../../strongs/h/h2077.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_7_5"></a>2Chronicles 7:5

And [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md) of twenty and two thousand [bāqār](../../strongs/h/h1241.md), and an hundred and twenty thousand [tso'n](../../strongs/h/h6629.md): so the [melek](../../strongs/h/h4428.md) and all the ['am](../../strongs/h/h5971.md) [ḥānaḵ](../../strongs/h/h2596.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_7_6"></a>2Chronicles 7:6

And the [kōhēn](../../strongs/h/h3548.md) ['amad](../../strongs/h/h5975.md) on their [mišmereṯ](../../strongs/h/h4931.md): the [Lᵊvî](../../strongs/h/h3881.md) also with [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md) of [Yĕhovah](../../strongs/h/h3068.md), which [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) had ['asah](../../strongs/h/h6213.md) to [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md), because his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md), when [Dāviḏ](../../strongs/h/h1732.md) [halal](../../strongs/h/h1984.md) by their [yad](../../strongs/h/h3027.md); and the [kōhēn](../../strongs/h/h3548.md) [ḥāṣar](../../strongs/h/h2690.md) [ḥāṣar](../../strongs/h/h2690.md) before them, and all [Yisra'el](../../strongs/h/h3478.md) ['amad](../../strongs/h/h5975.md).

<a name="2chronicles_7_7"></a>2Chronicles 7:7

Moreover [Šᵊlōmô](../../strongs/h/h8010.md) [qadash](../../strongs/h/h6942.md) the [tavek](../../strongs/h/h8432.md) of the [ḥāṣēr](../../strongs/h/h2691.md) that was [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): for there he ['asah](../../strongs/h/h6213.md) [ʿōlâ](../../strongs/h/h5930.md), and the [cheleb](../../strongs/h/h2459.md) of the [šelem](../../strongs/h/h8002.md), because the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md) which [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md) was not [yakol](../../strongs/h/h3201.md) to [kûl](../../strongs/h/h3557.md) the [ʿōlâ](../../strongs/h/h5930.md), and the [minchah](../../strongs/h/h4503.md), and the [cheleb](../../strongs/h/h2459.md).

<a name="2chronicles_7_8"></a>2Chronicles 7:8

Also at the same [ʿēṯ](../../strongs/h/h6256.md) [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) seven [yowm](../../strongs/h/h3117.md), and all [Yisra'el](../../strongs/h/h3478.md) with him, a [me'od](../../strongs/h/h3966.md) [gadowl](../../strongs/h/h1419.md) [qāhēl](../../strongs/h/h6951.md), from the [bow'](../../strongs/h/h935.md) of [Ḥămāṯ](../../strongs/h/h2574.md) unto the [nachal](../../strongs/h/h5158.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="2chronicles_7_9"></a>2Chronicles 7:9

And in the eighth [yowm](../../strongs/h/h3117.md) they ['asah](../../strongs/h/h6213.md) a [ʿăṣārâ](../../strongs/h/h6116.md): for they ['asah](../../strongs/h/h6213.md) the [ḥănukâ](../../strongs/h/h2598.md) of the [mizbeach](../../strongs/h/h4196.md) seven [yowm](../../strongs/h/h3117.md), and the [ḥāḡ](../../strongs/h/h2282.md) seven [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_7_10"></a>2Chronicles 7:10

And on the three and twentieth [yowm](../../strongs/h/h3117.md) of the seventh [ḥōḏeš](../../strongs/h/h2320.md) he [shalach](../../strongs/h/h7971.md) the ['am](../../strongs/h/h5971.md) [shalach](../../strongs/h/h7971.md) into their ['ohel](../../strongs/h/h168.md), [śāmēaḥ](../../strongs/h/h8056.md) and [towb](../../strongs/h/h2896.md) in [leb](../../strongs/h/h3820.md) for the [towb](../../strongs/h/h2896.md) that [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) unto [Dāviḏ](../../strongs/h/h1732.md), and to [Šᵊlōmô](../../strongs/h/h8010.md), and to [Yisra'el](../../strongs/h/h3478.md) his ['am](../../strongs/h/h5971.md).

<a name="2chronicles_7_11"></a>2Chronicles 7:11

Thus [Šᵊlōmô](../../strongs/h/h8010.md) [kalah](../../strongs/h/h3615.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md): and all that [bow'](../../strongs/h/h935.md) into [Šᵊlōmô](../../strongs/h/h8010.md) [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in his own [bayith](../../strongs/h/h1004.md), he [tsalach](../../strongs/h/h6743.md).

<a name="2chronicles_7_12"></a>2Chronicles 7:12

And [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) to [Šᵊlōmô](../../strongs/h/h8010.md) by [layil](../../strongs/h/h3915.md), and ['āmar](../../strongs/h/h559.md) unto him, I have [shama'](../../strongs/h/h8085.md) thy [tĕphillah](../../strongs/h/h8605.md), and have [bāḥar](../../strongs/h/h977.md) this [maqowm](../../strongs/h/h4725.md) to myself for a [bayith](../../strongs/h/h1004.md) of [zebach](../../strongs/h/h2077.md).

<a name="2chronicles_7_13"></a>2Chronicles 7:13

[hen](../../strongs/h/h2005.md) I [ʿāṣar](../../strongs/h/h6113.md) [shamayim](../../strongs/h/h8064.md) that there be no [māṭār](../../strongs/h/h4306.md), or if I [tsavah](../../strongs/h/h6680.md) the [ḥāḡāḇ](../../strongs/h/h2284.md) to ['akal](../../strongs/h/h398.md) the ['erets](../../strongs/h/h776.md), or if I [shalach](../../strongs/h/h7971.md) [deḇer](../../strongs/h/h1698.md) among my ['am](../../strongs/h/h5971.md);

<a name="2chronicles_7_14"></a>2Chronicles 7:14

If my ['am](../../strongs/h/h5971.md), which are [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), shall [kānaʿ](../../strongs/h/h3665.md) themselves, and [palal](../../strongs/h/h6419.md), and [bāqaš](../../strongs/h/h1245.md) my [paniym](../../strongs/h/h6440.md), and [shuwb](../../strongs/h/h7725.md) from their [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md); then will I [shama'](../../strongs/h/h8085.md) from [shamayim](../../strongs/h/h8064.md), and will [sālaḥ](../../strongs/h/h5545.md) their [chatta'ath](../../strongs/h/h2403.md), and will [rapha'](../../strongs/h/h7495.md) their ['erets](../../strongs/h/h776.md).

<a name="2chronicles_7_15"></a>2Chronicles 7:15

Now mine ['ayin](../../strongs/h/h5869.md) shall be [pāṯaḥ](../../strongs/h/h6605.md), and mine ['ozen](../../strongs/h/h241.md) [qaššāḇ](../../strongs/h/h7183.md) unto the [tĕphillah](../../strongs/h/h8605.md) that is made in this [maqowm](../../strongs/h/h4725.md).

<a name="2chronicles_7_16"></a>2Chronicles 7:16

For now have I [bāḥar](../../strongs/h/h977.md) and [qadash](../../strongs/h/h6942.md) this [bayith](../../strongs/h/h1004.md), that my [shem](../../strongs/h/h8034.md) may be there ['owlam](../../strongs/h/h5769.md): and mine ['ayin](../../strongs/h/h5869.md) and mine [leb](../../strongs/h/h3820.md) shall be there [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_7_17"></a>2Chronicles 7:17

And as for thee, if thou wilt [yālaḵ](../../strongs/h/h3212.md) [paniym](../../strongs/h/h6440.md) me, as [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md) [halak](../../strongs/h/h1980.md), and ['asah](../../strongs/h/h6213.md) according to all that I have [tsavah](../../strongs/h/h6680.md) thee, and shalt [shamar](../../strongs/h/h8104.md) my [choq](../../strongs/h/h2706.md) and my [mishpat](../../strongs/h/h4941.md);

<a name="2chronicles_7_18"></a>2Chronicles 7:18

Then will I [quwm](../../strongs/h/h6965.md) the [kicce'](../../strongs/h/h3678.md) of thy [malkuwth](../../strongs/h/h4438.md), according as I have [karath](../../strongs/h/h3772.md) with [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), There shall not [karath](../../strongs/h/h3772.md) thee an ['iysh](../../strongs/h/h376.md) to be [mashal](../../strongs/h/h4910.md) in [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_7_19"></a>2Chronicles 7:19

But if ye [shuwb](../../strongs/h/h7725.md), and ['azab](../../strongs/h/h5800.md) my [chuqqah](../../strongs/h/h2708.md) and my [mitsvah](../../strongs/h/h4687.md), which I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you, and shall [halak](../../strongs/h/h1980.md) and ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them;

<a name="2chronicles_7_20"></a>2Chronicles 7:20

Then will I [nathash](../../strongs/h/h5428.md) them out of my ['ăḏāmâ](../../strongs/h/h127.md) which I have [nathan](../../strongs/h/h5414.md) them; and this [bayith](../../strongs/h/h1004.md), which I have [qadash](../../strongs/h/h6942.md) for my [shem](../../strongs/h/h8034.md), will I [shalak](../../strongs/h/h7993.md) of my [paniym](../../strongs/h/h6440.md), and will [nathan](../../strongs/h/h5414.md) it to be a [māšāl](../../strongs/h/h4912.md) and a [šᵊnînâ](../../strongs/h/h8148.md) among all ['am](../../strongs/h/h5971.md).

<a name="2chronicles_7_21"></a>2Chronicles 7:21

And this [bayith](../../strongs/h/h1004.md), which is ['elyown](../../strongs/h/h5945.md), shall be a [šāmēm](../../strongs/h/h8074.md) to every one that ['abar](../../strongs/h/h5674.md) by it; so that he shall ['āmar](../../strongs/h/h559.md), Why hath [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) thus unto this ['erets](../../strongs/h/h776.md), and unto this [bayith](../../strongs/h/h1004.md)?

<a name="2chronicles_7_22"></a>2Chronicles 7:22

And it shall be ['āmar](../../strongs/h/h559.md), Because they ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), which [yāṣā'](../../strongs/h/h3318.md) them out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and [ḥāzaq](../../strongs/h/h2388.md) on ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) them, and ['abad](../../strongs/h/h5647.md) them: therefore hath he [bow'](../../strongs/h/h935.md) all this [ra'](../../strongs/h/h7451.md) upon them.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 6](2chronicles_6.md) - [2Chronicles 8](2chronicles_8.md)