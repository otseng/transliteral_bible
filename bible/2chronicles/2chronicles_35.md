# [2Chronicles 35](https://www.blueletterbible.org/kjv/2chronicles/35)

<a name="2chronicles_35_1"></a>2Chronicles 35:1

Moreover [Yō'Šîyâ](../../strongs/h/h2977.md) ['asah](../../strongs/h/h6213.md) a [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): and they [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md) on the fourteenth day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2chronicles_35_2"></a>2Chronicles 35:2

And he ['amad](../../strongs/h/h5975.md) the [kōhēn](../../strongs/h/h3548.md) in their [mišmereṯ](../../strongs/h/h4931.md), and [ḥāzaq](../../strongs/h/h2388.md) them to the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="2chronicles_35_3"></a>2Chronicles 35:3

And ['āmar](../../strongs/h/h559.md) unto the [Lᵊvî](../../strongs/h/h3881.md) that [bîn](../../strongs/h/h995.md) [mᵊḇônîm](../../strongs/h/h4000.md) all [Yisra'el](../../strongs/h/h3478.md), which were [qadowsh](../../strongs/h/h6918.md) unto [Yĕhovah](../../strongs/h/h3068.md), [nathan](../../strongs/h/h5414.md) the [qodesh](../../strongs/h/h6944.md) ['ārôn](../../strongs/h/h727.md) in the [bayith](../../strongs/h/h1004.md) which [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) did [bānâ](../../strongs/h/h1129.md); it shall not be a [maśśā'](../../strongs/h/h4853.md) upon your [kāṯēp̄](../../strongs/h/h3802.md): ['abad](../../strongs/h/h5647.md) now [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md),

<a name="2chronicles_35_4"></a>2Chronicles 35:4

And [kuwn](../../strongs/h/h3559.md) [kuwn](../../strongs/h/h3559.md) yourselves by the [bayith](../../strongs/h/h1004.md) of your ['ab](../../strongs/h/h1.md), after your [maḥălōqeṯ](../../strongs/h/h4256.md), according to the [kᵊṯāḇ](../../strongs/h/h3791.md) of [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and according to the [miḵtāḇ](../../strongs/h/h4385.md) of [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md).

<a name="2chronicles_35_5"></a>2Chronicles 35:5

And ['amad](../../strongs/h/h5975.md) the [qodesh](../../strongs/h/h6944.md) according to the [pᵊlugâ](../../strongs/h/h6391.md) of the [bayith](../../strongs/h/h1004.md) of the ['ab](../../strongs/h/h1.md) of your ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) ['am](../../strongs/h/h5971.md), and after the [ḥăluqqâ](../../strongs/h/h2515.md) of the ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) of the [Lᵊvî](../../strongs/h/h3881.md).

<a name="2chronicles_35_6"></a>2Chronicles 35:6

So [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md), and [qadash](../../strongs/h/h6942.md) yourselves, and [kuwn](../../strongs/h/h3559.md) your ['ach](../../strongs/h/h251.md), that they may ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="2chronicles_35_7"></a>2Chronicles 35:7

And [Yō'Šîyâ](../../strongs/h/h2977.md) [ruwm](../../strongs/h/h7311.md) to the [ben](../../strongs/h/h1121.md) ['am](../../strongs/h/h5971.md), of the [tso'n](../../strongs/h/h6629.md), [keḇeś](../../strongs/h/h3532.md) and [ben](../../strongs/h/h1121.md) [ʿēz](../../strongs/h/h5795.md), all for the [pecach](../../strongs/h/h6453.md), for all that were [māṣā'](../../strongs/h/h4672.md), to the [mispār](../../strongs/h/h4557.md) of thirty thousand, and three thousand [bāqār](../../strongs/h/h1241.md): these were of the [melek](../../strongs/h/h4428.md) [rᵊḵûš](../../strongs/h/h7399.md).

<a name="2chronicles_35_8"></a>2Chronicles 35:8

And his [śar](../../strongs/h/h8269.md) [ruwm](../../strongs/h/h7311.md) [nᵊḏāḇâ](../../strongs/h/h5071.md) unto the ['am](../../strongs/h/h5971.md), to the [kōhēn](../../strongs/h/h3548.md), and to the [Lᵊvî](../../strongs/h/h3881.md): [Ḥilqîyâ](../../strongs/h/h2518.md) and [Zᵊḵaryâ](../../strongs/h/h2148.md) and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), [nathan](../../strongs/h/h5414.md) unto the [kōhēn](../../strongs/h/h3548.md) for [pecach](../../strongs/h/h6453.md) two thousand and six hundred, and three hundred [bāqār](../../strongs/h/h1241.md).

<a name="2chronicles_35_9"></a>2Chronicles 35:9

[Kônanyâû](../../strongs/h/h3562.md) also, and [ŠᵊmaʿYâ](../../strongs/h/h8098.md) and [Nᵊṯan'ēl](../../strongs/h/h5417.md), his ['ach](../../strongs/h/h251.md), and [Ḥăšaḇyâ](../../strongs/h/h2811.md) and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md) and [Yôzāḇāḏ](../../strongs/h/h3107.md), [śar](../../strongs/h/h8269.md) of the [Lᵊvî](../../strongs/h/h3881.md), [ruwm](../../strongs/h/h7311.md) unto the [Lᵊvî](../../strongs/h/h3881.md) for [pecach](../../strongs/h/h6453.md) five thousand, and five hundred [bāqār](../../strongs/h/h1241.md).

<a name="2chronicles_35_10"></a>2Chronicles 35:10

So the [ʿăḇōḏâ](../../strongs/h/h5656.md) was [kuwn](../../strongs/h/h3559.md), and the [kōhēn](../../strongs/h/h3548.md) ['amad](../../strongs/h/h5975.md) in their [ʿōmeḏ](../../strongs/h/h5977.md), and the [Lᵊvî](../../strongs/h/h3881.md) in their [maḥălōqeṯ](../../strongs/h/h4256.md), according to the [melek](../../strongs/h/h4428.md) [mitsvah](../../strongs/h/h4687.md).

<a name="2chronicles_35_11"></a>2Chronicles 35:11

And they [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md), and the [kōhēn](../../strongs/h/h3548.md) [zāraq](../../strongs/h/h2236.md) from their [yad](../../strongs/h/h3027.md), and the [Lᵊvî](../../strongs/h/h3881.md) [pāšaṭ](../../strongs/h/h6584.md) them.

<a name="2chronicles_35_12"></a>2Chronicles 35:12

And they [cuwr](../../strongs/h/h5493.md) the [ʿōlâ](../../strongs/h/h5930.md), that they might [nathan](../../strongs/h/h5414.md) according to the [mip̄lagâ](../../strongs/h/h4653.md) of the ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md) of the [ben](../../strongs/h/h1121.md) ['am](../../strongs/h/h5971.md), to [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of [Mōshe](../../strongs/h/h4872.md). And so did they with the [bāqār](../../strongs/h/h1241.md).

<a name="2chronicles_35_13"></a>2Chronicles 35:13

And they [bāšal](../../strongs/h/h1310.md) the [pecach](../../strongs/h/h6453.md) with ['esh](../../strongs/h/h784.md) according to the [mishpat](../../strongs/h/h4941.md): but the other [qodesh](../../strongs/h/h6944.md) [bāšal](../../strongs/h/h1310.md) they in [sîr](../../strongs/h/h5518.md), and in [dûḏ](../../strongs/h/h1731.md), and in [ṣēlaḥāṯ](../../strongs/h/h6745.md), and [rûṣ](../../strongs/h/h7323.md) them among all the [ben](../../strongs/h/h1121.md) ['am](../../strongs/h/h5971.md).

<a name="2chronicles_35_14"></a>2Chronicles 35:14

And ['aḥar](../../strongs/h/h310.md) they made [kuwn](../../strongs/h/h3559.md) for themselves, and for the [kōhēn](../../strongs/h/h3548.md): because the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) were busied in [ʿālâ](../../strongs/h/h5927.md) of [ʿōlâ](../../strongs/h/h5930.md) and the [cheleb](../../strongs/h/h2459.md) until [layil](../../strongs/h/h3915.md); therefore the [Lᵊvî](../../strongs/h/h3881.md) [kuwn](../../strongs/h/h3559.md) for themselves, and for the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md).

<a name="2chronicles_35_15"></a>2Chronicles 35:15

And the [shiyr](../../strongs/h/h7891.md) the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md) were in their [maʿămāḏ](../../strongs/h/h4612.md), according to the [mitsvah](../../strongs/h/h4687.md) of [Dāviḏ](../../strongs/h/h1732.md), and ['Āsāp̄](../../strongs/h/h623.md), and [Hêmān](../../strongs/h/h1968.md), and [Yᵊḏûṯûn](../../strongs/h/h3038.md) the [melek](../../strongs/h/h4428.md) [ḥōzê](../../strongs/h/h2374.md); and the [šôʿēr](../../strongs/h/h7778.md) at every [sha'ar](../../strongs/h/h8179.md); they might not [cuwr](../../strongs/h/h5493.md) from their [ʿăḇōḏâ](../../strongs/h/h5656.md); for their ['ach](../../strongs/h/h251.md) the [Lᵊvî](../../strongs/h/h3881.md) [kuwn](../../strongs/h/h3559.md) for them.

<a name="2chronicles_35_16"></a>2Chronicles 35:16

So all the [ʿăḇōḏâ](../../strongs/h/h5656.md) of [Yĕhovah](../../strongs/h/h3068.md) was [kuwn](../../strongs/h/h3559.md) the same [yowm](../../strongs/h/h3117.md), to ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md), and to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) upon the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), according to the [mitsvah](../../strongs/h/h4687.md) of [melek](../../strongs/h/h4428.md) [Yō'Šîyâ](../../strongs/h/h2977.md).

<a name="2chronicles_35_17"></a>2Chronicles 35:17

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that were [māṣā'](../../strongs/h/h4672.md) ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) at that [ʿēṯ](../../strongs/h/h6256.md), and the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) seven [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_35_18"></a>2Chronicles 35:18

And there was no [pecach](../../strongs/h/h6453.md) like to that ['asah](../../strongs/h/h6213.md) in [Yisra'el](../../strongs/h/h3478.md) from the [yowm](../../strongs/h/h3117.md) of [Šᵊmû'Ēl](../../strongs/h/h8050.md) the [nāḇî'](../../strongs/h/h5030.md); neither ['asah](../../strongs/h/h6213.md) all the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) such a [pecach](../../strongs/h/h6453.md) as [Yō'Šîyâ](../../strongs/h/h2977.md) ['asah](../../strongs/h/h6213.md), and the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and all [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md) that were [māṣā'](../../strongs/h/h4672.md), and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_35_19"></a>2Chronicles 35:19

In the eighteenth [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) was this [pecach](../../strongs/h/h6453.md) ['asah](../../strongs/h/h6213.md).

<a name="2chronicles_35_20"></a>2Chronicles 35:20

['aḥar](../../strongs/h/h310.md) all this, when [Yō'Šîyâ](../../strongs/h/h2977.md) had [kuwn](../../strongs/h/h3559.md) the [bayith](../../strongs/h/h1004.md), [Nᵊḵô](../../strongs/h/h5224.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) to [lāḥam](../../strongs/h/h3898.md) against [karkᵊmîš](../../strongs/h/h3751.md) by [Pᵊrāṯ](../../strongs/h/h6578.md): and [Yō'Šîyâ](../../strongs/h/h2977.md) [yāṣā'](../../strongs/h/h3318.md) [qārā'](../../strongs/h/h7125.md) him.

<a name="2chronicles_35_21"></a>2Chronicles 35:21

But he [shalach](../../strongs/h/h7971.md) [mal'ak](../../strongs/h/h4397.md) to him, ['āmar](../../strongs/h/h559.md), What have I to do with thee, thou [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md)? I come not against thee this [yowm](../../strongs/h/h3117.md), but against the [bayith](../../strongs/h/h1004.md) wherewith I have [milḥāmâ](../../strongs/h/h4421.md): for ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) me to make [bahal](../../strongs/h/h926.md): [ḥāḏal](../../strongs/h/h2308.md) thee from meddling with ['Elohiym](../../strongs/h/h430.md), who is with me, that he [shachath](../../strongs/h/h7843.md) thee not.

<a name="2chronicles_35_22"></a>2Chronicles 35:22

Nevertheless [Yō'Šîyâ](../../strongs/h/h2977.md) would not [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md) from him, but [ḥāp̄aś](../../strongs/h/h2664.md) himself, that he might [lāḥam](../../strongs/h/h3898.md) with him, and [shama'](../../strongs/h/h8085.md) not unto the [dabar](../../strongs/h/h1697.md) of [Nᵊḵô](../../strongs/h/h5224.md) from the [peh](../../strongs/h/h6310.md) of ['Elohiym](../../strongs/h/h430.md), and [bow'](../../strongs/h/h935.md) to [lāḥam](../../strongs/h/h3898.md) in the [biqʿâ](../../strongs/h/h1237.md) of [Mᵊḡidôn](../../strongs/h/h4023.md).

<a name="2chronicles_35_23"></a>2Chronicles 35:23

And the [yārâ](../../strongs/h/h3384.md) [yārâ](../../strongs/h/h3384.md) at [melek](../../strongs/h/h4428.md) [Yō'Šîyâ](../../strongs/h/h2977.md); and the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to his ['ebed](../../strongs/h/h5650.md), Have me ['abar](../../strongs/h/h5674.md); for I am [me'od](../../strongs/h/h3966.md) [ḥālâ](../../strongs/h/h2470.md).

<a name="2chronicles_35_24"></a>2Chronicles 35:24

His ['ebed](../../strongs/h/h5650.md) therefore ['abar](../../strongs/h/h5674.md) him out of that [merkāḇâ](../../strongs/h/h4818.md), and [rāḵaḇ](../../strongs/h/h7392.md) him in the [mišnê](../../strongs/h/h4932.md) [reḵeḇ](../../strongs/h/h7393.md) that he had; and they [yālaḵ](../../strongs/h/h3212.md) him to [Yĕruwshalaim](../../strongs/h/h3389.md), and he [muwth](../../strongs/h/h4191.md), and was [qāḇar](../../strongs/h/h6912.md) in one of the [qeber](../../strongs/h/h6913.md) of his ['ab](../../strongs/h/h1.md). And all [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) ['āḇal](../../strongs/h/h56.md) for [Yō'Šîyâ](../../strongs/h/h2977.md).

<a name="2chronicles_35_25"></a>2Chronicles 35:25

And [Yirmᵊyâ](../../strongs/h/h3414.md) [qônēn](../../strongs/h/h6969.md) for [Yō'Šîyâ](../../strongs/h/h2977.md): and all the[shiyr](../../strongs/h/h7891.md) and the [shiyr](../../strongs/h/h7891.md) ['āmar](../../strongs/h/h559.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) in their [qînâ](../../strongs/h/h7015.md) to this [yowm](../../strongs/h/h3117.md), and [nathan](../../strongs/h/h5414.md) them a [choq](../../strongs/h/h2706.md) in [Yisra'el](../../strongs/h/h3478.md): and, behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [qînâ](../../strongs/h/h7015.md).

<a name="2chronicles_35_26"></a>2Chronicles 35:26

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yō'Šîyâ](../../strongs/h/h2977.md), and his [checed](../../strongs/h/h2617.md), according to that which was [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="2chronicles_35_27"></a>2Chronicles 35:27

And his [dabar](../../strongs/h/h1697.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 34](2chronicles_34.md) - [2Chronicles 36](2chronicles_36.md)