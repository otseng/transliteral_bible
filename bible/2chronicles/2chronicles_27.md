# [2Chronicles 27](https://www.blueletterbible.org/kjv/2chronicles/27)

<a name="2chronicles_27_1"></a>2Chronicles 27:1

[Yôṯām](../../strongs/h/h3147.md) was twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) sixteen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was [Yᵊrûšā'](../../strongs/h/h3388.md), the [bath](../../strongs/h/h1323.md) of [Ṣāḏôq](../../strongs/h/h6659.md).

<a name="2chronicles_27_2"></a>2Chronicles 27:2

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that his ['ab](../../strongs/h/h1.md) ['Uzziyah](../../strongs/h/h5818.md) ['asah](../../strongs/h/h6213.md): howbeit he [bow'](../../strongs/h/h935.md) not into the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md). And the ['am](../../strongs/h/h5971.md) did yet [shachath](../../strongs/h/h7843.md).

<a name="2chronicles_27_3"></a>2Chronicles 27:3

He [bānâ](../../strongs/h/h1129.md) the ['elyown](../../strongs/h/h5945.md) [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and on the [ḥômâ](../../strongs/h/h2346.md) of [ʿŌp̄El](../../strongs/h/h6077.md) he [bānâ](../../strongs/h/h1129.md) [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_27_4"></a>2Chronicles 27:4

Moreover he [bānâ](../../strongs/h/h1129.md) [ʿîr](../../strongs/h/h5892.md) in the [har](../../strongs/h/h2022.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥōreš](../../strongs/h/h2793.md) he [bānâ](../../strongs/h/h1129.md) [bîrānîṯ](../../strongs/h/h1003.md) and [miḡdāl](../../strongs/h/h4026.md).

<a name="2chronicles_27_5"></a>2Chronicles 27:5

He [lāḥam](../../strongs/h/h3898.md) also with the [melek](../../strongs/h/h4428.md) of the [ʿAmmôn](../../strongs/h/h5984.md), and [ḥāzaq](../../strongs/h/h2388.md) against them. And the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [nathan](../../strongs/h/h5414.md) him the same [šānâ](../../strongs/h/h8141.md) an hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md), and ten thousand [kōr](../../strongs/h/h3734.md) of [ḥiṭṭâ](../../strongs/h/h2406.md), and ten thousand of [śᵊʿōrâ](../../strongs/h/h8184.md). So much did the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md) [shuwb](../../strongs/h/h7725.md) unto him, both the second [šānâ](../../strongs/h/h8141.md), and the third.

<a name="2chronicles_27_6"></a>2Chronicles 27:6

So [Yôṯām](../../strongs/h/h3147.md) became [ḥāzaq](../../strongs/h/h2388.md), because he [kuwn](../../strongs/h/h3559.md) his [derek](../../strongs/h/h1870.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_27_7"></a>2Chronicles 27:7

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yôṯām](../../strongs/h/h3147.md), and all his [milḥāmâ](../../strongs/h/h4421.md), and his [derek](../../strongs/h/h1870.md), lo, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_27_8"></a>2Chronicles 27:8

He was five and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) sixteen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_27_9"></a>2Chronicles 27:9

And [Yôṯām](../../strongs/h/h3147.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Āḥāz](../../strongs/h/h271.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 26](2chronicles_26.md) - [2Chronicles 28](2chronicles_28.md)