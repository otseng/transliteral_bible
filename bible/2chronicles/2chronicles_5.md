# [2Chronicles 5](https://www.blueletterbible.org/kjv/2chronicles/5)

<a name="2chronicles_5_1"></a>2Chronicles 5:1

Thus all the [mĕla'kah](../../strongs/h/h4399.md) that [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) was [shalam](../../strongs/h/h7999.md): and [Šᵊlōmô](../../strongs/h/h8010.md) [bow'](../../strongs/h/h935.md) in all the [qodesh](../../strongs/h/h6944.md) that [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md) had [qodesh](../../strongs/h/h6944.md); and the [keceph](../../strongs/h/h3701.md), and the [zāhāḇ](../../strongs/h/h2091.md), and all the [kĕliy](../../strongs/h/h3627.md), [nathan](../../strongs/h/h5414.md) he among the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_5_2"></a>2Chronicles 5:2

Then [Šᵊlōmô](../../strongs/h/h8010.md) [qāhal](../../strongs/h/h6950.md) the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md), and all the [ro'sh](../../strongs/h/h7218.md) of the [maṭṭê](../../strongs/h/h4294.md), the [nāśî'](../../strongs/h/h5387.md) of the ['ab](../../strongs/h/h1.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), unto [Yĕruwshalaim](../../strongs/h/h3389.md), to [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) out of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), which is [Tsiyown](../../strongs/h/h6726.md).

<a name="2chronicles_5_3"></a>2Chronicles 5:3

Wherefore all the ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [qāhal](../../strongs/h/h6950.md) themselves unto the [melek](../../strongs/h/h4428.md) in the [ḥāḡ](../../strongs/h/h2282.md) which was in the seventh [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2chronicles_5_4"></a>2Chronicles 5:4

And all the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md); and the [Lᵊvî](../../strongs/h/h3881.md) [nasa'](../../strongs/h/h5375.md) the ['ārôn](../../strongs/h/h727.md).

<a name="2chronicles_5_5"></a>2Chronicles 5:5

And they [ʿālâ](../../strongs/h/h5927.md) the ['ārôn](../../strongs/h/h727.md), and the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and all the [qodesh](../../strongs/h/h6944.md) [kĕliy](../../strongs/h/h3627.md) that were in the ['ohel](../../strongs/h/h168.md), these did the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) [ʿālâ](../../strongs/h/h5927.md).

<a name="2chronicles_5_6"></a>2Chronicles 5:6

Also [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md), and all the ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md) that were [yāʿaḏ](../../strongs/h/h3259.md) unto him [paniym](../../strongs/h/h6440.md) the ['ārôn](../../strongs/h/h727.md), [zabach](../../strongs/h/h2076.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md), which could not be [sāp̄ar](../../strongs/h/h5608.md) nor [mānâ](../../strongs/h/h4487.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_5_7"></a>2Chronicles 5:7

And the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) the ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) unto his [maqowm](../../strongs/h/h4725.md), to the [dᵊḇîr](../../strongs/h/h1687.md) of the [bayith](../../strongs/h/h1004.md), into the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) place, even under the [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md):

<a name="2chronicles_5_8"></a>2Chronicles 5:8

For the [kĕruwb](../../strongs/h/h3742.md) [pāraś](../../strongs/h/h6566.md) their [kanaph](../../strongs/h/h3671.md) over the [maqowm](../../strongs/h/h4725.md) of the ['ārôn](../../strongs/h/h727.md), and the [kĕruwb](../../strongs/h/h3742.md) [kāsâ](../../strongs/h/h3680.md) the ['ārôn](../../strongs/h/h727.md) and the [baḏ](../../strongs/h/h905.md) thereof [maʿal](../../strongs/h/h4605.md).

<a name="2chronicles_5_9"></a>2Chronicles 5:9

And they ['arak](../../strongs/h/h748.md) the [baḏ](../../strongs/h/h905.md), that the [ro'sh](../../strongs/h/h7218.md) of the [baḏ](../../strongs/h/h905.md) were [ra'ah](../../strongs/h/h7200.md) from the ['ārôn](../../strongs/h/h727.md) [paniym](../../strongs/h/h6440.md) the [dᵊḇîr](../../strongs/h/h1687.md); but they were not [ra'ah](../../strongs/h/h7200.md) [ḥûṣ](../../strongs/h/h2351.md). And there it is unto this [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_5_10"></a>2Chronicles 5:10

There was nothing in the ['ārôn](../../strongs/h/h727.md) save the two [lûaḥ](../../strongs/h/h3871.md) which [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) therein at [ḥōrēḇ](../../strongs/h/h2722.md), when [Yĕhovah](../../strongs/h/h3068.md) [karath](../../strongs/h/h3772.md) a covenant with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), when they [yāṣā'](../../strongs/h/h3318.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="2chronicles_5_11"></a>2Chronicles 5:11

And it came to pass, when the [kōhēn](../../strongs/h/h3548.md) were [yāṣā'](../../strongs/h/h3318.md) of the [qodesh](../../strongs/h/h6944.md): (for all the [kōhēn](../../strongs/h/h3548.md) that were [māṣā'](../../strongs/h/h4672.md) were [qadash](../../strongs/h/h6942.md), and did not then [shamar](../../strongs/h/h8104.md) by [maḥălōqeṯ](../../strongs/h/h4256.md):

<a name="2chronicles_5_12"></a>2Chronicles 5:12

Also the [Lᵊvî](../../strongs/h/h3881.md) which were the [shiyr](../../strongs/h/h7891.md), all of them of ['Āsāp̄](../../strongs/h/h623.md), of [Hêmān](../../strongs/h/h1968.md), of [Yᵊḏûṯûn](../../strongs/h/h3038.md), with their [ben](../../strongs/h/h1121.md) and their ['ach](../../strongs/h/h251.md), being [labash](../../strongs/h/h3847.md) in [bûṣ](../../strongs/h/h948.md), having [mᵊṣēleṯ](../../strongs/h/h4700.md) and [neḇel](../../strongs/h/h5035.md) and [kinnôr](../../strongs/h/h3658.md), ['amad](../../strongs/h/h5975.md) at the [mizrach](../../strongs/h/h4217.md) of the [mizbeach](../../strongs/h/h4196.md), and with them an hundred and twenty [kōhēn](../../strongs/h/h3548.md) [ḥāṣar](../../strongs/h/h2690.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md):)

<a name="2chronicles_5_13"></a>2Chronicles 5:13

It came even to pass, as the [ḥāṣar](../../strongs/h/h2690.md) [ḥāṣar](../../strongs/h/h2690.md) and [shiyr](../../strongs/h/h7891.md) were as one, to make one [qowl](../../strongs/h/h6963.md) to be [shama'](../../strongs/h/h8085.md) in [halal](../../strongs/h/h1984.md) and [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md); and when they [ruwm](../../strongs/h/h7311.md) their [qowl](../../strongs/h/h6963.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md) and [mᵊṣēleṯ](../../strongs/h/h4700.md) and [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md), and [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md), saying, For he is [towb](../../strongs/h/h2896.md); for his [checed](../../strongs/h/h2617.md) ['owlam](../../strongs/h/h5769.md): that then the [bayith](../../strongs/h/h1004.md) was [mālā'](../../strongs/h/h4390.md) with a [ʿānān](../../strongs/h/h6051.md), even the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="2chronicles_5_14"></a>2Chronicles 5:14

So that the [kōhēn](../../strongs/h/h3548.md) [yakol](../../strongs/h/h3201.md) not ['amad](../../strongs/h/h5975.md) to [sharath](../../strongs/h/h8334.md) by [paniym](../../strongs/h/h6440.md) of the [ʿānān](../../strongs/h/h6051.md): for the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) had [mālā'](../../strongs/h/h4390.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 4](2chronicles_4.md) - [2Chronicles 6](2chronicles_6.md)