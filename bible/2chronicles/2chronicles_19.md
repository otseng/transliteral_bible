# [2Chronicles 19](https://www.blueletterbible.org/kjv/2chronicles/19)

<a name="2chronicles_19_1"></a>2Chronicles 19:1

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [shuwb](../../strongs/h/h7725.md) to his [bayith](../../strongs/h/h1004.md) in [shalowm](../../strongs/h/h7965.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_19_2"></a>2Chronicles 19:2

And [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Ḥănānî](../../strongs/h/h2607.md) the [ḥōzê](../../strongs/h/h2374.md) [yāṣā'](../../strongs/h/h3318.md) to [paniym](../../strongs/h/h6440.md) him, and ['āmar](../../strongs/h/h559.md) to [melek](../../strongs/h/h4428.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Shouldest thou [ʿāzar](../../strongs/h/h5826.md) the [rasha'](../../strongs/h/h7563.md), and ['ahab](../../strongs/h/h157.md) them that [sane'](../../strongs/h/h8130.md) [Yĕhovah](../../strongs/h/h3068.md)? therefore is [qeṣep̄](../../strongs/h/h7110.md) upon thee from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_19_3"></a>2Chronicles 19:3

['ăḇāl](../../strongs/h/h61.md) there are [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) [māṣā'](../../strongs/h/h4672.md) in thee, in that thou hast [bāʿar](../../strongs/h/h1197.md) the ['ăšērâ](../../strongs/h/h842.md) out of the ['erets](../../strongs/h/h776.md), and hast [kuwn](../../strongs/h/h3559.md) thine [lebab](../../strongs/h/h3824.md) to [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_19_4"></a>2Chronicles 19:4

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): and he [yāṣā'](../../strongs/h/h3318.md) [shuwb](../../strongs/h/h7725.md) through the ['am](../../strongs/h/h5971.md) from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) to [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and [shuwb](../../strongs/h/h7725.md) them unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_19_5"></a>2Chronicles 19:5

And he ['amad](../../strongs/h/h5975.md) [shaphat](../../strongs/h/h8199.md) in the ['erets](../../strongs/h/h776.md) throughout all the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), [ʿîr](../../strongs/h/h5892.md) by [ʿîr](../../strongs/h/h5892.md),

<a name="2chronicles_19_6"></a>2Chronicles 19:6

And ['āmar](../../strongs/h/h559.md) to the [shaphat](../../strongs/h/h8199.md), [ra'ah](../../strongs/h/h7200.md) what ye ['asah](../../strongs/h/h6213.md): for ye [shaphat](../../strongs/h/h8199.md) not for ['āḏām](../../strongs/h/h120.md), but for [Yĕhovah](../../strongs/h/h3068.md), who is with you in the [dabar](../../strongs/h/h1697.md) [mishpat](../../strongs/h/h4941.md).

<a name="2chronicles_19_7"></a>2Chronicles 19:7

Wherefore now let the [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md) be upon you; take [shamar](../../strongs/h/h8104.md) and ['asah](../../strongs/h/h6213.md) it: for there is no ['evel](../../strongs/h/h5766.md) with [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), nor [maśś'](../../strongs/h/h4856.md) of [paniym](../../strongs/h/h6440.md), nor [miqqāḥ](../../strongs/h/h4727.md) of [shachad](../../strongs/h/h7810.md).

<a name="2chronicles_19_8"></a>2Chronicles 19:8

Moreover in [Yĕruwshalaim](../../strongs/h/h3389.md) did [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['amad](../../strongs/h/h5975.md) of the [Lᵊvî](../../strongs/h/h3881.md), and of the [kōhēn](../../strongs/h/h3548.md), and of the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of [Yisra'el](../../strongs/h/h3478.md), for the [mishpat](../../strongs/h/h4941.md) of [Yĕhovah](../../strongs/h/h3068.md), and for [rîḇ](../../strongs/h/h7379.md), when they [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_19_9"></a>2Chronicles 19:9

And he [tsavah](../../strongs/h/h6680.md) them, ['āmar](../../strongs/h/h559.md), Thus shall ye ['asah](../../strongs/h/h6213.md) in the [yir'ah](../../strongs/h/h3374.md) of [Yĕhovah](../../strongs/h/h3068.md), ['ĕmûnâ](../../strongs/h/h530.md), and with a [šālēm](../../strongs/h/h8003.md) [lebab](../../strongs/h/h3824.md).

<a name="2chronicles_19_10"></a>2Chronicles 19:10

And what [rîḇ](../../strongs/h/h7379.md) soever shall [bow'](../../strongs/h/h935.md) to you of your ['ach](../../strongs/h/h251.md) that [yashab](../../strongs/h/h3427.md) in their [ʿîr](../../strongs/h/h5892.md), between [dam](../../strongs/h/h1818.md) and [dam](../../strongs/h/h1818.md), between [towrah](../../strongs/h/h8451.md) and [mitsvah](../../strongs/h/h4687.md), [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md), ye shall even [zāhar](../../strongs/h/h2094.md) them that they ['asham](../../strongs/h/h816.md) not against [Yĕhovah](../../strongs/h/h3068.md), and so [qeṣep̄](../../strongs/h/h7110.md) come upon you, and upon your ['ach](../../strongs/h/h251.md): this ['asah](../../strongs/h/h6213.md), and ye shall not ['asham](../../strongs/h/h816.md).

<a name="2chronicles_19_11"></a>2Chronicles 19:11

And, behold, ['Ămaryâ](../../strongs/h/h568.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md) is over you in all [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md); and [Zᵊḇaḏyâ](../../strongs/h/h2069.md) the [ben](../../strongs/h/h1121.md) of [Yišmāʿē'l](../../strongs/h/h3458.md), the [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), for all the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md): also the [Lᵊvî](../../strongs/h/h3881.md) shall be [šāṭar](../../strongs/h/h7860.md) [paniym](../../strongs/h/h6440.md) you. ['asah](../../strongs/h/h6213.md) [ḥāzaq](../../strongs/h/h2388.md), and [Yĕhovah](../../strongs/h/h3068.md) shall be with the [towb](../../strongs/h/h2896.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 18](2chronicles_18.md) - [2Chronicles 20](2chronicles_20.md)