# [2Chronicles 15](https://www.blueletterbible.org/kjv/2chronicles/15)

<a name="2chronicles_15_1"></a>2Chronicles 15:1

And the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) came upon [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [ʿÔḏēḏ](../../strongs/h/h5752.md):

<a name="2chronicles_15_2"></a>2Chronicles 15:2

And he [yāṣā'](../../strongs/h/h3318.md) to [paniym](../../strongs/h/h6440.md) ['Āsā'](../../strongs/h/h609.md), and ['āmar](../../strongs/h/h559.md) unto him, [shama'](../../strongs/h/h8085.md) ye me, ['Āsā'](../../strongs/h/h609.md), and all [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md); [Yĕhovah](../../strongs/h/h3068.md) is with you, while ye be with him; and if ye [darash](../../strongs/h/h1875.md) him, he will be [māṣā'](../../strongs/h/h4672.md) of you; but if ye ['azab](../../strongs/h/h5800.md) him, he will ['azab](../../strongs/h/h5800.md) you.

<a name="2chronicles_15_3"></a>2Chronicles 15:3

Now for a [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) [Yisra'el](../../strongs/h/h3478.md) hath been without the ['emeth](../../strongs/h/h571.md) ['Elohiym](../../strongs/h/h430.md), and without a [yārâ](../../strongs/h/h3384.md) [kōhēn](../../strongs/h/h3548.md), and without [towrah](../../strongs/h/h8451.md).

<a name="2chronicles_15_4"></a>2Chronicles 15:4

But when they in their [tsar](../../strongs/h/h6862.md) did [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), and [bāqaš](../../strongs/h/h1245.md) him, he was [māṣā'](../../strongs/h/h4672.md) of them.

<a name="2chronicles_15_5"></a>2Chronicles 15:5

And in those [ʿēṯ](../../strongs/h/h6256.md) there was no [shalowm](../../strongs/h/h7965.md) to him that [yāṣā'](../../strongs/h/h3318.md), nor to him that [bow'](../../strongs/h/h935.md), but [rab](../../strongs/h/h7227.md) [mᵊhûmâ](../../strongs/h/h4103.md) were upon all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="2chronicles_15_6"></a>2Chronicles 15:6

And [gowy](../../strongs/h/h1471.md) was [kāṯaṯ](../../strongs/h/h3807.md) of [gowy](../../strongs/h/h1471.md), and [ʿîr](../../strongs/h/h5892.md) of [ʿîr](../../strongs/h/h5892.md): for ['Elohiym](../../strongs/h/h430.md) did [hāmam](../../strongs/h/h2000.md) them with all [tsarah](../../strongs/h/h6869.md).

<a name="2chronicles_15_7"></a>2Chronicles 15:7

Be ye [ḥāzaq](../../strongs/h/h2388.md) therefore, and let not your [yad](../../strongs/h/h3027.md) be [rāp̄â](../../strongs/h/h7503.md): for your [pe'ullah](../../strongs/h/h6468.md) shall be [śāḵār](../../strongs/h/h7939.md).

<a name="2chronicles_15_8"></a>2Chronicles 15:8

And when ['Āsā'](../../strongs/h/h609.md) [shama'](../../strongs/h/h8085.md) these [dabar](../../strongs/h/h1697.md), and the [nᵊḇû'â](../../strongs/h/h5016.md) of [ʿÔḏēḏ](../../strongs/h/h5752.md) the [nāḇî'](../../strongs/h/h5030.md), he [ḥāzaq](../../strongs/h/h2388.md), and ['abar](../../strongs/h/h5674.md) the [šiqqûṣ](../../strongs/h/h8251.md) out of all the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), and out of the [ʿîr](../../strongs/h/h5892.md) which he had [lāḵaḏ](../../strongs/h/h3920.md) from [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and [ḥādaš](../../strongs/h/h2318.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), that was [paniym](../../strongs/h/h6440.md) the ['ûlām](../../strongs/h/h197.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_15_9"></a>2Chronicles 15:9

And he [qāḇaṣ](../../strongs/h/h6908.md) all [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), and the [guwr](../../strongs/h/h1481.md) with them out of ['Ep̄rayim](../../strongs/h/h669.md) and [Mᵊnaššê](../../strongs/h/h4519.md), and out of [Šimʿôn](../../strongs/h/h8095.md): for they [naphal](../../strongs/h/h5307.md) to him out of [Yisra'el](../../strongs/h/h3478.md) in [rōḇ](../../strongs/h/h7230.md), when they [ra'ah](../../strongs/h/h7200.md) that [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) was with him.

<a name="2chronicles_15_10"></a>2Chronicles 15:10

So they gathered themselves [qāḇaṣ](../../strongs/h/h6908.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) in the third [ḥōḏeš](../../strongs/h/h2320.md), in the fifteenth [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of ['Āsā'](../../strongs/h/h609.md).

<a name="2chronicles_15_11"></a>2Chronicles 15:11

And they [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) the same [yowm](../../strongs/h/h3117.md), of the [šālāl](../../strongs/h/h7998.md) which they had [bow'](../../strongs/h/h935.md), seven hundred [bāqār](../../strongs/h/h1241.md) and seven thousand [tso'n](../../strongs/h/h6629.md).

<a name="2chronicles_15_12"></a>2Chronicles 15:12

And they [bow'](../../strongs/h/h935.md) into a [bĕriyth](../../strongs/h/h1285.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md) with all their [lebab](../../strongs/h/h3824.md) and with all their [nephesh](../../strongs/h/h5315.md);

<a name="2chronicles_15_13"></a>2Chronicles 15:13

That whosoever would not [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) should be put to [muwth](../../strongs/h/h4191.md), whether [qāṭān](../../strongs/h/h6996.md) or [gadowl](../../strongs/h/h1419.md), whether ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md).

<a name="2chronicles_15_14"></a>2Chronicles 15:14

And they [shaba'](../../strongs/h/h7650.md) unto [Yĕhovah](../../strongs/h/h3068.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md), and with [tᵊrûʿâ](../../strongs/h/h8643.md), and with [ḥăṣōṣrâ](../../strongs/h/h2689.md), and with [šôp̄ār](../../strongs/h/h7782.md).

<a name="2chronicles_15_15"></a>2Chronicles 15:15

And all [Yehuwdah](../../strongs/h/h3063.md) [samach](../../strongs/h/h8055.md) at the [šᵊḇûʿâ](../../strongs/h/h7621.md): for they had [shaba'](../../strongs/h/h7650.md) with all their [lebab](../../strongs/h/h3824.md), and [bāqaš](../../strongs/h/h1245.md) him with their whole [ratsown](../../strongs/h/h7522.md); and he was [māṣā'](../../strongs/h/h4672.md) of them: and [Yĕhovah](../../strongs/h/h3068.md) gave them [nuwach](../../strongs/h/h5117.md) [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_15_16"></a>2Chronicles 15:16

And also concerning [Maʿăḵâ](../../strongs/h/h4601.md) the ['em](../../strongs/h/h517.md) of ['Āsā'](../../strongs/h/h609.md) the [melek](../../strongs/h/h4428.md), he [cuwr](../../strongs/h/h5493.md) her from being [ḡᵊḇîrâ](../../strongs/h/h1377.md), because she had ['asah](../../strongs/h/h6213.md) a [mip̄leṣeṯ](../../strongs/h/h4656.md) in a ['ăšērâ](../../strongs/h/h842.md): and ['Āsā'](../../strongs/h/h609.md) [karath](../../strongs/h/h3772.md) her [mip̄leṣeṯ](../../strongs/h/h4656.md), and [dāqaq](../../strongs/h/h1854.md) it, and [śārap̄](../../strongs/h/h8313.md) it at the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md).

<a name="2chronicles_15_17"></a>2Chronicles 15:17

But the [bāmâ](../../strongs/h/h1116.md) were not [cuwr](../../strongs/h/h5493.md) out of [Yisra'el](../../strongs/h/h3478.md): nevertheless the [lebab](../../strongs/h/h3824.md) of ['Āsā'](../../strongs/h/h609.md) was [šālēm](../../strongs/h/h8003.md) all his [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_15_18"></a>2Chronicles 15:18

And he [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) the things that his ['ab](../../strongs/h/h1.md) had [qodesh](../../strongs/h/h6944.md), and that he himself had [qodesh](../../strongs/h/h6944.md), [keceph](../../strongs/h/h3701.md), and [zāhāḇ](../../strongs/h/h2091.md), and [kĕliy](../../strongs/h/h3627.md).

<a name="2chronicles_15_19"></a>2Chronicles 15:19

And there was no more [milḥāmâ](../../strongs/h/h4421.md) unto the five and thirtieth [šānâ](../../strongs/h/h8141.md) of the [malkuwth](../../strongs/h/h4438.md) of ['Āsā'](../../strongs/h/h609.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 14](2chronicles_14.md) - [2Chronicles 16](2chronicles_16.md)