# [2Chronicles 28](https://www.blueletterbible.org/kjv/2chronicles/28)

<a name="2chronicles_28_1"></a>2Chronicles 28:1

['Āḥāz](../../strongs/h/h271.md) was twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) sixteen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): but he ['asah](../../strongs/h/h6213.md) not that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), like [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md):

<a name="2chronicles_28_2"></a>2Chronicles 28:2

For he [yālaḵ](../../strongs/h/h3212.md) the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and ['asah](../../strongs/h/h6213.md) also [massēḵâ](../../strongs/h/h4541.md) for [BaʿAl](../../strongs/h/h1168.md).

<a name="2chronicles_28_3"></a>2Chronicles 28:3

Moreover he [qāṭar](../../strongs/h/h6999.md) in the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), and [bāʿar](../../strongs/h/h1197.md) his [ben](../../strongs/h/h1121.md) in the ['esh](../../strongs/h/h784.md), after the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md) whom [Yĕhovah](../../strongs/h/h3068.md) had [yarash](../../strongs/h/h3423.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_28_4"></a>2Chronicles 28:4

He [zabach](../../strongs/h/h2076.md) also and [qāṭar](../../strongs/h/h6999.md) in the [bāmâ](../../strongs/h/h1116.md), and on the [giḇʿâ](../../strongs/h/h1389.md), and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md).

<a name="2chronicles_28_5"></a>2Chronicles 28:5

Wherefore [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) him into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md); and they [nakah](../../strongs/h/h5221.md) him, and [šāḇâ](../../strongs/h/h7617.md) a [gadowl](../../strongs/h/h1419.md) of them [šiḇyâ](../../strongs/h/h7633.md), and [bow'](../../strongs/h/h935.md) them to [Dammeśeq](../../strongs/h/h1834.md). And he was also [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), who [nakah](../../strongs/h/h5221.md) him with a [gadowl](../../strongs/h/h1419.md) [makâ](../../strongs/h/h4347.md).

<a name="2chronicles_28_6"></a>2Chronicles 28:6

For [Peqaḥ](../../strongs/h/h6492.md) the [ben](../../strongs/h/h1121.md) of [Rᵊmalyâû](../../strongs/h/h7425.md) [harag](../../strongs/h/h2026.md) in [Yehuwdah](../../strongs/h/h3063.md) an hundred and twenty thousand in one [yowm](../../strongs/h/h3117.md), which were all [ḥayil](../../strongs/h/h2428.md) [ben](../../strongs/h/h1121.md); because they had ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_28_7"></a>2Chronicles 28:7

And [Ziḵrî](../../strongs/h/h2147.md), a [gibôr](../../strongs/h/h1368.md) of ['Ep̄rayim](../../strongs/h/h669.md), [harag](../../strongs/h/h2026.md) [MaʿĂśêâ](../../strongs/h/h4641.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and [ʿAzrîqām](../../strongs/h/h5840.md) the [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md), and ['Elqānâ](../../strongs/h/h511.md) that was [mišnê](../../strongs/h/h4932.md) to the [melek](../../strongs/h/h4428.md).

<a name="2chronicles_28_8"></a>2Chronicles 28:8

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [šāḇâ](../../strongs/h/h7617.md) of their ['ach](../../strongs/h/h251.md) two hundred thousand, ['ishshah](../../strongs/h/h802.md), [ben](../../strongs/h/h1121.md), and [bath](../../strongs/h/h1323.md), and took also [bāzaz](../../strongs/h/h962.md) [rab](../../strongs/h/h7227.md) [šālāl](../../strongs/h/h7998.md) from them, and [bow'](../../strongs/h/h935.md) the [šālāl](../../strongs/h/h7998.md) to [Šōmrôn](../../strongs/h/h8111.md).

<a name="2chronicles_28_9"></a>2Chronicles 28:9

But a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md) was there, whose [shem](../../strongs/h/h8034.md) was [ʿÔḏēḏ](../../strongs/h/h5752.md): and he [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) the [tsaba'](../../strongs/h/h6635.md) that [bow'](../../strongs/h/h935.md) to [Šōmrôn](../../strongs/h/h8111.md), and ['āmar](../../strongs/h/h559.md) unto them, Behold, because [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md) was [chemah](../../strongs/h/h2534.md) with [Yehuwdah](../../strongs/h/h3063.md), he hath [nathan](../../strongs/h/h5414.md) them into your [yad](../../strongs/h/h3027.md), and ye have [harag](../../strongs/h/h2026.md) them in a [zaʿap̄](../../strongs/h/h2197.md) that reacheth [naga'](../../strongs/h/h5060.md) unto [shamayim](../../strongs/h/h8064.md).

<a name="2chronicles_28_10"></a>2Chronicles 28:10

And now ye ['āmar](../../strongs/h/h559.md) to [kāḇaš](../../strongs/h/h3533.md) the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) for ['ebed](../../strongs/h/h5650.md) and [šip̄ḥâ](../../strongs/h/h8198.md) unto you: but are there not with you, even with you, ['ašmâ](../../strongs/h/h819.md) against [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md)?

<a name="2chronicles_28_11"></a>2Chronicles 28:11

Now [shama'](../../strongs/h/h8085.md) me therefore, and [shuwb](../../strongs/h/h7725.md) the [šiḇyâ](../../strongs/h/h7633.md) again, which ye have taken [šāḇâ](../../strongs/h/h7617.md) of your ['ach](../../strongs/h/h251.md): for the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon you.

<a name="2chronicles_28_12"></a>2Chronicles 28:12

Then ['enowsh](../../strongs/h/h582.md) of the [ro'sh](../../strongs/h/h7218.md) of the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md), [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôḥānān](../../strongs/h/h3076.md), [Bereḵyâ](../../strongs/h/h1296.md) the [ben](../../strongs/h/h1121.md) of [mᵊšillēmôṯ](../../strongs/h/h4919.md), and [Yᵊḥizqîyâ](../../strongs/h/h3169.md) the [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md), and [ʿĂmāśā'](../../strongs/h/h6021.md) the [ben](../../strongs/h/h1121.md) of [Ḥaḏlay](../../strongs/h/h2311.md), stood [quwm](../../strongs/h/h6965.md) against them that [bow'](../../strongs/h/h935.md) from the [tsaba'](../../strongs/h/h6635.md),

<a name="2chronicles_28_13"></a>2Chronicles 28:13

And ['āmar](../../strongs/h/h559.md) unto them, Ye shall not [bow'](../../strongs/h/h935.md) the [šiḇyâ](../../strongs/h/h7633.md) hither: for whereas we have ['ašmâ](../../strongs/h/h819.md) against [Yĕhovah](../../strongs/h/h3068.md) already, ye ['āmar](../../strongs/h/h559.md) to add more to our [chatta'ath](../../strongs/h/h2403.md) and to our ['ašmâ](../../strongs/h/h819.md): for our ['ašmâ](../../strongs/h/h819.md) is [rab](../../strongs/h/h7227.md), and there is [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) against [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_28_14"></a>2Chronicles 28:14

So the [chalats](../../strongs/h/h2502.md) ['azab](../../strongs/h/h5800.md) the [šiḇyâ](../../strongs/h/h7633.md) and the [bizzâ](../../strongs/h/h961.md) [paniym](../../strongs/h/h6440.md) the [śar](../../strongs/h/h8269.md) and all the [qāhēl](../../strongs/h/h6951.md).

<a name="2chronicles_28_15"></a>2Chronicles 28:15

And the ['enowsh](../../strongs/h/h582.md) which were [nāqaḇ](../../strongs/h/h5344.md) by [shem](../../strongs/h/h8034.md) [quwm](../../strongs/h/h6965.md), and [ḥāzaq](../../strongs/h/h2388.md) the [šiḇyâ](../../strongs/h/h7633.md), and with the [šālāl](../../strongs/h/h7998.md) [labash](../../strongs/h/h3847.md) all that were [maʿărōm](../../strongs/h/h4636.md) among them, and [labash](../../strongs/h/h3847.md) them, and [nāʿal](../../strongs/h/h5274.md) them, and gave them to ['akal](../../strongs/h/h398.md) and to [šāqâ](../../strongs/h/h8248.md), and [sûḵ](../../strongs/h/h5480.md) them, and [nāhal](../../strongs/h/h5095.md) all the [kashal](../../strongs/h/h3782.md) of them upon [chamowr](../../strongs/h/h2543.md), and [bow'](../../strongs/h/h935.md) them to [Yᵊrēḥô](../../strongs/h/h3405.md), the [ʿîr](../../strongs/h/h5892.md) of [tāmār](../../strongs/h/h8558.md) [ʿîr hatmārîm](../../strongs/h/h5899.md), to their ['ach](../../strongs/h/h251.md): then they [shuwb](../../strongs/h/h7725.md) to [Šōmrôn](../../strongs/h/h8111.md).

<a name="2chronicles_28_16"></a>2Chronicles 28:16

At that [ʿēṯ](../../strongs/h/h6256.md) did [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) [shalach](../../strongs/h/h7971.md) unto the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) to [ʿāzar](../../strongs/h/h5826.md) him.

<a name="2chronicles_28_17"></a>2Chronicles 28:17

For again the ['Ăḏōmî](../../strongs/h/h130.md) had [bow'](../../strongs/h/h935.md) and [nakah](../../strongs/h/h5221.md) [Yehuwdah](../../strongs/h/h3063.md), and [šāḇâ](../../strongs/h/h7617.md) [šᵊḇî](../../strongs/h/h7628.md).

<a name="2chronicles_28_18"></a>2Chronicles 28:18

The [Pᵊlištî](../../strongs/h/h6430.md) also had [pāšaṭ](../../strongs/h/h6584.md) the [ʿîr](../../strongs/h/h5892.md) of the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and of the [neḡeḇ](../../strongs/h/h5045.md) of [Yehuwdah](../../strongs/h/h3063.md), and had [lāḵaḏ](../../strongs/h/h3920.md) [Bêṯ Šemeš](../../strongs/h/h1053.md), and ['Ayyālôn](../../strongs/h/h357.md), and [Gᵊḏērôṯ](../../strongs/h/h1450.md), and [Śôḵô](../../strongs/h/h7755.md) with the [bath](../../strongs/h/h1323.md) thereof, and [Timnâ](../../strongs/h/h8553.md) with the [bath](../../strongs/h/h1323.md) thereof, [Gimzô](../../strongs/h/h1579.md) also and the [bath](../../strongs/h/h1323.md) thereof: and they [yashab](../../strongs/h/h3427.md) there.

<a name="2chronicles_28_19"></a>2Chronicles 28:19

For [Yĕhovah](../../strongs/h/h3068.md) [kānaʿ](../../strongs/h/h3665.md) [Yehuwdah](../../strongs/h/h3063.md) [kānaʿ](../../strongs/h/h3665.md) because of ['Āḥāz](../../strongs/h/h271.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md); for he [pāraʿ](../../strongs/h/h6544.md) [Yehuwdah](../../strongs/h/h3063.md) [pāraʿ](../../strongs/h/h6544.md), and [māʿal](../../strongs/h/h4603.md) [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_28_20"></a>2Chronicles 28:20

And [Tiḡlaṯ Pil'Eser](../../strongs/h/h8407.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [bow'](../../strongs/h/h935.md) unto him, and [ṣûr](../../strongs/h/h6696.md) him, but [ḥāzaq](../../strongs/h/h2388.md) him not.

<a name="2chronicles_28_21"></a>2Chronicles 28:21

For ['Āḥāz](../../strongs/h/h271.md) took away a [chalaq](../../strongs/h/h2505.md) out of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and out of the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md), and of the [śar](../../strongs/h/h8269.md), and [nathan](../../strongs/h/h5414.md) it unto the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md): but he [ʿezrâ](../../strongs/h/h5833.md) him not.

<a name="2chronicles_28_22"></a>2Chronicles 28:22

And in the [ʿēṯ](../../strongs/h/h6256.md) of his [tsarar](../../strongs/h/h6887.md) did he [māʿal](../../strongs/h/h4603.md) yet more against [Yĕhovah](../../strongs/h/h3068.md): this is that [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md).

<a name="2chronicles_28_23"></a>2Chronicles 28:23

For he [zabach](../../strongs/h/h2076.md) unto the ['Elohiym](../../strongs/h/h430.md) of [Dammeśeq](../../strongs/h/h1834.md), which [nakah](../../strongs/h/h5221.md) him: and he ['āmar](../../strongs/h/h559.md), Because the ['Elohiym](../../strongs/h/h430.md) of the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) [ʿāzar](../../strongs/h/h5826.md) them, therefore will I [zabach](../../strongs/h/h2076.md) to them, that they may [ʿāzar](../../strongs/h/h5826.md) me. But they were the [kashal](../../strongs/h/h3782.md) of him, and of all [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_28_24"></a>2Chronicles 28:24

And ['Āḥāz](../../strongs/h/h271.md) ['āsap̄](../../strongs/h/h622.md) the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and [qāṣaṣ](../../strongs/h/h7112.md) the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and he ['asah](../../strongs/h/h6213.md) him [mizbeach](../../strongs/h/h4196.md) in every [pinnâ](../../strongs/h/h6438.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_28_25"></a>2Chronicles 28:25

And in every several [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) he ['asah](../../strongs/h/h6213.md) [bāmâ](../../strongs/h/h1116.md) to [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [kāʿas](../../strongs/h/h3707.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md).

<a name="2chronicles_28_26"></a>2Chronicles 28:26

Now the [yeṯer](../../strongs/h/h3499.md) of his [dabar](../../strongs/h/h1697.md) and of all his [derek](../../strongs/h/h1870.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_28_27"></a>2Chronicles 28:27

And ['Āḥāz](../../strongs/h/h271.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md), even in [Yĕruwshalaim](../../strongs/h/h3389.md): but they [bow'](../../strongs/h/h935.md) him not into the [qeber](../../strongs/h/h6913.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md): and [Yᵊḥizqîyâ](../../strongs/h/h3169.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 27](2chronicles_27.md) - [2Chronicles 29](2chronicles_29.md)