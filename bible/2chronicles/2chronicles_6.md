# [2Chronicles 6](https://www.blueletterbible.org/kjv/2chronicles/6)

<a name="2chronicles_6_1"></a>2Chronicles 6:1

Then ['āmar](../../strongs/h/h559.md) [Šᵊlōmô](../../strongs/h/h8010.md), [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md) that he would [shakan](../../strongs/h/h7931.md) in the ['araphel](../../strongs/h/h6205.md).

<a name="2chronicles_6_2"></a>2Chronicles 6:2

But I have [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) of [zᵊḇûl](../../strongs/h/h2073.md) for thee, and a [māḵôn](../../strongs/h/h4349.md) for thy [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md).

<a name="2chronicles_6_3"></a>2Chronicles 6:3

And the [melek](../../strongs/h/h4428.md) [cabab](../../strongs/h/h5437.md) his [paniym](../../strongs/h/h6440.md), and [barak](../../strongs/h/h1288.md) the whole [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md): and all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md) ['amad](../../strongs/h/h5975.md).

<a name="2chronicles_6_4"></a>2Chronicles 6:4

And he ['āmar](../../strongs/h/h559.md), [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), who hath with his [yad](../../strongs/h/h3027.md) [mālā'](../../strongs/h/h4390.md) that which he [dabar](../../strongs/h/h1696.md) with his [peh](../../strongs/h/h6310.md) to my ['ab](../../strongs/h/h1.md) [Dāviḏ](../../strongs/h/h1732.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_6_5"></a>2Chronicles 6:5

Since the [yowm](../../strongs/h/h3117.md) that I [yāṣā'](../../strongs/h/h3318.md) my ['am](../../strongs/h/h5971.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) I [bāḥar](../../strongs/h/h977.md) no [ʿîr](../../strongs/h/h5892.md) among all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) in, that my [shem](../../strongs/h/h8034.md) might be there; neither [bāḥar](../../strongs/h/h977.md) I any ['iysh](../../strongs/h/h376.md) to be a [nāḡîḏ](../../strongs/h/h5057.md) over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md):

<a name="2chronicles_6_6"></a>2Chronicles 6:6

But I have [bāḥar](../../strongs/h/h977.md) [Yĕruwshalaim](../../strongs/h/h3389.md), that my [shem](../../strongs/h/h8034.md) might be there; and have [bāḥar](../../strongs/h/h977.md) [Dāviḏ](../../strongs/h/h1732.md) to be over my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_6_7"></a>2Chronicles 6:7

Now it was in the [lebab](../../strongs/h/h3824.md) of [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_6_8"></a>2Chronicles 6:8

But [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), Forasmuch as it was in thine [lebab](../../strongs/h/h3824.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for my [shem](../../strongs/h/h8034.md), thou didst [ṭôḇ](../../strongs/h/h2895.md) in that it was in thine [lebab](../../strongs/h/h3824.md):

<a name="2chronicles_6_9"></a>2Chronicles 6:9

Notwithstanding thou shalt not [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md); but thy [ben](../../strongs/h/h1121.md) which shall [yāṣā'](../../strongs/h/h3318.md) out of thy [ḥālāṣ](../../strongs/h/h2504.md), he shall [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) for my [shem](../../strongs/h/h8034.md).

<a name="2chronicles_6_10"></a>2Chronicles 6:10

[Yĕhovah](../../strongs/h/h3068.md) therefore hath [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md) that he hath [dabar](../../strongs/h/h1696.md): for I am [quwm](../../strongs/h/h6965.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and am [yashab](../../strongs/h/h3427.md) on the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md), as [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), and have [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) for the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_6_11"></a>2Chronicles 6:11

And in it have I [śûm](../../strongs/h/h7760.md) the ['ārôn](../../strongs/h/h727.md), wherein is the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md), that he [karath](../../strongs/h/h3772.md) with the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_6_12"></a>2Chronicles 6:12

And he ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) in the presence of all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), and [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md):

<a name="2chronicles_6_13"></a>2Chronicles 6:13

For [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md) a [nᵊḥšeṯ](../../strongs/h/h5178.md) [kîyôr](../../strongs/h/h3595.md), of five ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and five ['ammâ](../../strongs/h/h520.md) [rōḥaḇ](../../strongs/h/h7341.md), and three ['ammâ](../../strongs/h/h520.md) [qômâ](../../strongs/h/h6967.md), and had [nathan](../../strongs/h/h5414.md) it in the [tavek](../../strongs/h/h8432.md) of the [ʿăzārâ](../../strongs/h/h5835.md): and upon it he ['amad](../../strongs/h/h5975.md), and [barak](../../strongs/h/h1288.md) upon his [bereḵ](../../strongs/h/h1290.md) before all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), and [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md) toward [shamayim](../../strongs/h/h8064.md),

<a name="2chronicles_6_14"></a>2Chronicles 6:14

And ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), there is no ['Elohiym](../../strongs/h/h430.md) like thee in the [shamayim](../../strongs/h/h8064.md), nor in the ['erets](../../strongs/h/h776.md); which [shamar](../../strongs/h/h8104.md) [bĕriyth](../../strongs/h/h1285.md), and shewest [checed](../../strongs/h/h2617.md) unto thy ['ebed](../../strongs/h/h5650.md), that [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) thee with all their [leb](../../strongs/h/h3820.md):

<a name="2chronicles_6_15"></a>2Chronicles 6:15

Thou which hast [shamar](../../strongs/h/h8104.md) with thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) that which thou hast [dabar](../../strongs/h/h1696.md) him; and [dabar](../../strongs/h/h1696.md) with thy [peh](../../strongs/h/h6310.md), and hast [mālā'](../../strongs/h/h4390.md) it with thine [yad](../../strongs/h/h3027.md), as it is this [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_6_16"></a>2Chronicles 6:16

Now therefore, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [shamar](../../strongs/h/h8104.md) with thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) that which thou hast [dabar](../../strongs/h/h1696.md) him, ['āmar](../../strongs/h/h559.md), There shall not [karath](../../strongs/h/h3772.md) thee an ['iysh](../../strongs/h/h376.md) in my [paniym](../../strongs/h/h6440.md) to [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Yisra'el](../../strongs/h/h3478.md); yet so that thy [ben](../../strongs/h/h1121.md) take [shamar](../../strongs/h/h8104.md) to their [derek](../../strongs/h/h1870.md) to [yālaḵ](../../strongs/h/h3212.md) in my [towrah](../../strongs/h/h8451.md), as thou hast [halak](../../strongs/h/h1980.md) [paniym](../../strongs/h/h6440.md) me.

<a name="2chronicles_6_17"></a>2Chronicles 6:17

Now then, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), let thy [dabar](../../strongs/h/h1697.md) be ['aman](../../strongs/h/h539.md), which thou hast [dabar](../../strongs/h/h1696.md) unto thy ['ebed](../../strongs/h/h5650.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2chronicles_6_18"></a>2Chronicles 6:18

But will ['Elohiym](../../strongs/h/h430.md) in very ['umnām](../../strongs/h/h552.md) [yashab](../../strongs/h/h3427.md) with ['āḏām](../../strongs/h/h120.md) on the ['erets](../../strongs/h/h776.md)? behold, [shamayim](../../strongs/h/h8064.md) and the [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md) cannot [kûl](../../strongs/h/h3557.md) thee; how much less this [bayith](../../strongs/h/h1004.md) which I have [bānâ](../../strongs/h/h1129.md)!

<a name="2chronicles_6_19"></a>2Chronicles 6:19

[panah](../../strongs/h/h6437.md) therefore to the [tĕphillah](../../strongs/h/h8605.md) of thy ['ebed](../../strongs/h/h5650.md), and to his [tĕchinnah](../../strongs/h/h8467.md), [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), to [shama'](../../strongs/h/h8085.md) unto the [rinnah](../../strongs/h/h7440.md) and the [tĕphillah](../../strongs/h/h8605.md) which thy ['ebed](../../strongs/h/h5650.md) [palal](../../strongs/h/h6419.md) [paniym](../../strongs/h/h6440.md) thee:

<a name="2chronicles_6_20"></a>2Chronicles 6:20

That thine ['ayin](../../strongs/h/h5869.md) may be [pāṯaḥ](../../strongs/h/h6605.md) upon this [bayith](../../strongs/h/h1004.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), upon the [maqowm](../../strongs/h/h4725.md) whereof thou hast ['āmar](../../strongs/h/h559.md) that thou wouldest [śûm](../../strongs/h/h7760.md) thy [shem](../../strongs/h/h8034.md) there; to [shama'](../../strongs/h/h8085.md) unto the [tĕphillah](../../strongs/h/h8605.md) which thy ['ebed](../../strongs/h/h5650.md) [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md).

<a name="2chronicles_6_21"></a>2Chronicles 6:21

[shama'](../../strongs/h/h8085.md) therefore unto the [taḥănûn](../../strongs/h/h8469.md) of thy ['ebed](../../strongs/h/h5650.md), and of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), which they shall [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md): [shama'](../../strongs/h/h8085.md) thou from thy [yashab](../../strongs/h/h3427.md) [maqowm](../../strongs/h/h4725.md), even from [shamayim](../../strongs/h/h8064.md); and when thou [shama'](../../strongs/h/h8085.md), [sālaḥ](../../strongs/h/h5545.md).

<a name="2chronicles_6_22"></a>2Chronicles 6:22

If an ['iysh](../../strongs/h/h376.md) [chata'](../../strongs/h/h2398.md) against his [rea'](../../strongs/h/h7453.md), and an ['alah](../../strongs/h/h423.md) be [nasa'](../../strongs/h/h5375.md) upon him to make him ['ālâ](../../strongs/h/h422.md), and the ['alah](../../strongs/h/h423.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thine [mizbeach](../../strongs/h/h4196.md) in this [bayith](../../strongs/h/h1004.md);

<a name="2chronicles_6_23"></a>2Chronicles 6:23

Then [shama'](../../strongs/h/h8085.md) thou from [shamayim](../../strongs/h/h8064.md), and ['asah](../../strongs/h/h6213.md), and [shaphat](../../strongs/h/h8199.md) thy ['ebed](../../strongs/h/h5650.md), by [shuwb](../../strongs/h/h7725.md) the [rasha'](../../strongs/h/h7563.md), by [nathan](../../strongs/h/h5414.md) his [derek](../../strongs/h/h1870.md) upon his own [ro'sh](../../strongs/h/h7218.md); and by [ṣāḏaq](../../strongs/h/h6663.md) the [tsaddiyq](../../strongs/h/h6662.md), by [nathan](../../strongs/h/h5414.md) him according to his [tsedaqah](../../strongs/h/h6666.md).

<a name="2chronicles_6_24"></a>2Chronicles 6:24

And if thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) be put to the [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md), because they have [chata'](../../strongs/h/h2398.md) against thee; and shall [shuwb](../../strongs/h/h7725.md) and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md), and [palal](../../strongs/h/h6419.md) and make [chanan](../../strongs/h/h2603.md) [paniym](../../strongs/h/h6440.md) thee in this [bayith](../../strongs/h/h1004.md);

<a name="2chronicles_6_25"></a>2Chronicles 6:25

Then [shama'](../../strongs/h/h8085.md) thou from the [shamayim](../../strongs/h/h8064.md), and [sālaḥ](../../strongs/h/h5545.md) the [chatta'ath](../../strongs/h/h2403.md) of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and [shuwb](../../strongs/h/h7725.md) them unto the ['ăḏāmâ](../../strongs/h/h127.md) which thou [nathan](../../strongs/h/h5414.md) to them and to their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_6_26"></a>2Chronicles 6:26

When the [shamayim](../../strongs/h/h8064.md) is [ʿāṣar](../../strongs/h/h6113.md), and there is no [māṭār](../../strongs/h/h4306.md), because they have [chata'](../../strongs/h/h2398.md) against thee; yet if they [palal](../../strongs/h/h6419.md) toward this [maqowm](../../strongs/h/h4725.md), and [yadah](../../strongs/h/h3034.md) thy [shem](../../strongs/h/h8034.md), and [shuwb](../../strongs/h/h7725.md) from their [chatta'ath](../../strongs/h/h2403.md), when thou dost [ʿānâ](../../strongs/h/h6031.md) them;

<a name="2chronicles_6_27"></a>2Chronicles 6:27

Then [shama'](../../strongs/h/h8085.md) thou from [shamayim](../../strongs/h/h8064.md), and [sālaḥ](../../strongs/h/h5545.md) the [chatta'ath](../../strongs/h/h2403.md) of thy ['ebed](../../strongs/h/h5650.md), and of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), when thou hast [yārâ](../../strongs/h/h3384.md) them the [towb](../../strongs/h/h2896.md) [derek](../../strongs/h/h1870.md), wherein they should [yālaḵ](../../strongs/h/h3212.md); and [nathan](../../strongs/h/h5414.md) [māṭār](../../strongs/h/h4306.md) upon thy ['erets](../../strongs/h/h776.md), which thou hast [nathan](../../strongs/h/h5414.md) unto thy ['am](../../strongs/h/h5971.md) for a [nachalah](../../strongs/h/h5159.md).

<a name="2chronicles_6_28"></a>2Chronicles 6:28

If there be [rāʿāḇ](../../strongs/h/h7458.md) in the ['erets](../../strongs/h/h776.md), if there be [deḇer](../../strongs/h/h1698.md), if there be [šᵊḏēp̄â](../../strongs/h/h7711.md), or [yērāqôn](../../strongs/h/h3420.md), ['arbê](../../strongs/h/h697.md), or [ḥāsîl](../../strongs/h/h2625.md); if their ['oyeb](../../strongs/h/h341.md) [tsarar](../../strongs/h/h6887.md) them in the [sha'ar](../../strongs/h/h8179.md) of their ['erets](../../strongs/h/h776.md); whatsoever [neḡaʿ](../../strongs/h/h5061.md) or whatsoever [maḥălê](../../strongs/h/h4245.md) there be:

<a name="2chronicles_6_29"></a>2Chronicles 6:29

Then what [tĕphillah](../../strongs/h/h8605.md) or what [tĕchinnah](../../strongs/h/h8467.md) soever shall be made of any ['āḏām](../../strongs/h/h120.md), or of all thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), when every ['iysh](../../strongs/h/h376.md) shall [yada'](../../strongs/h/h3045.md) his own [neḡaʿ](../../strongs/h/h5061.md) and his own [maḵ'ōḇ](../../strongs/h/h4341.md), and shall spread [pāraś](../../strongs/h/h6566.md) his [kaph](../../strongs/h/h3709.md) in this [bayith](../../strongs/h/h1004.md):

<a name="2chronicles_6_30"></a>2Chronicles 6:30

Then [shama'](../../strongs/h/h8085.md) thou from [shamayim](../../strongs/h/h8064.md) thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), and [sālaḥ](../../strongs/h/h5545.md), and [nathan](../../strongs/h/h5414.md) unto every ['iysh](../../strongs/h/h376.md) according unto all his [derek](../../strongs/h/h1870.md), whose [lebab](../../strongs/h/h3824.md) thou [yada'](../../strongs/h/h3045.md); (for thou only [yada'](../../strongs/h/h3045.md) the [lebab](../../strongs/h/h3824.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md):)

<a name="2chronicles_6_31"></a>2Chronicles 6:31

That they may [yare'](../../strongs/h/h3372.md) thee, to [yālaḵ](../../strongs/h/h3212.md) in thy [derek](../../strongs/h/h1870.md), so [yowm](../../strongs/h/h3117.md) they [chay](../../strongs/h/h2416.md) [paniym](../../strongs/h/h6440.md) in the ['ăḏāmâ](../../strongs/h/h127.md) which thou [nathan](../../strongs/h/h5414.md) unto our ['ab](../../strongs/h/h1.md).

<a name="2chronicles_6_32"></a>2Chronicles 6:32

Moreover concerning the [nāḵrî](../../strongs/h/h5237.md), which is not of thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), but is [bow'](../../strongs/h/h935.md) from a [rachowq](../../strongs/h/h7350.md) ['erets](../../strongs/h/h776.md) for thy [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md) sake, and thy [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and thy [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md); if they [bow'](../../strongs/h/h935.md) and [palal](../../strongs/h/h6419.md) in this [bayith](../../strongs/h/h1004.md);

<a name="2chronicles_6_33"></a>2Chronicles 6:33

Then [shama'](../../strongs/h/h8085.md) thou from the [shamayim](../../strongs/h/h8064.md), even from thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), and ['asah](../../strongs/h/h6213.md) according to all that the [nāḵrî](../../strongs/h/h5237.md) [qara'](../../strongs/h/h7121.md) to thee for; that all ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) may [yada'](../../strongs/h/h3045.md) thy [shem](../../strongs/h/h8034.md), and [yare'](../../strongs/h/h3372.md) thee, as doth thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md), and may [yada'](../../strongs/h/h3045.md) that this [bayith](../../strongs/h/h1004.md) which I have [bānâ](../../strongs/h/h1129.md) is [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md).

<a name="2chronicles_6_34"></a>2Chronicles 6:34

If thy ['am](../../strongs/h/h5971.md) [yāṣā'](../../strongs/h/h3318.md) to [milḥāmâ](../../strongs/h/h4421.md) against their ['oyeb](../../strongs/h/h341.md) by the [derek](../../strongs/h/h1870.md) that thou shalt [shalach](../../strongs/h/h7971.md) them, and they [palal](../../strongs/h/h6419.md) unto thee [derek](../../strongs/h/h1870.md) this [ʿîr](../../strongs/h/h5892.md) which thou hast [bāḥar](../../strongs/h/h977.md), and the [bayith](../../strongs/h/h1004.md) which I have [bānâ](../../strongs/h/h1129.md) for thy [shem](../../strongs/h/h8034.md);

<a name="2chronicles_6_35"></a>2Chronicles 6:35

Then [shama'](../../strongs/h/h8085.md) thou from the [shamayim](../../strongs/h/h8064.md) their [tĕphillah](../../strongs/h/h8605.md) and their [tĕchinnah](../../strongs/h/h8467.md), and ['asah](../../strongs/h/h6213.md) their [mishpat](../../strongs/h/h4941.md).

<a name="2chronicles_6_36"></a>2Chronicles 6:36

If they [chata'](../../strongs/h/h2398.md) against thee, (for there is no ['āḏām](../../strongs/h/h120.md) which [chata'](../../strongs/h/h2398.md) not,) and thou be ['anaph](../../strongs/h/h599.md) with them, and [nathan](../../strongs/h/h5414.md) them over [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), and they carry them [šāḇâ](../../strongs/h/h7617.md) [šāḇâ](../../strongs/h/h7617.md) unto an ['erets](../../strongs/h/h776.md) far [rachowq](../../strongs/h/h7350.md) or [qarowb](../../strongs/h/h7138.md);

<a name="2chronicles_6_37"></a>2Chronicles 6:37

Yet if they [shuwb](../../strongs/h/h7725.md) [lebab](../../strongs/h/h3824.md) themselves in the ['erets](../../strongs/h/h776.md) whither they are [šāḇâ](../../strongs/h/h7617.md), and [shuwb](../../strongs/h/h7725.md) and [chanan](../../strongs/h/h2603.md) unto thee in the ['erets](../../strongs/h/h776.md) of their [šᵊḇî](../../strongs/h/h7628.md), ['āmar](../../strongs/h/h559.md), We have [chata'](../../strongs/h/h2398.md), we have done [ʿāvâ](../../strongs/h/h5753.md), and have dealt [rāšaʿ](../../strongs/h/h7561.md);

<a name="2chronicles_6_38"></a>2Chronicles 6:38

If they [shuwb](../../strongs/h/h7725.md) to thee with all their [leb](../../strongs/h/h3820.md) and with all their [nephesh](../../strongs/h/h5315.md) in the ['erets](../../strongs/h/h776.md) of their [šᵊḇî](../../strongs/h/h7628.md), whither they have [šāḇâ](../../strongs/h/h7617.md) them, and [palal](../../strongs/h/h6419.md) [derek](../../strongs/h/h1870.md) their ['erets](../../strongs/h/h776.md), which thou [nathan](../../strongs/h/h5414.md) unto their ['ab](../../strongs/h/h1.md), and toward the [ʿîr](../../strongs/h/h5892.md) which thou hast [bāḥar](../../strongs/h/h977.md), and toward the [bayith](../../strongs/h/h1004.md) which I have [bānâ](../../strongs/h/h1129.md) for thy [shem](../../strongs/h/h8034.md):

<a name="2chronicles_6_39"></a>2Chronicles 6:39

Then [shama'](../../strongs/h/h8085.md) thou from the [shamayim](../../strongs/h/h8064.md), even from thy [yashab](../../strongs/h/h3427.md) [māḵôn](../../strongs/h/h4349.md), their [tĕphillah](../../strongs/h/h8605.md) and their [tĕchinnah](../../strongs/h/h8467.md), and ['asah](../../strongs/h/h6213.md) their [mishpat](../../strongs/h/h4941.md), and [sālaḥ](../../strongs/h/h5545.md) thy ['am](../../strongs/h/h5971.md) which have [chata'](../../strongs/h/h2398.md) against thee.

<a name="2chronicles_6_40"></a>2Chronicles 6:40

Now, my ['Elohiym](../../strongs/h/h430.md), let thine ['ayin](../../strongs/h/h5869.md) be [pāṯaḥ](../../strongs/h/h6605.md), and let thine ['ozen](../../strongs/h/h241.md) be [qaššāḇ](../../strongs/h/h7183.md) unto the [tĕphillah](../../strongs/h/h8605.md) that is made in this [maqowm](../../strongs/h/h4725.md).

<a name="2chronicles_6_41"></a>2Chronicles 6:41

Now therefore [quwm](../../strongs/h/h6965.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), into thy [nûaḥ](../../strongs/h/h5118.md), thou, and the ['ārôn](../../strongs/h/h727.md) of thy ['oz](../../strongs/h/h5797.md): let thy [kōhēn](../../strongs/h/h3548.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), be [labash](../../strongs/h/h3847.md) with [tᵊšûʿâ](../../strongs/h/h8668.md), and let thy [chaciyd](../../strongs/h/h2623.md) [samach](../../strongs/h/h8055.md) in [towb](../../strongs/h/h2896.md).

<a name="2chronicles_6_42"></a>2Chronicles 6:42

[Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), [shuwb](../../strongs/h/h7725.md) not the [paniym](../../strongs/h/h6440.md) of thine [mashiyach](../../strongs/h/h4899.md): [zakar](../../strongs/h/h2142.md) the [checed](../../strongs/h/h2617.md) of [Dāviḏ](../../strongs/h/h1732.md) thy ['ebed](../../strongs/h/h5650.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 5](2chronicles_5.md) - [2Chronicles 7](2chronicles_7.md)