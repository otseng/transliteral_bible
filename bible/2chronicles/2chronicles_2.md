# [2Chronicles 2](https://www.blueletterbible.org/kjv/2chronicles/2)

<a name="2chronicles_2_1"></a>2Chronicles 2:1

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md) to [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), and a [bayith](../../strongs/h/h1004.md) for his [malkuwth](../../strongs/h/h4438.md).

<a name="2chronicles_2_2"></a>2Chronicles 2:2

And [Šᵊlōmô](../../strongs/h/h8010.md) [sāp̄ar](../../strongs/h/h5608.md) threescore and ten thousand ['iysh](../../strongs/h/h376.md) to bear [sabāl](../../strongs/h/h5449.md), and fourscore thousand ['iysh](../../strongs/h/h376.md) to [ḥāṣaḇ](../../strongs/h/h2672.md) in the [har](../../strongs/h/h2022.md), and three thousand and six hundred to [nāṣaḥ](../../strongs/h/h5329.md) them.

<a name="2chronicles_2_3"></a>2Chronicles 2:3

And [Šᵊlōmô](../../strongs/h/h8010.md) [shalach](../../strongs/h/h7971.md) to [Ḥûrām](../../strongs/h/h2361.md) the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md), ['āmar](../../strongs/h/h559.md), As thou didst ['asah](../../strongs/h/h6213.md) with [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and didst [shalach](../../strongs/h/h7971.md) him ['erez](../../strongs/h/h730.md) to [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md) to [yashab](../../strongs/h/h3427.md) therein.

<a name="2chronicles_2_4"></a>2Chronicles 2:4

Behold, I [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) to the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md), to [qadash](../../strongs/h/h6942.md) it to him, and to [qāṭar](../../strongs/h/h6999.md) [paniym](../../strongs/h/h6440.md) him [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md), and for the [tāmîḏ](../../strongs/h/h8548.md) [maʿăreḵeṯ](../../strongs/h/h4635.md), and for the [ʿōlâ](../../strongs/h/h5930.md) [boqer](../../strongs/h/h1242.md) and ['ereb](../../strongs/h/h6153.md), on the [shabbath](../../strongs/h/h7676.md), and on the [ḥōḏeš](../../strongs/h/h2320.md), and on the [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md). This is ['owlam](../../strongs/h/h5769.md) to [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_2_5"></a>2Chronicles 2:5

And the [bayith](../../strongs/h/h1004.md) which I [bānâ](../../strongs/h/h1129.md) is [gadowl](../../strongs/h/h1419.md): for [gadowl](../../strongs/h/h1419.md) is our ['Elohiym](../../strongs/h/h430.md) above all ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_2_6"></a>2Chronicles 2:6

But who is [ʿāṣar](../../strongs/h/h6113.md) [koach](../../strongs/h/h3581.md) to [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md), the [shamayim](../../strongs/h/h8064.md) and [shamayim](../../strongs/h/h8064.md) of [shamayim](../../strongs/h/h8064.md) cannot [kûl](../../strongs/h/h3557.md) him? who am I then, that I should [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md), ['im](../../strongs/h/h518.md) to [qāṭar](../../strongs/h/h6999.md) [paniym](../../strongs/h/h6440.md) him?

<a name="2chronicles_2_7"></a>2Chronicles 2:7

[shalach](../../strongs/h/h7971.md) me now therefore an ['iysh](../../strongs/h/h376.md) [ḥāḵām](../../strongs/h/h2450.md) to ['asah](../../strongs/h/h6213.md) in [zāhāḇ](../../strongs/h/h2091.md), and in [keceph](../../strongs/h/h3701.md), and in [nᵊḥšeṯ](../../strongs/h/h5178.md), and in [barzel](../../strongs/h/h1270.md), and in ['arḡᵊvān](../../strongs/h/h710.md), and [karmîl](../../strongs/h/h3758.md), and [tᵊḵēleṯ](../../strongs/h/h8504.md), and that can [yada'](../../strongs/h/h3045.md) to [pāṯaḥ](../../strongs/h/h6605.md) [pitûaḥ](../../strongs/h/h6603.md) with the [ḥāḵām](../../strongs/h/h2450.md) that are with me in [Yehuwdah](../../strongs/h/h3063.md) and in [Yĕruwshalaim](../../strongs/h/h3389.md), whom [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) did [kuwn](../../strongs/h/h3559.md).

<a name="2chronicles_2_8"></a>2Chronicles 2:8

[shalach](../../strongs/h/h7971.md) me also ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), fir [bᵊrôš](../../strongs/h/h1265.md), and ['algûmmîm](../../strongs/h/h418.md), out of [Lᵊḇānôn](../../strongs/h/h3844.md): for I [yada'](../../strongs/h/h3045.md) that thy ['ebed](../../strongs/h/h5650.md) can [yada'](../../strongs/h/h3045.md) to [karath](../../strongs/h/h3772.md) ['ets](../../strongs/h/h6086.md) in [Lᵊḇānôn](../../strongs/h/h3844.md); and, behold, my ['ebed](../../strongs/h/h5650.md) shall be with thy ['ebed](../../strongs/h/h5650.md),

<a name="2chronicles_2_9"></a>2Chronicles 2:9

Even to [kuwn](../../strongs/h/h3559.md) me ['ets](../../strongs/h/h6086.md) in [rōḇ](../../strongs/h/h7230.md): for the [bayith](../../strongs/h/h1004.md) which I am about to [bānâ](../../strongs/h/h1129.md) shall be [pala'](../../strongs/h/h6381.md) [gadowl](../../strongs/h/h1419.md).

<a name="2chronicles_2_10"></a>2Chronicles 2:10

And, behold, I will [nathan](../../strongs/h/h5414.md) to thy ['ebed](../../strongs/h/h5650.md), the [ḥāṭaḇ](../../strongs/h/h2404.md) that [karath](../../strongs/h/h3772.md) ['ets](../../strongs/h/h6086.md), twenty thousand [kōr](../../strongs/h/h3734.md) of [makâ](../../strongs/h/h4347.md) [ḥiṭṭâ](../../strongs/h/h2406.md), and twenty thousand [kōr](../../strongs/h/h3734.md) of [śᵊʿōrâ](../../strongs/h/h8184.md), and twenty thousand [baṯ](../../strongs/h/h1324.md) of [yayin](../../strongs/h/h3196.md), and twenty thousand [baṯ](../../strongs/h/h1324.md) of [šemen](../../strongs/h/h8081.md).

<a name="2chronicles_2_11"></a>2Chronicles 2:11

Then [Ḥûrām](../../strongs/h/h2361.md) the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md) ['āmar](../../strongs/h/h559.md) in [kᵊṯāḇ](../../strongs/h/h3791.md), which he [shalach](../../strongs/h/h7971.md) to [Šᵊlōmô](../../strongs/h/h8010.md), Because [Yĕhovah](../../strongs/h/h3068.md) hath ['ahăḇâ](../../strongs/h/h160.md) his ['am](../../strongs/h/h5971.md), he hath [nathan](../../strongs/h/h5414.md) thee [melek](../../strongs/h/h4428.md) over them.

<a name="2chronicles_2_12"></a>2Chronicles 2:12

[Ḥûrām](../../strongs/h/h2361.md) ['āmar](../../strongs/h/h559.md) moreover, [barak](../../strongs/h/h1288.md) be [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), that ['asah](../../strongs/h/h6213.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md), who hath [nathan](../../strongs/h/h5414.md) to [Dāviḏ](../../strongs/h/h1732.md) the [melek](../../strongs/h/h4428.md) a [ḥāḵām](../../strongs/h/h2450.md) [ben](../../strongs/h/h1121.md), [yada'](../../strongs/h/h3045.md) with [śēḵel](../../strongs/h/h7922.md) and [bînâ](../../strongs/h/h998.md), that might [bānâ](../../strongs/h/h1129.md) a [bayith](../../strongs/h/h1004.md) for [Yĕhovah](../../strongs/h/h3068.md), and a [bayith](../../strongs/h/h1004.md) for his [malkuwth](../../strongs/h/h4438.md).

<a name="2chronicles_2_13"></a>2Chronicles 2:13

And now I have [shalach](../../strongs/h/h7971.md) a [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md), [yada'](../../strongs/h/h3045.md) with [bînâ](../../strongs/h/h998.md), of [Ḥûrām](../../strongs/h/h2361.md) my ['ab](../../strongs/h/h1.md),

<a name="2chronicles_2_14"></a>2Chronicles 2:14

The [ben](../../strongs/h/h1121.md) of an ['ishshah](../../strongs/h/h802.md) of the [bath](../../strongs/h/h1323.md) of [Dān](../../strongs/h/h1835.md), and his ['ab](../../strongs/h/h1.md) was an ['iysh](../../strongs/h/h376.md) of [ṣōrî](../../strongs/h/h6876.md), [yada'](../../strongs/h/h3045.md) to ['asah](../../strongs/h/h6213.md) in [zāhāḇ](../../strongs/h/h2091.md), and in [keceph](../../strongs/h/h3701.md), in [nᵊḥšeṯ](../../strongs/h/h5178.md), in [barzel](../../strongs/h/h1270.md), in ['eben](../../strongs/h/h68.md), and in ['ets](../../strongs/h/h6086.md), in ['argāmān](../../strongs/h/h713.md), in [tᵊḵēleṯ](../../strongs/h/h8504.md), and in [bûṣ](../../strongs/h/h948.md), and in [karmîl](../../strongs/h/h3758.md); also to [pāṯaḥ](../../strongs/h/h6605.md) any manner of [pitûaḥ](../../strongs/h/h6603.md), and to find [chashab](../../strongs/h/h2803.md) every [maḥăšāḇâ](../../strongs/h/h4284.md) which shall be [nathan](../../strongs/h/h5414.md) to him, with thy [ḥāḵām](../../strongs/h/h2450.md), and with the [ḥāḵām](../../strongs/h/h2450.md) of my ['adown](../../strongs/h/h113.md) [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md).

<a name="2chronicles_2_15"></a>2Chronicles 2:15

Now therefore the [ḥiṭṭâ](../../strongs/h/h2406.md), and the [śᵊʿōrâ](../../strongs/h/h8184.md), the [šemen](../../strongs/h/h8081.md), and the [yayin](../../strongs/h/h3196.md), which my ['adown](../../strongs/h/h113.md) hath ['āmar](../../strongs/h/h559.md) of, let him [shalach](../../strongs/h/h7971.md) unto his ['ebed](../../strongs/h/h5650.md):

<a name="2chronicles_2_16"></a>2Chronicles 2:16

And we will [karath](../../strongs/h/h3772.md) ['ets](../../strongs/h/h6086.md) out of [Lᵊḇānôn](../../strongs/h/h3844.md), as much as thou shalt [ṣōreḵ](../../strongs/h/h6878.md): and we will [bow'](../../strongs/h/h935.md) it to thee in [rap̄sōḏôṯ](../../strongs/h/h7513.md) by [yam](../../strongs/h/h3220.md) to [Yāp̄Vô](../../strongs/h/h3305.md); and thou shalt [ʿālâ](../../strongs/h/h5927.md) it to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_2_17"></a>2Chronicles 2:17

And [Šᵊlōmô](../../strongs/h/h8010.md) [sāp̄ar](../../strongs/h/h5608.md) all the ['enowsh](../../strongs/h/h582.md) [ger](../../strongs/h/h1616.md) that were in the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), ['aḥar](../../strongs/h/h310.md) the [sᵊp̄ār](../../strongs/h/h5610.md) wherewith [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md) had [sāp̄ar](../../strongs/h/h5608.md) them; and they were [māṣā'](../../strongs/h/h4672.md) an hundred and fifty thousand and three thousand and six hundred.

<a name="2chronicles_2_18"></a>2Chronicles 2:18

And he ['asah](../../strongs/h/h6213.md) threescore and ten thousand of them to be bearers of [sabāl](../../strongs/h/h5449.md), and fourscore thousand to be [ḥāṣaḇ](../../strongs/h/h2672.md) in the [har](../../strongs/h/h2022.md), and three thousand and six hundred [nāṣaḥ](../../strongs/h/h5329.md) to ['abad](../../strongs/h/h5647.md) the ['am](../../strongs/h/h5971.md) an ['abad](../../strongs/h/h5647.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 1](2chronicles_1.md) - [2Chronicles 3](2chronicles_3.md)