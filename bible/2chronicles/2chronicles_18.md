# [2Chronicles 18](https://www.blueletterbible.org/kjv/2chronicles/18)

<a name="2chronicles_18_1"></a>2Chronicles 18:1

Now [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) had [ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md) in [rōḇ](../../strongs/h/h7230.md), and joined [ḥāṯan](../../strongs/h/h2859.md) with ['Aḥ'Āḇ](../../strongs/h/h256.md).

<a name="2chronicles_18_2"></a>2Chronicles 18:2

And [qēṣ](../../strongs/h/h7093.md) [šānâ](../../strongs/h/h8141.md) he [yarad](../../strongs/h/h3381.md) to ['Aḥ'Āḇ](../../strongs/h/h256.md) to [Šōmrôn](../../strongs/h/h8111.md). And ['Aḥ'Āḇ](../../strongs/h/h256.md) [zabach](../../strongs/h/h2076.md) [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md) for him in [rōḇ](../../strongs/h/h7230.md), and for the ['am](../../strongs/h/h5971.md) that he had with him, and [sûṯ](../../strongs/h/h5496.md) him to [ʿālâ](../../strongs/h/h5927.md) with him to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="2chronicles_18_3"></a>2Chronicles 18:3

And ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), Wilt thou [yālaḵ](../../strongs/h/h3212.md) with me to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md)? And he ['āmar](../../strongs/h/h559.md) him, I am as thou art, and my ['am](../../strongs/h/h5971.md) as thy ['am](../../strongs/h/h5971.md); and we will be with thee in the [milḥāmâ](../../strongs/h/h4421.md).

<a name="2chronicles_18_4"></a>2Chronicles 18:4

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), [darash](../../strongs/h/h1875.md), I pray thee, at the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_18_5"></a>2Chronicles 18:5

Therefore the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [qāḇaṣ](../../strongs/h/h6908.md) of [nāḇî'](../../strongs/h/h5030.md) four hundred ['iysh](../../strongs/h/h376.md), and ['āmar](../../strongs/h/h559.md) unto them, Shall we [yālaḵ](../../strongs/h/h3212.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md) to [milḥāmâ](../../strongs/h/h4421.md), or shall I [ḥāḏal](../../strongs/h/h2308.md)? And they ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md); for ['Elohiym](../../strongs/h/h430.md) will [nathan](../../strongs/h/h5414.md) it into the [melek](../../strongs/h/h4428.md) [yad](../../strongs/h/h3027.md).

<a name="2chronicles_18_6"></a>2Chronicles 18:6

But [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), Is there not here a [nāḇî'](../../strongs/h/h5030.md) of [Yĕhovah](../../strongs/h/h3068.md) besides, that we might [darash](../../strongs/h/h1875.md) of him?

<a name="2chronicles_18_7"></a>2Chronicles 18:7

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), There is yet one ['iysh](../../strongs/h/h376.md), by whom we may [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md): but I [sane'](../../strongs/h/h8130.md) him; for he never [nāḇā'](../../strongs/h/h5012.md) [towb](../../strongs/h/h2896.md) unto me, but [yowm](../../strongs/h/h3117.md) [ra'](../../strongs/h/h7451.md): the same is [Mîḵāyhû](../../strongs/h/h4321.md) the [ben](../../strongs/h/h1121.md) of [Yimlā'](../../strongs/h/h3229.md). And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) ['āmar](../../strongs/h/h559.md), Let not the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) so.

<a name="2chronicles_18_8"></a>2Chronicles 18:8

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [qara'](../../strongs/h/h7121.md) for one of his [sārîs](../../strongs/h/h5631.md), and ['āmar](../../strongs/h/h559.md), [māhar](../../strongs/h/h4116.md) [Mîḵâû](../../strongs/h/h4319.md) the [ben](../../strongs/h/h1121.md) of [Yimlā'](../../strongs/h/h3229.md).

<a name="2chronicles_18_9"></a>2Chronicles 18:9

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yashab](../../strongs/h/h3427.md) ['iysh](../../strongs/h/h376.md) of them on his [kicce'](../../strongs/h/h3678.md), [labash](../../strongs/h/h3847.md) in their [beḡeḏ](../../strongs/h/h899.md), and they [yashab](../../strongs/h/h3427.md) in a void [gōren](../../strongs/h/h1637.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of [Šōmrôn](../../strongs/h/h8111.md); and all the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) [paniym](../../strongs/h/h6440.md) them.

<a name="2chronicles_18_10"></a>2Chronicles 18:10

And [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [KᵊnaʿĂnâ](../../strongs/h/h3668.md) had ['asah](../../strongs/h/h6213.md) him [qeren](../../strongs/h/h7161.md) of [barzel](../../strongs/h/h1270.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), With these thou shalt [nāḡaḥ](../../strongs/h/h5055.md) ['Ărām](../../strongs/h/h758.md) until they be [kalah](../../strongs/h/h3615.md).

<a name="2chronicles_18_11"></a>2Chronicles 18:11

And all the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) so, ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md), and [tsalach](../../strongs/h/h6743.md): for [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md).

<a name="2chronicles_18_12"></a>2Chronicles 18:12

And the [mal'ak](../../strongs/h/h4397.md) that [halak](../../strongs/h/h1980.md) to [qara'](../../strongs/h/h7121.md) [Mîḵāyhû](../../strongs/h/h4321.md) [dabar](../../strongs/h/h1696.md) to him, ['āmar](../../strongs/h/h559.md), Behold, the [dabar](../../strongs/h/h1697.md) of the [nāḇî'](../../strongs/h/h5030.md) [towb](../../strongs/h/h2896.md) to the [melek](../../strongs/h/h4428.md) with one [peh](../../strongs/h/h6310.md); let thy [dabar](../../strongs/h/h1697.md) therefore, I pray thee, be like one of theirs, and [dabar](../../strongs/h/h1696.md) thou [towb](../../strongs/h/h2896.md).

<a name="2chronicles_18_13"></a>2Chronicles 18:13

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), even what my ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), that will I [dabar](../../strongs/h/h1696.md).

<a name="2chronicles_18_14"></a>2Chronicles 18:14

And when he was [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md), the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto him, [Mîḵâ](../../strongs/h/h4318.md), shall we [yālaḵ](../../strongs/h/h3212.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md) to [milḥāmâ](../../strongs/h/h4421.md), or shall I [ḥāḏal](../../strongs/h/h2308.md)? And he ['āmar](../../strongs/h/h559.md), [ʿālâ](../../strongs/h/h5927.md) ye, and [tsalach](../../strongs/h/h6743.md), and they shall be [nathan](../../strongs/h/h5414.md) into your [yad](../../strongs/h/h3027.md).

<a name="2chronicles_18_15"></a>2Chronicles 18:15

And the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) to him, How many [pa'am](../../strongs/h/h6471.md) shall I [shaba'](../../strongs/h/h7650.md) thee that thou [dabar](../../strongs/h/h1696.md) nothing but the ['emeth](../../strongs/h/h571.md) to me in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="2chronicles_18_16"></a>2Chronicles 18:16

Then he ['āmar](../../strongs/h/h559.md), I did [ra'ah](../../strongs/h/h7200.md) all [Yisra'el](../../strongs/h/h3478.md) [puwts](../../strongs/h/h6327.md) upon the [har](../../strongs/h/h2022.md), as [tso'n](../../strongs/h/h6629.md) that have no [ra'ah](../../strongs/h/h7462.md): and [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), These have no ['adown](../../strongs/h/h113.md); let them [shuwb](../../strongs/h/h7725.md) therefore every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md) in [shalowm](../../strongs/h/h7965.md).

<a name="2chronicles_18_17"></a>2Chronicles 18:17

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) to [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), Did I not ['āmar](../../strongs/h/h559.md) thee that he would not [nāḇā'](../../strongs/h/h5012.md) [towb](../../strongs/h/h2896.md) unto me, but [ra'](../../strongs/h/h7451.md)?

<a name="2chronicles_18_18"></a>2Chronicles 18:18

Again he ['āmar](../../strongs/h/h559.md), Therefore [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md); I [ra'ah](../../strongs/h/h7200.md) [Yĕhovah](../../strongs/h/h3068.md) [yashab](../../strongs/h/h3427.md) upon his [kicce'](../../strongs/h/h3678.md), and all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) ['amad](../../strongs/h/h5975.md) on his [yamiyn](../../strongs/h/h3225.md) and on his [śᵊmō'l](../../strongs/h/h8040.md).

<a name="2chronicles_18_19"></a>2Chronicles 18:19

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Who shall [pāṯâ](../../strongs/h/h6601.md) ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), that he may [ʿālâ](../../strongs/h/h5927.md) and [naphal](../../strongs/h/h5307.md) at [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md)? And one ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) after this manner, and another ['āmar](../../strongs/h/h559.md) after that manner.

<a name="2chronicles_18_20"></a>2Chronicles 18:20

Then there [yāṣā'](../../strongs/h/h3318.md) a [ruwach](../../strongs/h/h7307.md), and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), I will [pāṯâ](../../strongs/h/h6601.md) him. And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto him, Wherewith?

<a name="2chronicles_18_21"></a>2Chronicles 18:21

And he ['āmar](../../strongs/h/h559.md), I will [yāṣā'](../../strongs/h/h3318.md), and be a [sheqer](../../strongs/h/h8267.md) [ruwach](../../strongs/h/h7307.md) in the [peh](../../strongs/h/h6310.md) of all his [nāḇî'](../../strongs/h/h5030.md). And the LORD ['āmar](../../strongs/h/h559.md), Thou shalt [pāṯâ](../../strongs/h/h6601.md) him, and thou shalt also [yakol](../../strongs/h/h3201.md): [yāṣā'](../../strongs/h/h3318.md), and ['asah](../../strongs/h/h6213.md) even so.

<a name="2chronicles_18_22"></a>2Chronicles 18:22

Now therefore, behold, [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) a [sheqer](../../strongs/h/h8267.md) [ruwach](../../strongs/h/h7307.md) in the [peh](../../strongs/h/h6310.md) of these thy [nāḇî'](../../strongs/h/h5030.md), and [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) [ra'](../../strongs/h/h7451.md) against thee.

<a name="2chronicles_18_23"></a>2Chronicles 18:23

Then [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [KᵊnaʿĂnâ](../../strongs/h/h3668.md) [nāḡaš](../../strongs/h/h5066.md), and [nakah](../../strongs/h/h5221.md) [Mîḵāyhû](../../strongs/h/h4321.md) upon the [lᵊḥî](../../strongs/h/h3895.md), and ['āmar](../../strongs/h/h559.md), Which [derek](../../strongs/h/h1870.md) ['abar](../../strongs/h/h5674.md) the [ruwach](../../strongs/h/h7307.md) of [Yĕhovah](../../strongs/h/h3068.md) from me to [dabar](../../strongs/h/h1696.md) unto thee?

<a name="2chronicles_18_24"></a>2Chronicles 18:24

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), Behold, thou shalt [ra'ah](../../strongs/h/h7200.md) on that [yowm](../../strongs/h/h3117.md) when thou shalt [bow'](../../strongs/h/h935.md) into an [ḥeḏer](../../strongs/h/h2315.md) [ḥeḏer](../../strongs/h/h2315.md) to [chaba'](../../strongs/h/h2244.md) thyself.

<a name="2chronicles_18_25"></a>2Chronicles 18:25

Then the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) ye [Mîḵāyhû](../../strongs/h/h4321.md), and [shuwb](../../strongs/h/h7725.md) him to ['Āmôn](../../strongs/h/h526.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md), and to [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md);

<a name="2chronicles_18_26"></a>2Chronicles 18:26

And ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) the [melek](../../strongs/h/h4428.md), [śûm](../../strongs/h/h7760.md) this fellow in the [bayith](../../strongs/h/h1004.md) [kele'](../../strongs/h/h3608.md), and ['akal](../../strongs/h/h398.md) him with [lechem](../../strongs/h/h3899.md) of [laḥaṣ](../../strongs/h/h3906.md) and with [mayim](../../strongs/h/h4325.md) of [laḥaṣ](../../strongs/h/h3906.md), until I [shuwb](../../strongs/h/h7725.md) in [shalowm](../../strongs/h/h7965.md).

<a name="2chronicles_18_27"></a>2Chronicles 18:27

And [Mîḵāyhû](../../strongs/h/h4321.md) ['āmar](../../strongs/h/h559.md), If thou [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) in [shalowm](../../strongs/h/h7965.md), then hath not [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) by me. And he ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md), all ye ['am](../../strongs/h/h5971.md).

<a name="2chronicles_18_28"></a>2Chronicles 18:28

So the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ʿālâ](../../strongs/h/h5927.md) to [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="2chronicles_18_29"></a>2Chronicles 18:29

And the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) ['āmar](../../strongs/h/h559.md) unto [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), I will [ḥāp̄aś](../../strongs/h/h2664.md) myself, and will [bow'](../../strongs/h/h935.md) to the [milḥāmâ](../../strongs/h/h4421.md); but [labash](../../strongs/h/h3847.md) thou on thy [beḡeḏ](../../strongs/h/h899.md). So the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ḥāp̄aś](../../strongs/h/h2664.md) himself; and they [bow'](../../strongs/h/h935.md) to the [milḥāmâ](../../strongs/h/h4421.md).

<a name="2chronicles_18_30"></a>2Chronicles 18:30

Now the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) had [tsavah](../../strongs/h/h6680.md) the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md) that were with him, ['āmar](../../strongs/h/h559.md), [lāḥam](../../strongs/h/h3898.md) ye not with [qāṭān](../../strongs/h/h6996.md) or [gadowl](../../strongs/h/h1419.md), save only with the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_18_31"></a>2Chronicles 18:31

And it came to pass, when the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md) [ra'ah](../../strongs/h/h7200.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), that they ['āmar](../../strongs/h/h559.md), It is the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md). Therefore they [cabab](../../strongs/h/h5437.md) him to [lāḥam](../../strongs/h/h3898.md): but [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [zāʿaq](../../strongs/h/h2199.md), and [Yĕhovah](../../strongs/h/h3068.md) [ʿāzar](../../strongs/h/h5826.md) him; and ['Elohiym](../../strongs/h/h430.md) [sûṯ](../../strongs/h/h5496.md) them to depart from him.

<a name="2chronicles_18_32"></a>2Chronicles 18:32

For it came to pass, that, when the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md) [ra'ah](../../strongs/h/h7200.md) that it was not the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), they [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md) him.

<a name="2chronicles_18_33"></a>2Chronicles 18:33

And a certain man [mashak](../../strongs/h/h4900.md) a [qesheth](../../strongs/h/h7198.md) at a [tom](../../strongs/h/h8537.md), and [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) between the [deḇeq](../../strongs/h/h1694.md) of the [širyôn](../../strongs/h/h8302.md): therefore he ['āmar](../../strongs/h/h559.md) to his [rakāḇ](../../strongs/h/h7395.md) ['iysh](../../strongs/h/h376.md), [hāp̄aḵ](../../strongs/h/h2015.md) thine [yad](../../strongs/h/h3027.md), that thou mayest [yāṣā'](../../strongs/h/h3318.md) me of the [maḥănê](../../strongs/h/h4264.md); for I am [ḥālâ](../../strongs/h/h2470.md).

<a name="2chronicles_18_34"></a>2Chronicles 18:34

And the [milḥāmâ](../../strongs/h/h4421.md) [ʿālâ](../../strongs/h/h5927.md) that [yowm](../../strongs/h/h3117.md): howbeit the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) stayed himself ['amad](../../strongs/h/h5975.md) in his [merkāḇâ](../../strongs/h/h4818.md) against the ['Ărām](../../strongs/h/h758.md) until the ['ereb](../../strongs/h/h6153.md): and about the [ʿēṯ](../../strongs/h/h6256.md) of the [šemeš](../../strongs/h/h8121.md) going [bow'](../../strongs/h/h935.md) he [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 17](2chronicles_17.md) - [2Chronicles 19](2chronicles_19.md)