# [2Chronicles 24](https://www.blueletterbible.org/kjv/2chronicles/24)

<a name="2chronicles_24_1"></a>2Chronicles 24:1

[Yô'Āš](../../strongs/h/h3101.md) was seven [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) forty [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was [Ṣiḇyâ](../../strongs/h/h6645.md) of [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md).

<a name="2chronicles_24_2"></a>2Chronicles 24:2

And [Yô'Āš](../../strongs/h/h3101.md) ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) all the [yowm](../../strongs/h/h3117.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md).

<a name="2chronicles_24_3"></a>2Chronicles 24:3

And [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [nasa'](../../strongs/h/h5375.md) for him two ['ishshah](../../strongs/h/h802.md); and he [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md).

<a name="2chronicles_24_4"></a>2Chronicles 24:4

And it came to pass ['aḥar](../../strongs/h/h310.md) this, that [Yô'Āš](../../strongs/h/h3101.md) was [leb](../../strongs/h/h3820.md) to [ḥādaš](../../strongs/h/h2318.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_24_5"></a>2Chronicles 24:5

And he [qāḇaṣ](../../strongs/h/h6908.md) the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and ['āmar](../../strongs/h/h559.md) to them, [yāṣā'](../../strongs/h/h3318.md) unto the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [qāḇaṣ](../../strongs/h/h6908.md) of all [Yisra'el](../../strongs/h/h3478.md) [keceph](../../strongs/h/h3701.md) to [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md) of your ['Elohiym](../../strongs/h/h430.md) [day](../../strongs/h/h1767.md) [šānâ](../../strongs/h/h8141.md) to [šānâ](../../strongs/h/h8141.md), and see that ye [māhar](../../strongs/h/h4116.md) the [dabar](../../strongs/h/h1697.md). Howbeit the [Lᵊvî](../../strongs/h/h3881.md) [māhar](../../strongs/h/h4116.md) it not.

<a name="2chronicles_24_6"></a>2Chronicles 24:6

And the [melek](../../strongs/h/h4428.md) [qara'](../../strongs/h/h7121.md) for [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [ro'sh](../../strongs/h/h7218.md), and ['āmar](../../strongs/h/h559.md) unto him, Why hast thou not [darash](../../strongs/h/h1875.md) of the [Lᵊvî](../../strongs/h/h3881.md) to [bow'](../../strongs/h/h935.md) out of [Yehuwdah](../../strongs/h/h3063.md) and out of [Yĕruwshalaim](../../strongs/h/h3389.md) the [maśśᵊ'ēṯ](../../strongs/h/h4864.md), according to [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md), and of the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md), for the ['ohel](../../strongs/h/h168.md) of [ʿēḏûṯ](../../strongs/h/h5715.md)?

<a name="2chronicles_24_7"></a>2Chronicles 24:7

For the [ben](../../strongs/h/h1121.md) of [ʿĂṯalyâ](../../strongs/h/h6271.md), that [miršaʿaṯ](../../strongs/h/h4849.md), had [pāraṣ](../../strongs/h/h6555.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md); and also all the [qodesh](../../strongs/h/h6944.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) did they ['asah](../../strongs/h/h6213.md) upon [BaʿAl](../../strongs/h/h1168.md).

<a name="2chronicles_24_8"></a>2Chronicles 24:8

And at the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) they ['asah](../../strongs/h/h6213.md) an ['ārôn](../../strongs/h/h727.md), and [nathan](../../strongs/h/h5414.md) it [ḥûṣ](../../strongs/h/h2351.md) at the [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_24_9"></a>2Chronicles 24:9

And they [nathan](../../strongs/h/h5414.md) a [qowl](../../strongs/h/h6963.md) through [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), to [bow'](../../strongs/h/h935.md) to [Yĕhovah](../../strongs/h/h3068.md) the [maśśᵊ'ēṯ](../../strongs/h/h4864.md) that [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of ['Elohiym](../../strongs/h/h430.md) laid upon [Yisra'el](../../strongs/h/h3478.md) in the [midbar](../../strongs/h/h4057.md).

<a name="2chronicles_24_10"></a>2Chronicles 24:10

And all the [śar](../../strongs/h/h8269.md) and all the ['am](../../strongs/h/h5971.md) [samach](../../strongs/h/h8055.md), and [bow'](../../strongs/h/h935.md), and [shalak](../../strongs/h/h7993.md) into the ['ārôn](../../strongs/h/h727.md), until they had [kalah](../../strongs/h/h3615.md).

<a name="2chronicles_24_11"></a>2Chronicles 24:11

Now it came to pass, that at what [ʿēṯ](../../strongs/h/h6256.md) the ['ārôn](../../strongs/h/h727.md) was [bow'](../../strongs/h/h935.md) unto the [melek](../../strongs/h/h4428.md) [pᵊqudâ](../../strongs/h/h6486.md) by the [yad](../../strongs/h/h3027.md) of the [Lᵊvî](../../strongs/h/h3881.md), and when they [ra'ah](../../strongs/h/h7200.md) that there was [rab](../../strongs/h/h7227.md) [keceph](../../strongs/h/h3701.md), the [melek](../../strongs/h/h4428.md) [sāp̄ar](../../strongs/h/h5608.md) and the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md) [pāqîḏ](../../strongs/h/h6496.md) [bow'](../../strongs/h/h935.md) and [ʿārâ](../../strongs/h/h6168.md) the ['ārôn](../../strongs/h/h727.md), and [nasa'](../../strongs/h/h5375.md) it, and [shuwb](../../strongs/h/h7725.md) it to his [maqowm](../../strongs/h/h4725.md) [shuwb](../../strongs/h/h7725.md). Thus they ['asah](../../strongs/h/h6213.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md), and ['āsap̄](../../strongs/h/h622.md) [keceph](../../strongs/h/h3701.md) in [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_24_12"></a>2Chronicles 24:12

And the [melek](../../strongs/h/h4428.md) and [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [nathan](../../strongs/h/h5414.md) it to such as ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) of the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [śāḵar](../../strongs/h/h7936.md) [ḥāṣaḇ](../../strongs/h/h2672.md) and [ḥārāš](../../strongs/h/h2796.md) to [ḥādaš](../../strongs/h/h2318.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and also such as [ḥārāš](../../strongs/h/h2796.md) [barzel](../../strongs/h/h1270.md) and [nᵊḥšeṯ](../../strongs/h/h5178.md) to [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_24_13"></a>2Chronicles 24:13

So the [mĕla'kah](../../strongs/h/h4399.md) ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md), and the [mĕla'kah](../../strongs/h/h4399.md) [ʿālâ](../../strongs/h/h5927.md) ['ărûḵâ](../../strongs/h/h724.md) by [yad](../../strongs/h/h3027.md), and they ['amad](../../strongs/h/h5975.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) in his [maṯkōneṯ](../../strongs/h/h4971.md), and ['amats](../../strongs/h/h553.md) it.

<a name="2chronicles_24_14"></a>2Chronicles 24:14

And when they had [kalah](../../strongs/h/h3615.md) it, they [bow'](../../strongs/h/h935.md) the [šᵊ'ār](../../strongs/h/h7605.md) of the [keceph](../../strongs/h/h3701.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) and [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md), whereof were ['asah](../../strongs/h/h6213.md) [kĕliy](../../strongs/h/h3627.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), even [kĕliy](../../strongs/h/h3627.md) to [šārēṯ](../../strongs/h/h8335.md), and to [ʿālâ](../../strongs/h/h5927.md) withal, and [kaph](../../strongs/h/h3709.md), and [kĕliy](../../strongs/h/h3627.md) of [zāhāḇ](../../strongs/h/h2091.md) and [keceph](../../strongs/h/h3701.md). And they [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md) all the [yowm](../../strongs/h/h3117.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md).

<a name="2chronicles_24_15"></a>2Chronicles 24:15

But [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [zāqēn](../../strongs/h/h2204.md), and was [sāׂbaʿ](../../strongs/h/h7646.md) of [yowm](../../strongs/h/h3117.md) when he [muwth](../../strongs/h/h4191.md); an hundred and thirty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was he when he [maveth](../../strongs/h/h4194.md).

<a name="2chronicles_24_16"></a>2Chronicles 24:16

And they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md) among the [melek](../../strongs/h/h4428.md), because he had ['asah](../../strongs/h/h6213.md) [towb](../../strongs/h/h2896.md) in [Yisra'el](../../strongs/h/h3478.md), both toward ['Elohiym](../../strongs/h/h430.md), and toward his [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_24_17"></a>2Chronicles 24:17

Now ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [bow'](../../strongs/h/h935.md) the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md), and made [shachah](../../strongs/h/h7812.md) to the [melek](../../strongs/h/h4428.md). Then the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) unto them.

<a name="2chronicles_24_18"></a>2Chronicles 24:18

And they ['azab](../../strongs/h/h5800.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), and ['abad](../../strongs/h/h5647.md) ['ăšērâ](../../strongs/h/h842.md) and [ʿāṣāḇ](../../strongs/h/h6091.md): and [qeṣep̄](../../strongs/h/h7110.md) came upon [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) for this their ['ašmâ](../../strongs/h/h819.md).

<a name="2chronicles_24_19"></a>2Chronicles 24:19

Yet he [shalach](../../strongs/h/h7971.md) [nāḇî'](../../strongs/h/h5030.md) to them, to [shuwb](../../strongs/h/h7725.md) them unto [Yĕhovah](../../strongs/h/h3068.md); and they [ʿûḏ](../../strongs/h/h5749.md) against them: but they would not ['azan](../../strongs/h/h238.md).

<a name="2chronicles_24_20"></a>2Chronicles 24:20

And the [ruwach](../../strongs/h/h7307.md) of ['Elohiym](../../strongs/h/h430.md) [labash](../../strongs/h/h3847.md) upon [Zᵊḵaryâ](../../strongs/h/h2148.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md), which ['amad](../../strongs/h/h5975.md) above the ['am](../../strongs/h/h5971.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) ['Elohiym](../../strongs/h/h430.md), Why ['abar](../../strongs/h/h5674.md) ye the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md), that ye cannot [tsalach](../../strongs/h/h6743.md)? because ye have ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), he hath also ['azab](../../strongs/h/h5800.md) you.

<a name="2chronicles_24_21"></a>2Chronicles 24:21

And they [qāšar](../../strongs/h/h7194.md) against him, and [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md) at the [mitsvah](../../strongs/h/h4687.md) of the [melek](../../strongs/h/h4428.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_24_22"></a>2Chronicles 24:22

Thus [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) [zakar](../../strongs/h/h2142.md) not the [checed](../../strongs/h/h2617.md) which [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md) to him, but [harag](../../strongs/h/h2026.md) his [ben](../../strongs/h/h1121.md). And when he [maveth](../../strongs/h/h4194.md), he ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) upon it, and [darash](../../strongs/h/h1875.md) it.

<a name="2chronicles_24_23"></a>2Chronicles 24:23

And it came to pass at the [tᵊqûp̄â](../../strongs/h/h8622.md) of the [šānâ](../../strongs/h/h8141.md), that the [ḥayil](../../strongs/h/h2428.md) of ['Ărām](../../strongs/h/h758.md) [ʿālâ](../../strongs/h/h5927.md) against him: and they [bow'](../../strongs/h/h935.md) to [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), and [shachath](../../strongs/h/h7843.md) all the [śar](../../strongs/h/h8269.md) of the ['am](../../strongs/h/h5971.md) from among the ['am](../../strongs/h/h5971.md), and [shalach](../../strongs/h/h7971.md) all the [šālāl](../../strongs/h/h7998.md) of them unto the [melek](../../strongs/h/h4428.md) of [Dammeśeq](../../strongs/h/h1834.md).

<a name="2chronicles_24_24"></a>2Chronicles 24:24

For the [ḥayil](../../strongs/h/h2428.md) of the ['Ărām](../../strongs/h/h758.md) [bow'](../../strongs/h/h935.md) with a [miṣʿār](../../strongs/h/h4705.md) company of ['enowsh](../../strongs/h/h582.md), and [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) a [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md) [ḥayil](../../strongs/h/h2428.md) into their [yad](../../strongs/h/h3027.md), because they had ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md). So they ['asah](../../strongs/h/h6213.md) [šep̄eṭ](../../strongs/h/h8201.md) against [Yô'Āš](../../strongs/h/h3101.md).

<a name="2chronicles_24_25"></a>2Chronicles 24:25

And when they were [yālaḵ](../../strongs/h/h3212.md) from him, (for they ['azab](../../strongs/h/h5800.md) him in [rab](../../strongs/h/h7227.md) [maḥăluyîm](../../strongs/h/h4251.md),) his own ['ebed](../../strongs/h/h5650.md) [qāšar](../../strongs/h/h7194.md) against him for the [dam](../../strongs/h/h1818.md) of the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md), and [harag](../../strongs/h/h2026.md) him on his [mittah](../../strongs/h/h4296.md), and he [muwth](../../strongs/h/h4191.md): and they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), but they [qāḇar](../../strongs/h/h6912.md) him not in the [qeber](../../strongs/h/h6913.md) of the [melek](../../strongs/h/h4428.md).

<a name="2chronicles_24_26"></a>2Chronicles 24:26

And these are they that [qāšar](../../strongs/h/h7194.md) against him; [Zāḇāḏ](../../strongs/h/h2066.md) the [ben](../../strongs/h/h1121.md) of [ŠimʿĀṯ](../../strongs/h/h8100.md) an [ʿammônîṯ](../../strongs/h/h5985.md), and [Yᵊhôzāḇāḏ](../../strongs/h/h3075.md) the [ben](../../strongs/h/h1121.md) of [Šimrîṯ](../../strongs/h/h8116.md) a [Mô'āḇî](../../strongs/h/h4125.md).

<a name="2chronicles_24_27"></a>2Chronicles 24:27

Now concerning his [ben](../../strongs/h/h1121.md), and the [rabah](../../strongs/h/h7235.md) [rōḇ](../../strongs/h/h7230.md) of the [maśśā'](../../strongs/h/h4853.md) laid upon him, and the [yᵊsôḏ](../../strongs/h/h3247.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [miḏrāš](../../strongs/h/h4097.md) of the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md). And ['Ămaṣyâ](../../strongs/h/h558.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 23](2chronicles_23.md) - [2Chronicles 25](2chronicles_25.md)