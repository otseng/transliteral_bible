# [2Chronicles 22](https://www.blueletterbible.org/kjv/2chronicles/22)

<a name="2chronicles_22_1"></a>2Chronicles 22:1

And the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [mālaḵ](../../strongs/h/h4427.md) ['Ăḥazyâ](../../strongs/h/h274.md) his [qāṭān](../../strongs/h/h6996.md) [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead: for the band of [gᵊḏûḏ](../../strongs/h/h1416.md) that [bow'](../../strongs/h/h935.md) with the [ʿĂrāḇî](../../strongs/h/h6163.md) to the [maḥănê](../../strongs/h/h4264.md) had [harag](../../strongs/h/h2026.md) all the [ri'šôn](../../strongs/h/h7223.md). So ['Ăḥazyâ](../../strongs/h/h274.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôrām](../../strongs/h/h3088.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [mālaḵ](../../strongs/h/h4427.md).

<a name="2chronicles_22_2"></a>2Chronicles 22:2

Forty and two [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) was ['Ăḥazyâ](../../strongs/h/h274.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) one [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was [ʿĂṯalyâ](../../strongs/h/h6271.md) the [bath](../../strongs/h/h1323.md) of [ʿĀmrî](../../strongs/h/h6018.md).

<a name="2chronicles_22_3"></a>2Chronicles 22:3

He also [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for his ['em](../../strongs/h/h517.md) was his [ya'ats](../../strongs/h/h3289.md) to do [rāšaʿ](../../strongs/h/h7561.md).

<a name="2chronicles_22_4"></a>2Chronicles 22:4

Wherefore he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) like the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for they were his [ya'ats](../../strongs/h/h3289.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of his ['ab](../../strongs/h/h1.md) to his [mašḥîṯ](../../strongs/h/h4889.md).

<a name="2chronicles_22_5"></a>2Chronicles 22:5

He [halak](../../strongs/h/h1980.md) also after their ['etsah](../../strongs/h/h6098.md), and [yālaḵ](../../strongs/h/h3212.md) with [Yᵊhôrām](../../strongs/h/h3088.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) to [milḥāmâ](../../strongs/h/h4421.md) against [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md) at [Rāmôṯ GilʿĀḏ](../../strongs/h/h7433.md) [Gilʿāḏ](../../strongs/h/h1568.md): and the [rammî](../../strongs/h/h7421.md) [nakah](../../strongs/h/h5221.md) [Yôrām](../../strongs/h/h3141.md).

<a name="2chronicles_22_6"></a>2Chronicles 22:6

And he [shuwb](../../strongs/h/h7725.md) to be [rapha'](../../strongs/h/h7495.md) in [YizrᵊʿE'L](../../strongs/h/h3157.md) because of the [makâ](../../strongs/h/h4347.md) which were [nakah](../../strongs/h/h5221.md) him at [rāmâ](../../strongs/h/h7414.md), when he [lāḥam](../../strongs/h/h3898.md) with [Ḥăzā'Ēl](../../strongs/h/h2371.md) [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md). And [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôrām](../../strongs/h/h3088.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [yarad](../../strongs/h/h3381.md) to [ra'ah](../../strongs/h/h7200.md) [Yᵊhôrām](../../strongs/h/h3088.md) the [ben](../../strongs/h/h1121.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) at [YizrᵊʿE'L](../../strongs/h/h3157.md), because he was [ḥālâ](../../strongs/h/h2470.md).

<a name="2chronicles_22_7"></a>2Chronicles 22:7

And the [tᵊḇûsâ](../../strongs/h/h8395.md) of ['Ăḥazyâ](../../strongs/h/h274.md) was of ['Elohiym](../../strongs/h/h430.md) by [bow'](../../strongs/h/h935.md) to [Yôrām](../../strongs/h/h3141.md): for when he was [bow'](../../strongs/h/h935.md), he [yāṣā'](../../strongs/h/h3318.md) with [Yᵊhôrām](../../strongs/h/h3088.md) against [Yêû'](../../strongs/h/h3058.md) the [ben](../../strongs/h/h1121.md) of [Nimšî](../../strongs/h/h5250.md), whom [Yĕhovah](../../strongs/h/h3068.md) had [māšaḥ](../../strongs/h/h4886.md) to [karath](../../strongs/h/h3772.md) the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md).

<a name="2chronicles_22_8"></a>2Chronicles 22:8

And it came to pass, that, when [Yêû'](../../strongs/h/h3058.md) was [shaphat](../../strongs/h/h8199.md) upon the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), and [māṣā'](../../strongs/h/h4672.md) the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [ben](../../strongs/h/h1121.md) of the ['ach](../../strongs/h/h251.md) of ['Ăḥazyâ](../../strongs/h/h274.md), that [sharath](../../strongs/h/h8334.md) to ['Ăḥazyâ](../../strongs/h/h274.md), he [harag](../../strongs/h/h2026.md) them.

<a name="2chronicles_22_9"></a>2Chronicles 22:9

And he [bāqaš](../../strongs/h/h1245.md) ['Ăḥazyâ](../../strongs/h/h274.md): and they [lāḵaḏ](../../strongs/h/h3920.md) him, (for he was [chaba'](../../strongs/h/h2244.md) in [Šōmrôn](../../strongs/h/h8111.md),) and [bow'](../../strongs/h/h935.md) him to [Yêû'](../../strongs/h/h3058.md): and when they had [muwth](../../strongs/h/h4191.md) him, they [qāḇar](../../strongs/h/h6912.md) him: Because, ['āmar](../../strongs/h/h559.md) they, he is the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), who [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) with all his [lebab](../../strongs/h/h3824.md). So the [bayith](../../strongs/h/h1004.md) of ['Ăḥazyâ](../../strongs/h/h274.md) had no [koach](../../strongs/h/h3581.md) to [ʿāṣar](../../strongs/h/h6113.md) still the [mamlāḵâ](../../strongs/h/h4467.md).

<a name="2chronicles_22_10"></a>2Chronicles 22:10

But when [ʿĂṯalyâ](../../strongs/h/h6271.md) the ['em](../../strongs/h/h517.md) of ['Ăḥazyâ](../../strongs/h/h274.md) [ra'ah](../../strongs/h/h7200.md) that her [ben](../../strongs/h/h1121.md) was [muwth](../../strongs/h/h4191.md), she [quwm](../../strongs/h/h6965.md) and [dabar](../../strongs/h/h1696.md) all the [zera'](../../strongs/h/h2233.md) [mamlāḵâ](../../strongs/h/h4467.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_22_11"></a>2Chronicles 22:11

But [YᵊhôשAḇʿAṯ](../../strongs/h/h3090.md), the [bath](../../strongs/h/h1323.md) of the [melek](../../strongs/h/h4428.md), [laqach](../../strongs/h/h3947.md) [Yô'Āš](../../strongs/h/h3101.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥazyâ](../../strongs/h/h274.md), and [ganab](../../strongs/h/h1589.md) him from [tavek](../../strongs/h/h8432.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) that were [muwth](../../strongs/h/h4191.md), and [nathan](../../strongs/h/h5414.md) him and his [yānaq](../../strongs/h/h3243.md) in a [ḥeḏer](../../strongs/h/h2315.md) [mittah](../../strongs/h/h4296.md). So [YᵊhôשAḇʿAṯ](../../strongs/h/h3090.md), the [bath](../../strongs/h/h1323.md) of [melek](../../strongs/h/h4428.md) [Yᵊhôrām](../../strongs/h/h3088.md), the ['ishshah](../../strongs/h/h802.md) of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md), (for she was the ['āḥôṯ](../../strongs/h/h269.md) of ['Ăḥazyâ](../../strongs/h/h274.md),) [cathar](../../strongs/h/h5641.md) him [paniym](../../strongs/h/h6440.md) [ʿĂṯalyâ](../../strongs/h/h6271.md), so that she [muwth](../../strongs/h/h4191.md) him not.

<a name="2chronicles_22_12"></a>2Chronicles 22:12

And he was with them [chaba'](../../strongs/h/h2244.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) six [šānâ](../../strongs/h/h8141.md): and [ʿĂṯalyâ](../../strongs/h/h6271.md) [mālaḵ](../../strongs/h/h4427.md) over the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 21](2chronicles_21.md) - [2Chronicles 23](2chronicles_23.md)