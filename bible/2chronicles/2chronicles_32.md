# [2Chronicles 32](https://www.blueletterbible.org/kjv/2chronicles/32)

<a name="2chronicles_32_1"></a>2Chronicles 32:1

['aḥar](../../strongs/h/h310.md) these [dabar](../../strongs/h/h1697.md), and the ['emeth](../../strongs/h/h571.md) thereof, [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [bow'](../../strongs/h/h935.md), and [bow'](../../strongs/h/h935.md) into [Yehuwdah](../../strongs/h/h3063.md), and [ḥānâ](../../strongs/h/h2583.md) against the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md), and ['āmar](../../strongs/h/h559.md) to [bāqaʿ](../../strongs/h/h1234.md) them for himself.

<a name="2chronicles_32_2"></a>2Chronicles 32:2

And when [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [ra'ah](../../strongs/h/h7200.md) that [Sanḥērîḇ](../../strongs/h/h5576.md) was [bow'](../../strongs/h/h935.md), and that he was [paniym](../../strongs/h/h6440.md) to [milḥāmâ](../../strongs/h/h4421.md) against [Yĕruwshalaim](../../strongs/h/h3389.md),

<a name="2chronicles_32_3"></a>2Chronicles 32:3

He took [ya'ats](../../strongs/h/h3289.md) with his [śar](../../strongs/h/h8269.md) and his [gibôr](../../strongs/h/h1368.md) to [sāṯam](../../strongs/h/h5640.md) the [mayim](../../strongs/h/h4325.md) of the ['ayin](../../strongs/h/h5869.md) which were [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md): and they did [ʿāzar](../../strongs/h/h5826.md) him.

<a name="2chronicles_32_4"></a>2Chronicles 32:4

So there was [qāḇaṣ](../../strongs/h/h6908.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) [qāḇaṣ](../../strongs/h/h6908.md), who [sāṯam](../../strongs/h/h5640.md) all the [maʿyān](../../strongs/h/h4599.md), and the [nachal](../../strongs/h/h5158.md) that [šāṭap̄](../../strongs/h/h7857.md) through the [tavek](../../strongs/h/h8432.md) of the ['erets](../../strongs/h/h776.md), ['āmar](../../strongs/h/h559.md), Why should the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [bow'](../../strongs/h/h935.md), and [māṣā'](../../strongs/h/h4672.md) [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md)?

<a name="2chronicles_32_5"></a>2Chronicles 32:5

Also he [ḥāzaq](../../strongs/h/h2388.md) himself, and [bānâ](../../strongs/h/h1129.md) all the [ḥômâ](../../strongs/h/h2346.md) that was [pāraṣ](../../strongs/h/h6555.md), and raised it [ʿālâ](../../strongs/h/h5927.md) to the [miḡdāl](../../strongs/h/h4026.md), and ['aḥēr](../../strongs/h/h312.md) [ḥômâ](../../strongs/h/h2346.md) [ḥûṣ](../../strongs/h/h2351.md), and [ḥāzaq](../../strongs/h/h2388.md) [Millô'](../../strongs/h/h4407.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), and ['asah](../../strongs/h/h6213.md) [šelaḥ](../../strongs/h/h7973.md) and [magen](../../strongs/h/h4043.md) in [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_32_6"></a>2Chronicles 32:6

And he [nathan](../../strongs/h/h5414.md) [śar](../../strongs/h/h8269.md) of [milḥāmâ](../../strongs/h/h4421.md) over the ['am](../../strongs/h/h5971.md), and gathered them [qāḇaṣ](../../strongs/h/h6908.md) to him in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [sha'ar](../../strongs/h/h8179.md) of the [ʿîr](../../strongs/h/h5892.md), and [dabar](../../strongs/h/h1696.md) [lebab](../../strongs/h/h3824.md) to them, ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_32_7"></a>2Chronicles 32:7

Be [ḥāzaq](../../strongs/h/h2388.md) and ['amats](../../strongs/h/h553.md), be not [yare'](../../strongs/h/h3372.md) nor [ḥāṯaṯ](../../strongs/h/h2865.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), nor for all the [hāmôn](../../strongs/h/h1995.md) that is with him: for there be [rab](../../strongs/h/h7227.md) with us than with him:

<a name="2chronicles_32_8"></a>2Chronicles 32:8

With him is a [zerowa'](../../strongs/h/h2220.md) of [basar](../../strongs/h/h1320.md); but with us is [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) to [ʿāzar](../../strongs/h/h5826.md) us, and to [lāḥam](../../strongs/h/h3898.md) our [milḥāmâ](../../strongs/h/h4421.md). And the ['am](../../strongs/h/h5971.md) [camak](../../strongs/h/h5564.md) themselves upon the [dabar](../../strongs/h/h1697.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_32_9"></a>2Chronicles 32:9

['aḥar](../../strongs/h/h310.md) this did [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) [shalach](../../strongs/h/h7971.md) his ['ebed](../../strongs/h/h5650.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), (but he himself laid siege against [Lāḵîš](../../strongs/h/h3923.md), and all his [memshalah](../../strongs/h/h4475.md) with him,) unto [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and unto all [Yehuwdah](../../strongs/h/h3063.md) that were at [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_32_10"></a>2Chronicles 32:10

Thus ['āmar](../../strongs/h/h559.md) [Sanḥērîḇ](../../strongs/h/h5576.md) [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), Whereon do ye [batach](../../strongs/h/h982.md), that ye [yashab](../../strongs/h/h3427.md) in the [māṣôr](../../strongs/h/h4692.md) in [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="2chronicles_32_11"></a>2Chronicles 32:11

Doth not [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [sûṯ](../../strongs/h/h5496.md) you to [nathan](../../strongs/h/h5414.md) over yourselves to [muwth](../../strongs/h/h4191.md) by [rāʿāḇ](../../strongs/h/h7458.md) and by [ṣāmā'](../../strongs/h/h6772.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall [natsal](../../strongs/h/h5337.md) us out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md)?

<a name="2chronicles_32_12"></a>2Chronicles 32:12

Hath not the same [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [cuwr](../../strongs/h/h5493.md) his [bāmâ](../../strongs/h/h1116.md) and his [mizbeach](../../strongs/h/h4196.md), and ['āmar](../../strongs/h/h559.md) [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), Ye shall [shachah](../../strongs/h/h7812.md) [paniym](../../strongs/h/h6440.md) one [mizbeach](../../strongs/h/h4196.md), and [qāṭar](../../strongs/h/h6999.md) upon it?

<a name="2chronicles_32_13"></a>2Chronicles 32:13

[yada'](../../strongs/h/h3045.md) ye not what I and my ['ab](../../strongs/h/h1.md) have ['asah](../../strongs/h/h6213.md) unto all the ['am](../../strongs/h/h5971.md) of other ['erets](../../strongs/h/h776.md)? were the ['Elohiym](../../strongs/h/h430.md) of the [gowy](../../strongs/h/h1471.md) of those ['erets](../../strongs/h/h776.md) any [yakol](../../strongs/h/h3201.md) [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) their ['erets](../../strongs/h/h776.md) out of mine [yad](../../strongs/h/h3027.md)?

<a name="2chronicles_32_14"></a>2Chronicles 32:14

Who was there among all the ['Elohiym](../../strongs/h/h430.md) of those [gowy](../../strongs/h/h1471.md) that my ['ab](../../strongs/h/h1.md) [ḥāram](../../strongs/h/h2763.md), that [yakol](../../strongs/h/h3201.md) [natsal](../../strongs/h/h5337.md) his ['am](../../strongs/h/h5971.md) out of mine [yad](../../strongs/h/h3027.md), that your ['Elohiym](../../strongs/h/h430.md) should be [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) you out of mine [yad](../../strongs/h/h3027.md)?

<a name="2chronicles_32_15"></a>2Chronicles 32:15

Now therefore let not [Ḥizqîyâ](../../strongs/h/h2396.md) [nasha'](../../strongs/h/h5377.md) you, nor [sûṯ](../../strongs/h/h5496.md) you on this manner, neither yet ['aman](../../strongs/h/h539.md) him: for no ['ĕlvôha](../../strongs/h/h433.md) of any [gowy](../../strongs/h/h1471.md) or [mamlāḵâ](../../strongs/h/h4467.md) was [yakol](../../strongs/h/h3201.md) to [natsal](../../strongs/h/h5337.md) his ['am](../../strongs/h/h5971.md) out of mine [yad](../../strongs/h/h3027.md), and out of the [yad](../../strongs/h/h3027.md) of my ['ab](../../strongs/h/h1.md): how much less shall your ['Elohiym](../../strongs/h/h430.md) [natsal](../../strongs/h/h5337.md) you out of mine [yad](../../strongs/h/h3027.md)?

<a name="2chronicles_32_16"></a>2Chronicles 32:16

And his ['ebed](../../strongs/h/h5650.md) [dabar](../../strongs/h/h1696.md) yet more against [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), and against his ['ebed](../../strongs/h/h5650.md) [Yᵊḥizqîyâ](../../strongs/h/h3169.md).

<a name="2chronicles_32_17"></a>2Chronicles 32:17

He [kāṯaḇ](../../strongs/h/h3789.md) also [sēp̄er](../../strongs/h/h5612.md) to [ḥārap̄](../../strongs/h/h2778.md) on [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), and to ['āmar](../../strongs/h/h559.md) against him, ['āmar](../../strongs/h/h559.md), As the ['Elohiym](../../strongs/h/h430.md) of the [gowy](../../strongs/h/h1471.md) of other ['erets](../../strongs/h/h776.md) have not [natsal](../../strongs/h/h5337.md) their ['am](../../strongs/h/h5971.md) out of mine [yad](../../strongs/h/h3027.md), so shall not the ['Elohiym](../../strongs/h/h430.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [natsal](../../strongs/h/h5337.md) his ['am](../../strongs/h/h5971.md) out of mine [yad](../../strongs/h/h3027.md).

<a name="2chronicles_32_18"></a>2Chronicles 32:18

Then they [qara'](../../strongs/h/h7121.md) with a [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md) in the [yᵊhûḏîṯ](../../strongs/h/h3066.md) unto the ['am](../../strongs/h/h5971.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) that were on the [ḥômâ](../../strongs/h/h2346.md), to [yare'](../../strongs/h/h3372.md) them, and to [bahal](../../strongs/h/h926.md) them; that they might [lāḵaḏ](../../strongs/h/h3920.md) the [ʿîr](../../strongs/h/h5892.md).

<a name="2chronicles_32_19"></a>2Chronicles 32:19

And they [dabar](../../strongs/h/h1696.md) against the ['Elohiym](../../strongs/h/h430.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), as against the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), which were the [ma'aseh](../../strongs/h/h4639.md) of the [yad](../../strongs/h/h3027.md) of ['āḏām](../../strongs/h/h120.md).

<a name="2chronicles_32_20"></a>2Chronicles 32:20

And for this cause [Yᵊḥizqîyâ](../../strongs/h/h3169.md) the [melek](../../strongs/h/h4428.md), and the [nāḇî'](../../strongs/h/h5030.md) [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md), [palal](../../strongs/h/h6419.md) and [zāʿaq](../../strongs/h/h2199.md) to [shamayim](../../strongs/h/h8064.md).

<a name="2chronicles_32_21"></a>2Chronicles 32:21

And [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) a [mal'ak](../../strongs/h/h4397.md), which [kāḥaḏ](../../strongs/h/h3582.md) all the [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and the [nāḡîḏ](../../strongs/h/h5057.md) and [śar](../../strongs/h/h8269.md) in the [maḥănê](../../strongs/h/h4264.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md). So he [shuwb](../../strongs/h/h7725.md) with [bšeṯ](../../strongs/h/h1322.md) of [paniym](../../strongs/h/h6440.md) to his own ['erets](../../strongs/h/h776.md). And when he was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of his ['Elohiym](../../strongs/h/h430.md), they that [yāṣî'](../../strongs/h/h3329.md) of his own [me'ah](../../strongs/h/h4578.md) [naphal](../../strongs/h/h5307.md) him there with the [chereb](../../strongs/h/h2719.md).

<a name="2chronicles_32_22"></a>2Chronicles 32:22

Thus [Yĕhovah](../../strongs/h/h3068.md) [yasha'](../../strongs/h/h3467.md) [Yᵊḥizqîyâ](../../strongs/h/h3169.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) from the [yad](../../strongs/h/h3027.md) of [Sanḥērîḇ](../../strongs/h/h5576.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), and from the [yad](../../strongs/h/h3027.md) of all other, and [nāhal](../../strongs/h/h5095.md) them [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_32_23"></a>2Chronicles 32:23

And [rab](../../strongs/h/h7227.md) [bow'](../../strongs/h/h935.md) [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), and [miḡdānôṯ](../../strongs/h/h4030.md) to [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md): so that he was [nasa'](../../strongs/h/h5375.md) in the ['ayin](../../strongs/h/h5869.md) of all [gowy](../../strongs/h/h1471.md) from ['aḥar](../../strongs/h/h310.md).

<a name="2chronicles_32_24"></a>2Chronicles 32:24

In those [yowm](../../strongs/h/h3117.md) [Yᵊḥizqîyâ](../../strongs/h/h3169.md) was [ḥālâ](../../strongs/h/h2470.md) to the [muwth](../../strongs/h/h4191.md), and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md): and he ['āmar](../../strongs/h/h559.md) unto him, and he [nathan](../../strongs/h/h5414.md) him a [môp̄ēṯ](../../strongs/h/h4159.md).

<a name="2chronicles_32_25"></a>2Chronicles 32:25

But [Yᵊḥizqîyâ](../../strongs/h/h3169.md) rendered not [shuwb](../../strongs/h/h7725.md) according to the [gĕmwl](../../strongs/h/h1576.md) done unto him; for his [leb](../../strongs/h/h3820.md) was [gāḇah](../../strongs/h/h1361.md): therefore there was [qeṣep̄](../../strongs/h/h7110.md) upon him, and upon [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_32_26"></a>2Chronicles 32:26

Notwithstanding [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [kānaʿ](../../strongs/h/h3665.md) himself for the [gobahh](../../strongs/h/h1363.md) of his [leb](../../strongs/h/h3820.md), both he and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), so that the [qeṣep̄](../../strongs/h/h7110.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) not upon them in the [yowm](../../strongs/h/h3117.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md).

<a name="2chronicles_32_27"></a>2Chronicles 32:27

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) had [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md): and he ['asah](../../strongs/h/h6213.md) himself ['ôṣār](../../strongs/h/h214.md) for [keceph](../../strongs/h/h3701.md), and for [zāhāḇ](../../strongs/h/h2091.md), and for [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md), and for [beśem](../../strongs/h/h1314.md), and for [magen](../../strongs/h/h4043.md), and for all manner of [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md);

<a name="2chronicles_32_28"></a>2Chronicles 32:28

[miskᵊnôṯ](../../strongs/h/h4543.md) also for the [tᵊḇû'â](../../strongs/h/h8393.md) of [dagan](../../strongs/h/h1715.md), and [tiyrowsh](../../strongs/h/h8492.md), and [yiṣhār](../../strongs/h/h3323.md); and ['urvâ](../../strongs/h/h723.md) for all manner of [bĕhemah](../../strongs/h/h929.md), and ['uryâ](../../strongs/h/h220.md) for [ʿēḏer](../../strongs/h/h5739.md).

<a name="2chronicles_32_29"></a>2Chronicles 32:29

Moreover he ['asah](../../strongs/h/h6213.md) him [ʿîr](../../strongs/h/h5892.md), and [miqnê](../../strongs/h/h4735.md) of [tso'n](../../strongs/h/h6629.md) and [bāqār](../../strongs/h/h1241.md) in [rōḇ](../../strongs/h/h7230.md): for ['Elohiym](../../strongs/h/h430.md) had [nathan](../../strongs/h/h5414.md) him [rᵊḵûš](../../strongs/h/h7399.md) [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md).

<a name="2chronicles_32_30"></a>2Chronicles 32:30

This same [Yᵊḥizqîyâ](../../strongs/h/h3169.md) also [sāṯam](../../strongs/h/h5640.md) the ['elyown](../../strongs/h/h5945.md) [môṣā'](../../strongs/h/h4161.md) [mayim](../../strongs/h/h4325.md) of [gîḥôn](../../strongs/h/h1521.md), and brought it [yashar](../../strongs/h/h3474.md) [maṭṭâ](../../strongs/h/h4295.md) to the west [ma'arab](../../strongs/h/h4628.md) of the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md). And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [tsalach](../../strongs/h/h6743.md) in all his [ma'aseh](../../strongs/h/h4639.md).

<a name="2chronicles_32_31"></a>2Chronicles 32:31

Howbeit in the [luwts](../../strongs/h/h3887.md) of the [śar](../../strongs/h/h8269.md) of [Bāḇel](../../strongs/h/h894.md), who [shalach](../../strongs/h/h7971.md) unto him to [darash](../../strongs/h/h1875.md) of the [môp̄ēṯ](../../strongs/h/h4159.md) that was done in the ['erets](../../strongs/h/h776.md), ['Elohiym](../../strongs/h/h430.md) ['azab](../../strongs/h/h5800.md) him, to [nāsâ](../../strongs/h/h5254.md) him, that he might [yada'](../../strongs/h/h3045.md) all that was in his [lebab](../../strongs/h/h3824.md).

<a name="2chronicles_32_32"></a>2Chronicles 32:32

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md), and his [checed](../../strongs/h/h2617.md), behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [ḥāzôn](../../strongs/h/h2377.md) of [Yᵊšaʿyâ](../../strongs/h/h3470.md) the [nāḇî'](../../strongs/h/h5030.md), the [ben](../../strongs/h/h1121.md) of ['Āmôṣ](../../strongs/h/h531.md), and in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_32_33"></a>2Chronicles 32:33

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him in the [maʿălê](../../strongs/h/h4608.md) of the [qeber](../../strongs/h/h6913.md) of the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md): and all [Yehuwdah](../../strongs/h/h3063.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) did ['asah](../../strongs/h/h6213.md) [kabowd](../../strongs/h/h3519.md) at his [maveth](../../strongs/h/h4194.md). And [Mᵊnaššê](../../strongs/h/h4519.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 31](2chronicles_31.md) - [2Chronicles 33](2chronicles_33.md)