# [2Chronicles 4](https://www.blueletterbible.org/kjv/2chronicles/4)

<a name="2chronicles_4_1"></a>2Chronicles 4:1

Moreover he ['asah](../../strongs/h/h6213.md) a [mizbeach](../../strongs/h/h4196.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), twenty ['ammâ](../../strongs/h/h520.md) the ['ōreḵ](../../strongs/h/h753.md) thereof, and twenty ['ammâ](../../strongs/h/h520.md) the [rōḥaḇ](../../strongs/h/h7341.md) thereof, and ten ['ammâ](../../strongs/h/h520.md) the [qômâ](../../strongs/h/h6967.md) thereof.

<a name="2chronicles_4_2"></a>2Chronicles 4:2

Also he ['asah](../../strongs/h/h6213.md) a [yāṣaq](../../strongs/h/h3332.md) [yam](../../strongs/h/h3220.md) of ten ['ammâ](../../strongs/h/h520.md) from [saphah](../../strongs/h/h8193.md) to [saphah](../../strongs/h/h8193.md), [ʿāḡōl](../../strongs/h/h5696.md) in [cabiyb](../../strongs/h/h5439.md), and five ['ammâ](../../strongs/h/h520.md) the [qômâ](../../strongs/h/h6967.md) thereof; and a [qāv](../../strongs/h/h6957.md) of thirty ['ammâ](../../strongs/h/h520.md) did [cabab](../../strongs/h/h5437.md) it [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_4_3"></a>2Chronicles 4:3

And under it was the [dĕmuwth](../../strongs/h/h1823.md) of [bāqār](../../strongs/h/h1241.md), which did [cabab](../../strongs/h/h5437.md) it [cabiyb](../../strongs/h/h5439.md): ten in an ['ammâ](../../strongs/h/h520.md), [naqaph](../../strongs/h/h5362.md) the [yam](../../strongs/h/h3220.md) [cabiyb](../../strongs/h/h5439.md). Two [ṭûr](../../strongs/h/h2905.md) of [bāqār](../../strongs/h/h1241.md) were [yāṣaq](../../strongs/h/h3332.md), when it was [mûṣāqâ](../../strongs/h/h4166.md).

<a name="2chronicles_4_4"></a>2Chronicles 4:4

It ['amad](../../strongs/h/h5975.md) upon twelve [bāqār](../../strongs/h/h1241.md), three [panah](../../strongs/h/h6437.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and three [panah](../../strongs/h/h6437.md) toward the [yam](../../strongs/h/h3220.md), and three [panah](../../strongs/h/h6437.md) toward the [neḡeḇ](../../strongs/h/h5045.md), and three [panah](../../strongs/h/h6437.md) toward the [mizrach](../../strongs/h/h4217.md): and the [yam](../../strongs/h/h3220.md) was set [maʿal](../../strongs/h/h4605.md) upon them, and all their hinder ['āḥôr](../../strongs/h/h268.md) were [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_4_5"></a>2Chronicles 4:5

And the [ʿăḇî](../../strongs/h/h5672.md) of it was an [ṭep̄aḥ](../../strongs/h/h2947.md), and the [saphah](../../strongs/h/h8193.md) of it like the [ma'aseh](../../strongs/h/h4639.md) of the [saphah](../../strongs/h/h8193.md) of a [kowc](../../strongs/h/h3563.md), with [peraḥ](../../strongs/h/h6525.md) of [šûšan](../../strongs/h/h7799.md); and it [ḥāzaq](../../strongs/h/h2388.md) and [kûl](../../strongs/h/h3557.md) three thousand [baṯ](../../strongs/h/h1324.md).

<a name="2chronicles_4_6"></a>2Chronicles 4:6

He ['asah](../../strongs/h/h6213.md) also ten [kîyôr](../../strongs/h/h3595.md), and [nathan](../../strongs/h/h5414.md) five on the [yamiyn](../../strongs/h/h3225.md), and five on the [śᵊmō'l](../../strongs/h/h8040.md), to [rāḥaṣ](../../strongs/h/h7364.md) in them: such things as they [ma'aseh](../../strongs/h/h4639.md) for the [ʿōlâ](../../strongs/h/h5930.md) they [dûaḥ](../../strongs/h/h1740.md) in them; but the [yam](../../strongs/h/h3220.md) was for the [kōhēn](../../strongs/h/h3548.md) to [rāḥaṣ](../../strongs/h/h7364.md).

<a name="2chronicles_4_7"></a>2Chronicles 4:7

And he ['asah](../../strongs/h/h6213.md) ten [mᵊnôrâ](../../strongs/h/h4501.md) of [zāhāḇ](../../strongs/h/h2091.md) according to their [mishpat](../../strongs/h/h4941.md), and [nathan](../../strongs/h/h5414.md) them in the [heykal](../../strongs/h/h1964.md), five on the [yamiyn](../../strongs/h/h3225.md), and five on the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="2chronicles_4_8"></a>2Chronicles 4:8

He ['asah](../../strongs/h/h6213.md) also ten [šulḥān](../../strongs/h/h7979.md), and [yānaḥ](../../strongs/h/h3240.md) them in the [heykal](../../strongs/h/h1964.md), five on the [yamiyn](../../strongs/h/h3225.md), and five on the [śᵊmō'l](../../strongs/h/h8040.md). And he ['asah](../../strongs/h/h6213.md) an hundred [mizrāq](../../strongs/h/h4219.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_4_9"></a>2Chronicles 4:9

Furthermore he ['asah](../../strongs/h/h6213.md) the [ḥāṣēr](../../strongs/h/h2691.md) of the [kōhēn](../../strongs/h/h3548.md), and the [gadowl](../../strongs/h/h1419.md) [ʿăzārâ](../../strongs/h/h5835.md), and [deleṯ](../../strongs/h/h1817.md) for the [ʿăzārâ](../../strongs/h/h5835.md), and [ṣāp̄â](../../strongs/h/h6823.md) the [deleṯ](../../strongs/h/h1817.md) of them with [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="2chronicles_4_10"></a>2Chronicles 4:10

And he [nathan](../../strongs/h/h5414.md) the [yam](../../strongs/h/h3220.md) on the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [qeḏem](../../strongs/h/h6924.md), over [môl](../../strongs/h/h4136.md) the [neḡeḇ](../../strongs/h/h5045.md).

<a name="2chronicles_4_11"></a>2Chronicles 4:11

And [Ḥûrām](../../strongs/h/h2361.md) ['asah](../../strongs/h/h6213.md) the [sîr](../../strongs/h/h5518.md), and the [yāʿ](../../strongs/h/h3257.md), and the [mizrāq](../../strongs/h/h4219.md). And [Ḥûrām](../../strongs/h/h2361.md) [Ḥîrām](../../strongs/h/h2438.md) [kalah](../../strongs/h/h3615.md) ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) that he was to ['asah](../../strongs/h/h6213.md) for [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) for the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md);

<a name="2chronicles_4_12"></a>2Chronicles 4:12

the two [ʿammûḏ](../../strongs/h/h5982.md), and the [gullâ](../../strongs/h/h1543.md), and the [kōṯereṯ](../../strongs/h/h3805.md) which were on the [ro'sh](../../strongs/h/h7218.md) of the two [ʿammûḏ](../../strongs/h/h5982.md), and the two [śᵊḇāḵâ](../../strongs/h/h7639.md) to [kāsâ](../../strongs/h/h3680.md) the two [gullâ](../../strongs/h/h1543.md) of the [kōṯereṯ](../../strongs/h/h3805.md) which were on the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md);

<a name="2chronicles_4_13"></a>2Chronicles 4:13

And four hundred [rimmôn](../../strongs/h/h7416.md) on the two [śᵊḇāḵâ](../../strongs/h/h7639.md); two [ṭûr](../../strongs/h/h2905.md) of [rimmôn](../../strongs/h/h7416.md) on each [śᵊḇāḵâ](../../strongs/h/h7639.md), to [kāsâ](../../strongs/h/h3680.md) the two [gullâ](../../strongs/h/h1543.md) of the [kōṯereṯ](../../strongs/h/h3805.md) which were [paniym](../../strongs/h/h6440.md) the [ʿammûḏ](../../strongs/h/h5982.md).

<a name="2chronicles_4_14"></a>2Chronicles 4:14

He ['asah](../../strongs/h/h6213.md) also [mᵊḵônâ](../../strongs/h/h4350.md), and [kîyôr](../../strongs/h/h3595.md) ['asah](../../strongs/h/h6213.md) he upon the [mᵊḵônâ](../../strongs/h/h4350.md);

<a name="2chronicles_4_15"></a>2Chronicles 4:15

One [yam](../../strongs/h/h3220.md), and twelve [bāqār](../../strongs/h/h1241.md) under it.

<a name="2chronicles_4_16"></a>2Chronicles 4:16

The [sîr](../../strongs/h/h5518.md) also, and the [yāʿ](../../strongs/h/h3257.md), and the [mazlēḡ](../../strongs/h/h4207.md), and all their [kĕliy](../../strongs/h/h3627.md), did [Ḥûrām](../../strongs/h/h2361.md) his ['ab](../../strongs/h/h1.md) ['asah](../../strongs/h/h6213.md) to [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) for the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) of [māraq](../../strongs/h/h4838.md) [nᵊḥšeṯ](../../strongs/h/h5178.md).

<a name="2chronicles_4_17"></a>2Chronicles 4:17

In the [kikār](../../strongs/h/h3603.md) of [Yardēn](../../strongs/h/h3383.md) did the [melek](../../strongs/h/h4428.md) [yāṣaq](../../strongs/h/h3332.md) them, in the ['ab](../../strongs/h/h5645.md) ['ăḏāmâ](../../strongs/h/h127.md) between [Sukôṯ](../../strongs/h/h5523.md) and [Ṣᵊrāḏâ](../../strongs/h/h6868.md).

<a name="2chronicles_4_18"></a>2Chronicles 4:18

Thus [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) all these [kĕliy](../../strongs/h/h3627.md) in [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md): for the [mišqāl](../../strongs/h/h4948.md) of the [nᵊḥšeṯ](../../strongs/h/h5178.md) could not be [chaqar](../../strongs/h/h2713.md).

<a name="2chronicles_4_19"></a>2Chronicles 4:19

And [Šᵊlōmô](../../strongs/h/h8010.md) ['asah](../../strongs/h/h6213.md) all the [kĕliy](../../strongs/h/h3627.md) that were for the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), the [zāhāḇ](../../strongs/h/h2091.md) [mizbeach](../../strongs/h/h4196.md) also, and the [šulḥān](../../strongs/h/h7979.md) whereon the [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) was set;

<a name="2chronicles_4_20"></a>2Chronicles 4:20

Moreover the [mᵊnôrâ](../../strongs/h/h4501.md) with their [nîr](../../strongs/h/h5216.md), that they should [bāʿar](../../strongs/h/h1197.md) after the [mishpat](../../strongs/h/h4941.md) [paniym](../../strongs/h/h6440.md) the [dᵊḇîr](../../strongs/h/h1687.md), of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md);

<a name="2chronicles_4_21"></a>2Chronicles 4:21

And the [peraḥ](../../strongs/h/h6525.md), and the [nîr](../../strongs/h/h5216.md), and the [malqāḥayim](../../strongs/h/h4457.md), made he of [zāhāḇ](../../strongs/h/h2091.md), and that [miḵlâ](../../strongs/h/h4357.md) [zāhāḇ](../../strongs/h/h2091.md);

<a name="2chronicles_4_22"></a>2Chronicles 4:22

And the [mᵊzammerê](../../strongs/h/h4212.md), and the [mizrāq](../../strongs/h/h4219.md), and the [kaph](../../strongs/h/h3709.md), and the [maḥtâ](../../strongs/h/h4289.md), of [cagar](../../strongs/h/h5462.md) [zāhāḇ](../../strongs/h/h2091.md): and the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md), the [pᵊnîmî](../../strongs/h/h6442.md) [deleṯ](../../strongs/h/h1817.md) thereof for the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) place, and the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md) of the [heykal](../../strongs/h/h1964.md), were of [zāhāḇ](../../strongs/h/h2091.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 3](2chronicles_3.md) - [2Chronicles 5](2chronicles_5.md)