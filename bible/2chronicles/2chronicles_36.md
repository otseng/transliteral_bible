# [2Chronicles 36](https://www.blueletterbible.org/kjv/2chronicles/36)

<a name="2chronicles_36_1"></a>2Chronicles 36:1

Then the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [laqach](../../strongs/h/h3947.md) [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md), and made him [mālaḵ](../../strongs/h/h4427.md) in his ['ab](../../strongs/h/h1.md) stead in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_36_2"></a>2Chronicles 36:2

[Yô'Āḥāz](../../strongs/h/h3099.md) was twenty and three [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) three [ḥōḏeš](../../strongs/h/h2320.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_36_3"></a>2Chronicles 36:3

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [cuwr](../../strongs/h/h5493.md) him at [Yĕruwshalaim](../../strongs/h/h3389.md), and [ʿānaš](../../strongs/h/h6064.md) the ['erets](../../strongs/h/h776.md) in an hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md) and a [kikār](../../strongs/h/h3603.md) of [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_36_4"></a>2Chronicles 36:4

And the [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) made ['Elyāqîm](../../strongs/h/h471.md) his ['ach](../../strongs/h/h251.md) [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), and [cabab](../../strongs/h/h5437.md) his [shem](../../strongs/h/h8034.md) to [Yᵊhôyāqîm](../../strongs/h/h3079.md). And [Nᵊḵô](../../strongs/h/h5224.md) [laqach](../../strongs/h/h3947.md) [Yô'Āḥāz](../../strongs/h/h3099.md) his ['ach](../../strongs/h/h251.md), and [bow'](../../strongs/h/h935.md) him to [Mitsrayim](../../strongs/h/h4714.md).

<a name="2chronicles_36_5"></a>2Chronicles 36:5

[Yᵊhôyāqîm](../../strongs/h/h3079.md) was twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) eleven [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): and he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_36_6"></a>2Chronicles 36:6

Against him [ʿālâ](../../strongs/h/h5927.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and ['āsar](../../strongs/h/h631.md) him in [nᵊḥšeṯ](../../strongs/h/h5178.md), to [yālaḵ](../../strongs/h/h3212.md) him to [Bāḇel](../../strongs/h/h894.md).

<a name="2chronicles_36_7"></a>2Chronicles 36:7

[Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) also [bow'](../../strongs/h/h935.md) of the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) to [Bāḇel](../../strongs/h/h894.md), and [nathan](../../strongs/h/h5414.md) them in his [heykal](../../strongs/h/h1964.md) at [Bāḇel](../../strongs/h/h894.md).

<a name="2chronicles_36_8"></a>2Chronicles 36:8

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md), and his [tôʿēḇâ](../../strongs/h/h8441.md) which he ['asah](../../strongs/h/h6213.md), and that which was [māṣā'](../../strongs/h/h4672.md) in him, behold, they are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md): and [Yᵊhôyāḵîn](../../strongs/h/h3078.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2chronicles_36_9"></a>2Chronicles 36:9

[Yᵊhôyāḵîn](../../strongs/h/h3078.md) was eight [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) three [ḥōḏeš](../../strongs/h/h2320.md) and ten [yowm](../../strongs/h/h3117.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): and he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_36_10"></a>2Chronicles 36:10

And when the [šānâ](../../strongs/h/h8141.md) was [tᵊšûḇâ](../../strongs/h/h8666.md), [melek](../../strongs/h/h4428.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [shalach](../../strongs/h/h7971.md), and [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md), with the [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and made [Ṣḏqyh](../../strongs/h/h6667.md) his ['ach](../../strongs/h/h251.md) [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_36_11"></a>2Chronicles 36:11

[Ṣḏqyh](../../strongs/h/h6667.md) was one and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) eleven [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_36_12"></a>2Chronicles 36:12

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and [kānaʿ](../../strongs/h/h3665.md) not himself [paniym](../../strongs/h/h6440.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) from the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_36_13"></a>2Chronicles 36:13

And he also [māraḏ](../../strongs/h/h4775.md) against [melek](../../strongs/h/h4428.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md), who had made him [shaba'](../../strongs/h/h7650.md) by ['Elohiym](../../strongs/h/h430.md): but he [qāšâ](../../strongs/h/h7185.md) his [ʿōrep̄](../../strongs/h/h6203.md), and ['amats](../../strongs/h/h553.md) his [lebab](../../strongs/h/h3824.md) from [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_36_14"></a>2Chronicles 36:14

Moreover all the [śar](../../strongs/h/h8269.md) of the [kōhēn](../../strongs/h/h3548.md), and the ['am](../../strongs/h/h5971.md), [māʿal](../../strongs/h/h4603.md) [maʿal](../../strongs/h/h4604.md) [rabah](../../strongs/h/h7235.md) after all the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md); and [ṭāmē'](../../strongs/h/h2930.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) which he had [qadash](../../strongs/h/h6942.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_36_15"></a>2Chronicles 36:15

And [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md) [shalach](../../strongs/h/h7971.md) to them [yad](../../strongs/h/h3027.md) his [mal'ak](../../strongs/h/h4397.md), [šāḵam](../../strongs/h/h7925.md), and [shalach](../../strongs/h/h7971.md); because he had [ḥāmal](../../strongs/h/h2550.md) on his ['am](../../strongs/h/h5971.md), and on his [māʿôn](../../strongs/h/h4583.md):

<a name="2chronicles_36_16"></a>2Chronicles 36:16

But they [lāʿaḇ](../../strongs/h/h3931.md) the [mal'ak](../../strongs/h/h4397.md) of ['Elohiym](../../strongs/h/h430.md), and [bazah](../../strongs/h/h959.md) his [dabar](../../strongs/h/h1697.md), and [tāʿaʿ](../../strongs/h/h8591.md) his [nāḇî'](../../strongs/h/h5030.md), until the [chemah](../../strongs/h/h2534.md) of [Yĕhovah](../../strongs/h/h3068.md) [ʿālâ](../../strongs/h/h5927.md) against his ['am](../../strongs/h/h5971.md), till there was no [marpē'](../../strongs/h/h4832.md).

<a name="2chronicles_36_17"></a>2Chronicles 36:17

Therefore he [ʿālâ](../../strongs/h/h5927.md) upon them the [melek](../../strongs/h/h4428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), who [harag](../../strongs/h/h2026.md) their [bāḥûr](../../strongs/h/h970.md) with the [chereb](../../strongs/h/h2719.md) in the [bayith](../../strongs/h/h1004.md) of their [miqdash](../../strongs/h/h4720.md), and had no [ḥāmal](../../strongs/h/h2550.md) upon [bāḥûr](../../strongs/h/h970.md) or [bᵊṯûlâ](../../strongs/h/h1330.md), old [zāqēn](../../strongs/h/h2205.md), or him that [yāšēš](../../strongs/h/h3486.md) : he [nathan](../../strongs/h/h5414.md) them all into his [yad](../../strongs/h/h3027.md).

<a name="2chronicles_36_18"></a>2Chronicles 36:18

And all the [kĕliy](../../strongs/h/h3627.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), [gadowl](../../strongs/h/h1419.md) and [qāṭān](../../strongs/h/h6996.md), and the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md), and of his [śar](../../strongs/h/h8269.md); all these he [bow'](../../strongs/h/h935.md) to [Bāḇel](../../strongs/h/h894.md).

<a name="2chronicles_36_19"></a>2Chronicles 36:19

And they [śārap̄](../../strongs/h/h8313.md) the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and [nāṯaṣ](../../strongs/h/h5422.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and [śārap̄](../../strongs/h/h8313.md) all the ['armôn](../../strongs/h/h759.md) thereof with ['esh](../../strongs/h/h784.md), and [shachath](../../strongs/h/h7843.md) all the [maḥmāḏ](../../strongs/h/h4261.md) [kĕliy](../../strongs/h/h3627.md) thereof.

<a name="2chronicles_36_20"></a>2Chronicles 36:20

And them that had [šᵊ'ērîṯ](../../strongs/h/h7611.md) from the [chereb](../../strongs/h/h2719.md) he [gālâ](../../strongs/h/h1540.md) to [Bāḇel](../../strongs/h/h894.md); where they were ['ebed](../../strongs/h/h5650.md) to him and his [ben](../../strongs/h/h1121.md) until the [mālaḵ](../../strongs/h/h4427.md) of the [malkuwth](../../strongs/h/h4438.md) of [Pāras](../../strongs/h/h6539.md):

<a name="2chronicles_36_21"></a>2Chronicles 36:21

To [mālā'](../../strongs/h/h4390.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md), until the ['erets](../../strongs/h/h776.md) had [ratsah](../../strongs/h/h7521.md) her [shabbath](../../strongs/h/h7676.md): for as [yowm](../../strongs/h/h3117.md) she [šāmēm](../../strongs/h/h8074.md) she [shabath](../../strongs/h/h7673.md), to [mālā'](../../strongs/h/h4390.md) threescore and ten [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_36_22"></a>2Chronicles 36:22

Now in the first [šānâ](../../strongs/h/h8141.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) might be [kalah](../../strongs/h/h3615.md), [Yĕhovah](../../strongs/h/h3068.md) [ʿûr](../../strongs/h/h5782.md) the [ruwach](../../strongs/h/h7307.md) of [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), that he made an ['abar](../../strongs/h/h5674.md) [qowl](../../strongs/h/h6963.md) throughout all his [malkuwth](../../strongs/h/h4438.md), and put it also in [miḵtāḇ](../../strongs/h/h4385.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_36_23"></a>2Chronicles 36:23

Thus ['āmar](../../strongs/h/h559.md) [Kôreš](../../strongs/h/h3566.md) [melek](../../strongs/h/h4428.md) of [Pāras](../../strongs/h/h6539.md), All the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) hath [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [shamayim](../../strongs/h/h8064.md) [nathan](../../strongs/h/h5414.md) me; and he hath [paqad](../../strongs/h/h6485.md) me to [bānâ](../../strongs/h/h1129.md) him a [bayith](../../strongs/h/h1004.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), which is in [Yehuwdah](../../strongs/h/h3063.md). Who is there among you of all his ['am](../../strongs/h/h5971.md)? [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) be with him, and let him [ʿālâ](../../strongs/h/h5927.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 35](2chronicles_35.md)