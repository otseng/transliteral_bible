# [2Chronicles 3](https://www.blueletterbible.org/kjv/2chronicles/3)

<a name="2chronicles_3_1"></a>2Chronicles 3:1

Then [Šᵊlōmô](../../strongs/h/h8010.md) [ḥālal](../../strongs/h/h2490.md) to [bānâ](../../strongs/h/h1129.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) in [har](../../strongs/h/h2022.md) [Môrîyâ](../../strongs/h/h4179.md), where the LORD [ra'ah](../../strongs/h/h7200.md) unto [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md), in the [maqowm](../../strongs/h/h4725.md) that [Dāviḏ](../../strongs/h/h1732.md) had [kuwn](../../strongs/h/h3559.md) in the [gōren](../../strongs/h/h1637.md) of ['Ārnān](../../strongs/h/h771.md) the [Yᵊḇûsî](../../strongs/h/h2983.md).

<a name="2chronicles_3_2"></a>2Chronicles 3:2

And he [ḥālal](../../strongs/h/h2490.md) to [bānâ](../../strongs/h/h1129.md) in the second day of the second [ḥōḏeš](../../strongs/h/h2320.md), in the fourth [šānâ](../../strongs/h/h8141.md) of his [malkuwth](../../strongs/h/h4438.md).

<a name="2chronicles_3_3"></a>2Chronicles 3:3

Now these are the things wherein [Šᵊlōmô](../../strongs/h/h8010.md) was [yacad](../../strongs/h/h3245.md) for the [bānâ](../../strongs/h/h1129.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md). The ['ōreḵ](../../strongs/h/h753.md) by ['ammâ](../../strongs/h/h520.md) after the [ri'šôn](../../strongs/h/h7223.md) [midâ](../../strongs/h/h4060.md) was threescore ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) twenty ['ammâ](../../strongs/h/h520.md).

<a name="2chronicles_3_4"></a>2Chronicles 3:4

And the ['ûlām](../../strongs/h/h197.md) that was in the [paniym](../../strongs/h/h6440.md), the ['ōreḵ](../../strongs/h/h753.md) of it was according to the [rōḥaḇ](../../strongs/h/h7341.md) of the [bayith](../../strongs/h/h1004.md), twenty ['ammâ](../../strongs/h/h520.md), and the [gobahh](../../strongs/h/h1363.md) was an hundred and twenty: and he [ṣāp̄â](../../strongs/h/h6823.md) it [pᵊnîmâ](../../strongs/h/h6441.md) with [tahowr](../../strongs/h/h2889.md) [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_3_5"></a>2Chronicles 3:5

And the [gadowl](../../strongs/h/h1419.md) [bayith](../../strongs/h/h1004.md) he [ḥāp̄â](../../strongs/h/h2645.md) with [bᵊrôš](../../strongs/h/h1265.md) ['ets](../../strongs/h/h6086.md), which he [ḥāp̄â](../../strongs/h/h2645.md) with [towb](../../strongs/h/h2896.md) [zāhāḇ](../../strongs/h/h2091.md), and [ʿālâ](../../strongs/h/h5927.md) thereon [timmōrâ](../../strongs/h/h8561.md) and [šaršᵊrâ](../../strongs/h/h8333.md).

<a name="2chronicles_3_6"></a>2Chronicles 3:6

And he [ṣāp̄â](../../strongs/h/h6823.md) the [bayith](../../strongs/h/h1004.md) with [yāqār](../../strongs/h/h3368.md) ['eben](../../strongs/h/h68.md) for [tip̄'ārâ](../../strongs/h/h8597.md): and the [zāhāḇ](../../strongs/h/h2091.md) was [zāhāḇ](../../strongs/h/h2091.md) of [Parvayim](../../strongs/h/h6516.md).

<a name="2chronicles_3_7"></a>2Chronicles 3:7

He [ḥāp̄â](../../strongs/h/h2645.md) also the [bayith](../../strongs/h/h1004.md), the [qôrâ](../../strongs/h/h6982.md), the [caph](../../strongs/h/h5592.md), and the [qîr](../../strongs/h/h7023.md) thereof, and the [deleṯ](../../strongs/h/h1817.md) thereof, with [zāhāḇ](../../strongs/h/h2091.md); and [pāṯaḥ](../../strongs/h/h6605.md) [kĕruwb](../../strongs/h/h3742.md) on the [qîr](../../strongs/h/h7023.md).

<a name="2chronicles_3_8"></a>2Chronicles 3:8

And he ['asah](../../strongs/h/h6213.md) the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) [bayith](../../strongs/h/h1004.md), the ['ōreḵ](../../strongs/h/h753.md) whereof was according [paniym](../../strongs/h/h6440.md) the [rōḥaḇ](../../strongs/h/h7341.md) of the [bayith](../../strongs/h/h1004.md), twenty ['ammâ](../../strongs/h/h520.md), and the [rōḥaḇ](../../strongs/h/h7341.md) thereof twenty ['ammâ](../../strongs/h/h520.md): and he [ḥāp̄â](../../strongs/h/h2645.md) it with [towb](../../strongs/h/h2896.md) [zāhāḇ](../../strongs/h/h2091.md), amounting to six hundred [kikār](../../strongs/h/h3603.md).

<a name="2chronicles_3_9"></a>2Chronicles 3:9

And the [mišqāl](../../strongs/h/h4948.md) of the [masmēr](../../strongs/h/h4548.md) was fifty [šeqel](../../strongs/h/h8255.md) of [zāhāḇ](../../strongs/h/h2091.md). And he [ḥāp̄â](../../strongs/h/h2645.md) the upper [ʿălîyâ](../../strongs/h/h5944.md) with [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_3_10"></a>2Chronicles 3:10

And in the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) [bayith](../../strongs/h/h1004.md) he ['asah](../../strongs/h/h6213.md) two [kĕruwb](../../strongs/h/h3742.md) of [ṣaʿăṣuʿîm](../../strongs/h/h6816.md) [ma'aseh](../../strongs/h/h4639.md), and [ṣāp̄â](../../strongs/h/h6823.md) them with [zāhāḇ](../../strongs/h/h2091.md).

<a name="2chronicles_3_11"></a>2Chronicles 3:11

And the [kanaph](../../strongs/h/h3671.md) of the [kĕruwb](../../strongs/h/h3742.md) were twenty ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md): one [kanaph](../../strongs/h/h3671.md) of the one cherub was five ['ammâ](../../strongs/h/h520.md), [naga'](../../strongs/h/h5060.md) to the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md): and the ['aḥēr](../../strongs/h/h312.md) [kanaph](../../strongs/h/h3671.md) was likewise five ['ammâ](../../strongs/h/h520.md), [naga'](../../strongs/h/h5060.md) to the [kanaph](../../strongs/h/h3671.md) of the ['aḥēr](../../strongs/h/h312.md) [kĕruwb](../../strongs/h/h3742.md).

<a name="2chronicles_3_12"></a>2Chronicles 3:12

And one [kanaph](../../strongs/h/h3671.md) of the other [kĕruwb](../../strongs/h/h3742.md) was five ['ammâ](../../strongs/h/h520.md), [naga'](../../strongs/h/h5060.md) to the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md): and the ['aḥēr](../../strongs/h/h312.md) [kanaph](../../strongs/h/h3671.md) was five ['ammâ](../../strongs/h/h520.md) also, [dāḇēq](../../strongs/h/h1695.md) to the [kanaph](../../strongs/h/h3671.md) of the ['aḥēr](../../strongs/h/h312.md) [kĕruwb](../../strongs/h/h3742.md).

<a name="2chronicles_3_13"></a>2Chronicles 3:13

The [kanaph](../../strongs/h/h3671.md) of these [kĕruwb](../../strongs/h/h3742.md) [pāraś](../../strongs/h/h6566.md) themselves twenty ['ammâ](../../strongs/h/h520.md): and they ['amad](../../strongs/h/h5975.md) on their [regel](../../strongs/h/h7272.md), and their [paniym](../../strongs/h/h6440.md) were [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_3_14"></a>2Chronicles 3:14

And he ['asah](../../strongs/h/h6213.md) the [pārōḵeṯ](../../strongs/h/h6532.md) of [tᵊḵēleṯ](../../strongs/h/h8504.md), and ['argāmān](../../strongs/h/h713.md), and [karmîl](../../strongs/h/h3758.md), and [bûṣ](../../strongs/h/h948.md), and [ʿālâ](../../strongs/h/h5927.md) [kĕruwb](../../strongs/h/h3742.md) thereon.

<a name="2chronicles_3_15"></a>2Chronicles 3:15

Also he ['asah](../../strongs/h/h6213.md) [paniym](../../strongs/h/h6440.md) the [bayith](../../strongs/h/h1004.md) two [ʿammûḏ](../../strongs/h/h5982.md) of thirty and five ['ammâ](../../strongs/h/h520.md) ['ōreḵ](../../strongs/h/h753.md), and the [ṣep̄eṯ](../../strongs/h/h6858.md) that was on the [ro'sh](../../strongs/h/h7218.md) of each of them was five ['ammâ](../../strongs/h/h520.md).

<a name="2chronicles_3_16"></a>2Chronicles 3:16

And he ['asah](../../strongs/h/h6213.md) [šaršᵊrâ](../../strongs/h/h8333.md), as in the [dᵊḇîr](../../strongs/h/h1687.md), and [nathan](../../strongs/h/h5414.md) them on the [ro'sh](../../strongs/h/h7218.md) of the [ʿammûḏ](../../strongs/h/h5982.md); and ['asah](../../strongs/h/h6213.md) an hundred [rimmôn](../../strongs/h/h7416.md), and [nathan](../../strongs/h/h5414.md) them on the [šaršᵊrâ](../../strongs/h/h8333.md).

<a name="2chronicles_3_17"></a>2Chronicles 3:17

And he [quwm](../../strongs/h/h6965.md) the [ʿammûḏ](../../strongs/h/h5982.md) [paniym](../../strongs/h/h6440.md) the [heykal](../../strongs/h/h1964.md), one on the [yamiyn](../../strongs/h/h3225.md), and the other on the [śᵊmō'l](../../strongs/h/h8040.md); and [qara'](../../strongs/h/h7121.md) the [shem](../../strongs/h/h8034.md) of that on the right [yᵊmānî](../../strongs/h/h3233.md) [yᵊmînî](../../strongs/h/h3227.md) [Yāḵîn](../../strongs/h/h3199.md), and the [shem](../../strongs/h/h8034.md) of that on the [śᵊmā'lî](../../strongs/h/h8042.md) [BōʿAz](../../strongs/h/h1162.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 2](2chronicles_2.md) - [2Chronicles 4](2chronicles_4.md)