# [2Chronicles 10](https://www.blueletterbible.org/kjv/2chronicles/10)

<a name="2chronicles_10_1"></a>2Chronicles 10:1

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [yālaḵ](../../strongs/h/h3212.md) to [Šᵊḵem](../../strongs/h/h7927.md): for to [Šᵊḵem](../../strongs/h/h7927.md) were all [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to make him [mālaḵ](../../strongs/h/h4427.md).

<a name="2chronicles_10_2"></a>2Chronicles 10:2

And it came to pass, when [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), who was in [Mitsrayim](../../strongs/h/h4714.md), whither he had [bāraḥ](../../strongs/h/h1272.md) from the [paniym](../../strongs/h/h6440.md) of [Šᵊlōmô](../../strongs/h/h8010.md) the [melek](../../strongs/h/h4428.md), [shama'](../../strongs/h/h8085.md) it, that [YārāḇʿĀm](../../strongs/h/h3379.md) [shuwb](../../strongs/h/h7725.md) out of [Mitsrayim](../../strongs/h/h4714.md).

<a name="2chronicles_10_3"></a>2Chronicles 10:3

And they [shalach](../../strongs/h/h7971.md) and [qara'](../../strongs/h/h7121.md) him. So [YārāḇʿĀm](../../strongs/h/h3379.md) and all [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) and [dabar](../../strongs/h/h1696.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_10_4"></a>2Chronicles 10:4

Thy ['ab](../../strongs/h/h1.md) made our [ʿōl](../../strongs/h/h5923.md) [qāšâ](../../strongs/h/h7185.md): now therefore [qālal](../../strongs/h/h7043.md) thou somewhat the [qāšê](../../strongs/h/h7186.md) [ʿăḇōḏâ](../../strongs/h/h5656.md) of thy ['ab](../../strongs/h/h1.md), and his [kāḇēḏ](../../strongs/h/h3515.md) [ʿōl](../../strongs/h/h5923.md) that he [nathan](../../strongs/h/h5414.md) upon us, and we will ['abad](../../strongs/h/h5647.md) thee.

<a name="2chronicles_10_5"></a>2Chronicles 10:5

And he ['āmar](../../strongs/h/h559.md) unto them, [shuwb](../../strongs/h/h7725.md) unto me after three [yowm](../../strongs/h/h3117.md). And the ['am](../../strongs/h/h5971.md) [yālaḵ](../../strongs/h/h3212.md).

<a name="2chronicles_10_6"></a>2Chronicles 10:6

And [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) took [ya'ats](../../strongs/h/h3289.md) with the [zāqēn](../../strongs/h/h2205.md) men that had ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Šᵊlōmô](../../strongs/h/h8010.md) his ['ab](../../strongs/h/h1.md) while he yet [chay](../../strongs/h/h2416.md), ['āmar](../../strongs/h/h559.md), What [ya'ats](../../strongs/h/h3289.md) ye me to [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) to this ['am](../../strongs/h/h5971.md)?

<a name="2chronicles_10_7"></a>2Chronicles 10:7

And they [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), If thou be [towb](../../strongs/h/h2896.md) to this ['am](../../strongs/h/h5971.md), and [ratsah](../../strongs/h/h7521.md) them, and [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) to them, they will be thy ['ebed](../../strongs/h/h5650.md) for [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_10_8"></a>2Chronicles 10:8

But he ['azab](../../strongs/h/h5800.md) the ['etsah](../../strongs/h/h6098.md) which the old [zāqēn](../../strongs/h/h2205.md) [ya'ats](../../strongs/h/h3289.md) him, and took [ya'ats](../../strongs/h/h3289.md) with the [yeleḏ](../../strongs/h/h3206.md) that were [gāḏal](../../strongs/h/h1431.md) with him, that ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him.

<a name="2chronicles_10_9"></a>2Chronicles 10:9

And he ['āmar](../../strongs/h/h559.md) unto them, What [ya'ats](../../strongs/h/h3289.md) give ye that we may [shuwb](../../strongs/h/h7725.md) [dabar](../../strongs/h/h1697.md) to this ['am](../../strongs/h/h5971.md), which have [dabar](../../strongs/h/h1696.md) to me, ['āmar](../../strongs/h/h559.md), [qālal](../../strongs/h/h7043.md) somewhat the [ʿōl](../../strongs/h/h5923.md) that thy ['ab](../../strongs/h/h1.md) did [nathan](../../strongs/h/h5414.md) upon us?

<a name="2chronicles_10_10"></a>2Chronicles 10:10

And the [yeleḏ](../../strongs/h/h3206.md) that were [gāḏal](../../strongs/h/h1431.md) with him [dabar](../../strongs/h/h1696.md) unto him, ['āmar](../../strongs/h/h559.md), Thus shalt thou ['āmar](../../strongs/h/h559.md) the ['am](../../strongs/h/h5971.md) that [dabar](../../strongs/h/h1696.md) unto thee, ['āmar](../../strongs/h/h559.md), Thy ['ab](../../strongs/h/h1.md) [kabad](../../strongs/h/h3513.md) our [ʿōl](../../strongs/h/h5923.md) [kabad](../../strongs/h/h3513.md), but make thou it somewhat [qālal](../../strongs/h/h7043.md) for us; thus shalt thou ['āmar](../../strongs/h/h559.md) unto them, My [qōṭen](../../strongs/h/h6995.md) shall be [ʿāḇâ](../../strongs/h/h5666.md) than my ['ab](../../strongs/h/h1.md) [māṯnayim](../../strongs/h/h4975.md).

<a name="2chronicles_10_11"></a>2Chronicles 10:11

For whereas my ['ab](../../strongs/h/h1.md) [ʿāmas](../../strongs/h/h6006.md) a [kāḇēḏ](../../strongs/h/h3515.md) [ʿōl](../../strongs/h/h5923.md) upon you, I will put more to your [ʿōl](../../strongs/h/h5923.md): my ['ab](../../strongs/h/h1.md) [yacar](../../strongs/h/h3256.md) you with [šôṭ](../../strongs/h/h7752.md), but I will with [ʿaqrāḇ](../../strongs/h/h6137.md).

<a name="2chronicles_10_12"></a>2Chronicles 10:12

So [YārāḇʿĀm](../../strongs/h/h3379.md) and all the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) on the third [yowm](../../strongs/h/h3117.md), as the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) to me on the third [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_10_13"></a>2Chronicles 10:13

And the [melek](../../strongs/h/h4428.md) ['anah](../../strongs/h/h6030.md) them [qāšê](../../strongs/h/h7186.md); and [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) ['azab](../../strongs/h/h5800.md) the ['etsah](../../strongs/h/h6098.md) of the [zāqēn](../../strongs/h/h2205.md),

<a name="2chronicles_10_14"></a>2Chronicles 10:14

And [dabar](../../strongs/h/h1696.md) them after the ['etsah](../../strongs/h/h6098.md) of the [yeleḏ](../../strongs/h/h3206.md), ['āmar](../../strongs/h/h559.md), My ['ab](../../strongs/h/h1.md) made your [ʿōl](../../strongs/h/h5923.md) [kabad](../../strongs/h/h3513.md), but I will add thereto: my ['ab](../../strongs/h/h1.md) [yacar](../../strongs/h/h3256.md) you with [šôṭ](../../strongs/h/h7752.md), but I will with [ʿaqrāḇ](../../strongs/h/h6137.md).

<a name="2chronicles_10_15"></a>2Chronicles 10:15

So the [melek](../../strongs/h/h4428.md) [shama'](../../strongs/h/h8085.md) not unto the ['am](../../strongs/h/h5971.md): for the [nᵊsibâ](../../strongs/h/h5252.md) was of ['Elohiym](../../strongs/h/h430.md), that [Yĕhovah](../../strongs/h/h3068.md) might [quwm](../../strongs/h/h6965.md) his [dabar](../../strongs/h/h1697.md), which he [dabar](../../strongs/h/h1696.md) by the [yad](../../strongs/h/h3027.md) of ['Ăḥîyâ](../../strongs/h/h281.md) the [Šîlōnî](../../strongs/h/h7888.md) to [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md).

<a name="2chronicles_10_16"></a>2Chronicles 10:16

And when all [Yisra'el](../../strongs/h/h3478.md) [ra'ah](../../strongs/h/h7200.md) that the [melek](../../strongs/h/h4428.md) would not [shama'](../../strongs/h/h8085.md) unto them, the ['am](../../strongs/h/h5971.md) [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), What [cheleq](../../strongs/h/h2506.md) have we in [Dāviḏ](../../strongs/h/h1732.md)? and we have none [nachalah](../../strongs/h/h5159.md) in the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md): every ['iysh](../../strongs/h/h376.md) to your ['ohel](../../strongs/h/h168.md), O [Yisra'el](../../strongs/h/h3478.md): and now, [Dāviḏ](../../strongs/h/h1732.md), [ra'ah](../../strongs/h/h7200.md) to thine own [bayith](../../strongs/h/h1004.md). So all [Yisra'el](../../strongs/h/h3478.md) [yālaḵ](../../strongs/h/h3212.md) to their ['ohel](../../strongs/h/h168.md).

<a name="2chronicles_10_17"></a>2Chronicles 10:17

But as for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [mālaḵ](../../strongs/h/h4427.md) over them.

<a name="2chronicles_10_18"></a>2Chronicles 10:18

Then [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [shalach](../../strongs/h/h7971.md) [Hăḏôrām](../../strongs/h/h1913.md) that was over the [mas](../../strongs/h/h4522.md); and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md), that he [muwth](../../strongs/h/h4191.md). But [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) made ['amats](../../strongs/h/h553.md) to get him [ʿālâ](../../strongs/h/h5927.md) to his [merkāḇâ](../../strongs/h/h4818.md), to [nûs](../../strongs/h/h5127.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_10_19"></a>2Chronicles 10:19


And [Yisra'el](../../strongs/h/h3478.md) [pāšaʿ](../../strongs/h/h6586.md) against the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md) unto this [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 9](2chronicles_9.md) - [2Chronicles 11](2chronicles_11.md)