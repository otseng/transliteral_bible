# [2Chronicles 31](https://www.blueletterbible.org/kjv/2chronicles/31)

<a name="2chronicles_31_1"></a>2Chronicles 31:1

Now when all this was [kalah](../../strongs/h/h3615.md), all [Yisra'el](../../strongs/h/h3478.md) that were [māṣā'](../../strongs/h/h4672.md) [yāṣā'](../../strongs/h/h3318.md) to the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [shabar](../../strongs/h/h7665.md) the [maṣṣēḇâ](../../strongs/h/h4676.md) in [shabar](../../strongs/h/h7665.md), and [gāḏaʿ](../../strongs/h/h1438.md) the ['ăšērâ](../../strongs/h/h842.md), and [nāṯaṣ](../../strongs/h/h5422.md) the [bāmâ](../../strongs/h/h1116.md) and the [mizbeach](../../strongs/h/h4196.md) out of all [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), in ['Ep̄rayim](../../strongs/h/h669.md) also and [Mᵊnaššê](../../strongs/h/h4519.md), until they had [kalah](../../strongs/h/h3615.md) them all. Then all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md), every ['iysh](../../strongs/h/h376.md) to his ['achuzzah](../../strongs/h/h272.md), into their own [ʿîr](../../strongs/h/h5892.md).

<a name="2chronicles_31_2"></a>2Chronicles 31:2

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) ['amad](../../strongs/h/h5975.md) the [maḥălōqeṯ](../../strongs/h/h4256.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) after their [maḥălōqeṯ](../../strongs/h/h4256.md), every ['iysh](../../strongs/h/h376.md) [peh](../../strongs/h/h6310.md) to his [ʿăḇōḏâ](../../strongs/h/h5656.md), the [kōhēn](../../strongs/h/h3548.md) and [Lᵊvî](../../strongs/h/h3881.md) for [ʿōlâ](../../strongs/h/h5930.md) and for [šelem](../../strongs/h/h8002.md), to [sharath](../../strongs/h/h8334.md), and to [yadah](../../strongs/h/h3034.md), and to [halal](../../strongs/h/h1984.md) in the [sha'ar](../../strongs/h/h8179.md) of the [maḥănê](../../strongs/h/h4264.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_31_3"></a>2Chronicles 31:3

He appointed also the [melek](../../strongs/h/h4428.md) [mᵊnāṯ](../../strongs/h/h4521.md) of his [rᵊḵûš](../../strongs/h/h7399.md) for the [ʿōlâ](../../strongs/h/h5930.md), to wit, for the [boqer](../../strongs/h/h1242.md) and ['ereb](../../strongs/h/h6153.md) [ʿōlâ](../../strongs/h/h5930.md), and the [ʿōlâ](../../strongs/h/h5930.md) for the [shabbath](../../strongs/h/h7676.md), and for the [ḥōḏeš](../../strongs/h/h2320.md), and for the [môʿēḏ](../../strongs/h/h4150.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_31_4"></a>2Chronicles 31:4

Moreover he ['āmar](../../strongs/h/h559.md) the ['am](../../strongs/h/h5971.md) that [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) to [nathan](../../strongs/h/h5414.md) the [mᵊnāṯ](../../strongs/h/h4521.md) of the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), that they might be [ḥāzaq](../../strongs/h/h2388.md) in the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_31_5"></a>2Chronicles 31:5

And as soon as the [dabar](../../strongs/h/h1697.md) came [pāraṣ](../../strongs/h/h6555.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) in [rabah](../../strongs/h/h7235.md) the [re'shiyth](../../strongs/h/h7225.md) of [dagan](../../strongs/h/h1715.md), [tiyrowsh](../../strongs/h/h8492.md), and [yiṣhār](../../strongs/h/h3323.md), and [dĕbash](../../strongs/h/h1706.md), and of all the [tᵊḇû'â](../../strongs/h/h8393.md) of the [sadeh](../../strongs/h/h7704.md); and the [maʿăśēr](../../strongs/h/h4643.md) of all things [bow'](../../strongs/h/h935.md) they in [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_31_6"></a>2Chronicles 31:6

And concerning the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md), that [yashab](../../strongs/h/h3427.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), they also [bow'](../../strongs/h/h935.md) the [maʿăśēr](../../strongs/h/h4643.md) of [bāqār](../../strongs/h/h1241.md) and [tso'n](../../strongs/h/h6629.md), and the [maʿăśēr](../../strongs/h/h4643.md) of [qodesh](../../strongs/h/h6944.md) which were [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and [nathan](../../strongs/h/h5414.md) them by [ʿărēmâ](../../strongs/h/h6194.md) [ʿărēmâ](../../strongs/h/h6194.md).

<a name="2chronicles_31_7"></a>2Chronicles 31:7

In the third [ḥōḏeš](../../strongs/h/h2320.md) they [ḥālal](../../strongs/h/h2490.md) to [yacad](../../strongs/h/h3245.md) the [ʿărēmâ](../../strongs/h/h6194.md), and [kalah](../../strongs/h/h3615.md) them in the seventh [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2chronicles_31_8"></a>2Chronicles 31:8

And when [Yᵊḥizqîyâ](../../strongs/h/h3169.md) and the [śar](../../strongs/h/h8269.md) [bow'](../../strongs/h/h935.md) and [ra'ah](../../strongs/h/h7200.md) the [ʿărēmâ](../../strongs/h/h6194.md), they [barak](../../strongs/h/h1288.md) [Yĕhovah](../../strongs/h/h3068.md), and his ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_31_9"></a>2Chronicles 31:9

Then [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [darash](../../strongs/h/h1875.md) with the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) concerning the [ʿărēmâ](../../strongs/h/h6194.md).

<a name="2chronicles_31_10"></a>2Chronicles 31:10

And [ʿĂzaryâ](../../strongs/h/h5838.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md) of the [bayith](../../strongs/h/h1004.md) of [Ṣāḏôq](../../strongs/h/h6659.md) ['āmar](../../strongs/h/h559.md) him, and ['āmar](../../strongs/h/h559.md), Since the people [ḥālal](../../strongs/h/h2490.md) to [bow'](../../strongs/h/h935.md) the [tᵊrûmâ](../../strongs/h/h8641.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), we have had [sāׂbaʿ](../../strongs/h/h7646.md) to ['akal](../../strongs/h/h398.md), and have [yāṯar](../../strongs/h/h3498.md) [rōḇ](../../strongs/h/h7230.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [barak](../../strongs/h/h1288.md) his ['am](../../strongs/h/h5971.md); and that which is [yāṯar](../../strongs/h/h3498.md) is this great [hāmôn](../../strongs/h/h1995.md).

<a name="2chronicles_31_11"></a>2Chronicles 31:11

Then [Yᵊḥizqîyâ](../../strongs/h/h3169.md) ['āmar](../../strongs/h/h559.md) to [kuwn](../../strongs/h/h3559.md) [liškâ](../../strongs/h/h3957.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); and they [kuwn](../../strongs/h/h3559.md) them,

<a name="2chronicles_31_12"></a>2Chronicles 31:12

And [bow'](../../strongs/h/h935.md) the [tᵊrûmâ](../../strongs/h/h8641.md) and the [maʿăśēr](../../strongs/h/h4643.md) and the [qodesh](../../strongs/h/h6944.md) things ['ĕmûnâ](../../strongs/h/h530.md): over which [Kônanyâû](../../strongs/h/h3562.md) the [Lᵊvî](../../strongs/h/h3881.md) was [nāḡîḏ](../../strongs/h/h5057.md), and [Šimʿî](../../strongs/h/h8096.md) his ['ach](../../strongs/h/h251.md) was the [mišnê](../../strongs/h/h4932.md).

<a name="2chronicles_31_13"></a>2Chronicles 31:13

And [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [ʿĀzazyâû](../../strongs/h/h5812.md), and [Naḥaṯ](../../strongs/h/h5184.md), and [ʿĂśâ'Ēl](../../strongs/h/h6214.md), and [Yᵊrêmôṯ](../../strongs/h/h3406.md), and [Yôzāḇāḏ](../../strongs/h/h3107.md), and ['Ĕlî'Ēl](../../strongs/h/h447.md), and [Yismaḵyâû](../../strongs/h/h3253.md), and [Maḥaṯ](../../strongs/h/h4287.md), and [Bᵊnāyâ](../../strongs/h/h1141.md), were [pāqîḏ](../../strongs/h/h6496.md) under the [yad](../../strongs/h/h3027.md) of [Kônanyâû](../../strongs/h/h3562.md) and [Šimʿî](../../strongs/h/h8096.md) his ['ach](../../strongs/h/h251.md), at the [mip̄qāḏ](../../strongs/h/h4662.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md) the [melek](../../strongs/h/h4428.md), and [ʿĂzaryâ](../../strongs/h/h5838.md) the [nāḡîḏ](../../strongs/h/h5057.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_31_14"></a>2Chronicles 31:14

And [Qōrē'](../../strongs/h/h6981.md) the [ben](../../strongs/h/h1121.md) of [Yimnâ](../../strongs/h/h3232.md) the [Lᵊvî](../../strongs/h/h3881.md), the [šôʿēr](../../strongs/h/h7778.md) toward the [mizrach](../../strongs/h/h4217.md), was over the [nᵊḏāḇâ](../../strongs/h/h5071.md) of ['Elohiym](../../strongs/h/h430.md), to [nathan](../../strongs/h/h5414.md) the [tᵊrûmâ](../../strongs/h/h8641.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="2chronicles_31_15"></a>2Chronicles 31:15

And next him were ['Eden](../../strongs/h/h5731.md), and [Minyāmîn](../../strongs/h/h4509.md), and [Yēšûaʿ](../../strongs/h/h3442.md), and [ŠᵊmaʿYâ](../../strongs/h/h8098.md), ['Ămaryâ](../../strongs/h/h568.md), and [Šᵊḵanyâ](../../strongs/h/h7935.md), in the [ʿîr](../../strongs/h/h5892.md) of the [kōhēn](../../strongs/h/h3548.md), in their set ['ĕmûnâ](../../strongs/h/h530.md), to [nathan](../../strongs/h/h5414.md) to their ['ach](../../strongs/h/h251.md) [yad](../../strongs/h/h3027.md) [maḥălōqeṯ](../../strongs/h/h4256.md), as well to the [gadowl](../../strongs/h/h1419.md) as to the [qāṭān](../../strongs/h/h6996.md):

<a name="2chronicles_31_16"></a>2Chronicles 31:16

Beside their [yāḥaś](../../strongs/h/h3187.md) of [zāḵār](../../strongs/h/h2145.md), from three [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), even unto every one that [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), his [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) [dabar](../../strongs/h/h1697.md) for their [ʿăḇōḏâ](../../strongs/h/h5656.md) in their [mišmereṯ](../../strongs/h/h4931.md) according to their [maḥălōqeṯ](../../strongs/h/h4256.md);

<a name="2chronicles_31_17"></a>2Chronicles 31:17

Both to the [yāḥaś](../../strongs/h/h3187.md) of the [kōhēn](../../strongs/h/h3548.md) by the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), and the [Lᵊvî](../../strongs/h/h3881.md) from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), in their [mišmereṯ](../../strongs/h/h4931.md) by their [maḥălōqeṯ](../../strongs/h/h4256.md);

<a name="2chronicles_31_18"></a>2Chronicles 31:18

And to the [yāḥaś](../../strongs/h/h3187.md) of all their [ṭap̄](../../strongs/h/h2945.md), their ['ishshah](../../strongs/h/h802.md), and their [ben](../../strongs/h/h1121.md), and their [bath](../../strongs/h/h1323.md), through all the [qāhēl](../../strongs/h/h6951.md): for in their set ['ĕmûnâ](../../strongs/h/h530.md) they [qadash](../../strongs/h/h6942.md) themselves in [qodesh](../../strongs/h/h6944.md):

<a name="2chronicles_31_19"></a>2Chronicles 31:19

Also of the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), which were in the [sadeh](../../strongs/h/h7704.md) of the [miḡrāš](../../strongs/h/h4054.md) of their [ʿîr](../../strongs/h/h5892.md), in every several [ʿîr](../../strongs/h/h5892.md), the ['enowsh](../../strongs/h/h582.md) that were [nāqaḇ](../../strongs/h/h5344.md) by [shem](../../strongs/h/h8034.md), to [nathan](../../strongs/h/h5414.md) [mānâ](../../strongs/h/h4490.md) to all the [zāḵār](../../strongs/h/h2145.md) among the [kōhēn](../../strongs/h/h3548.md), and to all that were [yāḥaś](../../strongs/h/h3187.md) among the [Lᵊvî](../../strongs/h/h3881.md).

<a name="2chronicles_31_20"></a>2Chronicles 31:20

And thus ['asah](../../strongs/h/h6213.md) [Yᵊḥizqîyâ](../../strongs/h/h3169.md) throughout all [Yehuwdah](../../strongs/h/h3063.md), and ['asah](../../strongs/h/h6213.md) that which was [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) and ['emeth](../../strongs/h/h571.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_31_21"></a>2Chronicles 31:21

And in every [ma'aseh](../../strongs/h/h4639.md) that he [ḥālal](../../strongs/h/h2490.md) in the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), and in the [towrah](../../strongs/h/h8451.md), and in the [mitsvah](../../strongs/h/h4687.md), to [darash](../../strongs/h/h1875.md) his ['Elohiym](../../strongs/h/h430.md), he ['asah](../../strongs/h/h6213.md) it with all his [lebab](../../strongs/h/h3824.md), and [tsalach](../../strongs/h/h6743.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 30](2chronicles_30.md) - [2Chronicles 32](2chronicles_32.md)