# [2Chronicles 13](https://www.blueletterbible.org/kjv/2chronicles/13)

<a name="2chronicles_13_1"></a>2Chronicles 13:1

Now in the eighteenth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [YārāḇʿĀm](../../strongs/h/h3379.md) began ['Ăḇîâ](../../strongs/h/h29.md) to [mālaḵ](../../strongs/h/h4427.md) over [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_13_2"></a>2Chronicles 13:2

He [mālaḵ](../../strongs/h/h4427.md) three [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). His ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) also was [Mîḵāyâû](../../strongs/h/h4322.md) the [bath](../../strongs/h/h1323.md) of ['Ûrî'Ēl](../../strongs/h/h222.md) of [giḇʿâ](../../strongs/h/h1390.md). And there was [milḥāmâ](../../strongs/h/h4421.md) between ['Ăḇîâ](../../strongs/h/h29.md) and [YārāḇʿĀm](../../strongs/h/h3379.md).

<a name="2chronicles_13_3"></a>2Chronicles 13:3

And ['Ăḇîâ](../../strongs/h/h29.md) ['āsar](../../strongs/h/h631.md) the [milḥāmâ](../../strongs/h/h4421.md) in ['āsar](../../strongs/h/h631.md) with an [ḥayil](../../strongs/h/h2428.md) of [gibôr](../../strongs/h/h1368.md) of [milḥāmâ](../../strongs/h/h4421.md), even four hundred thousand [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md): [YārāḇʿĀm](../../strongs/h/h3379.md) also set the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) against him with eight hundred thousand [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md), being [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="2chronicles_13_4"></a>2Chronicles 13:4

And ['Ăḇîâ](../../strongs/h/h29.md) [quwm](../../strongs/h/h6965.md) upon [har](../../strongs/h/h2022.md) [Ṣᵊmārayim](../../strongs/h/h6787.md), which is in [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md), and ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) me, thou [YārāḇʿĀm](../../strongs/h/h3379.md), and all [Yisra'el](../../strongs/h/h3478.md);

<a name="2chronicles_13_5"></a>2Chronicles 13:5

Ought ye not to [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [nathan](../../strongs/h/h5414.md) the [mamlāḵâ](../../strongs/h/h4467.md) over [Yisra'el](../../strongs/h/h3478.md) to [Dāviḏ](../../strongs/h/h1732.md) ['owlam](../../strongs/h/h5769.md), even to him and to his [ben](../../strongs/h/h1121.md) by a [bĕriyth](../../strongs/h/h1285.md) of [melaḥ](../../strongs/h/h4417.md)?

<a name="2chronicles_13_6"></a>2Chronicles 13:6

Yet [YārāḇʿĀm](../../strongs/h/h3379.md) the [ben](../../strongs/h/h1121.md) of [Nᵊḇāṭ](../../strongs/h/h5028.md), the ['ebed](../../strongs/h/h5650.md) of [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md), is [quwm](../../strongs/h/h6965.md), and hath [māraḏ](../../strongs/h/h4775.md) against his ['adown](../../strongs/h/h113.md).

<a name="2chronicles_13_7"></a>2Chronicles 13:7

And there are [qāḇaṣ](../../strongs/h/h6908.md) unto him [reyq](../../strongs/h/h7386.md) ['enowsh](../../strongs/h/h582.md), the [ben](../../strongs/h/h1121.md) of [beliya'al](../../strongs/h/h1100.md), and have ['amats](../../strongs/h/h553.md) themselves against [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md), when [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) was [naʿar](../../strongs/h/h5288.md) and [raḵ](../../strongs/h/h7390.md) [lebab](../../strongs/h/h3824.md), and could not [ḥāzaq](../../strongs/h/h2388.md) [paniym](../../strongs/h/h6440.md).

<a name="2chronicles_13_8"></a>2Chronicles 13:8

And now ye ['āmar](../../strongs/h/h559.md) to [ḥāzaq](../../strongs/h/h2388.md) the [paniym](../../strongs/h/h6440.md) [mamlāḵâ](../../strongs/h/h4467.md) of [Yĕhovah](../../strongs/h/h3068.md) in the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md); and ye be a [rab](../../strongs/h/h7227.md) [hāmôn](../../strongs/h/h1995.md), and there are with you [zāhāḇ](../../strongs/h/h2091.md) [ʿēḡel](../../strongs/h/h5695.md), which [YārāḇʿĀm](../../strongs/h/h3379.md) ['asah](../../strongs/h/h6213.md) you for ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_13_9"></a>2Chronicles 13:9

Have ye not [nāḏaḥ](../../strongs/h/h5080.md) the [kōhēn](../../strongs/h/h3548.md) of [Yĕhovah](../../strongs/h/h3068.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), and the [Lᵊvî](../../strongs/h/h3881.md), and have ['asah](../../strongs/h/h6213.md) you [kōhēn](../../strongs/h/h3548.md) after the manner of the ['am](../../strongs/h/h5971.md) of other ['erets](../../strongs/h/h776.md)? so that whosoever [bow'](../../strongs/h/h935.md) to [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) himself with a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) and seven ['ayil](../../strongs/h/h352.md), the same may be a [kōhēn](../../strongs/h/h3548.md) of them that are no ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_13_10"></a>2Chronicles 13:10

But as for us, [Yĕhovah](../../strongs/h/h3068.md) is our ['Elohiym](../../strongs/h/h430.md), and we have not ['azab](../../strongs/h/h5800.md) him; and the [kōhēn](../../strongs/h/h3548.md), which [sharath](../../strongs/h/h8334.md) unto [Yĕhovah](../../strongs/h/h3068.md), are the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), and the [Lᵊvî](../../strongs/h/h3881.md) wait upon their [mĕla'kah](../../strongs/h/h4399.md):

<a name="2chronicles_13_11"></a>2Chronicles 13:11

And they [qāṭar](../../strongs/h/h6999.md) unto [Yĕhovah](../../strongs/h/h3068.md) every [boqer](../../strongs/h/h1242.md) and every ['ereb](../../strongs/h/h6153.md) [ʿōlâ](../../strongs/h/h5930.md) and [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md): the [lechem](../../strongs/h/h3899.md) [maʿăreḵeṯ](../../strongs/h/h4635.md) also set they in order upon the [tahowr](../../strongs/h/h2889.md) [šulḥān](../../strongs/h/h7979.md); and the [mᵊnôrâ](../../strongs/h/h4501.md) of [zāhāḇ](../../strongs/h/h2091.md) with the [nîr](../../strongs/h/h5216.md) thereof, to [bāʿar](../../strongs/h/h1197.md) every ['ereb](../../strongs/h/h6153.md): for we [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); but ye have ['azab](../../strongs/h/h5800.md) him.

<a name="2chronicles_13_12"></a>2Chronicles 13:12

And, behold, ['Elohiym](../../strongs/h/h430.md) himself is with us for our [ro'sh](../../strongs/h/h7218.md), and his [kōhēn](../../strongs/h/h3548.md) with [tᵊrûʿâ](../../strongs/h/h8643.md) [ḥăṣōṣrâ](../../strongs/h/h2689.md) to [rûaʿ](../../strongs/h/h7321.md) against you. O [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [lāḥam](../../strongs/h/h3898.md) ye not against [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md); for ye shall not [tsalach](../../strongs/h/h6743.md).

<a name="2chronicles_13_13"></a>2Chronicles 13:13

But [YārāḇʿĀm](../../strongs/h/h3379.md) caused a [ma'ărāḇ](../../strongs/h/h3993.md) to [bow'](../../strongs/h/h935.md) [cabab](../../strongs/h/h5437.md) ['aḥar](../../strongs/h/h310.md) them: so they were [paniym](../../strongs/h/h6440.md) [Yehuwdah](../../strongs/h/h3063.md), and the [ma'ărāḇ](../../strongs/h/h3993.md) was ['aḥar](../../strongs/h/h310.md) them.

<a name="2chronicles_13_14"></a>2Chronicles 13:14

And when [Yehuwdah](../../strongs/h/h3063.md) looked [panah](../../strongs/h/h6437.md), behold, the [milḥāmâ](../../strongs/h/h4421.md) was [paniym](../../strongs/h/h6440.md) and ['āḥôr](../../strongs/h/h268.md): and they [ṣāʿaq](../../strongs/h/h6817.md) unto [Yĕhovah](../../strongs/h/h3068.md), and the [kōhēn](../../strongs/h/h3548.md) [ḥāṣar](../../strongs/h/h2690.md) [ḥāṣar](../../strongs/h/h2690.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md).

<a name="2chronicles_13_15"></a>2Chronicles 13:15

Then the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) gave a [rûaʿ](../../strongs/h/h7321.md): and as the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) [rûaʿ](../../strongs/h/h7321.md), it came to pass, that ['Elohiym](../../strongs/h/h430.md) [nāḡap̄](../../strongs/h/h5062.md) [YārāḇʿĀm](../../strongs/h/h3379.md) and all [Yisra'el](../../strongs/h/h3478.md) [paniym](../../strongs/h/h6440.md) ['Ăḇîâ](../../strongs/h/h29.md) and [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_13_16"></a>2Chronicles 13:16

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [nûs](../../strongs/h/h5127.md) [paniym](../../strongs/h/h6440.md) [Yehuwdah](../../strongs/h/h3063.md): and ['Elohiym](../../strongs/h/h430.md) [nathan](../../strongs/h/h5414.md) them into their [yad](../../strongs/h/h3027.md).

<a name="2chronicles_13_17"></a>2Chronicles 13:17

And ['Ăḇîâ](../../strongs/h/h29.md) and his ['am](../../strongs/h/h5971.md) [nakah](../../strongs/h/h5221.md) them with a [rab](../../strongs/h/h7227.md) [makâ](../../strongs/h/h4347.md): so there [naphal](../../strongs/h/h5307.md) [ḥālāl](../../strongs/h/h2491.md) of [Yisra'el](../../strongs/h/h3478.md) five hundred thousand [bāḥar](../../strongs/h/h977.md) ['iysh](../../strongs/h/h376.md).

<a name="2chronicles_13_18"></a>2Chronicles 13:18

Thus the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) were [kānaʿ](../../strongs/h/h3665.md) under at that [ʿēṯ](../../strongs/h/h6256.md), and the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) ['amats](../../strongs/h/h553.md), because they [šāʿan](../../strongs/h/h8172.md) upon [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_13_19"></a>2Chronicles 13:19

And ['Ăḇîâ](../../strongs/h/h29.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) [YārāḇʿĀm](../../strongs/h/h3379.md), and [lāḵaḏ](../../strongs/h/h3920.md) [ʿîr](../../strongs/h/h5892.md) from him, [Bêṯ-'ēl](../../strongs/h/h1008.md) with the [bath](../../strongs/h/h1323.md) thereof, and [yᵊšānâ](../../strongs/h/h3466.md) with the [bath](../../strongs/h/h1323.md) thereof, and [ʿep̄rôn](../../strongs/h/h6085.md) with the [bath](../../strongs/h/h1323.md) thereof.

<a name="2chronicles_13_20"></a>2Chronicles 13:20

Neither did [YārāḇʿĀm](../../strongs/h/h3379.md) [ʿāṣar](../../strongs/h/h6113.md) [koach](../../strongs/h/h3581.md) again in the [yowm](../../strongs/h/h3117.md) of ['Ăḇîâ](../../strongs/h/h29.md): and [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) him, and he [muwth](../../strongs/h/h4191.md).

<a name="2chronicles_13_21"></a>2Chronicles 13:21

But ['Ăḇîâ](../../strongs/h/h29.md) [ḥāzaq](../../strongs/h/h2388.md), and [nasa'](../../strongs/h/h5375.md) fourteen ['ishshah](../../strongs/h/h802.md), and [yalad](../../strongs/h/h3205.md) twenty and two [ben](../../strongs/h/h1121.md), and sixteen [bath](../../strongs/h/h1323.md).

<a name="2chronicles_13_22"></a>2Chronicles 13:22

And the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ăḇîâ](../../strongs/h/h29.md), and his [derek](../../strongs/h/h1870.md), and his [dabar](../../strongs/h/h1697.md), are [kāṯaḇ](../../strongs/h/h3789.md) in the [miḏrāš](../../strongs/h/h4097.md) of the [nāḇî'](../../strongs/h/h5030.md) [ʿIdô](../../strongs/h/h5714.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 12](2chronicles_12.md) - [2Chronicles 14](2chronicles_14.md)