# [2Chronicles 12](https://www.blueletterbible.org/kjv/2chronicles/12)

<a name="2chronicles_12_1"></a>2Chronicles 12:1

And it came to pass, when [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) had [kuwn](../../strongs/h/h3559.md) the [malkuwth](../../strongs/h/h4438.md), and had [ḥezqâ](../../strongs/h/h2393.md) himself, he ['azab](../../strongs/h/h5800.md) the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md), and all [Yisra'el](../../strongs/h/h3478.md) with him.

<a name="2chronicles_12_2"></a>2Chronicles 12:2

And it came to pass, that in the fifth [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [Šîšaq](../../strongs/h/h7895.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), because they had [māʿal](../../strongs/h/h4603.md) against [Yĕhovah](../../strongs/h/h3068.md),

<a name="2chronicles_12_3"></a>2Chronicles 12:3

With twelve hundred [reḵeḇ](../../strongs/h/h7393.md), and threescore thousand [pārāš](../../strongs/h/h6571.md): and the ['am](../../strongs/h/h5971.md) were without [mispār](../../strongs/h/h4557.md) that [bow'](../../strongs/h/h935.md) with him out of [Mitsrayim](../../strongs/h/h4714.md); the [Luḇî](../../strongs/h/h3864.md), the [Sukî](../../strongs/h/h5525.md), and the [Kûšî](../../strongs/h/h3569.md).

<a name="2chronicles_12_4"></a>2Chronicles 12:4

And he [lāḵaḏ](../../strongs/h/h3920.md) the [mᵊṣûrâ](../../strongs/h/h4694.md) [ʿîr](../../strongs/h/h5892.md) which pertained to [Yehuwdah](../../strongs/h/h3063.md), and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_12_5"></a>2Chronicles 12:5

Then [bow'](../../strongs/h/h935.md) [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [nāḇî'](../../strongs/h/h5030.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), and to the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md), that were ['āsap̄](../../strongs/h/h622.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) [paniym](../../strongs/h/h6440.md) of [Šîšaq](../../strongs/h/h7895.md), and ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Ye have ['azab](../../strongs/h/h5800.md) me, and therefore have I also ['azab](../../strongs/h/h5800.md) you in the [yad](../../strongs/h/h3027.md) of [Šîšaq](../../strongs/h/h7895.md).

<a name="2chronicles_12_6"></a>2Chronicles 12:6

Whereupon the [śar](../../strongs/h/h8269.md) of [Yisra'el](../../strongs/h/h3478.md) and the [melek](../../strongs/h/h4428.md) [kānaʿ](../../strongs/h/h3665.md) themselves; and they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) is [tsaddiyq](../../strongs/h/h6662.md).

<a name="2chronicles_12_7"></a>2Chronicles 12:7

And when [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) that they [kānaʿ](../../strongs/h/h3665.md) themselves, the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [ŠᵊmaʿYâ](../../strongs/h/h8098.md), ['āmar](../../strongs/h/h559.md), They have [kānaʿ](../../strongs/h/h3665.md) themselves; therefore I will not [shachath](../../strongs/h/h7843.md) them, but I will [nathan](../../strongs/h/h5414.md) them [mᵊʿaṭ](../../strongs/h/h4592.md) [pᵊlêṭâ](../../strongs/h/h6413.md); and my [chemah](../../strongs/h/h2534.md) shall not be [nāṯaḵ](../../strongs/h/h5413.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md) by the [yad](../../strongs/h/h3027.md) of [Šîšaq](../../strongs/h/h7895.md).

<a name="2chronicles_12_8"></a>2Chronicles 12:8

Nevertheless they shall be his ['ebed](../../strongs/h/h5650.md); that they may [yada'](../../strongs/h/h3045.md) my [ʿăḇōḏâ](../../strongs/h/h5656.md), and the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md).

<a name="2chronicles_12_9"></a>2Chronicles 12:9

So [Šîšaq](../../strongs/h/h7895.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and [laqach](../../strongs/h/h3947.md) the ['ôṣār](../../strongs/h/h214.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md); he [laqach](../../strongs/h/h3947.md) all: he [laqach](../../strongs/h/h3947.md) also the [magen](../../strongs/h/h4043.md) of [zāhāḇ](../../strongs/h/h2091.md) which [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md).

<a name="2chronicles_12_10"></a>2Chronicles 12:10

Instead of which [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) ['asah](../../strongs/h/h6213.md) [magen](../../strongs/h/h4043.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md), and [paqad](../../strongs/h/h6485.md) them to the [yad](../../strongs/h/h3027.md) of the [śar](../../strongs/h/h8269.md) of the [rûṣ](../../strongs/h/h7323.md), that [shamar](../../strongs/h/h8104.md) the [peṯaḥ](../../strongs/h/h6607.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_12_11"></a>2Chronicles 12:11

And [day](../../strongs/h/h1767.md) the [melek](../../strongs/h/h4428.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), the [rûṣ](../../strongs/h/h7323.md) [bow'](../../strongs/h/h935.md) and [nasa'](../../strongs/h/h5375.md) them, and [shuwb](../../strongs/h/h7725.md) them into the [rûṣ](../../strongs/h/h7323.md) [tā'](../../strongs/h/h8372.md).

<a name="2chronicles_12_12"></a>2Chronicles 12:12

And when he [kānaʿ](../../strongs/h/h3665.md) himself, the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) [shuwb](../../strongs/h/h7725.md) from him, that he would not [shachath](../../strongs/h/h7843.md) him [kālâ](../../strongs/h/h3617.md): and also in [Yehuwdah](../../strongs/h/h3063.md) [dabar](../../strongs/h/h1697.md) went [towb](../../strongs/h/h2896.md).

<a name="2chronicles_12_13"></a>2Chronicles 12:13

So [melek](../../strongs/h/h4428.md) [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [ḥāzaq](../../strongs/h/h2388.md) himself in [Yĕruwshalaim](../../strongs/h/h3389.md), and [mālaḵ](../../strongs/h/h4427.md): for [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) was one and forty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) seventeen [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), the [ʿîr](../../strongs/h/h5892.md) which [Yĕhovah](../../strongs/h/h3068.md) had [bāḥar](../../strongs/h/h977.md) out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), to [śûm](../../strongs/h/h7760.md) his [shem](../../strongs/h/h8034.md) there. And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [naʿămâ](../../strongs/h/h5279.md) an [ʿammônîṯ](../../strongs/h/h5985.md).

<a name="2chronicles_12_14"></a>2Chronicles 12:14

And he ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md), because he [kuwn](../../strongs/h/h3559.md) not his [leb](../../strongs/h/h3820.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_12_15"></a>2Chronicles 12:15

Now the [dabar](../../strongs/h/h1697.md) of [RᵊḥaḇʿĀm](../../strongs/h/h7346.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [dabar](../../strongs/h/h1697.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [nāḇî'](../../strongs/h/h5030.md), and of [ʿIdô](../../strongs/h/h5714.md) the [ḥōzê](../../strongs/h/h2374.md) concerning [yāḥaś](../../strongs/h/h3187.md)? And there were [milḥāmâ](../../strongs/h/h4421.md) between [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) and [YārāḇʿĀm](../../strongs/h/h3379.md) [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_12_16"></a>2Chronicles 12:16

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Ăḇîâ](../../strongs/h/h29.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 11](2chronicles_11.md) - [2Chronicles 13](2chronicles_13.md)