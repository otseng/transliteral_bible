# [2Chronicles 17](https://www.blueletterbible.org/kjv/2chronicles/17)

<a name="2chronicles_17_1"></a>2Chronicles 17:1

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead, and [ḥāzaq](../../strongs/h/h2388.md) himself against [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_17_2"></a>2Chronicles 17:2

And he [nathan](../../strongs/h/h5414.md) [ḥayil](../../strongs/h/h2428.md) in all the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [nathan](../../strongs/h/h5414.md) [nᵊṣîḇ](../../strongs/h/h5333.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ʿîr](../../strongs/h/h5892.md) of ['Ep̄rayim](../../strongs/h/h669.md), which ['Āsā'](../../strongs/h/h609.md) his ['ab](../../strongs/h/h1.md) had [lāḵaḏ](../../strongs/h/h3920.md).

<a name="2chronicles_17_3"></a>2Chronicles 17:3

And [Yĕhovah](../../strongs/h/h3068.md) was with [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), because he [halak](../../strongs/h/h1980.md) in the [ri'šôn](../../strongs/h/h7223.md) [derek](../../strongs/h/h1870.md) of his ['ab](../../strongs/h/h1.md) [Dāviḏ](../../strongs/h/h1732.md), and [darash](../../strongs/h/h1875.md) not unto [BaʿAl](../../strongs/h/h1168.md);

<a name="2chronicles_17_4"></a>2Chronicles 17:4

But [darash](../../strongs/h/h1875.md) to ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md), and [halak](../../strongs/h/h1980.md) in his [mitsvah](../../strongs/h/h4687.md), and not after the [ma'aseh](../../strongs/h/h4639.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_17_5"></a>2Chronicles 17:5

Therefore [Yĕhovah](../../strongs/h/h3068.md) [kuwn](../../strongs/h/h3559.md) the [mamlāḵâ](../../strongs/h/h4467.md) in his [yad](../../strongs/h/h3027.md); and all [Yehuwdah](../../strongs/h/h3063.md) [nathan](../../strongs/h/h5414.md) to [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [minchah](../../strongs/h/h4503.md); and he had [ʿōšer](../../strongs/h/h6239.md) and [kabowd](../../strongs/h/h3519.md) in [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_17_6"></a>2Chronicles 17:6

And his [leb](../../strongs/h/h3820.md) was [gāḇah](../../strongs/h/h1361.md) in the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md): moreover he [cuwr](../../strongs/h/h5493.md) the [bāmâ](../../strongs/h/h1116.md) and ['ăšērâ](../../strongs/h/h842.md) out of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_17_7"></a>2Chronicles 17:7

Also in the third [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md) he [shalach](../../strongs/h/h7971.md) to his [śar](../../strongs/h/h8269.md), even to [Ben-Ḥayil](../../strongs/h/h1134.md), and to [ʿŌḇaḏyâ](../../strongs/h/h5662.md), and to [Zᵊḵaryâ](../../strongs/h/h2148.md), and to [Nᵊṯan'ēl](../../strongs/h/h5417.md), and to [Mîḵāyâû](../../strongs/h/h4322.md), to [lamad](../../strongs/h/h3925.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_17_8"></a>2Chronicles 17:8

And with them he sent [Lᵊvî](../../strongs/h/h3881.md), even [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [Nᵊṯanyâ](../../strongs/h/h5418.md), and [Zᵊḇaḏyâ](../../strongs/h/h2069.md), and [ʿĂśâ'Ēl](../../strongs/h/h6214.md), and [Šᵊmîrāmôṯ](../../strongs/h/h8070.md), and [Yᵊhônāṯān](../../strongs/h/h3083.md), and ['Ăḏōnîyâ](../../strongs/h/h138.md), and [Ṭôḇîyâ](../../strongs/h/h2900.md), and [Ṭôḇ 'Ăḏōnîyâû](../../strongs/h/h2899.md), [Lᵊvî](../../strongs/h/h3881.md); and with them ['Ĕlîšāmāʿ](../../strongs/h/h476.md) and [Yᵊhôrām](../../strongs/h/h3088.md), [kōhēn](../../strongs/h/h3548.md).

<a name="2chronicles_17_9"></a>2Chronicles 17:9

And they [lamad](../../strongs/h/h3925.md) in [Yehuwdah](../../strongs/h/h3063.md), and had the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) with them, and went [cabab](../../strongs/h/h5437.md) throughout all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [lamad](../../strongs/h/h3925.md) the ['am](../../strongs/h/h5971.md).

<a name="2chronicles_17_10"></a>2Chronicles 17:10

And the [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md) fell upon all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) that were [cabiyb](../../strongs/h/h5439.md) [Yehuwdah](../../strongs/h/h3063.md), so that they made no [lāḥam](../../strongs/h/h3898.md) against [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md).

<a name="2chronicles_17_11"></a>2Chronicles 17:11

Also some of the [Pᵊlištî](../../strongs/h/h6430.md) [bow'](../../strongs/h/h935.md) [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [minchah](../../strongs/h/h4503.md), and [maśśā'](../../strongs/h/h4853.md) [keceph](../../strongs/h/h3701.md); and the [ʿĂrāḇî](../../strongs/h/h6163.md) [bow'](../../strongs/h/h935.md) him [tso'n](../../strongs/h/h6629.md), seven thousand and seven hundred ['ayil](../../strongs/h/h352.md), and seven thousand and seven hundred he [tayiš](../../strongs/h/h8495.md).

<a name="2chronicles_17_12"></a>2Chronicles 17:12

And [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [halak](../../strongs/h/h1980.md) [gāḏēl](../../strongs/h/h1432.md) [maʿal](../../strongs/h/h4605.md); and he [bānâ](../../strongs/h/h1129.md) in [Yehuwdah](../../strongs/h/h3063.md) [bîrānîṯ](../../strongs/h/h1003.md), and [ʿîr](../../strongs/h/h5892.md) of [miskᵊnôṯ](../../strongs/h/h4543.md).

<a name="2chronicles_17_13"></a>2Chronicles 17:13

And he had [rab](../../strongs/h/h7227.md) [mĕla'kah](../../strongs/h/h4399.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md): and the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), were in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_17_14"></a>2Chronicles 17:14

And these are the [pᵊqudâ](../../strongs/h/h6486.md) of them according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md): Of [Yehuwdah](../../strongs/h/h3063.md), the [śar](../../strongs/h/h8269.md) of thousands; [ʿAḏnâ](../../strongs/h/h5734.md) the [śar](../../strongs/h/h8269.md), and with him [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md) three hundred thousand.

<a name="2chronicles_17_15"></a>2Chronicles 17:15

And [yad](../../strongs/h/h3027.md) to him was [Yᵊhôḥānān](../../strongs/h/h3076.md) the [śar](../../strongs/h/h8269.md), and with him two hundred and fourscore thousand.

<a name="2chronicles_17_16"></a>2Chronicles 17:16

And [yad](../../strongs/h/h3027.md) him was [ʿĂmasyâ](../../strongs/h/h6007.md) the [ben](../../strongs/h/h1121.md) of [Ziḵrî](../../strongs/h/h2147.md), who willingly [nāḏaḇ](../../strongs/h/h5068.md) himself unto [Yĕhovah](../../strongs/h/h3068.md); and with him two hundred thousand [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="2chronicles_17_17"></a>2Chronicles 17:17

And of [Binyāmîn](../../strongs/h/h1144.md); ['Elyāḏāʿ](../../strongs/h/h450.md) a [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md), and with him [nashaq](../../strongs/h/h5401.md) with [qesheth](../../strongs/h/h7198.md) and [magen](../../strongs/h/h4043.md) two hundred thousand.

<a name="2chronicles_17_18"></a>2Chronicles 17:18

And [yad](../../strongs/h/h3027.md) him was [Yᵊhôzāḇāḏ](../../strongs/h/h3075.md), and with him an hundred and fourscore thousand ready [chalats](../../strongs/h/h2502.md) for the [tsaba'](../../strongs/h/h6635.md).

<a name="2chronicles_17_19"></a>2Chronicles 17:19

These [sharath](../../strongs/h/h8334.md) on the [melek](../../strongs/h/h4428.md), beside those whom the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) in the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) throughout all [Yehuwdah](../../strongs/h/h3063.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 16](2chronicles_16.md) - [2Chronicles 18](2chronicles_18.md)