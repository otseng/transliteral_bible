# [2Chronicles 25](https://www.blueletterbible.org/kjv/2chronicles/25)

<a name="2chronicles_25_1"></a>2Chronicles 25:1

['Ămaṣyâ](../../strongs/h/h558.md) was twenty and five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) twenty and nine [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [YᵊhôʿĀḏîn](../../strongs/h/h3086.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_25_2"></a>2Chronicles 25:2

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), but not with a [šālēm](../../strongs/h/h8003.md) [lebab](../../strongs/h/h3824.md).

<a name="2chronicles_25_3"></a>2Chronicles 25:3

Now it came to pass, when the [mamlāḵâ](../../strongs/h/h4467.md) was [ḥāzaq](../../strongs/h/h2388.md) to him, that he [harag](../../strongs/h/h2026.md) his ['ebed](../../strongs/h/h5650.md) that had [nakah](../../strongs/h/h5221.md) the [melek](../../strongs/h/h4428.md) his ['ab](../../strongs/h/h1.md).

<a name="2chronicles_25_4"></a>2Chronicles 25:4

But he [muwth](../../strongs/h/h4191.md) not their [ben](../../strongs/h/h1121.md), but did as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) in the [sēp̄er](../../strongs/h/h5612.md) of [Mōshe](../../strongs/h/h4872.md), where [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), The ['ab](../../strongs/h/h1.md) shall not [muwth](../../strongs/h/h4191.md) for the [ben](../../strongs/h/h1121.md), neither shall the [ben](../../strongs/h/h1121.md) [muwth](../../strongs/h/h4191.md) for the ['ab](../../strongs/h/h1.md), but every ['iysh](../../strongs/h/h376.md) shall [muwth](../../strongs/h/h4191.md) for his own [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="2chronicles_25_5"></a>2Chronicles 25:5

Moreover ['Ămaṣyâ](../../strongs/h/h558.md) [qāḇaṣ](../../strongs/h/h6908.md) [Yehuwdah](../../strongs/h/h3063.md) [qāḇaṣ](../../strongs/h/h6908.md), and ['amad](../../strongs/h/h5975.md) them [śar](../../strongs/h/h8269.md) over thousands, and [śar](../../strongs/h/h8269.md) over hundreds, according to the [bayith](../../strongs/h/h1004.md) of their ['ab](../../strongs/h/h1.md), throughout all [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md): and he [paqad](../../strongs/h/h6485.md) them from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md), and [māṣā'](../../strongs/h/h4672.md) them three hundred thousand [bāḥar](../../strongs/h/h977.md) men, able to [yāṣā'](../../strongs/h/h3318.md) to [tsaba'](../../strongs/h/h6635.md), that could ['āḥaz](../../strongs/h/h270.md) [rōmaḥ](../../strongs/h/h7420.md) and [tsinnah](../../strongs/h/h6793.md).

<a name="2chronicles_25_6"></a>2Chronicles 25:6

He [śāḵar](../../strongs/h/h7936.md) also an hundred thousand [gibôr](../../strongs/h/h1368.md) men of [ḥayil](../../strongs/h/h2428.md) out of [Yisra'el](../../strongs/h/h3478.md) for an hundred [kikār](../../strongs/h/h3603.md) of [keceph](../../strongs/h/h3701.md).

<a name="2chronicles_25_7"></a>2Chronicles 25:7

But there [bow'](../../strongs/h/h935.md) an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) to him, ['āmar](../../strongs/h/h559.md), O [melek](../../strongs/h/h4428.md), let not the [tsaba'](../../strongs/h/h6635.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) with thee; for [Yĕhovah](../../strongs/h/h3068.md) is not with [Yisra'el](../../strongs/h/h3478.md), to wit, with all the [ben](../../strongs/h/h1121.md) of ['Ep̄rayim](../../strongs/h/h669.md).

<a name="2chronicles_25_8"></a>2Chronicles 25:8

But if thou wilt [bow'](../../strongs/h/h935.md), ['asah](../../strongs/h/h6213.md) it, be [ḥāzaq](../../strongs/h/h2388.md) for the [milḥāmâ](../../strongs/h/h4421.md): ['Elohiym](../../strongs/h/h430.md) shall make thee [kashal](../../strongs/h/h3782.md) [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md): for ['Elohiym](../../strongs/h/h430.md) hath [koach](../../strongs/h/h3581.md) to [ʿāzar](../../strongs/h/h5826.md), and to cast [kashal](../../strongs/h/h3782.md).

<a name="2chronicles_25_9"></a>2Chronicles 25:9

And ['Ămaṣyâ](../../strongs/h/h558.md) ['āmar](../../strongs/h/h559.md) to the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), But what shall we ['asah](../../strongs/h/h6213.md) for the hundred [kikār](../../strongs/h/h3603.md) which I have [nathan](../../strongs/h/h5414.md) to the [gᵊḏûḏ](../../strongs/h/h1416.md) of [Yisra'el](../../strongs/h/h3478.md)? And the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) is able to [nathan](../../strongs/h/h5414.md) thee much [rabah](../../strongs/h/h7235.md) than this.

<a name="2chronicles_25_10"></a>2Chronicles 25:10

Then ['Ămaṣyâ](../../strongs/h/h558.md) [bāḏal](../../strongs/h/h914.md) them, to wit, the [gᵊḏûḏ](../../strongs/h/h1416.md) that was [bow'](../../strongs/h/h935.md) to him out of ['Ep̄rayim](../../strongs/h/h669.md), to [yālaḵ](../../strongs/h/h3212.md) [maqowm](../../strongs/h/h4725.md) [yālaḵ](../../strongs/h/h3212.md): wherefore their ['aph](../../strongs/h/h639.md) was [me'od](../../strongs/h/h3966.md) [ḥārâ](../../strongs/h/h2734.md) against [Yehuwdah](../../strongs/h/h3063.md), and they [shuwb](../../strongs/h/h7725.md) [maqowm](../../strongs/h/h4725.md) in [ḥŏrî](../../strongs/h/h2750.md) ['aph](../../strongs/h/h639.md).

<a name="2chronicles_25_11"></a>2Chronicles 25:11

And ['Ămaṣyâ](../../strongs/h/h558.md) [ḥāzaq](../../strongs/h/h2388.md) himself, and [nāhaḡ](../../strongs/h/h5090.md) his ['am](../../strongs/h/h5971.md), and [yālaḵ](../../strongs/h/h3212.md) to the [gay'](../../strongs/h/h1516.md) of [melaḥ](../../strongs/h/h4417.md), and [nakah](../../strongs/h/h5221.md) of the [ben](../../strongs/h/h1121.md) of [Śēʿîr](../../strongs/h/h8165.md) ten thousand.

<a name="2chronicles_25_12"></a>2Chronicles 25:12

And other ten thousand left [chay](../../strongs/h/h2416.md) did the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [šāḇâ](../../strongs/h/h7617.md), and [bow'](../../strongs/h/h935.md) them unto the [ro'sh](../../strongs/h/h7218.md) of the [cela'](../../strongs/h/h5553.md), and cast them [shalak](../../strongs/h/h7993.md) from the [ro'sh](../../strongs/h/h7218.md) of the [cela'](../../strongs/h/h5553.md), that they all were broken in [bāqaʿ](../../strongs/h/h1234.md).

<a name="2chronicles_25_13"></a>2Chronicles 25:13

But the [ben](../../strongs/h/h1121.md) of the [gᵊḏûḏ](../../strongs/h/h1416.md) which ['Ămaṣyâ](../../strongs/h/h558.md) sent [shuwb](../../strongs/h/h7725.md), that they should not [yālaḵ](../../strongs/h/h3212.md) with him to [milḥāmâ](../../strongs/h/h4421.md), [pāšaṭ](../../strongs/h/h6584.md) upon the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), from [Šōmrôn](../../strongs/h/h8111.md) even unto [Bêṯ-Ḥōvrôn](../../strongs/h/h1032.md), and [nakah](../../strongs/h/h5221.md) three thousand of them, and [bāzaz](../../strongs/h/h962.md) [rab](../../strongs/h/h7227.md) [bizzâ](../../strongs/h/h961.md).

<a name="2chronicles_25_14"></a>2Chronicles 25:14

Now it came to pass, ['aḥar](../../strongs/h/h310.md) that ['Ămaṣyâ](../../strongs/h/h558.md) was [bow'](../../strongs/h/h935.md) from the [nakah](../../strongs/h/h5221.md) of the ['Ăḏōmî](../../strongs/h/h130.md), that he [bow'](../../strongs/h/h935.md) the ['Elohiym](../../strongs/h/h430.md) of the [ben](../../strongs/h/h1121.md) of [Śēʿîr](../../strongs/h/h8165.md), and set them ['amad](../../strongs/h/h5975.md) to be his ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) himself [paniym](../../strongs/h/h6440.md) them, and [qāṭar](../../strongs/h/h6999.md) unto them.

<a name="2chronicles_25_15"></a>2Chronicles 25:15

Wherefore the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) was [ḥārâ](../../strongs/h/h2734.md) against ['Ămaṣyâ](../../strongs/h/h558.md), and he [shalach](../../strongs/h/h7971.md) unto him a [nāḇî'](../../strongs/h/h5030.md), which ['āmar](../../strongs/h/h559.md) unto him, Why hast thou [darash](../../strongs/h/h1875.md) after the ['Elohiym](../../strongs/h/h430.md) of the ['am](../../strongs/h/h5971.md), which could not [natsal](../../strongs/h/h5337.md) their own ['am](../../strongs/h/h5971.md) out of thine [yad](../../strongs/h/h3027.md)?

<a name="2chronicles_25_16"></a>2Chronicles 25:16

And it came to pass, as he [dabar](../../strongs/h/h1696.md) with him, that ['āmar](../../strongs/h/h559.md) unto him, Art thou [nathan](../../strongs/h/h5414.md) of the [melek](../../strongs/h/h4428.md) [ya'ats](../../strongs/h/h3289.md)? [ḥāḏal](../../strongs/h/h2308.md); why shouldest thou be [nakah](../../strongs/h/h5221.md)? Then the [nāḇî'](../../strongs/h/h5030.md) [ḥāḏal](../../strongs/h/h2308.md), and ['āmar](../../strongs/h/h559.md), I [yada'](../../strongs/h/h3045.md) that ['Elohiym](../../strongs/h/h430.md) hath [ya'ats](../../strongs/h/h3289.md) to [shachath](../../strongs/h/h7843.md) thee, because thou hast ['asah](../../strongs/h/h6213.md) this, and hast not [shama'](../../strongs/h/h8085.md) unto my ['etsah](../../strongs/h/h6098.md).

<a name="2chronicles_25_17"></a>2Chronicles 25:17

Then ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) took [ya'ats](../../strongs/h/h3289.md), and [shalach](../../strongs/h/h7971.md) to [Yô'Āš](../../strongs/h/h3101.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md), the [ben](../../strongs/h/h1121.md) of [Yêû'](../../strongs/h/h3058.md), [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [yālaḵ](../../strongs/h/h3212.md), let us see one [ra'ah](../../strongs/h/h7200.md) in the [paniym](../../strongs/h/h6440.md).

<a name="2chronicles_25_18"></a>2Chronicles 25:18

And [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [shalach](../../strongs/h/h7971.md) to ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), The [ḥôaḥ](../../strongs/h/h2336.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md) [shalach](../../strongs/h/h7971.md) to the ['erez](../../strongs/h/h730.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md), ['āmar](../../strongs/h/h559.md), [nathan](../../strongs/h/h5414.md) thy [bath](../../strongs/h/h1323.md) to my [ben](../../strongs/h/h1121.md) to ['ishshah](../../strongs/h/h802.md): and there ['abar](../../strongs/h/h5674.md) a [sadeh](../../strongs/h/h7704.md) [chay](../../strongs/h/h2416.md) that was in [Lᵊḇānôn](../../strongs/h/h3844.md), and [rāmas](../../strongs/h/h7429.md) the [ḥôaḥ](../../strongs/h/h2336.md).

<a name="2chronicles_25_19"></a>2Chronicles 25:19

Thou ['āmar](../../strongs/h/h559.md), Lo, thou hast [nakah](../../strongs/h/h5221.md) the ['Ĕḏōm](../../strongs/h/h123.md); and thine [leb](../../strongs/h/h3820.md) lifteth thee [nasa'](../../strongs/h/h5375.md) to [kabad](../../strongs/h/h3513.md): [yashab](../../strongs/h/h3427.md) now at [bayith](../../strongs/h/h1004.md); why shouldest thou [gārâ](../../strongs/h/h1624.md) to thine [ra'](../../strongs/h/h7451.md), that thou shouldest [naphal](../../strongs/h/h5307.md), even thou, and [Yehuwdah](../../strongs/h/h3063.md) with thee?

<a name="2chronicles_25_20"></a>2Chronicles 25:20

But ['Ămaṣyâ](../../strongs/h/h558.md) would not [shama'](../../strongs/h/h8085.md); for it came of ['Elohiym](../../strongs/h/h430.md), that he might [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md), because they [darash](../../strongs/h/h1875.md) after the ['Elohiym](../../strongs/h/h430.md) of ['Ĕḏōm](../../strongs/h/h123.md).

<a name="2chronicles_25_21"></a>2Chronicles 25:21

So [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [ʿālâ](../../strongs/h/h5927.md); and they [ra'ah](../../strongs/h/h7200.md) in the [paniym](../../strongs/h/h6440.md), both he and ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), at [Bêṯ Šemeš](../../strongs/h/h1053.md), which belongeth to [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_25_22"></a>2Chronicles 25:22

And [Yehuwdah](../../strongs/h/h3063.md) was put to the [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) [Yisra'el](../../strongs/h/h3478.md), and they [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) to his ['ohel](../../strongs/h/h168.md).

<a name="2chronicles_25_23"></a>2Chronicles 25:23

And [Yô'Āš](../../strongs/h/h3101.md) the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) [tāp̄aś](../../strongs/h/h8610.md) ['Ămaṣyâ](../../strongs/h/h558.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md), the [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md), at [Bêṯ Šemeš](../../strongs/h/h1053.md), and [bow'](../../strongs/h/h935.md) him to [Yĕruwshalaim](../../strongs/h/h3389.md), and brake [pāraṣ](../../strongs/h/h6555.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) from the [sha'ar](../../strongs/h/h8179.md) of ['Ep̄rayim](../../strongs/h/h669.md) to the [panah](../../strongs/h/h6437.md) [sha'ar](../../strongs/h/h8179.md), four hundred ['ammâ](../../strongs/h/h520.md).

<a name="2chronicles_25_24"></a>2Chronicles 25:24

And he took all the [zāhāḇ](../../strongs/h/h2091.md) and the [keceph](../../strongs/h/h3701.md), and all the [kĕliy](../../strongs/h/h3627.md) that were [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md) with [ʿŌḇēḏ 'Ĕḏōm](../../strongs/h/h5654.md), and the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), the [ben](../../strongs/h/h1121.md) [taʿărûḇâ](../../strongs/h/h8594.md) also, and [shuwb](../../strongs/h/h7725.md) to [Šōmrôn](../../strongs/h/h8111.md).

<a name="2chronicles_25_25"></a>2Chronicles 25:25

And ['Ămaṣyâ](../../strongs/h/h558.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āš](../../strongs/h/h3101.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ḥāyâ](../../strongs/h/h2421.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of [Yô'Āš](../../strongs/h/h3101.md) [ben](../../strongs/h/h1121.md) of [Yᵊhô'Āḥāz](../../strongs/h/h3059.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) fifteen [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_25_26"></a>2Chronicles 25:26

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of ['Ămaṣyâ](../../strongs/h/h558.md), [ri'šôn](../../strongs/h/h7223.md) and ['aḥărôn](../../strongs/h/h314.md), behold, are they not [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yisra'el](../../strongs/h/h3478.md)?

<a name="2chronicles_25_27"></a>2Chronicles 25:27

Now after the [ʿēṯ](../../strongs/h/h6256.md) that ['Ămaṣyâ](../../strongs/h/h558.md) did [cuwr](../../strongs/h/h5493.md) from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md) they [qāšar](../../strongs/h/h7194.md) a [qešer](../../strongs/h/h7195.md) against him in [Yĕruwshalaim](../../strongs/h/h3389.md); and he [nûs](../../strongs/h/h5127.md) to [Lāḵîš](../../strongs/h/h3923.md): but they [shalach](../../strongs/h/h7971.md) to [Lāḵîš](../../strongs/h/h3923.md) ['aḥar](../../strongs/h/h310.md) him, and [muwth](../../strongs/h/h4191.md) him there.

<a name="2chronicles_25_28"></a>2Chronicles 25:28

And they [nasa'](../../strongs/h/h5375.md) him upon [sûs](../../strongs/h/h5483.md), and [qāḇar](../../strongs/h/h6912.md) him with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 24](2chronicles_24.md) - [2Chronicles 26](2chronicles_26.md)