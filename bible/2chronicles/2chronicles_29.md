# [2Chronicles 29](https://www.blueletterbible.org/kjv/2chronicles/29)

<a name="2chronicles_29_1"></a>2Chronicles 29:1

[Yᵊḥizqîyâ](../../strongs/h/h3169.md) began to [mālaḵ](../../strongs/h/h4427.md) when he was five and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), and he [mālaḵ](../../strongs/h/h4427.md) nine and twenty [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was ['Ăḇîâ](../../strongs/h/h29.md), the [bath](../../strongs/h/h1323.md) of [Zᵊḵaryâ](../../strongs/h/h2148.md).

<a name="2chronicles_29_2"></a>2Chronicles 29:2

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md).

<a name="2chronicles_29_3"></a>2Chronicles 29:3

He in the [ri'šôn](../../strongs/h/h7223.md) [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), in the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md), [pāṯaḥ](../../strongs/h/h6605.md) the [deleṯ](../../strongs/h/h1817.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [ḥāzaq](../../strongs/h/h2388.md) them.

<a name="2chronicles_29_4"></a>2Chronicles 29:4

And he [bow'](../../strongs/h/h935.md) the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and gathered them ['āsap̄](../../strongs/h/h622.md) into the [mizrach](../../strongs/h/h4217.md) [rᵊḥōḇ](../../strongs/h/h7339.md),

<a name="2chronicles_29_5"></a>2Chronicles 29:5

And ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md) me, ye [Lᵊvî](../../strongs/h/h3881.md), [qadash](../../strongs/h/h6942.md) now yourselves, and [qadash](../../strongs/h/h6942.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of your ['ab](../../strongs/h/h1.md), and [yāṣā'](../../strongs/h/h3318.md) the [nidâ](../../strongs/h/h5079.md) out of the [qodesh](../../strongs/h/h6944.md) place.

<a name="2chronicles_29_6"></a>2Chronicles 29:6

For our ['ab](../../strongs/h/h1.md) have [māʿal](../../strongs/h/h4603.md), and ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), and have ['azab](../../strongs/h/h5800.md) him, and have [cabab](../../strongs/h/h5437.md) their [paniym](../../strongs/h/h6440.md) from the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md), and [nathan](../../strongs/h/h5414.md) their [ʿōrep̄](../../strongs/h/h6203.md).

<a name="2chronicles_29_7"></a>2Chronicles 29:7

Also they have [cagar](../../strongs/h/h5462.md) the [deleṯ](../../strongs/h/h1817.md) of the ['ûlām](../../strongs/h/h197.md), and  [kāḇâ](../../strongs/h/h3518.md) the [nîr](../../strongs/h/h5216.md), and have not [qāṭar](../../strongs/h/h6999.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) nor [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) in the [qodesh](../../strongs/h/h6944.md) place unto the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_29_8"></a>2Chronicles 29:8

Wherefore the [qeṣep̄](../../strongs/h/h7110.md) of [Yĕhovah](../../strongs/h/h3068.md) was upon [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), and he hath [nathan](../../strongs/h/h5414.md) them to [zaʿăvâ](../../strongs/h/h2189.md) [zᵊvāʿâ](../../strongs/h/h2113.md), to [šammâ](../../strongs/h/h8047.md), and to [šᵊrēqâ](../../strongs/h/h8322.md), as ye [ra'ah](../../strongs/h/h7200.md) with your ['ayin](../../strongs/h/h5869.md).

<a name="2chronicles_29_9"></a>2Chronicles 29:9

For, lo, our ['ab](../../strongs/h/h1.md) have [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), and our [ben](../../strongs/h/h1121.md) and our [bath](../../strongs/h/h1323.md) and our ['ishshah](../../strongs/h/h802.md) are in [šᵊḇî](../../strongs/h/h7628.md) for this.

<a name="2chronicles_29_10"></a>2Chronicles 29:10

Now it is in mine [lebab](../../strongs/h/h3824.md) to [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), that his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) may [shuwb](../../strongs/h/h7725.md) from us.

<a name="2chronicles_29_11"></a>2Chronicles 29:11

My [ben](../../strongs/h/h1121.md), be not now [šālâ](../../strongs/h/h7952.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md) you to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) him, to [sharath](../../strongs/h/h8334.md) him, and that ye should [sharath](../../strongs/h/h8334.md) unto him, and [qāṭar](../../strongs/h/h6999.md).

<a name="2chronicles_29_12"></a>2Chronicles 29:12

Then the [Lᵊvî](../../strongs/h/h3881.md) [quwm](../../strongs/h/h6965.md), [Maḥaṯ](../../strongs/h/h4287.md) the [ben](../../strongs/h/h1121.md) of [ʿĂmāśay](../../strongs/h/h6022.md), and [Yô'Ēl](../../strongs/h/h3100.md) the [ben](../../strongs/h/h1121.md) of [ʿĂzaryâ](../../strongs/h/h5838.md), of the [ben](../../strongs/h/h1121.md) of the [Qᵊhāṯî](../../strongs/h/h6956.md): and of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md), [Qîš](../../strongs/h/h7027.md) the [ben](../../strongs/h/h1121.md) of [ʿAḇdî](../../strongs/h/h5660.md), and [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhallel'Ēl](../../strongs/h/h3094.md): and of the [Gēršunnî](../../strongs/h/h1649.md); [Yô'āḥ](../../strongs/h/h3098.md) the [ben](../../strongs/h/h1121.md) of [Zimmâ](../../strongs/h/h2155.md), and ['Eden](../../strongs/h/h5731.md) the [ben](../../strongs/h/h1121.md) of [Yô'āḥ](../../strongs/h/h3098.md):

<a name="2chronicles_29_13"></a>2Chronicles 29:13

And of the [ben](../../strongs/h/h1121.md) of ['Ĕlîṣāp̄ān](../../strongs/h/h469.md); [Šimrî](../../strongs/h/h8113.md), and [YᵊʿÎ'Ēl](../../strongs/h/h3273.md): and of the [ben](../../strongs/h/h1121.md) of ['Āsāp̄](../../strongs/h/h623.md); [Zᵊḵaryâ](../../strongs/h/h2148.md), and [Matanyâ](../../strongs/h/h4983.md):

<a name="2chronicles_29_14"></a>2Chronicles 29:14

And of the [ben](../../strongs/h/h1121.md) of [Hêmān](../../strongs/h/h1968.md); [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [Šimʿî](../../strongs/h/h8096.md): and of the [ben](../../strongs/h/h1121.md) of [Yᵊḏûṯûn](../../strongs/h/h3038.md); [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and [ʿUzzî'ēl](../../strongs/h/h5816.md).

<a name="2chronicles_29_15"></a>2Chronicles 29:15

And they ['āsap̄](../../strongs/h/h622.md) their ['ach](../../strongs/h/h251.md), and [qadash](../../strongs/h/h6942.md) themselves, and [bow'](../../strongs/h/h935.md), according to the [mitsvah](../../strongs/h/h4687.md) of the [melek](../../strongs/h/h4428.md), by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), to [ṭāhēr](../../strongs/h/h2891.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_29_16"></a>2Chronicles 29:16

And the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) into the [pᵊnîmâ](../../strongs/h/h6441.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), to [ṭāhēr](../../strongs/h/h2891.md) it, and [yāṣā'](../../strongs/h/h3318.md) all the [ṭām'â](../../strongs/h/h2932.md) that they [māṣā'](../../strongs/h/h4672.md) in the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md) into the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And the [Lᵊvî](../../strongs/h/h3881.md) [qāḇal](../../strongs/h/h6901.md) it, to [yāṣā'](../../strongs/h/h3318.md) it [ḥûṣ](../../strongs/h/h2351.md) into the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md).

<a name="2chronicles_29_17"></a>2Chronicles 29:17

Now they [ḥālal](../../strongs/h/h2490.md) on the first day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) to [qadash](../../strongs/h/h6942.md), and on the eighth [yowm](../../strongs/h/h3117.md) of the [ḥōḏeš](../../strongs/h/h2320.md) [bow'](../../strongs/h/h935.md) they to the ['ûlām](../../strongs/h/h197.md) of [Yĕhovah](../../strongs/h/h3068.md): so they [qadash](../../strongs/h/h6942.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) in eight [yowm](../../strongs/h/h3117.md); and in the sixteenth day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) they made a [kalah](../../strongs/h/h3615.md).

<a name="2chronicles_29_18"></a>2Chronicles 29:18

Then they [bow'](../../strongs/h/h935.md) [pᵊnîmâ](../../strongs/h/h6441.md) to [Ḥizqîyâ](../../strongs/h/h2396.md) the [melek](../../strongs/h/h4428.md), and ['āmar](../../strongs/h/h559.md), We have [ṭāhēr](../../strongs/h/h2891.md) all the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [mizbeach](../../strongs/h/h4196.md) of [ʿōlâ](../../strongs/h/h5930.md), with all the [kĕliy](../../strongs/h/h3627.md) thereof, and the [maʿăreḵeṯ](../../strongs/h/h4635.md) [šulḥān](../../strongs/h/h7979.md), with all the [kĕliy](../../strongs/h/h3627.md) thereof.

<a name="2chronicles_29_19"></a>2Chronicles 29:19

Moreover all the [kĕliy](../../strongs/h/h3627.md), which [melek](../../strongs/h/h4428.md) ['Āḥāz](../../strongs/h/h271.md) in his [malkuwth](../../strongs/h/h4438.md) did [zānaḥ](../../strongs/h/h2186.md) in his [maʿal](../../strongs/h/h4604.md), have we [kuwn](../../strongs/h/h3559.md) and [qadash](../../strongs/h/h6942.md), and, behold, they are [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_29_20"></a>2Chronicles 29:20

Then [Yᵊḥizqîyâ](../../strongs/h/h3169.md) the [melek](../../strongs/h/h4428.md) [šāḵam](../../strongs/h/h7925.md), and ['āsap̄](../../strongs/h/h622.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md), and [ʿālâ](../../strongs/h/h5927.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_29_21"></a>2Chronicles 29:21

And they [bow'](../../strongs/h/h935.md) seven [par](../../strongs/h/h6499.md), and seven ['ayil](../../strongs/h/h352.md), and seven [keḇeś](../../strongs/h/h3532.md), and seven [ṣāp̄îr](../../strongs/h/h6842.md) [ʿēz](../../strongs/h/h5795.md), for a sin [chatta'ath](../../strongs/h/h2403.md) for the [mamlāḵâ](../../strongs/h/h4467.md), and for the [miqdash](../../strongs/h/h4720.md), and for [Yehuwdah](../../strongs/h/h3063.md). And he ['āmar](../../strongs/h/h559.md) the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) to [ʿālâ](../../strongs/h/h5927.md) them on the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_29_22"></a>2Chronicles 29:22

So they [šāḥaṭ](../../strongs/h/h7819.md) the [bāqār](../../strongs/h/h1241.md), and the [kōhēn](../../strongs/h/h3548.md) [qāḇal](../../strongs/h/h6901.md) the [dam](../../strongs/h/h1818.md), and [zāraq](../../strongs/h/h2236.md) it on the [mizbeach](../../strongs/h/h4196.md): likewise, when they had [šāḥaṭ](../../strongs/h/h7819.md) the ['ayil](../../strongs/h/h352.md), they [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md): they [šāḥaṭ](../../strongs/h/h7819.md) also the [keḇeś](../../strongs/h/h3532.md), and they [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md).

<a name="2chronicles_29_23"></a>2Chronicles 29:23

And they [nāḡaš](../../strongs/h/h5066.md) the [śāʿîr](../../strongs/h/h8163.md) for the [chatta'ath](../../strongs/h/h2403.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) and the [qāhēl](../../strongs/h/h6951.md); and they [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon them:

<a name="2chronicles_29_24"></a>2Chronicles 29:24

And the [kōhēn](../../strongs/h/h3548.md) [šāḥaṭ](../../strongs/h/h7819.md) them, and they made [chata'](../../strongs/h/h2398.md) with their [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for all [Yisra'el](../../strongs/h/h3478.md): for the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) that the [ʿōlâ](../../strongs/h/h5930.md) and the [chatta'ath](../../strongs/h/h2403.md) should be made for all [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_29_25"></a>2Chronicles 29:25

And he ['amad](../../strongs/h/h5975.md) the [Lᵊvî](../../strongs/h/h3881.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) with [mᵊṣēleṯ](../../strongs/h/h4700.md), with [neḇel](../../strongs/h/h5035.md), and with [kinnôr](../../strongs/h/h3658.md), according to the [mitsvah](../../strongs/h/h4687.md) of [Dāviḏ](../../strongs/h/h1732.md), and of [Gāḏ](../../strongs/h/h1410.md) the [melek](../../strongs/h/h4428.md) [ḥōzê](../../strongs/h/h2374.md), and [Nāṯān](../../strongs/h/h5416.md) the [nāḇî'](../../strongs/h/h5030.md): for so was the [mitsvah](../../strongs/h/h4687.md) [yad](../../strongs/h/h3027.md) [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md) his [nāḇî'](../../strongs/h/h5030.md).

<a name="2chronicles_29_26"></a>2Chronicles 29:26

And the [Lᵊvî](../../strongs/h/h3881.md) ['amad](../../strongs/h/h5975.md) with the [kĕliy](../../strongs/h/h3627.md) of [Dāviḏ](../../strongs/h/h1732.md), and the [kōhēn](../../strongs/h/h3548.md) with the [ḥăṣōṣrâ](../../strongs/h/h2689.md).

<a name="2chronicles_29_27"></a>2Chronicles 29:27

And [Ḥizqîyâ](../../strongs/h/h2396.md) ['āmar](../../strongs/h/h559.md) to [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md) upon the [mizbeach](../../strongs/h/h4196.md). And [ʿēṯ](../../strongs/h/h6256.md) the [ʿōlâ](../../strongs/h/h5930.md) [ḥālal](../../strongs/h/h2490.md), the [šîr](../../strongs/h/h7892.md) of [Yĕhovah](../../strongs/h/h3068.md) [ḥālal](../../strongs/h/h2490.md) also with the [ḥăṣōṣrâ](../../strongs/h/h2689.md), and with the [kĕliy](../../strongs/h/h3627.md) ordained [yad](../../strongs/h/h3027.md) [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_29_28"></a>2Chronicles 29:28

And all the [qāhēl](../../strongs/h/h6951.md) [shachah](../../strongs/h/h7812.md), and the [šîr](../../strongs/h/h7892.md) [shiyr](../../strongs/h/h7891.md), and the [ḥăṣōṣrâ](../../strongs/h/h2689.md) [ḥāṣar](../../strongs/h/h2690.md) [ḥāṣar](../../strongs/h/h2690.md): and all this continued until the [ʿōlâ](../../strongs/h/h5930.md) was [kalah](../../strongs/h/h3615.md).

<a name="2chronicles_29_29"></a>2Chronicles 29:29

And when they had made a [kalah](../../strongs/h/h3615.md) of [ʿālâ](../../strongs/h/h5927.md), the [melek](../../strongs/h/h4428.md) and all that were [māṣā'](../../strongs/h/h4672.md) with him [kara'](../../strongs/h/h3766.md) themselves, and [shachah](../../strongs/h/h7812.md).

<a name="2chronicles_29_30"></a>2Chronicles 29:30

Moreover [Yᵊḥizqîyâ](../../strongs/h/h3169.md) the [melek](../../strongs/h/h4428.md) and the [śar](../../strongs/h/h8269.md) ['āmar](../../strongs/h/h559.md) the [Lᵊvî](../../strongs/h/h3881.md) to [halal](../../strongs/h/h1984.md) unto [Yĕhovah](../../strongs/h/h3068.md) with the [dabar](../../strongs/h/h1697.md) of [Dāviḏ](../../strongs/h/h1732.md), and of ['Āsāp̄](../../strongs/h/h623.md) the [ḥōzê](../../strongs/h/h2374.md). And they [halal](../../strongs/h/h1984.md) with [simchah](../../strongs/h/h8057.md), and they [qāḏaḏ](../../strongs/h/h6915.md) and [shachah](../../strongs/h/h7812.md).

<a name="2chronicles_29_31"></a>2Chronicles 29:31

Then [Yᵊḥizqîyâ](../../strongs/h/h3169.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md), Now ye have [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) yourselves unto [Yĕhovah](../../strongs/h/h3068.md), [nāḡaš](../../strongs/h/h5066.md) and [bow'](../../strongs/h/h935.md) [zebach](../../strongs/h/h2077.md) and [tôḏâ](../../strongs/h/h8426.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And the [qāhēl](../../strongs/h/h6951.md) [bow'](../../strongs/h/h935.md) [zebach](../../strongs/h/h2077.md) and [tôḏâ](../../strongs/h/h8426.md); and as many as were of a [nāḏîḇ](../../strongs/h/h5081.md) [leb](../../strongs/h/h3820.md) [ʿōlâ](../../strongs/h/h5930.md).

<a name="2chronicles_29_32"></a>2Chronicles 29:32

And the [mispār](../../strongs/h/h4557.md) of the [ʿōlâ](../../strongs/h/h5930.md), which the [qāhēl](../../strongs/h/h6951.md) [bow'](../../strongs/h/h935.md), was threescore and ten [bāqār](../../strongs/h/h1241.md), an hundred ['ayil](../../strongs/h/h352.md), and two hundred [keḇeś](../../strongs/h/h3532.md): all these were for a [ʿōlâ](../../strongs/h/h5930.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_29_33"></a>2Chronicles 29:33

And the [qodesh](../../strongs/h/h6944.md) were six hundred [bāqār](../../strongs/h/h1241.md) and three thousand [tso'n](../../strongs/h/h6629.md).

<a name="2chronicles_29_34"></a>2Chronicles 29:34

But the [kōhēn](../../strongs/h/h3548.md) were too [mᵊʿaṭ](../../strongs/h/h4592.md), so that they [yakol](../../strongs/h/h3201.md) not [pāšaṭ](../../strongs/h/h6584.md) all the [ʿōlâ](../../strongs/h/h5930.md): wherefore their ['ach](../../strongs/h/h251.md) the [Lᵊvî](../../strongs/h/h3881.md) did [ḥāzaq](../../strongs/h/h2388.md) them, till the [mĕla'kah](../../strongs/h/h4399.md) was [kalah](../../strongs/h/h3615.md), and until the other [kōhēn](../../strongs/h/h3548.md) had [qadash](../../strongs/h/h6942.md) themselves: for the [Lᵊvî](../../strongs/h/h3881.md) were more [yashar](../../strongs/h/h3477.md) in [lebab](../../strongs/h/h3824.md) to [qadash](../../strongs/h/h6942.md) themselves than the [kōhēn](../../strongs/h/h3548.md).

<a name="2chronicles_29_35"></a>2Chronicles 29:35

And also the [ʿōlâ](../../strongs/h/h5930.md) were in [rōḇ](../../strongs/h/h7230.md), with the [cheleb](../../strongs/h/h2459.md) of the [šelem](../../strongs/h/h8002.md), and the [necek](../../strongs/h/h5262.md) for every [ʿōlâ](../../strongs/h/h5930.md). So the [ʿăḇōḏâ](../../strongs/h/h5656.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) was set in [kuwn](../../strongs/h/h3559.md).

<a name="2chronicles_29_36"></a>2Chronicles 29:36

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [samach](../../strongs/h/h8055.md), and all the ['am](../../strongs/h/h5971.md), that ['Elohiym](../../strongs/h/h430.md) had [kuwn](../../strongs/h/h3559.md) the ['am](../../strongs/h/h5971.md): for the [dabar](../../strongs/h/h1697.md) was done [piṯ'ōm](../../strongs/h/h6597.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 28](2chronicles_28.md) - [2Chronicles 30](2chronicles_30.md)