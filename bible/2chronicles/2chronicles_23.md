# [2Chronicles 23](https://www.blueletterbible.org/kjv/2chronicles/23)

<a name="2chronicles_23_1"></a>2Chronicles 23:1

And in the seventh [šānâ](../../strongs/h/h8141.md) [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [ḥāzaq](../../strongs/h/h2388.md) himself, and [laqach](../../strongs/h/h3947.md) the [śar](../../strongs/h/h8269.md) of hundreds, [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [Yᵊrōḥām](../../strongs/h/h3395.md), and [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôḥānān](../../strongs/h/h3076.md), and [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [ʿÔḇēḏ](../../strongs/h/h5744.md), and [MaʿĂśêâ](../../strongs/h/h4641.md) the [ben](../../strongs/h/h1121.md) of [ʿĂḏāyâ](../../strongs/h/h5718.md), and ['Ĕlîšāp̄Āṭ](../../strongs/h/h478.md) the [ben](../../strongs/h/h1121.md) of [Ziḵrî](../../strongs/h/h2147.md), into [bĕriyth](../../strongs/h/h1285.md) with him.

<a name="2chronicles_23_2"></a>2Chronicles 23:2

And they went [cabab](../../strongs/h/h5437.md) in [Yehuwdah](../../strongs/h/h3063.md), and [qāḇaṣ](../../strongs/h/h6908.md) the [Lᵊvî](../../strongs/h/h3881.md) out of all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md) of [Yisra'el](../../strongs/h/h3478.md), and they [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_23_3"></a>2Chronicles 23:3

And all the [qāhēl](../../strongs/h/h6951.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with the [melek](../../strongs/h/h4428.md) in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md). And he ['āmar](../../strongs/h/h559.md) unto them, Behold, the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md) shall [mālaḵ](../../strongs/h/h4427.md), as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) of the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md).

<a name="2chronicles_23_4"></a>2Chronicles 23:4

This is the [dabar](../../strongs/h/h1697.md) that ye shall ['asah](../../strongs/h/h6213.md); A third part of you [bow'](../../strongs/h/h935.md) on the [shabbath](../../strongs/h/h7676.md), of the [kōhēn](../../strongs/h/h3548.md) and of the [Lᵊvî](../../strongs/h/h3881.md), shall be [šôʿēr](../../strongs/h/h7778.md) of the [caph](../../strongs/h/h5592.md);

<a name="2chronicles_23_5"></a>2Chronicles 23:5

And a third part shall be at the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md); and a third part at the [sha'ar](../../strongs/h/h8179.md) of the [yᵊsôḏ](../../strongs/h/h3247.md): and all the ['am](../../strongs/h/h5971.md) shall be in the [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_23_6"></a>2Chronicles 23:6

But let none [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), save the [kōhēn](../../strongs/h/h3548.md), and they that [sharath](../../strongs/h/h8334.md) of the [Lᵊvî](../../strongs/h/h3881.md); they shall [bow'](../../strongs/h/h935.md), for they are [qodesh](../../strongs/h/h6944.md): but all the ['am](../../strongs/h/h5971.md) shall [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_23_7"></a>2Chronicles 23:7

And the [Lᵊvî](../../strongs/h/h3881.md) shall [naqaph](../../strongs/h/h5362.md) the [melek](../../strongs/h/h4428.md) [cabiyb](../../strongs/h/h5439.md), every ['iysh](../../strongs/h/h376.md) with his [kĕliy](../../strongs/h/h3627.md) in his [yad](../../strongs/h/h3027.md); and whosoever else [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md), he shall be put to [muwth](../../strongs/h/h4191.md): but be ye with the [melek](../../strongs/h/h4428.md) when he [bow'](../../strongs/h/h935.md), and when he [yāṣā'](../../strongs/h/h3318.md).

<a name="2chronicles_23_8"></a>2Chronicles 23:8

So the [Lᵊvî](../../strongs/h/h3881.md) and all [Yehuwdah](../../strongs/h/h3063.md) ['asah](../../strongs/h/h6213.md) according to all things that [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) had [tsavah](../../strongs/h/h6680.md), and [laqach](../../strongs/h/h3947.md) every ['iysh](../../strongs/h/h376.md) his ['enowsh](../../strongs/h/h582.md) that were to [bow'](../../strongs/h/h935.md) on the [shabbath](../../strongs/h/h7676.md), with them that were to [yāṣā'](../../strongs/h/h3318.md) out on the [shabbath](../../strongs/h/h7676.md): for [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [p̄ṭvr](../../strongs/h/h6358.md) not the [maḥălōqeṯ](../../strongs/h/h4256.md).

<a name="2chronicles_23_9"></a>2Chronicles 23:9

Moreover [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [nathan](../../strongs/h/h5414.md) to the [śar](../../strongs/h/h8269.md) of hundreds [ḥănîṯ](../../strongs/h/h2595.md), and [magen](../../strongs/h/h4043.md), and [šeleṭ](../../strongs/h/h7982.md), that had been [melek](../../strongs/h/h4428.md) [Dāviḏ](../../strongs/h/h1732.md), which were in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_23_10"></a>2Chronicles 23:10

And he ['amad](../../strongs/h/h5975.md) all the ['am](../../strongs/h/h5971.md), every ['iysh](../../strongs/h/h376.md) having his [šelaḥ](../../strongs/h/h7973.md) in his [yad](../../strongs/h/h3027.md), from the [yᵊmānî](../../strongs/h/h3233.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md) to the [śᵊmā'lî](../../strongs/h/h8042.md) [kāṯēp̄](../../strongs/h/h3802.md) of the [bayith](../../strongs/h/h1004.md), along by the [mizbeach](../../strongs/h/h4196.md) and the [bayith](../../strongs/h/h1004.md), by the [melek](../../strongs/h/h4428.md) [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_23_11"></a>2Chronicles 23:11

Then they [yāṣā'](../../strongs/h/h3318.md) the [melek](../../strongs/h/h4428.md) [ben](../../strongs/h/h1121.md), and [nathan](../../strongs/h/h5414.md) upon him the [nēzer](../../strongs/h/h5145.md), and gave him the [ʿēḏûṯ](../../strongs/h/h5715.md), and made him [mālaḵ](../../strongs/h/h4427.md). And [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) and his [ben](../../strongs/h/h1121.md) [māšaḥ](../../strongs/h/h4886.md) him, and ['āmar](../../strongs/h/h559.md), [ḥāyâ](../../strongs/h/h2421.md) the [melek](../../strongs/h/h4428.md).

<a name="2chronicles_23_12"></a>2Chronicles 23:12

Now when [ʿĂṯalyâ](../../strongs/h/h6271.md) [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the ['am](../../strongs/h/h5971.md) [rûṣ](../../strongs/h/h7323.md) and [halal](../../strongs/h/h1984.md) the [melek](../../strongs/h/h4428.md), she [bow'](../../strongs/h/h935.md) to the ['am](../../strongs/h/h5971.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="2chronicles_23_13"></a>2Chronicles 23:13

And she [ra'ah](../../strongs/h/h7200.md), and, behold, the [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) at his [ʿammûḏ](../../strongs/h/h5982.md) at the [māḇô'](../../strongs/h/h3996.md), and the [śar](../../strongs/h/h8269.md) and the [ḥăṣōṣrâ](../../strongs/h/h2689.md) by the [melek](../../strongs/h/h4428.md): and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [śāmēaḥ](../../strongs/h/h8056.md), and [tāqaʿ](../../strongs/h/h8628.md) with [ḥăṣōṣrâ](../../strongs/h/h2689.md), also the [shiyr](../../strongs/h/h7891.md) with [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md), and such as [yada'](../../strongs/h/h3045.md) to [halal](../../strongs/h/h1984.md). Then [ʿĂṯalyâ](../../strongs/h/h6271.md) [qāraʿ](../../strongs/h/h7167.md) her [beḡeḏ](../../strongs/h/h899.md), and ['āmar](../../strongs/h/h559.md), [qešer](../../strongs/h/h7195.md), [qešer](../../strongs/h/h7195.md).

<a name="2chronicles_23_14"></a>2Chronicles 23:14

Then [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md) [yāṣā'](../../strongs/h/h3318.md) the [śar](../../strongs/h/h8269.md) of hundreds that were set [paqad](../../strongs/h/h6485.md) the [ḥayil](../../strongs/h/h2428.md), and ['āmar](../../strongs/h/h559.md) unto them, Have her [yāṣā'](../../strongs/h/h3318.md) of the [bayith](../../strongs/h/h1004.md) [śᵊḏērâ](../../strongs/h/h7713.md): and whoso [bow'](../../strongs/h/h935.md) ['aḥar](../../strongs/h/h310.md) her, let him be [muwth](../../strongs/h/h4191.md) with the [chereb](../../strongs/h/h2719.md). For the [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md), [muwth](../../strongs/h/h4191.md) her not in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_23_15"></a>2Chronicles 23:15

So they [śûm](../../strongs/h/h7760.md) [yad](../../strongs/h/h3027.md) on her; and when she was [bow'](../../strongs/h/h935.md) to the [māḇô'](../../strongs/h/h3996.md) of the [sûs](../../strongs/h/h5483.md) [sha'ar](../../strongs/h/h8179.md) by the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), they [muwth](../../strongs/h/h4191.md) her there.

<a name="2chronicles_23_16"></a>2Chronicles 23:16

And [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) between him, and between all the ['am](../../strongs/h/h5971.md), and between the [melek](../../strongs/h/h4428.md), that they should be [Yĕhovah](../../strongs/h/h3068.md) ['am](../../strongs/h/h5971.md).

<a name="2chronicles_23_17"></a>2Chronicles 23:17

Then all the ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of [BaʿAl](../../strongs/h/h1168.md), and [nāṯaṣ](../../strongs/h/h5422.md) it, and [shabar](../../strongs/h/h7665.md) his [mizbeach](../../strongs/h/h4196.md) and his [tselem](../../strongs/h/h6754.md) in [shabar](../../strongs/h/h7665.md), and [harag](../../strongs/h/h2026.md) [Matān](../../strongs/h/h4977.md) the [kōhēn](../../strongs/h/h3548.md) of [BaʿAl](../../strongs/h/h1168.md) [paniym](../../strongs/h/h6440.md) the [mizbeach](../../strongs/h/h4196.md).

<a name="2chronicles_23_18"></a>2Chronicles 23:18

Also [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) [śûm](../../strongs/h/h7760.md) the [pᵊqudâ](../../strongs/h/h6486.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) by the [yad](../../strongs/h/h3027.md) of the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md), whom [Dāviḏ](../../strongs/h/h1732.md) had [chalaq](../../strongs/h/h2505.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), to [ʿālâ](../../strongs/h/h5927.md) the [ʿōlâ](../../strongs/h/h5930.md) of [Yĕhovah](../../strongs/h/h3068.md), as it is [kāṯaḇ](../../strongs/h/h3789.md) in the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md), with [simchah](../../strongs/h/h8057.md) and with [šîr](../../strongs/h/h7892.md), as it was [yad](../../strongs/h/h3027.md) [Dāviḏ](../../strongs/h/h1732.md).

<a name="2chronicles_23_19"></a>2Chronicles 23:19

And he ['amad](../../strongs/h/h5975.md) the [šôʿēr](../../strongs/h/h7778.md) at the [sha'ar](../../strongs/h/h8179.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), that none which was [tame'](../../strongs/h/h2931.md) in any [dabar](../../strongs/h/h1697.md) should [bow'](../../strongs/h/h935.md).

<a name="2chronicles_23_20"></a>2Chronicles 23:20

And he [laqach](../../strongs/h/h3947.md) the [śar](../../strongs/h/h8269.md) of hundreds, and the ['addiyr](../../strongs/h/h117.md), and the [mashal](../../strongs/h/h4910.md) of the ['am](../../strongs/h/h5971.md), and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), and brought [yarad](../../strongs/h/h3381.md) the [melek](../../strongs/h/h4428.md) from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and they [bow'](../../strongs/h/h935.md) [tavek](../../strongs/h/h8432.md) the ['elyown](../../strongs/h/h5945.md) [sha'ar](../../strongs/h/h8179.md) into the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [yashab](../../strongs/h/h3427.md) the [melek](../../strongs/h/h4428.md) upon the [kicce'](../../strongs/h/h3678.md) of the [mamlāḵâ](../../strongs/h/h4467.md).

<a name="2chronicles_23_21"></a>2Chronicles 23:21

And all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [samach](../../strongs/h/h8055.md): and the [ʿîr](../../strongs/h/h5892.md) was [šāqaṭ](../../strongs/h/h8252.md), after that they had [muwth](../../strongs/h/h4191.md) [ʿĂṯalyâ](../../strongs/h/h6271.md) with the [chereb](../../strongs/h/h2719.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 22](2chronicles_22.md) - [2Chronicles 24](2chronicles_24.md)