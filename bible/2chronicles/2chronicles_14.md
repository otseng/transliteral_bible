# [2Chronicles 14](https://www.blueletterbible.org/kjv/2chronicles/14)

<a name="2chronicles_14_1"></a>2Chronicles 14:1

So ['Ăḇîâ](../../strongs/h/h29.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md): and ['Āsā'](../../strongs/h/h609.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead. In his [yowm](../../strongs/h/h3117.md) the ['erets](../../strongs/h/h776.md) was [šāqaṭ](../../strongs/h/h8252.md) ten [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_14_2"></a>2Chronicles 14:2

And ['Āsā'](../../strongs/h/h609.md) ['asah](../../strongs/h/h6213.md) that which was [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md):

<a name="2chronicles_14_3"></a>2Chronicles 14:3

For he [cuwr](../../strongs/h/h5493.md) the [mizbeach](../../strongs/h/h4196.md) of the [nēḵār](../../strongs/h/h5236.md), and the [bāmâ](../../strongs/h/h1116.md), and [shabar](../../strongs/h/h7665.md) the [maṣṣēḇâ](../../strongs/h/h4676.md), and [gāḏaʿ](../../strongs/h/h1438.md) the ['ăšērâ](../../strongs/h/h842.md):

<a name="2chronicles_14_4"></a>2Chronicles 14:4

And ['āmar](../../strongs/h/h559.md) [Yehuwdah](../../strongs/h/h3063.md) to [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), and to ['asah](../../strongs/h/h6213.md) the [towrah](../../strongs/h/h8451.md) and the [mitsvah](../../strongs/h/h4687.md).

<a name="2chronicles_14_5"></a>2Chronicles 14:5

Also he [cuwr](../../strongs/h/h5493.md) out of all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) the [bāmâ](../../strongs/h/h1116.md) and the [ḥammān](../../strongs/h/h2553.md): and the [mamlāḵâ](../../strongs/h/h4467.md) was [šāqaṭ](../../strongs/h/h8252.md) [paniym](../../strongs/h/h6440.md) him.

<a name="2chronicles_14_6"></a>2Chronicles 14:6

And he [bānâ](../../strongs/h/h1129.md) [mᵊṣûrâ](../../strongs/h/h4694.md) [ʿîr](../../strongs/h/h5892.md) in [Yehuwdah](../../strongs/h/h3063.md): for the ['erets](../../strongs/h/h776.md) had [šāqaṭ](../../strongs/h/h8252.md), and he had no [milḥāmâ](../../strongs/h/h4421.md) in those [šānâ](../../strongs/h/h8141.md); because [Yĕhovah](../../strongs/h/h3068.md) had given him [nuwach](../../strongs/h/h5117.md).

<a name="2chronicles_14_7"></a>2Chronicles 14:7

Therefore he ['āmar](../../strongs/h/h559.md) unto [Yehuwdah](../../strongs/h/h3063.md), Let us [bānâ](../../strongs/h/h1129.md) these [ʿîr](../../strongs/h/h5892.md), and make [cabab](../../strongs/h/h5437.md) them [ḥômâ](../../strongs/h/h2346.md), and [miḡdāl](../../strongs/h/h4026.md), [deleṯ](../../strongs/h/h1817.md), and [bᵊrîaḥ](../../strongs/h/h1280.md), while the ['erets](../../strongs/h/h776.md) is yet [paniym](../../strongs/h/h6440.md) us; because we have [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), we have [darash](../../strongs/h/h1875.md) him, and he hath [nuwach](../../strongs/h/h5117.md) us [cabiyb](../../strongs/h/h5439.md). So they [bānâ](../../strongs/h/h1129.md) and [tsalach](../../strongs/h/h6743.md).

<a name="2chronicles_14_8"></a>2Chronicles 14:8

And ['Āsā'](../../strongs/h/h609.md) had an [ḥayil](../../strongs/h/h2428.md) of men that [nasa'](../../strongs/h/h5375.md) [tsinnah](../../strongs/h/h6793.md) and [rōmaḥ](../../strongs/h/h7420.md), out of [Yehuwdah](../../strongs/h/h3063.md) three hundred thousand; and out of [Binyāmîn](../../strongs/h/h1144.md), that [nasa'](../../strongs/h/h5375.md) [magen](../../strongs/h/h4043.md) and [dāraḵ](../../strongs/h/h1869.md) [qesheth](../../strongs/h/h7198.md), two hundred and fourscore thousand: all these were [gibôr](../../strongs/h/h1368.md) of [ḥayil](../../strongs/h/h2428.md).

<a name="2chronicles_14_9"></a>2Chronicles 14:9

And there [yāṣā'](../../strongs/h/h3318.md) against them [Zeraḥ](../../strongs/h/h2226.md) the [Kûšî](../../strongs/h/h3569.md) with an [ḥayil](../../strongs/h/h2428.md) of a thousand thousand, and three hundred [merkāḇâ](../../strongs/h/h4818.md); and [bow'](../../strongs/h/h935.md) unto [Mārē'Šâ](../../strongs/h/h4762.md).

<a name="2chronicles_14_10"></a>2Chronicles 14:10

Then ['Āsā'](../../strongs/h/h609.md) [yāṣā'](../../strongs/h/h3318.md) [paniym](../../strongs/h/h6440.md) him, and they set the [milḥāmâ](../../strongs/h/h4421.md) in ['arak](../../strongs/h/h6186.md) in the [gay'](../../strongs/h/h1516.md) of [Ṣᵊp̄Āṯâ](../../strongs/h/h6859.md) at [Mārē'Šâ](../../strongs/h/h4762.md).

<a name="2chronicles_14_11"></a>2Chronicles 14:11

And ['Āsā'](../../strongs/h/h609.md) [qara'](../../strongs/h/h7121.md) unto [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), it is nothing with thee to [ʿāzar](../../strongs/h/h5826.md), whether with [rab](../../strongs/h/h7227.md), or with them that have no [koach](../../strongs/h/h3581.md): [ʿāzar](../../strongs/h/h5826.md) us, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); for we [šāʿan](../../strongs/h/h8172.md) on thee, and in thy [shem](../../strongs/h/h8034.md) we [bow'](../../strongs/h/h935.md) against this [hāmôn](../../strongs/h/h1995.md). [Yĕhovah](../../strongs/h/h3068.md), thou art our ['Elohiym](../../strongs/h/h430.md); let not ['enowsh](../../strongs/h/h582.md) [ʿāṣar](../../strongs/h/h6113.md) against thee.

<a name="2chronicles_14_12"></a>2Chronicles 14:12

So [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) the [Kûšî](../../strongs/h/h3569.md) [paniym](../../strongs/h/h6440.md) ['Āsā'](../../strongs/h/h609.md), and [paniym](../../strongs/h/h6440.md) [Yehuwdah](../../strongs/h/h3063.md); and the [Kûšî](../../strongs/h/h3569.md) [nûs](../../strongs/h/h5127.md).

<a name="2chronicles_14_13"></a>2Chronicles 14:13

And ['Āsā'](../../strongs/h/h609.md) and the ['am](../../strongs/h/h5971.md) that were with him [radaph](../../strongs/h/h7291.md) them unto [gᵊrār](../../strongs/h/h1642.md): and the [Kûšî](../../strongs/h/h3569.md) were [naphal](../../strongs/h/h5307.md), that they could not [miḥyâ](../../strongs/h/h4241.md) themselves; for they were [shabar](../../strongs/h/h7665.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [paniym](../../strongs/h/h6440.md) his [maḥănê](../../strongs/h/h4264.md); and they [nasa'](../../strongs/h/h5375.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md) [šālāl](../../strongs/h/h7998.md).

<a name="2chronicles_14_14"></a>2Chronicles 14:14

And they [nakah](../../strongs/h/h5221.md) all the [ʿîr](../../strongs/h/h5892.md) [cabiyb](../../strongs/h/h5439.md) [gᵊrār](../../strongs/h/h1642.md); for the [paḥaḏ](../../strongs/h/h6343.md) of [Yĕhovah](../../strongs/h/h3068.md) came upon them: and they [bāzaz](../../strongs/h/h962.md) all the [ʿîr](../../strongs/h/h5892.md); for there was [rab](../../strongs/h/h7227.md) [bizzâ](../../strongs/h/h961.md) in them.

<a name="2chronicles_14_15"></a>2Chronicles 14:15

They [nakah](../../strongs/h/h5221.md) also the ['ohel](../../strongs/h/h168.md) of [miqnê](../../strongs/h/h4735.md), and carried [šāḇâ](../../strongs/h/h7617.md) [tso'n](../../strongs/h/h6629.md) and [gāmāl](../../strongs/h/h1581.md) in [rōḇ](../../strongs/h/h7230.md), and [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 13](2chronicles_13.md) - [2Chronicles 15](2chronicles_15.md)