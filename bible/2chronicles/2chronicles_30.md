# [2Chronicles 30](https://www.blueletterbible.org/kjv/2chronicles/30)

<a name="2chronicles_30_1"></a>2Chronicles 30:1

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [shalach](../../strongs/h/h7971.md) to all [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md), and [kāṯaḇ](../../strongs/h/h3789.md) ['agereṯ](../../strongs/h/h107.md) also to ['Ep̄rayim](../../strongs/h/h669.md) and [Mᵊnaššê](../../strongs/h/h4519.md), that they should [bow'](../../strongs/h/h935.md) to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) at [Yĕruwshalaim](../../strongs/h/h3389.md), to ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_30_2"></a>2Chronicles 30:2

For the [melek](../../strongs/h/h4428.md) had taken [ya'ats](../../strongs/h/h3289.md), and his [śar](../../strongs/h/h8269.md), and all the [qāhēl](../../strongs/h/h6951.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), to ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) in the second [ḥōḏeš](../../strongs/h/h2320.md).

<a name="2chronicles_30_3"></a>2Chronicles 30:3

For they [yakol](../../strongs/h/h3201.md) not ['asah](../../strongs/h/h6213.md) it at that [ʿēṯ](../../strongs/h/h6256.md), because the [kōhēn](../../strongs/h/h3548.md) had not [qadash](../../strongs/h/h6942.md) themselves [maday](../../strongs/h/h4078.md) [day](../../strongs/h/h1767.md), neither had the ['am](../../strongs/h/h5971.md) gathered themselves ['āsap̄](../../strongs/h/h622.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_30_4"></a>2Chronicles 30:4

And the [dabar](../../strongs/h/h1697.md) [yashar](../../strongs/h/h3474.md) ['ayin](../../strongs/h/h5869.md) the [melek](../../strongs/h/h4428.md) and all the [qāhēl](../../strongs/h/h6951.md).

<a name="2chronicles_30_5"></a>2Chronicles 30:5

So they ['amad](../../strongs/h/h5975.md) a [dabar](../../strongs/h/h1697.md) to make ['abar](../../strongs/h/h5674.md) [qowl](../../strongs/h/h6963.md) throughout all [Yisra'el](../../strongs/h/h3478.md), from [Bᵊ'ēr šeḇaʿ](../../strongs/h/h884.md) even to [Dān](../../strongs/h/h1835.md), that they should [bow'](../../strongs/h/h935.md) to ['asah](../../strongs/h/h6213.md) the [pecach](../../strongs/h/h6453.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) at [Yĕruwshalaim](../../strongs/h/h3389.md): for they had not ['asah](../../strongs/h/h6213.md) it of a [rōḇ](../../strongs/h/h7230.md) time in such sort as it was [kāṯaḇ](../../strongs/h/h3789.md).

<a name="2chronicles_30_6"></a>2Chronicles 30:6

So the [rûṣ](../../strongs/h/h7323.md) [yālaḵ](../../strongs/h/h3212.md) with the ['agereṯ](../../strongs/h/h107.md) [yad](../../strongs/h/h3027.md) the [melek](../../strongs/h/h4428.md) and his [śar](../../strongs/h/h8269.md) throughout all [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md), and according to the [mitsvah](../../strongs/h/h4687.md) of the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), Ye [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of ['Abraham](../../strongs/h/h85.md), [Yiṣḥāq](../../strongs/h/h3327.md), and [Yisra'el](../../strongs/h/h3478.md), and he will [shuwb](../../strongs/h/h7725.md) to the [šā'ar](../../strongs/h/h7604.md) of you, that are [pᵊlêṭâ](../../strongs/h/h6413.md) out of the [kaph](../../strongs/h/h3709.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="2chronicles_30_7"></a>2Chronicles 30:7

And be not ye like your ['ab](../../strongs/h/h1.md), and like your ['ach](../../strongs/h/h251.md), which [māʿal](../../strongs/h/h4603.md) against [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md), who therefore gave them [nathan](../../strongs/h/h5414.md) to [šammâ](../../strongs/h/h8047.md), as ye [ra'ah](../../strongs/h/h7200.md).

<a name="2chronicles_30_8"></a>2Chronicles 30:8

Now be ye not [qāšâ](../../strongs/h/h7185.md) [ʿōrep̄](../../strongs/h/h6203.md), as your ['ab](../../strongs/h/h1.md) were, but [nathan](../../strongs/h/h5414.md) [yad](../../strongs/h/h3027.md) unto [Yĕhovah](../../strongs/h/h3068.md), and [bow'](../../strongs/h/h935.md) into his [miqdash](../../strongs/h/h4720.md), which he hath [qadash](../../strongs/h/h6942.md) ['owlam](../../strongs/h/h5769.md): and ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), that the [charown](../../strongs/h/h2740.md) of his ['aph](../../strongs/h/h639.md) may [shuwb](../../strongs/h/h7725.md) from you.

<a name="2chronicles_30_9"></a>2Chronicles 30:9

For if ye [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md), your ['ach](../../strongs/h/h251.md) and your [ben](../../strongs/h/h1121.md) shall find [raḥam](../../strongs/h/h7356.md) [paniym](../../strongs/h/h6440.md) them that lead them [šāḇâ](../../strongs/h/h7617.md), so that they shall [shuwb](../../strongs/h/h7725.md) into this ['erets](../../strongs/h/h776.md): for [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) is [ḥanwn](../../strongs/h/h2587.md) and [raḥwm](../../strongs/h/h7349.md), and will not [cuwr](../../strongs/h/h5493.md) his [paniym](../../strongs/h/h6440.md) from you, if ye [shuwb](../../strongs/h/h7725.md) unto him.

<a name="2chronicles_30_10"></a>2Chronicles 30:10

So the [rûṣ](../../strongs/h/h7323.md) ['abar](../../strongs/h/h5674.md) from [ʿîr](../../strongs/h/h5892.md) to [ʿîr](../../strongs/h/h5892.md) through the ['erets](../../strongs/h/h776.md) of ['Ep̄rayim](../../strongs/h/h669.md) and [Mᵊnaššê](../../strongs/h/h4519.md) even unto [Zᵊḇûlûn](../../strongs/h/h2074.md): but they [śāḥaq](../../strongs/h/h7832.md) them, and [lāʿaḡ](../../strongs/h/h3932.md) them.

<a name="2chronicles_30_11"></a>2Chronicles 30:11

Nevertheless ['enowsh](../../strongs/h/h582.md) of ['Āšēr](../../strongs/h/h836.md) and [Mᵊnaššê](../../strongs/h/h4519.md) and of [Zᵊḇûlûn](../../strongs/h/h2074.md) [kānaʿ](../../strongs/h/h3665.md) themselves, and [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_30_12"></a>2Chronicles 30:12

Also in [Yehuwdah](../../strongs/h/h3063.md) the [yad](../../strongs/h/h3027.md) of ['Elohiym](../../strongs/h/h430.md) was to [nathan](../../strongs/h/h5414.md) them one [leb](../../strongs/h/h3820.md) to ['asah](../../strongs/h/h6213.md) the [mitsvah](../../strongs/h/h4687.md) of the [melek](../../strongs/h/h4428.md) and of the [śar](../../strongs/h/h8269.md), by the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_30_13"></a>2Chronicles 30:13

And there ['āsap̄](../../strongs/h/h622.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) to ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) in the second [ḥōḏeš](../../strongs/h/h2320.md), a [me'od](../../strongs/h/h3966.md) [rōḇ](../../strongs/h/h7230.md) [qāhēl](../../strongs/h/h6951.md).

<a name="2chronicles_30_14"></a>2Chronicles 30:14

And they [quwm](../../strongs/h/h6965.md) and [cuwr](../../strongs/h/h5493.md) the [mizbeach](../../strongs/h/h4196.md) that were in [Yĕruwshalaim](../../strongs/h/h3389.md), and all the [qāṭar](../../strongs/h/h6999.md) they [cuwr](../../strongs/h/h5493.md), and [shalak](../../strongs/h/h7993.md) them into the [nachal](../../strongs/h/h5158.md) [Qiḏrôn](../../strongs/h/h6939.md).

<a name="2chronicles_30_15"></a>2Chronicles 30:15

Then they [šāḥaṭ](../../strongs/h/h7819.md) the [pecach](../../strongs/h/h6453.md) on the fourteenth day of the second [ḥōḏeš](../../strongs/h/h2320.md): and the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) were [kālam](../../strongs/h/h3637.md), and [qadash](../../strongs/h/h6942.md) themselves, and [bow'](../../strongs/h/h935.md) the [ʿōlâ](../../strongs/h/h5930.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_30_16"></a>2Chronicles 30:16

And they ['amad](../../strongs/h/h5975.md) in their [ʿōmeḏ](../../strongs/h/h5977.md) after their [mishpat](../../strongs/h/h4941.md), according to the [towrah](../../strongs/h/h8451.md) of [Mōshe](../../strongs/h/h4872.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md): the [kōhēn](../../strongs/h/h3548.md) [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md), which they received of the [yad](../../strongs/h/h3027.md) of the [Lᵊvî](../../strongs/h/h3881.md).

<a name="2chronicles_30_17"></a>2Chronicles 30:17

For there were [rab](../../strongs/h/h7227.md) in the [qāhēl](../../strongs/h/h6951.md) that were not [qadash](../../strongs/h/h6942.md): therefore the [Lᵊvî](../../strongs/h/h3881.md) had the charge of the [šᵊḥîṭâ](../../strongs/h/h7821.md) of the [pecach](../../strongs/h/h6453.md) for every one that was not [tahowr](../../strongs/h/h2889.md), to [qadash](../../strongs/h/h6942.md) them unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_30_18"></a>2Chronicles 30:18

For a [marbîṯ](../../strongs/h/h4768.md) of the ['am](../../strongs/h/h5971.md), even [rab](../../strongs/h/h7227.md) of ['Ep̄rayim](../../strongs/h/h669.md), and [Mᵊnaššê](../../strongs/h/h4519.md), [Yiśśāśḵār](../../strongs/h/h3485.md), and [Zᵊḇûlûn](../../strongs/h/h2074.md), had not [ṭāhēr](../../strongs/h/h2891.md) themselves, yet did they ['akal](../../strongs/h/h398.md) the [pecach](../../strongs/h/h6453.md) otherwise than it was [kāṯaḇ](../../strongs/h/h3789.md). But [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [palal](../../strongs/h/h6419.md) for them, ['āmar](../../strongs/h/h559.md), The [towb](../../strongs/h/h2896.md) [Yĕhovah](../../strongs/h/h3068.md) [kāp̄ar](../../strongs/h/h3722.md) every one

<a name="2chronicles_30_19"></a>2Chronicles 30:19

that [kuwn](../../strongs/h/h3559.md) his [lebab](../../strongs/h/h3824.md) to [darash](../../strongs/h/h1875.md) ['Elohiym](../../strongs/h/h430.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md), though not [ṭāhŏrâ](../../strongs/h/h2893.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="2chronicles_30_20"></a>2Chronicles 30:20

And [Yĕhovah](../../strongs/h/h3068.md) [shama'](../../strongs/h/h8085.md) to [Yᵊḥizqîyâ](../../strongs/h/h3169.md), and [rapha'](../../strongs/h/h7495.md) the ['am](../../strongs/h/h5971.md).

<a name="2chronicles_30_21"></a>2Chronicles 30:21

And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) that were [māṣā'](../../strongs/h/h4672.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) ['asah](../../strongs/h/h6213.md) the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) seven [yowm](../../strongs/h/h3117.md) with [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md): and the [Lᵊvî](../../strongs/h/h3881.md) and the [kōhēn](../../strongs/h/h3548.md) [halal](../../strongs/h/h1984.md) [Yĕhovah](../../strongs/h/h3068.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md), singing with ['oz](../../strongs/h/h5797.md) [kĕliy](../../strongs/h/h3627.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_30_22"></a>2Chronicles 30:22

And [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [dabar](../../strongs/h/h1696.md) [leb](../../strongs/h/h3820.md) unto all the [Lᵊvî](../../strongs/h/h3881.md) that [sakal](../../strongs/h/h7919.md) the [towb](../../strongs/h/h2896.md) [śēḵel](../../strongs/h/h7922.md) of [Yĕhovah](../../strongs/h/h3068.md): and they did ['akal](../../strongs/h/h398.md) throughout the [môʿēḏ](../../strongs/h/h4150.md) seven [yowm](../../strongs/h/h3117.md), [zabach](../../strongs/h/h2076.md) [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md), and making [yadah](../../strongs/h/h3034.md) to [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_30_23"></a>2Chronicles 30:23

And the whole [qāhēl](../../strongs/h/h6951.md) took [ya'ats](../../strongs/h/h3289.md) to ['asah](../../strongs/h/h6213.md) ['aḥēr](../../strongs/h/h312.md) seven [yowm](../../strongs/h/h3117.md): and they ['asah](../../strongs/h/h6213.md) other seven [yowm](../../strongs/h/h3117.md) with [simchah](../../strongs/h/h8057.md).

<a name="2chronicles_30_24"></a>2Chronicles 30:24

For [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) did [ruwm](../../strongs/h/h7311.md) to the [qāhēl](../../strongs/h/h6951.md) a thousand [par](../../strongs/h/h6499.md) and seven thousand [tso'n](../../strongs/h/h6629.md); and the [śar](../../strongs/h/h8269.md) [ruwm](../../strongs/h/h7311.md) to the [qāhēl](../../strongs/h/h6951.md) a thousand [par](../../strongs/h/h6499.md) and ten thousand [tso'n](../../strongs/h/h6629.md): and a great [rōḇ](../../strongs/h/h7230.md) of [kōhēn](../../strongs/h/h3548.md) [qadash](../../strongs/h/h6942.md) themselves.

<a name="2chronicles_30_25"></a>2Chronicles 30:25

And all the [qāhēl](../../strongs/h/h6951.md) of [Yehuwdah](../../strongs/h/h3063.md), with the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md), and all the [qāhēl](../../strongs/h/h6951.md) that [bow'](../../strongs/h/h935.md) of [Yisra'el](../../strongs/h/h3478.md), and the [ger](../../strongs/h/h1616.md) that [bow'](../../strongs/h/h935.md) of the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), and that [yashab](../../strongs/h/h3427.md) in [Yehuwdah](../../strongs/h/h3063.md), [samach](../../strongs/h/h8055.md).

<a name="2chronicles_30_26"></a>2Chronicles 30:26

So there was [gadowl](../../strongs/h/h1419.md) [simchah](../../strongs/h/h8057.md) in [Yĕruwshalaim](../../strongs/h/h3389.md): for since the [yowm](../../strongs/h/h3117.md) of [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md) there was not the like in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_30_27"></a>2Chronicles 30:27

Then the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) [quwm](../../strongs/h/h6965.md) and [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md): and their [qowl](../../strongs/h/h6963.md) was [shama'](../../strongs/h/h8085.md), and their [tĕphillah](../../strongs/h/h8605.md) [bow'](../../strongs/h/h935.md) up to his [qodesh](../../strongs/h/h6944.md) [māʿôn](../../strongs/h/h4583.md), even unto [shamayim](../../strongs/h/h8064.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 29](2chronicles_29.md) - [2Chronicles 31](2chronicles_31.md)