# [2Chronicles 11](https://www.blueletterbible.org/kjv/2chronicles/11)

<a name="2chronicles_11_1"></a>2Chronicles 11:1

And when [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) was [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), he [qāhal](../../strongs/h/h6950.md) of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md) an hundred and fourscore thousand [bāḥar](../../strongs/h/h977.md) men, which were ['asah](../../strongs/h/h6213.md) [milḥāmâ](../../strongs/h/h4421.md), to [lāḥam](../../strongs/h/h3898.md) against [Yisra'el](../../strongs/h/h3478.md), that he might [shuwb](../../strongs/h/h7725.md) the [mamlāḵâ](../../strongs/h/h4467.md) [shuwb](../../strongs/h/h7725.md) to [RᵊḥaḇʿĀm](../../strongs/h/h7346.md).

<a name="2chronicles_11_2"></a>2Chronicles 11:2

But the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) to [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_11_3"></a>2Chronicles 11:3

['āmar](../../strongs/h/h559.md) unto [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md), [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and to all [Yisra'el](../../strongs/h/h3478.md) in [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_11_4"></a>2Chronicles 11:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Ye shall not [ʿālâ](../../strongs/h/h5927.md), nor [lāḥam](../../strongs/h/h3898.md) against your ['ach](../../strongs/h/h251.md): [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) to his [bayith](../../strongs/h/h1004.md): for this [dabar](../../strongs/h/h1697.md) is [hayah](../../strongs/h/h1961.md) of me. And they [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and [shuwb](../../strongs/h/h7725.md) from [yālaḵ](../../strongs/h/h3212.md) against [YārāḇʿĀm](../../strongs/h/h3379.md).

<a name="2chronicles_11_5"></a>2Chronicles 11:5

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and [bānâ](../../strongs/h/h1129.md) [ʿîr](../../strongs/h/h5892.md) for [māṣôr](../../strongs/h/h4692.md) in [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_11_6"></a>2Chronicles 11:6

He [bānâ](../../strongs/h/h1129.md) even [Bêṯ leḥem](../../strongs/h/h1035.md), and [ʿÊṭām](../../strongs/h/h5862.md), and [Tᵊqôaʿ](../../strongs/h/h8620.md),

<a name="2chronicles_11_7"></a>2Chronicles 11:7

And [Bêṯ-Ṣûr](../../strongs/h/h1049.md), and [Śôḵô](../../strongs/h/h7755.md), and [ʿĂḏullām](../../strongs/h/h5725.md),

<a name="2chronicles_11_8"></a>2Chronicles 11:8

And [Gaṯ](../../strongs/h/h1661.md), and [Mārē'Šâ](../../strongs/h/h4762.md), and [Zîp̄](../../strongs/h/h2128.md),

<a name="2chronicles_11_9"></a>2Chronicles 11:9

And ['Ăḏôrayim](../../strongs/h/h115.md), and [Lāḵîš](../../strongs/h/h3923.md), and [ʿĂzēqâ](../../strongs/h/h5825.md),

<a name="2chronicles_11_10"></a>2Chronicles 11:10

And [ṢārʿÂ](../../strongs/h/h6881.md), and ['Ayyālôn](../../strongs/h/h357.md), and [Ḥeḇrôn](../../strongs/h/h2275.md), which are in [Yehuwdah](../../strongs/h/h3063.md) and in [Binyāmîn](../../strongs/h/h1144.md) [mᵊṣûrâ](../../strongs/h/h4694.md) [ʿîr](../../strongs/h/h5892.md).

<a name="2chronicles_11_11"></a>2Chronicles 11:11

And he [ḥāzaq](../../strongs/h/h2388.md) the [mᵊṣûrâ](../../strongs/h/h4694.md), and [nathan](../../strongs/h/h5414.md) [nāḡîḏ](../../strongs/h/h5057.md) in them, and ['ôṣār](../../strongs/h/h214.md) of [ma'akal](../../strongs/h/h3978.md), and of [šemen](../../strongs/h/h8081.md) and [yayin](../../strongs/h/h3196.md).

<a name="2chronicles_11_12"></a>2Chronicles 11:12

And in every several [ʿîr](../../strongs/h/h5892.md) he put [tsinnah](../../strongs/h/h6793.md) and [rōmaḥ](../../strongs/h/h7420.md), and made them [rabah](../../strongs/h/h7235.md) [me'od](../../strongs/h/h3966.md) [ḥāzaq](../../strongs/h/h2388.md), having [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md) on his side.

<a name="2chronicles_11_13"></a>2Chronicles 11:13

And the [kōhēn](../../strongs/h/h3548.md) and the [Lᵊvî](../../strongs/h/h3881.md) that were in all [Yisra'el](../../strongs/h/h3478.md) [yatsab](../../strongs/h/h3320.md) to him out of all their [gᵊḇûl](../../strongs/h/h1366.md).

<a name="2chronicles_11_14"></a>2Chronicles 11:14

For the [Lᵊvî](../../strongs/h/h3881.md) ['azab](../../strongs/h/h5800.md) their [miḡrāš](../../strongs/h/h4054.md) and their ['achuzzah](../../strongs/h/h272.md), and [yālaḵ](../../strongs/h/h3212.md) to [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md): for [YārāḇʿĀm](../../strongs/h/h3379.md) and his [ben](../../strongs/h/h1121.md) had cast them [zānaḥ](../../strongs/h/h2186.md) from [kāhan](../../strongs/h/h3547.md) unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="2chronicles_11_15"></a>2Chronicles 11:15

And he ['amad](../../strongs/h/h5975.md) him [kōhēn](../../strongs/h/h3548.md) for the [bāmâ](../../strongs/h/h1116.md), and for the [śāʿîr](../../strongs/h/h8163.md), and for the [ʿēḡel](../../strongs/h/h5695.md) which he had ['asah](../../strongs/h/h6213.md).

<a name="2chronicles_11_16"></a>2Chronicles 11:16

And ['aḥar](../../strongs/h/h310.md) them out of all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md) such as [nathan](../../strongs/h/h5414.md) their [lebab](../../strongs/h/h3824.md) to [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), to [zabach](../../strongs/h/h2076.md) unto [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_11_17"></a>2Chronicles 11:17

So they [ḥāzaq](../../strongs/h/h2388.md) the [malkuwth](../../strongs/h/h4438.md) of [Yehuwdah](../../strongs/h/h3063.md), and made [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) the [ben](../../strongs/h/h1121.md) of [Šᵊlōmô](../../strongs/h/h8010.md) ['amats](../../strongs/h/h553.md), three [šānâ](../../strongs/h/h8141.md): for three [šānâ](../../strongs/h/h8141.md) they [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) of [Dāviḏ](../../strongs/h/h1732.md) and [Šᵊlōmô](../../strongs/h/h8010.md).

<a name="2chronicles_11_18"></a>2Chronicles 11:18

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) [laqach](../../strongs/h/h3947.md) him [Maḥălaṯ](../../strongs/h/h4258.md) the [bath](../../strongs/h/h1323.md) [ben](../../strongs/h/h1121.md) of [Yᵊrêmôṯ](../../strongs/h/h3406.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) to ['ishshah](../../strongs/h/h802.md), and ['Ăḇîhayil](../../strongs/h/h32.md) the [bath](../../strongs/h/h1323.md) of ['Ĕlî'āḇ](../../strongs/h/h446.md) the [ben](../../strongs/h/h1121.md) of [Yišay](../../strongs/h/h3448.md);

<a name="2chronicles_11_19"></a>2Chronicles 11:19

Which [yalad](../../strongs/h/h3205.md) him [ben](../../strongs/h/h1121.md); [Yᵊʿûš](../../strongs/h/h3266.md), and [Šᵊmaryâ](../../strongs/h/h8114.md), and [Zaham](../../strongs/h/h2093.md).

<a name="2chronicles_11_20"></a>2Chronicles 11:20

And ['aḥar](../../strongs/h/h310.md) her he [laqach](../../strongs/h/h3947.md) [Maʿăḵâ](../../strongs/h/h4601.md) the [bath](../../strongs/h/h1323.md) of ['Ăbyšālôm](../../strongs/h/h53.md); which [yalad](../../strongs/h/h3205.md) him ['Ăḇîâ](../../strongs/h/h29.md), and [ʿAtay](../../strongs/h/h6262.md), and [Zîzā'](../../strongs/h/h2124.md), and [Šᵊlōmîṯ](../../strongs/h/h8019.md).

<a name="2chronicles_11_21"></a>2Chronicles 11:21

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) ['ahab](../../strongs/h/h157.md) [Maʿăḵâ](../../strongs/h/h4601.md) the [bath](../../strongs/h/h1323.md) of ['Ăbyšālôm](../../strongs/h/h53.md) above all his ['ishshah](../../strongs/h/h802.md) and his [pîleḡeš](../../strongs/h/h6370.md): (for he [nasa'](../../strongs/h/h5375.md) eighteen ['ishshah](../../strongs/h/h802.md), and threescore [pîleḡeš](../../strongs/h/h6370.md); and [yalad](../../strongs/h/h3205.md) twenty and eight [ben](../../strongs/h/h1121.md), and threescore [bath](../../strongs/h/h1323.md).)

<a name="2chronicles_11_22"></a>2Chronicles 11:22

And [RᵊḥaḇʿĀm](../../strongs/h/h7346.md) ['amad](../../strongs/h/h5975.md) ['Ăḇîâ](../../strongs/h/h29.md) the [ben](../../strongs/h/h1121.md) of [Maʿăḵâ](../../strongs/h/h4601.md) the [ro'sh](../../strongs/h/h7218.md), to be [nāḡîḏ](../../strongs/h/h5057.md) among his ['ach](../../strongs/h/h251.md): for he thought to make him [mālaḵ](../../strongs/h/h4427.md).

<a name="2chronicles_11_23"></a>2Chronicles 11:23

And he dealt [bîn](../../strongs/h/h995.md), and [pāraṣ](../../strongs/h/h6555.md) of all his [ben](../../strongs/h/h1121.md) throughout all the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md), unto every [mᵊṣûrâ](../../strongs/h/h4694.md) [ʿîr](../../strongs/h/h5892.md): and he [nathan](../../strongs/h/h5414.md) them [māzôn](../../strongs/h/h4202.md) in [rōḇ](../../strongs/h/h7230.md). And he [sha'al](../../strongs/h/h7592.md) [hāmôn](../../strongs/h/h1995.md) ['ishshah](../../strongs/h/h802.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 10](2chronicles_10.md) - [2Chronicles 12](2chronicles_12.md)