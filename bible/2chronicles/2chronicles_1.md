# [2Chronicles 1](https://www.blueletterbible.org/kjv/2chronicles/1)

<a name="2chronicles_1_1"></a>2Chronicles 1:1

And [Šᵊlōmô](../../strongs/h/h8010.md) the [ben](../../strongs/h/h1121.md) of [Dāviḏ](../../strongs/h/h1732.md) was [ḥāzaq](../../strongs/h/h2388.md) in his [malkuwth](../../strongs/h/h4438.md), and [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) was with him, and [gāḏal](../../strongs/h/h1431.md) him [maʿal](../../strongs/h/h4605.md).

<a name="2chronicles_1_2"></a>2Chronicles 1:2

Then [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md) unto all [Yisra'el](../../strongs/h/h3478.md), to the [śar](../../strongs/h/h8269.md) of thousands and of hundreds, and to the [shaphat](../../strongs/h/h8199.md), and to every [nāśî'](../../strongs/h/h5387.md) in all [Yisra'el](../../strongs/h/h3478.md), the [ro'sh](../../strongs/h/h7218.md) of the ['ab](../../strongs/h/h1.md).

<a name="2chronicles_1_3"></a>2Chronicles 1:3

So [Šᵊlōmô](../../strongs/h/h8010.md), and all the [qāhēl](../../strongs/h/h6951.md) with him, [yālaḵ](../../strongs/h/h3212.md) to the [bāmâ](../../strongs/h/h1116.md) that was at [Giḇʿôn](../../strongs/h/h1391.md); for there was the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) of ['Elohiym](../../strongs/h/h430.md), which [Mōshe](../../strongs/h/h4872.md) the ['ebed](../../strongs/h/h5650.md) of [Yĕhovah](../../strongs/h/h3068.md) had ['asah](../../strongs/h/h6213.md) in the [midbar](../../strongs/h/h4057.md).

<a name="2chronicles_1_4"></a>2Chronicles 1:4

['ăḇāl](../../strongs/h/h61.md) the ['ārôn](../../strongs/h/h727.md) of ['Elohiym](../../strongs/h/h430.md) had [Dāviḏ](../../strongs/h/h1732.md) [ʿālâ](../../strongs/h/h5927.md) from [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md) to the place which [Dāviḏ](../../strongs/h/h1732.md) had [kuwn](../../strongs/h/h3559.md) for it: for he had [natah](../../strongs/h/h5186.md) a ['ohel](../../strongs/h/h168.md) for it at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_1_5"></a>2Chronicles 1:5

Moreover the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md), that [Bᵊṣal'ēl](../../strongs/h/h1212.md) the [ben](../../strongs/h/h1121.md) of ['ûrî](../../strongs/h/h221.md), the [ben](../../strongs/h/h1121.md) of [Ḥûr](../../strongs/h/h2354.md), had ['asah](../../strongs/h/h6213.md), he [śûm](../../strongs/h/h7760.md) [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md): and [Šᵊlōmô](../../strongs/h/h8010.md) and the [qāhēl](../../strongs/h/h6951.md) [darash](../../strongs/h/h1875.md) unto it.

<a name="2chronicles_1_6"></a>2Chronicles 1:6

And [Šᵊlōmô](../../strongs/h/h8010.md) [ʿālâ](../../strongs/h/h5927.md) thither to the [nᵊḥšeṯ](../../strongs/h/h5178.md) [mizbeach](../../strongs/h/h4196.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), which was at the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [ʿālâ](../../strongs/h/h5927.md) a thousand [ʿōlâ](../../strongs/h/h5930.md) upon it.

<a name="2chronicles_1_7"></a>2Chronicles 1:7

In that [layil](../../strongs/h/h3915.md) did ['Elohiym](../../strongs/h/h430.md) [ra'ah](../../strongs/h/h7200.md) unto [Šᵊlōmô](../../strongs/h/h8010.md), and ['āmar](../../strongs/h/h559.md) unto him, [sha'al](../../strongs/h/h7592.md) what I shall [nathan](../../strongs/h/h5414.md) thee.

<a name="2chronicles_1_8"></a>2Chronicles 1:8

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āmar](../../strongs/h/h559.md) unto ['Elohiym](../../strongs/h/h430.md), Thou hast ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [checed](../../strongs/h/h2617.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md), and hast made me to [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2chronicles_1_9"></a>2Chronicles 1:9

Now, [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md), let thy [dabar](../../strongs/h/h1697.md) unto [Dāviḏ](../../strongs/h/h1732.md) my ['ab](../../strongs/h/h1.md) be ['aman](../../strongs/h/h539.md): for thou hast made me [mālaḵ](../../strongs/h/h4427.md) over an ['am](../../strongs/h/h5971.md) like the ['aphar](../../strongs/h/h6083.md) of the ['erets](../../strongs/h/h776.md) in [rab](../../strongs/h/h7227.md).

<a name="2chronicles_1_10"></a>2Chronicles 1:10

[nathan](../../strongs/h/h5414.md) me now [ḥāḵmâ](../../strongs/h/h2451.md) and [madāʿ](../../strongs/h/h4093.md), that I may [yāṣā'](../../strongs/h/h3318.md) and [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) this ['am](../../strongs/h/h5971.md): for who can [shaphat](../../strongs/h/h8199.md) this thy ['am](../../strongs/h/h5971.md), that is so [gadowl](../../strongs/h/h1419.md)?

<a name="2chronicles_1_11"></a>2Chronicles 1:11

And ['Elohiym](../../strongs/h/h430.md) ['āmar](../../strongs/h/h559.md) to [Šᵊlōmô](../../strongs/h/h8010.md), Because this was in thine [lebab](../../strongs/h/h3824.md), and thou hast not [sha'al](../../strongs/h/h7592.md) [ʿōšer](../../strongs/h/h6239.md), [neḵes](../../strongs/h/h5233.md), or [kabowd](../../strongs/h/h3519.md), nor the [nephesh](../../strongs/h/h5315.md) of thine [sane'](../../strongs/h/h8130.md), neither yet hast [sha'al](../../strongs/h/h7592.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md); but hast [sha'al](../../strongs/h/h7592.md) [ḥāḵmâ](../../strongs/h/h2451.md) and [madāʿ](../../strongs/h/h4093.md) for thyself, that thou mayest [shaphat](../../strongs/h/h8199.md) my ['am](../../strongs/h/h5971.md), over whom I have made thee [mālaḵ](../../strongs/h/h4427.md):

<a name="2chronicles_1_12"></a>2Chronicles 1:12

[ḥāḵmâ](../../strongs/h/h2451.md) and [madāʿ](../../strongs/h/h4093.md) is [nathan](../../strongs/h/h5414.md) unto thee; and I will [nathan](../../strongs/h/h5414.md) thee [ʿōšer](../../strongs/h/h6239.md), and [neḵes](../../strongs/h/h5233.md), and [kabowd](../../strongs/h/h3519.md), such as none of the [melek](../../strongs/h/h4428.md) have had that have been [paniym](../../strongs/h/h6440.md) thee, neither shall there any ['aḥar](../../strongs/h/h310.md) thee have the like.

<a name="2chronicles_1_13"></a>2Chronicles 1:13

Then [Šᵊlōmô](../../strongs/h/h8010.md) [bow'](../../strongs/h/h935.md) to the [bāmâ](../../strongs/h/h1116.md) that was at [Giḇʿôn](../../strongs/h/h1391.md) to [Yĕruwshalaim](../../strongs/h/h3389.md), from [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [mālaḵ](../../strongs/h/h4427.md) over [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_1_14"></a>2Chronicles 1:14

And [Šᵊlōmô](../../strongs/h/h8010.md) ['āsap̄](../../strongs/h/h622.md) [reḵeḇ](../../strongs/h/h7393.md) and [pārāš](../../strongs/h/h6571.md): and he had a thousand and four hundred [reḵeḇ](../../strongs/h/h7393.md), and twelve thousand [pārāš](../../strongs/h/h6571.md), which he [yānaḥ](../../strongs/h/h3240.md) in the [reḵeḇ](../../strongs/h/h7393.md) [ʿîr](../../strongs/h/h5892.md), and with the [melek](../../strongs/h/h4428.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_1_15"></a>2Chronicles 1:15

And the [melek](../../strongs/h/h4428.md) [nathan](../../strongs/h/h5414.md) [keceph](../../strongs/h/h3701.md) and [zāhāḇ](../../strongs/h/h2091.md) at [Yĕruwshalaim](../../strongs/h/h3389.md) as plenteous as ['eben](../../strongs/h/h68.md), and ['erez](../../strongs/h/h730.md) [nathan](../../strongs/h/h5414.md) he as the [šiqmâ](../../strongs/h/h8256.md) that are in the [šᵊp̄ēlâ](../../strongs/h/h8219.md) for [rōḇ](../../strongs/h/h7230.md).

<a name="2chronicles_1_16"></a>2Chronicles 1:16

And [Šᵊlōmô](../../strongs/h/h8010.md) had [sûs](../../strongs/h/h5483.md) [môṣā'](../../strongs/h/h4161.md) out of [Mitsrayim](../../strongs/h/h4714.md), and [miqvê](../../strongs/h/h4723.md): the [melek](../../strongs/h/h4428.md) [sāḥar](../../strongs/h/h5503.md) [laqach](../../strongs/h/h3947.md) the [miqvê](../../strongs/h/h4723.md) at a [mᵊḥîr](../../strongs/h/h4242.md).

<a name="2chronicles_1_17"></a>2Chronicles 1:17

And they [ʿālâ](../../strongs/h/h5927.md), and [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md) a [merkāḇâ](../../strongs/h/h4818.md) for six hundred shekels of [keceph](../../strongs/h/h3701.md), and a [sûs](../../strongs/h/h5483.md) for an hundred and fifty: and so [yāṣā'](../../strongs/h/h3318.md) horses for all the [melek](../../strongs/h/h4428.md) of the [Ḥitî](../../strongs/h/h2850.md), and for the [melek](../../strongs/h/h4428.md) of ['Ărām](../../strongs/h/h758.md), by their [yad](../../strongs/h/h3027.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 2](2chronicles_2.md)