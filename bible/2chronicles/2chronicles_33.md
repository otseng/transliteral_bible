# [2Chronicles 33](https://www.blueletterbible.org/kjv/2chronicles/33)

<a name="2chronicles_33_1"></a>2Chronicles 33:1

[Mᵊnaššê](../../strongs/h/h4519.md) was twelve [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) fifty and five [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md):

<a name="2chronicles_33_2"></a>2Chronicles 33:2

But ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), like unto the [tôʿēḇâ](../../strongs/h/h8441.md) of the [gowy](../../strongs/h/h1471.md), whom [Yĕhovah](../../strongs/h/h3068.md) had [yarash](../../strongs/h/h3423.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_33_3"></a>2Chronicles 33:3

For he [bānâ](../../strongs/h/h1129.md) [shuwb](../../strongs/h/h7725.md) the [bāmâ](../../strongs/h/h1116.md) which [Yᵊḥizqîyâ](../../strongs/h/h3169.md) his ['ab](../../strongs/h/h1.md) had [nāṯaṣ](../../strongs/h/h5422.md), and he  [quwm](../../strongs/h/h6965.md) [mizbeach](../../strongs/h/h4196.md) for [BaʿAl](../../strongs/h/h1168.md), and ['asah](../../strongs/h/h6213.md) ['ăšērâ](../../strongs/h/h842.md), and [shachah](../../strongs/h/h7812.md) all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), and ['abad](../../strongs/h/h5647.md) them.

<a name="2chronicles_33_4"></a>2Chronicles 33:4

Also he [bānâ](../../strongs/h/h1129.md) [mizbeach](../../strongs/h/h4196.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), whereof [Yĕhovah](../../strongs/h/h3068.md) had ['āmar](../../strongs/h/h559.md), In [Yĕruwshalaim](../../strongs/h/h3389.md) shall my [shem](../../strongs/h/h8034.md) be ['owlam](../../strongs/h/h5769.md).

<a name="2chronicles_33_5"></a>2Chronicles 33:5

And he [bānâ](../../strongs/h/h1129.md) [mizbeach](../../strongs/h/h4196.md) for all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) in the two [ḥāṣēr](../../strongs/h/h2691.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_33_6"></a>2Chronicles 33:6

And he ['abar](../../strongs/h/h5674.md) his [ben](../../strongs/h/h1121.md) to ['abar](../../strongs/h/h5674.md) the ['esh](../../strongs/h/h784.md) in the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md): also he observed [ʿānan](../../strongs/h/h6049.md), and used [nāḥaš](../../strongs/h/h5172.md), and used [kāšap̄](../../strongs/h/h3784.md), and ['asah](../../strongs/h/h6213.md) with a ['ôḇ](../../strongs/h/h178.md), and with [yiḏʿōnî](../../strongs/h/h3049.md): he ['asah](../../strongs/h/h6213.md) [rabah](../../strongs/h/h7235.md) [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), to provoke him to [kāʿas](../../strongs/h/h3707.md).

<a name="2chronicles_33_7"></a>2Chronicles 33:7

And he [śûm](../../strongs/h/h7760.md) a [pecel](../../strongs/h/h6459.md), the [semel](../../strongs/h/h5566.md) which he had ['asah](../../strongs/h/h6213.md), in the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), of which ['Elohiym](../../strongs/h/h430.md) had ['āmar](../../strongs/h/h559.md) to [Dāviḏ](../../strongs/h/h1732.md) and to [Šᵊlōmô](../../strongs/h/h8010.md) his [ben](../../strongs/h/h1121.md), In this [bayith](../../strongs/h/h1004.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md), which I have [bāḥar](../../strongs/h/h977.md) before all the [shebet](../../strongs/h/h7626.md) of [Yisra'el](../../strongs/h/h3478.md), will I [śûm](../../strongs/h/h7760.md) my [shem](../../strongs/h/h8034.md) for [ʿêlôm](../../strongs/h/h5865.md):

<a name="2chronicles_33_8"></a>2Chronicles 33:8

Neither will I any more [cuwr](../../strongs/h/h5493.md) the [regel](../../strongs/h/h7272.md) of [Yisra'el](../../strongs/h/h3478.md) from out of the ['ăḏāmâ](../../strongs/h/h127.md) which I have ['amad](../../strongs/h/h5975.md) for your ['ab](../../strongs/h/h1.md); so that they will take [shamar](../../strongs/h/h8104.md) to ['asah](../../strongs/h/h6213.md) all that I have [tsavah](../../strongs/h/h6680.md) them, according to the whole [towrah](../../strongs/h/h8451.md) and the [choq](../../strongs/h/h2706.md) and the [mishpat](../../strongs/h/h4941.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="2chronicles_33_9"></a>2Chronicles 33:9

So [Mᵊnaššê](../../strongs/h/h4519.md) [tāʿâ](../../strongs/h/h8582.md) [Yehuwdah](../../strongs/h/h3063.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) to [tāʿâ](../../strongs/h/h8582.md), and to ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) than the [gowy](../../strongs/h/h1471.md), whom [Yĕhovah](../../strongs/h/h3068.md) had [šāmaḏ](../../strongs/h/h8045.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_33_10"></a>2Chronicles 33:10

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) to [Mᵊnaššê](../../strongs/h/h4519.md), and to his ['am](../../strongs/h/h5971.md): but they would not [qashab](../../strongs/h/h7181.md).

<a name="2chronicles_33_11"></a>2Chronicles 33:11

Wherefore [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md) upon them the [śar](../../strongs/h/h8269.md) of the [tsaba'](../../strongs/h/h6635.md) of the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md), which [lāḵaḏ](../../strongs/h/h3920.md) [Mᵊnaššê](../../strongs/h/h4519.md) among the [ḥôaḥ](../../strongs/h/h2336.md), and ['āsar](../../strongs/h/h631.md) him with [nᵊḥšeṯ](../../strongs/h/h5178.md), and [yālaḵ](../../strongs/h/h3212.md) him to [Bāḇel](../../strongs/h/h894.md).

<a name="2chronicles_33_12"></a>2Chronicles 33:12

And when he was in [tsarar](../../strongs/h/h6887.md), he [ḥālâ](../../strongs/h/h2470.md) [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md), and [kānaʿ](../../strongs/h/h3665.md) himself [me'od](../../strongs/h/h3966.md) [paniym](../../strongs/h/h6440.md) the ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md),

<a name="2chronicles_33_13"></a>2Chronicles 33:13

And [palal](../../strongs/h/h6419.md) unto him: and he was [ʿāṯar](../../strongs/h/h6279.md) of him, and [shama'](../../strongs/h/h8085.md) his [tĕchinnah](../../strongs/h/h8467.md), and [shuwb](../../strongs/h/h7725.md) him to [Yĕruwshalaim](../../strongs/h/h3389.md) into his [malkuwth](../../strongs/h/h4438.md). Then [Mᵊnaššê](../../strongs/h/h4519.md) [yada'](../../strongs/h/h3045.md) that [Yĕhovah](../../strongs/h/h3068.md) he was ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_33_14"></a>2Chronicles 33:14

Now ['aḥar](../../strongs/h/h310.md) he [bānâ](../../strongs/h/h1129.md) a [ḥômâ](../../strongs/h/h2346.md) [ḥîṣôn](../../strongs/h/h2435.md) the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), on the [ma'arab](../../strongs/h/h4628.md) of [gîḥôn](../../strongs/h/h1521.md), in the [nachal](../../strongs/h/h5158.md), even to the [bow'](../../strongs/h/h935.md) at the [dag](../../strongs/h/h1709.md) [sha'ar](../../strongs/h/h8179.md), and [cabab](../../strongs/h/h5437.md) about [ʿŌp̄El](../../strongs/h/h6077.md), and [gāḇah](../../strongs/h/h1361.md) it [me'od](../../strongs/h/h3966.md), and [śûm](../../strongs/h/h7760.md) [śar](../../strongs/h/h8269.md) of [ḥayil](../../strongs/h/h2428.md) in all the [bāṣar](../../strongs/h/h1219.md) [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="2chronicles_33_15"></a>2Chronicles 33:15

And he [cuwr](../../strongs/h/h5493.md) the [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md), and the [semel](../../strongs/h/h5566.md) out of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the [mizbeach](../../strongs/h/h4196.md) that he had [bānâ](../../strongs/h/h1129.md) in the [har](../../strongs/h/h2022.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in [Yĕruwshalaim](../../strongs/h/h3389.md), and [shalak](../../strongs/h/h7993.md) them [ḥûṣ](../../strongs/h/h2351.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="2chronicles_33_16"></a>2Chronicles 33:16

And he [bānâ](../../strongs/h/h1129.md) the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md), and [zabach](../../strongs/h/h2076.md) thereon [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md) and thank [tôḏâ](../../strongs/h/h8426.md), and ['āmar](../../strongs/h/h559.md) [Yehuwdah](../../strongs/h/h3063.md) to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_33_17"></a>2Chronicles 33:17

['ăḇāl](../../strongs/h/h61.md) the ['am](../../strongs/h/h5971.md) did [zabach](../../strongs/h/h2076.md) still in the [bāmâ](../../strongs/h/h1116.md), yet unto [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) only.

<a name="2chronicles_33_18"></a>2Chronicles 33:18

Now the [yeṯer](../../strongs/h/h3499.md) of the [dabar](../../strongs/h/h1697.md) of [Mᵊnaššê](../../strongs/h/h4519.md), and his [tĕphillah](../../strongs/h/h8605.md) unto his ['Elohiym](../../strongs/h/h430.md), and the [dabar](../../strongs/h/h1697.md) of the [ḥōzê](../../strongs/h/h2374.md) that [dabar](../../strongs/h/h1696.md) to him in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), behold, they are in the [dabar](../../strongs/h/h1697.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_33_19"></a>2Chronicles 33:19

His [tĕphillah](../../strongs/h/h8605.md) also, and [ʿāṯar](../../strongs/h/h6279.md) of him, and all his [chatta'ath](../../strongs/h/h2403.md), and his [maʿal](../../strongs/h/h4604.md), and the [maqowm](../../strongs/h/h4725.md) wherein he [bānâ](../../strongs/h/h1129.md) [bāmâ](../../strongs/h/h1116.md), and set ['amad](../../strongs/h/h5975.md) ['ăšērâ](../../strongs/h/h842.md) and [pāsîl](../../strongs/h/h6456.md), [paniym](../../strongs/h/h6440.md) he was [kānaʿ](../../strongs/h/h3665.md): behold, they are [kāṯaḇ](../../strongs/h/h3789.md) among the [dabar](../../strongs/h/h1697.md) of the [ḥōzê](../../strongs/h/h2374.md) [ḥôzay](../../strongs/h/h2335.md) .

<a name="2chronicles_33_20"></a>2Chronicles 33:20

So [Mᵊnaššê](../../strongs/h/h4519.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and they [qāḇar](../../strongs/h/h6912.md) him in his own [bayith](../../strongs/h/h1004.md): and ['Āmôn](../../strongs/h/h526.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2chronicles_33_21"></a>2Chronicles 33:21

['Āmôn](../../strongs/h/h526.md) was two and twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and [mālaḵ](../../strongs/h/h4427.md) two [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_33_22"></a>2Chronicles 33:22

But he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), as ['asah](../../strongs/h/h6213.md) [Mᵊnaššê](../../strongs/h/h4519.md) his ['ab](../../strongs/h/h1.md): for ['Āmôn](../../strongs/h/h526.md) [zabach](../../strongs/h/h2076.md) unto all the [pāsîl](../../strongs/h/h6456.md) which [Mᵊnaššê](../../strongs/h/h4519.md) his ['ab](../../strongs/h/h1.md) had ['asah](../../strongs/h/h6213.md), and ['abad](../../strongs/h/h5647.md) them;

<a name="2chronicles_33_23"></a>2Chronicles 33:23

And [kānaʿ](../../strongs/h/h3665.md) not himself [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), as [Mᵊnaššê](../../strongs/h/h4519.md) his ['ab](../../strongs/h/h1.md) had [kānaʿ](../../strongs/h/h3665.md) himself; but ['Āmôn](../../strongs/h/h526.md) ['ašmâ](../../strongs/h/h819.md) more and [rabah](../../strongs/h/h7235.md).

<a name="2chronicles_33_24"></a>2Chronicles 33:24

And his ['ebed](../../strongs/h/h5650.md) [qāšar](../../strongs/h/h7194.md) against him, and [muwth](../../strongs/h/h4191.md) him in his own [bayith](../../strongs/h/h1004.md).

<a name="2chronicles_33_25"></a>2Chronicles 33:25

But the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [nakah](../../strongs/h/h5221.md) all them that had [qāšar](../../strongs/h/h7194.md) against [melek](../../strongs/h/h4428.md) ['Āmôn](../../strongs/h/h526.md); and the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) [mālaḵ](../../strongs/h/h4427.md) [Yō'Šîyâ](../../strongs/h/h2977.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 32](2chronicles_32.md) - [2Chronicles 34](2chronicles_34.md)