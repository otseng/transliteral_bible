# [2Chronicles 21](https://www.blueletterbible.org/kjv/2chronicles/21)

<a name="2chronicles_21_1"></a>2Chronicles 21:1

Now [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md), and was [qāḇar](../../strongs/h/h6912.md) with his ['ab](../../strongs/h/h1.md) in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md). And [Yᵊhôrām](../../strongs/h/h3088.md) his [ben](../../strongs/h/h1121.md) [mālaḵ](../../strongs/h/h4427.md) in his stead.

<a name="2chronicles_21_2"></a>2Chronicles 21:2

And he had ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), [ʿĂzaryâ](../../strongs/h/h5838.md), and [Yᵊḥî'Ēl](../../strongs/h/h3171.md), and [Zᵊḵaryâ](../../strongs/h/h2148.md), and [ʿĂzaryâ](../../strongs/h/h5838.md), and [Mîḵā'ēl](../../strongs/h/h4317.md), and [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md): all these were the [ben](../../strongs/h/h1121.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_21_3"></a>2Chronicles 21:3

And their ['ab](../../strongs/h/h1.md) [nathan](../../strongs/h/h5414.md) them [rab](../../strongs/h/h7227.md) [matānâ](../../strongs/h/h4979.md) of [keceph](../../strongs/h/h3701.md), and of [zāhāḇ](../../strongs/h/h2091.md), and of [miḡdānôṯ](../../strongs/h/h4030.md), with [mᵊṣûrâ](../../strongs/h/h4694.md) [ʿîr](../../strongs/h/h5892.md) in [Yehuwdah](../../strongs/h/h3063.md): but the [mamlāḵâ](../../strongs/h/h4467.md) [nathan](../../strongs/h/h5414.md) he to [Yᵊhôrām](../../strongs/h/h3088.md); because he was the [bᵊḵôr](../../strongs/h/h1060.md).

<a name="2chronicles_21_4"></a>2Chronicles 21:4

Now when [Yᵊhôrām](../../strongs/h/h3088.md) was [quwm](../../strongs/h/h6965.md) to the [mamlāḵâ](../../strongs/h/h4467.md) of his ['ab](../../strongs/h/h1.md), he [ḥāzaq](../../strongs/h/h2388.md) himself, and [harag](../../strongs/h/h2026.md) all his ['ach](../../strongs/h/h251.md) with the [chereb](../../strongs/h/h2719.md), and of the [śar](../../strongs/h/h8269.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="2chronicles_21_5"></a>2Chronicles 21:5

[Yᵊhôrām](../../strongs/h/h3088.md) was thirty and two [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) eight [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_21_6"></a>2Chronicles 21:6

And he [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), like as ['asah](../../strongs/h/h6213.md) the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md): for he had the [bath](../../strongs/h/h1323.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md) to ['ishshah](../../strongs/h/h802.md): and he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_21_7"></a>2Chronicles 21:7

Howbeit [Yĕhovah](../../strongs/h/h3068.md) ['āḇâ](../../strongs/h/h14.md) not [shachath](../../strongs/h/h7843.md) the [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), because of the [bĕriyth](../../strongs/h/h1285.md) that he had [karath](../../strongs/h/h3772.md) with [Dāviḏ](../../strongs/h/h1732.md), and as he ['āmar](../../strongs/h/h559.md) to [nathan](../../strongs/h/h5414.md) a [nîr](../../strongs/h/h5216.md) to him and to his [ben](../../strongs/h/h1121.md) for [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_21_8"></a>2Chronicles 21:8

In his [yowm](../../strongs/h/h3117.md) the ['Ĕḏōm](../../strongs/h/h123.md) [pāšaʿ](../../strongs/h/h6586.md) from under the [yad](../../strongs/h/h3027.md) of [Yehuwdah](../../strongs/h/h3063.md), and [mālaḵ](../../strongs/h/h4427.md) themselves a [melek](../../strongs/h/h4428.md).

<a name="2chronicles_21_9"></a>2Chronicles 21:9

Then [Yᵊhôrām](../../strongs/h/h3088.md) ['abar](../../strongs/h/h5674.md) with his [śar](../../strongs/h/h8269.md), and all his [reḵeḇ](../../strongs/h/h7393.md) with him: and he [quwm](../../strongs/h/h6965.md) by [layil](../../strongs/h/h3915.md), and [nakah](../../strongs/h/h5221.md) the ['Ĕḏōm](../../strongs/h/h123.md) which compassed him [cabab](../../strongs/h/h5437.md), and the [śar](../../strongs/h/h8269.md) of the [reḵeḇ](../../strongs/h/h7393.md).

<a name="2chronicles_21_10"></a>2Chronicles 21:10

So the ['Ĕḏōm](../../strongs/h/h123.md) [pāšaʿ](../../strongs/h/h6586.md) from under the [yad](../../strongs/h/h3027.md) of [Yehuwdah](../../strongs/h/h3063.md) unto this [yowm](../../strongs/h/h3117.md). The same [ʿēṯ](../../strongs/h/h6256.md) also did [Liḇnâ](../../strongs/h/h3841.md) [pāšaʿ](../../strongs/h/h6586.md) from under his [yad](../../strongs/h/h3027.md); because he had ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of his ['ab](../../strongs/h/h1.md).

<a name="2chronicles_21_11"></a>2Chronicles 21:11

Moreover he ['asah](../../strongs/h/h6213.md) [bāmâ](../../strongs/h/h1116.md) in the [har](../../strongs/h/h2022.md) of [Yehuwdah](../../strongs/h/h3063.md), and caused the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) to [zānâ](../../strongs/h/h2181.md), and [nāḏaḥ](../../strongs/h/h5080.md) [Yehuwdah](../../strongs/h/h3063.md) thereto.

<a name="2chronicles_21_12"></a>2Chronicles 21:12

And there [bow'](../../strongs/h/h935.md) a [miḵtāḇ](../../strongs/h/h4385.md) to him from ['Ēlîyâ](../../strongs/h/h452.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Dāviḏ](../../strongs/h/h1732.md) thy ['ab](../../strongs/h/h1.md), Because thou hast not [halak](../../strongs/h/h1980.md) in the [derek](../../strongs/h/h1870.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md) thy ['ab](../../strongs/h/h1.md), nor in the [derek](../../strongs/h/h1870.md) of ['Āsā'](../../strongs/h/h609.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md),

<a name="2chronicles_21_13"></a>2Chronicles 21:13

But hast [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md), and hast made [Yehuwdah](../../strongs/h/h3063.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) to [zānâ](../../strongs/h/h2181.md), like to the [zānâ](../../strongs/h/h2181.md) of the [bayith](../../strongs/h/h1004.md) of ['Aḥ'Āḇ](../../strongs/h/h256.md), and also hast [harag](../../strongs/h/h2026.md) thy ['ach](../../strongs/h/h251.md) of thy ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), which were [towb](../../strongs/h/h2896.md) than thyself:

<a name="2chronicles_21_14"></a>2Chronicles 21:14

Behold, with a [gadowl](../../strongs/h/h1419.md) [magēp̄â](../../strongs/h/h4046.md) will [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) thy ['am](../../strongs/h/h5971.md), and thy [ben](../../strongs/h/h1121.md), and thy ['ishshah](../../strongs/h/h802.md), and all thy [rᵊḵûš](../../strongs/h/h7399.md):

<a name="2chronicles_21_15"></a>2Chronicles 21:15

And thou shalt have [rab](../../strongs/h/h7227.md) [ḥŏlî](../../strongs/h/h2483.md) by [maḥălê](../../strongs/h/h4245.md) of thy [me'ah](../../strongs/h/h4578.md), until thy [me'ah](../../strongs/h/h4578.md) [yāṣā'](../../strongs/h/h3318.md) by the [ḥŏlî](../../strongs/h/h2483.md) [yowm](../../strongs/h/h3117.md) by [yowm](../../strongs/h/h3117.md).

<a name="2chronicles_21_16"></a>2Chronicles 21:16

Moreover [Yĕhovah](../../strongs/h/h3068.md) [ʿûr](../../strongs/h/h5782.md) against [Yᵊhôrām](../../strongs/h/h3088.md) the [ruwach](../../strongs/h/h7307.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and of the [ʿĂrāḇî](../../strongs/h/h6163.md), that were near [yad](../../strongs/h/h3027.md) the [Kûšî](../../strongs/h/h3569.md):

<a name="2chronicles_21_17"></a>2Chronicles 21:17

And they [ʿālâ](../../strongs/h/h5927.md) into [Yehuwdah](../../strongs/h/h3063.md), and [bāqaʿ](../../strongs/h/h1234.md) into it, and [šāḇâ](../../strongs/h/h7617.md) all the [rᵊḵûš](../../strongs/h/h7399.md) that was [māṣā'](../../strongs/h/h4672.md) in the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and his [ben](../../strongs/h/h1121.md) also, and his ['ishshah](../../strongs/h/h802.md); so that there was never a [ben](../../strongs/h/h1121.md) [šā'ar](../../strongs/h/h7604.md) him, save [Yᵊhô'Āḥāz](../../strongs/h/h3059.md), the [qāṭān](../../strongs/h/h6996.md) of his [ben](../../strongs/h/h1121.md).

<a name="2chronicles_21_18"></a>2Chronicles 21:18

And ['aḥar](../../strongs/h/h310.md) all this [Yĕhovah](../../strongs/h/h3068.md) [nāḡap̄](../../strongs/h/h5062.md) him in his [me'ah](../../strongs/h/h4578.md) with a [marpē'](../../strongs/h/h4832.md) [ḥŏlî](../../strongs/h/h2483.md).

<a name="2chronicles_21_19"></a>2Chronicles 21:19

And it came to pass, that in process of [yowm](../../strongs/h/h3117.md), [yāṣā'](../../strongs/h/h3318.md) [ʿēṯ](../../strongs/h/h6256.md) the [qēṣ](../../strongs/h/h7093.md) of two [yowm](../../strongs/h/h3117.md), his [me'ah](../../strongs/h/h4578.md) fell [yāṣā'](../../strongs/h/h3318.md) by his [ḥŏlî](../../strongs/h/h2483.md): so he [muwth](../../strongs/h/h4191.md) of [ra'](../../strongs/h/h7451.md) [taḥălu'iym](../../strongs/h/h8463.md). And his ['am](../../strongs/h/h5971.md) ['asah](../../strongs/h/h6213.md) no [śᵊrēp̄â](../../strongs/h/h8316.md) for him, like the [śᵊrēp̄â](../../strongs/h/h8316.md) of his ['ab](../../strongs/h/h1.md).

<a name="2chronicles_21_20"></a>2Chronicles 21:20

Thirty and two years [ben](../../strongs/h/h1121.md) was he when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) eight [šānâ](../../strongs/h/h8141.md), and [yālaḵ](../../strongs/h/h3212.md) without being [ḥemdâ](../../strongs/h/h2532.md). Howbeit they [qāḇar](../../strongs/h/h6912.md) him in the [ʿîr](../../strongs/h/h5892.md) of [Dāviḏ](../../strongs/h/h1732.md), but not in the [qeber](../../strongs/h/h6913.md) of the [melek](../../strongs/h/h4428.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 20](2chronicles_20.md) - [2Chronicles 22](2chronicles_22.md)