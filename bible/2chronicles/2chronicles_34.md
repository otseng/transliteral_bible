# [2Chronicles 34](https://www.blueletterbible.org/kjv/2chronicles/34)

<a name="2chronicles_34_1"></a>2Chronicles 34:1

[Yō'Šîyâ](../../strongs/h/h2977.md) was eight [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) one and thirty [šānâ](../../strongs/h/h8141.md).

<a name="2chronicles_34_2"></a>2Chronicles 34:2

And he ['asah](../../strongs/h/h6213.md) that which was [yashar](../../strongs/h/h3477.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yālaḵ](../../strongs/h/h3212.md) in the [derek](../../strongs/h/h1870.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md), and [cuwr](../../strongs/h/h5493.md) neither to the [yamiyn](../../strongs/h/h3225.md), nor to the [śᵊmō'l](../../strongs/h/h8040.md).

<a name="2chronicles_34_3"></a>2Chronicles 34:3

For in the eighth [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), while he was yet [naʿar](../../strongs/h/h5288.md), he [ḥālal](../../strongs/h/h2490.md) to [darash](../../strongs/h/h1875.md) after the ['Elohiym](../../strongs/h/h430.md) of [Dāviḏ](../../strongs/h/h1732.md) his ['ab](../../strongs/h/h1.md): and in the twelfth [šānâ](../../strongs/h/h8141.md) he [ḥālal](../../strongs/h/h2490.md) to [ṭāhēr](../../strongs/h/h2891.md) [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) from the [bāmâ](../../strongs/h/h1116.md), and the ['ăšērâ](../../strongs/h/h842.md), and the [pāsîl](../../strongs/h/h6456.md), and the [massēḵâ](../../strongs/h/h4541.md).

<a name="2chronicles_34_4"></a>2Chronicles 34:4

And they [nāṯaṣ](../../strongs/h/h5422.md) the [mizbeach](../../strongs/h/h4196.md) of [BaʿAl](../../strongs/h/h1168.md) in his [paniym](../../strongs/h/h6440.md); and the [ḥammān](../../strongs/h/h2553.md), that were on high above [maʿal](../../strongs/h/h4605.md), he [gāḏaʿ](../../strongs/h/h1438.md); and the ['ăšērâ](../../strongs/h/h842.md), and the [pāsîl](../../strongs/h/h6456.md), and the [massēḵâ](../../strongs/h/h4541.md), he [shabar](../../strongs/h/h7665.md), and made [dāqaq](../../strongs/h/h1854.md) of them, and [zāraq](../../strongs/h/h2236.md) it [paniym](../../strongs/h/h6440.md) the [qeber](../../strongs/h/h6913.md) of them that had [zabach](../../strongs/h/h2076.md) unto them.

<a name="2chronicles_34_5"></a>2Chronicles 34:5

And he [śārap̄](../../strongs/h/h8313.md) the ['etsem](../../strongs/h/h6106.md) of the [kōhēn](../../strongs/h/h3548.md) upon their [mizbeach](../../strongs/h/h4196.md), and [ṭāhēr](../../strongs/h/h2891.md) [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_34_6"></a>2Chronicles 34:6

And so did he in the [ʿîr](../../strongs/h/h5892.md) of [Mᵊnaššê](../../strongs/h/h4519.md), and ['Ep̄rayim](../../strongs/h/h669.md), and [Šimʿôn](../../strongs/h/h8095.md), even unto [Nap̄tālî](../../strongs/h/h5321.md), with their [chereb](../../strongs/h/h2719.md) [cabiyb](../../strongs/h/h5439.md).

<a name="2chronicles_34_7"></a>2Chronicles 34:7

And when he had [nāṯaṣ](../../strongs/h/h5422.md) the [mizbeach](../../strongs/h/h4196.md) and the ['ăšērâ](../../strongs/h/h842.md), and had [kāṯaṯ](../../strongs/h/h3807.md) the [pāsîl](../../strongs/h/h6456.md) into [dāqaq](../../strongs/h/h1854.md), and cut [gāḏaʿ](../../strongs/h/h1438.md) all the [ḥammān](../../strongs/h/h2553.md) throughout all the ['erets](../../strongs/h/h776.md) of [Yisra'el](../../strongs/h/h3478.md), he [shuwb](../../strongs/h/h7725.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_34_8"></a>2Chronicles 34:8

Now in the eighteenth [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), when he had [ṭāhēr](../../strongs/h/h2891.md) the ['erets](../../strongs/h/h776.md), and the [bayith](../../strongs/h/h1004.md), he [shalach](../../strongs/h/h7971.md) [Šāp̄ān](../../strongs/h/h8227.md) the [ben](../../strongs/h/h1121.md) of ['Ăṣalyâû](../../strongs/h/h683.md), and [MaʿĂśêâ](../../strongs/h/h4641.md) the [śar](../../strongs/h/h8269.md) of the [ʿîr](../../strongs/h/h5892.md), and [Yô'āḥ](../../strongs/h/h3098.md) the [ben](../../strongs/h/h1121.md) of [Yô'Āḥāz](../../strongs/h/h3099.md) the [zakar](../../strongs/h/h2142.md), to [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md).

<a name="2chronicles_34_9"></a>2Chronicles 34:9

And when they [bow'](../../strongs/h/h935.md) to [Ḥilqîyâ](../../strongs/h/h2518.md) the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md), they [nathan](../../strongs/h/h5414.md) the [keceph](../../strongs/h/h3701.md) that was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of ['Elohiym](../../strongs/h/h430.md), which the [Lᵊvî](../../strongs/h/h3881.md) that [shamar](../../strongs/h/h8104.md) the [caph](../../strongs/h/h5592.md) had ['āsap̄](../../strongs/h/h622.md) of the [yad](../../strongs/h/h3027.md) of [Mᵊnaššê](../../strongs/h/h4519.md) and ['Ep̄rayim](../../strongs/h/h669.md), and of all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md), and of all [Yehuwdah](../../strongs/h/h3063.md) and [Binyāmîn](../../strongs/h/h1144.md); and they [shuwb](../../strongs/h/h7725.md) [yashab](../../strongs/h/h3427.md) to [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_34_10"></a>2Chronicles 34:10

And they [nathan](../../strongs/h/h5414.md) it in the [yad](../../strongs/h/h3027.md) of the ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) that had the [paqad](../../strongs/h/h6485.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and they [nathan](../../strongs/h/h5414.md) it to the ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md) that ['asah](../../strongs/h/h6213.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), to [bāḏaq](../../strongs/h/h918.md) and [ḥāzaq](../../strongs/h/h2388.md) the [bayith](../../strongs/h/h1004.md):

<a name="2chronicles_34_11"></a>2Chronicles 34:11

Even to the [ḥārāš](../../strongs/h/h2796.md) and [bānâ](../../strongs/h/h1129.md) [nathan](../../strongs/h/h5414.md) they it, to [qānâ](../../strongs/h/h7069.md) [maḥṣēḇ](../../strongs/h/h4274.md) ['eben](../../strongs/h/h68.md), and ['ets](../../strongs/h/h6086.md) for [mᵊḥabrâ](../../strongs/h/h4226.md), and to [qārâ](../../strongs/h/h7136.md) the [bayith](../../strongs/h/h1004.md) which the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [shachath](../../strongs/h/h7843.md).

<a name="2chronicles_34_12"></a>2Chronicles 34:12

And the ['enowsh](../../strongs/h/h582.md) ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) ['ĕmûnâ](../../strongs/h/h530.md): and the [nāṣaḥ](../../strongs/h/h5329.md) of them were [Yaḥaṯ](../../strongs/h/h3189.md) and [ʿŌḇaḏyâ](../../strongs/h/h5662.md), the [Lᵊvî](../../strongs/h/h3881.md), of the [ben](../../strongs/h/h1121.md) of [Mᵊrārî](../../strongs/h/h4847.md); and [Zᵊḵaryâ](../../strongs/h/h2148.md) and [Mᵊšullām](../../strongs/h/h4918.md), of the sons of the [Qᵊhāṯî](../../strongs/h/h6956.md), to set it [paqad](../../strongs/h/h6485.md); and other of the [Lᵊvî](../../strongs/h/h3881.md), all that could [bîn](../../strongs/h/h995.md) of [kĕliy](../../strongs/h/h3627.md) of [šîr](../../strongs/h/h7892.md).

<a name="2chronicles_34_13"></a>2Chronicles 34:13

Also they were over the [sabāl](../../strongs/h/h5449.md), and were [nāṣaḥ](../../strongs/h/h5329.md) of all that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) in any manner of [ʿăḇōḏâ](../../strongs/h/h5656.md): and of the [Lᵊvî](../../strongs/h/h3881.md) there were [sāp̄ar](../../strongs/h/h5608.md), and [šāṭar](../../strongs/h/h7860.md), and [šôʿēr](../../strongs/h/h7778.md).

<a name="2chronicles_34_14"></a>2Chronicles 34:14

And when they [yāṣā'](../../strongs/h/h3318.md) the [keceph](../../strongs/h/h3701.md) that was [bow'](../../strongs/h/h935.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md) [māṣā'](../../strongs/h/h4672.md) a [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) given [yad](../../strongs/h/h3027.md) [Mōshe](../../strongs/h/h4872.md).

<a name="2chronicles_34_15"></a>2Chronicles 34:15

And [Ḥilqîyâ](../../strongs/h/h2518.md) ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) to [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md), I have [māṣā'](../../strongs/h/h4672.md) the [sēp̄er](../../strongs/h/h5612.md) of the [towrah](../../strongs/h/h8451.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). And [Ḥilqîyâ](../../strongs/h/h2518.md) [nathan](../../strongs/h/h5414.md) the [sēp̄er](../../strongs/h/h5612.md) to [Šāp̄ān](../../strongs/h/h8227.md).

<a name="2chronicles_34_16"></a>2Chronicles 34:16

And [Šāp̄ān](../../strongs/h/h8227.md) [bow'](../../strongs/h/h935.md) the [sēp̄er](../../strongs/h/h5612.md) to the [melek](../../strongs/h/h4428.md), and [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md) again, ['āmar](../../strongs/h/h559.md), All that was [nathan](../../strongs/h/h5414.md) [yad](../../strongs/h/h3027.md) thy ['ebed](../../strongs/h/h5650.md), they ['asah](../../strongs/h/h6213.md) it.

<a name="2chronicles_34_17"></a>2Chronicles 34:17

And they have [nāṯaḵ](../../strongs/h/h5413.md) the [keceph](../../strongs/h/h3701.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and have [nathan](../../strongs/h/h5414.md) it into the [yad](../../strongs/h/h3027.md) of the [paqad](../../strongs/h/h6485.md), and to the [yad](../../strongs/h/h3027.md) of the ['asah](../../strongs/h/h6213.md) [mĕla'kah](../../strongs/h/h4399.md).

<a name="2chronicles_34_18"></a>2Chronicles 34:18

Then [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md) [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md), [Ḥilqîyâ](../../strongs/h/h2518.md) the [kōhēn](../../strongs/h/h3548.md) hath [nathan](../../strongs/h/h5414.md) me a [sēp̄er](../../strongs/h/h5612.md). And [Šāp̄ān](../../strongs/h/h8227.md) [qara'](../../strongs/h/h7121.md) it [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md).

<a name="2chronicles_34_19"></a>2Chronicles 34:19

And it came to pass, when the [melek](../../strongs/h/h4428.md) had [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of the [towrah](../../strongs/h/h8451.md), that he [qāraʿ](../../strongs/h/h7167.md) his [beḡeḏ](../../strongs/h/h899.md).

<a name="2chronicles_34_20"></a>2Chronicles 34:20

And the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Ḥilqîyâ](../../strongs/h/h2518.md), and ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), and [ʿAḇdôn](../../strongs/h/h5658.md) the [ben](../../strongs/h/h1121.md) of [Mîḵâ](../../strongs/h/h4318.md), and [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md), and [ʿĂśāyâ](../../strongs/h/h6222.md) an ['ebed](../../strongs/h/h5650.md) of the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md),

<a name="2chronicles_34_21"></a>2Chronicles 34:21

[yālaḵ](../../strongs/h/h3212.md), [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md) for me, and for them that are [šā'ar](../../strongs/h/h7604.md) in [Yisra'el](../../strongs/h/h3478.md) and in [Yehuwdah](../../strongs/h/h3063.md), concerning the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) that is [māṣā'](../../strongs/h/h4672.md): for [gadowl](../../strongs/h/h1419.md) is the [chemah](../../strongs/h/h2534.md) of [Yĕhovah](../../strongs/h/h3068.md) that is [nāṯaḵ](../../strongs/h/h5413.md) upon us, because our ['ab](../../strongs/h/h1.md) have not [shamar](../../strongs/h/h8104.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), to ['asah](../../strongs/h/h6213.md) after all that is [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md).

<a name="2chronicles_34_22"></a>2Chronicles 34:22

And [Ḥilqîyâ](../../strongs/h/h2518.md), and they that the [melek](../../strongs/h/h4428.md) had appointed, [yālaḵ](../../strongs/h/h3212.md) to [Ḥuldâ](../../strongs/h/h2468.md) the [nᵊḇî'â](../../strongs/h/h5031.md), the ['ishshah](../../strongs/h/h802.md) of [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Tiqvâ](../../strongs/h/h8616.md) [Tôqahaṯ](../../strongs/h/h8445.md) , the [ben](../../strongs/h/h1121.md) of [Ḥasrâ](../../strongs/h/h2641.md), [shamar](../../strongs/h/h8104.md) of the [beḡeḏ](../../strongs/h/h899.md); (now she [yashab](../../strongs/h/h3427.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) in the [mišnê](../../strongs/h/h4932.md):) and they [dabar](../../strongs/h/h1696.md) to her to that effect.

<a name="2chronicles_34_23"></a>2Chronicles 34:23

And she ['āmar](../../strongs/h/h559.md) them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md) ye the ['iysh](../../strongs/h/h376.md) that [shalach](../../strongs/h/h7971.md) you to me,

<a name="2chronicles_34_24"></a>2Chronicles 34:24

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon this [maqowm](../../strongs/h/h4725.md), and upon the [yashab](../../strongs/h/h3427.md) thereof, even all the ['alah](../../strongs/h/h423.md) that are [kāṯaḇ](../../strongs/h/h3789.md) in the [sēp̄er](../../strongs/h/h5612.md) which they have [qara'](../../strongs/h/h7121.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md):

<a name="2chronicles_34_25"></a>2Chronicles 34:25

Because they have ['azab](../../strongs/h/h5800.md) me, and have [qāṭar](../../strongs/h/h6999.md) [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), that they might [kāʿas](../../strongs/h/h3707.md) me with all the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md); therefore my [chemah](../../strongs/h/h2534.md) shall be [nāṯaḵ](../../strongs/h/h5413.md) upon this [maqowm](../../strongs/h/h4725.md), and shall not be [kāḇâ](../../strongs/h/h3518.md).

<a name="2chronicles_34_26"></a>2Chronicles 34:26

And as for the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), who [shalach](../../strongs/h/h7971.md) you to [darash](../../strongs/h/h1875.md) of [Yĕhovah](../../strongs/h/h3068.md), so shall ye ['āmar](../../strongs/h/h559.md) unto him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) concerning the [dabar](../../strongs/h/h1697.md) which thou hast [shama'](../../strongs/h/h8085.md);

<a name="2chronicles_34_27"></a>2Chronicles 34:27

Because thine [lebab](../../strongs/h/h3824.md) was [rāḵaḵ](../../strongs/h/h7401.md), and thou didst [kānaʿ](../../strongs/h/h3665.md) thyself [paniym](../../strongs/h/h6440.md) ['Elohiym](../../strongs/h/h430.md), when thou [shama'](../../strongs/h/h8085.md) his [dabar](../../strongs/h/h1697.md) against this [maqowm](../../strongs/h/h4725.md), and against the [yashab](../../strongs/h/h3427.md) thereof, and [kānaʿ](../../strongs/h/h3665.md) thyself [paniym](../../strongs/h/h6440.md) me, and didst [qāraʿ](../../strongs/h/h7167.md) thy [beḡeḏ](../../strongs/h/h899.md), and [bāḵâ](../../strongs/h/h1058.md) [paniym](../../strongs/h/h6440.md) me; I have even [shama'](../../strongs/h/h8085.md) thee also, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_34_28"></a>2Chronicles 34:28

Behold, I will ['āsap̄](../../strongs/h/h622.md) thee to thy ['ab](../../strongs/h/h1.md), and thou shalt be ['āsap̄](../../strongs/h/h622.md) to thy [qeber](../../strongs/h/h6913.md) in [shalowm](../../strongs/h/h7965.md), neither shall thine ['ayin](../../strongs/h/h5869.md) [ra'ah](../../strongs/h/h7200.md) all the [ra'](../../strongs/h/h7451.md) that I will [bow'](../../strongs/h/h935.md) upon this [maqowm](../../strongs/h/h4725.md), and upon the [yashab](../../strongs/h/h3427.md) of the same. So they [shuwb](../../strongs/h/h7725.md) the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1697.md) [shuwb](../../strongs/h/h7725.md).

<a name="2chronicles_34_29"></a>2Chronicles 34:29

Then the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) and ['āsap̄](../../strongs/h/h622.md) all the [zāqēn](../../strongs/h/h2205.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="2chronicles_34_30"></a>2Chronicles 34:30

And the [melek](../../strongs/h/h4428.md) [ʿālâ](../../strongs/h/h5927.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and all the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and the [kōhēn](../../strongs/h/h3548.md), and the [Lᵊvî](../../strongs/h/h3881.md), and all the ['am](../../strongs/h/h5971.md), [gadowl](../../strongs/h/h1419.md) and [qāṭān](../../strongs/h/h6996.md): and he [qara'](../../strongs/h/h7121.md) in their ['ozen](../../strongs/h/h241.md) all the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) of the [bĕriyth](../../strongs/h/h1285.md) that was [māṣā'](../../strongs/h/h4672.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="2chronicles_34_31"></a>2Chronicles 34:31

And the [melek](../../strongs/h/h4428.md) ['amad](../../strongs/h/h5975.md) in his [ʿōmeḏ](../../strongs/h/h5977.md), and [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), and to [shamar](../../strongs/h/h8104.md) his [mitsvah](../../strongs/h/h4687.md), and his [ʿēḏûṯ](../../strongs/h/h5715.md), and his [choq](../../strongs/h/h2706.md), with all his [lebab](../../strongs/h/h3824.md), and with all his [nephesh](../../strongs/h/h5315.md), to ['asah](../../strongs/h/h6213.md) the [dabar](../../strongs/h/h1697.md) of the [bĕriyth](../../strongs/h/h1285.md) which are [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md).

<a name="2chronicles_34_32"></a>2Chronicles 34:32

And he caused all that were [māṣā'](../../strongs/h/h4672.md) in [Yĕruwshalaim](../../strongs/h/h3389.md) and [Binyāmîn](../../strongs/h/h1144.md) to ['amad](../../strongs/h/h5975.md) to it. And the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) ['asah](../../strongs/h/h6213.md) according to the [bĕriyth](../../strongs/h/h1285.md) of ['Elohiym](../../strongs/h/h430.md), the ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

<a name="2chronicles_34_33"></a>2Chronicles 34:33

And [Yō'Šîyâ](../../strongs/h/h2977.md) took [cuwr](../../strongs/h/h5493.md) all the [tôʿēḇâ](../../strongs/h/h8441.md) out of all the ['erets](../../strongs/h/h776.md) that pertained to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and made all that were [māṣā'](../../strongs/h/h4672.md) in [Yisra'el](../../strongs/h/h3478.md) to ['abad](../../strongs/h/h5647.md), even to ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md). And all his [yowm](../../strongs/h/h3117.md) they [cuwr](../../strongs/h/h5493.md) not from ['aḥar](../../strongs/h/h310.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of their ['ab](../../strongs/h/h1.md).

---

[Transliteral Bible](../bible.md)

[2Chronicles](2chronicles.md)

[2Chronicles 33](2chronicles_33.md) - [2Chronicles 35](2chronicles_35.md)