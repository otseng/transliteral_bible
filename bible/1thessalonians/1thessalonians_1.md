# [1 Thessalonians 1](https://www.blueletterbible.org/kjv/1th/1/1/s_1112001)

<a name="1thessalonians_1_1"></a>1 Thessalonians 1:1

[Paulos](../../strongs/g/g3972.md), and [silouanos](../../strongs/g/g4610.md), and [Timotheos](../../strongs/g/g5095.md), unto the [ekklēsia](../../strongs/g/g1577.md) of the [Thessalonikeus](../../strongs/g/g2331.md) in [theos](../../strongs/g/g2316.md) the [patēr](../../strongs/g/g3962.md) and in the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md): [charis](../../strongs/g/g5485.md) unto you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1thessalonians_1_2"></a>1 Thessalonians 1:2

We [eucharisteō](../../strongs/g/g2168.md) to [theos](../../strongs/g/g2316.md) [pantote](../../strongs/g/g3842.md) for you all, [poieō](../../strongs/g/g4160.md) [mneia](../../strongs/g/g3417.md) of you in our [proseuchē](../../strongs/g/g4335.md);

<a name="1thessalonians_1_3"></a>1 Thessalonians 1:3

[mnēmoneuō](../../strongs/g/g3421.md) [adialeiptos](../../strongs/g/g89.md) your [ergon](../../strongs/g/g2041.md) of [pistis](../../strongs/g/g4102.md), and [kopos](../../strongs/g/g2873.md) of [agapē](../../strongs/g/g26.md), and [hypomonē](../../strongs/g/g5281.md) of [elpis](../../strongs/g/g1680.md) in our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [emprosthen](../../strongs/g/g1715.md) of [theos](../../strongs/g/g2316.md) and our [patēr](../../strongs/g/g3962.md);

<a name="1thessalonians_1_4"></a>1 Thessalonians 1:4

[eidō](../../strongs/g/g1492.md), [adelphos](../../strongs/g/g80.md) [agapaō](../../strongs/g/g25.md), your [eklogē](../../strongs/g/g1589.md) of [theos](../../strongs/g/g2316.md).

<a name="1thessalonians_1_5"></a>1 Thessalonians 1:5

For our [euaggelion](../../strongs/g/g2098.md) [ginomai](../../strongs/g/g1096.md) not unto you in [logos](../../strongs/g/g3056.md) only, but also in [dynamis](../../strongs/g/g1411.md), and in the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md), and in [polys](../../strongs/g/g4183.md) [plērophoria](../../strongs/g/g4136.md); as ye [eidō](../../strongs/g/g1492.md) [hoios](../../strongs/g/g3634.md) we were among you for your sake.

<a name="1thessalonians_1_6"></a>1 Thessalonians 1:6

And ye became [mimētēs](../../strongs/g/g3402.md) of us, and of the [kyrios](../../strongs/g/g2962.md), having [dechomai](../../strongs/g/g1209.md) the [logos](../../strongs/g/g3056.md) in [polys](../../strongs/g/g4183.md) [thlipsis](../../strongs/g/g2347.md), with [chara](../../strongs/g/g5479.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="1thessalonians_1_7"></a>1Thessalonians 1:7

So that ye were [typos](../../strongs/g/g5179.md) to all that [pisteuō](../../strongs/g/g4100.md) in [Makedonia](../../strongs/g/g3109.md) and [Achaïa](../../strongs/g/g882.md).

<a name="1thessalonians_1_8"></a>1 Thessalonians 1:8

For from you [exēcheō](../../strongs/g/g1837.md) out the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md) not only in [Makedonia](../../strongs/g/g3109.md) and [Achaïa](../../strongs/g/g882.md), but also in every [topos](../../strongs/g/g5117.md) your [pistis](../../strongs/g/g4102.md) to [pros](../../strongs/g/g4314.md) [theos](../../strongs/g/g2316.md) is [exerchomai](../../strongs/g/g1831.md); so that we [chreia](../../strongs/g/g5532.md) not to [laleō](../../strongs/g/g2980.md) any thing.

<a name="1thessalonians_1_9"></a>1 Thessalonians 1:9

For they themselves [apaggellō](../../strongs/g/g518.md) of us what manner of [eisodos](../../strongs/g/g1529.md) we had unto you, and how ye [epistrephō](../../strongs/g/g1994.md) to [theos](../../strongs/g/g2316.md) from [eidōlon](../../strongs/g/g1497.md) to [douleuō](../../strongs/g/g1398.md) the [zaō](../../strongs/g/g2198.md) and [alēthinos](../../strongs/g/g228.md) [theos](../../strongs/g/g2316.md);

<a name="1thessalonians_1_10"></a>1 Thessalonians 1:10

And to [anamenō](../../strongs/g/g362.md) for his [huios](../../strongs/g/g5207.md) from [ouranos](../../strongs/g/g3772.md), whom he [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), even [Iēsous](../../strongs/g/g2424.md), which [rhyomai](../../strongs/g/g4506.md) us from the [orgē](../../strongs/g/g3709.md) to [erchomai](../../strongs/g/g2064.md).

---

[Transliteral Bible](../bible.md)

[1 Thessalonians](1thessalonians.md)

[1 Thessalonians 2](1thessalonians_2.md)