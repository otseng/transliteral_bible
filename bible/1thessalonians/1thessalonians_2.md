# [1 Thessalonians 2](https://www.blueletterbible.org/kjv/1th/2/1/s_1113001)

<a name="1thessalonians_2_1"></a>1 Thessalonians 2:1

For yourselves, [adelphos](../../strongs/g/g80.md), [eidō](../../strongs/g/g1492.md) our [eisodos](../../strongs/g/g1529.md) in unto you, that it was not in [kenos](../../strongs/g/g2756.md):

<a name="1thessalonians_2_2"></a>1 Thessalonians 2:2

But even after that we had [propaschō](../../strongs/g/g4310.md) before, and were [hybrizō](../../strongs/g/g5195.md), as ye [eidō](../../strongs/g/g1492.md), at [Philippoi](../../strongs/g/g5375.md), we were [parrēsiazomai](../../strongs/g/g3955.md) in our [theos](../../strongs/g/g2316.md) to [laleō](../../strongs/g/g2980.md) unto you the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md) with [polys](../../strongs/g/g4183.md) [agōn](../../strongs/g/g73.md).

<a name="1thessalonians_2_3"></a>1 Thessalonians 2:3

For our [paraklēsis](../../strongs/g/g3874.md) was not of [planē](../../strongs/g/g4106.md), nor of [akatharsia](../../strongs/g/g167.md), nor in [dolos](../../strongs/g/g1388.md):

<a name="1thessalonians_2_4"></a>1 Thessalonians 2:4

But as we were [dokimazō](../../strongs/g/g1381.md) of [theos](../../strongs/g/g2316.md) to be put in [pisteuō](../../strongs/g/g4100.md) with the [euaggelion](../../strongs/g/g2098.md), even so we [laleō](../../strongs/g/g2980.md); not as [areskō](../../strongs/g/g700.md) [anthrōpos](../../strongs/g/g444.md), but [theos](../../strongs/g/g2316.md), which [dokimazō](../../strongs/g/g1381.md) our [kardia](../../strongs/g/g2588.md).

<a name="1thessalonians_2_5"></a>1 Thessalonians 2:5

For neither at any time used we [kolakeia](../../strongs/g/g2850.md) [logos](../../strongs/g/g3056.md), as ye [eidō](../../strongs/g/g1492.md), nor a [prophasis](../../strongs/g/g4392.md) of [pleonexia](../../strongs/g/g4124.md); [theos](../../strongs/g/g2316.md) [martys](../../strongs/g/g3144.md):

<a name="1thessalonians_2_6"></a>1 Thessalonians 2:6

Nor of [anthrōpos](../../strongs/g/g444.md) [zēteō](../../strongs/g/g2212.md) we [doxa](../../strongs/g/g1391.md), neither of you, nor of others, when we might have been [en](../../strongs/g/g1722.md) [baros](../../strongs/g/g922.md), as the [apostolos](../../strongs/g/g652.md) of [Christos](../../strongs/g/g5547.md).

<a name="1thessalonians_2_7"></a>1 Thessalonians 2:7

But we were [ēpios](../../strongs/g/g2261.md) among you, even as a [trophos](../../strongs/g/g5162.md) [thalpō](../../strongs/g/g2282.md) her [teknon](../../strongs/g/g5043.md):

<a name="1thessalonians_2_8"></a>1 Thessalonians 2:8

So being [homeiromai](../../strongs/g/g2442.md) of you, we were [eudokeō](../../strongs/g/g2106.md) to have [metadidōmi](../../strongs/g/g3330.md) unto you, not the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md) only, but also our own [psychē](../../strongs/g/g5590.md), because ye were [agapētos](../../strongs/g/g27.md) unto us.

<a name="1thessalonians_2_9"></a>1 Thessalonians 2:9

For ye [mnēmoneuō](../../strongs/g/g3421.md), [adelphos](../../strongs/g/g80.md), our [kopos](../../strongs/g/g2873.md) and [mochthos](../../strongs/g/g3449.md): for [ergazomai](../../strongs/g/g2038.md) [nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md), because we would not be [epibareō](../../strongs/g/g1912.md) unto any of you, we [kēryssō](../../strongs/g/g2784.md) unto you the [euaggelion](../../strongs/g/g2098.md) of [theos](../../strongs/g/g2316.md).

<a name="1thessalonians_2_10"></a>1 Thessalonians 2:10

Ye are [martys](../../strongs/g/g3144.md), and [theos](../../strongs/g/g2316.md), how [hosiōs](../../strongs/g/g3743.md) and [dikaiōs](../../strongs/g/g1346.md) and [amemptōs](../../strongs/g/g274.md) we [ginomai](../../strongs/g/g1096.md) among you that [pisteuō](../../strongs/g/g4100.md):

<a name="1thessalonians_2_11"></a>1 Thessalonians 2:11

As ye [eidō](../../strongs/g/g1492.md) how we [parakaleō](../../strongs/g/g3870.md) and [paramytheomai](../../strongs/g/g3888.md) and [martyreō](../../strongs/g/g3140.md) every one of you, as a [patēr](../../strongs/g/g3962.md) his [teknon](../../strongs/g/g5043.md),

<a name="1thessalonians_2_12"></a>1 Thessalonians 2:12

That ye would [peripateō](../../strongs/g/g4043.md) [axiōs](../../strongs/g/g516.md) of [theos](../../strongs/g/g2316.md), who hath [kaleō](../../strongs/g/g2564.md) you unto his [basileia](../../strongs/g/g932.md) and [doxa](../../strongs/g/g1391.md).

<a name="1thessalonians_2_13"></a>1 Thessalonians 2:13

For this cause also [eucharisteō](../../strongs/g/g2168.md) we [theos](../../strongs/g/g2316.md) [adialeiptos](../../strongs/g/g89.md), because, when ye [paralambanō](../../strongs/g/g3880.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) which ye [akoē](../../strongs/g/g189.md) of us, ye [dechomai](../../strongs/g/g1209.md) not as the [logos](../../strongs/g/g3056.md) of [anthrōpos](../../strongs/g/g444.md), but as it is in [alēthōs](../../strongs/g/g230.md), the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), which [energeō](../../strongs/g/g1754.md) also in you that [pisteuō](../../strongs/g/g4100.md).

<a name="1thessalonians_2_14"></a>1 Thessalonians 2:14

For ye, [adelphos](../../strongs/g/g80.md), became [mimētēs](../../strongs/g/g3402.md) of the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md) which in [Ioudaia](../../strongs/g/g2449.md) are in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md): for ye also have [paschō](../../strongs/g/g3958.md) like things of your own [symphyletēs](../../strongs/g/g4853.md), even as they of the [Ioudaios](../../strongs/g/g2453.md):

<a name="1thessalonians_2_15"></a>1 Thessalonians 2:15

Who both [apokteinō](../../strongs/g/g615.md) the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and their own [prophētēs](../../strongs/g/g4396.md), and have [ekdiōkō](../../strongs/g/g1559.md) us; and they [areskō](../../strongs/g/g700.md) not [theos](../../strongs/g/g2316.md), and are [enantios](../../strongs/g/g1727.md) to all [anthrōpos](../../strongs/g/g444.md):

<a name="1thessalonians_2_16"></a>1 Thessalonians 2:16

[kōlyō](../../strongs/g/g2967.md) us to [laleō](../../strongs/g/g2980.md) to the [ethnos](../../strongs/g/g1484.md) that they might be [sōzō](../../strongs/g/g4982.md), to [anaplēroō](../../strongs/g/g378.md) up their [hamartia](../../strongs/g/g266.md) [pantote](../../strongs/g/g3842.md): for the [orgē](../../strongs/g/g3709.md) is [phthanō](../../strongs/g/g5348.md) upon them to the [telos](../../strongs/g/g5056.md).

<a name="1thessalonians_2_17"></a>1 Thessalonians 2:17

But we, [adelphos](../../strongs/g/g80.md), being [aporphanizō](../../strongs/g/g642.md) from you for a [hōra](../../strongs/g/g5610.md) [kairos](../../strongs/g/g2540.md) in [prosōpon](../../strongs/g/g4383.md), not in [kardia](../../strongs/g/g2588.md), [spoudazō](../../strongs/g/g4704.md) the [perissoterōs](../../strongs/g/g4056.md) to [eidō](../../strongs/g/g1492.md) your [prosōpon](../../strongs/g/g4383.md) with [polys](../../strongs/g/g4183.md) [epithymia](../../strongs/g/g1939.md).

<a name="1thessalonians_2_18"></a>1 Thessalonians 2:18

Wherefore we would have [erchomai](../../strongs/g/g2064.md) unto you, even I [Paulos](../../strongs/g/g3972.md), [hapax](../../strongs/g/g530.md) and again; but [Satanas](../../strongs/g/g4567.md) [egkoptō](../../strongs/g/g1465.md) us.

<a name="1thessalonians_2_19"></a>1 Thessalonians 2:19

For what is our [elpis](../../strongs/g/g1680.md), or [chara](../../strongs/g/g5479.md), or [stephanos](../../strongs/g/g4735.md) of [kauchēsis](../../strongs/g/g2746.md)? Are not even ye in the [emprosthen](../../strongs/g/g1715.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) at his [parousia](../../strongs/g/g3952.md)?

<a name="1thessalonians_2_20"></a>1 Thessalonians 2:20

For ye are our [doxa](../../strongs/g/g1391.md) and [chara](../../strongs/g/g5479.md).

---

[Transliteral Bible](../bible.md)

[1 Thessalonians](1thessalonians.md)

[1 Thessalonians 1](1thessalonians_1.md) - [1 Thessalonians 3](1thessalonians_3.md)