# [1 Thessalonians 3](https://www.blueletterbible.org/kjv/1th/3/1/s_1114001)

<a name="1thessalonians_3_1"></a>1 Thessalonians 3:1

Wherefore when we could no longer [stegō](../../strongs/g/g4722.md), we thought it [eudokeō](../../strongs/g/g2106.md) to be [kataleipō](../../strongs/g/g2641.md) at [Athēnai](../../strongs/g/g116.md) alone;

<a name="1thessalonians_3_2"></a>1 Thessalonians 3:2

And [pempō](../../strongs/g/g3992.md) [Timotheos](../../strongs/g/g5095.md), our [adelphos](../../strongs/g/g80.md), and [diakonos](../../strongs/g/g1249.md) of [theos](../../strongs/g/g2316.md), and our [synergos](../../strongs/g/g4904.md) in the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md), to [stērizō](../../strongs/g/g4741.md) you, and to [parakaleō](../../strongs/g/g3870.md) you concerning your [pistis](../../strongs/g/g4102.md):

<a name="1thessalonians_3_3"></a>1 Thessalonians 3:3

That [mēdeis](../../strongs/g/g3367.md) should be [sainō](../../strongs/g/g4525.md) by these [thlipsis](../../strongs/g/g2347.md): for yourselves [eidō](../../strongs/g/g1492.md) that we are [keimai](../../strongs/g/g2749.md) thereunto.

<a name="1thessalonians_3_4"></a>1 Thessalonians 3:4

[kai](../../strongs/g/g2532.md), when we were with you, we [prolegō](../../strongs/g/g4302.md) you that we should [thlibō](../../strongs/g/g2346.md); even as [ginomai](../../strongs/g/g1096.md), and ye [eidō](../../strongs/g/g1492.md).

<a name="1thessalonians_3_5"></a>1 Thessalonians 3:5

For this cause, when I could no longer [stegō](../../strongs/g/g4722.md), I [pempō](../../strongs/g/g3992.md) to [ginōskō](../../strongs/g/g1097.md) your [pistis](../../strongs/g/g4102.md), lest by some means the [peirazō](../../strongs/g/g3985.md) have [peirazō](../../strongs/g/g3985.md) you, and our [kopos](../../strongs/g/g2873.md) be in [kenos](../../strongs/g/g2756.md).

<a name="1thessalonians_3_6"></a>1 Thessalonians 3:6

But now when [Timotheos](../../strongs/g/g5095.md) [erchomai](../../strongs/g/g2064.md) from you unto us, and [euaggelizō](../../strongs/g/g2097.md) us of your [pistis](../../strongs/g/g4102.md) and [agapē](../../strongs/g/g26.md), and that ye have [agathos](../../strongs/g/g18.md) [mneia](../../strongs/g/g3417.md) of us [pantote](../../strongs/g/g3842.md), [epipotheo](../../strongs/g/g1971.md) to [eidō](../../strongs/g/g1492.md) us, as we also to you:

<a name="1thessalonians_3_7"></a>1 Thessalonians 3:7

Therefore, [adelphos](../../strongs/g/g80.md), we were [parakaleō](../../strongs/g/g3870.md) over you in all our [thlipsis](../../strongs/g/g2347.md) and [anagkē](../../strongs/g/g318.md) by your [pistis](../../strongs/g/g4102.md):

<a name="1thessalonians_3_8"></a>1 Thessalonians 3:8

For now we [zaō](../../strongs/g/g2198.md), if ye [stēkō](../../strongs/g/g4739.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="1thessalonians_3_9"></a>1 Thessalonians 3:9

For what [eucharistia](../../strongs/g/g2169.md) can we [antapodidōmi](../../strongs/g/g467.md) to [theos](../../strongs/g/g2316.md) again for you, for all the [chara](../../strongs/g/g5479.md) wherewith we [chairō](../../strongs/g/g5463.md) for your sakes before our [theos](../../strongs/g/g2316.md);

<a name="1thessalonians_3_10"></a>1 Thessalonians 3:10

[nyx](../../strongs/g/g3571.md) and [hēmera](../../strongs/g/g2250.md) [deomai](../../strongs/g/g1189.md) [perissos](../../strongs/g/g4053.md) that we might [eidō](../../strongs/g/g1492.md) your [prosōpon](../../strongs/g/g4383.md), and might [katartizō](../../strongs/g/g2675.md) that which is [hysterēma](../../strongs/g/g5303.md) in your [pistis](../../strongs/g/g4102.md)?

<a name="1thessalonians_3_11"></a>1 Thessalonians 3:11

Now [theos](../../strongs/g/g2316.md) himself and our [patēr](../../strongs/g/g3962.md), and our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), [kateuthynō](../../strongs/g/g2720.md) our [hodos](../../strongs/g/g3598.md) unto you.

<a name="1thessalonians_3_12"></a>1 Thessalonians 3:12

And the [kyrios](../../strongs/g/g2962.md) make you to [pleonazō](../../strongs/g/g4121.md) and [perisseuō](../../strongs/g/g4052.md) in [agapē](../../strongs/g/g26.md) toward [allēlōn](../../strongs/g/g240.md), and toward [pas](../../strongs/g/g3956.md), even as we toward you:

<a name="1thessalonians_3_13"></a>1 Thessalonians 3:13

To the end he may [stērizō](../../strongs/g/g4741.md) your [kardia](../../strongs/g/g2588.md) [amemptos](../../strongs/g/g273.md) in [hagiosyne](../../strongs/g/g42.md) before [theos](../../strongs/g/g2316.md), even our [patēr](../../strongs/g/g3962.md), at the [parousia](../../strongs/g/g3952.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) with all his [hagios](../../strongs/g/g40.md).

---

[Transliteral Bible](../bible.md)

[1 Thessalonians](1thessalonians.md)

[1 Thessalonians 2](1thessalonians_2.md) - [1 Thessalonians 4](1thessalonians_4.md)