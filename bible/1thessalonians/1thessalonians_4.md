# [1 Thessalonians 4](https://www.blueletterbible.org/kjv/1th/4/1/s_1115001)

<a name="1thessalonians_4_1"></a>1 Thessalonians 4:1

[loipon](../../strongs/g/g3063.md) then we [erōtaō](../../strongs/g/g2065.md) you, [adelphos](../../strongs/g/g80.md), and [parakaleō](../../strongs/g/g3870.md) by the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), that as ye have [paralambanō](../../strongs/g/g3880.md) of us how ye ought to [peripateō](../../strongs/g/g4043.md) and to [areskō](../../strongs/g/g700.md) [theos](../../strongs/g/g2316.md), so ye would [perisseuō](../../strongs/g/g4052.md) more and more.

<a name="1thessalonians_4_2"></a>1 Thessalonians 4:2

For ye [eidō](../../strongs/g/g1492.md) what [paraggelia](../../strongs/g/g3852.md) we [didōmi](../../strongs/g/g1325.md) you by the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="1thessalonians_4_3"></a>1 Thessalonians 4:3

For this is the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), your [hagiasmos](../../strongs/g/g38.md), that ye should [apechomai](../../strongs/g/g567.md) from [porneia](../../strongs/g/g4202.md):

<a name="1thessalonians_4_4"></a>1 Thessalonians 4:4

That every one of you should [eidō](../../strongs/g/g1492.md) how to [ktaomai](../../strongs/g/g2932.md) his [skeuos](../../strongs/g/g4632.md) in [hagiasmos](../../strongs/g/g38.md) and [timē](../../strongs/g/g5092.md);

<a name="1thessalonians_4_5"></a>1 Thessalonians 4:5

Not in the [pathos](../../strongs/g/g3806.md) of [epithymia](../../strongs/g/g1939.md), even as the [ethnos](../../strongs/g/g1484.md) which [eidō](../../strongs/g/g1492.md) not [theos](../../strongs/g/g2316.md):

<a name="1thessalonians_4_6"></a>1 Thessalonians 4:6

That no [hyperbainō](../../strongs/g/g5233.md) and [pleonekteō](../../strongs/g/g4122.md) his [adelphos](../../strongs/g/g80.md) in [pragma](../../strongs/g/g4229.md): because that the [kyrios](../../strongs/g/g2962.md) is the [ekdikos](../../strongs/g/g1558.md) of all such, as we also have [proepō](../../strongs/g/g4277.md) you and [diamartyromai](../../strongs/g/g1263.md).

<a name="1thessalonians_4_7"></a>1 Thessalonians 4:7

For [theos](../../strongs/g/g2316.md) hath not [kaleō](../../strongs/g/g2564.md) us unto [akatharsia](../../strongs/g/g167.md), but unto [hagiasmos](../../strongs/g/g38.md).

<a name="1thessalonians_4_8"></a>1 Thessalonians 4:8

He [toigaroun](../../strongs/g/g5105.md) that [atheteō](../../strongs/g/g114.md), [atheteō](../../strongs/g/g114.md) not [anthrōpos](../../strongs/g/g444.md), but [theos](../../strongs/g/g2316.md), who hath also [didōmi](../../strongs/g/g1325.md) unto us his [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="1thessalonians_4_9"></a>1 Thessalonians 4:9

But as [peri](../../strongs/g/g4012.md) [philadelphia](../../strongs/g/g5360.md) ye [chreia](../../strongs/g/g5532.md) not that I [graphō](../../strongs/g/g1125.md) unto you: for ye yourselves are [theodidaktos](../../strongs/g/g2312.md) to [agapaō](../../strongs/g/g25.md) [allēlōn](../../strongs/g/g240.md).

<a name="1thessalonians_4_10"></a>1 Thessalonians 4:10

And indeed ye [poieō](../../strongs/g/g4160.md) it toward all the [adelphos](../../strongs/g/g80.md) which are in all [Makedonia](../../strongs/g/g3109.md): but we [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), that ye [perisseuō](../../strongs/g/g4052.md) more and more;

<a name="1thessalonians_4_11"></a>1 Thessalonians 4:11

And that ye [philotimeomai](../../strongs/g/g5389.md) to be [hēsychazō](../../strongs/g/g2270.md), and to [prassō](../../strongs/g/g4238.md) [idios](../../strongs/g/g2398.md), and to [ergazomai](../../strongs/g/g2038.md) with your [idios](../../strongs/g/g2398.md) [cheir](../../strongs/g/g5495.md), as we [paraggellō](../../strongs/g/g3853.md) you;

<a name="1thessalonians_4_12"></a>1 Thessalonians 4:12

That ye may [peripateō](../../strongs/g/g4043.md) [euschēmonōs](../../strongs/g/g2156.md) toward them that are without, and ye may have [chreia](../../strongs/g/g5532.md) of [mēdeis](../../strongs/g/g3367.md).

<a name="1thessalonians_4_13"></a>1 Thessalonians 4:13

But I would not have you to be [agnoeō](../../strongs/g/g50.md), [adelphos](../../strongs/g/g80.md), concerning them which are [koimaō](../../strongs/g/g2837.md), that ye [lypeō](../../strongs/g/g3076.md) not, even as [loipos](../../strongs/g/g3062.md) which have no [elpis](../../strongs/g/g1680.md).

<a name="1thessalonians_4_14"></a>1 Thessalonians 4:14

For if we [pisteuō](../../strongs/g/g4100.md) that [Iēsous](../../strongs/g/g2424.md) [apothnēskō](../../strongs/g/g599.md) and [anistēmi](../../strongs/g/g450.md), even so them also which [koimaō](../../strongs/g/g2837.md) in [Iēsous](../../strongs/g/g2424.md) will [theos](../../strongs/g/g2316.md) [agō](../../strongs/g/g71.md) with him.

<a name="1thessalonians_4_15"></a>1 Thessalonians 4:15

For this we [legō](../../strongs/g/g3004.md) unto you by the [logos](../../strongs/g/g3056.md) of the [kyrios](../../strongs/g/g2962.md), that we which are [zaō](../../strongs/g/g2198.md) [perileipomai](../../strongs/g/g4035.md) unto the [parousia](../../strongs/g/g3952.md) of the [kyrios](../../strongs/g/g2962.md) shall not [phthanō](../../strongs/g/g5348.md) them which are [koimaō](../../strongs/g/g2837.md).

<a name="1thessalonians_4_16"></a>1 Thessalonians 4:16

For the [kyrios](../../strongs/g/g2962.md) himself shall [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md) with a [keleusma](../../strongs/g/g2752.md), with the [phōnē](../../strongs/g/g5456.md) of the [archangelos](../../strongs/g/g743.md), and with the [salpigx](../../strongs/g/g4536.md) of [theos](../../strongs/g/g2316.md): and the [nekros](../../strongs/g/g3498.md) in [Christos](../../strongs/g/g5547.md) shall [anistēmi](../../strongs/g/g450.md) first:

<a name="1thessalonians_4_17"></a>1 Thessalonians 4:17

Then we which are [zaō](../../strongs/g/g2198.md) and [perileipomai](../../strongs/g/g4035.md) shall be [harpazō](../../strongs/g/g726.md) [hama](../../strongs/g/g260.md) with them in the [nephelē](../../strongs/g/g3507.md), to [apantēsis](../../strongs/g/g529.md) the [kyrios](../../strongs/g/g2962.md) in the [aēr](../../strongs/g/g109.md): and so shall we [pantote](../../strongs/g/g3842.md) be with the [kyrios](../../strongs/g/g2962.md).

<a name="1thessalonians_4_18"></a>1 Thessalonians 4:18

Wherefore [parakaleō](../../strongs/g/g3870.md) [allēlōn](../../strongs/g/g240.md) with these [logos](../../strongs/g/g3056.md).

---

[Transliteral Bible](../bible.md)

[1 Thessalonians](1thessalonians.md)

[1 Thessalonians 3](1thessalonians_3.md) - [1 Thessalonians 5](1thessalonians_5.md)