# [1 Thessalonians 5](https://www.blueletterbible.org/kjv/1th/5/1/s_1116001)

<a name="1thessalonians_5_1"></a>1 Thessalonians 5:1

But of the [chronos](../../strongs/g/g5550.md) and the [kairos](../../strongs/g/g2540.md), [adelphos](../../strongs/g/g80.md), ye have no [chreia](../../strongs/g/g5532.md) that I [graphō](../../strongs/g/g1125.md) unto you.

<a name="1thessalonians_5_2"></a>1 Thessalonians 5:2

For yourselves [eidō](../../strongs/g/g1492.md) [akribōs](../../strongs/g/g199.md) that the [hēmera](../../strongs/g/g2250.md) of the [kyrios](../../strongs/g/g2962.md) so [erchomai](../../strongs/g/g2064.md) as a [kleptēs](../../strongs/g/g2812.md) in the [nyx](../../strongs/g/g3571.md).

<a name="1thessalonians_5_3"></a>1 Thessalonians 5:3

For when they shall [legō](../../strongs/g/g3004.md), [eirēnē](../../strongs/g/g1515.md) and [asphaleia](../../strongs/g/g803.md); then [aiphnidios](../../strongs/g/g160.md) [olethros](../../strongs/g/g3639.md) [ephistēmi](../../strongs/g/g2186.md) them, as [ōdin](../../strongs/g/g5604.md) upon a [gastēr](../../strongs/g/g1064.md) [echō](../../strongs/g/g2192.md); and they shall not [ekpheugō](../../strongs/g/g1628.md).

<a name="1thessalonians_5_4"></a>1 Thessalonians 5:4

But ye, [adelphos](../../strongs/g/g80.md), are not in [skotos](../../strongs/g/g4655.md), that that [hēmera](../../strongs/g/g2250.md) should [katalambanō](../../strongs/g/g2638.md) you as a [kleptēs](../../strongs/g/g2812.md).

<a name="1thessalonians_5_5"></a>1 Thessalonians 5:5

Ye are all the [huios](../../strongs/g/g5207.md) of [phōs](../../strongs/g/g5457.md), and the [huios](../../strongs/g/g5207.md) of the [hēmera](../../strongs/g/g2250.md): we are not of the [nyx](../../strongs/g/g3571.md), nor of [skotos](../../strongs/g/g4655.md).

<a name="1thessalonians_5_6"></a>1 Thessalonians 5:6

Therefore let us not [katheudō](../../strongs/g/g2518.md), as do [loipos](../../strongs/g/g3062.md); but let us [grēgoreō](../../strongs/g/g1127.md) and [nēphō](../../strongs/g/g3525.md).

<a name="1thessalonians_5_7"></a>1 Thessalonians 5:7

For they that [katheudō](../../strongs/g/g2518.md) [katheudō](../../strongs/g/g2518.md) in the [nyx](../../strongs/g/g3571.md); and they that [methyskō](../../strongs/g/g3182.md) are [methyō](../../strongs/g/g3184.md) in the [nyx](../../strongs/g/g3571.md).

<a name="1thessalonians_5_8"></a>1 Thessalonians 5:8

But let us, who are of the [hēmera](../../strongs/g/g2250.md), [nēphō](../../strongs/g/g3525.md), [endyō](../../strongs/g/g1746.md) the [thōrax](../../strongs/g/g2382.md) of [pistis](../../strongs/g/g4102.md) and [agapē](../../strongs/g/g26.md); and for a [perikephalaia](../../strongs/g/g4030.md), the [elpis](../../strongs/g/g1680.md) of [sōtēria](../../strongs/g/g4991.md).

<a name="1thessalonians_5_9"></a>1 Thessalonians 5:9

For [theos](../../strongs/g/g2316.md) hath not [tithēmi](../../strongs/g/g5087.md) us to [orgē](../../strongs/g/g3709.md), but to [peripoiēsis](../../strongs/g/g4047.md) [sōtēria](../../strongs/g/g4991.md) by our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md),

<a name="1thessalonians_5_10"></a>1 Thessalonians 5:10

Who [apothnēskō](../../strongs/g/g599.md) for us, that, whether we [grēgoreō](../../strongs/g/g1127.md) or [katheudō](../../strongs/g/g2518.md), we should [zaō](../../strongs/g/g2198.md) [hama](../../strongs/g/g260.md) with him.

<a name="1thessalonians_5_11"></a>1 Thessalonians 5:11

Wherefore [parakaleō](../../strongs/g/g3870.md) [allēlōn](../../strongs/g/g240.md), and [oikodomeō](../../strongs/g/g3618.md) one another, even as also ye [poieō](../../strongs/g/g4160.md).

<a name="1thessalonians_5_12"></a>1 Thessalonians 5:12

And we [erōtaō](../../strongs/g/g2065.md) you, [adelphos](../../strongs/g/g80.md), to [eidō](../../strongs/g/g1492.md) them which [kopiaō](../../strongs/g/g2872.md) among you, and are [proistēmi](../../strongs/g/g4291.md) you in the [kyrios](../../strongs/g/g2962.md), and [noutheteō](../../strongs/g/g3560.md) you;

<a name="1thessalonians_5_13"></a>1 Thessalonians 5:13

And to [hēgeomai](../../strongs/g/g2233.md) them very [perissos](../../strongs/g/g4053.md) in [agapē](../../strongs/g/g26.md) for their [ergon](../../strongs/g/g2041.md). And be at [eirēneuō](../../strongs/g/g1514.md) among yourselves.

<a name="1thessalonians_5_14"></a>1 Thessalonians 5:14

Now we [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), [noutheteō](../../strongs/g/g3560.md) them that are [ataktos](../../strongs/g/g813.md), [paramytheomai](../../strongs/g/g3888.md) the [oligopsychos](../../strongs/g/g3642.md), [antechō](../../strongs/g/g472.md) the [asthenēs](../../strongs/g/g772.md), [makrothymeō](../../strongs/g/g3114.md) toward all.

<a name="1thessalonians_5_15"></a>1 Thessalonians 5:15

[horaō](../../strongs/g/g3708.md) that none [apodidōmi](../../strongs/g/g591.md) [kakos](../../strongs/g/g2556.md) for [kakos](../../strongs/g/g2556.md) unto any man; but [pantote](../../strongs/g/g3842.md) [diōkō](../../strongs/g/g1377.md) that which is [agathos](../../strongs/g/g18.md), both among [allēlōn](../../strongs/g/g240.md), and to all.

<a name="1thessalonians_5_16"></a>1 Thessalonians 5:16

[chairō](../../strongs/g/g5463.md) [pantote](../../strongs/g/g3842.md).

<a name="1thessalonians_5_17"></a>1 Thessalonians 5:17

[proseuchomai](../../strongs/g/g4336.md) [adialeiptos](../../strongs/g/g89.md).

<a name="1thessalonians_5_18"></a>1 Thessalonians 5:18

In every thing [eucharisteō](../../strongs/g/g2168.md): for this is the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) concerning you.

<a name="1thessalonians_5_19"></a>1 Thessalonians 5:19

[sbennymi](../../strongs/g/g4570.md) not the [pneuma](../../strongs/g/g4151.md).

<a name="1thessalonians_5_20"></a>1 Thessalonians 5:20

[exoutheneō](../../strongs/g/g1848.md) not [prophēteia](../../strongs/g/g4394.md).

<a name="1thessalonians_5_21"></a>1 Thessalonians 5:21

[dokimazō](../../strongs/g/g1381.md) all things; [katechō](../../strongs/g/g2722.md) that which is [kalos](../../strongs/g/g2570.md).

<a name="1thessalonians_5_22"></a>1 Thessalonians 5:22

[apechomai](../../strongs/g/g567.md) from all [eidos](../../strongs/g/g1491.md) of [ponēros](../../strongs/g/g4190.md).

<a name="1thessalonians_5_23"></a>1 Thessalonians 5:23

And the very [theos](../../strongs/g/g2316.md) of [eirēnē](../../strongs/g/g1515.md) [hagiazō](../../strongs/g/g37.md) you [holotelēs](../../strongs/g/g3651.md); your [holoklēros](../../strongs/g/g3648.md) [pneuma](../../strongs/g/g4151.md) and [psychē](../../strongs/g/g5590.md) and [sōma](../../strongs/g/g4983.md) be [tēreō](../../strongs/g/g5083.md) [amemptōs](../../strongs/g/g274.md) unto the [parousia](../../strongs/g/g3952.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1thessalonians_5_24"></a>1 Thessalonians 5:24

[pistos](../../strongs/g/g4103.md) is he that [kaleō](../../strongs/g/g2564.md) you, who also will [poieō](../../strongs/g/g4160.md) it.

<a name="1thessalonians_5_25"></a>1 Thessalonians 5:25

[adelphos](../../strongs/g/g80.md), [proseuchomai](../../strongs/g/g4336.md) for us.

<a name="1thessalonians_5_26"></a>1 Thessalonians 5:26

[aspazomai](../../strongs/g/g782.md) all the [adelphos](../../strongs/g/g80.md) with an [hagios](../../strongs/g/g40.md) [philēma](../../strongs/g/g5370.md).

<a name="1thessalonians_5_27"></a>1 Thessalonians 5:27

I [horkizō](../../strongs/g/g3726.md) you by the [kyrios](../../strongs/g/g2962.md) that this [epistolē](../../strongs/g/g1992.md) be [anaginōskō](../../strongs/g/g314.md) unto all the [hagios](../../strongs/g/g40.md) [adelphos](../../strongs/g/g80.md).

<a name="1thessalonians_5_28"></a>1 Thessalonians 5:28

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[1 Thessalonians](1thessalonians.md)

[1 Thessalonians 4](1thessalonians_4.md)