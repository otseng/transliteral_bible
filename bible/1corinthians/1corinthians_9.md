# [1 Corinthians 9](https://www.blueletterbible.org/kjv/1co/9/1/s_1071001)

<a name="1corinthians_9_1"></a>1 Corinthians 9:1

Am I not an [apostolos](../../strongs/g/g652.md)? am I not [eleutheros](../../strongs/g/g1658.md)? have I not [horaō](../../strongs/g/g3708.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md)? are not ye my [ergon](../../strongs/g/g2041.md) in the [kyrios](../../strongs/g/g2962.md)?

<a name="1corinthians_9_2"></a>1 Corinthians 9:2

If I be not an [apostolos](../../strongs/g/g652.md) unto others, yet doubtless I am to you: for the [sphragis](../../strongs/g/g4973.md) of mine [apostolē](../../strongs/g/g651.md) are ye in the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_9_3"></a>1 Corinthians 9:3

Mine [apologia](../../strongs/g/g627.md) to [anakrinō](../../strongs/g/g350.md) me is this,

<a name="1corinthians_9_4"></a>1 Corinthians 9:4

Have we not [exousia](../../strongs/g/g1849.md) to [phago](../../strongs/g/g5315.md) and to [pinō](../../strongs/g/g4095.md)?

<a name="1corinthians_9_5"></a>1 Corinthians 9:5

Have we not [exousia](../../strongs/g/g1849.md) to [periagō](../../strongs/g/g4013.md) an [adelphē](../../strongs/g/g79.md), a [gynē](../../strongs/g/g1135.md), as well as [loipos](../../strongs/g/g3062.md) [apostolos](../../strongs/g/g652.md), and as the [adelphos](../../strongs/g/g80.md) of the [kyrios](../../strongs/g/g2962.md), and [Kēphas](../../strongs/g/g2786.md)?

<a name="1corinthians_9_6"></a>1 Corinthians 9:6

Or I only and [Barnabas](../../strongs/g/g921.md)s, have not we [exousia](../../strongs/g/g1849.md) to [mē](../../strongs/g/g3361.md) [ergazomai](../../strongs/g/g2038.md)?

<a name="1corinthians_9_7"></a>1 Corinthians 9:7

Who [strateuō](../../strongs/g/g4754.md) any time at his own [opsōnion](../../strongs/g/g3800.md)? who [phyteuō](../../strongs/g/g5452.md) an [ampelōn](../../strongs/g/g290.md), and [esthiō](../../strongs/g/g2068.md) not of the [karpos](../../strongs/g/g2590.md) thereof? or who [poimainō](../../strongs/g/g4165.md) a [poimnē](../../strongs/g/g4167.md), and [esthiō](../../strongs/g/g2068.md) not of the [gala](../../strongs/g/g1051.md) of the [poimnē](../../strongs/g/g4167.md)?

<a name="1corinthians_9_8"></a>1 Corinthians 9:8

[laleō](../../strongs/g/g2980.md) I these things as an [anthrōpos](../../strongs/g/g444.md)? or [legō](../../strongs/g/g3004.md) not the [nomos](../../strongs/g/g3551.md) the same also?

<a name="1corinthians_9_9"></a>1 Corinthians 9:9

For it is [graphō](../../strongs/g/g1125.md) in the [nomos](../../strongs/g/g3551.md) of [Mōÿsēs](../../strongs/g/g3475.md), Thou shalt not [phimoō](../../strongs/g/g5392.md) the [bous](../../strongs/g/g1016.md) that [aloaō](../../strongs/g/g248.md). Doth [theos](../../strongs/g/g2316.md) [melei](../../strongs/g/g3199.md) for [bous](../../strongs/g/g1016.md)?

<a name="1corinthians_9_10"></a>1 Corinthians 9:10

Or [legō](../../strongs/g/g3004.md) he [pantōs](../../strongs/g/g3843.md) for our sakes? For our sakes, no doubt, is [graphō](../../strongs/g/g1125.md): that he that [arotriaō](../../strongs/g/g722.md) should [arotriaō](../../strongs/g/g722.md) in [elpis](../../strongs/g/g1680.md); and that he that [aloaō](../../strongs/g/g248.md) in [elpis](../../strongs/g/g1680.md) [opheilō](../../strongs/g/g3784.md) [metechō](../../strongs/g/g3348.md) of his [elpis](../../strongs/g/g1680.md).

<a name="1corinthians_9_11"></a>1 Corinthians 9:11

If we have [speirō](../../strongs/g/g4687.md) unto you [pneumatikos](../../strongs/g/g4152.md), is it [megas](../../strongs/g/g3173.md) if we shall [therizō](../../strongs/g/g2325.md) your [sarkikos](../../strongs/g/g4559.md)?

<a name="1corinthians_9_12"></a>1 Corinthians 9:12

If others be [metechō](../../strongs/g/g3348.md) of this [exousia](../../strongs/g/g1849.md) over you, are not we rather?  Nevertheless we have not [chraomai](../../strongs/g/g5530.md) this [exousia](../../strongs/g/g1849.md); but [stegō](../../strongs/g/g4722.md) all things, lest we [didōmi](../../strongs/g/g1325.md) [egkopē](../../strongs/g/g1464.md) the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_9_13"></a>1 Corinthians 9:13

Do ye not [eidō](../../strongs/g/g1492.md) that they which [ergazomai](../../strongs/g/g2038.md) about [hieros](../../strongs/g/g2413.md) [esthiō](../../strongs/g/g2068.md) of the things of the [hieron](../../strongs/g/g2411.md)? and they which [prosedreuō](../../strongs/g/g4332.md) at the [thysiastērion](../../strongs/g/g2379.md) are [symmerizō](../../strongs/g/g4829.md) with the [thysiastērion](../../strongs/g/g2379.md)?

<a name="1corinthians_9_14"></a>1 Corinthians 9:14

Even so hath the [kyrios](../../strongs/g/g2962.md) [diatassō](../../strongs/g/g1299.md) that they which [kataggellō](../../strongs/g/g2605.md) the [euaggelion](../../strongs/g/g2098.md) should [zaō](../../strongs/g/g2198.md) of the [euaggelion](../../strongs/g/g2098.md).

<a name="1corinthians_9_15"></a>1 Corinthians 9:15

But I have [chraomai](../../strongs/g/g5530.md) none of these things: neither have I [graphō](../../strongs/g/g1125.md) these things, that it should be so [ginomai](../../strongs/g/g1096.md) unto me: for it were better for me to [apothnēskō](../../strongs/g/g599.md) [kalos](../../strongs/g/g2570.md), than that [tis](../../strongs/g/g5100.md) should make my [kauchēma](../../strongs/g/g2745.md) [kenoō](../../strongs/g/g2758.md).

<a name="1corinthians_9_16"></a>1 Corinthians 9:16

For though I [euaggelizō](../../strongs/g/g2097.md), I have nothing to [kauchēma](../../strongs/g/g2745.md): for [anagkē](../../strongs/g/g318.md) is [epikeimai](../../strongs/g/g1945.md) me; yea, [ouai](../../strongs/g/g3759.md) is unto me, if I [euaggelizō](../../strongs/g/g2097.md) not!

<a name="1corinthians_9_17"></a>1 Corinthians 9:17

For if I [prassō](../../strongs/g/g4238.md) this thing [hekōn](../../strongs/g/g1635.md), I have a [misthos](../../strongs/g/g3408.md): but if [akōn](../../strongs/g/g210.md), a [oikonomia](../../strongs/g/g3622.md) of [pisteuō](../../strongs/g/g4100.md).

<a name="1corinthians_9_18"></a>1 Corinthians 9:18

What is my [misthos](../../strongs/g/g3408.md) then? [hina](../../strongs/g/g2443.md), when I [euaggelizō](../../strongs/g/g2097.md), I may [tithēmi](../../strongs/g/g5087.md) the [euaggelion](../../strongs/g/g2098.md) of [Christos](../../strongs/g/g5547.md) [adapanos](../../strongs/g/g77.md), that I [katachraomai](../../strongs/g/g2710.md) not my [exousia](../../strongs/g/g1849.md) in the [euaggelion](../../strongs/g/g2098.md).

<a name="1corinthians_9_19"></a>1 Corinthians 9:19

For though I be [eleutheros](../../strongs/g/g1658.md) from all, yet have I [douloō](../../strongs/g/g1402.md) myself unto all, that I might [kerdainō](../../strongs/g/g2770.md) the more.

<a name="1corinthians_9_20"></a>1 Corinthians 9:20

And unto the [Ioudaios](../../strongs/g/g2453.md) I became as an [Ioudaios](../../strongs/g/g2453.md), that I might [kerdainō](../../strongs/g/g2770.md) the [Ioudaios](../../strongs/g/g2453.md); to them that are under the [nomos](../../strongs/g/g3551.md), as under the [nomos](../../strongs/g/g3551.md), that I might [kerdainō](../../strongs/g/g2770.md) them that are under the [nomos](../../strongs/g/g3551.md); [^1]

<a name="1corinthians_9_21"></a>1 Corinthians 9:21

To [anomos](../../strongs/g/g459.md), as [anomos](../../strongs/g/g459.md), (being not [anomos](../../strongs/g/g459.md) to [theos](../../strongs/g/g2316.md), but [ennomos](../../strongs/g/g1772.md) to [Christos](../../strongs/g/g5547.md),) that I might [kerdainō](../../strongs/g/g2770.md) them that are [anomos](../../strongs/g/g459.md).

<a name="1corinthians_9_22"></a>1 Corinthians 9:22

To the [asthenēs](../../strongs/g/g772.md) became I as [asthenēs](../../strongs/g/g772.md), that I might [kerdainō](../../strongs/g/g2770.md) the [asthenēs](../../strongs/g/g772.md): I am [ginomai](../../strongs/g/g1096.md) all things to all, that I might [pantōs](../../strongs/g/g3843.md) [sōzō](../../strongs/g/g4982.md) [tis](../../strongs/g/g5100.md).

<a name="1corinthians_9_23"></a>1 Corinthians 9:23

And this I [poieō](../../strongs/g/g4160.md) for the [euaggelion](../../strongs/g/g2098.md) sake, that I might be [sygkoinōnos](../../strongs/g/g4791.md) thereof with you.

<a name="1corinthians_9_24"></a>1 Corinthians 9:24

[eidō](../../strongs/g/g1492.md) ye not that they which [trechō](../../strongs/g/g5143.md) in a [stadion](../../strongs/g/g4712.md) [trechō](../../strongs/g/g5143.md) all, but one [lambanō](../../strongs/g/g2983.md) the [brabeion](../../strongs/g/g1017.md)? So [trechō](../../strongs/g/g5143.md), that ye may [katalambanō](../../strongs/g/g2638.md).

<a name="1corinthians_9_25"></a>1 Corinthians 9:25

And [pas](../../strongs/g/g3956.md) that [agōnizomai](../../strongs/g/g75.md) is [egkrateuomai](../../strongs/g/g1467.md) in all things. Now they do it to [lambanō](../../strongs/g/g2983.md) a [phthartos](../../strongs/g/g5349.md) [stephanos](../../strongs/g/g4735.md); but we an [aphthartos](../../strongs/g/g862.md).

<a name="1corinthians_9_26"></a>1 Corinthians 9:26

I therefore so [trechō](../../strongs/g/g5143.md), not as [adēlōs](../../strongs/g/g84.md); so [pykteuō](../../strongs/g/g4438.md), not as one that [derō](../../strongs/g/g1194.md) the [aēr](../../strongs/g/g109.md):

<a name="1corinthians_9_27"></a>1 Corinthians 9:27

But I [hypōpiazō](../../strongs/g/g5299.md) my [sōma](../../strongs/g/g4983.md), and [doulagōgeō](../../strongs/g/g1396.md): lest that by any means, when I have [kēryssō](../../strongs/g/g2784.md) to others, I myself should be [adokimos](../../strongs/g/g96.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 8](1corinthians_8.md) - [1Corinthians 10](1corinthians_10.md)

---

[^1]: [1 Corinthians 9:20 Commentary](../../commentary/1corinthians/1corinthians_9_commentary.md#1corinthians_9_20)
