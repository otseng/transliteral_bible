# [1 Corinthians 6](https://www.blueletterbible.org/kjv/1co/6/1/s_1068001)

<a name="1corinthians_6_1"></a>1 Corinthians 6:1

[tolmaō](../../strongs/g/g5111.md) any of you, having a [pragma](../../strongs/g/g4229.md) against another, [krinō](../../strongs/g/g2919.md) before the [adikos](../../strongs/g/g94.md), and not before the [hagios](../../strongs/g/g40.md)?

<a name="1corinthians_6_2"></a>1 Corinthians 6:2

Do ye not [eidō](../../strongs/g/g1492.md) that the [hagios](../../strongs/g/g40.md) shall [krinō](../../strongs/g/g2919.md) the [kosmos](../../strongs/g/g2889.md)? and if the [kosmos](../../strongs/g/g2889.md) shall be [krinō](../../strongs/g/g2919.md) by you, are ye [anaxios](../../strongs/g/g370.md) to [kritērion](../../strongs/g/g2922.md) the [elachistos](../../strongs/g/g1646.md)?

<a name="1corinthians_6_3"></a>1 Corinthians 6:3

[eidō](../../strongs/g/g1492.md) ye not that we shall [krinō](../../strongs/g/g2919.md) [aggelos](../../strongs/g/g32.md)? [mēti ge](../../strongs/g/g3386.md) [biōtikos](../../strongs/g/g982.md)?

<a name="1corinthians_6_4"></a>1 Corinthians 6:4

If then ye have [kritērion](../../strongs/g/g2922.md) of [biōtikos](../../strongs/g/g982.md), [kathizō](../../strongs/g/g2523.md) them [exoutheneō](../../strongs/g/g1848.md) in the [ekklēsia](../../strongs/g/g1577.md).

<a name="1corinthians_6_5"></a>1 Corinthians 6:5

I [legō](../../strongs/g/g3004.md) to your [entropē](../../strongs/g/g1791.md). Is it so, that there is not a [sophos](../../strongs/g/g4680.md) among you? no, not one that shall be able to [diakrinō](../../strongs/g/g1252.md) between his [adelphos](../../strongs/g/g80.md)?

<a name="1corinthians_6_6"></a>1 Corinthians 6:6

But [adelphos](../../strongs/g/g80.md) [krinō](../../strongs/g/g2919.md) with [adelphos](../../strongs/g/g80.md), and that before the [apistos](../../strongs/g/g571.md).

<a name="1corinthians_6_7"></a>1 Corinthians 6:7

Now therefore there is utterly a [hēttēma](../../strongs/g/g2275.md) among you, because ye go [krima](../../strongs/g/g2917.md) one with another. Why do ye not rather [adikeō](../../strongs/g/g91.md)? why do ye not rather [apostereō](../../strongs/g/g650.md)?

<a name="1corinthians_6_8"></a>1 Corinthians 6:8

[alla](../../strongs/g/g235.md), ye [adikeō](../../strongs/g/g91.md), and [apostereō](../../strongs/g/g650.md), and that your [adelphos](../../strongs/g/g80.md).

<a name="1corinthians_6_9"></a>1 Corinthians 6:9

[eidō](../../strongs/g/g1492.md) ye not that the [adikos](../../strongs/g/g94.md) shall not [klēronomeō](../../strongs/g/g2816.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)?  Be not [planaō](../../strongs/g/g4105.md): neither [pornos](../../strongs/g/g4205.md), nor [eidōlolatrēs](../../strongs/g/g1496.md), nor [moichos](../../strongs/g/g3432.md), nor [malakos](../../strongs/g/g3120.md), nor [arsenokoitēs](../../strongs/g/g733.md),

<a name="1corinthians_6_10"></a>1 Corinthians 6:10

Nor [kleptēs](../../strongs/g/g2812.md), nor [pleonektēs](../../strongs/g/g4123.md), nor [methysos](../../strongs/g/g3183.md), nor [loidoros](../../strongs/g/g3060.md), nor [harpax](../../strongs/g/g727.md), shall [klēronomeō](../../strongs/g/g2816.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_6_11"></a>1 Corinthians 6:11

And such were some of you: but ye are [apolouō](../../strongs/g/g628.md), but ye are [hagiazō](../../strongs/g/g37.md), but ye are [dikaioō](../../strongs/g/g1344.md) in the [onoma](../../strongs/g/g3686.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and by the [pneuma](../../strongs/g/g4151.md) of our [theos](../../strongs/g/g2316.md).

<a name="1corinthians_6_12"></a>1 Corinthians 6:12

All things are [exesti](../../strongs/g/g1832.md) unto me, but all things are not [sympherō](../../strongs/g/g4851.md): all things are [exesti](../../strongs/g/g1832.md) for me, but I will not be [exousiazō](../../strongs/g/g1850.md) of any.

<a name="1corinthians_6_13"></a>1 Corinthians 6:13

[brōma](../../strongs/g/g1033.md) for the [koilia](../../strongs/g/g2836.md), and the [koilia](../../strongs/g/g2836.md) for [brōma](../../strongs/g/g1033.md): but [theos](../../strongs/g/g2316.md) shall [katargeō](../../strongs/g/g2673.md) both it and them. Now the [sōma](../../strongs/g/g4983.md) is not for [porneia](../../strongs/g/g4202.md), but for the [kyrios](../../strongs/g/g2962.md); and the [kyrios](../../strongs/g/g2962.md) for the [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_6_14"></a>1 Corinthians 6:14

And [theos](../../strongs/g/g2316.md) hath both [egeirō](../../strongs/g/g1453.md) the [kyrios](../../strongs/g/g2962.md), and will also [exegeirō](../../strongs/g/g1825.md) us by his own [dynamis](../../strongs/g/g1411.md).

<a name="1corinthians_6_15"></a>1 Corinthians 6:15

[eidō](../../strongs/g/g1492.md) ye not that your [sōma](../../strongs/g/g4983.md) are the [melos](../../strongs/g/g3196.md) of [Christos](../../strongs/g/g5547.md)? shall I then [airō](../../strongs/g/g142.md) the [melos](../../strongs/g/g3196.md) of [Christos](../../strongs/g/g5547.md), and [poieō](../../strongs/g/g4160.md) them the [melos](../../strongs/g/g3196.md) of [pornē](../../strongs/g/g4204.md)? [mē](../../strongs/g/g3361.md) [ginomai](../../strongs/g/g1096.md).

<a name="1corinthians_6_16"></a>1 Corinthians 6:16

What? [eidō](../../strongs/g/g1492.md) ye not that [kollaō](../../strongs/g/g2853.md) to a [pornē](../../strongs/g/g4204.md) is one [sōma](../../strongs/g/g4983.md)?  for two, [phēmi](../../strongs/g/g5346.md) he, shall be one [sarx](../../strongs/g/g4561.md).

<a name="1corinthians_6_17"></a>1 Corinthians 6:17

But he that is [kollaō](../../strongs/g/g2853.md) unto the [kyrios](../../strongs/g/g2962.md) is [heis](../../strongs/g/g1520.md) [pneuma](../../strongs/g/g4151.md).

<a name="1corinthians_6_18"></a>1 Corinthians 6:18

[pheugō](../../strongs/g/g5343.md) [porneia](../../strongs/g/g4202.md). Every [hamartēma](../../strongs/g/g265.md) that an [anthrōpos](../../strongs/g/g444.md) [poieō](../../strongs/g/g4160.md) is [ektos](../../strongs/g/g1622.md) the [sōma](../../strongs/g/g4983.md); but he that [porneuō](../../strongs/g/g4203.md) [hamartanō](../../strongs/g/g264.md) against his own [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_6_19"></a>1 Corinthians 6:19

What? [eidō](../../strongs/g/g1492.md) ye not that your [sōma](../../strongs/g/g4983.md) is the [naos](../../strongs/g/g3485.md) of the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) which is in you, which ye have of [theos](../../strongs/g/g2316.md), and ye are not your own?

<a name="1corinthians_6_20"></a>1 Corinthians 6:20

For ye are [agorazō](../../strongs/g/g59.md) with a [timē](../../strongs/g/g5092.md): therefore [doxazō](../../strongs/g/g1392.md) [theos](../../strongs/g/g2316.md) in your [sōma](../../strongs/g/g4983.md), and in your [pneuma](../../strongs/g/g4151.md), which are [theos](../../strongs/g/g2316.md). [^1]

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 5](1corinthians_5.md) - [1 Corinthians 7](1corinthians_7.md)

---

[^1]: [1 Corinthians 6:20 Commentary](../../commentary/1corinthians/1corinthians_6_commentary.md#1corinthians_6_20)
