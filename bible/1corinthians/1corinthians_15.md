# [1 Corinthians 15](https://www.blueletterbible.org/kjv/1co/15/1/rl1/s_1077001)

<a name="1corinthians_15_1"></a>1 Corinthians 15:1

Moreover, [adelphos](../../strongs/g/g80.md), I [gnōrizō](../../strongs/g/g1107.md) unto you the [euaggelion](../../strongs/g/g2098.md) which I [euaggelizō](../../strongs/g/g2097.md) unto you, which also ye have [paralambanō](../../strongs/g/g3880.md), and wherein ye [histēmi](../../strongs/g/g2476.md);

<a name="1corinthians_15_2"></a>1 Corinthians 15:2

By which also ye are [sōzō](../../strongs/g/g4982.md), if ye [katechō](../../strongs/g/g2722.md) [tis](../../strongs/g/g5101.md) [logos](../../strongs/g/g3056.md) what I [euaggelizō](../../strongs/g/g2097.md) unto you, unless ye have [pisteuō](../../strongs/g/g4100.md) [eikē](../../strongs/g/g1500.md).

<a name="1corinthians_15_3"></a>1 Corinthians 15:3

For I [paradidōmi](../../strongs/g/g3860.md) unto you [en](../../strongs/g/g1722.md) [prōtos](../../strongs/g/g4413.md) that which I also [paralambanō](../../strongs/g/g3880.md), how that [Christos](../../strongs/g/g5547.md) [apothnēskō](../../strongs/g/g599.md) for our [hamartia](../../strongs/g/g266.md) according to the [graphē](../../strongs/g/g1124.md);

<a name="1corinthians_15_4"></a>1 Corinthians 15:4

And that he was [thaptō](../../strongs/g/g2290.md), and that he [egeirō](../../strongs/g/g1453.md) the third [hēmera](../../strongs/g/g2250.md) according to the [graphē](../../strongs/g/g1124.md):

<a name="1corinthians_15_5"></a>1 Corinthians 15:5

And that he was [optanomai](../../strongs/g/g3700.md) of [Kēphas](../../strongs/g/g2786.md), then of the [dōdeka](../../strongs/g/g1427.md):

<a name="1corinthians_15_6"></a>1 Corinthians 15:6

After that, he was [optanomai](../../strongs/g/g3700.md) of above five hundred [adelphos](../../strongs/g/g80.md) at [ephapax](../../strongs/g/g2178.md); of whom the [pleiōn](../../strongs/g/g4119.md) [menō](../../strongs/g/g3306.md) unto [arti](../../strongs/g/g737.md), but some are [koimaō](../../strongs/g/g2837.md).

<a name="1corinthians_15_7"></a>1 Corinthians 15:7

After that, he was [optanomai](../../strongs/g/g3700.md) of [Iakōbos](../../strongs/g/g2385.md); then of all the [apostolos](../../strongs/g/g652.md).

<a name="1corinthians_15_8"></a>1 Corinthians 15:8

And [eschatos](../../strongs/g/g2078.md) of all he was [optanomai](../../strongs/g/g3700.md) of me also, as [ektrōma](../../strongs/g/g1626.md).

<a name="1corinthians_15_9"></a>1 Corinthians 15:9

For I am the [elachistos](../../strongs/g/g1646.md) of the [apostolos](../../strongs/g/g652.md), that am not [hikanos](../../strongs/g/g2425.md) to be [kaleō](../../strongs/g/g2564.md) an [apostolos](../../strongs/g/g652.md), because I [diōkō](../../strongs/g/g1377.md) the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_15_10"></a>1 Corinthians 15:10

But by the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) I am what I am: and his [charis](../../strongs/g/g5485.md) upon me was not [kenos](../../strongs/g/g2756.md); but I [kopiaō](../../strongs/g/g2872.md) [perissoteron](../../strongs/g/g4054.md) than they all: yet not I, but the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) which was with me.

<a name="1corinthians_15_11"></a>1 Corinthians 15:11

Therefore whether I or they, so we [kēryssō](../../strongs/g/g2784.md), and so ye [pisteuō](../../strongs/g/g4100.md).

<a name="1corinthians_15_12"></a>1 Corinthians 15:12

Now if [Christos](../../strongs/g/g5547.md) be [kēryssō](../../strongs/g/g2784.md) that he [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), how [legō](../../strongs/g/g3004.md) some among you that there is no [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md)?

<a name="1corinthians_15_13"></a>1 Corinthians 15:13

But if there be no [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md), then is [Christos](../../strongs/g/g5547.md) not [egeirō](../../strongs/g/g1453.md):

<a name="1corinthians_15_14"></a>1 Corinthians 15:14

And if [Christos](../../strongs/g/g5547.md) be not [egeirō](../../strongs/g/g1453.md), then is our [kērygma](../../strongs/g/g2782.md) [kenos](../../strongs/g/g2756.md), and your [pistis](../../strongs/g/g4102.md) is also [kenos](../../strongs/g/g2756.md).

<a name="1corinthians_15_15"></a>1 Corinthians 15:15

[de](../../strongs/g/g1161.md), and we are [heuriskō](../../strongs/g/g2147.md) [pseudomartys](../../strongs/g/g5575.md) of [theos](../../strongs/g/g2316.md); because we have [martyreō](../../strongs/g/g3140.md) of [theos](../../strongs/g/g2316.md) that he [egeirō](../../strongs/g/g1453.md) [Christos](../../strongs/g/g5547.md): whom he [egeirō](../../strongs/g/g1453.md) not, if so be that the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md) not.

<a name="1corinthians_15_16"></a>1 Corinthians 15:16

For if the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md) not, then is not [Christos](../../strongs/g/g5547.md) [egeirō](../../strongs/g/g1453.md):

<a name="1corinthians_15_17"></a>1 Corinthians 15:17

And if [Christos](../../strongs/g/g5547.md) be not [egeirō](../../strongs/g/g1453.md), your [pistis](../../strongs/g/g4102.md) is [mataios](../../strongs/g/g3152.md); ye are yet in your [hamartia](../../strongs/g/g266.md).

<a name="1corinthians_15_18"></a>1 Corinthians 15:18

Then they also which are [koimaō](../../strongs/g/g2837.md) in [Christos](../../strongs/g/g5547.md) are [apollymi](../../strongs/g/g622.md).

<a name="1corinthians_15_19"></a>1 Corinthians 15:19

If in this [zōē](../../strongs/g/g2222.md) only we have [elpizō](../../strongs/g/g1679.md) in [Christos](../../strongs/g/g5547.md), we are of all [anthrōpos](../../strongs/g/g444.md) most [eleeinos](../../strongs/g/g1652.md).

<a name="1corinthians_15_20"></a>1 Corinthians 15:20

But [nyni](../../strongs/g/g3570.md) is [Christos](../../strongs/g/g5547.md) [egeirō](../../strongs/g/g1453.md) from the [nekros](../../strongs/g/g3498.md), and become the [aparchē](../../strongs/g/g536.md) of [koimaō](../../strongs/g/g2837.md).

<a name="1corinthians_15_21"></a>1 Corinthians 15:21

For since by [anthrōpos](../../strongs/g/g444.md) [thanatos](../../strongs/g/g2288.md), by [anthrōpos](../../strongs/g/g444.md) also the [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md).

<a name="1corinthians_15_22"></a>1 Corinthians 15:22

For as in [Adam](../../strongs/g/g76.md) all [apothnēskō](../../strongs/g/g599.md), even so in [Christos](../../strongs/g/g5547.md) shall all be [zōopoieō](../../strongs/g/g2227.md).

<a name="1corinthians_15_23"></a>1 Corinthians 15:23

But [hekastos](../../strongs/g/g1538.md) in his own [tagma](../../strongs/g/g5001.md): [Christos](../../strongs/g/g5547.md) the [aparchē](../../strongs/g/g536.md); afterward they that are [Christos](../../strongs/g/g5547.md) at his [parousia](../../strongs/g/g3952.md).

<a name="1corinthians_15_24"></a>1 Corinthians 15:24

Then the [telos](../../strongs/g/g5056.md), when he shall have [paradidōmi](../../strongs/g/g3860.md) the [basileia](../../strongs/g/g932.md) to [theos](../../strongs/g/g2316.md), even the [patēr](../../strongs/g/g3962.md); when he shall have [katargeō](../../strongs/g/g2673.md) all [archē](../../strongs/g/g746.md) and all [exousia](../../strongs/g/g1849.md) and [dynamis](../../strongs/g/g1411.md).

<a name="1corinthians_15_25"></a>1 Corinthians 15:25

For he must [basileuō](../../strongs/g/g936.md), till he hath [tithēmi](../../strongs/g/g5087.md) all [echthros](../../strongs/g/g2190.md) under his [pous](../../strongs/g/g4228.md).

<a name="1corinthians_15_26"></a>1 Corinthians 15:26

The [eschatos](../../strongs/g/g2078.md) [echthros](../../strongs/g/g2190.md) that shall be [katargeō](../../strongs/g/g2673.md) is [thanatos](../../strongs/g/g2288.md).

<a name="1corinthians_15_27"></a>1 Corinthians 15:27

For he hath [hypotassō](../../strongs/g/g5293.md) all things under his [pous](../../strongs/g/g4228.md). But when he [eipon](../../strongs/g/g2036.md) all things are [hypotassō](../../strongs/g/g5293.md) him, it is [dēlos](../../strongs/g/g1212.md) that he is [ektos](../../strongs/g/g1622.md), which did [hypotassō](../../strongs/g/g5293.md) all things under him.

<a name="1corinthians_15_28"></a>1 Corinthians 15:28

And when all things shall be [hypotassō](../../strongs/g/g5293.md) unto him, then shall the [huios](../../strongs/g/g5207.md) also himself be [hypotassō](../../strongs/g/g5293.md) unto him that [hypotassō](../../strongs/g/g5293.md) all things under him, that [theos](../../strongs/g/g2316.md) may be all in all.

<a name="1corinthians_15_29"></a>1 Corinthians 15:29

Else what shall they [poieō](../../strongs/g/g4160.md) which are [baptizō](../../strongs/g/g907.md) for the [nekros](../../strongs/g/g3498.md), if the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md) not at all? why are they then [baptizō](../../strongs/g/g907.md) for the [nekros](../../strongs/g/g3498.md)?

<a name="1corinthians_15_30"></a>1 Corinthians 15:30

And why we in [kindyneuō](../../strongs/g/g2793.md) every [hōra](../../strongs/g/g5610.md)?

<a name="1corinthians_15_31"></a>1 Corinthians 15:31

[nē](../../strongs/g/g3513.md) [hēmeteros](../../strongs/g/g2251.md) [kauchēsis](../../strongs/g/g2746.md) which I have in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) our [kyrios](../../strongs/g/g2962.md), I [apothnēskō](../../strongs/g/g599.md) daily.

<a name="1corinthians_15_32"></a>1 Corinthians 15:32

If after the manner of [anthrōpos](../../strongs/g/g444.md) I have [thēriomacheō](../../strongs/g/g2341.md) at [Ephesos](../../strongs/g/g2181.md), what [ophelos](../../strongs/g/g3786.md) me, if the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md) not? let us [phago](../../strongs/g/g5315.md) and [pinō](../../strongs/g/g4095.md); for [aurion](../../strongs/g/g839.md) we [apothnēskō](../../strongs/g/g599.md).

<a name="1corinthians_15_33"></a>1 Corinthians 15:33

Be not [planaō](../../strongs/g/g4105.md): [kakos](../../strongs/g/g2556.md) [homilia](../../strongs/g/g3657.md) [phtheirō](../../strongs/g/g5351.md) [chrēstos](../../strongs/g/g5543.md) [ēthos](../../strongs/g/g2239.md).

<a name="1corinthians_15_34"></a>1 Corinthians 15:34

[eknēphō](../../strongs/g/g1594.md) to [dikaiōs](../../strongs/g/g1346.md), and [hamartanō](../../strongs/g/g264.md) not; for some have [agnōsia](../../strongs/g/g56.md) of [theos](../../strongs/g/g2316.md): I [legō](../../strongs/g/g3004.md) this to your [entropē](../../strongs/g/g1791.md).

<a name="1corinthians_15_35"></a>1 Corinthians 15:35

But some will [eipon](../../strongs/g/g2046.md), How are the [nekros](../../strongs/g/g3498.md) [egeirō](../../strongs/g/g1453.md)? and with what [sōma](../../strongs/g/g4983.md) do they [erchomai](../../strongs/g/g2064.md)?

<a name="1corinthians_15_36"></a>1 Corinthians 15:36

[aphrōn](../../strongs/g/g878.md), that which thou [speirō](../../strongs/g/g4687.md) is not [zōopoieō](../../strongs/g/g2227.md), except it [apothnēskō](../../strongs/g/g599.md):

<a name="1corinthians_15_37"></a>1 Corinthians 15:37

And that which thou [speirō](../../strongs/g/g4687.md), thou [speirō](../../strongs/g/g4687.md) not that [sōma](../../strongs/g/g4983.md) that shall be, but [gymnos](../../strongs/g/g1131.md) [kokkos](../../strongs/g/g2848.md), it may [tygchanō](../../strongs/g/g5177.md) of [sitos](../../strongs/g/g4621.md), or of some [loipos](../../strongs/g/g3062.md):

<a name="1corinthians_15_38"></a>1 Corinthians 15:38

But [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) it a [sōma](../../strongs/g/g4983.md) as it [thelō](../../strongs/g/g2309.md), and to every [sperma](../../strongs/g/g4690.md) his own [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_15_39"></a>1 Corinthians 15:39

All [sarx](../../strongs/g/g4561.md) is not the same [sarx](../../strongs/g/g4561.md): but [sarx](../../strongs/g/g4561.md) of [anthrōpos](../../strongs/g/g444.md), another [sarx](../../strongs/g/g4561.md) of [ktēnos](../../strongs/g/g2934.md), another of [ichthys](../../strongs/g/g2486.md), and another of [ptēnos](../../strongs/g/g4421.md).

<a name="1corinthians_15_40"></a>1 Corinthians 15:40

also [epouranios](../../strongs/g/g2032.md) [sōma](../../strongs/g/g4983.md), and [sōma](../../strongs/g/g4983.md) [epigeios](../../strongs/g/g1919.md): but the [doxa](../../strongs/g/g1391.md) of the [epouranios](../../strongs/g/g2032.md) is one, and of the [epigeios](../../strongs/g/g1919.md) is another.

<a name="1corinthians_15_41"></a>1 Corinthians 15:41

There is one [doxa](../../strongs/g/g1391.md) of the [hēlios](../../strongs/g/g2246.md), and another [doxa](../../strongs/g/g1391.md) of the [selēnē](../../strongs/g/g4582.md), and another [doxa](../../strongs/g/g1391.md) of the [astēr](../../strongs/g/g792.md): for one [astēr](../../strongs/g/g792.md) [diapherō](../../strongs/g/g1308.md) another [astēr](../../strongs/g/g792.md) in [doxa](../../strongs/g/g1391.md).

<a name="1corinthians_15_42"></a>1 Corinthians 15:42

So also the [anastasis](../../strongs/g/g386.md) of the [nekros](../../strongs/g/g3498.md). It is [speirō](../../strongs/g/g4687.md) in [phthora](../../strongs/g/g5356.md); it is [egeirō](../../strongs/g/g1453.md) in [aphtharsia](../../strongs/g/g861.md):

<a name="1corinthians_15_43"></a>1 Corinthians 15:43

It is [speirō](../../strongs/g/g4687.md) in [atimia](../../strongs/g/g819.md); it is [egeirō](../../strongs/g/g1453.md) in [doxa](../../strongs/g/g1391.md): it is [speirō](../../strongs/g/g4687.md) in [astheneia](../../strongs/g/g769.md); it is [egeirō](../../strongs/g/g1453.md) in [dynamis](../../strongs/g/g1411.md):

<a name="1corinthians_15_44"></a>1 Corinthians 15:44

It is [speirō](../../strongs/g/g4687.md) a [psychikos](../../strongs/g/g5591.md) [sōma](../../strongs/g/g4983.md); it is [egeirō](../../strongs/g/g1453.md) a [pneumatikos](../../strongs/g/g4152.md) [sōma](../../strongs/g/g4983.md). There is a [psychikos](../../strongs/g/g5591.md) [sōma](../../strongs/g/g4983.md), and there is a [pneumatikos](../../strongs/g/g4152.md) [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_15_45"></a>1 Corinthians 15:45

And so it is [graphō](../../strongs/g/g1125.md), The [prōtos](../../strongs/g/g4413.md) [anthrōpos](../../strongs/g/g444.md) [Adam](../../strongs/g/g76.md) was [ginomai](../../strongs/g/g1096.md) a [zaō](../../strongs/g/g2198.md) [psychē](../../strongs/g/g5590.md); the [eschatos](../../strongs/g/g2078.md) [Adam](../../strongs/g/g76.md) a [zōopoieō](../../strongs/g/g2227.md) [pneuma](../../strongs/g/g4151.md).

<a name="1corinthians_15_46"></a>1 Corinthians 15:46

Howbeit that was not first which is [pneumatikos](../../strongs/g/g4152.md), but that which is [psychikos](../../strongs/g/g5591.md); and afterward that which is [pneumatikos](../../strongs/g/g4152.md).

<a name="1corinthians_15_47"></a>1 Corinthians 15:47

The [prōtos](../../strongs/g/g4413.md) [anthrōpos](../../strongs/g/g444.md) is of the [gē](../../strongs/g/g1093.md), [choikos](../../strongs/g/g5517.md); the [deuteros](../../strongs/g/g1208.md) [anthrōpos](../../strongs/g/g444.md) is the [kyrios](../../strongs/g/g2962.md) from [ouranos](../../strongs/g/g3772.md).

<a name="1corinthians_15_48"></a>1 Corinthians 15:48

As is the [choikos](../../strongs/g/g5517.md), such are they also that are [choikos](../../strongs/g/g5517.md): and as is the [epouranios](../../strongs/g/g2032.md), such are they also that are [epouranios](../../strongs/g/g2032.md).

<a name="1corinthians_15_49"></a>1 Corinthians 15:49

And as we have [phoreō](../../strongs/g/g5409.md) the [eikōn](../../strongs/g/g1504.md) of the [choikos](../../strongs/g/g5517.md), we shall also [phoreō](../../strongs/g/g5409.md) the [eikōn](../../strongs/g/g1504.md) of the [epouranios](../../strongs/g/g2032.md).

<a name="1corinthians_15_50"></a>1 Corinthians 15:50

Now this I [phēmi](../../strongs/g/g5346.md), [adelphos](../../strongs/g/g80.md), that [sarx](../../strongs/g/g4561.md) and [haima](../../strongs/g/g129.md) cannot [klēronomeō](../../strongs/g/g2816.md) the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md); neither doth [phthora](../../strongs/g/g5356.md) [klēronomeō](../../strongs/g/g2816.md) [aphtharsia](../../strongs/g/g861.md).

<a name="1corinthians_15_51"></a>1 Corinthians 15:51

[idou](../../strongs/g/g2400.md), I [legō](../../strongs/g/g3004.md) you a [mystērion](../../strongs/g/g3466.md); We shall not all [koimaō](../../strongs/g/g2837.md), but we shall all be [allassō](../../strongs/g/g236.md),

<a name="1corinthians_15_52"></a>1 Corinthians 15:52

In [atomos](../../strongs/g/g823.md), in the [rhipē](../../strongs/g/g4493.md) of an [ophthalmos](../../strongs/g/g3788.md), at the [eschatos](../../strongs/g/g2078.md) [salpigx](../../strongs/g/g4536.md): for [salpizō](../../strongs/g/g4537.md), and the [nekros](../../strongs/g/g3498.md) shall be [egeirō](../../strongs/g/g1453.md) [aphthartos](../../strongs/g/g862.md), and we shall be [allassō](../../strongs/g/g236.md).

<a name="1corinthians_15_53"></a>1 Corinthians 15:53

For this [phthartos](../../strongs/g/g5349.md) must [endyō](../../strongs/g/g1746.md) [aphtharsia](../../strongs/g/g861.md), and this [thnētos](../../strongs/g/g2349.md) must [endyō](../../strongs/g/g1746.md) [athanasia](../../strongs/g/g110.md).

<a name="1corinthians_15_54"></a>1 Corinthians 15:54

So when this [phthartos](../../strongs/g/g5349.md) shall have [endyō](../../strongs/g/g1746.md) [aphtharsia](../../strongs/g/g861.md), and this [thnētos](../../strongs/g/g2349.md) shall have [endyō](../../strongs/g/g1746.md) [athanasia](../../strongs/g/g110.md), then shall be [ginomai](../../strongs/g/g1096.md) the [logos](../../strongs/g/g3056.md) that is [graphō](../../strongs/g/g1125.md), [thanatos](../../strongs/g/g2288.md) is [katapinō](../../strongs/g/g2666.md) in [nikos](../../strongs/g/g3534.md).

<a name="1corinthians_15_55"></a>1 Corinthians 15:55

[thanatos](../../strongs/g/g2288.md), where is thy [kentron](../../strongs/g/g2759.md)? [hadēs](../../strongs/g/g86.md), where is thy [nikos](../../strongs/g/g3534.md)?

<a name="1corinthians_15_56"></a>1 Corinthians 15:56

The [kentron](../../strongs/g/g2759.md) of [thanatos](../../strongs/g/g2288.md) is [hamartia](../../strongs/g/g266.md); and the [dynamis](../../strongs/g/g1411.md) of [hamartia](../../strongs/g/g266.md) is the [nomos](../../strongs/g/g3551.md).

<a name="1corinthians_15_57"></a>1 Corinthians 15:57

But [charis](../../strongs/g/g5485.md) be to [theos](../../strongs/g/g2316.md), which [didōmi](../../strongs/g/g1325.md) us the [nikos](../../strongs/g/g3534.md) through our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_15_58"></a>1 Corinthians 15:58

Therefore, my [agapētos](../../strongs/g/g27.md) [adelphos](../../strongs/g/g80.md), be ye [hedraios](../../strongs/g/g1476.md), [ametakinētos](../../strongs/g/g277.md), [pantote](../../strongs/g/g3842.md) [perisseuō](../../strongs/g/g4052.md) in the [ergon](../../strongs/g/g2041.md) of the [kyrios](../../strongs/g/g2962.md), forasmuch as ye [eidō](../../strongs/g/g1492.md) that your [kopos](../../strongs/g/g2873.md) is not in [kenos](../../strongs/g/g2756.md) in the [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1Corinthians 14](1corinthians_14.md) - [1Corinthians 16](1corinthians_16.md)
