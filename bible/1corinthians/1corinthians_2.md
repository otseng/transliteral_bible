# [1 Corinthians 2](https://www.blueletterbible.org/kjv/1co/2/1/s_1064001)

<a name="1corinthians_2_1"></a>1 Corinthians 2:1

And I, [adelphos](../../strongs/g/g80.md), when I [erchomai](../../strongs/g/g2064.md) to you, [erchomai](../../strongs/g/g2064.md) not with [hyperochē](../../strongs/g/g5247.md) [logos](../../strongs/g/g3056.md) or [sophia](../../strongs/g/g4678.md), [kataggellō](../../strongs/g/g2605.md) unto you the [martyrion](../../strongs/g/g3142.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_2_2"></a>1 Corinthians 2:2

For I [krinō](../../strongs/g/g2919.md) not to [eidō](../../strongs/g/g1492.md) any thing among you, save [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and him [stauroō](../../strongs/g/g4717.md).

<a name="1corinthians_2_3"></a>1 Corinthians 2:3

And I was with you in [astheneia](../../strongs/g/g769.md), and in [phobos](../../strongs/g/g5401.md), and in [polys](../../strongs/g/g4183.md) [tromos](../../strongs/g/g5156.md).

<a name="1corinthians_2_4"></a>1 Corinthians 2:4

And my [logos](../../strongs/g/g3056.md) and my [kērygma](../../strongs/g/g2782.md) was not with [peithos](../../strongs/g/g3981.md) [logos](../../strongs/g/g3056.md) of [anthrōpinos](../../strongs/g/g442.md) [sophia](../../strongs/g/g4678.md), but in [apodeixis](../../strongs/g/g585.md) of the [pneuma](../../strongs/g/g4151.md) and of [dynamis](../../strongs/g/g1411.md):

<a name="1corinthians_2_5"></a>1 Corinthians 2:5

That your [pistis](../../strongs/g/g4102.md) should not stand in the [sophia](../../strongs/g/g4678.md) of [anthrōpos](../../strongs/g/g444.md), but in the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_2_6"></a>1 Corinthians 2:6

Howbeit we [laleō](../../strongs/g/g2980.md) [sophia](../../strongs/g/g4678.md) among [teleios](../../strongs/g/g5046.md): yet not the [sophia](../../strongs/g/g4678.md) of this [aiōn](../../strongs/g/g165.md), nor of the [archōn](../../strongs/g/g758.md) of this [aiōn](../../strongs/g/g165.md), [katargeō](../../strongs/g/g2673.md):

<a name="1corinthians_2_7"></a>1 Corinthians 2:7

But we [laleō](../../strongs/g/g2980.md) the [sophia](../../strongs/g/g4678.md) of [theos](../../strongs/g/g2316.md) in a [mystērion](../../strongs/g/g3466.md), the [apokryptō](../../strongs/g/g613.md), which [theos](../../strongs/g/g2316.md) [proorizō](../../strongs/g/g4309.md) before the [aiōn](../../strongs/g/g165.md) unto our [doxa](../../strongs/g/g1391.md):

<a name="1corinthians_2_8"></a>1 Corinthians 2:8

Which none of the [archōn](../../strongs/g/g758.md) of this [aiōn](../../strongs/g/g165.md) [ginōskō](../../strongs/g/g1097.md): for had they [ginōskō](../../strongs/g/g1097.md) it, they would not have [stauroō](../../strongs/g/g4717.md) the [kyrios](../../strongs/g/g2962.md) of [doxa](../../strongs/g/g1391.md).

<a name="1corinthians_2_9"></a>1 Corinthians 2:9

But as it is [graphō](../../strongs/g/g1125.md), [ophthalmos](../../strongs/g/g3788.md) hath not [eidō](../../strongs/g/g1492.md), nor [ous](../../strongs/g/g3775.md) [akouō](../../strongs/g/g191.md), neither have [anabainō](../../strongs/g/g305.md) into the [kardia](../../strongs/g/g2588.md) of [anthrōpos](../../strongs/g/g444.md), the things which [theos](../../strongs/g/g2316.md) hath [hetoimazō](../../strongs/g/g2090.md) for them that [agapaō](../../strongs/g/g25.md) him.

<a name="1corinthians_2_10"></a>1 Corinthians 2:10

But [theos](../../strongs/g/g2316.md) hath [apokalyptō](../../strongs/g/g601.md) unto us by his [pneuma](../../strongs/g/g4151.md): for the [pneuma](../../strongs/g/g4151.md) [eraunaō](../../strongs/g/g2045.md) all things, [kai](../../strongs/g/g2532.md), the [bathos](../../strongs/g/g899.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_2_11"></a>1 Corinthians 2:11

For what [anthrōpos](../../strongs/g/g444.md) [eidō](../../strongs/g/g1492.md) the things of [anthrōpos](../../strongs/g/g444.md), save the [pneuma](../../strongs/g/g4151.md) of [anthrōpos](../../strongs/g/g444.md) which is in him? even so the things of [theos](../../strongs/g/g2316.md) [eidō](../../strongs/g/g1492.md) no man, but the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_2_12"></a>1 Corinthians 2:12

Now we have [lambanō](../../strongs/g/g2983.md), not the [pneuma](../../strongs/g/g4151.md) of the [kosmos](../../strongs/g/g2889.md), but the [pneuma](../../strongs/g/g4151.md) which is of [theos](../../strongs/g/g2316.md); that we might [eidō](../../strongs/g/g1492.md) the [charizomai](../../strongs/g/g5483.md) to us of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_2_13"></a>1 Corinthians 2:13

Which things also we [laleō](../../strongs/g/g2980.md), not in the [logos](../../strongs/g/g3056.md) which [anthrōpinos](../../strongs/g/g442.md) [sophia](../../strongs/g/g4678.md) [didaktos](../../strongs/g/g1318.md), but which the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md) [didaktos](../../strongs/g/g1318.md); [sygkrinō](../../strongs/g/g4793.md) [pneumatikos](../../strongs/g/g4152.md) with [pneumatikos](../../strongs/g/g4152.md).

<a name="1corinthians_2_14"></a>1 Corinthians 2:14

But the [psychikos](../../strongs/g/g5591.md) [anthrōpos](../../strongs/g/g444.md) [dechomai](../../strongs/g/g1209.md) not the things of the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md): for they are [mōria](../../strongs/g/g3472.md) unto him: neither can he [ginōskō](../../strongs/g/g1097.md) them, because they are [pneumatikōs](../../strongs/g/g4153.md) [anakrinō](../../strongs/g/g350.md).

<a name="1corinthians_2_15"></a>1 Corinthians 2:15

But he that is [pneumatikos](../../strongs/g/g4152.md) [anakrinō](../../strongs/g/g350.md) [men](../../strongs/g/g3303.md)  all things, yet he himself is [anakrinō](../../strongs/g/g350.md) of [oudeis](../../strongs/g/g3762.md).

<a name="1corinthians_2_16"></a>1 Corinthians 2:16

For who hath [ginōskō](../../strongs/g/g1097.md) the [nous](../../strongs/g/g3563.md) of the [kyrios](../../strongs/g/g2962.md), that he may [symbibazō](../../strongs/g/g4822.md) him?  But we have the [nous](../../strongs/g/g3563.md) of [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 1](1corinthians_1.md) - [1 Corinthians 3](1corinthians_3.md)