# [1 Corinthians 3](https://www.blueletterbible.org/kjv/1co/3/1/s_1065001)

<a name="1corinthians_3_1"></a>1 Corinthians 3:1

And I, [adelphos](../../strongs/g/g80.md), could not [laleō](../../strongs/g/g2980.md) unto you as unto [pneumatikos](../../strongs/g/g4152.md), but as unto [sarkikos](../../strongs/g/g4559.md), even as unto [nēpios](../../strongs/g/g3516.md) in [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_3_2"></a>1 Corinthians 3:2

I have [potizō](../../strongs/g/g4222.md) you with [gala](../../strongs/g/g1051.md), and not with [brōma](../../strongs/g/g1033.md): for hitherto ye were not [dynamai](../../strongs/g/g1410.md), neither yet now are ye [dynamai](../../strongs/g/g1410.md).

<a name="1corinthians_3_3"></a>1 Corinthians 3:3

For ye are yet [sarkikos](../../strongs/g/g4559.md): for whereas is among you [zēlos](../../strongs/g/g2205.md), and [eris](../../strongs/g/g2054.md), and [dichostasia](../../strongs/g/g1370.md), are ye not [sarkikos](../../strongs/g/g4559.md), and [peripateō](../../strongs/g/g4043.md) as [anthrōpos](../../strongs/g/g444.md)?

<a name="1corinthians_3_4"></a>1 Corinthians 3:4

For while one [legō](../../strongs/g/g3004.md), I am of [Paulos](../../strongs/g/g3972.md); and another, I am of [Apollōs](../../strongs/g/g625.md); are ye not [sarkikos](../../strongs/g/g4559.md)?

<a name="1corinthians_3_5"></a>1 Corinthians 3:5

Who then is [Paulos](../../strongs/g/g3972.md), and who is [Apollōs](../../strongs/g/g625.md), but [diakonos](../../strongs/g/g1249.md) by whom ye [pisteuō](../../strongs/g/g4100.md), even as the [kyrios](../../strongs/g/g2962.md) [didōmi](../../strongs/g/g1325.md) to [hekastos](../../strongs/g/g1538.md)?

<a name="1corinthians_3_6"></a>1 Corinthians 3:6

I have [phyteuō](../../strongs/g/g5452.md), [Apollōs](../../strongs/g/g625.md) [potizō](../../strongs/g/g4222.md); but [theos](../../strongs/g/g2316.md) [auxanō](../../strongs/g/g837.md).

<a name="1corinthians_3_7"></a>1 Corinthians 3:7

So then neither is he that [phyteuō](../../strongs/g/g5452.md) any thing, neither he that [potizō](../../strongs/g/g4222.md); but [theos](../../strongs/g/g2316.md) that [auxanō](../../strongs/g/g837.md).

<a name="1corinthians_3_8"></a>1 Corinthians 3:8

Now he that [phyteuō](../../strongs/g/g5452.md) and he that [potizō](../../strongs/g/g4222.md) are one: and [hekastos](../../strongs/g/g1538.md) shall [lambanō](../../strongs/g/g2983.md) his own [misthos](../../strongs/g/g3408.md) according to his own [kopos](../../strongs/g/g2873.md).

<a name="1corinthians_3_9"></a>1 Corinthians 3:9

For we are [synergos](../../strongs/g/g4904.md) with [theos](../../strongs/g/g2316.md): ye are [theos](../../strongs/g/g2316.md) [geōrgion](../../strongs/g/g1091.md), ye are [theos](../../strongs/g/g2316.md) [oikodomē](../../strongs/g/g3619.md).

<a name="1corinthians_3_10"></a>1 Corinthians 3:10

According to the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) which is [didōmi](../../strongs/g/g1325.md) unto me, as a [sophos](../../strongs/g/g4680.md) [architektōn](../../strongs/g/g753.md), I have [tithēmi](../../strongs/g/g5087.md) the [themelios](../../strongs/g/g2310.md), and another [epoikodomeō](../../strongs/g/g2026.md) thereon. But let [hekastos](../../strongs/g/g1538.md) [blepō](../../strongs/g/g991.md) how he [epoikodomeō](../../strongs/g/g2026.md) thereupon.

<a name="1corinthians_3_11"></a>1 Corinthians 3:11

For other [themelios](../../strongs/g/g2310.md) can [oudeis](../../strongs/g/g3762.md) [tithēmi](../../strongs/g/g5087.md) than that is [keimai](../../strongs/g/g2749.md), which is [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_3_12"></a>1 Corinthians 3:12

Now [ei tis](../../strongs/g/g1536.md) [epoikodomeō](../../strongs/g/g2026.md) upon this [themelios](../../strongs/g/g2310.md) [chrysos](../../strongs/g/g5557.md), [argyros](../../strongs/g/g696.md), [timios](../../strongs/g/g5093.md) [lithos](../../strongs/g/g3037.md), [xylon](../../strongs/g/g3586.md), [chortos](../../strongs/g/g5528.md), [kalamē](../../strongs/g/g2562.md);

<a name="1corinthians_3_13"></a>1 Corinthians 3:13

[hekastos](../../strongs/g/g1538.md) [ergon](../../strongs/g/g2041.md) shall be made [phaneros](../../strongs/g/g5318.md): for the [hēmera](../../strongs/g/g2250.md) shall [dēloō](../../strongs/g/g1213.md) it, because it shall be [apokalyptō](../../strongs/g/g601.md) by [pyr](../../strongs/g/g4442.md); and the [pyr](../../strongs/g/g4442.md) shall [dokimazō](../../strongs/g/g1381.md) [hekastos](../../strongs/g/g1538.md) [ergon](../../strongs/g/g2041.md) of what [hopoios](../../strongs/g/g3697.md) it is.

<a name="1corinthians_3_14"></a>1 Corinthians 3:14

[ei tis](../../strongs/g/g1536.md) [ergon](../../strongs/g/g2041.md) [menō](../../strongs/g/g3306.md) which he hath [epoikodomeō](../../strongs/g/g2026.md) thereupon, he shall [lambanō](../../strongs/g/g2983.md) a [misthos](../../strongs/g/g3408.md).

<a name="1corinthians_3_15"></a>1 Corinthians 3:15

[ei tis](../../strongs/g/g1536.md) [ergon](../../strongs/g/g2041.md) shall be [katakaiō](../../strongs/g/g2618.md), he shall [zēmioō](../../strongs/g/g2210.md): but he himself shall be [sōzō](../../strongs/g/g4982.md); yet so as by [pyr](../../strongs/g/g4442.md).

<a name="1corinthians_3_16"></a>1 Corinthians 3:16

[eidō](../../strongs/g/g1492.md) ye not that ye are the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md), and that the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [oikeō](../../strongs/g/g3611.md) in you?

<a name="1corinthians_3_17"></a>1 Corinthians 3:17

[ei tis](../../strongs/g/g1536.md) [phtheirō](../../strongs/g/g5351.md) the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md), him shall [theos](../../strongs/g/g2316.md) [phtheirō](../../strongs/g/g5351.md); for the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md) is [hagios](../../strongs/g/g40.md), which ye are.

<a name="1corinthians_3_18"></a>1 Corinthians 3:18

Let [mēdeis](../../strongs/g/g3367.md) [exapataō](../../strongs/g/g1818.md) himself. [ei tis](../../strongs/g/g1536.md) among you [dokeō](../../strongs/g/g1380.md) to be [sophos](../../strongs/g/g4680.md) in this [aiōn](../../strongs/g/g165.md), let him become a [mōros](../../strongs/g/g3474.md), that he may be [sophos](../../strongs/g/g4680.md).

<a name="1corinthians_3_19"></a>1 Corinthians 3:19

For the [sophia](../../strongs/g/g4678.md) of this [kosmos](../../strongs/g/g2889.md) is [mōria](../../strongs/g/g3472.md) with [theos](../../strongs/g/g2316.md). For it is [graphō](../../strongs/g/g1125.md), He [drassomai](../../strongs/g/g1405.md) the [sophos](../../strongs/g/g4680.md) in their own [panourgia](../../strongs/g/g3834.md).

<a name="1corinthians_3_20"></a>1 Corinthians 3:20

And again, The [kyrios](../../strongs/g/g2962.md) [ginōskō](../../strongs/g/g1097.md) the [dialogismos](../../strongs/g/g1261.md) of the [sophos](../../strongs/g/g4680.md), that they are [mataios](../../strongs/g/g3152.md).

<a name="1corinthians_3_21"></a>1 Corinthians 3:21

Therefore let [mēdeis](../../strongs/g/g3367.md) [kauchaomai](../../strongs/g/g2744.md) in [anthrōpos](../../strongs/g/g444.md). For all things are your's;

<a name="1corinthians_3_22"></a>1 Corinthians 3:22

Whether [Paulos](../../strongs/g/g3972.md), or [Apollōs](../../strongs/g/g625.md), or [Kēphas](../../strongs/g/g2786.md), or the [kosmos](../../strongs/g/g2889.md), or [zōē](../../strongs/g/g2222.md), or [thanatos](../../strongs/g/g2288.md), or [enistēmi](../../strongs/g/g1764.md), or [mellō](../../strongs/g/g3195.md); all are your's;

<a name="1corinthians_3_23"></a>1 Corinthians 3:23

And ye are [Christos](../../strongs/g/g5547.md); and [Christos](../../strongs/g/g5547.md) is [theos](../../strongs/g/g2316.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 2](1corinthians_2.md) - [1 Corinthians 4](1corinthians_4.md)