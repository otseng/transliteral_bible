# [1 Corinthians 10](https://www.blueletterbible.org/kjv/1co/10/1/s_1072001)

<a name="1corinthians_10_1"></a>1 Corinthians 10:1

Moreover, [adelphos](../../strongs/g/g80.md), I would not that ye should be [agnoeō](../../strongs/g/g50.md), how that all our [patēr](../../strongs/g/g3962.md) were under the [nephelē](../../strongs/g/g3507.md), and all [dierchomai](../../strongs/g/g1330.md) through the [thalassa](../../strongs/g/g2281.md);

<a name="1corinthians_10_2"></a>1 Corinthians 10:2

And were all [baptizō](../../strongs/g/g907.md) unto [Mōÿsēs](../../strongs/g/g3475.md) in the [nephelē](../../strongs/g/g3507.md) and in the [thalassa](../../strongs/g/g2281.md);

<a name="1corinthians_10_3"></a>1 Corinthians 10:3

And did all [phago](../../strongs/g/g5315.md) the same [pneumatikos](../../strongs/g/g4152.md) [brōma](../../strongs/g/g1033.md);

<a name="1corinthians_10_4"></a>1 Corinthians 10:4

And did all [pinō](../../strongs/g/g4095.md) the same [pneumatikos](../../strongs/g/g4152.md) [poma](../../strongs/g/g4188.md): for they [pinō](../../strongs/g/g4095.md) of that [pneumatikos](../../strongs/g/g4152.md) [petra](../../strongs/g/g4073.md) that [akoloutheō](../../strongs/g/g190.md) them: and that [petra](../../strongs/g/g4073.md) was [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_10_5"></a>1 Corinthians 10:5

But with many of them [theos](../../strongs/g/g2316.md) was not [eudokeō](../../strongs/g/g2106.md): for they were [katastrōnnymi](../../strongs/g/g2693.md) in the [erēmos](../../strongs/g/g2048.md).

<a name="1corinthians_10_6"></a>1 Corinthians 10:6

Now these things were our [typos](../../strongs/g/g5179.md), to the intent we should not [kakos](../../strongs/g/g2556.md) [epithymētēs](../../strongs/g/g1938.md), as they also [epithymeō](../../strongs/g/g1937.md).

<a name="1corinthians_10_7"></a>1 Corinthians 10:7

Neither be ye [eidōlolatrēs](../../strongs/g/g1496.md), as were some of them; as it is [graphō](../../strongs/g/g1125.md), The [laos](../../strongs/g/g2992.md) [kathizō](../../strongs/g/g2523.md) to [phago](../../strongs/g/g5315.md) and [pinō](../../strongs/g/g4095.md), and [anistēmi](../../strongs/g/g450.md) to [paizō](../../strongs/g/g3815.md).

<a name="1corinthians_10_8"></a>1 Corinthians 10:8

Neither let us [porneuō](../../strongs/g/g4203.md), as some of them [porneuō](../../strongs/g/g4203.md), and [piptō](../../strongs/g/g4098.md) in one [hēmera](../../strongs/g/g2250.md) three and twenty thousand.

<a name="1corinthians_10_9"></a>1 Corinthians 10:9

Neither let us [ekpeirazō](../../strongs/g/g1598.md) [Christos](../../strongs/g/g5547.md), as some of them also [peirazō](../../strongs/g/g3985.md), and were [apollymi](../../strongs/g/g622.md) of [ophis](../../strongs/g/g3789.md).

<a name="1corinthians_10_10"></a>1 Corinthians 10:10

Neither [goggyzō](../../strongs/g/g1111.md) ye, as some of them also [goggyzō](../../strongs/g/g1111.md), and were [apollymi](../../strongs/g/g622.md) of the [olothreutēs](../../strongs/g/g3644.md).

<a name="1corinthians_10_11"></a>1 Corinthians 10:11

Now all these things [symbainō](../../strongs/g/g4819.md) unto them for [typos](../../strongs/g/g5179.md): and they are [graphō](../../strongs/g/g1125.md) for our [nouthesia](../../strongs/g/g3559.md), upon whom the [telos](../../strongs/g/g5056.md) of the [aiōn](../../strongs/g/g165.md) [katantaō](../../strongs/g/g2658.md).

<a name="1corinthians_10_12"></a>1 Corinthians 10:12

Wherefore let him that [dokeō](../../strongs/g/g1380.md) he [histēmi](../../strongs/g/g2476.md) [blepō](../../strongs/g/g991.md) lest he [piptō](../../strongs/g/g4098.md).

<a name="1corinthians_10_13"></a>1 Corinthians 10:13

There hath no [peirasmos](../../strongs/g/g3986.md) [lambanō](../../strongs/g/g2983.md) you but such as is [anthrōpinos](../../strongs/g/g442.md): but [theos](../../strongs/g/g2316.md) [pistos](../../strongs/g/g4103.md), who will not [eaō](../../strongs/g/g1439.md) you to be [peirazō](../../strongs/g/g3985.md) above that ye are able; but will with the [peirasmos](../../strongs/g/g3986.md) also [poieō](../../strongs/g/g4160.md) an [ekbasis](../../strongs/g/g1545.md), that ye may be able to [hypopherō](../../strongs/g/g5297.md).

<a name="1corinthians_10_14"></a>1 Corinthians 10:14

Wherefore, my [agapētos](../../strongs/g/g27.md), [pheugō](../../strongs/g/g5343.md) from [eidōlolatria](../../strongs/g/g1495.md).

<a name="1corinthians_10_15"></a>1 Corinthians 10:15

I [legō](../../strongs/g/g3004.md) as to [phronimos](../../strongs/g/g5429.md); [krinō](../../strongs/g/g2919.md) ye what I [phēmi](../../strongs/g/g5346.md).

<a name="1corinthians_10_16"></a>1 Corinthians 10:16

The [potērion](../../strongs/g/g4221.md) of [eulogia](../../strongs/g/g2129.md) which we [eulogeō](../../strongs/g/g2127.md), is it not the [koinōnia](../../strongs/g/g2842.md) of the [haima](../../strongs/g/g129.md) of [Christos](../../strongs/g/g5547.md)? The [artos](../../strongs/g/g740.md) which we [klaō](../../strongs/g/g2806.md), is it not the [koinōnia](../../strongs/g/g2842.md) of the [sōma](../../strongs/g/g4983.md) of [Christos](../../strongs/g/g5547.md)?

<a name="1corinthians_10_17"></a>1 Corinthians 10:17

For we [polys](../../strongs/g/g4183.md) are one [artos](../../strongs/g/g740.md), and one [sōma](../../strongs/g/g4983.md): for we are all [metechō](../../strongs/g/g3348.md) of that one [artos](../../strongs/g/g740.md).

<a name="1corinthians_10_18"></a>1 Corinthians 10:18

[blepō](../../strongs/g/g991.md) [Israēl](../../strongs/g/g2474.md) after the [sarx](../../strongs/g/g4561.md): are not they which [esthiō](../../strongs/g/g2068.md) of the [thysia](../../strongs/g/g2378.md) [koinōnos](../../strongs/g/g2844.md) of the [thysiastērion](../../strongs/g/g2379.md)?

<a name="1corinthians_10_19"></a>1 Corinthians 10:19

What [phēmi](../../strongs/g/g5346.md) I then? that the [eidōlon](../../strongs/g/g1497.md) is any thing, or [eidōlothytos](../../strongs/g/g1494.md) is any thing?

<a name="1corinthians_10_20"></a>1 Corinthians 10:20

But, that the things which the [ethnos](../../strongs/g/g1484.md) [thyō](../../strongs/g/g2380.md), they [thyō](../../strongs/g/g2380.md) to [daimonion](../../strongs/g/g1140.md), and not to [theos](../../strongs/g/g2316.md): and I would not that ye should have [koinōnos](../../strongs/g/g2844.md) with [daimonion](../../strongs/g/g1140.md).

<a name="1corinthians_10_21"></a>1 Corinthians 10:21

Ye cannot [pinō](../../strongs/g/g4095.md) the [potērion](../../strongs/g/g4221.md) of the [kyrios](../../strongs/g/g2962.md), and the [potērion](../../strongs/g/g4221.md) of [daimonion](../../strongs/g/g1140.md): ye cannot be [metechō](../../strongs/g/g3348.md) of the [kyrios](../../strongs/g/g2962.md) [trapeza](../../strongs/g/g5132.md), and of the [trapeza](../../strongs/g/g5132.md) of [daimonion](../../strongs/g/g1140.md).

<a name="1corinthians_10_22"></a>1 Corinthians 10:22

Do we [parazēloō](../../strongs/g/g3863.md) the [kyrios](../../strongs/g/g2962.md)? are we [ischyros](../../strongs/g/g2478.md) than he?

<a name="1corinthians_10_23"></a>1 Corinthians 10:23

All things are [exesti](../../strongs/g/g1832.md) for me, but all things are not [sympherō](../../strongs/g/g4851.md): all things are [exesti](../../strongs/g/g1832.md) for me, but all things [oikodomeō](../../strongs/g/g3618.md) not.

<a name="1corinthians_10_24"></a>1 Corinthians 10:24

Let [mēdeis](../../strongs/g/g3367.md) [zēteō](../../strongs/g/g2212.md) his own, but [hekastos](../../strongs/g/g1538.md) another's.

<a name="1corinthians_10_25"></a>1 Corinthians 10:25

Whatsoever is [pōleō](../../strongs/g/g4453.md) in the [makellon](../../strongs/g/g3111.md), that [esthiō](../../strongs/g/g2068.md), [anakrinō](../../strongs/g/g350.md) [mēdeis](../../strongs/g/g3367.md) for [syneidēsis](../../strongs/g/g4893.md) sake:

<a name="1corinthians_10_26"></a>1 Corinthians 10:26

For the [gē](../../strongs/g/g1093.md) is the [kyrios](../../strongs/g/g2962.md), and the [plērōma](../../strongs/g/g4138.md) thereof.

<a name="1corinthians_10_27"></a>1 Corinthians 10:27

If any of them that [apistos](../../strongs/g/g571.md) [kaleō](../../strongs/g/g2564.md) you, and ye be [thelō](../../strongs/g/g2309.md) to [poreuō](../../strongs/g/g4198.md); whatsoever is [paratithēmi](../../strongs/g/g3908.md) you, [esthiō](../../strongs/g/g2068.md), [anakrinō](../../strongs/g/g350.md) [mēdeis](../../strongs/g/g3367.md) for [syneidēsis](../../strongs/g/g4893.md) sake.

<a name="1corinthians_10_28"></a>1 Corinthians 10:28

But if [tis](../../strongs/g/g5100.md) [eipon](../../strongs/g/g2036.md) unto you, This is [eidōlothytos](../../strongs/g/g1494.md), [esthiō](../../strongs/g/g2068.md) not for his sake that [mēnyō](../../strongs/g/g3377.md) it, and for [syneidēsis](../../strongs/g/g4893.md): for the [gē](../../strongs/g/g1093.md) is the [kyrios](../../strongs/g/g2962.md), and the [plērōma](../../strongs/g/g4138.md) thereof:

<a name="1corinthians_10_29"></a>1 Corinthians 10:29

[syneidēsis](../../strongs/g/g4893.md), I [legō](../../strongs/g/g3004.md), not thine own, but of the other: for why is my [eleutheria](../../strongs/g/g1657.md) [krinō](../../strongs/g/g2919.md) of another [syneidēsis](../../strongs/g/g4893.md)?

<a name="1corinthians_10_30"></a>1 Corinthians 10:30

For if I by [charis](../../strongs/g/g5485.md) [metechō](../../strongs/g/g3348.md), why am I [blasphēmeō](../../strongs/g/g987.md) for that for which I [eucharisteō](../../strongs/g/g2168.md)?

<a name="1corinthians_10_31"></a>1 Corinthians 10:31

Whether therefore ye [esthiō](../../strongs/g/g2068.md), or [pinō](../../strongs/g/g4095.md), or whatsoever ye [poieō](../../strongs/g/g4160.md), do all to the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_10_32"></a>1 Corinthians 10:32

[ginomai](../../strongs/g/g1096.md) [aproskopos](../../strongs/g/g677.md), neither to the [Ioudaios](../../strongs/g/g2453.md), nor to the [Hellēn](../../strongs/g/g1672.md), nor to the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md):

<a name="1corinthians_10_33"></a>1 Corinthians 10:33

Even as I [areskō](../../strongs/g/g700.md) all in all, not [zēteō](../../strongs/g/g2212.md) mine own [sympherō](../../strongs/g/g4851.md), but of [polys](../../strongs/g/g4183.md), that they may be [sōzō](../../strongs/g/g4982.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 9](1corinthians_9.md) - [1 Corinthians 11](1corinthians_11.md)