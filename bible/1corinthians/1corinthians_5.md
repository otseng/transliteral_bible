# [1 Corinthians 5](https://www.blueletterbible.org/kjv/1co/5/1/s_1067001)

<a name="1corinthians_5_1"></a>1 Corinthians 5:1

It is [akouō](../../strongs/g/g191.md) [holōs](../../strongs/g/g3654.md) [porneia](../../strongs/g/g4202.md) among you, and such [porneia](../../strongs/g/g4202.md) as is not so much as [onomazō](../../strongs/g/g3687.md) among the [ethnos](../../strongs/g/g1484.md), that one should have his [patēr](../../strongs/g/g3962.md) [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_5_2"></a>1 Corinthians 5:2

And ye are [physioō](../../strongs/g/g5448.md), and have not rather [pentheō](../../strongs/g/g3996.md), that he that hath [poieō](../../strongs/g/g4160.md) this [ergon](../../strongs/g/g2041.md) might be [exairō](../../strongs/g/g1808.md) from among you.

<a name="1corinthians_5_3"></a>1 Corinthians 5:3

For I [men](../../strongs/g/g3303.md), as [apeimi](../../strongs/g/g548.md) in [sōma](../../strongs/g/g4983.md), but [pareimi](../../strongs/g/g3918.md) in [pneuma](../../strongs/g/g4151.md), have [krinō](../../strongs/g/g2919.md) already, as though I were [pareimi](../../strongs/g/g3918.md), concerning him that hath so [katergazomai](../../strongs/g/g2716.md) this deed,

<a name="1corinthians_5_4"></a>1 Corinthians 5:4

In the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), when ye are [synagō](../../strongs/g/g4863.md), and my [pneuma](../../strongs/g/g4151.md), with the [dynamis](../../strongs/g/g1411.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md),

<a name="1corinthians_5_5"></a>1 Corinthians 5:5

To [paradidōmi](../../strongs/g/g3860.md) such an one unto [Satanas](../../strongs/g/g4567.md) for the [olethros](../../strongs/g/g3639.md) of the [sarx](../../strongs/g/g4561.md), that the [pneuma](../../strongs/g/g4151.md) may be [sōzō](../../strongs/g/g4982.md) in the [hēmera](../../strongs/g/g2250.md) of the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="1corinthians_5_6"></a>1 Corinthians 5:6

Your [kauchēma](../../strongs/g/g2745.md) is not [kalos](../../strongs/g/g2570.md). [eidō](../../strongs/g/g1492.md) ye not that a [mikros](../../strongs/g/g3398.md) [zymē](../../strongs/g/g2219.md) [zymoō](../../strongs/g/g2220.md) the whole [phyrama](../../strongs/g/g5445.md)?

<a name="1corinthians_5_7"></a>1 Corinthians 5:7

[ekkathairō](../../strongs/g/g1571.md) therefore the [palaios](../../strongs/g/g3820.md) [zymē](../../strongs/g/g2219.md), that ye may be a [neos](../../strongs/g/g3501.md) [phyrama](../../strongs/g/g5445.md), as ye are [azymos](../../strongs/g/g106.md). For even [Christos](../../strongs/g/g5547.md) our [pascha](../../strongs/g/g3957.md) is [thyō](../../strongs/g/g2380.md) for us:

<a name="1corinthians_5_8"></a>1 Corinthians 5:8

Therefore let us [heortazō](../../strongs/g/g1858.md), not with [palaios](../../strongs/g/g3820.md) [zymē](../../strongs/g/g2219.md), neither with the [zymē](../../strongs/g/g2219.md) of [kakia](../../strongs/g/g2549.md) and [ponēria](../../strongs/g/g4189.md); but with the [azymos](../../strongs/g/g106.md) of [eilikrineia](../../strongs/g/g1505.md) and [alētheia](../../strongs/g/g225.md).

<a name="1corinthians_5_9"></a>1 Corinthians 5:9

I [graphō](../../strongs/g/g1125.md) unto you in an [epistolē](../../strongs/g/g1992.md) not to [synanamignymi](../../strongs/g/g4874.md) with [pornos](../../strongs/g/g4205.md):

<a name="1corinthians_5_10"></a>1 Corinthians 5:10

Yet not [pantōs](../../strongs/g/g3843.md) with the [pornos](../../strongs/g/g4205.md) of this [tebel](../../strongs/h/h8398.md), or with the [pleonektēs](../../strongs/g/g4123.md), or [harpax](../../strongs/g/g727.md), or with [eidōlolatrēs](../../strongs/g/g1496.md); for then must ye [opheilō](../../strongs/g/g3784.md) [exerchomai](../../strongs/g/g1831.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="1corinthians_5_11"></a>1 Corinthians 5:11

But [nyni](../../strongs/g/g3570.md) I have [graphō](../../strongs/g/g1125.md) unto you not to [synanamignymi](../../strongs/g/g4874.md), if any man that is [onomazō](../../strongs/g/g3687.md) an [adelphos](../../strongs/g/g80.md) be a [pornos](../../strongs/g/g4205.md), or [pleonektēs](../../strongs/g/g4123.md), or an [eidōlolatrēs](../../strongs/g/g1496.md), or a [loidoros](../../strongs/g/g3060.md), or a [methysos](../../strongs/g/g3183.md), or an [harpax](../../strongs/g/g727.md); with such an one no not to [synesthiō](../../strongs/g/g4906.md).

<a name="1corinthians_5_12"></a>1 Corinthians 5:12

For what have I to do to [krinō](../../strongs/g/g2919.md) them also that are without? do not ye [krinō](../../strongs/g/g2919.md) them that are within?

<a name="1corinthians_5_13"></a>1 Corinthians 5:13

But them that are without [theos](../../strongs/g/g2316.md) [krinō](../../strongs/g/g2919.md). Therefore [exairō](../../strongs/g/g1808.md) from among yourselves that [ponēros](../../strongs/g/g4190.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 4](1corinthians_4.md) - [1 Corinthians 6](1corinthians_6.md)