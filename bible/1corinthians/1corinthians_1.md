# [1 Corinthians 1](https://www.blueletterbible.org/kjv/1co/1/1/s_1063001)

<a name="1corinthians_1_1"></a>1 Corinthians 1:1

[Paulos](../../strongs/g/g3972.md) [klētos](../../strongs/g/g2822.md) an [apostolos](../../strongs/g/g652.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) through the [thelēma](../../strongs/g/g2307.md) of [theos](../../strongs/g/g2316.md), and [Sōsthenēs](../../strongs/g/g4988.md) [adelphos](../../strongs/g/g80.md),

<a name="1corinthians_1_2"></a>1 Corinthians 1:2

Unto the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md) which is at [Korinthos](../../strongs/g/g2882.md), to them that are [hagiazō](../../strongs/g/g37.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), [klētos](../../strongs/g/g2822.md) to be [hagios](../../strongs/g/g40.md), with all that in every [topos](../../strongs/g/g5117.md) [epikaleō](../../strongs/g/g1941.md) the [onoma](../../strongs/g/g3686.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md), both their's and our's:

<a name="1corinthians_1_3"></a>1 Corinthians 1:3

[charis](../../strongs/g/g5485.md) be unto you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md), and from the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_1_4"></a>1 Corinthians 1:4

I [eucharisteō](../../strongs/g/g2168.md) my [theos](../../strongs/g/g2316.md) [pantote](../../strongs/g/g3842.md) on your behalf, for the [charis](../../strongs/g/g5485.md) of [theos](../../strongs/g/g2316.md) which is [didōmi](../../strongs/g/g1325.md) you by [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md);

<a name="1corinthians_1_5"></a>1 Corinthians 1:5

That in every thing ye are [ploutizō](../../strongs/g/g4148.md) by him, in all [logos](../../strongs/g/g3056.md), and in all [gnōsis](../../strongs/g/g1108.md);

<a name="1corinthians_1_6"></a>1 Corinthians 1:6

Even as the [martyrion](../../strongs/g/g3142.md) of [Christos](../../strongs/g/g5547.md) was [bebaioō](../../strongs/g/g950.md) in you:

<a name="1corinthians_1_7"></a>1 Corinthians 1:7

So that ye [hystereō](../../strongs/g/g5302.md) in [mēdeis](../../strongs/g/g3367.md) [charisma](../../strongs/g/g5486.md); [apekdechomai](../../strongs/g/g553.md) for the [apokalypsis](../../strongs/g/g602.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md):

<a name="1corinthians_1_8"></a>1 Corinthians 1:8

Who shall also [bebaioō](../../strongs/g/g950.md) you unto the [telos](../../strongs/g/g5056.md), that ye may be [anegklētos](../../strongs/g/g410.md) in the [hēmera](../../strongs/g/g2250.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_1_9"></a>1 Corinthians 1:9

[theos](../../strongs/g/g2316.md) [pistos](../../strongs/g/g4103.md), by whom ye were [kaleō](../../strongs/g/g2564.md) unto the [koinōnia](../../strongs/g/g2842.md) of his [huios](../../strongs/g/g5207.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) our [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_1_10"></a>1 Corinthians 1:10

Now I [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), by the [onoma](../../strongs/g/g3686.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), that ye all [legō](../../strongs/g/g3004.md) the same thing, and that there be no [schisma](../../strongs/g/g4978.md) among you; but that ye be [katartizō](../../strongs/g/g2675.md) in the same [nous](../../strongs/g/g3563.md) and in the same [gnōmē](../../strongs/g/g1106.md).

<a name="1corinthians_1_11"></a>1 Corinthians 1:11

For it hath been [dēloō](../../strongs/g/g1213.md) unto me of you, my [adelphos](../../strongs/g/g80.md), by them of [chloē](../../strongs/g/g5514.md), that there are [eris](../../strongs/g/g2054.md) among you.

<a name="1corinthians_1_12"></a>1 Corinthians 1:12

Now this I [legō](../../strongs/g/g3004.md), that every one of you [legō](../../strongs/g/g3004.md), I am of [Paulos](../../strongs/g/g3972.md); and I of [Apollōs](../../strongs/g/g625.md); and I of [Kēphas](../../strongs/g/g2786.md); and I of [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_1_13"></a>1 Corinthians 1:13

Is [Christos](../../strongs/g/g5547.md) [merizō](../../strongs/g/g3307.md)? was [Paulos](../../strongs/g/g3972.md) [stauroō](../../strongs/g/g4717.md) for you? or were ye [baptizō](../../strongs/g/g907.md) in the [onoma](../../strongs/g/g3686.md) of [Paulos](../../strongs/g/g3972.md)?

<a name="1corinthians_1_14"></a>1 Corinthians 1:14

I [eucharisteō](../../strongs/g/g2168.md) [theos](../../strongs/g/g2316.md) that I [baptizō](../../strongs/g/g907.md) none of you, but [Krispos](../../strongs/g/g2921.md) and [Gaïos](../../strongs/g/g1050.md);

<a name="1corinthians_1_15"></a>1 Corinthians 1:15

Lest any should [eipon](../../strongs/g/g2036.md) that I had [baptizō](../../strongs/g/g907.md) in mine own [onoma](../../strongs/g/g3686.md).

<a name="1corinthians_1_16"></a>1 Corinthians 1:16

And I [baptizō](../../strongs/g/g907.md) also the [oikos](../../strongs/g/g3624.md) of [stephanas](../../strongs/g/g4734.md): besides, I [eidō](../../strongs/g/g1492.md) not whether I [baptizō](../../strongs/g/g907.md) any other.

<a name="1corinthians_1_17"></a>1 Corinthians 1:17

For [Christos](../../strongs/g/g5547.md) [apostellō](../../strongs/g/g649.md) me not to [baptizō](../../strongs/g/g907.md), but to [euaggelizō](../../strongs/g/g2097.md): not with [sophia](../../strongs/g/g4678.md) of [logos](../../strongs/g/g3056.md), lest the [stauros](../../strongs/g/g4716.md) of [Christos](../../strongs/g/g5547.md) should be made [kenoō](../../strongs/g/g2758.md).

<a name="1corinthians_1_18"></a>1 Corinthians 1:18

For the [logos](../../strongs/g/g3056.md) of the [stauros](../../strongs/g/g4716.md) is to them that [apollymi](../../strongs/g/g622.md) [mōria](../../strongs/g/g3472.md); but unto us which are [sōzō](../../strongs/g/g4982.md) it is the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_1_19"></a>1 Corinthians 1:19

For it is [graphō](../../strongs/g/g1125.md), I will [apollymi](../../strongs/g/g622.md) the [sophia](../../strongs/g/g4678.md) of the [sophos](../../strongs/g/g4680.md), and will [atheteō](../../strongs/g/g114.md) the [synesis](../../strongs/g/g4907.md) of the [synetos](../../strongs/g/g4908.md).

<a name="1corinthians_1_20"></a>1 Corinthians 1:20

Where is the [sophos](../../strongs/g/g4680.md)? where is the [grammateus](../../strongs/g/g1122.md)? where is the [syzētētēs](../../strongs/g/g4804.md) of this [aiōn](../../strongs/g/g165.md)? hath not [theos](../../strongs/g/g2316.md) [mōrainō](../../strongs/g/g3471.md) the [sophia](../../strongs/g/g4678.md) of this [kosmos](../../strongs/g/g2889.md)?

<a name="1corinthians_1_21"></a>1 Corinthians 1:21

For after that in the [sophia](../../strongs/g/g4678.md) of [theos](../../strongs/g/g2316.md) the [kosmos](../../strongs/g/g2889.md) by [sophia](../../strongs/g/g4678.md) [ginōskō](../../strongs/g/g1097.md) not [theos](../../strongs/g/g2316.md), it [eudokeō](../../strongs/g/g2106.md) [theos](../../strongs/g/g2316.md) by the [mōria](../../strongs/g/g3472.md) of [kērygma](../../strongs/g/g2782.md) to [sōzō](../../strongs/g/g4982.md) them that [pisteuō](../../strongs/g/g4100.md).

<a name="1corinthians_1_22"></a>1 Corinthians 1:22

For the [Ioudaios](../../strongs/g/g2453.md) [aiteō](../../strongs/g/g154.md) a [sēmeion](../../strongs/g/g4592.md), and the [Hellēn](../../strongs/g/g1672.md) [zēteō](../../strongs/g/g2212.md) after [sophia](../../strongs/g/g4678.md):

<a name="1corinthians_1_23"></a>1 Corinthians 1:23

But we [kēryssō](../../strongs/g/g2784.md) [Christos](../../strongs/g/g5547.md) [stauroō](../../strongs/g/g4717.md), unto the [Ioudaios](../../strongs/g/g2453.md) [men](../../strongs/g/g3303.md) [skandalon](../../strongs/g/g4625.md), and unto the [Hellēn](../../strongs/g/g1672.md) [mōria](../../strongs/g/g3472.md);

<a name="1corinthians_1_24"></a>1 Corinthians 1:24

But unto them which are [klētos](../../strongs/g/g2822.md), both [Ioudaios](../../strongs/g/g2453.md) and [Hellēn](../../strongs/g/g1672.md), [Christos](../../strongs/g/g5547.md) the [dynamis](../../strongs/g/g1411.md) of [theos](../../strongs/g/g2316.md), and the [sophia](../../strongs/g/g4678.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_1_25"></a>1 Corinthians 1:25

Because the [mōros](../../strongs/g/g3474.md) of [theos](../../strongs/g/g2316.md) is [sophos](../../strongs/g/g4680.md) than [anthrōpos](../../strongs/g/g444.md); and the [asthenēs](../../strongs/g/g772.md) of [theos](../../strongs/g/g2316.md) is [ischyros](../../strongs/g/g2478.md) than [anthrōpos](../../strongs/g/g444.md).

<a name="1corinthians_1_26"></a>1 Corinthians 1:26

For ye [blepō](../../strongs/g/g991.md) your [klēsis](../../strongs/g/g2821.md), [adelphos](../../strongs/g/g80.md), how that not [polys](../../strongs/g/g4183.md) [sophos](../../strongs/g/g4680.md) after the [sarx](../../strongs/g/g4561.md), not [polys](../../strongs/g/g4183.md) [dynatos](../../strongs/g/g1415.md), not [polys](../../strongs/g/g4183.md) [eugenēs](../../strongs/g/g2104.md):

<a name="1corinthians_1_27"></a>1 Corinthians 1:27

But [theos](../../strongs/g/g2316.md) hath [eklegomai](../../strongs/g/g1586.md) the [mōros](../../strongs/g/g3474.md) of the [kosmos](../../strongs/g/g2889.md) to [kataischynō](../../strongs/g/g2617.md) the [sophos](../../strongs/g/g4680.md); and [theos](../../strongs/g/g2316.md) hath [eklegomai](../../strongs/g/g1586.md) the [asthenēs](../../strongs/g/g772.md) of the [kosmos](../../strongs/g/g2889.md) to [kataischynō](../../strongs/g/g2617.md) the [ischyros](../../strongs/g/g2478.md);

<a name="1corinthians_1_28"></a>1 Corinthians 1:28

And [agenēs](../../strongs/g/g36.md) of the [kosmos](../../strongs/g/g2889.md), and [exoutheneō](../../strongs/g/g1848.md), hath [theos](../../strongs/g/g2316.md) [eklegomai](../../strongs/g/g1586.md), yea, and things which are not, to [katargeō](../../strongs/g/g2673.md) things that are:

<a name="1corinthians_1_29"></a>1 Corinthians 1:29

That no [sarx](../../strongs/g/g4561.md) should [kauchaomai](../../strongs/g/g2744.md) in his [enōpion](../../strongs/g/g1799.md).

<a name="1corinthians_1_30"></a>1 Corinthians 1:30

But of him are ye in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md), who of [theos](../../strongs/g/g2316.md) is made unto us [sophia](../../strongs/g/g4678.md), and [dikaiosynē](../../strongs/g/g1343.md), and [hagiasmos](../../strongs/g/g38.md), and [apolytrōsis](../../strongs/g/g629.md):

<a name="1corinthians_1_31"></a>1 Corinthians 1:31

That, according as it is [graphō](../../strongs/g/g1125.md), He that [kauchaomai](../../strongs/g/g2744.md), let him [kauchaomai](../../strongs/g/g2744.md) in the [kyrios](../../strongs/g/g2962.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 2](1corinthians_2.md)