# [1 Corinthians 13](https://www.blueletterbible.org/kjv/1co/13/1/s_1075001)

<a name="1corinthians_13_1"></a>1 Corinthians 13:1

Though I [laleō](../../strongs/g/g2980.md) with the [glōssa](../../strongs/g/g1100.md) of [anthrōpos](../../strongs/g/g444.md) and of [aggelos](../../strongs/g/g32.md), and have not [agapē](../../strongs/g/g26.md), I am become as [ēcheō](../../strongs/g/g2278.md) [chalkos](../../strongs/g/g5475.md), or an [alalazō](../../strongs/g/g214.md) [kymbalon](../../strongs/g/g2950.md).

<a name="1corinthians_13_2"></a>1 Corinthians 13:2

And though I have [prophēteia](../../strongs/g/g4394.md), and [eidō](../../strongs/g/g1492.md) all [mystērion](../../strongs/g/g3466.md), and all [gnōsis](../../strongs/g/g1108.md); and though I have all [pistis](../../strongs/g/g4102.md), so that I could [methistēmi](../../strongs/g/g3179.md) [oros](../../strongs/g/g3735.md), and have not [agapē](../../strongs/g/g26.md), I am nothing.

<a name="1corinthians_13_3"></a>1 Corinthians 13:3

And though I [psōmizō](../../strongs/g/g5595.md) all my [hyparchonta](../../strongs/g/g5224.md), and though I [paradidōmi](../../strongs/g/g3860.md) my [sōma](../../strongs/g/g4983.md) to be [kaiō](../../strongs/g/g2545.md), and have not [agapē](../../strongs/g/g26.md), it [ōpheleō](../../strongs/g/g5623.md) me [oudeis](../../strongs/g/g3762.md).

<a name="1corinthians_13_4"></a>1 Corinthians 13:4

[agapē](../../strongs/g/g26.md) [makrothymeō](../../strongs/g/g3114.md), and is [chrēsteuomai](../../strongs/g/g5541.md); [agapē](../../strongs/g/g26.md) [zēloō](../../strongs/g/g2206.md) not; [agapē](../../strongs/g/g26.md) [perpereuomai](../../strongs/g/g4068.md) not, is not [physioō](../../strongs/g/g5448.md),

<a name="1corinthians_13_5"></a>1 Corinthians 13:5

Doth not [aschēmoneō](../../strongs/g/g807.md), [zēteō](../../strongs/g/g2212.md) not her own, is not [paroxynō](../../strongs/g/g3947.md), [logizomai](../../strongs/g/g3049.md) no [kakos](../../strongs/g/g2556.md);

<a name="1corinthians_13_6"></a>1 Corinthians 13:6

[chairō](../../strongs/g/g5463.md) not in [adikia](../../strongs/g/g93.md), but [sygchairō](../../strongs/g/g4796.md) in the [alētheia](../../strongs/g/g225.md);

<a name="1corinthians_13_7"></a>1 Corinthians 13:7

[stegō](../../strongs/g/g4722.md) all things, [pisteuō](../../strongs/g/g4100.md) all things, [elpizō](../../strongs/g/g1679.md) all things, [hypomenō](../../strongs/g/g5278.md) all things.

<a name="1corinthians_13_8"></a>1 Corinthians 13:8

[agapē](../../strongs/g/g26.md) never [ekpiptō](../../strongs/g/g1601.md): but whether [prophēteia](../../strongs/g/g4394.md), they shall [katargeō](../../strongs/g/g2673.md); whether [glōssa](../../strongs/g/g1100.md), they shall [pauō](../../strongs/g/g3973.md); whether [gnōsis](../../strongs/g/g1108.md), it shall [katargeō](../../strongs/g/g2673.md).

<a name="1corinthians_13_9"></a>1 Corinthians 13:9

For we [ginōskō](../../strongs/g/g1097.md) in [meros](../../strongs/g/g3313.md), and we [prophēteuō](../../strongs/g/g4395.md) in [meros](../../strongs/g/g3313.md).

<a name="1corinthians_13_10"></a>1 Corinthians 13:10

But when [teleios](../../strongs/g/g5046.md) is [erchomai](../../strongs/g/g2064.md), then that which is in [meros](../../strongs/g/g3313.md) shall be [katargeō](../../strongs/g/g2673.md).

<a name="1corinthians_13_11"></a>1 Corinthians 13:11

When I was a [nēpios](../../strongs/g/g3516.md), I [laleō](../../strongs/g/g2980.md) as a [nēpios](../../strongs/g/g3516.md), I [phroneō](../../strongs/g/g5426.md) as a [nēpios](../../strongs/g/g3516.md), I [logizomai](../../strongs/g/g3049.md) as a [nēpios](../../strongs/g/g3516.md): but when I [ginomai](../../strongs/g/g1096.md) an [anēr](../../strongs/g/g435.md), I [katargeō](../../strongs/g/g2673.md) [nēpios](../../strongs/g/g3516.md).

<a name="1corinthians_13_12"></a>1 Corinthians 13:12

For [arti](../../strongs/g/g737.md) we [blepō](../../strongs/g/g991.md) through an [esoptron](../../strongs/g/g2072.md), [en](../../strongs/g/g1722.md) [ainigma](../../strongs/g/g135.md); but then [prosōpon](../../strongs/g/g4383.md) to [prosōpon](../../strongs/g/g4383.md): [arti](../../strongs/g/g737.md) I [ginōskō](../../strongs/g/g1097.md) in [meros](../../strongs/g/g3313.md); but then shall I [epiginōskō](../../strongs/g/g1921.md) even as also I am [epiginōskō](../../strongs/g/g1921.md).

<a name="1corinthians_13_13"></a>1 Corinthians 13:13

And [nyni](../../strongs/g/g3570.md) [menō](../../strongs/g/g3306.md) [pistis](../../strongs/g/g4102.md), [elpis](../../strongs/g/g1680.md), [agapē](../../strongs/g/g26.md), these three; but the [meizōn](../../strongs/g/g3187.md) of these is [agapē](../../strongs/g/g26.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 12](1corinthians_12.md) - [1 Corinthians 14](1corinthians_14.md)