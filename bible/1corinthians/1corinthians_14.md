# [1 Corinthians 14](https://www.blueletterbible.org/kjv/1co/14/1/s_1076001)

<a name="1corinthians_14_1"></a>1 Corinthians 14:1

[diōkō](../../strongs/g/g1377.md) [agapē](../../strongs/g/g26.md), and [zēloō](../../strongs/g/g2206.md) [pneumatikos](../../strongs/g/g4152.md), but rather that ye may [prophēteuō](../../strongs/g/g4395.md).

<a name="1corinthians_14_2"></a>1 Corinthians 14:2

For he that [laleō](../../strongs/g/g2980.md) in a [glōssa](../../strongs/g/g1100.md) [laleō](../../strongs/g/g2980.md) not unto [anthrōpos](../../strongs/g/g444.md), but unto [theos](../../strongs/g/g2316.md): for no man [akouō](../../strongs/g/g191.md) him; howbeit in the [pneuma](../../strongs/g/g4151.md) he [laleō](../../strongs/g/g2980.md) [mystērion](../../strongs/g/g3466.md).

<a name="1corinthians_14_3"></a>1 Corinthians 14:3

But he that [prophēteuō](../../strongs/g/g4395.md) [laleō](../../strongs/g/g2980.md) unto [anthrōpos](../../strongs/g/g444.md) to [oikodomē](../../strongs/g/g3619.md), and [paraklēsis](../../strongs/g/g3874.md), and [paramythia](../../strongs/g/g3889.md).

<a name="1corinthians_14_4"></a>1 Corinthians 14:4

He that [laleō](../../strongs/g/g2980.md) in a [glōssa](../../strongs/g/g1100.md) [oikodomeō](../../strongs/g/g3618.md) himself; but he that [prophēteuō](../../strongs/g/g4395.md) [oikodomeō](../../strongs/g/g3618.md) the [ekklēsia](../../strongs/g/g1577.md).

<a name="1corinthians_14_5"></a>1 Corinthians 14:5

I would that ye all [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md) but rather that ye [prophēteuō](../../strongs/g/g4395.md): for [meizōn](../../strongs/g/g3187.md) is he that [prophēteuō](../../strongs/g/g4395.md) than he that [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md), except he [diermēneuō](../../strongs/g/g1329.md), that the [ekklēsia](../../strongs/g/g1577.md) may [lambanō](../../strongs/g/g2983.md) [oikodomē](../../strongs/g/g3619.md).

<a name="1corinthians_14_6"></a>1 Corinthians 14:6

[nyni](../../strongs/g/g3570.md), [adelphos](../../strongs/g/g80.md), if I [erchomai](../../strongs/g/g2064.md) unto you [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md), what shall I [ōpheleō](../../strongs/g/g5623.md) you, except I shall [laleō](../../strongs/g/g2980.md) to you either by [apokalypsis](../../strongs/g/g602.md), or by [gnōsis](../../strongs/g/g1108.md), or by [prophēteia](../../strongs/g/g4394.md), or by [didachē](../../strongs/g/g1322.md)?

<a name="1corinthians_14_7"></a>1 Corinthians 14:7

And even [apsychos](../../strongs/g/g895.md) [didōmi](../../strongs/g/g1325.md) [phōnē](../../strongs/g/g5456.md), whether [aulos](../../strongs/g/g836.md) or [kithara](../../strongs/g/g2788.md), except they [didōmi](../../strongs/g/g1325.md) a [diastolē](../../strongs/g/g1293.md) in the [phthongos](../../strongs/g/g5353.md), how shall it be [ginōskō](../../strongs/g/g1097.md) what is [auleō](../../strongs/g/g832.md) or [kitharizō](../../strongs/g/g2789.md)?

<a name="1corinthians_14_8"></a>1 Corinthians 14:8

For if the [salpigx](../../strongs/g/g4536.md) [didōmi](../../strongs/g/g1325.md) an [adēlos](../../strongs/g/g82.md) [phōnē](../../strongs/g/g5456.md), who shall [paraskeuazō](../../strongs/g/g3903.md) to the [polemos](../../strongs/g/g4171.md)?

<a name="1corinthians_14_9"></a>1 Corinthians 14:9

So likewise ye, except ye [didōmi](../../strongs/g/g1325.md) by the [glōssa](../../strongs/g/g1100.md) [logos](../../strongs/g/g3056.md) [eusēmos](../../strongs/g/g2154.md), how shall it be [ginōskō](../../strongs/g/g1097.md) what is [laleō](../../strongs/g/g2980.md)? for ye shall [laleō](../../strongs/g/g2980.md) into the [aēr](../../strongs/g/g109.md).

<a name="1corinthians_14_10"></a>1 Corinthians 14:10

There are, [tygchanō](../../strongs/g/g5177.md), [tosoutos](../../strongs/g/g5118.md) [genos](../../strongs/g/g1085.md) of [phōnē](../../strongs/g/g5456.md) in the [kosmos](../../strongs/g/g2889.md), and none of them is [aphōnos](../../strongs/g/g880.md).

<a name="1corinthians_14_11"></a>1 Corinthians 14:11

Therefore if I [eidō](../../strongs/g/g1492.md) not the [dynamis](../../strongs/g/g1411.md) of the [phōnē](../../strongs/g/g5456.md), I shall be unto him that [laleō](../../strongs/g/g2980.md) a [barbaros](../../strongs/g/g915.md), and he that [laleō](../../strongs/g/g2980.md) a [barbaros](../../strongs/g/g915.md) unto me.

<a name="1corinthians_14_12"></a>1 Corinthians 14:12

Even so ye, forasmuch as ye are [zēlōtēs](../../strongs/g/g2207.md) of [pneuma](../../strongs/g/g4151.md), [zēteō](../../strongs/g/g2212.md) that ye may [perisseuō](../../strongs/g/g4052.md) to the [oikodomē](../../strongs/g/g3619.md) of the [ekklēsia](../../strongs/g/g1577.md).

<a name="1corinthians_14_13"></a>1 Corinthians 14:13

Wherefore let him that [laleō](../../strongs/g/g2980.md) in a [glōssa](../../strongs/g/g1100.md) [proseuchomai](../../strongs/g/g4336.md) that he may [diermēneuō](../../strongs/g/g1329.md).

<a name="1corinthians_14_14"></a>1 Corinthians 14:14

For if I [proseuchomai](../../strongs/g/g4336.md) in a [glōssa](../../strongs/g/g1100.md), my [pneuma](../../strongs/g/g4151.md) [proseuchomai](../../strongs/g/g4336.md), but my [nous](../../strongs/g/g3563.md) is [akarpos](../../strongs/g/g175.md).

<a name="1corinthians_14_15"></a>1 Corinthians 14:15

What is it then? I will [proseuchomai](../../strongs/g/g4336.md) with the [pneuma](../../strongs/g/g4151.md), and I will [proseuchomai](../../strongs/g/g4336.md) with the [nous](../../strongs/g/g3563.md) also: I will [psallō](../../strongs/g/g5567.md) with the [pneuma](../../strongs/g/g4151.md), and I will [psallō](../../strongs/g/g5567.md) with the [nous](../../strongs/g/g3563.md) also.

<a name="1corinthians_14_16"></a>1 Corinthians 14:16

Else when thou shalt [eulogeō](../../strongs/g/g2127.md) with the [pneuma](../../strongs/g/g4151.md), how shall he that [anaplēroō](../../strongs/g/g378.md) the [topos](../../strongs/g/g5117.md) of the [idiōtēs](../../strongs/g/g2399.md) [eipon](../../strongs/g/g2046.md) [amēn](../../strongs/g/g281.md) at thy [eucharistia](../../strongs/g/g2169.md), seeing he [eidō](../../strongs/g/g1492.md) not what thou [legō](../../strongs/g/g3004.md)?

<a name="1corinthians_14_17"></a>1 Corinthians 14:17

For thou [men](../../strongs/g/g3303.md) [eucharisteō](../../strongs/g/g2168.md) [kalōs](../../strongs/g/g2573.md), but the other is not [oikodomeō](../../strongs/g/g3618.md).

<a name="1corinthians_14_18"></a>1 Corinthians 14:18

I [eucharisteō](../../strongs/g/g2168.md) my [theos](../../strongs/g/g2316.md), I [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md) more than ye all:

<a name="1corinthians_14_19"></a>1 Corinthians 14:19

Yet in the [ekklēsia](../../strongs/g/g1577.md) I had rather [laleō](../../strongs/g/g2980.md) five [logos](../../strongs/g/g3056.md) with my [nous](../../strongs/g/g3563.md), that by [katēcheō](../../strongs/g/g2727.md) others also, than [myrios](../../strongs/g/g3463.md) [logos](../../strongs/g/g3056.md) in a [glōssa](../../strongs/g/g1100.md).

<a name="1corinthians_14_20"></a>1 Corinthians 14:20

[adelphos](../../strongs/g/g80.md), be not [paidion](../../strongs/g/g3813.md) in [phrēn](../../strongs/g/g5424.md): howbeit in [kakia](../../strongs/g/g2549.md) be ye [nēpiazō](../../strongs/g/g3515.md), but in [phrēn](../../strongs/g/g5424.md) be [teleios](../../strongs/g/g5046.md).

<a name="1corinthians_14_21"></a>1 Corinthians 14:21

In the [nomos](../../strongs/g/g3551.md) it is [graphō](../../strongs/g/g1125.md), With [heteroglōssos](../../strongs/g/g2084.md) and [heteros](../../strongs/g/g2087.md) [cheilos](../../strongs/g/g5491.md) will I [laleō](../../strongs/g/g2980.md) unto this [laos](../../strongs/g/g2992.md); and yet for all that will they not [eisakouō](../../strongs/g/g1522.md) me, [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_14_22"></a>1 Corinthians 14:22

Wherefore [glōssa](../../strongs/g/g1100.md) are for a [sēmeion](../../strongs/g/g4592.md), not to them that [pisteuō](../../strongs/g/g4100.md), but to [apistos](../../strongs/g/g571.md): but [prophēteia](../../strongs/g/g4394.md) not for [apistos](../../strongs/g/g571.md), but for [pisteuō](../../strongs/g/g4100.md).

<a name="1corinthians_14_23"></a>1 Corinthians 14:23

If therefore the [holos](../../strongs/g/g3650.md) [ekklēsia](../../strongs/g/g1577.md) [synerchomai](../../strongs/g/g4905.md) into one place, and all [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md), and there [eiserchomai](../../strongs/g/g1525.md) [idiōtēs](../../strongs/g/g2399.md), or [apistos](../../strongs/g/g571.md), will they not [eipon](../../strongs/g/g2046.md) that ye are [mainomai](../../strongs/g/g3105.md)?

<a name="1corinthians_14_24"></a>1 Corinthians 14:24

But if all [prophēteuō](../../strongs/g/g4395.md), and there [eiserchomai](../../strongs/g/g1525.md) one that [apistos](../../strongs/g/g571.md), or [idiōtēs](../../strongs/g/g2399.md), he is [elegchō](../../strongs/g/g1651.md) of all, he is [anakrinō](../../strongs/g/g350.md) of all:

<a name="1corinthians_14_25"></a>1 Corinthians 14:25

And thus are the [kryptos](../../strongs/g/g2927.md) of his [kardia](../../strongs/g/g2588.md) [ginomai](../../strongs/g/g1096.md) [phaneros](../../strongs/g/g5318.md); and so [piptō](../../strongs/g/g4098.md) on his [prosōpon](../../strongs/g/g4383.md) he will [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md), and [apaggellō](../../strongs/g/g518.md) that [theos](../../strongs/g/g2316.md) is in you [ontōs](../../strongs/g/g3689.md).

<a name="1corinthians_14_26"></a>1 Corinthians 14:26

How is it then, [adelphos](../../strongs/g/g80.md)? when ye [synerchomai](../../strongs/g/g4905.md), every one of you hath a [psalmos](../../strongs/g/g5568.md), hath a [didachē](../../strongs/g/g1322.md), hath a [glōssa](../../strongs/g/g1100.md), hath an [apokalypsis](../../strongs/g/g602.md), hath an [hermēneia](../../strongs/g/g2058.md). Let all things be [ginomai](../../strongs/g/g1096.md) unto [oikodomē](../../strongs/g/g3619.md).

<a name="1corinthians_14_27"></a>1 Corinthians 14:27

If [tis](../../strongs/g/g5100.md) [laleō](../../strongs/g/g2980.md) in a [glōssa](../../strongs/g/g1100.md), by two, or at the [pleistos](../../strongs/g/g4118.md) three, and by [meros](../../strongs/g/g3313.md); and let one [diermēneuō](../../strongs/g/g1329.md).

<a name="1corinthians_14_28"></a>1 Corinthians 14:28

But if there be no [diermēneutēs](../../strongs/g/g1328.md), let him [sigaō](../../strongs/g/g4601.md) in the [ekklēsia](../../strongs/g/g1577.md); and let him [laleō](../../strongs/g/g2980.md) to himself, and to [theos](../../strongs/g/g2316.md).

<a name="1corinthians_14_29"></a>1 Corinthians 14:29

Let the [prophētēs](../../strongs/g/g4396.md) [laleō](../../strongs/g/g2980.md) two or three, and let the other [diakrinō](../../strongs/g/g1252.md).

<a name="1corinthians_14_30"></a>1 Corinthians 14:30

If [apokalyptō](../../strongs/g/g601.md) to another that [kathēmai](../../strongs/g/g2521.md), let the first [sigaō](../../strongs/g/g4601.md).

<a name="1corinthians_14_31"></a>1 Corinthians 14:31

For ye may all [prophēteuō](../../strongs/g/g4395.md) one by one, that all may [manthanō](../../strongs/g/g3129.md), and all may be [parakaleō](../../strongs/g/g3870.md).

<a name="1corinthians_14_32"></a>1 Corinthians 14:32

And the [pneuma](../../strongs/g/g4151.md) of the [prophētēs](../../strongs/g/g4396.md) are [hypotassō](../../strongs/g/g5293.md) the [prophētēs](../../strongs/g/g4396.md).

<a name="1corinthians_14_33"></a>1 Corinthians 14:33

For [theos](../../strongs/g/g2316.md) is not [akatastasia](../../strongs/g/g181.md), but of [eirēnē](../../strongs/g/g1515.md), as in all [ekklēsia](../../strongs/g/g1577.md) of the [hagios](../../strongs/g/g40.md).

<a name="1corinthians_14_34"></a>1 Corinthians 14:34

Let your [gynē](../../strongs/g/g1135.md) [sigaō](../../strongs/g/g4601.md) in the [ekklēsia](../../strongs/g/g1577.md): for it is not [epitrepō](../../strongs/g/g2010.md) unto them to [laleō](../../strongs/g/g2980.md); but to be [hypotassō](../../strongs/g/g5293.md) as also [legō](../../strongs/g/g3004.md) the [nomos](../../strongs/g/g3551.md).

<a name="1corinthians_14_35"></a>1 Corinthians 14:35

And if they will [manthanō](../../strongs/g/g3129.md) any thing, let them [eperōtaō](../../strongs/g/g1905.md) their [anēr](../../strongs/g/g435.md) at [oikos](../../strongs/g/g3624.md): for it is [aischron](../../strongs/g/g149.md) for [gynē](../../strongs/g/g1135.md) to [laleō](../../strongs/g/g2980.md) in the [ekklēsia](../../strongs/g/g1577.md). [^1]

<a name="1corinthians_14_36"></a>1 Corinthians 14:36

[ē](../../strongs/g/g2228.md)? [exerchomai](../../strongs/g/g1831.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md) out from you? or [katantaō](../../strongs/g/g2658.md) it unto you only?

<a name="1corinthians_14_37"></a>1 Corinthians 14:37

[ei tis](../../strongs/g/g1536.md) [dokeō](../../strongs/g/g1380.md) to be a [prophētēs](../../strongs/g/g4396.md), or [pneumatikos](../../strongs/g/g4152.md), let him [epiginōskō](../../strongs/g/g1921.md) that the things that I [graphō](../../strongs/g/g1125.md) unto you are the [entolē](../../strongs/g/g1785.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_14_38"></a>1 Corinthians 14:38

But [ei tis](../../strongs/g/g1536.md) be [agnoeō](../../strongs/g/g50.md), let him be [agnoeō](../../strongs/g/g50.md). [^2]

<a name="1corinthians_14_39"></a>1 Corinthians 14:39

Wherefore, [adelphos](../../strongs/g/g80.md), [zēloō](../../strongs/g/g2206.md) to [prophēteuō](../../strongs/g/g4395.md), and [kōlyō](../../strongs/g/g2967.md) not to [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md).

<a name="1corinthians_14_40"></a>1 Corinthians 14:40

Let all things [ginomai](../../strongs/g/g1096.md) [euschēmonōs](../../strongs/g/g2156.md) and in [taxis](../../strongs/g/g5010.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 13](1corinthians_13.md) - [1Corinthians 15](1corinthians_15.md)

---

[^1]: [1 Corinthians 14:35 Commentary](../../commentary/1corinthians/1corinthians_14_commentary.md#1corinthians_14_35)

[^2]: [1 Corinthians 14:38 Commentary](../../commentary/1corinthians/1corinthians_14_commentary.md#1corinthians_14_38)
