# [1 Corinthians 16](https://www.blueletterbible.org/kjv/1co/16/1/s_1078001)

<a name="1corinthians_16_1"></a>1 Corinthians 16:1

Now concerning the [logeia](../../strongs/g/g3048.md) for the [hagios](../../strongs/g/g40.md), as I have [diatassō](../../strongs/g/g1299.md) to the [ekklēsia](../../strongs/g/g1577.md) of [galatia](../../strongs/g/g1053.md), even so [poieō](../../strongs/g/g4160.md) ye.

<a name="1corinthians_16_2"></a>1 Corinthians 16:2

Upon the first [sabbaton](../../strongs/g/g4521.md) let every one of you [tithēmi](../../strongs/g/g5087.md) by him in [thēsaurizō](../../strongs/g/g2343.md), as [euodoo](../../strongs/g/g2137.md), that there be no [logeia](../../strongs/g/g3048.md) [tote](../../strongs/g/g5119.md) when I [erchomai](../../strongs/g/g2064.md).

<a name="1corinthians_16_3"></a>1 Corinthians 16:3

And when I [paraginomai](../../strongs/g/g3854.md), whomsoever ye shall [dokimazō](../../strongs/g/g1381.md) by your [epistolē](../../strongs/g/g1992.md), them will I [pempō](../../strongs/g/g3992.md) to [apopherō](../../strongs/g/g667.md) your [charis](../../strongs/g/g5485.md) unto [Ierousalēm](../../strongs/g/g2419.md).

<a name="1corinthians_16_4"></a>1 Corinthians 16:4

And if it be [axios](../../strongs/g/g514.md) that I [poreuō](../../strongs/g/g4198.md) also, they shall [poreuō](../../strongs/g/g4198.md) with me.

<a name="1corinthians_16_5"></a>1 Corinthians 16:5

Now I will [erchomai](../../strongs/g/g2064.md) unto you, when I shall [dierchomai](../../strongs/g/g1330.md) through [Makedonia](../../strongs/g/g3109.md): for I [dierchomai](../../strongs/g/g1330.md) [Makedonia](../../strongs/g/g3109.md).

<a name="1corinthians_16_6"></a>1 Corinthians 16:6

And it [tygchanō](../../strongs/g/g5177.md) that I will [paramenō](../../strongs/g/g3887.md), yea, and [paracheimazō](../../strongs/g/g3914.md) with you, that ye may [propempō](../../strongs/g/g4311.md) me whithersoever I [poreuō](../../strongs/g/g4198.md).

<a name="1corinthians_16_7"></a>1 Corinthians 16:7

For I will not [eidō](../../strongs/g/g1492.md) you now by [parodos](../../strongs/g/g3938.md); but I [elpizō](../../strongs/g/g1679.md) to [epimenō](../../strongs/g/g1961.md) a [chronos](../../strongs/g/g5550.md) with you, if the [kyrios](../../strongs/g/g2962.md) [epitrepō](../../strongs/g/g2010.md).

<a name="1corinthians_16_8"></a>1 Corinthians 16:8

But I will [epimenō](../../strongs/g/g1961.md) at [Ephesos](../../strongs/g/g2181.md) until [pentēkostē](../../strongs/g/g4005.md).

<a name="1corinthians_16_9"></a>1 Corinthians 16:9

For a [megas](../../strongs/g/g3173.md) [thyra](../../strongs/g/g2374.md) and [energēs](../../strongs/g/g1756.md) is [anoigō](../../strongs/g/g455.md) unto me, and there are [polys](../../strongs/g/g4183.md) [antikeimai](../../strongs/g/g480.md).

<a name="1corinthians_16_10"></a>1 Corinthians 16:10

Now if [Timotheos](../../strongs/g/g5095.md) [erchomai](../../strongs/g/g2064.md), [blepō](../../strongs/g/g991.md) that he may be with you [aphobōs](../../strongs/g/g870.md): for he [ergazomai](../../strongs/g/g2038.md) the [ergon](../../strongs/g/g2041.md) of the [kyrios](../../strongs/g/g2962.md), as I also.

<a name="1corinthians_16_11"></a>1 Corinthians 16:11

Let no [tis](../../strongs/g/g5100.md) therefore [exoutheneō](../../strongs/g/g1848.md) him: but [propempō](../../strongs/g/g4311.md) him in [eirēnē](../../strongs/g/g1515.md), that he may [erchomai](../../strongs/g/g2064.md) unto me: for I [ekdechomai](../../strongs/g/g1551.md) him with the [adelphos](../../strongs/g/g80.md).

<a name="1corinthians_16_12"></a>1 Corinthians 16:12

As touching our [adelphos](../../strongs/g/g80.md) [Apollōs](../../strongs/g/g625.md), I [polys](../../strongs/g/g4183.md) [parakaleō](../../strongs/g/g3870.md) him to [erchomai](../../strongs/g/g2064.md) unto you with the [adelphos](../../strongs/g/g80.md): but his [thelēma](../../strongs/g/g2307.md) was not at [pantōs](../../strongs/g/g3843.md) to [erchomai](../../strongs/g/g2064.md) [nyn](../../strongs/g/g3568.md); but he will [erchomai](../../strongs/g/g2064.md) when [eukaireō](../../strongs/g/g2119.md).

<a name="1corinthians_16_13"></a>1 Corinthians 16:13

[grēgoreō](../../strongs/g/g1127.md), [stēkō](../../strongs/g/g4739.md) in the [pistis](../../strongs/g/g4102.md), [andrizomai](../../strongs/g/g407.md), [krataioō](../../strongs/g/g2901.md).

<a name="1corinthians_16_14"></a>1 Corinthians 16:14

Let all your things be [ginomai](../../strongs/g/g1096.md) with [agapē](../../strongs/g/g26.md).

<a name="1corinthians_16_15"></a>1 Corinthians 16:15

I [parakaleō](../../strongs/g/g3870.md) you, [adelphos](../../strongs/g/g80.md), (ye [eidō](../../strongs/g/g1492.md) the [oikia](../../strongs/g/g3614.md) of [stephanas](../../strongs/g/g4734.md), that it is the [aparchē](../../strongs/g/g536.md) of [Achaïa](../../strongs/g/g882.md), and that they have [tassō](../../strongs/g/g5021.md) themselves to the [diakonia](../../strongs/g/g1248.md) of the [hagios](../../strongs/g/g40.md),)

<a name="1corinthians_16_16"></a>1 Corinthians 16:16

That ye [hypotassō](../../strongs/g/g5293.md) unto such, and to every one that [synergeō](../../strongs/g/g4903.md) with us, and [kopiaō](../../strongs/g/g2872.md).

<a name="1corinthians_16_17"></a>1 Corinthians 16:17

I am [chairō](../../strongs/g/g5463.md) of the [parousia](../../strongs/g/g3952.md) of [stephanas](../../strongs/g/g4734.md) and [phortounatos](../../strongs/g/g5415.md) and [achaikos](../../strongs/g/g883.md): for that which was [hysterēma](../../strongs/g/g5303.md) on your part they have [anaplēroō](../../strongs/g/g378.md).

<a name="1corinthians_16_18"></a>1 Corinthians 16:18

For they have [anapauō](../../strongs/g/g373.md) my [pneuma](../../strongs/g/g4151.md) and your's: therefore [epiginōskō](../../strongs/g/g1921.md) ye them that are such.

<a name="1corinthians_16_19"></a>1 Corinthians 16:19

The [ekklēsia](../../strongs/g/g1577.md) of [Asia](../../strongs/g/g773.md) [aspazomai](../../strongs/g/g782.md) you. [Akylas](../../strongs/g/g207.md) and [Priskilla](../../strongs/g/g4252.md) [aspazomai](../../strongs/g/g782.md) you [polys](../../strongs/g/g4183.md) in the [kyrios](../../strongs/g/g2962.md), with the [ekklēsia](../../strongs/g/g1577.md) that is in their [oikos](../../strongs/g/g3624.md).

<a name="1corinthians_16_20"></a>1 Corinthians 16:20

All the [adelphos](../../strongs/g/g80.md) [aspazomai](../../strongs/g/g782.md) you. [aspazomai](../../strongs/g/g782.md) ye [allēlōn](../../strongs/g/g240.md) with an [hagios](../../strongs/g/g40.md) [philēma](../../strongs/g/g5370.md).

<a name="1corinthians_16_21"></a>1 Corinthians 16:21

The [aspasmos](../../strongs/g/g783.md) of [Paulos](../../strongs/g/g3972.md) with mine own [cheir](../../strongs/g/g5495.md).

<a name="1corinthians_16_22"></a>1 Corinthians 16:22

[ei tis](../../strongs/g/g1536.md) [phileō](../../strongs/g/g5368.md) not the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), let him be [anathema](../../strongs/g/g331.md) [marana tha](../../strongs/g/g3134.md).

<a name="1corinthians_16_23"></a>1 Corinthians 16:23

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you.

<a name="1corinthians_16_24"></a>1 Corinthians 16:24

My [agapē](../../strongs/g/g26.md) be with you all in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 15](1corinthians_15.md)
