# [1 Corinthians 7](https://www.blueletterbible.org/kjv/1co/7/32/p0/s_1069001)

<a name="1corinthians_7_1"></a>1 Corinthians 7:1

Now concerning the things whereof ye [graphō](../../strongs/g/g1125.md) unto me: [kalos](../../strongs/g/g2570.md) for an [anthrōpos](../../strongs/g/g444.md) not to [haptomai](../../strongs/g/g680.md) a [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_7_2"></a>1 Corinthians 7:2

Nevertheless, [porneia](../../strongs/g/g4202.md), let [hekastos](../../strongs/g/g1538.md) have his own [gynē](../../strongs/g/g1135.md), and let [hekastos](../../strongs/g/g1538.md) have her own [anēr](../../strongs/g/g435.md).

<a name="1corinthians_7_3"></a>1 Corinthians 7:3

Let the [anēr](../../strongs/g/g435.md) [apodidōmi](../../strongs/g/g591.md) unto the [gynē](../../strongs/g/g1135.md) [opheilō](../../strongs/g/g3784.md) [eunoia](../../strongs/g/g2133.md): and [homoiōs](../../strongs/g/g3668.md) also the [gynē](../../strongs/g/g1135.md) unto the [anēr](../../strongs/g/g435.md).

<a name="1corinthians_7_4"></a>1 Corinthians 7:4

The [gynē](../../strongs/g/g1135.md) hath not [exousiazō](../../strongs/g/g1850.md) of her own [sōma](../../strongs/g/g4983.md), but the [anēr](../../strongs/g/g435.md): and [homoiōs](../../strongs/g/g3668.md) also the [anēr](../../strongs/g/g435.md) hath not [exousiazō](../../strongs/g/g1850.md) of his own [sōma](../../strongs/g/g4983.md), but the [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_7_5"></a>1 Corinthians 7:5

[apostereō](../../strongs/g/g650.md) ye not [allēlōn](../../strongs/g/g240.md), except it be with [symphōnos](../../strongs/g/g4859.md) for a [kairos](../../strongs/g/g2540.md), that ye may [scholazō](../../strongs/g/g4980.md) to [nēsteia](../../strongs/g/g3521.md) and [proseuchē](../../strongs/g/g4335.md); and [synerchomai](../../strongs/g/g4905.md) together again, that [Satanas](../../strongs/g/g4567.md) [peirazō](../../strongs/g/g3985.md) you not for your [akrasia](../../strongs/g/g192.md).

<a name="1corinthians_7_6"></a>1 Corinthians 7:6

But I [legō](../../strongs/g/g3004.md) this by [syngnōmē](../../strongs/g/g4774.md), and not of [epitagē](../../strongs/g/g2003.md).

<a name="1corinthians_7_7"></a>1 Corinthians 7:7

For I would that all [anthrōpos](../../strongs/g/g444.md) were even as I myself. But [hekastos](../../strongs/g/g1538.md) hath his [idios](../../strongs/g/g2398.md) [charisma](../../strongs/g/g5486.md) of [theos](../../strongs/g/g2316.md), one [houtō(s)](../../strongs/g/g3779.md), and another [houtō(s)](../../strongs/g/g3779.md).

<a name="1corinthians_7_8"></a>1 Corinthians 7:8

I [legō](../../strongs/g/g3004.md) therefore to the [agamos](../../strongs/g/g22.md) and [chēra](../../strongs/g/g5503.md), It is [kalos](../../strongs/g/g2570.md) for them if they [menō](../../strongs/g/g3306.md) even as I.

<a name="1corinthians_7_9"></a>1 Corinthians 7:9

But if they cannot [egkrateuomai](../../strongs/g/g1467.md), let them [gameō](../../strongs/g/g1060.md): for it is [kreittōn](../../strongs/g/g2909.md) to [gameō](../../strongs/g/g1060.md) than [pyroō](../../strongs/g/g4448.md).

<a name="1corinthians_7_10"></a>1 Corinthians 7:10

And unto the [gameō](../../strongs/g/g1060.md) I [paraggellō](../../strongs/g/g3853.md), yet not I, but the [kyrios](../../strongs/g/g2962.md), Let not the [gynē](../../strongs/g/g1135.md) [chōrizō](../../strongs/g/g5563.md) from her [anēr](../../strongs/g/g435.md):

<a name="1corinthians_7_11"></a>1 Corinthians 7:11

But and if she [chōrizō](../../strongs/g/g5563.md), let her [menō](../../strongs/g/g3306.md) [agamos](../../strongs/g/g22.md) or be [katallassō](../../strongs/g/g2644.md) to her [anēr](../../strongs/g/g435.md): and let not the [anēr](../../strongs/g/g435.md) [aphiēmi](../../strongs/g/g863.md) his [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_7_12"></a>1 Corinthians 7:12

But to [loipos](../../strongs/g/g3062.md) [legō](../../strongs/g/g3004.md) I, not the [kyrios](../../strongs/g/g2962.md): If any [adelphos](../../strongs/g/g80.md) hath a [gynē](../../strongs/g/g1135.md) that [apistos](../../strongs/g/g571.md), and she [syneudokeō](../../strongs/g/g4909.md) to [oikeō](../../strongs/g/g3611.md) with him, let him not [aphiēmi](../../strongs/g/g863.md) her.

<a name="1corinthians_7_13"></a>1 Corinthians 7:13

And the [gynē](../../strongs/g/g1135.md) which hath an [anēr](../../strongs/g/g435.md) that [apistos](../../strongs/g/g571.md), and if he [syneudokeō](../../strongs/g/g4909.md) to [oikeō](../../strongs/g/g3611.md) with her, let her not [aphiēmi](../../strongs/g/g863.md) him.

<a name="1corinthians_7_14"></a>1 Corinthians 7:14

For the [apistos](../../strongs/g/g571.md) [anēr](../../strongs/g/g435.md) is [hagiazō](../../strongs/g/g37.md) by the [gynē](../../strongs/g/g1135.md), and the [apistos](../../strongs/g/g571.md) wife is [hagiazō](../../strongs/g/g37.md) by the [anēr](../../strongs/g/g435.md): else were your [teknon](../../strongs/g/g5043.md) [akathartos](../../strongs/g/g169.md); but now are they [hagios](../../strongs/g/g40.md).

<a name="1corinthians_7_15"></a>1 Corinthians 7:15

But if the [apistos](../../strongs/g/g571.md) [chōrizō](../../strongs/g/g5563.md), let him [chōrizō](../../strongs/g/g5563.md). An [adelphos](../../strongs/g/g80.md) or an [adelphē](../../strongs/g/g79.md) is not [douloō](../../strongs/g/g1402.md) in such cases: but [theos](../../strongs/g/g2316.md) hath [kaleō](../../strongs/g/g2564.md) us to [eirēnē](../../strongs/g/g1515.md).

<a name="1corinthians_7_16"></a>1 Corinthians 7:16

For what [eidō](../../strongs/g/g1492.md) thou, O [gynē](../../strongs/g/g1135.md), whether thou shalt [sōzō](../../strongs/g/g4982.md) thy [anēr](../../strongs/g/g435.md)?  or how [eidō](../../strongs/g/g1492.md) thou, O [anēr](../../strongs/g/g435.md), whether thou shalt [sōzō](../../strongs/g/g4982.md) thy [gynē](../../strongs/g/g1135.md)?

<a name="1corinthians_7_17"></a>1 Corinthians 7:17

But as [theos](../../strongs/g/g2316.md) hath [merizō](../../strongs/g/g3307.md) to [hekastos](../../strongs/g/g1538.md), as the [kyrios](../../strongs/g/g2962.md) hath [kaleō](../../strongs/g/g2564.md) [hekastos](../../strongs/g/g1538.md), so let him [peripateō](../../strongs/g/g4043.md). And so [diatassō](../../strongs/g/g1299.md) I in all [ekklēsia](../../strongs/g/g1577.md).

<a name="1corinthians_7_18"></a>1 Corinthians 7:18

Is any [tis](../../strongs/g/g5100.md) [kaleō](../../strongs/g/g2564.md) being [peritemnō](../../strongs/g/g4059.md)? let him not become [epispaō](../../strongs/g/g1986.md). Is any [kaleō](../../strongs/g/g2564.md) in [akrobystia](../../strongs/g/g203.md)? let him not be [peritemnō](../../strongs/g/g4059.md).

<a name="1corinthians_7_19"></a>1 Corinthians 7:19

[peritomē](../../strongs/g/g4061.md) is [oudeis](../../strongs/g/g3762.md), and [akrobystia](../../strongs/g/g203.md) is [oudeis](../../strongs/g/g3762.md), but the [tērēsis](../../strongs/g/g5084.md) of the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_7_20"></a>1 Corinthians 7:20

Let [hekastos](../../strongs/g/g1538.md) [menō](../../strongs/g/g3306.md) in the same [klēsis](../../strongs/g/g2821.md) wherein he was [kaleō](../../strongs/g/g2564.md).

<a name="1corinthians_7_21"></a>1 Corinthians 7:21

Art thou [kaleō](../../strongs/g/g2564.md) being a [doulos](../../strongs/g/g1401.md)? [melei](../../strongs/g/g3199.md) not for it: but if thou mayest be made [eleutheros](../../strongs/g/g1658.md), [chraomai](../../strongs/g/g5530.md) it rather.

<a name="1corinthians_7_22"></a>1 Corinthians 7:22

For he that is [kaleō](../../strongs/g/g2564.md) in the [kyrios](../../strongs/g/g2962.md), being a [doulos](../../strongs/g/g1401.md), is the [kyrios](../../strongs/g/g2962.md) [apeleutheros](../../strongs/g/g558.md): [homoiōs](../../strongs/g/g3668.md) also he that is [kaleō](../../strongs/g/g2564.md), being [eleutheros](../../strongs/g/g1658.md), is [Christos](../../strongs/g/g5547.md) [doulos](../../strongs/g/g1401.md).

<a name="1corinthians_7_23"></a>1 Corinthians 7:23

Ye are [agorazō](../../strongs/g/g59.md) with a [timē](../../strongs/g/g5092.md); be not ye the [doulos](../../strongs/g/g1401.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="1corinthians_7_24"></a>1 Corinthians 7:24

[adelphos](../../strongs/g/g80.md), let [hekastos](../../strongs/g/g1538.md), wherein he is [kaleō](../../strongs/g/g2564.md), therein [menō](../../strongs/g/g3306.md) with [theos](../../strongs/g/g2316.md).

<a name="1corinthians_7_25"></a>1 Corinthians 7:25

Now concerning [parthenos](../../strongs/g/g3933.md) I have no [epitagē](../../strongs/g/g2003.md) of the [kyrios](../../strongs/g/g2962.md): yet I [didōmi](../../strongs/g/g1325.md) my [gnōmē](../../strongs/g/g1106.md), as one that hath obtained [eleeō](../../strongs/g/g1653.md) of the [kyrios](../../strongs/g/g2962.md) to be [pistos](../../strongs/g/g4103.md).

<a name="1corinthians_7_26"></a>1 Corinthians 7:26

I [nomizō](../../strongs/g/g3543.md) therefore that this is [kalos](../../strongs/g/g2570.md) for the [enistēmi](../../strongs/g/g1764.md) [anagkē](../../strongs/g/g318.md), that it is [kalos](../../strongs/g/g2570.md) for an [anthrōpos](../../strongs/g/g444.md) so to be.

<a name="1corinthians_7_27"></a>1 Corinthians 7:27

Art thou [deō](../../strongs/g/g1210.md) unto a [gynē](../../strongs/g/g1135.md)? [zēteō](../../strongs/g/g2212.md) not to be [lysis](../../strongs/g/g3080.md). Art thou [lyō](../../strongs/g/g3089.md) from a [gynē](../../strongs/g/g1135.md)? [zēteō](../../strongs/g/g2212.md) not a [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_7_28"></a>1 Corinthians 7:28

But and if thou [gameō](../../strongs/g/g1060.md), thou hast not [hamartanō](../../strongs/g/g264.md); and if a [parthenos](../../strongs/g/g3933.md) [gameō](../../strongs/g/g1060.md), she hath not [hamartanō](../../strongs/g/g264.md). Nevertheless such shall have [thlipsis](../../strongs/g/g2347.md) in the [sarx](../../strongs/g/g4561.md): but I [pheidomai](../../strongs/g/g5339.md) you.

<a name="1corinthians_7_29"></a>1 Corinthians 7:29

But this I [phēmi](../../strongs/g/g5346.md), [adelphos](../../strongs/g/g80.md), the [kairos](../../strongs/g/g2540.md) is [systellō](../../strongs/g/g4958.md): it remaineth, that both they that have [gynē](../../strongs/g/g1135.md) be as though they had none;

<a name="1corinthians_7_30"></a>1 Corinthians 7:30

And they that [klaiō](../../strongs/g/g2799.md), as though they [klaiō](../../strongs/g/g2799.md) not; and they that [chairō](../../strongs/g/g5463.md), as though they [chairō](../../strongs/g/g5463.md) not; and they that [agorazō](../../strongs/g/g59.md), as though they [katechō](../../strongs/g/g2722.md) not;

<a name="1corinthians_7_31"></a>1 Corinthians 7:31

And they that [chraomai](../../strongs/g/g5530.md) this [kosmos](../../strongs/g/g2889.md), as not [katachraomai](../../strongs/g/g2710.md): for the [schēma](../../strongs/g/g4976.md) of this [kosmos](../../strongs/g/g2889.md) [paragō](../../strongs/g/g3855.md).

<a name="1corinthians_7_32"></a>1 Corinthians 7:32

But I would have you [amerimnos](../../strongs/g/g275.md). He that is [agamos](../../strongs/g/g22.md) [merimnaō](../../strongs/g/g3309.md) for the things that belong to the [kyrios](../../strongs/g/g2962.md), how he may [areskō](../../strongs/g/g700.md) the [kyrios](../../strongs/g/g2962.md):

<a name="1corinthians_7_33"></a>1 Corinthians 7:33

But he that is [gameō](../../strongs/g/g1060.md) [merimnaō](../../strongs/g/g3309.md) for the things that are of the [kosmos](../../strongs/g/g2889.md), how he may [areskō](../../strongs/g/g700.md) his [gynē](../../strongs/g/g1135.md).

<a name="1corinthians_7_34"></a>1 Corinthians 7:34

There is [merizō](../../strongs/g/g3307.md) a [gynē](../../strongs/g/g1135.md) and a [parthenos](../../strongs/g/g3933.md). The [agamos](../../strongs/g/g22.md) [merimnaō](../../strongs/g/g3309.md) for the things of the [kyrios](../../strongs/g/g2962.md), that she may be [hagios](../../strongs/g/g40.md) both in [sōma](../../strongs/g/g4983.md) and in [pneuma](../../strongs/g/g4151.md): but she that is [gameō](../../strongs/g/g1060.md) [merimnaō](../../strongs/g/g3309.md) for the things of the [kosmos](../../strongs/g/g2889.md), how she may [areskō](../../strongs/g/g700.md) [anēr](../../strongs/g/g435.md).

<a name="1corinthians_7_35"></a>1 Corinthians 7:35

And this I [legō](../../strongs/g/g3004.md) for your own [sympherō](../../strongs/g/g4851.md); not that I may [epiballō](../../strongs/g/g1911.md) a [brochos](../../strongs/g/g1029.md) upon you, but for that which is [euschēmōn](../../strongs/g/g2158.md), and that ye may [euprosedros](../../strongs/g/g2145.md) the [kyrios](../../strongs/g/g2962.md) [aperispastōs](../../strongs/g/g563.md).

<a name="1corinthians_7_36"></a>1 Corinthians 7:36

But [ei tis](../../strongs/g/g1536.md) [nomizō](../../strongs/g/g3543.md) that he [aschēmoneō](../../strongs/g/g807.md) toward his [parthenos](../../strongs/g/g3933.md), if she [hyperakmos](../../strongs/g/g5230.md), and [opheilō](../../strongs/g/g3784.md) so [ginomai](../../strongs/g/g1096.md), let him [poieō](../../strongs/g/g4160.md) what he will, he [hamartanō](../../strongs/g/g264.md) not: let them [gameō](../../strongs/g/g1060.md).

<a name="1corinthians_7_37"></a>1 Corinthians 7:37

Nevertheless he that [histēmi](../../strongs/g/g2476.md) [hedraios](../../strongs/g/g1476.md) in his [kardia](../../strongs/g/g2588.md), having no [anagkē](../../strongs/g/g318.md), but hath [exousia](../../strongs/g/g1849.md) over his own [thelēma](../../strongs/g/g2307.md), and hath so [krinō](../../strongs/g/g2919.md) in his [kardia](../../strongs/g/g2588.md) that he will [tēreō](../../strongs/g/g5083.md) his [parthenos](../../strongs/g/g3933.md), [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md).

<a name="1corinthians_7_38"></a>1 Corinthians 7:38

So then he that [ekgamizō](../../strongs/g/g1547.md) [poieō](../../strongs/g/g4160.md) [kalōs](../../strongs/g/g2573.md); but he that [ekgamizō](../../strongs/g/g1547.md) not [poieō](../../strongs/g/g4160.md) [kreisson](../../strongs/g/g2908.md).

<a name="1corinthians_7_39"></a>1 Corinthians 7:39

The [gynē](../../strongs/g/g1135.md) is [deō](../../strongs/g/g1210.md) by the [nomos](../../strongs/g/g3551.md) as [chronos](../../strongs/g/g5550.md) as her [anēr](../../strongs/g/g435.md) [zaō](../../strongs/g/g2198.md); but if her [anēr](../../strongs/g/g435.md) be [koimaō](../../strongs/g/g2837.md), she is [eleutheros](../../strongs/g/g1658.md) to be [gameō](../../strongs/g/g1060.md) to whom she will; only in the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_7_40"></a>1 Corinthians 7:40

But she is [makarios](../../strongs/g/g3107.md) if she so [menō](../../strongs/g/g3306.md), after my [gnōmē](../../strongs/g/g1106.md): and I [dokeō](../../strongs/g/g1380.md) also that I have the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 6](1corinthians_6.md) - [1Corinthians 8](1corinthians_8.md)