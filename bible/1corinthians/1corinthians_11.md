# [1 Corinthians 11](https://www.blueletterbible.org/kjv/1co/11/1/s_1073001)

<a name="1corinthians_11_1"></a>1 Corinthians 11:1

Be ye [mimētēs](../../strongs/g/g3402.md) of me, even as I also of [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_11_2"></a>1 Corinthians 11:2

Now I [epaineō](../../strongs/g/g1867.md) you, [adelphos](../../strongs/g/g80.md), that ye [mnaomai](../../strongs/g/g3415.md) me in all things, and [katechō](../../strongs/g/g2722.md) the [paradosis](../../strongs/g/g3862.md), as I [paradidōmi](../../strongs/g/g3860.md) them to you.

<a name="1corinthians_11_3"></a>1 Corinthians 11:3

But I would have you [eidō](../../strongs/g/g1492.md), that the [kephalē](../../strongs/g/g2776.md) of every [anēr](../../strongs/g/g435.md) is [Christos](../../strongs/g/g5547.md); and the [kephalē](../../strongs/g/g2776.md) of the [gynē](../../strongs/g/g1135.md) is the [anēr](../../strongs/g/g435.md); and the [kephalē](../../strongs/g/g2776.md) of [Christos](../../strongs/g/g5547.md) is [theos](../../strongs/g/g2316.md).

<a name="1corinthians_11_4"></a>1 Corinthians 11:4

Every [anēr](../../strongs/g/g435.md) [proseuchomai](../../strongs/g/g4336.md) or [prophēteuō](../../strongs/g/g4395.md), having his [kephalē](../../strongs/g/g2776.md) [kata](../../strongs/g/g2596.md), [kataischynō](../../strongs/g/g2617.md) his [kephalē](../../strongs/g/g2776.md).

<a name="1corinthians_11_5"></a>1 Corinthians 11:5

But every [gynē](../../strongs/g/g1135.md) that [proseuchomai](../../strongs/g/g4336.md) or [prophēteuō](../../strongs/g/g4395.md) with her [kephalē](../../strongs/g/g2776.md) [akatakalyptos](../../strongs/g/g177.md) [kataischynō](../../strongs/g/g2617.md) her [kephalē](../../strongs/g/g2776.md): for that is even all one as if she were [xyraō](../../strongs/g/g3587.md).

<a name="1corinthians_11_6"></a>1 Corinthians 11:6

For if the [gynē](../../strongs/g/g1135.md) be not [katakalyptō](../../strongs/g/g2619.md), let her also be [keirō](../../strongs/g/g2751.md): but if it be an [aischron](../../strongs/g/g149.md) for a [gynē](../../strongs/g/g1135.md) to be [keirō](../../strongs/g/g2751.md) or [xyraō](../../strongs/g/g3587.md), let her be [katakalyptō](../../strongs/g/g2619.md).

<a name="1corinthians_11_7"></a>1 Corinthians 11:7

For an [anēr](../../strongs/g/g435.md) indeed [opheilō](../../strongs/g/g3784.md) not to [katakalyptō](../../strongs/g/g2619.md) his [kephalē](../../strongs/g/g2776.md), forasmuch as he is the [eikōn](../../strongs/g/g1504.md) and [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md): but the [gynē](../../strongs/g/g1135.md) is the [doxa](../../strongs/g/g1391.md) of the [anēr](../../strongs/g/g435.md).

<a name="1corinthians_11_8"></a>1 Corinthians 11:8

For the [anēr](../../strongs/g/g435.md) is not of the [gynē](../../strongs/g/g1135.md): but the [gynē](../../strongs/g/g1135.md) of the [anēr](../../strongs/g/g435.md).

<a name="1corinthians_11_9"></a>1 Corinthians 11:9

Neither was the [anēr](../../strongs/g/g435.md) [ktizō](../../strongs/g/g2936.md) for the [gynē](../../strongs/g/g1135.md); but the [gynē](../../strongs/g/g1135.md) for the [anēr](../../strongs/g/g435.md).

<a name="1corinthians_11_10"></a>1 Corinthians 11:10

For this cause [opheilō](../../strongs/g/g3784.md) the [gynē](../../strongs/g/g1135.md) to have [exousia](../../strongs/g/g1849.md) on [kephalē](../../strongs/g/g2776.md) because of the [aggelos](../../strongs/g/g32.md).

<a name="1corinthians_11_11"></a>1 Corinthians 11:11

Nevertheless neither is the [anēr](../../strongs/g/g435.md) without the [gynē](../../strongs/g/g1135.md), neither the [gynē](../../strongs/g/g1135.md) without the [anēr](../../strongs/g/g435.md), in the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_11_12"></a>1 Corinthians 11:12

For as the [gynē](../../strongs/g/g1135.md) of the [anēr](../../strongs/g/g435.md), even so is the [anēr](../../strongs/g/g435.md) also by the [gynē](../../strongs/g/g1135.md); but all things of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_11_13"></a>1 Corinthians 11:13

[krinō](../../strongs/g/g2919.md) in yourselves: is it [prepō](../../strongs/g/g4241.md) that a [gynē](../../strongs/g/g1135.md) [proseuchomai](../../strongs/g/g4336.md) unto [theos](../../strongs/g/g2316.md) [akatakalyptos](../../strongs/g/g177.md)?

<a name="1corinthians_11_14"></a>1 Corinthians 11:14

Doth not even [physis](../../strongs/g/g5449.md) itself [didaskō](../../strongs/g/g1321.md) you, that, if an [anēr](../../strongs/g/g435.md) have [komaō](../../strongs/g/g2863.md), it is an [atimia](../../strongs/g/g819.md) unto him?

<a name="1corinthians_11_15"></a>1 Corinthians 11:15

But if a [gynē](../../strongs/g/g1135.md) have [komaō](../../strongs/g/g2863.md), it is a [doxa](../../strongs/g/g1391.md) to her: for her [komē](../../strongs/g/g2864.md) is [didōmi](../../strongs/g/g1325.md) her for a [peribolaion](../../strongs/g/g4018.md).

<a name="1corinthians_11_16"></a>1 Corinthians 11:16

But [ei tis](../../strongs/g/g1536.md) [dokeō](../../strongs/g/g1380.md) to be [philoneikos](../../strongs/g/g5380.md), we have no such [synētheia](../../strongs/g/g4914.md), neither the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_11_17"></a>1 Corinthians 11:17

Now in this that I [paraggellō](../../strongs/g/g3853.md) unto you I [epaineō](../../strongs/g/g1867.md) you not, that ye [synerchomai](../../strongs/g/g4905.md) not for the [kreittōn](../../strongs/g/g2909.md), but for the [hēssōn](../../strongs/g/g2276.md).

<a name="1corinthians_11_18"></a>1 Corinthians 11:18

For [prōton](../../strongs/g/g4412.md), when ye [synerchomai](../../strongs/g/g4905.md) in the [ekklēsia](../../strongs/g/g1577.md), I [akouō](../../strongs/g/g191.md) that there be [schisma](../../strongs/g/g4978.md) among you; and I [meros](../../strongs/g/g3313.md) [pisteuō](../../strongs/g/g4100.md) it.

<a name="1corinthians_11_19"></a>1 Corinthians 11:19

For there must be also [hairesis](../../strongs/g/g139.md) among you, that they which are [dokimos](../../strongs/g/g1384.md) may be made [phaneros](../../strongs/g/g5318.md) among you.

<a name="1corinthians_11_20"></a>1 Corinthians 11:20

When ye [synerchomai](../../strongs/g/g4905.md) therefore into [autos](../../strongs/g/g846.md), this is not to [phago](../../strongs/g/g5315.md) the [kyriakos](../../strongs/g/g2960.md) [deipnon](../../strongs/g/g1173.md).

<a name="1corinthians_11_21"></a>1 Corinthians 11:21

For in [phago](../../strongs/g/g5315.md) every one [prolambanō](../../strongs/g/g4301.md) other his own [deipnon](../../strongs/g/g1173.md): and one is [peinaō](../../strongs/g/g3983.md), and another is [methyō](../../strongs/g/g3184.md).

<a name="1corinthians_11_22"></a>1 Corinthians 11:22

What? have ye not [oikia](../../strongs/g/g3614.md) to [esthiō](../../strongs/g/g2068.md) and to [pinō](../../strongs/g/g4095.md) in? or [kataphroneō](../../strongs/g/g2706.md) ye the [ekklēsia](../../strongs/g/g1577.md) of [theos](../../strongs/g/g2316.md), and [kataischynō](../../strongs/g/g2617.md) them that have not? What shall I [eipon](../../strongs/g/g2036.md) to you? shall I [epaineō](../../strongs/g/g1867.md) you in this? I [epaineō](../../strongs/g/g1867.md) you not.

<a name="1corinthians_11_23"></a>1 Corinthians 11:23

For I have [paralambanō](../../strongs/g/g3880.md) of the [kyrios](../../strongs/g/g2962.md) that which also I [paradidōmi](../../strongs/g/g3860.md) unto you, That the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) the [nyx](../../strongs/g/g3571.md) in which he was [paradidōmi](../../strongs/g/g3860.md) [lambanō](../../strongs/g/g2983.md) [artos](../../strongs/g/g740.md):

<a name="1corinthians_11_24"></a>1 Corinthians 11:24

And when he had [eucharisteō](../../strongs/g/g2168.md), he [klaō](../../strongs/g/g2806.md) it, and [eipon](../../strongs/g/g2036.md), [lambanō](../../strongs/g/g2983.md), [phago](../../strongs/g/g5315.md): this is my [sōma](../../strongs/g/g4983.md), which is [klaō](../../strongs/g/g2806.md) for you: this [poieō](../../strongs/g/g4160.md) in [anamnēsis](../../strongs/g/g364.md) of me. [^1]

<a name="1corinthians_11_25"></a>1 Corinthians 11:25

[hōsautōs](../../strongs/g/g5615.md) also the [potērion](../../strongs/g/g4221.md), when he had [deipneō](../../strongs/g/g1172.md), [legō](../../strongs/g/g3004.md), This [potērion](../../strongs/g/g4221.md) is the [kainos](../../strongs/g/g2537.md) [diathēkē](../../strongs/g/g1242.md) in my [haima](../../strongs/g/g129.md): this [poieō](../../strongs/g/g4160.md) ye, as oft as ye [pinō](../../strongs/g/g4095.md) it, in [anamnēsis](../../strongs/g/g364.md) of me.

<a name="1corinthians_11_26"></a>1 Corinthians 11:26

For as often as ye [esthiō](../../strongs/g/g2068.md) this [artos](../../strongs/g/g740.md), and [pinō](../../strongs/g/g4095.md) this [potērion](../../strongs/g/g4221.md), ye do [kataggellō](../../strongs/g/g2605.md) the [kyrios](../../strongs/g/g2962.md) [thanatos](../../strongs/g/g2288.md) till he [erchomai](../../strongs/g/g2064.md).

<a name="1corinthians_11_27"></a>1 Corinthians 11:27

Wherefore whosoever shall [esthiō](../../strongs/g/g2068.md) this [artos](../../strongs/g/g740.md), and [pinō](../../strongs/g/g4095.md) this [potērion](../../strongs/g/g4221.md) of the [kyrios](../../strongs/g/g2962.md), [anaxiōs](../../strongs/g/g371.md), shall be [enochos](../../strongs/g/g1777.md) of the [sōma](../../strongs/g/g4983.md) and [haima](../../strongs/g/g129.md) of the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_11_28"></a>1 Corinthians 11:28

But let an [anthrōpos](../../strongs/g/g444.md) [dokimazō](../../strongs/g/g1381.md) himself, and so let him [esthiō](../../strongs/g/g2068.md) of that [artos](../../strongs/g/g740.md), and [pinō](../../strongs/g/g4095.md) of that [potērion](../../strongs/g/g4221.md).

<a name="1corinthians_11_29"></a>1 Corinthians 11:29

For he that [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) [anaxiōs](../../strongs/g/g371.md), [esthiō](../../strongs/g/g2068.md) and [pinō](../../strongs/g/g4095.md) [krima](../../strongs/g/g2917.md) to himself, not [diakrinō](../../strongs/g/g1252.md) the [kyrios](../../strongs/g/g2962.md) [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_11_30"></a>1 Corinthians 11:30

For this cause [polys](../../strongs/g/g4183.md) are [asthenēs](../../strongs/g/g772.md) and [arrōstos](../../strongs/g/g732.md) among you, and [hikanos](../../strongs/g/g2425.md) [koimaō](../../strongs/g/g2837.md).

<a name="1corinthians_11_31"></a>1 Corinthians 11:31

For if we would [diakrinō](../../strongs/g/g1252.md) ourselves, we should not be [krinō](../../strongs/g/g2919.md).

<a name="1corinthians_11_32"></a>1 Corinthians 11:32

But when we are [krinō](../../strongs/g/g2919.md), we are [paideuō](../../strongs/g/g3811.md) of the [kyrios](../../strongs/g/g2962.md), that we should not be [katakrinō](../../strongs/g/g2632.md) with the [kosmos](../../strongs/g/g2889.md).

<a name="1corinthians_11_33"></a>1 Corinthians 11:33

Wherefore, my [adelphos](../../strongs/g/g80.md), when ye [synerchomai](../../strongs/g/g4905.md) to [phago](../../strongs/g/g5315.md), [ekdechomai](../../strongs/g/g1551.md) for [allēlōn](../../strongs/g/g240.md).

<a name="1corinthians_11_34"></a>1 Corinthians 11:34

And [ei tis](../../strongs/g/g1536.md) [peinaō](../../strongs/g/g3983.md), let him [esthiō](../../strongs/g/g2068.md) at [oikos](../../strongs/g/g3624.md); that ye [synerchomai](../../strongs/g/g4905.md) not unto [krima](../../strongs/g/g2917.md). And the [loipos](../../strongs/g/g3062.md) will I [diatassō](../../strongs/g/g1299.md) when I [erchomai](../../strongs/g/g2064.md).

---

[Transliteral Bible](../bible.md)

[1Corinthians](1corinthians.md)

[1Corinthians 10](1corinthians_10.md) - [1Corinthians 12](1corinthians_12.md)

---

[^1]: [1 Corinthians 11:24 Commentary](../../commentary/1corinthians/1corinthians_11_commentary.md#1corinthians_11_24)
