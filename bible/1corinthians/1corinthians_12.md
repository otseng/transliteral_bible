# [1 Corinthians 12](https://www.blueletterbible.org/kjv/1co/12/1/s_1074001)

<a name="1corinthians_12_1"></a>1 Corinthians 12:1

Now concerning [pneumatikos](../../strongs/g/g4152.md), [adelphos](../../strongs/g/g80.md), I would not have you [agnoeō](../../strongs/g/g50.md).

<a name="1corinthians_12_2"></a>1 Corinthians 12:2

Ye [eidō](../../strongs/g/g1492.md) that ye were [ethnos](../../strongs/g/g1484.md), [apagō](../../strongs/g/g520.md) unto these [aphōnos](../../strongs/g/g880.md) [eidōlon](../../strongs/g/g1497.md), even as ye were [agō](../../strongs/g/g71.md).

<a name="1corinthians_12_3"></a>1 Corinthians 12:3

Wherefore I [gnōrizō](../../strongs/g/g1107.md) give you, that [oudeis](../../strongs/g/g3762.md) [laleō](../../strongs/g/g2980.md) by the [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [legō](../../strongs/g/g3004.md) [Iēsous](../../strongs/g/g2424.md) [anathema](../../strongs/g/g331.md): and [oudeis](../../strongs/g/g3762.md) can [eipon](../../strongs/g/g2036.md) that [Iēsous](../../strongs/g/g2424.md) is the [kyrios](../../strongs/g/g2962.md), but by the [hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md).

<a name="1corinthians_12_4"></a>1 Corinthians 12:4

Now there are [diairesis](../../strongs/g/g1243.md) of [charisma](../../strongs/g/g5486.md), but the same [pneuma](../../strongs/g/g4151.md).

<a name="1corinthians_12_5"></a>1 Corinthians 12:5

And there are [diairesis](../../strongs/g/g1243.md) of [diakonia](../../strongs/g/g1248.md), but the same [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_12_6"></a>1 Corinthians 12:6

And there are [diairesis](../../strongs/g/g1243.md) of [energēma](../../strongs/g/g1755.md), but it is the same [theos](../../strongs/g/g2316.md) which [energeō](../../strongs/g/g1754.md) all in all.

<a name="1corinthians_12_7"></a>1 Corinthians 12:7

But the [phanerōsis](../../strongs/g/g5321.md) of the [pneuma](../../strongs/g/g4151.md) is [didōmi](../../strongs/g/g1325.md) to every man to [sympherō](../../strongs/g/g4851.md).

<a name="1corinthians_12_8"></a>1 Corinthians 12:8

For to one is [didōmi](../../strongs/g/g1325.md) by the [pneuma](../../strongs/g/g4151.md) the [logos](../../strongs/g/g3056.md) of [sophia](../../strongs/g/g4678.md); to another the [logos](../../strongs/g/g3056.md) of [gnōsis](../../strongs/g/g1108.md) by the same [pneuma](../../strongs/g/g4151.md);

<a name="1corinthians_12_9"></a>1 Corinthians 12:9

To another [pistis](../../strongs/g/g4102.md) by the same [pneuma](../../strongs/g/g4151.md); to another the [charisma](../../strongs/g/g5486.md) of [iama](../../strongs/g/g2386.md) by the same [pneuma](../../strongs/g/g4151.md);

<a name="1corinthians_12_10"></a>1 Corinthians 12:10

To another the [energēma](../../strongs/g/g1755.md) of [dynamis](../../strongs/g/g1411.md); to another [prophēteia](../../strongs/g/g4394.md); to another [diakrisis](../../strongs/g/g1253.md) of [pneuma](../../strongs/g/g4151.md); to another [genos](../../strongs/g/g1085.md) of [glōssa](../../strongs/g/g1100.md); to another the [hermēneia](../../strongs/g/g2058.md) of [glōssa](../../strongs/g/g1100.md):

<a name="1corinthians_12_11"></a>1 Corinthians 12:11

But all these [energeō](../../strongs/g/g1754.md) that one and the selfsame [pneuma](../../strongs/g/g4151.md), [diaireō](../../strongs/g/g1244.md) to [hekastos](../../strongs/g/g1538.md) [idios](../../strongs/g/g2398.md) as he [boulomai](../../strongs/g/g1014.md).

<a name="1corinthians_12_12"></a>1 Corinthians 12:12

For as the [sōma](../../strongs/g/g4983.md) is one, and hath [polys](../../strongs/g/g4183.md) [melos](../../strongs/g/g3196.md), and all the [melos](../../strongs/g/g3196.md) of that one [sōma](../../strongs/g/g4983.md), being [polys](../../strongs/g/g4183.md), are one [sōma](../../strongs/g/g4983.md): so also is [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_12_13"></a>1 Corinthians 12:13

For by one [pneuma](../../strongs/g/g4151.md) are we all [baptizō](../../strongs/g/g907.md) into one [sōma](../../strongs/g/g4983.md), whether we be [Ioudaios](../../strongs/g/g2453.md) or [Hellēn](../../strongs/g/g1672.md), whether we be [doulos](../../strongs/g/g1401.md) or [eleutheros](../../strongs/g/g1658.md); and have been all made to [potizō](../../strongs/g/g4222.md) into one [pneuma](../../strongs/g/g4151.md).

<a name="1corinthians_12_14"></a>1 Corinthians 12:14

For the [sōma](../../strongs/g/g4983.md) is not one [melos](../../strongs/g/g3196.md), but [polys](../../strongs/g/g4183.md).

<a name="1corinthians_12_15"></a>1 Corinthians 12:15

If the [pous](../../strongs/g/g4228.md) shall [eipon](../../strongs/g/g2036.md), Because I am not the [cheir](../../strongs/g/g5495.md), I am not of the [sōma](../../strongs/g/g4983.md); is it therefore not of the [sōma](../../strongs/g/g4983.md)?

<a name="1corinthians_12_16"></a>1 Corinthians 12:16

And if the [ous](../../strongs/g/g3775.md) shall [eipon](../../strongs/g/g2036.md), Because I am not the [ophthalmos](../../strongs/g/g3788.md), I am not of the [sōma](../../strongs/g/g4983.md); is it therefore not of the [sōma](../../strongs/g/g4983.md)?

<a name="1corinthians_12_17"></a>1 Corinthians 12:17

If the [holos](../../strongs/g/g3650.md) [sōma](../../strongs/g/g4983.md) were an [ophthalmos](../../strongs/g/g3788.md), where were the [akoē](../../strongs/g/g189.md)? If the [holos](../../strongs/g/g3650.md) were [akoē](../../strongs/g/g189.md), where were the [osphrēsis](../../strongs/g/g3750.md)?

<a name="1corinthians_12_18"></a>1 Corinthians 12:18

But [nyni](../../strongs/g/g3570.md) hath [theos](../../strongs/g/g2316.md) [tithēmi](../../strongs/g/g5087.md) the [melos](../../strongs/g/g3196.md) every one of them in the [sōma](../../strongs/g/g4983.md), as [thelō](../../strongs/g/g2309.md).

<a name="1corinthians_12_19"></a>1 Corinthians 12:19

And if they were all one [melos](../../strongs/g/g3196.md), where were the [sōma](../../strongs/g/g4983.md)?

<a name="1corinthians_12_20"></a>1 Corinthians 12:20

But now [polys](../../strongs/g/g4183.md) [melos](../../strongs/g/g3196.md), yet but one [sōma](../../strongs/g/g4983.md).

<a name="1corinthians_12_21"></a>1 Corinthians 12:21

And the [ophthalmos](../../strongs/g/g3788.md) cannot [eipon](../../strongs/g/g2036.md) unto the [cheir](../../strongs/g/g5495.md), I have no [chreia](../../strongs/g/g5532.md) of thee: nor again the [kephalē](../../strongs/g/g2776.md) to the [pous](../../strongs/g/g4228.md), I have no [chreia](../../strongs/g/g5532.md) of you.

<a name="1corinthians_12_22"></a>1 Corinthians 12:22

Nay, [polys](../../strongs/g/g4183.md) [mallon](../../strongs/g/g3123.md) those [melos](../../strongs/g/g3196.md) of the [sōma](../../strongs/g/g4983.md), which [dokeō](../../strongs/g/g1380.md) to be [asthenēs](../../strongs/g/g772.md), are [anagkaios](../../strongs/g/g316.md):

<a name="1corinthians_12_23"></a>1 Corinthians 12:23

And those of the [sōma](../../strongs/g/g4983.md), which we [dokeō](../../strongs/g/g1380.md) to be [atimos](../../strongs/g/g820.md), upon these we [peritithēmi](../../strongs/g/g4060.md) [perissoteros](../../strongs/g/g4055.md) [timē](../../strongs/g/g5092.md); and our [aschēmōn](../../strongs/g/g809.md) parts have more [perissoteros](../../strongs/g/g4055.md) [euschēmosynē](../../strongs/g/g2157.md).

<a name="1corinthians_12_24"></a>1 Corinthians 12:24

For our [euschēmōn](../../strongs/g/g2158.md) parts have no [chreia](../../strongs/g/g5532.md): but [theos](../../strongs/g/g2316.md) hath [sygkerannymi](../../strongs/g/g4786.md) the [sōma](../../strongs/g/g4983.md), having [didōmi](../../strongs/g/g1325.md) [perissoteros](../../strongs/g/g4055.md) [timē](../../strongs/g/g5092.md) to [hystereō](../../strongs/g/g5302.md).

<a name="1corinthians_12_25"></a>1 Corinthians 12:25

That there should be no [schisma](../../strongs/g/g4978.md) in the [sōma](../../strongs/g/g4983.md); but that the [melos](../../strongs/g/g3196.md) should have the same [merimnaō](../../strongs/g/g3309.md) for [allēlōn](../../strongs/g/g240.md).

<a name="1corinthians_12_26"></a>1 Corinthians 12:26

And whether one [melos](../../strongs/g/g3196.md) [paschō](../../strongs/g/g3958.md), all the [melos](../../strongs/g/g3196.md) [sympaschō](../../strongs/g/g4841.md); or one [melos](../../strongs/g/g3196.md) [doxazō](../../strongs/g/g1392.md), all the [melos](../../strongs/g/g3196.md) [sygchairō](../../strongs/g/g4796.md).

<a name="1corinthians_12_27"></a>1 Corinthians 12:27

Now ye are the [sōma](../../strongs/g/g4983.md) of [Christos](../../strongs/g/g5547.md), and [melos](../../strongs/g/g3196.md) in [meros](../../strongs/g/g3313.md).

<a name="1corinthians_12_28"></a>1 Corinthians 12:28

And [theos](../../strongs/g/g2316.md) hath [tithēmi](../../strongs/g/g5087.md) some in the [ekklēsia](../../strongs/g/g1577.md), first [apostolos](../../strongs/g/g652.md), secondarily [prophētēs](../../strongs/g/g4396.md), thirdly [didaskalos](../../strongs/g/g1320.md), after that [dynamis](../../strongs/g/g1411.md), then [charisma](../../strongs/g/g5486.md) of [iama](../../strongs/g/g2386.md), [antilēmpsis](../../strongs/g/g484.md), [kybernēsis](../../strongs/g/g2941.md), [genos](../../strongs/g/g1085.md) of [glōssa](../../strongs/g/g1100.md).

<a name="1corinthians_12_29"></a>1 Corinthians 12:29

Are all [apostolos](../../strongs/g/g652.md)? are all [prophētēs](../../strongs/g/g4396.md)? are all [didaskalos](../../strongs/g/g1320.md)? are all [dynamis](../../strongs/g/g1411.md)?

<a name="1corinthians_12_30"></a>1 Corinthians 12:30

Have all the [charisma](../../strongs/g/g5486.md) of [iama](../../strongs/g/g2386.md)? do all [laleō](../../strongs/g/g2980.md) with [glōssa](../../strongs/g/g1100.md)? do all [diermēneuō](../../strongs/g/g1329.md)?

<a name="1corinthians_12_31"></a>1 Corinthians 12:31

But [zēloō](../../strongs/g/g2206.md) the [kreittōn](../../strongs/g/g2909.md) [charisma](../../strongs/g/g5486.md): and yet [deiknyō](../../strongs/g/g1166.md) I unto you a more [hyperbolē](../../strongs/g/g5236.md) [hodos](../../strongs/g/g3598.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 11](1corinthians_11.md) - [1 Corinthians 13](1corinthians_13.md)