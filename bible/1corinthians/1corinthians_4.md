# [1 Corinthians 4](https://www.blueletterbible.org/kjv/1co/4/1/s_1066001)

<a name="1corinthians_4_1"></a>1 Corinthians 4:1

Let an [anthrōpos](../../strongs/g/g444.md) so [logizomai](../../strongs/g/g3049.md) of us, as of the [hypēretēs](../../strongs/g/g5257.md) of [Christos](../../strongs/g/g5547.md), and [oikonomos](../../strongs/g/g3623.md) of the [mystērion](../../strongs/g/g3466.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_4_2"></a>1 Corinthians 4:2

Moreover it is [zēteō](../../strongs/g/g2212.md) in [oikonomos](../../strongs/g/g3623.md), that a [tis](../../strongs/g/g5100.md) be [heuriskō](../../strongs/g/g2147.md) [pistos](../../strongs/g/g4103.md).

<a name="1corinthians_4_3"></a>1 Corinthians 4:3

But with me it is an [elachistos](../../strongs/g/g1646.md) that I should be [anakrinō](../../strongs/g/g350.md) of you, or of [anthrōpinos](../../strongs/g/g442.md) [hēmera](../../strongs/g/g2250.md): yea, I [anakrinō](../../strongs/g/g350.md) not mine own self.

<a name="1corinthians_4_4"></a>1 Corinthians 4:4

For I [synoraō](../../strongs/g/g4894.md) nothing by myself; yet am I not hereby [dikaioō](../../strongs/g/g1344.md): but he that [anakrinō](../../strongs/g/g350.md) me is the [kyrios](../../strongs/g/g2962.md).

<a name="1corinthians_4_5"></a>1 Corinthians 4:5

Therefore [krinō](../../strongs/g/g2919.md) nothing before the [kairos](../../strongs/g/g2540.md), until the [kyrios](../../strongs/g/g2962.md) [erchomai](../../strongs/g/g2064.md), who both will [phōtizō](../../strongs/g/g5461.md) the [kryptos](../../strongs/g/g2927.md) of [skotos](../../strongs/g/g4655.md), and will [phaneroō](../../strongs/g/g5319.md) the [boulē](../../strongs/g/g1012.md) of the [kardia](../../strongs/g/g2588.md): and then shall [hekastos](../../strongs/g/g1538.md) have [epainos](../../strongs/g/g1868.md) of [theos](../../strongs/g/g2316.md).

<a name="1corinthians_4_6"></a>1 Corinthians 4:6

And these things, [adelphos](../../strongs/g/g80.md), I have [metaschēmatizō](../../strongs/g/g3345.md) to myself and to [Apollōs](../../strongs/g/g625.md) for your sakes; that ye might [manthanō](../../strongs/g/g3129.md) in us not to [phroneō](../../strongs/g/g5426.md) above that which is [graphō](../../strongs/g/g1125.md), that no one of you be [physioō](../../strongs/g/g5448.md) for one against another.

<a name="1corinthians_4_7"></a>1 Corinthians 4:7

For who maketh thee to [diakrinō](../../strongs/g/g1252.md) from another? and what hast thou that thou didst not [lambanō](../../strongs/g/g2983.md)? now if thou didst [lambanō](../../strongs/g/g2983.md) it, why dost thou [kauchaomai](../../strongs/g/g2744.md), as if thou hadst not [lambanō](../../strongs/g/g2983.md) it?

<a name="1corinthians_4_8"></a>1 Corinthians 4:8

Now ye are [korennymi](../../strongs/g/g2880.md), now ye are [plouteō](../../strongs/g/g4147.md), ye have [basileuō](../../strongs/g/g936.md) without us: and I [ophelon](../../strongs/g/g3785.md) ye did [basileuō](../../strongs/g/g936.md), that we also might [symbasileuō](../../strongs/g/g4821.md) you.

<a name="1corinthians_4_9"></a>1 Corinthians 4:9

For I [dokeō](../../strongs/g/g1380.md) that [theos](../../strongs/g/g2316.md) hath [apodeiknymi](../../strongs/g/g584.md) us the [apostolos](../../strongs/g/g652.md) [eschatos](../../strongs/g/g2078.md), as it were [epithanatios](../../strongs/g/g1935.md): for we are made a [theatron](../../strongs/g/g2302.md) unto the [kosmos](../../strongs/g/g2889.md), and to [aggelos](../../strongs/g/g32.md), and to [anthrōpos](../../strongs/g/g444.md).

<a name="1corinthians_4_10"></a>1 Corinthians 4:10

We are [mōros](../../strongs/g/g3474.md) for [Christos](../../strongs/g/g5547.md) sake, but ye are [phronimos](../../strongs/g/g5429.md) in [Christos](../../strongs/g/g5547.md); we are [asthenēs](../../strongs/g/g772.md), but ye are [ischyros](../../strongs/g/g2478.md); ye are [endoxos](../../strongs/g/g1741.md), but we are [atimos](../../strongs/g/g820.md).

<a name="1corinthians_4_11"></a>1 Corinthians 4:11

Even unto this [arti](../../strongs/g/g737.md) [hōra](../../strongs/g/g5610.md) we both [peinaō](../../strongs/g/g3983.md), and [dipsaō](../../strongs/g/g1372.md), and [gymnēteuō](../../strongs/g/g1130.md), and [kolaphizō](../../strongs/g/g2852.md), and [astateō](../../strongs/g/g790.md);

<a name="1corinthians_4_12"></a>1 Corinthians 4:12

And [kopiaō](../../strongs/g/g2872.md), [ergazomai](../../strongs/g/g2038.md) with our own [cheir](../../strongs/g/g5495.md): being [loidoreō](../../strongs/g/g3058.md), we [eulogeō](../../strongs/g/g2127.md); being [diōkō](../../strongs/g/g1377.md), we [anechō](../../strongs/g/g430.md) it:

<a name="1corinthians_4_13"></a>1 Corinthians 4:13

Being [blasphēmeō](../../strongs/g/g987.md), we [parakaleō](../../strongs/g/g3870.md): we are made as the [perikatharma](../../strongs/g/g4027.md) of the [kosmos](../../strongs/g/g2889.md), and are the [peripsēma](../../strongs/g/g4067.md) of all things unto [arti](../../strongs/g/g737.md).

<a name="1corinthians_4_14"></a>1 Corinthians 4:14

I [graphō](../../strongs/g/g1125.md) not these things to [entrepō](../../strongs/g/g1788.md) you, but as my [agapētos](../../strongs/g/g27.md) [teknon](../../strongs/g/g5043.md) I [noutheteō](../../strongs/g/g3560.md) you.

<a name="1corinthians_4_15"></a>1 Corinthians 4:15

For though ye have [myrios](../../strongs/g/g3463.md) [paidagōgos](../../strongs/g/g3807.md) in [Christos](../../strongs/g/g5547.md), yet not [polys](../../strongs/g/g4183.md) [patēr](../../strongs/g/g3962.md): for in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md) I have [gennaō](../../strongs/g/g1080.md) you through the [euaggelion](../../strongs/g/g2098.md).

<a name="1corinthians_4_16"></a>1 Corinthians 4:16

Wherefore I [parakaleō](../../strongs/g/g3870.md) you, be ye [mimētēs](../../strongs/g/g3402.md) of me.

<a name="1corinthians_4_17"></a>1 Corinthians 4:17

For this cause have I [pempō](../../strongs/g/g3992.md) unto you [Timotheos](../../strongs/g/g5095.md), who is my [agapētos](../../strongs/g/g27.md) [teknon](../../strongs/g/g5043.md), and [pistos](../../strongs/g/g4103.md) in the [kyrios](../../strongs/g/g2962.md), who shall [anamimnēskō](../../strongs/g/g363.md) you of my [hodos](../../strongs/g/g3598.md) which be in [Christos](../../strongs/g/g5547.md), as I [didaskō](../../strongs/g/g1321.md) [pantachou](../../strongs/g/g3837.md) in every [ekklēsia](../../strongs/g/g1577.md).

<a name="1corinthians_4_18"></a>1 Corinthians 4:18

Now some are [physioō](../../strongs/g/g5448.md), as though I would not [erchomai](../../strongs/g/g2064.md) to you.

<a name="1corinthians_4_19"></a>1 Corinthians 4:19

But I will [erchomai](../../strongs/g/g2064.md) to you [tacheōs](../../strongs/g/g5030.md), if the [kyrios](../../strongs/g/g2962.md) [thelō](../../strongs/g/g2309.md), and will [ginōskō](../../strongs/g/g1097.md), not the [logos](../../strongs/g/g3056.md) of [physioō](../../strongs/g/g5448.md), but [dynamis](../../strongs/g/g1411.md).

<a name="1corinthians_4_20"></a>1 Corinthians 4:20

For the [basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md) is not in [logos](../../strongs/g/g3056.md), but in [dynamis](../../strongs/g/g1411.md).

<a name="1corinthians_4_21"></a>1 Corinthians 4:21

What [thelō](../../strongs/g/g2309.md) ye? shall I [erchomai](../../strongs/g/g2064.md) unto you with a [rhabdos](../../strongs/g/g4464.md), or in [agapē](../../strongs/g/g26.md), and in the [pneuma](../../strongs/g/g4151.md) of [praotēs](../../strongs/g/g4236.md)?

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 3](1corinthians_3.md) - [1 Corinthians 5](1corinthians_5.md)