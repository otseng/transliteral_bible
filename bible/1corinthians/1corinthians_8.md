# [1 Corinthians 8](https://www.blueletterbible.org/kjv/1co/8/1/s_1070001)

<a name="1corinthians_8_1"></a>1 Corinthians 8:1

Now as touching [eidōlothytos](../../strongs/g/g1494.md), we [eidō](../../strongs/g/g1492.md) that we all have [gnōsis](../../strongs/g/g1108.md). [gnōsis](../../strongs/g/g1108.md) [physioō](../../strongs/g/g5448.md), but [agapē](../../strongs/g/g26.md) [oikodomeō](../../strongs/g/g3618.md).

<a name="1corinthians_8_2"></a>1 Corinthians 8:2

And [ei tis](../../strongs/g/g1536.md) [dokeō](../../strongs/g/g1380.md) that he [eidō](../../strongs/g/g1492.md) any thing, he [ginōskō](../../strongs/g/g1097.md) nothing yet as he ought to [ginōskō](../../strongs/g/g1097.md).

<a name="1corinthians_8_3"></a>1 Corinthians 8:3

But [ei tis](../../strongs/g/g1536.md) [agapaō](../../strongs/g/g25.md) [theos](../../strongs/g/g2316.md), the same is [ginōskō](../../strongs/g/g1097.md) of him.

<a name="1corinthians_8_4"></a>1 Corinthians 8:4

As concerning therefore the [brōsis](../../strongs/g/g1035.md) of [eidōlothytos](../../strongs/g/g1494.md), we [eidō](../../strongs/g/g1492.md) that an [eidōlon](../../strongs/g/g1497.md) is nothing in the [kosmos](../../strongs/g/g2889.md), and that there is none other [theos](../../strongs/g/g2316.md) but one.

<a name="1corinthians_8_5"></a>1 Corinthians 8:5

For though there be that are [legō](../../strongs/g/g3004.md) [theos](../../strongs/g/g2316.md), whether in [ouranos](../../strongs/g/g3772.md) or in [gē](../../strongs/g/g1093.md), (as there be [theos](../../strongs/g/g2316.md) [polys](../../strongs/g/g4183.md), and [kyrios](../../strongs/g/g2962.md) [polys](../../strongs/g/g4183.md),)

<a name="1corinthians_8_6"></a>1 Corinthians 8:6

But to us one [theos](../../strongs/g/g2316.md), the [patēr](../../strongs/g/g3962.md), of whom are all things, and we in him; and one [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), by whom are all things, and we by him.

<a name="1corinthians_8_7"></a>1 Corinthians 8:7

Howbeit not in [pas](../../strongs/g/g3956.md) that [gnōsis](../../strongs/g/g1108.md): for some with [syneidēsis](../../strongs/g/g4893.md) of the [eidōlon](../../strongs/g/g1497.md) unto [arti](../../strongs/g/g737.md) [esthiō](../../strongs/g/g2068.md) it as [eidōlothytos](../../strongs/g/g1494.md); and their [syneidēsis](../../strongs/g/g4893.md) being [asthenēs](../../strongs/g/g772.md) is [molynō](../../strongs/g/g3435.md).

<a name="1corinthians_8_8"></a>1 Corinthians 8:8

But [brōma](../../strongs/g/g1033.md) [paristēmi](../../strongs/g/g3936.md) us not to [theos](../../strongs/g/g2316.md): for neither, if we [phago](../../strongs/g/g5315.md), are we the [perisseuō](../../strongs/g/g4052.md); neither, if we [phago](../../strongs/g/g5315.md) not, are we [hystereō](../../strongs/g/g5302.md).

<a name="1corinthians_8_9"></a>1 Corinthians 8:9

But [blepō](../../strongs/g/g991.md) lest by any means this [exousia](../../strongs/g/g1849.md) of your's become a [proskomma](../../strongs/g/g4348.md) to [astheneō](../../strongs/g/g770.md).

<a name="1corinthians_8_10"></a>1 Corinthians 8:10

For if any [tis](../../strongs/g/g5100.md) [eidō](../../strongs/g/g1492.md) thee which hast [gnōsis](../../strongs/g/g1108.md) [katakeimai](../../strongs/g/g2621.md) in [eidōleion](../../strongs/g/g1493.md), shall not the [syneidēsis](../../strongs/g/g4893.md) of him which is [asthenēs](../../strongs/g/g772.md) [oikodomeō](../../strongs/g/g3618.md) to [esthiō](../../strongs/g/g2068.md) [eidōlothytos](../../strongs/g/g1494.md);

<a name="1corinthians_8_11"></a>1 Corinthians 8:11

And through thy [gnōsis](../../strongs/g/g1108.md) shall the [astheneō](../../strongs/g/g770.md) [adelphos](../../strongs/g/g80.md) [apollymi](../../strongs/g/g622.md), for whom [Christos](../../strongs/g/g5547.md) [apothnēskō](../../strongs/g/g599.md)?

<a name="1corinthians_8_12"></a>1 Corinthians 8:12

But when ye [hamartanō](../../strongs/g/g264.md) so against the [adelphos](../../strongs/g/g80.md), and [typtō](../../strongs/g/g5180.md) their [astheneō](../../strongs/g/g770.md) [syneidēsis](../../strongs/g/g4893.md), ye [hamartanō](../../strongs/g/g264.md) against [Christos](../../strongs/g/g5547.md).

<a name="1corinthians_8_13"></a>1 Corinthians 8:13

Wherefore, if [brōma](../../strongs/g/g1033.md) [skandalizō](../../strongs/g/g4624.md) my [adelphos](../../strongs/g/g80.md), I will [phago](../../strongs/g/g5315.md) no [kreas](../../strongs/g/g2907.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md), lest I [skandalizō](../../strongs/g/g4624.md) my [adelphos](../../strongs/g/g80.md).

---

[Transliteral Bible](../bible.md)

[1 Corinthians](1corinthians.md)

[1 Corinthians 7](1corinthians_7.md) - [1 Corinthians 9](1corinthians_9.md)