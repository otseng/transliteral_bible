# [Revelation 7](https://www.blueletterbible.org/kjv/rev/7/1/s_1174001)

<a name="revelation_7_1"></a>Revelation 7:1

And after these things I [eidō](../../strongs/g/g1492.md) four [aggelos](../../strongs/g/g32.md) [histēmi](../../strongs/g/g2476.md) on the four [gōnia](../../strongs/g/g1137.md) of [gē](../../strongs/g/g1093.md), [krateō](../../strongs/g/g2902.md) the four [anemos](../../strongs/g/g417.md) of [gē](../../strongs/g/g1093.md), that the [anemos](../../strongs/g/g417.md) should not [pneō](../../strongs/g/g4154.md) on [gē](../../strongs/g/g1093.md), nor on the [thalassa](../../strongs/g/g2281.md), nor on any [dendron](../../strongs/g/g1186.md).

<a name="revelation_7_2"></a>Revelation 7:2

And I [eidō](../../strongs/g/g1492.md) another [aggelos](../../strongs/g/g32.md) [anabainō](../../strongs/g/g305.md) from the [anatolē](../../strongs/g/g395.md) [hēlios](../../strongs/g/g2246.md), having the [sphragis](../../strongs/g/g4973.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md): and he [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) to the four [aggelos](../../strongs/g/g32.md), to whom it was [didōmi](../../strongs/g/g1325.md) to [adikeō](../../strongs/g/g91.md) [gē](../../strongs/g/g1093.md) and the [thalassa](../../strongs/g/g2281.md),

<a name="revelation_7_3"></a>Revelation 7:3

[legō](../../strongs/g/g3004.md), [adikeō](../../strongs/g/g91.md) not [gē](../../strongs/g/g1093.md), neither the [thalassa](../../strongs/g/g2281.md), nor the [dendron](../../strongs/g/g1186.md), till we have [sphragizō](../../strongs/g/g4972.md) the [doulos](../../strongs/g/g1401.md) of our [theos](../../strongs/g/g2316.md) in their [metōpon](../../strongs/g/g3359.md).

<a name="revelation_7_4"></a>Revelation 7:4

And I [akouō](../../strongs/g/g191.md) the [arithmos](../../strongs/g/g706.md) of them which were [sphragizō](../../strongs/g/g4972.md): were [sphragizō](../../strongs/g/g4972.md) an hundred forty four thousand of all the [phylē](../../strongs/g/g5443.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md).

<a name="revelation_7_5"></a>Revelation 7:5

Of the [phylē](../../strongs/g/g5443.md) of [Ioudas](../../strongs/g/g2455.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Rhoubēn](../../strongs/g/g4502.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Gad](../../strongs/g/g1045.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand.

<a name="revelation_7_6"></a>Revelation 7:6

Of the [phylē](../../strongs/g/g5443.md) of [Asēr](../../strongs/g/g768.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Nephthalim](../../strongs/g/g3508.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Manassēs](../../strongs/g/g3128.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand.

<a name="revelation_7_7"></a>Revelation 7:7

Of the [phylē](../../strongs/g/g5443.md) of [Symeōn](../../strongs/g/g4826.md) [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Leui](../../strongs/g/g3017.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [issachar](../../strongs/g/g2466.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand.

<a name="revelation_7_8"></a>Revelation 7:8

Of the [phylē](../../strongs/g/g5443.md) of [Zaboulōn](../../strongs/g/g2194.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Iōsēph](../../strongs/g/g2501.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand. Of the [phylē](../../strongs/g/g5443.md) of [Beniam(e)in](../../strongs/g/g958.md) were [sphragizō](../../strongs/g/g4972.md) twelve thousand.

<a name="revelation_7_9"></a>Revelation 7:9

After this I [eidō](../../strongs/g/g1492.md), and, [idou](../../strongs/g/g2400.md), a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md), which [oudeis](../../strongs/g/g3762.md) could [arithmeō](../../strongs/g/g705.md), of all [ethnos](../../strongs/g/g1484.md), and [phylē](../../strongs/g/g5443.md), and [laos](../../strongs/g/g2992.md), and [glōssa](../../strongs/g/g1100.md), [histēmi](../../strongs/g/g2476.md) before the [thronos](../../strongs/g/g2362.md), and before the [arnion](../../strongs/g/g721.md), [periballō](../../strongs/g/g4016.md) with [leukos](../../strongs/g/g3022.md) [stolē](../../strongs/g/g4749.md), and [phoinix](../../strongs/g/g5404.md) in their [cheir](../../strongs/g/g5495.md);

<a name="revelation_7_10"></a>Revelation 7:10

And [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), [sōtēria](../../strongs/g/g4991.md) to our [theos](../../strongs/g/g2316.md) which [kathēmai](../../strongs/g/g2521.md) upon the [thronos](../../strongs/g/g2362.md), and unto the [arnion](../../strongs/g/g721.md).

<a name="revelation_7_11"></a>Revelation 7:11

And all the [aggelos](../../strongs/g/g32.md) [histēmi](../../strongs/g/g2476.md) [kyklō](../../strongs/g/g2945.md) the [thronos](../../strongs/g/g2362.md), and about the [presbyteros](../../strongs/g/g4245.md) and the four [zōon](../../strongs/g/g2226.md), and [piptō](../../strongs/g/g4098.md) before the [thronos](../../strongs/g/g2362.md) on their [prosōpon](../../strongs/g/g4383.md), and [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md),

<a name="revelation_7_12"></a>Revelation 7:12

[legō](../../strongs/g/g3004.md), [amēn](../../strongs/g/g281.md): [eulogia](../../strongs/g/g2129.md), and [doxa](../../strongs/g/g1391.md), and [sophia](../../strongs/g/g4678.md), and [eucharistia](../../strongs/g/g2169.md), and [timē](../../strongs/g/g5092.md), and [dynamis](../../strongs/g/g1411.md), and [ischys](../../strongs/g/g2479.md), be unto our [theos](../../strongs/g/g2316.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="revelation_7_13"></a>Revelation 7:13

And one of the [presbyteros](../../strongs/g/g4245.md) [apokrinomai](../../strongs/g/g611.md), [legō](../../strongs/g/g3004.md) unto me, What are these which are [periballō](../../strongs/g/g4016.md) in [leukos](../../strongs/g/g3022.md) [stolē](../../strongs/g/g4749.md)? and whence [erchomai](../../strongs/g/g2064.md) they?

<a name="revelation_7_14"></a>Revelation 7:14

And I [eipon](../../strongs/g/g2046.md) unto him, [kyrios](../../strongs/g/g2962.md), thou [eidō](../../strongs/g/g1492.md). And he [eipon](../../strongs/g/g2036.md) to me, These are they which [erchomai](../../strongs/g/g2064.md) out of [megas](../../strongs/g/g3173.md) [thlipsis](../../strongs/g/g2347.md), and have [plynō](../../strongs/g/g4150.md) their [stolē](../../strongs/g/g4749.md), and [leukainō](../../strongs/g/g3021.md) them in the [haima](../../strongs/g/g129.md) of the [arnion](../../strongs/g/g721.md).

<a name="revelation_7_15"></a>Revelation 7:15

Therefore are they before the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md), and [latreuō](../../strongs/g/g3000.md) him [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md) in his [naos](../../strongs/g/g3485.md): and he that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md) shall [skēnoō](../../strongs/g/g4637.md) among them.

<a name="revelation_7_16"></a>Revelation 7:16

They shall [peinaō](../../strongs/g/g3983.md) no more, neither [dipsaō](../../strongs/g/g1372.md) any more; neither shall the [hēlios](../../strongs/g/g2246.md) [piptō](../../strongs/g/g4098.md) on them, nor any [kauma](../../strongs/g/g2738.md).

<a name="revelation_7_17"></a>Revelation 7:17

For the [arnion](../../strongs/g/g721.md) which is in the midst of the [thronos](../../strongs/g/g2362.md) shall [poimainō](../../strongs/g/g4165.md) them, and shall [hodēgeō](../../strongs/g/g3594.md) them unto [zaō](../../strongs/g/g2198.md) [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md): and [theos](../../strongs/g/g2316.md) shall [exaleiphō](../../strongs/g/g1813.md) all [dakry](../../strongs/g/g1144.md) from their [ophthalmos](../../strongs/g/g3788.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 6](revelation_6.md) - [Revelation 8](revelation_8.md)