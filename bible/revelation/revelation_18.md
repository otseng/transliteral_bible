# [Revelation 18](https://www.blueletterbible.org/kjv/rev/18/1/s_1185001)

<a name="revelation_18_1"></a>Revelation 18:1

And after these things I [eidō](../../strongs/g/g1492.md) another [aggelos](../../strongs/g/g32.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), having [megas](../../strongs/g/g3173.md) [exousia](../../strongs/g/g1849.md); and [gē](../../strongs/g/g1093.md) was [phōtizō](../../strongs/g/g5461.md) with his [doxa](../../strongs/g/g1391.md).

<a name="revelation_18_2"></a>Revelation 18:2

And he [krazō](../../strongs/g/g2896.md) [ischys](../../strongs/g/g2479.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), [Babylōn](../../strongs/g/g897.md) the [megas](../../strongs/g/g3173.md) is [piptō](../../strongs/g/g4098.md), is [piptō](../../strongs/g/g4098.md), and is become the [katoikētērion](../../strongs/g/g2732.md) of [daimōn](../../strongs/g/g1142.md), and the [phylakē](../../strongs/g/g5438.md) of every [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md), and a [phylakē](../../strongs/g/g5438.md) of every [akathartos](../../strongs/g/g169.md) and [miseō](../../strongs/g/g3404.md) [orneon](../../strongs/g/g3732.md).

<a name="revelation_18_3"></a>Revelation 18:3

For all [ethnos](../../strongs/g/g1484.md) have [pinō](../../strongs/g/g4095.md) of the [oinos](../../strongs/g/g3631.md) of the [thymos](../../strongs/g/g2372.md) of her [porneia](../../strongs/g/g4202.md), and the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md) have [porneuō](../../strongs/g/g4203.md) with her, and the [emporos](../../strongs/g/g1713.md) of [gē](../../strongs/g/g1093.md) are [plouteō](../../strongs/g/g4147.md) through the [dynamis](../../strongs/g/g1411.md) of her [strēnos](../../strongs/g/g4764.md).

<a name="revelation_18_4"></a>Revelation 18:4

And I [akouō](../../strongs/g/g191.md) another [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md), [legō](../../strongs/g/g3004.md), [exerchomai](../../strongs/g/g1831.md) out of her, my [laos](../../strongs/g/g2992.md), that ye [sygkoinōneō](../../strongs/g/g4790.md) not of her [hamartia](../../strongs/g/g266.md), and that ye [lambanō](../../strongs/g/g2983.md) not of her [plēgē](../../strongs/g/g4127.md).

<a name="revelation_18_5"></a>Revelation 18:5

For her [hamartia](../../strongs/g/g266.md) have [akoloutheō](../../strongs/g/g190.md) [kollaō](../../strongs/g/g2853.md) unto [ouranos](../../strongs/g/g3772.md), and [theos](../../strongs/g/g2316.md) hath [mnēmoneuō](../../strongs/g/g3421.md) her [adikēma](../../strongs/g/g92.md).

<a name="revelation_18_6"></a>Revelation 18:6

[apodidōmi](../../strongs/g/g591.md) her even as she [apodidōmi](../../strongs/g/g591.md) you, and [diploō](../../strongs/g/g1363.md) unto her [diplous](../../strongs/g/g1362.md) according to her [ergon](../../strongs/g/g2041.md): in the [potērion](../../strongs/g/g4221.md) which she hath [kerannymi](../../strongs/g/g2767.md) [kerannymi](../../strongs/g/g2767.md) to her [diplous](../../strongs/g/g1362.md).

<a name="revelation_18_7"></a>Revelation 18:7

How much she hath [doxazō](../../strongs/g/g1392.md) herself, and [strēniaō](../../strongs/g/g4763.md), [tosoutos](../../strongs/g/g5118.md) [basanismos](../../strongs/g/g929.md) and [penthos](../../strongs/g/g3997.md) [didōmi](../../strongs/g/g1325.md) her: for she [legō](../../strongs/g/g3004.md) in her [kardia](../../strongs/g/g2588.md), I [kathēmai](../../strongs/g/g2521.md) a [basilissa](../../strongs/g/g938.md), and am no [chēra](../../strongs/g/g5503.md), and shall [eidō](../../strongs/g/g1492.md) no [penthos](../../strongs/g/g3997.md).

<a name="revelation_18_8"></a>Revelation 18:8

Therefore shall her [plēgē](../../strongs/g/g4127.md) [hēkō](../../strongs/g/g2240.md) in one [hēmera](../../strongs/g/g2250.md), [thanatos](../../strongs/g/g2288.md), and [penthos](../../strongs/g/g3997.md), and [limos](../../strongs/g/g3042.md); and she shall be [katakaiō](../../strongs/g/g2618.md) with [pyr](../../strongs/g/g4442.md): for [ischyros](../../strongs/g/g2478.md) the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) who [krinō](../../strongs/g/g2919.md) her.

<a name="revelation_18_9"></a>Revelation 18:9

And the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md), who have [porneuō](../../strongs/g/g4203.md) and [strēniaō](../../strongs/g/g4763.md) with her, shall [klaiō](../../strongs/g/g2799.md) her, and [koptō](../../strongs/g/g2875.md) for her, when they shall [blepō](../../strongs/g/g991.md) the [kapnos](../../strongs/g/g2586.md) of her [pyrōsis](../../strongs/g/g4451.md),

<a name="revelation_18_10"></a>Revelation 18:10

[histēmi](../../strongs/g/g2476.md) [makrothen](../../strongs/g/g3113.md) off for the [phobos](../../strongs/g/g5401.md) of her [basanismos](../../strongs/g/g929.md), [legō](../../strongs/g/g3004.md), [ouai](../../strongs/g/g3759.md), [ouai](../../strongs/g/g3759.md) that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md) [Babylōn](../../strongs/g/g897.md), that [ischyros](../../strongs/g/g2478.md) [polis](../../strongs/g/g4172.md)! for in one [hōra](../../strongs/g/g5610.md) is thy [krisis](../../strongs/g/g2920.md) [erchomai](../../strongs/g/g2064.md).

<a name="revelation_18_11"></a>Revelation 18:11

And the [emporos](../../strongs/g/g1713.md) of [gē](../../strongs/g/g1093.md) shall [klaiō](../../strongs/g/g2799.md) and [pentheō](../../strongs/g/g3996.md) over her; for [oudeis](../../strongs/g/g3762.md) [agorazō](../../strongs/g/g59.md) their [gomos](../../strongs/g/g1117.md) any more:

<a name="revelation_18_12"></a>Revelation 18:12

The [gomos](../../strongs/g/g1117.md) of [chrysos](../../strongs/g/g5557.md), and [argyros](../../strongs/g/g696.md), and [timios](../../strongs/g/g5093.md) [lithos](../../strongs/g/g3037.md), and of [margaritēs](../../strongs/g/g3135.md), and [byssos](../../strongs/g/g1040.md), and [porphyra](../../strongs/g/g4209.md), and [sirikos](../../strongs/g/g4596.md), and [kokkinos](../../strongs/g/g2847.md), and all [thuinos](../../strongs/g/g2367.md) [xylon](../../strongs/g/g3586.md), and all manner [skeuos](../../strongs/g/g4632.md) of [elephantinos](../../strongs/g/g1661.md), and all manner [skeuos](../../strongs/g/g4632.md) of [timios](../../strongs/g/g5093.md) [xylon](../../strongs/g/g3586.md), and of [chalkos](../../strongs/g/g5475.md), and [sidēros](../../strongs/g/g4604.md), and [marmaros](../../strongs/g/g3139.md),

<a name="revelation_18_13"></a>Revelation 18:13

And [kinnamōmon](../../strongs/g/g2792.md), and [thymiama](../../strongs/g/g2368.md), and [myron](../../strongs/g/g3464.md), and [libanos](../../strongs/g/g3030.md), and [oinos](../../strongs/g/g3631.md), and [elaion](../../strongs/g/g1637.md), and [semidalis](../../strongs/g/g4585.md), and [sitos](../../strongs/g/g4621.md), and [ktēnos](../../strongs/g/g2934.md), and [probaton](../../strongs/g/g4263.md), and [hippos](../../strongs/g/g2462.md), and [rhedē](../../strongs/g/g4480.md), and [sōma](../../strongs/g/g4983.md), and [psychē](../../strongs/g/g5590.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="revelation_18_14"></a>Revelation 18:14

And the [opōra](../../strongs/g/g3703.md) that thy [psychē](../../strongs/g/g5590.md) [epithymia](../../strongs/g/g1939.md) are departed from thee, and all things which were [liparos](../../strongs/g/g3045.md) and [lampros](../../strongs/g/g2986.md) are [aperchomai](../../strongs/g/g565.md) from thee, and thou shalt [heuriskō](../../strongs/g/g2147.md) them no more at all.

<a name="revelation_18_15"></a>Revelation 18:15

The [emporos](../../strongs/g/g1713.md) of these things, which were [plouteō](../../strongs/g/g4147.md) by her, shall [histēmi](../../strongs/g/g2476.md) [makrothen](../../strongs/g/g3113.md) off for the [phobos](../../strongs/g/g5401.md) of her [basanismos](../../strongs/g/g929.md), [klaiō](../../strongs/g/g2799.md) and [pentheō](../../strongs/g/g3996.md),

<a name="revelation_18_16"></a>Revelation 18:16

And [legō](../../strongs/g/g3004.md), [ouai](../../strongs/g/g3759.md), [ouai](../../strongs/g/g3759.md) that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), that was [periballō](../../strongs/g/g4016.md) in [byssinos](../../strongs/g/g1039.md), and [porphyrous](../../strongs/g/g4210.md), and [kokkinos](../../strongs/g/g2847.md), and [chrysoō](../../strongs/g/g5558.md)d with [chrysos](../../strongs/g/g5557.md), and [timios](../../strongs/g/g5093.md) [lithos](../../strongs/g/g3037.md), and [margaritēs](../../strongs/g/g3135.md)!

<a name="revelation_18_17"></a>Revelation 18:17

For in one [hōra](../../strongs/g/g5610.md) [tosoutos](../../strongs/g/g5118.md) [ploutos](../../strongs/g/g4149.md) is [erēmoō](../../strongs/g/g2049.md). And every [kybernētēs](../../strongs/g/g2942.md), and all the [homilos](../../strongs/g/g3658.md) in [ploion](../../strongs/g/g4143.md), and [nautēs](../../strongs/g/g3492.md), and as many as [ergazomai](../../strongs/g/g2038.md) by [thalassa](../../strongs/g/g2281.md), [histēmi](../../strongs/g/g2476.md) [makrothen](../../strongs/g/g3113.md) off,

<a name="revelation_18_18"></a>Revelation 18:18

And [krazō](../../strongs/g/g2896.md) when they [horaō](../../strongs/g/g3708.md) the [kapnos](../../strongs/g/g2586.md) of her [pyrōsis](../../strongs/g/g4451.md), [legō](../../strongs/g/g3004.md), What is [homoios](../../strongs/g/g3664.md) unto this [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md)!

<a name="revelation_18_19"></a>Revelation 18:19

And they [ballō](../../strongs/g/g906.md) [chous](../../strongs/g/g5522.md) on their [kephalē](../../strongs/g/g2776.md), and [krazō](../../strongs/g/g2896.md), [klaiō](../../strongs/g/g2799.md) and [pentheō](../../strongs/g/g3996.md), [legō](../../strongs/g/g3004.md), [ouai](../../strongs/g/g3759.md), [ouai](../../strongs/g/g3759.md) that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), wherein were [plouteō](../../strongs/g/g4147.md) all that had [ploion](../../strongs/g/g4143.md) in the [thalassa](../../strongs/g/g2281.md) by reason of her [timiotēs](../../strongs/g/g5094.md)! for in one [hōra](../../strongs/g/g5610.md) is she [erēmoō](../../strongs/g/g2049.md).

<a name="revelation_18_20"></a>Revelation 18:20

[euphrainō](../../strongs/g/g2165.md) over her, thou [ouranos](../../strongs/g/g3772.md), and ye [hagios](../../strongs/g/g40.md) [apostolos](../../strongs/g/g652.md) and [prophētēs](../../strongs/g/g4396.md); for [theos](../../strongs/g/g2316.md) hath [krima](../../strongs/g/g2917.md) [krinō](../../strongs/g/g2919.md) you on her.

<a name="revelation_18_21"></a>Revelation 18:21

And an [ischyros](../../strongs/g/g2478.md) [aggelos](../../strongs/g/g32.md) [airō](../../strongs/g/g142.md) a [lithos](../../strongs/g/g3037.md) like a [megas](../../strongs/g/g3173.md) [mylos](../../strongs/g/g3458.md), and [ballō](../../strongs/g/g906.md) it into the [thalassa](../../strongs/g/g2281.md), [legō](../../strongs/g/g3004.md), Thus with [hormēma](../../strongs/g/g3731.md) shall that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md) [Babylōn](../../strongs/g/g897.md) be [ballō](../../strongs/g/g906.md), and shall be [heuriskō](../../strongs/g/g2147.md) no more at all.

<a name="revelation_18_22"></a>Revelation 18:22

And the [phōnē](../../strongs/g/g5456.md) of [kitharōdos](../../strongs/g/g2790.md), and [mousikos](../../strongs/g/g3451.md), and of [aulētēs](../../strongs/g/g834.md), and [salpistēs](../../strongs/g/g4538.md), shall be [akouō](../../strongs/g/g191.md) no more at all in thee; and no [technitēs](../../strongs/g/g5079.md), of whatsoever [technē](../../strongs/g/g5078.md) he be, shall be [heuriskō](../../strongs/g/g2147.md) any more in thee; and the [phōnē](../../strongs/g/g5456.md) of a [mylos](../../strongs/g/g3458.md) shall be [akouō](../../strongs/g/g191.md) no more at all in thee;

<a name="revelation_18_23"></a>Revelation 18:23

And the [phōs](../../strongs/g/g5457.md) of a [lychnos](../../strongs/g/g3088.md) shall [phainō](../../strongs/g/g5316.md) no more at all in thee; and the [phōnē](../../strongs/g/g5456.md) of the [nymphios](../../strongs/g/g3566.md) and of the [nymphē](../../strongs/g/g3565.md) shall be [akouō](../../strongs/g/g191.md) no more at all in thee: for thy [emporos](../../strongs/g/g1713.md) were the [megistan](../../strongs/g/g3175.md) of [gē](../../strongs/g/g1093.md); for by thy [pharmakeia](../../strongs/g/g5331.md) were all [ethnos](../../strongs/g/g1484.md) [planaō](../../strongs/g/g4105.md).

<a name="revelation_18_24"></a>Revelation 18:24

And in her was [heuriskō](../../strongs/g/g2147.md) the [haima](../../strongs/g/g129.md) of [prophētēs](../../strongs/g/g4396.md), and of [hagios](../../strongs/g/g40.md), and of all that were [sphazō](../../strongs/g/g4969.md) upon [gē](../../strongs/g/g1093.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 17](revelation_17.md) - [Revelation 19](revelation_19.md)