# [Revelation 11](https://www.blueletterbible.org/kjv/rev/11/1/s_1178001)

<a name="revelation_11_1"></a>Revelation 11:1

And there was [didōmi](../../strongs/g/g1325.md) me a [kalamos](../../strongs/g/g2563.md) [homoios](../../strongs/g/g3664.md) unto a [rhabdos](../../strongs/g/g4464.md): and the [aggelos](../../strongs/g/g32.md) [histēmi](../../strongs/g/g2476.md), [legō](../../strongs/g/g3004.md), [egeirō](../../strongs/g/g1453.md), and [metreō](../../strongs/g/g3354.md) the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md), and the [thysiastērion](../../strongs/g/g2379.md), and them that [proskyneō](../../strongs/g/g4352.md) therein.

<a name="revelation_11_2"></a>Revelation 11:2

But the [aulē](../../strongs/g/g833.md) which is [exōthen](../../strongs/g/g1855.md) [esōthen](../../strongs/g/g2081.md) the [naos](../../strongs/g/g3485.md) [ekballō](../../strongs/g/g1544.md) out, and [metreō](../../strongs/g/g3354.md) it not; for it is [didōmi](../../strongs/g/g1325.md) unto the [ethnos](../../strongs/g/g1484.md): and the [hagios](../../strongs/g/g40.md) [polis](../../strongs/g/g4172.md) shall they [pateō](../../strongs/g/g3961.md) forty two [mēn](../../strongs/g/g3376.md).

<a name="revelation_11_3"></a>Revelation 11:3

And I will [didōmi](../../strongs/g/g1325.md) unto my two [martys](../../strongs/g/g3144.md), and they shall [prophēteuō](../../strongs/g/g4395.md) a thousand two hundred threescore [hēmera](../../strongs/g/g2250.md), [periballō](../../strongs/g/g4016.md) in [sakkos](../../strongs/g/g4526.md).

<a name="revelation_11_4"></a>Revelation 11:4

These are the two [elaia](../../strongs/g/g1636.md), and the two [lychnia](../../strongs/g/g3087.md) [histēmi](../../strongs/g/g2476.md) before the [theos](../../strongs/g/g2316.md) of [gē](../../strongs/g/g1093.md).

<a name="revelation_11_5"></a>Revelation 11:5

And [ei tis](../../strongs/g/g1536.md) will [adikeō](../../strongs/g/g91.md) them, [pyr](../../strongs/g/g4442.md) [ekporeuomai](../../strongs/g/g1607.md) out of their [stoma](../../strongs/g/g4750.md), and [katesthiō](../../strongs/g/g2719.md) their [echthros](../../strongs/g/g2190.md): and [ei tis](../../strongs/g/g1536.md) will [adikeō](../../strongs/g/g91.md) them, he must in this manner be [apokteinō](../../strongs/g/g615.md).

<a name="revelation_11_6"></a>Revelation 11:6

These have [exousia](../../strongs/g/g1849.md) to [kleiō](../../strongs/g/g2808.md) [ouranos](../../strongs/g/g3772.md), that it [brechō](../../strongs/g/g1026.md) [hyetos](../../strongs/g/g5205.md) not in the days of their [prophēteia](../../strongs/g/g4394.md): and have [exousia](../../strongs/g/g1849.md) over [hydōr](../../strongs/g/g5204.md) to [strephō](../../strongs/g/g4762.md) them to [haima](../../strongs/g/g129.md), and to [patassō](../../strongs/g/g3960.md) [gē](../../strongs/g/g1093.md) with all [plēgē](../../strongs/g/g4127.md), as often as they will.

<a name="revelation_11_7"></a>Revelation 11:7

And when they shall have [teleō](../../strongs/g/g5055.md) their [martyria](../../strongs/g/g3141.md), the [thērion](../../strongs/g/g2342.md) that [anabainō](../../strongs/g/g305.md) out of the [abyssos](../../strongs/g/g12.md) shall [poieō](../../strongs/g/g4160.md) [polemos](../../strongs/g/g4171.md) against them, and shall [nikaō](../../strongs/g/g3528.md) them, and [apokteinō](../../strongs/g/g615.md) them.

<a name="revelation_11_8"></a>Revelation 11:8

And their [ptōma](../../strongs/g/g4430.md) shall in the [plateia](../../strongs/g/g4113.md) of the [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), which [pneumatikōs](../../strongs/g/g4153.md) is [kaleō](../../strongs/g/g2564.md) [Sodoma](../../strongs/g/g4670.md) and [Aigyptos](../../strongs/g/g125.md), where also our [kyrios](../../strongs/g/g2962.md) was [stauroō](../../strongs/g/g4717.md).

<a name="revelation_11_9"></a>Revelation 11:9

And they of the [laos](../../strongs/g/g2992.md) and [phylē](../../strongs/g/g5443.md) and [glōssa](../../strongs/g/g1100.md) and [ethnos](../../strongs/g/g1484.md) shall [blepō](../../strongs/g/g991.md) their [ptōma](../../strongs/g/g4430.md) three [hēmera](../../strongs/g/g2250.md) and an [ēmisys](../../strongs/g/g2255.md), and shall not [aphiēmi](../../strongs/g/g863.md) their [ptōma](../../strongs/g/g4430.md) to be [tithēmi](../../strongs/g/g5087.md) in [mnēma](../../strongs/g/g3418.md).

<a name="revelation_11_10"></a>Revelation 11:10

And they that [katoikeō](../../strongs/g/g2730.md) upon [gē](../../strongs/g/g1093.md) shall [chairō](../../strongs/g/g5463.md) over them, and [euphrainō](../../strongs/g/g2165.md), and shall [pempō](../../strongs/g/g3992.md) [dōron](../../strongs/g/g1435.md) to [allēlōn](../../strongs/g/g240.md); because these two [prophētēs](../../strongs/g/g4396.md) [basanizō](../../strongs/g/g928.md) them that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md).

<a name="revelation_11_11"></a>Revelation 11:11

And after three [hēmera](../../strongs/g/g2250.md) and an [ēmisys](../../strongs/g/g2255.md) the [pneuma](../../strongs/g/g4151.md) of [zōē](../../strongs/g/g2222.md) from [theos](../../strongs/g/g2316.md) [eiserchomai](../../strongs/g/g1525.md) into them, and they [histēmi](../../strongs/g/g2476.md) upon their [pous](../../strongs/g/g4228.md); and [megas](../../strongs/g/g3173.md) [phobos](../../strongs/g/g5401.md) [piptō](../../strongs/g/g4098.md) upon them which [theōreō](../../strongs/g/g2334.md) them.

<a name="revelation_11_12"></a>Revelation 11:12

And they [akouō](../../strongs/g/g191.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md) [legō](../../strongs/g/g3004.md) unto them, [anabainō](../../strongs/g/g305.md) [hōde](../../strongs/g/g5602.md). And they [anabainō](../../strongs/g/g305.md) to [ouranos](../../strongs/g/g3772.md) in a [nephelē](../../strongs/g/g3507.md); and their [echthros](../../strongs/g/g2190.md) [theōreō](../../strongs/g/g2334.md) them.

<a name="revelation_11_13"></a>Revelation 11:13

And the same [hōra](../../strongs/g/g5610.md) was there a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md), and the tenth part of the [polis](../../strongs/g/g4172.md) [piptō](../../strongs/g/g4098.md), and in the [seismos](../../strongs/g/g4578.md) were [apokteinō](../../strongs/g/g615.md) [onoma](../../strongs/g/g3686.md) of [anthrōpos](../../strongs/g/g444.md) seven thousand: and the [loipos](../../strongs/g/g3062.md) were [emphobos](../../strongs/g/g1719.md), and [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) to the [theos](../../strongs/g/g2316.md) of [ouranos](../../strongs/g/g3772.md).

<a name="revelation_11_14"></a>Revelation 11:14

The second [ouai](../../strongs/g/g3759.md) is [aperchomai](../../strongs/g/g565.md); [idou](../../strongs/g/g2400.md), the third [ouai](../../strongs/g/g3759.md) [erchomai](../../strongs/g/g2064.md) [tachy](../../strongs/g/g5035.md).

<a name="revelation_11_15"></a>Revelation 11:15

And the seventh [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md); and there were [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) in [ouranos](../../strongs/g/g3772.md), [legō](../../strongs/g/g3004.md), The [basileia](../../strongs/g/g932.md) of this [kosmos](../../strongs/g/g2889.md) are become of our [kyrios](../../strongs/g/g2962.md), and of his [Christos](../../strongs/g/g5547.md); and he shall [basileuō](../../strongs/g/g936.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_11_16"></a>Revelation 11:16

And the four and twenty [presbyteros](../../strongs/g/g4245.md), which [kathēmai](../../strongs/g/g2521.md) before [theos](../../strongs/g/g2316.md) on their [thronos](../../strongs/g/g2362.md), [piptō](../../strongs/g/g4098.md) upon their [prosōpon](../../strongs/g/g4383.md), and [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md),

<a name="revelation_11_17"></a>Revelation 11:17

[legō](../../strongs/g/g3004.md), We [eucharisteō](../../strongs/g/g2168.md) thee, [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md), which art, and wast, and art to [erchomai](../../strongs/g/g2064.md); because thou hast [lambanō](../../strongs/g/g2983.md) to thee thy [megas](../../strongs/g/g3173.md) [dynamis](../../strongs/g/g1411.md), and hast [basileuō](../../strongs/g/g936.md). [^1]

<a name="revelation_11_18"></a>Revelation 11:18

And the [ethnos](../../strongs/g/g1484.md) were [orgizō](../../strongs/g/g3710.md), and thy [orgē](../../strongs/g/g3709.md) is [erchomai](../../strongs/g/g2064.md), and the [kairos](../../strongs/g/g2540.md) of the [nekros](../../strongs/g/g3498.md), that they should be [krinō](../../strongs/g/g2919.md), and that thou shouldest [didōmi](../../strongs/g/g1325.md) [misthos](../../strongs/g/g3408.md) unto thy [doulos](../../strongs/g/g1401.md) the [prophētēs](../../strongs/g/g4396.md), and to the [hagios](../../strongs/g/g40.md), and them that [phobeō](../../strongs/g/g5399.md) thy [onoma](../../strongs/g/g3686.md), [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md); and shouldest [diaphtheirō](../../strongs/g/g1311.md) them which [diaphtheirō](../../strongs/g/g1311.md) [gē](../../strongs/g/g1093.md).

<a name="revelation_11_19"></a>Revelation 11:19

And the [naos](../../strongs/g/g3485.md) of [theos](../../strongs/g/g2316.md) was [anoigō](../../strongs/g/g455.md) in [ouranos](../../strongs/g/g3772.md), and there was [optanomai](../../strongs/g/g3700.md) in his [naos](../../strongs/g/g3485.md) the [kibōtos](../../strongs/g/g2787.md) of his [diathēkē](../../strongs/g/g1242.md): and there were [astrapē](../../strongs/g/g796.md), and [phōnē](../../strongs/g/g5456.md), and [brontē](../../strongs/g/g1027.md), and a [seismos](../../strongs/g/g4578.md), and [megas](../../strongs/g/g3173.md) [chalaza](../../strongs/g/g5464.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 10](revelation_10.md) - [Revelation 12](revelation_12.md)

---

[^1]: [Revelation 11:17 Commentary](../../commentary/revelation/revelation_11_commentary.md#revelation_11_17)
