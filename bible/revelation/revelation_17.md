# [Revelation 17](https://www.blueletterbible.org/kjv/rev/17/1/s_1184001)

<a name="revelation_17_1"></a>Revelation 17:1

And there [erchomai](../../strongs/g/g2064.md) one of the seven [aggelos](../../strongs/g/g32.md) which had the seven [phialē](../../strongs/g/g5357.md), and [laleō](../../strongs/g/g2980.md) with me, [legō](../../strongs/g/g3004.md) unto me, [deuro](../../strongs/g/g1204.md); I will [deiknyō](../../strongs/g/g1166.md) unto thee the [krima](../../strongs/g/g2917.md) of the [megas](../../strongs/g/g3173.md) [pornē](../../strongs/g/g4204.md) that [kathēmai](../../strongs/g/g2521.md) upon [polys](../../strongs/g/g4183.md) [hydōr](../../strongs/g/g5204.md):

<a name="revelation_17_2"></a>Revelation 17:2

With whom the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md) have [porneuō](../../strongs/g/g4203.md), and the [katoikeō](../../strongs/g/g2730.md) of [gē](../../strongs/g/g1093.md) have been [methyō](../../strongs/g/g3184.md) with the [oinos](../../strongs/g/g3631.md) of her [porneia](../../strongs/g/g4202.md).

<a name="revelation_17_3"></a>Revelation 17:3

So he [apopherō](../../strongs/g/g667.md) me in the [pneuma](../../strongs/g/g4151.md) into the [erēmos](../../strongs/g/g2048.md): and I [eidō](../../strongs/g/g1492.md) a [gynē](../../strongs/g/g1135.md) [kathēmai](../../strongs/g/g2521.md) upon a [kokkinos](../../strongs/g/g2847.md) [thērion](../../strongs/g/g2342.md), [gemō](../../strongs/g/g1073.md) of [onoma](../../strongs/g/g3686.md) of [blasphēmia](../../strongs/g/g988.md), having seven [kephalē](../../strongs/g/g2776.md) and ten [keras](../../strongs/g/g2768.md).

<a name="revelation_17_4"></a>Revelation 17:4

And the [gynē](../../strongs/g/g1135.md) was [periballō](../../strongs/g/g4016.md) in [porphyra](../../strongs/g/g4209.md) and [kokkinos](../../strongs/g/g2847.md), and [chrysoō](../../strongs/g/g5558.md) with [chrysos](../../strongs/g/g5557.md) and [timios](../../strongs/g/g5093.md) [lithos](../../strongs/g/g3037.md) and [margaritēs](../../strongs/g/g3135.md), having a [chrysous](../../strongs/g/g5552.md) [potērion](../../strongs/g/g4221.md) in her [cheir](../../strongs/g/g5495.md) [gemō](../../strongs/g/g1073.md) of [bdelygma](../../strongs/g/g946.md) and [akathartēs](../../strongs/g/g168.md) of her [porneia](../../strongs/g/g4202.md):

<a name="revelation_17_5"></a>Revelation 17:5

And upon her [metōpon](../../strongs/g/g3359.md) was an [onoma](../../strongs/g/g3686.md) [graphō](../../strongs/g/g1125.md), [mystērion](../../strongs/g/g3466.md), [Babylōn](../../strongs/g/g897.md) [megas](../../strongs/g/g3173.md), [mētēr](../../strongs/g/g3384.md) [pornē](../../strongs/g/g4204.md) [kai](../../strongs/g/g2532.md) [bdelygma](../../strongs/g/g946.md) [gē](../../strongs/g/g1093.md).

<a name="revelation_17_6"></a>Revelation 17:6

And I [eidō](../../strongs/g/g1492.md) the [gynē](../../strongs/g/g1135.md) [methyō](../../strongs/g/g3184.md) with the [haima](../../strongs/g/g129.md) of the [hagios](../../strongs/g/g40.md), and with the [haima](../../strongs/g/g129.md) of the [martys](../../strongs/g/g3144.md) of [Iēsous](../../strongs/g/g2424.md): and when I [eidō](../../strongs/g/g1492.md) her, I [thaumazō](../../strongs/g/g2296.md) with [megas](../../strongs/g/g3173.md) [thauma](../../strongs/g/g2295.md).

<a name="revelation_17_7"></a>Revelation 17:7

And the [aggelos](../../strongs/g/g32.md) [eipon](../../strongs/g/g2036.md) unto me, Wherefore didst thou [thaumazō](../../strongs/g/g2296.md)? I will [eipon](../../strongs/g/g2046.md) thee the [mystērion](../../strongs/g/g3466.md) of the [gynē](../../strongs/g/g1135.md), and of the [thērion](../../strongs/g/g2342.md) that [bastazō](../../strongs/g/g941.md) her, which hath the seven [kephalē](../../strongs/g/g2776.md) and ten [keras](../../strongs/g/g2768.md).

<a name="revelation_17_8"></a>Revelation 17:8

The [thērion](../../strongs/g/g2342.md) that thou [eidō](../../strongs/g/g1492.md) was, and is not; and shall [anabainō](../../strongs/g/g305.md) of the [abyssos](../../strongs/g/g12.md), and [hypagō](../../strongs/g/g5217.md) into [apōleia](../../strongs/g/g684.md): and they that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md) shall [thaumazō](../../strongs/g/g2296.md), whose [onoma](../../strongs/g/g3686.md) were not [graphō](../../strongs/g/g1125.md) in the [biblion](../../strongs/g/g975.md) of [zōē](../../strongs/g/g2222.md) from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md), when they [blepō](../../strongs/g/g991.md) the [thērion](../../strongs/g/g2342.md) that was, and is not, and yet is.

<a name="revelation_17_9"></a>Revelation 17:9

And here is the [nous](../../strongs/g/g3563.md) which hath [sophia](../../strongs/g/g4678.md). The seven [kephalē](../../strongs/g/g2776.md) are seven [oros](../../strongs/g/g3735.md), on which the [gynē](../../strongs/g/g1135.md) [kathēmai](../../strongs/g/g2521.md).

<a name="revelation_17_10"></a>Revelation 17:10

And there are seven [basileus](../../strongs/g/g935.md): five are [piptō](../../strongs/g/g4098.md), and one is, and the other is not yet [erchomai](../../strongs/g/g2064.md); and when he [erchomai](../../strongs/g/g2064.md), he must [menō](../../strongs/g/g3306.md) a [oligos](../../strongs/g/g3641.md).

<a name="revelation_17_11"></a>Revelation 17:11

And the [thērion](../../strongs/g/g2342.md) that was, and is not, even he is the eighth, and is of the seven, and [hypagō](../../strongs/g/g5217.md) into [apōleia](../../strongs/g/g684.md).

<a name="revelation_17_12"></a>Revelation 17:12

And the ten [keras](../../strongs/g/g2768.md) which thou [eidō](../../strongs/g/g1492.md) are ten [basileus](../../strongs/g/g935.md), which have [lambanō](../../strongs/g/g2983.md) no [basileia](../../strongs/g/g932.md) as yet; but [lambanō](../../strongs/g/g2983.md) [exousia](../../strongs/g/g1849.md) as [basileus](../../strongs/g/g935.md) one [hōra](../../strongs/g/g5610.md) with the [thērion](../../strongs/g/g2342.md).

<a name="revelation_17_13"></a>Revelation 17:13

These have one [gnōmē](../../strongs/g/g1106.md), and shall [diadidōmi](../../strongs/g/g1239.md) their [dynamis](../../strongs/g/g1411.md) and [exousia](../../strongs/g/g1849.md) unto the [thērion](../../strongs/g/g2342.md).

<a name="revelation_17_14"></a>Revelation 17:14

These shall [polemeō](../../strongs/g/g4170.md) with the [arnion](../../strongs/g/g721.md), and the [arnion](../../strongs/g/g721.md) shall [nikaō](../../strongs/g/g3528.md) them: for he is [kyrios](../../strongs/g/g2962.md) of [kyrios](../../strongs/g/g2962.md), and [basileus](../../strongs/g/g935.md) of [basileus](../../strongs/g/g935.md): and they that are with him are [klētos](../../strongs/g/g2822.md), and [eklektos](../../strongs/g/g1588.md), and [pistos](../../strongs/g/g4103.md).

<a name="revelation_17_15"></a>Revelation 17:15

And he [legō](../../strongs/g/g3004.md) unto me, The [hydōr](../../strongs/g/g5204.md) which thou [eidō](../../strongs/g/g1492.md), where the [pornē](../../strongs/g/g4204.md) [kathēmai](../../strongs/g/g2521.md), are [laos](../../strongs/g/g2992.md), and [ochlos](../../strongs/g/g3793.md), and [ethnos](../../strongs/g/g1484.md), and [glōssa](../../strongs/g/g1100.md).

<a name="revelation_17_16"></a>Revelation 17:16

And the ten [keras](../../strongs/g/g2768.md) which thou [eidō](../../strongs/g/g1492.md) upon the [thērion](../../strongs/g/g2342.md), these shall [miseō](../../strongs/g/g3404.md) the [pornē](../../strongs/g/g4204.md), and shall [poieō](../../strongs/g/g4160.md) her [erēmoō](../../strongs/g/g2049.md) and [gymnos](../../strongs/g/g1131.md), and shall [phago](../../strongs/g/g5315.md) her [sarx](../../strongs/g/g4561.md), and [katakaiō](../../strongs/g/g2618.md) her with [pyr](../../strongs/g/g4442.md).

<a name="revelation_17_17"></a>Revelation 17:17

For [theos](../../strongs/g/g2316.md) hath [didōmi](../../strongs/g/g1325.md) in their [kardia](../../strongs/g/g2588.md) to [poieō](../../strongs/g/g4160.md) his [gnōmē](../../strongs/g/g1106.md), and to [poieō](../../strongs/g/g4160.md) [mia](../../strongs/g/g3391.md) [gnōmē](../../strongs/g/g1106.md), and [didōmi](../../strongs/g/g1325.md) their [basileia](../../strongs/g/g932.md) unto the [thērion](../../strongs/g/g2342.md), until the [rhēma](../../strongs/g/g4487.md) of [theos](../../strongs/g/g2316.md) shall be [teleō](../../strongs/g/g5055.md).

<a name="revelation_17_18"></a>Revelation 17:18

And the [gynē](../../strongs/g/g1135.md) which thou [eidō](../../strongs/g/g1492.md) is that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), which [basileia](../../strongs/g/g932.md) over the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 16](revelation_16.md) - [Revelation 18](revelation_18.md)