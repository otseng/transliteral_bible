# [Revelation 14](https://www.blueletterbible.org/kjv/rev/14/1/s_1181001)

<a name="revelation_14_1"></a>Revelation 14:1

And I [eidō](../../strongs/g/g1492.md), and, [idou](../../strongs/g/g2400.md), an [arnion](../../strongs/g/g721.md) [histēmi](../../strongs/g/g2476.md) on the [oros](../../strongs/g/g3735.md) [Siōn](../../strongs/g/g4622.md), and with him an hundred forty four thousand, having his [patēr](../../strongs/g/g3962.md) [onoma](../../strongs/g/g3686.md) [graphō](../../strongs/g/g1125.md) in their [metōpon](../../strongs/g/g3359.md).

<a name="revelation_14_2"></a>Revelation 14:2

And I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md), as the [phōnē](../../strongs/g/g5456.md) of [polys](../../strongs/g/g4183.md) [hydōr](../../strongs/g/g5204.md), and as the [phōnē](../../strongs/g/g5456.md) of a [megas](../../strongs/g/g3173.md) [brontē](../../strongs/g/g1027.md): and I [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of [kitharōdos](../../strongs/g/g2790.md) [kitharizō](../../strongs/g/g2789.md) with their [kithara](../../strongs/g/g2788.md):

<a name="revelation_14_3"></a>Revelation 14:3

And they [adō](../../strongs/g/g103.md) as it were a [kainos](../../strongs/g/g2537.md) [ōdē](../../strongs/g/g5603.md) before the [thronos](../../strongs/g/g2362.md), and before the four [zōon](../../strongs/g/g2226.md), and the [presbyteros](../../strongs/g/g4245.md): and no man could [manthanō](../../strongs/g/g3129.md) that [ōdē](../../strongs/g/g5603.md) but the hundred forty four thousand, which were [agorazō](../../strongs/g/g59.md) from [gē](../../strongs/g/g1093.md).

<a name="revelation_14_4"></a>Revelation 14:4

These are they which were not [molynō](../../strongs/g/g3435.md) with [gynē](../../strongs/g/g1135.md); for they are [parthenos](../../strongs/g/g3933.md). These are they which [akoloutheō](../../strongs/g/g190.md) the [arnion](../../strongs/g/g721.md) whithersoever he [hypagō](../../strongs/g/g5217.md). These were [agorazō](../../strongs/g/g59.md) from among [anthrōpos](../../strongs/g/g444.md), the [aparchē](../../strongs/g/g536.md) unto [theos](../../strongs/g/g2316.md) and to the [arnion](../../strongs/g/g721.md).

<a name="revelation_14_5"></a>Revelation 14:5

And in their [stoma](../../strongs/g/g4750.md) was [heuriskō](../../strongs/g/g2147.md) no [dolos](../../strongs/g/g1388.md): for they are [amōmos](../../strongs/g/g299.md) before the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md). [^1]

<a name="revelation_14_6"></a>Revelation 14:6

And I [eidō](../../strongs/g/g1492.md) another [aggelos](../../strongs/g/g32.md) [petomai](../../strongs/g/g4072.md) in [mesouranēma](../../strongs/g/g3321.md), having the [aiōnios](../../strongs/g/g166.md) [euaggelion](../../strongs/g/g2098.md) to [euaggelizō](../../strongs/g/g2097.md) unto them that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md), and to every [ethnos](../../strongs/g/g1484.md), and [phylē](../../strongs/g/g5443.md), and [glōssa](../../strongs/g/g1100.md), and [laos](../../strongs/g/g2992.md),

<a name="revelation_14_7"></a>Revelation 14:7

[legō](../../strongs/g/g3004.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [phobeō](../../strongs/g/g5399.md) [theos](../../strongs/g/g2316.md), and [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) to him; for the [hōra](../../strongs/g/g5610.md) of his [krisis](../../strongs/g/g2920.md) is [erchomai](../../strongs/g/g2064.md): and [proskyneō](../../strongs/g/g4352.md) him that [poieō](../../strongs/g/g4160.md) [ouranos](../../strongs/g/g3772.md), and [gē](../../strongs/g/g1093.md), and the [thalassa](../../strongs/g/g2281.md), and the [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md).

<a name="revelation_14_8"></a>Revelation 14:8

And there [akoloutheō](../../strongs/g/g190.md) another [aggelos](../../strongs/g/g32.md), [legō](../../strongs/g/g3004.md), [Babylōn](../../strongs/g/g897.md) is [piptō](../../strongs/g/g4098.md), is [piptō](../../strongs/g/g4098.md), that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), because she made all [ethnos](../../strongs/g/g1484.md) [potizō](../../strongs/g/g4222.md) of the [oinos](../../strongs/g/g3631.md) of the [thymos](../../strongs/g/g2372.md) of her [porneia](../../strongs/g/g4202.md).

<a name="revelation_14_9"></a>Revelation 14:9

And the third [aggelos](../../strongs/g/g32.md) [akoloutheō](../../strongs/g/g190.md) them, [legō](../../strongs/g/g3004.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), If any man [proskyneō](../../strongs/g/g4352.md) the [thērion](../../strongs/g/g2342.md) and his [eikōn](../../strongs/g/g1504.md), and [lambanō](../../strongs/g/g2983.md) his [charagma](../../strongs/g/g5480.md) in [metōpon](../../strongs/g/g3359.md), or in his [cheir](../../strongs/g/g5495.md),

<a name="revelation_14_10"></a>Revelation 14:10

The same shall [pinō](../../strongs/g/g4095.md) of the [oinos](../../strongs/g/g3631.md) of the [thymos](../../strongs/g/g2372.md) of [theos](../../strongs/g/g2316.md), which is [kerannymi](../../strongs/g/g2767.md) [akratos](../../strongs/g/g194.md) into the [potērion](../../strongs/g/g4221.md) of his [orgē](../../strongs/g/g3709.md); and he shall be [basanizō](../../strongs/g/g928.md) with [pyr](../../strongs/g/g4442.md) and [theion](../../strongs/g/g2303.md) in the presence of the [hagios](../../strongs/g/g40.md) [aggelos](../../strongs/g/g32.md), and in the presence of the [arnion](../../strongs/g/g721.md):

<a name="revelation_14_11"></a>Revelation 14:11

And the [kapnos](../../strongs/g/g2586.md) of their [basanismos](../../strongs/g/g929.md) [anabainō](../../strongs/g/g305.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md): and they have no [anapausis](../../strongs/g/g372.md) [hēmera](../../strongs/g/g2250.md) nor [nyx](../../strongs/g/g3571.md), who [proskyneō](../../strongs/g/g4352.md) the [thērion](../../strongs/g/g2342.md) and his [eikōn](../../strongs/g/g1504.md), and whosoever [lambanō](../../strongs/g/g2983.md) the [charagma](../../strongs/g/g5480.md) of his [onoma](../../strongs/g/g3686.md).

<a name="revelation_14_12"></a>Revelation 14:12

Here is the [hypomonē](../../strongs/g/g5281.md) of the [hagios](../../strongs/g/g40.md): here they that [tēreō](../../strongs/g/g5083.md) the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md), and the [pistis](../../strongs/g/g4102.md) of [Iēsous](../../strongs/g/g2424.md).

<a name="revelation_14_13"></a>Revelation 14:13

And I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md) saying unto me, [graphō](../../strongs/g/g1125.md), [makarios](../../strongs/g/g3107.md) the [nekros](../../strongs/g/g3498.md) which [apothnēskō](../../strongs/g/g599.md) in the [kyrios](../../strongs/g/g2962.md) [aparti](../../strongs/g/g534.md): [nai](../../strongs/g/g3483.md), [legō](../../strongs/g/g3004.md) the [pneuma](../../strongs/g/g4151.md), that they may [anapauō](../../strongs/g/g373.md) from their [kopos](../../strongs/g/g2873.md); and their [ergon](../../strongs/g/g2041.md) do [akoloutheō](../../strongs/g/g190.md) them.

<a name="revelation_14_14"></a>Revelation 14:14

And I [eidō](../../strongs/g/g1492.md), and [idou](../../strongs/g/g2400.md) a [leukos](../../strongs/g/g3022.md) [nephelē](../../strongs/g/g3507.md), and upon the [nephelē](../../strongs/g/g3507.md) [kathēmai](../../strongs/g/g2521.md) [homoios](../../strongs/g/g3664.md) unto the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), having on his [kephalē](../../strongs/g/g2776.md) a [chrysous](../../strongs/g/g5552.md) [stephanos](../../strongs/g/g4735.md), and in his [cheir](../../strongs/g/g5495.md) a [oxys](../../strongs/g/g3691.md) [drepanon](../../strongs/g/g1407.md).

<a name="revelation_14_15"></a>Revelation 14:15

And another [aggelos](../../strongs/g/g32.md) [exerchomai](../../strongs/g/g1831.md) out of the [naos](../../strongs/g/g3485.md), [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) to him that [kathēmai](../../strongs/g/g2521.md) on the [nephelē](../../strongs/g/g3507.md), [pempō](../../strongs/g/g3992.md) thy [drepanon](../../strongs/g/g1407.md), and [therizō](../../strongs/g/g2325.md): for the [hōra](../../strongs/g/g5610.md) is [erchomai](../../strongs/g/g2064.md) for thee to [therizō](../../strongs/g/g2325.md); for the [therismos](../../strongs/g/g2326.md) of [gē](../../strongs/g/g1093.md) is [xērainō](../../strongs/g/g3583.md).

<a name="revelation_14_16"></a>Revelation 14:16

And he that [kathēmai](../../strongs/g/g2521.md) on the [nephelē](../../strongs/g/g3507.md) [ballō](../../strongs/g/g906.md) his [drepanon](../../strongs/g/g1407.md) on [gē](../../strongs/g/g1093.md); and [gē](../../strongs/g/g1093.md) was [therizō](../../strongs/g/g2325.md).

<a name="revelation_14_17"></a>Revelation 14:17

And another [aggelos](../../strongs/g/g32.md) [exerchomai](../../strongs/g/g1831.md) out of the [naos](../../strongs/g/g3485.md) which is in [ouranos](../../strongs/g/g3772.md), he also having a [oxys](../../strongs/g/g3691.md) [drepanon](../../strongs/g/g1407.md).

<a name="revelation_14_18"></a>Revelation 14:18

And another [aggelos](../../strongs/g/g32.md) [exerchomai](../../strongs/g/g1831.md) out from the [thysiastērion](../../strongs/g/g2379.md), which had [exousia](../../strongs/g/g1849.md) over [pyr](../../strongs/g/g4442.md); and [phōneō](../../strongs/g/g5455.md) with a [megas](../../strongs/g/g3173.md) [kraugē](../../strongs/g/g2906.md) to him that had the [oxys](../../strongs/g/g3691.md) [drepanon](../../strongs/g/g1407.md), [legō](../../strongs/g/g3004.md), [pempō](../../strongs/g/g3992.md) thy [oxys](../../strongs/g/g3691.md) [drepanon](../../strongs/g/g1407.md), and [trygaō](../../strongs/g/g5166.md) the [botrys](../../strongs/g/g1009.md) of the [ampelos](../../strongs/g/g288.md) of [gē](../../strongs/g/g1093.md); for her [staphylē](../../strongs/g/g4718.md) are [akmazō](../../strongs/g/g187.md).

<a name="revelation_14_19"></a>Revelation 14:19

And the [aggelos](../../strongs/g/g32.md) [ballō](../../strongs/g/g906.md) his [drepanon](../../strongs/g/g1407.md) into [gē](../../strongs/g/g1093.md), and [trygaō](../../strongs/g/g5166.md) the [ampelos](../../strongs/g/g288.md) of [gē](../../strongs/g/g1093.md), and [ballō](../../strongs/g/g906.md) it into the [megas](../../strongs/g/g3173.md) [lēnos](../../strongs/g/g3025.md) of the [thymos](../../strongs/g/g2372.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_14_20"></a>Revelation 14:20

And the [lēnos](../../strongs/g/g3025.md) was [pateō](../../strongs/g/g3961.md) without the [polis](../../strongs/g/g4172.md), and [haima](../../strongs/g/g129.md) [exerchomai](../../strongs/g/g1831.md) out of the [lēnos](../../strongs/g/g3025.md), even unto the [hippos](../../strongs/g/g2462.md) [chalinos](../../strongs/g/g5469.md), by the space of a thousand and six hundred [stadion](../../strongs/g/g4712.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 13](revelation_13.md) - [Revelation 15](revelation_15.md)

---

[^1]: [Revelation 14:5 Commentary](../../commentary/revelation/revelation_14_commentary.md#revelation_14_5)
