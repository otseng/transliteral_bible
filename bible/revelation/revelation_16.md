# [Revelation 16](https://www.blueletterbible.org/kjv/rev/16/1/s_1183001)

<a name="revelation_16_1"></a>Revelation 16:1

And I [akouō](../../strongs/g/g191.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) out of the [naos](../../strongs/g/g3485.md) [legō](../../strongs/g/g3004.md) to the seven [aggelos](../../strongs/g/g32.md), [hypagō](../../strongs/g/g5217.md), and [ekcheō](../../strongs/g/g1632.md) the [phialē](../../strongs/g/g5357.md) of the [thymos](../../strongs/g/g2372.md) of [theos](../../strongs/g/g2316.md) upon [gē](../../strongs/g/g1093.md).

<a name="revelation_16_2"></a>Revelation 16:2

And the first [aperchomai](../../strongs/g/g565.md), and [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon [gē](../../strongs/g/g1093.md); and there [ginomai](../../strongs/g/g1096.md) a [kakos](../../strongs/g/g2556.md) and [ponēros](../../strongs/g/g4190.md) [helkos](../../strongs/g/g1668.md) upon the [anthrōpos](../../strongs/g/g444.md) which had the [charagma](../../strongs/g/g5480.md) of the [thērion](../../strongs/g/g2342.md), and upon them which [proskyneō](../../strongs/g/g4352.md) his [eikōn](../../strongs/g/g1504.md).

<a name="revelation_16_3"></a>Revelation 16:3

And the second [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon the [thalassa](../../strongs/g/g2281.md); and it became as the [haima](../../strongs/g/g129.md) of a [nekros](../../strongs/g/g3498.md): and every [zaō](../../strongs/g/g2198.md) [psychē](../../strongs/g/g5590.md) [apothnēskō](../../strongs/g/g599.md) in the [thalassa](../../strongs/g/g2281.md).

<a name="revelation_16_4"></a>Revelation 16:4

And the third [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon the [potamos](../../strongs/g/g4215.md) and [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md); and they became [haima](../../strongs/g/g129.md).

<a name="revelation_16_5"></a>Revelation 16:5

And I [akouō](../../strongs/g/g191.md) the [aggelos](../../strongs/g/g32.md) of the [hydōr](../../strongs/g/g5204.md) [legō](../../strongs/g/g3004.md), Thou art [dikaios](../../strongs/g/g1342.md), [kyrios](../../strongs/g/g2962.md), which art, and wast, and shalt [hosios](../../strongs/g/g3741.md), because thou hast [krinō](../../strongs/g/g2919.md) thus. [^1]

<a name="revelation_16_6"></a>Revelation 16:6

For they have [ekcheō](../../strongs/g/g1632.md) the [haima](../../strongs/g/g129.md) of [hagios](../../strongs/g/g40.md) and [prophētēs](../../strongs/g/g4396.md), and thou hast [didōmi](../../strongs/g/g1325.md) them [haima](../../strongs/g/g129.md) to [pinō](../../strongs/g/g4095.md); for they are [axios](../../strongs/g/g514.md).

<a name="revelation_16_7"></a>Revelation 16:7

And I [akouō](../../strongs/g/g191.md) another out of the [thysiastērion](../../strongs/g/g2379.md) [legō](../../strongs/g/g3004.md), [nai](../../strongs/g/g3483.md), [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md), [alēthinos](../../strongs/g/g228.md) and [dikaios](../../strongs/g/g1342.md) are thy [krisis](../../strongs/g/g2920.md).

<a name="revelation_16_8"></a>Revelation 16:8

And the fourth [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon the [hēlios](../../strongs/g/g2246.md); and was [didōmi](../../strongs/g/g1325.md) unto him to [kaumatizō](../../strongs/g/g2739.md) [anthrōpos](../../strongs/g/g444.md) with [pyr](../../strongs/g/g4442.md).

<a name="revelation_16_9"></a>Revelation 16:9

And [anthrōpos](../../strongs/g/g444.md) were [kaumatizō](../../strongs/g/g2739.md) with [megas](../../strongs/g/g3173.md) [kauma](../../strongs/g/g2738.md), and [blasphēmeō](../../strongs/g/g987.md) the [onoma](../../strongs/g/g3686.md) of [theos](../../strongs/g/g2316.md), which hath [exousia](../../strongs/g/g1849.md) over these [plēgē](../../strongs/g/g4127.md): and they [metanoeō](../../strongs/g/g3340.md) not to [didōmi](../../strongs/g/g1325.md) him [doxa](../../strongs/g/g1391.md).

<a name="revelation_16_10"></a>Revelation 16:10

And the fifth [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon the [thronos](../../strongs/g/g2362.md) of the [thērion](../../strongs/g/g2342.md); and his [basileia](../../strongs/g/g932.md) was [skotoō](../../strongs/g/g4656.md); and they [masaomai](../../strongs/g/g3145.md) their [glōssa](../../strongs/g/g1100.md) for [ponos](../../strongs/g/g4192.md),

<a name="revelation_16_11"></a>Revelation 16:11

And [blasphēmeō](../../strongs/g/g987.md) the [theos](../../strongs/g/g2316.md) of [ouranos](../../strongs/g/g3772.md) because of their [ponos](../../strongs/g/g4192.md) and their [helkos](../../strongs/g/g1668.md), and [metanoeō](../../strongs/g/g3340.md) not of their [ergon](../../strongs/g/g2041.md).

<a name="revelation_16_12"></a>Revelation 16:12

And the sixth [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) upon the [megas](../../strongs/g/g3173.md) [potamos](../../strongs/g/g4215.md) [Euphratēs](../../strongs/g/g2166.md); and the [hydōr](../../strongs/g/g5204.md) thereof was [xērainō](../../strongs/g/g3583.md), that [hodos](../../strongs/g/g3598.md) of the [basileus](../../strongs/g/g935.md) of the [anatolē](../../strongs/g/g395.md) [hēlios](../../strongs/g/g2246.md) might be [hetoimazō](../../strongs/g/g2090.md).

<a name="revelation_16_13"></a>Revelation 16:13

And I [eidō](../../strongs/g/g1492.md) three [akathartos](../../strongs/g/g169.md) [pneuma](../../strongs/g/g4151.md) [homoios](../../strongs/g/g3664.md) [batrachos](../../strongs/g/g944.md) out of the [stoma](../../strongs/g/g4750.md) of the [drakōn](../../strongs/g/g1404.md), and out of the [stoma](../../strongs/g/g4750.md) of the [thērion](../../strongs/g/g2342.md), and out of the [stoma](../../strongs/g/g4750.md) of the [pseudoprophētēs](../../strongs/g/g5578.md).

<a name="revelation_16_14"></a>Revelation 16:14

For they are the [pneuma](../../strongs/g/g4151.md) of [daimōn](../../strongs/g/g1142.md), [poieō](../../strongs/g/g4160.md) [sēmeion](../../strongs/g/g4592.md), which [ekporeuomai](../../strongs/g/g1607.md) unto the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md) and of the whole [oikoumenē](../../strongs/g/g3625.md), to [synagō](../../strongs/g/g4863.md) them to the [polemos](../../strongs/g/g4171.md) of that [megas](../../strongs/g/g3173.md) [hēmera](../../strongs/g/g2250.md) of [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md).

<a name="revelation_16_15"></a>Revelation 16:15

[idou](../../strongs/g/g2400.md), I [erchomai](../../strongs/g/g2064.md) as a [kleptēs](../../strongs/g/g2812.md). [makarios](../../strongs/g/g3107.md) is he that [grēgoreō](../../strongs/g/g1127.md), and [tēreō](../../strongs/g/g5083.md) his [himation](../../strongs/g/g2440.md), lest he [peripateō](../../strongs/g/g4043.md) [gymnos](../../strongs/g/g1131.md), and they [blepō](../../strongs/g/g991.md) his [aschemosyne](../../strongs/g/g808.md).

<a name="revelation_16_16"></a>Revelation 16:16

And he [synagō](../../strongs/g/g4863.md) them into a [topos](../../strongs/g/g5117.md) [kaleō](../../strongs/g/g2564.md) in [Hebraïsti](../../strongs/g/g1447.md) [harmagedōn](../../strongs/g/g717.md).

<a name="revelation_16_17"></a>Revelation 16:17

And the seventh [aggelos](../../strongs/g/g32.md) [ekcheō](../../strongs/g/g1632.md) his [phialē](../../strongs/g/g5357.md) into the [aēr](../../strongs/g/g109.md); and there [exerchomai](../../strongs/g/g1831.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) out of the [naos](../../strongs/g/g3485.md) of [ouranos](../../strongs/g/g3772.md), from the [thronos](../../strongs/g/g2362.md), [legō](../../strongs/g/g3004.md), It is [ginomai](../../strongs/g/g1096.md).

<a name="revelation_16_18"></a>Revelation 16:18

And there were [phōnē](../../strongs/g/g5456.md), and [brontē](../../strongs/g/g1027.md), and [astrapē](../../strongs/g/g796.md); and there was a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md), such as was not since [anthrōpos](../../strongs/g/g444.md) were upon [gē](../../strongs/g/g1093.md), so [tēlikoutos](../../strongs/g/g5082.md) a [seismos](../../strongs/g/g4578.md), and so [megas](../../strongs/g/g3173.md).

<a name="revelation_16_19"></a>Revelation 16:19

And the [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md) was [ginomai](../../strongs/g/g1096.md) into three [meros](../../strongs/g/g3313.md), and the [polis](../../strongs/g/g4172.md) of the [ethnos](../../strongs/g/g1484.md) [piptō](../../strongs/g/g4098.md): and [megas](../../strongs/g/g3173.md) [Babylōn](../../strongs/g/g897.md) in [mnaomai](../../strongs/g/g3415.md) before [theos](../../strongs/g/g2316.md), to [didōmi](../../strongs/g/g1325.md) unto her the [potērion](../../strongs/g/g4221.md) of the [oinos](../../strongs/g/g3631.md) of the [thymos](../../strongs/g/g2372.md) of his [orgē](../../strongs/g/g3709.md).

<a name="revelation_16_20"></a>Revelation 16:20

And every [nēsos](../../strongs/g/g3520.md) [pheugō](../../strongs/g/g5343.md), and the [oros](../../strongs/g/g3735.md) were not [heuriskō](../../strongs/g/g2147.md).

<a name="revelation_16_21"></a>Revelation 16:21

And there [katabainō](../../strongs/g/g2597.md) upon [anthrōpos](../../strongs/g/g444.md) a [megas](../../strongs/g/g3173.md) [chalaza](../../strongs/g/g5464.md) out of [ouranos](../../strongs/g/g3772.md), about the [talantiaios](../../strongs/g/g5006.md): and [anthrōpos](../../strongs/g/g444.md) [blasphēmeō](../../strongs/g/g987.md) [theos](../../strongs/g/g2316.md) because of the [plēgē](../../strongs/g/g4127.md) of the [chalaza](../../strongs/g/g5464.md); for the [plēgē](../../strongs/g/g4127.md) thereof was [sphodra](../../strongs/g/g4970.md) [megas](../../strongs/g/g3173.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 15](revelation_15.md) - [Revelation 17](revelation_17.md)

---

[^1]: [Revelation 16:5 Commentary](../../commentary/revelation/revelation_16_commentary.md#revelation_16_5)
