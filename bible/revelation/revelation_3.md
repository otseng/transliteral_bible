# [Revelation 3](https://www.blueletterbible.org/kjv/rev/3/1/s_1170001)

<a name="revelation_3_1"></a>Revelation 3:1

And unto the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) in [sardeis](../../strongs/g/g4554.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) he that hath the seven [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md), and the seven [astēr](../../strongs/g/g792.md); I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), that thou hast an [onoma](../../strongs/g/g3686.md) that thou [zaō](../../strongs/g/g2198.md), and art [nekros](../../strongs/g/g3498.md).

<a name="revelation_3_2"></a>Revelation 3:2

Be [grēgoreō](../../strongs/g/g1127.md), and [stērizō](../../strongs/g/g4741.md) the things which [loipos](../../strongs/g/g3062.md), that are ready to [apothnēskō](../../strongs/g/g599.md): for I have not [heuriskō](../../strongs/g/g2147.md) thy [ergon](../../strongs/g/g2041.md) [plēroō](../../strongs/g/g4137.md) before [theos](../../strongs/g/g2316.md).

<a name="revelation_3_3"></a>Revelation 3:3

[mnēmoneuō](../../strongs/g/g3421.md) therefore how thou hast [lambanō](../../strongs/g/g2983.md) and [akouō](../../strongs/g/g191.md), and [tēreō](../../strongs/g/g5083.md), and [metanoeō](../../strongs/g/g3340.md). If therefore thou shalt not [grēgoreō](../../strongs/g/g1127.md), I will [hēkō](../../strongs/g/g2240.md) on thee as a [kleptēs](../../strongs/g/g2812.md), and thou shalt not [ginōskō](../../strongs/g/g1097.md) what [hōra](../../strongs/g/g5610.md) I will [hēkō](../../strongs/g/g2240.md) upon thee.

<a name="revelation_3_4"></a>Revelation 3:4

Thou hast an [oligos](../../strongs/g/g3641.md) [onoma](../../strongs/g/g3686.md) even in [sardeis](../../strongs/g/g4554.md) which have not [molynō](../../strongs/g/g3435.md) their [himation](../../strongs/g/g2440.md); and they shall [peripateō](../../strongs/g/g4043.md) with me in [leukos](../../strongs/g/g3022.md): for they are [axios](../../strongs/g/g514.md).

<a name="revelation_3_5"></a>Revelation 3:5

He that [nikaō](../../strongs/g/g3528.md), the same shall be [periballō](../../strongs/g/g4016.md) in [leukos](../../strongs/g/g3022.md) [himation](../../strongs/g/g2440.md); and I will not [exaleiphō](../../strongs/g/g1813.md) his [onoma](../../strongs/g/g3686.md) out of the [biblos](../../strongs/g/g976.md) of [zōē](../../strongs/g/g2222.md), but I will [exomologeō](../../strongs/g/g1843.md) his [onoma](../../strongs/g/g3686.md) before my [patēr](../../strongs/g/g3962.md), and before his [aggelos](../../strongs/g/g32.md).

<a name="revelation_3_6"></a>Revelation 3:6

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md).

<a name="revelation_3_7"></a>Revelation 3:7

And to the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) in [philadelpheia](../../strongs/g/g5359.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) he that is [hagios](../../strongs/g/g40.md), he that is [alēthinos](../../strongs/g/g228.md), he that hath the [kleis](../../strongs/g/g2807.md) of [Dabid](../../strongs/g/g1138.md), he that [anoigō](../../strongs/g/g455.md), and no man [kleiō](../../strongs/g/g2808.md); and [kleiō](../../strongs/g/g2808.md), and no man [anoigō](../../strongs/g/g455.md);

<a name="revelation_3_8"></a>Revelation 3:8

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md): [idou](../../strongs/g/g2400.md), I have [didōmi](../../strongs/g/g1325.md) before thee an [anoigō](../../strongs/g/g455.md) [thyra](../../strongs/g/g2374.md), and no man can [kleiō](../../strongs/g/g2808.md) it: for thou hast a [mikros](../../strongs/g/g3398.md) [dynamis](../../strongs/g/g1411.md), and hast [tēreō](../../strongs/g/g5083.md) my [logos](../../strongs/g/g3056.md), and hast not [arneomai](../../strongs/g/g720.md) my [onoma](../../strongs/g/g3686.md).

<a name="revelation_3_9"></a>Revelation 3:9

[idou](../../strongs/g/g2400.md), I will [didōmi](../../strongs/g/g1325.md) them of the [synagōgē](../../strongs/g/g4864.md) of [Satanas](../../strongs/g/g4567.md), which [legō](../../strongs/g/g3004.md) they are [Ioudaios](../../strongs/g/g2453.md), and are not, but do [pseudomai](../../strongs/g/g5574.md); [idou](../../strongs/g/g2400.md), I will [poieō](../../strongs/g/g4160.md) them to [hēkō](../../strongs/g/g2240.md) and [proskyneō](../../strongs/g/g4352.md) before thy [pous](../../strongs/g/g4228.md), and to [ginōskō](../../strongs/g/g1097.md) that I have [agapaō](../../strongs/g/g25.md) thee.

<a name="revelation_3_10"></a>Revelation 3:10

Because thou hast [tēreō](../../strongs/g/g5083.md) the [logos](../../strongs/g/g3056.md) of my [hypomonē](../../strongs/g/g5281.md), I also will [tēreō](../../strongs/g/g5083.md) thee from the [hōra](../../strongs/g/g5610.md) of [peirasmos](../../strongs/g/g3986.md), which shall [erchomai](../../strongs/g/g2064.md) upon all the [oikoumenē](../../strongs/g/g3625.md), to [peirazō](../../strongs/g/g3985.md) them that [katoikeō](../../strongs/g/g2730.md) upon [gē](../../strongs/g/g1093.md).

<a name="revelation_3_11"></a>Revelation 3:11

[idou](../../strongs/g/g2400.md), I [erchomai](../../strongs/g/g2064.md) [tachy](../../strongs/g/g5035.md): [krateō](../../strongs/g/g2902.md) which thou hast, that [mēdeis](../../strongs/g/g3367.md) [lambanō](../../strongs/g/g2983.md) thy [stephanos](../../strongs/g/g4735.md).

<a name="revelation_3_12"></a>Revelation 3:12

Him that [nikaō](../../strongs/g/g3528.md) will I [poieō](../../strongs/g/g4160.md) a [stylos](../../strongs/g/g4769.md) in the [naos](../../strongs/g/g3485.md) of my [theos](../../strongs/g/g2316.md), and he shall [exerchomai](../../strongs/g/g1831.md) no more out: and I will [graphō](../../strongs/g/g1125.md) upon him the [onoma](../../strongs/g/g3686.md) of my [theos](../../strongs/g/g2316.md), and the [onoma](../../strongs/g/g3686.md) of the [polis](../../strongs/g/g4172.md) of my [theos](../../strongs/g/g2316.md), [kainos](../../strongs/g/g2537.md) [Ierousalēm](../../strongs/g/g2419.md), which [katabainō](../../strongs/g/g2597.md) out of [ouranos](../../strongs/g/g3772.md) from my [theos](../../strongs/g/g2316.md): and my new [onoma](../../strongs/g/g3686.md).

<a name="revelation_3_13"></a>Revelation 3:13

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md).

<a name="revelation_3_14"></a>Revelation 3:14

And unto the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) of the [laodikeus](../../strongs/g/g2994.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) the [amēn](../../strongs/g/g281.md), the [pistos](../../strongs/g/g4103.md) and [alēthinos](../../strongs/g/g228.md) [martys](../../strongs/g/g3144.md), the [archē](../../strongs/g/g746.md) of the [ktisis](../../strongs/g/g2937.md) of [theos](../../strongs/g/g2316.md);

<a name="revelation_3_15"></a>Revelation 3:15

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), that thou art neither [psychros](../../strongs/g/g5593.md) nor [zestos](../../strongs/g/g2200.md): I [ophelon](../../strongs/g/g3785.md) thou wert [psychros](../../strongs/g/g5593.md) or [zestos](../../strongs/g/g2200.md).

<a name="revelation_3_16"></a>Revelation 3:16

So then because thou art [chliaros](../../strongs/g/g5513.md), and neither [psychros](../../strongs/g/g5593.md) nor [zestos](../../strongs/g/g2200.md), I will [emeō](../../strongs/g/g1692.md) thee out of my [stoma](../../strongs/g/g4750.md).

<a name="revelation_3_17"></a>Revelation 3:17

Because thou [legō](../../strongs/g/g3004.md), I am [plousios](../../strongs/g/g4145.md), and [plouteō](../../strongs/g/g4147.md), and have [chreia](../../strongs/g/g5532.md) [oudeis](../../strongs/g/g3762.md); and [eidō](../../strongs/g/g1492.md) not that thou art [talaipōros](../../strongs/g/g5005.md), and [eleeinos](../../strongs/g/g1652.md), and [ptōchos](../../strongs/g/g4434.md), and [typhlos](../../strongs/g/g5185.md), and [gymnos](../../strongs/g/g1131.md):

<a name="revelation_3_18"></a>Revelation 3:18

I [symbouleuō](../../strongs/g/g4823.md) thee to [agorazō](../../strongs/g/g59.md) of me [chrysion](../../strongs/g/g5553.md) [pyroō](../../strongs/g/g4448.md) in the [pyr](../../strongs/g/g4442.md), that thou mayest [plouteō](../../strongs/g/g4147.md); and [leukos](../../strongs/g/g3022.md) [himation](../../strongs/g/g2440.md), that thou mayest be [periballō](../../strongs/g/g4016.md), and the [aischynē](../../strongs/g/g152.md) of thy [gymnotēs](../../strongs/g/g1132.md) [phaneroō](../../strongs/g/g5319.md) not; and [egchriō](../../strongs/g/g1472.md) thine [ophthalmos](../../strongs/g/g3788.md) with [kollourion](../../strongs/g/g2854.md), that thou mayest [blepō](../../strongs/g/g991.md).

<a name="revelation_3_19"></a>Revelation 3:19

As many as I [phileō](../../strongs/g/g5368.md), I [elegchō](../../strongs/g/g1651.md) and [paideuō](../../strongs/g/g3811.md): be [zēloō](../../strongs/g/g2206.md) therefore, and [metanoeō](../../strongs/g/g3340.md).

<a name="revelation_3_20"></a>Revelation 3:20

[idou](../../strongs/g/g2400.md), I [histēmi](../../strongs/g/g2476.md) at the [thyra](../../strongs/g/g2374.md), and [krouō](../../strongs/g/g2925.md): if any man [akouō](../../strongs/g/g191.md) my [phōnē](../../strongs/g/g5456.md), and [anoigō](../../strongs/g/g455.md) the [thyra](../../strongs/g/g2374.md), I will [eiserchomai](../../strongs/g/g1525.md) to him, and will [deipneō](../../strongs/g/g1172.md) with him, and he with me.

<a name="revelation_3_21"></a>Revelation 3:21

To him that [nikaō](../../strongs/g/g3528.md) will I [didōmi](../../strongs/g/g1325.md) to [kathizō](../../strongs/g/g2523.md) with me in my [thronos](../../strongs/g/g2362.md), even as I also [nikaō](../../strongs/g/g3528.md), and am [kathizō](../../strongs/g/g2523.md) with my [patēr](../../strongs/g/g3962.md) in his [thronos](../../strongs/g/g2362.md).

<a name="revelation_3_22"></a>Revelation 3:22

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 2](revelation_2.md) - [Revelation 4](revelation_4.md)