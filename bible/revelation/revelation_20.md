# [Revelation 20](https://www.blueletterbible.org/kjv/rev/20/1/s_1187001)

<a name="revelation_20_1"></a>Revelation 20:1

And I [eidō](../../strongs/g/g1492.md) an [aggelos](../../strongs/g/g32.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), having the [kleis](../../strongs/g/g2807.md) of the [abyssos](../../strongs/g/g12.md) and a [megas](../../strongs/g/g3173.md) [halysis](../../strongs/g/g254.md) in his [cheir](../../strongs/g/g5495.md).

<a name="revelation_20_2"></a>Revelation 20:2

And he [krateō](../../strongs/g/g2902.md) the [drakōn](../../strongs/g/g1404.md), that [archaios](../../strongs/g/g744.md) [ophis](../../strongs/g/g3789.md), which is the [diabolos](../../strongs/g/g1228.md), and [Satanas](../../strongs/g/g4567.md), and [deō](../../strongs/g/g1210.md) him a thousand [etos](../../strongs/g/g2094.md),

<a name="revelation_20_3"></a>Revelation 20:3

And [ballō](../../strongs/g/g906.md) him into the [abyssos](../../strongs/g/g12.md), and [kleiō](../../strongs/g/g2808.md) him, and [sphragizō](../../strongs/g/g4972.md) upon him, that he should [planaō](../../strongs/g/g4105.md) the [ethnos](../../strongs/g/g1484.md) no more, till the thousand [etos](../../strongs/g/g2094.md) should be [teleō](../../strongs/g/g5055.md): and after that he must be [lyō](../../strongs/g/g3089.md) a [mikros](../../strongs/g/g3398.md) [chronos](../../strongs/g/g5550.md).

<a name="revelation_20_4"></a>Revelation 20:4

And I [eidō](../../strongs/g/g1492.md) [thronos](../../strongs/g/g2362.md), and they [kathizō](../../strongs/g/g2523.md) upon them, and [krima](../../strongs/g/g2917.md) was [didōmi](../../strongs/g/g1325.md) unto them: and the [psychē](../../strongs/g/g5590.md) of them that were [pelekizō](../../strongs/g/g3990.md) for the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md), and for the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and which had not [proskyneō](../../strongs/g/g4352.md) the [thērion](../../strongs/g/g2342.md), neither his [eikōn](../../strongs/g/g1504.md), neither had [lambanō](../../strongs/g/g2983.md) [charagma](../../strongs/g/g5480.md) upon their [metōpon](../../strongs/g/g3359.md), or in their [cheir](../../strongs/g/g5495.md); and they [zaō](../../strongs/g/g2198.md) and [basileuō](../../strongs/g/g936.md) with [Christos](../../strongs/g/g5547.md) a thousand [etos](../../strongs/g/g2094.md).

<a name="revelation_20_5"></a>Revelation 20:5

But the [loipos](../../strongs/g/g3062.md) of the [nekros](../../strongs/g/g3498.md) [anazaō](../../strongs/g/g326.md) not until the thousand [etos](../../strongs/g/g2094.md) were [teleō](../../strongs/g/g5055.md). This the first [anastasis](../../strongs/g/g386.md).

<a name="revelation_20_6"></a>Revelation 20:6

[makarios](../../strongs/g/g3107.md) and [hagios](../../strongs/g/g40.md) he that hath [meros](../../strongs/g/g3313.md) in the first [anastasis](../../strongs/g/g386.md): on such the second [thanatos](../../strongs/g/g2288.md) hath no [exousia](../../strongs/g/g1849.md), but they shall be [hiereus](../../strongs/g/g2409.md) of [theos](../../strongs/g/g2316.md) and of [Christos](../../strongs/g/g5547.md), and shall [basileuō](../../strongs/g/g936.md) with him a thousand [etos](../../strongs/g/g2094.md).

<a name="revelation_20_7"></a>Revelation 20:7

And when the thousand [etos](../../strongs/g/g2094.md) are [teleō](../../strongs/g/g5055.md), [Satanas](../../strongs/g/g4567.md) shall be [lyō](../../strongs/g/g3089.md) out of his [phylakē](../../strongs/g/g5438.md),

<a name="revelation_20_8"></a>Revelation 20:8

And shall [exerchomai](../../strongs/g/g1831.md) to [planaō](../../strongs/g/g4105.md) the [ethnos](../../strongs/g/g1484.md) which are in the four [gōnia](../../strongs/g/g1137.md) of [gē](../../strongs/g/g1093.md), [Gōg](../../strongs/g/g1136.md), and [Magōg](../../strongs/g/g3098.md), to [synagō](../../strongs/g/g4863.md) them to [polemos](../../strongs/g/g4171.md): the [arithmos](../../strongs/g/g706.md) of whom is as the [ammos](../../strongs/g/g285.md) of the [thalassa](../../strongs/g/g2281.md).

<a name="revelation_20_9"></a>Revelation 20:9

And they [anabainō](../../strongs/g/g305.md) on the [platos](../../strongs/g/g4114.md) of [gē](../../strongs/g/g1093.md), and [kykloō](../../strongs/g/g2944.md) the [parembolē](../../strongs/g/g3925.md) of the [hagios](../../strongs/g/g40.md), and the [agapaō](../../strongs/g/g25.md) [polis](../../strongs/g/g4172.md): and [pyr](../../strongs/g/g4442.md) [katabainō](../../strongs/g/g2597.md) from [theos](../../strongs/g/g2316.md) out of [ouranos](../../strongs/g/g3772.md), and [katesthiō](../../strongs/g/g2719.md) them.

<a name="revelation_20_10"></a>Revelation 20:10

And the [diabolos](../../strongs/g/g1228.md) that [planaō](../../strongs/g/g4105.md) them was [ballō](../../strongs/g/g906.md) into the [limnē](../../strongs/g/g3041.md) of [pyr](../../strongs/g/g4442.md) and [theion](../../strongs/g/g2303.md), where the [thērion](../../strongs/g/g2342.md) and the [pseudoprophētēs](../../strongs/g/g5578.md) are, and shall be [basanizō](../../strongs/g/g928.md) [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_20_11"></a>Revelation 20:11

And I [eidō](../../strongs/g/g1492.md) a [megas](../../strongs/g/g3173.md) [leukos](../../strongs/g/g3022.md) [thronos](../../strongs/g/g2362.md), and him that [kathēmai](../../strongs/g/g2521.md) on it, from whose [prosōpon](../../strongs/g/g4383.md) [gē](../../strongs/g/g1093.md) and the [ouranos](../../strongs/g/g3772.md) [pheugō](../../strongs/g/g5343.md); and there was [heuriskō](../../strongs/g/g2147.md) no [topos](../../strongs/g/g5117.md) for them.

<a name="revelation_20_12"></a>Revelation 20:12

And I [eidō](../../strongs/g/g1492.md) the [nekros](../../strongs/g/g3498.md), [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md), [histēmi](../../strongs/g/g2476.md) before [theos](../../strongs/g/g2316.md); and the [biblion](../../strongs/g/g975.md) were [anoigō](../../strongs/g/g455.md): and another [biblion](../../strongs/g/g975.md) was [anoigō](../../strongs/g/g455.md), which is of [zōē](../../strongs/g/g2222.md): and the [nekros](../../strongs/g/g3498.md) were [krinō](../../strongs/g/g2919.md) out of those things which were [graphō](../../strongs/g/g1125.md) in the [biblion](../../strongs/g/g975.md), according to their [ergon](../../strongs/g/g2041.md).

<a name="revelation_20_13"></a>Revelation 20:13

And the [thalassa](../../strongs/g/g2281.md) [didōmi](../../strongs/g/g1325.md) the [nekros](../../strongs/g/g3498.md) which were in it; and [thanatos](../../strongs/g/g2288.md) and [hadēs](../../strongs/g/g86.md) [didōmi](../../strongs/g/g1325.md) the [nekros](../../strongs/g/g3498.md) which were in them: and they were [krinō](../../strongs/g/g2919.md) [hekastos](../../strongs/g/g1538.md) according to their [ergon](../../strongs/g/g2041.md).

<a name="revelation_20_14"></a>Revelation 20:14

And [thanatos](../../strongs/g/g2288.md) and [hadēs](../../strongs/g/g86.md) were [ballō](../../strongs/g/g906.md) into the [limnē](../../strongs/g/g3041.md) of [pyr](../../strongs/g/g4442.md). This is the second [thanatos](../../strongs/g/g2288.md).

<a name="revelation_20_15"></a>Revelation 20:15

And whosoever was not [heuriskō](../../strongs/g/g2147.md) [graphō](../../strongs/g/g1125.md) in the [biblos](../../strongs/g/g976.md) of [zōē](../../strongs/g/g2222.md) was [ballō](../../strongs/g/g906.md) into the [limnē](../../strongs/g/g3041.md) of [pyr](../../strongs/g/g4442.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 19](revelation_19.md) - [Revelation 21](revelation_21.md)