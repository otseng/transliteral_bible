# [Revelation 6](https://www.blueletterbible.org/kjv/rev/6/1/s_1173001)

<a name="revelation_6_1"></a>Revelation 6:1

And I [eidō](../../strongs/g/g1492.md) when the [arnion](../../strongs/g/g721.md) [anoigō](../../strongs/g/g455.md) one of the [sphragis](../../strongs/g/g4973.md), and I [akouō](../../strongs/g/g191.md), as it were the [phōnē](../../strongs/g/g5456.md) of [brontē](../../strongs/g/g1027.md), one of the four [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), [erchomai](../../strongs/g/g2064.md) and [blepō](../../strongs/g/g991.md).

<a name="revelation_6_2"></a>Revelation 6:2

And I [eidō](../../strongs/g/g1492.md), and [idou](../../strongs/g/g2400.md) a [leukos](../../strongs/g/g3022.md) [hippos](../../strongs/g/g2462.md): and he that [kathēmai](../../strongs/g/g2521.md) on him had a [toxon](../../strongs/g/g5115.md); and a [stephanos](../../strongs/g/g4735.md) was [didōmi](../../strongs/g/g1325.md) unto him: and he [exerchomai](../../strongs/g/g1831.md) [nikaō](../../strongs/g/g3528.md), and to [nikaō](../../strongs/g/g3528.md).

<a name="revelation_6_3"></a>Revelation 6:3

And when he had [anoigō](../../strongs/g/g455.md) the second [sphragis](../../strongs/g/g4973.md), I [akouō](../../strongs/g/g191.md) the second [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), [erchomai](../../strongs/g/g2064.md) and [blepō](../../strongs/g/g991.md).

<a name="revelation_6_4"></a>Revelation 6:4

And there [exerchomai](../../strongs/g/g1831.md) another [hippos](../../strongs/g/g2462.md) [pyrros](../../strongs/g/g4450.md): and was [didōmi](../../strongs/g/g1325.md) to him that [kathēmai](../../strongs/g/g2521.md) thereon to [lambanō](../../strongs/g/g2983.md) [eirēnē](../../strongs/g/g1515.md) from [gē](../../strongs/g/g1093.md), and that they should [sphazō](../../strongs/g/g4969.md) [allēlōn](../../strongs/g/g240.md): and there was [didōmi](../../strongs/g/g1325.md) unto him a [megas](../../strongs/g/g3173.md) [machaira](../../strongs/g/g3162.md).

<a name="revelation_6_5"></a>Revelation 6:5

And when he had [anoigō](../../strongs/g/g455.md) the third [sphragis](../../strongs/g/g4973.md), I [akouō](../../strongs/g/g191.md) the third [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), [erchomai](../../strongs/g/g2064.md) and [blepō](../../strongs/g/g991.md). And I [eidō](../../strongs/g/g1492.md), and [idou](../../strongs/g/g2400.md) a [melas](../../strongs/g/g3189.md) [hippos](../../strongs/g/g2462.md); and he that [kathēmai](../../strongs/g/g2521.md) on him had a [zygos](../../strongs/g/g2218.md) in his [cheir](../../strongs/g/g5495.md).

<a name="revelation_6_6"></a>Revelation 6:6

And I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) in the midst of the four [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), A [choinix](../../strongs/g/g5518.md) of [sitos](../../strongs/g/g4621.md) for a [dēnarion](../../strongs/g/g1220.md), and three [choinix](../../strongs/g/g5518.md) of [krithē](../../strongs/g/g2915.md) for a [dēnarion](../../strongs/g/g1220.md); and thou [adikeō](../../strongs/g/g91.md) not the [elaion](../../strongs/g/g1637.md) and the [oinos](../../strongs/g/g3631.md).

<a name="revelation_6_7"></a>Revelation 6:7

And when he had [anoigō](../../strongs/g/g455.md) the fourth [sphragis](../../strongs/g/g4973.md), I [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of the fourth [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), [erchomai](../../strongs/g/g2064.md) and [blepō](../../strongs/g/g991.md).

<a name="revelation_6_8"></a>Revelation 6:8

And I [eidō](../../strongs/g/g1492.md), and [idou](../../strongs/g/g2400.md) a [chlōros](../../strongs/g/g5515.md) [hippos](../../strongs/g/g2462.md): and his [onoma](../../strongs/g/g3686.md) that [kathēmai](../../strongs/g/g2521.md) on him was [thanatos](../../strongs/g/g2288.md), and [hadēs](../../strongs/g/g86.md) [akoloutheō](../../strongs/g/g190.md) with him. And [exousia](../../strongs/g/g1849.md) was [didōmi](../../strongs/g/g1325.md) unto them over the fourth part of [gē](../../strongs/g/g1093.md), to [apokteinō](../../strongs/g/g615.md) with [rhomphaia](../../strongs/g/g4501.md), and with [limos](../../strongs/g/g3042.md), and with [thanatos](../../strongs/g/g2288.md), and with the [thērion](../../strongs/g/g2342.md) of [gē](../../strongs/g/g1093.md).

<a name="revelation_6_9"></a>Revelation 6:9

And when he had [anoigō](../../strongs/g/g455.md) the fifth [sphragis](../../strongs/g/g4973.md), I [eidō](../../strongs/g/g1492.md) [hypokatō](../../strongs/g/g5270.md) the [thysiastērion](../../strongs/g/g2379.md) the [psychē](../../strongs/g/g5590.md) of them that were [sphazō](../../strongs/g/g4969.md) for the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and for the [martyria](../../strongs/g/g3141.md) which they held:

<a name="revelation_6_10"></a>Revelation 6:10

And they [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md), How long, [despotēs](../../strongs/g/g1203.md), [hagios](../../strongs/g/g40.md) and [alēthinos](../../strongs/g/g228.md), dost thou not [krinō](../../strongs/g/g2919.md) and [ekdikeō](../../strongs/g/g1556.md) our [haima](../../strongs/g/g129.md) on them that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md)?

<a name="revelation_6_11"></a>Revelation 6:11

And [leukos](../../strongs/g/g3022.md) [stolē](../../strongs/g/g4749.md) were [didōmi](../../strongs/g/g1325.md) unto every one of them; and it was [rheō](../../strongs/g/g4483.md) unto them, that they should [anapauō](../../strongs/g/g373.md) yet for a [mikros](../../strongs/g/g3398.md) [chronos](../../strongs/g/g5550.md), until their [syndoulos](../../strongs/g/g4889.md) also and their [adelphos](../../strongs/g/g80.md), that should be [apokteinō](../../strongs/g/g615.md) as they were, should be [plēroō](../../strongs/g/g4137.md).

<a name="revelation_6_12"></a>Revelation 6:12

And I [eidō](../../strongs/g/g1492.md) when he had [anoigō](../../strongs/g/g455.md) the sixth [sphragis](../../strongs/g/g4973.md), and, [idou](../../strongs/g/g2400.md), there was a [megas](../../strongs/g/g3173.md) [seismos](../../strongs/g/g4578.md); and the [hēlios](../../strongs/g/g2246.md) became [melas](../../strongs/g/g3189.md) as [sakkos](../../strongs/g/g4526.md) of [trichinos](../../strongs/g/g5155.md), and the [selēnē](../../strongs/g/g4582.md) became as [haima](../../strongs/g/g129.md);

<a name="revelation_6_13"></a>Revelation 6:13

And the [astēr](../../strongs/g/g792.md) of [ouranos](../../strongs/g/g3772.md) [piptō](../../strongs/g/g4098.md) unto [gē](../../strongs/g/g1093.md), even as a [sykē](../../strongs/g/g4808.md) [ballō](../../strongs/g/g906.md) her [olynthos](../../strongs/g/g3653.md), when she is [seiō](../../strongs/g/g4579.md) of a [megas](../../strongs/g/g3173.md) [anemos](../../strongs/g/g417.md).

<a name="revelation_6_14"></a>Revelation 6:14

And the [ouranos](../../strongs/g/g3772.md) [apochōrizō](../../strongs/g/g673.md) as a [biblion](../../strongs/g/g975.md) when it is [heilissō](../../strongs/g/g1507.md); and every [oros](../../strongs/g/g3735.md) and [nēsos](../../strongs/g/g3520.md) were [kineō](../../strongs/g/g2795.md) out of their [topos](../../strongs/g/g5117.md).

<a name="revelation_6_15"></a>Revelation 6:15

And the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md), and [megistan](../../strongs/g/g3175.md), and [plousios](../../strongs/g/g4145.md), and [chiliarchos](../../strongs/g/g5506.md), and [dynatos](../../strongs/g/g1415.md), and every [doulos](../../strongs/g/g1401.md), and every [eleutheros](../../strongs/g/g1658.md), [kryptō](../../strongs/g/g2928.md) themselves in the [spēlaion](../../strongs/g/g4693.md) and in the [petra](../../strongs/g/g4073.md) of the [oros](../../strongs/g/g3735.md);

<a name="revelation_6_16"></a>Revelation 6:16

And [legō](../../strongs/g/g3004.md) to the [oros](../../strongs/g/g3735.md) and [petra](../../strongs/g/g4073.md), [piptō](../../strongs/g/g4098.md) on us, and [kryptō](../../strongs/g/g2928.md) us from the [prosōpon](../../strongs/g/g4383.md) of him that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md), and from the [orgē](../../strongs/g/g3709.md) of the [arnion](../../strongs/g/g721.md):

<a name="revelation_6_17"></a>Revelation 6:17

For the [megas](../../strongs/g/g3173.md) [hēmera](../../strongs/g/g2250.md) of his [orgē](../../strongs/g/g3709.md) is [erchomai](../../strongs/g/g2064.md); and who shall be able to [histēmi](../../strongs/g/g2476.md)?

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 5](revelation_5.md) - [Revelation 7](revelation_7.md)