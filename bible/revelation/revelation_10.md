# [Revelation 10](https://www.blueletterbible.org/kjv/rev/10/1/s_1177001)

<a name="revelation_10_1"></a>Revelation 10:1

And I [eidō](../../strongs/g/g1492.md) another [ischyros](../../strongs/g/g2478.md) [aggelos](../../strongs/g/g32.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md), [periballō](../../strongs/g/g4016.md) with a [nephelē](../../strongs/g/g3507.md): and an [iris](../../strongs/g/g2463.md) upon his [kephalē](../../strongs/g/g2776.md), and his [prosōpon](../../strongs/g/g4383.md) was as it were the [hēlios](../../strongs/g/g2246.md), and his [pous](../../strongs/g/g4228.md) as [stylos](../../strongs/g/g4769.md) of [pyr](../../strongs/g/g4442.md):

<a name="revelation_10_2"></a>Revelation 10:2

And he had in his [cheir](../../strongs/g/g5495.md) a [biblaridion](../../strongs/g/g974.md) [anoigō](../../strongs/g/g455.md): and he [tithēmi](../../strongs/g/g5087.md) his [dexios](../../strongs/g/g1188.md) [pous](../../strongs/g/g4228.md) upon the [thalassa](../../strongs/g/g2281.md), and [euōnymos](../../strongs/g/g2176.md) on [gē](../../strongs/g/g1093.md),

<a name="revelation_10_3"></a>Revelation 10:3

And [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), as a [leōn](../../strongs/g/g3023.md) [mykaomai](../../strongs/g/g3455.md): and when he had [krazō](../../strongs/g/g2896.md), seven [brontē](../../strongs/g/g1027.md) [laleō](../../strongs/g/g2980.md) their [phōnē](../../strongs/g/g5456.md).

<a name="revelation_10_4"></a>Revelation 10:4

And when the seven [brontē](../../strongs/g/g1027.md) had [laleō](../../strongs/g/g2980.md) their [phōnē](../../strongs/g/g5456.md), I was about to [graphō](../../strongs/g/g1125.md): and I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) from [ouranos](../../strongs/g/g3772.md) [legō](../../strongs/g/g3004.md) unto me, [sphragizō](../../strongs/g/g4972.md) those things which the seven [brontē](../../strongs/g/g1027.md) [laleō](../../strongs/g/g2980.md), and [graphō](../../strongs/g/g1125.md) them not.

<a name="revelation_10_5"></a>Revelation 10:5

And the [aggelos](../../strongs/g/g32.md) which I [eidō](../../strongs/g/g1492.md) [histēmi](../../strongs/g/g2476.md) upon the [thalassa](../../strongs/g/g2281.md) and upon [gē](../../strongs/g/g1093.md) [airō](../../strongs/g/g142.md) his [cheir](../../strongs/g/g5495.md) to [ouranos](../../strongs/g/g3772.md),

<a name="revelation_10_6"></a>Revelation 10:6

And [omnyō](../../strongs/g/g3660.md) by him that [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md), who [ktizō](../../strongs/g/g2936.md) [ouranos](../../strongs/g/g3772.md), and the things that therein are, and [gē](../../strongs/g/g1093.md), and the things that therein are, and the [thalassa](../../strongs/g/g2281.md), and the things which are therein, that there should be [chronos](../../strongs/g/g5550.md) no longer:

<a name="revelation_10_7"></a>Revelation 10:7

But in the [hēmera](../../strongs/g/g2250.md) of the [phōnē](../../strongs/g/g5456.md) of the seventh [aggelos](../../strongs/g/g32.md), when he [mellō](../../strongs/g/g3195.md) to [salpizō](../../strongs/g/g4537.md), the [mystērion](../../strongs/g/g3466.md) of [theos](../../strongs/g/g2316.md) should be [teleō](../../strongs/g/g5055.md), as he hath [euaggelizō](../../strongs/g/g2097.md) to his [doulos](../../strongs/g/g1401.md) the [prophētēs](../../strongs/g/g4396.md).

<a name="revelation_10_8"></a>Revelation 10:8

And the [phōnē](../../strongs/g/g5456.md) which I [akouō](../../strongs/g/g191.md) from [ouranos](../../strongs/g/g3772.md) [laleō](../../strongs/g/g2980.md) unto me again, and [legō](../../strongs/g/g3004.md), [hypagō](../../strongs/g/g5217.md) [lambanō](../../strongs/g/g2983.md) the [biblaridion](../../strongs/g/g974.md) which is [anoigō](../../strongs/g/g455.md) in the [cheir](../../strongs/g/g5495.md) of the [aggelos](../../strongs/g/g32.md) which [histēmi](../../strongs/g/g2476.md) upon the [thalassa](../../strongs/g/g2281.md) and upon [gē](../../strongs/g/g1093.md).

<a name="revelation_10_9"></a>Revelation 10:9

And I [aperchomai](../../strongs/g/g565.md) unto the [aggelos](../../strongs/g/g32.md), and [legō](../../strongs/g/g3004.md) unto him, [didōmi](../../strongs/g/g1325.md) me the [biblaridion](../../strongs/g/g974.md). And he [legō](../../strongs/g/g3004.md) unto me, [lambanō](../../strongs/g/g2983.md), and [katesthiō](../../strongs/g/g2719.md) it; and it shall [pikrainō](../../strongs/g/g4087.md) thy [koilia](../../strongs/g/g2836.md), but it shall be in thy [stoma](../../strongs/g/g4750.md) [glykys](../../strongs/g/g1099.md) as [meli](../../strongs/g/g3192.md).

<a name="revelation_10_10"></a>Revelation 10:10

And I [lambanō](../../strongs/g/g2983.md) the [biblaridion](../../strongs/g/g974.md) out of the [aggelos](../../strongs/g/g32.md) [cheir](../../strongs/g/g5495.md), and [katesthiō](../../strongs/g/g2719.md) it; and it was in my [stoma](../../strongs/g/g4750.md) [glykys](../../strongs/g/g1099.md) as [meli](../../strongs/g/g3192.md): and as soon as I had [phago](../../strongs/g/g5315.md) it, my [koilia](../../strongs/g/g2836.md) was [pikrainō](../../strongs/g/g4087.md).

<a name="revelation_10_11"></a>Revelation 10:11

And he [legō](../../strongs/g/g3004.md) unto me, Thou must [prophēteuō](../../strongs/g/g4395.md) again before [polys](../../strongs/g/g4183.md) [laos](../../strongs/g/g2992.md), and [ethnos](../../strongs/g/g1484.md), and [glōssa](../../strongs/g/g1100.md), and [basileus](../../strongs/g/g935.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 9](revelation_9.md) - [Revelation 11](revelation_11.md)