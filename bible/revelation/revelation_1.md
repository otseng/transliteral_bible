# [Revelation 1](https://www.blueletterbible.org/kjv/rev/1/1/s_1168001)

<a name="revelation_1_1"></a>Revelation 1:1

The [apokalypsis](../../strongs/g/g602.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), which [theos](../../strongs/g/g2316.md) [didōmi](../../strongs/g/g1325.md) unto him, to [deiknyō](../../strongs/g/g1166.md) unto his [doulos](../../strongs/g/g1401.md) things which must [tachos](../../strongs/g/g5034.md) [ginomai](../../strongs/g/g1096.md); and he [apostellō](../../strongs/g/g649.md) and [sēmainō](../../strongs/g/g4591.md) by his [aggelos](../../strongs/g/g32.md) unto his [doulos](../../strongs/g/g1401.md) [Iōannēs](../../strongs/g/g2491.md):

<a name="revelation_1_2"></a>Revelation 1:2

Who [martyreō](../../strongs/g/g3140.md) of the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and of the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and of all things that he [eidō](../../strongs/g/g1492.md).

<a name="revelation_1_3"></a>Revelation 1:3

[makarios](../../strongs/g/g3107.md) he that [anaginōskō](../../strongs/g/g314.md), and they that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of this [prophēteia](../../strongs/g/g4394.md), and [tēreō](../../strongs/g/g5083.md) those things which are [graphō](../../strongs/g/g1125.md) therein: for the [kairos](../../strongs/g/g2540.md) is [eggys](../../strongs/g/g1451.md).

<a name="revelation_1_4"></a>Revelation 1:4

[Iōannēs](../../strongs/g/g2491.md) to the seven [ekklēsia](../../strongs/g/g1577.md) which are in [Asia](../../strongs/g/g773.md): [charis](../../strongs/g/g5485.md) unto you, and [eirēnē](../../strongs/g/g1515.md), from him which is, and which was, and which is to [erchomai](../../strongs/g/g2064.md); and from the seven [pneuma](../../strongs/g/g4151.md) which are before his [thronos](../../strongs/g/g2362.md);

<a name="revelation_1_5"></a>Revelation 1:5

And from [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), the [pistos](../../strongs/g/g4103.md) [martys](../../strongs/g/g3144.md), and the [prōtotokos](../../strongs/g/g4416.md) of the [nekros](../../strongs/g/g3498.md), and the [archōn](../../strongs/g/g758.md) of the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md). Unto him that [agapaō](../../strongs/g/g25.md) us, and [louō](../../strongs/g/g3068.md) us from our [hamartia](../../strongs/g/g266.md) in his own [haima](../../strongs/g/g129.md),

<a name="revelation_1_6"></a>Revelation 1:6

And hath [poieō](../../strongs/g/g4160.md) us [basileus](../../strongs/g/g935.md) and [hiereus](../../strongs/g/g2409.md) unto [theos](../../strongs/g/g2316.md) and his [patēr](../../strongs/g/g3962.md); to him [doxa](../../strongs/g/g1391.md) and [kratos](../../strongs/g/g2904.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [amēn](../../strongs/g/g281.md).

<a name="revelation_1_7"></a>Revelation 1:7

[idou](../../strongs/g/g2400.md), he [erchomai](../../strongs/g/g2064.md) with [nephelē](../../strongs/g/g3507.md); and every [ophthalmos](../../strongs/g/g3788.md) shall [optanomai](../../strongs/g/g3700.md) him, and they also which [ekkenteō](../../strongs/g/g1574.md) him: and all [phylē](../../strongs/g/g5443.md) of [gē](../../strongs/g/g1093.md) shall [koptō](../../strongs/g/g2875.md) because of him. [nai](../../strongs/g/g3483.md), [amēn](../../strongs/g/g281.md).

<a name="revelation_1_8"></a>Revelation 1:8

I am [alpha](../../strongs/g/g1.md) and [ō](../../strongs/g/g5598.md), the [archē](../../strongs/g/g746.md) and the [telos](../../strongs/g/g5056.md), [legō](../../strongs/g/g3004.md) the [kyrios](../../strongs/g/g2962.md), which is, and which was, and which is to [erchomai](../../strongs/g/g2064.md), the [pantokratōr](../../strongs/g/g3841.md). [^1]

<a name="revelation_1_9"></a>Revelation 1:9

I [Iōannēs](../../strongs/g/g2491.md), who also am your [adelphos](../../strongs/g/g80.md), and [sygkoinōnos](../../strongs/g/g4791.md) in [thlipsis](../../strongs/g/g2347.md), and in the [basileia](../../strongs/g/g932.md) and [hypomonē](../../strongs/g/g5281.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), was in the [nēsos](../../strongs/g/g3520.md) that is [kaleō](../../strongs/g/g2564.md) [patmos](../../strongs/g/g3963.md), for the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md), and for the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="revelation_1_10"></a>Revelation 1:10

I was in the [pneuma](../../strongs/g/g4151.md) on the [kyriakos](../../strongs/g/g2960.md) [hēmera](../../strongs/g/g2250.md), and [akouō](../../strongs/g/g191.md) behind me a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), as of a [salpigx](../../strongs/g/g4536.md),

<a name="revelation_1_11"></a>Revelation 1:11

[legō](../../strongs/g/g3004.md), I am [alpha](../../strongs/g/g1.md) and [ō](../../strongs/g/g5598.md), the [prōtos](../../strongs/g/g4413.md) and the [eschatos](../../strongs/g/g2078.md): and, What thou [blepō](../../strongs/g/g991.md), [graphō](../../strongs/g/g1125.md) in a [biblion](../../strongs/g/g975.md), and [pempō](../../strongs/g/g3992.md) it unto the seven [ekklēsia](../../strongs/g/g1577.md) which are in [Asia](../../strongs/g/g773.md); unto [Ephesos](../../strongs/g/g2181.md), and unto [smyrna](../../strongs/g/g4667.md), and unto [pergamos](../../strongs/g/g4010.md), and unto [Thyateira](../../strongs/g/g2363.md), and unto [sardeis](../../strongs/g/g4554.md), and unto [philadelpheia](../../strongs/g/g5359.md), and unto [laodikeia](../../strongs/g/g2993.md). [^2]

<a name="revelation_1_12"></a>Revelation 1:12

And I [epistrephō](../../strongs/g/g1994.md) to [blepō](../../strongs/g/g991.md) the [phōnē](../../strongs/g/g5456.md) that [laleō](../../strongs/g/g2980.md) with me. And being [epistrephō](../../strongs/g/g1994.md), I [eidō](../../strongs/g/g1492.md) seven [chrysous](../../strongs/g/g5552.md) [lychnia](../../strongs/g/g3087.md);

<a name="revelation_1_13"></a>Revelation 1:13

And in the [mesos](../../strongs/g/g3319.md) of the seven [lychnia](../../strongs/g/g3087.md) one [homoios](../../strongs/g/g3664.md) unto the [huios](../../strongs/g/g5207.md) of [anthrōpos](../../strongs/g/g444.md), [endyō](../../strongs/g/g1746.md) [podērēs](../../strongs/g/g4158.md), and [perizōnnymi](../../strongs/g/g4024.md) about the [mastos](../../strongs/g/g3149.md) with a [chrysous](../../strongs/g/g5552.md) [zōnē](../../strongs/g/g2223.md).

<a name="revelation_1_14"></a>Revelation 1:14

His [kephalē](../../strongs/g/g2776.md) and [thrix](../../strongs/g/g2359.md) were [leukos](../../strongs/g/g3022.md) like [erion](../../strongs/g/g2053.md), as [leukos](../../strongs/g/g3022.md) as [chiōn](../../strongs/g/g5510.md); and his [ophthalmos](../../strongs/g/g3788.md) were as a [phlox](../../strongs/g/g5395.md) of [pyr](../../strongs/g/g4442.md);

<a name="revelation_1_15"></a>Revelation 1:15

And his [pous](../../strongs/g/g4228.md) [homoios](../../strongs/g/g3664.md) unto [chalkolibanon](../../strongs/g/g5474.md), as if they [pyroō](../../strongs/g/g4448.md) in a [kaminos](../../strongs/g/g2575.md); and his [phōnē](../../strongs/g/g5456.md) as the [phōnē](../../strongs/g/g5456.md) of [polys](../../strongs/g/g4183.md) [hydōr](../../strongs/g/g5204.md).

<a name="revelation_1_16"></a>Revelation 1:16

And he had in his [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md) seven [astēr](../../strongs/g/g792.md): and out of his [stoma](../../strongs/g/g4750.md) [ekporeuomai](../../strongs/g/g1607.md) a [oxys](../../strongs/g/g3691.md) [distomos](../../strongs/g/g1366.md) [rhomphaia](../../strongs/g/g4501.md): and his [opsis](../../strongs/g/g3799.md) was as the [hēlios](../../strongs/g/g2246.md) [phainō](../../strongs/g/g5316.md) in his [dynamis](../../strongs/g/g1411.md).

<a name="revelation_1_17"></a>Revelation 1:17

And when I [eidō](../../strongs/g/g1492.md) him, I [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md) as [nekros](../../strongs/g/g3498.md). And he [epitithēmi](../../strongs/g/g2007.md) his [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md) upon me, [legō](../../strongs/g/g3004.md) unto me, [phobeō](../../strongs/g/g5399.md) not; I am the [prōtos](../../strongs/g/g4413.md) and the [eschatos](../../strongs/g/g2078.md):

<a name="revelation_1_18"></a>Revelation 1:18

I am he that [zaō](../../strongs/g/g2198.md), and was [nekros](../../strongs/g/g3498.md); and, [idou](../../strongs/g/g2400.md), I am [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md), [amēn](../../strongs/g/g281.md); and have the [kleis](../../strongs/g/g2807.md) of [hadēs](../../strongs/g/g86.md) and of [thanatos](../../strongs/g/g2288.md).

<a name="revelation_1_19"></a>Revelation 1:19

[graphō](../../strongs/g/g1125.md) the things which thou hast [eidō](../../strongs/g/g1492.md), and the things which are, and the things which shall [ginomai](../../strongs/g/g1096.md) hereafter;

<a name="revelation_1_20"></a>Revelation 1:20

The [mystērion](../../strongs/g/g3466.md) of the seven [astēr](../../strongs/g/g792.md) which thou [eidō](../../strongs/g/g1492.md) in my [dexios](../../strongs/g/g1188.md), and the seven [chrysous](../../strongs/g/g5552.md) [lychnia](../../strongs/g/g3087.md). The seven [astēr](../../strongs/g/g792.md) are the [aggelos](../../strongs/g/g32.md) of the seven [ekklēsia](../../strongs/g/g1577.md): and the seven [lychnia](../../strongs/g/g3087.md) which thou [eidō](../../strongs/g/g1492.md) are the seven [ekklēsia](../../strongs/g/g1577.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 2](revelation_2.md)

---

[^1]: [Revelation 1:8 Commentary](../../commentary/revelation/revelation_1_commentary.md#revelation_1_8)

[^2]: [Revelation 1:11 Commentary](../../commentary/revelation/revelation_1_commentary.md#revelation_1_11)
