# [Revelation 2](https://www.blueletterbible.org/kjv/gen/2/1/s_2001)

<a name="revelation_2_1"></a>Revelation 2:1

Unto the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) of [ephesinos](../../strongs/g/g2179.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) he that [krateō](../../strongs/g/g2902.md) the seven [astēr](../../strongs/g/g792.md) in his [dexios](../../strongs/g/g1188.md), who [peripateō](../../strongs/g/g4043.md) in the [mesos](../../strongs/g/g3319.md) of the seven [chrysous](../../strongs/g/g5552.md) [lychnia](../../strongs/g/g3087.md);

<a name="revelation_2_2"></a>Revelation 2:2

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), and thy [kopos](../../strongs/g/g2873.md), and thy [hypomonē](../../strongs/g/g5281.md), and how thou canst not [bastazō](../../strongs/g/g941.md) them which are [kakos](../../strongs/g/g2556.md): and thou hast [peirazō](../../strongs/g/g3985.md) them which [phasko](../../strongs/g/g5335.md) they are [apostolos](../../strongs/g/g652.md), and are not, and hast [heuriskō](../../strongs/g/g2147.md) them [pseudēs](../../strongs/g/g5571.md):

<a name="revelation_2_3"></a>Revelation 2:3

And hast [bastazō](../../strongs/g/g941.md), and hast [hypomonē](../../strongs/g/g5281.md), and for my [onoma](../../strongs/g/g3686.md) sake hast [kopiaō](../../strongs/g/g2872.md), and hast not [kamnō](../../strongs/g/g2577.md).

<a name="revelation_2_4"></a>Revelation 2:4

Nevertheless I have against thee, because thou hast [aphiēmi](../../strongs/g/g863.md) thy [prōtos](../../strongs/g/g4413.md) [agapē](../../strongs/g/g26.md).

<a name="revelation_2_5"></a>Revelation 2:5

[mnēmoneuō](../../strongs/g/g3421.md) therefore from whence thou art [ekpiptō](../../strongs/g/g1601.md), and [metanoeō](../../strongs/g/g3340.md), and [poieō](../../strongs/g/g4160.md) the [prōtos](../../strongs/g/g4413.md) [ergon](../../strongs/g/g2041.md); or else I will [erchomai](../../strongs/g/g2064.md) unto thee [tachy](../../strongs/g/g5035.md), and will [kineō](../../strongs/g/g2795.md) thy [lychnia](../../strongs/g/g3087.md) out of his [topos](../../strongs/g/g5117.md), except thou [metanoeō](../../strongs/g/g3340.md).

<a name="revelation_2_6"></a>Revelation 2:6

But this thou hast, that thou [miseō](../../strongs/g/g3404.md) the [ergon](../../strongs/g/g2041.md) of the [nikolaitēs](../../strongs/g/g3531.md), which I also [miseō](../../strongs/g/g3404.md).

<a name="revelation_2_7"></a>Revelation 2:7

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md); To him that [nikaō](../../strongs/g/g3528.md) will I [didōmi](../../strongs/g/g1325.md) to [phago](../../strongs/g/g5315.md) of the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md), which is in the [mesos](../../strongs/g/g3319.md) of the [paradeisos](../../strongs/g/g3857.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_2_8"></a>Revelation 2:8

And unto the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) in [smyrnaios](../../strongs/g/g4668.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) the [prōtos](../../strongs/g/g4413.md) and the [eschatos](../../strongs/g/g2078.md), which was [nekros](../../strongs/g/g3498.md), and is [zaō](../../strongs/g/g2198.md);

<a name="revelation_2_9"></a>Revelation 2:9

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), and [thlipsis](../../strongs/g/g2347.md), and [ptōcheia](../../strongs/g/g4432.md), (but thou art [plousios](../../strongs/g/g4145.md)) and the [blasphēmia](../../strongs/g/g988.md) of them which [legō](../../strongs/g/g3004.md) they are [Ioudaios](../../strongs/g/g2453.md), and are not, but are the [synagōgē](../../strongs/g/g4864.md) of [Satanas](../../strongs/g/g4567.md).

<a name="revelation_2_10"></a>Revelation 2:10

[phobeō](../../strongs/g/g5399.md) [mēdeis](../../strongs/g/g3367.md) of those things which thou shalt [paschō](../../strongs/g/g3958.md): [idou](../../strongs/g/g2400.md), the [diabolos](../../strongs/g/g1228.md) shall [ballō](../../strongs/g/g906.md) some of you into [phylakē](../../strongs/g/g5438.md), that ye may be [peirazō](../../strongs/g/g3985.md); and ye shall have [thlipsis](../../strongs/g/g2347.md) ten [hēmera](../../strongs/g/g2250.md): be thou [pistos](../../strongs/g/g4103.md) unto [thanatos](../../strongs/g/g2288.md), and I will [didōmi](../../strongs/g/g1325.md) thee a [stephanos](../../strongs/g/g4735.md) of [zōē](../../strongs/g/g2222.md).

<a name="revelation_2_11"></a>Revelation 2:11

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md); He that [nikaō](../../strongs/g/g3528.md) shall not be [adikeō](../../strongs/g/g91.md) of the second [thanatos](../../strongs/g/g2288.md).

<a name="revelation_2_12"></a>Revelation 2:12

And to the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) in [pergamos](../../strongs/g/g4010.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) he which hath the [oxys](../../strongs/g/g3691.md) [rhomphaia](../../strongs/g/g4501.md) [distomos](../../strongs/g/g1366.md);

<a name="revelation_2_13"></a>Revelation 2:13

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), and where thou [katoikeō](../../strongs/g/g2730.md), where [Satanas](../../strongs/g/g4567.md) [thronos](../../strongs/g/g2362.md): and thou [krateō](../../strongs/g/g2902.md) my [onoma](../../strongs/g/g3686.md), and hast not [arneomai](../../strongs/g/g720.md) my [pistis](../../strongs/g/g4102.md), even in those [hēmera](../../strongs/g/g2250.md) wherein [antipas](../../strongs/g/g493.md) was my [pistos](../../strongs/g/g4103.md) [martys](../../strongs/g/g3144.md), who was [apokteinō](../../strongs/g/g615.md) among you, where [Satanas](../../strongs/g/g4567.md) dwelleth.

<a name="revelation_2_14"></a>Revelation 2:14

But I have an [oligos](../../strongs/g/g3641.md) against thee, because thou hast there them that [krateō](../../strongs/g/g2902.md) the [didachē](../../strongs/g/g1322.md) of [Balaam](../../strongs/g/g903.md), who [didaskō](../../strongs/g/g1321.md) [Balak](../../strongs/g/g904.md) to [ballō](../../strongs/g/g906.md) a [skandalon](../../strongs/g/g4625.md) before the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md), to [phago](../../strongs/g/g5315.md) [eidōlothytos](../../strongs/g/g1494.md), and [porneuō](../../strongs/g/g4203.md).

<a name="revelation_2_15"></a>Revelation 2:15

So hast thou also them that [krateō](../../strongs/g/g2902.md) the [didachē](../../strongs/g/g1322.md) of the [nikolaitēs](../../strongs/g/g3531.md), which thing I [miseō](../../strongs/g/g3404.md).

<a name="revelation_2_16"></a>Revelation 2:16

[metanoeō](../../strongs/g/g3340.md); or else I will [erchomai](../../strongs/g/g2064.md) unto thee [tachy](../../strongs/g/g5035.md), and will [polemeō](../../strongs/g/g4170.md) against them with the [rhomphaia](../../strongs/g/g4501.md) of my [stoma](../../strongs/g/g4750.md).

<a name="revelation_2_17"></a>Revelation 2:17

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md); To him that [nikaō](../../strongs/g/g3528.md) will I give to [phago](../../strongs/g/g5315.md) of the [kryptō](../../strongs/g/g2928.md) [manna](../../strongs/g/g3131.md), and will [didōmi](../../strongs/g/g1325.md) him a [leukos](../../strongs/g/g3022.md) [psēphos](../../strongs/g/g5586.md), and in the [psēphos](../../strongs/g/g5586.md) a [kainos](../../strongs/g/g2537.md) [onoma](../../strongs/g/g3686.md) [graphō](../../strongs/g/g1125.md), which [oudeis](../../strongs/g/g3762.md) [ginōskō](../../strongs/g/g1097.md) saving he that [lambanō](../../strongs/g/g2983.md) it.

<a name="revelation_2_18"></a>Revelation 2:18

And unto the [aggelos](../../strongs/g/g32.md) of the [ekklēsia](../../strongs/g/g1577.md) in [Thyateira](../../strongs/g/g2363.md) [graphō](../../strongs/g/g1125.md); These things [legō](../../strongs/g/g3004.md) the [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md), who hath his [ophthalmos](../../strongs/g/g3788.md) like unto a [phlox](../../strongs/g/g5395.md) of [pyr](../../strongs/g/g4442.md), and his [pous](../../strongs/g/g4228.md) [homoios](../../strongs/g/g3664.md) [chalkolibanon](../../strongs/g/g5474.md);

<a name="revelation_2_19"></a>Revelation 2:19

I [eidō](../../strongs/g/g1492.md) thy [ergon](../../strongs/g/g2041.md), and [agapē](../../strongs/g/g26.md), and [diakonia](../../strongs/g/g1248.md), and [pistis](../../strongs/g/g4102.md), and thy [hypomonē](../../strongs/g/g5281.md), and thy [ergon](../../strongs/g/g2041.md); and the [eschatos](../../strongs/g/g2078.md) more than the first.

<a name="revelation_2_20"></a>Revelation 2:20

Notwithstanding I have an [oligos](../../strongs/g/g3641.md) against thee, because thou [eaō](../../strongs/g/g1439.md) that [gynē](../../strongs/g/g1135.md) [Iezabel](../../strongs/g/g2403.md), which [legō](../../strongs/g/g3004.md) herself a [prophētis](../../strongs/g/g4398.md), to [didaskō](../../strongs/g/g1321.md) and to [planaō](../../strongs/g/g4105.md) my [doulos](../../strongs/g/g1401.md) to [porneuō](../../strongs/g/g4203.md), and to [phago](../../strongs/g/g5315.md) [eidōlothytos](../../strongs/g/g1494.md). [^1]

<a name="revelation_2_21"></a>Revelation 2:21

And I [didōmi](../../strongs/g/g1325.md) her [chronos](../../strongs/g/g5550.md) to [metanoeō](../../strongs/g/g3340.md) of her [porneia](../../strongs/g/g4202.md); and she [metanoeō](../../strongs/g/g3340.md) not.

<a name="revelation_2_22"></a>Revelation 2:22

[idou](../../strongs/g/g2400.md), I will [ballō](../../strongs/g/g906.md) her into a [klinē](../../strongs/g/g2825.md), and them that [moicheuō](../../strongs/g/g3431.md) with her into [megas](../../strongs/g/g3173.md) [thlipsis](../../strongs/g/g2347.md), except they [metanoeō](../../strongs/g/g3340.md) of their [ergon](../../strongs/g/g2041.md).

<a name="revelation_2_23"></a>Revelation 2:23

And I will [apokteinō](../../strongs/g/g615.md) her [teknon](../../strongs/g/g5043.md) with [thanatos](../../strongs/g/g2288.md); and all the [ekklēsia](../../strongs/g/g1577.md) shall [ginōskō](../../strongs/g/g1097.md) that I am he which [eraunaō](../../strongs/g/g2045.md) the [nephros](../../strongs/g/g3510.md) and [kardia](../../strongs/g/g2588.md): and I will [didōmi](../../strongs/g/g1325.md) unto every one of you according to your [ergon](../../strongs/g/g2041.md).

<a name="revelation_2_24"></a>Revelation 2:24

But unto you I [legō](../../strongs/g/g3004.md), and unto the [loipos](../../strongs/g/g3062.md) in [Thyateira](../../strongs/g/g2363.md), as many as have not this [didachē](../../strongs/g/g1322.md), and which have not [ginōskō](../../strongs/g/g1097.md) the [bathos](../../strongs/g/g899.md) of [Satanas](../../strongs/g/g4567.md), as they [legō](../../strongs/g/g3004.md); I will [ballō](../../strongs/g/g906.md) upon you none other [baros](../../strongs/g/g922.md).

<a name="revelation_2_25"></a>Revelation 2:25

But that which ye have [krateō](../../strongs/g/g2902.md) till I [hēkō](../../strongs/g/g2240.md).

<a name="revelation_2_26"></a>Revelation 2:26

And he that [nikaō](../../strongs/g/g3528.md), and [tēreō](../../strongs/g/g5083.md) my [ergon](../../strongs/g/g2041.md) unto the [telos](../../strongs/g/g5056.md), to him will I [didōmi](../../strongs/g/g1325.md) [exousia](../../strongs/g/g1849.md) over the [ethnos](../../strongs/g/g1484.md):

<a name="revelation_2_27"></a>Revelation 2:27

And he shall [poimainō](../../strongs/g/g4165.md) them with a [rhabdos](../../strongs/g/g4464.md) of [sidērous](../../strongs/g/g4603.md); as the [skeuos](../../strongs/g/g4632.md) [keramikos](../../strongs/g/g2764.md) shall they be [syntribō](../../strongs/g/g4937.md): even as I [lambanō](../../strongs/g/g2983.md) of my [patēr](../../strongs/g/g3962.md).

<a name="revelation_2_28"></a>Revelation 2:28

And I will [didōmi](../../strongs/g/g1325.md) him the [prōinos](../../strongs/g/g4407.md) [astēr](../../strongs/g/g792.md).

<a name="revelation_2_29"></a>Revelation 2:29

He that hath an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md) what the [pneuma](../../strongs/g/g4151.md) [legō](../../strongs/g/g3004.md) unto the [ekklēsia](../../strongs/g/g1577.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 1](revelation_1.md) - [Revelation 3](revelation_3.md)

---

[^1]: [Revelation 2:20 Commentary](../../commentary/revelation/revelation_2_commentary.md#revelation_2_20)
