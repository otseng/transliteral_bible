# [Revelation 21](https://www.blueletterbible.org/kjv/rev/21/1/s_1188001)

<a name="revelation_21_1"></a>Revelation 21:1

And I [eidō](../../strongs/g/g1492.md) a [kainos](../../strongs/g/g2537.md) [ouranos](../../strongs/g/g3772.md) and a [kainos](../../strongs/g/g2537.md) [gē](../../strongs/g/g1093.md): for the [prōtos](../../strongs/g/g4413.md) [ouranos](../../strongs/g/g3772.md) and the [prōtos](../../strongs/g/g4413.md) [gē](../../strongs/g/g1093.md) were [parerchomai](../../strongs/g/g3928.md); and there was no more [thalassa](../../strongs/g/g2281.md).

<a name="revelation_21_2"></a>Revelation 21:2

And I [Iōannēs](../../strongs/g/g2491.md) [eidō](../../strongs/g/g1492.md) the [hagios](../../strongs/g/g40.md) [polis](../../strongs/g/g4172.md), [kainos](../../strongs/g/g2537.md) [Ierousalēm](../../strongs/g/g2419.md), [katabainō](../../strongs/g/g2597.md) from [theos](../../strongs/g/g2316.md) out of [ouranos](../../strongs/g/g3772.md), [hetoimazō](../../strongs/g/g2090.md) as a [nymphē](../../strongs/g/g3565.md) [kosmeō](../../strongs/g/g2885.md) for her [anēr](../../strongs/g/g435.md).

<a name="revelation_21_3"></a>Revelation 21:3

And I [akouō](../../strongs/g/g191.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) out of [ouranos](../../strongs/g/g3772.md) [legō](../../strongs/g/g3004.md), [idou](../../strongs/g/g2400.md), the [skēnē](../../strongs/g/g4633.md) of [theos](../../strongs/g/g2316.md) is with [anthrōpos](../../strongs/g/g444.md), and he will [skēnoō](../../strongs/g/g4637.md) with them, and they shall be his [laos](../../strongs/g/g2992.md), and [theos](../../strongs/g/g2316.md) himself shall be with them, and be their [theos](../../strongs/g/g2316.md).

<a name="revelation_21_4"></a>Revelation 21:4

And [theos](../../strongs/g/g2316.md) shall [exaleiphō](../../strongs/g/g1813.md) all [dakry](../../strongs/g/g1144.md) from their [ophthalmos](../../strongs/g/g3788.md); and there shall be no more [thanatos](../../strongs/g/g2288.md), neither [penthos](../../strongs/g/g3997.md), nor [kraugē](../../strongs/g/g2906.md), neither shall there be any more [ponos](../../strongs/g/g4192.md): for the [prōtos](../../strongs/g/g4413.md) are [aperchomai](../../strongs/g/g565.md).

<a name="revelation_21_5"></a>Revelation 21:5

And he that [kathēmai](../../strongs/g/g2521.md) upon the [thronos](../../strongs/g/g2362.md) [eipon](../../strongs/g/g2036.md), [idou](../../strongs/g/g2400.md), I [poieō](../../strongs/g/g4160.md) all things [kainos](../../strongs/g/g2537.md). And he [legō](../../strongs/g/g3004.md) unto me, [graphō](../../strongs/g/g1125.md): for these [logos](../../strongs/g/g3056.md) are [alēthinos](../../strongs/g/g228.md) and [pistos](../../strongs/g/g4103.md).

<a name="revelation_21_6"></a>Revelation 21:6

And he [eipon](../../strongs/g/g2036.md) unto me, It is [ginomai](../../strongs/g/g1096.md). I am [alpha](../../strongs/g/g1.md) and [ō](../../strongs/g/g5598.md), the [archē](../../strongs/g/g746.md) and the [telos](../../strongs/g/g5056.md). I will [didōmi](../../strongs/g/g1325.md) unto him that is [dipsaō](../../strongs/g/g1372.md) of the [pēgē](../../strongs/g/g4077.md) of the [hydōr](../../strongs/g/g5204.md) of [zōē](../../strongs/g/g2222.md) [dōrean](../../strongs/g/g1432.md).

<a name="revelation_21_7"></a>Revelation 21:7

He that [nikaō](../../strongs/g/g3528.md) shall [klēronomeō](../../strongs/g/g2816.md) all things; and I will be his [theos](../../strongs/g/g2316.md), and he shall be my [huios](../../strongs/g/g5207.md).

<a name="revelation_21_8"></a>Revelation 21:8

But the [deilos](../../strongs/g/g1169.md), and [apistos](../../strongs/g/g571.md), and the [bdelyssō](../../strongs/g/g948.md), and [phoneus](../../strongs/g/g5406.md), and [pornos](../../strongs/g/g4205.md), and [pharmakeus](../../strongs/g/g5332.md), and [eidōlolatrēs](../../strongs/g/g1496.md), and all [pseudēs](../../strongs/g/g5571.md), shall have their [meros](../../strongs/g/g3313.md) in the [limnē](../../strongs/g/g3041.md) which [kaiō](../../strongs/g/g2545.md) with [pyr](../../strongs/g/g4442.md) and [theion](../../strongs/g/g2303.md): which is the second [thanatos](../../strongs/g/g2288.md).

<a name="revelation_21_9"></a>Revelation 21:9

And there [erchomai](../../strongs/g/g2064.md) unto me one of the seven [aggelos](../../strongs/g/g32.md) which had the seven [phialē](../../strongs/g/g5357.md) [gemō](../../strongs/g/g1073.md) of the seven [eschatos](../../strongs/g/g2078.md) [plēgē](../../strongs/g/g4127.md), and [laleō](../../strongs/g/g2980.md) with me, [legō](../../strongs/g/g3004.md), [deuro](../../strongs/g/g1204.md), I will [deiknyō](../../strongs/g/g1166.md) thee the [nymphē](../../strongs/g/g3565.md), the [arnion](../../strongs/g/g721.md) [gynē](../../strongs/g/g1135.md).

<a name="revelation_21_10"></a>Revelation 21:10

And he [apopherō](../../strongs/g/g667.md) me in the [pneuma](../../strongs/g/g4151.md) to a [megas](../../strongs/g/g3173.md) and [hypsēlos](../../strongs/g/g5308.md) [oros](../../strongs/g/g3735.md), and [deiknyō](../../strongs/g/g1166.md) me that [megas](../../strongs/g/g3173.md) [polis](../../strongs/g/g4172.md), the [hagios](../../strongs/g/g40.md) [Ierousalēm](../../strongs/g/g2419.md), [katabainō](../../strongs/g/g2597.md) out of [ouranos](../../strongs/g/g3772.md) from [theos](../../strongs/g/g2316.md),

<a name="revelation_21_11"></a>Revelation 21:11

Having the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md): and her [phōstēr](../../strongs/g/g5458.md) was [homoios](../../strongs/g/g3664.md) unto a [lithos](../../strongs/g/g3037.md) [timios](../../strongs/g/g5093.md), even like an [iaspis](../../strongs/g/g2393.md) [lithos](../../strongs/g/g3037.md), [krystallizō](../../strongs/g/g2929.md);

<a name="revelation_21_12"></a>Revelation 21:12

And had a [teichos](../../strongs/g/g5038.md) [megas](../../strongs/g/g3173.md) and [hypsēlos](../../strongs/g/g5308.md), and had twelve [pylōn](../../strongs/g/g4440.md), and at the [pylōn](../../strongs/g/g4440.md) twelve [aggelos](../../strongs/g/g32.md), and [onoma](../../strongs/g/g3686.md) [epigraphō](../../strongs/g/g1924.md), which are of the twelve [phylē](../../strongs/g/g5443.md) of the [huios](../../strongs/g/g5207.md) of [Israēl](../../strongs/g/g2474.md):

<a name="revelation_21_13"></a>Revelation 21:13

On the [anatolē](../../strongs/g/g395.md) three [pylōn](../../strongs/g/g4440.md); on the [borras](../../strongs/g/g1005.md) three [pylōn](../../strongs/g/g4440.md); on the [notos](../../strongs/g/g3558.md) three [pylōn](../../strongs/g/g4440.md); and on the [dysmē](../../strongs/g/g1424.md) three [pylōn](../../strongs/g/g4440.md).

<a name="revelation_21_14"></a>Revelation 21:14

And the [teichos](../../strongs/g/g5038.md) of the [polis](../../strongs/g/g4172.md) had twelve [themelios](../../strongs/g/g2310.md), and in them the [onoma](../../strongs/g/g3686.md) of the twelve [apostolos](../../strongs/g/g652.md) of the [arnion](../../strongs/g/g721.md).

<a name="revelation_21_15"></a>Revelation 21:15

And he that [laleō](../../strongs/g/g2980.md) with me had a [chrysous](../../strongs/g/g5552.md) [kalamos](../../strongs/g/g2563.md) to [metreō](../../strongs/g/g3354.md) the [polis](../../strongs/g/g4172.md), and the [pylōn](../../strongs/g/g4440.md) thereof, and the [teichos](../../strongs/g/g5038.md) thereof.

<a name="revelation_21_16"></a>Revelation 21:16

And the [polis](../../strongs/g/g4172.md) [keimai](../../strongs/g/g2749.md) [tetragōnos](../../strongs/g/g5068.md), and the [mēkos](../../strongs/g/g3372.md) is [tosoutos](../../strongs/g/g5118.md) as the [platos](../../strongs/g/g4114.md): and he [metreō](../../strongs/g/g3354.md) the [polis](../../strongs/g/g4172.md) with the [kalamos](../../strongs/g/g2563.md), twelve thousand [stadion](../../strongs/g/g4712.md). The [mēkos](../../strongs/g/g3372.md) and the [platos](../../strongs/g/g4114.md) and the [hypsos](../../strongs/g/g5311.md) of it are [isos](../../strongs/g/g2470.md).

<a name="revelation_21_17"></a>Revelation 21:17

And he [metreō](../../strongs/g/g3354.md) the [teichos](../../strongs/g/g5038.md) thereof, an hundred and forty and four [pēchys](../../strongs/g/g4083.md), the [metron](../../strongs/g/g3358.md) of an [anthrōpos](../../strongs/g/g444.md), that is, of the [aggelos](../../strongs/g/g32.md).

<a name="revelation_21_18"></a>Revelation 21:18

And the [endōmēsis](../../strongs/g/g1739.md) of the [teichos](../../strongs/g/g5038.md) of it was of [iaspis](../../strongs/g/g2393.md): and the [polis](../../strongs/g/g4172.md) was [katharos](../../strongs/g/g2513.md) [chrysion](../../strongs/g/g5553.md), [homoios](../../strongs/g/g3664.md) unto [katharos](../../strongs/g/g2513.md) [hyalos](../../strongs/g/g5194.md).

<a name="revelation_21_19"></a>Revelation 21:19

And the [themelios](../../strongs/g/g2310.md) of the [teichos](../../strongs/g/g5038.md) of the [polis](../../strongs/g/g4172.md) were [kosmeō](../../strongs/g/g2885.md) with all manner of [timios](../../strongs/g/g5093.md) [lithos](../../strongs/g/g3037.md). The first [themelios](../../strongs/g/g2310.md) was [iaspis](../../strongs/g/g2393.md); the second, [sapphiros](../../strongs/g/g4552.md); the third, a [chalkēdōn](../../strongs/g/g5472.md); the fourth, a [smaragdos](../../strongs/g/g4665.md);

<a name="revelation_21_20"></a>Revelation 21:20

The fifth, [sardonyx](../../strongs/g/g4557.md); the sixth, [sardion](../../strongs/g/g4556.md); the seventh, [chrysolithos](../../strongs/g/g5555.md); the eighth, [bēryllos](../../strongs/g/g969.md); the ninth, a [topazion](../../strongs/g/g5116.md); the tenth, a [chrysoprasos](../../strongs/g/g5556.md); the eleventh, a [hyakinthos](../../strongs/g/g5192.md); the twelfth, an [amethystos](../../strongs/g/g271.md).

<a name="revelation_21_21"></a>Revelation 21:21

And the twelve [pylōn](../../strongs/g/g4440.md) were twelve [margaritēs](../../strongs/g/g3135.md): every several [pylōn](../../strongs/g/g4440.md) was of one [margaritēs](../../strongs/g/g3135.md): and the [plateia](../../strongs/g/g4113.md) of the [polis](../../strongs/g/g4172.md) was [katharos](../../strongs/g/g2513.md) [chrysion](../../strongs/g/g5553.md), as it were [diaphanēs](../../strongs/g/g1307.md) [hyalos](../../strongs/g/g5194.md).

<a name="revelation_21_22"></a>Revelation 21:22

And I [eidō](../../strongs/g/g1492.md) no [naos](../../strongs/g/g3485.md) therein: for the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md) and the [arnion](../../strongs/g/g721.md) are the [naos](../../strongs/g/g3485.md) of it.

<a name="revelation_21_23"></a>Revelation 21:23

And the [polis](../../strongs/g/g4172.md) had no [chreia](../../strongs/g/g5532.md) of the [hēlios](../../strongs/g/g2246.md), neither of the [selēnē](../../strongs/g/g4582.md), to [phainō](../../strongs/g/g5316.md) in it: for the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md) did [phōtizō](../../strongs/g/g5461.md) it, and the [arnion](../../strongs/g/g721.md) is the [lychnos](../../strongs/g/g3088.md) thereof.

<a name="revelation_21_24"></a>Revelation 21:24

And the [ethnos](../../strongs/g/g1484.md) of them which are [sōzō](../../strongs/g/g4982.md) shall [peripateō](../../strongs/g/g4043.md) in the [phōs](../../strongs/g/g5457.md) of it: and the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md) do [pherō](../../strongs/g/g5342.md) their [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md) into it. [^1]

<a name="revelation_21_25"></a>Revelation 21:25

And the [pylōn](../../strongs/g/g4440.md) of it shall not be [kleiō](../../strongs/g/g2808.md) at all by [hēmera](../../strongs/g/g2250.md): for there shall be no [nyx](../../strongs/g/g3571.md) there.

<a name="revelation_21_26"></a>Revelation 21:26

And they shall [pherō](../../strongs/g/g5342.md) the [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md) of the [ethnos](../../strongs/g/g1484.md) into it.

<a name="revelation_21_27"></a>Revelation 21:27

And there shall in no wise [eiserchomai](../../strongs/g/g1525.md) into it any thing that [koinoō](../../strongs/g/g2840.md), neither [poieō](../../strongs/g/g4160.md) [bdelygma](../../strongs/g/g946.md), or [pseudos](../../strongs/g/g5579.md): but they which are [graphō](../../strongs/g/g1125.md) in the [arnion](../../strongs/g/g721.md) [biblion](../../strongs/g/g975.md) of [zōē](../../strongs/g/g2222.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 20](revelation_20.md) - [Revelation 22](revelation_22.md)

---

[^1]: [Revelation 21:24 Commentary](../../commentary/revelation/revelation_21_commentary.md#revelation_21_24)
