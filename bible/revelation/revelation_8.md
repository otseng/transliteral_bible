# [Revelation 8](https://www.blueletterbible.org/kjv/rev/8/1/s_1175001)

<a name="revelation_8_1"></a>Revelation 8:1

And when he had [anoigō](../../strongs/g/g455.md) the seventh [sphragis](../../strongs/g/g4973.md), there was [sigē](../../strongs/g/g4602.md) in [ouranos](../../strongs/g/g3772.md) about the space of [hēmiōrion](../../strongs/g/g2256.md).

<a name="revelation_8_2"></a>Revelation 8:2

And I [eidō](../../strongs/g/g1492.md) the seven [aggelos](../../strongs/g/g32.md) which [histēmi](../../strongs/g/g2476.md) before [theos](../../strongs/g/g2316.md); and to them were [didōmi](../../strongs/g/g1325.md) seven [salpigx](../../strongs/g/g4536.md).

<a name="revelation_8_3"></a>Revelation 8:3

And another [aggelos](../../strongs/g/g32.md) [erchomai](../../strongs/g/g2064.md) and [histēmi](../../strongs/g/g2476.md) at the [thysiastērion](../../strongs/g/g2379.md), having a [chrysous](../../strongs/g/g5552.md) [libanōtos](../../strongs/g/g3031.md); and there was [didōmi](../../strongs/g/g1325.md) unto him [polys](../../strongs/g/g4183.md) [thymiama](../../strongs/g/g2368.md), that he should [didōmi](../../strongs/g/g1325.md) with the [proseuchē](../../strongs/g/g4335.md) of all [hagios](../../strongs/g/g40.md) upon the [chrysous](../../strongs/g/g5552.md) [thysiastērion](../../strongs/g/g2379.md) which was before the [thronos](../../strongs/g/g2362.md).

<a name="revelation_8_4"></a>Revelation 8:4

And the [kapnos](../../strongs/g/g2586.md) of the [thymiama](../../strongs/g/g2368.md), with the [proseuchē](../../strongs/g/g4335.md) of the [hagios](../../strongs/g/g40.md), [anabainō](../../strongs/g/g305.md) before [theos](../../strongs/g/g2316.md) out of the [aggelos](../../strongs/g/g32.md) [cheir](../../strongs/g/g5495.md).

<a name="revelation_8_5"></a>Revelation 8:5

And the [aggelos](../../strongs/g/g32.md) [lambanō](../../strongs/g/g2983.md) the [libanōtos](../../strongs/g/g3031.md), and [gemizō](../../strongs/g/g1072.md) it with [pyr](../../strongs/g/g4442.md) of the [thysiastērion](../../strongs/g/g2379.md), and [ballō](../../strongs/g/g906.md) into [gē](../../strongs/g/g1093.md): and there were [phōnē](../../strongs/g/g5456.md), and [brontē](../../strongs/g/g1027.md), and [astrapē](../../strongs/g/g796.md), and a [seismos](../../strongs/g/g4578.md).

<a name="revelation_8_6"></a>Revelation 8:6

And the seven [aggelos](../../strongs/g/g32.md) which had the seven [salpigx](../../strongs/g/g4536.md) [hetoimazō](../../strongs/g/g2090.md) themselves to [salpizō](../../strongs/g/g4537.md).

<a name="revelation_8_7"></a>Revelation 8:7

The first [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and there [ginomai](../../strongs/g/g1096.md) [chalaza](../../strongs/g/g5464.md) and [pyr](../../strongs/g/g4442.md) [mignymi](../../strongs/g/g3396.md) with [haima](../../strongs/g/g129.md), and they were [ballō](../../strongs/g/g906.md) upon [gē](../../strongs/g/g1093.md): and the third part of [dendron](../../strongs/g/g1186.md) was [katakaiō](../../strongs/g/g2618.md), and all [chlōros](../../strongs/g/g5515.md) [chortos](../../strongs/g/g5528.md) was [katakaiō](../../strongs/g/g2618.md). [^1]

<a name="revelation_8_8"></a>Revelation 8:8

And the second [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and as it were a [megas](../../strongs/g/g3173.md) [oros](../../strongs/g/g3735.md) [kaiō](../../strongs/g/g2545.md) with [pyr](../../strongs/g/g4442.md) was [ballō](../../strongs/g/g906.md) into the [thalassa](../../strongs/g/g2281.md): and the third part of the [thalassa](../../strongs/g/g2281.md) became [haima](../../strongs/g/g129.md);

<a name="revelation_8_9"></a>Revelation 8:9

And the third part of the [ktisma](../../strongs/g/g2938.md) which were in the [thalassa](../../strongs/g/g2281.md), and had [psychē](../../strongs/g/g5590.md), [apothnēskō](../../strongs/g/g599.md); and the third part of the [ploion](../../strongs/g/g4143.md) [diaphtheirō](../../strongs/g/g1311.md).

<a name="revelation_8_10"></a>Revelation 8:10

And the third [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and there [piptō](../../strongs/g/g4098.md) a [megas](../../strongs/g/g3173.md) [astēr](../../strongs/g/g792.md) from [ouranos](../../strongs/g/g3772.md), [kaiō](../../strongs/g/g2545.md) as it were a [lampas](../../strongs/g/g2985.md), and it [piptō](../../strongs/g/g4098.md) upon the third part of the [potamos](../../strongs/g/g4215.md), and upon the [pēgē](../../strongs/g/g4077.md) of [hydōr](../../strongs/g/g5204.md);

<a name="revelation_8_11"></a>Revelation 8:11

And the [onoma](../../strongs/g/g3686.md) of the [astēr](../../strongs/g/g792.md) is [legō](../../strongs/g/g3004.md) [apsinthion](../../strongs/g/g894.md): and the third part of the [hydōr](../../strongs/g/g5204.md) became [apsinthion](../../strongs/g/g894.md); and [polys](../../strongs/g/g4183.md) [anthrōpos](../../strongs/g/g444.md) [apothnēskō](../../strongs/g/g599.md) of the [hydōr](../../strongs/g/g5204.md), because they were [pikrainō](../../strongs/g/g4087.md).

<a name="revelation_8_12"></a>Revelation 8:12

And the fourth [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and the third part of the [hēlios](../../strongs/g/g2246.md) was [plēssō](../../strongs/g/g4141.md), and the third part of the [selēnē](../../strongs/g/g4582.md), and the third part of the [astēr](../../strongs/g/g792.md); so as the third part of them was [skotizō](../../strongs/g/g4654.md), and the [hēmera](../../strongs/g/g2250.md) [phainō](../../strongs/g/g5316.md) not for a third part of it, and the [nyx](../../strongs/g/g3571.md) [homoiōs](../../strongs/g/g3668.md).

<a name="revelation_8_13"></a>Revelation 8:13

And I [eidō](../../strongs/g/g1492.md), and [akouō](../../strongs/g/g191.md) an [aggelos](../../strongs/g/g32.md) [petomai](../../strongs/g/g4072.md) through the [mesouranēma](../../strongs/g/g3321.md), [legō](../../strongs/g/g3004.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [ouai](../../strongs/g/g3759.md), [ouai](../../strongs/g/g3759.md), [ouai](../../strongs/g/g3759.md), to the [katoikeō](../../strongs/g/g2730.md) of [gē](../../strongs/g/g1093.md) by reason of the [loipos](../../strongs/g/g3062.md) [phōnē](../../strongs/g/g5456.md) of the [salpigx](../../strongs/g/g4536.md) of the three [aggelos](../../strongs/g/g32.md), which are yet to [salpizō](../../strongs/g/g4537.md)!

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 7](revelation_7.md) - [Revelation 9](revelation_9.md)

---

[^1]: [Revelation 8:7 Commentary](../../commentary/revelation/revelation_8_commentary.md#revelation_8_7)
