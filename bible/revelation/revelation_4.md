# [Revelation 4](https://www.blueletterbible.org/kjv/rev/4/1/s_1171001)

<a name="revelation_4_1"></a>Revelation 4:1

After this I [eidō](../../strongs/g/g1492.md), and, [idou](../../strongs/g/g2400.md), a [thyra](../../strongs/g/g2374.md) was [anoigō](../../strongs/g/g455.md) in [ouranos](../../strongs/g/g3772.md): and the [prōtos](../../strongs/g/g4413.md) [phōnē](../../strongs/g/g5456.md) which I [akouō](../../strongs/g/g191.md) was as it were of a [salpigx](../../strongs/g/g4536.md) [laleō](../../strongs/g/g2980.md) with me; which [legō](../../strongs/g/g3004.md), [anabainō](../../strongs/g/g305.md) hither, and I will [deiknyō](../../strongs/g/g1166.md) thee things which must be hereafter.

<a name="revelation_4_2"></a>Revelation 4:2

And [eutheōs](../../strongs/g/g2112.md) I was in the [pneuma](../../strongs/g/g4151.md): and, [idou](../../strongs/g/g2400.md), a [thronos](../../strongs/g/g2362.md) was [keimai](../../strongs/g/g2749.md) in [ouranos](../../strongs/g/g3772.md), and [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md). [^1]

<a name="revelation_4_3"></a>Revelation 4:3

And he that [kathēmai](../../strongs/g/g2521.md) was to [horasis](../../strongs/g/g3706.md) [homoios](../../strongs/g/g3664.md) an [iaspis](../../strongs/g/g2393.md) and a [sardinos](../../strongs/g/g4555.md) [lithos](../../strongs/g/g3037.md): and an [iris](../../strongs/g/g2463.md) [kyklothen](../../strongs/g/g2943.md) the [thronos](../../strongs/g/g2362.md), in [horasis](../../strongs/g/g3706.md) [homoios](../../strongs/g/g3664.md) unto a [smaragdinos](../../strongs/g/g4664.md).

<a name="revelation_4_4"></a>Revelation 4:4

And [kyklothen](../../strongs/g/g2943.md) the [thronos](../../strongs/g/g2362.md) four and twenty [thronos](../../strongs/g/g2362.md): and upon the [thronos](../../strongs/g/g2362.md) I [eidō](../../strongs/g/g1492.md) four and twenty [presbyteros](../../strongs/g/g4245.md) [kathēmai](../../strongs/g/g2521.md), [periballō](../../strongs/g/g4016.md) in [leukos](../../strongs/g/g3022.md) [himation](../../strongs/g/g2440.md); and they had on their [kephalē](../../strongs/g/g2776.md) [stephanos](../../strongs/g/g4735.md) of [chrysous](../../strongs/g/g5552.md).

<a name="revelation_4_5"></a>Revelation 4:5

And out of the [thronos](../../strongs/g/g2362.md) [ekporeuomai](../../strongs/g/g1607.md) [astrapē](../../strongs/g/g796.md) and [brontē](../../strongs/g/g1027.md) and [phōnē](../../strongs/g/g5456.md): and seven [lampas](../../strongs/g/g2985.md) of [pyr](../../strongs/g/g4442.md) [kaiō](../../strongs/g/g2545.md) before the [thronos](../../strongs/g/g2362.md), which are the seven [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_4_6"></a>Revelation 4:6

And before the [thronos](../../strongs/g/g2362.md) a [thalassa](../../strongs/g/g2281.md) of [hyalinos](../../strongs/g/g5193.md) [homoios](../../strongs/g/g3664.md) unto [krystallos](../../strongs/g/g2930.md): and in the midst of the [thronos](../../strongs/g/g2362.md), and [kyklō](../../strongs/g/g2945.md) the [thronos](../../strongs/g/g2362.md), four [zōon](../../strongs/g/g2226.md) [gemō](../../strongs/g/g1073.md) of [ophthalmos](../../strongs/g/g3788.md) [emprosthen](../../strongs/g/g1715.md) and [opisthen](../../strongs/g/g3693.md).

<a name="revelation_4_7"></a>Revelation 4:7

And the first [zōon](../../strongs/g/g2226.md) [homoios](../../strongs/g/g3664.md) a [leōn](../../strongs/g/g3023.md), and the second [zōon](../../strongs/g/g2226.md) [homoios](../../strongs/g/g3664.md) a [moschos](../../strongs/g/g3448.md), and the third [zōon](../../strongs/g/g2226.md) had a [prosōpon](../../strongs/g/g4383.md) as an [anthrōpos](../../strongs/g/g444.md), and the fourth [zōon](../../strongs/g/g2226.md) was [homoios](../../strongs/g/g3664.md) a [petomai](../../strongs/g/g4072.md) [aetos](../../strongs/g/g105.md).

<a name="revelation_4_8"></a>Revelation 4:8

And the four [zōon](../../strongs/g/g2226.md) had each of them six [pteryx](../../strongs/g/g4420.md) [kyklothen](../../strongs/g/g2943.md); and [gemō](../../strongs/g/g1073.md) of [ophthalmos](../../strongs/g/g3788.md) [esōthen](../../strongs/g/g2081.md): and they [anapausis](../../strongs/g/g372.md) not [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md), [legō](../../strongs/g/g3004.md), [hagios](../../strongs/g/g40.md), [hagios](../../strongs/g/g40.md), [hagios](../../strongs/g/g40.md), [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md), which was, and is, and is to [erchomai](../../strongs/g/g2064.md).

<a name="revelation_4_9"></a>Revelation 4:9

And when those [zōon](../../strongs/g/g2226.md) [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md) and [eucharistia](../../strongs/g/g2169.md) to him that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md), who [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md),

<a name="revelation_4_10"></a>Revelation 4:10

The four and twenty [presbyteros](../../strongs/g/g4245.md) [piptō](../../strongs/g/g4098.md) before him that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md), and [proskyneō](../../strongs/g/g4352.md) him that [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md), and [ballō](../../strongs/g/g906.md) their [stephanos](../../strongs/g/g4735.md) before the [thronos](../../strongs/g/g2362.md), [legō](../../strongs/g/g3004.md),

<a name="revelation_4_11"></a>Revelation 4:11

Thou art [axios](../../strongs/g/g514.md), [kyrios](../../strongs/g/g2962.md), to [lambanō](../../strongs/g/g2983.md) [doxa](../../strongs/g/g1391.md) and [timē](../../strongs/g/g5092.md) and [dynamis](../../strongs/g/g1411.md): for thou hast [ktizō](../../strongs/g/g2936.md) all things, and for thy [thelēma](../../strongs/g/g2307.md) they are and were [ktizō](../../strongs/g/g2936.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 3](revelation_3.md) - [Revelation 5](revelation_5.md)

---

[^1]: [Revelation 4:2 Commentary](../../commentary/revelation/revelation_4_commentary.md#revelation_4_2)
