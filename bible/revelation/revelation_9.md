# [Revelation 9](https://www.blueletterbible.org/kjv/rev/9/1/s_1176001)

<a name="revelation_9_1"></a>Revelation 9:1

And the fifth [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and I [eidō](../../strongs/g/g1492.md) an [astēr](../../strongs/g/g792.md) [piptō](../../strongs/g/g4098.md) from [ouranos](../../strongs/g/g3772.md) unto [gē](../../strongs/g/g1093.md): and to him was [didōmi](../../strongs/g/g1325.md) the [kleis](../../strongs/g/g2807.md) of the [abyssos](../../strongs/g/g12.md) [phrear](../../strongs/g/g5421.md).

<a name="revelation_9_2"></a>Revelation 9:2

And he [anoigō](../../strongs/g/g455.md) the [abyssos](../../strongs/g/g12.md) [phrear](../../strongs/g/g5421.md); and there [anabainō](../../strongs/g/g305.md) a [kapnos](../../strongs/g/g2586.md) out of the [phrear](../../strongs/g/g5421.md), as the [kapnos](../../strongs/g/g2586.md) of a [megas](../../strongs/g/g3173.md) [kaminos](../../strongs/g/g2575.md); and the [hēlios](../../strongs/g/g2246.md) and the [aēr](../../strongs/g/g109.md) were [skotizō](../../strongs/g/g4654.md) by reason of the [kapnos](../../strongs/g/g2586.md) of the [phrear](../../strongs/g/g5421.md).

<a name="revelation_9_3"></a>Revelation 9:3

And there [exerchomai](../../strongs/g/g1831.md) of the [kapnos](../../strongs/g/g2586.md) [akris](../../strongs/g/g200.md) upon [gē](../../strongs/g/g1093.md): and unto them was [didōmi](../../strongs/g/g1325.md) [exousia](../../strongs/g/g1849.md), as the [skorpios](../../strongs/g/g4651.md) of [gē](../../strongs/g/g1093.md) have [exousia](../../strongs/g/g1849.md).

<a name="revelation_9_4"></a>Revelation 9:4

And it was [rheō](../../strongs/g/g4483.md) them that they should not [adikeō](../../strongs/g/g91.md) the [chortos](../../strongs/g/g5528.md) of [gē](../../strongs/g/g1093.md), neither any [chlōros](../../strongs/g/g5515.md), neither any [dendron](../../strongs/g/g1186.md); but only those [anthrōpos](../../strongs/g/g444.md) which have not the [sphragis](../../strongs/g/g4973.md) of [theos](../../strongs/g/g2316.md) in their [metōpon](../../strongs/g/g3359.md).

<a name="revelation_9_5"></a>Revelation 9:5

And to them it was [didōmi](../../strongs/g/g1325.md) that they should not [apokteinō](../../strongs/g/g615.md) them, but that they should be [basanizō](../../strongs/g/g928.md) five [mēn](../../strongs/g/g3376.md): and their [basanismos](../../strongs/g/g929.md) was as the [basanismos](../../strongs/g/g929.md) of a [skorpios](../../strongs/g/g4651.md), when he [paiō](../../strongs/g/g3817.md) an [anthrōpos](../../strongs/g/g444.md).

<a name="revelation_9_6"></a>Revelation 9:6

And in those [hēmera](../../strongs/g/g2250.md) shall [anthrōpos](../../strongs/g/g444.md) [zēteō](../../strongs/g/g2212.md) [thanatos](../../strongs/g/g2288.md), and shall not [heuriskō](../../strongs/g/g2147.md) it; and shall [epithymeō](../../strongs/g/g1937.md) to [apothnēskō](../../strongs/g/g599.md), and [thanatos](../../strongs/g/g2288.md) shall [pheugō](../../strongs/g/g5343.md) from them.

<a name="revelation_9_7"></a>Revelation 9:7

And the [homoiōma](../../strongs/g/g3667.md) of the [akris](../../strongs/g/g200.md) [homoios](../../strongs/g/g3664.md) unto [hippos](../../strongs/g/g2462.md) [hetoimazō](../../strongs/g/g2090.md) unto [polemos](../../strongs/g/g4171.md); and on their [kephalē](../../strongs/g/g2776.md) were as it were [stephanos](../../strongs/g/g4735.md) [homoios](../../strongs/g/g3664.md) [chrysos](../../strongs/g/g5557.md), and their [prosōpon](../../strongs/g/g4383.md) were as the [prosōpon](../../strongs/g/g4383.md) of [anthrōpos](../../strongs/g/g444.md).

<a name="revelation_9_8"></a>Revelation 9:8

And they had [thrix](../../strongs/g/g2359.md) as the [thrix](../../strongs/g/g2359.md) of [gynē](../../strongs/g/g1135.md), and their [odous](../../strongs/g/g3599.md) were as of [leōn](../../strongs/g/g3023.md).

<a name="revelation_9_9"></a>Revelation 9:9

And they had [thōrax](../../strongs/g/g2382.md), as it were [thōrax](../../strongs/g/g2382.md) of [sidērous](../../strongs/g/g4603.md); and the [phōnē](../../strongs/g/g5456.md) of their [pteryx](../../strongs/g/g4420.md) was as the [phōnē](../../strongs/g/g5456.md) of [harma](../../strongs/g/g716.md) of [polys](../../strongs/g/g4183.md) [hippos](../../strongs/g/g2462.md) [trechō](../../strongs/g/g5143.md) to [polemos](../../strongs/g/g4171.md).

<a name="revelation_9_10"></a>Revelation 9:10

And they had [oura](../../strongs/g/g3769.md) [homoios](../../strongs/g/g3664.md) unto [skorpios](../../strongs/g/g4651.md), and there were [kentron](../../strongs/g/g2759.md) in their [oura](../../strongs/g/g3769.md): and their [exousia](../../strongs/g/g1849.md) was to [adikeō](../../strongs/g/g91.md) [anthrōpos](../../strongs/g/g444.md) five [mēn](../../strongs/g/g3376.md).

<a name="revelation_9_11"></a>Revelation 9:11

And they had a [basileus](../../strongs/g/g935.md) over them, the [aggelos](../../strongs/g/g32.md) of the [abyssos](../../strongs/g/g12.md), whose [onoma](../../strongs/g/g3686.md) in the [Hebraïsti](../../strongs/g/g1447.md) [abaddōn](../../strongs/g/g3.md), but in the [Hellēnikos](../../strongs/g/g1673.md) hath name [apollyōn](../../strongs/g/g623.md).

<a name="revelation_9_12"></a>Revelation 9:12

One [ouai](../../strongs/g/g3759.md) is [aperchomai](../../strongs/g/g565.md); [idou](../../strongs/g/g2400.md), there [erchomai](../../strongs/g/g2064.md) two [ouai](../../strongs/g/g3759.md) more hereafter.

<a name="revelation_9_13"></a>Revelation 9:13

And the sixth [aggelos](../../strongs/g/g32.md) [salpizō](../../strongs/g/g4537.md), and I [akouō](../../strongs/g/g191.md) a [phōnē](../../strongs/g/g5456.md) from the four [keras](../../strongs/g/g2768.md) of the [chrysous](../../strongs/g/g5552.md) [thysiastērion](../../strongs/g/g2379.md) which is before [theos](../../strongs/g/g2316.md),

<a name="revelation_9_14"></a>Revelation 9:14

[legō](../../strongs/g/g3004.md) to the sixth [aggelos](../../strongs/g/g32.md) which had the [salpigx](../../strongs/g/g4536.md), [lyō](../../strongs/g/g3089.md) the four [aggelos](../../strongs/g/g32.md) which are [deō](../../strongs/g/g1210.md) in the [megas](../../strongs/g/g3173.md) [potamos](../../strongs/g/g4215.md) [Euphratēs](../../strongs/g/g2166.md).

<a name="revelation_9_15"></a>Revelation 9:15

And the four [aggelos](../../strongs/g/g32.md) were [lyō](../../strongs/g/g3089.md), which were [hetoimazō](../../strongs/g/g2090.md) for an [hōra](../../strongs/g/g5610.md), and a [hēmera](../../strongs/g/g2250.md), and a [mēn](../../strongs/g/g3376.md), and an [eniautos](../../strongs/g/g1763.md), for to [apokteinō](../../strongs/g/g615.md) the third part of [anthrōpos](../../strongs/g/g444.md).

<a name="revelation_9_16"></a>Revelation 9:16

And the [arithmos](../../strongs/g/g706.md) of the [strateuma](../../strongs/g/g4753.md) of the [hippikos](../../strongs/g/g2461.md) were [dyo](../../strongs/g/g1417.md) [myrias](../../strongs/g/g3461.md) [myrias](../../strongs/g/g3461.md): and I [akouō](../../strongs/g/g191.md) the [arithmos](../../strongs/g/g706.md) of them.

<a name="revelation_9_17"></a>Revelation 9:17

And thus I [eidō](../../strongs/g/g1492.md) the [hippos](../../strongs/g/g2462.md) in the [horasis](../../strongs/g/g3706.md), and them that [kathēmai](../../strongs/g/g2521.md) on them, having [thōrax](../../strongs/g/g2382.md) of [pyrinos](../../strongs/g/g4447.md), and of [hyakinthinos](../../strongs/g/g5191.md), and [theiōdēs](../../strongs/g/g2306.md): and the [kephalē](../../strongs/g/g2776.md) of the [hippos](../../strongs/g/g2462.md) as the [kephalē](../../strongs/g/g2776.md) of [leōn](../../strongs/g/g3023.md); and out of their [stoma](../../strongs/g/g4750.md) [ekporeuomai](../../strongs/g/g1607.md) [pyr](../../strongs/g/g4442.md) and [kapnos](../../strongs/g/g2586.md) and [theion](../../strongs/g/g2303.md).

<a name="revelation_9_18"></a>Revelation 9:18

By these three was the third part of [anthrōpos](../../strongs/g/g444.md) [apokteinō](../../strongs/g/g615.md), by the [pyr](../../strongs/g/g4442.md), and by the [kapnos](../../strongs/g/g2586.md), and by the [theion](../../strongs/g/g2303.md), which [ekporeuomai](../../strongs/g/g1607.md) out of their [stoma](../../strongs/g/g4750.md).

<a name="revelation_9_19"></a>Revelation 9:19

For their [exousia](../../strongs/g/g1849.md) is in their [stoma](../../strongs/g/g4750.md), and in their [oura](../../strongs/g/g3769.md): for their [oura](../../strongs/g/g3769.md) [homoios](../../strongs/g/g3664.md) unto [ophis](../../strongs/g/g3789.md), and had [kephalē](../../strongs/g/g2776.md), and with them they [adikeō](../../strongs/g/g91.md).

<a name="revelation_9_20"></a>Revelation 9:20

And the [loipos](../../strongs/g/g3062.md) of the [anthrōpos](../../strongs/g/g444.md) which were not [apokteinō](../../strongs/g/g615.md) by these [plēgē](../../strongs/g/g4127.md) yet [metanoeō](../../strongs/g/g3340.md) not of the [ergon](../../strongs/g/g2041.md) of their [cheir](../../strongs/g/g5495.md), that they should not [proskyneō](../../strongs/g/g4352.md) [daimonion](../../strongs/g/g1140.md), and [eidōlon](../../strongs/g/g1497.md) of [chrysous](../../strongs/g/g5552.md), and [argyreos](../../strongs/g/g693.md), and [chalkous](../../strongs/g/g5470.md), and [lithinos](../../strongs/g/g3035.md), and of [xylinos](../../strongs/g/g3585.md): which neither can [blepō](../../strongs/g/g991.md), nor [akouō](../../strongs/g/g191.md), nor [peripateō](../../strongs/g/g4043.md):

<a name="revelation_9_21"></a>Revelation 9:21

Neither [metanoeō](../../strongs/g/g3340.md) they of their [phonos](../../strongs/g/g5408.md), nor of their [pharmakeia](../../strongs/g/g5331.md), nor of their [porneia](../../strongs/g/g4202.md), nor of their [klemma](../../strongs/g/g2809.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 8](revelation_8.md) - [Revelation 10](revelation_10.md)