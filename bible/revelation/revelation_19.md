# [Revelation 19](https://www.blueletterbible.org/kjv/rev/19/1/s_1186001)

<a name="revelation_19_1"></a>Revelation 19:1

And after these things I [akouō](../../strongs/g/g191.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) of [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md) in [ouranos](../../strongs/g/g3772.md), [legō](../../strongs/g/g3004.md), [hallēlouïa](../../strongs/g/g239.md); [sōtēria](../../strongs/g/g4991.md), and [doxa](../../strongs/g/g1391.md), and [timē](../../strongs/g/g5092.md), and [dynamis](../../strongs/g/g1411.md), unto the [kyrios](../../strongs/g/g2962.md) our [theos](../../strongs/g/g2316.md):

<a name="revelation_19_2"></a>Revelation 19:2

For [alēthinos](../../strongs/g/g228.md) and [dikaios](../../strongs/g/g1342.md) are his [krisis](../../strongs/g/g2920.md): for he hath [krinō](../../strongs/g/g2919.md) the [megas](../../strongs/g/g3173.md) [pornē](../../strongs/g/g4204.md), which did [phtheirō](../../strongs/g/g5351.md) [gē](../../strongs/g/g1093.md) with her [porneia](../../strongs/g/g4202.md), and hath [ekdikeō](../../strongs/g/g1556.md) the [haima](../../strongs/g/g129.md) of his [doulos](../../strongs/g/g1401.md) at her [cheir](../../strongs/g/g5495.md).

<a name="revelation_19_3"></a>Revelation 19:3

And again they [eipon](../../strongs/g/g2046.md), [hallēlouïa](../../strongs/g/g239.md) And her [kapnos](../../strongs/g/g2586.md) [anabainō](../../strongs/g/g305.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_19_4"></a>Revelation 19:4

And the four and twenty [presbyteros](../../strongs/g/g4245.md) and the four [zōon](../../strongs/g/g2226.md) [piptō](../../strongs/g/g4098.md) and [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md) that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md), [legō](../../strongs/g/g3004.md), [amēn](../../strongs/g/g281.md); [hallēlouïa](../../strongs/g/g239.md).

<a name="revelation_19_5"></a>Revelation 19:5

And a [phōnē](../../strongs/g/g5456.md) [exerchomai](../../strongs/g/g1831.md) out of the [thronos](../../strongs/g/g2362.md), [legō](../../strongs/g/g3004.md), [aineō](../../strongs/g/g134.md) our [theos](../../strongs/g/g2316.md), all ye his [doulos](../../strongs/g/g1401.md), and ye that [phobeō](../../strongs/g/g5399.md) him, both [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md).

<a name="revelation_19_6"></a>Revelation 19:6

And I [akouō](../../strongs/g/g191.md) as it were the [phōnē](../../strongs/g/g5456.md) of a [polys](../../strongs/g/g4183.md) [ochlos](../../strongs/g/g3793.md), and as the [phōnē](../../strongs/g/g5456.md) of many [hydōr](../../strongs/g/g5204.md), and as the [phōnē](../../strongs/g/g5456.md) of [ischyros](../../strongs/g/g2478.md) [brontē](../../strongs/g/g1027.md), [legō](../../strongs/g/g3004.md), [hallēlouïa](../../strongs/g/g239.md): for the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md) [basileuō](../../strongs/g/g936.md).

<a name="revelation_19_7"></a>Revelation 19:7

Let us be [chairō](../../strongs/g/g5463.md) and [agalliaō](../../strongs/g/g21.md), and [didōmi](../../strongs/g/g1325.md) [doxa](../../strongs/g/g1391.md) to him: for the [gamos](../../strongs/g/g1062.md) of the [arnion](../../strongs/g/g721.md) is [erchomai](../../strongs/g/g2064.md), and his [gynē](../../strongs/g/g1135.md) hath [hetoimazō](../../strongs/g/g2090.md) herself.

<a name="revelation_19_8"></a>Revelation 19:8

And to her was [didōmi](../../strongs/g/g1325.md) that she should be [periballō](../../strongs/g/g4016.md) in [byssinos](../../strongs/g/g1039.md), [katharos](../../strongs/g/g2513.md) and [lampros](../../strongs/g/g2986.md): for the [byssinos](../../strongs/g/g1039.md) is the [dikaiōma](../../strongs/g/g1345.md) of [hagios](../../strongs/g/g40.md).

<a name="revelation_19_9"></a>Revelation 19:9

And he [legō](../../strongs/g/g3004.md) unto me, [graphō](../../strongs/g/g1125.md), [makarios](../../strongs/g/g3107.md) are they which are [kaleō](../../strongs/g/g2564.md) unto the [gamos](../../strongs/g/g1062.md) [deipnon](../../strongs/g/g1173.md) of the [arnion](../../strongs/g/g721.md). And he [legō](../../strongs/g/g3004.md) unto me, These are the [alēthinos](../../strongs/g/g228.md) [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_19_10"></a>Revelation 19:10

And I [piptō](../../strongs/g/g4098.md) at his [pous](../../strongs/g/g4228.md) to [proskyneō](../../strongs/g/g4352.md) him. And he [legō](../../strongs/g/g3004.md) unto me, [horaō](../../strongs/g/g3708.md) thou do it not: I am thy [syndoulos](../../strongs/g/g4889.md), and of thy [adelphos](../../strongs/g/g80.md) that have the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md): [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md): for the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md) is the [pneuma](../../strongs/g/g4151.md) of [prophēteia](../../strongs/g/g4394.md).

<a name="revelation_19_11"></a>Revelation 19:11

And I [eidō](../../strongs/g/g1492.md) [ouranos](../../strongs/g/g3772.md) [anoigō](../../strongs/g/g455.md), and [idou](../../strongs/g/g2400.md) a [leukos](../../strongs/g/g3022.md) [hippos](../../strongs/g/g2462.md); and he that [kathēmai](../../strongs/g/g2521.md) upon him was [kaleō](../../strongs/g/g2564.md) [pistos](../../strongs/g/g4103.md) and [alēthinos](../../strongs/g/g228.md), and in [dikaiosynē](../../strongs/g/g1343.md) he doth [krinō](../../strongs/g/g2919.md) and make [polemeō](../../strongs/g/g4170.md).

<a name="revelation_19_12"></a>Revelation 19:12

His [ophthalmos](../../strongs/g/g3788.md) were as a [phlox](../../strongs/g/g5395.md) of [pyr](../../strongs/g/g4442.md), and on his [kephalē](../../strongs/g/g2776.md) were [polys](../../strongs/g/g4183.md) [diadēma](../../strongs/g/g1238.md); and he had an [onoma](../../strongs/g/g3686.md) [graphō](../../strongs/g/g1125.md), that [oudeis](../../strongs/g/g3762.md) [eidō](../../strongs/g/g1492.md), but he himself.

<a name="revelation_19_13"></a>Revelation 19:13

And he was [periballō](../../strongs/g/g4016.md) with a [himation](../../strongs/g/g2440.md) [baptō](../../strongs/g/g911.md) in [haima](../../strongs/g/g129.md): and his [onoma](../../strongs/g/g3686.md) is [kaleō](../../strongs/g/g2564.md) the [logos](../../strongs/g/g3056.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_19_14"></a>Revelation 19:14

And the [strateuma](../../strongs/g/g4753.md) which were in [ouranos](../../strongs/g/g3772.md) [akoloutheō](../../strongs/g/g190.md) him upon [leukos](../../strongs/g/g3022.md) [hippos](../../strongs/g/g2462.md), [endyō](../../strongs/g/g1746.md) in [byssinos](../../strongs/g/g1039.md), [leukos](../../strongs/g/g3022.md) and [katharos](../../strongs/g/g2513.md).

<a name="revelation_19_15"></a>Revelation 19:15

And out of his [stoma](../../strongs/g/g4750.md) [ekporeuomai](../../strongs/g/g1607.md) a [oxys](../../strongs/g/g3691.md) [rhomphaia](../../strongs/g/g4501.md), that with it he should [patassō](../../strongs/g/g3960.md) the [ethnos](../../strongs/g/g1484.md): and he shall [poimainō](../../strongs/g/g4165.md) them with a [rhabdos](../../strongs/g/g4464.md) of [sidērous](../../strongs/g/g4603.md): and he [pateō](../../strongs/g/g3961.md) the [lēnos](../../strongs/g/g3025.md) [oinos](../../strongs/g/g3631.md) of the [thymos](../../strongs/g/g2372.md) and [orgē](../../strongs/g/g3709.md) of [pantokratōr](../../strongs/g/g3841.md) [theos](../../strongs/g/g2316.md).

<a name="revelation_19_16"></a>Revelation 19:16

And he hath on his [himation](../../strongs/g/g2440.md) and on his [mēros](../../strongs/g/g3382.md) an [onoma](../../strongs/g/g3686.md) [graphō](../../strongs/g/g1125.md), [basileus](../../strongs/g/g935.md) of [basileus](../../strongs/g/g935.md), and [kyrios](../../strongs/g/g2962.md) OF [kyrios](../../strongs/g/g2962.md).

<a name="revelation_19_17"></a>Revelation 19:17

And I [eidō](../../strongs/g/g1492.md) an [aggelos](../../strongs/g/g32.md) [histēmi](../../strongs/g/g2476.md) in the [hēlios](../../strongs/g/g2246.md); and he [krazō](../../strongs/g/g2896.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [legō](../../strongs/g/g3004.md) to all the [orneon](../../strongs/g/g3732.md) that [petomai](../../strongs/g/g4072.md) in the [mesouranēma](../../strongs/g/g3321.md), [deute](../../strongs/g/g1205.md) and [synagō](../../strongs/g/g4863.md) unto the [deipnon](../../strongs/g/g1173.md) of the [megas](../../strongs/g/g3173.md) [theos](../../strongs/g/g2316.md);

<a name="revelation_19_18"></a>Revelation 19:18

That ye may [phago](../../strongs/g/g5315.md) the [sarx](../../strongs/g/g4561.md) of [basileus](../../strongs/g/g935.md), and the [sarx](../../strongs/g/g4561.md) of [chiliarchos](../../strongs/g/g5506.md), and the [sarx](../../strongs/g/g4561.md) of [ischyros](../../strongs/g/g2478.md), and the [sarx](../../strongs/g/g4561.md) of [hippos](../../strongs/g/g2462.md), and of them that [kathēmai](../../strongs/g/g2521.md) on them, and the [sarx](../../strongs/g/g4561.md) of all, both [eleutheros](../../strongs/g/g1658.md) and [doulos](../../strongs/g/g1401.md), both [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md).

<a name="revelation_19_19"></a>Revelation 19:19

And I [eidō](../../strongs/g/g1492.md) the [thērion](../../strongs/g/g2342.md), and the [basileus](../../strongs/g/g935.md) of [gē](../../strongs/g/g1093.md), and their [strateuma](../../strongs/g/g4753.md), [synagō](../../strongs/g/g4863.md) to [poieō](../../strongs/g/g4160.md) [polemos](../../strongs/g/g4171.md) against him that [kathēmai](../../strongs/g/g2521.md) on the [hippos](../../strongs/g/g2462.md), and against his [strateuma](../../strongs/g/g4753.md).

<a name="revelation_19_20"></a>Revelation 19:20

And the [thērion](../../strongs/g/g2342.md) was [piazō](../../strongs/g/g4084.md), and with him the [pseudoprophētēs](../../strongs/g/g5578.md) that [poieō](../../strongs/g/g4160.md) [sēmeion](../../strongs/g/g4592.md) before him, with which he [planaō](../../strongs/g/g4105.md) them that had [lambanō](../../strongs/g/g2983.md) the [charagma](../../strongs/g/g5480.md) of the [thērion](../../strongs/g/g2342.md), and them that [proskyneō](../../strongs/g/g4352.md) his [eikōn](../../strongs/g/g1504.md). These both were [ballō](../../strongs/g/g906.md) [zaō](../../strongs/g/g2198.md) into a [limnē](../../strongs/g/g3041.md) of [pyr](../../strongs/g/g4442.md) [kaiō](../../strongs/g/g2545.md) with [theion](../../strongs/g/g2303.md).

<a name="revelation_19_21"></a>Revelation 19:21

And the [loipos](../../strongs/g/g3062.md) were [apokteinō](../../strongs/g/g615.md) with the [rhomphaia](../../strongs/g/g4501.md) of him that [kathēmai](../../strongs/g/g2521.md) upon the [hippos](../../strongs/g/g2462.md), which [ekporeuomai](../../strongs/g/g1607.md) out of his [stoma](../../strongs/g/g4750.md): and all the [orneon](../../strongs/g/g3732.md) were [chortazō](../../strongs/g/g5526.md) with their [sarx](../../strongs/g/g4561.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 18](revelation_18.md) - [Revelation 20](revelation_20.md)