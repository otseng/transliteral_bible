# [Revelation 13](https://www.blueletterbible.org/kjv/rev/13/1/s_1180001)

<a name="revelation_13_1"></a>Revelation 13:1

And I [histēmi](../../strongs/g/g2476.md) upon the [ammos](../../strongs/g/g285.md) of the [thalassa](../../strongs/g/g2281.md), and [eidō](../../strongs/g/g1492.md) a [thērion](../../strongs/g/g2342.md) [anabainō](../../strongs/g/g305.md) out of the [thalassa](../../strongs/g/g2281.md), having seven [kephalē](../../strongs/g/g2776.md) and ten [keras](../../strongs/g/g2768.md), and upon his [keras](../../strongs/g/g2768.md) ten [diadēma](../../strongs/g/g1238.md), and upon his [kephalē](../../strongs/g/g2776.md) the [onoma](../../strongs/g/g3686.md) of [blasphēmia](../../strongs/g/g988.md).

<a name="revelation_13_2"></a>Revelation 13:2

And the [thērion](../../strongs/g/g2342.md) which I [eidō](../../strongs/g/g1492.md) was [homoios](../../strongs/g/g3664.md) unto a [pardalis](../../strongs/g/g3917.md), and his [pous](../../strongs/g/g4228.md) were as of an [arktos](../../strongs/g/g715.md), and his [stoma](../../strongs/g/g4750.md) as the [stoma](../../strongs/g/g4750.md) of a [leōn](../../strongs/g/g3023.md): and the [drakōn](../../strongs/g/g1404.md) [didōmi](../../strongs/g/g1325.md) him his [dynamis](../../strongs/g/g1411.md), and his [thronos](../../strongs/g/g2362.md), and [megas](../../strongs/g/g3173.md) [exousia](../../strongs/g/g1849.md).

<a name="revelation_13_3"></a>Revelation 13:3

And I [eidō](../../strongs/g/g1492.md) one of his [kephalē](../../strongs/g/g2776.md) as it were [sphazō](../../strongs/g/g4969.md) to [thanatos](../../strongs/g/g2288.md); and his [thanatos](../../strongs/g/g2288.md) [plēgē](../../strongs/g/g4127.md) was [therapeuō](../../strongs/g/g2323.md): and all [gē](../../strongs/g/g1093.md) [thaumazō](../../strongs/g/g2296.md) after the [thērion](../../strongs/g/g2342.md).

<a name="revelation_13_4"></a>Revelation 13:4

And they [proskyneō](../../strongs/g/g4352.md) the [drakōn](../../strongs/g/g1404.md) which [didōmi](../../strongs/g/g1325.md) [exousia](../../strongs/g/g1849.md) unto the [thērion](../../strongs/g/g2342.md): and they [proskyneō](../../strongs/g/g4352.md) the [thērion](../../strongs/g/g2342.md), [legō](../../strongs/g/g3004.md), Who is [homoios](../../strongs/g/g3664.md) unto the [thērion](../../strongs/g/g2342.md)? who is able to [polemeō](../../strongs/g/g4170.md) with him?

<a name="revelation_13_5"></a>Revelation 13:5

And there was [didōmi](../../strongs/g/g1325.md) unto him a [stoma](../../strongs/g/g4750.md) [laleō](../../strongs/g/g2980.md) [megas](../../strongs/g/g3173.md) and [blasphēmia](../../strongs/g/g988.md); and [exousia](../../strongs/g/g1849.md) was [didōmi](../../strongs/g/g1325.md) unto him to [poieō](../../strongs/g/g4160.md) forty and two [mēn](../../strongs/g/g3376.md).

<a name="revelation_13_6"></a>Revelation 13:6

And he [anoigō](../../strongs/g/g455.md) his [stoma](../../strongs/g/g4750.md) in [blasphēmia](../../strongs/g/g988.md) against [theos](../../strongs/g/g2316.md), to [blasphēmeō](../../strongs/g/g987.md) his [onoma](../../strongs/g/g3686.md), and his [skēnē](../../strongs/g/g4633.md), and them that [skēnoō](../../strongs/g/g4637.md) in [ouranos](../../strongs/g/g3772.md).

<a name="revelation_13_7"></a>Revelation 13:7

And it was [didōmi](../../strongs/g/g1325.md) unto him to [poieō](../../strongs/g/g4160.md) [polemos](../../strongs/g/g4171.md) with the [hagios](../../strongs/g/g40.md), and to [nikaō](../../strongs/g/g3528.md) them: and [exousia](../../strongs/g/g1849.md) was [didōmi](../../strongs/g/g1325.md) him over all [phylē](../../strongs/g/g5443.md), and [glōssa](../../strongs/g/g1100.md), and [ethnos](../../strongs/g/g1484.md).

<a name="revelation_13_8"></a>Revelation 13:8

And all that [katoikeō](../../strongs/g/g2730.md) upon [gē](../../strongs/g/g1093.md) shall [proskyneō](../../strongs/g/g4352.md) him, whose [onoma](../../strongs/g/g3686.md) are not [graphō](../../strongs/g/g1125.md) in the [biblos](../../strongs/g/g976.md) of [zōē](../../strongs/g/g2222.md) of the [arnion](../../strongs/g/g721.md) [sphazō](../../strongs/g/g4969.md) from the [katabolē](../../strongs/g/g2602.md) of the [kosmos](../../strongs/g/g2889.md).

<a name="revelation_13_9"></a>Revelation 13:9

[ei tis](../../strongs/g/g1536.md) have an [ous](../../strongs/g/g3775.md), let him [akouō](../../strongs/g/g191.md).

<a name="revelation_13_10"></a>Revelation 13:10

He that [synagō](../../strongs/g/g4863.md) into [aichmalōsia](../../strongs/g/g161.md) shall [hypagō](../../strongs/g/g5217.md) into [aichmalōsia](../../strongs/g/g161.md): he that [apokteinō](../../strongs/g/g615.md) with the [machaira](../../strongs/g/g3162.md) must be [apokteinō](../../strongs/g/g615.md) with the [machaira](../../strongs/g/g3162.md). Here is the [hypomonē](../../strongs/g/g5281.md) and the [pistis](../../strongs/g/g4102.md) of the [hagios](../../strongs/g/g40.md).

<a name="revelation_13_11"></a>Revelation 13:11

And I [eidō](../../strongs/g/g1492.md) another [thērion](../../strongs/g/g2342.md) [anabainō](../../strongs/g/g305.md) out of [gē](../../strongs/g/g1093.md); and he had two [keras](../../strongs/g/g2768.md) [homoios](../../strongs/g/g3664.md) an [arnion](../../strongs/g/g721.md), and he [laleō](../../strongs/g/g2980.md) as a [drakōn](../../strongs/g/g1404.md).

<a name="revelation_13_12"></a>Revelation 13:12

And he [poieō](../../strongs/g/g4160.md) all the [exousia](../../strongs/g/g1849.md) of the first [thērion](../../strongs/g/g2342.md) before him, and causeth [gē](../../strongs/g/g1093.md) and them which [katoikeō](../../strongs/g/g2730.md) therein to [proskyneō](../../strongs/g/g4352.md) the first [thērion](../../strongs/g/g2342.md), whose [thanatos](../../strongs/g/g2288.md) [plēgē](../../strongs/g/g4127.md) was [therapeuō](../../strongs/g/g2323.md).

<a name="revelation_13_13"></a>Revelation 13:13

And he [poieō](../../strongs/g/g4160.md) [megas](../../strongs/g/g3173.md) [sēmeion](../../strongs/g/g4592.md), so that he [poieō](../../strongs/g/g4160.md) [pyr](../../strongs/g/g4442.md) [katabainō](../../strongs/g/g2597.md) from [ouranos](../../strongs/g/g3772.md) on [gē](../../strongs/g/g1093.md) in the [enōpion](../../strongs/g/g1799.md) of [anthrōpos](../../strongs/g/g444.md),

<a name="revelation_13_14"></a>Revelation 13:14

And [planaō](../../strongs/g/g4105.md) them that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md) by those [sēmeion](../../strongs/g/g4592.md) which he had [didōmi](../../strongs/g/g1325.md) to [poieō](../../strongs/g/g4160.md) in the [enōpion](../../strongs/g/g1799.md) of the [thērion](../../strongs/g/g2342.md); [legō](../../strongs/g/g3004.md) to them that [katoikeō](../../strongs/g/g2730.md) on [gē](../../strongs/g/g1093.md), that they should make an [eikōn](../../strongs/g/g1504.md) to the [thērion](../../strongs/g/g2342.md), which had the [plēgē](../../strongs/g/g4127.md) by a [machaira](../../strongs/g/g3162.md), and did [zaō](../../strongs/g/g2198.md).

<a name="revelation_13_15"></a>Revelation 13:15

And he had [didōmi](../../strongs/g/g1325.md) to [didōmi](../../strongs/g/g1325.md) [pneuma](../../strongs/g/g4151.md) unto the [eikōn](../../strongs/g/g1504.md) of the [thērion](../../strongs/g/g2342.md), that the [eikōn](../../strongs/g/g1504.md) of the [thērion](../../strongs/g/g2342.md) should both [laleō](../../strongs/g/g2980.md), and [poieō](../../strongs/g/g4160.md) that as many as would not [proskyneō](../../strongs/g/g4352.md) the [eikōn](../../strongs/g/g1504.md) of the [thērion](../../strongs/g/g2342.md) should be [apokteinō](../../strongs/g/g615.md).

<a name="revelation_13_16"></a>Revelation 13:16

And he [poieō](../../strongs/g/g4160.md) all, both [mikros](../../strongs/g/g3398.md) and [megas](../../strongs/g/g3173.md), [plousios](../../strongs/g/g4145.md) and [ptōchos](../../strongs/g/g4434.md), [eleutheros](../../strongs/g/g1658.md) and [doulos](../../strongs/g/g1401.md), to [didōmi](../../strongs/g/g1325.md) a [charagma](../../strongs/g/g5480.md) in their [dexios](../../strongs/g/g1188.md) [cheir](../../strongs/g/g5495.md), or in their [metōpon](../../strongs/g/g3359.md):

<a name="revelation_13_17"></a>Revelation 13:17

And that no [tis](../../strongs/g/g5100.md) might [agorazō](../../strongs/g/g59.md) or [pōleō](../../strongs/g/g4453.md), save he that had the [charagma](../../strongs/g/g5480.md), or the [onoma](../../strongs/g/g3686.md) of the [thērion](../../strongs/g/g2342.md), or the [arithmos](../../strongs/g/g706.md) of his [onoma](../../strongs/g/g3686.md).

<a name="revelation_13_18"></a>Revelation 13:18

Here is [sophia](../../strongs/g/g4678.md). Let him that hath [nous](../../strongs/g/g3563.md) [psēphizō](../../strongs/g/g5585.md) the [arithmos](../../strongs/g/g706.md) of the [thērion](../../strongs/g/g2342.md): for it is the [arithmos](../../strongs/g/g706.md) of [anthrōpos](../../strongs/g/g444.md); and his [arithmos](../../strongs/g/g706.md) [chxϚ](../../strongs/g/g5516.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 12](revelation_12.md) - [Revelation 14](revelation_14.md)