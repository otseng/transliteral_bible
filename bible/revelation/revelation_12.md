# [Revelation 12](https://www.blueletterbible.org/kjv/rev/12/1/s_1179001)

<a name="revelation_12_1"></a>Revelation 12:1

And there [optanomai](../../strongs/g/g3700.md) a [megas](../../strongs/g/g3173.md) [sēmeion](../../strongs/g/g4592.md) in [ouranos](../../strongs/g/g3772.md); a [gynē](../../strongs/g/g1135.md) [periballō](../../strongs/g/g4016.md) with the [hēlios](../../strongs/g/g2246.md), and the [selēnē](../../strongs/g/g4582.md) [hypokatō](../../strongs/g/g5270.md) her [pous](../../strongs/g/g4228.md), and upon her [kephalē](../../strongs/g/g2776.md) a [stephanos](../../strongs/g/g4735.md) of twelve [astēr](../../strongs/g/g792.md):

<a name="revelation_12_2"></a>Revelation 12:2

And she being with [gastēr](../../strongs/g/g1064.md) [krazō](../../strongs/g/g2896.md), [ōdinō](../../strongs/g/g5605.md), and [basanizō](../../strongs/g/g928.md) to be [tiktō](../../strongs/g/g5088.md).

<a name="revelation_12_3"></a>Revelation 12:3

And there [optanomai](../../strongs/g/g3700.md) another [sēmeion](../../strongs/g/g4592.md) in [ouranos](../../strongs/g/g3772.md); and [idou](../../strongs/g/g2400.md) a [megas](../../strongs/g/g3173.md) [pyrros](../../strongs/g/g4450.md) [drakōn](../../strongs/g/g1404.md), having seven [kephalē](../../strongs/g/g2776.md) and ten [keras](../../strongs/g/g2768.md), and seven [diadēma](../../strongs/g/g1238.md) upon his [kephalē](../../strongs/g/g2776.md).

<a name="revelation_12_4"></a>Revelation 12:4

And his [oura](../../strongs/g/g3769.md) [syrō](../../strongs/g/g4951.md) the third part of the [astēr](../../strongs/g/g792.md) of [ouranos](../../strongs/g/g3772.md), and did [ballō](../../strongs/g/g906.md) them to [gē](../../strongs/g/g1093.md): and the [drakōn](../../strongs/g/g1404.md) [histēmi](../../strongs/g/g2476.md) before the [gynē](../../strongs/g/g1135.md) which was ready to be [tiktō](../../strongs/g/g5088.md), for to [katesthiō](../../strongs/g/g2719.md) her [teknon](../../strongs/g/g5043.md) as soon as it was [tiktō](../../strongs/g/g5088.md).

<a name="revelation_12_5"></a>Revelation 12:5

And she [tiktō](../../strongs/g/g5088.md) an [arrēn](../../strongs/g/g730.md) [huios](../../strongs/g/g5207.md), who was to [poimainō](../../strongs/g/g4165.md)e all [ethnos](../../strongs/g/g1484.md) with a [rhabdos](../../strongs/g/g4464.md) of [sidērous](../../strongs/g/g4603.md): and her [teknon](../../strongs/g/g5043.md) was [harpazō](../../strongs/g/g726.md) unto [theos](../../strongs/g/g2316.md), and to his [thronos](../../strongs/g/g2362.md).

<a name="revelation_12_6"></a>Revelation 12:6

And the [gynē](../../strongs/g/g1135.md) [pheugō](../../strongs/g/g5343.md) into the [erēmos](../../strongs/g/g2048.md), where she hath a [topos](../../strongs/g/g5117.md) [hetoimazō](../../strongs/g/g2090.md) of [theos](../../strongs/g/g2316.md), that they should [trephō](../../strongs/g/g5142.md) her there a thousand two hundred and threescore [hēmera](../../strongs/g/g2250.md).

<a name="revelation_12_7"></a>Revelation 12:7

And there was [polemos](../../strongs/g/g4171.md) in [ouranos](../../strongs/g/g3772.md): [Michaēl](../../strongs/g/g3413.md) and his [aggelos](../../strongs/g/g32.md) [polemeō](../../strongs/g/g4170.md) against the [drakōn](../../strongs/g/g1404.md); and the [drakōn](../../strongs/g/g1404.md) [polemeō](../../strongs/g/g4170.md) and his [aggelos](../../strongs/g/g32.md),

<a name="revelation_12_8"></a>Revelation 12:8

And [ischyō](../../strongs/g/g2480.md) not; neither was their [topos](../../strongs/g/g5117.md) [heuriskō](../../strongs/g/g2147.md) any more in [ouranos](../../strongs/g/g3772.md).

<a name="revelation_12_9"></a>Revelation 12:9

And the [megas](../../strongs/g/g3173.md) [drakōn](../../strongs/g/g1404.md) was [ballō](../../strongs/g/g906.md), that [archaios](../../strongs/g/g744.md) [ophis](../../strongs/g/g3789.md), [kaleō](../../strongs/g/g2564.md) the [diabolos](../../strongs/g/g1228.md), and [Satanas](../../strongs/g/g4567.md), which [planaō](../../strongs/g/g4105.md) the whole [oikoumenē](../../strongs/g/g3625.md): he was [ballō](../../strongs/g/g906.md) into [gē](../../strongs/g/g1093.md), and his [aggelos](../../strongs/g/g32.md) were [ballō](../../strongs/g/g906.md) with him.

<a name="revelation_12_10"></a>Revelation 12:10

And I [akouō](../../strongs/g/g191.md) a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md) [legō](../../strongs/g/g3004.md) in [ouranos](../../strongs/g/g3772.md), Now is [ginomai](../../strongs/g/g1096.md) [sōtēria](../../strongs/g/g4991.md), and [dynamis](../../strongs/g/g1411.md), and the [basileia](../../strongs/g/g932.md) of our [theos](../../strongs/g/g2316.md), and the [exousia](../../strongs/g/g1849.md) of his [Christos](../../strongs/g/g5547.md): for the [katēgoreō](../../strongs/g/g2723.md) of our [adelphos](../../strongs/g/g80.md) is [kataballō](../../strongs/g/g2598.md), which [katēgoros](../../strongs/g/g2725.md) them before our [theos](../../strongs/g/g2316.md) [hēmera](../../strongs/g/g2250.md) and [nyx](../../strongs/g/g3571.md).

<a name="revelation_12_11"></a>Revelation 12:11

And they [nikaō](../../strongs/g/g3528.md) him by the [haima](../../strongs/g/g129.md) of the [arnion](../../strongs/g/g721.md), and by the [logos](../../strongs/g/g3056.md) of their [martyria](../../strongs/g/g3141.md); and they [agapaō](../../strongs/g/g25.md) not their [psychē](../../strongs/g/g5590.md) unto the [thanatos](../../strongs/g/g2288.md).

<a name="revelation_12_12"></a>Revelation 12:12

Therefore [euphrainō](../../strongs/g/g2165.md), [ouranos](../../strongs/g/g3772.md), and ye that [skēnoō](../../strongs/g/g4637.md) in them. [ouai](../../strongs/g/g3759.md) to the [katoikeō](../../strongs/g/g2730.md) of [gē](../../strongs/g/g1093.md) and of the [thalassa](../../strongs/g/g2281.md)! for the [diabolos](../../strongs/g/g1228.md) is [katabainō](../../strongs/g/g2597.md) unto you, having [megas](../../strongs/g/g3173.md) [thymos](../../strongs/g/g2372.md), because he [eidō](../../strongs/g/g1492.md) that he hath but a [oligos](../../strongs/g/g3641.md) [kairos](../../strongs/g/g2540.md).

<a name="revelation_12_13"></a>Revelation 12:13

And when the [drakōn](../../strongs/g/g1404.md) [eidō](../../strongs/g/g1492.md) that he was [ballō](../../strongs/g/g906.md) unto [gē](../../strongs/g/g1093.md), he [diōkō](../../strongs/g/g1377.md) the [gynē](../../strongs/g/g1135.md) which [tiktō](../../strongs/g/g5088.md) the [arrēn](../../strongs/g/g730.md).

<a name="revelation_12_14"></a>Revelation 12:14

And to the [gynē](../../strongs/g/g1135.md) were [didōmi](../../strongs/g/g1325.md) two [pteryx](../../strongs/g/g4420.md) of a [megas](../../strongs/g/g3173.md) [aetos](../../strongs/g/g105.md), that she might [petomai](../../strongs/g/g4072.md) into the [erēmos](../../strongs/g/g2048.md), into her [topos](../../strongs/g/g5117.md), where she is [trephō](../../strongs/g/g5142.md) for a [kairos](../../strongs/g/g2540.md), and [kairos](../../strongs/g/g2540.md), and [ēmisys](../../strongs/g/g2255.md) a [kairos](../../strongs/g/g2540.md), from the [prosōpon](../../strongs/g/g4383.md) of the [ophis](../../strongs/g/g3789.md).

<a name="revelation_12_15"></a>Revelation 12:15

And the [ophis](../../strongs/g/g3789.md) [ballō](../../strongs/g/g906.md) of his [stoma](../../strongs/g/g4750.md) [hydōr](../../strongs/g/g5204.md) as a [potamos](../../strongs/g/g4215.md) after the [gynē](../../strongs/g/g1135.md), that he might [poieō](../../strongs/g/g4160.md)her to be [potamophorētos](../../strongs/g/g4216.md).

<a name="revelation_12_16"></a>Revelation 12:16

And [gē](../../strongs/g/g1093.md) [boētheō](../../strongs/g/g997.md) the [gynē](../../strongs/g/g1135.md), and [gē](../../strongs/g/g1093.md) [anoigō](../../strongs/g/g455.md) her [stoma](../../strongs/g/g4750.md), and [katapinō](../../strongs/g/g2666.md) the [potamos](../../strongs/g/g4215.md) which the [drakōn](../../strongs/g/g1404.md) [ballō](../../strongs/g/g906.md) out of his [stoma](../../strongs/g/g4750.md).

<a name="revelation_12_17"></a>Revelation 12:17

And the [drakōn](../../strongs/g/g1404.md) was [orgizō](../../strongs/g/g3710.md) with the [gynē](../../strongs/g/g1135.md), and [aperchomai](../../strongs/g/g565.md) to [poieō](../../strongs/g/g4160.md) [polemos](../../strongs/g/g4171.md) with the [loipos](../../strongs/g/g3062.md) of her [sperma](../../strongs/g/g4690.md), which [tēreō](../../strongs/g/g5083.md) the [entolē](../../strongs/g/g1785.md) of [theos](../../strongs/g/g2316.md), and have the [martyria](../../strongs/g/g3141.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 11](revelation_11.md) - [Revelation 13](revelation_13.md)