# [Revelation 22](https://www.blueletterbible.org/kjv/rev/22/1/s_1189001)

<a name="revelation_22_1"></a>Revelation 22:1

And he [deiknyō](../../strongs/g/g1166.md) me a [katharos](../../strongs/g/g2513.md) [potamos](../../strongs/g/g4215.md) of [hydōr](../../strongs/g/g5204.md) of [zōē](../../strongs/g/g2222.md), [lampros](../../strongs/g/g2986.md) as [krystallos](../../strongs/g/g2930.md), [ekporeuomai](../../strongs/g/g1607.md) out of the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md) and of the [arnion](../../strongs/g/g721.md).

<a name="revelation_22_2"></a>Revelation 22:2

In the [mesos](../../strongs/g/g3319.md) of the [plateia](../../strongs/g/g4113.md) of it, and on either side of the [potamos](../../strongs/g/g4215.md), was there the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md), which [poieō](../../strongs/g/g4160.md) twelve [karpos](../../strongs/g/g2590.md), and [apodidōmi](../../strongs/g/g591.md) her [karpos](../../strongs/g/g2590.md) every [mēn](../../strongs/g/g3376.md): and the [phyllon](../../strongs/g/g5444.md) of the [xylon](../../strongs/g/g3586.md) were for the [therapeia](../../strongs/g/g2322.md) of the [ethnos](../../strongs/g/g1484.md).

<a name="revelation_22_3"></a>Revelation 22:3

And there shall be no more [katathema](../../strongs/g/g2652.md): but the [thronos](../../strongs/g/g2362.md) of [theos](../../strongs/g/g2316.md) and of the [arnion](../../strongs/g/g721.md) shall be in it; and his [doulos](../../strongs/g/g1401.md) shall [latreuō](../../strongs/g/g3000.md) him:

<a name="revelation_22_4"></a>Revelation 22:4

And they shall [optanomai](../../strongs/g/g3700.md) his [prosōpon](../../strongs/g/g4383.md); and his [onoma](../../strongs/g/g3686.md) shall be in their [metōpon](../../strongs/g/g3359.md).

<a name="revelation_22_5"></a>Revelation 22:5

And there shall be no [nyx](../../strongs/g/g3571.md) there; and they [chreia](../../strongs/g/g5532.md) no [lychnos](../../strongs/g/g3088.md), neither [phōs](../../strongs/g/g5457.md) of the [hēlios](../../strongs/g/g2246.md); for the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [phōtizō](../../strongs/g/g5461.md) them: and they shall [basileuō](../../strongs/g/g936.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_22_6"></a>Revelation 22:6

And he [eipon](../../strongs/g/g2036.md) unto me, These [logos](../../strongs/g/g3056.md) are [pistos](../../strongs/g/g4103.md) and [alēthinos](../../strongs/g/g228.md): and the [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) of the [hagios](../../strongs/g/g40.md) [prophētēs](../../strongs/g/g4396.md) [apostellō](../../strongs/g/g649.md) his [aggelos](../../strongs/g/g32.md) to [deiknyō](../../strongs/g/g1166.md) unto his [doulos](../../strongs/g/g1401.md) the things which must [tachos](../../strongs/g/g5034.md) be [ginomai](../../strongs/g/g1096.md).

<a name="revelation_22_7"></a>Revelation 22:7

[idou](../../strongs/g/g2400.md), I [erchomai](../../strongs/g/g2064.md) [tachy](../../strongs/g/g5035.md): [makarios](../../strongs/g/g3107.md) is he that [tēreō](../../strongs/g/g5083.md) the [logos](../../strongs/g/g3056.md) of the [prophēteia](../../strongs/g/g4394.md) of this [biblion](../../strongs/g/g975.md).

<a name="revelation_22_8"></a>Revelation 22:8

And I [Iōannēs](../../strongs/g/g2491.md) [blepō](../../strongs/g/g991.md) these things, and [akouō](../../strongs/g/g191.md) them. And when I had [akouō](../../strongs/g/g191.md) and [blepō](../../strongs/g/g991.md), I [piptō](../../strongs/g/g4098.md) to [proskyneō](../../strongs/g/g4352.md) before the [pous](../../strongs/g/g4228.md) of the [aggelos](../../strongs/g/g32.md) which [deiknyō](../../strongs/g/g1166.md) me these things.

<a name="revelation_22_9"></a>Revelation 22:9

Then [legō](../../strongs/g/g3004.md) he unto me, [horaō](../../strongs/g/g3708.md) not: for I am thy [syndoulos](../../strongs/g/g4889.md), and of thy [adelphos](../../strongs/g/g80.md) the [prophētēs](../../strongs/g/g4396.md), and of them which [tēreō](../../strongs/g/g5083.md) the [logos](../../strongs/g/g3056.md) of this [biblion](../../strongs/g/g975.md): [proskyneō](../../strongs/g/g4352.md) [theos](../../strongs/g/g2316.md).

<a name="revelation_22_10"></a>Revelation 22:10

And he [legō](../../strongs/g/g3004.md) unto me, [sphragizō](../../strongs/g/g4972.md) not the [logos](../../strongs/g/g3056.md) of the [prophēteia](../../strongs/g/g4394.md) of this [biblion](../../strongs/g/g975.md): for the [kairos](../../strongs/g/g2540.md) is [eggys](../../strongs/g/g1451.md).

<a name="revelation_22_11"></a>Revelation 22:11

He that is [adikeō](../../strongs/g/g91.md), let him be [adikeō](../../strongs/g/g91.md) still: and he which is [rhypainō](../../strongs/g/g4510.md), let him be [rhypainō](../../strongs/g/g4510.md) still: and he that is [dikaios](../../strongs/g/g1342.md), let him be [dikaioō](../../strongs/g/g1344.md) still: and he that is [hagios](../../strongs/g/g40.md), let him be [hagiazō](../../strongs/g/g37.md) still.

<a name="revelation_22_12"></a>Revelation 22:12

And, [idou](../../strongs/g/g2400.md), I [erchomai](../../strongs/g/g2064.md) [tachy](../../strongs/g/g5035.md); and my [misthos](../../strongs/g/g3408.md) is with me, to [apodidōmi](../../strongs/g/g591.md) [hekastos](../../strongs/g/g1538.md) according as his [ergon](../../strongs/g/g2041.md) shall be.

<a name="revelation_22_13"></a>Revelation 22:13

I am [alpha](../../strongs/g/g1.md) and [ō](../../strongs/g/g5598.md), the [archē](../../strongs/g/g746.md) and the [telos](../../strongs/g/g5056.md), the [prōtos](../../strongs/g/g4413.md) and the [eschatos](../../strongs/g/g2078.md).

<a name="revelation_22_14"></a>Revelation 22:14

[makarios](../../strongs/g/g3107.md) are they that [poieō](../../strongs/g/g4160.md) his [entolē](../../strongs/g/g1785.md), that they may have [exousia](../../strongs/g/g1849.md) to the [xylon](../../strongs/g/g3586.md) of [zōē](../../strongs/g/g2222.md), and may [eiserchomai](../../strongs/g/g1525.md) in through the [pylōn](../../strongs/g/g4440.md) into the [polis](../../strongs/g/g4172.md). [^1]

<a name="revelation_22_15"></a>Revelation 22:15

For without are [kyōn](../../strongs/g/g2965.md), and [pharmakos](../../strongs/g/g5333.md), and [pornos](../../strongs/g/g4205.md), and [phoneus](../../strongs/g/g5406.md), and [eidōlolatrēs](../../strongs/g/g1496.md), and whosoever [phileō](../../strongs/g/g5368.md) and [poieō](../../strongs/g/g4160.md) a [pseudos](../../strongs/g/g5579.md).

<a name="revelation_22_16"></a>Revelation 22:16

I [Iēsous](../../strongs/g/g2424.md) have [pempō](../../strongs/g/g3992.md) mine [aggelos](../../strongs/g/g32.md) to [martyreō](../../strongs/g/g3140.md) unto you these things in the [ekklēsia](../../strongs/g/g1577.md). I am the [rhiza](../../strongs/g/g4491.md) and the [genos](../../strongs/g/g1085.md) of [Dabid](../../strongs/g/g1138.md), and the [lampros](../../strongs/g/g2986.md) and [orthrinos](../../strongs/g/g3720.md) [astēr](../../strongs/g/g792.md).

<a name="revelation_22_17"></a>Revelation 22:17

And the [pneuma](../../strongs/g/g4151.md) and the [nymphē](../../strongs/g/g3565.md) [legō](../../strongs/g/g3004.md), [erchomai](../../strongs/g/g2064.md). And let him that [akouō](../../strongs/g/g191.md) [eipon](../../strongs/g/g2036.md), [erchomai](../../strongs/g/g2064.md). And let him that is [dipsaō](../../strongs/g/g1372.md) [erchomai](../../strongs/g/g2064.md). And whosoever will, let him [lambanō](../../strongs/g/g2983.md) the [hydōr](../../strongs/g/g5204.md) of [zōē](../../strongs/g/g2222.md) [dōrean](../../strongs/g/g1432.md).

<a name="revelation_22_18"></a>Revelation 22:18

For I [symmartyreō](../../strongs/g/g4828.md) unto [pas](../../strongs/g/g3956.md) that [akouō](../../strongs/g/g191.md) the [logos](../../strongs/g/g3056.md) of the [prophēteia](../../strongs/g/g4394.md) of this [biblion](../../strongs/g/g975.md), If [tis](../../strongs/g/g5100.md) shall [epitithēmi](../../strongs/g/g2007.md) unto these things, [theos](../../strongs/g/g2316.md) shall [epitithēmi](../../strongs/g/g2007.md) unto him the [plēgē](../../strongs/g/g4127.md) that are [graphō](../../strongs/g/g1125.md) in this [biblion](../../strongs/g/g975.md):

<a name="revelation_22_19"></a>Revelation 22:19

And if [tis](../../strongs/g/g5100.md) shall [aphaireō](../../strongs/g/g851.md) from the [logos](../../strongs/g/g3056.md) of the [biblos](../../strongs/g/g976.md) of this [prophēteia](../../strongs/g/g4394.md), [theos](../../strongs/g/g2316.md) shall [aphaireō](../../strongs/g/g851.md) his [meros](../../strongs/g/g3313.md) out of the [biblos](../../strongs/g/g976.md) of [zōē](../../strongs/g/g2222.md), and out of the [hagios](../../strongs/g/g40.md) [polis](../../strongs/g/g4172.md), and from the things which are [graphō](../../strongs/g/g1125.md) in this [biblion](../../strongs/g/g975.md). [^2]

<a name="revelation_22_20"></a>Revelation 22:20

He which [martyreō](../../strongs/g/g3140.md) these things [legō](../../strongs/g/g3004.md), [nai](../../strongs/g/g3483.md) I [erchomai](../../strongs/g/g2064.md) [tachy](../../strongs/g/g5035.md). [amēn](../../strongs/g/g281.md). Even so, [erchomai](../../strongs/g/g2064.md), [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md).

<a name="revelation_22_21"></a>Revelation 22:21

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with you all. [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 21](revelation_21.md)

---

[^1]: [Revelation 22:14 Commentary](../../commentary/revelation/revelation_22_commentary.md#revelation_22_14)

[^2]: [Revelation 22:19 Commentary](../../commentary/revelation/revelation_22_commentary.md#revelation_22_19)
