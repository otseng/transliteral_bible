# [Revelation 15](https://www.blueletterbible.org/kjv/rev/15/1/s_1182001)

<a name="revelation_15_1"></a>Revelation 15:1

And I [eidō](../../strongs/g/g1492.md) another [sēmeion](../../strongs/g/g4592.md) in [ouranos](../../strongs/g/g3772.md), [megas](../../strongs/g/g3173.md) and [thaumastos](../../strongs/g/g2298.md), seven [aggelos](../../strongs/g/g32.md) having the seven [eschatos](../../strongs/g/g2078.md) [plēgē](../../strongs/g/g4127.md); for in them is [teleō](../../strongs/g/g5055.md) the [thymos](../../strongs/g/g2372.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_15_2"></a>Revelation 15:2

And I [eidō](../../strongs/g/g1492.md) as it were a [thalassa](../../strongs/g/g2281.md) of [hyalinos](../../strongs/g/g5193.md) [mignymi](../../strongs/g/g3396.md) with [pyr](../../strongs/g/g4442.md): and them that had [nikaō](../../strongs/g/g3528.md) over the [thērion](../../strongs/g/g2342.md), and over his [eikōn](../../strongs/g/g1504.md), and over his [charagma](../../strongs/g/g5480.md), over the [arithmos](../../strongs/g/g706.md) of his [onoma](../../strongs/g/g3686.md), [histēmi](../../strongs/g/g2476.md) on the [thalassa](../../strongs/g/g2281.md) of [hyalinos](../../strongs/g/g5193.md), having the [kithara](../../strongs/g/g2788.md) of [theos](../../strongs/g/g2316.md).

<a name="revelation_15_3"></a>Revelation 15:3

And they [adō](../../strongs/g/g103.md) the [ōdē](../../strongs/g/g5603.md) of [Mōÿsēs](../../strongs/g/g3475.md) the [doulos](../../strongs/g/g1401.md) of [theos](../../strongs/g/g2316.md), and the [ōdē](../../strongs/g/g5603.md) of the [arnion](../../strongs/g/g721.md), [legō](../../strongs/g/g3004.md), [megas](../../strongs/g/g3173.md) and [thaumastos](../../strongs/g/g2298.md) thy [ergon](../../strongs/g/g2041.md), [kyrios](../../strongs/g/g2962.md) [theos](../../strongs/g/g2316.md) [pantokratōr](../../strongs/g/g3841.md); [dikaios](../../strongs/g/g1342.md) and [alēthinos](../../strongs/g/g228.md) thy [hodos](../../strongs/g/g3598.md), thou [basileus](../../strongs/g/g935.md) of [hagios](../../strongs/g/g40.md).

<a name="revelation_15_4"></a>Revelation 15:4

Who shall not [phobeō](../../strongs/g/g5399.md) thee, O [kyrios](../../strongs/g/g2962.md), and [doxazō](../../strongs/g/g1392.md) thy [onoma](../../strongs/g/g3686.md)? for only [hosios](../../strongs/g/g3741.md): for all [ethnos](../../strongs/g/g1484.md) shall [hēkō](../../strongs/g/g2240.md) and [proskyneō](../../strongs/g/g4352.md) before thee; for thy [dikaiōma](../../strongs/g/g1345.md) are [phaneroō](../../strongs/g/g5319.md).

<a name="revelation_15_5"></a>Revelation 15:5

And after that I [eidō](../../strongs/g/g1492.md), and, [idou](../../strongs/g/g2400.md), the [naos](../../strongs/g/g3485.md) of the [skēnē](../../strongs/g/g4633.md) of the [martyrion](../../strongs/g/g3142.md) in [ouranos](../../strongs/g/g3772.md) was [anoigō](../../strongs/g/g455.md):

<a name="revelation_15_6"></a>Revelation 15:6

And the seven [aggelos](../../strongs/g/g32.md) [exerchomai](../../strongs/g/g1831.md) of the [naos](../../strongs/g/g3485.md), having the seven [plēgē](../../strongs/g/g4127.md), [endyō](../../strongs/g/g1746.md) in [katharos](../../strongs/g/g2513.md) and [lampros](../../strongs/g/g2986.md) [linon](../../strongs/g/g3043.md), and having their [stēthos](../../strongs/g/g4738.md) [perizōnnymi](../../strongs/g/g4024.md) with [chrysous](../../strongs/g/g5552.md) [zōnē](../../strongs/g/g2223.md).

<a name="revelation_15_7"></a>Revelation 15:7

And one of the four [zōon](../../strongs/g/g2226.md) [didōmi](../../strongs/g/g1325.md) unto the seven [aggelos](../../strongs/g/g32.md) seven [chrysous](../../strongs/g/g5552.md) [phialē](../../strongs/g/g5357.md) [gemō](../../strongs/g/g1073.md) of the [thymos](../../strongs/g/g2372.md) of [theos](../../strongs/g/g2316.md), who [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_15_8"></a>Revelation 15:8

And the [naos](../../strongs/g/g3485.md) was [gemizō](../../strongs/g/g1072.md) with [kapnos](../../strongs/g/g2586.md) from the [doxa](../../strongs/g/g1391.md) of [theos](../../strongs/g/g2316.md), and from his [dynamis](../../strongs/g/g1411.md); and [oudeis](../../strongs/g/g3762.md) was able to [eiserchomai](../../strongs/g/g1525.md) into the [naos](../../strongs/g/g3485.md), till the seven [plēgē](../../strongs/g/g4127.md) of the seven [aggelos](../../strongs/g/g32.md) were [teleō](../../strongs/g/g5055.md).

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 14](revelation_14.md) - [Revelation 16](revelation_16.md)