# [Revelation 5](https://www.blueletterbible.org/kjv/rev/5/1/s_1172001)

<a name="revelation_5_1"></a>Revelation 5:1

And I [eidō](../../strongs/g/g1492.md) in the [dexios](../../strongs/g/g1188.md) of him that [kathēmai](../../strongs/g/g2521.md) on the [thronos](../../strongs/g/g2362.md) a [biblion](../../strongs/g/g975.md) [graphō](../../strongs/g/g1125.md) [esōthen](../../strongs/g/g2081.md) and on the [opisthen](../../strongs/g/g3693.md), [katasphragizō](../../strongs/g/g2696.md) with seven [sphragis](../../strongs/g/g4973.md).

<a name="revelation_5_2"></a>Revelation 5:2

And I [eidō](../../strongs/g/g1492.md) an [ischyros](../../strongs/g/g2478.md) [aggelos](../../strongs/g/g32.md) [kēryssō](../../strongs/g/g2784.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), Who is [axios](../../strongs/g/g514.md) to [anoigō](../../strongs/g/g455.md) the [biblion](../../strongs/g/g975.md), and to [lyō](../../strongs/g/g3089.md) the [sphragis](../../strongs/g/g4973.md) thereof?

<a name="revelation_5_3"></a>Revelation 5:3

And [oudeis](../../strongs/g/g3762.md) in [ouranos](../../strongs/g/g3772.md), nor in [gē](../../strongs/g/g1093.md), neither [hypokatō](../../strongs/g/g5270.md) [gē](../../strongs/g/g1093.md), was able to [anoigō](../../strongs/g/g455.md) the [biblion](../../strongs/g/g975.md), neither to [blepō](../../strongs/g/g991.md) thereon.

<a name="revelation_5_4"></a>Revelation 5:4

And I [klaiō](../../strongs/g/g2799.md) [polys](../../strongs/g/g4183.md), because [oudeis](../../strongs/g/g3762.md) was [heuriskō](../../strongs/g/g2147.md) [axios](../../strongs/g/g514.md) to [anoigō](../../strongs/g/g455.md) and to [anaginōskō](../../strongs/g/g314.md) the [biblion](../../strongs/g/g975.md), neither to [blepō](../../strongs/g/g991.md) thereon.

<a name="revelation_5_5"></a>Revelation 5:5

And one of the [presbyteros](../../strongs/g/g4245.md) [legō](../../strongs/g/g3004.md) unto me, [klaiō](../../strongs/g/g2799.md) not: [idou](../../strongs/g/g2400.md), the [leōn](../../strongs/g/g3023.md) of the [phylē](../../strongs/g/g5443.md) of [Ioudas](../../strongs/g/g2455.md), the [rhiza](../../strongs/g/g4491.md) of [Dabid](../../strongs/g/g1138.md), hath [nikaō](../../strongs/g/g3528.md) to [anoigō](../../strongs/g/g455.md) the [biblion](../../strongs/g/g975.md), and to [lyō](../../strongs/g/g3089.md) the seven [sphragis](../../strongs/g/g4973.md) thereof.

<a name="revelation_5_6"></a>Revelation 5:6

And I [eidō](../../strongs/g/g1492.md), and, [idou](../../strongs/g/g2400.md), in the [mesos](../../strongs/g/g3319.md) of the [thronos](../../strongs/g/g2362.md) and of the four [zōon](../../strongs/g/g2226.md), and in the [mesos](../../strongs/g/g3319.md) of the [presbyteros](../../strongs/g/g4245.md), [histēmi](../../strongs/g/g2476.md) an [arnion](../../strongs/g/g721.md) as it had been [sphazō](../../strongs/g/g4969.md), having seven [keras](../../strongs/g/g2768.md) and seven [ophthalmos](../../strongs/g/g3788.md), which are the seven [pneuma](../../strongs/g/g4151.md) of [theos](../../strongs/g/g2316.md) [apostellō](../../strongs/g/g649.md) into all [gē](../../strongs/g/g1093.md).

<a name="revelation_5_7"></a>Revelation 5:7

And he [erchomai](../../strongs/g/g2064.md) and [lambanō](../../strongs/g/g2983.md) the [biblion](../../strongs/g/g975.md) out of the [dexios](../../strongs/g/g1188.md) of him that [kathēmai](../../strongs/g/g2521.md) upon the [thronos](../../strongs/g/g2362.md).

<a name="revelation_5_8"></a>Revelation 5:8

And when he had [lambanō](../../strongs/g/g2983.md) the [biblion](../../strongs/g/g975.md), the four [zōon](../../strongs/g/g2226.md) and four and twenty [presbyteros](../../strongs/g/g4245.md) [piptō](../../strongs/g/g4098.md) before the [arnion](../../strongs/g/g721.md), having every one of them [kithara](../../strongs/g/g2788.md), and [chrysous](../../strongs/g/g5552.md) [phialē](../../strongs/g/g5357.md) [gemō](../../strongs/g/g1073.md) of [thymiama](../../strongs/g/g2368.md), which are the [proseuchē](../../strongs/g/g4335.md) of [hagios](../../strongs/g/g40.md).

<a name="revelation_5_9"></a>Revelation 5:9

And they [adō](../../strongs/g/g103.md) a [kainos](../../strongs/g/g2537.md) [ōdē](../../strongs/g/g5603.md), [legō](../../strongs/g/g3004.md), Thou art [axios](../../strongs/g/g514.md) to [lambanō](../../strongs/g/g2983.md) the [biblion](../../strongs/g/g975.md), and to [anoigō](../../strongs/g/g455.md) the [sphragis](../../strongs/g/g4973.md) thereof: for thou wast [sphazō](../../strongs/g/g4969.md), and hast [agorazō](../../strongs/g/g59.md) us to [theos](../../strongs/g/g2316.md) by thy [haima](../../strongs/g/g129.md) out of every [phylē](../../strongs/g/g5443.md), and [glōssa](../../strongs/g/g1100.md), and [laos](../../strongs/g/g2992.md), and [ethnos](../../strongs/g/g1484.md);

<a name="revelation_5_10"></a>Revelation 5:10

And hast [poieō](../../strongs/g/g4160.md) us unto our [theos](../../strongs/g/g2316.md) [basileus](../../strongs/g/g935.md) and [hiereus](../../strongs/g/g2409.md): and we shall [basileuō](../../strongs/g/g936.md) on [gē](../../strongs/g/g1093.md).

<a name="revelation_5_11"></a>Revelation 5:11

And I [eidō](../../strongs/g/g1492.md), and I [akouō](../../strongs/g/g191.md) the [phōnē](../../strongs/g/g5456.md) of [polys](../../strongs/g/g4183.md) [aggelos](../../strongs/g/g32.md) [kyklothen](../../strongs/g/g2943.md) the [thronos](../../strongs/g/g2362.md) and the [zōon](../../strongs/g/g2226.md) and the [presbyteros](../../strongs/g/g4245.md): and the [arithmos](../../strongs/g/g706.md)r of them was [myrias](../../strongs/g/g3461.md) [myrias](../../strongs/g/g3461.md), and [chilias](../../strongs/g/g5505.md) [chilias](../../strongs/g/g5505.md);

<a name="revelation_5_12"></a>Revelation 5:12

[legō](../../strongs/g/g3004.md) with a [megas](../../strongs/g/g3173.md) [phōnē](../../strongs/g/g5456.md), [axios](../../strongs/g/g514.md) is the [arnion](../../strongs/g/g721.md) that was [sphazō](../../strongs/g/g4969.md) to [lambanō](../../strongs/g/g2983.md) [dynamis](../../strongs/g/g1411.md), and [ploutos](../../strongs/g/g4149.md), and [sophia](../../strongs/g/g4678.md), and [ischys](../../strongs/g/g2479.md), and [timē](../../strongs/g/g5092.md), and [doxa](../../strongs/g/g1391.md), and [eulogia](../../strongs/g/g2129.md).

<a name="revelation_5_13"></a>Revelation 5:13

And every [ktisma](../../strongs/g/g2938.md) which is in [ouranos](../../strongs/g/g3772.md), and on [gē](../../strongs/g/g1093.md), and [hypokatō](../../strongs/g/g5270.md) [gē](../../strongs/g/g1093.md), and such as are in the [thalassa](../../strongs/g/g2281.md), and all that are in them, [akouō](../../strongs/g/g191.md) I [legō](../../strongs/g/g3004.md), [eulogia](../../strongs/g/g2129.md), and [timē](../../strongs/g/g5092.md), and [doxa](../../strongs/g/g1391.md), and [kratos](../../strongs/g/g2904.md), be unto him that [kathēmai](../../strongs/g/g2521.md) upon the [thronos](../../strongs/g/g2362.md), and unto the [arnion](../../strongs/g/g721.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md).

<a name="revelation_5_14"></a>Revelation 5:14

And the four [zōon](../../strongs/g/g2226.md) [legō](../../strongs/g/g3004.md), [amēn](../../strongs/g/g281.md). And the four and twenty [presbyteros](../../strongs/g/g4245.md) [piptō](../../strongs/g/g4098.md) and [proskyneō](../../strongs/g/g4352.md) him that [zaō](../../strongs/g/g2198.md) [eis](../../strongs/g/g1519.md) [aiōn](../../strongs/g/g165.md) and [aiōn](../../strongs/g/g165.md). [^1]

---

[Transliteral Bible](../bible.md)

[Revelation](revelation.md)

[Revelation 4](revelation_4.md) - [Revelation 6](revelation_6.md)

---

[^1]: [Revelation 5:14 Commentary](../../commentary/revelation/revelation_5_commentary.md#revelation_5_14)
