# [Lamentations 2](https://www.blueletterbible.org/kjv/lamentations/2)

<a name="lamentations_2_1"></a>Lamentations 2:1

How hath the ['adonay](../../strongs/h/h136.md) [ʿûḇ](../../strongs/h/h5743.md) the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) with a [ʿûḇ](../../strongs/h/h5743.md) in his ['aph](../../strongs/h/h639.md), and [shalak](../../strongs/h/h7993.md) from [shamayim](../../strongs/h/h8064.md) unto the ['erets](../../strongs/h/h776.md) the [tip̄'ārâ](../../strongs/h/h8597.md) of [Yisra'el](../../strongs/h/h3478.md), and [zakar](../../strongs/h/h2142.md) not his [hăḏōm](../../strongs/h/h1916.md) [regel](../../strongs/h/h7272.md) in the [yowm](../../strongs/h/h3117.md) of his ['aph](../../strongs/h/h639.md)!

<a name="lamentations_2_2"></a>Lamentations 2:2

The ['adonay](../../strongs/h/h136.md) hath [bālaʿ](../../strongs/h/h1104.md) all the [nā'â](../../strongs/h/h4999.md) of [Ya'aqob](../../strongs/h/h3290.md), and hath not [ḥāmal](../../strongs/h/h2550.md): he hath [harac](../../strongs/h/h2040.md) in his ['ebrah](../../strongs/h/h5678.md) the [miḇṣār](../../strongs/h/h4013.md) of the [bath](../../strongs/h/h1323.md) of [Yehuwdah](../../strongs/h/h3063.md); he hath [naga'](../../strongs/h/h5060.md) them to the ['erets](../../strongs/h/h776.md): he hath [ḥālal](../../strongs/h/h2490.md) the [mamlāḵâ](../../strongs/h/h4467.md) and the [śar](../../strongs/h/h8269.md) thereof.

<a name="lamentations_2_3"></a>Lamentations 2:3

He hath [gāḏaʿ](../../strongs/h/h1438.md) in his [ḥŏrî](../../strongs/h/h2750.md) ['aph](../../strongs/h/h639.md) all the [qeren](../../strongs/h/h7161.md) of [Yisra'el](../../strongs/h/h3478.md): he hath [shuwb](../../strongs/h/h7725.md) ['āḥôr](../../strongs/h/h268.md) his [yamiyn](../../strongs/h/h3225.md) from [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md), and he [bāʿar](../../strongs/h/h1197.md) against [Ya'aqob](../../strongs/h/h3290.md) like a [lehāḇâ](../../strongs/h/h3852.md) ['esh](../../strongs/h/h784.md), which ['akal](../../strongs/h/h398.md) [cabiyb](../../strongs/h/h5439.md).

<a name="lamentations_2_4"></a>Lamentations 2:4

He hath [dāraḵ](../../strongs/h/h1869.md) his [qesheth](../../strongs/h/h7198.md) like an ['oyeb](../../strongs/h/h341.md): he [nāṣaḇ](../../strongs/h/h5324.md) with his [yamiyn](../../strongs/h/h3225.md) as a [tsar](../../strongs/h/h6862.md), and [harag](../../strongs/h/h2026.md) all that were [maḥmāḏ](../../strongs/h/h4261.md) to the ['ayin](../../strongs/h/h5869.md) in the ['ohel](../../strongs/h/h168.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): he poured [šāp̄aḵ](../../strongs/h/h8210.md) his [chemah](../../strongs/h/h2534.md) like ['esh](../../strongs/h/h784.md).

<a name="lamentations_2_5"></a>Lamentations 2:5

The ['adonay](../../strongs/h/h136.md) was as an ['oyeb](../../strongs/h/h341.md): he hath [bālaʿ](../../strongs/h/h1104.md) [Yisra'el](../../strongs/h/h3478.md), he hath [bālaʿ](../../strongs/h/h1104.md) all her ['armôn](../../strongs/h/h759.md): he hath [shachath](../../strongs/h/h7843.md) his [miḇṣār](../../strongs/h/h4013.md), and hath [rabah](../../strongs/h/h7235.md) in the [bath](../../strongs/h/h1323.md) of [Yehuwdah](../../strongs/h/h3063.md) [ta'ănîyâ](../../strongs/h/h8386.md) and ['ănîâ](../../strongs/h/h592.md).

<a name="lamentations_2_6"></a>Lamentations 2:6

And he hath [ḥāmas](../../strongs/h/h2554.md) his [śōḵ](../../strongs/h/h7900.md), as if it were of a [gan](../../strongs/h/h1588.md): he hath [shachath](../../strongs/h/h7843.md) his places of the [môʿēḏ](../../strongs/h/h4150.md): [Yĕhovah](../../strongs/h/h3068.md) hath caused the solemn [môʿēḏ](../../strongs/h/h4150.md) and [shabbath](../../strongs/h/h7676.md) to be [shakach](../../strongs/h/h7911.md) in [Tsiyown](../../strongs/h/h6726.md), and hath [na'ats](../../strongs/h/h5006.md) in the [zaʿam](../../strongs/h/h2195.md) of his ['aph](../../strongs/h/h639.md) the [melek](../../strongs/h/h4428.md) and the [kōhēn](../../strongs/h/h3548.md).

<a name="lamentations_2_7"></a>Lamentations 2:7

The ['adonay](../../strongs/h/h136.md) hath [zānaḥ](../../strongs/h/h2186.md) his [mizbeach](../../strongs/h/h4196.md), he hath [nā'ar](../../strongs/h/h5010.md) his [miqdash](../../strongs/h/h4720.md), he hath [cagar](../../strongs/h/h5462.md) into the [yad](../../strongs/h/h3027.md) of the ['oyeb](../../strongs/h/h341.md) the [ḥômâ](../../strongs/h/h2346.md) of her ['armôn](../../strongs/h/h759.md); they have [nathan](../../strongs/h/h5414.md) a [qowl](../../strongs/h/h6963.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), as in the [yowm](../../strongs/h/h3117.md) of a solemn [môʿēḏ](../../strongs/h/h4150.md).

<a name="lamentations_2_8"></a>Lamentations 2:8

[Yĕhovah](../../strongs/h/h3068.md) hath [chashab](../../strongs/h/h2803.md) to [shachath](../../strongs/h/h7843.md) the [ḥômâ](../../strongs/h/h2346.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md): he hath [natah](../../strongs/h/h5186.md) a [qāv](../../strongs/h/h6957.md), he hath not [shuwb](../../strongs/h/h7725.md) his [yad](../../strongs/h/h3027.md) from [bālaʿ](../../strongs/h/h1104.md): therefore he made the [cheyl](../../strongs/h/h2426.md) and the [ḥômâ](../../strongs/h/h2346.md) to ['āḇal](../../strongs/h/h56.md); they ['āmal](../../strongs/h/h535.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="lamentations_2_9"></a>Lamentations 2:9

Her [sha'ar](../../strongs/h/h8179.md) are [ṭāḇaʿ](../../strongs/h/h2883.md) into the ['erets](../../strongs/h/h776.md); he hath ['abad](../../strongs/h/h6.md) and [shabar](../../strongs/h/h7665.md) her [bᵊrîaḥ](../../strongs/h/h1280.md): her [melek](../../strongs/h/h4428.md) and her [śar](../../strongs/h/h8269.md) are among the [gowy](../../strongs/h/h1471.md): the [towrah](../../strongs/h/h8451.md) is no more; her [nāḇî'](../../strongs/h/h5030.md) also [māṣā'](../../strongs/h/h4672.md) no [ḥāzôn](../../strongs/h/h2377.md) from [Yĕhovah](../../strongs/h/h3068.md).

<a name="lamentations_2_10"></a>Lamentations 2:10

The [zāqēn](../../strongs/h/h2205.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) [yashab](../../strongs/h/h3427.md) upon the ['erets](../../strongs/h/h776.md), and [damam](../../strongs/h/h1826.md): they have [ʿālâ](../../strongs/h/h5927.md) ['aphar](../../strongs/h/h6083.md) upon their [ro'sh](../../strongs/h/h7218.md); they have [ḥāḡar](../../strongs/h/h2296.md) themselves with [śaq](../../strongs/h/h8242.md): the [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [yarad](../../strongs/h/h3381.md) their [ro'sh](../../strongs/h/h7218.md) to the ['erets](../../strongs/h/h776.md).

<a name="lamentations_2_11"></a>Lamentations 2:11

Mine ['ayin](../../strongs/h/h5869.md) do [kalah](../../strongs/h/h3615.md) with [dim'ah](../../strongs/h/h1832.md), my [me'ah](../../strongs/h/h4578.md) are [ḥāmar](../../strongs/h/h2560.md), my [kāḇēḏ](../../strongs/h/h3516.md) is [šāp̄aḵ](../../strongs/h/h8210.md) upon the ['erets](../../strongs/h/h776.md), for the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md); because the ['owlel](../../strongs/h/h5768.md) and the [yānaq](../../strongs/h/h3243.md) [ʿāṭap̄](../../strongs/h/h5848.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [qiryâ](../../strongs/h/h7151.md).

<a name="lamentations_2_12"></a>Lamentations 2:12

They ['āmar](../../strongs/h/h559.md) to their ['em](../../strongs/h/h517.md), Where is [dagan](../../strongs/h/h1715.md) and [yayin](../../strongs/h/h3196.md)? when they [ʿāṭap̄](../../strongs/h/h5848.md) as the [ḥālāl](../../strongs/h/h2491.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) of the [ʿîr](../../strongs/h/h5892.md), when their [nephesh](../../strongs/h/h5315.md) was poured [šāp̄aḵ](../../strongs/h/h8210.md) into their ['em](../../strongs/h/h517.md) [ḥêq](../../strongs/h/h2436.md).

<a name="lamentations_2_13"></a>Lamentations 2:13

What thing shall I take to [ʿûḏ](../../strongs/h/h5749.md) [ʿûḏ](../../strongs/h/h5749.md) for thee? what thing shall I [dāmâ](../../strongs/h/h1819.md) to thee, O [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md)? what shall I [šāvâ](../../strongs/h/h7737.md) to thee, that I may [nacham](../../strongs/h/h5162.md) thee, O [bᵊṯûlâ](../../strongs/h/h1330.md) [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md)? for thy [šeḇar](../../strongs/h/h7667.md) is [gadowl](../../strongs/h/h1419.md) like the [yam](../../strongs/h/h3220.md): who can [rapha'](../../strongs/h/h7495.md) thee?

<a name="lamentations_2_14"></a>Lamentations 2:14

Thy [nāḇî'](../../strongs/h/h5030.md) have [chazah](../../strongs/h/h2372.md) [shav'](../../strongs/h/h7723.md) and [tāp̄ēl](../../strongs/h/h8602.md) for thee: and they have not [gālâ](../../strongs/h/h1540.md) thine ['avon](../../strongs/h/h5771.md), to [shuwb](../../strongs/h/h7725.md) thy [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md); but have [chazah](../../strongs/h/h2372.md) for thee [shav'](../../strongs/h/h7723.md) [maśśᵊ'ēṯ](../../strongs/h/h4864.md) and causes of [madûaḥ](../../strongs/h/h4065.md).

<a name="lamentations_2_15"></a>Lamentations 2:15

All that ['abar](../../strongs/h/h5674.md) by [sāp̄aq](../../strongs/h/h5606.md) their [kaph](../../strongs/h/h3709.md) at [derek](../../strongs/h/h1870.md); they [šāraq](../../strongs/h/h8319.md) and [nûaʿ](../../strongs/h/h5128.md) their [ro'sh](../../strongs/h/h7218.md) at the [bath](../../strongs/h/h1323.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), saying, Is this the [ʿîr](../../strongs/h/h5892.md) that men ['āmar](../../strongs/h/h559.md) The [kālîl](../../strongs/h/h3632.md) of [yᵊp̄î](../../strongs/h/h3308.md), The [māśôś](../../strongs/h/h4885.md) of the ['erets](../../strongs/h/h776.md)?

<a name="lamentations_2_16"></a>Lamentations 2:16

All thine ['oyeb](../../strongs/h/h341.md) have [pāṣâ](../../strongs/h/h6475.md) their [peh](../../strongs/h/h6310.md) against thee: they [šāraq](../../strongs/h/h8319.md) and [ḥāraq](../../strongs/h/h2786.md) the [šēn](../../strongs/h/h8127.md): they ['āmar](../../strongs/h/h559.md), We have swallowed her [bālaʿ](../../strongs/h/h1104.md): certainly this is the [yowm](../../strongs/h/h3117.md) that we looked [qāvâ](../../strongs/h/h6960.md); we have [māṣā'](../../strongs/h/h4672.md), we have [ra'ah](../../strongs/h/h7200.md) it.

<a name="lamentations_2_17"></a>Lamentations 2:17

[Yĕhovah](../../strongs/h/h3068.md) hath ['asah](../../strongs/h/h6213.md) that which he had [zāmam](../../strongs/h/h2161.md); he hath [batsa'](../../strongs/h/h1214.md) his ['imrah](../../strongs/h/h565.md) that he had [tsavah](../../strongs/h/h6680.md) in the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md): he hath [harac](../../strongs/h/h2040.md), and hath not [ḥāmal](../../strongs/h/h2550.md): and he hath caused thine ['oyeb](../../strongs/h/h341.md) to [samach](../../strongs/h/h8055.md) over thee, he hath [ruwm](../../strongs/h/h7311.md) the [qeren](../../strongs/h/h7161.md) of thine [tsar](../../strongs/h/h6862.md).

<a name="lamentations_2_18"></a>Lamentations 2:18

Their [leb](../../strongs/h/h3820.md) [ṣāʿaq](../../strongs/h/h6817.md) unto the ['adonay](../../strongs/h/h136.md), O [ḥômâ](../../strongs/h/h2346.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), let [dim'ah](../../strongs/h/h1832.md) run [yarad](../../strongs/h/h3381.md) like a [nachal](../../strongs/h/h5158.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md): [nathan](../../strongs/h/h5414.md) thyself no [pûḡâ](../../strongs/h/h6314.md); let not the [bath](../../strongs/h/h1323.md) of thine ['ayin](../../strongs/h/h5869.md) [damam](../../strongs/h/h1826.md).

<a name="lamentations_2_19"></a>Lamentations 2:19

[quwm](../../strongs/h/h6965.md), cry [ranan](../../strongs/h/h7442.md) in the [layil](../../strongs/h/h3915.md): in the [ro'sh](../../strongs/h/h7218.md) of the ['ašmurâ](../../strongs/h/h821.md) [šāp̄aḵ](../../strongs/h/h8210.md) thine [leb](../../strongs/h/h3820.md) like [mayim](../../strongs/h/h4325.md) before the [paniym](../../strongs/h/h6440.md) of the ['adonay](../../strongs/h/h136.md): [nasa'](../../strongs/h/h5375.md) thy [kaph](../../strongs/h/h3709.md) toward him for the [nephesh](../../strongs/h/h5315.md) of thy ['owlel](../../strongs/h/h5768.md), that [ʿāṭap̄](../../strongs/h/h5848.md) for [rāʿāḇ](../../strongs/h/h7458.md) in the [ro'sh](../../strongs/h/h7218.md) of every [ḥûṣ](../../strongs/h/h2351.md).

<a name="lamentations_2_20"></a>Lamentations 2:20

[ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md), and [nabat](../../strongs/h/h5027.md) to whom thou hast [ʿālal](../../strongs/h/h5953.md) this. Shall the ['ishshah](../../strongs/h/h802.md) ['akal](../../strongs/h/h398.md) their [pĕriy](../../strongs/h/h6529.md), and ['owlel](../../strongs/h/h5768.md) of a [ṭipuḥ](../../strongs/h/h2949.md)? shall the [kōhēn](../../strongs/h/h3548.md) and the [nāḇî'](../../strongs/h/h5030.md) be [harag](../../strongs/h/h2026.md) in the [miqdash](../../strongs/h/h4720.md) of the ['adonay](../../strongs/h/h136.md)?

<a name="lamentations_2_21"></a>Lamentations 2:21

The [naʿar](../../strongs/h/h5288.md) and the [zāqēn](../../strongs/h/h2205.md) [shakab](../../strongs/h/h7901.md) on the ['erets](../../strongs/h/h776.md) in the [ḥûṣ](../../strongs/h/h2351.md): my [bᵊṯûlâ](../../strongs/h/h1330.md) and my [bāḥûr](../../strongs/h/h970.md) are [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md); thou hast [harag](../../strongs/h/h2026.md) them in the [yowm](../../strongs/h/h3117.md) of thine ['aph](../../strongs/h/h639.md); thou hast [ṭāḇaḥ](../../strongs/h/h2873.md), and not [ḥāmal](../../strongs/h/h2550.md).

<a name="lamentations_2_22"></a>Lamentations 2:22

Thou hast [qara'](../../strongs/h/h7121.md) as in a [môʿēḏ](../../strongs/h/h4150.md) [yowm](../../strongs/h/h3117.md) my [māḡôr](../../strongs/h/h4032.md) [cabiyb](../../strongs/h/h5439.md), so that in the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) ['aph](../../strongs/h/h639.md) none [pālîṭ](../../strongs/h/h6412.md) nor [śārîḏ](../../strongs/h/h8300.md): those that I have [ṭāp̄aḥ](../../strongs/h/h2946.md) and [rabah](../../strongs/h/h7235.md) hath mine ['oyeb](../../strongs/h/h341.md) [kalah](../../strongs/h/h3615.md).

---

[Transliteral Bible](../bible.md)

[Lamentations](lamentations.md)

[Lamentations 1](lamentations_1.md) - [Lamentations 3](lamentations_3.md)