# [Lamentations 3](https://www.blueletterbible.org/kjv/lamentations/3)

<a name="lamentations_3_1"></a>Lamentations 3:1

I am the [geḇer](../../strongs/h/h1397.md) that hath [ra'ah](../../strongs/h/h7200.md) ['oniy](../../strongs/h/h6040.md) by the [shebet](../../strongs/h/h7626.md) of his ['ebrah](../../strongs/h/h5678.md).

<a name="lamentations_3_2"></a>Lamentations 3:2

He hath [nāhaḡ](../../strongs/h/h5090.md) me, and [yālaḵ](../../strongs/h/h3212.md) me into [choshek](../../strongs/h/h2822.md), but not into ['owr](../../strongs/h/h216.md).

<a name="lamentations_3_3"></a>Lamentations 3:3

Surely against me is he [shuwb](../../strongs/h/h7725.md); he [hāp̄aḵ](../../strongs/h/h2015.md) his [yad](../../strongs/h/h3027.md) against me all the [yowm](../../strongs/h/h3117.md).

<a name="lamentations_3_4"></a>Lamentations 3:4

My [basar](../../strongs/h/h1320.md) and my ['owr](../../strongs/h/h5785.md) hath he made [bālâ](../../strongs/h/h1086.md); he hath [shabar](../../strongs/h/h7665.md) my ['etsem](../../strongs/h/h6106.md).

<a name="lamentations_3_5"></a>Lamentations 3:5

He hath [bānâ](../../strongs/h/h1129.md) me, and [naqaph](../../strongs/h/h5362.md) me with [rō'š](../../strongs/h/h7219.md) and [tᵊlā'â](../../strongs/h/h8513.md).

<a name="lamentations_3_6"></a>Lamentations 3:6

He hath [yashab](../../strongs/h/h3427.md) me in [maḥšāḵ](../../strongs/h/h4285.md), as they that be [muwth](../../strongs/h/h4191.md) of ['owlam](../../strongs/h/h5769.md).

<a name="lamentations_3_7"></a>Lamentations 3:7

He hath [gāḏar](../../strongs/h/h1443.md) me about, that I cannot [yāṣā'](../../strongs/h/h3318.md): he hath made my [nᵊḥšeṯ](../../strongs/h/h5178.md) [kabad](../../strongs/h/h3513.md).

<a name="lamentations_3_8"></a>Lamentations 3:8

Also when I [zāʿaq](../../strongs/h/h2199.md) and [šāvaʿ](../../strongs/h/h7768.md), he [sāṯam](../../strongs/h/h5640.md) my [tĕphillah](../../strongs/h/h8605.md).

<a name="lamentations_3_9"></a>Lamentations 3:9

He hath [gāḏar](../../strongs/h/h1443.md) my [derek](../../strongs/h/h1870.md) with [gāzîṯ](../../strongs/h/h1496.md), he hath made my [nāṯîḇ](../../strongs/h/h5410.md) [ʿāvâ](../../strongs/h/h5753.md).

<a name="lamentations_3_10"></a>Lamentations 3:10

He was unto me as a [dōḇ](../../strongs/h/h1677.md) lying in ['arab](../../strongs/h/h693.md), and as an ['ariy](../../strongs/h/h738.md) in [mictar](../../strongs/h/h4565.md).

<a name="lamentations_3_11"></a>Lamentations 3:11

He hath [cuwr](../../strongs/h/h5493.md) my [derek](../../strongs/h/h1870.md), and [pāšaḥ](../../strongs/h/h6582.md) me: he hath [śûm](../../strongs/h/h7760.md) me [šāmēm](../../strongs/h/h8074.md).

<a name="lamentations_3_12"></a>Lamentations 3:12

He hath [dāraḵ](../../strongs/h/h1869.md) his [qesheth](../../strongs/h/h7198.md), and [nāṣaḇ](../../strongs/h/h5324.md) me as a [maṭṭārâ](../../strongs/h/h4307.md) for the [chets](../../strongs/h/h2671.md).

<a name="lamentations_3_13"></a>Lamentations 3:13

He hath caused the [ben](../../strongs/h/h1121.md) of his ['ašpâ](../../strongs/h/h827.md) to [bow'](../../strongs/h/h935.md) into my [kilyah](../../strongs/h/h3629.md).

<a name="lamentations_3_14"></a>Lamentations 3:14

I was a [śᵊḥôq](../../strongs/h/h7814.md) to all my ['am](../../strongs/h/h5971.md); and their [nᵊḡînâ](../../strongs/h/h5058.md) all the [yowm](../../strongs/h/h3117.md).

<a name="lamentations_3_15"></a>Lamentations 3:15

He hath [sāׂbaʿ](../../strongs/h/h7646.md) me with [merōr](../../strongs/h/h4844.md), he hath made me [rāvâ](../../strongs/h/h7301.md) with [laʿănâ](../../strongs/h/h3939.md).

<a name="lamentations_3_16"></a>Lamentations 3:16

He hath also [gāras](../../strongs/h/h1638.md) my [šēn](../../strongs/h/h8127.md) with [ḥāṣāṣ](../../strongs/h/h2687.md), he hath [kāp̄aš](../../strongs/h/h3728.md) me with ['ēp̄er](../../stzrongs/h/h665.md).

<a name="lamentations_3_17"></a>Lamentations 3:17

And thou hast [zānaḥ](../../strongs/h/h2186.md) my [nephesh](../../strongs/h/h5315.md) far [zānaḥ](../../strongs/h/h2186.md) from [shalowm](../../strongs/h/h7965.md): I [nāšâ](../../strongs/h/h5382.md) [towb](../../strongs/h/h2896.md).

<a name="lamentations_3_18"></a>Lamentations 3:18

And I ['āmar](../../strongs/h/h559.md), My [netsach](../../strongs/h/h5331.md) and my [tôḥeleṯ](../../strongs/h/h8431.md) is ['abad](../../strongs/h/h6.md) from [Yĕhovah](../../strongs/h/h3068.md):

<a name="lamentations_3_19"></a>Lamentations 3:19

[zakar](../../strongs/h/h2142.md) mine ['oniy](../../strongs/h/h6040.md) and my [mārûḏ](../../strongs/h/h4788.md), the [laʿănâ](../../strongs/h/h3939.md) and the [rō'š](../../strongs/h/h7219.md).

<a name="lamentations_3_20"></a>Lamentations 3:20

My [nephesh](../../strongs/h/h5315.md) hath them [zakar](../../strongs/h/h2142.md) in [zakar](../../strongs/h/h2142.md), and is [šûaḥ](../../strongs/h/h7743.md) in me.

<a name="lamentations_3_21"></a>Lamentations 3:21

This I [shuwb](../../strongs/h/h7725.md) to my [leb](../../strongs/h/h3820.md), therefore have I [yāḥal](../../strongs/h/h3176.md).

<a name="lamentations_3_22"></a>Lamentations 3:22

It is of [Yĕhovah](../../strongs/h/h3068.md) [checed](../../strongs/h/h2617.md) that we are not [tamam](../../strongs/h/h8552.md), because his [raḥam](../../strongs/h/h7356.md) [kalah](../../strongs/h/h3615.md) not.

<a name="lamentations_3_23"></a>Lamentations 3:23

They are [ḥāḏāš](../../strongs/h/h2319.md) every [boqer](../../strongs/h/h1242.md): [rab](../../strongs/h/h7227.md) is thy ['ĕmûnâ](../../strongs/h/h530.md).

<a name="lamentations_3_24"></a>Lamentations 3:24

[Yĕhovah](../../strongs/h/h3068.md) is my [cheleq](../../strongs/h/h2506.md), ['āmar](../../strongs/h/h559.md) my [nephesh](../../strongs/h/h5315.md); therefore will I [yāḥal](../../strongs/h/h3176.md) in him.

<a name="lamentations_3_25"></a>Lamentations 3:25

[Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md) unto them that [qāvâ](../../strongs/h/h6960.md) for him, to the [nephesh](../../strongs/h/h5315.md) that [darash](../../strongs/h/h1875.md) him.

<a name="lamentations_3_26"></a>Lamentations 3:26

It is [towb](../../strongs/h/h2896.md) that a man should both [yāḥîl](../../strongs/h/h3175.md) [chuwl](../../strongs/h/h2342.md) and [dûmām](../../strongs/h/h1748.md) for the [tᵊšûʿâ](../../strongs/h/h8668.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="lamentations_3_27"></a>Lamentations 3:27

It is [towb](../../strongs/h/h2896.md) for a [geḇer](../../strongs/h/h1397.md) that he [nasa'](../../strongs/h/h5375.md) the [ʿōl](../../strongs/h/h5923.md) in his [nāʿur](../../strongs/h/h5271.md).

<a name="lamentations_3_28"></a>Lamentations 3:28

He [yashab](../../strongs/h/h3427.md) [bāḏāḏ](../../strongs/h/h910.md) and keepeth [damam](../../strongs/h/h1826.md), because he hath [nāṭal](../../strongs/h/h5190.md) it upon him.

<a name="lamentations_3_29"></a>Lamentations 3:29

He [nathan](../../strongs/h/h5414.md) his [peh](../../strongs/h/h6310.md) in the ['aphar](../../strongs/h/h6083.md); if so be there may be [tiqvâ](../../strongs/h/h8615.md).

<a name="lamentations_3_30"></a>Lamentations 3:30

He [nathan](../../strongs/h/h5414.md) his [lᵊḥî](../../strongs/h/h3895.md) to him that [nakah](../../strongs/h/h5221.md) him: he is [sāׂbaʿ](../../strongs/h/h7646.md) with [cherpah](../../strongs/h/h2781.md).

<a name="lamentations_3_31"></a>Lamentations 3:31

For the ['adonay](../../strongs/h/h136.md) will not [zānaḥ](../../strongs/h/h2186.md) ['owlam](../../strongs/h/h5769.md):

<a name="lamentations_3_32"></a>Lamentations 3:32

But though he cause [yāḡâ](../../strongs/h/h3013.md), yet will he have [racham](../../strongs/h/h7355.md) according to the [rōḇ](../../strongs/h/h7230.md) of his [checed](../../strongs/h/h2617.md).

<a name="lamentations_3_33"></a>Lamentations 3:33

For he doth not [ʿānâ](../../strongs/h/h6031.md) [leb](../../strongs/h/h3820.md) nor [yāḡâ](../../strongs/h/h3013.md) the [ben](../../strongs/h/h1121.md) of ['iysh](../../strongs/h/h376.md).

<a name="lamentations_3_34"></a>Lamentations 3:34

To [dāḵā'](../../strongs/h/h1792.md) under his [regel](../../strongs/h/h7272.md) all the ['āsîr](../../strongs/h/h615.md) of the ['erets](../../strongs/h/h776.md),

<a name="lamentations_3_35"></a>Lamentations 3:35

To [natah](../../strongs/h/h5186.md) the [mishpat](../../strongs/h/h4941.md) of a [geḇer](../../strongs/h/h1397.md) before the [paniym](../../strongs/h/h6440.md) of the ['elyown](../../strongs/h/h5945.md),

<a name="lamentations_3_36"></a>Lamentations 3:36

To [ʿāvaṯ](../../strongs/h/h5791.md) an ['āḏām](../../strongs/h/h120.md) in his [rîḇ](../../strongs/h/h7379.md), the ['adonay](../../strongs/h/h136.md) [ra'ah](../../strongs/h/h7200.md) not.

<a name="lamentations_3_37"></a>Lamentations 3:37

Who is he that ['āmar](../../strongs/h/h559.md), and it cometh to pass, when the ['adonay](../../strongs/h/h136.md) [tsavah](../../strongs/h/h6680.md) it not?

<a name="lamentations_3_38"></a>Lamentations 3:38

Out of the [peh](../../strongs/h/h6310.md) of the ['elyown](../../strongs/h/h5945.md) [yāṣā'](../../strongs/h/h3318.md) not [ra'](../../strongs/h/h7451.md) and [towb](../../strongs/h/h2896.md)?

<a name="lamentations_3_39"></a>Lamentations 3:39

Wherefore doth a [chay](../../strongs/h/h2416.md) ['āḏām](../../strongs/h/h120.md) ['ānan](../../strongs/h/h596.md), a [geḇer](../../strongs/h/h1397.md) for his [ḥēṭĕ'](../../strongs/h/h2399.md)?

<a name="lamentations_3_40"></a>Lamentations 3:40

Let us [ḥāp̄aś](../../strongs/h/h2664.md) and [chaqar](../../strongs/h/h2713.md) our [derek](../../strongs/h/h1870.md), and [shuwb](../../strongs/h/h7725.md) to [Yĕhovah](../../strongs/h/h3068.md).

<a name="lamentations_3_41"></a>Lamentations 3:41

Let us [nasa'](../../strongs/h/h5375.md) our [lebab](../../strongs/h/h3824.md) with our [kaph](../../strongs/h/h3709.md) unto ['el](../../strongs/h/h410.md) in the [shamayim](../../strongs/h/h8064.md).

<a name="lamentations_3_42"></a>Lamentations 3:42

We have [pāšaʿ](../../strongs/h/h6586.md) and have [marah](../../strongs/h/h4784.md): thou hast not [sālaḥ](../../strongs/h/h5545.md).

<a name="lamentations_3_43"></a>Lamentations 3:43

Thou hast [cakak](../../strongs/h/h5526.md) with ['aph](../../strongs/h/h639.md), and [radaph](../../strongs/h/h7291.md) us: thou hast [harag](../../strongs/h/h2026.md), thou hast not [ḥāmal](../../strongs/h/h2550.md).

<a name="lamentations_3_44"></a>Lamentations 3:44

Thou hast [cakak](../../strongs/h/h5526.md) thyself with a [ʿānān](../../strongs/h/h6051.md), that our [tĕphillah](../../strongs/h/h8605.md) should not ['abar](../../strongs/h/h5674.md).

<a name="lamentations_3_45"></a>Lamentations 3:45

Thou hast [śûm](../../strongs/h/h7760.md) us as the [sᵊḥî](../../strongs/h/h5501.md) and [mā'ôs](../../strongs/h/h3973.md) in the [qereḇ](../../strongs/h/h7130.md) of the ['am](../../strongs/h/h5971.md).

<a name="lamentations_3_46"></a>Lamentations 3:46

All our ['oyeb](../../strongs/h/h341.md) have [pāṣâ](../../strongs/h/h6475.md) their [peh](../../strongs/h/h6310.md) against us.

<a name="lamentations_3_47"></a>Lamentations 3:47

[paḥaḏ](../../strongs/h/h6343.md) and a [paḥaṯ](../../strongs/h/h6354.md) is come upon us, [šē'ṯ](../../strongs/h/h7612.md) and [šeḇar](../../strongs/h/h7667.md).

<a name="lamentations_3_48"></a>Lamentations 3:48

Mine ['ayin](../../strongs/h/h5869.md) [yarad](../../strongs/h/h3381.md) with [peleḡ](../../strongs/h/h6388.md) of [mayim](../../strongs/h/h4325.md) for the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md).

<a name="lamentations_3_49"></a>Lamentations 3:49

Mine ['ayin](../../strongs/h/h5869.md) trickleth [nāḡar](../../strongs/h/h5064.md), and [damah](../../strongs/h/h1820.md) not, without any [hăp̄uḡâ](../../strongs/h/h2014.md),

<a name="lamentations_3_50"></a>Lamentations 3:50

Till [Yĕhovah](../../strongs/h/h3068.md) [šāqap̄](../../strongs/h/h8259.md), and [ra'ah](../../strongs/h/h7200.md) from [shamayim](../../strongs/h/h8064.md).

<a name="lamentations_3_51"></a>Lamentations 3:51

Mine ['ayin](../../strongs/h/h5869.md) [ʿālal](../../strongs/h/h5953.md) mine [nephesh](../../strongs/h/h5315.md) because of all the [bath](../../strongs/h/h1323.md) of my [ʿîr](../../strongs/h/h5892.md).

<a name="lamentations_3_52"></a>Lamentations 3:52

Mine ['oyeb](../../strongs/h/h341.md) [ṣûḏ](../../strongs/h/h6679.md) me [ṣûḏ](../../strongs/h/h6679.md), like a [tsippowr](../../strongs/h/h6833.md), without [ḥinnām](../../strongs/h/h2600.md).

<a name="lamentations_3_53"></a>Lamentations 3:53

They have [ṣāmaṯ](../../strongs/h/h6789.md) my [chay](../../strongs/h/h2416.md) in the [bowr](../../strongs/h/h953.md), and [yadah](../../strongs/h/h3034.md) an ['eben](../../strongs/h/h68.md) upon me.

<a name="lamentations_3_54"></a>Lamentations 3:54

[mayim](../../strongs/h/h4325.md) [ṣûp̄](../../strongs/h/h6687.md) mine [ro'sh](../../strongs/h/h7218.md); then I ['āmar](../../strongs/h/h559.md), I am [gāzar](../../strongs/h/h1504.md).

<a name="lamentations_3_55"></a>Lamentations 3:55

I [qara'](../../strongs/h/h7121.md) upon thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md), out of the [taḥtî](../../strongs/h/h8482.md) [bowr](../../strongs/h/h953.md).

<a name="lamentations_3_56"></a>Lamentations 3:56

Thou hast [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md): ['alam](../../strongs/h/h5956.md) not thine ['ozen](../../strongs/h/h241.md) at my [rᵊvāḥâ](../../strongs/h/h7309.md), at my [shav'ah](../../strongs/h/h7775.md).

<a name="lamentations_3_57"></a>Lamentations 3:57

Thou [qāraḇ](../../strongs/h/h7126.md) in the [yowm](../../strongs/h/h3117.md) that I [qara'](../../strongs/h/h7121.md) upon thee: thou ['āmar](../../strongs/h/h559.md), [yare'](../../strongs/h/h3372.md) not.

<a name="lamentations_3_58"></a>Lamentations 3:58

['Adonay](../../strongs/h/h136.md), thou hast [riyb](../../strongs/h/h7378.md) the [rîḇ](../../strongs/h/h7379.md) of my [nephesh](../../strongs/h/h5315.md); thou hast [gā'al](../../strongs/h/h1350.md) my [chay](../../strongs/h/h2416.md).

<a name="lamentations_3_59"></a>Lamentations 3:59

[Yĕhovah](../../strongs/h/h3068.md), thou hast [ra'ah](../../strongs/h/h7200.md) my [ʿaûāṯâ](../../strongs/h/h5792.md): [shaphat](../../strongs/h/h8199.md) thou my [mishpat](../../strongs/h/h4941.md).

<a name="lamentations_3_60"></a>Lamentations 3:60

Thou hast [ra'ah](../../strongs/h/h7200.md) all their [nᵊqāmâ](../../strongs/h/h5360.md) and all their [maḥăšāḇâ](../../strongs/h/h4284.md) against me.

<a name="lamentations_3_61"></a>Lamentations 3:61

Thou hast [shama'](../../strongs/h/h8085.md) their [cherpah](../../strongs/h/h2781.md), [Yĕhovah](../../strongs/h/h3068.md), and all their [maḥăšāḇâ](../../strongs/h/h4284.md) against me;

<a name="lamentations_3_62"></a>Lamentations 3:62

The [saphah](../../strongs/h/h8193.md) of those that [quwm](../../strongs/h/h6965.md) against me, and their [higgayown](../../strongs/h/h1902.md) against me all the [yowm](../../strongs/h/h3117.md).

<a name="lamentations_3_63"></a>Lamentations 3:63

[nabat](../../strongs/h/h5027.md) their [yashab](../../strongs/h/h3427.md), and their [qîmâ](../../strongs/h/h7012.md); I am their [mangînâ](../../strongs/h/h4485.md).

<a name="lamentations_3_64"></a>Lamentations 3:64

[shuwb](../../strongs/h/h7725.md) unto them a [gĕmwl](../../strongs/h/h1576.md), [Yĕhovah](../../strongs/h/h3068.md), according to the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md).

<a name="lamentations_3_65"></a>Lamentations 3:65

[nathan](../../strongs/h/h5414.md) them [mᵊḡinnâ](../../strongs/h/h4044.md) of [leb](../../strongs/h/h3820.md), thy [ta'ălâ](../../strongs/h/h8381.md) unto them.

<a name="lamentations_3_66"></a>Lamentations 3:66

[radaph](../../strongs/h/h7291.md) and [šāmaḏ](../../strongs/h/h8045.md) them in ['aph](../../strongs/h/h639.md) from under the [shamayim](../../strongs/h/h8064.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Lamentations](lamentations.md)

[Lamentations 2](lamentations_2.md) - [Lamentations 4](lamentations_4.md)