# [Lamentations 5](https://www.blueletterbible.org/kjv/lamentations/5)

<a name="lamentations_5_1"></a>Lamentations 5:1

[zakar](../../strongs/h/h2142.md), [Yĕhovah](../../strongs/h/h3068.md), what is come upon us: [nabat](../../strongs/h/h5027.md), and [ra'ah](../../strongs/h/h7200.md) our [cherpah](../../strongs/h/h2781.md).

<a name="lamentations_5_2"></a>Lamentations 5:2

Our [nachalah](../../strongs/h/h5159.md) is [hāp̄aḵ](../../strongs/h/h2015.md) to [zûr](../../strongs/h/h2114.md), our [bayith](../../strongs/h/h1004.md) to [nāḵrî](../../strongs/h/h5237.md).

<a name="lamentations_5_3"></a>Lamentations 5:3

We are [yathowm](../../strongs/h/h3490.md) and ['în](../../strongs/h/h369.md) ['ab](../../strongs/h/h1.md), our ['em](../../strongs/h/h517.md) are as ['almānâ](../../strongs/h/h490.md).

<a name="lamentations_5_4"></a>Lamentations 5:4

We have [šāṯâ](../../strongs/h/h8354.md) our [mayim](../../strongs/h/h4325.md) for [keceph](../../strongs/h/h3701.md); our ['ets](../../strongs/h/h6086.md) is [bow'](../../strongs/h/h935.md) [mᵊḥîr](../../strongs/h/h4242.md) unto us.

<a name="lamentations_5_5"></a>Lamentations 5:5

Our [ṣaûā'r](../../strongs/h/h6677.md) are under [radaph](../../strongs/h/h7291.md): we [yaga'](../../strongs/h/h3021.md), and have no [nuwach](../../strongs/h/h5117.md).

<a name="lamentations_5_6"></a>Lamentations 5:6

We have [nathan](../../strongs/h/h5414.md) the [yad](../../strongs/h/h3027.md) to the [Mitsrayim](../../strongs/h/h4714.md), and to the ['Aššûr](../../strongs/h/h804.md), to be [sāׂbaʿ](../../strongs/h/h7646.md) with [lechem](../../strongs/h/h3899.md).

<a name="lamentations_5_7"></a>Lamentations 5:7

Our ['ab](../../strongs/h/h1.md) have [chata'](../../strongs/h/h2398.md), and are not; and we have [sāḇal](../../strongs/h/h5445.md) their ['avon](../../strongs/h/h5771.md).

<a name="lamentations_5_8"></a>Lamentations 5:8

['ebed](../../strongs/h/h5650.md) have [mashal](../../strongs/h/h4910.md) over us: there is none that doth [paraq](../../strongs/h/h6561.md) us out of their [yad](../../strongs/h/h3027.md).

<a name="lamentations_5_9"></a>Lamentations 5:9

We [bow'](../../strongs/h/h935.md) our [lechem](../../strongs/h/h3899.md) with the peril of our [nephesh](../../strongs/h/h5315.md) [paniym](../../strongs/h/h6440.md) of the [chereb](../../strongs/h/h2719.md) of the [midbar](../../strongs/h/h4057.md).

<a name="lamentations_5_10"></a>Lamentations 5:10

Our ['owr](../../strongs/h/h5785.md) was [kāmar](../../strongs/h/h3648.md) like a [tannûr](../../strongs/h/h8574.md) [paniym](../../strongs/h/h6440.md) of the [zal'aphah](../../strongs/h/h2152.md) [rāʿāḇ](../../strongs/h/h7458.md).

<a name="lamentations_5_11"></a>Lamentations 5:11

They [ʿānâ](../../strongs/h/h6031.md) the ['ishshah](../../strongs/h/h802.md) in [Tsiyown](../../strongs/h/h6726.md), and the [bᵊṯûlâ](../../strongs/h/h1330.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="lamentations_5_12"></a>Lamentations 5:12

[śar](../../strongs/h/h8269.md) are [tālâ](../../strongs/h/h8518.md) by their [yad](../../strongs/h/h3027.md): the [paniym](../../strongs/h/h6440.md) of [zāqēn](../../strongs/h/h2205.md) were not [hāḏar](../../strongs/h/h1921.md).

<a name="lamentations_5_13"></a>Lamentations 5:13

They [nasa'](../../strongs/h/h5375.md) the [bāḥûr](../../strongs/h/h970.md) to [ṭᵊḥôn](../../strongs/h/h2911.md) , and the [naʿar](../../strongs/h/h5288.md) [kashal](../../strongs/h/h3782.md) under the ['ets](../../strongs/h/h6086.md).

<a name="lamentations_5_14"></a>Lamentations 5:14

The [zāqēn](../../strongs/h/h2205.md) have [shabath](../../strongs/h/h7673.md) from the [sha'ar](../../strongs/h/h8179.md), the [bāḥûr](../../strongs/h/h970.md) from their [nᵊḡînâ](../../strongs/h/h5058.md).

<a name="lamentations_5_15"></a>Lamentations 5:15

The [māśôś](../../strongs/h/h4885.md) of our [leb](../../strongs/h/h3820.md) is [shabath](../../strongs/h/h7673.md); our [māḥôl](../../strongs/h/h4234.md) is [hāp̄aḵ](../../strongs/h/h2015.md) into ['ēḇel](../../strongs/h/h60.md).

<a name="lamentations_5_16"></a>Lamentations 5:16

The [ʿăṭārâ](../../strongs/h/h5850.md) is [naphal](../../strongs/h/h5307.md) from our [ro'sh](../../strongs/h/h7218.md): ['owy](../../strongs/h/h188.md) unto us, that we have [chata'](../../strongs/h/h2398.md)!

<a name="lamentations_5_17"></a>Lamentations 5:17

For this our [leb](../../strongs/h/h3820.md) is [dāvê](../../strongs/h/h1739.md); for these things our ['ayin](../../strongs/h/h5869.md) are [ḥāšaḵ](../../strongs/h/h2821.md).

<a name="lamentations_5_18"></a>Lamentations 5:18

Because of the [har](../../strongs/h/h2022.md) of [Tsiyown](../../strongs/h/h6726.md), which is [šāmēm](../../strongs/h/h8074.md), the [šûʿāl](../../strongs/h/h7776.md) [halak](../../strongs/h/h1980.md) upon it.

<a name="lamentations_5_19"></a>Lamentations 5:19

Thou, [Yĕhovah](../../strongs/h/h3068.md), [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md); thy [kicce'](../../strongs/h/h3678.md) from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md).

<a name="lamentations_5_20"></a>Lamentations 5:20

Wherefore dost thou [shakach](../../strongs/h/h7911.md) us for [netsach](../../strongs/h/h5331.md), and ['azab](../../strongs/h/h5800.md) us so ['ōreḵ](../../strongs/h/h753.md) [yowm](../../strongs/h/h3117.md)?

<a name="lamentations_5_21"></a>Lamentations 5:21

[shuwb](../../strongs/h/h7725.md) thou us unto thee, [Yĕhovah](../../strongs/h/h3068.md), and we shall be [shuwb](../../strongs/h/h7725.md); [ḥādaš](../../strongs/h/h2318.md) our [yowm](../../strongs/h/h3117.md) as of [qeḏem](../../strongs/h/h6924.md).

<a name="lamentations_5_22"></a>Lamentations 5:22

But thou hast [mā'as](../../strongs/h/h3988.md) [mā'as](../../strongs/h/h3988.md) us; thou art [me'od](../../strongs/h/h3966.md) [qāṣap̄](../../strongs/h/h7107.md) against us.

---

[Transliteral Bible](../bible.md)

[Lamentations](lamentations.md)

[Lamentations 4](lamentations_4.md)