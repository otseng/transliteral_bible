# [Lamentations 1](https://www.blueletterbible.org/kjv/lamentations/1)

<a name="lamentations_1_1"></a>Lamentations 1:1

How doth the [ʿîr](../../strongs/h/h5892.md) [yashab](../../strongs/h/h3427.md) [bāḏāḏ](../../strongs/h/h910.md), that was [rab](../../strongs/h/h7227.md) of ['am](../../strongs/h/h5971.md)! how is she become as an ['almānâ](../../strongs/h/h490.md)! she that was [rab](../../strongs/h/h7227.md) among the [gowy](../../strongs/h/h1471.md), and [śārâ](../../strongs/h/h8282.md) among the [mᵊḏînâ](../../strongs/h/h4082.md), how is she become [mas](../../strongs/h/h4522.md)!

<a name="lamentations_1_2"></a>Lamentations 1:2

She [bāḵâ](../../strongs/h/h1058.md) [bāḵâ](../../strongs/h/h1058.md) in the [layil](../../strongs/h/h3915.md), and her [dim'ah](../../strongs/h/h1832.md) are on her [lᵊḥî](../../strongs/h/h3895.md): among all her ['ahab](../../strongs/h/h157.md) she hath none to [nacham](../../strongs/h/h5162.md) her: all her [rea'](../../strongs/h/h7453.md) have [bāḡaḏ](../../strongs/h/h898.md) with her, they are become her ['oyeb](../../strongs/h/h341.md).

<a name="lamentations_1_3"></a>Lamentations 1:3

[Yehuwdah](../../strongs/h/h3063.md) is gone into [gālâ](../../strongs/h/h1540.md) because of ['oniy](../../strongs/h/h6040.md), and because of [rōḇ](../../strongs/h/h7230.md) [ʿăḇōḏâ](../../strongs/h/h5656.md): she [yashab](../../strongs/h/h3427.md) among the [gowy](../../strongs/h/h1471.md), she [māṣā'](../../strongs/h/h4672.md) no [mānôaḥ](../../strongs/h/h4494.md): all her [radaph](../../strongs/h/h7291.md) [nāśaḡ](../../strongs/h/h5381.md) her between the [mēṣar](../../strongs/h/h4712.md).

<a name="lamentations_1_4"></a>Lamentations 1:4

The [derek](../../strongs/h/h1870.md) of [Tsiyown](../../strongs/h/h6726.md) do ['āḇēl](../../strongs/h/h57.md), because none [bow'](../../strongs/h/h935.md) to the [môʿēḏ](../../strongs/h/h4150.md): all her [sha'ar](../../strongs/h/h8179.md) are [šāmēm](../../strongs/h/h8074.md): her [kōhēn](../../strongs/h/h3548.md) ['ānaḥ](../../strongs/h/h584.md), her [bᵊṯûlâ](../../strongs/h/h1330.md) are [yāḡâ](../../strongs/h/h3013.md), and she is in [mārar](../../strongs/h/h4843.md).

<a name="lamentations_1_5"></a>Lamentations 1:5

Her [tsar](../../strongs/h/h6862.md) are the [ro'sh](../../strongs/h/h7218.md), her ['oyeb](../../strongs/h/h341.md) [šālâ](../../strongs/h/h7951.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [yāḡâ](../../strongs/h/h3013.md) her for the [rōḇ](../../strongs/h/h7230.md) of her [pesha'](../../strongs/h/h6588.md): her ['owlel](../../strongs/h/h5768.md) are [halak](../../strongs/h/h1980.md) into [šᵊḇî](../../strongs/h/h7628.md) [paniym](../../strongs/h/h6440.md) the [tsar](../../strongs/h/h6862.md).

<a name="lamentations_1_6"></a>Lamentations 1:6

And from the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) all her [hadar](../../strongs/h/h1926.md) is [yāṣā'](../../strongs/h/h3318.md): her [śar](../../strongs/h/h8269.md) are become like ['ayyāl](../../strongs/h/h354.md) that [māṣā'](../../strongs/h/h4672.md) no [mirʿê](../../strongs/h/h4829.md), and they are [yālaḵ](../../strongs/h/h3212.md) without [koach](../../strongs/h/h3581.md) [paniym](../../strongs/h/h6440.md) the [radaph](../../strongs/h/h7291.md).

<a name="lamentations_1_7"></a>Lamentations 1:7

[Yĕruwshalaim](../../strongs/h/h3389.md) [zakar](../../strongs/h/h2142.md) in the [yowm](../../strongs/h/h3117.md) of her ['oniy](../../strongs/h/h6040.md) and of her [mārûḏ](../../strongs/h/h4788.md) all her pleasant [maḥmaḏ](../../strongs/h/h4262.md) that she had in the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md), when her ['am](../../strongs/h/h5971.md) [naphal](../../strongs/h/h5307.md) into the [yad](../../strongs/h/h3027.md) of the [tsar](../../strongs/h/h6862.md), and none did [ʿāzar](../../strongs/h/h5826.md) her: the [tsar](../../strongs/h/h6862.md) [ra'ah](../../strongs/h/h7200.md) her, and did [śāḥaq](../../strongs/h/h7832.md) at her [mišbāṯ](../../strongs/h/h4868.md).

<a name="lamentations_1_8"></a>Lamentations 1:8

[Yĕruwshalaim](../../strongs/h/h3389.md) hath [ḥēṭĕ'](../../strongs/h/h2399.md) [chata'](../../strongs/h/h2398.md); therefore she is [nîḏâ](../../strongs/h/h5206.md): all that [kabad](../../strongs/h/h3513.md) her [zûl](../../strongs/h/h2107.md) her, because they have [ra'ah](../../strongs/h/h7200.md) her [ʿervâ](../../strongs/h/h6172.md): yea, she ['ānaḥ](../../strongs/h/h584.md), and [shuwb](../../strongs/h/h7725.md) ['āḥôr](../../strongs/h/h268.md).

<a name="lamentations_1_9"></a>Lamentations 1:9

Her [ṭām'â](../../strongs/h/h2932.md) is in her [shuwl](../../strongs/h/h7757.md); she [zakar](../../strongs/h/h2142.md) not her ['aḥărîṯ](../../strongs/h/h319.md); therefore she [yarad](../../strongs/h/h3381.md) [pele'](../../strongs/h/h6382.md): she had no [nacham](../../strongs/h/h5162.md). [Yĕhovah](../../strongs/h/h3068.md), [ra'ah](../../strongs/h/h7200.md) my ['oniy](../../strongs/h/h6040.md): for the ['oyeb](../../strongs/h/h341.md) hath [gāḏal](../../strongs/h/h1431.md) himself.

<a name="lamentations_1_10"></a>Lamentations 1:10

The [tsar](../../strongs/h/h6862.md) hath [pāraś](../../strongs/h/h6566.md) his [yad](../../strongs/h/h3027.md) upon all her [maḥmāḏ](../../strongs/h/h4261.md): for she hath [ra'ah](../../strongs/h/h7200.md) that the [gowy](../../strongs/h/h1471.md) [bow'](../../strongs/h/h935.md) into her [miqdash](../../strongs/h/h4720.md), whom thou didst [tsavah](../../strongs/h/h6680.md) that they should not [bow'](../../strongs/h/h935.md) into thy [qāhēl](../../strongs/h/h6951.md).

<a name="lamentations_1_11"></a>Lamentations 1:11

All her ['am](../../strongs/h/h5971.md) ['ānaḥ](../../strongs/h/h584.md), they [bāqaš](../../strongs/h/h1245.md) [lechem](../../strongs/h/h3899.md); they have [nathan](../../strongs/h/h5414.md) their [maḥmāḏ](../../strongs/h/h4261.md) [maḥmaḏ](../../strongs/h/h4262.md) for ['ōḵel](../../strongs/h/h400.md) to [shuwb](../../strongs/h/h7725.md) the [nephesh](../../strongs/h/h5315.md): [ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md), and [nabat](../../strongs/h/h5027.md); for I am become [zālal](../../strongs/h/h2151.md).

<a name="lamentations_1_12"></a>Lamentations 1:12

Is it nothing to you, all ye that ['abar](../../strongs/h/h5674.md) [derek](../../strongs/h/h1870.md)? [nabat](../../strongs/h/h5027.md), and [ra'ah](../../strongs/h/h7200.md) if there be any [maḵ'ōḇ](../../strongs/h/h4341.md) like unto my [maḵ'ōḇ](../../strongs/h/h4341.md), which is [ʿālal](../../strongs/h/h5953.md) unto me, wherewith [Yĕhovah](../../strongs/h/h3068.md) hath [yāḡâ](../../strongs/h/h3013.md) me in the [yowm](../../strongs/h/h3117.md) of his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md).

<a name="lamentations_1_13"></a>Lamentations 1:13

From [marowm](../../strongs/h/h4791.md) hath he [shalach](../../strongs/h/h7971.md) ['esh](../../strongs/h/h784.md) into my ['etsem](../../strongs/h/h6106.md), and it [radah](../../strongs/h/h7287.md) against them: he hath [pāraś](../../strongs/h/h6566.md) a [rešeṯ](../../strongs/h/h7568.md) for my [regel](../../strongs/h/h7272.md), he hath [shuwb](../../strongs/h/h7725.md) me ['āḥôr](../../strongs/h/h268.md): he hath [nathan](../../strongs/h/h5414.md) me [šāmēm](../../strongs/h/h8074.md) and [dāvê](../../strongs/h/h1739.md) all the [yowm](../../strongs/h/h3117.md).

<a name="lamentations_1_14"></a>Lamentations 1:14

The [ʿōl](../../strongs/h/h5923.md) of my [pesha'](../../strongs/h/h6588.md) is [śāqaḏ](../../strongs/h/h8244.md) by his [yad](../../strongs/h/h3027.md): they are [śāraḡ](../../strongs/h/h8276.md), and [ʿālâ](../../strongs/h/h5927.md) upon my [ṣaûā'r](../../strongs/h/h6677.md): he hath made my [koach](../../strongs/h/h3581.md) to [kashal](../../strongs/h/h3782.md), the ['adonay](../../strongs/h/h136.md) hath [nathan](../../strongs/h/h5414.md) me into their [yad](../../strongs/h/h3027.md), from whom I am not [yakol](../../strongs/h/h3201.md) to [quwm](../../strongs/h/h6965.md).

<a name="lamentations_1_15"></a>Lamentations 1:15

The ['adonay](../../strongs/h/h136.md) hath [sālâ](../../strongs/h/h5541.md) all my ['abîr](../../strongs/h/h47.md) in the [qereḇ](../../strongs/h/h7130.md) of me: he hath [qara'](../../strongs/h/h7121.md) a [môʿēḏ](../../strongs/h/h4150.md) against me to [shabar](../../strongs/h/h7665.md) my [bāḥûr](../../strongs/h/h970.md): the ['adonay](../../strongs/h/h136.md) hath [dāraḵ](../../strongs/h/h1869.md) the [bᵊṯûlâ](../../strongs/h/h1330.md), the [bath](../../strongs/h/h1323.md) of [Yehuwdah](../../strongs/h/h3063.md), as in a [gaṯ](../../strongs/h/h1660.md).

<a name="lamentations_1_16"></a>Lamentations 1:16

For these things I [bāḵâ](../../strongs/h/h1058.md); mine ['ayin](../../strongs/h/h5869.md), mine ['ayin](../../strongs/h/h5869.md) [yarad](../../strongs/h/h3381.md) with [mayim](../../strongs/h/h4325.md), because the [nacham](../../strongs/h/h5162.md) that should [shuwb](../../strongs/h/h7725.md) my [nephesh](../../strongs/h/h5315.md) is [rachaq](../../strongs/h/h7368.md) from me: my [ben](../../strongs/h/h1121.md) are [šāmēm](../../strongs/h/h8074.md), because the ['oyeb](../../strongs/h/h341.md) [gabar](../../strongs/h/h1396.md).

<a name="lamentations_1_17"></a>Lamentations 1:17

[Tsiyown](../../strongs/h/h6726.md) [pāraś](../../strongs/h/h6566.md) her [yad](../../strongs/h/h3027.md), and there is none to [nacham](../../strongs/h/h5162.md) her: [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md) concerning [Ya'aqob](../../strongs/h/h3290.md), that his [tsar](../../strongs/h/h6862.md) should be [cabiyb](../../strongs/h/h5439.md) him: [Yĕruwshalaim](../../strongs/h/h3389.md) is as a [nidâ](../../strongs/h/h5079.md) among them.

<a name="lamentations_1_18"></a>Lamentations 1:18

[Yĕhovah](../../strongs/h/h3068.md) is [tsaddiyq](../../strongs/h/h6662.md); for I have [marah](../../strongs/h/h4784.md) against his [peh](../../strongs/h/h6310.md): [shama'](../../strongs/h/h8085.md), I pray you, all ['am](../../strongs/h/h5971.md), and [ra'ah](../../strongs/h/h7200.md) my [maḵ'ōḇ](../../strongs/h/h4341.md): my [bᵊṯûlâ](../../strongs/h/h1330.md) and my [bāḥûr](../../strongs/h/h970.md) are [halak](../../strongs/h/h1980.md) into [šᵊḇî](../../strongs/h/h7628.md).

<a name="lamentations_1_19"></a>Lamentations 1:19

I [qara'](../../strongs/h/h7121.md) for my ['ahab](../../strongs/h/h157.md), but they [rāmâ](../../strongs/h/h7411.md) me: my [kōhēn](../../strongs/h/h3548.md) and mine [zāqēn](../../strongs/h/h2205.md) gave up the [gāvaʿ](../../strongs/h/h1478.md) in the [ʿîr](../../strongs/h/h5892.md), while they [bāqaš](../../strongs/h/h1245.md) their ['ōḵel](../../strongs/h/h400.md) to [shuwb](../../strongs/h/h7725.md) their [nephesh](../../strongs/h/h5315.md).

<a name="lamentations_1_20"></a>Lamentations 1:20

[ra'ah](../../strongs/h/h7200.md), [Yĕhovah](../../strongs/h/h3068.md); for I am in [tsarar](../../strongs/h/h6887.md): my [me'ah](../../strongs/h/h4578.md) are [ḥāmar](../../strongs/h/h2560.md); mine [leb](../../strongs/h/h3820.md) is [hāp̄aḵ](../../strongs/h/h2015.md) [qereḇ](../../strongs/h/h7130.md) me; for I have [marah](../../strongs/h/h4784.md) [marah](../../strongs/h/h4784.md): [ḥûṣ](../../strongs/h/h2351.md) the [chereb](../../strongs/h/h2719.md) [šāḵōl](../../strongs/h/h7921.md), at [bayith](../../strongs/h/h1004.md) there is as [maveth](../../strongs/h/h4194.md).

<a name="lamentations_1_21"></a>Lamentations 1:21

They have [shama'](../../strongs/h/h8085.md) that I ['ānaḥ](../../strongs/h/h584.md): there is none to [nacham](../../strongs/h/h5162.md) me: all mine ['oyeb](../../strongs/h/h341.md) have [shama'](../../strongs/h/h8085.md) of my [ra'](../../strongs/h/h7451.md); they are [śûś](../../strongs/h/h7797.md) that thou hast ['asah](../../strongs/h/h6213.md) it: thou wilt [bow'](../../strongs/h/h935.md) the [yowm](../../strongs/h/h3117.md) that thou hast [qara'](../../strongs/h/h7121.md), and they shall be like unto me.

<a name="lamentations_1_22"></a>Lamentations 1:22

Let all their [ra'](../../strongs/h/h7451.md) [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) thee; and [ʿālal](../../strongs/h/h5953.md) unto them, as thou hast [ʿālal](../../strongs/h/h5953.md) unto me for all my [pesha'](../../strongs/h/h6588.md): for my ['anachah](../../strongs/h/h585.md) are [rab](../../strongs/h/h7227.md), and my [leb](../../strongs/h/h3820.md) is [daûāy](../../strongs/h/h1742.md).

---

[Transliteral Bible](../bible.md)

[Lamentations](lamentations.md)

[Lamentations 2](lamentations_2.md)