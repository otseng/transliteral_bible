# [Lamentations 4](https://www.blueletterbible.org/kjv/lamentations/4)

<a name="lamentations_4_1"></a>Lamentations 4:1

How is the [zāhāḇ](../../strongs/h/h2091.md) become [ʿāmam](../../strongs/h/h6004.md)! how is the [towb](../../strongs/h/h2896.md) [keṯem](../../strongs/h/h3800.md) [šānā'](../../strongs/h/h8132.md)! the ['eben](../../strongs/h/h68.md) of the [qodesh](../../strongs/h/h6944.md) are [šāp̄aḵ](../../strongs/h/h8210.md) in the [ro'sh](../../strongs/h/h7218.md) of every [ḥûṣ](../../strongs/h/h2351.md).

<a name="lamentations_4_2"></a>Lamentations 4:2

The [yāqār](../../strongs/h/h3368.md) [ben](../../strongs/h/h1121.md) of [Tsiyown](../../strongs/h/h6726.md), [sālā'](../../strongs/h/h5537.md) to [pāz](../../strongs/h/h6337.md), how are they [chashab](../../strongs/h/h2803.md) as [ḥereś](../../strongs/h/h2789.md) [neḇel](../../strongs/h/h5035.md), the [ma'aseh](../../strongs/h/h4639.md) of the [yad](../../strongs/h/h3027.md) of the [yāṣar](../../strongs/h/h3335.md)!

<a name="lamentations_4_3"></a>Lamentations 4:3

Even the [tannîn](../../strongs/h/h8577.md) [chalats](../../strongs/h/h2502.md) the [šaḏ](../../strongs/h/h7699.md), they [yānaq](../../strongs/h/h3243.md) to their [gûr](../../strongs/h/h1482.md): the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) is become ['aḵzār](../../strongs/h/h393.md), like the [yāʿēn](../../strongs/h/h3283.md) in the [midbar](../../strongs/h/h4057.md).

<a name="lamentations_4_4"></a>Lamentations 4:4

The [lashown](../../strongs/h/h3956.md) of the [yānaq](../../strongs/h/h3243.md) [dāḇaq](../../strongs/h/h1692.md) to the roof of his [ḥēḵ](../../strongs/h/h2441.md) for [ṣāmā'](../../strongs/h/h6772.md): the ['owlel](../../strongs/h/h5768.md) [sha'al](../../strongs/h/h7592.md) [lechem](../../strongs/h/h3899.md), and no man [pāraś](../../strongs/h/h6566.md) it unto them.

<a name="lamentations_4_5"></a>Lamentations 4:5

They that did ['akal](../../strongs/h/h398.md) [maʿăḏān](../../strongs/h/h4574.md) are [šāmēm](../../strongs/h/h8074.md) in the [ḥûṣ](../../strongs/h/h2351.md): they that were ['aman](../../strongs/h/h539.md) in [tôlāʿ](../../strongs/h/h8438.md) [ḥāḇaq](../../strongs/h/h2263.md) ['ašpōṯ](../../strongs/h/h830.md).

<a name="lamentations_4_6"></a>Lamentations 4:6

For the punishment of the ['avon](../../strongs/h/h5771.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) is [gāḏal](../../strongs/h/h1431.md) than the [chatta'ath](../../strongs/h/h2403.md) of the sin of [Sᵊḏōm](../../strongs/h/h5467.md), that was [hāp̄aḵ](../../strongs/h/h2015.md) as in a [reḡaʿ](../../strongs/h/h7281.md), and no [yad](../../strongs/h/h3027.md) [chuwl](../../strongs/h/h2342.md) on her.

<a name="lamentations_4_7"></a>Lamentations 4:7

Her [nāzîr](../../strongs/h/h5139.md) were [zāḵaḵ](../../strongs/h/h2141.md) than [šeleḡ](../../strongs/h/h7950.md), they were [ṣāḥaḥ](../../strongs/h/h6705.md) than [chalab](../../strongs/h/h2461.md), they were more ['āḏam](../../strongs/h/h119.md) in ['etsem](../../strongs/h/h6106.md) than [p̄nyym](../../strongs/h/h6443.md), their [gizrâ](../../strongs/h/h1508.md) was of [sapîr](../../strongs/h/h5601.md):

<a name="lamentations_4_8"></a>Lamentations 4:8

Their [tō'ar](../../strongs/h/h8389.md) is [ḥāšaḵ](../../strongs/h/h2821.md) than a [šᵊḥôr](../../strongs/h/h7815.md); they are not [nāḵar](../../strongs/h/h5234.md) in the [ḥûṣ](../../strongs/h/h2351.md): their ['owr](../../strongs/h/h5785.md) [ṣāp̄aḏ](../../strongs/h/h6821.md) to their ['etsem](../../strongs/h/h6106.md); it is [yāḇēš](../../strongs/h/h3001.md), it is become like an ['ets](../../strongs/h/h6086.md).

<a name="lamentations_4_9"></a>Lamentations 4:9

They that be [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md) are [towb](../../strongs/h/h2896.md) than they that be [ḥālāl](../../strongs/h/h2491.md) with [rāʿāḇ](../../strongs/h/h7458.md): for these pine [zûḇ](../../strongs/h/h2100.md), [dāqar](../../strongs/h/h1856.md) through for want of the [tᵊnûḇâ](../../strongs/h/h8570.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="lamentations_4_10"></a>Lamentations 4:10

The [yad](../../strongs/h/h3027.md) of the [raḥmānî](../../strongs/h/h7362.md) ['ishshah](../../strongs/h/h802.md) have [bāšal](../../strongs/h/h1310.md) their own [yeleḏ](../../strongs/h/h3206.md): they were their [bārâ](../../strongs/h/h1262.md) in the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md).

<a name="lamentations_4_11"></a>Lamentations 4:11

[Yĕhovah](../../strongs/h/h3068.md) hath [kalah](../../strongs/h/h3615.md) his [chemah](../../strongs/h/h2534.md); he hath [šāp̄aḵ](../../strongs/h/h8210.md) his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md), and hath [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in [Tsiyown](../../strongs/h/h6726.md), and it hath ['akal](../../strongs/h/h398.md) the [yᵊsôḏ](../../strongs/h/h3247.md) thereof.

<a name="lamentations_4_12"></a>Lamentations 4:12

The [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md), and all the [yashab](../../strongs/h/h3427.md) of the [tebel](../../strongs/h/h8398.md), would not have ['aman](../../strongs/h/h539.md) that the [tsar](../../strongs/h/h6862.md) and the ['oyeb](../../strongs/h/h341.md) should have [bow'](../../strongs/h/h935.md) into the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="lamentations_4_13"></a>Lamentations 4:13

For the [chatta'ath](../../strongs/h/h2403.md) of her [nāḇî'](../../strongs/h/h5030.md), and the ['avon](../../strongs/h/h5771.md) of her [kōhēn](../../strongs/h/h3548.md), that have [šāp̄aḵ](../../strongs/h/h8210.md) the [dam](../../strongs/h/h1818.md) of the [tsaddiyq](../../strongs/h/h6662.md) in the [qereḇ](../../strongs/h/h7130.md) of her,

<a name="lamentations_4_14"></a>Lamentations 4:14

They have [nûaʿ](../../strongs/h/h5128.md) as [ʿiûēr](../../strongs/h/h5787.md) men in the [ḥûṣ](../../strongs/h/h2351.md), they have [gā'al](../../strongs/h/h1351.md) themselves with [dam](../../strongs/h/h1818.md), so that men [yakol](../../strongs/h/h3201.md) not [naga'](../../strongs/h/h5060.md) their [lᵊḇûš](../../strongs/h/h3830.md).

<a name="lamentations_4_15"></a>Lamentations 4:15

They [qara'](../../strongs/h/h7121.md) unto them, [cuwr](../../strongs/h/h5493.md) ye; it is [tame'](../../strongs/h/h2931.md); [cuwr](../../strongs/h/h5493.md), [cuwr](../../strongs/h/h5493.md), [naga'](../../strongs/h/h5060.md) not: when they [nûṣ](../../strongs/h/h5132.md) and [nûaʿ](../../strongs/h/h5128.md), they ['āmar](../../strongs/h/h559.md) among the [gowy](../../strongs/h/h1471.md), They shall no more [guwr](../../strongs/h/h1481.md) there.

<a name="lamentations_4_16"></a>Lamentations 4:16

The [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [chalaq](../../strongs/h/h2505.md) them; he will no more [nabat](../../strongs/h/h5027.md) them: they [nasa'](../../strongs/h/h5375.md) not the [paniym](../../strongs/h/h6440.md) of the [kōhēn](../../strongs/h/h3548.md), they [chanan](../../strongs/h/h2603.md) not the [zāqēn](../../strongs/h/h2205.md).

<a name="lamentations_4_17"></a>Lamentations 4:17

As for us, our ['ayin](../../strongs/h/h5869.md) as yet [kalah](../../strongs/h/h3615.md) for our [heḇel](../../strongs/h/h1892.md) [ʿezrâ](../../strongs/h/h5833.md): in our [ṣipîyâ](../../strongs/h/h6836.md) we have [tsaphah](../../strongs/h/h6822.md) for a [gowy](../../strongs/h/h1471.md) that could not [yasha'](../../strongs/h/h3467.md) us.

<a name="lamentations_4_18"></a>Lamentations 4:18

They [ṣûḏ](../../strongs/h/h6679.md) our [ṣaʿaḏ](../../strongs/h/h6806.md), that we cannot [yālaḵ](../../strongs/h/h3212.md) in our [rᵊḥōḇ](../../strongs/h/h7339.md): our [qēṣ](../../strongs/h/h7093.md) is [qāraḇ](../../strongs/h/h7126.md), our [yowm](../../strongs/h/h3117.md) are [mālā'](../../strongs/h/h4390.md); for our [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md).

<a name="lamentations_4_19"></a>Lamentations 4:19

Our [radaph](../../strongs/h/h7291.md) are [qal](../../strongs/h/h7031.md) than the [nesheׁr](../../strongs/h/h5404.md) of the [shamayim](../../strongs/h/h8064.md): they [dalaq](../../strongs/h/h1814.md) us upon the [har](../../strongs/h/h2022.md), they ['arab](../../strongs/h/h693.md) for us in the [midbar](../../strongs/h/h4057.md).

<a name="lamentations_4_20"></a>Lamentations 4:20

The [ruwach](../../strongs/h/h7307.md) of our ['aph](../../strongs/h/h639.md), the [mashiyach](../../strongs/h/h4899.md) of [Yĕhovah](../../strongs/h/h3068.md), was [lāḵaḏ](../../strongs/h/h3920.md) in their [šᵊḥîṯ](../../strongs/h/h7825.md), of whom we ['āmar](../../strongs/h/h559.md), Under his [ṣēl](../../strongs/h/h6738.md) we shall [ḥāyâ](../../strongs/h/h2421.md) among the [gowy](../../strongs/h/h1471.md).

<a name="lamentations_4_21"></a>Lamentations 4:21

[śûś](../../strongs/h/h7797.md) and be [samach](../../strongs/h/h8055.md), O [bath](../../strongs/h/h1323.md) of ['Ĕḏōm](../../strongs/h/h123.md), that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [ʿÛṣ](../../strongs/h/h5780.md); the [kowc](../../strongs/h/h3563.md) also shall ['abar](../../strongs/h/h5674.md) through unto thee: thou shalt be [šāḵar](../../strongs/h/h7937.md), and shalt make thyself [ʿārâ](../../strongs/h/h6168.md).

<a name="lamentations_4_22"></a>Lamentations 4:22

The punishment of thine ['avon](../../strongs/h/h5771.md) is [tamam](../../strongs/h/h8552.md), O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md); he will no more carry thee away into [gālâ](../../strongs/h/h1540.md): he will [paqad](../../strongs/h/h6485.md) thine ['avon](../../strongs/h/h5771.md), O [bath](../../strongs/h/h1323.md) of ['Ĕḏōm](../../strongs/h/h123.md); he will [gālâ](../../strongs/h/h1540.md) thy [chatta'ath](../../strongs/h/h2403.md).

---

[Transliteral Bible](../bible.md)

[Lamentations](lamentations.md)

[Lamentations 3](lamentations_3.md) - [Lamentations 5](lamentations_5.md)