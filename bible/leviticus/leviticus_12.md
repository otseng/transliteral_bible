# [Leviticus 12](https://www.blueletterbible.org/kjv/lev/12/1/)

<a name="leviticus_12_1"></a>Leviticus 12:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_12_2"></a>Leviticus 12:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), If an ['ishshah](../../strongs/h/h802.md) have [zāraʿ](../../strongs/h/h2232.md), and [yalad](../../strongs/h/h3205.md) a [zāḵār](../../strongs/h/h2145.md): then she shall be [ṭāmē'](../../strongs/h/h2930.md) seven [yowm](../../strongs/h/h3117.md); according to the [yowm](../../strongs/h/h3117.md) of the [nidâ](../../strongs/h/h5079.md) for her [dāvâ](../../strongs/h/h1738.md) shall she be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_12_3"></a>Leviticus 12:3

And in the eighth [yowm](../../strongs/h/h3117.md) the [basar](../../strongs/h/h1320.md) of his [ʿārlâ](../../strongs/h/h6190.md) shall be [muwl](../../strongs/h/h4135.md).

<a name="leviticus_12_4"></a>Leviticus 12:4

And she shall then [yashab](../../strongs/h/h3427.md) in the [dam](../../strongs/h/h1818.md) of her [ṭāhŏrâ](../../strongs/h/h2893.md) three and thirty [yowm](../../strongs/h/h3117.md); she shall [naga'](../../strongs/h/h5060.md) no [qodesh](../../strongs/h/h6944.md) thing, nor [bow'](../../strongs/h/h935.md) into the [miqdash](../../strongs/h/h4720.md), until the [yowm](../../strongs/h/h3117.md) of her [ṭōhar](../../strongs/h/h2892.md) be [mālā'](../../strongs/h/h4390.md).

<a name="leviticus_12_5"></a>Leviticus 12:5

But if she [yalad](../../strongs/h/h3205.md) a [nᵊqēḇâ](../../strongs/h/h5347.md), then she shall be [ṭāmē'](../../strongs/h/h2930.md) two [šāḇûaʿ](../../strongs/h/h7620.md), as in her [nidâ](../../strongs/h/h5079.md): and she shall [yashab](../../strongs/h/h3427.md) in the [dam](../../strongs/h/h1818.md) of her [ṭāhŏrâ](../../strongs/h/h2893.md) threescore and six [yowm](../../strongs/h/h3117.md).

<a name="leviticus_12_6"></a>Leviticus 12:6

And when the [yowm](../../strongs/h/h3117.md) of her [ṭōhar](../../strongs/h/h2892.md) are [mālā'](../../strongs/h/h4390.md), for a [ben](../../strongs/h/h1121.md), or for a [bath](../../strongs/h/h1323.md), she shall [bow'](../../strongs/h/h935.md) a [keḇeś](../../strongs/h/h3532.md) of the first [šānâ](../../strongs/h/h8141.md) for an [ʿōlâ](../../strongs/h/h5930.md), and a [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), or a [tôr](../../strongs/h/h8449.md), for a [chatta'ath](../../strongs/h/h2403.md), unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_12_7"></a>Leviticus 12:7

Who shall [qāraḇ](../../strongs/h/h7126.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for her; and she shall be [ṭāhēr](../../strongs/h/h2891.md) from the [māqôr](../../strongs/h/h4726.md) of her [dam](../../strongs/h/h1818.md). This is the [towrah](../../strongs/h/h8451.md) for her that hath [yalad](../../strongs/h/h3205.md) a [zāḵār](../../strongs/h/h2145.md) or a [nᵊqēḇâ](../../strongs/h/h5347.md).

<a name="leviticus_12_8"></a>Leviticus 12:8

And if she be not [māṣā'](../../strongs/h/h4672.md) [day](../../strongs/h/h1767.md) [yad](../../strongs/h/h3027.md) a [śê](../../strongs/h/h7716.md), then she shall [laqach](../../strongs/h/h3947.md) two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md); the one for the an [ʿōlâ](../../strongs/h/h5930.md), and the other for a [chatta'ath](../../strongs/h/h2403.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for her, and she shall be [ṭāhēr](../../strongs/h/h2891.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 11](leviticus_11.md) - [Leviticus 13](leviticus_13.md)