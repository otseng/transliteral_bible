# [Leviticus 17](https://www.blueletterbible.org/kjv/lev/17/1/)

<a name="leviticus_17_1"></a>Leviticus 17:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_17_2"></a>Leviticus 17:2

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), and unto his [ben](../../strongs/h/h1121.md), and unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them; This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_17_3"></a>Leviticus 17:3

What ['iysh](../../strongs/h/h376.md) soever there be of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), that [šāḥaṭ](../../strongs/h/h7819.md) a [showr](../../strongs/h/h7794.md), or [keśeḇ](../../strongs/h/h3775.md), or [ʿēz](../../strongs/h/h5795.md), in the [maḥănê](../../strongs/h/h4264.md), or that [šāḥaṭ](../../strongs/h/h7819.md) it [ḥûṣ](../../strongs/h/h2351.md) of the [maḥănê](../../strongs/h/h4264.md),

<a name="leviticus_17_4"></a>Leviticus 17:4

And [bow'](../../strongs/h/h935.md) it not unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), to [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md) the [miškān](../../strongs/h/h4908.md) of [Yĕhovah](../../strongs/h/h3068.md); [dam](../../strongs/h/h1818.md) shall be [chashab](../../strongs/h/h2803.md) unto that ['iysh](../../strongs/h/h376.md); he hath [šāp̄aḵ](../../strongs/h/h8210.md) [dam](../../strongs/h/h1818.md); and that ['iysh](../../strongs/h/h376.md) shall be [karath](../../strongs/h/h3772.md) from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md):

<a name="leviticus_17_5"></a>Leviticus 17:5

To the end that the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) may [bow'](../../strongs/h/h935.md) their [zebach](../../strongs/h/h2077.md), which they [zabach](../../strongs/h/h2076.md) in the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), even that they may [bow'](../../strongs/h/h935.md) them unto [Yĕhovah](../../strongs/h/h3068.md), unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), unto the [kōhēn](../../strongs/h/h3548.md), and [zabach](../../strongs/h/h2076.md) them for [šelem](../../strongs/h/h8002.md) [zebach](../../strongs/h/h2077.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_17_6"></a>Leviticus 17:6

And the [kōhēn](../../strongs/h/h3548.md) shall [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md) of [Yĕhovah](../../strongs/h/h3068.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [qāṭar](../../strongs/h/h6999.md) the [cheleb](../../strongs/h/h2459.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_17_7"></a>Leviticus 17:7

And they shall no more [zabach](../../strongs/h/h2076.md) their [zebach](../../strongs/h/h2077.md) unto [śāʿîr](../../strongs/h/h8163.md), ['aḥar](../../strongs/h/h310.md) whom they have [zānâ](../../strongs/h/h2181.md). This shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) unto them throughout their [dôr](../../strongs/h/h1755.md).

<a name="leviticus_17_8"></a>Leviticus 17:8

And thou shalt ['āmar](../../strongs/h/h559.md) unto them, Whatsoever ['iysh](../../strongs/h/h376.md) there be of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) which [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you, that [ʿālâ](../../strongs/h/h5927.md) an [ʿōlâ](../../strongs/h/h5930.md) or [zebach](../../strongs/h/h2077.md),

<a name="leviticus_17_9"></a>Leviticus 17:9

And [bow'](../../strongs/h/h935.md) it not unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), to ['asah](../../strongs/h/h6213.md) it unto [Yĕhovah](../../strongs/h/h3068.md); even that ['iysh](../../strongs/h/h376.md) shall be [karath](../../strongs/h/h3772.md) from among his ['am](../../strongs/h/h5971.md).

<a name="leviticus_17_10"></a>Leviticus 17:10

And whatsoever ['iysh](../../strongs/h/h376.md) there be of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you, that ['akal](../../strongs/h/h398.md) any manner of [dam](../../strongs/h/h1818.md); I will even [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against that [nephesh](../../strongs/h/h5315.md) that ['akal](../../strongs/h/h398.md) [dam](../../strongs/h/h1818.md), and will [karath](../../strongs/h/h3772.md) him from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md).

<a name="leviticus_17_11"></a>Leviticus 17:11

For the [nephesh](../../strongs/h/h5315.md) of the [basar](../../strongs/h/h1320.md) is in the [dam](../../strongs/h/h1818.md): and I have [nathan](../../strongs/h/h5414.md) it to you upon the [mizbeach](../../strongs/h/h4196.md) to make a [kāp̄ar](../../strongs/h/h3722.md) for your [nephesh](../../strongs/h/h5315.md): for it is the [dam](../../strongs/h/h1818.md) that maketh a [kāp̄ar](../../strongs/h/h3722.md) for the [nephesh](../../strongs/h/h5315.md).

<a name="leviticus_17_12"></a>Leviticus 17:12

Therefore I ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), No [nephesh](../../strongs/h/h5315.md) of you shall ['akal](../../strongs/h/h398.md) [dam](../../strongs/h/h1818.md), neither shall any [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you ['akal](../../strongs/h/h398.md) [dam](../../strongs/h/h1818.md).

<a name="leviticus_17_13"></a>Leviticus 17:13

And whatsoever ['iysh](../../strongs/h/h376.md) there be of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you, which [ṣûḏ](../../strongs/h/h6679.md) and [ṣayiḏ](../../strongs/h/h6718.md) any [chay](../../strongs/h/h2416.md) or [ʿôp̄](../../strongs/h/h5775.md) that may be ['akal](../../strongs/h/h398.md); he shall even [šāp̄aḵ](../../strongs/h/h8210.md) out the [dam](../../strongs/h/h1818.md) thereof, and [kāsâ](../../strongs/h/h3680.md) it with ['aphar](../../strongs/h/h6083.md).

<a name="leviticus_17_14"></a>Leviticus 17:14

For it is the [nephesh](../../strongs/h/h5315.md) of all [basar](../../strongs/h/h1320.md); the [dam](../../strongs/h/h1818.md) of it is for the [nephesh](../../strongs/h/h5315.md) thereof: therefore I ['āmar](../../strongs/h/h559.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), Ye shall ['akal](../../strongs/h/h398.md) the [dam](../../strongs/h/h1818.md) of no manner of [basar](../../strongs/h/h1320.md): for the [nephesh](../../strongs/h/h5315.md) of all [basar](../../strongs/h/h1320.md) is the [dam](../../strongs/h/h1818.md) thereof: whosoever ['akal](../../strongs/h/h398.md) it shall be [karath](../../strongs/h/h3772.md).

<a name="leviticus_17_15"></a>Leviticus 17:15

And every [nephesh](../../strongs/h/h5315.md) that ['akal](../../strongs/h/h398.md) that which [nᵊḇēlâ](../../strongs/h/h5038.md), or that which was [ṭᵊrēp̄â](../../strongs/h/h2966.md), whether it be one of your own ['ezrāḥ](../../strongs/h/h249.md), or a [ger](../../strongs/h/h1616.md), he shall both [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md): then shall he be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_17_16"></a>Leviticus 17:16

But if he [kāḇas](../../strongs/h/h3526.md) them not, nor [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md); then he shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 16](leviticus_16.md) - [Leviticus 18](leviticus_18.md)