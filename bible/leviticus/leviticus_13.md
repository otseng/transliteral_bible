# [Leviticus 13](https://www.blueletterbible.org/kjv/lev/13/1/)

<a name="leviticus_13_1"></a>Leviticus 13:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_13_2"></a>Leviticus 13:2

When an ['adam](../../strongs/h/h120.md) shall have in the ['owr](../../strongs/h/h5785.md) of his [basar](../../strongs/h/h1320.md) a [śᵊ'ēṯ](../../strongs/h/h7613.md), a [sapaḥaṯ](../../strongs/h/h5597.md), or [bahereṯ](../../strongs/h/h934.md), and it be in the ['owr](../../strongs/h/h5785.md) of his [basar](../../strongs/h/h1320.md) like the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md); then he shall be [bow'](../../strongs/h/h935.md) unto ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md), or unto one of his [ben](../../strongs/h/h1121.md) the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_13_3"></a>Leviticus 13:3

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md) in the ['owr](../../strongs/h/h5785.md) of the [basar](../../strongs/h/h1320.md): and when the [śēʿār](../../strongs/h/h8181.md) in the [neḡaʿ](../../strongs/h/h5061.md) is [hāp̄aḵ](../../strongs/h/h2015.md) [lāḇān](../../strongs/h/h3836.md), and the [neḡaʿ](../../strongs/h/h5061.md) in [mar'ê](../../strongs/h/h4758.md) be [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md) of his [basar](../../strongs/h/h1320.md), it is a [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md): and the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on him, and pronounce him [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_13_4"></a>Leviticus 13:4

If the [bahereṯ](../../strongs/h/h934.md) be [lāḇān](../../strongs/h/h3836.md) in the ['owr](../../strongs/h/h5785.md) of his [basar](../../strongs/h/h1320.md), and in [mar'ê](../../strongs/h/h4758.md) be not [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md), and the [śēʿār](../../strongs/h/h8181.md) thereof be not [hāp̄aḵ](../../strongs/h/h2015.md) [lāḇān](../../strongs/h/h3836.md); then the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) up him that hath the [neḡaʿ](../../strongs/h/h5061.md) seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_13_5"></a>Leviticus 13:5

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on him the seventh [yowm](../../strongs/h/h3117.md): and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) in his ['ayin](../../strongs/h/h5869.md) be at an ['amad](../../strongs/h/h5975.md), and the [neḡaʿ](../../strongs/h/h5061.md) [pāśâ](../../strongs/h/h6581.md) not in the ['owr](../../strongs/h/h5785.md); then the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) him up seven [yowm](../../strongs/h/h3117.md) more:

<a name="leviticus_13_6"></a>Leviticus 13:6

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on him again the seventh [yowm](../../strongs/h/h3117.md): and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) be [kēhê](../../strongs/h/h3544.md), and the [neḡaʿ](../../strongs/h/h5061.md) [pāśâ](../../strongs/h/h6581.md) not in the ['owr](../../strongs/h/h5785.md), the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md): it is but a [mispaḥaṯ](../../strongs/h/h4556.md): and he shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_13_7"></a>Leviticus 13:7

But if the [mispaḥaṯ](../../strongs/h/h4556.md) [pāśâ](../../strongs/h/h6581.md) [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), ['aḥar](../../strongs/h/h310.md) that he hath been [ra'ah](../../strongs/h/h7200.md) of the [kōhēn](../../strongs/h/h3548.md) for his [ṭāhŏrâ](../../strongs/h/h2893.md), he shall be [ra'ah](../../strongs/h/h7200.md) of the [kōhēn](../../strongs/h/h3548.md) again.

<a name="leviticus_13_8"></a>Leviticus 13:8

And if the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md) that, behold, the [mispaḥaṯ](../../strongs/h/h4556.md) [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is a [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="leviticus_13_9"></a>Leviticus 13:9

When the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) is in an ['adam](../../strongs/h/h120.md), then he shall be [bow'](../../strongs/h/h935.md) unto the [kōhēn](../../strongs/h/h3548.md);

<a name="leviticus_13_10"></a>Leviticus 13:10

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) him: and, behold, if the [śᵊ'ēṯ](../../strongs/h/h7613.md) be [lāḇān](../../strongs/h/h3836.md) in the ['owr](../../strongs/h/h5785.md), and it have [hāp̄aḵ](../../strongs/h/h2015.md) the [śēʿār](../../strongs/h/h8181.md) [lāḇān](../../strongs/h/h3836.md), and there be [miḥyâ](../../strongs/h/h4241.md) [chay](../../strongs/h/h2416.md) [basar](../../strongs/h/h1320.md) in the [śᵊ'ēṯ](../../strongs/h/h7613.md);

<a name="leviticus_13_11"></a>Leviticus 13:11

It is a [yashen](../../strongs/h/h3462.md) [ṣāraʿaṯ](../../strongs/h/h6883.md) in the ['owr](../../strongs/h/h5785.md) of his [basar](../../strongs/h/h1320.md), and the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md), and shall not [cagar](../../strongs/h/h5462.md) him up: for he is [tame'](../../strongs/h/h2931.md).

<a name="leviticus_13_12"></a>Leviticus 13:12

And if a [ṣāraʿaṯ](../../strongs/h/h6883.md) [pāraḥ](../../strongs/h/h6524.md) [pāraḥ](../../strongs/h/h6524.md) in the ['owr](../../strongs/h/h5785.md), and the [ṣāraʿaṯ](../../strongs/h/h6883.md) [kāsâ](../../strongs/h/h3680.md) all the ['owr](../../strongs/h/h5785.md) of him that hath the [neḡaʿ](../../strongs/h/h5061.md) from his [ro'sh](../../strongs/h/h7218.md) even to his [regel](../../strongs/h/h7272.md), wheresoever the [kōhēn](../../strongs/h/h3548.md) [mar'ê](../../strongs/h/h4758.md) ['ayin](../../strongs/h/h5869.md);

<a name="leviticus_13_13"></a>Leviticus 13:13

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md): and, behold, if the [ṣāraʿaṯ](../../strongs/h/h6883.md) have [kāsâ](../../strongs/h/h3680.md) all his [basar](../../strongs/h/h1320.md), he shall pronounce him [ṭāhēr](../../strongs/h/h2891.md) that hath the [neḡaʿ](../../strongs/h/h5061.md): it is all [hāp̄aḵ](../../strongs/h/h2015.md) [lāḇān](../../strongs/h/h3836.md): he is [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_13_14"></a>Leviticus 13:14

But [yowm](../../strongs/h/h3117.md) [chay](../../strongs/h/h2416.md) [basar](../../strongs/h/h1320.md) [ra'ah](../../strongs/h/h7200.md) in him, he shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_13_15"></a>Leviticus 13:15

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) the [chay](../../strongs/h/h2416.md) [basar](../../strongs/h/h1320.md), and pronounce him to be [ṭāmē'](../../strongs/h/h2930.md): for the [chay](../../strongs/h/h2416.md) [basar](../../strongs/h/h1320.md) is [tame'](../../strongs/h/h2931.md): it is a [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="leviticus_13_16"></a>Leviticus 13:16

Or if the [chay](../../strongs/h/h2416.md) [basar](../../strongs/h/h1320.md) [shuwb](../../strongs/h/h7725.md), and be [hāp̄aḵ](../../strongs/h/h2015.md) unto [lāḇān](../../strongs/h/h3836.md), he shall [bow'](../../strongs/h/h935.md) unto the [kōhēn](../../strongs/h/h3548.md);

<a name="leviticus_13_17"></a>Leviticus 13:17

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) him: and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) be [hāp̄aḵ](../../strongs/h/h2015.md) into [lāḇān](../../strongs/h/h3836.md); then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md) that hath the [neḡaʿ](../../strongs/h/h5061.md): he is [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_13_18"></a>Leviticus 13:18

The [basar](../../strongs/h/h1320.md) also, in which, even in the ['owr](../../strongs/h/h5785.md) thereof, was a [šiḥîn](../../strongs/h/h7822.md), and is [rapha'](../../strongs/h/h7495.md),

<a name="leviticus_13_19"></a>Leviticus 13:19

And in the [maqowm](../../strongs/h/h4725.md) of the [šiḥîn](../../strongs/h/h7822.md) there be a [lāḇān](../../strongs/h/h3836.md) [śᵊ'ēṯ](../../strongs/h/h7613.md), or a [bahereṯ](../../strongs/h/h934.md), [lāḇān](../../strongs/h/h3836.md), and ['ăḏamdām](../../strongs/h/h125.md), and it be [ra'ah](../../strongs/h/h7200.md) to the [kōhēn](../../strongs/h/h3548.md);

<a name="leviticus_13_20"></a>Leviticus 13:20

And if, when the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md) it, behold, it be in [mar'ê](../../strongs/h/h4758.md) [šāp̄āl](../../strongs/h/h8217.md) than the ['owr](../../strongs/h/h5785.md), and the [śēʿār](../../strongs/h/h8181.md) thereof be [hāp̄aḵ](../../strongs/h/h2015.md) [lāḇān](../../strongs/h/h3836.md); the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is a [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) [pāraḥ](../../strongs/h/h6524.md) out of the [šiḥîn](../../strongs/h/h7822.md).

<a name="leviticus_13_21"></a>Leviticus 13:21

But if the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md) on it, and, behold, there be no [lāḇān](../../strongs/h/h3836.md) [śēʿār](../../strongs/h/h8181.md) therein, and if it be not [šāp̄āl](../../strongs/h/h8217.md) than the ['owr](../../strongs/h/h5785.md), but be [kēhê](../../strongs/h/h3544.md); then the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) him up seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_13_22"></a>Leviticus 13:22

And if it [pāśâ](../../strongs/h/h6581.md) [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is a [neḡaʿ](../../strongs/h/h5061.md).

<a name="leviticus_13_23"></a>Leviticus 13:23

But if the [bahereṯ](../../strongs/h/h934.md) ['amad](../../strongs/h/h5975.md) in his place, and [pāśâ](../../strongs/h/h6581.md) not, it is a [ṣāreḇeṯ](../../strongs/h/h6867.md) [šiḥîn](../../strongs/h/h7822.md); and the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_13_24"></a>Leviticus 13:24

Or if there be any [basar](../../strongs/h/h1320.md), in the ['owr](../../strongs/h/h5785.md) whereof there is an ['esh](../../strongs/h/h784.md) [miḵvâ](../../strongs/h/h4348.md), and the [miḥyâ](../../strongs/h/h4241.md) that [miḵvâ](../../strongs/h/h4348.md) have a [lāḇān](../../strongs/h/h3836.md) [bahereṯ](../../strongs/h/h934.md), ['ăḏamdām](../../strongs/h/h125.md), or [lāḇān](../../strongs/h/h3836.md);

<a name="leviticus_13_25"></a>Leviticus 13:25

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) upon it: and, behold, if the [śēʿār](../../strongs/h/h8181.md) in the [bahereṯ](../../strongs/h/h934.md) be [hāp̄aḵ](../../strongs/h/h2015.md) [lāḇān](../../strongs/h/h3836.md), and it be in [mar'ê](../../strongs/h/h4758.md) [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md); it is a [ṣāraʿaṯ](../../strongs/h/h6883.md) [pāraḥ](../../strongs/h/h6524.md) of the [miḵvâ](../../strongs/h/h4348.md): wherefore the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="leviticus_13_26"></a>Leviticus 13:26

But if the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md) on it, and, behold, there be no [lāḇān](../../strongs/h/h3836.md) [śēʿār](../../strongs/h/h8181.md) in the [bahereṯ](../../strongs/h/h934.md), and it be no [šāp̄āl](../../strongs/h/h8217.md) than the other ['owr](../../strongs/h/h5785.md), but be [kēhê](../../strongs/h/h3544.md); then the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) him up seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_13_27"></a>Leviticus 13:27

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) upon him the seventh [yowm](../../strongs/h/h3117.md): and if it be [pāśâ](../../strongs/h/h6581.md) [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md).

<a name="leviticus_13_28"></a>Leviticus 13:28

And if the [bahereṯ](../../strongs/h/h934.md) ['amad](../../strongs/h/h5975.md) in his place, and [pāśâ](../../strongs/h/h6581.md) not in the ['owr](../../strongs/h/h5785.md), but it be [kēhê](../../strongs/h/h3544.md); it is a [śᵊ'ēṯ](../../strongs/h/h7613.md) of the [miḵvâ](../../strongs/h/h4348.md), and the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md): for it is a [ṣāreḇeṯ](../../strongs/h/h6867.md) of the [miḵvâ](../../strongs/h/h4348.md).

<a name="leviticus_13_29"></a>Leviticus 13:29

If an ['iysh](../../strongs/h/h376.md) or ['ishshah](../../strongs/h/h802.md) have a [neḡaʿ](../../strongs/h/h5061.md) upon the [ro'sh](../../strongs/h/h7218.md) or the [zāqān](../../strongs/h/h2206.md);

<a name="leviticus_13_30"></a>Leviticus 13:30

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) the [neḡaʿ](../../strongs/h/h5061.md): and, behold, if it be in [mar'ê](../../strongs/h/h4758.md) [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md); and there be in it a [ṣāhōḇ](../../strongs/h/h6669.md) [daq](../../strongs/h/h1851.md) [śēʿār](../../strongs/h/h8181.md); then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md): it is a [neṯeq](../../strongs/h/h5424.md), even a [ṣāraʿaṯ](../../strongs/h/h6883.md) upon the [ro'sh](../../strongs/h/h7218.md) or [zāqān](../../strongs/h/h2206.md).

<a name="leviticus_13_31"></a>Leviticus 13:31

And if the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md) of the [neṯeq](../../strongs/h/h5424.md), and, behold, it be not in [mar'ê](../../strongs/h/h4758.md) [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md), and that there is no [šāḥōr](../../strongs/h/h7838.md) [śēʿār](../../strongs/h/h8181.md) in it; then the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) up him that hath the [neḡaʿ](../../strongs/h/h5061.md) of the [neṯeq](../../strongs/h/h5424.md) seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_13_32"></a>Leviticus 13:32

And in the seventh [yowm](../../strongs/h/h3117.md) the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md): and, behold, if the [neṯeq](../../strongs/h/h5424.md) [pāśâ](../../strongs/h/h6581.md) not, and there be in it no [ṣāhōḇ](../../strongs/h/h6669.md) [śēʿār](../../strongs/h/h8181.md), and the [neṯeq](../../strongs/h/h5424.md) be not in [mar'ê](../../strongs/h/h4758.md) [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md);

<a name="leviticus_13_33"></a>Leviticus 13:33

He shall be [gālaḥ](../../strongs/h/h1548.md), but the [neṯeq](../../strongs/h/h5424.md) shall he not [gālaḥ](../../strongs/h/h1548.md); and the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) up him that hath the [neṯeq](../../strongs/h/h5424.md) seven [yowm](../../strongs/h/h3117.md) more:
He shall be [gālaḥ](../../strongs/h/h1548.md), but the [neṯeq](../../strongs/h/h5424.md) shall he not [gālaḥ](../../strongs/h/h1548.md); and the [kōhēn](../../strongs/h/h3548.md) shall [cagar](../../strongs/h/h5462.md) up him that hath the [neṯeq](../../strongs/h/h5424.md) seven [yowm](../../strongs/h/h3117.md) more:

<a name="leviticus_13_34"></a>Leviticus 13:34

And in the seventh [yowm](../../strongs/h/h3117.md) the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on the [neṯeq](../../strongs/h/h5424.md): and, behold, if the [neṯeq](../../strongs/h/h5424.md) be not [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), nor be in [mar'ê](../../strongs/h/h4758.md) [ʿāmōq](../../strongs/h/h6013.md) than the ['owr](../../strongs/h/h5785.md); then the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md): and he shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_13_35"></a>Leviticus 13:35

But if the [neṯeq](../../strongs/h/h5424.md) [pāśâ](../../strongs/h/h6581.md) [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md) ['aḥar](../../strongs/h/h310.md) his [ṭāhŏrâ](../../strongs/h/h2893.md);

<a name="leviticus_13_36"></a>Leviticus 13:36

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on him: and, behold, if the [neṯeq](../../strongs/h/h5424.md) be [pāśâ](../../strongs/h/h6581.md) in the ['owr](../../strongs/h/h5785.md), the [kōhēn](../../strongs/h/h3548.md) shall not [bāqar](../../strongs/h/h1239.md) for [ṣāhōḇ](../../strongs/h/h6669.md) [śēʿār](../../strongs/h/h8181.md); he is [tame'](../../strongs/h/h2931.md).

<a name="leviticus_13_37"></a>Leviticus 13:37

But if the [neṯeq](../../strongs/h/h5424.md) be in his ['ayin](../../strongs/h/h5869.md) at an ['amad](../../strongs/h/h5975.md), and that there is [šāḥōr](../../strongs/h/h7838.md) [śēʿār](../../strongs/h/h8181.md) [ṣāmaḥ](../../strongs/h/h6779.md) therein; the [neṯeq](../../strongs/h/h5424.md) is [rapha'](../../strongs/h/h7495.md), he is [tahowr](../../strongs/h/h2889.md): and the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md).
But if the [neṯeq](../../strongs/h/h5424.md) be in his ['ayin](../../strongs/h/h5869.md) at an ['amad](../../strongs/h/h5975.md), and that there is [šāḥōr](../../strongs/h/h7838.md) [śēʿār](../../strongs/h/h8181.md) [ṣāmaḥ](../../strongs/h/h6779.md) up therein; the [neṯeq](../../strongs/h/h5424.md) is [rapha'](../../strongs/h/h7495.md), he is [tahowr](../../strongs/h/h2889.md): and the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_13_38"></a>Leviticus 13:38

If an ['iysh](../../strongs/h/h376.md) also or an ['ishshah](../../strongs/h/h802.md) have in the ['owr](../../strongs/h/h5785.md) of their [basar](../../strongs/h/h1320.md) [bahereṯ](../../strongs/h/h934.md), even [lāḇān](../../strongs/h/h3836.md) [bahereṯ](../../strongs/h/h934.md);

<a name="leviticus_13_39"></a>Leviticus 13:39

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md): and, behold, if the [bahereṯ](../../strongs/h/h934.md) in the ['owr](../../strongs/h/h5785.md) of their [basar](../../strongs/h/h1320.md) be [kēhê](../../strongs/h/h3544.md) [lāḇān](../../strongs/h/h3836.md); it is a [bōhaq](../../strongs/h/h933.md) that [pāraḥ](../../strongs/h/h6524.md) in the ['owr](../../strongs/h/h5785.md); he is [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_13_40"></a>Leviticus 13:40

And the ['iysh](../../strongs/h/h376.md) is [māraṭ](../../strongs/h/h4803.md) off his [ro'sh](../../strongs/h/h7218.md), he is [qērēaḥ](../../strongs/h/h7142.md); yet is he [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_13_41"></a>Leviticus 13:41

And he that hath his [māraṭ](../../strongs/h/h4803.md) from the [pē'â](../../strongs/h/h6285.md) of his [ro'sh](../../strongs/h/h7218.md) toward his [paniym](../../strongs/h/h6440.md), he is [gibēaḥ](../../strongs/h/h1371.md): yet is he [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_13_42"></a>Leviticus 13:42

And if there be in the [qāraḥaṯ](../../strongs/h/h7146.md), or [gabaḥaṯ](../../strongs/h/h1372.md), a [lāḇān](../../strongs/h/h3836.md) ['ăḏamdām](../../strongs/h/h125.md) [neḡaʿ](../../strongs/h/h5061.md); it is a [ṣāraʿaṯ](../../strongs/h/h6883.md) [pāraḥ](../../strongs/h/h6524.md) in his [qāraḥaṯ](../../strongs/h/h7146.md), or his [gabaḥaṯ](../../strongs/h/h1372.md).

<a name="leviticus_13_43"></a>Leviticus 13:43

Then the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) upon it: and, behold, if the [śᵊ'ēṯ](../../strongs/h/h7613.md) of the [neḡaʿ](../../strongs/h/h5061.md) be [lāḇān](../../strongs/h/h3836.md) ['ăḏamdām](../../strongs/h/h125.md) in his [qāraḥaṯ](../../strongs/h/h7146.md), or in his [gabaḥaṯ](../../strongs/h/h1372.md), as the [ṣāraʿaṯ](../../strongs/h/h6883.md) [mar'ê](../../strongs/h/h4758.md) in the ['owr](../../strongs/h/h5785.md) of the [basar](../../strongs/h/h1320.md);

<a name="leviticus_13_44"></a>Leviticus 13:44

He is a [ṣāraʿ](../../strongs/h/h6879.md) ['iysh](../../strongs/h/h376.md), he is [tame'](../../strongs/h/h2931.md): the [kōhēn](../../strongs/h/h3548.md) shall pronounce him [ṭāmē'](../../strongs/h/h2930.md) [ṭāmē'](../../strongs/h/h2930.md); his [neḡaʿ](../../strongs/h/h5061.md) is in his [ro'sh](../../strongs/h/h7218.md).

<a name="leviticus_13_45"></a>Leviticus 13:45

And the [ṣāraʿ](../../strongs/h/h6879.md) in whom the [neḡaʿ](../../strongs/h/h5061.md) is, his [beḡeḏ](../../strongs/h/h899.md) shall be [pāram](../../strongs/h/h6533.md), and his [ro'sh](../../strongs/h/h7218.md) [pāraʿ](../../strongs/h/h6544.md), and he shall [ʿāṭâ](../../strongs/h/h5844.md) upon his [śāp̄ām](../../strongs/h/h8222.md), and shall [qara'](../../strongs/h/h7121.md), [tame'](../../strongs/h/h2931.md), [tame'](../../strongs/h/h2931.md).

<a name="leviticus_13_46"></a>Leviticus 13:46

All the [yowm](../../strongs/h/h3117.md) wherein the [neḡaʿ](../../strongs/h/h5061.md) shall be in him he shall be [ṭāmē'](../../strongs/h/h2930.md); he is [tame'](../../strongs/h/h2931.md): he shall [yashab](../../strongs/h/h3427.md) [bāḏāḏ](../../strongs/h/h910.md); [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) shall his [môšāḇ](../../strongs/h/h4186.md) be.

<a name="leviticus_13_47"></a>Leviticus 13:47

The [beḡeḏ](../../strongs/h/h899.md) also that the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) is in, whether it be a [ṣemer](../../strongs/h/h6785.md) [beḡeḏ](../../strongs/h/h899.md), or a [pēšeṯ](../../strongs/h/h6593.md) [beḡeḏ](../../strongs/h/h899.md);

<a name="leviticus_13_48"></a>Leviticus 13:48

Whether it be in the [šᵊṯî](../../strongs/h/h8359.md), or [ʿēreḇ](../../strongs/h/h6154.md); of [pēšeṯ](../../strongs/h/h6593.md), or of [ṣemer](../../strongs/h/h6785.md); whether in a ['owr](../../strongs/h/h5785.md), or in any [mĕla'kah](../../strongs/h/h4399.md) of ['owr](../../strongs/h/h5785.md);

<a name="leviticus_13_49"></a>Leviticus 13:49

And if the [neḡaʿ](../../strongs/h/h5061.md) be [yᵊraqraq](../../strongs/h/h3422.md) or ['ăḏamdām](../../strongs/h/h125.md) in the [beḡeḏ](../../strongs/h/h899.md), or in the ['owr](../../strongs/h/h5785.md), either in the [šᵊṯî](../../strongs/h/h8359.md), or in the [ʿēreḇ](../../strongs/h/h6154.md), or in [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md); it is a [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md), and shall be [ra'ah](../../strongs/h/h7200.md) unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_13_50"></a>Leviticus 13:50

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) upon the [neḡaʿ](../../strongs/h/h5061.md), and [cagar](../../strongs/h/h5462.md) up it that hath the [neḡaʿ](../../strongs/h/h5061.md) seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_13_51"></a>Leviticus 13:51

And he shall [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md) on the seventh [yowm](../../strongs/h/h3117.md): if the [neḡaʿ](../../strongs/h/h5061.md) be [pāśâ](../../strongs/h/h6581.md) in the [beḡeḏ](../../strongs/h/h899.md), either in the [šᵊṯî](../../strongs/h/h8359.md), or in the [ʿēreḇ](../../strongs/h/h6154.md), or in a ['owr](../../strongs/h/h5785.md), or in any [mĕla'kah](../../strongs/h/h4399.md) that is ['asah](../../strongs/h/h6213.md) of ['owr](../../strongs/h/h5785.md); the [neḡaʿ](../../strongs/h/h5061.md) is a [mā'ar](../../strongs/h/h3992.md) [ṣāraʿaṯ](../../strongs/h/h6883.md); it is [tame'](../../strongs/h/h2931.md).

<a name="leviticus_13_52"></a>Leviticus 13:52

He shall therefore [śārap̄](../../strongs/h/h8313.md) that [beḡeḏ](../../strongs/h/h899.md), whether [šᵊṯî](../../strongs/h/h8359.md) or [ʿēreḇ](../../strongs/h/h6154.md), in [ṣemer](../../strongs/h/h6785.md) or in [pēšeṯ](../../strongs/h/h6593.md), or any [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md), wherein the [neḡaʿ](../../strongs/h/h5061.md) is: for it is a [mā'ar](../../strongs/h/h3992.md) [ṣāraʿaṯ](../../strongs/h/h6883.md); it shall be [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md).

<a name="leviticus_13_53"></a>Leviticus 13:53

And if the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md), and, behold, the [neḡaʿ](../../strongs/h/h5061.md) be not [pāśâ](../../strongs/h/h6581.md) in the [beḡeḏ](../../strongs/h/h899.md), either in the [šᵊṯî](../../strongs/h/h8359.md), or in the [ʿēreḇ](../../strongs/h/h6154.md), or in any [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md);

<a name="leviticus_13_54"></a>Leviticus 13:54

Then the [kōhēn](../../strongs/h/h3548.md) shall [tsavah](../../strongs/h/h6680.md) that they [kāḇas](../../strongs/h/h3526.md) the thing wherein the [neḡaʿ](../../strongs/h/h5061.md) is, and he shall [cagar](../../strongs/h/h5462.md) it up seven [yowm](../../strongs/h/h3117.md) more:

<a name="leviticus_13_55"></a>Leviticus 13:55

And the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md), ['aḥar](../../strongs/h/h310.md) that it is [kāḇas](../../strongs/h/h3526.md): and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) have not [hāp̄aḵ](../../strongs/h/h2015.md) his ['ayin](../../strongs/h/h5869.md), and the [neḡaʿ](../../strongs/h/h5061.md) be not [pāśâ](../../strongs/h/h6581.md); it is [tame'](../../strongs/h/h2931.md); thou shalt [śārap̄](../../strongs/h/h8313.md) it in the ['esh](../../strongs/h/h784.md); it is [pᵊḥeṯeṯ](../../strongs/h/h6356.md) inward, whether it be [qāraḥaṯ](../../strongs/h/h7146.md) or [gabaḥaṯ](../../strongs/h/h1372.md).

<a name="leviticus_13_56"></a>Leviticus 13:56

And if the [kōhēn](../../strongs/h/h3548.md) [ra'ah](../../strongs/h/h7200.md), and, behold, the [neḡaʿ](../../strongs/h/h5061.md) be [kēhê](../../strongs/h/h3544.md) ['aḥar](../../strongs/h/h310.md) the [kāḇas](../../strongs/h/h3526.md) of it; then he shall [qāraʿ](../../strongs/h/h7167.md) it out of the [beḡeḏ](../../strongs/h/h899.md), or out of the ['owr](../../strongs/h/h5785.md), or out of the [šᵊṯî](../../strongs/h/h8359.md), or out of the [ʿēreḇ](../../strongs/h/h6154.md):

<a name="leviticus_13_57"></a>Leviticus 13:57

And if it [ra'ah](../../strongs/h/h7200.md) still in the [beḡeḏ](../../strongs/h/h899.md), either in the [šᵊṯî](../../strongs/h/h8359.md), or in the [ʿēreḇ](../../strongs/h/h6154.md), or in any [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md); it is a [pāraḥ](../../strongs/h/h6524.md): thou shalt [śārap̄](../../strongs/h/h8313.md) that wherein the [neḡaʿ](../../strongs/h/h5061.md) is with ['esh](../../strongs/h/h784.md).

<a name="leviticus_13_58"></a>Leviticus 13:58

And the [beḡeḏ](../../strongs/h/h899.md), either [šᵊṯî](../../strongs/h/h8359.md), or [ʿēreḇ](../../strongs/h/h6154.md), or whatsoever [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md) it be, which thou shalt [kāḇas](../../strongs/h/h3526.md), if the [neḡaʿ](../../strongs/h/h5061.md) be [cuwr](../../strongs/h/h5493.md) from them, then it shall be [kāḇas](../../strongs/h/h3526.md) the second time, and shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_13_59"></a>Leviticus 13:59

This is the [towrah](../../strongs/h/h8451.md) of the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) in a [beḡeḏ](../../strongs/h/h899.md) of [ṣemer](../../strongs/h/h6785.md) or [pēšeṯ](../../strongs/h/h6593.md), either in the [šᵊṯî](../../strongs/h/h8359.md), or [ʿēreḇ](../../strongs/h/h6154.md), or any [kĕliy](../../strongs/h/h3627.md) of ['owr](../../strongs/h/h5785.md), to pronounce it [ṭāhēr](../../strongs/h/h2891.md), or to pronounce it [ṭāmē'](../../strongs/h/h2930.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 12](leviticus_12.md) - [Leviticus 14](leviticus_14.md)