# [Leviticus 1](https://www.blueletterbible.org/kjv/lev/1/1)

<a name="leviticus_1_1"></a>Leviticus 1:1

And [Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) unto [Mōshe](../../strongs/h/h4872.md), and [dabar](../../strongs/h/h1696.md) unto him out of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_1_2"></a>Leviticus 1:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, If any ['adam](../../strongs/h/h120.md) of you [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), ye shall [qāraḇ](../../strongs/h/h7126.md) your [qorban](../../strongs/h/h7133.md) of the [bĕhemah](../../strongs/h/h929.md), even of the [bāqār](../../strongs/h/h1241.md), and of the [tso'n](../../strongs/h/h6629.md).

<a name="leviticus_1_3"></a>Leviticus 1:3

If his [qorban](../../strongs/h/h7133.md) be an [ʿōlâ](../../strongs/h/h5930.md) of the [bāqār](../../strongs/h/h1241.md), let him [qāraḇ](../../strongs/h/h7126.md) a [zāḵār](../../strongs/h/h2145.md) [tamiym](../../strongs/h/h8549.md): he shall [qāraḇ](../../strongs/h/h7126.md) it of his own [ratsown](../../strongs/h/h7522.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_1_4"></a>Leviticus 1:4

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [ʿōlâ](../../strongs/h/h5930.md); and it shall be [ratsah](../../strongs/h/h7521.md) for him to make [kāp̄ar](../../strongs/h/h3722.md) for him.

<a name="leviticus_1_5"></a>Leviticus 1:5

And he shall [šāḥaṭ](../../strongs/h/h7819.md) the [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and the [kōhēn](../../strongs/h/h3548.md), ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md), shall [qāraḇ](../../strongs/h/h7126.md) the [dam](../../strongs/h/h1818.md), and [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) [cabiyb](../../strongs/h/h5439.md) upon thea [mizbeach](../../strongs/h/h4196.md) that is by the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_1_6"></a>Leviticus 1:6

And he shall [pāšaṭ](../../strongs/h/h6584.md) the [ʿōlâ](../../strongs/h/h5930.md), and [nāṯaḥ](../../strongs/h/h5408.md) it into his [nēṯaḥ](../../strongs/h/h5409.md).

<a name="leviticus_1_7"></a>Leviticus 1:7

And the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) shall [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) upon thea [mizbeach](../../strongs/h/h4196.md), and lay the ['ets](../../strongs/h/h6086.md) in ['arak](../../strongs/h/h6186.md) upon the ['esh](../../strongs/h/h784.md):

<a name="leviticus_1_8"></a>Leviticus 1:8

And the [kōhēn](../../strongs/h/h3548.md), ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md), shall ['arak](../../strongs/h/h6186.md) the [nēṯaḥ](../../strongs/h/h5409.md), the [ro'sh](../../strongs/h/h7218.md), and the [peḏer](../../strongs/h/h6309.md), in ['arak](../../strongs/h/h6186.md) upon the ['ets](../../strongs/h/h6086.md) that is on the ['esh](../../strongs/h/h784.md) which is upon thea [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_1_9"></a>Leviticus 1:9

But his [qereḇ](../../strongs/h/h7130.md) and his [keraʿ](../../strongs/h/h3767.md) shall he [rāḥaṣ](../../strongs/h/h7364.md) in [mayim](../../strongs/h/h4325.md): and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) all on thea [mizbeach](../../strongs/h/h4196.md), to be an [ʿōlâ](../../strongs/h/h5930.md), an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_1_10"></a>Leviticus 1:10

And if his [qorban](../../strongs/h/h7133.md) be of the [tso'n](../../strongs/h/h6629.md), namely, of the [keśeḇ](../../strongs/h/h3775.md), or of the [ʿēz](../../strongs/h/h5795.md), for an [ʿōlâ](../../strongs/h/h5930.md); he shall [qāraḇ](../../strongs/h/h7126.md) it a [zāḵār](../../strongs/h/h2145.md) [tamiym](../../strongs/h/h8549.md).

<a name="leviticus_1_11"></a>Leviticus 1:11

And he shall [šāḥaṭ](../../strongs/h/h7819.md) it on the [yārēḵ](../../strongs/h/h3409.md) of thea [mizbeach](../../strongs/h/h4196.md) [ṣāp̄ôn](../../strongs/h/h6828.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and the [kōhēn](../../strongs/h/h3548.md), ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md), shall [zāraq](../../strongs/h/h2236.md) his [dam](../../strongs/h/h1818.md) [cabiyb](../../strongs/h/h5439.md) upon thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_1_12"></a>Leviticus 1:12

And he shall [nāṯaḥ](../../strongs/h/h5408.md) it into his [nēṯaḥ](../../strongs/h/h5409.md), with his [ro'sh](../../strongs/h/h7218.md) and his [peḏer](../../strongs/h/h6309.md): and the [kōhēn](../../strongs/h/h3548.md) shall ['arak](../../strongs/h/h6186.md) on the ['ets](../../strongs/h/h6086.md) that is on the ['esh](../../strongs/h/h784.md) which is upon thea [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_1_13"></a>Leviticus 1:13

But he shall [rāḥaṣ](../../strongs/h/h7364.md) the [qereḇ](../../strongs/h/h7130.md) and the [keraʿ](../../strongs/h/h3767.md) with [mayim](../../strongs/h/h4325.md): and the [kōhēn](../../strongs/h/h3548.md) shall [qāraḇ](../../strongs/h/h7126.md) it all, and [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md): it is an [ʿōlâ](../../strongs/h/h5930.md), an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_1_14"></a>Leviticus 1:14

And if the [ʿōlâ](../../strongs/h/h5930.md) for his [qorban](../../strongs/h/h7133.md) to [Yĕhovah](../../strongs/h/h3068.md) be of [ʿôp̄](../../strongs/h/h5775.md), then he shall [qāraḇ](../../strongs/h/h7126.md) his [qorban](../../strongs/h/h7133.md) of [tôr](../../strongs/h/h8449.md), or of [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md).

<a name="leviticus_1_15"></a>Leviticus 1:15

And the [kōhēn](../../strongs/h/h3548.md) shall [qāraḇ](../../strongs/h/h7126.md) it unto thea [mizbeach](../../strongs/h/h4196.md), and [mālaq](../../strongs/h/h4454.md) his [ro'sh](../../strongs/h/h7218.md), and [qāṭar](../../strongs/h/h6999.md) it on thea [mizbeach](../../strongs/h/h4196.md); and the [dam](../../strongs/h/h1818.md) thereof shall be [māṣâ](../../strongs/h/h4680.md) at the [qîr](../../strongs/h/h7023.md) of thea [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_1_16"></a>Leviticus 1:16

And he shall [cuwr](../../strongs/h/h5493.md) his [mur'â](../../strongs/h/h4760.md) with his [nôṣâ](../../strongs/h/h5133.md), and [shalak](../../strongs/h/h7993.md) it beside thea [mizbeach](../../strongs/h/h4196.md) on the [qeḏem](../../strongs/h/h6924.md), by the [maqowm](../../strongs/h/h4725.md) of the [dešen](../../strongs/h/h1880.md):

<a name="leviticus_1_17"></a>Leviticus 1:17

And he shall [šāsaʿ](../../strongs/h/h8156.md) it with the [kanaph](../../strongs/h/h3671.md) thereof, but shall not [bāḏal](../../strongs/h/h914.md): and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md), upon the ['ets](../../strongs/h/h6086.md) that is upon the ['esh](../../strongs/h/h784.md): it is an [ʿōlâ](../../strongs/h/h5930.md), an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 2](leviticus_2.md)