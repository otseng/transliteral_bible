# [Leviticus 2](https://www.blueletterbible.org/kjv/lev/2)

<a name="leviticus_2_1"></a>Leviticus 2:1

And when [nephesh](../../strongs/h/h5315.md) will [qāraḇ](../../strongs/h/h7126.md) a [minchah](../../strongs/h/h4503.md) [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), his [qorban](../../strongs/h/h7133.md) shall be of [sōleṯ](../../strongs/h/h5560.md); and he shall [yāṣaq](../../strongs/h/h3332.md) [šemen](../../strongs/h/h8081.md) upon it, and [nathan](../../strongs/h/h5414.md) [lᵊḇônâ](../../strongs/h/h3828.md) thereon:

<a name="leviticus_2_2"></a>Leviticus 2:2

And he shall [bow'](../../strongs/h/h935.md) it to ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) the [kōhēn](../../strongs/h/h3548.md): and he shall [qāmaṣ](../../strongs/h/h7061.md) [šām](../../strongs/h/h8033.md) his [qōmeṣ](../../strongs/h/h7062.md) [mᵊlō'](../../strongs/h/h4393.md) of the [sōleṯ](../../strongs/h/h5560.md) thereof, and of the [šemen](../../strongs/h/h8081.md) thereof, with all the [lᵊḇônâ](../../strongs/h/h3828.md) thereof; and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) the ['azkārâ](../../strongs/h/h234.md) of it upon thea [mizbeach](../../strongs/h/h4196.md), to be an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md):

<a name="leviticus_2_3"></a>Leviticus 2:3

And the [yāṯar](../../strongs/h/h3498.md) of the [minchah](../../strongs/h/h4503.md) shall be ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md): it is a thing [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_2_4"></a>Leviticus 2:4

And if thou [qāraḇ](../../strongs/h/h7126.md) a  [qorban](../../strongs/h/h7133.md) of a [minchah](../../strongs/h/h4503.md) [ma'ăp̄ê](../../strongs/h/h3989.md) in the [tannûr](../../strongs/h/h8574.md), it shall be [maṣṣâ](../../strongs/h/h4682.md) [ḥallâ](../../strongs/h/h2471.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), or [maṣṣâ](../../strongs/h/h4682.md) [rāqîq](../../strongs/h/h7550.md) [māšaḥ](../../strongs/h/h4886.md) with [šemen](../../strongs/h/h8081.md).

<a name="leviticus_2_5"></a>Leviticus 2:5

And if thy [qorban](../../strongs/h/h7133.md) be a [minchah](../../strongs/h/h4503.md) baken in a [maḥăḇaṯ](../../strongs/h/h4227.md), it shall be of fine [sōleṯ](../../strongs/h/h5560.md) [maṣṣâ](../../strongs/h/h4682.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md).

<a name="leviticus_2_6"></a>Leviticus 2:6

Thou shalt [pāṯaṯ](../../strongs/h/h6626.md) it in [paṯ](../../strongs/h/h6595.md), and [yāṣaq](../../strongs/h/h3332.md) [šemen](../../strongs/h/h8081.md) thereon: it is a [minchah](../../strongs/h/h4503.md).

<a name="leviticus_2_7"></a>Leviticus 2:7

And if thy [qorban](../../strongs/h/h7133.md) be a [minchah](../../strongs/h/h4503.md) baken in the [marḥešeṯ](../../strongs/h/h4802.md), it shall be ['asah](../../strongs/h/h6213.md) of fine [sōleṯ](../../strongs/h/h5560.md) with [šemen](../../strongs/h/h8081.md).

<a name="leviticus_2_8"></a>Leviticus 2:8

And thou shalt [bow'](../../strongs/h/h935.md) the [minchah](../../strongs/h/h4503.md) that is ['asah](../../strongs/h/h6213.md) of these things unto [Yĕhovah](../../strongs/h/h3068.md): and when it is [qāraḇ](../../strongs/h/h7126.md) unto the [kōhēn](../../strongs/h/h3548.md), he shall [nāḡaš](../../strongs/h/h5066.md) it unto thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_2_9"></a>Leviticus 2:9

And the [kōhēn](../../strongs/h/h3548.md) shall [ruwm](../../strongs/h/h7311.md) from the [minchah](../../strongs/h/h4503.md) an ['azkārâ](../../strongs/h/h234.md) thereof, and shall [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md): it is an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_2_10"></a>Leviticus 2:10

And that which is [yāṯar](../../strongs/h/h3498.md) of the [minchah](../../strongs/h/h4503.md) shall be ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md): it is a thing [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_2_11"></a>Leviticus 2:11

No [minchah](../../strongs/h/h4503.md), which ye shall [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md), shall be ['asah](../../strongs/h/h6213.md) with [ḥāmēṣ](../../strongs/h/h2557.md): for ye shall [qāṭar](../../strongs/h/h6999.md) no [śᵊ'ōr](../../strongs/h/h7603.md), nor any [dĕbash](../../strongs/h/h1706.md), in any ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_2_12"></a>Leviticus 2:12

As for the [qorban](../../strongs/h/h7133.md) of the [re'shiyth](../../strongs/h/h7225.md), ye shall [qāraḇ](../../strongs/h/h7126.md) them unto [Yĕhovah](../../strongs/h/h3068.md): but they shall not be [ʿālâ](../../strongs/h/h5927.md) on thea [mizbeach](../../strongs/h/h4196.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md).

<a name="leviticus_2_13"></a>Leviticus 2:13

And every [qorban](../../strongs/h/h7133.md) of thy [minchah](../../strongs/h/h4503.md) shalt thou [mālaḥ](../../strongs/h/h4414.md) with [melaḥ](../../strongs/h/h4417.md); neither shalt thou suffer the [melaḥ](../../strongs/h/h4417.md) of the [bĕriyth](../../strongs/h/h1285.md) of thy ['Elohiym](../../strongs/h/h430.md) to be [shabath](../../strongs/h/h7673.md) from thy [minchah](../../strongs/h/h4503.md): with all thine [qorban](../../strongs/h/h7133.md) thou shalt [qāraḇ](../../strongs/h/h7126.md) [melaḥ](../../strongs/h/h4417.md).

<a name="leviticus_2_14"></a>Leviticus 2:14

And if thou [qāraḇ](../../strongs/h/h7126.md) a [minchah](../../strongs/h/h4503.md) of thy [bikûr](../../strongs/h/h1061.md) unto [Yĕhovah](../../strongs/h/h3068.md), thou shalt [qāraḇ](../../strongs/h/h7126.md) for the [minchah](../../strongs/h/h4503.md) of thy [bikûr](../../strongs/h/h1061.md) ['āḇîḇ](../../strongs/h/h24.md) [qālâ](../../strongs/h/h7033.md) by the ['esh](../../strongs/h/h784.md), even [gereś](../../strongs/h/h1643.md) out of [karmel](../../strongs/h/h3759.md).

<a name="leviticus_2_15"></a>Leviticus 2:15

And thou shalt [nathan](../../strongs/h/h5414.md) [šemen](../../strongs/h/h8081.md) upon it, and [śûm](../../strongs/h/h7760.md) [lᵊḇônâ](../../strongs/h/h3828.md) thereon: it is a [minchah](../../strongs/h/h4503.md).

<a name="leviticus_2_16"></a>Leviticus 2:16

And the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) the ['azkārâ](../../strongs/h/h234.md) of it, part of the [gereś](../../strongs/h/h1643.md) thereof, and part of the [šemen](../../strongs/h/h8081.md) thereof, with all the [lᵊḇônâ](../../strongs/h/h3828.md) thereof: it is an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 1](leviticus_1.md) - [Leviticus 3](leviticus_3.md)