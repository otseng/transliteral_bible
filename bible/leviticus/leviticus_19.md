# [Leviticus 19](https://www.blueletterbible.org/kjv/lev/19/1/)

<a name="leviticus_19_1"></a>Leviticus 19:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_19_2"></a>Leviticus 19:2

[dabar](../../strongs/h/h1696.md) unto all the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Ye shall be [qadowsh](../../strongs/h/h6918.md): for I [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) am [qadowsh](../../strongs/h/h6918.md).

<a name="leviticus_19_3"></a>Leviticus 19:3

Ye shall [yare'](../../strongs/h/h3372.md) every ['iysh](../../strongs/h/h376.md) his ['em](../../strongs/h/h517.md), and his ['ab](../../strongs/h/h1.md), and [shamar](../../strongs/h/h8104.md) my [shabbath](../../strongs/h/h7676.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_4"></a>Leviticus 19:4

[panah](../../strongs/h/h6437.md) ye not unto ['ĕlîl](../../strongs/h/h457.md), nor ['asah](../../strongs/h/h6213.md) to yourselves [massēḵâ](../../strongs/h/h4541.md) ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_5"></a>Leviticus 19:5

And if ye [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md), ye shall [zabach](../../strongs/h/h2076.md) it at your own [ratsown](../../strongs/h/h7522.md).

<a name="leviticus_19_6"></a>Leviticus 19:6

It shall be ['akal](../../strongs/h/h398.md) the same [yowm](../../strongs/h/h3117.md) ye [zebach](../../strongs/h/h2077.md) it, and on the [māḥŏrāṯ](../../strongs/h/h4283.md): and if ought [yāṯar](../../strongs/h/h3498.md) until the third [yowm](../../strongs/h/h3117.md), it shall be [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md).

<a name="leviticus_19_7"></a>Leviticus 19:7

And if it be ['akal](../../strongs/h/h398.md) at all on the third [yowm](../../strongs/h/h3117.md), it is [pigûl](../../strongs/h/h6292.md); it shall not be [ratsah](../../strongs/h/h7521.md).

<a name="leviticus_19_8"></a>Leviticus 19:8

Therefore every one that ['akal](../../strongs/h/h398.md) it shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md), because he hath [ḥālal](../../strongs/h/h2490.md) the [qodesh](../../strongs/h/h6944.md) of [Yĕhovah](../../strongs/h/h3068.md): and that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from among his ['am](../../strongs/h/h5971.md).

<a name="leviticus_19_9"></a>Leviticus 19:9

And when ye [qāṣar](../../strongs/h/h7114.md) the [qāṣîr](../../strongs/h/h7105.md) of your ['erets](../../strongs/h/h776.md), thou shalt not [kalah](../../strongs/h/h3615.md) the [pē'â](../../strongs/h/h6285.md) of thy [sadeh](../../strongs/h/h7704.md), neither shalt thou [lāqaṭ](../../strongs/h/h3950.md) the [leqeṭ](../../strongs/h/h3951.md) of thy [qāṣîr](../../strongs/h/h7105.md).

<a name="leviticus_19_10"></a>Leviticus 19:10

And thou shalt not [ʿālal](../../strongs/h/h5953.md) thy [kerem](../../strongs/h/h3754.md), neither shalt thou [lāqaṭ](../../strongs/h/h3950.md) every [pereṭ](../../strongs/h/h6528.md) of thy [kerem](../../strongs/h/h3754.md); thou shalt ['azab](../../strongs/h/h5800.md) them for the ['aniy](../../strongs/h/h6041.md) and [ger](../../strongs/h/h1616.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_11"></a>Leviticus 19:11

Ye shall not [ganab](../../strongs/h/h1589.md), neither [kāḥaš](../../strongs/h/h3584.md), neither [šāqar](../../strongs/h/h8266.md) ['iysh](../../strongs/h/h376.md) to [ʿāmîṯ](../../strongs/h/h5997.md).

<a name="leviticus_19_12"></a>Leviticus 19:12

And ye shall not [shaba'](../../strongs/h/h7650.md) by my [shem](../../strongs/h/h8034.md) [sheqer](../../strongs/h/h8267.md), neither shalt thou [ḥālal](../../strongs/h/h2490.md) the [shem](../../strongs/h/h8034.md) of thy ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_13"></a>Leviticus 19:13

Thou shalt not [ʿāšaq](../../strongs/h/h6231.md) thy [rea'](../../strongs/h/h7453.md), neither [gāzal](../../strongs/h/h1497.md) him: the [pe'ullah](../../strongs/h/h6468.md) of him that is [śāḵîr](../../strongs/h/h7916.md) shall not [lûn](../../strongs/h/h3885.md) with thee all night until the [boqer](../../strongs/h/h1242.md).

<a name="leviticus_19_14"></a>Leviticus 19:14

Thou shalt not [qālal](../../strongs/h/h7043.md) the [ḥērēš](../../strongs/h/h2795.md), nor [nathan](../../strongs/h/h5414.md) a [miḵšôl](../../strongs/h/h4383.md) [paniym](../../strongs/h/h6440.md) the [ʿiûēr](../../strongs/h/h5787.md), but shalt [yare'](../../strongs/h/h3372.md) thy ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_15"></a>Leviticus 19:15

Ye shall ['asah](../../strongs/h/h6213.md) no ['evel](../../strongs/h/h5766.md) in [mishpat](../../strongs/h/h4941.md): thou shalt not [nasa'](../../strongs/h/h5375.md) the [paniym](../../strongs/h/h6440.md) of the [dal](../../strongs/h/h1800.md), nor [hāḏar](../../strongs/h/h1921.md) the [paniym](../../strongs/h/h6440.md) of the [gadowl](../../strongs/h/h1419.md): but in [tsedeq](../../strongs/h/h6664.md) shalt thou [shaphat](../../strongs/h/h8199.md) thy [ʿāmîṯ](../../strongs/h/h5997.md).

<a name="leviticus_19_16"></a>Leviticus 19:16

Thou shalt not [yālaḵ](../../strongs/h/h3212.md) up and down as a [rāḵîl](../../strongs/h/h7400.md) among thy ['am](../../strongs/h/h5971.md): neither shalt thou ['amad](../../strongs/h/h5975.md) against the [dam](../../strongs/h/h1818.md) of thy [rea'](../../strongs/h/h7453.md); I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_17"></a>Leviticus 19:17

Thou shalt not [sane'](../../strongs/h/h8130.md) thy ['ach](../../strongs/h/h251.md) in thine [lebab](../../strongs/h/h3824.md): thou shalt in any [yakach](../../strongs/h/h3198.md) [yakach](../../strongs/h/h3198.md) thy [ʿāmîṯ](../../strongs/h/h5997.md), and not [nasa'](../../strongs/h/h5375.md) [ḥēṭĕ'](../../strongs/h/h2399.md) upon him.

<a name="leviticus_19_18"></a>Leviticus 19:18

Thou shalt not [naqam](../../strongs/h/h5358.md), nor [nāṭar](../../strongs/h/h5201.md) against the [ben](../../strongs/h/h1121.md) of thy ['am](../../strongs/h/h5971.md), but thou shalt ['ahab](../../strongs/h/h157.md) thy [rea'](../../strongs/h/h7453.md) as thyself: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_19"></a>Leviticus 19:19

Ye shall [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md). Thou shalt not let thy [bĕhemah](../../strongs/h/h929.md) [rāḇaʿ](../../strongs/h/h7250.md) with a [kil'ayim](../../strongs/h/h3610.md): thou shalt not [zāraʿ](../../strongs/h/h2232.md) thy [sadeh](../../strongs/h/h7704.md) with [kil'ayim](../../strongs/h/h3610.md): neither shall a [beḡeḏ](../../strongs/h/h899.md) [kil'ayim](../../strongs/h/h3610.md) of [šaʿaṭnēz](../../strongs/h/h8162.md) [ʿālâ](../../strongs/h/h5927.md) upon thee.

<a name="leviticus_19_20"></a>Leviticus 19:20

And whosoever [shakab](../../strongs/h/h7901.md) [zera'](../../strongs/h/h2233.md) [šᵊḵāḇâ](../../strongs/h/h7902.md) with an ['ishshah](../../strongs/h/h802.md), that is a [šip̄ḥâ](../../strongs/h/h8198.md), [ḥārap̄](../../strongs/h/h2778.md) to an ['iysh](../../strongs/h/h376.md), and not [pāḏâ](../../strongs/h/h6299.md) [pāḏâ](../../strongs/h/h6299.md), nor [ḥup̄šâ](../../strongs/h/h2668.md) [nathan](../../strongs/h/h5414.md) her; she shall be [biqqōreṯ](../../strongs/h/h1244.md); they shall not be put to [muwth](../../strongs/h/h4191.md), because she was not [ḥāp̄aš](../../strongs/h/h2666.md).

<a name="leviticus_19_21"></a>Leviticus 19:21

And he shall [bow'](../../strongs/h/h935.md) his ['āšām](../../strongs/h/h817.md) unto [Yĕhovah](../../strongs/h/h3068.md), unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), even an ['ayil](../../strongs/h/h352.md) for an ['āšām](../../strongs/h/h817.md).

<a name="leviticus_19_22"></a>Leviticus 19:22

And the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him with the ['ayil](../../strongs/h/h352.md) of the ['āšām](../../strongs/h/h817.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) for his [chatta'ath](../../strongs/h/h2403.md) which he hath [chata'](../../strongs/h/h2398.md): and the [chatta'ath](../../strongs/h/h2403.md) which he hath [chata'](../../strongs/h/h2398.md) shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_19_23"></a>Leviticus 19:23

And when ye shall [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md), and shall have [nāṭaʿ](../../strongs/h/h5193.md) all manner of ['ets](../../strongs/h/h6086.md) for [ma'akal](../../strongs/h/h3978.md), then ye shall [ʿārēl](../../strongs/h/h6188.md) the [pĕriy](../../strongs/h/h6529.md) thereof as [ʿārlâ](../../strongs/h/h6190.md): three [šānâ](../../strongs/h/h8141.md) shall it be as [ʿārēl](../../strongs/h/h6189.md) unto you: it shall not be ['akal](../../strongs/h/h398.md) of.

<a name="leviticus_19_24"></a>Leviticus 19:24

But in the fourth [šānâ](../../strongs/h/h8141.md) all the [pĕriy](../../strongs/h/h6529.md) thereof shall be [qodesh](../../strongs/h/h6944.md) to [hillûl](../../strongs/h/h1974.md) [Yĕhovah](../../strongs/h/h3068.md) withal.

<a name="leviticus_19_25"></a>Leviticus 19:25

And in the fifth [šānâ](../../strongs/h/h8141.md) shall ye ['akal](../../strongs/h/h398.md) of the [pĕriy](../../strongs/h/h6529.md) thereof, that it may [yāsap̄](../../strongs/h/h3254.md) unto you the [tᵊḇû'â](../../strongs/h/h8393.md) thereof: I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_26"></a>Leviticus 19:26

Ye shall not ['akal](../../strongs/h/h398.md) any thing with the [dam](../../strongs/h/h1818.md): neither shall ye use [nāḥaš](../../strongs/h/h5172.md), nor [ʿānan](../../strongs/h/h6049.md).

<a name="leviticus_19_27"></a>Leviticus 19:27

Ye shall not [naqaph](../../strongs/h/h5362.md) the [pē'â](../../strongs/h/h6285.md) of your [ro'sh](../../strongs/h/h7218.md), neither shalt thou [shachath](../../strongs/h/h7843.md) the [pē'â](../../strongs/h/h6285.md) of thy [zāqān](../../strongs/h/h2206.md).

<a name="leviticus_19_28"></a>Leviticus 19:28

Ye shall not [nathan](../../strongs/h/h5414.md) [kᵊṯōḇeṯ](../../strongs/h/h3793.md) [śereṭ](../../strongs/h/h8296.md) in your [basar](../../strongs/h/h1320.md) for the [nephesh](../../strongs/h/h5315.md), nor [nathan](../../strongs/h/h5414.md) any [qaʿăqaʿ](../../strongs/h/h7085.md) upon you: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_29"></a>Leviticus 19:29

Do not [ḥālal](../../strongs/h/h2490.md) thy [bath](../../strongs/h/h1323.md), to cause her to be a [zānâ](../../strongs/h/h2181.md); lest the ['erets](../../strongs/h/h776.md) fall to [zānâ](../../strongs/h/h2181.md), and the ['erets](../../strongs/h/h776.md) become [mālā'](../../strongs/h/h4390.md) of [zimmâ](../../strongs/h/h2154.md).

<a name="leviticus_19_30"></a>Leviticus 19:30

Ye shall [shamar](../../strongs/h/h8104.md) my [shabbath](../../strongs/h/h7676.md), and [yare'](../../strongs/h/h3372.md) my [miqdash](../../strongs/h/h4720.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_31"></a>Leviticus 19:31

[panah](../../strongs/h/h6437.md) not them that have ['ôḇ](../../strongs/h/h178.md), neither [bāqaš](../../strongs/h/h1245.md) after [yiḏʿōnî](../../strongs/h/h3049.md), to be [ṭāmē'](../../strongs/h/h2930.md) by them: I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_32"></a>Leviticus 19:32

Thou shalt [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) the [śêḇâ](../../strongs/h/h7872.md), and [hāḏar](../../strongs/h/h1921.md) the [paniym](../../strongs/h/h6440.md) of the [zāqēn](../../strongs/h/h2205.md), and [yare'](../../strongs/h/h3372.md) thy ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_19_33"></a>Leviticus 19:33

And if a [ger](../../strongs/h/h1616.md) [guwr](../../strongs/h/h1481.md) with thee in your ['erets](../../strongs/h/h776.md), ye shall not [yānâ](../../strongs/h/h3238.md) him.

<a name="leviticus_19_34"></a>Leviticus 19:34

But the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) with you shall be unto you as one ['ezrāḥ](../../strongs/h/h249.md) among you, and thou shalt ['ahab](../../strongs/h/h157.md) him as thyself; for ye were [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_19_35"></a>Leviticus 19:35

Ye shall ['asah](../../strongs/h/h6213.md) no ['evel](../../strongs/h/h5766.md) in [mishpat](../../strongs/h/h4941.md), in [midâ](../../strongs/h/h4060.md), in [mišqāl](../../strongs/h/h4948.md), or in [mᵊśûrâ](../../strongs/h/h4884.md).

<a name="leviticus_19_36"></a>Leviticus 19:36

[tsedeq](../../strongs/h/h6664.md) [mō'znayim](../../strongs/h/h3976.md), [tsedeq](../../strongs/h/h6664.md) ['eben](../../strongs/h/h68.md), a [tsedeq](../../strongs/h/h6664.md) ['êp̄â](../../strongs/h/h374.md), and a [tsedeq](../../strongs/h/h6664.md) [hîn](../../strongs/h/h1969.md), shall ye have: I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="leviticus_19_37"></a>Leviticus 19:37

Therefore shall ye [shamar](../../strongs/h/h8104.md) all my [chuqqah](../../strongs/h/h2708.md), and all my [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them: I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 18](leviticus_18.md) - [Leviticus 20](leviticus_20.md)