# [Leviticus 20](https://www.blueletterbible.org/kjv/lev/20)

<a name="leviticus_20_1"></a>Leviticus 20:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_20_2"></a>Leviticus 20:2

Again, thou shalt ['āmar](../../strongs/h/h559.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['iysh](../../strongs/h/h376.md) be of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) in [Yisra'el](../../strongs/h/h3478.md), that [nathan](../../strongs/h/h5414.md) any of his [zera'](../../strongs/h/h2233.md) unto [mōleḵ](../../strongs/h/h4432.md); he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) shall [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md).

<a name="leviticus_20_3"></a>Leviticus 20:3

And I will [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against that ['iysh](../../strongs/h/h376.md), and will [karath](../../strongs/h/h3772.md) him from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md); because he hath [nathan](../../strongs/h/h5414.md) of his [zera'](../../strongs/h/h2233.md) unto [mōleḵ](../../strongs/h/h4432.md), to [ṭāmē'](../../strongs/h/h2930.md) my [miqdash](../../strongs/h/h4720.md), and to [ḥālal](../../strongs/h/h2490.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md).

<a name="leviticus_20_4"></a>Leviticus 20:4

And if the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md) do any ['alam](../../strongs/h/h5956.md) ['alam](../../strongs/h/h5956.md) their ['ayin](../../strongs/h/h5869.md) from the ['iysh](../../strongs/h/h376.md), when he [nathan](../../strongs/h/h5414.md) of his [zera'](../../strongs/h/h2233.md) unto [mōleḵ](../../strongs/h/h4432.md), and [muwth](../../strongs/h/h4191.md) him not:

<a name="leviticus_20_5"></a>Leviticus 20:5

Then I will [śûm](../../strongs/h/h7760.md) my [paniym](../../strongs/h/h6440.md) against that ['iysh](../../strongs/h/h376.md), and against his [mišpāḥâ](../../strongs/h/h4940.md), and will [karath](../../strongs/h/h3772.md) him, and all that [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) him, to [zānâ](../../strongs/h/h2181.md) with [mōleḵ](../../strongs/h/h4432.md), from [qereḇ](../../strongs/h/h7130.md) their ['am](../../strongs/h/h5971.md).

<a name="leviticus_20_6"></a>Leviticus 20:6

And the [nephesh](../../strongs/h/h5315.md) that [panah](../../strongs/h/h6437.md) ['ēl](../../strongs/h/h413.md) such as have ['ôḇ](../../strongs/h/h178.md), and ['aḥar](../../strongs/h/h310.md) [yiḏʿōnî](../../strongs/h/h3049.md), to [zānâ](../../strongs/h/h2181.md) ['aḥar](../../strongs/h/h310.md) them, I will even [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against that [nephesh](../../strongs/h/h5315.md), and will [karath](../../strongs/h/h3772.md) him from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md).

<a name="leviticus_20_7"></a>Leviticus 20:7

[qadash](../../strongs/h/h6942.md) yourselves therefore, and be ye [qadowsh](../../strongs/h/h6918.md): for I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_20_8"></a>Leviticus 20:8

And ye shall [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md), and ['asah](../../strongs/h/h6213.md) them: I am [Yĕhovah](../../strongs/h/h3068.md) which [qadash](../../strongs/h/h6942.md) you.

<a name="leviticus_20_9"></a>Leviticus 20:9

For ['iysh](../../strongs/h/h376.md) ['iysh](../../strongs/h/h376.md) that [qālal](../../strongs/h/h7043.md) his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): he hath [qālal](../../strongs/h/h7043.md) his ['ab](../../strongs/h/h1.md) or his ['em](../../strongs/h/h517.md); his [dam](../../strongs/h/h1818.md) shall be upon him.

<a name="leviticus_20_10"></a>Leviticus 20:10

And the ['iysh](../../strongs/h/h376.md) that [na'aph](../../strongs/h/h5003.md) with another ['iysh](../../strongs/h/h376.md) ['ishshah](../../strongs/h/h802.md), even he that [na'aph](../../strongs/h/h5003.md) with his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md), the [na'aph](../../strongs/h/h5003.md) and the [na'aph](../../strongs/h/h5003.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="leviticus_20_11"></a>Leviticus 20:11

And the ['iysh](../../strongs/h/h376.md) that [shakab](../../strongs/h/h7901.md) with his ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md) hath [gālâ](../../strongs/h/h1540.md) his ['ab](../../strongs/h/h1.md) [ʿervâ](../../strongs/h/h6172.md): both of them shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); their [dam](../../strongs/h/h1818.md) shall be upon them.

<a name="leviticus_20_12"></a>Leviticus 20:12

And if an ['iysh](../../strongs/h/h376.md) [shakab](../../strongs/h/h7901.md) with his [kallâ](../../strongs/h/h3618.md), both of them shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): they have ['asah](../../strongs/h/h6213.md) [teḇel](../../strongs/h/h8397.md); their [dam](../../strongs/h/h1818.md) shall be upon them.

<a name="leviticus_20_13"></a>Leviticus 20:13

If an ['iysh](../../strongs/h/h376.md) also [shakab](../../strongs/h/h7901.md) with [zāḵār](../../strongs/h/h2145.md), as he [miškāḇ](../../strongs/h/h4904.md) with an ['ishshah](../../strongs/h/h802.md), both of them have ['asah](../../strongs/h/h6213.md) a [tôʿēḇâ](../../strongs/h/h8441.md): they shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); their [dam](../../strongs/h/h1818.md) shall be upon them.

<a name="leviticus_20_14"></a>Leviticus 20:14

And if an ['iysh](../../strongs/h/h376.md) [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) and her ['em](../../strongs/h/h517.md), it is [zimmâ](../../strongs/h/h2154.md): they shall be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md), both he and they; that there be no [zimmâ](../../strongs/h/h2154.md) [tavek](../../strongs/h/h8432.md) you.

<a name="leviticus_20_15"></a>Leviticus 20:15

And if an ['iysh](../../strongs/h/h376.md) [nathan](../../strongs/h/h5414.md) [šᵊḵōḇeṯ](../../strongs/h/h7903.md) with a [bĕhemah](../../strongs/h/h929.md), he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): and ye shall [harag](../../strongs/h/h2026.md) the [bĕhemah](../../strongs/h/h929.md).

<a name="leviticus_20_16"></a>Leviticus 20:16

And if an ['ishshah](../../strongs/h/h802.md) [qāraḇ](../../strongs/h/h7126.md) unto any [bĕhemah](../../strongs/h/h929.md), and [rāḇaʿ](../../strongs/h/h7250.md) thereto, thou shalt [harag](../../strongs/h/h2026.md) the ['ishshah](../../strongs/h/h802.md), and the [bĕhemah](../../strongs/h/h929.md): they shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md); their [dam](../../strongs/h/h1818.md) shall be upon them.

<a name="leviticus_20_17"></a>Leviticus 20:17

And if an ['iysh](../../strongs/h/h376.md) shall [laqach](../../strongs/h/h3947.md) his ['āḥôṯ](../../strongs/h/h269.md), his ['ab](../../strongs/h/h1.md) [bath](../../strongs/h/h1323.md), or his ['em](../../strongs/h/h517.md) [bath](../../strongs/h/h1323.md), and [ra'ah](../../strongs/h/h7200.md) her [ʿervâ](../../strongs/h/h6172.md), and she [ra'ah](../../strongs/h/h7200.md) his [ʿervâ](../../strongs/h/h6172.md); it is a [checed](../../strongs/h/h2617.md); and they shall be [karath](../../strongs/h/h3772.md) in the ['ayin](../../strongs/h/h5869.md) of their ['am](../../strongs/h/h5971.md) [ben](../../strongs/h/h1121.md): he hath [gālâ](../../strongs/h/h1540.md) his ['āḥôṯ](../../strongs/h/h269.md) [ʿervâ](../../strongs/h/h6172.md); he shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md).

<a name="leviticus_20_18"></a>Leviticus 20:18

And if an ['iysh](../../strongs/h/h376.md) shall [shakab](../../strongs/h/h7901.md) with an ['ishshah](../../strongs/h/h802.md) having her [dāvê](../../strongs/h/h1739.md), and shall [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md); he hath [ʿārâ](../../strongs/h/h6168.md) her [māqôr](../../strongs/h/h4726.md), and she hath [gālâ](../../strongs/h/h1540.md) the [māqôr](../../strongs/h/h4726.md) of her [dam](../../strongs/h/h1818.md): and both of them shall be [karath](../../strongs/h/h3772.md) from [qereḇ](../../strongs/h/h7130.md) their ['am](../../strongs/h/h5971.md).

<a name="leviticus_20_19"></a>Leviticus 20:19

And thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy ['em](../../strongs/h/h517.md) ['āḥôṯ](../../strongs/h/h269.md), nor of thy ['ab](../../strongs/h/h1.md) ['āḥôṯ](../../strongs/h/h269.md): for he [ʿārâ](../../strongs/h/h6168.md) his [šᵊ'ēr](../../strongs/h/h7607.md): they shall [nasa'](../../strongs/h/h5375.md) their ['avon](../../strongs/h/h5771.md).

<a name="leviticus_20_20"></a>Leviticus 20:20

And if an ['iysh](../../strongs/h/h376.md) shall [shakab](../../strongs/h/h7901.md) with his [dôḏâ](../../strongs/h/h1733.md), he hath [gālâ](../../strongs/h/h1540.md) his [dôḏ](../../strongs/h/h1730.md) [ʿervâ](../../strongs/h/h6172.md): they shall [nasa'](../../strongs/h/h5375.md) their [ḥēṭĕ'](../../strongs/h/h2399.md); they shall [muwth](../../strongs/h/h4191.md) [ʿărîrî](../../strongs/h/h6185.md).

<a name="leviticus_20_21"></a>Leviticus 20:21

And if an ['iysh](../../strongs/h/h376.md) shall [laqach](../../strongs/h/h3947.md) his ['ach](../../strongs/h/h251.md) ['ishshah](../../strongs/h/h802.md), it is a [nidâ](../../strongs/h/h5079.md): he hath [gālâ](../../strongs/h/h1540.md) his ['ach](../../strongs/h/h251.md) [ʿervâ](../../strongs/h/h6172.md); they shall be [ʿărîrî](../../strongs/h/h6185.md).

<a name="leviticus_20_22"></a>Leviticus 20:22

Ye shall therefore [shamar](../../strongs/h/h8104.md) all my [chuqqah](../../strongs/h/h2708.md), and all my [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them: that the ['erets](../../strongs/h/h776.md), whither I [bow'](../../strongs/h/h935.md) you to [yashab](../../strongs/h/h3427.md) therein, [qî'](../../strongs/h/h6958.md) you not out.

<a name="leviticus_20_23"></a>Leviticus 20:23

And ye shall not [yālaḵ](../../strongs/h/h3212.md) in the [chuqqah](../../strongs/h/h2708.md) of the [gowy](../../strongs/h/h1471.md), which I [shalach](../../strongs/h/h7971.md) [paniym](../../strongs/h/h6440.md) you: for they ['asah](../../strongs/h/h6213.md) all these things, and therefore I [qûṣ](../../strongs/h/h6973.md) them.

<a name="leviticus_20_24"></a>Leviticus 20:24

But I have ['āmar](../../strongs/h/h559.md) unto you, Ye shall [yarash](../../strongs/h/h3423.md) their ['ăḏāmâ](../../strongs/h/h127.md), and I will [nathan](../../strongs/h/h5414.md) it unto you to [yarash](../../strongs/h/h3423.md) it, an ['erets](../../strongs/h/h776.md) that [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which have [bāḏal](../../strongs/h/h914.md) you from other ['am](../../strongs/h/h5971.md).

<a name="leviticus_20_25"></a>Leviticus 20:25

Ye shall therefore [bāḏal](../../strongs/h/h914.md) between [tahowr](../../strongs/h/h2889.md) [bĕhemah](../../strongs/h/h929.md) and [tame'](../../strongs/h/h2931.md), and between [tame'](../../strongs/h/h2931.md) [ʿôp̄](../../strongs/h/h5775.md) and [tahowr](../../strongs/h/h2889.md): and ye shall not make your [nephesh](../../strongs/h/h5315.md) [šāqaṣ](../../strongs/h/h8262.md) by [bĕhemah](../../strongs/h/h929.md), or by [ʿôp̄](../../strongs/h/h5775.md), or by any [rāmaś](../../strongs/h/h7430.md) on the ['adamah](../../strongs/h/h127.md), which I have [bāḏal](../../strongs/h/h914.md) from you as [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_20_26"></a>Leviticus 20:26

And ye shall be [qadowsh](../../strongs/h/h6918.md) unto me: for I [Yĕhovah](../../strongs/h/h3068.md) am [qadowsh](../../strongs/h/h6918.md), and have [bāḏal](../../strongs/h/h914.md) you from other ['am](../../strongs/h/h5971.md), that ye should be mine.

<a name="leviticus_20_27"></a>Leviticus 20:27

An ['iysh](../../strongs/h/h376.md) also or ['ishshah](../../strongs/h/h802.md) that hath a ['ôḇ](../../strongs/h/h178.md), or that is a [yiḏʿōnî](../../strongs/h/h3049.md), shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md): they shall [rāḡam](../../strongs/h/h7275.md) them with ['eben](../../strongs/h/h68.md): their [dam](../../strongs/h/h1818.md) shall be upon them.

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 19](leviticus_19.md) - [Leviticus 21](leviticus_21.md)