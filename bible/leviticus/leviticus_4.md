# [Leviticus 4](https://www.blueletterbible.org/kjv/lev/4)

<a name="leviticus_4_1"></a>Leviticus 4:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_4_2"></a>Leviticus 4:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), If a [nephesh](../../strongs/h/h5315.md) shall [chata'](../../strongs/h/h2398.md) through [šᵊḡāḡâ](../../strongs/h/h7684.md) against any of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) concerning things which ought not to be ['asah](../../strongs/h/h6213.md), and shall ['asah](../../strongs/h/h6213.md) against any of them:

<a name="leviticus_4_3"></a>Leviticus 4:3

If the [kōhēn](../../strongs/h/h3548.md) that is [mashiyach](../../strongs/h/h4899.md) do [chata'](../../strongs/h/h2398.md) according to the ['ašmâ](../../strongs/h/h819.md) of the ['am](../../strongs/h/h5971.md); then let him [qāraḇ](../../strongs/h/h7126.md) for his [chatta'ath](../../strongs/h/h2403.md), which he hath [chata'](../../strongs/h/h2398.md), a [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [par](../../strongs/h/h6499.md) [tamiym](../../strongs/h/h8549.md) unto [Yĕhovah](../../strongs/h/h3068.md) for a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_4_4"></a>Leviticus 4:4

And he shall [bow'](../../strongs/h/h935.md) the [par](../../strongs/h/h6499.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [par](../../strongs/h/h6499.md) [ro'sh](../../strongs/h/h7218.md), and [šāḥaṭ](../../strongs/h/h7819.md) the [par](../../strongs/h/h6499.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_4_5"></a>Leviticus 4:5

And the [kōhēn](../../strongs/h/h3548.md) that is [mashiyach](../../strongs/h/h4899.md) shall [laqach](../../strongs/h/h3947.md) of the [par](../../strongs/h/h6499.md) [dam](../../strongs/h/h1818.md), and [bow'](../../strongs/h/h935.md) it to the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="leviticus_4_6"></a>Leviticus 4:6

And the [kōhēn](../../strongs/h/h3548.md) shall [ṭāḇal](../../strongs/h/h2881.md) his ['etsba'](../../strongs/h/h676.md) in the [dam](../../strongs/h/h1818.md), and [nāzâ](../../strongs/h/h5137.md) of the [dam](../../strongs/h/h1818.md) seven times [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), [paniym](../../strongs/h/h6440.md) the [pārōḵeṯ](../../strongs/h/h6532.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_4_7"></a>Leviticus 4:7

And the [kōhēn](../../strongs/h/h3548.md) shall [nathan](../../strongs/h/h5414.md) some of the [dam](../../strongs/h/h1818.md) upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) of [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), which is in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md); and shall [šāp̄aḵ](../../strongs/h/h8210.md) all the [dam](../../strongs/h/h1818.md) of the [par](../../strongs/h/h6499.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md), which is at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_4_8"></a>Leviticus 4:8

And he shall [ruwm](../../strongs/h/h7311.md) from it all the [cheleb](../../strongs/h/h2459.md) of the [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md); the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and all the [cheleb](../../strongs/h/h2459.md) that is upon the [qereḇ](../../strongs/h/h7130.md),

<a name="leviticus_4_9"></a>Leviticus 4:9

And the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is upon them, which is by the [kesel](../../strongs/h/h3689.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), with the [kilyah](../../strongs/h/h3629.md), it shall he [cuwr](../../strongs/h/h5493.md),

<a name="leviticus_4_10"></a>Leviticus 4:10

As it was [ruwm](../../strongs/h/h7311.md) from the [showr](../../strongs/h/h7794.md) of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md): and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) them upon the [mizbeach](../../strongs/h/h4196.md) of the [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_4_11"></a>Leviticus 4:11

And the ['owr](../../strongs/h/h5785.md) of the [par](../../strongs/h/h6499.md), and all his [basar](../../strongs/h/h1320.md), with his [ro'sh](../../strongs/h/h7218.md), and with his [keraʿ](../../strongs/h/h3767.md), and his [qereḇ](../../strongs/h/h7130.md), and his [pereš](../../strongs/h/h6569.md),

<a name="leviticus_4_12"></a>Leviticus 4:12

Even the whole [par](../../strongs/h/h6499.md) shall he [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) unto a [tahowr](../../strongs/h/h2889.md) [maqowm](../../strongs/h/h4725.md), where the [dešen](../../strongs/h/h1880.md) are [šep̄eḵ](../../strongs/h/h8211.md), and [śārap̄](../../strongs/h/h8313.md) him on the ['ets](../../strongs/h/h6086.md) with ['esh](../../strongs/h/h784.md): where the [dešen](../../strongs/h/h1880.md) are [šep̄eḵ](../../strongs/h/h8211.md) shall he be [śārap̄](../../strongs/h/h8313.md).

<a name="leviticus_4_13"></a>Leviticus 4:13

And if the whole ['edah](../../strongs/h/h5712.md) of [Yisra'el](../../strongs/h/h3478.md) [šāḡâ](../../strongs/h/h7686.md), and the [dabar](../../strongs/h/h1697.md) be ['alam](../../strongs/h/h5956.md) from the ['ayin](../../strongs/h/h5869.md) of the [qāhēl](../../strongs/h/h6951.md), and they have ['asah](../../strongs/h/h6213.md) somewhat against any of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) concerning things which should not be ['asah](../../strongs/h/h6213.md), and are ['asham](../../strongs/h/h816.md);

<a name="leviticus_4_14"></a>Leviticus 4:14

When the [chatta'ath](../../strongs/h/h2403.md), which they have [chata'](../../strongs/h/h2398.md) against it, is [yada'](../../strongs/h/h3045.md), then the [qāhēl](../../strongs/h/h6951.md) shall [qāraḇ](../../strongs/h/h7126.md) a [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md), and [bow'](../../strongs/h/h935.md) him [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_4_15"></a>Leviticus 4:15

And the [zāqēn](../../strongs/h/h2205.md) of the ['edah](../../strongs/h/h5712.md) shall [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [par](../../strongs/h/h6499.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and the [par](../../strongs/h/h6499.md) shall be [šāḥaṭ](../../strongs/h/h7819.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_4_16"></a>Leviticus 4:16

And the [kōhēn](../../strongs/h/h3548.md) that is [mashiyach](../../strongs/h/h4899.md) shall [bow'](../../strongs/h/h935.md) of the [par](../../strongs/h/h6499.md) [dam](../../strongs/h/h1818.md) to the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="leviticus_4_17"></a>Leviticus 4:17

And the [kōhēn](../../strongs/h/h3548.md) shall [ṭāḇal](../../strongs/h/h2881.md) his ['etsba'](../../strongs/h/h676.md) in some of the [dam](../../strongs/h/h1818.md), and [nāzâ](../../strongs/h/h5137.md) it seven [pa'am](../../strongs/h/h6471.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), even [paniym](../../strongs/h/h6440.md) the [pārōḵeṯ](../../strongs/h/h6532.md).

<a name="leviticus_4_18"></a>Leviticus 4:18

And he shall [nathan](../../strongs/h/h5414.md) some of the [dam](../../strongs/h/h1818.md) upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) which is [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), that is in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and shall [šāp̄aḵ](../../strongs/h/h8210.md) all the [dam](../../strongs/h/h1818.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md) of the an [ʿōlâ](../../strongs/h/h5930.md), which is at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_4_19"></a>Leviticus 4:19

And he shall [ruwm](../../strongs/h/h7311.md) all his [cheleb](../../strongs/h/h2459.md) from him, and [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_4_20"></a>Leviticus 4:20

And he shall ['asah](../../strongs/h/h6213.md) with the [par](../../strongs/h/h6499.md) as he ['asah](../../strongs/h/h6213.md) with the [par](../../strongs/h/h6499.md) for a [chatta'ath](../../strongs/h/h2403.md), so shall he ['asah](../../strongs/h/h6213.md) with this: and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for them, and it shall be [sālaḥ](../../strongs/h/h5545.md) them.

<a name="leviticus_4_21"></a>Leviticus 4:21

And he shall [yāṣā'](../../strongs/h/h3318.md) the [par](../../strongs/h/h6499.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md), and [śārap̄](../../strongs/h/h8313.md) him as he [śārap̄](../../strongs/h/h8313.md) the [ri'šôn](../../strongs/h/h7223.md) [par](../../strongs/h/h6499.md): it is a [chatta'ath](../../strongs/h/h2403.md) for the [qāhēl](../../strongs/h/h6951.md).

<a name="leviticus_4_22"></a>Leviticus 4:22

When a [nāśî'](../../strongs/h/h5387.md) hath [chata'](../../strongs/h/h2398.md), and ['asah](../../strongs/h/h6213.md) somewhat through [šᵊḡāḡâ](../../strongs/h/h7684.md) against any of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) his ['Elohiym](../../strongs/h/h430.md) concerning things which should not be ['asah](../../strongs/h/h6213.md), and is ['asham](../../strongs/h/h816.md);

<a name="leviticus_4_23"></a>Leviticus 4:23

Or if his [chatta'ath](../../strongs/h/h2403.md), wherein he hath [chata'](../../strongs/h/h2398.md), come to his [yada'](../../strongs/h/h3045.md); he shall [bow'](../../strongs/h/h935.md) his [qorban](../../strongs/h/h7133.md), a [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md), a [zāḵār](../../strongs/h/h2145.md) [tamiym](../../strongs/h/h8549.md):

<a name="leviticus_4_24"></a>Leviticus 4:24

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [śāʿîr](../../strongs/h/h8163.md), and [šāḥaṭ](../../strongs/h/h7819.md) it in the [maqowm](../../strongs/h/h4725.md) where they [šāḥaṭ](../../strongs/h/h7819.md) the an [ʿōlâ](../../strongs/h/h5930.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): it is a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_4_25"></a>Leviticus 4:25

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [chatta'ath](../../strongs/h/h2403.md) with his ['etsba'](../../strongs/h/h676.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) of an [ʿōlâ](../../strongs/h/h5930.md), and shall [šāp̄aḵ](../../strongs/h/h8210.md) his [dam](../../strongs/h/h1818.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md) of an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_4_26"></a>Leviticus 4:26

And he shall [qāṭar](../../strongs/h/h6999.md) all his [cheleb](../../strongs/h/h2459.md) upon thea [mizbeach](../../strongs/h/h4196.md), as the [cheleb](../../strongs/h/h2459.md) of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him as concerning his [chatta'ath](../../strongs/h/h2403.md), and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_4_27"></a>Leviticus 4:27

And if any [nephesh](../../strongs/h/h5315.md) of the ['erets](../../strongs/h/h776.md) ['am](../../strongs/h/h5971.md) [chata'](../../strongs/h/h2398.md) [šᵊḡāḡâ](../../strongs/h/h7684.md), while he ['asah](../../strongs/h/h6213.md) somewhat against any of the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md) concerning things which ought not to be ['asah](../../strongs/h/h6213.md), and be ['asham](../../strongs/h/h816.md);

<a name="leviticus_4_28"></a>Leviticus 4:28

Or if his [chatta'ath](../../strongs/h/h2403.md), which he hath [chata'](../../strongs/h/h2398.md), come to his [yada'](../../strongs/h/h3045.md): then he shall [bow'](../../strongs/h/h935.md) his [qorban](../../strongs/h/h7133.md), a [śᵊʿîrâ](../../strongs/h/h8166.md) of the [ʿēz](../../strongs/h/h5795.md), a [nᵊqēḇâ](../../strongs/h/h5347.md) [tamiym](../../strongs/h/h8549.md), for his [chatta'ath](../../strongs/h/h2403.md) which he hath [chata'](../../strongs/h/h2398.md).

<a name="leviticus_4_29"></a>Leviticus 4:29

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [chatta'ath](../../strongs/h/h2403.md), and [šāḥaṭ](../../strongs/h/h7819.md) the [chatta'ath](../../strongs/h/h2403.md) in the [maqowm](../../strongs/h/h4725.md) of the an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_4_30"></a>Leviticus 4:30

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) thereof with his ['etsba'](../../strongs/h/h676.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) of an [ʿōlâ](../../strongs/h/h5930.md), and shall [šāp̄aḵ](../../strongs/h/h8210.md) all the [dam](../../strongs/h/h1818.md) thereof at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_4_31"></a>Leviticus 4:31

And he shall take[cuwr](../../strongs/h/h5493.md) all the [cheleb](../../strongs/h/h2459.md) thereof, as the [cheleb](../../strongs/h/h2459.md) is taken[cuwr](../../strongs/h/h5493.md) from off the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md); and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md); and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him, and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_4_32"></a>Leviticus 4:32

And if he [bow'](../../strongs/h/h935.md) a [keḇeś](../../strongs/h/h3532.md) for a [chatta'ath](../../strongs/h/h2403.md) [qorban](../../strongs/h/h7133.md), he shall [bow'](../../strongs/h/h935.md) it a [nᵊqēḇâ](../../strongs/h/h5347.md) [tamiym](../../strongs/h/h8549.md).

<a name="leviticus_4_33"></a>Leviticus 4:33

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [chatta'ath](../../strongs/h/h2403.md), and [šāḥaṭ](../../strongs/h/h7819.md) it for a [chatta'ath](../../strongs/h/h2403.md) in the [maqowm](../../strongs/h/h4725.md) where they [šāḥaṭ](../../strongs/h/h7819.md) the an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_4_34"></a>Leviticus 4:34

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [chatta'ath](../../strongs/h/h2403.md) with his ['etsba'](../../strongs/h/h676.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of thea [mizbeach](../../strongs/h/h4196.md) of an [ʿōlâ](../../strongs/h/h5930.md), and shall [šāp̄aḵ](../../strongs/h/h8210.md) all the [dam](../../strongs/h/h1818.md) thereof at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_4_35"></a>Leviticus 4:35

And he shall take [cuwr](../../strongs/h/h5493.md) all the [cheleb](../../strongs/h/h2459.md) thereof, as the [cheleb](../../strongs/h/h2459.md) of the [keśeḇ](../../strongs/h/h3775.md) is taken [cuwr](../../strongs/h/h5493.md) from the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md); and the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) them upon the [mizbeach](../../strongs/h/h4196.md), according to the ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for his [chatta'ath](../../strongs/h/h2403.md) that he hath [chata'](../../strongs/h/h2398.md), and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 3](leviticus_3.md) - [Leviticus 5](leviticus_5.md)