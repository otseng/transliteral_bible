# [Leviticus 22](https://www.blueletterbible.org/kjv/lev/22)

<a name="leviticus_22_1"></a>Leviticus 22:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_22_2"></a>Leviticus 22:2

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md), that they [nāzar](../../strongs/h/h5144.md) themselves from the [qodesh](../../strongs/h/h6944.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and that they [ḥālal](../../strongs/h/h2490.md) not my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md) in those things which they [qadash](../../strongs/h/h6942.md) unto me: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_3"></a>Leviticus 22:3

['āmar](../../strongs/h/h559.md) unto them, ['iysh](../../strongs/h/h376.md) he be of all your [zera'](../../strongs/h/h2233.md) among your [dôr](../../strongs/h/h1755.md), that [qāraḇ](../../strongs/h/h7126.md) unto the [qodesh](../../strongs/h/h6944.md), which the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md), having his [ṭām'â](../../strongs/h/h2932.md) upon him, that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from my [paniym](../../strongs/h/h6440.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_4"></a>Leviticus 22:4

What ['iysh](../../strongs/h/h376.md) of the [zera'](../../strongs/h/h2233.md) of ['Ahărôn](../../strongs/h/h175.md) is a [ṣāraʿ](../../strongs/h/h6879.md), or hath a [zûḇ](../../strongs/h/h2100.md); he shall not ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md), until he be [ṭāhēr](../../strongs/h/h2891.md). And whoso [naga'](../../strongs/h/h5060.md) any thing that is [tame'](../../strongs/h/h2931.md) by the [nephesh](../../strongs/h/h5315.md), or an ['iysh](../../strongs/h/h376.md) whose [zera'](../../strongs/h/h2233.md) [šᵊḵāḇâ](../../strongs/h/h7902.md) [yāṣā'](../../strongs/h/h3318.md) from him;

<a name="leviticus_22_5"></a>Leviticus 22:5

Or ['iysh](../../strongs/h/h376.md) [naga'](../../strongs/h/h5060.md) any [šereṣ](../../strongs/h/h8318.md), whereby he may be made [ṭāmē'](../../strongs/h/h2930.md), or an ['adam](../../strongs/h/h120.md) of whom he may take [ṭāmē'](../../strongs/h/h2930.md), whatsoever [ṭām'â](../../strongs/h/h2932.md) he hath;

<a name="leviticus_22_6"></a>Leviticus 22:6

The [nephesh](../../strongs/h/h5315.md) which hath [naga'](../../strongs/h/h5060.md) any such shall be [ṭāmē'](../../strongs/h/h2930.md) until ['ereb](../../strongs/h/h6153.md), and shall not ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md), unless he [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) with [mayim](../../strongs/h/h4325.md).

<a name="leviticus_22_7"></a>Leviticus 22:7

And when the [šemeš](../../strongs/h/h8121.md) is [bow'](../../strongs/h/h935.md), he shall be [ṭāhēr](../../strongs/h/h2891.md), and shall ['aḥar](../../strongs/h/h310.md) ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md); because it is his [lechem](../../strongs/h/h3899.md).

<a name="leviticus_22_8"></a>Leviticus 22:8

That which [nᵊḇēlâ](../../strongs/h/h5038.md), or is [ṭᵊrēp̄â](../../strongs/h/h2966.md), he shall not ['akal](../../strongs/h/h398.md) to [ṭāmē'](../../strongs/h/h2930.md) himself therewith; I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_9"></a>Leviticus 22:9

They shall therefore [shamar](../../strongs/h/h8104.md) mine [mišmereṯ](../../strongs/h/h4931.md), lest they [nasa'](../../strongs/h/h5375.md) [ḥēṭĕ'](../../strongs/h/h2399.md) for it, and [muwth](../../strongs/h/h4191.md) therefore, if they [ḥālal](../../strongs/h/h2490.md) it: I [Yĕhovah](../../strongs/h/h3068.md) do [qadash](../../strongs/h/h6942.md) them.

<a name="leviticus_22_10"></a>Leviticus 22:10

There shall no [zûr](../../strongs/h/h2114.md) ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md): a [tôšāḇ](../../strongs/h/h8453.md) of the [kōhēn](../../strongs/h/h3548.md), or a [śāḵîr](../../strongs/h/h7916.md), shall not ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_22_11"></a>Leviticus 22:11

But if the [kōhēn](../../strongs/h/h3548.md) [qānâ](../../strongs/h/h7069.md) any [nephesh](../../strongs/h/h5315.md) [qinyān](../../strongs/h/h7075.md) his [keceph](../../strongs/h/h3701.md), he shall ['akal](../../strongs/h/h398.md) of it, and he that is [yālîḏ](../../strongs/h/h3211.md) in his [bayith](../../strongs/h/h1004.md): they shall ['akal](../../strongs/h/h398.md) of his [lechem](../../strongs/h/h3899.md).

<a name="leviticus_22_12"></a>Leviticus 22:12

If the [kōhēn](../../strongs/h/h3548.md) [bath](../../strongs/h/h1323.md) also ['iysh](../../strongs/h/h376.md) [zûr](../../strongs/h/h2114.md), she may not ['akal](../../strongs/h/h398.md) of a [tᵊrûmâ](../../strongs/h/h8641.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_22_13"></a>Leviticus 22:13

But if the [kōhēn](../../strongs/h/h3548.md) [bath](../../strongs/h/h1323.md) be an ['almānâ](../../strongs/h/h490.md), or [gāraš](../../strongs/h/h1644.md), and have no [zera'](../../strongs/h/h2233.md), and is [shuwb](../../strongs/h/h7725.md) unto her ['ab](../../strongs/h/h1.md) [bayith](../../strongs/h/h1004.md), as in her [nāʿur](../../strongs/h/h5271.md), she shall ['akal](../../strongs/h/h398.md) of her ['ab](../../strongs/h/h1.md) [lechem](../../strongs/h/h3899.md): but there shall be no [zûr](../../strongs/h/h2114.md) ['akal](../../strongs/h/h398.md) thereof.

<a name="leviticus_22_14"></a>Leviticus 22:14

And if an ['iysh](../../strongs/h/h376.md) ['akal](../../strongs/h/h398.md) of the [qodesh](../../strongs/h/h6944.md) [šᵊḡāḡâ](../../strongs/h/h7684.md), then he shall [yāsap̄](../../strongs/h/h3254.md) the [ḥămîšî](../../strongs/h/h2549.md) thereof unto it, and shall [nathan](../../strongs/h/h5414.md) it unto the [kōhēn](../../strongs/h/h3548.md) with the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_22_15"></a>Leviticus 22:15

And they shall not [ḥālal](../../strongs/h/h2490.md) the [qodesh](../../strongs/h/h6944.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), which they [ruwm](../../strongs/h/h7311.md) unto [Yĕhovah](../../strongs/h/h3068.md);

<a name="leviticus_22_16"></a>Leviticus 22:16

Or suffer them to [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of ['ašmâ](../../strongs/h/h819.md), when they ['akal](../../strongs/h/h398.md) their [qodesh](../../strongs/h/h6944.md): for I [Yĕhovah](../../strongs/h/h3068.md) do [qadash](../../strongs/h/h6942.md) them.

<a name="leviticus_22_17"></a>Leviticus 22:17

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_22_18"></a>Leviticus 22:18

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), and to his [ben](../../strongs/h/h1121.md), and unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, ['iysh](../../strongs/h/h376.md) be of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), or of the [ger](../../strongs/h/h1616.md) in [Yisra'el](../../strongs/h/h3478.md), that will [qāraḇ](../../strongs/h/h7126.md) his [qorban](../../strongs/h/h7133.md) for all his [neḏer](../../strongs/h/h5088.md), and for all his [nᵊḏāḇâ](../../strongs/h/h5071.md), which they will [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md) for an [ʿōlâ](../../strongs/h/h5930.md);

<a name="leviticus_22_19"></a>Leviticus 22:19

Ye shall offer at your own [ratsown](../../strongs/h/h7522.md) a [zāḵār](../../strongs/h/h2145.md) [tamiym](../../strongs/h/h8549.md), of the [bāqār](../../strongs/h/h1241.md), of the [keśeḇ](../../strongs/h/h3775.md), or of the [ʿēz](../../strongs/h/h5795.md).

<a name="leviticus_22_20"></a>Leviticus 22:20

But whatsoever hath a [mᵊ'ûm](../../strongs/h/h3971.md), that shall ye not [qāraḇ](../../strongs/h/h7126.md): for it shall not be [ratsown](../../strongs/h/h7522.md) for you.

<a name="leviticus_22_21"></a>Leviticus 22:21

And ['iysh](../../strongs/h/h376.md) [qāraḇ](../../strongs/h/h7126.md) a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md) to [pala'](../../strongs/h/h6381.md) his [neḏer](../../strongs/h/h5088.md), or a [nᵊḏāḇâ](../../strongs/h/h5071.md) in [bāqār](../../strongs/h/h1241.md) or [tso'n](../../strongs/h/h6629.md), it shall be [tamiym](../../strongs/h/h8549.md) to be [ratsown](../../strongs/h/h7522.md); there shall be no [mᵊ'ûm](../../strongs/h/h3971.md) therein.

<a name="leviticus_22_22"></a>Leviticus 22:22

[ʿiûārôn](../../strongs/h/h5788.md), or [shabar](../../strongs/h/h7665.md), or [ḥāraṣ](../../strongs/h/h2782.md), or having a [yabāl](../../strongs/h/h2990.md), or [gārāḇ](../../strongs/h/h1618.md), or [yallep̄eṯ](../../strongs/h/h3217.md), ye shall not [qāraḇ](../../strongs/h/h7126.md) these unto [Yĕhovah](../../strongs/h/h3068.md), nor [nathan](../../strongs/h/h5414.md) an ['iššê](../../strongs/h/h801.md) of them upon the [mizbeach](../../strongs/h/h4196.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_23"></a>Leviticus 22:23

Either a [showr](../../strongs/h/h7794.md) or a [śê](../../strongs/h/h7716.md) that hath any thing [śāraʿ](../../strongs/h/h8311.md) or [qālaṭ](../../strongs/h/h7038.md), that mayest thou ['asah](../../strongs/h/h6213.md) for a [nᵊḏāḇâ](../../strongs/h/h5071.md); but for a [neḏer](../../strongs/h/h5088.md) it shall not be [ratsah](../../strongs/h/h7521.md).

<a name="leviticus_22_24"></a>Leviticus 22:24

Ye shall not [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md) that which is [māʿaḵ](../../strongs/h/h4600.md), or [kāṯaṯ](../../strongs/h/h3807.md), or [nathaq](../../strongs/h/h5423.md), or [karath](../../strongs/h/h3772.md); neither shall ye ['asah](../../strongs/h/h6213.md) any offering thereof in your ['erets](../../strongs/h/h776.md).

<a name="leviticus_22_25"></a>Leviticus 22:25

Neither from a [nēḵār](../../strongs/h/h5236.md) [yad](../../strongs/h/h3027.md) [ben](../../strongs/h/h1121.md) shall ye [qāraḇ](../../strongs/h/h7126.md) the [lechem](../../strongs/h/h3899.md) of your ['Elohiym](../../strongs/h/h430.md) of any of these; because their [mišḥāṯ](../../strongs/h/h4893.md) is in them, and [mᵊ'ûm](../../strongs/h/h3971.md) be in them: they shall not be [ratsah](../../strongs/h/h7521.md) for you.

<a name="leviticus_22_26"></a>Leviticus 22:26

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_22_27"></a>Leviticus 22:27

When a [showr](../../strongs/h/h7794.md), or a [keśeḇ](../../strongs/h/h3775.md), or an [ʿēz](../../strongs/h/h5795.md), is [yalad](../../strongs/h/h3205.md), then it shall be seven [yowm](../../strongs/h/h3117.md) under the ['em](../../strongs/h/h517.md); and from the eighth [yowm](../../strongs/h/h3117.md) and [hāl'â](../../strongs/h/h1973.md) it shall be [ratsah](../../strongs/h/h7521.md) for a [qorban](../../strongs/h/h7133.md) ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_28"></a>Leviticus 22:28

And whether it be [showr](../../strongs/h/h7794.md), or [śê](../../strongs/h/h7716.md), ye shall not [šāḥaṭ](../../strongs/h/h7819.md) it and her [ben](../../strongs/h/h1121.md) both in one [yowm](../../strongs/h/h3117.md).

<a name="leviticus_22_29"></a>Leviticus 22:29

And when ye will [zabach](../../strongs/h/h2076.md) a [zebach](../../strongs/h/h2077.md) of [tôḏâ](../../strongs/h/h8426.md) unto [Yĕhovah](../../strongs/h/h3068.md), [zabach](../../strongs/h/h2076.md) it at your own [ratsown](../../strongs/h/h7522.md).

<a name="leviticus_22_30"></a>Leviticus 22:30

On the same [yowm](../../strongs/h/h3117.md) it shall be ['akal](../../strongs/h/h398.md) up; ye shall [yāṯar](../../strongs/h/h3498.md) none of it until the [boqer](../../strongs/h/h1242.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_31"></a>Leviticus 22:31

Therefore shall ye [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and ['asah](../../strongs/h/h6213.md) them: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_22_32"></a>Leviticus 22:32

Neither shall ye [ḥālal](../../strongs/h/h2490.md) my [qodesh](../../strongs/h/h6944.md) [shem](../../strongs/h/h8034.md); but I will be [qadash](../../strongs/h/h6942.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): I am [Yĕhovah](../../strongs/h/h3068.md) which [qadash](../../strongs/h/h6942.md) you,

<a name="leviticus_22_33"></a>Leviticus 22:33

That [yāṣā'](../../strongs/h/h3318.md) you of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), to be your ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 21](leviticus_21.md) - [Leviticus 23](leviticus_23.md)