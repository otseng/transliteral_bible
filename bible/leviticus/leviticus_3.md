# [Leviticus 3](https://www.blueletterbible.org/kjv/lev/3)

<a name="leviticus_3_1"></a>Leviticus 3:1

And if his [qorban](../../strongs/h/h7133.md) be a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), if he [qāraḇ](../../strongs/h/h7126.md) it of the [bāqār](../../strongs/h/h1241.md); whether it be a [zāḵār](../../strongs/h/h2145.md) or [nᵊqēḇâ](../../strongs/h/h5347.md), he shall [qāraḇ](../../strongs/h/h7126.md) it [tamiym](../../strongs/h/h8549.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_2"></a>Leviticus 3:2

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of his [qorban](../../strongs/h/h7133.md), and [šāḥaṭ](../../strongs/h/h7819.md) it at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) the [kōhēn](../../strongs/h/h3548.md) shall [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon thea [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="leviticus_3_3"></a>Leviticus 3:3

And he shall [qāraḇ](../../strongs/h/h7126.md) of the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md); the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and all the [cheleb](../../strongs/h/h2459.md) that is upon the [qereḇ](../../strongs/h/h7130.md),

<a name="leviticus_3_4"></a>Leviticus 3:4

And the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is on them, which is by the [kesel](../../strongs/h/h3689.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), with the [kilyah](../../strongs/h/h3629.md), it shall he [cuwr](../../strongs/h/h5493.md).

<a name="leviticus_3_5"></a>Leviticus 3:5

And ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) shall [qāṭar](../../strongs/h/h6999.md) it on thea [mizbeach](../../strongs/h/h4196.md) upon the [ʿōlâ](../../strongs/h/h5930.md), which is upon the ['ets](../../strongs/h/h6086.md) that is on the ['esh](../../strongs/h/h784.md): it is an ['iššê](../../strongs/h/h801.md), of a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_6"></a>Leviticus 3:6

And if his [qorban](../../strongs/h/h7133.md) for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md) be of the [tso'n](../../strongs/h/h6629.md); [zāḵār](../../strongs/h/h2145.md) or [nᵊqēḇâ](../../strongs/h/h5347.md), he shall [qāraḇ](../../strongs/h/h7126.md) it [tamiym](../../strongs/h/h8549.md).

<a name="leviticus_3_7"></a>Leviticus 3:7

If he [qāraḇ](../../strongs/h/h7126.md) a [keśeḇ](../../strongs/h/h3775.md) for his [qorban](../../strongs/h/h7133.md), then shall he [qāraḇ](../../strongs/h/h7126.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_8"></a>Leviticus 3:8

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of his [qorban](../../strongs/h/h7133.md), and [šāḥaṭ](../../strongs/h/h7819.md) it [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) shall [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) thereof [cabiyb](../../strongs/h/h5439.md) upon thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_3_9"></a>Leviticus 3:9

And he shall [qāraḇ](../../strongs/h/h7126.md) of the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md); the [cheleb](../../strongs/h/h2459.md) thereof, and the [tamiym](../../strongs/h/h8549.md) ['alyâ](../../strongs/h/h451.md), it shall he [cuwr](../../strongs/h/h5493.md) [ʿummâ](../../strongs/h/h5980.md) by the [ʿāṣê](../../strongs/h/h6096.md); and the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and all the [cheleb](../../strongs/h/h2459.md) that is upon the [qereḇ](../../strongs/h/h7130.md),

<a name="leviticus_3_10"></a>Leviticus 3:10

And the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is upon them, which is by the [kesel](../../strongs/h/h3689.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), with the [kilyah](../../strongs/h/h3629.md), it shall he [cuwr](../../strongs/h/h5493.md).

<a name="leviticus_3_11"></a>Leviticus 3:11

And the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md): it is the [lechem](../../strongs/h/h3899.md) of the ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_12"></a>Leviticus 3:12

And if his [qorban](../../strongs/h/h7133.md) be an [ʿēz](../../strongs/h/h5795.md), then he shall [qāraḇ](../../strongs/h/h7126.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_13"></a>Leviticus 3:13

And he shall [camak](../../strongs/h/h5564.md) his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of it, and [šāḥaṭ](../../strongs/h/h7819.md) it [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) shall [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) thereof upon thea [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="leviticus_3_14"></a>Leviticus 3:14

And he shall [qāraḇ](../../strongs/h/h7126.md) thereof his [qorban](../../strongs/h/h7133.md), even an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md); the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md), and all the [cheleb](../../strongs/h/h2459.md) that is upon the [qereḇ](../../strongs/h/h7130.md),

<a name="leviticus_3_15"></a>Leviticus 3:15

And the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is upon them, which is by the [kesel](../../strongs/h/h3689.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), with the [kilyah](../../strongs/h/h3629.md), it shall he [cuwr](../../strongs/h/h5493.md).

<a name="leviticus_3_16"></a>Leviticus 3:16

And the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) them upon thea [mizbeach](../../strongs/h/h4196.md): it is the [lechem](../../strongs/h/h3899.md) of the ['iššê](../../strongs/h/h801.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md): all the [cheleb](../../strongs/h/h2459.md) is [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_3_17"></a>Leviticus 3:17

It shall be a ['owlam](../../strongs/h/h5769.md) [chuqqah](../../strongs/h/h2708.md) for your [dôr](../../strongs/h/h1755.md) throughout all your [môšāḇ](../../strongs/h/h4186.md), that ye ['akal](../../strongs/h/h398.md) neither [cheleb](../../strongs/h/h2459.md) nor [dam](../../strongs/h/h1818.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 2](leviticus_2.md) - [Leviticus 4](leviticus_4.md)