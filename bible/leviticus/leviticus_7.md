# [Leviticus 7](https://www.blueletterbible.org/kjv/lev/7)

<a name="leviticus_7_1"></a>Leviticus 7:1

Likewise this is the [towrah](../../strongs/h/h8451.md) of the ['āšām](../../strongs/h/h817.md): it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_7_2"></a>Leviticus 7:2

In the [maqowm](../../strongs/h/h4725.md) where they [šāḥaṭ](../../strongs/h/h7819.md) the an [ʿōlâ](../../strongs/h/h5930.md) shall they [šāḥaṭ](../../strongs/h/h7819.md) the ['āšām](../../strongs/h/h817.md): and the [dam](../../strongs/h/h1818.md) thereof shall he [zāraq](../../strongs/h/h2236.md) [cabiyb](../../strongs/h/h5439.md) upon the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_7_3"></a>Leviticus 7:3

And he shall [qāraḇ](../../strongs/h/h7126.md) of it all the [cheleb](../../strongs/h/h2459.md) thereof; the ['alyâ](../../strongs/h/h451.md), and the [cheleb](../../strongs/h/h2459.md) that [kāsâ](../../strongs/h/h3680.md) the [qereḇ](../../strongs/h/h7130.md),

<a name="leviticus_7_4"></a>Leviticus 7:4

And the two [kilyah](../../strongs/h/h3629.md), and the [cheleb](../../strongs/h/h2459.md) that is on them, which is by the [kesel](../../strongs/h/h3689.md), and the [yōṯereṯ](../../strongs/h/h3508.md) that is above the [kāḇēḏ](../../strongs/h/h3516.md), with the [kilyah](../../strongs/h/h3629.md), it shall he [cuwr](../../strongs/h/h5493.md):

<a name="leviticus_7_5"></a>Leviticus 7:5

And the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) them upon the [mizbeach](../../strongs/h/h4196.md) for an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md): it is an ['āšām](../../strongs/h/h817.md).

<a name="leviticus_7_6"></a>Leviticus 7:6

Every [zāḵār](../../strongs/h/h2145.md) among the [kōhēn](../../strongs/h/h3548.md) shall ['akal](../../strongs/h/h398.md) thereof: it shall be ['akal](../../strongs/h/h398.md) in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md): it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_7_7"></a>Leviticus 7:7

As the [chatta'ath](../../strongs/h/h2403.md) is, so is the ['āšām](../../strongs/h/h817.md): there is one [towrah](../../strongs/h/h8451.md) for them: the [kōhēn](../../strongs/h/h3548.md) that [kāp̄ar](../../strongs/h/h3722.md) therewith shall have it.

<a name="leviticus_7_8"></a>Leviticus 7:8

And the [kōhēn](../../strongs/h/h3548.md) that [qāraḇ](../../strongs/h/h7126.md) any ['iysh](../../strongs/h/h376.md) an [ʿōlâ](../../strongs/h/h5930.md), even the [kōhēn](../../strongs/h/h3548.md) shall have to himself the ['owr](../../strongs/h/h5785.md) of the an [ʿōlâ](../../strongs/h/h5930.md) which he hath [qāraḇ](../../strongs/h/h7126.md).

<a name="leviticus_7_9"></a>Leviticus 7:9

And all the [minchah](../../strongs/h/h4503.md) that is ['āp̄â](../../strongs/h/h644.md) in the [tannûr](../../strongs/h/h8574.md), and all that is ['asah](../../strongs/h/h6213.md) in the [marḥešeṯ](../../strongs/h/h4802.md), and in the [maḥăḇaṯ](../../strongs/h/h4227.md), shall be the [kōhēn](../../strongs/h/h3548.md) that [qāraḇ](../../strongs/h/h7126.md) it.

<a name="leviticus_7_10"></a>Leviticus 7:10

And every [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and [ḥārēḇ](../../strongs/h/h2720.md), shall all the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) have, ['iysh](../../strongs/h/h376.md) as much as ['ach](../../strongs/h/h251.md).

<a name="leviticus_7_11"></a>Leviticus 7:11

And this is the [towrah](../../strongs/h/h8451.md) of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), which he shall [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_7_12"></a>Leviticus 7:12

If he [qāraḇ](../../strongs/h/h7126.md) it for a [tôḏâ](../../strongs/h/h8426.md), then he shall [qāraḇ](../../strongs/h/h7126.md) with the [zebach](../../strongs/h/h2077.md) of [tôḏâ](../../strongs/h/h8426.md) [maṣṣâ](../../strongs/h/h4682.md) [ḥallâ](../../strongs/h/h2471.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and [maṣṣâ](../../strongs/h/h4682.md) [rāqîq](../../strongs/h/h7550.md) [māšaḥ](../../strongs/h/h4886.md) with [šemen](../../strongs/h/h8081.md), and [ḥallâ](../../strongs/h/h2471.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), of [sōleṯ](../../strongs/h/h5560.md), [rāḇaḵ](../../strongs/h/h7246.md).

<a name="leviticus_7_13"></a>Leviticus 7:13

Besides the [ḥallâ](../../strongs/h/h2471.md), he shall [qāraḇ](../../strongs/h/h7126.md) for his [qorban](../../strongs/h/h7133.md) [ḥāmēṣ](../../strongs/h/h2557.md) [lechem](../../strongs/h/h3899.md) with the [zebach](../../strongs/h/h2077.md) of [tôḏâ](../../strongs/h/h8426.md) of his [šelem](../../strongs/h/h8002.md).

<a name="leviticus_7_14"></a>Leviticus 7:14

And of it he shall [qāraḇ](../../strongs/h/h7126.md) one out of the [qorban](../../strongs/h/h7133.md) for a [tᵊrûmâ](../../strongs/h/h8641.md) unto [Yĕhovah](../../strongs/h/h3068.md), and it shall be the [kōhēn](../../strongs/h/h3548.md) that [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) of the [šelem](../../strongs/h/h8002.md).

<a name="leviticus_7_15"></a>Leviticus 7:15

And the [basar](../../strongs/h/h1320.md) of the [zebach](../../strongs/h/h2077.md) of his [šelem](../../strongs/h/h8002.md) for [tôḏâ](../../strongs/h/h8426.md) shall be ['akal](../../strongs/h/h398.md) the same [yowm](../../strongs/h/h3117.md) that it is [qorban](../../strongs/h/h7133.md); he shall not [yānaḥ](../../strongs/h/h3240.md) any of it until the [boqer](../../strongs/h/h1242.md).

<a name="leviticus_7_16"></a>Leviticus 7:16

But if the [zebach](../../strongs/h/h2077.md) of his [qorban](../../strongs/h/h7133.md) be a [neḏer](../../strongs/h/h5088.md), or a [nᵊḏāḇâ](../../strongs/h/h5071.md), it shall be ['akal](../../strongs/h/h398.md) the same [yowm](../../strongs/h/h3117.md) that he [qāraḇ](../../strongs/h/h7126.md) his [zebach](../../strongs/h/h2077.md): and on the [māḥŏrāṯ](../../strongs/h/h4283.md) also the [yāṯar](../../strongs/h/h3498.md) of it shall be ['akal](../../strongs/h/h398.md):

<a name="leviticus_7_17"></a>Leviticus 7:17

But the [yāṯar](../../strongs/h/h3498.md) of the [basar](../../strongs/h/h1320.md) of the [zebach](../../strongs/h/h2077.md) on the third [yowm](../../strongs/h/h3117.md) shall be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="leviticus_7_18"></a>Leviticus 7:18

And if any of the [basar](../../strongs/h/h1320.md) of the [zebach](../../strongs/h/h2077.md) of his [šelem](../../strongs/h/h8002.md) be ['akal](../../strongs/h/h398.md) at all on the third [yowm](../../strongs/h/h3117.md), it shall not be [ratsah](../../strongs/h/h7521.md), neither shall it be [chashab](../../strongs/h/h2803.md) unto him that [qāraḇ](../../strongs/h/h7126.md) it: it shall be a [pigûl](../../strongs/h/h6292.md), and the [nephesh](../../strongs/h/h5315.md) that ['akal](../../strongs/h/h398.md) of it shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md).

<a name="leviticus_7_19"></a>Leviticus 7:19

And the [basar](../../strongs/h/h1320.md) that [naga'](../../strongs/h/h5060.md) any [tame'](../../strongs/h/h2931.md) thing shall not be ['akal](../../strongs/h/h398.md); it shall be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md): and as for the [basar](../../strongs/h/h1320.md), all that be [tahowr](../../strongs/h/h2889.md) shall ['akal](../../strongs/h/h398.md) thereof.

<a name="leviticus_7_20"></a>Leviticus 7:20

But the [nephesh](../../strongs/h/h5315.md) that ['akal](../../strongs/h/h398.md) of the [basar](../../strongs/h/h1320.md) of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), that pertain unto [Yĕhovah](../../strongs/h/h3068.md), having his [ṭām'â](../../strongs/h/h2932.md) upon him, even that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

<a name="leviticus_7_21"></a>Leviticus 7:21

Moreover the [nephesh](../../strongs/h/h5315.md) that shall [naga'](../../strongs/h/h5060.md) any [ṭām'â](../../strongs/h/h2932.md) thing, as the [ṭām'â](../../strongs/h/h2932.md) of ['adam](../../strongs/h/h120.md), or any [tame'](../../strongs/h/h2931.md) [bĕhemah](../../strongs/h/h929.md), or any [šeqeṣ](../../strongs/h/h8263.md) [tame'](../../strongs/h/h2931.md) thing, and ['akal](../../strongs/h/h398.md) of the [basar](../../strongs/h/h1320.md) of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), which pertain unto [Yĕhovah](../../strongs/h/h3068.md), even that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

<a name="leviticus_7_22"></a>Leviticus 7:22

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_7_23"></a>Leviticus 7:23

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Ye shall ['akal](../../strongs/h/h398.md) no manner of [cheleb](../../strongs/h/h2459.md), of [showr](../../strongs/h/h7794.md), or of [keśeḇ](../../strongs/h/h3775.md), or of [ʿēz](../../strongs/h/h5795.md).

<a name="leviticus_7_24"></a>Leviticus 7:24

And the [cheleb](../../strongs/h/h2459.md) of the [nᵊḇēlâ](../../strongs/h/h5038.md), and the [cheleb](../../strongs/h/h2459.md) of [ṭᵊrēp̄â](../../strongs/h/h2966.md), may be ['asah](../../strongs/h/h6213.md) in any other [mĕla'kah](../../strongs/h/h4399.md): but ye shall in no wise ['akal](../../strongs/h/h398.md) of it.

<a name="leviticus_7_25"></a>Leviticus 7:25

For whosoever ['akal](../../strongs/h/h398.md) the [cheleb](../../strongs/h/h2459.md) of the [bĕhemah](../../strongs/h/h929.md), of which men [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md), even the [nephesh](../../strongs/h/h5315.md) that ['akal](../../strongs/h/h398.md) it shall be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

<a name="leviticus_7_26"></a>Leviticus 7:26

Moreover ye shall ['akal](../../strongs/h/h398.md) no manner of [dam](../../strongs/h/h1818.md), whether it be of [ʿôp̄](../../strongs/h/h5775.md) or of [bĕhemah](../../strongs/h/h929.md), in any of your [môšāḇ](../../strongs/h/h4186.md).

<a name="leviticus_7_27"></a>Leviticus 7:27

Whatsoever [nephesh](../../strongs/h/h5315.md) it be that ['akal](../../strongs/h/h398.md) any [kōl](../../strongs/h/h3605.md) of [dam](../../strongs/h/h1818.md), even that [nephesh](../../strongs/h/h5315.md) shall be [karath](../../strongs/h/h3772.md) from his ['am](../../strongs/h/h5971.md).

<a name="leviticus_7_28"></a>Leviticus 7:28

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_7_29"></a>Leviticus 7:29

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), He that [qāraḇ](../../strongs/h/h7126.md) the [zebach](../../strongs/h/h2077.md) of his [šelem](../../strongs/h/h8002.md) unto [Yĕhovah](../../strongs/h/h3068.md) shall [bow'](../../strongs/h/h935.md) his [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md) of the [zebach](../../strongs/h/h2077.md) of his [šelem](../../strongs/h/h8002.md).

<a name="leviticus_7_30"></a>Leviticus 7:30

His own [yad](../../strongs/h/h3027.md) shall [bow'](../../strongs/h/h935.md) the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md), the [cheleb](../../strongs/h/h2459.md) with the [ḥāzê](../../strongs/h/h2373.md), it shall he [bow'](../../strongs/h/h935.md), that the [ḥāzê](../../strongs/h/h2373.md) may be [nûp̄](../../strongs/h/h5130.md) for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_7_31"></a>Leviticus 7:31

And the [kōhēn](../../strongs/h/h3548.md) shall [qāṭar](../../strongs/h/h6999.md) the [cheleb](../../strongs/h/h2459.md) upon the [mizbeach](../../strongs/h/h4196.md): but the [ḥāzê](../../strongs/h/h2373.md) shall be ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md).

<a name="leviticus_7_32"></a>Leviticus 7:32

And the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md) shall ye [nathan](../../strongs/h/h5414.md) unto the [kōhēn](../../strongs/h/h3548.md) for a [tᵊrûmâ](../../strongs/h/h8641.md) of the [zebach](../../strongs/h/h2077.md) of your [šelem](../../strongs/h/h8002.md).

<a name="leviticus_7_33"></a>Leviticus 7:33

He among the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), that [qāraḇ](../../strongs/h/h7126.md) the [dam](../../strongs/h/h1818.md) of the [šelem](../../strongs/h/h8002.md), and the [cheleb](../../strongs/h/h2459.md), shall have the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md) for his [mānâ](../../strongs/h/h4490.md).

<a name="leviticus_7_34"></a>Leviticus 7:34

For the [tᵊnûp̄â](../../strongs/h/h8573.md) [ḥāzê](../../strongs/h/h2373.md) and the [tᵊrûmâ](../../strongs/h/h8641.md) [šôq](../../strongs/h/h7785.md) have I [laqach](../../strongs/h/h3947.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from off the [zebach](../../strongs/h/h2077.md) of their [šelem](../../strongs/h/h8002.md), and have [nathan](../../strongs/h/h5414.md) them unto ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) and unto his [ben](../../strongs/h/h1121.md) by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md) from among the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="leviticus_7_35"></a>Leviticus 7:35

This is the portion of the [māšḥâ](../../strongs/h/h4888.md) of ['Ahărôn](../../strongs/h/h175.md), and of the [māšḥâ](../../strongs/h/h4888.md) of his [ben](../../strongs/h/h1121.md), out of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md), in the [yowm](../../strongs/h/h3117.md) when he [qāraḇ](../../strongs/h/h7126.md) them to minister unto [Yĕhovah](../../strongs/h/h3068.md) in the [kāhan](../../strongs/h/h3547.md);

<a name="leviticus_7_36"></a>Leviticus 7:36

Which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) to be [nathan](../../strongs/h/h5414.md) them of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), in the [yowm](../../strongs/h/h3117.md) that he [māšaḥ](../../strongs/h/h4886.md) them, by a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout their [dôr](../../strongs/h/h1755.md).

<a name="leviticus_7_37"></a>Leviticus 7:37

This is the [towrah](../../strongs/h/h8451.md) of the an [ʿōlâ](../../strongs/h/h5930.md), of the [minchah](../../strongs/h/h4503.md), and of the [chatta'ath](../../strongs/h/h2403.md), and of the ['āšām](../../strongs/h/h817.md), and of the [millu'](../../strongs/h/h4394.md), and of the [zebach](../../strongs/h/h2077.md) of the [šelem](../../strongs/h/h8002.md);

<a name="leviticus_7_38"></a>Leviticus 7:38

Which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), in the [yowm](../../strongs/h/h3117.md) that he [tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) to [qāraḇ](../../strongs/h/h7126.md) their [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), in the [midbar](../../strongs/h/h4057.md) of [Sînay](../../strongs/h/h5514.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 6](leviticus_6.md) - [Leviticus 8](leviticus_8.md)