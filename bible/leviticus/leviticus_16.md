# [Leviticus 16](https://www.blueletterbible.org/kjv/lev/16/1/)

<a name="leviticus_16_1"></a>Leviticus 16:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) ['aḥar](../../strongs/h/h310.md) the [maveth](../../strongs/h/h4194.md) of the two [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), when they [qāraḇ](../../strongs/h/h7126.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and [muwth](../../strongs/h/h4191.md);

<a name="leviticus_16_2"></a>Leviticus 16:2

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), [dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md) thy ['ach](../../strongs/h/h251.md), that he [bow'](../../strongs/h/h935.md) not at all [ʿēṯ](../../strongs/h/h6256.md) into the [qodesh](../../strongs/h/h6944.md) [bayith](../../strongs/h/h1004.md) the [pārōḵeṯ](../../strongs/h/h6532.md) [paniym](../../strongs/h/h6440.md) the [kapōreṯ](../../strongs/h/h3727.md), which is upon the ['ārôn](../../strongs/h/h727.md); that he [muwth](../../strongs/h/h4191.md) not: for I will [ra'ah](../../strongs/h/h7200.md) in the [ʿānān](../../strongs/h/h6051.md) upon the [kapōreṯ](../../strongs/h/h3727.md).

<a name="leviticus_16_3"></a>Leviticus 16:3

Thus shall ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) into the [qodesh](../../strongs/h/h6944.md): with a [ben](../../strongs/h/h1121.md) [bāqār](../../strongs/h/h1241.md) [par](../../strongs/h/h6499.md) for a [chatta'ath](../../strongs/h/h2403.md), and an ['ayil](../../strongs/h/h352.md) for an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_16_4"></a>Leviticus 16:4

He shall [labash](../../strongs/h/h3847.md) the [qodesh](../../strongs/h/h6944.md) [baḏ](../../strongs/h/h906.md) [kĕthoneth](../../strongs/h/h3801.md), and he shall have the [baḏ](../../strongs/h/h906.md) [miḵnās](../../strongs/h/h4370.md) upon his [basar](../../strongs/h/h1320.md), and shall be [ḥāḡar](../../strongs/h/h2296.md) with a [baḏ](../../strongs/h/h906.md) ['aḇnēṭ](../../strongs/h/h73.md), and with the [baḏ](../../strongs/h/h906.md) [miṣnep̄eṯ](../../strongs/h/h4701.md) shall he be [ṣānap̄](../../strongs/h/h6801.md): these are [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md); therefore shall he [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and so [labash](../../strongs/h/h3847.md).

<a name="leviticus_16_5"></a>Leviticus 16:5

And he shall [laqach](../../strongs/h/h3947.md) of the ['edah](../../strongs/h/h5712.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) two [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md), and one ['ayil](../../strongs/h/h352.md) for an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_16_6"></a>Leviticus 16:6

And ['Ahărôn](../../strongs/h/h175.md) shall [qāraḇ](../../strongs/h/h7126.md) his [par](../../strongs/h/h6499.md) of the [chatta'ath](../../strongs/h/h2403.md), which is for himself, and make a [kāp̄ar](../../strongs/h/h3722.md) for himself, and for his [bayith](../../strongs/h/h1004.md).

<a name="leviticus_16_7"></a>Leviticus 16:7

And he shall [laqach](../../strongs/h/h3947.md) the two [śāʿîr](../../strongs/h/h8163.md), and ['amad](../../strongs/h/h5975.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_16_8"></a>Leviticus 16:8

And ['Ahărôn](../../strongs/h/h175.md) shall [nathan](../../strongs/h/h5414.md) [gôrāl](../../strongs/h/h1486.md) upon the two [śāʿîr](../../strongs/h/h8163.md); one [gôrāl](../../strongs/h/h1486.md) for [Yĕhovah](../../strongs/h/h3068.md), and the other [gôrāl](../../strongs/h/h1486.md) for the [ʿăzā'zēl](../../strongs/h/h5799.md).

<a name="leviticus_16_9"></a>Leviticus 16:9

And ['Ahărôn](../../strongs/h/h175.md) shall [qāraḇ](../../strongs/h/h7126.md) the [śāʿîr](../../strongs/h/h8163.md) upon which [Yĕhovah](../../strongs/h/h3068.md) [gôrāl](../../strongs/h/h1486.md) [ʿālâ](../../strongs/h/h5927.md), and ['asah](../../strongs/h/h6213.md) him for a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_16_10"></a>Leviticus 16:10

But the [śāʿîr](../../strongs/h/h8163.md), on which the [gôrāl](../../strongs/h/h1486.md) [ʿālâ](../../strongs/h/h5927.md) to be the [ʿăzā'zēl](../../strongs/h/h5799.md), shall be ['amad](../../strongs/h/h5975.md) [chay](../../strongs/h/h2416.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to make a [kāp̄ar](../../strongs/h/h3722.md) with him, and to let him [shalach](../../strongs/h/h7971.md) for a [ʿăzā'zēl](../../strongs/h/h5799.md) into the [midbar](../../strongs/h/h4057.md).

<a name="leviticus_16_11"></a>Leviticus 16:11

And ['Ahărôn](../../strongs/h/h175.md) shall [qāraḇ](../../strongs/h/h7126.md) the [par](../../strongs/h/h6499.md) of the [chatta'ath](../../strongs/h/h2403.md), which is for himself, and shall make a [kāp̄ar](../../strongs/h/h3722.md) for himself, and for his [bayith](../../strongs/h/h1004.md), and shall [šāḥaṭ](../../strongs/h/h7819.md) the [par](../../strongs/h/h6499.md) of the [chatta'ath](../../strongs/h/h2403.md) which is for himself:

<a name="leviticus_16_12"></a>Leviticus 16:12

And he shall [laqach](../../strongs/h/h3947.md) a [maḥtâ](../../strongs/h/h4289.md) [mᵊlō'](../../strongs/h/h4393.md) of ['esh](../../strongs/h/h784.md) [gechel](../../strongs/h/h1513.md) of ['esh](../../strongs/h/h784.md) from off the [mizbeach](../../strongs/h/h4196.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and his [ḥōp̄en](../../strongs/h/h2651.md) [mᵊlō'](../../strongs/h/h4393.md) of [sam](../../strongs/h/h5561.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) [daq](../../strongs/h/h1851.md), and [bow'](../../strongs/h/h935.md) it [bayith](../../strongs/h/h1004.md) the [pārōḵeṯ](../../strongs/h/h6532.md):

<a name="leviticus_16_13"></a>Leviticus 16:13

And he shall [nathan](../../strongs/h/h5414.md) the [qᵊṭōreṯ](../../strongs/h/h7004.md) upon the ['esh](../../strongs/h/h784.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), that the [ʿānān](../../strongs/h/h6051.md) of the [qᵊṭōreṯ](../../strongs/h/h7004.md) may [kāsâ](../../strongs/h/h3680.md) the [kapōreṯ](../../strongs/h/h3727.md) that is upon the [ʿēḏûṯ](../../strongs/h/h5715.md), that he [muwth](../../strongs/h/h4191.md) not:

<a name="leviticus_16_14"></a>Leviticus 16:14

And he shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [par](../../strongs/h/h6499.md), and [nāzâ](../../strongs/h/h5137.md) it with his ['etsba'](../../strongs/h/h676.md) upon the [kapōreṯ](../../strongs/h/h3727.md) [qeḏem](../../strongs/h/h6924.md); and [paniym](../../strongs/h/h6440.md) the [kapōreṯ](../../strongs/h/h3727.md) shall he [nāzâ](../../strongs/h/h5137.md) of the [dam](../../strongs/h/h1818.md) with his ['etsba'](../../strongs/h/h676.md) seven [pa'am](../../strongs/h/h6471.md).

<a name="leviticus_16_15"></a>Leviticus 16:15

Then shall he [šāḥaṭ](../../strongs/h/h7819.md) the [śāʿîr](../../strongs/h/h8163.md) of the [chatta'ath](../../strongs/h/h2403.md), that is for the ['am](../../strongs/h/h5971.md), and [bow'](../../strongs/h/h935.md) his [dam](../../strongs/h/h1818.md) [bayith](../../strongs/h/h1004.md) the [pārōḵeṯ](../../strongs/h/h6532.md), and ['asah](../../strongs/h/h6213.md) with that [dam](../../strongs/h/h1818.md) as he ['asah](../../strongs/h/h6213.md) with the [dam](../../strongs/h/h1818.md) of the [par](../../strongs/h/h6499.md), and [nāzâ](../../strongs/h/h5137.md) it upon the [kapōreṯ](../../strongs/h/h3727.md), and [paniym](../../strongs/h/h6440.md) the [kapōreṯ](../../strongs/h/h3727.md):

<a name="leviticus_16_16"></a>Leviticus 16:16

And he shall make a [kāp̄ar](../../strongs/h/h3722.md) for the [qodesh](../../strongs/h/h6944.md), because of the [ṭām'â](../../strongs/h/h2932.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and because of their [pesha'](../../strongs/h/h6588.md) in all their [chatta'ath](../../strongs/h/h2403.md): and so shall he ['asah](../../strongs/h/h6213.md) for the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), that [shakan](../../strongs/h/h7931.md) among them in the [tavek](../../strongs/h/h8432.md) of their [ṭām'â](../../strongs/h/h2932.md).

<a name="leviticus_16_17"></a>Leviticus 16:17

And there shall be no ['adam](../../strongs/h/h120.md) in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) when he [bow'](../../strongs/h/h935.md) in to make a [kāp̄ar](../../strongs/h/h3722.md) in the [qodesh](../../strongs/h/h6944.md), until he [yāṣā'](../../strongs/h/h3318.md), and have made a [kāp̄ar](../../strongs/h/h3722.md) for himself, and for his [bayith](../../strongs/h/h1004.md), and for all the [qāhēl](../../strongs/h/h6951.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="leviticus_16_18"></a>Leviticus 16:18

And he shall [yāṣā'](../../strongs/h/h3318.md) unto the [mizbeach](../../strongs/h/h4196.md) that is [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for it; and shall [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of the [par](../../strongs/h/h6499.md), and of the [dam](../../strongs/h/h1818.md) of the [śāʿîr](../../strongs/h/h8163.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="leviticus_16_19"></a>Leviticus 16:19

And he shall [nāzâ](../../strongs/h/h5137.md) of the [dam](../../strongs/h/h1818.md) upon it with his ['etsba'](../../strongs/h/h676.md) seven [pa'am](../../strongs/h/h6471.md), and [ṭāhēr](../../strongs/h/h2891.md) it, and [qadash](../../strongs/h/h6942.md) it from the [ṭām'â](../../strongs/h/h2932.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="leviticus_16_20"></a>Leviticus 16:20

And when he hath made a [kalah](../../strongs/h/h3615.md) of [kāp̄ar](../../strongs/h/h3722.md) the [qodesh](../../strongs/h/h6944.md), and the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and the [mizbeach](../../strongs/h/h4196.md), he shall [qāraḇ](../../strongs/h/h7126.md) the [chay](../../strongs/h/h2416.md) [śāʿîr](../../strongs/h/h8163.md):

<a name="leviticus_16_21"></a>Leviticus 16:21

And ['Ahărôn](../../strongs/h/h175.md) shall [camak](../../strongs/h/h5564.md) both his [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [chay](../../strongs/h/h2416.md) [śāʿîr](../../strongs/h/h8163.md), and [yadah](../../strongs/h/h3034.md) over him all the ['avon](../../strongs/h/h5771.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and all their [pesha'](../../strongs/h/h6588.md) in all their [chatta'ath](../../strongs/h/h2403.md), [nathan](../../strongs/h/h5414.md) them upon the [ro'sh](../../strongs/h/h7218.md) of the [śāʿîr](../../strongs/h/h8163.md), and shall [shalach](../../strongs/h/h7971.md) him by the [yad](../../strongs/h/h3027.md) of a [ʿitî](../../strongs/h/h6261.md) ['iysh](../../strongs/h/h376.md) into the [midbar](../../strongs/h/h4057.md):

<a name="leviticus_16_22"></a>Leviticus 16:22

And the [śāʿîr](../../strongs/h/h8163.md) shall [nasa'](../../strongs/h/h5375.md) upon him all their ['avon](../../strongs/h/h5771.md) unto an ['erets](../../strongs/h/h776.md) [gᵊzērâ](../../strongs/h/h1509.md): and he shall let [shalach](../../strongs/h/h7971.md) the [śāʿîr](../../strongs/h/h8163.md) in the [midbar](../../strongs/h/h4057.md).

<a name="leviticus_16_23"></a>Leviticus 16:23

And ['Ahărôn](../../strongs/h/h175.md) shall [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and shall [pāšaṭ](../../strongs/h/h6584.md) the [baḏ](../../strongs/h/h906.md) [beḡeḏ](../../strongs/h/h899.md), which he [labash](../../strongs/h/h3847.md) when he [bow'](../../strongs/h/h935.md) into the [qodesh](../../strongs/h/h6944.md), and shall [yānaḥ](../../strongs/h/h3240.md) them there:

<a name="leviticus_16_24"></a>Leviticus 16:24

And he shall [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) with [mayim](../../strongs/h/h4325.md) in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md), and [labash](../../strongs/h/h3847.md) his [beḡeḏ](../../strongs/h/h899.md), and [yāṣā'](../../strongs/h/h3318.md), and ['asah](../../strongs/h/h6213.md) his an [ʿōlâ](../../strongs/h/h5930.md), and the an [ʿōlâ](../../strongs/h/h5930.md) of the ['am](../../strongs/h/h5971.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for himself, and for the ['am](../../strongs/h/h5971.md).

<a name="leviticus_16_25"></a>Leviticus 16:25

And the [cheleb](../../strongs/h/h2459.md) of the [chatta'ath](../../strongs/h/h2403.md) shall he [qāṭar](../../strongs/h/h6999.md) upon the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_16_26"></a>Leviticus 16:26

And he that let [shalach](../../strongs/h/h7971.md) the [śāʿîr](../../strongs/h/h8163.md) for the [ʿăzā'zēl](../../strongs/h/h5799.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and ['aḥar](../../strongs/h/h310.md) [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md).

<a name="leviticus_16_27"></a>Leviticus 16:27

And the [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md), and the [śāʿîr](../../strongs/h/h8163.md) for the [chatta'ath](../../strongs/h/h2403.md), whose [dam](../../strongs/h/h1818.md) was [bow'](../../strongs/h/h935.md) in to make [kāp̄ar](../../strongs/h/h3722.md) in the [qodesh](../../strongs/h/h6944.md), shall one [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md); and they shall [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md) their ['owr](../../strongs/h/h5785.md), and their [basar](../../strongs/h/h1320.md), and their [pereš](../../strongs/h/h6569.md).

<a name="leviticus_16_28"></a>Leviticus 16:28

And he that [śārap̄](../../strongs/h/h8313.md) them shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and ['aḥar](../../strongs/h/h310.md) he shall [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md).

<a name="leviticus_16_29"></a>Leviticus 16:29

And this shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) unto you: that in the seventh [ḥōḏeš](../../strongs/h/h2320.md), on the tenth day of the [ḥōḏeš](../../strongs/h/h2320.md), ye shall [ʿānâ](../../strongs/h/h6031.md) your [nephesh](../../strongs/h/h5315.md), and ['asah](../../strongs/h/h6213.md) no [mĕla'kah](../../strongs/h/h4399.md) at all, whether it be one of your own ['ezrāḥ](../../strongs/h/h249.md), or a [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you:

<a name="leviticus_16_30"></a>Leviticus 16:30

For on that [yowm](../../strongs/h/h3117.md) shall the priest make a [kāp̄ar](../../strongs/h/h3722.md) for you, to [ṭāhēr](../../strongs/h/h2891.md) you, that ye may be [ṭāhēr](../../strongs/h/h2891.md) from all your [chatta'ath](../../strongs/h/h2403.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_16_31"></a>Leviticus 16:31

It shall be a [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md) unto you, and ye shall [ʿānâ](../../strongs/h/h6031.md) your [nephesh](../../strongs/h/h5315.md), by a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md).

<a name="leviticus_16_32"></a>Leviticus 16:32

And the [kōhēn](../../strongs/h/h3548.md), whom he shall [māšaḥ](../../strongs/h/h4886.md), and whom he shall [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) to minister in the [kāhan](../../strongs/h/h3547.md) in his ['ab](../../strongs/h/h1.md) stead, shall make the [kāp̄ar](../../strongs/h/h3722.md), and shall [labash](../../strongs/h/h3847.md) the [baḏ](../../strongs/h/h906.md) [beḡeḏ](../../strongs/h/h899.md), even the [qodesh](../../strongs/h/h6944.md) [beḡeḏ](../../strongs/h/h899.md):

<a name="leviticus_16_33"></a>Leviticus 16:33

And he shall make a [kāp̄ar](../../strongs/h/h3722.md) for the [qodesh](../../strongs/h/h6944.md) [miqdash](../../strongs/h/h4720.md), and he shall make a [kāp̄ar](../../strongs/h/h3722.md) for the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and for the [mizbeach](../../strongs/h/h4196.md), and he shall make a [kāp̄ar](../../strongs/h/h3722.md) for the [kōhēn](../../strongs/h/h3548.md), and for all the ['am](../../strongs/h/h5971.md) of the [qāhēl](../../strongs/h/h6951.md).

<a name="leviticus_16_34"></a>Leviticus 16:34

And this shall be an ['owlam](../../strongs/h/h5769.md) [chuqqah](../../strongs/h/h2708.md) unto you, to make a [kāp̄ar](../../strongs/h/h3722.md) for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) for all their [chatta'ath](../../strongs/h/h2403.md) once a [šānâ](../../strongs/h/h8141.md). And he ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 15](leviticus_15.md) - [Leviticus 17](leviticus_17.md)