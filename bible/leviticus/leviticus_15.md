# [Leviticus 15](https://www.blueletterbible.org/kjv/lev/15/1/)

<a name="leviticus_15_1"></a>Leviticus 15:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and to ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_15_2"></a>Leviticus 15:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ['iysh](../../strongs/h/h376.md) ['iysh](../../strongs/h/h376.md) hath a [zûḇ](../../strongs/h/h2100.md) out of his [basar](../../strongs/h/h1320.md), because of his [zôḇ](../../strongs/h/h2101.md) he is [tame'](../../strongs/h/h2931.md).

<a name="leviticus_15_3"></a>Leviticus 15:3

And this shall be his [ṭām'â](../../strongs/h/h2932.md) in his [zôḇ](../../strongs/h/h2101.md): whether his [basar](../../strongs/h/h1320.md) [rîr](../../strongs/h/h7325.md) with his [zôḇ](../../strongs/h/h2101.md), or his [basar](../../strongs/h/h1320.md) be [ḥāṯam](../../strongs/h/h2856.md) from his [zôḇ](../../strongs/h/h2101.md), it is his [ṭām'â](../../strongs/h/h2932.md).

<a name="leviticus_15_4"></a>Leviticus 15:4

Every [miškāḇ](../../strongs/h/h4904.md), whereon he [shakab](../../strongs/h/h7901.md) that [zûḇ](../../strongs/h/h2100.md), is [ṭāmē'](../../strongs/h/h2930.md): and every [kĕliy](../../strongs/h/h3627.md), whereon he [yashab](../../strongs/h/h3427.md), shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_15_5"></a>Leviticus 15:5

And ['iysh](../../strongs/h/h376.md) [naga'](../../strongs/h/h5060.md) his [miškāḇ](../../strongs/h/h4904.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_6"></a>Leviticus 15:6

And he that [yashab](../../strongs/h/h3427.md) on any [kĕliy](../../strongs/h/h3627.md) whereon he [yashab](../../strongs/h/h3427.md) that [zûḇ](../../strongs/h/h2100.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_7"></a>Leviticus 15:7

And he that [naga'](../../strongs/h/h5060.md) the [basar](../../strongs/h/h1320.md) of him that [zûḇ](../../strongs/h/h2100.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_8"></a>Leviticus 15:8

And if he that [zûḇ](../../strongs/h/h2100.md) [rāqaq](../../strongs/h/h7556.md) upon him that is [tahowr](../../strongs/h/h2889.md); then he shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_9"></a>Leviticus 15:9

And what [merkāḇ](../../strongs/h/h4817.md) soever he [rāḵaḇ](../../strongs/h/h7392.md) upon that [zûḇ](../../strongs/h/h2100.md) shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_15_10"></a>Leviticus 15:10

And whosoever [naga'](../../strongs/h/h5060.md) any thing that was under him shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md): and he that [nasa'](../../strongs/h/h5375.md) any of those things shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_11"></a>Leviticus 15:11

And whomsoever he [naga'](../../strongs/h/h5060.md) that [zûḇ](../../strongs/h/h2100.md), and hath not [šāṭap̄](../../strongs/h/h7857.md) his [yad](../../strongs/h/h3027.md) in [mayim](../../strongs/h/h4325.md), he shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_12"></a>Leviticus 15:12

And the [kĕliy](../../strongs/h/h3627.md) of [ḥereś](../../strongs/h/h2789.md), that he [naga'](../../strongs/h/h5060.md) which [zûḇ](../../strongs/h/h2100.md), shall be [shabar](../../strongs/h/h7665.md): and every [kĕliy](../../strongs/h/h3627.md) of ['ets](../../strongs/h/h6086.md) shall be [šāṭap̄](../../strongs/h/h7857.md) in [mayim](../../strongs/h/h4325.md).

<a name="leviticus_15_13"></a>Leviticus 15:13

And when he that hath a [zûḇ](../../strongs/h/h2100.md) is [ṭāhēr](../../strongs/h/h2891.md) of his [zôḇ](../../strongs/h/h2101.md); then he shall [sāp̄ar](../../strongs/h/h5608.md) to himself seven [yowm](../../strongs/h/h3117.md) for his [ṭāhŏrâ](../../strongs/h/h2893.md), and [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md), and shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_15_14"></a>Leviticus 15:14

And on the eighth [yowm](../../strongs/h/h3117.md) he shall [laqach](../../strongs/h/h3947.md) to him two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), and [bow'](../../strongs/h/h935.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [nathan](../../strongs/h/h5414.md) them unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_15_15"></a>Leviticus 15:15

And the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) them, the one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md); and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) for his [zôḇ](../../strongs/h/h2101.md).

<a name="leviticus_15_16"></a>Leviticus 15:16

And if any ['iysh](../../strongs/h/h376.md) [zera'](../../strongs/h/h2233.md) of [šᵊḵāḇâ](../../strongs/h/h7902.md) [yāṣā'](../../strongs/h/h3318.md) from him, then he shall [rāḥaṣ](../../strongs/h/h7364.md) all his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_17"></a>Leviticus 15:17

And every [beḡeḏ](../../strongs/h/h899.md), and every ['owr](../../strongs/h/h5785.md), whereon is the [zera'](../../strongs/h/h2233.md) of [šᵊḵāḇâ](../../strongs/h/h7902.md), shall be [kāḇas](../../strongs/h/h3526.md) with [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_18"></a>Leviticus 15:18

The ['ishshah](../../strongs/h/h802.md) also with whom ['iysh](../../strongs/h/h376.md) shall [shakab](../../strongs/h/h7901.md) with [zera'](../../strongs/h/h2233.md) of [šᵊḵāḇâ](../../strongs/h/h7902.md), they shall both [rāḥaṣ](../../strongs/h/h7364.md) themselves in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_19"></a>Leviticus 15:19

And if an ['ishshah](../../strongs/h/h802.md) have a [zûḇ](../../strongs/h/h2100.md), and her [zôḇ](../../strongs/h/h2101.md) in her [basar](../../strongs/h/h1320.md) be [dam](../../strongs/h/h1818.md), she shall be [nidâ](../../strongs/h/h5079.md) seven [yowm](../../strongs/h/h3117.md): and whosoever [naga'](../../strongs/h/h5060.md) her shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_20"></a>Leviticus 15:20

And every thing that she [shakab](../../strongs/h/h7901.md) upon in her [nidâ](../../strongs/h/h5079.md) shall be [ṭāmē'](../../strongs/h/h2930.md): every thing also that she [yashab](../../strongs/h/h3427.md) upon shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_15_21"></a>Leviticus 15:21

And whosoever [naga'](../../strongs/h/h5060.md) her [miškāḇ](../../strongs/h/h4904.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_22"></a>Leviticus 15:22

And whosoever [naga'](../../strongs/h/h5060.md) any [kĕliy](../../strongs/h/h3627.md) that she [yashab](../../strongs/h/h3427.md) upon shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_23"></a>Leviticus 15:23

And if it be on her [miškāḇ](../../strongs/h/h4904.md), or on any [kĕliy](../../strongs/h/h3627.md) whereon she [yashab](../../strongs/h/h3427.md), when he [naga'](../../strongs/h/h5060.md) it, he shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_24"></a>Leviticus 15:24

And if any ['iysh](../../strongs/h/h376.md) [shakab](../../strongs/h/h7901.md) with her at all, and her [nidâ](../../strongs/h/h5079.md) be upon him, he shall be [ṭāmē'](../../strongs/h/h2930.md) seven [yowm](../../strongs/h/h3117.md); and all the [miškāḇ](../../strongs/h/h4904.md) whereon he [shakab](../../strongs/h/h7901.md) shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_15_25"></a>Leviticus 15:25

And if an ['ishshah](../../strongs/h/h802.md) have a [zôḇ](../../strongs/h/h2101.md) of her [dam](../../strongs/h/h1818.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) out of the [ʿēṯ](../../strongs/h/h6256.md) of her [nidâ](../../strongs/h/h5079.md), or if it [zûḇ](../../strongs/h/h2100.md) [ʿal](../../strongs/h/h5921.md) of her [nidâ](../../strongs/h/h5079.md); all the [yowm](../../strongs/h/h3117.md) of the [zôḇ](../../strongs/h/h2101.md) of her [ṭām'â](../../strongs/h/h2932.md) shall be as the [yowm](../../strongs/h/h3117.md) of her [nidâ](../../strongs/h/h5079.md): she shall be [tame'](../../strongs/h/h2931.md).

<a name="leviticus_15_26"></a>Leviticus 15:26

Every [miškāḇ](../../strongs/h/h4904.md) whereon she [shakab](../../strongs/h/h7901.md) all the [yowm](../../strongs/h/h3117.md) of her [zôḇ](../../strongs/h/h2101.md) shall be unto her as the [miškāḇ](../../strongs/h/h4904.md) of her [nidâ](../../strongs/h/h5079.md): and [kĕliy](../../strongs/h/h3627.md) she [yashab](../../strongs/h/h3427.md) upon shall be [tame'](../../strongs/h/h2931.md), as the [ṭām'â](../../strongs/h/h2932.md) of her [nidâ](../../strongs/h/h5079.md).

<a name="leviticus_15_27"></a>Leviticus 15:27

And whosoever [naga'](../../strongs/h/h5060.md) those things shall be [ṭāmē'](../../strongs/h/h2930.md), and shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_15_28"></a>Leviticus 15:28

But if she be [ṭāhēr](../../strongs/h/h2891.md) of her [zôḇ](../../strongs/h/h2101.md), then she shall [sāp̄ar](../../strongs/h/h5608.md) to herself seven [yowm](../../strongs/h/h3117.md), and ['aḥar](../../strongs/h/h310.md) that she shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_15_29"></a>Leviticus 15:29

And on the eighth [yowm](../../strongs/h/h3117.md) she shall [laqach](../../strongs/h/h3947.md) unto her two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), and [bow'](../../strongs/h/h935.md) them unto the [kōhēn](../../strongs/h/h3548.md), to the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_15_30"></a>Leviticus 15:30

And the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) the one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md); and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for her [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) for the [zôḇ](../../strongs/h/h2101.md) of her [ṭām'â](../../strongs/h/h2932.md).

<a name="leviticus_15_31"></a>Leviticus 15:31

Thus shall ye [nāzar](../../strongs/h/h5144.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from their [ṭām'â](../../strongs/h/h2932.md); that they [muwth](../../strongs/h/h4191.md) not in their [ṭām'â](../../strongs/h/h2932.md), when they [ṭāmē'](../../strongs/h/h2930.md) my [miškān](../../strongs/h/h4908.md) that is [tavek](../../strongs/h/h8432.md) them.

<a name="leviticus_15_32"></a>Leviticus 15:32

This is the [towrah](../../strongs/h/h8451.md) of him that hath a [zûḇ](../../strongs/h/h2100.md), and of him whose [zera'](../../strongs/h/h2233.md) [šᵊḵāḇâ](../../strongs/h/h7902.md) [yāṣā'](../../strongs/h/h3318.md) from him, and is [ṭāmē'](../../strongs/h/h2930.md) therewith;

<a name="leviticus_15_33"></a>Leviticus 15:33

And of her that is [dāvê](../../strongs/h/h1739.md) of her [nidâ](../../strongs/h/h5079.md), and of him that [zûḇ](../../strongs/h/h2100.md) a [zôḇ](../../strongs/h/h2101.md), of the [zāḵār](../../strongs/h/h2145.md), and of the [nᵊqēḇâ](../../strongs/h/h5347.md), and of ['iysh](../../strongs/h/h376.md) that [shakab](../../strongs/h/h7901.md) with her that is [tame'](../../strongs/h/h2931.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 14](leviticus_14.md) - [Leviticus 16](leviticus_16.md)