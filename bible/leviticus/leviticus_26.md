# [Leviticus 26](https://www.blueletterbible.org/kjv/lev/26)

<a name="leviticus_26_1"></a>Leviticus 26:1

Ye shall ['asah](../../strongs/h/h6213.md) you no ['ĕlîl](../../strongs/h/h457.md) nor [pecel](../../strongs/h/h6459.md), neither [quwm](../../strongs/h/h6965.md) a [maṣṣēḇâ](../../strongs/h/h4676.md), neither shall ye [nathan](../../strongs/h/h5414.md) any [maśkîṯ](../../strongs/h/h4906.md) of ['eben](../../strongs/h/h68.md) in your ['erets](../../strongs/h/h776.md), to [shachah](../../strongs/h/h7812.md) unto it: for I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_26_2"></a>Leviticus 26:2

Ye shall [shamar](../../strongs/h/h8104.md) my [shabbath](../../strongs/h/h7676.md), and [yare'](../../strongs/h/h3372.md) my [miqdash](../../strongs/h/h4720.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_26_3"></a>Leviticus 26:3

If ye [yālaḵ](../../strongs/h/h3212.md) in my [chuqqah](../../strongs/h/h2708.md), and [shamar](../../strongs/h/h8104.md) my [mitsvah](../../strongs/h/h4687.md), and ['asah](../../strongs/h/h6213.md) them;

<a name="leviticus_26_4"></a>Leviticus 26:4

Then I will [nathan](../../strongs/h/h5414.md) you [gešem](../../strongs/h/h1653.md) in [ʿēṯ](../../strongs/h/h6256.md), and the ['erets](../../strongs/h/h776.md) shall [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md), and the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md) shall [nathan](../../strongs/h/h5414.md) their [pĕriy](../../strongs/h/h6529.md).

<a name="leviticus_26_5"></a>Leviticus 26:5

And your [dayiš](../../strongs/h/h1786.md) shall [nāśaḡ](../../strongs/h/h5381.md) unto the [bāṣîr](../../strongs/h/h1210.md), and the [bāṣîr](../../strongs/h/h1210.md) shall [nāśaḡ](../../strongs/h/h5381.md) unto the [zera'](../../strongs/h/h2233.md): and ye shall ['akal](../../strongs/h/h398.md) your [lechem](../../strongs/h/h3899.md) to the [śōḇaʿ](../../strongs/h/h7648.md), and [yashab](../../strongs/h/h3427.md) in your ['erets](../../strongs/h/h776.md) [betach](../../strongs/h/h983.md).

<a name="leviticus_26_6"></a>Leviticus 26:6

And I will [nathan](../../strongs/h/h5414.md) [shalowm](../../strongs/h/h7965.md) in the ['erets](../../strongs/h/h776.md), and ye shall [shakab](../../strongs/h/h7901.md), and none shall make you [ḥārēḏ](../../strongs/h/h2729.md): and I will [shabath](../../strongs/h/h7673.md) [ra'](../../strongs/h/h7451.md) [chay](../../strongs/h/h2416.md) out of the ['erets](../../strongs/h/h776.md), neither shall the [chereb](../../strongs/h/h2719.md) ['abar](../../strongs/h/h5674.md) through your ['erets](../../strongs/h/h776.md).

<a name="leviticus_26_7"></a>Leviticus 26:7

And ye shall [radaph](../../strongs/h/h7291.md) your ['oyeb](../../strongs/h/h341.md), and they shall [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) you by the [chereb](../../strongs/h/h2719.md).

<a name="leviticus_26_8"></a>Leviticus 26:8

And five of you shall [radaph](../../strongs/h/h7291.md) an hundred, and an hundred of you shall put ten thousand to [radaph](../../strongs/h/h7291.md): and your ['oyeb](../../strongs/h/h341.md) shall [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) you by the [chereb](../../strongs/h/h2719.md).

<a name="leviticus_26_9"></a>Leviticus 26:9

For I will have [panah](../../strongs/h/h6437.md) unto you, and make you [parah](../../strongs/h/h6509.md), and [rabah](../../strongs/h/h7235.md) you, and [quwm](../../strongs/h/h6965.md) my [bĕriyth](../../strongs/h/h1285.md) with you.

<a name="leviticus_26_10"></a>Leviticus 26:10

And ye shall ['akal](../../strongs/h/h398.md) [yashen](../../strongs/h/h3462.md), and [yāṣā'](../../strongs/h/h3318.md) the [yāšān](../../strongs/h/h3465.md) [paniym](../../strongs/h/h6440.md) of the [ḥāḏāš](../../strongs/h/h2319.md).

<a name="leviticus_26_11"></a>Leviticus 26:11

And I [nathan](../../strongs/h/h5414.md) my [miškān](../../strongs/h/h4908.md) [tavek](../../strongs/h/h8432.md) you: and my [nephesh](../../strongs/h/h5315.md) shall not [gāʿal](../../strongs/h/h1602.md) you.

<a name="leviticus_26_12"></a>Leviticus 26:12

And I will [halak](../../strongs/h/h1980.md) [tavek](../../strongs/h/h8432.md) you, and will [hayah](../../strongs/h/h1961.md) your ['Elohiym](../../strongs/h/h430.md), and ye shall [hayah](../../strongs/h/h1961.md) my ['am](../../strongs/h/h5971.md).

<a name="leviticus_26_13"></a>Leviticus 26:13

I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), that ye should not be their ['ebed](../../strongs/h/h5650.md); and I have [shabar](../../strongs/h/h7665.md) the [môṭâ](../../strongs/h/h4133.md) of your [ʿōl](../../strongs/h/h5923.md), and made you [yālaḵ](../../strongs/h/h3212.md) [qômmîyûṯ](../../strongs/h/h6968.md).

<a name="leviticus_26_14"></a>Leviticus 26:14

But if ye will not [shama'](../../strongs/h/h8085.md) unto me, and will not ['asah](../../strongs/h/h6213.md) all these [mitsvah](../../strongs/h/h4687.md);

<a name="leviticus_26_15"></a>Leviticus 26:15

And if ye shall [mā'as](../../strongs/h/h3988.md) my [chuqqah](../../strongs/h/h2708.md), or if your [nephesh](../../strongs/h/h5315.md) [gāʿal](../../strongs/h/h1602.md) my [mishpat](../../strongs/h/h4941.md), so that ye will not ['asah](../../strongs/h/h6213.md) all my [mitsvah](../../strongs/h/h4687.md), but that ye [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md):

<a name="leviticus_26_16"></a>Leviticus 26:16

I also will ['asah](../../strongs/h/h6213.md) this unto you; I will even [paqad](../../strongs/h/h6485.md) over you [behālâ](../../strongs/h/h928.md), [šaḥep̄eṯ](../../strongs/h/h7829.md), and the [qadaḥaṯ](../../strongs/h/h6920.md), that shall [kalah](../../strongs/h/h3615.md) the ['ayin](../../strongs/h/h5869.md), and cause [dûḇ](../../strongs/h/h1727.md) of [nephesh](../../strongs/h/h5315.md): and ye shall [zāraʿ](../../strongs/h/h2232.md) your [zera'](../../strongs/h/h2233.md) in [riyq](../../strongs/h/h7385.md), for your ['oyeb](../../strongs/h/h341.md) shall ['akal](../../strongs/h/h398.md) it.

<a name="leviticus_26_17"></a>Leviticus 26:17

And I will [nathan](../../strongs/h/h5414.md) my [paniym](../../strongs/h/h6440.md) against you, and ye shall be [nāḡap̄](../../strongs/h/h5062.md) [paniym](../../strongs/h/h6440.md) your ['oyeb](../../strongs/h/h341.md): they that [sane'](../../strongs/h/h8130.md) you shall [radah](../../strongs/h/h7287.md) over you; and ye shall [nûs](../../strongs/h/h5127.md) when none [radaph](../../strongs/h/h7291.md) you.

<a name="leviticus_26_18"></a>Leviticus 26:18

And if ye will not yet for all this [shama'](../../strongs/h/h8085.md) unto me, then I will [yacar](../../strongs/h/h3256.md) you seven times [yāsap̄](../../strongs/h/h3254.md) for your [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_26_19"></a>Leviticus 26:19

And I will [shabar](../../strongs/h/h7665.md) the [gā'ôn](../../strongs/h/h1347.md) of your ['oz](../../strongs/h/h5797.md); and I will [nathan](../../strongs/h/h5414.md) your [shamayim](../../strongs/h/h8064.md) as [barzel](../../strongs/h/h1270.md), and your ['erets](../../strongs/h/h776.md) as [nᵊḥûšâ](../../strongs/h/h5154.md):

<a name="leviticus_26_20"></a>Leviticus 26:20

And your [koach](../../strongs/h/h3581.md) shall be [tamam](../../strongs/h/h8552.md) in [riyq](../../strongs/h/h7385.md): for your ['erets](../../strongs/h/h776.md) shall not [nathan](../../strongs/h/h5414.md) her [yᵊḇûl](../../strongs/h/h2981.md), neither shall the ['ets](../../strongs/h/h6086.md) of the ['erets](../../strongs/h/h776.md) [nathan](../../strongs/h/h5414.md) their [pĕriy](../../strongs/h/h6529.md).

<a name="leviticus_26_21"></a>Leviticus 26:21

And if ye [yālaḵ](../../strongs/h/h3212.md) [qᵊrî](../../strongs/h/h7147.md) unto me, and ['āḇâ](../../strongs/h/h14.md) not [shama'](../../strongs/h/h8085.md) unto me; I will [yāsap̄](../../strongs/h/h3254.md) seven times [makâ](../../strongs/h/h4347.md) upon you according to your [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_26_22"></a>Leviticus 26:22

I will also [shalach](../../strongs/h/h7971.md) [sadeh](../../strongs/h/h7704.md) [chay](../../strongs/h/h2416.md) among you, which shall [šāḵōl](../../strongs/h/h7921.md) you, and [karath](../../strongs/h/h3772.md) your [bĕhemah](../../strongs/h/h929.md), and [māʿaṭ](../../strongs/h/h4591.md) you; and your [derek](../../strongs/h/h1870.md) shall be [šāmēm](../../strongs/h/h8074.md).

<a name="leviticus_26_23"></a>Leviticus 26:23

And if ye will not be [yacar](../../strongs/h/h3256.md) by me by these things, but will [halak](../../strongs/h/h1980.md) [qᵊrî](../../strongs/h/h7147.md) unto me;

<a name="leviticus_26_24"></a>Leviticus 26:24

Then will I also [halak](../../strongs/h/h1980.md) [qᵊrî](../../strongs/h/h7147.md) unto you, and will [nakah](../../strongs/h/h5221.md) you yet seven times for your [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_26_25"></a>Leviticus 26:25

And I will [bow'](../../strongs/h/h935.md) a [chereb](../../strongs/h/h2719.md) upon you, that shall [naqam](../../strongs/h/h5358.md) the [nāqām](../../strongs/h/h5359.md) of my [bĕriyth](../../strongs/h/h1285.md): and when ye are ['āsap̄](../../strongs/h/h622.md) within your [ʿîr](../../strongs/h/h5892.md), I will [shalach](../../strongs/h/h7971.md) the [deḇer](../../strongs/h/h1698.md) [tavek](../../strongs/h/h8432.md) you; and ye shall be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the ['oyeb](../../strongs/h/h341.md).

<a name="leviticus_26_26"></a>Leviticus 26:26

And when I have [shabar](../../strongs/h/h7665.md) the [maṭṭê](../../strongs/h/h4294.md) of your [lechem](../../strongs/h/h3899.md), ten ['ishshah](../../strongs/h/h802.md) shall ['āp̄â](../../strongs/h/h644.md) your [lechem](../../strongs/h/h3899.md) in one [tannûr](../../strongs/h/h8574.md), and they shall [shuwb](../../strongs/h/h7725.md) you your [lechem](../../strongs/h/h3899.md) by [mišqāl](../../strongs/h/h4948.md): and ye shall ['akal](../../strongs/h/h398.md), and not be [sāׂbaʿ](../../strongs/h/h7646.md).

<a name="leviticus_26_27"></a>Leviticus 26:27

And if ye will not for all this [shama'](../../strongs/h/h8085.md) unto me, but [halak](../../strongs/h/h1980.md) [qᵊrî](../../strongs/h/h7147.md) unto me;

<a name="leviticus_26_28"></a>Leviticus 26:28

Then I will [halak](../../strongs/h/h1980.md) [qᵊrî](../../strongs/h/h7147.md) unto you also in [chemah](../../strongs/h/h2534.md); and I, even I, will [yacar](../../strongs/h/h3256.md) you seven times for your [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_26_29"></a>Leviticus 26:29

And ye shall ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of your [ben](../../strongs/h/h1121.md), and the [basar](../../strongs/h/h1320.md) of your [bath](../../strongs/h/h1323.md) shall ye ['akal](../../strongs/h/h398.md).

<a name="leviticus_26_30"></a>Leviticus 26:30

And I will [šāmaḏ](../../strongs/h/h8045.md) your [bāmâ](../../strongs/h/h1116.md), and [karath](../../strongs/h/h3772.md) your [ḥammān](../../strongs/h/h2553.md), and [nathan](../../strongs/h/h5414.md) your [peḡer](../../strongs/h/h6297.md) upon the [peḡer](../../strongs/h/h6297.md) of your [gillûl](../../strongs/h/h1544.md), and my [nephesh](../../strongs/h/h5315.md) shall [gāʿal](../../strongs/h/h1602.md) you.

<a name="leviticus_26_31"></a>Leviticus 26:31

And I will [nathan](../../strongs/h/h5414.md) your [ʿîr](../../strongs/h/h5892.md) [chorbah](../../strongs/h/h2723.md), and [šāmēm](../../strongs/h/h8074.md) your [miqdash](../../strongs/h/h4720.md), and I will not [rîaḥ](../../strongs/h/h7306.md) the [rêaḥ](../../strongs/h/h7381.md) of your [nîḥōaḥ](../../strongs/h/h5207.md).

<a name="leviticus_26_32"></a>Leviticus 26:32

And I will [šāmēm](../../strongs/h/h8074.md) the ['erets](../../strongs/h/h776.md): and your ['oyeb](../../strongs/h/h341.md) which [yashab](../../strongs/h/h3427.md) therein shall be [šāmēm](../../strongs/h/h8074.md) at it.

<a name="leviticus_26_33"></a>Leviticus 26:33

And I will [zārâ](../../strongs/h/h2219.md) you among the [gowy](../../strongs/h/h1471.md), and will [rîq](../../strongs/h/h7324.md) a [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) you: and your ['erets](../../strongs/h/h776.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md), and your [ʿîr](../../strongs/h/h5892.md) [chorbah](../../strongs/h/h2723.md).

<a name="leviticus_26_34"></a>Leviticus 26:34

Then shall the ['erets](../../strongs/h/h776.md) [ratsah](../../strongs/h/h7521.md) her [shabbath](../../strongs/h/h7676.md), as [yowm](../../strongs/h/h3117.md) as it [šāmēm](../../strongs/h/h8074.md), and ye be in your ['oyeb](../../strongs/h/h341.md) ['erets](../../strongs/h/h776.md); even then shall the ['erets](../../strongs/h/h776.md) [shabath](../../strongs/h/h7673.md), and [ratsah](../../strongs/h/h7521.md) her [shabbath](../../strongs/h/h7676.md).

<a name="leviticus_26_35"></a>Leviticus 26:35

As [yowm](../../strongs/h/h3117.md) it [šāmēm](../../strongs/h/h8074.md) it shall [shabath](../../strongs/h/h7673.md); because it did not [shabath](../../strongs/h/h7673.md) in your [shabbath](../../strongs/h/h7676.md), when ye [yashab](../../strongs/h/h3427.md) upon it.

<a name="leviticus_26_36"></a>Leviticus 26:36

And upon them that are [šā'ar](../../strongs/h/h7604.md) of you I will [bow'](../../strongs/h/h935.md) a [mōreḵ](../../strongs/h/h4816.md) into their [lebab](../../strongs/h/h3824.md) in the ['erets](../../strongs/h/h776.md) of their ['oyeb](../../strongs/h/h341.md); and the [qowl](../../strongs/h/h6963.md) of a [nāḏap̄](../../strongs/h/h5086.md) ['aleh](../../strongs/h/h5929.md) shall [radaph](../../strongs/h/h7291.md) them; and they shall [nûs](../../strongs/h/h5127.md), as [mᵊnûsâ](../../strongs/h/h4499.md) from a [chereb](../../strongs/h/h2719.md); and they shall [naphal](../../strongs/h/h5307.md) when none [radaph](../../strongs/h/h7291.md).

<a name="leviticus_26_37"></a>Leviticus 26:37

And they shall [kashal](../../strongs/h/h3782.md) ['iysh](../../strongs/h/h376.md) upon ['ach](../../strongs/h/h251.md), as it were [paniym](../../strongs/h/h6440.md) a [chereb](../../strongs/h/h2719.md), when none [radaph](../../strongs/h/h7291.md): and ye shall have no [tᵊqûmâ](../../strongs/h/h8617.md) [paniym](../../strongs/h/h6440.md) your ['oyeb](../../strongs/h/h341.md).

<a name="leviticus_26_38"></a>Leviticus 26:38

And ye shall ['abad](../../strongs/h/h6.md) among the [gowy](../../strongs/h/h1471.md), and the ['erets](../../strongs/h/h776.md) of your ['oyeb](../../strongs/h/h341.md) shall ['akal](../../strongs/h/h398.md) you.

<a name="leviticus_26_39"></a>Leviticus 26:39

And they that are [šā'ar](../../strongs/h/h7604.md) of you shall [māqaq](../../strongs/h/h4743.md) in their ['avon](../../strongs/h/h5771.md) in your ['oyeb](../../strongs/h/h341.md) ['erets](../../strongs/h/h776.md); and also in the ['avon](../../strongs/h/h5771.md) of their ['ab](../../strongs/h/h1.md) shall they [māqaq](../../strongs/h/h4743.md) with them.

<a name="leviticus_26_40"></a>Leviticus 26:40

If they shall [yadah](../../strongs/h/h3034.md) their ['avon](../../strongs/h/h5771.md), and the ['avon](../../strongs/h/h5771.md) of their ['ab](../../strongs/h/h1.md), with their [maʿal](../../strongs/h/h4604.md) which they [māʿal](../../strongs/h/h4603.md) against me, and that also they have [halak](../../strongs/h/h1980.md) [qᵊrî](../../strongs/h/h7147.md) unto me;

<a name="leviticus_26_41"></a>Leviticus 26:41

And that I also have [yālaḵ](../../strongs/h/h3212.md) [qᵊrî](../../strongs/h/h7147.md) unto them, and have [bow'](../../strongs/h/h935.md) them into the ['erets](../../strongs/h/h776.md) of their ['oyeb](../../strongs/h/h341.md); if then their [ʿārēl](../../strongs/h/h6189.md) [lebab](../../strongs/h/h3824.md) be [kānaʿ](../../strongs/h/h3665.md), and they then [ratsah](../../strongs/h/h7521.md) of the punishment of their ['avon](../../strongs/h/h5771.md):

<a name="leviticus_26_42"></a>Leviticus 26:42

Then will I [zakar](../../strongs/h/h2142.md) my [bĕriyth](../../strongs/h/h1285.md) with [Ya'aqob](../../strongs/h/h3290.md), and also my [bĕriyth](../../strongs/h/h1285.md) with [Yiṣḥāq](../../strongs/h/h3327.md), and also my [bĕriyth](../../strongs/h/h1285.md) with ['Abraham](../../strongs/h/h85.md) will I [zakar](../../strongs/h/h2142.md); and I will [zakar](../../strongs/h/h2142.md) the ['erets](../../strongs/h/h776.md).

<a name="leviticus_26_43"></a>Leviticus 26:43

The ['erets](../../strongs/h/h776.md) also shall be ['azab](../../strongs/h/h5800.md) of them, and shall [ratsah](../../strongs/h/h7521.md) her [shabbath](../../strongs/h/h7676.md), while she [šāmēm](../../strongs/h/h8074.md) without them: and they shall [ratsah](../../strongs/h/h7521.md) of the punishment of their ['avon](../../strongs/h/h5771.md): because, even because they [mā'as](../../strongs/h/h3988.md) my [mishpat](../../strongs/h/h4941.md), and because their [nephesh](../../strongs/h/h5315.md) [gāʿal](../../strongs/h/h1602.md) my [chuqqah](../../strongs/h/h2708.md).

<a name="leviticus_26_44"></a>Leviticus 26:44

And yet for all that, when they [gam](../../strongs/h/h1571.md) in the ['erets](../../strongs/h/h776.md) of their ['oyeb](../../strongs/h/h341.md), I will not [mā'as](../../strongs/h/h3988.md) them, neither will I [gāʿal](../../strongs/h/h1602.md) them, to [kalah](../../strongs/h/h3615.md) them, and to [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) with them: for I am [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_26_45"></a>Leviticus 26:45

But I will for their sakes [zakar](../../strongs/h/h2142.md) the [bĕriyth](../../strongs/h/h1285.md) of their [ri'šôn](../../strongs/h/h7223.md), whom I [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) in the ['ayin](../../strongs/h/h5869.md) of the [gowy](../../strongs/h/h1471.md), that I might be their ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_26_46"></a>Leviticus 26:46

These are the [choq](../../strongs/h/h2706.md) and [mishpat](../../strongs/h/h4941.md) and [towrah](../../strongs/h/h8451.md), which [Yĕhovah](../../strongs/h/h3068.md) [nathan](../../strongs/h/h5414.md) between him and the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 25](leviticus_25.md) - [Leviticus 27](leviticus_27.md)