# [Leviticus 11](https://www.blueletterbible.org/kjv/lev/11/1/s_101001)

<a name="leviticus_11_1"></a>Leviticus 11:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and to ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md) unto them,

<a name="leviticus_11_2"></a>Leviticus 11:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), These are the [chay](../../strongs/h/h2416.md) which ye shall ['akal](../../strongs/h/h398.md) among all the [bĕhemah](../../strongs/h/h929.md) that are on the ['erets](../../strongs/h/h776.md).

<a name="leviticus_11_3"></a>Leviticus 11:3

Whatsoever [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md), and is [šāsaʿ](../../strongs/h/h8156.md) [šesaʿ](../../strongs/h/h8157.md), and [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), among the [bĕhemah](../../strongs/h/h929.md), that shall ye ['akal](../../strongs/h/h398.md).

<a name="leviticus_11_4"></a>Leviticus 11:4

['aḵ](../../strongs/h/h389.md) these shall ye not ['akal](../../strongs/h/h398.md) of them that [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), or of them that [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md): as the [gāmāl](../../strongs/h/h1581.md), because he [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), but [Pāras](../../strongs/h/h6536.md) not the [parsâ](../../strongs/h/h6541.md); he is [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_5"></a>Leviticus 11:5

And the [šāp̄ān](../../strongs/h/h8227.md), because he [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), but [Pāras](../../strongs/h/h6536.md) not the [parsâ](../../strongs/h/h6541.md); he is [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_6"></a>Leviticus 11:6

And the ['arneḇeṯ](../../strongs/h/h768.md), because he [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), but [Pāras](../../strongs/h/h6536.md) not the [parsâ](../../strongs/h/h6541.md); he is [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_7"></a>Leviticus 11:7

And the [ḥăzîr](../../strongs/h/h2386.md), though he [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md), and be [šāsaʿ](../../strongs/h/h8156.md) [šesaʿ](../../strongs/h/h8157.md), yet he [gārar](../../strongs/h/h1641.md) not the [gērâ](../../strongs/h/h1625.md); he is [tame'](../../strongs/h/h2931.md) to you.

<a name="leviticus_11_8"></a>Leviticus 11:8

Of their [basar](../../strongs/h/h1320.md) shall ye not ['akal](../../strongs/h/h398.md), and their [nᵊḇēlâ](../../strongs/h/h5038.md) shall ye not [naga'](../../strongs/h/h5060.md); they are [tame'](../../strongs/h/h2931.md) to you.

<a name="leviticus_11_9"></a>Leviticus 11:9

These shall ye ['akal](../../strongs/h/h398.md) of all that are in the [mayim](../../strongs/h/h4325.md): whatsoever hath [sᵊnapîr](../../strongs/h/h5579.md) and [qaśqeśeṯ](../../strongs/h/h7193.md) in the [mayim](../../strongs/h/h4325.md), in the [yam](../../strongs/h/h3220.md), and in the [nachal](../../strongs/h/h5158.md), them shall ye ['akal](../../strongs/h/h398.md).

<a name="leviticus_11_10"></a>Leviticus 11:10

And all that have not [sᵊnapîr](../../strongs/h/h5579.md) and [qaśqeśeṯ](../../strongs/h/h7193.md) in the [yam](../../strongs/h/h3220.md), and in the [nachal](../../strongs/h/h5158.md), of all that [šereṣ](../../strongs/h/h8318.md) in the [mayim](../../strongs/h/h4325.md), and of any [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) which is in the [mayim](../../strongs/h/h4325.md), they shall be a [šeqeṣ](../../strongs/h/h8263.md) unto you:

<a name="leviticus_11_11"></a>Leviticus 11:11

They shall be even a [šeqeṣ](../../strongs/h/h8263.md) unto you; ye shall not ['akal](../../strongs/h/h398.md) of their [basar](../../strongs/h/h1320.md), but ye shall have their [nᵊḇēlâ](../../strongs/h/h5038.md) in [šāqaṣ](../../strongs/h/h8262.md).

<a name="leviticus_11_12"></a>Leviticus 11:12

Whatsoever hath no [sᵊnapîr](../../strongs/h/h5579.md) nor [qaśqeśeṯ](../../strongs/h/h7193.md) in the [mayim](../../strongs/h/h4325.md), that shall be a [šeqeṣ](../../strongs/h/h8263.md) unto you.

<a name="leviticus_11_13"></a>Leviticus 11:13

And these are they which ye shall have in [šāqaṣ](../../strongs/h/h8262.md) among the [ʿôp̄](../../strongs/h/h5775.md); they shall not be ['akal](../../strongs/h/h398.md), they are a [šeqeṣ](../../strongs/h/h8263.md): the [nesheׁr](../../strongs/h/h5404.md), and the [peres](../../strongs/h/h6538.md), and the [ʿāznîyâ](../../strongs/h/h5822.md),

<a name="leviticus_11_14"></a>Leviticus 11:14

And the [dā'â](../../strongs/h/h1676.md), and the ['ayyâ](../../strongs/h/h344.md) after his [miyn](../../strongs/h/h4327.md);

<a name="leviticus_11_15"></a>Leviticus 11:15

Every [ʿōrēḇ](../../strongs/h/h6158.md) after his [miyn](../../strongs/h/h4327.md);

<a name="leviticus_11_16"></a>Leviticus 11:16

And the [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md), and the [taḥmās](../../strongs/h/h8464.md), and the [šaḥap̄](../../strongs/h/h7828.md), and the [nēṣ](../../strongs/h/h5322.md) after his [miyn](../../strongs/h/h4327.md),

<a name="leviticus_11_17"></a>Leviticus 11:17

And the [kowc](../../strongs/h/h3563.md), and the [šālaḵ](../../strongs/h/h7994.md), and the [yanšûp̄](../../strongs/h/h3244.md),

<a name="leviticus_11_18"></a>Leviticus 11:18

And the [tinšemeṯ](../../strongs/h/h8580.md), and the [qā'aṯ](../../strongs/h/h6893.md), and the [rāḥām](../../strongs/h/h7360.md),

<a name="leviticus_11_19"></a>Leviticus 11:19

And the [ḥăsîḏâ](../../strongs/h/h2624.md), the ['ănāp̄â](../../strongs/h/h601.md) after her [miyn](../../strongs/h/h4327.md), and the [dûḵîp̄aṯ](../../strongs/h/h1744.md), and the [ʿăṭallēp̄](../../strongs/h/h5847.md).

<a name="leviticus_11_20"></a>Leviticus 11:20

All [ʿôp̄](../../strongs/h/h5775.md) that [šereṣ](../../strongs/h/h8318.md), [halak](../../strongs/h/h1980.md) upon all four, shall be a [šeqeṣ](../../strongs/h/h8263.md) unto you.

<a name="leviticus_11_21"></a>Leviticus 11:21

Yet these may ye ['akal](../../strongs/h/h398.md) of every [ʿôp̄](../../strongs/h/h5775.md) [šereṣ](../../strongs/h/h8318.md) that [halak](../../strongs/h/h1980.md) upon all four, which have [keraʿ](../../strongs/h/h3767.md) [maʿal](../../strongs/h/h4605.md) their [regel](../../strongs/h/h7272.md), to [nāṯar](../../strongs/h/h5425.md) withal upon the ['erets](../../strongs/h/h776.md);

<a name="leviticus_11_22"></a>Leviticus 11:22

Even these of them ye may ['akal](../../strongs/h/h398.md); the ['arbê](../../strongs/h/h697.md) after his [miyn](../../strongs/h/h4327.md), and the [sālʿām](../../strongs/h/h5556.md) after his [miyn](../../strongs/h/h4327.md), and the [ḥargōl](../../strongs/h/h2728.md) after his [miyn](../../strongs/h/h4327.md), and the [ḥāḡāḇ](../../strongs/h/h2284.md) after his [miyn](../../strongs/h/h4327.md).

<a name="leviticus_11_23"></a>Leviticus 11:23

But all other [ʿôp̄](../../strongs/h/h5775.md) [šereṣ](../../strongs/h/h8318.md), which have four [regel](../../strongs/h/h7272.md), shall be a [šeqeṣ](../../strongs/h/h8263.md) unto you.

<a name="leviticus_11_24"></a>Leviticus 11:24

And for these ye shall be [ṭāmē'](../../strongs/h/h2930.md): whosoever [naga'](../../strongs/h/h5060.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) of them shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_25"></a>Leviticus 11:25

And whosoever [nasa'](../../strongs/h/h5375.md) of the [nᵊḇēlâ](../../strongs/h/h5038.md) of them shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_26"></a>Leviticus 11:26

The carcases of every [bĕhemah](../../strongs/h/h929.md) which [Pāras](../../strongs/h/h6536.md) the [parsâ](../../strongs/h/h6541.md), and is not [šesaʿ](../../strongs/h/h8157.md) [šāsaʿ](../../strongs/h/h8156.md), nor [ʿālâ](../../strongs/h/h5927.md) the [gērâ](../../strongs/h/h1625.md), are [tame'](../../strongs/h/h2931.md) unto you: every one that [naga'](../../strongs/h/h5060.md) them shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_11_27"></a>Leviticus 11:27

And whatsoever [halak](../../strongs/h/h1980.md) upon his [kaph](../../strongs/h/h3709.md), among all manner of [chay](../../strongs/h/h2416.md) that [halak](../../strongs/h/h1980.md) on all four, those are [tame'](../../strongs/h/h2931.md) unto you: whoso [naga'](../../strongs/h/h5060.md) their [nᵊḇēlâ](../../strongs/h/h5038.md) shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_28"></a>Leviticus 11:28

And he that [nasa'](../../strongs/h/h5375.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) of them shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md): they are [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_29"></a>Leviticus 11:29

These also shall be [tame'](../../strongs/h/h2931.md) unto you among the [šereṣ](../../strongs/h/h8318.md) things that [šāraṣ](../../strongs/h/h8317.md) upon the ['erets](../../strongs/h/h776.md); the [ḥōleḏ](../../strongs/h/h2467.md), and the [ʿaḵbār](../../strongs/h/h5909.md), and the [ṣaḇ](../../strongs/h/h6632.md) after his [miyn](../../strongs/h/h4327.md),

<a name="leviticus_11_30"></a>Leviticus 11:30

And the ['ănāqâ](../../strongs/h/h604.md), and the [koach](../../strongs/h/h3581.md), and the [lᵊṭā'â](../../strongs/h/h3911.md), and the [ḥōmeṭ](../../strongs/h/h2546.md), and the [tinšemeṯ](../../strongs/h/h8580.md).

<a name="leviticus_11_31"></a>Leviticus 11:31

These are [tame'](../../strongs/h/h2931.md) to you among all that [šereṣ](../../strongs/h/h8318.md): whosoever doth [naga'](../../strongs/h/h5060.md) them, when they be [maveth](../../strongs/h/h4194.md), shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_32"></a>Leviticus 11:32

And upon whatsoever any of them, when they are [maveth](../../strongs/h/h4194.md), doth [naphal](../../strongs/h/h5307.md), it shall be [ṭāmē'](../../strongs/h/h2930.md); whether it be any [kĕliy](../../strongs/h/h3627.md) of ['ets](../../strongs/h/h6086.md), or [beḡeḏ](../../strongs/h/h899.md), or ['owr](../../strongs/h/h5785.md), or [śaq](../../strongs/h/h8242.md), whatsoever [kĕliy](../../strongs/h/h3627.md) it be, wherein any [mĕla'kah](../../strongs/h/h4399.md) is ['asah](../../strongs/h/h6213.md), it must be [bow'](../../strongs/h/h935.md) into [mayim](../../strongs/h/h4325.md), and it shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md); so it shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_11_33"></a>Leviticus 11:33

And every [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md), whereinto any of them [naphal](../../strongs/h/h5307.md) [tavek](../../strongs/h/h8432.md), whatsoever is in it shall be [ṭāmē'](../../strongs/h/h2930.md); and ye shall [shabar](../../strongs/h/h7665.md) it.

<a name="leviticus_11_34"></a>Leviticus 11:34

Of all ['ōḵel](../../strongs/h/h400.md) which may be ['akal](../../strongs/h/h398.md), that on which such [mayim](../../strongs/h/h4325.md) [bow'](../../strongs/h/h935.md) shall be [ṭāmē'](../../strongs/h/h2930.md): and all [mašqê](../../strongs/h/h4945.md) that may be [šāṯâ](../../strongs/h/h8354.md) in every such [kĕliy](../../strongs/h/h3627.md) shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_11_35"></a>Leviticus 11:35

And every thing whereupon any part of their [nᵊḇēlâ](../../strongs/h/h5038.md) [naphal](../../strongs/h/h5307.md) shall be [ṭāmē'](../../strongs/h/h2930.md); whether it be [tannûr](../../strongs/h/h8574.md), or [kîr](../../strongs/h/h3600.md), they shall be [nāṯaṣ](../../strongs/h/h5422.md): for they are [tame'](../../strongs/h/h2931.md) and shall be [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_36"></a>Leviticus 11:36

Nevertheless a [maʿyān](../../strongs/h/h4599.md) or [bowr](../../strongs/h/h953.md), wherein there is [miqvê](../../strongs/h/h4723.md) of [mayim](../../strongs/h/h4325.md), shall be [tahowr](../../strongs/h/h2889.md): but that which [naga'](../../strongs/h/h5060.md) their [nᵊḇēlâ](../../strongs/h/h5038.md) shall be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_11_37"></a>Leviticus 11:37

And if any part of their [nᵊḇēlâ](../../strongs/h/h5038.md) [naphal](../../strongs/h/h5307.md) upon any [zērûaʿ](../../strongs/h/h2221.md) [zera'](../../strongs/h/h2233.md) which is to be [zāraʿ](../../strongs/h/h2232.md), it shall be [tahowr](../../strongs/h/h2889.md).

<a name="leviticus_11_38"></a>Leviticus 11:38

But if any [mayim](../../strongs/h/h4325.md) be [nathan](../../strongs/h/h5414.md) upon the [zera'](../../strongs/h/h2233.md), and any part of their [nᵊḇēlâ](../../strongs/h/h5038.md) [naphal](../../strongs/h/h5307.md) thereon, it shall be [tame'](../../strongs/h/h2931.md) unto you.

<a name="leviticus_11_39"></a>Leviticus 11:39

And if any [bĕhemah](../../strongs/h/h929.md), of which ye may ['oklah](../../strongs/h/h402.md), [muwth](../../strongs/h/h4191.md); he that [naga'](../../strongs/h/h5060.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) thereof shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_40"></a>Leviticus 11:40

And he that ['akal](../../strongs/h/h398.md) of the [nᵊḇēlâ](../../strongs/h/h5038.md) of it shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md): he also that [nasa'](../../strongs/h/h5375.md) the [nᵊḇēlâ](../../strongs/h/h5038.md) of it shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_11_41"></a>Leviticus 11:41

And every [šereṣ](../../strongs/h/h8318.md) that [šāraṣ](../../strongs/h/h8317.md) upon the ['erets](../../strongs/h/h776.md) shall be a [šeqeṣ](../../strongs/h/h8263.md); it shall not be ['akal](../../strongs/h/h398.md).

<a name="leviticus_11_42"></a>Leviticus 11:42

Whatsoever [halak](../../strongs/h/h1980.md) upon the [gachown](../../strongs/h/h1512.md), and whatsoever [halak](../../strongs/h/h1980.md) upon all four, or whatsoever hath [rabah](../../strongs/h/h7235.md) [regel](../../strongs/h/h7272.md) among all [šereṣ](../../strongs/h/h8318.md) that [šāraṣ](../../strongs/h/h8317.md) upon the ['erets](../../strongs/h/h776.md), them ye shall not ['akal](../../strongs/h/h398.md); for they are a [šeqeṣ](../../strongs/h/h8263.md).

<a name="leviticus_11_43"></a>Leviticus 11:43

Ye shall not [šāqaṣ](../../strongs/h/h8262.md) [nephesh](../../strongs/h/h5315.md) with any [šereṣ](../../strongs/h/h8318.md) that [šāraṣ](../../strongs/h/h8317.md), neither shall ye [ṭāmâ](../../strongs/h/h2933.md) yourselves with them, that ye should be [ṭāmē'](../../strongs/h/h2930.md) thereby.

<a name="leviticus_11_44"></a>Leviticus 11:44

For I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): ye shall therefore [qadash](../../strongs/h/h6942.md) yourselves, and ye shall be [qadowsh](../../strongs/h/h6918.md); for I am [qadowsh](../../strongs/h/h6918.md): neither shall ye [ṭāmē'](../../strongs/h/h2930.md) [nephesh](../../strongs/h/h5315.md) with any manner of [šereṣ](../../strongs/h/h8318.md) that [rāmaś](../../strongs/h/h7430.md) upon the ['erets](../../strongs/h/h776.md).

<a name="leviticus_11_45"></a>Leviticus 11:45

For I am [Yĕhovah](../../strongs/h/h3068.md) that [ʿālâ](../../strongs/h/h5927.md) you up out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), to be your ['Elohiym](../../strongs/h/h430.md): ye shall therefore be [qadowsh](../../strongs/h/h6918.md), for I am [qadowsh](../../strongs/h/h6918.md).

<a name="leviticus_11_46"></a>Leviticus 11:46

This is the [towrah](../../strongs/h/h8451.md) of the [bĕhemah](../../strongs/h/h929.md), and of the [ʿôp̄](../../strongs/h/h5775.md), and of every [chay](../../strongs/h/h2416.md) [nephesh](../../strongs/h/h5315.md) that [rāmaś](../../strongs/h/h7430.md) in the [mayim](../../strongs/h/h4325.md), and of every [nephesh](../../strongs/h/h5315.md) that [šāraṣ](../../strongs/h/h8317.md) upon the ['erets](../../strongs/h/h776.md):

<a name="leviticus_11_47"></a>Leviticus 11:47

To [bāḏal](../../strongs/h/h914.md) between the [tame'](../../strongs/h/h2931.md) and the [tahowr](../../strongs/h/h2889.md), and between the [chay](../../strongs/h/h2416.md) that may be ['akal](../../strongs/h/h398.md) and the [chay](../../strongs/h/h2416.md) that may not be ['akal](../../strongs/h/h398.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 10](leviticus_10.md) - [Leviticus 12](leviticus_12.md)