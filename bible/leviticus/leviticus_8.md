# [Leviticus 8](https://www.blueletterbible.org/kjv/lev/8)

<a name="leviticus_8_1"></a>Leviticus 8:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_8_2"></a>Leviticus 8:2

[laqach](../../strongs/h/h3947.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) with him, and the [beḡeḏ](../../strongs/h/h899.md), and the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and a [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md), and two ['ayil](../../strongs/h/h352.md), and a [sal](../../strongs/h/h5536.md) of [maṣṣâ](../../strongs/h/h4682.md);

<a name="leviticus_8_3"></a>Leviticus 8:3

And [qāhal](../../strongs/h/h6950.md) thou all the ['edah](../../strongs/h/h5712.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_8_4"></a>Leviticus 8:4

And [Mōshe](../../strongs/h/h4872.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) him; and the ['edah](../../strongs/h/h5712.md) was [qāhal](../../strongs/h/h6950.md) unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_8_5"></a>Leviticus 8:5

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto the ['edah](../../strongs/h/h5712.md), This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) to be ['asah](../../strongs/h/h6213.md).

<a name="leviticus_8_6"></a>Leviticus 8:6

And [Mōshe](../../strongs/h/h4872.md) [qāraḇ](../../strongs/h/h7126.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), and [rāḥaṣ](../../strongs/h/h7364.md) them with [mayim](../../strongs/h/h4325.md).

<a name="leviticus_8_7"></a>Leviticus 8:7

And he [nathan](../../strongs/h/h5414.md) upon him the [kĕthoneth](../../strongs/h/h3801.md), and [ḥāḡar](../../strongs/h/h2296.md) him with the ['aḇnēṭ](../../strongs/h/h73.md), and [labash](../../strongs/h/h3847.md) him with the [mᵊʿîl](../../strongs/h/h4598.md), and [nathan](../../strongs/h/h5414.md) the ['ēp̄ôḏ](../../strongs/h/h646.md) upon him, and he [ḥāḡar](../../strongs/h/h2296.md) him with the [ḥēšeḇ](../../strongs/h/h2805.md) of the ['ēp̄ôḏ](../../strongs/h/h646.md), and ['āp̄aḏ](../../strongs/h/h640.md) it unto him therewith.

<a name="leviticus_8_8"></a>Leviticus 8:8

And he [śûm](../../strongs/h/h7760.md) the [ḥōšen](../../strongs/h/h2833.md) upon him: also he [nathan](../../strongs/h/h5414.md) in the [ḥōšen](../../strongs/h/h2833.md) the ['Ûrîm](../../strongs/h/h224.md) and the [Tummîm](../../strongs/h/h8550.md).

<a name="leviticus_8_9"></a>Leviticus 8:9

And he [śûm](../../strongs/h/h7760.md) the [miṣnep̄eṯ](../../strongs/h/h4701.md) upon his [ro'sh](../../strongs/h/h7218.md); also upon the [miṣnep̄eṯ](../../strongs/h/h4701.md), even upon his [paniym](../../strongs/h/h6440.md) [môl](../../strongs/h/h4136.md), did he [śûm](../../strongs/h/h7760.md) the [zāhāḇ](../../strongs/h/h2091.md) [tsiyts](../../strongs/h/h6731.md), the [qodesh](../../strongs/h/h6944.md) [nēzer](../../strongs/h/h5145.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_8_10"></a>Leviticus 8:10

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and [māšaḥ](../../strongs/h/h4886.md) the [miškān](../../strongs/h/h4908.md) and all that was therein, and [qadash](../../strongs/h/h6942.md) them.

<a name="leviticus_8_11"></a>Leviticus 8:11

And he [nāzâ](../../strongs/h/h5137.md) thereof upon the [mizbeach](../../strongs/h/h4196.md) seven times, and [māšaḥ](../../strongs/h/h4886.md) the [mizbeach](../../strongs/h/h4196.md) and all his [kĕliy](../../strongs/h/h3627.md), both the [kîyôr](../../strongs/h/h3595.md) and his [kēn](../../strongs/h/h3653.md), to [qadash](../../strongs/h/h6942.md) them.

<a name="leviticus_8_12"></a>Leviticus 8:12

And he [yāṣaq](../../strongs/h/h3332.md) of the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md) upon ['Ahărôn](../../strongs/h/h175.md) [ro'sh](../../strongs/h/h7218.md), and [māšaḥ](../../strongs/h/h4886.md) him, to [qadash](../../strongs/h/h6942.md) him.

<a name="leviticus_8_13"></a>Leviticus 8:13

And [Mōshe](../../strongs/h/h4872.md) [qāraḇ](../../strongs/h/h7126.md) ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md), and [labash](../../strongs/h/h3847.md) [kĕthoneth](../../strongs/h/h3801.md) upon them, and [ḥāḡar](../../strongs/h/h2296.md) them with ['aḇnēṭ](../../strongs/h/h73.md), and [ḥāḇaš](../../strongs/h/h2280.md) [miḡbāʿâ](../../strongs/h/h4021.md) upon them; as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_8_14"></a>Leviticus 8:14

And he [nāḡaš](../../strongs/h/h5066.md) the [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md): and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [par](../../strongs/h/h6499.md) for the [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_8_15"></a>Leviticus 8:15

And he [šāḥaṭ](../../strongs/h/h7819.md) it; and [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [dam](../../strongs/h/h1818.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md) with his ['etsba'](../../strongs/h/h676.md), and [chata'](../../strongs/h/h2398.md) the [mizbeach](../../strongs/h/h4196.md), and [yāṣaq](../../strongs/h/h3332.md) the [dam](../../strongs/h/h1818.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of the [mizbeach](../../strongs/h/h4196.md), and [qadash](../../strongs/h/h6942.md) it, to [kāp̄ar](../../strongs/h/h3722.md) upon it.

<a name="leviticus_8_16"></a>Leviticus 8:16

And he [laqach](../../strongs/h/h3947.md) all the [cheleb](../../strongs/h/h2459.md) that was upon the [qereḇ](../../strongs/h/h7130.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), and the two [kilyah](../../strongs/h/h3629.md), and their [cheleb](../../strongs/h/h2459.md), and [Mōshe](../../strongs/h/h4872.md) [qāṭar](../../strongs/h/h6999.md) it upon the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_8_17"></a>Leviticus 8:17

But the [par](../../strongs/h/h6499.md), and his ['owr](../../strongs/h/h5785.md), his [basar](../../strongs/h/h1320.md), and his [pereš](../../strongs/h/h6569.md), he [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_8_18"></a>Leviticus 8:18

And he [qāraḇ](../../strongs/h/h7126.md) the ['ayil](../../strongs/h/h352.md) for the an [ʿōlâ](../../strongs/h/h5930.md): and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the ['ayil](../../strongs/h/h352.md).

<a name="leviticus_8_19"></a>Leviticus 8:19

And he [šāḥaṭ](../../strongs/h/h7819.md) it; and [Mōshe](../../strongs/h/h4872.md) [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="leviticus_8_20"></a>Leviticus 8:20

And he [nāṯaḥ](../../strongs/h/h5408.md) the ['ayil](../../strongs/h/h352.md) into [nēṯaḥ](../../strongs/h/h5409.md); and [Mōshe](../../strongs/h/h4872.md) [qāṭar](../../strongs/h/h6999.md) the [ro'sh](../../strongs/h/h7218.md), and the [nēṯaḥ](../../strongs/h/h5409.md), and the [peḏer](../../strongs/h/h6309.md).

<a name="leviticus_8_21"></a>Leviticus 8:21

And he [rāḥaṣ](../../strongs/h/h7364.md) the [qereḇ](../../strongs/h/h7130.md) and the [keraʿ](../../strongs/h/h3767.md) in [mayim](../../strongs/h/h4325.md); and [Mōshe](../../strongs/h/h4872.md) [qāṭar](../../strongs/h/h6999.md) the whole ['ayil](../../strongs/h/h352.md) upon the [mizbeach](../../strongs/h/h4196.md): it was an [ʿōlâ](../../strongs/h/h5930.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), and an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_8_22"></a>Leviticus 8:22

And he [qāraḇ](../../strongs/h/h7126.md) the other ['ayil](../../strongs/h/h352.md), the ['ayil](../../strongs/h/h352.md) of [millu'](../../strongs/h/h4394.md): and ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon the [ro'sh](../../strongs/h/h7218.md) of the ['ayil](../../strongs/h/h352.md).

<a name="leviticus_8_23"></a>Leviticus 8:23

And he [šāḥaṭ](../../strongs/h/h7819.md) it; and [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) of the [dam](../../strongs/h/h1818.md) of it, and [nathan](../../strongs/h/h5414.md) it upon the [tᵊnûḵ](../../strongs/h/h8571.md) of ['Ahărôn](../../strongs/h/h175.md) [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md).

<a name="leviticus_8_24"></a>Leviticus 8:24

And he [qāraḇ](../../strongs/h/h7126.md) ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md), and [Mōshe](../../strongs/h/h4872.md) [nathan](../../strongs/h/h5414.md) of the [dam](../../strongs/h/h1818.md) upon the [tᵊnûḵ](../../strongs/h/h8571.md) of their [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md), and upon the [bōhen](../../strongs/h/h931.md) of their [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of their [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md): and [Mōshe](../../strongs/h/h4872.md) [zāraq](../../strongs/h/h2236.md) the [dam](../../strongs/h/h1818.md) upon the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md).

<a name="leviticus_8_25"></a>Leviticus 8:25

And he [laqach](../../strongs/h/h3947.md) the [cheleb](../../strongs/h/h2459.md), and the ['alyâ](../../strongs/h/h451.md), and all the [cheleb](../../strongs/h/h2459.md) that was upon the [qereḇ](../../strongs/h/h7130.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md), and the two [kilyah](../../strongs/h/h3629.md), and their [cheleb](../../strongs/h/h2459.md), and the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md):

<a name="leviticus_8_26"></a>Leviticus 8:26

And out of the [sal](../../strongs/h/h5536.md) of [maṣṣâ](../../strongs/h/h4682.md) [lechem](../../strongs/h/h3899.md), that was [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), he [laqach](../../strongs/h/h3947.md) one [maṣṣâ](../../strongs/h/h4682.md) [ḥallâ](../../strongs/h/h2471.md), and a [ḥallâ](../../strongs/h/h2471.md) of [šemen](../../strongs/h/h8081.md), and one [rāqîq](../../strongs/h/h7550.md), and [śûm](../../strongs/h/h7760.md) them on the [cheleb](../../strongs/h/h2459.md), and upon the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md):

<a name="leviticus_8_27"></a>Leviticus 8:27

And he [nathan](../../strongs/h/h5414.md) all upon ['Ahărôn](../../strongs/h/h175.md) [kaph](../../strongs/h/h3709.md), and upon his [ben](../../strongs/h/h1121.md) [kaph](../../strongs/h/h3709.md), and [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_8_28"></a>Leviticus 8:28

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) them from off their [kaph](../../strongs/h/h3709.md), and [qāṭar](../../strongs/h/h6999.md) them on the [mizbeach](../../strongs/h/h4196.md) upon the an [ʿōlâ](../../strongs/h/h5930.md): they were [millu'](../../strongs/h/h4394.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md): it is an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_8_29"></a>Leviticus 8:29

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) the [ḥāzê](../../strongs/h/h2373.md), and [nûp̄](../../strongs/h/h5130.md) it for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): for of the ['ayil](../../strongs/h/h352.md) of [millu'](../../strongs/h/h4394.md) it was [Mōshe](../../strongs/h/h4872.md) [mānâ](../../strongs/h/h4490.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_8_30"></a>Leviticus 8:30

And [Mōshe](../../strongs/h/h4872.md) [laqach](../../strongs/h/h3947.md) of the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md), and of the [dam](../../strongs/h/h1818.md) which was upon the [mizbeach](../../strongs/h/h4196.md), and [nāzâ](../../strongs/h/h5137.md) it upon ['Ahărôn](../../strongs/h/h175.md), and upon his [beḡeḏ](../../strongs/h/h899.md), and upon his [ben](../../strongs/h/h1121.md), and upon his [ben](../../strongs/h/h1121.md) [beḡeḏ](../../strongs/h/h899.md) with him; and [qadash](../../strongs/h/h6942.md) ['Ahărôn](../../strongs/h/h175.md), and his [beḡeḏ](../../strongs/h/h899.md), and his [ben](../../strongs/h/h1121.md), and his [ben](../../strongs/h/h1121.md) [beḡeḏ](../../strongs/h/h899.md) with him.

<a name="leviticus_8_31"></a>Leviticus 8:31

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md), [bāšal](../../strongs/h/h1310.md) the [basar](../../strongs/h/h1320.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and there ['akal](../../strongs/h/h398.md) it with the [lechem](../../strongs/h/h3899.md) that is in the [sal](../../strongs/h/h5536.md) of [millu'](../../strongs/h/h4394.md), as I [tsavah](../../strongs/h/h6680.md), ['āmar](../../strongs/h/h559.md), ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) shall ['akal](../../strongs/h/h398.md) it.

<a name="leviticus_8_32"></a>Leviticus 8:32

And that which [yāṯar](../../strongs/h/h3498.md) of the [basar](../../strongs/h/h1320.md) and of the [lechem](../../strongs/h/h3899.md) shall ye [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="leviticus_8_33"></a>Leviticus 8:33

And ye shall not [yāṣā'](../../strongs/h/h3318.md) of the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) in seven [yowm](../../strongs/h/h3117.md), until the [yowm](../../strongs/h/h3117.md) of your [millu'](../../strongs/h/h4394.md) be at a [mālā'](../../strongs/h/h4390.md): for seven [yowm](../../strongs/h/h3117.md) shall he [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) you.

<a name="leviticus_8_34"></a>Leviticus 8:34

As he hath ['asah](../../strongs/h/h6213.md) this [yowm](../../strongs/h/h3117.md), so [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md) to do, to make a [kāp̄ar](../../strongs/h/h3722.md) for you.

<a name="leviticus_8_35"></a>Leviticus 8:35

Therefore shall ye [yashab](../../strongs/h/h3427.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md) seven [yowm](../../strongs/h/h3117.md), and [shamar](../../strongs/h/h8104.md) the [mišmereṯ](../../strongs/h/h4931.md) of [Yĕhovah](../../strongs/h/h3068.md), that ye [muwth](../../strongs/h/h4191.md) not: for so I am [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_8_36"></a>Leviticus 8:36

So ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) ['asah](../../strongs/h/h6213.md) all [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 7](leviticus_7.md) - [Leviticus 9](leviticus_9.md)