# [Leviticus 5](https://www.blueletterbible.org/kjv/lev/5)

<a name="leviticus_5_1"></a>Leviticus 5:1

And if a [nephesh](../../strongs/h/h5315.md) [chata'](../../strongs/h/h2398.md), and [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of ['alah](../../strongs/h/h423.md), and is an ['ed](../../strongs/h/h5707.md), whether he hath [ra'ah](../../strongs/h/h7200.md) or [yada'](../../strongs/h/h3045.md) of it; if he do not [nāḡaḏ](../../strongs/h/h5046.md) it, then he shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md).

<a name="leviticus_5_2"></a>Leviticus 5:2

Or if a [nephesh](../../strongs/h/h5315.md) [naga'](../../strongs/h/h5060.md) any [tame'](../../strongs/h/h2931.md) [dabar](../../strongs/h/h1697.md), whether it be a [nᵊḇēlâ](../../strongs/h/h5038.md) of a [tame'](../../strongs/h/h2931.md) [chay](../../strongs/h/h2416.md), or a [nᵊḇēlâ](../../strongs/h/h5038.md) of [tame'](../../strongs/h/h2931.md) [bĕhemah](../../strongs/h/h929.md), or the [nᵊḇēlâ](../../strongs/h/h5038.md) of [tame'](../../strongs/h/h2931.md) [šereṣ](../../strongs/h/h8318.md), and if it be ['alam](../../strongs/h/h5956.md) from him; he also shall be [tame'](../../strongs/h/h2931.md), and ['asham](../../strongs/h/h816.md).

<a name="leviticus_5_3"></a>Leviticus 5:3

Or if he [naga'](../../strongs/h/h5060.md) the [ṭām'â](../../strongs/h/h2932.md) of ['adam](../../strongs/h/h120.md), whatsoever [ṭām'â](../../strongs/h/h2932.md) it be that a man shall be [ṭāmē'](../../strongs/h/h2930.md) withal, and it be ['alam](../../strongs/h/h5956.md) from him; when he [yada'](../../strongs/h/h3045.md) of it, then he shall be ['asham](../../strongs/h/h816.md).

<a name="leviticus_5_4"></a>Leviticus 5:4

Or if a [nephesh](../../strongs/h/h5315.md) [shaba'](../../strongs/h/h7650.md), [bāṭā'](../../strongs/h/h981.md) with his [saphah](../../strongs/h/h8193.md) to do [ra'a'](../../strongs/h/h7489.md), or to do [yatab](../../strongs/h/h3190.md), whatsoever it be that an ['adam](../../strongs/h/h120.md) shall [bāṭā'](../../strongs/h/h981.md) with a [šᵊḇûʿâ](../../strongs/h/h7621.md), and it be ['alam](../../strongs/h/h5956.md) from him; when he [yada'](../../strongs/h/h3045.md) of it, then he shall be ['asham](../../strongs/h/h816.md) in one of these.

<a name="leviticus_5_5"></a>Leviticus 5:5

And it shall be, when he shall be ['asham](../../strongs/h/h816.md) in one of these things, that he shall [yadah](../../strongs/h/h3034.md) that he hath [chata'](../../strongs/h/h2398.md) in that thing:

<a name="leviticus_5_6"></a>Leviticus 5:6

And he shall [bow'](../../strongs/h/h935.md) his ['āšām](../../strongs/h/h817.md) unto [Yĕhovah](../../strongs/h/h3068.md) for his [chatta'ath](../../strongs/h/h2403.md) which he hath [chata'](../../strongs/h/h2398.md), a [nᵊqēḇâ](../../strongs/h/h5347.md) from the [tso'n](../../strongs/h/h6629.md), a [kiśbâ](../../strongs/h/h3776.md) or a [śᵊʿîrâ](../../strongs/h/h8166.md) of the [ʿēz](../../strongs/h/h5795.md), for a [chatta'ath](../../strongs/h/h2403.md); and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him concerning his [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_5_7"></a>Leviticus 5:7

And if [yad](../../strongs/h/h3027.md) be not [day](../../strongs/h/h1767.md) to [naga'](../../strongs/h/h5060.md) a [śê](../../strongs/h/h7716.md), then he shall [bow'](../../strongs/h/h935.md) for his ['āšām](../../strongs/h/h817.md), which he hath [chata'](../../strongs/h/h2398.md), two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), unto [Yĕhovah](../../strongs/h/h3068.md); one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_5_8"></a>Leviticus 5:8

And he shall [bow'](../../strongs/h/h935.md) them unto the [kōhēn](../../strongs/h/h3548.md), who shall [qāraḇ](../../strongs/h/h7126.md) that which is for the [chatta'ath](../../strongs/h/h2403.md) [ri'šôn](../../strongs/h/h7223.md), and [mālaq](../../strongs/h/h4454.md) his [ro'sh](../../strongs/h/h7218.md) [môl](../../strongs/h/h4136.md) his [ʿōrep̄](../../strongs/h/h6203.md), but shall not [bāḏal](../../strongs/h/h914.md) it:

<a name="leviticus_5_9"></a>Leviticus 5:9

And he shall [nāzâ](../../strongs/h/h5137.md) of the [dam](../../strongs/h/h1818.md) of the [chatta'ath](../../strongs/h/h2403.md) upon the [qîr](../../strongs/h/h7023.md) of thea [mizbeach](../../strongs/h/h4196.md); and the [šā'ar](../../strongs/h/h7604.md) of the [dam](../../strongs/h/h1818.md) shall be [māṣâ](../../strongs/h/h4680.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of thea [mizbeach](../../strongs/h/h4196.md): it is a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_5_10"></a>Leviticus 5:10

And he shall ['asah](../../strongs/h/h6213.md) the second for an [ʿōlâ](../../strongs/h/h5930.md), according to the [mishpat](../../strongs/h/h4941.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him for his [chatta'ath](../../strongs/h/h2403.md) which he hath [chata'](../../strongs/h/h2398.md), and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_5_11"></a>Leviticus 5:11

But if [yad](../../strongs/h/h3027.md) be not able to [nāśaḡ](../../strongs/h/h5381.md) two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), then he that [chata'](../../strongs/h/h2398.md) shall [bow'](../../strongs/h/h935.md) for his [qorban](../../strongs/h/h7133.md) the tenth part of an ['êp̄â](../../strongs/h/h374.md) of [sōleṯ](../../strongs/h/h5560.md) for a [chatta'ath](../../strongs/h/h2403.md); he shall [śûm](../../strongs/h/h7760.md) no [šemen](../../strongs/h/h8081.md) upon it, neither shall he [nathan](../../strongs/h/h5414.md) any [lᵊḇônâ](../../strongs/h/h3828.md) thereon: for it is a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_5_12"></a>Leviticus 5:12

Then shall he [bow'](../../strongs/h/h935.md) it to the [kōhēn](../../strongs/h/h3548.md), and the [kōhēn](../../strongs/h/h3548.md) shall [qāmaṣ](../../strongs/h/h7061.md) his [mᵊlō'](../../strongs/h/h4393.md) [qōmeṣ](../../strongs/h/h7062.md) of it, even an ['azkārâ](../../strongs/h/h234.md) thereof, and [qāṭar](../../strongs/h/h6999.md) it on thea [mizbeach](../../strongs/h/h4196.md), according to the ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md): it is a [chatta'ath](../../strongs/h/h2403.md).

<a name="leviticus_5_13"></a>Leviticus 5:13

And the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him as touching his [chatta'ath](../../strongs/h/h2403.md) that he hath [chata'](../../strongs/h/h2398.md) in one of these, and it shall be [sālaḥ](../../strongs/h/h5545.md) him: and the remnant shall be the [kōhēn](../../strongs/h/h3548.md), as a [minchah](../../strongs/h/h4503.md).

<a name="leviticus_5_14"></a>Leviticus 5:14

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_5_15"></a>Leviticus 5:15

If a [nephesh](../../strongs/h/h5315.md) [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md), and [chata'](../../strongs/h/h2398.md) through [šᵊḡāḡâ](../../strongs/h/h7684.md), in the [qodesh](../../strongs/h/h6944.md) of [Yĕhovah](../../strongs/h/h3068.md); then he shall [bow'](../../strongs/h/h935.md) for his ['āšām](../../strongs/h/h817.md) unto [Yĕhovah](../../strongs/h/h3068.md) an ['ayil](../../strongs/h/h352.md) [tamiym](../../strongs/h/h8549.md) out of the [tso'n](../../strongs/h/h6629.md), with thy [ʿēreḵ](../../strongs/h/h6187.md) by [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md), for an ['āšām](../../strongs/h/h817.md).


<a name="leviticus_5_16"></a>Leviticus 5:16

And he shall make [shalam](../../strongs/h/h7999.md) for the [chata'](../../strongs/h/h2398.md) that he hath done in the [qodesh](../../strongs/h/h6944.md), and shall [yāsap̄](../../strongs/h/h3254.md) the fifth part thereto, and [nathan](../../strongs/h/h5414.md) it unto the [kōhēn](../../strongs/h/h3548.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him with the ['ayil](../../strongs/h/h352.md) of the ['āšām](../../strongs/h/h817.md), and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_5_17"></a>Leviticus 5:17

And if a [nephesh](../../strongs/h/h5315.md) [chata'](../../strongs/h/h2398.md), and ['asah](../../strongs/h/h6213.md) any of these things which are forbidden to be ['asah](../../strongs/h/h6213.md) by the [mitsvah](../../strongs/h/h4687.md) of [Yĕhovah](../../strongs/h/h3068.md); though he [yada'](../../strongs/h/h3045.md) it not, yet is he ['asham](../../strongs/h/h816.md), and shall [nasa'](../../strongs/h/h5375.md) his ['avon](../../strongs/h/h5771.md).

<a name="leviticus_5_18"></a>Leviticus 5:18

And he shall [bow'](../../strongs/h/h935.md) an ['ayil](../../strongs/h/h352.md) [tamiym](../../strongs/h/h8549.md) out of the [tso'n](../../strongs/h/h6629.md), with thy [ʿēreḵ](../../strongs/h/h6187.md), for an ['āšām](../../strongs/h/h817.md), unto the [kōhēn](../../strongs/h/h3548.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him concerning his [šᵊḡāḡâ](../../strongs/h/h7684.md) wherein he [šāḡaḡ](../../strongs/h/h7683.md) and [yada'](../../strongs/h/h3045.md) it not, and it shall be [sālaḥ](../../strongs/h/h5545.md) him.

<a name="leviticus_5_19"></a>Leviticus 5:19

It is an ['āšām](../../strongs/h/h817.md): he hath certainly ['asham](../../strongs/h/h816.md) against [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 4](leviticus_4.md) - [Leviticus 6](leviticus_6.md)