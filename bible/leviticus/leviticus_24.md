# [Leviticus 24](https://www.blueletterbible.org/kjv/lev/24)

<a name="leviticus_24_1"></a>Leviticus 24:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_24_2"></a>Leviticus 24:2

[tsavah](../../strongs/h/h6680.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they [laqach](../../strongs/h/h3947.md) unto thee [zāḵ](../../strongs/h/h2134.md) [šemen](../../strongs/h/h8081.md) [zayiṯ](../../strongs/h/h2132.md) [kāṯîṯ](../../strongs/h/h3795.md) for the [ma'owr](../../strongs/h/h3974.md), to cause the [nîr](../../strongs/h/h5216.md) to [ʿālâ](../../strongs/h/h5927.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="leviticus_24_3"></a>Leviticus 24:3

[ḥûṣ](../../strongs/h/h2351.md) the [pārōḵeṯ](../../strongs/h/h6532.md) of the [ʿēḏûṯ](../../strongs/h/h5715.md), in the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), shall ['Ahărôn](../../strongs/h/h175.md) ['arak](../../strongs/h/h6186.md) it from the ['ereb](../../strongs/h/h6153.md) unto the [boqer](../../strongs/h/h1242.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) in your [dôr](../../strongs/h/h1755.md).

<a name="leviticus_24_4"></a>Leviticus 24:4

He shall ['arak](../../strongs/h/h6186.md) the [nîr](../../strongs/h/h5216.md) upon the [tahowr](../../strongs/h/h2889.md) [mᵊnôrâ](../../strongs/h/h4501.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md).

<a name="leviticus_24_5"></a>Leviticus 24:5

And thou shalt [laqach](../../strongs/h/h3947.md) [sōleṯ](../../strongs/h/h5560.md), and ['āp̄â](../../strongs/h/h644.md) twelve [ḥallâ](../../strongs/h/h2471.md) thereof: two [ʿiśśārôn](../../strongs/h/h6241.md) shall be in one [ḥallâ](../../strongs/h/h2471.md).

<a name="leviticus_24_6"></a>Leviticus 24:6

And thou shalt [śûm](../../strongs/h/h7760.md) them in two [maʿărāḵâ](../../strongs/h/h4634.md), six on a [maʿăreḵeṯ](../../strongs/h/h4635.md), upon the [tahowr](../../strongs/h/h2889.md) [šulḥān](../../strongs/h/h7979.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_24_7"></a>Leviticus 24:7

And thou shalt [nathan](../../strongs/h/h5414.md) [zāḵ](../../strongs/h/h2134.md) [lᵊḇônâ](../../strongs/h/h3828.md) upon each [maʿăreḵeṯ](../../strongs/h/h4635.md), that it may be on the [lechem](../../strongs/h/h3899.md) for an ['azkārâ](../../strongs/h/h234.md), even an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_24_8"></a>Leviticus 24:8

Every [shabbath](../../strongs/h/h7676.md) he shall [yowm](../../strongs/h/h3117.md) in ['arak](../../strongs/h/h6186.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) [tāmîḏ](../../strongs/h/h8548.md), being taken from the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) by a ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md).

<a name="leviticus_24_9"></a>Leviticus 24:9

And it shall be ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md); and they shall ['akal](../../strongs/h/h398.md) it in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md): for it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) unto him of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md) by a ['owlam](../../strongs/h/h5769.md) [choq](../../strongs/h/h2706.md).

<a name="leviticus_24_10"></a>Leviticus 24:10

And the [ben](../../strongs/h/h1121.md) of a [yiśrᵊ'ēlîṯ](../../strongs/h/h3482.md) ['ishshah](../../strongs/h/h802.md), whose [ben](../../strongs/h/h1121.md) was an ['iysh](../../strongs/h/h376.md) [Miṣrî](../../strongs/h/h4713.md), [yāṣā'](../../strongs/h/h3318.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) [yiśrᵊ'ēlî](../../strongs/h/h3481.md): and this [ben](../../strongs/h/h1121.md) of the [yiśrᵊ'ēlîṯ](../../strongs/h/h3482.md) and an ['iysh](../../strongs/h/h376.md) of [Yisra'el](../../strongs/h/h3478.md) [nāṣâ](../../strongs/h/h5327.md) in the [maḥănê](../../strongs/h/h4264.md);

<a name="leviticus_24_11"></a>Leviticus 24:11

And the [yiśrᵊ'ēlîṯ](../../strongs/h/h3482.md) ['ishshah](../../strongs/h/h802.md) [ben](../../strongs/h/h1121.md) [nāqaḇ](../../strongs/h/h5344.md) the [shem](../../strongs/h/h8034.md), and [qālal](../../strongs/h/h7043.md). And they [bow'](../../strongs/h/h935.md) him unto [Mōshe](../../strongs/h/h4872.md): (and his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Šᵊlōmîṯ](../../strongs/h/h8019.md), the [bath](../../strongs/h/h1323.md) of [diḇrî](../../strongs/h/h1704.md), of the [maṭṭê](../../strongs/h/h4294.md) of [Dān](../../strongs/h/h1835.md):)

<a name="leviticus_24_12"></a>Leviticus 24:12

And they [yānaḥ](../../strongs/h/h3240.md) him in [mišmār](../../strongs/h/h4929.md), that the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) might be [pāraš](../../strongs/h/h6567.md) them.

<a name="leviticus_24_13"></a>Leviticus 24:13

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_24_14"></a>Leviticus 24:14

[yāṣā'](../../strongs/h/h3318.md) him that hath [qālal](../../strongs/h/h7043.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md); and let all that [shama'](../../strongs/h/h8085.md) him [camak](../../strongs/h/h5564.md) their [yad](../../strongs/h/h3027.md) upon his [ro'sh](../../strongs/h/h7218.md), and let all the ['edah](../../strongs/h/h5712.md) [rāḡam](../../strongs/h/h7275.md) him.

<a name="leviticus_24_15"></a>Leviticus 24:15

And thou shalt [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), ['iysh](../../strongs/h/h376.md) [qālal](../../strongs/h/h7043.md) his ['Elohiym](../../strongs/h/h430.md) shall [nasa'](../../strongs/h/h5375.md) his [ḥēṭĕ'](../../strongs/h/h2399.md).

<a name="leviticus_24_16"></a>Leviticus 24:16

And he that [nāqaḇ](../../strongs/h/h5344.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), he shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md), and all the ['edah](../../strongs/h/h5712.md) shall [rāḡam](../../strongs/h/h7275.md) [rāḡam](../../strongs/h/h7275.md) him: as well the [ger](../../strongs/h/h1616.md), as he that is ['ezrāḥ](../../strongs/h/h249.md), when he [nāqaḇ](../../strongs/h/h5344.md) the [shem](../../strongs/h/h8034.md), shall be [muwth](../../strongs/h/h4191.md).

<a name="leviticus_24_17"></a>Leviticus 24:17

And ['iysh](../../strongs/h/h376.md) that [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md) ['adam](../../strongs/h/h120.md) shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="leviticus_24_18"></a>Leviticus 24:18

And he that [nakah](../../strongs/h/h5221.md) a [nephesh](../../strongs/h/h5315.md) [bĕhemah](../../strongs/h/h929.md) shall [shalam](../../strongs/h/h7999.md); [nephesh](../../strongs/h/h5315.md) for [nephesh](../../strongs/h/h5315.md).

<a name="leviticus_24_19"></a>Leviticus 24:19

And if an ['iysh](../../strongs/h/h376.md) [nathan](../../strongs/h/h5414.md) a [mᵊ'ûm](../../strongs/h/h3971.md) in his [ʿāmîṯ](../../strongs/h/h5997.md); as he hath ['asah](../../strongs/h/h6213.md), so shall it be ['asah](../../strongs/h/h6213.md) to him;

<a name="leviticus_24_20"></a>Leviticus 24:20

[šeḇar](../../strongs/h/h7667.md) for [šeḇar](../../strongs/h/h7667.md), ['ayin](../../strongs/h/h5869.md) for ['ayin](../../strongs/h/h5869.md), [šēn](../../strongs/h/h8127.md) for [šēn](../../strongs/h/h8127.md): as he hath [nathan](../../strongs/h/h5414.md) a [mᵊ'ûm](../../strongs/h/h3971.md) in an ['adam](../../strongs/h/h120.md), so shall it be [nathan](../../strongs/h/h5414.md) to him again.

<a name="leviticus_24_21"></a>Leviticus 24:21

And he that [nakah](../../strongs/h/h5221.md) a [bĕhemah](../../strongs/h/h929.md), he shall [shalam](../../strongs/h/h7999.md) it: and he that [nakah](../../strongs/h/h5221.md) an ['adam](../../strongs/h/h120.md), he shall be put to [muwth](../../strongs/h/h4191.md).

<a name="leviticus_24_22"></a>Leviticus 24:22

Ye shall have one [mishpat](../../strongs/h/h4941.md), as well for the [ger](../../strongs/h/h1616.md), as for one of your own ['ezrāḥ](../../strongs/h/h249.md): for I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_24_23"></a>Leviticus 24:23

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) to the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), that they should [yāṣā'](../../strongs/h/h3318.md) him that had [qālal](../../strongs/h/h7043.md) [ḥûṣ](../../strongs/h/h2351.md) of the [maḥănê](../../strongs/h/h4264.md), and [rāḡam](../../strongs/h/h7275.md) him with ['eben](../../strongs/h/h68.md). And the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) ['asah](../../strongs/h/h6213.md) as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 23](leviticus_23.md) - [Leviticus 25](leviticus_25.md)