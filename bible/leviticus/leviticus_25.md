# [Leviticus 25](https://www.blueletterbible.org/kjv/lev/25)

<a name="leviticus_25_1"></a>Leviticus 25:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_25_2"></a>Leviticus 25:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) which I [nathan](../../strongs/h/h5414.md) you, then shall the ['erets](../../strongs/h/h776.md) [shabath](../../strongs/h/h7673.md) a [shabbath](../../strongs/h/h7676.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_25_3"></a>Leviticus 25:3

Six [šānâ](../../strongs/h/h8141.md) thou shalt [zāraʿ](../../strongs/h/h2232.md) thy [sadeh](../../strongs/h/h7704.md), and six [šānâ](../../strongs/h/h8141.md) thou shalt [zāmar](../../strongs/h/h2168.md) thy [kerem](../../strongs/h/h3754.md), and ['āsap̄](../../strongs/h/h622.md) in the [tᵊḇû'â](../../strongs/h/h8393.md) thereof;

<a name="leviticus_25_4"></a>Leviticus 25:4

But in the seventh [šānâ](../../strongs/h/h8141.md) shall be a [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md) unto the ['erets](../../strongs/h/h776.md), a [shabbath](../../strongs/h/h7676.md) for [Yĕhovah](../../strongs/h/h3068.md): thou shalt neither [zāraʿ](../../strongs/h/h2232.md) thy [sadeh](../../strongs/h/h7704.md), nor [zāmar](../../strongs/h/h2168.md) thy [kerem](../../strongs/h/h3754.md).

<a name="leviticus_25_5"></a>Leviticus 25:5

That which [sāp̄îaḥ](../../strongs/h/h5599.md) of thy [qāṣîr](../../strongs/h/h7105.md) thou shalt not [qāṣar](../../strongs/h/h7114.md), neither [bāṣar](../../strongs/h/h1219.md) the [ʿēnāḇ](../../strongs/h/h6025.md) of thy [nāzîr](../../strongs/h/h5139.md): for it is a [šānâ](../../strongs/h/h8141.md) of [šabāṯôn](../../strongs/h/h7677.md) unto the ['erets](../../strongs/h/h776.md).

<a name="leviticus_25_6"></a>Leviticus 25:6

And the [shabbath](../../strongs/h/h7676.md) of the ['erets](../../strongs/h/h776.md) shall be ['oklah](../../strongs/h/h402.md) for you; for thee, and for thy ['ebed](../../strongs/h/h5650.md), and for thy ['amah](../../strongs/h/h519.md), and for thy [śāḵîr](../../strongs/h/h7916.md), and for thy [tôšāḇ](../../strongs/h/h8453.md) that [guwr](../../strongs/h/h1481.md) with thee.

<a name="leviticus_25_7"></a>Leviticus 25:7

And for thy [bĕhemah](../../strongs/h/h929.md), and for the [chay](../../strongs/h/h2416.md) that are in thy ['erets](../../strongs/h/h776.md), shall all the [tᵊḇû'â](../../strongs/h/h8393.md) thereof be ['akal](../../strongs/h/h398.md).

<a name="leviticus_25_8"></a>Leviticus 25:8

And thou shalt [sāp̄ar](../../strongs/h/h5608.md) seven [shabbath](../../strongs/h/h7676.md) of [šānâ](../../strongs/h/h8141.md) unto thee, seven [pa'am](../../strongs/h/h6471.md) seven [šānâ](../../strongs/h/h8141.md); and the [yowm](../../strongs/h/h3117.md) of the seven [shabbath](../../strongs/h/h7676.md) of [šānâ](../../strongs/h/h8141.md) shall be unto thee forty and nine [šānâ](../../strongs/h/h8141.md).

<a name="leviticus_25_9"></a>Leviticus 25:9

Then shalt thou cause the [šôp̄ār](../../strongs/h/h7782.md) of the [tᵊrûʿâ](../../strongs/h/h8643.md) to ['abar](../../strongs/h/h5674.md) on the tenth of the seventh [ḥōḏeš](../../strongs/h/h2320.md), in the [yowm](../../strongs/h/h3117.md) of [kipur](../../strongs/h/h3725.md) shall ye make the [šôp̄ār](../../strongs/h/h7782.md) ['abar](../../strongs/h/h5674.md) throughout all your ['erets](../../strongs/h/h776.md).

<a name="leviticus_25_10"></a>Leviticus 25:10

And ye shall [qadash](../../strongs/h/h6942.md) the fiftieth [šānâ](../../strongs/h/h8141.md), and [qara'](../../strongs/h/h7121.md) [dᵊrôr](../../strongs/h/h1865.md) throughout all the ['erets](../../strongs/h/h776.md) unto all the [yashab](../../strongs/h/h3427.md) thereof: it shall be a [yôḇēl](../../strongs/h/h3104.md) unto you; and ye shall [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) unto his ['achuzzah](../../strongs/h/h272.md), and ye shall [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) unto his [mišpāḥâ](../../strongs/h/h4940.md).

<a name="leviticus_25_11"></a>Leviticus 25:11

A [yôḇēl](../../strongs/h/h3104.md) shall that fiftieth [šānâ](../../strongs/h/h8141.md) be unto you: ye shall not [zāraʿ](../../strongs/h/h2232.md), neither [qāṣar](../../strongs/h/h7114.md) that which [sāp̄îaḥ](../../strongs/h/h5599.md) in it, nor [bāṣar](../../strongs/h/h1219.md) the [nāzîr](../../strongs/h/h5139.md).

<a name="leviticus_25_12"></a>Leviticus 25:12

For it is the [yôḇēl](../../strongs/h/h3104.md); it shall be [qodesh](../../strongs/h/h6944.md) unto you: ye shall ['akal](../../strongs/h/h398.md) the [tᵊḇû'â](../../strongs/h/h8393.md) thereof out of the [sadeh](../../strongs/h/h7704.md).

<a name="leviticus_25_13"></a>Leviticus 25:13

In the [šānâ](../../strongs/h/h8141.md) of this [yôḇēl](../../strongs/h/h3104.md) ye shall [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) unto his ['achuzzah](../../strongs/h/h272.md).

<a name="leviticus_25_14"></a>Leviticus 25:14

And if thou [māḵar](../../strongs/h/h4376.md) [mimkār](../../strongs/h/h4465.md) unto thy [ʿāmîṯ](../../strongs/h/h5997.md), or [qānâ](../../strongs/h/h7069.md) of thy [ʿāmîṯ](../../strongs/h/h5997.md) [yad](../../strongs/h/h3027.md), ye shall not [yānâ](../../strongs/h/h3238.md) ['iysh](../../strongs/h/h376.md) ['ach](../../strongs/h/h251.md):

<a name="leviticus_25_15"></a>Leviticus 25:15

According to the [mispār](../../strongs/h/h4557.md) of [šānâ](../../strongs/h/h8141.md) ['aḥar](../../strongs/h/h310.md) the [yôḇēl](../../strongs/h/h3104.md) thou shalt [qānâ](../../strongs/h/h7069.md) of thy [ʿāmîṯ](../../strongs/h/h5997.md), and according unto the [mispār](../../strongs/h/h4557.md) of [šānâ](../../strongs/h/h8141.md) of the [tᵊḇû'â](../../strongs/h/h8393.md) he shall [māḵar](../../strongs/h/h4376.md) unto thee:

<a name="leviticus_25_16"></a>Leviticus 25:16

[peh](../../strongs/h/h6310.md) to the [rōḇ](../../strongs/h/h7230.md) of [šānâ](../../strongs/h/h8141.md) thou shalt [rabah](../../strongs/h/h7235.md) the [miqnâ](../../strongs/h/h4736.md) thereof, and [peh](../../strongs/h/h6310.md) to the [māʿaṭ](../../strongs/h/h4591.md) of [šānâ](../../strongs/h/h8141.md) thou shalt [māʿaṭ](../../strongs/h/h4591.md) the [miqnâ](../../strongs/h/h4736.md) of it: for according to the [mispār](../../strongs/h/h4557.md) of the years of the [tᵊḇû'â](../../strongs/h/h8393.md) doth he [māḵar](../../strongs/h/h4376.md) unto thee.

<a name="leviticus_25_17"></a>Leviticus 25:17

Ye shall not therefore [yānâ](../../strongs/h/h3238.md) ['iysh](../../strongs/h/h376.md) [ʿāmîṯ](../../strongs/h/h5997.md); but thou shalt [yare'](../../strongs/h/h3372.md) thy ['Elohiym](../../strongs/h/h430.md):for I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_25_18"></a>Leviticus 25:18

Wherefore ye shall ['asah](../../strongs/h/h6213.md) my [chuqqah](../../strongs/h/h2708.md), and [shamar](../../strongs/h/h8104.md) my [mishpat](../../strongs/h/h4941.md), and ['asah](../../strongs/h/h6213.md) them; and ye shall [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) in [betach](../../strongs/h/h983.md).

<a name="leviticus_25_19"></a>Leviticus 25:19

And the ['erets](../../strongs/h/h776.md) shall [nathan](../../strongs/h/h5414.md) her [pĕriy](../../strongs/h/h6529.md), and ye shall ['akal](../../strongs/h/h398.md) your [śōḇaʿ](../../strongs/h/h7648.md), and [yashab](../../strongs/h/h3427.md) therein in [betach](../../strongs/h/h983.md).

<a name="leviticus_25_20"></a>Leviticus 25:20

And if ye shall ['āmar](../../strongs/h/h559.md), What shall we ['akal](../../strongs/h/h398.md) the seventh [šānâ](../../strongs/h/h8141.md)? behold, we shall not [zāraʿ](../../strongs/h/h2232.md), nor ['āsap̄](../../strongs/h/h622.md) in our [tᵊḇû'â](../../strongs/h/h8393.md):

<a name="leviticus_25_21"></a>Leviticus 25:21

Then I will [tsavah](../../strongs/h/h6680.md) my [bĕrakah](../../strongs/h/h1293.md) upon you in the sixth [šānâ](../../strongs/h/h8141.md), and it shall ['asah](../../strongs/h/h6213.md) [tᵊḇû'â](../../strongs/h/h8393.md) for three [šānâ](../../strongs/h/h8141.md).

<a name="leviticus_25_22"></a>Leviticus 25:22

And ye shall [zāraʿ](../../strongs/h/h2232.md) the eighth [šānâ](../../strongs/h/h8141.md), and ['akal](../../strongs/h/h398.md) yet of [yāšān](../../strongs/h/h3465.md) [tᵊḇû'â](../../strongs/h/h8393.md) until the ninth [šānâ](../../strongs/h/h8141.md); until her [tᵊḇû'â](../../strongs/h/h8393.md) [bow'](../../strongs/h/h935.md) ye shall ['akal](../../strongs/h/h398.md) of the [yāšān](../../strongs/h/h3465.md).

<a name="leviticus_25_23"></a>Leviticus 25:23

The ['erets](../../strongs/h/h776.md) shall not be [māḵar](../../strongs/h/h4376.md) [ṣmṯṯ](../../strongs/h/h6783.md): for the ['erets](../../strongs/h/h776.md) is mine, for ye are [ger](../../strongs/h/h1616.md) and [tôšāḇ](../../strongs/h/h8453.md) with me.

<a name="leviticus_25_24"></a>Leviticus 25:24

And in all the ['erets](../../strongs/h/h776.md) of your ['achuzzah](../../strongs/h/h272.md) ye shall [nathan](../../strongs/h/h5414.md) a [gᵊ'ullâ](../../strongs/h/h1353.md) for the ['erets](../../strongs/h/h776.md).

<a name="leviticus_25_25"></a>Leviticus 25:25

If thy ['ach](../../strongs/h/h251.md) be [mûḵ](../../strongs/h/h4134.md), and hath [māḵar](../../strongs/h/h4376.md) away some of his ['achuzzah](../../strongs/h/h272.md), and if any of his [qarowb](../../strongs/h/h7138.md) [bow'](../../strongs/h/h935.md) to [gā'al](../../strongs/h/h1350.md) it, then shall he [gā'al](../../strongs/h/h1350.md) that which his ['ach](../../strongs/h/h251.md) [mimkār](../../strongs/h/h4465.md).

<a name="leviticus_25_26"></a>Leviticus 25:26

And if the ['iysh](../../strongs/h/h376.md) have none to [gā'al](../../strongs/h/h1350.md) it, and [yad](../../strongs/h/h3027.md) be [day](../../strongs/h/h1767.md) [nāśaḡ](../../strongs/h/h5381.md) to [māṣā'](../../strongs/h/h4672.md) [gᵊ'ullâ](../../strongs/h/h1353.md) it;

<a name="leviticus_25_27"></a>Leviticus 25:27

Then let him [chashab](../../strongs/h/h2803.md) the [šānâ](../../strongs/h/h8141.md) of the [mimkār](../../strongs/h/h4465.md) thereof, and [shuwb](../../strongs/h/h7725.md) the [ʿāḏap̄](../../strongs/h/h5736.md) unto the ['iysh](../../strongs/h/h376.md) to whom he [māḵar](../../strongs/h/h4376.md) it; that he may [shuwb](../../strongs/h/h7725.md) unto his ['achuzzah](../../strongs/h/h272.md).

<a name="leviticus_25_28"></a>Leviticus 25:28

But if he be not [day](../../strongs/h/h1767.md) to [māṣā'](../../strongs/h/h4672.md) [shuwb](../../strongs/h/h7725.md) it to him, then that which is [mimkār](../../strongs/h/h4465.md) shall remain in the [yad](../../strongs/h/h3027.md) of him that hath [qānâ](../../strongs/h/h7069.md) it until the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md): and in the [yôḇēl](../../strongs/h/h3104.md) it shall [yāṣā'](../../strongs/h/h3318.md), and he shall [shuwb](../../strongs/h/h7725.md) unto his ['achuzzah](../../strongs/h/h272.md).

<a name="leviticus_25_29"></a>Leviticus 25:29

And if an ['iysh](../../strongs/h/h376.md) [māḵar](../../strongs/h/h4376.md) a [môšāḇ](../../strongs/h/h4186.md) [bayith](../../strongs/h/h1004.md) in a [ḥômâ](../../strongs/h/h2346.md) [ʿîr](../../strongs/h/h5892.md), then he may [gᵊ'ullâ](../../strongs/h/h1353.md) it within a [tamam](../../strongs/h/h8552.md) [šānâ](../../strongs/h/h8141.md) after it is [mimkār](../../strongs/h/h4465.md); within a [yowm](../../strongs/h/h3117.md) may he [gᵊ'ullâ](../../strongs/h/h1353.md) it.

<a name="leviticus_25_30"></a>Leviticus 25:30

And if it be not [gā'al](../../strongs/h/h1350.md) within the [mālā'](../../strongs/h/h4390.md) of a [tamiym](../../strongs/h/h8549.md) [šānâ](../../strongs/h/h8141.md), then the [bayith](../../strongs/h/h1004.md) that is in the [ḥômâ](../../strongs/h/h2346.md) [ʿîr](../../strongs/h/h5892.md) shall be [quwm](../../strongs/h/h6965.md) [ṣmṯṯ](../../strongs/h/h6783.md) to him that [qānâ](../../strongs/h/h7069.md) it throughout his [dôr](../../strongs/h/h1755.md): it shall not [yāṣā'](../../strongs/h/h3318.md) in the [yôḇēl](../../strongs/h/h3104.md).

<a name="leviticus_25_31"></a>Leviticus 25:31

But the [bayith](../../strongs/h/h1004.md) of the [ḥāṣēr](../../strongs/h/h2691.md) which have no [ḥômâ](../../strongs/h/h2346.md) [cabiyb](../../strongs/h/h5439.md) them shall be [chashab](../../strongs/h/h2803.md) as the [sadeh](../../strongs/h/h7704.md) of the ['erets](../../strongs/h/h776.md): they may be [gᵊ'ullâ](../../strongs/h/h1353.md), and they shall [yāṣā'](../../strongs/h/h3318.md) in the [yôḇēl](../../strongs/h/h3104.md).

<a name="leviticus_25_32"></a>Leviticus 25:32

Notwithstanding the [ʿîr](../../strongs/h/h5892.md) of the [Lᵊvî](../../strongs/h/h3881.md), and the [bayith](../../strongs/h/h1004.md) of the [ʿîr](../../strongs/h/h5892.md) of their ['achuzzah](../../strongs/h/h272.md), may the [Lᵊvî](../../strongs/h/h3881.md) [gᵊ'ullâ](../../strongs/h/h1353.md) ['owlam](../../strongs/h/h5769.md).

<a name="leviticus_25_33"></a>Leviticus 25:33

And if [gā'al](../../strongs/h/h1350.md) of the [Lᵊvî](../../strongs/h/h3881.md), then the [bayith](../../strongs/h/h1004.md) that was [mimkār](../../strongs/h/h4465.md), and the [ʿîr](../../strongs/h/h5892.md) of his ['achuzzah](../../strongs/h/h272.md), shall [yāṣā'](../../strongs/h/h3318.md) in [yôḇēl](../../strongs/h/h3104.md): for the [bayith](../../strongs/h/h1004.md) of the [ʿîr](../../strongs/h/h5892.md) of the [Lᵊvî](../../strongs/h/h3881.md) are their ['achuzzah](../../strongs/h/h272.md) [tavek](../../strongs/h/h8432.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="leviticus_25_34"></a>Leviticus 25:34

But the [sadeh](../../strongs/h/h7704.md) of the [miḡrāš](../../strongs/h/h4054.md) of their [ʿîr](../../strongs/h/h5892.md) may not be [māḵar](../../strongs/h/h4376.md); for it is their ['owlam](../../strongs/h/h5769.md) ['achuzzah](../../strongs/h/h272.md).

<a name="leviticus_25_35"></a>Leviticus 25:35

And if thy ['ach](../../strongs/h/h251.md) be [mûḵ](../../strongs/h/h4134.md), and [mowt](../../strongs/h/h4131.md) with [yad](../../strongs/h/h3027.md); then thou shalt [ḥāzaq](../../strongs/h/h2388.md) him: yea, though he be a [ger](../../strongs/h/h1616.md), or a [tôšāḇ](../../strongs/h/h8453.md); that he may [chay](../../strongs/h/h2416.md) with thee.

<a name="leviticus_25_36"></a>Leviticus 25:36

[laqach](../../strongs/h/h3947.md) thou no [neshek](../../strongs/h/h5392.md) of him, or [tarbîṯ](../../strongs/h/h8636.md): but [yare'](../../strongs/h/h3372.md) thy ['Elohiym](../../strongs/h/h430.md); that thy ['ach](../../strongs/h/h251.md) may [chay](../../strongs/h/h2416.md) with thee.

<a name="leviticus_25_37"></a>Leviticus 25:37

Thou shalt not [nathan](../../strongs/h/h5414.md) him thy [keceph](../../strongs/h/h3701.md) upon [neshek](../../strongs/h/h5392.md), nor [nathan](../../strongs/h/h5414.md) him thy ['ōḵel](../../strongs/h/h400.md) for [marbîṯ](../../strongs/h/h4768.md).

<a name="leviticus_25_38"></a>Leviticus 25:38

I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), which [yāṣā'](../../strongs/h/h3318.md) you of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), to [nathan](../../strongs/h/h5414.md) you the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), and to be your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_25_39"></a>Leviticus 25:39

And if thy ['ach](../../strongs/h/h251.md) that dwelleth by thee be [mûḵ](../../strongs/h/h4134.md), and be [māḵar](../../strongs/h/h4376.md) unto thee; thou shalt not ['abad](../../strongs/h/h5647.md) him to [ʿăḇōḏâ](../../strongs/h/h5656.md) as an ['ebed](../../strongs/h/h5650.md):

<a name="leviticus_25_40"></a>Leviticus 25:40

But as a [śāḵîr](../../strongs/h/h7916.md), and as a [tôšāḇ](../../strongs/h/h8453.md), he shall be with thee, and shall ['abad](../../strongs/h/h5647.md) thee unto the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md).

<a name="leviticus_25_41"></a>Leviticus 25:41

And then shall he [yāṣā'](../../strongs/h/h3318.md) from thee, both he and his [ben](../../strongs/h/h1121.md) with him, and shall [shuwb](../../strongs/h/h7725.md) unto his own [mišpāḥâ](../../strongs/h/h4940.md), and unto the ['achuzzah](../../strongs/h/h272.md) of his ['ab](../../strongs/h/h1.md) shall he [shuwb](../../strongs/h/h7725.md).

<a name="leviticus_25_42"></a>Leviticus 25:42

For they are my ['ebed](../../strongs/h/h5650.md), which I [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): they shall not be [māḵar](../../strongs/h/h4376.md) [mimkereṯ](../../strongs/h/h4466.md) ['ebed](../../strongs/h/h5650.md).

<a name="leviticus_25_43"></a>Leviticus 25:43

Thou shalt not [radah](../../strongs/h/h7287.md) over him with [pereḵ](../../strongs/h/h6531.md); but shalt [yare'](../../strongs/h/h3372.md) thy ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_25_44"></a>Leviticus 25:44

Both thy ['ebed](../../strongs/h/h5650.md), and thy ['amah](../../strongs/h/h519.md), which thou shalt have, shall be of the [gowy](../../strongs/h/h1471.md) that are [cabiyb](../../strongs/h/h5439.md) you; of them shall ye [qānâ](../../strongs/h/h7069.md) ['ebed](../../strongs/h/h5650.md) and ['amah](../../strongs/h/h519.md).

<a name="leviticus_25_45"></a>Leviticus 25:45

Moreover of the [ben](../../strongs/h/h1121.md) of the [tôšāḇ](../../strongs/h/h8453.md) that do [guwr](../../strongs/h/h1481.md) among you, of them shall ye [qānâ](../../strongs/h/h7069.md), and of their [mišpāḥâ](../../strongs/h/h4940.md) that are with you, which they [yalad](../../strongs/h/h3205.md) in your ['erets](../../strongs/h/h776.md): and they shall be your ['achuzzah](../../strongs/h/h272.md).

<a name="leviticus_25_46"></a>Leviticus 25:46

And ye shall take them as a [nāḥal](../../strongs/h/h5157.md) for your [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) you, to [yarash](../../strongs/h/h3423.md) them for an ['achuzzah](../../strongs/h/h272.md); they shall be your ['abad](../../strongs/h/h5647.md) ['owlam](../../strongs/h/h5769.md): but over your ['ach](../../strongs/h/h251.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ye shall not [radah](../../strongs/h/h7287.md) ['iysh](../../strongs/h/h376.md) over ['ach](../../strongs/h/h251.md) with [pereḵ](../../strongs/h/h6531.md).

<a name="leviticus_25_47"></a>Leviticus 25:47

And if a [ger](../../strongs/h/h1616.md) or [tôšāḇ](../../strongs/h/h8453.md) [nāśaḡ](../../strongs/h/h5381.md) by [yad](../../strongs/h/h3027.md), and thy ['ach](../../strongs/h/h251.md) that by him [mûḵ](../../strongs/h/h4134.md), and [māḵar](../../strongs/h/h4376.md) himself unto the [ger](../../strongs/h/h1616.md) or [tôšāḇ](../../strongs/h/h8453.md) by thee, or to the [ʿēqer](../../strongs/h/h6133.md) of the [ger](../../strongs/h/h1616.md) [mišpāḥâ](../../strongs/h/h4940.md):

<a name="leviticus_25_48"></a>Leviticus 25:48

['aḥar](../../strongs/h/h310.md) that he is [māḵar](../../strongs/h/h4376.md) he may be [gᵊ'ullâ](../../strongs/h/h1353.md); one of his ['ach](../../strongs/h/h251.md) may [gā'al](../../strongs/h/h1350.md) him:

<a name="leviticus_25_49"></a>Leviticus 25:49

Either his [dôḏ](../../strongs/h/h1730.md), or his [dôḏ](../../strongs/h/h1730.md) [ben](../../strongs/h/h1121.md), may [gā'al](../../strongs/h/h1350.md) him, or any that is [šᵊ'ēr](../../strongs/h/h7607.md) of [basar](../../strongs/h/h1320.md) unto him of his [mišpāḥâ](../../strongs/h/h4940.md) may [gā'al](../../strongs/h/h1350.md) him; or if he be [nāśaḡ](../../strongs/h/h5381.md), he may [gā'al](../../strongs/h/h1350.md) [yad](../../strongs/h/h3027.md).

<a name="leviticus_25_50"></a>Leviticus 25:50

And he shall [chashab](../../strongs/h/h2803.md) with him that [qānâ](../../strongs/h/h7069.md) him from the [šānâ](../../strongs/h/h8141.md) that he was [māḵar](../../strongs/h/h4376.md) to him unto the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md): and the [keceph](../../strongs/h/h3701.md) of his [mimkār](../../strongs/h/h4465.md) shall be according unto the [mispār](../../strongs/h/h4557.md) of [šānâ](../../strongs/h/h8141.md), according to the [yowm](../../strongs/h/h3117.md) of a [śāḵîr](../../strongs/h/h7916.md) shall it be with him.

<a name="leviticus_25_51"></a>Leviticus 25:51

If there be yet [rab](../../strongs/h/h7227.md) [šānâ](../../strongs/h/h8141.md) behind, [peh](../../strongs/h/h6310.md) unto them he shall [shuwb](../../strongs/h/h7725.md) the price of his [gᵊ'ullâ](../../strongs/h/h1353.md) out of the [keceph](../../strongs/h/h3701.md) that he was [miqnâ](../../strongs/h/h4736.md) for.

<a name="leviticus_25_52"></a>Leviticus 25:52

And if there [šā'ar](../../strongs/h/h7604.md) but [mᵊʿaṭ](../../strongs/h/h4592.md) [šānâ](../../strongs/h/h8141.md) unto the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md), then he shall [chashab](../../strongs/h/h2803.md) with him, and [peh](../../strongs/h/h6310.md) unto his [šānâ](../../strongs/h/h8141.md) shall he [shuwb](../../strongs/h/h7725.md) the [gᵊ'ullâ](../../strongs/h/h1353.md).

<a name="leviticus_25_53"></a>Leviticus 25:53

And as a [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) [śāḵîr](../../strongs/h/h7916.md) shall he be with him: and the other shall not [radah](../../strongs/h/h7287.md) with [pereḵ](../../strongs/h/h6531.md) over him in thy ['ayin](../../strongs/h/h5869.md).

<a name="leviticus_25_54"></a>Leviticus 25:54

And if he be not [gā'al](../../strongs/h/h1350.md) in these, then he shall [yāṣā'](../../strongs/h/h3318.md) in the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md), both he, and his [ben](../../strongs/h/h1121.md) with him.

<a name="leviticus_25_55"></a>Leviticus 25:55

For unto me the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) are ['ebed](../../strongs/h/h5650.md); they are my ['ebed](../../strongs/h/h5650.md) whom I [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 24](leviticus_24.md) - [Leviticus 26](leviticus_26.md)