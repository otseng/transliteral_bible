# [Leviticus 10](https://www.blueletterbible.org/kjv/lev/10)

<a name="leviticus_10_1"></a>Leviticus 10:1

And [Nāḏāḇ](../../strongs/h/h5070.md) and ['Ăḇîhû'](../../strongs/h/h30.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) ['iysh](../../strongs/h/h376.md) of them his [maḥtâ](../../strongs/h/h4289.md), and [nathan](../../strongs/h/h5414.md) ['esh](../../strongs/h/h784.md) therein, and [śûm](../../strongs/h/h7760.md) [qᵊṭōreṯ](../../strongs/h/h7004.md) thereon, and [qāraḇ](../../strongs/h/h7126.md) [zûr](../../strongs/h/h2114.md) ['esh](../../strongs/h/h784.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), which he [tsavah](../../strongs/h/h6680.md) them not.

<a name="leviticus_10_2"></a>Leviticus 10:2

And there [yāṣā'](../../strongs/h/h3318.md) ['esh](../../strongs/h/h784.md) from [Yĕhovah](../../strongs/h/h3068.md), and ['akal](../../strongs/h/h398.md) them, and they [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_10_3"></a>Leviticus 10:3

Then [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), This is it that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), I will be [qadash](../../strongs/h/h6942.md) in them that [qarowb](../../strongs/h/h7138.md) me, and [paniym](../../strongs/h/h6440.md) all the ['am](../../strongs/h/h5971.md) I will be [kabad](../../strongs/h/h3513.md). And ['Ahărôn](../../strongs/h/h175.md) [damam](../../strongs/h/h1826.md).

<a name="leviticus_10_4"></a>Leviticus 10:4

And [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) [Mîšā'ēl](../../strongs/h/h4332.md) and ['Ĕlîṣāp̄ān](../../strongs/h/h469.md), the [ben](../../strongs/h/h1121.md) of [ʿUzzî'ēl](../../strongs/h/h5816.md) the [dôḏ](../../strongs/h/h1730.md) of ['Ahărôn](../../strongs/h/h175.md), and ['āmar](../../strongs/h/h559.md) unto them, [qāraḇ](../../strongs/h/h7126.md), [nasa'](../../strongs/h/h5375.md) your ['ach](../../strongs/h/h251.md) from [paniym](../../strongs/h/h6440.md) the [qodesh](../../strongs/h/h6944.md) [ḥûṣ](../../strongs/h/h2351.md) of the [maḥănê](../../strongs/h/h4264.md).

<a name="leviticus_10_5"></a>Leviticus 10:5

So they [qāraḇ](../../strongs/h/h7126.md), and [nasa'](../../strongs/h/h5375.md) them in their [kĕthoneth](../../strongs/h/h3801.md) [ḥûṣ](../../strongs/h/h2351.md) of the [maḥănê](../../strongs/h/h4264.md); as [Mōshe](../../strongs/h/h4872.md) had [dabar](../../strongs/h/h1696.md).

<a name="leviticus_10_6"></a>Leviticus 10:6

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), and unto ['Elʿāzār](../../strongs/h/h499.md) and unto ['Îṯāmār](../../strongs/h/h385.md), his [ben](../../strongs/h/h1121.md), [pāraʿ](../../strongs/h/h6544.md) not your [ro'sh](../../strongs/h/h7218.md), neither [pāram](../../strongs/h/h6533.md) your [beḡeḏ](../../strongs/h/h899.md); lest ye [muwth](../../strongs/h/h4191.md), and lest [qāṣap̄](../../strongs/h/h7107.md) come upon all the ['edah](../../strongs/h/h5712.md): but let your ['ach](../../strongs/h/h251.md), the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [bāḵâ](../../strongs/h/h1058.md) the [śᵊrēp̄â](../../strongs/h/h8316.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [śārap̄](../../strongs/h/h8313.md).

<a name="leviticus_10_7"></a>Leviticus 10:7

And ye shall not [yāṣā'](../../strongs/h/h3318.md) from the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), lest ye [muwth](../../strongs/h/h4191.md): for the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md) of [Yĕhovah](../../strongs/h/h3068.md) is upon you. And they ['asah](../../strongs/h/h6213.md) according to the [dabar](../../strongs/h/h1697.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_10_8"></a>Leviticus 10:8

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_10_9"></a>Leviticus 10:9

Do not [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md) nor [šēḵār](../../strongs/h/h7941.md), thou, nor thy [ben](../../strongs/h/h1121.md) with thee, when ye [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), lest ye [muwth](../../strongs/h/h4191.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout your [dôr](../../strongs/h/h1755.md):

<a name="leviticus_10_10"></a>Leviticus 10:10

And that ye may [bāḏal](../../strongs/h/h914.md) between [qodesh](../../strongs/h/h6944.md) and [ḥōl](../../strongs/h/h2455.md), and between [tame'](../../strongs/h/h2931.md) and [tahowr](../../strongs/h/h2889.md);

<a name="leviticus_10_11"></a>Leviticus 10:11

And that ye may [yārâ](../../strongs/h/h3384.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) all the [choq](../../strongs/h/h2706.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) unto them by the [yad](../../strongs/h/h3027.md) of [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_10_12"></a>Leviticus 10:12

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), and unto ['Elʿāzār](../../strongs/h/h499.md) and unto ['Îṯāmār](../../strongs/h/h385.md), his [ben](../../strongs/h/h1121.md) that [yāṯar](../../strongs/h/h3498.md), [laqach](../../strongs/h/h3947.md) the [minchah](../../strongs/h/h4503.md) that [yāṯar](../../strongs/h/h3498.md) of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['akal](../../strongs/h/h398.md) it [maṣṣâ](../../strongs/h/h4682.md) beside the [mizbeach](../../strongs/h/h4196.md): for it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md):

<a name="leviticus_10_13"></a>Leviticus 10:13

And ye shall ['akal](../../strongs/h/h398.md) it in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md), because it is thy [choq](../../strongs/h/h2706.md), and thy [ben](../../strongs/h/h1121.md) [choq](../../strongs/h/h2706.md), of the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md): for so I am [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_10_14"></a>Leviticus 10:14

And the [tᵊnûp̄â](../../strongs/h/h8573.md) [ḥāzê](../../strongs/h/h2373.md) and [tᵊrûmâ](../../strongs/h/h8641.md) [šôq](../../strongs/h/h7785.md) shall ye ['akal](../../strongs/h/h398.md) in a [tahowr](../../strongs/h/h2889.md) [maqowm](../../strongs/h/h4725.md); thou, and thy [ben](../../strongs/h/h1121.md), and thy [bath](../../strongs/h/h1323.md) with thee: for they be thy [choq](../../strongs/h/h2706.md), and thy [ben](../../strongs/h/h1121.md) [choq](../../strongs/h/h2706.md), which are [nathan](../../strongs/h/h5414.md) out of the [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="leviticus_10_15"></a>Leviticus 10:15

The [tᵊrûmâ](../../strongs/h/h8641.md) [šôq](../../strongs/h/h7785.md) and the [tᵊnûp̄â](../../strongs/h/h8573.md) [ḥāzê](../../strongs/h/h2373.md) shall they [bow'](../../strongs/h/h935.md) with the ['iššê](../../strongs/h/h801.md) of the [cheleb](../../strongs/h/h2459.md), to [nûp̄](../../strongs/h/h5130.md) it for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and it shall be thine, and thy [ben](../../strongs/h/h1121.md) with thee, by a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md); as [Yĕhovah](../../strongs/h/h3068.md) hath [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_10_16"></a>Leviticus 10:16

And [Mōshe](../../strongs/h/h4872.md) [darash](../../strongs/h/h1875.md) [darash](../../strongs/h/h1875.md) the [śāʿîr](../../strongs/h/h8163.md) of the [chatta'ath](../../strongs/h/h2403.md), and, behold, it was [śārap̄](../../strongs/h/h8313.md): and he was [qāṣap̄](../../strongs/h/h7107.md) with ['Elʿāzār](../../strongs/h/h499.md) and ['Îṯāmār](../../strongs/h/h385.md), the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) which were [yāṯar](../../strongs/h/h3498.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_10_17"></a>Leviticus 10:17

Wherefore have ye not ['akal](../../strongs/h/h398.md) the [chatta'ath](../../strongs/h/h2403.md) in the [qodesh](../../strongs/h/h6944.md) [maqowm](../../strongs/h/h4725.md), seeing it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), and God hath [nathan](../../strongs/h/h5414.md) it you to [nasa'](../../strongs/h/h5375.md) the ['avon](../../strongs/h/h5771.md) of the ['edah](../../strongs/h/h5712.md), to make [kāp̄ar](../../strongs/h/h3722.md) for them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md)?

<a name="leviticus_10_18"></a>Leviticus 10:18

[hen](../../strongs/h/h2005.md), the [dam](../../strongs/h/h1818.md) of it was not [bow'](../../strongs/h/h935.md) in [pᵊnîmâ](../../strongs/h/h6441.md) the [qodesh](../../strongs/h/h6944.md): ye should have ['akal](../../strongs/h/h398.md) ['akal](../../strongs/h/h398.md) it in the [qodesh](../../strongs/h/h6944.md), as I [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_10_19"></a>Leviticus 10:19

And ['Ahărôn](../../strongs/h/h175.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), Behold, this [yowm](../../strongs/h/h3117.md) have they [qāraḇ](../../strongs/h/h7126.md) their [chatta'ath](../../strongs/h/h2403.md) and their an [ʿōlâ](../../strongs/h/h5930.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and such things have [qārā'](../../strongs/h/h7122.md) me: and if I had ['akal](../../strongs/h/h398.md) the [chatta'ath](../../strongs/h/h2403.md) to [yowm](../../strongs/h/h3117.md), should it have been [yatab](../../strongs/h/h3190.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md)?

<a name="leviticus_10_20"></a>Leviticus 10:20

And when [Mōshe](../../strongs/h/h4872.md) [shama'](../../strongs/h/h8085.md) that, he was [yatab](../../strongs/h/h3190.md) ['ayin](../../strongs/h/h5869.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 9](leviticus_9.md) - [Leviticus 11](leviticus_11.md)