# [Leviticus 6](https://www.blueletterbible.org/kjv/lev/6)

<a name="leviticus_6_1"></a>Leviticus 6:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_6_2"></a>Leviticus 6:2

If a [nephesh](../../strongs/h/h5315.md) [chata'](../../strongs/h/h2398.md), and [māʿal](../../strongs/h/h4603.md) a [maʿal](../../strongs/h/h4604.md) against [Yĕhovah](../../strongs/h/h3068.md), and [kāḥaš](../../strongs/h/h3584.md) unto his [ʿāmîṯ](../../strongs/h/h5997.md) in that which was [piqqāḏôn](../../strongs/h/h6487.md) him, or in [tᵊśûmeṯ](../../strongs/h/h8667.md) [yad](../../strongs/h/h3027.md), or in a thing [gāzēl](../../strongs/h/h1498.md), or hath [ʿāšaq](../../strongs/h/h6231.md) his [ʿāmîṯ](../../strongs/h/h5997.md);

<a name="leviticus_6_3"></a>Leviticus 6:3

Or have [māṣā'](../../strongs/h/h4672.md) that which was ['ăḇēḏâ](../../strongs/h/h9.md), and [kāḥaš](../../strongs/h/h3584.md) concerning it, and [shaba'](../../strongs/h/h7650.md) [sheqer](../../strongs/h/h8267.md); in any of all these that an ['adam](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), [chata'](../../strongs/h/h2398.md) therein:

<a name="leviticus_6_4"></a>Leviticus 6:4

Then it shall be, because he hath [chata'](../../strongs/h/h2398.md), and is ['asham](../../strongs/h/h816.md), that he shall [shuwb](../../strongs/h/h7725.md) [gᵊzēlâ](../../strongs/h/h1500.md) which he [gāzal](../../strongs/h/h1497.md), or the [ʿōšeq](../../strongs/h/h6233.md) which he hath [ʿāšaq](../../strongs/h/h6231.md), or that which was [piqqāḏôn](../../strongs/h/h6487.md) him to [paqad](../../strongs/h/h6485.md), or the ['ăḇēḏâ](../../strongs/h/h9.md) which he [māṣā'](../../strongs/h/h4672.md),

<a name="leviticus_6_5"></a>Leviticus 6:5

Or all that about which he hath [shaba'](../../strongs/h/h7650.md) [sheqer](../../strongs/h/h8267.md); he shall even [shalam](../../strongs/h/h7999.md) it in the [ro'sh](../../strongs/h/h7218.md), and shall [yāsap̄](../../strongs/h/h3254.md) the fifth part thereto, and [nathan](../../strongs/h/h5414.md) it unto him to whom it appertaineth, in the [yowm](../../strongs/h/h3117.md) of his ['ašmâ](../../strongs/h/h819.md).

<a name="leviticus_6_6"></a>Leviticus 6:6

And he shall [bow'](../../strongs/h/h935.md) his ['āšām](../../strongs/h/h817.md) unto [Yĕhovah](../../strongs/h/h3068.md), an ['ayil](../../strongs/h/h352.md) [tamiym](../../strongs/h/h8549.md) out of the [tso'n](../../strongs/h/h6629.md), with thy [ʿēreḵ](../../strongs/h/h6187.md), for an ['āšām](../../strongs/h/h817.md), unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_6_7"></a>Leviticus 6:7

And the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): and it shall be [sālaḥ](../../strongs/h/h5545.md) him for any ['echad](../../strongs/h/h259.md) of all that he hath ['asah](../../strongs/h/h6213.md) in ['ašmâ](../../strongs/h/h819.md) therein.

<a name="leviticus_6_8"></a>Leviticus 6:8

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_6_9"></a>Leviticus 6:9

[tsavah](../../strongs/h/h6680.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), This is the [towrah](../../strongs/h/h8451.md) of the an [ʿōlâ](../../strongs/h/h5930.md): It is the an [ʿōlâ](../../strongs/h/h5930.md), because of the [môqḏâ](../../strongs/h/h4169.md) upon thea [mizbeach](../../strongs/h/h4196.md) all [layil](../../strongs/h/h3915.md) unto the [boqer](../../strongs/h/h1242.md), and the ['esh](../../strongs/h/h784.md) of thea [mizbeach](../../strongs/h/h4196.md) shall be [yāqaḏ](../../strongs/h/h3344.md) in it.

<a name="leviticus_6_10"></a>Leviticus 6:10

And the [kōhēn](../../strongs/h/h3548.md) shall [labash](../../strongs/h/h3847.md) on his [baḏ](../../strongs/h/h906.md) [maḏ](../../strongs/h/h4055.md), and his [baḏ](../../strongs/h/h906.md) [miḵnās](../../strongs/h/h4370.md) shall he [labash](../../strongs/h/h3847.md) upon his [basar](../../strongs/h/h1320.md), and [ruwm](../../strongs/h/h7311.md) the [dešen](../../strongs/h/h1880.md) which the ['esh](../../strongs/h/h784.md) hath ['akal](../../strongs/h/h398.md) with the an [ʿōlâ](../../strongs/h/h5930.md) on thea [mizbeach](../../strongs/h/h4196.md), and he shall [śûm](../../strongs/h/h7760.md) them beside thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_6_11"></a>Leviticus 6:11

And he shall [pāšaṭ](../../strongs/h/h6584.md) his [beḡeḏ](../../strongs/h/h899.md), and [labash](../../strongs/h/h3847.md) ['aḥēr](../../strongs/h/h312.md) [beḡeḏ](../../strongs/h/h899.md), and [yāṣā'](../../strongs/h/h3318.md) the [dešen](../../strongs/h/h1880.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md) unto a [tahowr](../../strongs/h/h2889.md) [maqowm](../../strongs/h/h4725.md).

<a name="leviticus_6_12"></a>Leviticus 6:12

And the ['esh](../../strongs/h/h784.md) upon thea [mizbeach](../../strongs/h/h4196.md) shall be [yāqaḏ](../../strongs/h/h3344.md) in it; it shall not be [kāḇâ](../../strongs/h/h3518.md): and the [kōhēn](../../strongs/h/h3548.md) shall [bāʿar](../../strongs/h/h1197.md) ['ets](../../strongs/h/h6086.md) on it every [boqer](../../strongs/h/h1242.md), and lay the an [ʿōlâ](../../strongs/h/h5930.md) ['arak](../../strongs/h/h6186.md) upon it; and he shall [qāṭar](../../strongs/h/h6999.md) thereon the [cheleb](../../strongs/h/h2459.md) of the [šelem](../../strongs/h/h8002.md).

<a name="leviticus_6_13"></a>Leviticus 6:13

The ['esh](../../strongs/h/h784.md) shall [tāmîḏ](../../strongs/h/h8548.md) be [yāqaḏ](../../strongs/h/h3344.md) upon thea [mizbeach](../../strongs/h/h4196.md); it shall never [kāḇâ](../../strongs/h/h3518.md).

<a name="leviticus_6_14"></a>Leviticus 6:14

And this is the [towrah](../../strongs/h/h8451.md) of the [minchah](../../strongs/h/h4503.md): the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) shall [qāraḇ](../../strongs/h/h7126.md) it [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), [paniym](../../strongs/h/h6440.md) thea [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_6_15"></a>Leviticus 6:15

And he shall [ruwm](../../strongs/h/h7311.md) of it his [qōmeṣ](../../strongs/h/h7062.md), of the [sōleṯ](../../strongs/h/h5560.md) of the [minchah](../../strongs/h/h4503.md), and of the [šemen](../../strongs/h/h8081.md) thereof, and all the [lᵊḇônâ](../../strongs/h/h3828.md) which is upon the [minchah](../../strongs/h/h4503.md), and shall [qāṭar](../../strongs/h/h6999.md) it upon thea [mizbeach](../../strongs/h/h4196.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md), even the ['azkārâ](../../strongs/h/h234.md) of it, unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_6_16"></a>Leviticus 6:16

And the [yāṯar](../../strongs/h/h3498.md) thereof shall ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md) ['akal](../../strongs/h/h398.md): with [maṣṣâ](../../strongs/h/h4682.md) shall it be ['akal](../../strongs/h/h398.md) in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md); in the [ḥāṣēr](../../strongs/h/h2691.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) they shall ['akal](../../strongs/h/h398.md) it.

<a name="leviticus_6_17"></a>Leviticus 6:17

It shall not be ['āp̄â](../../strongs/h/h644.md) with [ḥāmēṣ](../../strongs/h/h2557.md). I have [nathan](../../strongs/h/h5414.md) it unto them for their [cheleq](../../strongs/h/h2506.md) of my ['iššê](../../strongs/h/h801.md); it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), as is the [chatta'ath](../../strongs/h/h2403.md), and as the ['āšām](../../strongs/h/h817.md).

<a name="leviticus_6_18"></a>Leviticus 6:18

All the [zāḵār](../../strongs/h/h2145.md) among the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) shall ['akal](../../strongs/h/h398.md) of it. It shall be a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md) in your [dôr](../../strongs/h/h1755.md) concerning the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md): every one that [naga'](../../strongs/h/h5060.md) them shall be [qadash](../../strongs/h/h6942.md).

<a name="leviticus_6_19"></a>Leviticus 6:19

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_6_20"></a>Leviticus 6:20

This is the [qorban](../../strongs/h/h7133.md) of ['Ahărôn](../../strongs/h/h175.md) and of his [ben](../../strongs/h/h1121.md), which they shall [qāraḇ](../../strongs/h/h7126.md) unto [Yĕhovah](../../strongs/h/h3068.md) in the [yowm](../../strongs/h/h3117.md) when he is [māšaḥ](../../strongs/h/h4886.md); the tenth part of an ['êp̄â](../../strongs/h/h374.md) of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md) [tāmîḏ](../../strongs/h/h8548.md), half of it in the [boqer](../../strongs/h/h1242.md), and half thereof at ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_6_21"></a>Leviticus 6:21

In a [maḥăḇaṯ](../../strongs/h/h4227.md) it shall be ['asah](../../strongs/h/h6213.md) with [šemen](../../strongs/h/h8081.md); and when it is [rāḇaḵ](../../strongs/h/h7246.md), thou shalt [bow'](../../strongs/h/h935.md) it in: and the [tup̄înîm](../../strongs/h/h8601.md) [paṯ](../../strongs/h/h6595.md) of the [minchah](../../strongs/h/h4503.md) shalt thou [qāraḇ](../../strongs/h/h7126.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_6_22"></a>Leviticus 6:22

And the [kōhēn](../../strongs/h/h3548.md) of his [ben](../../strongs/h/h1121.md) that is [mashiyach](../../strongs/h/h4899.md) in his stead shall ['asah](../../strongs/h/h6213.md) it: it is a [choq](../../strongs/h/h2706.md) ['owlam](../../strongs/h/h5769.md) unto [Yĕhovah](../../strongs/h/h3068.md); it shall be [kālîl](../../strongs/h/h3632.md) [qāṭar](../../strongs/h/h6999.md).

<a name="leviticus_6_23"></a>Leviticus 6:23

For every [minchah](../../strongs/h/h4503.md) for the [kōhēn](../../strongs/h/h3548.md) shall be [kālîl](../../strongs/h/h3632.md): it shall not be ['akal](../../strongs/h/h398.md).

<a name="leviticus_6_24"></a>Leviticus 6:24

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_6_25"></a>Leviticus 6:25

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md) and to his [ben](../../strongs/h/h1121.md), ['āmar](../../strongs/h/h559.md), This is the [towrah](../../strongs/h/h8451.md) of the [chatta'ath](../../strongs/h/h2403.md): In the [maqowm](../../strongs/h/h4725.md) where the an [ʿōlâ](../../strongs/h/h5930.md) is [šāḥaṭ](../../strongs/h/h7819.md) shall the [chatta'ath](../../strongs/h/h2403.md) be [šāḥaṭ](../../strongs/h/h7819.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md): it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_6_26"></a>Leviticus 6:26

The [kōhēn](../../strongs/h/h3548.md) that offereth it for [chata'](../../strongs/h/h2398.md) shall ['akal](../../strongs/h/h398.md) it: in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md) shall it be ['akal](../../strongs/h/h398.md), in the [ḥāṣēr](../../strongs/h/h2691.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_6_27"></a>Leviticus 6:27

Whatsoever shall [naga'](../../strongs/h/h5060.md) the [basar](../../strongs/h/h1320.md) thereof shall be [qadash](../../strongs/h/h6942.md): and when there is [nāzâ](../../strongs/h/h5137.md) of the [dam](../../strongs/h/h1818.md) thereof upon any [beḡeḏ](../../strongs/h/h899.md), thou shalt [kāḇas](../../strongs/h/h3526.md) that whereon it was [nāzâ](../../strongs/h/h5137.md) in the [qadowsh](../../strongs/h/h6918.md) [maqowm](../../strongs/h/h4725.md).

<a name="leviticus_6_28"></a>Leviticus 6:28

But the [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md) wherein it is [bāšal](../../strongs/h/h1310.md) shall be [shabar](../../strongs/h/h7665.md): and if it be [bāšal](../../strongs/h/h1310.md) in a [nᵊḥšeṯ](../../strongs/h/h5178.md) [kĕliy](../../strongs/h/h3627.md), it shall be both [māraq](../../strongs/h/h4838.md), and [šāṭap̄](../../strongs/h/h7857.md) in [mayim](../../strongs/h/h4325.md).

<a name="leviticus_6_29"></a>Leviticus 6:29

All the [zāḵār](../../strongs/h/h2145.md) among the [kōhēn](../../strongs/h/h3548.md) shall ['akal](../../strongs/h/h398.md) thereof: it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_6_30"></a>Leviticus 6:30

And no [chatta'ath](../../strongs/h/h2403.md), whereof any of the [dam](../../strongs/h/h1818.md) is [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md) to [kāp̄ar](../../strongs/h/h3722.md) withal in the [qodesh](../../strongs/h/h6944.md), shall be ['akal](../../strongs/h/h398.md): it shall be [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 5](leviticus_5.md) - [Leviticus 7](leviticus_7.md)