# [Leviticus 27](https://www.blueletterbible.org/kjv/lev/27)

<a name="leviticus_27_1"></a>Leviticus 27:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_27_2"></a>Leviticus 27:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When an ['iysh](../../strongs/h/h376.md) shall make a [pala'](../../strongs/h/h6381.md) [neḏer](../../strongs/h/h5088.md), the [nephesh](../../strongs/h/h5315.md) shall be for [Yĕhovah](../../strongs/h/h3068.md) by thy [ʿēreḵ](../../strongs/h/h6187.md).

<a name="leviticus_27_3"></a>Leviticus 27:3

And thy [ʿēreḵ](../../strongs/h/h6187.md) shall be of the [zāḵār](../../strongs/h/h2145.md) from twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) even unto sixty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), even thy [ʿēreḵ](../../strongs/h/h6187.md) shall be fifty [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), after the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_27_4"></a>Leviticus 27:4

And if it be a [nᵊqēḇâ](../../strongs/h/h5347.md), then thy [ʿēreḵ](../../strongs/h/h6187.md) shall be thirty [šeqel](../../strongs/h/h8255.md).

<a name="leviticus_27_5"></a>Leviticus 27:5

And if it be from five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) even unto twenty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), then thy [ʿēreḵ](../../strongs/h/h6187.md) shall be of the [zāḵār](../../strongs/h/h2145.md) twenty [šeqel](../../strongs/h/h8255.md), and for the [nᵊqēḇâ](../../strongs/h/h5347.md) ten [šeqel](../../strongs/h/h8255.md).

<a name="leviticus_27_6"></a>Leviticus 27:6

And if it be from a [ḥōḏeš](../../strongs/h/h2320.md) [ben](../../strongs/h/h1121.md) even unto five [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md), then thy [ʿēreḵ](../../strongs/h/h6187.md) shall be of the [zāḵār](../../strongs/h/h2145.md) five [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md), and for the [nᵊqēḇâ](../../strongs/h/h5347.md) thy [ʿēreḵ](../../strongs/h/h6187.md) shall be three [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md).

<a name="leviticus_27_7"></a>Leviticus 27:7

And if it be from sixty [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) and [maʿal](../../strongs/h/h4605.md); if it be a [zāḵār](../../strongs/h/h2145.md), then thy [ʿēreḵ](../../strongs/h/h6187.md) shall be fifteen [šeqel](../../strongs/h/h8255.md), and for the [nᵊqēḇâ](../../strongs/h/h5347.md) ten [šeqel](../../strongs/h/h8255.md).

<a name="leviticus_27_8"></a>Leviticus 27:8

But if he be [mûḵ](../../strongs/h/h4134.md) than thy [ʿēreḵ](../../strongs/h/h6187.md), then he shall ['amad](../../strongs/h/h5975.md) himself [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md), and the [kōhēn](../../strongs/h/h3548.md) shall ['arak](../../strongs/h/h6186.md) him; [peh](../../strongs/h/h6310.md) his [yad](../../strongs/h/h3027.md) [nāśaḡ](../../strongs/h/h5381.md) that [nāḏar](../../strongs/h/h5087.md) shall the [kōhēn](../../strongs/h/h3548.md) ['arak](../../strongs/h/h6186.md) him.

<a name="leviticus_27_9"></a>Leviticus 27:9

And if it be a [bĕhemah](../../strongs/h/h929.md), whereof men [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), all that any man [nathan](../../strongs/h/h5414.md) of such unto [Yĕhovah](../../strongs/h/h3068.md) shall be [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_27_10"></a>Leviticus 27:10

He shall not [ḥālap̄](../../strongs/h/h2498.md) it, nor [mûr](../../strongs/h/h4171.md) it, a [towb](../../strongs/h/h2896.md) for a [ra'](../../strongs/h/h7451.md), or a [ra'](../../strongs/h/h7451.md) for a [towb](../../strongs/h/h2896.md): and if he shall at all [mûr](../../strongs/h/h4171.md) [bĕhemah](../../strongs/h/h929.md) for [bĕhemah](../../strongs/h/h929.md), then it and the [tᵊmûrâ](../../strongs/h/h8545.md) thereof shall be [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_27_11"></a>Leviticus 27:11

And if it be any [tame'](../../strongs/h/h2931.md) [bĕhemah](../../strongs/h/h929.md), of which they do not [qāraḇ](../../strongs/h/h7126.md) a [qorban](../../strongs/h/h7133.md) unto [Yĕhovah](../../strongs/h/h3068.md), then he shall ['amad](../../strongs/h/h5975.md) the [bĕhemah](../../strongs/h/h929.md) [paniym](../../strongs/h/h6440.md) the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_27_12"></a>Leviticus 27:12

And the [kōhēn](../../strongs/h/h3548.md) shall ['arak](../../strongs/h/h6186.md) it, whether it be [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md): as thou [ʿēreḵ](../../strongs/h/h6187.md) it, who art the [kōhēn](../../strongs/h/h3548.md), so shall it be.

<a name="leviticus_27_13"></a>Leviticus 27:13

But if he will at all [gā'al](../../strongs/h/h1350.md) it, then he shall [yāsap̄](../../strongs/h/h3254.md) a fifth part thereof unto thy [ʿēreḵ](../../strongs/h/h6187.md).

<a name="leviticus_27_14"></a>Leviticus 27:14

And when an ['iysh](../../strongs/h/h376.md) shall [qadash](../../strongs/h/h6942.md) his [bayith](../../strongs/h/h1004.md) to be [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md), then the [kōhēn](../../strongs/h/h3548.md) shall ['arak](../../strongs/h/h6186.md) it, whether it be [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md): as the [kōhēn](../../strongs/h/h3548.md) shall ['arak](../../strongs/h/h6186.md) it, so shall it [quwm](../../strongs/h/h6965.md).

<a name="leviticus_27_15"></a>Leviticus 27:15

And if he that [qadash](../../strongs/h/h6942.md) it will [gā'al](../../strongs/h/h1350.md) his [bayith](../../strongs/h/h1004.md), then he shall [yāsap̄](../../strongs/h/h3254.md) the fifth of the [keceph](../../strongs/h/h3701.md) of thy [ʿēreḵ](../../strongs/h/h6187.md) unto it, and it shall be his.

<a name="leviticus_27_16"></a>Leviticus 27:16

And if an ['iysh](../../strongs/h/h376.md) shall [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md) some part of a [sadeh](../../strongs/h/h7704.md) of his ['achuzzah](../../strongs/h/h272.md), then thy [ʿēreḵ](../../strongs/h/h6187.md) shall be [peh](../../strongs/h/h6310.md) to the [zera'](../../strongs/h/h2233.md) thereof: an [ḥōmer](../../strongs/h/h2563.md) of [śᵊʿōrâ](../../strongs/h/h8184.md) [zera'](../../strongs/h/h2233.md) at fifty [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md).

<a name="leviticus_27_17"></a>Leviticus 27:17

If he [qadash](../../strongs/h/h6942.md) his [sadeh](../../strongs/h/h7704.md) from the [šānâ](../../strongs/h/h8141.md) of [yôḇēl](../../strongs/h/h3104.md), according to thy [ʿēreḵ](../../strongs/h/h6187.md) it shall [quwm](../../strongs/h/h6965.md).

<a name="leviticus_27_18"></a>Leviticus 27:18

But if he [qadash](../../strongs/h/h6942.md) his [sadeh](../../strongs/h/h7704.md) ['aḥar](../../strongs/h/h310.md) the [yôḇēl](../../strongs/h/h3104.md), then the [kōhēn](../../strongs/h/h3548.md) shall [chashab](../../strongs/h/h2803.md) unto him the [keceph](../../strongs/h/h3701.md) [peh](../../strongs/h/h6310.md) to the [šānâ](../../strongs/h/h8141.md) that [yāṯar](../../strongs/h/h3498.md), even unto the [šānâ](../../strongs/h/h8141.md) of the [yôḇēl](../../strongs/h/h3104.md), and it shall be [gāraʿ](../../strongs/h/h1639.md) from thy [ʿēreḵ](../../strongs/h/h6187.md).

<a name="leviticus_27_19"></a>Leviticus 27:19

And if he that [qadash](../../strongs/h/h6942.md) the [sadeh](../../strongs/h/h7704.md) will in any [gā'al](../../strongs/h/h1350.md) [gā'al](../../strongs/h/h1350.md) it, then he shall [yāsap̄](../../strongs/h/h3254.md) the fifth of the [keceph](../../strongs/h/h3701.md) of thy [ʿēreḵ](../../strongs/h/h6187.md) unto it, and it shall be [quwm](../../strongs/h/h6965.md) to him.

<a name="leviticus_27_20"></a>Leviticus 27:20

And if he will not [gā'al](../../strongs/h/h1350.md) the [sadeh](../../strongs/h/h7704.md), or if he have [māḵar](../../strongs/h/h4376.md) the [sadeh](../../strongs/h/h7704.md) to ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md), it shall not be [gā'al](../../strongs/h/h1350.md) any more.

<a name="leviticus_27_21"></a>Leviticus 27:21

But the [sadeh](../../strongs/h/h7704.md), when it [yāṣā'](../../strongs/h/h3318.md) in the [yôḇēl](../../strongs/h/h3104.md), shall be [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md), as a [sadeh](../../strongs/h/h7704.md) [ḥērem](../../strongs/h/h2764.md); the ['achuzzah](../../strongs/h/h272.md) thereof shall be the [kōhēn](../../strongs/h/h3548.md).

<a name="leviticus_27_22"></a>Leviticus 27:22

And if a man [qadash](../../strongs/h/h6942.md) unto [Yĕhovah](../../strongs/h/h3068.md) a [sadeh](../../strongs/h/h7704.md) which he hath [miqnâ](../../strongs/h/h4736.md), which is not of the [sadeh](../../strongs/h/h7704.md) of his ['achuzzah](../../strongs/h/h272.md);

<a name="leviticus_27_23"></a>Leviticus 27:23

Then the [kōhēn](../../strongs/h/h3548.md) shall [chashab](../../strongs/h/h2803.md) unto him the [miḵsâ](../../strongs/h/h4373.md) of thy [ʿēreḵ](../../strongs/h/h6187.md), even unto the [šānâ](../../strongs/h/h8141.md) of the [yôḇēl](../../strongs/h/h3104.md): and he shall [nathan](../../strongs/h/h5414.md) thine [ʿēreḵ](../../strongs/h/h6187.md) in that [yowm](../../strongs/h/h3117.md), as a [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_27_24"></a>Leviticus 27:24

In the [šānâ](../../strongs/h/h8141.md) of the [yôḇēl](../../strongs/h/h3104.md) the [sadeh](../../strongs/h/h7704.md) shall [shuwb](../../strongs/h/h7725.md) unto him of whom it was [qānâ](../../strongs/h/h7069.md), even to him to whom the ['achuzzah](../../strongs/h/h272.md) of the ['erets](../../strongs/h/h776.md) did belong.

<a name="leviticus_27_25"></a>Leviticus 27:25

And all thy [ʿēreḵ](../../strongs/h/h6187.md) shall be according to the [šeqel](../../strongs/h/h8255.md) of the [qodesh](../../strongs/h/h6944.md): twenty [gērâ](../../strongs/h/h1626.md) shall be the [šeqel](../../strongs/h/h8255.md).

<a name="leviticus_27_26"></a>Leviticus 27:26

Only the [bᵊḵôr](../../strongs/h/h1060.md) of the [bĕhemah](../../strongs/h/h929.md), which should be [Yĕhovah](../../strongs/h/h3068.md) [bāḵar](../../strongs/h/h1069.md), no ['iysh](../../strongs/h/h376.md) shall [qadash](../../strongs/h/h6942.md) it; whether it be [showr](../../strongs/h/h7794.md), or [śê](../../strongs/h/h7716.md): it is [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_27_27"></a>Leviticus 27:27

And if it be of a [tame'](../../strongs/h/h2931.md) [bĕhemah](../../strongs/h/h929.md), then he shall [pāḏâ](../../strongs/h/h6299.md) it according to thine [ʿēreḵ](../../strongs/h/h6187.md), and shall [yāsap̄](../../strongs/h/h3254.md) a fifth of it thereto: or if it be not [gā'al](../../strongs/h/h1350.md), then it shall be [māḵar](../../strongs/h/h4376.md) according to thy [ʿēreḵ](../../strongs/h/h6187.md).

<a name="leviticus_27_28"></a>Leviticus 27:28

Notwithstanding no [ḥērem](../../strongs/h/h2764.md), that an ['iysh](../../strongs/h/h376.md) shall [ḥāram](../../strongs/h/h2763.md) unto [Yĕhovah](../../strongs/h/h3068.md) of all that he hath, both of ['adam](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md), and of the [sadeh](../../strongs/h/h7704.md) of his ['achuzzah](../../strongs/h/h272.md), shall be [māḵar](../../strongs/h/h4376.md) or [gā'al](../../strongs/h/h1350.md): every [ḥērem](../../strongs/h/h2764.md) is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_27_29"></a>Leviticus 27:29

None [ḥērem](../../strongs/h/h2764.md), which shall be [ḥāram](../../strongs/h/h2763.md) of ['adam](../../strongs/h/h120.md), shall be [pāḏâ](../../strongs/h/h6299.md); but shall be [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="leviticus_27_30"></a>Leviticus 27:30

And all the [maʿăśēr](../../strongs/h/h4643.md) of the ['erets](../../strongs/h/h776.md), whether of the [zera'](../../strongs/h/h2233.md) of the ['erets](../../strongs/h/h776.md), or of the [pĕriy](../../strongs/h/h6529.md) of the ['ets](../../strongs/h/h6086.md), is [Yĕhovah](../../strongs/h/h3068.md): it is [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_27_31"></a>Leviticus 27:31

And if an ['iysh](../../strongs/h/h376.md) will [gā'al](../../strongs/h/h1350.md) [gā'al](../../strongs/h/h1350.md) ought of his [maʿăśēr](../../strongs/h/h4643.md), he shall [yāsap̄](../../strongs/h/h3254.md) thereto the fifth thereof.

<a name="leviticus_27_32"></a>Leviticus 27:32

And concerning the [maʿăśēr](../../strongs/h/h4643.md) of the [bāqār](../../strongs/h/h1241.md), or of the [tso'n](../../strongs/h/h6629.md), even of whatsoever ['abar](../../strongs/h/h5674.md) under the [shebet](../../strongs/h/h7626.md), the tenth shall be [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_27_33"></a>Leviticus 27:33

He shall not [bāqar](../../strongs/h/h1239.md) whether it be [towb](../../strongs/h/h2896.md) or [ra'](../../strongs/h/h7451.md), neither shall he [mûr](../../strongs/h/h4171.md) it: and if he [mûr](../../strongs/h/h4171.md) it, then both it and the [tᵊmûrâ](../../strongs/h/h8545.md) thereof shall be [qodesh](../../strongs/h/h6944.md); it shall not be [gā'al](../../strongs/h/h1350.md).

<a name="leviticus_27_34"></a>Leviticus 27:34

These are the [mitsvah](../../strongs/h/h4687.md), which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md) for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) in [har](../../strongs/h/h2022.md) [Sînay](../../strongs/h/h5514.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 26](leviticus_26.md)