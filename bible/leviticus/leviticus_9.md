# [Leviticus 9](https://www.blueletterbible.org/kjv/lev/9)

<a name="leviticus_9_1"></a>Leviticus 9:1

And it came to pass on the eighth [yowm](../../strongs/h/h3117.md), that [Mōshe](../../strongs/h/h4872.md) [qara'](../../strongs/h/h7121.md) ['Ahărôn](../../strongs/h/h175.md) and his [ben](../../strongs/h/h1121.md), and the [zāqēn](../../strongs/h/h2205.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="leviticus_9_2"></a>Leviticus 9:2

And he ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [laqach](../../strongs/h/h3947.md) thee a [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [ʿēḡel](../../strongs/h/h5695.md) for a [chatta'ath](../../strongs/h/h2403.md), and an ['ayil](../../strongs/h/h352.md) for an [ʿōlâ](../../strongs/h/h5930.md), [tamiym](../../strongs/h/h8549.md), and [qāraḇ](../../strongs/h/h7126.md) them [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_9_3"></a>Leviticus 9:3

And unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) thou shalt [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) ye a [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md); and a [ʿēḡel](../../strongs/h/h5695.md) and a [keḇeś](../../strongs/h/h3532.md), both of the first [šānâ](../../strongs/h/h8141.md), [tamiym](../../strongs/h/h8549.md), for an [ʿōlâ](../../strongs/h/h5930.md);

<a name="leviticus_9_4"></a>Leviticus 9:4

Also a [showr](../../strongs/h/h7794.md) and an ['ayil](../../strongs/h/h352.md) for [šelem](../../strongs/h/h8002.md), to [zabach](../../strongs/h/h2076.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); and a [minchah](../../strongs/h/h4503.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md): for to [yowm](../../strongs/h/h3117.md) [Yĕhovah](../../strongs/h/h3068.md) will [ra'ah](../../strongs/h/h7200.md) unto you.

<a name="leviticus_9_5"></a>Leviticus 9:5

And they [laqach](../../strongs/h/h3947.md) that which [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md) [paniym](../../strongs/h/h6440.md) the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md): and all the ['edah](../../strongs/h/h5712.md) [qāraḇ](../../strongs/h/h7126.md) and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_9_6"></a>Leviticus 9:6

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md), This is the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) that ye should ['asah](../../strongs/h/h6213.md): and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) shall [ra'ah](../../strongs/h/h7200.md) unto you.

<a name="leviticus_9_7"></a>Leviticus 9:7

And [Mōshe](../../strongs/h/h4872.md) ['āmar](../../strongs/h/h559.md) unto ['Ahărôn](../../strongs/h/h175.md), [qāraḇ](../../strongs/h/h7126.md) unto the [mizbeach](../../strongs/h/h4196.md), and ['asah](../../strongs/h/h6213.md) thy [chatta'ath](../../strongs/h/h2403.md), and thy an [ʿōlâ](../../strongs/h/h5930.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for thyself, and for the ['am](../../strongs/h/h5971.md): and ['asah](../../strongs/h/h6213.md) the [qorban](../../strongs/h/h7133.md) of the ['am](../../strongs/h/h5971.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for them; as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_9_8"></a>Leviticus 9:8

['Ahărôn](../../strongs/h/h175.md) therefore [qāraḇ](../../strongs/h/h7126.md) unto the [mizbeach](../../strongs/h/h4196.md), and [šāḥaṭ](../../strongs/h/h7819.md) the [ʿēḡel](../../strongs/h/h5695.md) of the [chatta'ath](../../strongs/h/h2403.md), which was for himself.

<a name="leviticus_9_9"></a>Leviticus 9:9

And the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md) [qāraḇ](../../strongs/h/h7126.md) the [dam](../../strongs/h/h1818.md) unto him: and he [ṭāḇal](../../strongs/h/h2881.md) his ['etsba'](../../strongs/h/h676.md) in the [dam](../../strongs/h/h1818.md), and [nathan](../../strongs/h/h5414.md) it upon the [qeren](../../strongs/h/h7161.md) of the [mizbeach](../../strongs/h/h4196.md), and [yāṣaq](../../strongs/h/h3332.md) the [dam](../../strongs/h/h1818.md) at the [yᵊsôḏ](../../strongs/h/h3247.md) of the [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_9_10"></a>Leviticus 9:10

But the [cheleb](../../strongs/h/h2459.md), and the [kilyah](../../strongs/h/h3629.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md) of the [chatta'ath](../../strongs/h/h2403.md), he [qāṭar](../../strongs/h/h6999.md) upon the [mizbeach](../../strongs/h/h4196.md); as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) [Mōshe](../../strongs/h/h4872.md).

<a name="leviticus_9_11"></a>Leviticus 9:11

And the [basar](../../strongs/h/h1320.md) and the ['owr](../../strongs/h/h5785.md) he [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md) [ḥûṣ](../../strongs/h/h2351.md) the [maḥănê](../../strongs/h/h4264.md).

<a name="leviticus_9_12"></a>Leviticus 9:12

And he [šāḥaṭ](../../strongs/h/h7819.md) the an [ʿōlâ](../../strongs/h/h5930.md); and ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) [māṣā'](../../strongs/h/h4672.md) unto him the [dam](../../strongs/h/h1818.md), which he [zāraq](../../strongs/h/h2236.md) [cabiyb](../../strongs/h/h5439.md) upon the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_9_13"></a>Leviticus 9:13

And they [māṣā'](../../strongs/h/h4672.md) the an [ʿōlâ](../../strongs/h/h5930.md) unto him, with the [nēṯaḥ](../../strongs/h/h5409.md) thereof, and the [ro'sh](../../strongs/h/h7218.md): and he [qāṭar](../../strongs/h/h6999.md) them upon the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_9_14"></a>Leviticus 9:14

And he did [rāḥaṣ](../../strongs/h/h7364.md) the [qereḇ](../../strongs/h/h7130.md) and the [keraʿ](../../strongs/h/h3767.md), and [qāṭar](../../strongs/h/h6999.md) them upon the an [ʿōlâ](../../strongs/h/h5930.md) on the [mizbeach](../../strongs/h/h4196.md).

<a name="leviticus_9_15"></a>Leviticus 9:15

And he [qāraḇ](../../strongs/h/h7126.md) the ['am](../../strongs/h/h5971.md) [qorban](../../strongs/h/h7133.md), and [laqach](../../strongs/h/h3947.md) the [śāʿîr](../../strongs/h/h8163.md), which was the [chatta'ath](../../strongs/h/h2403.md) for the ['am](../../strongs/h/h5971.md), and [šāḥaṭ](../../strongs/h/h7819.md) it, and offered it for [chata'](../../strongs/h/h2398.md), as the [ri'šôn](../../strongs/h/h7223.md).

<a name="leviticus_9_16"></a>Leviticus 9:16

And he [qāraḇ](../../strongs/h/h7126.md) the an [ʿōlâ](../../strongs/h/h5930.md), and ['asah](../../strongs/h/h6213.md) it according to the [mishpat](../../strongs/h/h4941.md).

<a name="leviticus_9_17"></a>Leviticus 9:17

And he [qāraḇ](../../strongs/h/h7126.md) the [minchah](../../strongs/h/h4503.md), and [mālā'](../../strongs/h/h4390.md) a [kaph](../../strongs/h/h3709.md) thereof, and [qāṭar](../../strongs/h/h6999.md) it upon the [mizbeach](../../strongs/h/h4196.md), [baḏ](../../strongs/h/h905.md) the [ʿōlâ](../../strongs/h/h5930.md) of the [boqer](../../strongs/h/h1242.md).

<a name="leviticus_9_18"></a>Leviticus 9:18

He [šāḥaṭ](../../strongs/h/h7819.md) also the [showr](../../strongs/h/h7794.md) and the ['ayil](../../strongs/h/h352.md) for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md), which was for the ['am](../../strongs/h/h5971.md): and ['Ahărôn](../../strongs/h/h175.md) [ben](../../strongs/h/h1121.md) [māṣā'](../../strongs/h/h4672.md) unto him the [dam](../../strongs/h/h1818.md), which he [zāraq](../../strongs/h/h2236.md) upon the [mizbeach](../../strongs/h/h4196.md) [cabiyb](../../strongs/h/h5439.md),

<a name="leviticus_9_19"></a>Leviticus 9:19

And the [cheleb](../../strongs/h/h2459.md) of the [showr](../../strongs/h/h7794.md) and of the ['ayil](../../strongs/h/h352.md), the ['alyâ](../../strongs/h/h451.md), and that which [mᵊḵassê](../../strongs/h/h4374.md), and the [kilyah](../../strongs/h/h3629.md), and the [yōṯereṯ](../../strongs/h/h3508.md) above the [kāḇēḏ](../../strongs/h/h3516.md):

<a name="leviticus_9_20"></a>Leviticus 9:20

And they [śûm](../../strongs/h/h7760.md) the [cheleb](../../strongs/h/h2459.md) upon the [ḥāzê](../../strongs/h/h2373.md), and he [qāṭar](../../strongs/h/h6999.md) the [cheleb](../../strongs/h/h2459.md) upon the [mizbeach](../../strongs/h/h4196.md):

<a name="leviticus_9_21"></a>Leviticus 9:21

And the [ḥāzê](../../strongs/h/h2373.md) and the [yamiyn](../../strongs/h/h3225.md) [šôq](../../strongs/h/h7785.md) ['Ahărôn](../../strongs/h/h175.md) [nûp̄](../../strongs/h/h5130.md) for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md); as [Mōshe](../../strongs/h/h4872.md) [tsavah](../../strongs/h/h6680.md).

<a name="leviticus_9_22"></a>Leviticus 9:22

And ['Ahărôn](../../strongs/h/h175.md) [nasa'](../../strongs/h/h5375.md) his [yad](../../strongs/h/h3027.md) toward the ['am](../../strongs/h/h5971.md), and [barak](../../strongs/h/h1288.md) them, and [yarad](../../strongs/h/h3381.md) from ['asah](../../strongs/h/h6213.md) of the [chatta'ath](../../strongs/h/h2403.md), and the an [ʿōlâ](../../strongs/h/h5930.md), and [šelem](../../strongs/h/h8002.md).

<a name="leviticus_9_23"></a>Leviticus 9:23

And [Mōshe](../../strongs/h/h4872.md) and ['Ahărôn](../../strongs/h/h175.md) [bow'](../../strongs/h/h935.md) into the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), and [yāṣā'](../../strongs/h/h3318.md), and [barak](../../strongs/h/h1288.md) the ['am](../../strongs/h/h5971.md): and the [kabowd](../../strongs/h/h3519.md) of [Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) unto all the ['am](../../strongs/h/h5971.md).

<a name="leviticus_9_24"></a>Leviticus 9:24

And there [yāṣā'](../../strongs/h/h3318.md) an ['esh](../../strongs/h/h784.md) out from [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and ['akal](../../strongs/h/h398.md) upon the [mizbeach](../../strongs/h/h4196.md) the an [ʿōlâ](../../strongs/h/h5930.md) and the [cheleb](../../strongs/h/h2459.md): which when all the ['am](../../strongs/h/h5971.md) [ra'ah](../../strongs/h/h7200.md), they [ranan](../../strongs/h/h7442.md), and [naphal](../../strongs/h/h5307.md) on their [paniym](../../strongs/h/h6440.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 8](leviticus_8.md) - [Leviticus 10](leviticus_10.md)