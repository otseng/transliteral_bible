# [Leviticus 18](https://www.blueletterbible.org/kjv/lev/18/1/)

<a name="leviticus_18_1"></a>Leviticus 18:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_18_2"></a>Leviticus 18:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_18_3"></a>Leviticus 18:3

After the [ma'aseh](../../strongs/h/h4639.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), wherein ye [yashab](../../strongs/h/h3427.md), shall ye not ['asah](../../strongs/h/h6213.md): and after the [ma'aseh](../../strongs/h/h4639.md) of the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), whither I [bow'](../../strongs/h/h935.md) you, shall ye not ['asah](../../strongs/h/h6213.md): neither shall ye [yālaḵ](../../strongs/h/h3212.md) in their [chuqqah](../../strongs/h/h2708.md).

<a name="leviticus_18_4"></a>Leviticus 18:4

Ye shall ['asah](../../strongs/h/h6213.md) my [mishpat](../../strongs/h/h4941.md), and [shamar](../../strongs/h/h8104.md) mine [chuqqah](../../strongs/h/h2708.md), to [yālaḵ](../../strongs/h/h3212.md) therein: I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_18_5"></a>Leviticus 18:5

Ye shall therefore [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md), and my [mishpat](../../strongs/h/h4941.md): which if an ['adam](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md), he shall [chayay](../../strongs/h/h2425.md) in them: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_18_6"></a>Leviticus 18:6

None of you shall [qāraḇ](../../strongs/h/h7126.md) to ['iysh](../../strongs/h/h376.md) that is [šᵊ'ēr](../../strongs/h/h7607.md) of [basar](../../strongs/h/h1320.md) to him, to [gālâ](../../strongs/h/h1540.md) their [ʿervâ](../../strongs/h/h6172.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_18_7"></a>Leviticus 18:7

The [ʿervâ](../../strongs/h/h6172.md) of thy ['ab](../../strongs/h/h1.md), or the [ʿervâ](../../strongs/h/h6172.md) of thy ['em](../../strongs/h/h517.md), shalt thou not [gālâ](../../strongs/h/h1540.md): she is thy ['em](../../strongs/h/h517.md); thou shalt not [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_8"></a>Leviticus 18:8

The [ʿervâ](../../strongs/h/h6172.md) of thy ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md) shalt thou not [gālâ](../../strongs/h/h1540.md): it is thy ['ab](../../strongs/h/h1.md) [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_9"></a>Leviticus 18:9

The [ʿervâ](../../strongs/h/h6172.md) of thy ['āḥôṯ](../../strongs/h/h269.md), the [bath](../../strongs/h/h1323.md) of thy ['ab](../../strongs/h/h1.md), or [bath](../../strongs/h/h1323.md) of thy ['em](../../strongs/h/h517.md), whether she be [môleḏeṯ](../../strongs/h/h4138.md) at [bayith](../../strongs/h/h1004.md), or [môleḏeṯ](../../strongs/h/h4138.md) [ḥûṣ](../../strongs/h/h2351.md), even their [ʿervâ](../../strongs/h/h6172.md) thou shalt not [gālâ](../../strongs/h/h1540.md).

<a name="leviticus_18_10"></a>Leviticus 18:10

The [ʿervâ](../../strongs/h/h6172.md) of thy [ben](../../strongs/h/h1121.md) [bath](../../strongs/h/h1323.md), or of thy [bath](../../strongs/h/h1323.md) [bath](../../strongs/h/h1323.md), even their [ʿervâ](../../strongs/h/h6172.md) thou shalt not [gālâ](../../strongs/h/h1540.md): for theirs is thine own [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_11"></a>Leviticus 18:11

The [ʿervâ](../../strongs/h/h6172.md) of thy ['ab](../../strongs/h/h1.md) ['ishshah](../../strongs/h/h802.md) [bath](../../strongs/h/h1323.md), [môleḏeṯ](../../strongs/h/h4138.md) of thy ['ab](../../strongs/h/h1.md), she is thy ['āḥôṯ](../../strongs/h/h269.md), thou shalt not [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_12"></a>Leviticus 18:12

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy ['ab](../../strongs/h/h1.md) ['āḥôṯ](../../strongs/h/h269.md): she is thy ['ab](../../strongs/h/h1.md) [šᵊ'ēr](../../strongs/h/h7607.md).

<a name="leviticus_18_13"></a>Leviticus 18:13

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy ['em](../../strongs/h/h517.md) ['āḥôṯ](../../strongs/h/h269.md): for she is thy ['em](../../strongs/h/h517.md) [šᵊ'ēr](../../strongs/h/h7607.md).

<a name="leviticus_18_14"></a>Leviticus 18:14

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy ['ab](../../strongs/h/h1.md) ['ach](../../strongs/h/h251.md), thou shalt not [qāraḇ](../../strongs/h/h7126.md) to his ['ishshah](../../strongs/h/h802.md): she is thine [dôḏâ](../../strongs/h/h1733.md).

<a name="leviticus_18_15"></a>Leviticus 18:15

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy [kallâ](../../strongs/h/h3618.md): she is thy [ben](../../strongs/h/h1121.md) ['ishshah](../../strongs/h/h802.md); thou shalt not [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_16"></a>Leviticus 18:16

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of thy ['ach](../../strongs/h/h251.md) ['ishshah](../../strongs/h/h802.md): it is thy ['ach](../../strongs/h/h251.md) [ʿervâ](../../strongs/h/h6172.md).

<a name="leviticus_18_17"></a>Leviticus 18:17

Thou shalt not [gālâ](../../strongs/h/h1540.md) the [ʿervâ](../../strongs/h/h6172.md) of an ['ishshah](../../strongs/h/h802.md) and her [bath](../../strongs/h/h1323.md), neither shalt thou [laqach](../../strongs/h/h3947.md) her [ben](../../strongs/h/h1121.md) [bath](../../strongs/h/h1323.md), or her [bath](../../strongs/h/h1323.md) [bath](../../strongs/h/h1323.md), to [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md); for they are her [ša'ărâ](../../strongs/h/h7608.md): it is [zimmâ](../../strongs/h/h2154.md).

<a name="leviticus_18_18"></a>Leviticus 18:18

Neither shalt thou [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) to her ['āḥôṯ](../../strongs/h/h269.md), to [tsarar](../../strongs/h/h6887.md) her, to [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md), beside the other in her [chay](../../strongs/h/h2416.md) time.

<a name="leviticus_18_19"></a>Leviticus 18:19

Also thou shalt not [qāraḇ](../../strongs/h/h7126.md) unto an ['ishshah](../../strongs/h/h802.md) to [gālâ](../../strongs/h/h1540.md) her [ʿervâ](../../strongs/h/h6172.md), as long as she is [nidâ](../../strongs/h/h5079.md) for her [ṭām'â](../../strongs/h/h2932.md).

<a name="leviticus_18_20"></a>Leviticus 18:20

Moreover thou shalt not [nathan](../../strongs/h/h5414.md) [šᵊḵōḇeṯ](../../strongs/h/h7903.md) [zera'](../../strongs/h/h2233.md) with thy [ʿāmîṯ](../../strongs/h/h5997.md) ['ishshah](../../strongs/h/h802.md), to [ṭāmē'](../../strongs/h/h2930.md) thyself with her.

<a name="leviticus_18_21"></a>Leviticus 18:21

And thou shalt not [nathan](../../strongs/h/h5414.md) any of thy [zera'](../../strongs/h/h2233.md) ['abar](../../strongs/h/h5674.md) through the [mōleḵ](../../strongs/h/h4432.md), neither shalt thou [ḥālal](../../strongs/h/h2490.md) the [shem](../../strongs/h/h8034.md) of thy ['Elohiym](../../strongs/h/h430.md): I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_18_22"></a>Leviticus 18:22

Thou shalt not [shakab](../../strongs/h/h7901.md) with [zāḵār](../../strongs/h/h2145.md), as [miškāḇ](../../strongs/h/h4904.md) ['ishshah](../../strongs/h/h802.md): it is [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="leviticus_18_23"></a>Leviticus 18:23

Neither shalt [nathan](../../strongs/h/h5414.md) [šᵊḵōḇeṯ](../../strongs/h/h7903.md) with any [bĕhemah](../../strongs/h/h929.md) to [ṭāmē'](../../strongs/h/h2930.md) thyself therewith: neither shall any ['ishshah](../../strongs/h/h802.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) a [bĕhemah](../../strongs/h/h929.md) to [rāḇaʿ](../../strongs/h/h7250.md) thereto: it is [teḇel](../../strongs/h/h8397.md).

<a name="leviticus_18_24"></a>Leviticus 18:24

[ṭāmē'](../../strongs/h/h2930.md) not ye yourselves in any of these things: for in all these the [gowy](../../strongs/h/h1471.md) are [ṭāmē'](../../strongs/h/h2930.md) which I [shalach](../../strongs/h/h7971.md) [paniym](../../strongs/h/h6440.md) you:

<a name="leviticus_18_25"></a>Leviticus 18:25

And the ['erets](../../strongs/h/h776.md) is [ṭāmē'](../../strongs/h/h2930.md): therefore I do [paqad](../../strongs/h/h6485.md) the ['avon](../../strongs/h/h5771.md) thereof upon it, and the ['erets](../../strongs/h/h776.md) itself [qî'](../../strongs/h/h6958.md) her [yashab](../../strongs/h/h3427.md).

<a name="leviticus_18_26"></a>Leviticus 18:26

Ye shall therefore [shamar](../../strongs/h/h8104.md) my [chuqqah](../../strongs/h/h2708.md) and my [mishpat](../../strongs/h/h4941.md), and shall not ['asah](../../strongs/h/h6213.md) any of these [tôʿēḇâ](../../strongs/h/h8441.md); neither any of your own ['ezrāḥ](../../strongs/h/h249.md), nor any [ger](../../strongs/h/h1616.md) that [guwr](../../strongs/h/h1481.md) [tavek](../../strongs/h/h8432.md) you:

<a name="leviticus_18_27"></a>Leviticus 18:27

(For all these [tôʿēḇâ](../../strongs/h/h8441.md) have the ['enowsh](../../strongs/h/h582.md) of the ['erets](../../strongs/h/h776.md) ['asah](../../strongs/h/h6213.md), which were [paniym](../../strongs/h/h6440.md) you, and the ['erets](../../strongs/h/h776.md) is [ṭāmē'](../../strongs/h/h2930.md);)

<a name="leviticus_18_28"></a>Leviticus 18:28

That the ['erets](../../strongs/h/h776.md) [qî'](../../strongs/h/h6958.md) not you also, when ye [ṭāmē'](../../strongs/h/h2930.md) it, as it [qî'](../../strongs/h/h6958.md) the [gowy](../../strongs/h/h1471.md) that were [paniym](../../strongs/h/h6440.md) you.

<a name="leviticus_18_29"></a>Leviticus 18:29

For whosoever shall ['asah](../../strongs/h/h6213.md) any of these [tôʿēḇâ](../../strongs/h/h8441.md), even the [nephesh](../../strongs/h/h5315.md) that ['asah](../../strongs/h/h6213.md) them shall be [karath](../../strongs/h/h3772.md) from [qereḇ](../../strongs/h/h7130.md) their ['am](../../strongs/h/h5971.md).

<a name="leviticus_18_30"></a>Leviticus 18:30

Therefore shall ye [shamar](../../strongs/h/h8104.md) mine [mišmereṯ](../../strongs/h/h4931.md), that ye ['asah](../../strongs/h/h6213.md) not any one of these [tôʿēḇâ](../../strongs/h/h8441.md) [chuqqah](../../strongs/h/h2708.md), which were ['asah](../../strongs/h/h6213.md) [paniym](../../strongs/h/h6440.md) you, and that ye [ṭāmē'](../../strongs/h/h2930.md) not yourselves therein: I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 17](leviticus_17.md) - [Leviticus 19](leviticus_19.md)