# [Leviticus 14](https://www.blueletterbible.org/kjv/lev/14/1/)

<a name="leviticus_14_1"></a>Leviticus 14:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_14_2"></a>Leviticus 14:2

This shall be the [towrah](../../strongs/h/h8451.md) of the [ṣāraʿ](../../strongs/h/h6879.md) in the [yowm](../../strongs/h/h3117.md) of his [ṭāhŏrâ](../../strongs/h/h2893.md): He shall be [bow'](../../strongs/h/h935.md) unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_14_3"></a>Leviticus 14:3

And the [kōhēn](../../strongs/h/h3548.md) shall [yāṣā'](../../strongs/h/h3318.md) [ḥûṣ](../../strongs/h/h2351.md) of the [maḥănê](../../strongs/h/h4264.md); and the [kōhēn](../../strongs/h/h3548.md) shall [ra'ah](../../strongs/h/h7200.md), and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) be [rapha'](../../strongs/h/h7495.md) in the [ṣāraʿ](../../strongs/h/h6879.md);

<a name="leviticus_14_4"></a>Leviticus 14:4

Then shall the [kōhēn](../../strongs/h/h3548.md) [tsavah](../../strongs/h/h6680.md) to [laqach](../../strongs/h/h3947.md) for him that is to be [ṭāhēr](../../strongs/h/h2891.md) two [tsippowr](../../strongs/h/h6833.md) [chay](../../strongs/h/h2416.md) and [tahowr](../../strongs/h/h2889.md), and ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and ['ēzôḇ](../../strongs/h/h231.md):

<a name="leviticus_14_5"></a>Leviticus 14:5

And the [kōhēn](../../strongs/h/h3548.md) shall [tsavah](../../strongs/h/h6680.md) that one of the [tsippowr](../../strongs/h/h6833.md) be [šāḥaṭ](../../strongs/h/h7819.md) in an [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md) over [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md):

<a name="leviticus_14_6"></a>Leviticus 14:6

As for the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md), he shall [laqach](../../strongs/h/h3947.md) it, and the ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and the [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and the ['ēzôḇ](../../strongs/h/h231.md), and shall [ṭāḇal](../../strongs/h/h2881.md) them and the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md) in the [dam](../../strongs/h/h1818.md) of the [tsippowr](../../strongs/h/h6833.md) that was [šāḥaṭ](../../strongs/h/h7819.md) over the [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md):

<a name="leviticus_14_7"></a>Leviticus 14:7

And he shall [nāzâ](../../strongs/h/h5137.md) upon him that is to be [ṭāhēr](../../strongs/h/h2891.md) from the [ṣāraʿaṯ](../../strongs/h/h6883.md) seven [pa'am](../../strongs/h/h6471.md), and shall pronounce him [ṭāhēr](../../strongs/h/h2891.md), and shall let the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md) [shalach](../../strongs/h/h7971.md) into the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md).

<a name="leviticus_14_8"></a>Leviticus 14:8

And he that is to be [ṭāhēr](../../strongs/h/h2891.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), and [gālaḥ](../../strongs/h/h1548.md) off all his [śēʿār](../../strongs/h/h8181.md), and [rāḥaṣ](../../strongs/h/h7364.md) himself in [mayim](../../strongs/h/h4325.md), that he may be [ṭāhēr](../../strongs/h/h2891.md): and ['aḥar](../../strongs/h/h310.md) that he shall [bow'](../../strongs/h/h935.md) into the [maḥănê](../../strongs/h/h4264.md), and shall [yashab](../../strongs/h/h3427.md) [ḥûṣ](../../strongs/h/h2351.md) out of his ['ohel](../../strongs/h/h168.md) seven [yowm](../../strongs/h/h3117.md).

<a name="leviticus_14_9"></a>Leviticus 14:9

But it shall be on the seventh [yowm](../../strongs/h/h3117.md), that he shall [gālaḥ](../../strongs/h/h1548.md) all his [śēʿār](../../strongs/h/h8181.md) off his [ro'sh](../../strongs/h/h7218.md) and his [zāqān](../../strongs/h/h2206.md) and his [gaḇ](../../strongs/h/h1354.md) ['ayin](../../strongs/h/h5869.md), even all his [śēʿār](../../strongs/h/h8181.md) he shall [gālaḥ](../../strongs/h/h1548.md) off: and he shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md), also he shall [rāḥaṣ](../../strongs/h/h7364.md) his [basar](../../strongs/h/h1320.md) in [mayim](../../strongs/h/h4325.md), and he shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_14_10"></a>Leviticus 14:10

And on the eighth [yowm](../../strongs/h/h3117.md) he shall [laqach](../../strongs/h/h3947.md) two [keḇeś](../../strongs/h/h3532.md) [tamiym](../../strongs/h/h8549.md), and one [kiḇśâ](../../strongs/h/h3535.md) of the [bath](../../strongs/h/h1323.md) [šānâ](../../strongs/h/h8141.md) [tamiym](../../strongs/h/h8549.md), and three [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) for a [minchah](../../strongs/h/h4503.md), [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), and one [lōḡ](../../strongs/h/h3849.md) of [šemen](../../strongs/h/h8081.md).

<a name="leviticus_14_11"></a>Leviticus 14:11

And the [kōhēn](../../strongs/h/h3548.md) that maketh him [ṭāhēr](../../strongs/h/h2891.md) shall ['amad](../../strongs/h/h5975.md) the ['iysh](../../strongs/h/h376.md) that is to be made [ṭāhēr](../../strongs/h/h2891.md), and those things, [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), at the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md):

<a name="leviticus_14_12"></a>Leviticus 14:12

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) one he [keḇeś](../../strongs/h/h3532.md), and [qāraḇ](../../strongs/h/h7126.md) him for an ['āšām](../../strongs/h/h817.md), and the [lōḡ](../../strongs/h/h3849.md) of [šemen](../../strongs/h/h8081.md), and [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="leviticus_14_13"></a>Leviticus 14:13

And he shall [šāḥaṭ](../../strongs/h/h7819.md) the [keḇeś](../../strongs/h/h3532.md) in the [maqowm](../../strongs/h/h4725.md) where he shall [šāḥaṭ](../../strongs/h/h7819.md) the [chatta'ath](../../strongs/h/h2403.md) and the an [ʿōlâ](../../strongs/h/h5930.md), in the [qodesh](../../strongs/h/h6944.md) [maqowm](../../strongs/h/h4725.md): for as the [chatta'ath](../../strongs/h/h2403.md) is the [kōhēn](../../strongs/h/h3548.md), so is the ['āšām](../../strongs/h/h817.md): it is [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md):

<a name="leviticus_14_14"></a>Leviticus 14:14

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) some of the [dam](../../strongs/h/h1818.md) of the ['āšām](../../strongs/h/h817.md), and the [kōhēn](../../strongs/h/h3548.md) shall [nathan](../../strongs/h/h5414.md) it upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md):

<a name="leviticus_14_15"></a>Leviticus 14:15

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) some of the [lōḡ](../../strongs/h/h3849.md) of [šemen](../../strongs/h/h8081.md), and [yāṣaq](../../strongs/h/h3332.md) it into the [kaph](../../strongs/h/h3709.md) of his own [śᵊmā'lî](../../strongs/h/h8042.md):

<a name="leviticus_14_16"></a>Leviticus 14:16

And the [kōhēn](../../strongs/h/h3548.md) shall [ṭāḇal](../../strongs/h/h2881.md) his [yᵊmānî](../../strongs/h/h3233.md) ['etsba'](../../strongs/h/h676.md) in the [šemen](../../strongs/h/h8081.md) that is in his [śᵊmā'lî](../../strongs/h/h8042.md) [kaph](../../strongs/h/h3709.md), and shall [nāzâ](../../strongs/h/h5137.md) of the [šemen](../../strongs/h/h8081.md) with his ['etsba'](../../strongs/h/h676.md) seven [pa'am](../../strongs/h/h6471.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="leviticus_14_17"></a>Leviticus 14:17

And of the [yeṯer](../../strongs/h/h3499.md) of the [šemen](../../strongs/h/h8081.md) that is in his [kaph](../../strongs/h/h3709.md) shall the [kōhēn](../../strongs/h/h3548.md) [nathan](../../strongs/h/h5414.md) upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md), upon the [dam](../../strongs/h/h1818.md) of the ['āšām](../../strongs/h/h817.md):

<a name="leviticus_14_18"></a>Leviticus 14:18

And the [yāṯar](../../strongs/h/h3498.md) of the [šemen](../../strongs/h/h8081.md) that is in the [kōhēn](../../strongs/h/h3548.md) [kaph](../../strongs/h/h3709.md) he shall [nathan](../../strongs/h/h5414.md) upon the [ro'sh](../../strongs/h/h7218.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md): and the [kōhēn](../../strongs/h/h3548.md) shall [kāp̄ar](../../strongs/h/h3722.md) for him [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_14_19"></a>Leviticus 14:19

And the [kōhēn](../../strongs/h/h3548.md) shall ['asah](../../strongs/h/h6213.md) the [chatta'ath](../../strongs/h/h2403.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for him that is to be [ṭāhēr](../../strongs/h/h2891.md) from his [ṭām'â](../../strongs/h/h2932.md); and ['aḥar](../../strongs/h/h310.md) he shall [šāḥaṭ](../../strongs/h/h7819.md) the an [ʿōlâ](../../strongs/h/h5930.md):

<a name="leviticus_14_20"></a>Leviticus 14:20

And the [kōhēn](../../strongs/h/h3548.md) shall [ʿālâ](../../strongs/h/h5927.md) the an [ʿōlâ](../../strongs/h/h5930.md) and the [minchah](../../strongs/h/h4503.md) upon the [mizbeach](../../strongs/h/h4196.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him, and he shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_14_21"></a>Leviticus 14:21

And if he be [dal](../../strongs/h/h1800.md), and cannot [yad](../../strongs/h/h3027.md) [nāśaḡ](../../strongs/h/h5381.md) so much; then he shall [laqach](../../strongs/h/h3947.md) one [keḇeś](../../strongs/h/h3532.md) for an ['āšām](../../strongs/h/h817.md) to be [tᵊnûp̄â](../../strongs/h/h8573.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for him, and one [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md) for a [minchah](../../strongs/h/h4503.md), and a [lōḡ](../../strongs/h/h3849.md) of [šemen](../../strongs/h/h8081.md);

<a name="leviticus_14_22"></a>Leviticus 14:22

And two [tôr](../../strongs/h/h8449.md), or two [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), such as he is able to [yad](../../strongs/h/h3027.md) [nāśaḡ](../../strongs/h/h5381.md); and the one shall be a [chatta'ath](../../strongs/h/h2403.md), and the other an [ʿōlâ](../../strongs/h/h5930.md).

<a name="leviticus_14_23"></a>Leviticus 14:23

And he shall [bow'](../../strongs/h/h935.md) them on the eighth [yowm](../../strongs/h/h3117.md) for his [ṭāhŏrâ](../../strongs/h/h2893.md) unto the [kōhēn](../../strongs/h/h3548.md), unto the [peṯaḥ](../../strongs/h/h6607.md) of the ['ohel](../../strongs/h/h168.md) of the [môʿēḏ](../../strongs/h/h4150.md), [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_14_24"></a>Leviticus 14:24

And the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) the [keḇeś](../../strongs/h/h3532.md) of the ['āšām](../../strongs/h/h817.md), and the [lōḡ](../../strongs/h/h3849.md) of [šemen](../../strongs/h/h8081.md), and the [kōhēn](../../strongs/h/h3548.md) shall [nûp̄](../../strongs/h/h5130.md) them for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="leviticus_14_25"></a>Leviticus 14:25

And he shall [šāḥaṭ](../../strongs/h/h7819.md) the [keḇeś](../../strongs/h/h3532.md) of the ['āšām](../../strongs/h/h817.md), and the [kōhēn](../../strongs/h/h3548.md) shall [laqach](../../strongs/h/h3947.md) some of the [dam](../../strongs/h/h1818.md) of the ['āšām](../../strongs/h/h817.md), and [nathan](../../strongs/h/h5414.md) it upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md):

<a name="leviticus_14_26"></a>Leviticus 14:26

And the [kōhēn](../../strongs/h/h3548.md) shall [yāṣaq](../../strongs/h/h3332.md) of the [šemen](../../strongs/h/h8081.md) into the [kaph](../../strongs/h/h3709.md) of his own [śᵊmā'lî](../../strongs/h/h8042.md):

<a name="leviticus_14_27"></a>Leviticus 14:27

And the [kōhēn](../../strongs/h/h3548.md) shall [nāzâ](../../strongs/h/h5137.md) with his [yᵊmānî](../../strongs/h/h3233.md) ['etsba'](../../strongs/h/h676.md) some of the [šemen](../../strongs/h/h8081.md) that is in his [śᵊmā'lî](../../strongs/h/h8042.md) [kaph](../../strongs/h/h3709.md) seven [pa'am](../../strongs/h/h6471.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="leviticus_14_28"></a>Leviticus 14:28

And the [kōhēn](../../strongs/h/h3548.md) shall [nathan](../../strongs/h/h5414.md) of the [šemen](../../strongs/h/h8081.md) that is in his [kaph](../../strongs/h/h3709.md) upon the [tᵊnûḵ](../../strongs/h/h8571.md) of the [yᵊmānî](../../strongs/h/h3233.md) ['ozen](../../strongs/h/h241.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [yad](../../strongs/h/h3027.md), and upon the [bōhen](../../strongs/h/h931.md) of his [yᵊmānî](../../strongs/h/h3233.md) [regel](../../strongs/h/h7272.md), upon the [maqowm](../../strongs/h/h4725.md) of the [dam](../../strongs/h/h1818.md) of the ['āšām](../../strongs/h/h817.md):

<a name="leviticus_14_29"></a>Leviticus 14:29

And the [yāṯar](../../strongs/h/h3498.md) of the [šemen](../../strongs/h/h8081.md) that is in the [kōhēn](../../strongs/h/h3548.md) [kaph](../../strongs/h/h3709.md) he shall [nathan](../../strongs/h/h5414.md) upon the [ro'sh](../../strongs/h/h7218.md) of him that is to be [ṭāhēr](../../strongs/h/h2891.md), to make a [kāp̄ar](../../strongs/h/h3722.md) for him [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_14_30"></a>Leviticus 14:30

And he shall ['asah](../../strongs/h/h6213.md) the one of the [tôr](../../strongs/h/h8449.md), or of the [ben](../../strongs/h/h1121.md) [yônâ](../../strongs/h/h3123.md), such as he can [yad](../../strongs/h/h3027.md) [nāśaḡ](../../strongs/h/h5381.md);

<a name="leviticus_14_31"></a>Leviticus 14:31

Even such as he is able to [yad](../../strongs/h/h3027.md) [nāśaḡ](../../strongs/h/h5381.md), the one for a [chatta'ath](../../strongs/h/h2403.md), and the other for an [ʿōlâ](../../strongs/h/h5930.md), with the [minchah](../../strongs/h/h4503.md): and the [kōhēn](../../strongs/h/h3548.md) shall make a [kāp̄ar](../../strongs/h/h3722.md) for him that is to be [ṭāhēr](../../strongs/h/h2891.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_14_32"></a>Leviticus 14:32

This is the [towrah](../../strongs/h/h8451.md) of him in whom is the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md), whose [yad](../../strongs/h/h3027.md) is not able to [nāśaḡ](../../strongs/h/h5381.md) that which pertaineth to his [ṭāhŏrâ](../../strongs/h/h2893.md).

<a name="leviticus_14_33"></a>Leviticus 14:33

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md) and unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_14_34"></a>Leviticus 14:34

When ye be [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Kĕna'an](../../strongs/h/h3667.md), which I [nathan](../../strongs/h/h5414.md) to you for an ['achuzzah](../../strongs/h/h272.md), and I put the [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md) in a [bayith](../../strongs/h/h1004.md) of the ['erets](../../strongs/h/h776.md) of your ['achuzzah](../../strongs/h/h272.md);

<a name="leviticus_14_35"></a>Leviticus 14:35

And he that owneth the [bayith](../../strongs/h/h1004.md) shall [bow'](../../strongs/h/h935.md) and [nāḡaḏ](../../strongs/h/h5046.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md), It [ra'ah](../../strongs/h/h7200.md) to me there is as it were a [neḡaʿ](../../strongs/h/h5061.md) in the [bayith](../../strongs/h/h1004.md):

<a name="leviticus_14_36"></a>Leviticus 14:36

Then the [kōhēn](../../strongs/h/h3548.md) shall [tsavah](../../strongs/h/h6680.md) that they [panah](../../strongs/h/h6437.md) the [bayith](../../strongs/h/h1004.md), before the [kōhēn](../../strongs/h/h3548.md) [bow'](../../strongs/h/h935.md) into it to [ra'ah](../../strongs/h/h7200.md) the [neḡaʿ](../../strongs/h/h5061.md), that all that is in the [bayith](../../strongs/h/h1004.md) be not made [ṭāmē'](../../strongs/h/h2930.md): and ['aḥar](../../strongs/h/h310.md) the [kōhēn](../../strongs/h/h3548.md) shall [bow'](../../strongs/h/h935.md) in to [ra'ah](../../strongs/h/h7200.md) the [bayith](../../strongs/h/h1004.md):

<a name="leviticus_14_37"></a>Leviticus 14:37

And he shall [ra'ah](../../strongs/h/h7200.md) on the [neḡaʿ](../../strongs/h/h5061.md), and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) be in the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md) with [šᵊqaʿărûrâ](../../strongs/h/h8258.md), [yᵊraqraq](../../strongs/h/h3422.md) or ['ăḏamdām](../../strongs/h/h125.md), which in [mar'ê](../../strongs/h/h4758.md) are [šāp̄āl](../../strongs/h/h8217.md) than the [qîr](../../strongs/h/h7023.md);

<a name="leviticus_14_38"></a>Leviticus 14:38

Then the [kōhēn](../../strongs/h/h3548.md) shall [yāṣā'](../../strongs/h/h3318.md) of the [bayith](../../strongs/h/h1004.md) to the [peṯaḥ](../../strongs/h/h6607.md) of the [bayith](../../strongs/h/h1004.md), and [cagar](../../strongs/h/h5462.md) up the [bayith](../../strongs/h/h1004.md) seven [yowm](../../strongs/h/h3117.md):

<a name="leviticus_14_39"></a>Leviticus 14:39

And the [kōhēn](../../strongs/h/h3548.md) shall [shuwb](../../strongs/h/h7725.md) the seventh [yowm](../../strongs/h/h3117.md), and shall [ra'ah](../../strongs/h/h7200.md): and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) be [pāśâ](../../strongs/h/h6581.md) in the [qîr](../../strongs/h/h7023.md) of the [bayith](../../strongs/h/h1004.md);

<a name="leviticus_14_40"></a>Leviticus 14:40

Then the [kōhēn](../../strongs/h/h3548.md) shall [tsavah](../../strongs/h/h6680.md) that they [chalats](../../strongs/h/h2502.md) the ['eben](../../strongs/h/h68.md) in which the [neḡaʿ](../../strongs/h/h5061.md) is, and they shall [shalak](../../strongs/h/h7993.md) them into a [tame'](../../strongs/h/h2931.md) [maqowm](../../strongs/h/h4725.md) [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md):

<a name="leviticus_14_41"></a>Leviticus 14:41

And he shall cause the [bayith](../../strongs/h/h1004.md) to be [qāṣaʿ](../../strongs/h/h7106.md) within [cabiyb](../../strongs/h/h5439.md), and they shall [šāp̄aḵ](../../strongs/h/h8210.md) out the ['aphar](../../strongs/h/h6083.md) that they [qāṣâ](../../strongs/h/h7096.md) [ḥûṣ](../../strongs/h/h2351.md) the [ʿîr](../../strongs/h/h5892.md) into a [tame'](../../strongs/h/h2931.md) [maqowm](../../strongs/h/h4725.md):

<a name="leviticus_14_42"></a>Leviticus 14:42

And they shall [laqach](../../strongs/h/h3947.md) ['aḥēr](../../strongs/h/h312.md) ['eben](../../strongs/h/h68.md), and [bow'](../../strongs/h/h935.md) them in the [taḥaṯ](../../strongs/h/h8478.md) of those ['eben](../../strongs/h/h68.md); and he shall [laqach](../../strongs/h/h3947.md) ['aḥēr](../../strongs/h/h312.md) ['aphar](../../strongs/h/h6083.md), and shall [ṭûaḥ](../../strongs/h/h2902.md) the [bayith](../../strongs/h/h1004.md).

<a name="leviticus_14_43"></a>Leviticus 14:43

And if the [neḡaʿ](../../strongs/h/h5061.md) [shuwb](../../strongs/h/h7725.md), and [pāraḥ](../../strongs/h/h6524.md) in the [bayith](../../strongs/h/h1004.md), ['aḥar](../../strongs/h/h310.md) that he hath [chalats](../../strongs/h/h2502.md) the ['eben](../../strongs/h/h68.md), and ['aḥar](../../strongs/h/h310.md) he hath [qāṣâ](../../strongs/h/h7096.md) the [bayith](../../strongs/h/h1004.md), and ['aḥar](../../strongs/h/h310.md) it is [ṭûaḥ](../../strongs/h/h2902.md);

<a name="leviticus_14_44"></a>Leviticus 14:44

Then the [kōhēn](../../strongs/h/h3548.md) shall [bow'](../../strongs/h/h935.md) and [ra'ah](../../strongs/h/h7200.md), and, behold, if the [neḡaʿ](../../strongs/h/h5061.md) be [pāśâ](../../strongs/h/h6581.md) in the [bayith](../../strongs/h/h1004.md), it is a [mā'ar](../../strongs/h/h3992.md) [ṣāraʿaṯ](../../strongs/h/h6883.md) in the [bayith](../../strongs/h/h1004.md); it is [tame'](../../strongs/h/h2931.md).

<a name="leviticus_14_45"></a>Leviticus 14:45

And he shall [nāṯaṣ](../../strongs/h/h5422.md) the [bayith](../../strongs/h/h1004.md), the ['eben](../../strongs/h/h68.md) of it, and the ['ets](../../strongs/h/h6086.md) thereof, and all the ['aphar](../../strongs/h/h6083.md) of the [bayith](../../strongs/h/h1004.md); and he shall [yāṣā'](../../strongs/h/h3318.md) them [ḥûṣ](../../strongs/h/h2351.md) of the [ʿîr](../../strongs/h/h5892.md) into a [tame'](../../strongs/h/h2931.md) [maqowm](../../strongs/h/h4725.md).

<a name="leviticus_14_46"></a>Leviticus 14:46

Moreover he that [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) all the [yowm](../../strongs/h/h3117.md) that it is [cagar](../../strongs/h/h5462.md) shall be [ṭāmē'](../../strongs/h/h2930.md) until the ['ereb](../../strongs/h/h6153.md).

<a name="leviticus_14_47"></a>Leviticus 14:47

And he that [shakab](../../strongs/h/h7901.md) in the [bayith](../../strongs/h/h1004.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md); and he that ['akal](../../strongs/h/h398.md) in the [bayith](../../strongs/h/h1004.md) shall [kāḇas](../../strongs/h/h3526.md) his [beḡeḏ](../../strongs/h/h899.md).

<a name="leviticus_14_48"></a>Leviticus 14:48

And if the [kōhēn](../../strongs/h/h3548.md) shall [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md), and [ra'ah](../../strongs/h/h7200.md) upon it, and, behold, the [neḡaʿ](../../strongs/h/h5061.md) hath not [pāśâ](../../strongs/h/h6581.md) in the [bayith](../../strongs/h/h1004.md), ['aḥar](../../strongs/h/h310.md) the [bayith](../../strongs/h/h1004.md) was [ṭûaḥ](../../strongs/h/h2902.md): then the [kōhēn](../../strongs/h/h3548.md) shall pronounce the [bayith](../../strongs/h/h1004.md) [ṭāhēr](../../strongs/h/h2891.md), because the [neḡaʿ](../../strongs/h/h5061.md) is [rapha'](../../strongs/h/h7495.md).

<a name="leviticus_14_49"></a>Leviticus 14:49

And he shall [laqach](../../strongs/h/h3947.md) to [chata'](../../strongs/h/h2398.md) the [bayith](../../strongs/h/h1004.md) two [tsippowr](../../strongs/h/h6833.md), and ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and ['ēzôḇ](../../strongs/h/h231.md):

<a name="leviticus_14_50"></a>Leviticus 14:50

And he shall [šāḥaṭ](../../strongs/h/h7819.md) the one of the [tsippowr](../../strongs/h/h6833.md) in an [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md) over [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md):

<a name="leviticus_14_51"></a>Leviticus 14:51

And he shall [laqach](../../strongs/h/h3947.md) the ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and the ['ēzôḇ](../../strongs/h/h231.md), and the [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md), and the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md), and [ṭāḇal](../../strongs/h/h2881.md) them in the [dam](../../strongs/h/h1818.md) of the [šāḥaṭ](../../strongs/h/h7819.md) [tsippowr](../../strongs/h/h6833.md), and in the [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md), and [nāzâ](../../strongs/h/h5137.md) the [bayith](../../strongs/h/h1004.md) seven [pa'am](../../strongs/h/h6471.md):

<a name="leviticus_14_52"></a>Leviticus 14:52

And he shall [chata'](../../strongs/h/h2398.md) the [bayith](../../strongs/h/h1004.md) with the [dam](../../strongs/h/h1818.md) of the [tsippowr](../../strongs/h/h6833.md), and with the [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md), and with the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md), and with the ['erez](../../strongs/h/h730.md) ['ets](../../strongs/h/h6086.md), and with the ['ēzôḇ](../../strongs/h/h231.md), and with the [šānî](../../strongs/h/h8144.md) [tôlāʿ](../../strongs/h/h8438.md):

<a name="leviticus_14_53"></a>Leviticus 14:53

But he shall [shalach](../../strongs/h/h7971.md) the [chay](../../strongs/h/h2416.md) [tsippowr](../../strongs/h/h6833.md) [ḥûṣ](../../strongs/h/h2351.md) of the [ʿîr](../../strongs/h/h5892.md) into the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), and make a [kāp̄ar](../../strongs/h/h3722.md) for the [bayith](../../strongs/h/h1004.md): and it shall be [ṭāhēr](../../strongs/h/h2891.md).

<a name="leviticus_14_54"></a>Leviticus 14:54

This is the [towrah](../../strongs/h/h8451.md) for all manner of [neḡaʿ](../../strongs/h/h5061.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md), and [neṯeq](../../strongs/h/h5424.md),

<a name="leviticus_14_55"></a>Leviticus 14:55

And for the [ṣāraʿaṯ](../../strongs/h/h6883.md) of a [beḡeḏ](../../strongs/h/h899.md), and of a [bayith](../../strongs/h/h1004.md),

<a name="leviticus_14_56"></a>Leviticus 14:56

And for a [śᵊ'ēṯ](../../strongs/h/h7613.md), and for a [sapaḥaṯ](../../strongs/h/h5597.md), and for a [bahereṯ](../../strongs/h/h934.md):

<a name="leviticus_14_57"></a>Leviticus 14:57

To [yārâ](../../strongs/h/h3384.md) [yowm](../../strongs/h/h3117.md) it is [tame'](../../strongs/h/h2931.md), and [yowm](../../strongs/h/h3117.md) it is [tahowr](../../strongs/h/h2889.md): this is the [towrah](../../strongs/h/h8451.md) of [ṣāraʿaṯ](../../strongs/h/h6883.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 13](leviticus_13.md) - [Leviticus 15](leviticus_15.md)