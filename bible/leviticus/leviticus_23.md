# [Leviticus 23](https://www.blueletterbible.org/kjv/lev/23)

<a name="leviticus_23_1"></a>Leviticus 23:1

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_23_2"></a>Leviticus 23:2

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, Concerning the [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md), which ye shall [qara'](../../strongs/h/h7121.md) to be [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md), even these are my [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_23_3"></a>Leviticus 23:3

Six [yowm](../../strongs/h/h3117.md) shall [mĕla'kah](../../strongs/h/h4399.md) be ['asah](../../strongs/h/h6213.md): but the seventh [yowm](../../strongs/h/h3117.md) is the [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md), a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md); ye shall ['asah](../../strongs/h/h6213.md) no [mĕla'kah](../../strongs/h/h4399.md) therein: it is the [shabbath](../../strongs/h/h7676.md) of [Yĕhovah](../../strongs/h/h3068.md) in all your [môšāḇ](../../strongs/h/h4186.md).

<a name="leviticus_23_4"></a>Leviticus 23:4

These are the [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md), even [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md), which ye shall [qara'](../../strongs/h/h7121.md) in their [môʿēḏ](../../strongs/h/h4150.md).

<a name="leviticus_23_5"></a>Leviticus 23:5

In the fourteenth day of the [ri'šôn](../../strongs/h/h7223.md) [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md) is [Yĕhovah](../../strongs/h/h3068.md) [pecach](../../strongs/h/h6453.md).

<a name="leviticus_23_6"></a>Leviticus 23:6

And on the fifteenth [yowm](../../strongs/h/h3117.md) of the same [ḥōḏeš](../../strongs/h/h2320.md) is the [ḥāḡ](../../strongs/h/h2282.md) of [maṣṣâ](../../strongs/h/h4682.md) unto [Yĕhovah](../../strongs/h/h3068.md): seven [yowm](../../strongs/h/h3117.md) ye must ['akal](../../strongs/h/h398.md) [maṣṣâ](../../strongs/h/h4682.md).

<a name="leviticus_23_7"></a>Leviticus 23:7

In the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) ye shall have a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md): ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein.

<a name="leviticus_23_8"></a>Leviticus 23:8

But ye shall [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md) seven [yowm](../../strongs/h/h3117.md): in the seventh [yowm](../../strongs/h/h3117.md) is a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md): ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein.

<a name="leviticus_23_9"></a>Leviticus 23:9

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_23_10"></a>Leviticus 23:10

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), and ['āmar](../../strongs/h/h559.md) unto them, When ye be [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) which I [nathan](../../strongs/h/h5414.md) unto you, and shall [qāṣar](../../strongs/h/h7114.md) the [qāṣîr](../../strongs/h/h7105.md) thereof, then ye shall [bow'](../../strongs/h/h935.md) an [ʿōmer](../../strongs/h/h6016.md) of the [re'shiyth](../../strongs/h/h7225.md) of your [qāṣîr](../../strongs/h/h7105.md) unto the [kōhēn](../../strongs/h/h3548.md):

<a name="leviticus_23_11"></a>Leviticus 23:11

And he shall [nûp̄](../../strongs/h/h5130.md) the [ʿōmer](../../strongs/h/h6016.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), to be [ratsown](../../strongs/h/h7522.md) for you: on the [māḥŏrāṯ](../../strongs/h/h4283.md) after the [shabbath](../../strongs/h/h7676.md) the [kōhēn](../../strongs/h/h3548.md) shall [nûp̄](../../strongs/h/h5130.md) it.

<a name="leviticus_23_12"></a>Leviticus 23:12

And ye shall ['asah](../../strongs/h/h6213.md) that [yowm](../../strongs/h/h3117.md) when ye [nûp̄](../../strongs/h/h5130.md) the [ʿōmer](../../strongs/h/h6016.md) a [keḇeś](../../strongs/h/h3532.md) [tamiym](../../strongs/h/h8549.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) for an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_13"></a>Leviticus 23:13

And the [minchah](../../strongs/h/h4503.md) thereof shall be two [ʿiśśārôn](../../strongs/h/h6241.md) of [sōleṯ](../../strongs/h/h5560.md) [bālal](../../strongs/h/h1101.md) with [šemen](../../strongs/h/h8081.md), an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md) for a [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md): and the [necek](../../strongs/h/h5262.md) thereof shall be of [yayin](../../strongs/h/h3196.md), the fourth part of an [hîn](../../strongs/h/h1969.md).

<a name="leviticus_23_14"></a>Leviticus 23:14

And ye shall ['akal](../../strongs/h/h398.md) neither [lechem](../../strongs/h/h3899.md), nor [qālî](../../strongs/h/h7039.md), nor [karmel](../../strongs/h/h3759.md), until the ['etsem](../../strongs/h/h6106.md) [zê](../../strongs/h/h2088.md) [yowm](../../strongs/h/h3117.md) that ye have [bow'](../../strongs/h/h935.md) a [qorban](../../strongs/h/h7133.md) unto your ['Elohiym](../../strongs/h/h430.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout your [dôr](../../strongs/h/h1755.md) in all your [môšāḇ](../../strongs/h/h4186.md).

<a name="leviticus_23_15"></a>Leviticus 23:15

And ye shall [sāp̄ar](../../strongs/h/h5608.md) unto you from the [māḥŏrāṯ](../../strongs/h/h4283.md) after the [shabbath](../../strongs/h/h7676.md), from the [yowm](../../strongs/h/h3117.md) that ye [bow'](../../strongs/h/h935.md) the [ʿōmer](../../strongs/h/h6016.md) of the [tᵊnûp̄â](../../strongs/h/h8573.md); seven [shabbath](../../strongs/h/h7676.md) shall be [tamiym](../../strongs/h/h8549.md):

<a name="leviticus_23_16"></a>Leviticus 23:16

Even unto the [māḥŏrāṯ](../../strongs/h/h4283.md) after the seventh [shabbath](../../strongs/h/h7676.md) shall ye [sāp̄ar](../../strongs/h/h5608.md) fifty [yowm](../../strongs/h/h3117.md); and ye shall [qāraḇ](../../strongs/h/h7126.md) a [ḥāḏāš](../../strongs/h/h2319.md) [minchah](../../strongs/h/h4503.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_17"></a>Leviticus 23:17

Ye shall [bow'](../../strongs/h/h935.md) of your [môšāḇ](../../strongs/h/h4186.md) two [tᵊnûp̄â](../../strongs/h/h8573.md) [lechem](../../strongs/h/h3899.md) of two [ʿiśśārôn](../../strongs/h/h6241.md); they shall be of [sōleṯ](../../strongs/h/h5560.md); they shall be ['āp̄â](../../strongs/h/h644.md) with [ḥāmēṣ](../../strongs/h/h2557.md); they are the [bikûr](../../strongs/h/h1061.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_18"></a>Leviticus 23:18

And ye shall [qāraḇ](../../strongs/h/h7126.md) with the [lechem](../../strongs/h/h3899.md) seven [keḇeś](../../strongs/h/h3532.md) [tamiym](../../strongs/h/h8549.md) of the first [šānâ](../../strongs/h/h8141.md), and one [bāqār](../../strongs/h/h1241.md) [ben](../../strongs/h/h1121.md) [par](../../strongs/h/h6499.md), and two ['ayil](../../strongs/h/h352.md): they shall be for an [ʿōlâ](../../strongs/h/h5930.md) unto [Yĕhovah](../../strongs/h/h3068.md), with their [minchah](../../strongs/h/h4503.md), and their [necek](../../strongs/h/h5262.md), even an ['iššê](../../strongs/h/h801.md), of [nîḥōaḥ](../../strongs/h/h5207.md) [rêaḥ](../../strongs/h/h7381.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_19"></a>Leviticus 23:19

Then ye shall ['asah](../../strongs/h/h6213.md) one [śāʿîr](../../strongs/h/h8163.md) of the [ʿēz](../../strongs/h/h5795.md) for a [chatta'ath](../../strongs/h/h2403.md), and two [keḇeś](../../strongs/h/h3532.md) of the [ben](../../strongs/h/h1121.md) [šānâ](../../strongs/h/h8141.md) for a [zebach](../../strongs/h/h2077.md) of [šelem](../../strongs/h/h8002.md).

<a name="leviticus_23_20"></a>Leviticus 23:20

And the [kōhēn](../../strongs/h/h3548.md) shall [nûp̄](../../strongs/h/h5130.md) them with the [lechem](../../strongs/h/h3899.md) of the [bikûr](../../strongs/h/h1061.md) for a [tᵊnûp̄â](../../strongs/h/h8573.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), with the two [keḇeś](../../strongs/h/h3532.md): they shall be [qodesh](../../strongs/h/h6944.md) to [Yĕhovah](../../strongs/h/h3068.md) for the [kōhēn](../../strongs/h/h3548.md).

<a name="leviticus_23_21"></a>Leviticus 23:21

And ye shall [qara'](../../strongs/h/h7121.md) on the ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), that it may be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md) unto you: ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein: it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) in all your [môšāḇ](../../strongs/h/h4186.md) throughout your [dôr](../../strongs/h/h1755.md).

<a name="leviticus_23_22"></a>Leviticus 23:22

And when ye [qāṣar](../../strongs/h/h7114.md) the [qāṣîr](../../strongs/h/h7105.md) of your ['erets](../../strongs/h/h776.md), thou shalt not [kalah](../../strongs/h/h3615.md) of the [pē'â](../../strongs/h/h6285.md) of thy [sadeh](../../strongs/h/h7704.md) when thou [qāṣar](../../strongs/h/h7114.md), neither shalt thou [lāqaṭ](../../strongs/h/h3950.md) any [leqeṭ](../../strongs/h/h3951.md) of thy [qāṣîr](../../strongs/h/h7105.md): thou shalt ['azab](../../strongs/h/h5800.md) them unto the ['aniy](../../strongs/h/h6041.md), and to the [ger](../../strongs/h/h1616.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_23_23"></a>Leviticus 23:23

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_23_24"></a>Leviticus 23:24

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), In the seventh [ḥōḏeš](../../strongs/h/h2320.md), in the first of the [ḥōḏeš](../../strongs/h/h2320.md), shall ye have a [šabāṯôn](../../strongs/h/h7677.md), a [zikārôn](../../strongs/h/h2146.md) of [tᵊrûʿâ](../../strongs/h/h8643.md), a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md).

<a name="leviticus_23_25"></a>Leviticus 23:25

Ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein: but ye shall [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_26"></a>Leviticus 23:26

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_23_27"></a>Leviticus 23:27

Also on the tenth of this seventh [ḥōḏeš](../../strongs/h/h2320.md) there shall be a [yowm](../../strongs/h/h3117.md) of [kipur](../../strongs/h/h3725.md): it shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md) unto you; and ye shall [ʿānâ](../../strongs/h/h6031.md) your [nephesh](../../strongs/h/h5315.md), and [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_28"></a>Leviticus 23:28

And ye shall ['asah](../../strongs/h/h6213.md) no [mĕla'kah](../../strongs/h/h4399.md) in that ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md): for it is a [yowm](../../strongs/h/h3117.md) of [kipur](../../strongs/h/h3725.md), to [kāp̄ar](../../strongs/h/h3722.md) for you [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_23_29"></a>Leviticus 23:29

For whatsoever [nephesh](../../strongs/h/h5315.md) it be that shall not be [ʿānâ](../../strongs/h/h6031.md) in that ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), he shall be [karath](../../strongs/h/h3772.md) from among his ['am](../../strongs/h/h5971.md).

<a name="leviticus_23_30"></a>Leviticus 23:30

And whatsoever [nephesh](../../strongs/h/h5315.md) it be that ['asah](../../strongs/h/h6213.md) any [mĕla'kah](../../strongs/h/h4399.md) in that ['etsem](../../strongs/h/h6106.md) [yowm](../../strongs/h/h3117.md), the [nephesh](../../strongs/h/h5315.md) will I ['abad](../../strongs/h/h6.md) from [qereḇ](../../strongs/h/h7130.md) his ['am](../../strongs/h/h5971.md).

<a name="leviticus_23_31"></a>Leviticus 23:31

Ye shall ['asah](../../strongs/h/h6213.md) no manner of [mĕla'kah](../../strongs/h/h4399.md): it shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) throughout your [dôr](../../strongs/h/h1755.md) in all your [môšāḇ](../../strongs/h/h4186.md).

<a name="leviticus_23_32"></a>Leviticus 23:32

It shall be unto you a [shabbath](../../strongs/h/h7676.md) of [šabāṯôn](../../strongs/h/h7677.md), and ye shall [ʿānâ](../../strongs/h/h6031.md) your [nephesh](../../strongs/h/h5315.md): in the ninth of the [ḥōḏeš](../../strongs/h/h2320.md) at ['ereb](../../strongs/h/h6153.md), from ['ereb](../../strongs/h/h6153.md) unto ['ereb](../../strongs/h/h6153.md), shall ye [shabath](../../strongs/h/h7673.md) your [shabbath](../../strongs/h/h7676.md).

<a name="leviticus_23_33"></a>Leviticus 23:33

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_23_34"></a>Leviticus 23:34

[dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), The fifteenth [yowm](../../strongs/h/h3117.md) of this seventh [ḥōḏeš](../../strongs/h/h2320.md) shall be the [ḥāḡ](../../strongs/h/h2282.md) of [cukkah](../../strongs/h/h5521.md) for seven [yowm](../../strongs/h/h3117.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_35"></a>Leviticus 23:35

On the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md): ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein.

<a name="leviticus_23_36"></a>Leviticus 23:36

Seven [yowm](../../strongs/h/h3117.md) ye shall [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md): on the eighth [yowm](../../strongs/h/h3117.md) shall be a [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md) unto you; and ye shall [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md): it is an [ʿăṣārâ](../../strongs/h/h6116.md); and ye shall ['asah](../../strongs/h/h6213.md) no [ʿăḇōḏâ](../../strongs/h/h5656.md) [mĕla'kah](../../strongs/h/h4399.md) therein.

<a name="leviticus_23_37"></a>Leviticus 23:37

These are the [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md), which ye shall [qara'](../../strongs/h/h7121.md) to be [qodesh](../../strongs/h/h6944.md) [miqrā'](../../strongs/h/h4744.md), to [qāraḇ](../../strongs/h/h7126.md) an ['iššê](../../strongs/h/h801.md) unto [Yĕhovah](../../strongs/h/h3068.md), an [ʿōlâ](../../strongs/h/h5930.md), and a [minchah](../../strongs/h/h4503.md), a [zebach](../../strongs/h/h2077.md), and [necek](../../strongs/h/h5262.md), [dabar](../../strongs/h/h1697.md) upon his [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md):

<a name="leviticus_23_38"></a>Leviticus 23:38

Beside the [shabbath](../../strongs/h/h7676.md) of [Yĕhovah](../../strongs/h/h3068.md), and beside your [matānâ](../../strongs/h/h4979.md), and beside all your [neḏer](../../strongs/h/h5088.md), and beside all your [nᵊḏāḇâ](../../strongs/h/h5071.md), which ye [nathan](../../strongs/h/h5414.md) unto [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_23_39"></a>Leviticus 23:39

Also in the fifteenth [yowm](../../strongs/h/h3117.md) of the seventh [ḥōḏeš](../../strongs/h/h2320.md), when ye have ['āsap̄](../../strongs/h/h622.md) in the [tᵊḇû'â](../../strongs/h/h8393.md) of the ['erets](../../strongs/h/h776.md), ye shall [ḥāḡaḡ](../../strongs/h/h2287.md) a [ḥāḡ](../../strongs/h/h2282.md) unto [Yĕhovah](../../strongs/h/h3068.md) seven [yowm](../../strongs/h/h3117.md): on the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) shall be a [šabāṯôn](../../strongs/h/h7677.md), and on the eighth [yowm](../../strongs/h/h3117.md) shall be a [šabāṯôn](../../strongs/h/h7677.md).

<a name="leviticus_23_40"></a>Leviticus 23:40

And ye shall [laqach](../../strongs/h/h3947.md) you on the [ri'šôn](../../strongs/h/h7223.md) [yowm](../../strongs/h/h3117.md) the [pĕriy](../../strongs/h/h6529.md) of [hadar](../../strongs/h/h1926.md) ['ets](../../strongs/h/h6086.md), [kaph](../../strongs/h/h3709.md) of [tāmār](../../strongs/h/h8558.md), and the [ʿānāp̄](../../strongs/h/h6057.md) of [ʿāḇōṯ](../../strongs/h/h5687.md) ['ets](../../strongs/h/h6086.md), and [ʿărāḇâ](../../strongs/h/h6155.md) of the [nachal](../../strongs/h/h5158.md); and ye shall [samach](../../strongs/h/h8055.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) seven [yowm](../../strongs/h/h3117.md).

<a name="leviticus_23_41"></a>Leviticus 23:41

And ye shall [ḥāḡaḡ](../../strongs/h/h2287.md) it a [ḥāḡ](../../strongs/h/h2282.md) unto [Yĕhovah](../../strongs/h/h3068.md) seven [yowm](../../strongs/h/h3117.md) in the [šānâ](../../strongs/h/h8141.md). It shall be a [chuqqah](../../strongs/h/h2708.md) ['owlam](../../strongs/h/h5769.md) in your [dôr](../../strongs/h/h1755.md): ye shall [ḥāḡaḡ](../../strongs/h/h2287.md) it in the seventh [ḥōḏeš](../../strongs/h/h2320.md).

<a name="leviticus_23_42"></a>Leviticus 23:42

Ye shall [yashab](../../strongs/h/h3427.md) in [cukkah](../../strongs/h/h5521.md) seven [yowm](../../strongs/h/h3117.md); all that are [Yisra'el](../../strongs/h/h3478.md) ['ezrāḥ](../../strongs/h/h249.md) shall [yashab](../../strongs/h/h3427.md) in [cukkah](../../strongs/h/h5521.md):

<a name="leviticus_23_43"></a>Leviticus 23:43

That your [dôr](../../strongs/h/h1755.md) may [yada'](../../strongs/h/h3045.md) that I made the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) to [yashab](../../strongs/h/h3427.md) in [cukkah](../../strongs/h/h5521.md), when I [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_23_44"></a>Leviticus 23:44

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) unto the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) the [môʿēḏ](../../strongs/h/h4150.md) of [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 22](leviticus_22.md) - [Leviticus 24](leviticus_24.md)