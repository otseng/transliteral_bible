# [Leviticus 21](https://www.blueletterbible.org/kjv/lev/21)

<a name="leviticus_21_1"></a>Leviticus 21:1

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md) unto the [kōhēn](../../strongs/h/h3548.md) the [ben](../../strongs/h/h1121.md) of ['Ahărôn](../../strongs/h/h175.md), and say unto them, There shall none be [ṭāmē'](../../strongs/h/h2930.md) for the [nephesh](../../strongs/h/h5315.md) among his ['am](../../strongs/h/h5971.md):

<a name="leviticus_21_2"></a>Leviticus 21:2

But for his [šᵊ'ēr](../../strongs/h/h7607.md), that is [qarowb](../../strongs/h/h7138.md) unto him, that is, for his ['em](../../strongs/h/h517.md), and for his ['ab](../../strongs/h/h1.md), and for his [ben](../../strongs/h/h1121.md), and for his [bath](../../strongs/h/h1323.md), and for his ['ach](../../strongs/h/h251.md).

<a name="leviticus_21_3"></a>Leviticus 21:3

And for his ['āḥôṯ](../../strongs/h/h269.md) a [bᵊṯûlâ](../../strongs/h/h1330.md), that is [qarowb](../../strongs/h/h7138.md) unto him, which hath had no ['iysh](../../strongs/h/h376.md); for her may he be [ṭāmē'](../../strongs/h/h2930.md).

<a name="leviticus_21_4"></a>Leviticus 21:4

But he shall not [ṭāmē'](../../strongs/h/h2930.md) himself, being a [baʿal](../../strongs/h/h1167.md) among his ['am](../../strongs/h/h5971.md), to [ḥālal](../../strongs/h/h2490.md) himself.

<a name="leviticus_21_5"></a>Leviticus 21:5

They shall not [qāraḥ](../../strongs/h/h7139.md) [qrḥ](../../strongs/h/h7144.md) upon their [ro'sh](../../strongs/h/h7218.md), neither shall they [gālaḥ](../../strongs/h/h1548.md) the [pē'â](../../strongs/h/h6285.md) of their [zāqān](../../strongs/h/h2206.md), nor [śāraṭ](../../strongs/h/h8295.md) any [śereṭ](../../strongs/h/h8296.md) in their [basar](../../strongs/h/h1320.md).

<a name="leviticus_21_6"></a>Leviticus 21:6

They shall be [qodesh](../../strongs/h/h6944.md) unto their ['Elohiym](../../strongs/h/h430.md), and not [ḥālal](../../strongs/h/h2490.md) the [shem](../../strongs/h/h8034.md) of their ['Elohiym](../../strongs/h/h430.md): for the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [lechem](../../strongs/h/h3899.md) of their ['Elohiym](../../strongs/h/h430.md), they do [qāraḇ](../../strongs/h/h7126.md): therefore they shall be [qadowsh](../../strongs/h/h6918.md).

<a name="leviticus_21_7"></a>Leviticus 21:7

They shall not [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) that is a [zānâ](../../strongs/h/h2181.md), or [ḥālāl](../../strongs/h/h2491.md); neither shall they [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) [gāraš](../../strongs/h/h1644.md) from her ['iysh](../../strongs/h/h376.md): for he is [qadowsh](../../strongs/h/h6918.md) unto his ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_21_8"></a>Leviticus 21:8

Thou shalt [qadash](../../strongs/h/h6942.md) him therefore; for he [qāraḇ](../../strongs/h/h7126.md) the [lechem](../../strongs/h/h3899.md) of thy ['Elohiym](../../strongs/h/h430.md): he shall be [qadowsh](../../strongs/h/h6918.md) unto thee: for I [Yĕhovah](../../strongs/h/h3068.md), which [qadash](../../strongs/h/h6942.md) you, am [qadowsh](../../strongs/h/h6918.md).

<a name="leviticus_21_9"></a>Leviticus 21:9

And the [bath](../../strongs/h/h1323.md) of ['iysh](../../strongs/h/h376.md) [kōhēn](../../strongs/h/h3548.md), if she [ḥālal](../../strongs/h/h2490.md) herself by [zānâ](../../strongs/h/h2181.md), she [ḥālal](../../strongs/h/h2490.md) her ['ab](../../strongs/h/h1.md): she shall be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="leviticus_21_10"></a>Leviticus 21:10

And he that is the [gadowl](../../strongs/h/h1419.md) [kōhēn](../../strongs/h/h3548.md) among his ['ach](../../strongs/h/h251.md), upon whose [ro'sh](../../strongs/h/h7218.md) the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md) was [yāṣaq](../../strongs/h/h3332.md), and that is [mālā'](../../strongs/h/h4390.md) [yad](../../strongs/h/h3027.md) to [labash](../../strongs/h/h3847.md) on the [beḡeḏ](../../strongs/h/h899.md), shall not [pāraʿ](../../strongs/h/h6544.md) his [ro'sh](../../strongs/h/h7218.md), nor [pāram](../../strongs/h/h6533.md) his [beḡeḏ](../../strongs/h/h899.md);

<a name="leviticus_21_11"></a>Leviticus 21:11

Neither shall he [bow'](../../strongs/h/h935.md) to any [muwth](../../strongs/h/h4191.md) [nephesh](../../strongs/h/h5315.md), nor [ṭāmē'](../../strongs/h/h2930.md) himself for his ['ab](../../strongs/h/h1.md), or for his ['em](../../strongs/h/h517.md);

<a name="leviticus_21_12"></a>Leviticus 21:12

Neither shall he [yāṣā'](../../strongs/h/h3318.md) of the [miqdash](../../strongs/h/h4720.md), nor [ḥālal](../../strongs/h/h2490.md) the [miqdash](../../strongs/h/h4720.md) of his ['Elohiym](../../strongs/h/h430.md); for the [nēzer](../../strongs/h/h5145.md) of the [māšḥâ](../../strongs/h/h4888.md) [šemen](../../strongs/h/h8081.md) of his ['Elohiym](../../strongs/h/h430.md) is upon him: I am [Yĕhovah](../../strongs/h/h3068.md).

<a name="leviticus_21_13"></a>Leviticus 21:13

And he shall [laqach](../../strongs/h/h3947.md) an ['ishshah](../../strongs/h/h802.md) in her [bᵊṯûlîm](../../strongs/h/h1331.md).

<a name="leviticus_21_14"></a>Leviticus 21:14

An ['almānâ](../../strongs/h/h490.md), or a [gāraš](../../strongs/h/h1644.md), or [ḥālāl](../../strongs/h/h2491.md), or a [zānâ](../../strongs/h/h2181.md), these shall he not [laqach](../../strongs/h/h3947.md): but he shall [laqach](../../strongs/h/h3947.md) a [bᵊṯûlâ](../../strongs/h/h1330.md) of his own ['am](../../strongs/h/h5971.md) to ['ishshah](../../strongs/h/h802.md).

<a name="leviticus_21_15"></a>Leviticus 21:15

Neither shall he [ḥālal](../../strongs/h/h2490.md) his [zera'](../../strongs/h/h2233.md) among his ['am](../../strongs/h/h5971.md): for I [Yĕhovah](../../strongs/h/h3068.md) do [qadash](../../strongs/h/h6942.md) him.

<a name="leviticus_21_16"></a>Leviticus 21:16

And [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto [Mōshe](../../strongs/h/h4872.md), ['āmar](../../strongs/h/h559.md),

<a name="leviticus_21_17"></a>Leviticus 21:17

[dabar](../../strongs/h/h1696.md) unto ['Ahărôn](../../strongs/h/h175.md), ['āmar](../../strongs/h/h559.md), ['iysh](../../strongs/h/h376.md) be of thy [zera'](../../strongs/h/h2233.md) in their [dôr](../../strongs/h/h1755.md) that hath any [mᵊ'ûm](../../strongs/h/h3971.md), let him not [qāraḇ](../../strongs/h/h7126.md) to [qāraḇ](../../strongs/h/h7126.md) the [lechem](../../strongs/h/h3899.md) of his ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_21_18"></a>Leviticus 21:18

For whatsoever ['iysh](../../strongs/h/h376.md) he be that hath a [mᵊ'ûm](../../strongs/h/h3971.md), he shall not [qāraḇ](../../strongs/h/h7126.md): a [ʿiûēr](../../strongs/h/h5787.md) ['iysh](../../strongs/h/h376.md), or a [pissēaḥ](../../strongs/h/h6455.md), or he that hath a [ḥāram](../../strongs/h/h2763.md), or any thing [śāraʿ](../../strongs/h/h8311.md),

<a name="leviticus_21_19"></a>Leviticus 21:19

Or an ['iysh](../../strongs/h/h376.md) that is [šeḇar](../../strongs/h/h7667.md) [regel](../../strongs/h/h7272.md), or [šeḇar](../../strongs/h/h7667.md) [yad](../../strongs/h/h3027.md),

<a name="leviticus_21_20"></a>Leviticus 21:20

Or [gibēn](../../strongs/h/h1384.md), or a [daq](../../strongs/h/h1851.md), or that hath a [tᵊḇallul](../../strongs/h/h8400.md) in his ['ayin](../../strongs/h/h5869.md), or be [gārāḇ](../../strongs/h/h1618.md), or [yallep̄eṯ](../../strongs/h/h3217.md), or hath his ['ešeḵ](../../strongs/h/h810.md) [mārôaḥ](../../strongs/h/h4790.md);

<a name="leviticus_21_21"></a>Leviticus 21:21

No ['iysh](../../strongs/h/h376.md) that hath a [mᵊ'ûm](../../strongs/h/h3971.md) of the [zera'](../../strongs/h/h2233.md) of ['Ahărôn](../../strongs/h/h175.md) the [kōhēn](../../strongs/h/h3548.md) shall [nāḡaš](../../strongs/h/h5066.md) to [qāraḇ](../../strongs/h/h7126.md) the ['iššê](../../strongs/h/h801.md) of [Yĕhovah](../../strongs/h/h3068.md): he hath a [mᵊ'ûm](../../strongs/h/h3971.md)h; he shall not [nāḡaš](../../strongs/h/h5066.md) to [qāraḇ](../../strongs/h/h7126.md) the [lechem](../../strongs/h/h3899.md) of his ['Elohiym](../../strongs/h/h430.md).

<a name="leviticus_21_22"></a>Leviticus 21:22

He shall ['akal](../../strongs/h/h398.md) the [lechem](../../strongs/h/h3899.md) of his ['Elohiym](../../strongs/h/h430.md), both of the [qodesh](../../strongs/h/h6944.md) [qodesh](../../strongs/h/h6944.md), and of the [qodesh](../../strongs/h/h6944.md).

<a name="leviticus_21_23"></a>Leviticus 21:23

Only he shall not [bow'](../../strongs/h/h935.md) unto the [pārōḵeṯ](../../strongs/h/h6532.md), nor [nāḡaš](../../strongs/h/h5066.md) unto the [mizbeach](../../strongs/h/h4196.md), because he hath a [mᵊ'ûm](../../strongs/h/h3971.md); that he [ḥālal](../../strongs/h/h2490.md) not my [miqdash](../../strongs/h/h4720.md): for I [Yĕhovah](../../strongs/h/h3068.md) do [qadash](../../strongs/h/h6942.md) them.

<a name="leviticus_21_24"></a>Leviticus 21:24

And [Mōshe](../../strongs/h/h4872.md) [dabar](../../strongs/h/h1696.md) it unto ['Ahărôn](../../strongs/h/h175.md), and to his [ben](../../strongs/h/h1121.md), and unto all the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

---

[Transliteral Bible](../bible.md)

[Leviticus](leviticus.md)

[Leviticus 20](leviticus_20.md) - [Leviticus 22](leviticus_22.md)