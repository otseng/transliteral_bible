# [Philemon 1](https://www.blueletterbible.org/kjv/phm/1/1/s_1133001)

<a name="philemon_1_1"></a>Philemon 1:1

[Paulos](../../strongs/g/g3972.md), a [desmios](../../strongs/g/g1198.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md), and [Timotheos](../../strongs/g/g5095.md) [adelphos](../../strongs/g/g80.md), unto [philēmōn](../../strongs/g/g5371.md) our [agapētos](../../strongs/g/g27.md), and [synergos](../../strongs/g/g4904.md),

<a name="philemon_1_2"></a>Philemon 1:2

And to [agapētos](../../strongs/g/g27.md) [apphia](../../strongs/g/g682.md), and [archippos](../../strongs/g/g751.md) our [systratiōtēs](../../strongs/g/g4961.md), and to the [ekklēsia](../../strongs/g/g1577.md) in thy [oikos](../../strongs/g/g3624.md):

<a name="philemon_1_3"></a>Philemon 1:3

[charis](../../strongs/g/g5485.md) to you, and [eirēnē](../../strongs/g/g1515.md), from [theos](../../strongs/g/g2316.md) our [patēr](../../strongs/g/g3962.md) and the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="philemon_1_4"></a>Philemon 1:4

I [eucharisteō](../../strongs/g/g2168.md) my [theos](../../strongs/g/g2316.md), [poieō](../../strongs/g/g4160.md) [mneia](../../strongs/g/g3417.md) of thee [pantote](../../strongs/g/g3842.md) in my [proseuchē](../../strongs/g/g4335.md),

<a name="philemon_1_5"></a>Philemon 1:5

[akouō](../../strongs/g/g191.md) of thy [agapē](../../strongs/g/g26.md) and [pistis](../../strongs/g/g4102.md), which thou hast toward the [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md), and toward all [hagios](../../strongs/g/g40.md);

<a name="philemon_1_6"></a>Philemon 1:6

That the [koinōnia](../../strongs/g/g2842.md) of thy [pistis](../../strongs/g/g4102.md) may become [energēs](../../strongs/g/g1756.md) by the [epignōsis](../../strongs/g/g1922.md) of every [agathos](../../strongs/g/g18.md) which is in you in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md).

<a name="philemon_1_7"></a>Philemon 1:7

For we have [polys](../../strongs/g/g4183.md) [charis](../../strongs/g/g5485.md) and [paraklēsis](../../strongs/g/g3874.md) in thy [agapē](../../strongs/g/g26.md), because the [splagchnon](../../strongs/g/g4698.md) of the [hagios](../../strongs/g/g40.md) are [anapauō](../../strongs/g/g373.md) by thee, [adelphos](../../strongs/g/g80.md).

<a name="philemon_1_8"></a>Philemon 1:8

Wherefore, though I might be [polys](../../strongs/g/g4183.md) [parrēsia](../../strongs/g/g3954.md) in [Christos](../../strongs/g/g5547.md) to [epitassō](../../strongs/g/g2004.md) thee that which is [anēkō](../../strongs/g/g433.md),

<a name="philemon_1_9"></a>Philemon 1:9

Yet for [agapē](../../strongs/g/g26.md) sake I rather [parakaleō](../../strongs/g/g3870.md) thee, being such an one as [Paulos](../../strongs/g/g3972.md) the [presbytēs](../../strongs/g/g4246.md), and [nyni](../../strongs/g/g3570.md) also a [desmios](../../strongs/g/g1198.md) of [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md).

<a name="philemon_1_10"></a>Philemon 1:10

I [parakaleō](../../strongs/g/g3870.md) thee for my [teknon](../../strongs/g/g5043.md) [Onēsimos](../../strongs/g/g3682.md), whom I have [gennaō](../../strongs/g/g1080.md) in my [desmos](../../strongs/g/g1199.md):

<a name="philemon_1_11"></a>Philemon 1:11

Which in time past was to thee [achrēstos](../../strongs/g/g890.md), but [nyni](../../strongs/g/g3570.md) [euchrēstos](../../strongs/g/g2173.md) to thee and to me:

<a name="philemon_1_12"></a>Philemon 1:12

Whom I have [anapempō](../../strongs/g/g375.md): thou therefore [proslambanō](../../strongs/g/g4355.md) him, that is, mine own [splagchnon](../../strongs/g/g4698.md):

<a name="philemon_1_13"></a>Philemon 1:13

Whom I [boulomai](../../strongs/g/g1014.md) have [katechō](../../strongs/g/g2722.md) with me, that in thy stead he might have [diakoneō](../../strongs/g/g1247.md) unto me in the [desmos](../../strongs/g/g1199.md) of the [euaggelion](../../strongs/g/g2098.md):

<a name="philemon_1_14"></a>Philemon 1:14

But without thy [gnōmē](../../strongs/g/g1106.md) would I [poieō](../../strongs/g/g4160.md) nothing; that thy [agathos](../../strongs/g/g18.md) should not be as it were of [anagkē](../../strongs/g/g318.md), but [hekousios](../../strongs/g/g1595.md).

<a name="philemon_1_15"></a>Philemon 1:15

For [tacha](../../strongs/g/g5029.md) he therefore [chōrizō](../../strongs/g/g5563.md) for a [hōra](../../strongs/g/g5610.md), that thou shouldest [apechō](../../strongs/g/g568.md) him [aiōnios](../../strongs/g/g166.md);

<a name="philemon_1_16"></a>Philemon 1:16

Not now as a [doulos](../../strongs/g/g1401.md), but above a [doulos](../../strongs/g/g1401.md), an [adelphos](../../strongs/g/g80.md) [agapētos](../../strongs/g/g27.md), [malista](../../strongs/g/g3122.md) to me, but how much more unto thee, both in the [sarx](../../strongs/g/g4561.md), and in the [kyrios](../../strongs/g/g2962.md)?

<a name="philemon_1_17"></a>Philemon 1:17

If thou count me therefore a [koinōnos](../../strongs/g/g2844.md), [proslambanō](../../strongs/g/g4355.md) him as myself.

<a name="philemon_1_18"></a>Philemon 1:18

If he hath [adikeō](../../strongs/g/g91.md) thee, or [opheilō](../../strongs/g/g3784.md) thee ought, [ellogeō](../../strongs/g/g1677.md) that on [emoi](../../strongs/g/g1698.md);

<a name="philemon_1_19"></a>Philemon 1:19

I [Paulos](../../strongs/g/g3972.md) have [graphō](../../strongs/g/g1125.md) it with mine own [cheir](../../strongs/g/g5495.md), I will [apotinō](../../strongs/g/g661.md): albeit I do not [legō](../../strongs/g/g3004.md) to thee how thou [prosopheilō](../../strongs/g/g4359.md) unto me even thine own self.

<a name="philemon_1_20"></a>Philemon 1:20

[nai](../../strongs/g/g3483.md), [adelphos](../../strongs/g/g80.md), let me have [oninēmi](../../strongs/g/g3685.md) of thee in the [kyrios](../../strongs/g/g2962.md): [anapauō](../../strongs/g/g373.md) my [splagchnon](../../strongs/g/g4698.md) in the [kyrios](../../strongs/g/g2962.md).

<a name="philemon_1_21"></a>Philemon 1:21

[peithō](../../strongs/g/g3982.md) in thy [hypakoē](../../strongs/g/g5218.md) I [graphō](../../strongs/g/g1125.md) unto thee, [eidō](../../strongs/g/g1492.md) that thou wilt also [poieō](../../strongs/g/g4160.md) more than I [legō](../../strongs/g/g3004.md).

<a name="philemon_1_22"></a>Philemon 1:22

But withal [hetoimazō](../../strongs/g/g2090.md) me also a [xenia](../../strongs/g/g3578.md): for I [elpizō](../../strongs/g/g1679.md) that through your [proseuchē](../../strongs/g/g4335.md) I shall be [charizomai](../../strongs/g/g5483.md) unto you.

<a name="philemon_1_23"></a>Philemon 1:23

There [aspazomai](../../strongs/g/g782.md) thee [epaphras](../../strongs/g/g1889.md), my [synaichmalōtos](../../strongs/g/g4869.md) in [Christos](../../strongs/g/g5547.md) [Iēsous](../../strongs/g/g2424.md);

<a name="philemon_1_24"></a>Philemon 1:24

[Markos](../../strongs/g/g3138.md), [Aristarchos](../../strongs/g/g708.md), [dēmas](../../strongs/g/g1214.md), [loukas](../../strongs/g/g3065.md), my [synergos](../../strongs/g/g4904.md).

<a name="philemon_1_25"></a>Philemon 1:25

The [charis](../../strongs/g/g5485.md) of our [kyrios](../../strongs/g/g2962.md) [Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md) be with your [pneuma](../../strongs/g/g4151.md). [amēn](../../strongs/g/g281.md).

---

[Transliteral Bible](../bible.md)

[Philemon](philemon.md)

[Philemon 2](philemon_2.md)