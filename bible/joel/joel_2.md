# [Joel 2](https://www.blueletterbible.org/kjv/joel/2)

<a name="joel_2_1"></a>Joel 2:1

[tāqaʿ](../../strongs/h/h8628.md) ye the [šôp̄ār](../../strongs/h/h7782.md) in [Tsiyown](../../strongs/h/h6726.md), and [rûaʿ](../../strongs/h/h7321.md) in my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md): let all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) [ragaz](../../strongs/h/h7264.md): for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md), for it is [qarowb](../../strongs/h/h7138.md);

<a name="joel_2_2"></a>Joel 2:2

A [yowm](../../strongs/h/h3117.md) of [choshek](../../strongs/h/h2822.md) and of ['ăp̄ēlâ](../../strongs/h/h653.md), a [yowm](../../strongs/h/h3117.md) of [ʿānān](../../strongs/h/h6051.md) and of ['araphel](../../strongs/h/h6205.md), as the [šaḥar](../../strongs/h/h7837.md) [pāraś](../../strongs/h/h6566.md) upon the [har](../../strongs/h/h2022.md): a [rab](../../strongs/h/h7227.md) ['am](../../strongs/h/h5971.md) and an ['atsuwm](../../strongs/h/h6099.md); there hath not [hayah](../../strongs/h/h1961.md) ['owlam](../../strongs/h/h5769.md) the like, neither shall be any more ['aḥar](../../strongs/h/h310.md) it, even to the [šānâ](../../strongs/h/h8141.md) of [dôr](../../strongs/h/h1755.md) [dôr](../../strongs/h/h1755.md).

<a name="joel_2_3"></a>Joel 2:3

An ['esh](../../strongs/h/h784.md) ['akal](../../strongs/h/h398.md) [paniym](../../strongs/h/h6440.md) them; and ['aḥar](../../strongs/h/h310.md) them a [lehāḇâ](../../strongs/h/h3852.md) [lāhaṭ](../../strongs/h/h3857.md): the ['erets](../../strongs/h/h776.md) is as the [gan](../../strongs/h/h1588.md) of ['Eden](../../strongs/h/h5731.md) [paniym](../../strongs/h/h6440.md) them, and ['aḥar](../../strongs/h/h310.md) them a [šᵊmāmâ](../../strongs/h/h8077.md) [midbar](../../strongs/h/h4057.md); yea, and nothing shall [pᵊlêṭâ](../../strongs/h/h6413.md) them.

<a name="joel_2_4"></a>Joel 2:4

The [mar'ê](../../strongs/h/h4758.md) of them is as the [mar'ê](../../strongs/h/h4758.md) of [sûs](../../strongs/h/h5483.md); and as [pārāš](../../strongs/h/h6571.md), so shall they [rûṣ](../../strongs/h/h7323.md).

<a name="joel_2_5"></a>Joel 2:5

Like the [qowl](../../strongs/h/h6963.md) of [merkāḇâ](../../strongs/h/h4818.md) on the [ro'sh](../../strongs/h/h7218.md) of [har](../../strongs/h/h2022.md) shall they [rāqaḏ](../../strongs/h/h7540.md), like the [qowl](../../strongs/h/h6963.md) of a [lahaḇ](../../strongs/h/h3851.md) of ['esh](../../strongs/h/h784.md) that ['akal](../../strongs/h/h398.md) the [qaš](../../strongs/h/h7179.md), as an ['atsuwm](../../strongs/h/h6099.md) ['am](../../strongs/h/h5971.md) set in [milḥāmâ](../../strongs/h/h4421.md) ['arak](../../strongs/h/h6186.md).

<a name="joel_2_6"></a>Joel 2:6

Before their [paniym](../../strongs/h/h6440.md) the ['am](../../strongs/h/h5971.md) shall be much [chuwl](../../strongs/h/h2342.md): all [paniym](../../strongs/h/h6440.md) shall [qāḇaṣ](../../strongs/h/h6908.md) [pā'rûr](../../strongs/h/h6289.md).

<a name="joel_2_7"></a>Joel 2:7

They shall [rûṣ](../../strongs/h/h7323.md) like [gibôr](../../strongs/h/h1368.md); they shall [ʿālâ](../../strongs/h/h5927.md) the [ḥômâ](../../strongs/h/h2346.md) like ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md); and they shall [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) on his [derek](../../strongs/h/h1870.md), and they shall not [ʿāḇaṭ](../../strongs/h/h5670.md) their ['orach](../../strongs/h/h734.md):

<a name="joel_2_8"></a>Joel 2:8

Neither shall ['iysh](../../strongs/h/h376.md) [dāḥaq](../../strongs/h/h1766.md) ['ach](../../strongs/h/h251.md); they shall [yālaḵ](../../strongs/h/h3212.md) every [geḇer](../../strongs/h/h1397.md) in his [mĕcillah](../../strongs/h/h4546.md): and when they [naphal](../../strongs/h/h5307.md) upon the [šelaḥ](../../strongs/h/h7973.md), they shall not be [batsa'](../../strongs/h/h1214.md).

<a name="joel_2_9"></a>Joel 2:9

They shall [šāqaq](../../strongs/h/h8264.md) in the [ʿîr](../../strongs/h/h5892.md); they shall [rûṣ](../../strongs/h/h7323.md) upon the [ḥômâ](../../strongs/h/h2346.md), they shall [ʿālâ](../../strongs/h/h5927.md) upon the [bayith](../../strongs/h/h1004.md); they shall [bow'](../../strongs/h/h935.md) at the [ḥallôn](../../strongs/h/h2474.md) like a [gannāḇ](../../strongs/h/h1590.md).

<a name="joel_2_10"></a>Joel 2:10

The ['erets](../../strongs/h/h776.md) shall [ragaz](../../strongs/h/h7264.md) [paniym](../../strongs/h/h6440.md) them; the [shamayim](../../strongs/h/h8064.md) shall [rāʿaš](../../strongs/h/h7493.md): the [šemeš](../../strongs/h/h8121.md) and the [yareach](../../strongs/h/h3394.md) shall be [qāḏar](../../strongs/h/h6937.md), and the [kowkab](../../strongs/h/h3556.md) shall ['āsap̄](../../strongs/h/h622.md) their [nogahh](../../strongs/h/h5051.md):

<a name="joel_2_11"></a>Joel 2:11

And [Yĕhovah](../../strongs/h/h3068.md) shall [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md) [paniym](../../strongs/h/h6440.md) his [ḥayil](../../strongs/h/h2428.md): for his [maḥănê](../../strongs/h/h4264.md) is [me'od](../../strongs/h/h3966.md) [rab](../../strongs/h/h7227.md): for he is ['atsuwm](../../strongs/h/h6099.md) that ['asah](../../strongs/h/h6213.md) his [dabar](../../strongs/h/h1697.md): for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [gadowl](../../strongs/h/h1419.md) and [me'od](../../strongs/h/h3966.md) [yare'](../../strongs/h/h3372.md); and who can [kûl](../../strongs/h/h3557.md) it?

<a name="joel_2_12"></a>Joel 2:12

Therefore also now, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), [shuwb](../../strongs/h/h7725.md) ye even to me with all your [lebab](../../strongs/h/h3824.md), and with [ṣôm](../../strongs/h/h6685.md), and with [bĕkiy](../../strongs/h/h1065.md), and with [mispēḏ](../../strongs/h/h4553.md):

<a name="joel_2_13"></a>Joel 2:13

And [qāraʿ](../../strongs/h/h7167.md) your [lebab](../../strongs/h/h3824.md), and not your [beḡeḏ](../../strongs/h/h899.md), and [shuwb](../../strongs/h/h7725.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): for he is [ḥanwn](../../strongs/h/h2587.md) and [raḥwm](../../strongs/h/h7349.md), ['ārēḵ](../../strongs/h/h750.md) to ['aph](../../strongs/h/h639.md), and of [rab](../../strongs/h/h7227.md) [checed](../../strongs/h/h2617.md), and [nacham](../../strongs/h/h5162.md) him of the [ra'](../../strongs/h/h7451.md).

<a name="joel_2_14"></a>Joel 2:14

Who [yada'](../../strongs/h/h3045.md) if he will [shuwb](../../strongs/h/h7725.md) and [nacham](../../strongs/h/h5162.md), and [šā'ar](../../strongs/h/h7604.md) a [bĕrakah](../../strongs/h/h1293.md) ['aḥar](../../strongs/h/h310.md) him; even a [minchah](../../strongs/h/h4503.md) and a [necek](../../strongs/h/h5262.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md)?

<a name="joel_2_15"></a>Joel 2:15

[tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) in [Tsiyown](../../strongs/h/h6726.md), [qadash](../../strongs/h/h6942.md) a [ṣôm](../../strongs/h/h6685.md), [qara'](../../strongs/h/h7121.md) a [ʿăṣārâ](../../strongs/h/h6116.md):

<a name="joel_2_16"></a>Joel 2:16

['āsap̄](../../strongs/h/h622.md) the ['am](../../strongs/h/h5971.md), [qadash](../../strongs/h/h6942.md) the [qāhēl](../../strongs/h/h6951.md), [qāḇaṣ](../../strongs/h/h6908.md) the [zāqēn](../../strongs/h/h2205.md), ['āsap̄](../../strongs/h/h622.md) the ['owlel](../../strongs/h/h5768.md), and those that [yānaq](../../strongs/h/h3243.md) the [šaḏ](../../strongs/h/h7699.md): let the [ḥāṯān](../../strongs/h/h2860.md) [yāṣā'](../../strongs/h/h3318.md) of his [ḥeḏer](../../strongs/h/h2315.md), and the [kallâ](../../strongs/h/h3618.md) out of her [ḥupâ](../../strongs/h/h2646.md).

<a name="joel_2_17"></a>Joel 2:17

Let the [kōhēn](../../strongs/h/h3548.md), the [sharath](../../strongs/h/h8334.md) of [Yĕhovah](../../strongs/h/h3068.md), [bāḵâ](../../strongs/h/h1058.md) between the ['ûlām](../../strongs/h/h197.md) and the [mizbeach](../../strongs/h/h4196.md), and let them ['āmar](../../strongs/h/h559.md), [ḥûs](../../strongs/h/h2347.md) thy ['am](../../strongs/h/h5971.md), [Yĕhovah](../../strongs/h/h3068.md), and [nathan](../../strongs/h/h5414.md) not thine [nachalah](../../strongs/h/h5159.md) to [cherpah](../../strongs/h/h2781.md), that the [gowy](../../strongs/h/h1471.md) should [mashal](../../strongs/h/h4910.md) them: wherefore should they ['āmar](../../strongs/h/h559.md) among the ['am](../../strongs/h/h5971.md), Where is their ['Elohiym](../../strongs/h/h430.md)?

<a name="joel_2_18"></a>Joel 2:18

Then will [Yĕhovah](../../strongs/h/h3068.md) be [qānā'](../../strongs/h/h7065.md) for his ['erets](../../strongs/h/h776.md), and [ḥāmal](../../strongs/h/h2550.md) his ['am](../../strongs/h/h5971.md).

<a name="joel_2_19"></a>Joel 2:19

Yea, [Yĕhovah](../../strongs/h/h3068.md) will ['anah](../../strongs/h/h6030.md) and ['āmar](../../strongs/h/h559.md) unto his ['am](../../strongs/h/h5971.md), Behold, I will [shalach](../../strongs/h/h7971.md) you [dagan](../../strongs/h/h1715.md), and [tiyrowsh](../../strongs/h/h8492.md), and [yiṣhār](../../strongs/h/h3323.md), and ye shall be [sāׂbaʿ](../../strongs/h/h7646.md) therewith: and I will no more [nathan](../../strongs/h/h5414.md) you a [cherpah](../../strongs/h/h2781.md) among the [gowy](../../strongs/h/h1471.md):

<a name="joel_2_20"></a>Joel 2:20

But I will [rachaq](../../strongs/h/h7368.md) from you the [ṣᵊp̄ônî](../../strongs/h/h6830.md), and will [nāḏaḥ](../../strongs/h/h5080.md) him into an ['erets](../../strongs/h/h776.md) [ṣîyâ](../../strongs/h/h6723.md) and [šᵊmāmâ](../../strongs/h/h8077.md), with his [paniym](../../strongs/h/h6440.md) toward the [qaḏmōnî](../../strongs/h/h6931.md) [yam](../../strongs/h/h3220.md), and his [sôp̄](../../strongs/h/h5490.md) toward the ['aḥărôn](../../strongs/h/h314.md) [yam](../../strongs/h/h3220.md), and his [bᵊ'š](../../strongs/h/h889.md) shall [ʿālâ](../../strongs/h/h5927.md), and his ill [ṣaḥănâ](../../strongs/h/h6709.md) shall [ʿālâ](../../strongs/h/h5927.md), because he hath ['asah](../../strongs/h/h6213.md) [gāḏal](../../strongs/h/h1431.md).

<a name="joel_2_21"></a>Joel 2:21

[yare'](../../strongs/h/h3372.md) not, O ['ăḏāmâ](../../strongs/h/h127.md); be [giyl](../../strongs/h/h1523.md) and [samach](../../strongs/h/h8055.md): for [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) [gāḏal](../../strongs/h/h1431.md).

<a name="joel_2_22"></a>Joel 2:22

Be not [yare'](../../strongs/h/h3372.md), ye [bĕhemah](../../strongs/h/h929.md) of the [sadeh](../../strongs/h/h7704.md): for the [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md) do [dāšā'](../../strongs/h/h1876.md), for the ['ets](../../strongs/h/h6086.md) [nasa'](../../strongs/h/h5375.md) her [pĕriy](../../strongs/h/h6529.md), the [tĕ'en](../../strongs/h/h8384.md) and the [gep̄en](../../strongs/h/h1612.md) do [nathan](../../strongs/h/h5414.md) their [ḥayil](../../strongs/h/h2428.md).

<a name="joel_2_23"></a>Joel 2:23

Be [giyl](../../strongs/h/h1523.md) then, ye [ben](../../strongs/h/h1121.md) of [Tsiyown](../../strongs/h/h6726.md), and [samach](../../strongs/h/h8055.md) in [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md): for he hath [nathan](../../strongs/h/h5414.md) you the [môrê](../../strongs/h/h4175.md) [tsedaqah](../../strongs/h/h6666.md), and he will cause to [yarad](../../strongs/h/h3381.md) for you the [gešem](../../strongs/h/h1653.md), the [môrê](../../strongs/h/h4175.md), and the [malqôš](../../strongs/h/h4456.md) in the [ri'šôn](../../strongs/h/h7223.md) month.

<a name="joel_2_24"></a>Joel 2:24

And the [gōren](../../strongs/h/h1637.md) shall be [mālā'](../../strongs/h/h4390.md) of [bar](../../strongs/h/h1250.md), and the [yeqeḇ](../../strongs/h/h3342.md) shall [šûq](../../strongs/h/h7783.md) with [tiyrowsh](../../strongs/h/h8492.md) and [yiṣhār](../../strongs/h/h3323.md).

<a name="joel_2_25"></a>Joel 2:25

And I will [shalam](../../strongs/h/h7999.md) to you the [šānâ](../../strongs/h/h8141.md) that the ['arbê](../../strongs/h/h697.md) hath ['akal](../../strongs/h/h398.md), the [yeleq](../../strongs/h/h3218.md), and the [ḥāsîl](../../strongs/h/h2625.md), and the [gāzām](../../strongs/h/h1501.md), my [gadowl](../../strongs/h/h1419.md) [ḥayil](../../strongs/h/h2428.md) which I [shalach](../../strongs/h/h7971.md) among you.

<a name="joel_2_26"></a>Joel 2:26

And ye shall ['akal](../../strongs/h/h398.md) in ['akal](../../strongs/h/h398.md), and be [sāׂbaʿ](../../strongs/h/h7646.md), and [halal](../../strongs/h/h1984.md) the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), that hath ['asah](../../strongs/h/h6213.md) [pala'](../../strongs/h/h6381.md) with you: and my ['am](../../strongs/h/h5971.md) shall ['owlam](../../strongs/h/h5769.md) be [buwsh](../../strongs/h/h954.md).

<a name="joel_2_27"></a>Joel 2:27

And ye shall [yada'](../../strongs/h/h3045.md) that I am in the [qereḇ](../../strongs/h/h7130.md) of [Yisra'el](../../strongs/h/h3478.md), and that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and none else: and my ['am](../../strongs/h/h5971.md) shall ['owlam](../../strongs/h/h5769.md) be [buwsh](../../strongs/h/h954.md).

<a name="joel_2_28"></a>Joel 2:28

And it shall come to pass ['aḥar](../../strongs/h/h310.md), that I will [šāp̄aḵ](../../strongs/h/h8210.md) my [ruwach](../../strongs/h/h7307.md) upon all [basar](../../strongs/h/h1320.md); and your [ben](../../strongs/h/h1121.md) and your [bath](../../strongs/h/h1323.md) shall [nāḇā'](../../strongs/h/h5012.md), your [zāqēn](../../strongs/h/h2205.md) shall [ḥālam](../../strongs/h/h2492.md) [ḥălôm](../../strongs/h/h2472.md), your [bāḥûr](../../strongs/h/h970.md) shall [ra'ah](../../strongs/h/h7200.md) [ḥizzāyôn](../../strongs/h/h2384.md):

<a name="joel_2_29"></a>Joel 2:29

And also upon the ['ebed](../../strongs/h/h5650.md) and upon the [šip̄ḥâ](../../strongs/h/h8198.md) in those [yowm](../../strongs/h/h3117.md) will I [šāp̄aḵ](../../strongs/h/h8210.md) my [ruwach](../../strongs/h/h7307.md).

<a name="joel_2_30"></a>Joel 2:30

And I will [nathan](../../strongs/h/h5414.md) [môp̄ēṯ](../../strongs/h/h4159.md) in the [shamayim](../../strongs/h/h8064.md) and in the ['erets](../../strongs/h/h776.md), [dam](../../strongs/h/h1818.md), and ['esh](../../strongs/h/h784.md), and [tîmārâ](../../strongs/h/h8490.md) of ['ashan](../../strongs/h/h6227.md).

<a name="joel_2_31"></a>Joel 2:31

The [šemeš](../../strongs/h/h8121.md) shall be [hāp̄aḵ](../../strongs/h/h2015.md) into [choshek](../../strongs/h/h2822.md), and the [yareach](../../strongs/h/h3394.md) into [dam](../../strongs/h/h1818.md), [paniym](../../strongs/h/h6440.md) the [gadowl](../../strongs/h/h1419.md) and the [yare'](../../strongs/h/h3372.md) [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) [bow'](../../strongs/h/h935.md).

<a name="joel_2_32"></a>Joel 2:32

And it shall come to pass, that whosoever shall [qara'](../../strongs/h/h7121.md) on the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be [mālaṭ](../../strongs/h/h4422.md): for in [har](../../strongs/h/h2022.md) [Tsiyown](../../strongs/h/h6726.md) and in [Yĕruwshalaim](../../strongs/h/h3389.md) shall be [pᵊlêṭâ](../../strongs/h/h6413.md), as [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md), and in the [śārîḏ](../../strongs/h/h8300.md) whom [Yĕhovah](../../strongs/h/h3068.md) shall [qara'](../../strongs/h/h7121.md).

---

[Transliteral Bible](../bible.md)

[Joel](joel.md)

[Joel 1](joel_1.md) - [Joel 3](joel_3.md)