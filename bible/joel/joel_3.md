# [Joel 3](https://www.blueletterbible.org/kjv/joel/3)

<a name="joel_3_1"></a>Joel 3:1

For, behold, in those [yowm](../../strongs/h/h3117.md), and in that [ʿēṯ](../../strongs/h/h6256.md), when I shall [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md),

<a name="joel_3_2"></a>Joel 3:2

I will also [qāḇaṣ](../../strongs/h/h6908.md) all [gowy](../../strongs/h/h1471.md), and will [yarad](../../strongs/h/h3381.md) them into the [ʿēmeq](../../strongs/h/h6010.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md), and will [shaphat](../../strongs/h/h8199.md) with them there for my ['am](../../strongs/h/h5971.md) and for my [nachalah](../../strongs/h/h5159.md) [Yisra'el](../../strongs/h/h3478.md), whom they have [p̄zr](../../strongs/h/h6340.md) among the [gowy](../../strongs/h/h1471.md), and [chalaq](../../strongs/h/h2505.md) my ['erets](../../strongs/h/h776.md).

<a name="joel_3_3"></a>Joel 3:3

And they have [yāḏaḏ](../../strongs/h/h3032.md) [gôrāl](../../strongs/h/h1486.md) for my ['am](../../strongs/h/h5971.md); and have [nathan](../../strongs/h/h5414.md) a [yeleḏ](../../strongs/h/h3206.md) for a [zānâ](../../strongs/h/h2181.md), and [māḵar](../../strongs/h/h4376.md) a [yaldâ](../../strongs/h/h3207.md) for [yayin](../../strongs/h/h3196.md), that they might [šāṯâ](../../strongs/h/h8354.md).

<a name="joel_3_4"></a>Joel 3:4

Yea, and what have ye to do with me, O [Ṣōr](../../strongs/h/h6865.md), and [Ṣîḏôn](../../strongs/h/h6721.md), and all the [gᵊlîlâ](../../strongs/h/h1552.md) of [pᵊlešeṯ](../../strongs/h/h6429.md)? will ye [shalam](../../strongs/h/h7999.md) me a [gĕmwl](../../strongs/h/h1576.md)? and if ye [gamal](../../strongs/h/h1580.md) me, [qal](../../strongs/h/h7031.md) and [mᵊhērâ](../../strongs/h/h4120.md) will I [shuwb](../../strongs/h/h7725.md) your [gĕmwl](../../strongs/h/h1576.md) upon your own [ro'sh](../../strongs/h/h7218.md);

<a name="joel_3_5"></a>Joel 3:5

Because ye have [laqach](../../strongs/h/h3947.md) my [keceph](../../strongs/h/h3701.md) and my [zāhāḇ](../../strongs/h/h2091.md), and have [bow'](../../strongs/h/h935.md) into your [heykal](../../strongs/h/h1964.md) my [towb](../../strongs/h/h2896.md) [maḥmāḏ](../../strongs/h/h4261.md):

<a name="joel_3_6"></a>Joel 3:6

The [ben](../../strongs/h/h1121.md) also of [Yehuwdah](../../strongs/h/h3063.md) and the [ben](../../strongs/h/h1121.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) have ye [māḵar](../../strongs/h/h4376.md) unto the [yᵊvānî](../../strongs/h/h3125.md) [ben](../../strongs/h/h1121.md), that ye might [rachaq](../../strongs/h/h7368.md) them from their [gᵊḇûl](../../strongs/h/h1366.md).

<a name="joel_3_7"></a>Joel 3:7

Behold, I will [ʿûr](../../strongs/h/h5782.md) them out of the [maqowm](../../strongs/h/h4725.md) whither ye have [māḵar](../../strongs/h/h4376.md) them, and will [shuwb](../../strongs/h/h7725.md) your [gĕmwl](../../strongs/h/h1576.md) upon your own [ro'sh](../../strongs/h/h7218.md):

<a name="joel_3_8"></a>Joel 3:8

And I will [māḵar](../../strongs/h/h4376.md) your [ben](../../strongs/h/h1121.md) and your [bath](../../strongs/h/h1323.md) into the [yad](../../strongs/h/h3027.md) of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), and they shall [māḵar](../../strongs/h/h4376.md) them to the [šᵊḇā'î](../../strongs/h/h7615.md), to a [gowy](../../strongs/h/h1471.md) far [rachowq](../../strongs/h/h7350.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) it.

<a name="joel_3_9"></a>Joel 3:9

[qara'](../../strongs/h/h7121.md) ye this among the [gowy](../../strongs/h/h1471.md); [qadash](../../strongs/h/h6942.md) [milḥāmâ](../../strongs/h/h4421.md), [ʿûr](../../strongs/h/h5782.md) the [gibôr](../../strongs/h/h1368.md), let all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) [nāḡaš](../../strongs/h/h5066.md); let them [ʿālâ](../../strongs/h/h5927.md):

<a name="joel_3_10"></a>Joel 3:10

[kāṯaṯ](../../strongs/h/h3807.md) your ['ēṯ](../../strongs/h/h855.md) into [chereb](../../strongs/h/h2719.md), and your [mazmērâ](../../strongs/h/h4211.md) into [rōmaḥ](../../strongs/h/h7420.md): let the [ḥallāš](../../strongs/h/h2523.md) ['āmar](../../strongs/h/h559.md), I am [gibôr](../../strongs/h/h1368.md).

<a name="joel_3_11"></a>Joel 3:11

[ʿûš](../../strongs/h/h5789.md) yourselves, and [bow'](../../strongs/h/h935.md), all ye [gowy](../../strongs/h/h1471.md), and [qāḇaṣ](../../strongs/h/h6908.md) yourselves [cabiyb](../../strongs/h/h5439.md): thither cause thy [gibôr](../../strongs/h/h1368.md) to [nāḥaṯ](../../strongs/h/h5181.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="joel_3_12"></a>Joel 3:12

Let the [gowy](../../strongs/h/h1471.md) be [ʿûr](../../strongs/h/h5782.md), and [ʿālâ](../../strongs/h/h5927.md) to the [ʿēmeq](../../strongs/h/h6010.md) of [Yᵊhôšāp̄Āṭ](../../strongs/h/h3092.md): for there will I [yashab](../../strongs/h/h3427.md) to [shaphat](../../strongs/h/h8199.md) all the [gowy](../../strongs/h/h1471.md) [cabiyb](../../strongs/h/h5439.md).

<a name="joel_3_13"></a>Joel 3:13

[shalach](../../strongs/h/h7971.md) ye in the [magāl](../../strongs/h/h4038.md), for the [qāṣîr](../../strongs/h/h7105.md) is [bāšal](../../strongs/h/h1310.md): [bow'](../../strongs/h/h935.md), get you [yarad](../../strongs/h/h3381.md); for the [gaṯ](../../strongs/h/h1660.md) is [mālā'](../../strongs/h/h4390.md), the [yeqeḇ](../../strongs/h/h3342.md) [šûq](../../strongs/h/h7783.md); for their [ra'](../../strongs/h/h7451.md) is [rab](../../strongs/h/h7227.md).

<a name="joel_3_14"></a>Joel 3:14

[hāmôn](../../strongs/h/h1995.md), [hāmôn](../../strongs/h/h1995.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [ḥārûṣ](../../strongs/h/h2742.md): for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is [qarowb](../../strongs/h/h7138.md) in the [ʿēmeq](../../strongs/h/h6010.md) of [ḥārûṣ](../../strongs/h/h2742.md).

<a name="joel_3_15"></a>Joel 3:15

The [šemeš](../../strongs/h/h8121.md) and the [yareach](../../strongs/h/h3394.md) shall be [qāḏar](../../strongs/h/h6937.md), and the [kowkab](../../strongs/h/h3556.md) shall ['āsap̄](../../strongs/h/h622.md) their [nogahh](../../strongs/h/h5051.md).

<a name="joel_3_16"></a>Joel 3:16

[Yĕhovah](../../strongs/h/h3068.md) also shall [šā'aḡ](../../strongs/h/h7580.md) out of [Tsiyown](../../strongs/h/h6726.md), and [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md) from [Yĕruwshalaim](../../strongs/h/h3389.md); and the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md) shall [rāʿaš](../../strongs/h/h7493.md): but [Yĕhovah](../../strongs/h/h3068.md) will be the [machaceh](../../strongs/h/h4268.md) of his ['am](../../strongs/h/h5971.md), and the [māʿôz](../../strongs/h/h4581.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="joel_3_17"></a>Joel 3:17

So shall ye [yada'](../../strongs/h/h3045.md) that I am [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) [shakan](../../strongs/h/h7931.md) in [Tsiyown](../../strongs/h/h6726.md), my [qodesh](../../strongs/h/h6944.md) [har](../../strongs/h/h2022.md): then shall [Yĕruwshalaim](../../strongs/h/h3389.md) be [qodesh](../../strongs/h/h6944.md), and there shall no [zûr](../../strongs/h/h2114.md) ['abar](../../strongs/h/h5674.md) her any more.

<a name="joel_3_18"></a>Joel 3:18

And it shall come to pass in that [yowm](../../strongs/h/h3117.md), that the [har](../../strongs/h/h2022.md) shall [nāṭap̄](../../strongs/h/h5197.md) new [ʿāsîs](../../strongs/h/h6071.md), and the [giḇʿâ](../../strongs/h/h1389.md) shall [yālaḵ](../../strongs/h/h3212.md) with [chalab](../../strongs/h/h2461.md), and all the ['āp̄îq](../../strongs/h/h650.md) of [Yehuwdah](../../strongs/h/h3063.md) shall [yālaḵ](../../strongs/h/h3212.md) with [mayim](../../strongs/h/h4325.md), and a [maʿyān](../../strongs/h/h4599.md) shall [yāṣā'](../../strongs/h/h3318.md) of the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and shall [šāqâ](../../strongs/h/h8248.md) the [nachal](../../strongs/h/h5158.md) of [Šiṭṭāym](../../strongs/h/h7851.md).

<a name="joel_3_19"></a>Joel 3:19

[Mitsrayim](../../strongs/h/h4714.md) shall be a [šᵊmāmâ](../../strongs/h/h8077.md), and ['Ĕḏōm](../../strongs/h/h123.md) shall be a [šᵊmāmâ](../../strongs/h/h8077.md) [midbar](../../strongs/h/h4057.md), for the [chamac](../../strongs/h/h2555.md) against the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), because they have [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) in their ['erets](../../strongs/h/h776.md).

<a name="joel_3_20"></a>Joel 3:20

But [Yehuwdah](../../strongs/h/h3063.md) shall [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md).

<a name="joel_3_21"></a>Joel 3:21

For I will [naqah](../../strongs/h/h5352.md) their [dam](../../strongs/h/h1818.md) that I have not [naqah](../../strongs/h/h5352.md): for [Yĕhovah](../../strongs/h/h3068.md) [shakan](../../strongs/h/h7931.md) in [Tsiyown](../../strongs/h/h6726.md).

---

[Transliteral Bible](../bible.md)

[Joel](joel.md)

[Joel 2](joel_2.md)