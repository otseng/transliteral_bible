# [Joel 1](https://www.blueletterbible.org/kjv/joel/1)

<a name="joel_1_1"></a>Joel 1:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came to [Yô'Ēl](../../strongs/h/h3100.md) the [ben](../../strongs/h/h1121.md) of [Pᵊṯû'Ēl](../../strongs/h/h6602.md).

<a name="joel_1_2"></a>Joel 1:2

[shama'](../../strongs/h/h8085.md) this, ye [zāqēn](../../strongs/h/h2205.md), and ['azan](../../strongs/h/h238.md), all ye [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md). Hath this been in your [yowm](../../strongs/h/h3117.md), or even in the [yowm](../../strongs/h/h3117.md) of your ['ab](../../strongs/h/h1.md)?

<a name="joel_1_3"></a>Joel 1:3

[sāp̄ar](../../strongs/h/h5608.md) ye your [ben](../../strongs/h/h1121.md) of it, and let your [ben](../../strongs/h/h1121.md) tell their [ben](../../strongs/h/h1121.md), and their [ben](../../strongs/h/h1121.md) ['aḥēr](../../strongs/h/h312.md) [dôr](../../strongs/h/h1755.md).

<a name="joel_1_4"></a>Joel 1:4

That which the [gāzām](../../strongs/h/h1501.md) hath [yeṯer](../../strongs/h/h3499.md) hath the ['arbê](../../strongs/h/h697.md) ['akal](../../strongs/h/h398.md); and that which the ['arbê](../../strongs/h/h697.md) hath [yeṯer](../../strongs/h/h3499.md) hath the [yeleq](../../strongs/h/h3218.md) ['akal](../../strongs/h/h398.md); and that which the [yeleq](../../strongs/h/h3218.md) hath [yeṯer](../../strongs/h/h3499.md) hath the [ḥāsîl](../../strongs/h/h2625.md) ['akal](../../strongs/h/h398.md).

<a name="joel_1_5"></a>Joel 1:5

[quwts](../../strongs/h/h6974.md), ye [šikôr](../../strongs/h/h7910.md), and [bāḵâ](../../strongs/h/h1058.md); and [yālal](../../strongs/h/h3213.md), all ye [šāṯâ](../../strongs/h/h8354.md) of [yayin](../../strongs/h/h3196.md), because of the [ʿāsîs](../../strongs/h/h6071.md); for it is [karath](../../strongs/h/h3772.md) from your [peh](../../strongs/h/h6310.md).

<a name="joel_1_6"></a>Joel 1:6

For a [gowy](../../strongs/h/h1471.md) is [ʿālâ](../../strongs/h/h5927.md) upon my ['erets](../../strongs/h/h776.md), ['atsuwm](../../strongs/h/h6099.md), and without [mispār](../../strongs/h/h4557.md), whose [šēn](../../strongs/h/h8127.md) are the [šēn](../../strongs/h/h8127.md) of an ['ariy](../../strongs/h/h738.md), and he hath the [mᵊṯallᵊʿâ](../../strongs/h/h4973.md) of a [lāḇî'](../../strongs/h/h3833.md).

<a name="joel_1_7"></a>Joel 1:7

He hath [śûm](../../strongs/h/h7760.md) my [gep̄en](../../strongs/h/h1612.md) [šammâ](../../strongs/h/h8047.md), and [qᵊṣāp̄â](../../strongs/h/h7111.md) my [tĕ'en](../../strongs/h/h8384.md): he hath made it [ḥāśap̄](../../strongs/h/h2834.md) [ḥāśap̄](../../strongs/h/h2834.md), and [shalak](../../strongs/h/h7993.md) it; the [śārîḡ](../../strongs/h/h8299.md) thereof are [lāḇan](../../strongs/h/h3835.md).

<a name="joel_1_8"></a>Joel 1:8

['ālâ](../../strongs/h/h421.md) like a [bᵊṯûlâ](../../strongs/h/h1330.md) [ḥāḡar](../../strongs/h/h2296.md) with [śaq](../../strongs/h/h8242.md) for the [baʿal](../../strongs/h/h1167.md) of her [nāʿur](../../strongs/h/h5271.md).

<a name="joel_1_9"></a>Joel 1:9

The [minchah](../../strongs/h/h4503.md) and the [necek](../../strongs/h/h5262.md) is [karath](../../strongs/h/h3772.md) from the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md); the [kōhēn](../../strongs/h/h3548.md), [Yĕhovah](../../strongs/h/h3068.md) [sharath](../../strongs/h/h8334.md), ['āḇal](../../strongs/h/h56.md).

<a name="joel_1_10"></a>Joel 1:10

The [sadeh](../../strongs/h/h7704.md) is [shadad](../../strongs/h/h7703.md), the ['ăḏāmâ](../../strongs/h/h127.md) ['āḇal](../../strongs/h/h56.md); for the [dagan](../../strongs/h/h1715.md) is [shadad](../../strongs/h/h7703.md): the [tiyrowsh](../../strongs/h/h8492.md) is dried [yāḇēš](../../strongs/h/h3001.md), the [yiṣhār](../../strongs/h/h3323.md) ['āmal](../../strongs/h/h535.md).

<a name="joel_1_11"></a>Joel 1:11

Be ye [yāḇēš](../../strongs/h/h3001.md), O ye ['ikār](../../strongs/h/h406.md); [yālal](../../strongs/h/h3213.md), O ye [kōrēm](../../strongs/h/h3755.md), for the [ḥiṭṭâ](../../strongs/h/h2406.md) and for the [śᵊʿōrâ](../../strongs/h/h8184.md); because the [qāṣîr](../../strongs/h/h7105.md) of the [sadeh](../../strongs/h/h7704.md) is ['abad](../../strongs/h/h6.md).

<a name="joel_1_12"></a>Joel 1:12

The [gep̄en](../../strongs/h/h1612.md) is [yāḇēš](../../strongs/h/h3001.md), and the [tĕ'en](../../strongs/h/h8384.md) ['āmal](../../strongs/h/h535.md); the [rimmôn](../../strongs/h/h7416.md), the [tāmār](../../strongs/h/h8558.md) also, and the [tapûaḥ](../../strongs/h/h8598.md), even all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md), are [yāḇēš](../../strongs/h/h3001.md): because [śāśôn](../../strongs/h/h8342.md) is [yāḇēš](../../strongs/h/h3001.md) from the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md).

<a name="joel_1_13"></a>Joel 1:13

[ḥāḡar](../../strongs/h/h2296.md) yourselves, and [sāp̄aḏ](../../strongs/h/h5594.md), ye [kōhēn](../../strongs/h/h3548.md): [yālal](../../strongs/h/h3213.md), ye [sharath](../../strongs/h/h8334.md) of the [mizbeach](../../strongs/h/h4196.md): [bow'](../../strongs/h/h935.md), lie all [lûn](../../strongs/h/h3885.md) in [śaq](../../strongs/h/h8242.md), ye [sharath](../../strongs/h/h8334.md) of my ['Elohiym](../../strongs/h/h430.md): for the [minchah](../../strongs/h/h4503.md) and the [necek](../../strongs/h/h5262.md) is [mānaʿ](../../strongs/h/h4513.md) from the [bayith](../../strongs/h/h1004.md) of your ['Elohiym](../../strongs/h/h430.md).

<a name="joel_1_14"></a>Joel 1:14

[qadash](../../strongs/h/h6942.md) ye a [ṣôm](../../strongs/h/h6685.md), [qara'](../../strongs/h/h7121.md) a [ʿăṣārâ](../../strongs/h/h6116.md), ['āsap̄](../../strongs/h/h622.md) the [zāqēn](../../strongs/h/h2205.md) and all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), and [zāʿaq](../../strongs/h/h2199.md) unto [Yĕhovah](../../strongs/h/h3068.md),

<a name="joel_1_15"></a>Joel 1:15

['ăhâ](../../strongs/h/h162.md) for the [yowm](../../strongs/h/h3117.md)! for the [yowm](../../strongs/h/h3117.md) of [Yĕhovah](../../strongs/h/h3068.md) is at [qarowb](../../strongs/h/h7138.md), and as a [shod](../../strongs/h/h7701.md) from the [Šaday](../../strongs/h/h7706.md) shall it [bow'](../../strongs/h/h935.md).

<a name="joel_1_16"></a>Joel 1:16

Is not the ['ōḵel](../../strongs/h/h400.md) [karath](../../strongs/h/h3772.md) before our ['ayin](../../strongs/h/h5869.md), yea, [simchah](../../strongs/h/h8057.md) and [gîl](../../strongs/h/h1524.md) from the [bayith](../../strongs/h/h1004.md) of our ['Elohiym](../../strongs/h/h430.md)?

<a name="joel_1_17"></a>Joel 1:17

The [p̄rḏṯ](../../strongs/h/h6507.md) is [ʿāḇaš](../../strongs/h/h5685.md) under their [meḡrāp̄â](../../strongs/h/h4053.md), the ['ôṣār](../../strongs/h/h214.md) are [šāmēm](../../strongs/h/h8074.md), the [mammᵊḡurâ](../../strongs/h/h4460.md) are [harac](../../strongs/h/h2040.md); for the [dagan](../../strongs/h/h1715.md) is [yāḇēš](../../strongs/h/h3001.md).

<a name="joel_1_18"></a>Joel 1:18

How do the [bĕhemah](../../strongs/h/h929.md) ['ānaḥ](../../strongs/h/h584.md)! the [ʿēḏer](../../strongs/h/h5739.md) of [bāqār](../../strongs/h/h1241.md) are [bûḵ](../../strongs/h/h943.md), because they have no [mirʿê](../../strongs/h/h4829.md); yea, the [ʿēḏer](../../strongs/h/h5739.md) of [tso'n](../../strongs/h/h6629.md) are made ['asham](../../strongs/h/h816.md).

<a name="joel_1_19"></a>Joel 1:19

[Yĕhovah](../../strongs/h/h3068.md), to thee will I [qara'](../../strongs/h/h7121.md): for the ['esh](../../strongs/h/h784.md) hath ['akal](../../strongs/h/h398.md) the [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md), and the [lehāḇâ](../../strongs/h/h3852.md) hath [lāhaṭ](../../strongs/h/h3857.md) all the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md).

<a name="joel_1_20"></a>Joel 1:20

The [bĕhemah](../../strongs/h/h929.md) of the [sadeh](../../strongs/h/h7704.md) [ʿāraḡ](../../strongs/h/h6165.md) also unto thee: for the ['āp̄îq](../../strongs/h/h650.md) of [mayim](../../strongs/h/h4325.md) are [yāḇēš](../../strongs/h/h3001.md), and the ['esh](../../strongs/h/h784.md) hath ['akal](../../strongs/h/h398.md) the [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md).

---

[Transliteral Bible](../bible.md)

[Joel](joel.md)

[Joel 2](joel_2.md)