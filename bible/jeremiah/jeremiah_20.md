# [Jeremiah 20](https://www.blueletterbible.org/kjv/jeremiah/20)

<a name="jeremiah_20_1"></a>Jeremiah 20:1

Now [Pašḥûr](../../strongs/h/h6583.md) the [ben](../../strongs/h/h1121.md) of ['Immēr](../../strongs/h/h564.md) the [kōhēn](../../strongs/h/h3548.md), who was also [pāqîḏ](../../strongs/h/h6496.md) [nāḡîḏ](../../strongs/h/h5057.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), [shama'](../../strongs/h/h8085.md) that [Yirmᵊyâ](../../strongs/h/h3414.md) [nāḇā'](../../strongs/h/h5012.md) these [dabar](../../strongs/h/h1697.md).

<a name="jeremiah_20_2"></a>Jeremiah 20:2

Then [Pašḥûr](../../strongs/h/h6583.md) [nakah](../../strongs/h/h5221.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), and [nathan](../../strongs/h/h5414.md) him in the [mahpeḵeṯ](../../strongs/h/h4115.md) that were in the ['elyown](../../strongs/h/h5945.md) [sha'ar](../../strongs/h/h8179.md) of [Binyāmîn](../../strongs/h/h1144.md), which was by the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_20_3"></a>Jeremiah 20:3

And it came to pass on the [māḥŏrāṯ](../../strongs/h/h4283.md), that [Pašḥûr](../../strongs/h/h6583.md) [yāṣā'](../../strongs/h/h3318.md) [Yirmᵊyâ](../../strongs/h/h3414.md) out of the [mahpeḵeṯ](../../strongs/h/h4115.md). Then ['āmar](../../strongs/h/h559.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) hath not [qara'](../../strongs/h/h7121.md) thy [shem](../../strongs/h/h8034.md) [Pašḥûr](../../strongs/h/h6583.md), but [Māḡôr Missāḇîḇ](../../strongs/h/h4036.md).

<a name="jeremiah_20_4"></a>Jeremiah 20:4

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [nathan](../../strongs/h/h5414.md) thee a [māḡôr](../../strongs/h/h4032.md) to thyself, and to all thy ['ahab](../../strongs/h/h157.md): and they shall [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md) of their ['oyeb](../../strongs/h/h341.md), and thine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) it: and I will [nathan](../../strongs/h/h5414.md) all [Yehuwdah](../../strongs/h/h3063.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall carry them [gālâ](../../strongs/h/h1540.md) into [Bāḇel](../../strongs/h/h894.md), and shall [nakah](../../strongs/h/h5221.md) them with the [chereb](../../strongs/h/h2719.md).

<a name="jeremiah_20_5"></a>Jeremiah 20:5

Moreover I will [nathan](../../strongs/h/h5414.md) all the [ḥōsen](../../strongs/h/h2633.md) of this [ʿîr](../../strongs/h/h5892.md), and all the [yᵊḡîaʿ](../../strongs/h/h3018.md) thereof, and all the [yᵊqār](../../strongs/h/h3366.md) thereof, and all the ['ôṣār](../../strongs/h/h214.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) will I [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md), which shall [bāzaz](../../strongs/h/h962.md) them, and [laqach](../../strongs/h/h3947.md) them, and [bow'](../../strongs/h/h935.md) them to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_20_6"></a>Jeremiah 20:6

And thou, [Pašḥûr](../../strongs/h/h6583.md), and all that [yashab](../../strongs/h/h3427.md) in thine [bayith](../../strongs/h/h1004.md) shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md): and thou shalt [bow'](../../strongs/h/h935.md) to [Bāḇel](../../strongs/h/h894.md), and there thou shalt [muwth](../../strongs/h/h4191.md), and shalt be [qāḇar](../../strongs/h/h6912.md) there, thou, and all thy ['ahab](../../strongs/h/h157.md), to whom thou hast [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_20_7"></a>Jeremiah 20:7

[Yĕhovah](../../strongs/h/h3068.md), thou hast [pāṯâ](../../strongs/h/h6601.md) me, and I was [pāṯâ](../../strongs/h/h6601.md): thou art [ḥāzaq](../../strongs/h/h2388.md) than I, and hast [yakol](../../strongs/h/h3201.md): I am in [śᵊḥôq](../../strongs/h/h7814.md) [yowm](../../strongs/h/h3117.md), every one [lāʿaḡ](../../strongs/h/h3932.md) me.

<a name="jeremiah_20_8"></a>Jeremiah 20:8

For [day](../../strongs/h/h1767.md) I [dabar](../../strongs/h/h1696.md), I [zāʿaq](../../strongs/h/h2199.md), I [qara'](../../strongs/h/h7121.md) [chamac](../../strongs/h/h2555.md) and [shod](../../strongs/h/h7701.md); because the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) was made a [cherpah](../../strongs/h/h2781.md) unto me, and a [qeles](../../strongs/h/h7047.md), [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_20_9"></a>Jeremiah 20:9

Then I ['āmar](../../strongs/h/h559.md), I will not make [zakar](../../strongs/h/h2142.md) of him, nor [dabar](../../strongs/h/h1696.md) any more in his [shem](../../strongs/h/h8034.md). But his word was in mine [leb](../../strongs/h/h3820.md) as a [bāʿar](../../strongs/h/h1197.md) ['esh](../../strongs/h/h784.md) [ʿāṣar](../../strongs/h/h6113.md) in my ['etsem](../../strongs/h/h6106.md), and I was [lā'â](../../strongs/h/h3811.md) with [kûl](../../strongs/h/h3557.md), and I [yakol](../../strongs/h/h3201.md) not stay.

<a name="jeremiah_20_10"></a>Jeremiah 20:10

For I [shama'](../../strongs/h/h8085.md) the [dibâ](../../strongs/h/h1681.md) of [rab](../../strongs/h/h7227.md), [māḡôr](../../strongs/h/h4032.md) on every [cabiyb](../../strongs/h/h5439.md). [nāḡaḏ](../../strongs/h/h5046.md), say they, and we will [nāḡaḏ](../../strongs/h/h5046.md) it. All my ['enowsh](../../strongs/h/h582.md) [shalowm](../../strongs/h/h7965.md) [shamar](../../strongs/h/h8104.md) for my [tsela'](../../strongs/h/h6763.md) [ṣelaʿ](../../strongs/h/h6761.md), saying, Peradventure he will be [pāṯâ](../../strongs/h/h6601.md), and we shall [yakol](../../strongs/h/h3201.md) against him, and we shall [laqach](../../strongs/h/h3947.md) our [nᵊqāmâ](../../strongs/h/h5360.md) on him.

<a name="jeremiah_20_11"></a>Jeremiah 20:11

But [Yĕhovah](../../strongs/h/h3068.md) is with me as a [gibôr](../../strongs/h/h1368.md) [ʿārîṣ](../../strongs/h/h6184.md): therefore my [radaph](../../strongs/h/h7291.md) shall [kashal](../../strongs/h/h3782.md), and they shall not [yakol](../../strongs/h/h3201.md): they shall be [me'od](../../strongs/h/h3966.md) [buwsh](../../strongs/h/h954.md); for they shall not [sakal](../../strongs/h/h7919.md): their ['owlam](../../strongs/h/h5769.md) [kĕlimmah](../../strongs/h/h3639.md) shall never be [shakach](../../strongs/h/h7911.md).

<a name="jeremiah_20_12"></a>Jeremiah 20:12

But, [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that [bachan](../../strongs/h/h974.md) the [tsaddiyq](../../strongs/h/h6662.md), and [ra'ah](../../strongs/h/h7200.md) the [kilyah](../../strongs/h/h3629.md) and the [leb](../../strongs/h/h3820.md), let me [ra'ah](../../strongs/h/h7200.md) thy [nᵊqāmâ](../../strongs/h/h5360.md) on them: for unto thee have I [gālâ](../../strongs/h/h1540.md) my [rîḇ](../../strongs/h/h7379.md).

<a name="jeremiah_20_13"></a>Jeremiah 20:13

[shiyr](../../strongs/h/h7891.md) unto [Yĕhovah](../../strongs/h/h3068.md), [halal](../../strongs/h/h1984.md) ye [Yĕhovah](../../strongs/h/h3068.md): for he hath [natsal](../../strongs/h/h5337.md) the [nephesh](../../strongs/h/h5315.md) of the ['ebyown](../../strongs/h/h34.md) from the [yad](../../strongs/h/h3027.md) of [ra'a'](../../strongs/h/h7489.md).

<a name="jeremiah_20_14"></a>Jeremiah 20:14

['arar](../../strongs/h/h779.md) be the [yowm](../../strongs/h/h3117.md) wherein I was [yalad](../../strongs/h/h3205.md): let not the [yowm](../../strongs/h/h3117.md) wherein my ['em](../../strongs/h/h517.md) [yalad](../../strongs/h/h3205.md) me be [barak](../../strongs/h/h1288.md).

<a name="jeremiah_20_15"></a>Jeremiah 20:15

['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) who brought [bāśar](../../strongs/h/h1319.md) to my ['ab](../../strongs/h/h1.md), ['āmar](../../strongs/h/h559.md), A [zāḵār](../../strongs/h/h2145.md) [ben](../../strongs/h/h1121.md) is [yalad](../../strongs/h/h3205.md) unto thee; making him [samach](../../strongs/h/h8055.md) [samach](../../strongs/h/h8055.md).

<a name="jeremiah_20_16"></a>Jeremiah 20:16

And let that ['iysh](../../strongs/h/h376.md) be as the [ʿîr](../../strongs/h/h5892.md) which [Yĕhovah](../../strongs/h/h3068.md) [hāp̄aḵ](../../strongs/h/h2015.md), and [nacham](../../strongs/h/h5162.md) not: and let him [shama'](../../strongs/h/h8085.md) the [zaʿaq](../../strongs/h/h2201.md) in the [boqer](../../strongs/h/h1242.md), and the [tᵊrûʿâ](../../strongs/h/h8643.md) at [ʿēṯ](../../strongs/h/h6256.md) [ṣōhar](../../strongs/h/h6672.md);

<a name="jeremiah_20_17"></a>Jeremiah 20:17

Because he [muwth](../../strongs/h/h4191.md) me not from the [reḥem](../../strongs/h/h7358.md); or that my ['em](../../strongs/h/h517.md) might have been my [qeber](../../strongs/h/h6913.md), and her [reḥem](../../strongs/h/h7358.md) to be ['owlam](../../strongs/h/h5769.md) [hārê](../../strongs/h/h2030.md) with me.

<a name="jeremiah_20_18"></a>Jeremiah 20:18

Wherefore came I [yāṣā'](../../strongs/h/h3318.md) out of the [reḥem](../../strongs/h/h7358.md) to [ra'ah](../../strongs/h/h7200.md) ['amal](../../strongs/h/h5999.md) and [yagown](../../strongs/h/h3015.md), that my [yowm](../../strongs/h/h3117.md) should be [kalah](../../strongs/h/h3615.md) with [bšeṯ](../../strongs/h/h1322.md)?

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 19](jeremiah_19.md) - [Jeremiah 21](jeremiah_21.md)