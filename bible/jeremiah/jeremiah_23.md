# [Jeremiah 23](https://www.blueletterbible.org/kjv/jeremiah/23)

<a name="jeremiah_23_1"></a>Jeremiah 23:1

[hôy](../../strongs/h/h1945.md) be unto the [ra'ah](../../strongs/h/h7462.md) that ['abad](../../strongs/h/h6.md) and [puwts](../../strongs/h/h6327.md) the [tso'n](../../strongs/h/h6629.md) of my [marʿîṯ](../../strongs/h/h4830.md)! [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_2"></a>Jeremiah 23:2

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) against the [ra'ah](../../strongs/h/h7462.md) that [ra'ah](../../strongs/h/h7462.md) my ['am](../../strongs/h/h5971.md); Ye have [puwts](../../strongs/h/h6327.md) my [tso'n](../../strongs/h/h6629.md), and driven them [nāḏaḥ](../../strongs/h/h5080.md), and have not [paqad](../../strongs/h/h6485.md) them: behold, I will [paqad](../../strongs/h/h6485.md) upon you the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_3"></a>Jeremiah 23:3

And I will [qāḇaṣ](../../strongs/h/h6908.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of my [tso'n](../../strongs/h/h6629.md) out of all ['erets](../../strongs/h/h776.md) whither I have [nāḏaḥ](../../strongs/h/h5080.md) them, and will bring them [shuwb](../../strongs/h/h7725.md) to their [nāvê](../../strongs/h/h5116.md); and they shall be [parah](../../strongs/h/h6509.md) and [rabah](../../strongs/h/h7235.md).

<a name="jeremiah_23_4"></a>Jeremiah 23:4

And I will [quwm](../../strongs/h/h6965.md) [ra'ah](../../strongs/h/h7462.md) over them which shall [ra'ah](../../strongs/h/h7462.md) them: and they shall [yare'](../../strongs/h/h3372.md) no more, nor be [ḥāṯaṯ](../../strongs/h/h2865.md), neither shall they be [paqad](../../strongs/h/h6485.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_5"></a>Jeremiah 23:5

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [quwm](../../strongs/h/h6965.md) unto [Dāviḏ](../../strongs/h/h1732.md) a [tsaddiyq](../../strongs/h/h6662.md) [ṣemaḥ](../../strongs/h/h6780.md), and a [melek](../../strongs/h/h4428.md) shall [mālaḵ](../../strongs/h/h4427.md) and [sakal](../../strongs/h/h7919.md), and shall ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md) in the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_23_6"></a>Jeremiah 23:6

In his [yowm](../../strongs/h/h3117.md) [Yehuwdah](../../strongs/h/h3063.md) shall be [yasha'](../../strongs/h/h3467.md), and [Yisra'el](../../strongs/h/h3478.md) shall [shakan](../../strongs/h/h7931.md) [betach](../../strongs/h/h983.md): and this is his [shem](../../strongs/h/h8034.md) whereby he shall be [qara'](../../strongs/h/h7121.md), [Yᵊhōvâ Ṣiḏqēnû](../../strongs/h/h3072.md).

<a name="jeremiah_23_7"></a>Jeremiah 23:7

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that they shall no more ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), which brought [ʿālâ](../../strongs/h/h5927.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md);

<a name="jeremiah_23_8"></a>Jeremiah 23:8

But, [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), which brought [ʿālâ](../../strongs/h/h5927.md) and which [bow'](../../strongs/h/h935.md) the [zera'](../../strongs/h/h2233.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md), and from all ['erets](../../strongs/h/h776.md) whither I had [nāḏaḥ](../../strongs/h/h5080.md) them; and they shall [yashab](../../strongs/h/h3427.md) in their own ['ăḏāmâ](../../strongs/h/h127.md).

<a name="jeremiah_23_9"></a>Jeremiah 23:9

Mine [leb](../../strongs/h/h3820.md) [qereḇ](../../strongs/h/h7130.md) me is [shabar](../../strongs/h/h7665.md) because of the [nāḇî'](../../strongs/h/h5030.md); all my ['etsem](../../strongs/h/h6106.md) [rachaph](../../strongs/h/h7363.md); I am like a [šikôr](../../strongs/h/h7910.md) ['iysh](../../strongs/h/h376.md), and like a [geḇer](../../strongs/h/h1397.md) whom [yayin](../../strongs/h/h3196.md) hath ['abar](../../strongs/h/h5674.md), [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), and because of the [dabar](../../strongs/h/h1697.md) of his [qodesh](../../strongs/h/h6944.md).

<a name="jeremiah_23_10"></a>Jeremiah 23:10

For the ['erets](../../strongs/h/h776.md) is [mālā'](../../strongs/h/h4390.md) of [na'aph](../../strongs/h/h5003.md); for [paniym](../../strongs/h/h6440.md) of ['alah](../../strongs/h/h423.md) the ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md); the pleasant [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md) are dried [yāḇēš](../../strongs/h/h3001.md), and their [mᵊrûṣâ](../../strongs/h/h4794.md) is [ra'](../../strongs/h/h7451.md), and their [gᵊḇûrâ](../../strongs/h/h1369.md) is not right.

<a name="jeremiah_23_11"></a>Jeremiah 23:11

For both [nāḇî'](../../strongs/h/h5030.md) and [kōhēn](../../strongs/h/h3548.md) are [ḥānēp̄](../../strongs/h/h2610.md); yea, in my [bayith](../../strongs/h/h1004.md) have I [māṣā'](../../strongs/h/h4672.md) their [ra'](../../strongs/h/h7451.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_12"></a>Jeremiah 23:12

Wherefore their [derek](../../strongs/h/h1870.md) shall be unto them as [ḥălaqlaqqôṯ](../../strongs/h/h2519.md) ways in the ['ăp̄ēlâ](../../strongs/h/h653.md): they shall be [dāḥâ](../../strongs/h/h1760.md), and [naphal](../../strongs/h/h5307.md) therein: for I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon them, even the [šānâ](../../strongs/h/h8141.md) of their [pᵊqudâ](../../strongs/h/h6486.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_13"></a>Jeremiah 23:13

And I have [ra'ah](../../strongs/h/h7200.md) [tip̄lâ](../../strongs/h/h8604.md) in the [nāḇî'](../../strongs/h/h5030.md) of [Šōmrôn](../../strongs/h/h8111.md); they [nāḇā'](../../strongs/h/h5012.md) in [BaʿAl](../../strongs/h/h1168.md), and caused my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) to [tāʿâ](../../strongs/h/h8582.md).

<a name="jeremiah_23_14"></a>Jeremiah 23:14

I have [ra'ah](../../strongs/h/h7200.md) also in the [nāḇî'](../../strongs/h/h5030.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) an horrible [šaʿărûr](../../strongs/h/h8186.md): they commit [na'aph](../../strongs/h/h5003.md), and [halak](../../strongs/h/h1980.md) in [sheqer](../../strongs/h/h8267.md): they [ḥāzaq](../../strongs/h/h2388.md) also the [yad](../../strongs/h/h3027.md) of [ra'a'](../../strongs/h/h7489.md), that ['iysh](../../strongs/h/h376.md) doth [shuwb](../../strongs/h/h7725.md) from his [ra'](../../strongs/h/h7451.md): they are all of them unto me as [Sᵊḏōm](../../strongs/h/h5467.md), and the [yashab](../../strongs/h/h3427.md) thereof as [ʿĂmōrâ](../../strongs/h/h6017.md).

<a name="jeremiah_23_15"></a>Jeremiah 23:15

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) concerning the [nāḇî'](../../strongs/h/h5030.md); Behold, I will ['akal](../../strongs/h/h398.md) them with [laʿănâ](../../strongs/h/h3939.md), and make them [šāqâ](../../strongs/h/h8248.md) the [mayim](../../strongs/h/h4325.md) of [rō'š](../../strongs/h/h7219.md): for from the [nāḇî'](../../strongs/h/h5030.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) is [ḥănupâ](../../strongs/h/h2613.md) gone [yāṣā'](../../strongs/h/h3318.md) into all the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_23_16"></a>Jeremiah 23:16

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), [shama'](../../strongs/h/h8085.md) not unto the [dabar](../../strongs/h/h1697.md) of the [nāḇî'](../../strongs/h/h5030.md) that [nāḇā'](../../strongs/h/h5012.md) unto you: they make you [hāḇal](../../strongs/h/h1891.md): they [dabar](../../strongs/h/h1696.md) a [ḥāzôn](../../strongs/h/h2377.md) of their own [leb](../../strongs/h/h3820.md), and not out of the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_17"></a>Jeremiah 23:17

They ['āmar](../../strongs/h/h559.md) ['āmar](../../strongs/h/h559.md) unto them that [na'ats](../../strongs/h/h5006.md) me, [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md), Ye shall have [shalowm](../../strongs/h/h7965.md); and they ['āmar](../../strongs/h/h559.md) unto every one that [halak](../../strongs/h/h1980.md) after the [šᵊrîrûṯ](../../strongs/h/h8307.md) of his own [leb](../../strongs/h/h3820.md), No [ra'](../../strongs/h/h7451.md) shall [bow'](../../strongs/h/h935.md) upon you.

<a name="jeremiah_23_18"></a>Jeremiah 23:18

For who hath ['amad](../../strongs/h/h5975.md) in the [sôḏ](../../strongs/h/h5475.md) of [Yĕhovah](../../strongs/h/h3068.md), and hath [ra'ah](../../strongs/h/h7200.md) and [shama'](../../strongs/h/h8085.md) his [dabar](../../strongs/h/h1697.md)? who hath [qashab](../../strongs/h/h7181.md) his [dabar](../../strongs/h/h1697.md), and [shama'](../../strongs/h/h8085.md) it?

<a name="jeremiah_23_19"></a>Jeremiah 23:19

Behold, a [saʿar](../../strongs/h/h5591.md) of [Yĕhovah](../../strongs/h/h3068.md) is gone [yāṣā'](../../strongs/h/h3318.md) in [chemah](../../strongs/h/h2534.md), even a [chuwl](../../strongs/h/h2342.md) [saʿar](../../strongs/h/h5591.md): it shall fall [chuwl](../../strongs/h/h2342.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="jeremiah_23_20"></a>Jeremiah 23:20

The ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) shall not [shuwb](../../strongs/h/h7725.md), until he have ['asah](../../strongs/h/h6213.md), and till he have [quwm](../../strongs/h/h6965.md) the [mezimmah](../../strongs/h/h4209.md) of his [leb](../../strongs/h/h3820.md): in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md) ye shall [bîn](../../strongs/h/h995.md) it [bînâ](../../strongs/h/h998.md).

<a name="jeremiah_23_21"></a>Jeremiah 23:21

I have not [shalach](../../strongs/h/h7971.md) these [nāḇî'](../../strongs/h/h5030.md), yet they [rûṣ](../../strongs/h/h7323.md): I have not [dabar](../../strongs/h/h1696.md) to them, yet they [nāḇā'](../../strongs/h/h5012.md).

<a name="jeremiah_23_22"></a>Jeremiah 23:22

But if they had ['amad](../../strongs/h/h5975.md) in my [sôḏ](../../strongs/h/h5475.md), and had caused my ['am](../../strongs/h/h5971.md) to [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md), then they should have [shuwb](../../strongs/h/h7725.md) them from their [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and from the [rōaʿ](../../strongs/h/h7455.md) of their [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_23_23"></a>Jeremiah 23:23

Am I an ['Elohiym](../../strongs/h/h430.md) at [qarowb](../../strongs/h/h7138.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and not an ['Elohiym](../../strongs/h/h430.md) afar [rachowq](../../strongs/h/h7350.md)?

<a name="jeremiah_23_24"></a>Jeremiah 23:24

Can ['iysh](../../strongs/h/h376.md) [cathar](../../strongs/h/h5641.md) himself in [mictar](../../strongs/h/h4565.md) that I shall not [ra'ah](../../strongs/h/h7200.md) him? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Do not I [mālē'](../../strongs/h/h4392.md) [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_25"></a>Jeremiah 23:25

I have [shama'](../../strongs/h/h8085.md) what the [nāḇî'](../../strongs/h/h5030.md) ['āmar](../../strongs/h/h559.md), that [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md) in my [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md), I have [ḥālam](../../strongs/h/h2492.md), I have [ḥālam](../../strongs/h/h2492.md).

<a name="jeremiah_23_26"></a>Jeremiah 23:26

How long shall this [yēš](../../strongs/h/h3426.md) in the [leb](../../strongs/h/h3820.md) of the [nāḇî'](../../strongs/h/h5030.md) that [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md)? yea, they are [nāḇî'](../../strongs/h/h5030.md) of the [tārmâ](../../strongs/h/h8649.md) of their own [leb](../../strongs/h/h3820.md);

<a name="jeremiah_23_27"></a>Jeremiah 23:27

Which [chashab](../../strongs/h/h2803.md) to cause my ['am](../../strongs/h/h5971.md) to [shakach](../../strongs/h/h7911.md) my [shem](../../strongs/h/h8034.md) by their [ḥălôm](../../strongs/h/h2472.md) which they [sāp̄ar](../../strongs/h/h5608.md) every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md), as their ['ab](../../strongs/h/h1.md) have [shakach](../../strongs/h/h7911.md) my [shem](../../strongs/h/h8034.md) for [BaʿAl](../../strongs/h/h1168.md).

<a name="jeremiah_23_28"></a>Jeremiah 23:28

The [nāḇî'](../../strongs/h/h5030.md) that hath a [ḥălôm](../../strongs/h/h2472.md), let him [sāp̄ar](../../strongs/h/h5608.md) a [ḥălôm](../../strongs/h/h2472.md); and he that hath my [dabar](../../strongs/h/h1697.md), let him [dabar](../../strongs/h/h1696.md) my [dabar](../../strongs/h/h1697.md) ['emeth](../../strongs/h/h571.md). What is the [teḇen](../../strongs/h/h8401.md) to the [bar](../../strongs/h/h1250.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_29"></a>Jeremiah 23:29

Is not my [dabar](../../strongs/h/h1697.md) [kô](../../strongs/h/h3541.md) as an ['esh](../../strongs/h/h784.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and like a [paṭṭîš](../../strongs/h/h6360.md) that [puwts](../../strongs/h/h6327.md) the [cela'](../../strongs/h/h5553.md) in [puwts](../../strongs/h/h6327.md)?

<a name="jeremiah_23_30"></a>Jeremiah 23:30

Therefore, behold, I am against the [nāḇî'](../../strongs/h/h5030.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that [ganab](../../strongs/h/h1589.md) my [dabar](../../strongs/h/h1697.md) every ['iysh](../../strongs/h/h376.md) from his [rea'](../../strongs/h/h7453.md).

<a name="jeremiah_23_31"></a>Jeremiah 23:31

Behold, I am against the [nāḇî'](../../strongs/h/h5030.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that [laqach](../../strongs/h/h3947.md) their [lashown](../../strongs/h/h3956.md), and [nā'am](../../strongs/h/h5001.md), He [nᵊ'um](../../strongs/h/h5002.md).

<a name="jeremiah_23_32"></a>Jeremiah 23:32

Behold, I am against them that [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md) [ḥălôm](../../strongs/h/h2472.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and do [sāp̄ar](../../strongs/h/h5608.md) them, and cause my ['am](../../strongs/h/h5971.md) to [tāʿâ](../../strongs/h/h8582.md) by their [sheqer](../../strongs/h/h8267.md), and by their [paḥăzûṯ](../../strongs/h/h6350.md); yet I [shalach](../../strongs/h/h7971.md) them not, nor [tsavah](../../strongs/h/h6680.md) them: therefore they shall not [yāʿal](../../strongs/h/h3276.md) this ['am](../../strongs/h/h5971.md) at [yāʿal](../../strongs/h/h3276.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_33"></a>Jeremiah 23:33

And when this ['am](../../strongs/h/h5971.md), or the [nāḇî'](../../strongs/h/h5030.md), or a [kōhēn](../../strongs/h/h3548.md), shall [sha'al](../../strongs/h/h7592.md) thee, ['āmar](../../strongs/h/h559.md), What is the [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md)? thou shalt then ['āmar](../../strongs/h/h559.md) unto them, What [maśśā'](../../strongs/h/h4853.md)? I will even [nāṭaš](../../strongs/h/h5203.md) you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_23_34"></a>Jeremiah 23:34

And as for the [nāḇî'](../../strongs/h/h5030.md), and the [kōhēn](../../strongs/h/h3548.md), and the ['am](../../strongs/h/h5971.md), that shall ['āmar](../../strongs/h/h559.md), The [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md), I will even [paqad](../../strongs/h/h6485.md) that ['iysh](../../strongs/h/h376.md) and his [bayith](../../strongs/h/h1004.md).

<a name="jeremiah_23_35"></a>Jeremiah 23:35

Thus shall ye ['āmar](../../strongs/h/h559.md) every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md), and every ['iysh](../../strongs/h/h376.md) to his ['ach](../../strongs/h/h251.md), What hath [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md)? and, What hath [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md)?

<a name="jeremiah_23_36"></a>Jeremiah 23:36

And the [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md) shall ye [zakar](../../strongs/h/h2142.md) no more: for every ['iysh](../../strongs/h/h376.md) [dabar](../../strongs/h/h1697.md) shall be his [maśśā'](../../strongs/h/h4853.md); for ye have [hāp̄aḵ](../../strongs/h/h2015.md) the [dabar](../../strongs/h/h1697.md) of the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md), of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_23_37"></a>Jeremiah 23:37

Thus shalt thou ['āmar](../../strongs/h/h559.md) to the [nāḇî'](../../strongs/h/h5030.md), What hath [Yĕhovah](../../strongs/h/h3068.md) ['anah](../../strongs/h/h6030.md) thee? and, What hath [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md)?

<a name="jeremiah_23_38"></a>Jeremiah 23:38

['im](../../strongs/h/h518.md) since ye ['āmar](../../strongs/h/h559.md), The [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md); therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Because ye ['āmar](../../strongs/h/h559.md) this [dabar](../../strongs/h/h1697.md), The [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md), and I have [shalach](../../strongs/h/h7971.md) unto you, ['āmar](../../strongs/h/h559.md), Ye shall not ['āmar](../../strongs/h/h559.md), The [maśśā'](../../strongs/h/h4853.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="jeremiah_23_39"></a>Jeremiah 23:39

Therefore, behold, I, even I, will [nasha'](../../strongs/h/h5377.md) [nāšâ](../../strongs/h/h5382.md) you, and I will [nāṭaš](../../strongs/h/h5203.md) you, and the [ʿîr](../../strongs/h/h5892.md) that I [nathan](../../strongs/h/h5414.md) you and your ['ab](../../strongs/h/h1.md), and cast you out of my [paniym](../../strongs/h/h6440.md):

<a name="jeremiah_23_40"></a>Jeremiah 23:40

And I will [nathan](../../strongs/h/h5414.md) an ['owlam](../../strongs/h/h5769.md) [cherpah](../../strongs/h/h2781.md) upon you, and a ['owlam](../../strongs/h/h5769.md) [kᵊlimmûṯ](../../strongs/h/h3640.md), which shall not be [shakach](../../strongs/h/h7911.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 22](jeremiah_22.md) - [Jeremiah 24](jeremiah_24.md)