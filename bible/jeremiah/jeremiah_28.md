# [Jeremiah 28](https://www.blueletterbible.org/kjv/jeremiah/28)

<a name="jeremiah_28_1"></a>Jeremiah 28:1

And it came to pass the same [šānâ](../../strongs/h/h8141.md), in the [re'shiyth](../../strongs/h/h7225.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md), and in the [ḥămîšî](../../strongs/h/h2549.md) [ḥōḏeš](../../strongs/h/h2320.md), that [Ḥănanyâ](../../strongs/h/h2608.md) the [ben](../../strongs/h/h1121.md) of [ʿAzzûr](../../strongs/h/h5809.md) the [nāḇî'](../../strongs/h/h5030.md), which was of [Giḇʿôn](../../strongs/h/h1391.md), ['āmar](../../strongs/h/h559.md) unto me in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), in the ['ayin](../../strongs/h/h5869.md) of the [kōhēn](../../strongs/h/h3548.md) and of all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_28_2"></a>Jeremiah 28:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), I have [shabar](../../strongs/h/h7665.md) the [ʿōl](../../strongs/h/h5923.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_28_3"></a>Jeremiah 28:3

Within two [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md) will I [shuwb](../../strongs/h/h7725.md) into this [maqowm](../../strongs/h/h4725.md) all the [kĕliy](../../strongs/h/h3627.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), that [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) took [laqach](../../strongs/h/h3947.md) from this [maqowm](../../strongs/h/h4725.md), and [bow'](../../strongs/h/h935.md) them to [Bāḇel](../../strongs/h/h894.md):

<a name="jeremiah_28_4"></a>Jeremiah 28:4

And I will [shuwb](../../strongs/h/h7725.md) to this [maqowm](../../strongs/h/h4725.md) [Yᵊḵānyâ](../../strongs/h/h3204.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), with all the [gālûṯ](../../strongs/h/h1546.md) of [Yehuwdah](../../strongs/h/h3063.md), that [bow'](../../strongs/h/h935.md) into [Bāḇel](../../strongs/h/h894.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): for I will [shabar](../../strongs/h/h7665.md) the [ʿōl](../../strongs/h/h5923.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_28_5"></a>Jeremiah 28:5

Then the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto the [nāḇî'](../../strongs/h/h5030.md) [Ḥănanyâ](../../strongs/h/h2608.md) in the ['ayin](../../strongs/h/h5869.md) of the [kōhēn](../../strongs/h/h3548.md), and in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md) that ['amad](../../strongs/h/h5975.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="jeremiah_28_6"></a>Jeremiah 28:6

Even the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md), ['amen](../../strongs/h/h543.md): [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) so: [Yĕhovah](../../strongs/h/h3068.md) [quwm](../../strongs/h/h6965.md) thy [dabar](../../strongs/h/h1697.md) which thou hast [nāḇā'](../../strongs/h/h5012.md), to bring [shuwb](../../strongs/h/h7725.md) the [kĕliy](../../strongs/h/h3627.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), and all that is carried away [gôlâ](../../strongs/h/h1473.md), from [Bāḇel](../../strongs/h/h894.md) into this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_28_7"></a>Jeremiah 28:7

Nevertheless [shama'](../../strongs/h/h8085.md) thou now this [dabar](../../strongs/h/h1697.md) that I [dabar](../../strongs/h/h1696.md) in thine ['ozen](../../strongs/h/h241.md), and in the ['ozen](../../strongs/h/h241.md) of all the ['am](../../strongs/h/h5971.md);

<a name="jeremiah_28_8"></a>Jeremiah 28:8

The [nāḇî'](../../strongs/h/h5030.md) that have been [paniym](../../strongs/h/h6440.md) me and [paniym](../../strongs/h/h6440.md) thee of ['owlam](../../strongs/h/h5769.md) [nāḇā'](../../strongs/h/h5012.md) both against [rab](../../strongs/h/h7227.md) ['erets](../../strongs/h/h776.md), and against [gadowl](../../strongs/h/h1419.md) [mamlāḵâ](../../strongs/h/h4467.md), of [milḥāmâ](../../strongs/h/h4421.md), and of [ra'](../../strongs/h/h7451.md), and of [deḇer](../../strongs/h/h1698.md).

<a name="jeremiah_28_9"></a>Jeremiah 28:9

The [nāḇî'](../../strongs/h/h5030.md) which [nāḇā'](../../strongs/h/h5012.md) of [shalowm](../../strongs/h/h7965.md), when the [dabar](../../strongs/h/h1697.md) of the [nāḇî'](../../strongs/h/h5030.md) shall [bow'](../../strongs/h/h935.md), ['āz](../../strongs/h/h227.md) shall the [nāḇî'](../../strongs/h/h5030.md) be [yada'](../../strongs/h/h3045.md), that [Yĕhovah](../../strongs/h/h3068.md) hath ['emeth](../../strongs/h/h571.md) [shalach](../../strongs/h/h7971.md) him.

<a name="jeremiah_28_10"></a>Jeremiah 28:10

Then [Ḥănanyâ](../../strongs/h/h2608.md) the [nāḇî'](../../strongs/h/h5030.md) [laqach](../../strongs/h/h3947.md) the [môṭâ](../../strongs/h/h4133.md) from off the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md) [ṣaûā'r](../../strongs/h/h6677.md), and [shabar](../../strongs/h/h7665.md) it.

<a name="jeremiah_28_11"></a>Jeremiah 28:11

And [Ḥănanyâ](../../strongs/h/h2608.md) ['āmar](../../strongs/h/h559.md) in the ['ayin](../../strongs/h/h5869.md) of all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Even so will I [shabar](../../strongs/h/h7665.md) the [ʿōl](../../strongs/h/h5923.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) from the [ṣaûā'r](../../strongs/h/h6677.md) of all [gowy](../../strongs/h/h1471.md) within the space of two [yowm](../../strongs/h/h3117.md) [šānâ](../../strongs/h/h8141.md). And the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md) [yālaḵ](../../strongs/h/h3212.md) his [derek](../../strongs/h/h1870.md).

<a name="jeremiah_28_12"></a>Jeremiah 28:12

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md) the prophet, ['aḥar](../../strongs/h/h310.md) that [Ḥănanyâ](../../strongs/h/h2608.md) the [nāḇî'](../../strongs/h/h5030.md) had [shabar](../../strongs/h/h7665.md) the [môṭâ](../../strongs/h/h4133.md) from off the [ṣaûā'r](../../strongs/h/h6677.md) of the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_28_13"></a>Jeremiah 28:13

[halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md) [Ḥănanyâ](../../strongs/h/h2608.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Thou hast [shabar](../../strongs/h/h7665.md) the [môṭâ](../../strongs/h/h4133.md) of ['ets](../../strongs/h/h6086.md); but thou shalt ['asah](../../strongs/h/h6213.md) for them [môṭâ](../../strongs/h/h4133.md) of [barzel](../../strongs/h/h1270.md).

<a name="jeremiah_28_14"></a>Jeremiah 28:14

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); I have [nathan](../../strongs/h/h5414.md) a [ʿōl](../../strongs/h/h5923.md) of [barzel](../../strongs/h/h1270.md) upon the [ṣaûā'r](../../strongs/h/h6677.md) of all these [gowy](../../strongs/h/h1471.md), that they may ['abad](../../strongs/h/h5647.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md); and they shall ['abad](../../strongs/h/h5647.md) him: and I have [nathan](../../strongs/h/h5414.md) him the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) also.

<a name="jeremiah_28_15"></a>Jeremiah 28:15

Then ['āmar](../../strongs/h/h559.md) the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto [Ḥănanyâ](../../strongs/h/h2608.md) the [nāḇî'](../../strongs/h/h5030.md), [shama'](../../strongs/h/h8085.md) now, [Ḥănanyâ](../../strongs/h/h2608.md); [Yĕhovah](../../strongs/h/h3068.md) hath not [shalach](../../strongs/h/h7971.md) thee; but thou makest this ['am](../../strongs/h/h5971.md) to [batach](../../strongs/h/h982.md) in a [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_28_16"></a>Jeremiah 28:16

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [shalach](../../strongs/h/h7971.md) thee from off the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md): this [šānâ](../../strongs/h/h8141.md) thou shalt [muwth](../../strongs/h/h4191.md), because thou hast [dabar](../../strongs/h/h1696.md) [sārâ](../../strongs/h/h5627.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_28_17"></a>Jeremiah 28:17

So [Ḥănanyâ](../../strongs/h/h2608.md) the [nāḇî'](../../strongs/h/h5030.md) [muwth](../../strongs/h/h4191.md) the same [šānâ](../../strongs/h/h8141.md) in the [šᵊḇîʿî](../../strongs/h/h7637.md) [ḥōḏeš](../../strongs/h/h2320.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 27](jeremiah_27.md) - [Jeremiah 29](jeremiah_29.md)