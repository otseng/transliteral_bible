# [Jeremiah 45](https://www.blueletterbible.org/kjv/jeremiah/45)

<a name="jeremiah_45_1"></a>Jeremiah 45:1

The [dabar](../../strongs/h/h1697.md) that [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [dabar](../../strongs/h/h1696.md) unto [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md), when he had [kāṯaḇ](../../strongs/h/h3789.md) these [dabar](../../strongs/h/h1697.md) in a [sēp̄er](../../strongs/h/h5612.md) at the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md), in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_45_2"></a>Jeremiah 45:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), unto thee, O [Bārûḵ](../../strongs/h/h1263.md);

<a name="jeremiah_45_3"></a>Jeremiah 45:3

Thou didst ['āmar](../../strongs/h/h559.md), ['owy](../../strongs/h/h188.md) is me now! for [Yĕhovah](../../strongs/h/h3068.md) hath [yāsap̄](../../strongs/h/h3254.md) [yagown](../../strongs/h/h3015.md) to my [maḵ'ōḇ](../../strongs/h/h4341.md); I [yaga'](../../strongs/h/h3021.md) in my ['anachah](../../strongs/h/h585.md), and I [māṣā'](../../strongs/h/h4672.md) no [mᵊnûḥâ](../../strongs/h/h4496.md).

<a name="jeremiah_45_4"></a>Jeremiah 45:4

Thus shalt thou ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) thus; Behold, that which I have [bānâ](../../strongs/h/h1129.md) will I [harac](../../strongs/h/h2040.md), and that which I have [nāṭaʿ](../../strongs/h/h5193.md) I will [nathash](../../strongs/h/h5428.md), even this ['erets](../../strongs/h/h776.md).

<a name="jeremiah_45_5"></a>Jeremiah 45:5

And [bāqaš](../../strongs/h/h1245.md) thou [gadowl](../../strongs/h/h1419.md) for thyself? [bāqaš](../../strongs/h/h1245.md) them not: for, behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon all [basar](../../strongs/h/h1320.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): but thy [nephesh](../../strongs/h/h5315.md) will I [nathan](../../strongs/h/h5414.md) unto thee for a [šālāl](../../strongs/h/h7998.md) in all [maqowm](../../strongs/h/h4725.md) whither thou [yālaḵ](../../strongs/h/h3212.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 44](jeremiah_44.md) - [Jeremiah 46](jeremiah_46.md)