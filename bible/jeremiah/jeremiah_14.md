# [Jeremiah 14](https://www.blueletterbible.org/kjv/jeremiah/14)

<a name="jeremiah_14_1"></a>Jeremiah 14:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) [dabar](../../strongs/h/h1697.md) the [baṣṣōreṯ](../../strongs/h/h1226.md).

<a name="jeremiah_14_2"></a>Jeremiah 14:2

[Yehuwdah](../../strongs/h/h3063.md) ['āḇal](../../strongs/h/h56.md), and the [sha'ar](../../strongs/h/h8179.md) thereof ['āmal](../../strongs/h/h535.md); they are [qāḏar](../../strongs/h/h6937.md) unto the ['erets](../../strongs/h/h776.md); and the [ṣᵊvāḥâ](../../strongs/h/h6682.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) is [ʿālâ](../../strongs/h/h5927.md).

<a name="jeremiah_14_3"></a>Jeremiah 14:3

And their ['addiyr](../../strongs/h/h117.md) have [shalach](../../strongs/h/h7971.md) their [ṣāʿîr](../../strongs/h/h6810.md) to the [mayim](../../strongs/h/h4325.md): they [bow'](../../strongs/h/h935.md) to the [gēḇ](../../strongs/h/h1356.md) [geḇe'](../../strongs/h/h1360.md), and [māṣā'](../../strongs/h/h4672.md) no [mayim](../../strongs/h/h4325.md); they [shuwb](../../strongs/h/h7725.md) with their [kĕliy](../../strongs/h/h3627.md) [rêqām](../../strongs/h/h7387.md); they were [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md), and [ḥāp̄â](../../strongs/h/h2645.md) their [ro'sh](../../strongs/h/h7218.md).

<a name="jeremiah_14_4"></a>Jeremiah 14:4

Because the ['ăḏāmâ](../../strongs/h/h127.md) is [ḥāṯaṯ](../../strongs/h/h2865.md), for there was no [gešem](../../strongs/h/h1653.md) in the ['erets](../../strongs/h/h776.md), the ['ikār](../../strongs/h/h406.md) were [buwsh](../../strongs/h/h954.md), they [ḥāp̄â](../../strongs/h/h2645.md) their [ro'sh](../../strongs/h/h7218.md).

<a name="jeremiah_14_5"></a>Jeremiah 14:5

Yea, the ['ayyeleṯ](../../strongs/h/h365.md) also [yalad](../../strongs/h/h3205.md) in the [sadeh](../../strongs/h/h7704.md), and ['azab](../../strongs/h/h5800.md) it, because there was no [deše'](../../strongs/h/h1877.md).

<a name="jeremiah_14_6"></a>Jeremiah 14:6

And the [pere'](../../strongs/h/h6501.md) did ['amad](../../strongs/h/h5975.md) in the [šᵊp̄î](../../strongs/h/h8205.md), they [šā'ap̄](../../strongs/h/h7602.md) the [ruwach](../../strongs/h/h7307.md) like [tannîn](../../strongs/h/h8577.md); their ['ayin](../../strongs/h/h5869.md) did [kalah](../../strongs/h/h3615.md), because there was no ['eseb](../../strongs/h/h6212.md).

<a name="jeremiah_14_7"></a>Jeremiah 14:7

[Yĕhovah](../../strongs/h/h3068.md), though our ['avon](../../strongs/h/h5771.md) ['anah](../../strongs/h/h6030.md) against us, ['asah](../../strongs/h/h6213.md) thou it for thy [shem](../../strongs/h/h8034.md) sake: for our [mᵊšûḇâ](../../strongs/h/h4878.md) are [rabab](../../strongs/h/h7231.md); we have [chata'](../../strongs/h/h2398.md) against thee.

<a name="jeremiah_14_8"></a>Jeremiah 14:8

O the [miqvê](../../strongs/h/h4723.md) of [Yisra'el](../../strongs/h/h3478.md), the [yasha'](../../strongs/h/h3467.md) thereof in [ʿēṯ](../../strongs/h/h6256.md) of [tsarah](../../strongs/h/h6869.md), why shouldest thou be as a [ger](../../strongs/h/h1616.md) in the ['erets](../../strongs/h/h776.md), and as an ['āraḥ](../../strongs/h/h732.md) that [natah](../../strongs/h/h5186.md) to tarry for a [lûn](../../strongs/h/h3885.md)?

<a name="jeremiah_14_9"></a>Jeremiah 14:9

Why shouldest thou be as an ['iysh](../../strongs/h/h376.md) [dāham](../../strongs/h/h1724.md), as a [gibôr](../../strongs/h/h1368.md) that [yakol](../../strongs/h/h3201.md) [yasha'](../../strongs/h/h3467.md)? yet thou, [Yĕhovah](../../strongs/h/h3068.md), art in the [qereḇ](../../strongs/h/h7130.md) of us, and we are [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md); [yānaḥ](../../strongs/h/h3240.md) us not.

<a name="jeremiah_14_10"></a>Jeremiah 14:10

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto this ['am](../../strongs/h/h5971.md), Thus have they ['ahab](../../strongs/h/h157.md) to [nûaʿ](../../strongs/h/h5128.md), they have not [ḥāśaḵ](../../strongs/h/h2820.md) their [regel](../../strongs/h/h7272.md), therefore [Yĕhovah](../../strongs/h/h3068.md) doth not [ratsah](../../strongs/h/h7521.md) them; he will now [zakar](../../strongs/h/h2142.md) their ['avon](../../strongs/h/h5771.md), and [paqad](../../strongs/h/h6485.md) their [chatta'ath](../../strongs/h/h2403.md).

<a name="jeremiah_14_11"></a>Jeremiah 14:11

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, [palal](../../strongs/h/h6419.md) not for this ['am](../../strongs/h/h5971.md) for their [towb](../../strongs/h/h2896.md).

<a name="jeremiah_14_12"></a>Jeremiah 14:12

When they [ṣûm](../../strongs/h/h6684.md), I will not [shama'](../../strongs/h/h8085.md) their [rinnah](../../strongs/h/h7440.md); and when they [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md) and a [minchah](../../strongs/h/h4503.md), I will not [ratsah](../../strongs/h/h7521.md) them: but I will [kalah](../../strongs/h/h3615.md) them by the [chereb](../../strongs/h/h2719.md), and by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md).

<a name="jeremiah_14_13"></a>Jeremiah 14:13

Then ['āmar](../../strongs/h/h559.md) I, ['ăhâ](../../strongs/h/h162.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! behold, the [nāḇî'](../../strongs/h/h5030.md) ['āmar](../../strongs/h/h559.md) unto them, Ye shall not [ra'ah](../../strongs/h/h7200.md) the [chereb](../../strongs/h/h2719.md), neither shall ye have [rāʿāḇ](../../strongs/h/h7458.md); but I will [nathan](../../strongs/h/h5414.md) you ['emeth](../../strongs/h/h571.md) [shalowm](../../strongs/h/h7965.md) in this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_14_14"></a>Jeremiah 14:14

Then [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, The [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md) in my [shem](../../strongs/h/h8034.md): I [shalach](../../strongs/h/h7971.md) them not, neither have I [tsavah](../../strongs/h/h6680.md) them, neither [dabar](../../strongs/h/h1696.md) unto them: they [nāḇā'](../../strongs/h/h5012.md) unto you a [sheqer](../../strongs/h/h8267.md) [ḥāzôn](../../strongs/h/h2377.md) and [qesem](../../strongs/h/h7081.md), and a thing of ['ĕlîl](../../strongs/h/h457.md) H434, and the [tārmâ](../../strongs/h/h8649.md) of their [leb](../../strongs/h/h3820.md).

<a name="jeremiah_14_15"></a>Jeremiah 14:15

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning the [nāḇî'](../../strongs/h/h5030.md) that [nāḇā'](../../strongs/h/h5012.md) in my [shem](../../strongs/h/h8034.md), and I [shalach](../../strongs/h/h7971.md) them not, yet they ['āmar](../../strongs/h/h559.md), [chereb](../../strongs/h/h2719.md) and [rāʿāḇ](../../strongs/h/h7458.md) shall not be in this ['erets](../../strongs/h/h776.md); By [chereb](../../strongs/h/h2719.md) and [rāʿāḇ](../../strongs/h/h7458.md) shall those [nāḇî'](../../strongs/h/h5030.md) be [tamam](../../strongs/h/h8552.md).

<a name="jeremiah_14_16"></a>Jeremiah 14:16

And the ['am](../../strongs/h/h5971.md) to whom they [nāḇā'](../../strongs/h/h5012.md) shall be [shalak](../../strongs/h/h7993.md) in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [paniym](../../strongs/h/h6440.md) of the [rāʿāḇ](../../strongs/h/h7458.md) and the [chereb](../../strongs/h/h2719.md); and they shall have none to [qāḇar](../../strongs/h/h6912.md) [hēm](../../strongs/h/h1992.md), them, their ['ishshah](../../strongs/h/h802.md), nor their [ben](../../strongs/h/h1121.md), nor their [bath](../../strongs/h/h1323.md): for I will [šāp̄aḵ](../../strongs/h/h8210.md) their [ra'](../../strongs/h/h7451.md) upon them.

<a name="jeremiah_14_17"></a>Jeremiah 14:17

Therefore thou shalt ['āmar](../../strongs/h/h559.md) this [dabar](../../strongs/h/h1697.md) unto them; Let mine ['ayin](../../strongs/h/h5869.md) run [yarad](../../strongs/h/h3381.md) with [dim'ah](../../strongs/h/h1832.md) [layil](../../strongs/h/h3915.md) and [yômām](../../strongs/h/h3119.md), and let them not [damah](../../strongs/h/h1820.md): for the [bᵊṯûlâ](../../strongs/h/h1330.md) [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) is [shabar](../../strongs/h/h7665.md) with a [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md), with a [me'od](../../strongs/h/h3966.md) [ḥālâ](../../strongs/h/h2470.md) [makâ](../../strongs/h/h4347.md).

<a name="jeremiah_14_18"></a>Jeremiah 14:18

If I [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md), then behold the [ḥālāl](../../strongs/h/h2491.md) with the [chereb](../../strongs/h/h2719.md)! and if I [bow'](../../strongs/h/h935.md) into the [ʿîr](../../strongs/h/h5892.md), then behold them that are [taḥălu'iym](../../strongs/h/h8463.md) with [rāʿāḇ](../../strongs/h/h7458.md)! yea, both the [nāḇî'](../../strongs/h/h5030.md) and the [kōhēn](../../strongs/h/h3548.md) go [sāḥar](../../strongs/h/h5503.md) into an ['erets](../../strongs/h/h776.md) that they [yada'](../../strongs/h/h3045.md) not.

<a name="jeremiah_14_19"></a>Jeremiah 14:19

Hast thou [mā'as](../../strongs/h/h3988.md) [mā'as](../../strongs/h/h3988.md) [Yehuwdah](../../strongs/h/h3063.md)? hath thy [nephesh](../../strongs/h/h5315.md) [gāʿal](../../strongs/h/h1602.md) [Tsiyown](../../strongs/h/h6726.md)? why hast thou [nakah](../../strongs/h/h5221.md) us, and there is no [marpē'](../../strongs/h/h4832.md) for us? we [qāvâ](../../strongs/h/h6960.md) for [shalowm](../../strongs/h/h7965.md), and there is no [towb](../../strongs/h/h2896.md); and for the [ʿēṯ](../../strongs/h/h6256.md) of [marpē'](../../strongs/h/h4832.md), and behold [bᵊʿāṯâ](../../strongs/h/h1205.md)!

<a name="jeremiah_14_20"></a>Jeremiah 14:20

We [yada'](../../strongs/h/h3045.md), [Yĕhovah](../../strongs/h/h3068.md), our [resha'](../../strongs/h/h7562.md), and the ['avon](../../strongs/h/h5771.md) of our ['ab](../../strongs/h/h1.md): for we have [chata'](../../strongs/h/h2398.md) against thee.

<a name="jeremiah_14_21"></a>Jeremiah 14:21

Do not [na'ats](../../strongs/h/h5006.md) us, for thy [shem](../../strongs/h/h8034.md) sake, do not [nabel](../../strongs/h/h5034.md) the [kicce'](../../strongs/h/h3678.md) of thy [kabowd](../../strongs/h/h3519.md): [zakar](../../strongs/h/h2142.md), [pārar](../../strongs/h/h6565.md) not thy [bĕriyth](../../strongs/h/h1285.md) with us.

<a name="jeremiah_14_22"></a>Jeremiah 14:22

Are [yēš](../../strongs/h/h3426.md) any among the [heḇel](../../strongs/h/h1892.md) of the [gowy](../../strongs/h/h1471.md) that can cause [gāšam](../../strongs/h/h1652.md)? or can the [shamayim](../../strongs/h/h8064.md) [nathan](../../strongs/h/h5414.md) [rᵊḇîḇîm](../../strongs/h/h7241.md)? art not thou he, [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md)? therefore we will [qāvâ](../../strongs/h/h6960.md) upon thee: for thou hast ['asah](../../strongs/h/h6213.md) all these things.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 13](jeremiah_13.md) - [Jeremiah 15](jeremiah_15.md)