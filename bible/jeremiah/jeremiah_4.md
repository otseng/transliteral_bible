# [Jeremiah 4](https://www.blueletterbible.org/kjv/jeremiah/4)

<a name="jeremiah_4_1"></a>Jeremiah 4:1

If thou wilt [shuwb](../../strongs/h/h7725.md), O [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), [shuwb](../../strongs/h/h7725.md) unto me: and if thou wilt [cuwr](../../strongs/h/h5493.md) thine [šiqqûṣ](../../strongs/h/h8251.md) out of my [paniym](../../strongs/h/h6440.md), then shalt thou not [nuwd](../../strongs/h/h5110.md).

<a name="jeremiah_4_2"></a>Jeremiah 4:2

And thou shalt [shaba'](../../strongs/h/h7650.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), in ['emeth](../../strongs/h/h571.md), in [mishpat](../../strongs/h/h4941.md), and in [tsedaqah](../../strongs/h/h6666.md); and the [gowy](../../strongs/h/h1471.md) shall [barak](../../strongs/h/h1288.md) themselves in him, and in him shall they [halal](../../strongs/h/h1984.md).

<a name="jeremiah_4_3"></a>Jeremiah 4:3

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) to the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), [nîr](../../strongs/h/h5214.md) your [nîr](../../strongs/h/h5215.md), and [zāraʿ](../../strongs/h/h2232.md) not among [qowts](../../strongs/h/h6975.md).

<a name="jeremiah_4_4"></a>Jeremiah 4:4

[muwl](../../strongs/h/h4135.md) yourselves to [Yĕhovah](../../strongs/h/h3068.md), and [cuwr](../../strongs/h/h5493.md) the [ʿārlâ](../../strongs/h/h6190.md) of your [lebab](../../strongs/h/h3824.md), ye ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) and [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): lest my [chemah](../../strongs/h/h2534.md) [yāṣā'](../../strongs/h/h3318.md) like ['esh](../../strongs/h/h784.md), and [bāʿar](../../strongs/h/h1197.md) that none can [kāḇâ](../../strongs/h/h3518.md) it, [paniym](../../strongs/h/h6440.md) of the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_4_5"></a>Jeremiah 4:5

[nāḡaḏ](../../strongs/h/h5046.md) ye in [Yehuwdah](../../strongs/h/h3063.md), and [shama'](../../strongs/h/h8085.md) in [Yĕruwshalaim](../../strongs/h/h3389.md); and ['āmar](../../strongs/h/h559.md), [tāqaʿ](../../strongs/h/h8628.md) ye the [šôp̄ār](../../strongs/h/h7782.md) in the ['erets](../../strongs/h/h776.md): [qara'](../../strongs/h/h7121.md), [mālā'](../../strongs/h/h4390.md), and ['āmar](../../strongs/h/h559.md), ['āsap̄](../../strongs/h/h622.md) yourselves, and let us [bow'](../../strongs/h/h935.md) into the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_4_6"></a>Jeremiah 4:6

[nasa'](../../strongs/h/h5375.md) the [nēs](../../strongs/h/h5251.md) toward [Tsiyown](../../strongs/h/h6726.md): [ʿûz](../../strongs/h/h5756.md), ['amad](../../strongs/h/h5975.md) not: for I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md), and a [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md).

<a name="jeremiah_4_7"></a>Jeremiah 4:7

The ['ariy](../../strongs/h/h738.md) is [ʿālâ](../../strongs/h/h5927.md) from his [sōḇeḵ](../../strongs/h/h5441.md), and the [shachath](../../strongs/h/h7843.md) of the [gowy](../../strongs/h/h1471.md) is on his [nāsaʿ](../../strongs/h/h5265.md); he is  [yāṣā'](../../strongs/h/h3318.md) from his [maqowm](../../strongs/h/h4725.md) to [śûm](../../strongs/h/h7760.md) thy ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md); and thy [ʿîr](../../strongs/h/h5892.md) shall be [nāṣâ](../../strongs/h/h5327.md), without a [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_4_8"></a>Jeremiah 4:8

For this [ḥāḡar](../../strongs/h/h2296.md) you with [śaq](../../strongs/h/h8242.md), [sāp̄aḏ](../../strongs/h/h5594.md) and [yālal](../../strongs/h/h3213.md): for the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) is not [shuwb](../../strongs/h/h7725.md) from us.

<a name="jeremiah_4_9"></a>Jeremiah 4:9

And it shall come to pass at that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that the [leb](../../strongs/h/h3820.md) of the [melek](../../strongs/h/h4428.md) shall ['abad](../../strongs/h/h6.md), and the [leb](../../strongs/h/h3820.md) of the [śar](../../strongs/h/h8269.md); and the [kōhēn](../../strongs/h/h3548.md) shall be [šāmēm](../../strongs/h/h8074.md), and the [nāḇî'](../../strongs/h/h5030.md) shall [tāmah](../../strongs/h/h8539.md).

<a name="jeremiah_4_10"></a>Jeremiah 4:10

Then ['āmar](../../strongs/h/h559.md) I, ['ăhâ](../../strongs/h/h162.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! ['āḵēn](../../strongs/h/h403.md) thou hast [nasha'](../../strongs/h/h5377.md) [nasha'](../../strongs/h/h5377.md) this ['am](../../strongs/h/h5971.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), Ye shall have [shalowm](../../strongs/h/h7965.md); whereas the [chereb](../../strongs/h/h2719.md) [naga'](../../strongs/h/h5060.md) unto the [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_4_11"></a>Jeremiah 4:11

At that [ʿēṯ](../../strongs/h/h6256.md) shall it be ['āmar](../../strongs/h/h559.md) to this ['am](../../strongs/h/h5971.md) and to [Yĕruwshalaim](../../strongs/h/h3389.md), A [ṣaḥ](../../strongs/h/h6703.md) [ruwach](../../strongs/h/h7307.md) of the [šᵊp̄î](../../strongs/h/h8205.md) in the [midbar](../../strongs/h/h4057.md) [derek](../../strongs/h/h1870.md) the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md), not to [zārâ](../../strongs/h/h2219.md), nor to [bārar](../../strongs/h/h1305.md),

<a name="jeremiah_4_12"></a>Jeremiah 4:12

Even a [mālē'](../../strongs/h/h4392.md) [ruwach](../../strongs/h/h7307.md) from those places shall [bow'](../../strongs/h/h935.md) unto me: now also will I [dabar](../../strongs/h/h1696.md) [mishpat](../../strongs/h/h4941.md) against them.

<a name="jeremiah_4_13"></a>Jeremiah 4:13

Behold, he shall [ʿālâ](../../strongs/h/h5927.md) as [ʿānān](../../strongs/h/h6051.md), and his [merkāḇâ](../../strongs/h/h4818.md) shall be as a [sûp̄â](../../strongs/h/h5492.md): his [sûs](../../strongs/h/h5483.md) are [qālal](../../strongs/h/h7043.md) than [nesheׁr](../../strongs/h/h5404.md). ['owy](../../strongs/h/h188.md) unto us! for we are [shadad](../../strongs/h/h7703.md).

<a name="jeremiah_4_14"></a>Jeremiah 4:14

[Yĕruwshalaim](../../strongs/h/h3389.md), [kāḇas](../../strongs/h/h3526.md) thine [leb](../../strongs/h/h3820.md) from [ra'](../../strongs/h/h7451.md), that thou mayest be [yasha'](../../strongs/h/h3467.md). How long shall thy ['aven](../../strongs/h/h205.md) [maḥăšāḇâ](../../strongs/h/h4284.md) [lûn](../../strongs/h/h3885.md) [qereḇ](../../strongs/h/h7130.md) thee?

<a name="jeremiah_4_15"></a>Jeremiah 4:15

For a [qowl](../../strongs/h/h6963.md) [nāḡaḏ](../../strongs/h/h5046.md) from [Dān](../../strongs/h/h1835.md), and [shama'](../../strongs/h/h8085.md) ['aven](../../strongs/h/h205.md) from [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md).

<a name="jeremiah_4_16"></a>Jeremiah 4:16

Make ye [zakar](../../strongs/h/h2142.md) to the [gowy](../../strongs/h/h1471.md); behold, [shama'](../../strongs/h/h8085.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), that [nāṣar](../../strongs/h/h5341.md) [bow'](../../strongs/h/h935.md) from a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md), and give [nathan](../../strongs/h/h5414.md) their [qowl](../../strongs/h/h6963.md) against the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_4_17"></a>Jeremiah 4:17

As [shamar](../../strongs/h/h8104.md) of a [sadeh](../../strongs/h/h7704.md), are they against her [cabiyb](../../strongs/h/h5439.md); because she hath been [marah](../../strongs/h/h4784.md) against me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_4_18"></a>Jeremiah 4:18

Thy [derek](../../strongs/h/h1870.md) and thy [maʿălāl](../../strongs/h/h4611.md) have ['asah](../../strongs/h/h6213.md) these things unto thee; this is thy [ra'](../../strongs/h/h7451.md), because it is [mar](../../strongs/h/h4751.md), because it [naga'](../../strongs/h/h5060.md) unto thine [leb](../../strongs/h/h3820.md).

<a name="jeremiah_4_19"></a>Jeremiah 4:19

My [me'ah](../../strongs/h/h4578.md), my [me'ah](../../strongs/h/h4578.md)! I am [chuwl](../../strongs/h/h2342.md) [yāḥal](../../strongs/h/h3176.md) at my [qîr](../../strongs/h/h7023.md) [leb](../../strongs/h/h3820.md); my [leb](../../strongs/h/h3820.md) maketh a [hāmâ](../../strongs/h/h1993.md) in me; I cannot hold my [ḥāraš](../../strongs/h/h2790.md), because thou hast [shama'](../../strongs/h/h8085.md), O my [nephesh](../../strongs/h/h5315.md), the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), the [tᵊrûʿâ](../../strongs/h/h8643.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_4_20"></a>Jeremiah 4:20

[šeḇar](../../strongs/h/h7667.md) upon [šeḇar](../../strongs/h/h7667.md) is [qara'](../../strongs/h/h7121.md); for the ['erets](../../strongs/h/h776.md) is [shadad](../../strongs/h/h7703.md): [piṯ'ōm](../../strongs/h/h6597.md) are my ['ohel](../../strongs/h/h168.md) [shadad](../../strongs/h/h7703.md), and my [yᵊrîʿâ](../../strongs/h/h3407.md) in a [reḡaʿ](../../strongs/h/h7281.md).

<a name="jeremiah_4_21"></a>Jeremiah 4:21

How long shall I [ra'ah](../../strongs/h/h7200.md) the [nēs](../../strongs/h/h5251.md), and [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md)?

<a name="jeremiah_4_22"></a>Jeremiah 4:22

For my ['am](../../strongs/h/h5971.md) is ['ĕvîl](../../strongs/h/h191.md), they have not [yada'](../../strongs/h/h3045.md) me; they are [sāḵāl](../../strongs/h/h5530.md) [ben](../../strongs/h/h1121.md), and they have none [bîn](../../strongs/h/h995.md): they are [ḥāḵām](../../strongs/h/h2450.md) to do [ra'a'](../../strongs/h/h7489.md), but to do [yatab](../../strongs/h/h3190.md) they have no [yada'](../../strongs/h/h3045.md).

<a name="jeremiah_4_23"></a>Jeremiah 4:23

I [ra'ah](../../strongs/h/h7200.md) the ['erets](../../strongs/h/h776.md), and, lo, it was without [tohuw](../../strongs/h/h8414.md), and [bohuw](../../strongs/h/h922.md); and the [shamayim](../../strongs/h/h8064.md), and they had no ['owr](../../strongs/h/h216.md).

<a name="jeremiah_4_24"></a>Jeremiah 4:24

I [ra'ah](../../strongs/h/h7200.md) the [har](../../strongs/h/h2022.md), and, lo, they [rāʿaš](../../strongs/h/h7493.md), and all the [giḇʿâ](../../strongs/h/h1389.md) moved [qālal](../../strongs/h/h7043.md).

<a name="jeremiah_4_25"></a>Jeremiah 4:25

I [ra'ah](../../strongs/h/h7200.md), and, lo, there was no ['āḏām](../../strongs/h/h120.md), and all the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) were [nāḏaḏ](../../strongs/h/h5074.md).

<a name="jeremiah_4_26"></a>Jeremiah 4:26

I [ra'ah](../../strongs/h/h7200.md), and, lo, the [karmel](../../strongs/h/h3759.md) was a [midbar](../../strongs/h/h4057.md), and all the [ʿîr](../../strongs/h/h5892.md) thereof were [nāṯaṣ](../../strongs/h/h5422.md) at the [paniym](../../strongs/h/h6440.md) of [Yĕhovah](../../strongs/h/h3068.md), and by his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md).

<a name="jeremiah_4_27"></a>Jeremiah 4:27

For thus hath [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), The ['erets](../../strongs/h/h776.md) shall be [šᵊmāmâ](../../strongs/h/h8077.md); yet will I not ['asah](../../strongs/h/h6213.md) a [kālâ](../../strongs/h/h3617.md).

<a name="jeremiah_4_28"></a>Jeremiah 4:28

For this shall the ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md), and the [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md) be [qāḏar](../../strongs/h/h6937.md): because I have [dabar](../../strongs/h/h1696.md) it, I have [zāmam](../../strongs/h/h2161.md) it, and will not [nacham](../../strongs/h/h5162.md), neither will I [shuwb](../../strongs/h/h7725.md) from it.

<a name="jeremiah_4_29"></a>Jeremiah 4:29

The whole [ʿîr](../../strongs/h/h5892.md) shall [bāraḥ](../../strongs/h/h1272.md) for the [qowl](../../strongs/h/h6963.md) of the [pārāš](../../strongs/h/h6571.md) and [qesheth](../../strongs/h/h7198.md) [rāmâ](../../strongs/h/h7411.md); they shall [bow'](../../strongs/h/h935.md) into ['ab](../../strongs/h/h5645.md), and [ʿālâ](../../strongs/h/h5927.md) upon the [kᵊp̄](../../strongs/h/h3710.md): every [ʿîr](../../strongs/h/h5892.md) shall be ['azab](../../strongs/h/h5800.md), and not an ['iysh](../../strongs/h/h376.md) [yashab](../../strongs/h/h3427.md) [hēn](../../strongs/h/h2004.md).

<a name="jeremiah_4_30"></a>Jeremiah 4:30

And when thou art [shadad](../../strongs/h/h7703.md), what wilt thou ['asah](../../strongs/h/h6213.md)? Though thou [labash](../../strongs/h/h3847.md) thyself with [šānî](../../strongs/h/h8144.md), though thou [ʿāḏâ](../../strongs/h/h5710.md) thee with [ʿădiy](../../strongs/h/h5716.md) of [zāhāḇ](../../strongs/h/h2091.md), though thou [qāraʿ](../../strongs/h/h7167.md) thy ['ayin](../../strongs/h/h5869.md) with [pûḵ](../../strongs/h/h6320.md), in [shav'](../../strongs/h/h7723.md) shalt thou make thyself [yāp̄â](../../strongs/h/h3302.md); thy [ʿāḡaḇ](../../strongs/h/h5689.md) will [mā'as](../../strongs/h/h3988.md) thee, they will [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_4_31"></a>Jeremiah 4:31

For I have [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md) as of a woman in [ḥālâ](../../strongs/h/h2470.md), and the [tsarah](../../strongs/h/h6869.md) as of her that bringeth forth her first [bāḵar](../../strongs/h/h1069.md), the [qowl](../../strongs/h/h6963.md) of the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md), that [yāp̄aḥ](../../strongs/h/h3306.md) herself, that [pāraś](../../strongs/h/h6566.md) her [kaph](../../strongs/h/h3709.md), saying, ['owy](../../strongs/h/h188.md) is me now! for my [nephesh](../../strongs/h/h5315.md) is [ʿāyēp̄](../../strongs/h/h5888.md) because of [harag](../../strongs/h/h2026.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 3](jeremiah_3.md) - [Jeremiah 5](jeremiah_5.md)