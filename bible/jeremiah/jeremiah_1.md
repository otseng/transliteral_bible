# [Jeremiah 1](https://www.blueletterbible.org/kjv/jeremiah/1)

<a name="jeremiah_1_1"></a>Jeremiah 1:1

The [dabar](../../strongs/h/h1697.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), of the [kōhēn](../../strongs/h/h3548.md) that were in [ʿĂnāṯôṯ](../../strongs/h/h6068.md) in the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md):

<a name="jeremiah_1_2"></a>Jeremiah 1:2

To whom the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came in the [yowm](../../strongs/h/h3117.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) the [ben](../../strongs/h/h1121.md) of ['Āmôn](../../strongs/h/h526.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md).

<a name="jeremiah_1_3"></a>Jeremiah 1:3

It came also in the [yowm](../../strongs/h/h3117.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), unto the [tamam](../../strongs/h/h8552.md) of the [ʿaštê](../../strongs/h/h6249.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), unto the [gālâ](../../strongs/h/h1540.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [gālâ](../../strongs/h/h1540.md) in the [ḥămîšî](../../strongs/h/h2549.md) [ḥōḏeš](../../strongs/h/h2320.md).

<a name="jeremiah_1_4"></a>Jeremiah 1:4

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_1_5"></a>Jeremiah 1:5

[ṭerem](../../strongs/h/h2962.md) I [yāṣar](../../strongs/h/h3335.md) thee in the [beten](../../strongs/h/h990.md) I [yada'](../../strongs/h/h3045.md) thee; and before thou [yāṣā'](../../strongs/h/h3318.md) out of the [reḥem](../../strongs/h/h7358.md) I [qadash](../../strongs/h/h6942.md) thee, and I [nathan](../../strongs/h/h5414.md) thee a [nāḇî'](../../strongs/h/h5030.md) unto the [gowy](../../strongs/h/h1471.md).

<a name="jeremiah_1_6"></a>Jeremiah 1:6

Then ['āmar](../../strongs/h/h559.md) I, ['ăhâ](../../strongs/h/h162.md), ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! behold, I [yada'](../../strongs/h/h3045.md) [dabar](../../strongs/h/h1696.md): for I am a [naʿar](../../strongs/h/h5288.md).

<a name="jeremiah_1_7"></a>Jeremiah 1:7

But [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, ['āmar](../../strongs/h/h559.md) not, I am a [naʿar](../../strongs/h/h5288.md): for thou shalt [yālaḵ](../../strongs/h/h3212.md) to all that I shall [shalach](../../strongs/h/h7971.md) thee, and whatsoever I [tsavah](../../strongs/h/h6680.md) thee thou shalt [dabar](../../strongs/h/h1696.md).

<a name="jeremiah_1_8"></a>Jeremiah 1:8

Be not [yare'](../../strongs/h/h3372.md) of their [paniym](../../strongs/h/h6440.md): for I am with thee to [natsal](../../strongs/h/h5337.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_1_9"></a>Jeremiah 1:9

Then [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) his [yad](../../strongs/h/h3027.md), and [naga'](../../strongs/h/h5060.md) my [peh](../../strongs/h/h6310.md). And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, Behold, I have [nathan](../../strongs/h/h5414.md) my [dabar](../../strongs/h/h1697.md) in thy [peh](../../strongs/h/h6310.md).

<a name="jeremiah_1_10"></a>Jeremiah 1:10

[ra'ah](../../strongs/h/h7200.md), I have this [yowm](../../strongs/h/h3117.md) [paqad](../../strongs/h/h6485.md) thee over the [gowy](../../strongs/h/h1471.md) and over the [mamlāḵâ](../../strongs/h/h4467.md), to [nathash](../../strongs/h/h5428.md), and to [nāṯaṣ](../../strongs/h/h5422.md), and to ['abad](../../strongs/h/h6.md), and to [harac](../../strongs/h/h2040.md), to [bānâ](../../strongs/h/h1129.md), and to [nāṭaʿ](../../strongs/h/h5193.md).

<a name="jeremiah_1_11"></a>Jeremiah 1:11

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md), [Yirmᵊyâ](../../strongs/h/h3414.md), what [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) a [maqqēl](../../strongs/h/h4731.md) of a [šāqēḏ](../../strongs/h/h8247.md).

<a name="jeremiah_1_12"></a>Jeremiah 1:12

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, Thou hast [yatab](../../strongs/h/h3190.md) [ra'ah](../../strongs/h/h7200.md): for I will [šāqaḏ](../../strongs/h/h8245.md) my [dabar](../../strongs/h/h1697.md) to ['asah](../../strongs/h/h6213.md) it.

<a name="jeremiah_1_13"></a>Jeremiah 1:13

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me the [šēnî](../../strongs/h/h8145.md), ['āmar](../../strongs/h/h559.md), What [ra'ah](../../strongs/h/h7200.md) thou? And I ['āmar](../../strongs/h/h559.md), I [ra'ah](../../strongs/h/h7200.md) a [nāp̄aḥ](../../strongs/h/h5301.md) [sîr](../../strongs/h/h5518.md); and the [paniym](../../strongs/h/h6440.md) thereof is [paniym](../../strongs/h/h6440.md) the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="jeremiah_1_14"></a>Jeremiah 1:14

Then [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, Out of the [ṣāp̄ôn](../../strongs/h/h6828.md) a [ra'](../../strongs/h/h7451.md) shall [pāṯaḥ](../../strongs/h/h6605.md) upon all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_1_15"></a>Jeremiah 1:15

For, lo, I will [qara'](../../strongs/h/h7121.md) all the [mišpāḥâ](../../strongs/h/h4940.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and they shall [bow'](../../strongs/h/h935.md), and they shall [nathan](../../strongs/h/h5414.md) every ['iysh](../../strongs/h/h376.md) his [kicce'](../../strongs/h/h3678.md) at the [peṯaḥ](../../strongs/h/h6607.md) of the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and against all the [ḥômâ](../../strongs/h/h2346.md) thereof [cabiyb](../../strongs/h/h5439.md), and against all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_1_16"></a>Jeremiah 1:16

And I will [dabar](../../strongs/h/h1696.md) my [mishpat](../../strongs/h/h4941.md) against them [ʿal](../../strongs/h/h5921.md) all their [ra'](../../strongs/h/h7451.md), who have ['azab](../../strongs/h/h5800.md) me, and have [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) the [ma'aseh](../../strongs/h/h4639.md) of their own [yad](../../strongs/h/h3027.md).

<a name="jeremiah_1_17"></a>Jeremiah 1:17

Thou therefore ['āzar](../../strongs/h/h247.md) thy [māṯnayim](../../strongs/h/h4975.md), and [quwm](../../strongs/h/h6965.md), and [dabar](../../strongs/h/h1696.md) unto them all that I [tsavah](../../strongs/h/h6680.md) thee: be not [ḥāṯaṯ](../../strongs/h/h2865.md) at their [paniym](../../strongs/h/h6440.md), lest I [ḥāṯaṯ](../../strongs/h/h2865.md) thee [paniym](../../strongs/h/h6440.md) them.

<a name="jeremiah_1_18"></a>Jeremiah 1:18

For, behold, ['ănî](../../strongs/h/h589.md) have [nathan](../../strongs/h/h5414.md) thee this [yowm](../../strongs/h/h3117.md) a [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md), and a [barzel](../../strongs/h/h1270.md) [ʿammûḏ](../../strongs/h/h5982.md), and [nᵊḥšeṯ](../../strongs/h/h5178.md) [ḥômâ](../../strongs/h/h2346.md) against the ['erets](../../strongs/h/h776.md), against the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), against the [śar](../../strongs/h/h8269.md) thereof, against the [kōhēn](../../strongs/h/h3548.md) thereof, and against the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_1_19"></a>Jeremiah 1:19

And they shall [lāḥam](../../strongs/h/h3898.md) against thee; but they shall not [yakol](../../strongs/h/h3201.md) against thee; for I am with thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), to [natsal](../../strongs/h/h5337.md) thee.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 2](jeremiah_2.md)