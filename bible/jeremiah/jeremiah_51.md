# [Jeremiah 51](https://www.blueletterbible.org/kjv/jeremiah/51)

<a name="jeremiah_51_1"></a>Jeremiah 51:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [ʿûr](../../strongs/h/h5782.md) against [Bāḇel](../../strongs/h/h894.md), and against them that [yashab](../../strongs/h/h3427.md) in the [leb](../../strongs/h/h3820.md) of them that [quwm](../../strongs/h/h6965.md) against me, a [shachath](../../strongs/h/h7843.md) [ruwach](../../strongs/h/h7307.md);

<a name="jeremiah_51_2"></a>Jeremiah 51:2

And will [shalach](../../strongs/h/h7971.md) unto [Bāḇel](../../strongs/h/h894.md) [zûr](../../strongs/h/h2114.md), that shall [zārâ](../../strongs/h/h2219.md) her, and shall [bāqaq](../../strongs/h/h1238.md) her ['erets](../../strongs/h/h776.md): for in the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md) they shall be against her [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_51_3"></a>Jeremiah 51:3

Against him that [dāraḵ](../../strongs/h/h1869.md) let the [dāraḵ](../../strongs/h/h1869.md) [dāraḵ](../../strongs/h/h1869.md) his [qesheth](../../strongs/h/h7198.md), and against him that [ʿālâ](../../strongs/h/h5927.md) himself in his [siryōn](../../strongs/h/h5630.md): and [ḥāmal](../../strongs/h/h2550.md) ye not her [bāḥûr](../../strongs/h/h970.md); [ḥāram](../../strongs/h/h2763.md) ye all her [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_51_4"></a>Jeremiah 51:4

Thus the [ḥālāl](../../strongs/h/h2491.md) shall [naphal](../../strongs/h/h5307.md) in the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and they that are thrust [dāqar](../../strongs/h/h1856.md) in her [ḥûṣ](../../strongs/h/h2351.md).

<a name="jeremiah_51_5"></a>Jeremiah 51:5

For [Yisra'el](../../strongs/h/h3478.md) hath not been ['almān](../../strongs/h/h488.md), nor [Yehuwdah](../../strongs/h/h3063.md) of his ['Elohiym](../../strongs/h/h430.md), of [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); though their ['erets](../../strongs/h/h776.md) was [mālā'](../../strongs/h/h4390.md) with ['āšām](../../strongs/h/h817.md) against the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_51_6"></a>Jeremiah 51:6

Flee [nûs](../../strongs/h/h5127.md) of the [tavek](../../strongs/h/h8432.md) of [Bāḇel](../../strongs/h/h894.md), and [mālaṭ](../../strongs/h/h4422.md) every ['iysh](../../strongs/h/h376.md) his [nephesh](../../strongs/h/h5315.md): be not [damam](../../strongs/h/h1826.md) in her ['avon](../../strongs/h/h5771.md); for this is the [ʿēṯ](../../strongs/h/h6256.md) of [Yĕhovah](../../strongs/h/h3068.md) [nᵊqāmâ](../../strongs/h/h5360.md); he will [shalam](../../strongs/h/h7999.md) unto her a [gĕmwl](../../strongs/h/h1576.md).

<a name="jeremiah_51_7"></a>Jeremiah 51:7

[Bāḇel](../../strongs/h/h894.md) hath been a [zāhāḇ](../../strongs/h/h2091.md) [kowc](../../strongs/h/h3563.md) in [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md), that made all the ['erets](../../strongs/h/h776.md) [šāḵar](../../strongs/h/h7937.md): the [gowy](../../strongs/h/h1471.md) have [šāṯâ](../../strongs/h/h8354.md) of her [yayin](../../strongs/h/h3196.md); therefore the [gowy](../../strongs/h/h1471.md) are [halal](../../strongs/h/h1984.md).

<a name="jeremiah_51_8"></a>Jeremiah 51:8

[Bāḇel](../../strongs/h/h894.md) is [piṯ'ōm](../../strongs/h/h6597.md) [naphal](../../strongs/h/h5307.md) and [shabar](../../strongs/h/h7665.md): [yālal](../../strongs/h/h3213.md) for her; [laqach](../../strongs/h/h3947.md) [ṣŏrî](../../strongs/h/h6875.md) for her [maḵ'ōḇ](../../strongs/h/h4341.md), if so be she may be [rapha'](../../strongs/h/h7495.md).

<a name="jeremiah_51_9"></a>Jeremiah 51:9

We would have [rapha'](../../strongs/h/h7495.md) [Bāḇel](../../strongs/h/h894.md), but she is not [rapha'](../../strongs/h/h7495.md): ['azab](../../strongs/h/h5800.md) her, and let us [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) into his own ['erets](../../strongs/h/h776.md): for her [mishpat](../../strongs/h/h4941.md) [naga'](../../strongs/h/h5060.md) unto [shamayim](../../strongs/h/h8064.md), and is [nasa'](../../strongs/h/h5375.md) even to the [shachaq](../../strongs/h/h7834.md).

<a name="jeremiah_51_10"></a>Jeremiah 51:10

[Yĕhovah](../../strongs/h/h3068.md) hath [yāṣā'](../../strongs/h/h3318.md) our [tsedaqah](../../strongs/h/h6666.md): [bow'](../../strongs/h/h935.md), and let us [sāp̄ar](../../strongs/h/h5608.md) in [Tsiyown](../../strongs/h/h6726.md) the [ma'aseh](../../strongs/h/h4639.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_51_11"></a>Jeremiah 51:11

[bārar](../../strongs/h/h1305.md) the [chets](../../strongs/h/h2671.md); [mālā'](../../strongs/h/h4390.md) the [šeleṭ](../../strongs/h/h7982.md): [Yĕhovah](../../strongs/h/h3068.md) hath [ʿûr](../../strongs/h/h5782.md) the [ruwach](../../strongs/h/h7307.md) of the [melek](../../strongs/h/h4428.md) of the [Māḏay](../../strongs/h/h4074.md): for his [mezimmah](../../strongs/h/h4209.md) is against [Bāḇel](../../strongs/h/h894.md), to [shachath](../../strongs/h/h7843.md) it; because it is the [nᵊqāmâ](../../strongs/h/h5360.md) of [Yĕhovah](../../strongs/h/h3068.md), the [nᵊqāmâ](../../strongs/h/h5360.md) of his [heykal](../../strongs/h/h1964.md).

<a name="jeremiah_51_12"></a>Jeremiah 51:12

[nasa'](../../strongs/h/h5375.md) the [nēs](../../strongs/h/h5251.md) upon the [ḥômâ](../../strongs/h/h2346.md) of [Bāḇel](../../strongs/h/h894.md), make the [mišmār](../../strongs/h/h4929.md) [ḥāzaq](../../strongs/h/h2388.md), [quwm](../../strongs/h/h6965.md) the [shamar](../../strongs/h/h8104.md), [kuwn](../../strongs/h/h3559.md) the ['arab](../../strongs/h/h693.md): for [Yĕhovah](../../strongs/h/h3068.md) hath both [zāmam](../../strongs/h/h2161.md) and ['asah](../../strongs/h/h6213.md) that which he [dabar](../../strongs/h/h1696.md) against the [yashab](../../strongs/h/h3427.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_51_13"></a>Jeremiah 51:13

O thou that [shakan](../../strongs/h/h7931.md) [shakan](../../strongs/h/h7931.md) upon [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), [rab](../../strongs/h/h7227.md) in ['ôṣār](../../strongs/h/h214.md), thine [qēṣ](../../strongs/h/h7093.md) is [bow'](../../strongs/h/h935.md), and the ['ammâ](../../strongs/h/h520.md) of thy [beṣaʿ](../../strongs/h/h1215.md).

<a name="jeremiah_51_14"></a>Jeremiah 51:14

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) hath [shaba'](../../strongs/h/h7650.md) by [nephesh](../../strongs/h/h5315.md), saying, Surely I will [mālā'](../../strongs/h/h4390.md) thee with ['āḏām](../../strongs/h/h120.md), as with [yeleq](../../strongs/h/h3218.md); and they shall ['anah](../../strongs/h/h6030.md) a [hêḏāḏ](../../strongs/h/h1959.md) against thee.

<a name="jeremiah_51_15"></a>Jeremiah 51:15

He hath ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md) by his [koach](../../strongs/h/h3581.md), he hath [kuwn](../../strongs/h/h3559.md) the [tebel](../../strongs/h/h8398.md) by his [ḥāḵmâ](../../strongs/h/h2451.md), and hath [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) by his [tāḇûn](../../strongs/h/h8394.md).

<a name="jeremiah_51_16"></a>Jeremiah 51:16

When he [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md), there is a [hāmôn](../../strongs/h/h1995.md) of [mayim](../../strongs/h/h4325.md) in the [shamayim](../../strongs/h/h8064.md); and he causeth the [nāśî'](../../strongs/h/h5387.md) to [ʿālâ](../../strongs/h/h5927.md) from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md): he ['asah](../../strongs/h/h6213.md) [baraq](../../strongs/h/h1300.md) with [māṭār](../../strongs/h/h4306.md), and [yāṣā'](../../strongs/h/h3318.md) the [ruwach](../../strongs/h/h7307.md) out of his ['ôṣār](../../strongs/h/h214.md).

<a name="jeremiah_51_17"></a>Jeremiah 51:17

Every ['āḏām](../../strongs/h/h120.md) is [bāʿar](../../strongs/h/h1197.md) by his [da'ath](../../strongs/h/h1847.md); every [tsaraph](../../strongs/h/h6884.md) is [yāḇēš](../../strongs/h/h3001.md) by the [pecel](../../strongs/h/h6459.md): for his molten [necek](../../strongs/h/h5262.md) is [sheqer](../../strongs/h/h8267.md), and there is no [ruwach](../../strongs/h/h7307.md) in them.

<a name="jeremiah_51_18"></a>Jeremiah 51:18

They are [heḇel](../../strongs/h/h1892.md), the [ma'aseh](../../strongs/h/h4639.md) of [taʿtuʿîm](../../strongs/h/h8595.md): in the [ʿēṯ](../../strongs/h/h6256.md) of their [pᵊqudâ](../../strongs/h/h6486.md) they shall ['abad](../../strongs/h/h6.md).

<a name="jeremiah_51_19"></a>Jeremiah 51:19

The [cheleq](../../strongs/h/h2506.md) of [Ya'aqob](../../strongs/h/h3290.md) is not like them; for he is the [yāṣar](../../strongs/h/h3335.md) of all things: and Israel is the [shebet](../../strongs/h/h7626.md) of his [nachalah](../../strongs/h/h5159.md): [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md).

<a name="jeremiah_51_20"></a>Jeremiah 51:20

Thou art my [mapēṣ](../../strongs/h/h4661.md) and [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md): for with thee will I [naphats](../../strongs/h/h5310.md) the [gowy](../../strongs/h/h1471.md), and with thee will I [shachath](../../strongs/h/h7843.md) [mamlāḵâ](../../strongs/h/h4467.md);

<a name="jeremiah_51_21"></a>Jeremiah 51:21

And with thee will I [naphats](../../strongs/h/h5310.md) the [sûs](../../strongs/h/h5483.md) and his [rāḵaḇ](../../strongs/h/h7392.md); and with thee will I [naphats](../../strongs/h/h5310.md) the [reḵeḇ](../../strongs/h/h7393.md) and his [rāḵaḇ](../../strongs/h/h7392.md);

<a name="jeremiah_51_22"></a>Jeremiah 51:22

With thee also will I [naphats](../../strongs/h/h5310.md) ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md); and with thee will I [naphats](../../strongs/h/h5310.md) [zāqēn](../../strongs/h/h2205.md) and [naʿar](../../strongs/h/h5288.md); and with thee will I [naphats](../../strongs/h/h5310.md) the [bāḥûr](../../strongs/h/h970.md) and the [bᵊṯûlâ](../../strongs/h/h1330.md);

<a name="jeremiah_51_23"></a>Jeremiah 51:23

I will also [naphats](../../strongs/h/h5310.md) with thee the [ra'ah](../../strongs/h/h7462.md) and his [ʿēḏer](../../strongs/h/h5739.md); and with thee will I [naphats](../../strongs/h/h5310.md) the ['ikār](../../strongs/h/h406.md) and his [ṣemeḏ](../../strongs/h/h6776.md); and with thee will I [naphats](../../strongs/h/h5310.md) [peḥâ](../../strongs/h/h6346.md) and [sāḡān](../../strongs/h/h5461.md).

<a name="jeremiah_51_24"></a>Jeremiah 51:24

And I will [shalam](../../strongs/h/h7999.md) unto [Bāḇel](../../strongs/h/h894.md) and to all the [yashab](../../strongs/h/h3427.md) of [Kaśdîmâ](../../strongs/h/h3778.md) all their [ra'](../../strongs/h/h7451.md) that they have ['asah](../../strongs/h/h6213.md) in [Tsiyown](../../strongs/h/h6726.md) in your ['ayin](../../strongs/h/h5869.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_25"></a>Jeremiah 51:25

Behold, I am against thee, O [mašḥîṯ](../../strongs/h/h4889.md) [har](../../strongs/h/h2022.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), which [shachath](../../strongs/h/h7843.md) all the ['erets](../../strongs/h/h776.md): and I will [natah](../../strongs/h/h5186.md) mine [yad](../../strongs/h/h3027.md) upon thee, and roll thee [gālal](../../strongs/h/h1556.md) from the [cela'](../../strongs/h/h5553.md), and will [nathan](../../strongs/h/h5414.md) thee a [śᵊrēp̄â](../../strongs/h/h8316.md) [har](../../strongs/h/h2022.md).

<a name="jeremiah_51_26"></a>Jeremiah 51:26

And they shall not [laqach](../../strongs/h/h3947.md) of thee an ['eben](../../strongs/h/h68.md) for a [pinnâ](../../strongs/h/h6438.md), nor an ['eben](../../strongs/h/h68.md) for [mowcadah](../../strongs/h/h4146.md); but thou shalt be [šᵊmāmâ](../../strongs/h/h8077.md) ['owlam](../../strongs/h/h5769.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_27"></a>Jeremiah 51:27

Set ye [nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md) in the ['erets](../../strongs/h/h776.md), [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) among the [gowy](../../strongs/h/h1471.md), [qadash](../../strongs/h/h6942.md) the [gowy](../../strongs/h/h1471.md) against her, [shama'](../../strongs/h/h8085.md) against her the [mamlāḵâ](../../strongs/h/h4467.md) of ['Ărārāṭ](../../strongs/h/h780.md), [Minnî](../../strongs/h/h4508.md), and ['Aškᵊnaz](../../strongs/h/h813.md); [paqad](../../strongs/h/h6485.md) a [ṭip̄sar](../../strongs/h/h2951.md) against her; cause the [sûs](../../strongs/h/h5483.md) to [ʿālâ](../../strongs/h/h5927.md) as the [sāmār](../../strongs/h/h5569.md) [yeleq](../../strongs/h/h3218.md).

<a name="jeremiah_51_28"></a>Jeremiah 51:28

[qadash](../../strongs/h/h6942.md) against her the [gowy](../../strongs/h/h1471.md) with the [melek](../../strongs/h/h4428.md) of the [Māḏay](../../strongs/h/h4074.md), the [peḥâ](../../strongs/h/h6346.md) thereof, and all the [sāḡān](../../strongs/h/h5461.md) thereof, and all the ['erets](../../strongs/h/h776.md) of his [memshalah](../../strongs/h/h4475.md).

<a name="jeremiah_51_29"></a>Jeremiah 51:29

And the ['erets](../../strongs/h/h776.md) shall [rāʿaš](../../strongs/h/h7493.md) and [chuwl](../../strongs/h/h2342.md): for every [maḥăšāḇâ](../../strongs/h/h4284.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be [quwm](../../strongs/h/h6965.md) against [Bāḇel](../../strongs/h/h894.md), to [śûm](../../strongs/h/h7760.md) the ['erets](../../strongs/h/h776.md) of [Bāḇel](../../strongs/h/h894.md) a [šammâ](../../strongs/h/h8047.md) without a [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_51_30"></a>Jeremiah 51:30

The [gibôr](../../strongs/h/h1368.md) of [Bāḇel](../../strongs/h/h894.md) have [ḥāḏal](../../strongs/h/h2308.md) to [lāḥam](../../strongs/h/h3898.md), they have [yashab](../../strongs/h/h3427.md) in their [mᵊṣāḏ](../../strongs/h/h4679.md): their [gᵊḇûrâ](../../strongs/h/h1369.md) hath [nāšaṯ](../../strongs/h/h5405.md); they became as ['ishshah](../../strongs/h/h802.md): they have [yāṣaṯ](../../strongs/h/h3341.md) her [miškān](../../strongs/h/h4908.md); her [bᵊrîaḥ](../../strongs/h/h1280.md) are [shabar](../../strongs/h/h7665.md).

<a name="jeremiah_51_31"></a>Jeremiah 51:31

One [rûṣ](../../strongs/h/h7323.md) shall [rûṣ](../../strongs/h/h7323.md) to [qārā'](../../strongs/h/h7125.md) [rûṣ](../../strongs/h/h7323.md), and one [nāḡaḏ](../../strongs/h/h5046.md) to [qārā'](../../strongs/h/h7125.md) [nāḡaḏ](../../strongs/h/h5046.md), to [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) that his [ʿîr](../../strongs/h/h5892.md) is [lāḵaḏ](../../strongs/h/h3920.md) at one [qāṣê](../../strongs/h/h7097.md),

<a name="jeremiah_51_32"></a>Jeremiah 51:32

And that the [maʿăḇār](../../strongs/h/h4569.md) are [tāp̄aś](../../strongs/h/h8610.md), and the ['ăḡam](../../strongs/h/h98.md) they have [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md), and the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) are [bahal](../../strongs/h/h926.md).

<a name="jeremiah_51_33"></a>Jeremiah 51:33

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); The [bath](../../strongs/h/h1323.md) of [Bāḇel](../../strongs/h/h894.md) is like a [gōren](../../strongs/h/h1637.md), it is [ʿēṯ](../../strongs/h/h6256.md) to [dāraḵ](../../strongs/h/h1869.md) her: yet a [mᵊʿaṭ](../../strongs/h/h4592.md), and the [ʿēṯ](../../strongs/h/h6256.md) of her [qāṣîr](../../strongs/h/h7105.md) shall [bow'](../../strongs/h/h935.md).

<a name="jeremiah_51_34"></a>Jeremiah 51:34

[Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) hath ['akal](../../strongs/h/h398.md) me, he hath [hāmam](../../strongs/h/h2000.md) me, he hath [yāṣaḡ](../../strongs/h/h3322.md) me a [riyq](../../strongs/h/h7385.md) [kĕliy](../../strongs/h/h3627.md), he hath [bālaʿ](../../strongs/h/h1104.md) me like a [tannîn](../../strongs/h/h8577.md), he hath [mālā'](../../strongs/h/h4390.md) his [kᵊrēś](../../strongs/h/h3770.md) with my [ʿēḏen](../../strongs/h/h5730.md), he hath [dûaḥ](../../strongs/h/h1740.md) me.

<a name="jeremiah_51_35"></a>Jeremiah 51:35

The [chamac](../../strongs/h/h2555.md) done to me and to my [šᵊ'ēr](../../strongs/h/h7607.md) be upon [Bāḇel](../../strongs/h/h894.md), shall the [yashab](../../strongs/h/h3427.md) of [Tsiyown](../../strongs/h/h6726.md) ['āmar](../../strongs/h/h559.md); and my [dam](../../strongs/h/h1818.md) upon the [yashab](../../strongs/h/h3427.md) of [Kaśdîmâ](../../strongs/h/h3778.md), shall [Yĕruwshalaim](../../strongs/h/h3389.md) ['āmar](../../strongs/h/h559.md).

<a name="jeremiah_51_36"></a>Jeremiah 51:36

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [riyb](../../strongs/h/h7378.md) thy [rîḇ](../../strongs/h/h7379.md), and take [naqam](../../strongs/h/h5358.md) for [nᵊqāmâ](../../strongs/h/h5360.md); and I will dry [ḥāraḇ](../../strongs/h/h2717.md) her [yam](../../strongs/h/h3220.md), and make her [māqôr](../../strongs/h/h4726.md) [yāḇēš](../../strongs/h/h3001.md).

<a name="jeremiah_51_37"></a>Jeremiah 51:37

And [Bāḇel](../../strongs/h/h894.md) shall become [gal](../../strongs/h/h1530.md), a [māʿôn](../../strongs/h/h4583.md) for [tannîn](../../strongs/h/h8577.md), a [šammâ](../../strongs/h/h8047.md), and a [šᵊrēqâ](../../strongs/h/h8322.md), without a [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_51_38"></a>Jeremiah 51:38

They shall [šā'aḡ](../../strongs/h/h7580.md) [yaḥaḏ](../../strongs/h/h3162.md) like [kephiyr](../../strongs/h/h3715.md): they shall [nāʿar](../../strongs/h/h5286.md) as ['ariy](../../strongs/h/h738.md) [gôr](../../strongs/h/h1484.md).

<a name="jeremiah_51_39"></a>Jeremiah 51:39

In their [ḥōm](../../strongs/h/h2527.md) I will [shiyth](../../strongs/h/h7896.md) their [mištê](../../strongs/h/h4960.md), and I will make them [šāḵar](../../strongs/h/h7937.md), that they may [ʿālaz](../../strongs/h/h5937.md), and [yashen](../../strongs/h/h3462.md) a ['owlam](../../strongs/h/h5769.md) [šēnā'](../../strongs/h/h8142.md), and not [quwts](../../strongs/h/h6974.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_40"></a>Jeremiah 51:40

I will [yarad](../../strongs/h/h3381.md) them like [kar](../../strongs/h/h3733.md) to the [ṭāḇaḥ](../../strongs/h/h2873.md), like ['ayil](../../strongs/h/h352.md) with he [ʿatûḏ](../../strongs/h/h6260.md).

<a name="jeremiah_51_41"></a>Jeremiah 51:41

How is [Šēšaḵ](../../strongs/h/h8347.md) [lāḵaḏ](../../strongs/h/h3920.md)! and how is the [tehillah](../../strongs/h/h8416.md) of the ['erets](../../strongs/h/h776.md) [tāp̄aś](../../strongs/h/h8610.md)! how is [Bāḇel](../../strongs/h/h894.md) become a [šammâ](../../strongs/h/h8047.md) among the [gowy](../../strongs/h/h1471.md)!

<a name="jeremiah_51_42"></a>Jeremiah 51:42

The [yam](../../strongs/h/h3220.md) is [ʿālâ](../../strongs/h/h5927.md) upon [Bāḇel](../../strongs/h/h894.md): she is [kāsâ](../../strongs/h/h3680.md) with the [hāmôn](../../strongs/h/h1995.md) of the [gal](../../strongs/h/h1530.md) thereof.

<a name="jeremiah_51_43"></a>Jeremiah 51:43

Her [ʿîr](../../strongs/h/h5892.md) are a [šammâ](../../strongs/h/h8047.md), a [ṣîyâ](../../strongs/h/h6723.md) ['erets](../../strongs/h/h776.md), and an ['arabah](../../strongs/h/h6160.md), an ['erets](../../strongs/h/h776.md) wherein no ['iysh](../../strongs/h/h376.md) [yashab](../../strongs/h/h3427.md), neither doth any [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) ['abar](../../strongs/h/h5674.md) [hēn](../../strongs/h/h2004.md).

<a name="jeremiah_51_44"></a>Jeremiah 51:44

And I will [paqad](../../strongs/h/h6485.md) [bēl](../../strongs/h/h1078.md) in [Bāḇel](../../strongs/h/h894.md), and I will [yāṣā'](../../strongs/h/h3318.md) out of his [peh](../../strongs/h/h6310.md) that which he hath [belaʿ](../../strongs/h/h1105.md): and the [gowy](../../strongs/h/h1471.md) shall not [nāhar](../../strongs/h/h5102.md) any more unto him: yea, the [ḥômâ](../../strongs/h/h2346.md) of [Bāḇel](../../strongs/h/h894.md) shall [naphal](../../strongs/h/h5307.md).

<a name="jeremiah_51_45"></a>Jeremiah 51:45

My ['am](../../strongs/h/h5971.md), [yāṣā'](../../strongs/h/h3318.md) ye of the [tavek](../../strongs/h/h8432.md) of her, and [mālaṭ](../../strongs/h/h4422.md) ye every ['iysh](../../strongs/h/h376.md) his [nephesh](../../strongs/h/h5315.md) from the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_46"></a>Jeremiah 51:46

And lest your [lebab](../../strongs/h/h3824.md) [rāḵaḵ](../../strongs/h/h7401.md), and ye [yare'](../../strongs/h/h3372.md) for the [šᵊmûʿâ](../../strongs/h/h8052.md) that shall be [shama'](../../strongs/h/h8085.md) in the ['erets](../../strongs/h/h776.md); a [šᵊmûʿâ](../../strongs/h/h8052.md) shall both [bow'](../../strongs/h/h935.md) one [šānâ](../../strongs/h/h8141.md), and ['aḥar](../../strongs/h/h310.md) that in another [šānâ](../../strongs/h/h8141.md) shall come a [šᵊmûʿâ](../../strongs/h/h8052.md), and [chamac](../../strongs/h/h2555.md) in the ['erets](../../strongs/h/h776.md), [mashal](../../strongs/h/h4910.md) against [mashal](../../strongs/h/h4910.md).

<a name="jeremiah_51_47"></a>Jeremiah 51:47

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), that I will do [paqad](../../strongs/h/h6485.md) upon the [pāsîl](../../strongs/h/h6456.md) of [Bāḇel](../../strongs/h/h894.md): and her ['erets](../../strongs/h/h776.md) shall be [buwsh](../../strongs/h/h954.md), and all her [ḥālāl](../../strongs/h/h2491.md) shall [naphal](../../strongs/h/h5307.md) in the [tavek](../../strongs/h/h8432.md) of her.

<a name="jeremiah_51_48"></a>Jeremiah 51:48

Then the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md), and all that is therein, shall [ranan](../../strongs/h/h7442.md) for [Bāḇel](../../strongs/h/h894.md): for the [shadad](../../strongs/h/h7703.md) shall [bow'](../../strongs/h/h935.md) unto her from the [ṣāp̄ôn](../../strongs/h/h6828.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_49"></a>Jeremiah 51:49

[gam](../../strongs/h/h1571.md) [Bāḇel](../../strongs/h/h894.md) hath caused the [ḥālāl](../../strongs/h/h2491.md) of [Yisra'el](../../strongs/h/h3478.md) to [naphal](../../strongs/h/h5307.md), so at [Bāḇel](../../strongs/h/h894.md) shall [naphal](../../strongs/h/h5307.md) the [ḥālāl](../../strongs/h/h2491.md) of all the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_51_50"></a>Jeremiah 51:50

Ye that have [pallēṭ](../../strongs/h/h6405.md) the [chereb](../../strongs/h/h2719.md), go [halak](../../strongs/h/h1980.md), stand not ['amad](../../strongs/h/h5975.md): [zakar](../../strongs/h/h2142.md) [Yĕhovah](../../strongs/h/h3068.md) afar [rachowq](../../strongs/h/h7350.md), and let [Yĕruwshalaim](../../strongs/h/h3389.md) [ʿālâ](../../strongs/h/h5927.md) into your [lebab](../../strongs/h/h3824.md).

<a name="jeremiah_51_51"></a>Jeremiah 51:51

We are [buwsh](../../strongs/h/h954.md), because we have [shama'](../../strongs/h/h8085.md) [cherpah](../../strongs/h/h2781.md): [kĕlimmah](../../strongs/h/h3639.md) hath [kāsâ](../../strongs/h/h3680.md) our [paniym](../../strongs/h/h6440.md): for [zûr](../../strongs/h/h2114.md) are [bow'](../../strongs/h/h935.md) into the [miqdash](../../strongs/h/h4720.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md).

<a name="jeremiah_51_52"></a>Jeremiah 51:52

Wherefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will do [paqad](../../strongs/h/h6485.md) upon her [pāsîl](../../strongs/h/h6456.md): and through all her ['erets](../../strongs/h/h776.md) the [ḥālāl](../../strongs/h/h2491.md) shall ['ānaq](../../strongs/h/h602.md).

<a name="jeremiah_51_53"></a>Jeremiah 51:53

Though [Bāḇel](../../strongs/h/h894.md) should [ʿālâ](../../strongs/h/h5927.md) to [shamayim](../../strongs/h/h8064.md), and though she should [bāṣar](../../strongs/h/h1219.md) the [marowm](../../strongs/h/h4791.md) of her ['oz](../../strongs/h/h5797.md), ['āz](../../strongs/h/h227.md) from me shall [shadad](../../strongs/h/h7703.md) [bow'](../../strongs/h/h935.md) unto her, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_51_54"></a>Jeremiah 51:54

A [qowl](../../strongs/h/h6963.md) of a [zaʿaq](../../strongs/h/h2201.md) cometh from [Bāḇel](../../strongs/h/h894.md), and [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md) from the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md):

<a name="jeremiah_51_55"></a>Jeremiah 51:55

Because [Yĕhovah](../../strongs/h/h3068.md) hath [shadad](../../strongs/h/h7703.md) [Bāḇel](../../strongs/h/h894.md), and ['abad](../../strongs/h/h6.md) out of her the [gadowl](../../strongs/h/h1419.md) [qowl](../../strongs/h/h6963.md); when her [gal](../../strongs/h/h1530.md) do [hāmâ](../../strongs/h/h1993.md) like [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md), a [shā'ôn](../../strongs/h/h7588.md) of their [qowl](../../strongs/h/h6963.md) is [nathan](../../strongs/h/h5414.md):

<a name="jeremiah_51_56"></a>Jeremiah 51:56

Because the [shadad](../../strongs/h/h7703.md) is [bow'](../../strongs/h/h935.md) upon her, even upon [Bāḇel](../../strongs/h/h894.md), and her [gibôr](../../strongs/h/h1368.md) are [lāḵaḏ](../../strongs/h/h3920.md), every one of their [qesheth](../../strongs/h/h7198.md) is [ḥāṯaṯ](../../strongs/h/h2865.md): for [Yĕhovah](../../strongs/h/h3068.md) ['el](../../strongs/h/h410.md) of [gᵊmûlâ](../../strongs/h/h1578.md) shall [shalam](../../strongs/h/h7999.md) [shalam](../../strongs/h/h7999.md).

<a name="jeremiah_51_57"></a>Jeremiah 51:57

And I will make [šāḵar](../../strongs/h/h7937.md) her [śar](../../strongs/h/h8269.md), and her [ḥāḵām](../../strongs/h/h2450.md) men, her [peḥâ](../../strongs/h/h6346.md), and her [sāḡān](../../strongs/h/h5461.md), and her [gibôr](../../strongs/h/h1368.md): and they shall [yashen](../../strongs/h/h3462.md) a ['owlam](../../strongs/h/h5769.md) [šēnā'](../../strongs/h/h8142.md), and not [quwts](../../strongs/h/h6974.md), [nᵊ'um](../../strongs/h/h5002.md) the [melek](../../strongs/h/h4428.md), whose [shem](../../strongs/h/h8034.md) is [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_51_58"></a>Jeremiah 51:58

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); The [rāḥāḇ](../../strongs/h/h7342.md) [ḥômâ](../../strongs/h/h2346.md) of [Bāḇel](../../strongs/h/h894.md) shall be [ʿārar](../../strongs/h/h6209.md) [ʿārar](../../strongs/h/h6209.md), and her [gāḇōha](../../strongs/h/h1364.md) [sha'ar](../../strongs/h/h8179.md) shall be [yāṣaṯ](../../strongs/h/h3341.md) with ['esh](../../strongs/h/h784.md); and the ['am](../../strongs/h/h5971.md) shall [yaga'](../../strongs/h/h3021.md) in [riyq](../../strongs/h/h7385.md), and the [lĕom](../../strongs/h/h3816.md) [day](../../strongs/h/h1767.md) the ['esh](../../strongs/h/h784.md), and they shall be [yāʿap̄](../../strongs/h/h3286.md).

<a name="jeremiah_51_59"></a>Jeremiah 51:59

The [dabar](../../strongs/h/h1697.md) which [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [tsavah](../../strongs/h/h6680.md) [Śᵊrāyâ](../../strongs/h/h8304.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md), the [ben](../../strongs/h/h1121.md) of [Maḥsêâ](../../strongs/h/h4271.md), when he [yālaḵ](../../strongs/h/h3212.md) with [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) into [Bāḇel](../../strongs/h/h894.md) in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md). And this [Śᵊrāyâ](../../strongs/h/h8304.md) was a [mᵊnûḥâ](../../strongs/h/h4496.md) [śar](../../strongs/h/h8269.md).

<a name="jeremiah_51_60"></a>Jeremiah 51:60

So [Yirmᵊyâ](../../strongs/h/h3414.md) [kāṯaḇ](../../strongs/h/h3789.md) in ['echad](../../strongs/h/h259.md) [sēp̄er](../../strongs/h/h5612.md) all the [ra'](../../strongs/h/h7451.md) that should [bow'](../../strongs/h/h935.md) upon [Bāḇel](../../strongs/h/h894.md), even all these [dabar](../../strongs/h/h1697.md) that are [kāṯaḇ](../../strongs/h/h3789.md) against [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_51_61"></a>Jeremiah 51:61

And [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) to [Śᵊrāyâ](../../strongs/h/h8304.md), When thou [bow'](../../strongs/h/h935.md) to [Bāḇel](../../strongs/h/h894.md), and shalt [ra'ah](../../strongs/h/h7200.md), and shalt [qara'](../../strongs/h/h7121.md) all these [dabar](../../strongs/h/h1697.md);

<a name="jeremiah_51_62"></a>Jeremiah 51:62

Then shalt thou ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), thou hast [dabar](../../strongs/h/h1696.md) against this [maqowm](../../strongs/h/h4725.md), to [karath](../../strongs/h/h3772.md) it, that none shall [yashab](../../strongs/h/h3427.md) in it, neither ['āḏām](../../strongs/h/h120.md) nor [bĕhemah](../../strongs/h/h929.md), but that it shall be [šᵊmāmâ](../../strongs/h/h8077.md) ['owlam](../../strongs/h/h5769.md).

<a name="jeremiah_51_63"></a>Jeremiah 51:63

And it shall be, when thou hast made a [kalah](../../strongs/h/h3615.md) of [qara'](../../strongs/h/h7121.md) this [sēp̄er](../../strongs/h/h5612.md), that thou shalt [qāšar](../../strongs/h/h7194.md) an ['eben](../../strongs/h/h68.md) to it, and [shalak](../../strongs/h/h7993.md) it into the [tavek](../../strongs/h/h8432.md) of [Pᵊrāṯ](../../strongs/h/h6578.md):

<a name="jeremiah_51_64"></a>Jeremiah 51:64

And thou shalt ['āmar](../../strongs/h/h559.md), Thus shall [Bāḇel](../../strongs/h/h894.md) [šāqaʿ](../../strongs/h/h8257.md), and shall not [quwm](../../strongs/h/h6965.md) [paniym](../../strongs/h/h6440.md) the [ra'](../../strongs/h/h7451.md) that I will [bow'](../../strongs/h/h935.md) upon her: and they shall be [yāʿap̄](../../strongs/h/h3286.md). Thus far are the [dabar](../../strongs/h/h1697.md) of [Yirmᵊyâ](../../strongs/h/h3414.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 50](jeremiah_50.md) - [Jeremiah 52](jeremiah_52.md)