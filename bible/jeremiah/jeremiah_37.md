# [Jeremiah 37](https://www.blueletterbible.org/kjv/jeremiah/37)

<a name="jeremiah_37_1"></a>Jeremiah 37:1

And [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [mālaḵ](../../strongs/h/h4427.md) instead of [Kānyâû](../../strongs/h/h3659.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md), whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) made [mālaḵ](../../strongs/h/h4427.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_37_2"></a>Jeremiah 37:2

But neither he, nor his ['ebed](../../strongs/h/h5650.md), nor the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), did [shama'](../../strongs/h/h8085.md) unto the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he [dabar](../../strongs/h/h1696.md) [yad](../../strongs/h/h3027.md) the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md).

<a name="jeremiah_37_3"></a>Jeremiah 37:3

And [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) [Yᵊhûḵal](../../strongs/h/h3081.md) the [ben](../../strongs/h/h1121.md) of [Šelemyâ](../../strongs/h/h8018.md) and [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md) the [kōhēn](../../strongs/h/h3548.md) to the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md), [palal](../../strongs/h/h6419.md) now unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) for us.

<a name="jeremiah_37_4"></a>Jeremiah 37:4

Now [Yirmᵊyâ](../../strongs/h/h3414.md) [bow'](../../strongs/h/h935.md) and [yāṣā'](../../strongs/h/h3318.md) [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md): for they had not [nathan](../../strongs/h/h5414.md) him into [bayith](../../strongs/h/h1004.md) H3628.

<a name="jeremiah_37_5"></a>Jeremiah 37:5

Then [Parʿô](../../strongs/h/h6547.md) [ḥayil](../../strongs/h/h2428.md) was [yāṣā'](../../strongs/h/h3318.md) out of [Mitsrayim](../../strongs/h/h4714.md): and when the [Kaśdîmâ](../../strongs/h/h3778.md) that [ṣûr](../../strongs/h/h6696.md) [Yĕruwshalaim](../../strongs/h/h3389.md) [shama'](../../strongs/h/h8085.md) [šēmaʿ](../../strongs/h/h8088.md) of them, they [ʿālâ](../../strongs/h/h5927.md) from [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_37_6"></a>Jeremiah 37:6

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto the [nāḇî'](../../strongs/h/h5030.md) [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_37_7"></a>Jeremiah 37:7

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Thus shall ye ['āmar](../../strongs/h/h559.md) to the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), that [shalach](../../strongs/h/h7971.md) you unto me to [darash](../../strongs/h/h1875.md) of me; Behold, [Parʿô](../../strongs/h/h6547.md) [ḥayil](../../strongs/h/h2428.md), which is [yāṣā'](../../strongs/h/h3318.md) to [ʿezrâ](../../strongs/h/h5833.md) you, shall [shuwb](../../strongs/h/h7725.md) to [Mitsrayim](../../strongs/h/h4714.md) into their own ['erets](../../strongs/h/h776.md).

<a name="jeremiah_37_8"></a>Jeremiah 37:8

And the [Kaśdîmâ](../../strongs/h/h3778.md) shall [shuwb](../../strongs/h/h7725.md), and [lāḥam](../../strongs/h/h3898.md) against this [ʿîr](../../strongs/h/h5892.md), and [lāḵaḏ](../../strongs/h/h3920.md) it, and [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md).

<a name="jeremiah_37_9"></a>Jeremiah 37:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [nasha'](../../strongs/h/h5377.md) not [nephesh](../../strongs/h/h5315.md), ['āmar](../../strongs/h/h559.md), The [Kaśdîmâ](../../strongs/h/h3778.md) shall [halak](../../strongs/h/h1980.md) [yālaḵ](../../strongs/h/h3212.md) from us: for they shall not [yālaḵ](../../strongs/h/h3212.md).

<a name="jeremiah_37_10"></a>Jeremiah 37:10

For though ye had [nakah](../../strongs/h/h5221.md) the whole [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) that [lāḥam](../../strongs/h/h3898.md) against you, and there [šā'ar](../../strongs/h/h7604.md) but [dāqar](../../strongs/h/h1856.md) ['enowsh](../../strongs/h/h582.md) among them, yet should they [quwm](../../strongs/h/h6965.md) every ['iysh](../../strongs/h/h376.md) in his ['ohel](../../strongs/h/h168.md), and [śārap̄](../../strongs/h/h8313.md) this [ʿîr](../../strongs/h/h5892.md) with ['esh](../../strongs/h/h784.md).

<a name="jeremiah_37_11"></a>Jeremiah 37:11

And it came to pass, that when the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) was [ʿālâ](../../strongs/h/h5927.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) for [paniym](../../strongs/h/h6440.md) of [Parʿô](../../strongs/h/h6547.md) [ḥayil](../../strongs/h/h2428.md),

<a name="jeremiah_37_12"></a>Jeremiah 37:12

Then [Yirmᵊyâ](../../strongs/h/h3414.md) [yāṣā'](../../strongs/h/h3318.md) out of [Yĕruwshalaim](../../strongs/h/h3389.md) to [yālaḵ](../../strongs/h/h3212.md) into the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md), to [chalaq](../../strongs/h/h2505.md) himself thence in the [tavek](../../strongs/h/h8432.md) of the ['am](../../strongs/h/h5971.md).

<a name="jeremiah_37_13"></a>Jeremiah 37:13

And when he was in the [sha'ar](../../strongs/h/h8179.md) of [Binyāmîn](../../strongs/h/h1144.md), a [baʿal](../../strongs/h/h1167.md) of the [pᵊqiḏuṯ](../../strongs/h/h6488.md) was there, whose [shem](../../strongs/h/h8034.md) was [Yir'Îyāyh](../../strongs/h/h3376.md), the [ben](../../strongs/h/h1121.md) of [Šelemyâ](../../strongs/h/h8018.md), the [ben](../../strongs/h/h1121.md) of [Ḥănanyâ](../../strongs/h/h2608.md); and he [tāp̄aś](../../strongs/h/h8610.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), ['āmar](../../strongs/h/h559.md), Thou fallest [naphal](../../strongs/h/h5307.md) to the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="jeremiah_37_14"></a>Jeremiah 37:14

Then ['āmar](../../strongs/h/h559.md) [Yirmᵊyâ](../../strongs/h/h3414.md), It is [sheqer](../../strongs/h/h8267.md); I fall not [naphal](../../strongs/h/h5307.md) to the [Kaśdîmâ](../../strongs/h/h3778.md). But he [shama'](../../strongs/h/h8085.md) not to him: so [Yir'Îyāyh](../../strongs/h/h3376.md) [tāp̄aś](../../strongs/h/h8610.md) [Yirmᵊyâ](../../strongs/h/h3414.md), and [bow'](../../strongs/h/h935.md) him to the [śar](../../strongs/h/h8269.md).

<a name="jeremiah_37_15"></a>Jeremiah 37:15

Wherefore the [śar](../../strongs/h/h8269.md) were [qāṣap̄](../../strongs/h/h7107.md) with [Yirmᵊyâ](../../strongs/h/h3414.md), and [nakah](../../strongs/h/h5221.md) him, and [nathan](../../strongs/h/h5414.md) him in ['ēsûr](../../strongs/h/h612.md) in the [bayith](../../strongs/h/h1004.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) the [sāp̄ar](../../strongs/h/h5608.md): for they had ['asah](../../strongs/h/h6213.md) that the [kele'](../../strongs/h/h3608.md).

<a name="jeremiah_37_16"></a>Jeremiah 37:16

When [Yirmᵊyâ](../../strongs/h/h3414.md) was [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) [bowr](../../strongs/h/h953.md), and into the [ḥānûṯ](../../strongs/h/h2588.md), and [Yirmᵊyâ](../../strongs/h/h3414.md) had [yashab](../../strongs/h/h3427.md) there [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md);

<a name="jeremiah_37_17"></a>Jeremiah 37:17

Then [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md), and took him [laqach](../../strongs/h/h3947.md): and the [melek](../../strongs/h/h4428.md) [sha'al](../../strongs/h/h7592.md) him [cether](../../strongs/h/h5643.md) in his [bayith](../../strongs/h/h1004.md), and ['āmar](../../strongs/h/h559.md), Is there any [dabar](../../strongs/h/h1697.md) from [Yĕhovah](../../strongs/h/h3068.md)? And [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md), There [yēš](../../strongs/h/h3426.md): for, ['āmar](../../strongs/h/h559.md) he, thou shalt be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_37_18"></a>Jeremiah 37:18

Moreover [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md), What have I [chata'](../../strongs/h/h2398.md) against thee, or against thy ['ebed](../../strongs/h/h5650.md), or against this ['am](../../strongs/h/h5971.md), that ye have [nathan](../../strongs/h/h5414.md) me in [bayith](../../strongs/h/h1004.md) [kele'](../../strongs/h/h3608.md)?

<a name="jeremiah_37_19"></a>Jeremiah 37:19

Where are ['ayyê](../../strongs/h/h346.md) your [nāḇî'](../../strongs/h/h5030.md) which [nāḇā'](../../strongs/h/h5012.md) unto you, ['āmar](../../strongs/h/h559.md), The [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) shall not [bow'](../../strongs/h/h935.md) against you, nor against this ['erets](../../strongs/h/h776.md)?

<a name="jeremiah_37_20"></a>Jeremiah 37:20

Therefore [shama'](../../strongs/h/h8085.md) now, I pray thee, O my ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md): let my [tĕchinnah](../../strongs/h/h8467.md), I pray thee, be [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) thee; that thou cause me not to [shuwb](../../strongs/h/h7725.md) to the [bayith](../../strongs/h/h1004.md) of [Yᵊhônāṯān](../../strongs/h/h3083.md) the [sāp̄ar](../../strongs/h/h5608.md), lest I [muwth](../../strongs/h/h4191.md) there.

<a name="jeremiah_37_21"></a>Jeremiah 37:21

Then [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) that they should [paqad](../../strongs/h/h6485.md) [Yirmᵊyâ](../../strongs/h/h3414.md) into the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md), and that they should [nathan](../../strongs/h/h5414.md) him [yowm](../../strongs/h/h3117.md) a [kikār](../../strongs/h/h3603.md) of [lechem](../../strongs/h/h3899.md) out of the ['āp̄â](../../strongs/h/h644.md) [ḥûṣ](../../strongs/h/h2351.md), until all the [lechem](../../strongs/h/h3899.md) in the [ʿîr](../../strongs/h/h5892.md) were [tamam](../../strongs/h/h8552.md). Thus [Yirmᵊyâ](../../strongs/h/h3414.md) [yashab](../../strongs/h/h3427.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 36](jeremiah_36.md) - [Jeremiah 38](jeremiah_38.md)