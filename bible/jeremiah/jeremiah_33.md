# [Jeremiah 33](https://www.blueletterbible.org/kjv/jeremiah/33)

<a name="jeremiah_33_1"></a>Jeremiah 33:1

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md) the [šēnî](../../strongs/h/h8145.md) time, while he was yet [ʿāṣar](../../strongs/h/h6113.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_33_2"></a>Jeremiah 33:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) the ['asah](../../strongs/h/h6213.md) thereof, [Yĕhovah](../../strongs/h/h3068.md) that [yāṣar](../../strongs/h/h3335.md) it, to [kuwn](../../strongs/h/h3559.md) it; [Yĕhovah](../../strongs/h/h3068.md) is his [shem](../../strongs/h/h8034.md);

<a name="jeremiah_33_3"></a>Jeremiah 33:3

[qara'](../../strongs/h/h7121.md) unto me, and I will ['anah](../../strongs/h/h6030.md) thee, and [nāḡaḏ](../../strongs/h/h5046.md) thee [gadowl](../../strongs/h/h1419.md) and [bāṣar](../../strongs/h/h1219.md), which thou [yada'](../../strongs/h/h3045.md) not.

<a name="jeremiah_33_4"></a>Jeremiah 33:4

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), concerning the [bayith](../../strongs/h/h1004.md) of this [ʿîr](../../strongs/h/h5892.md), and concerning the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), which are [nāṯaṣ](../../strongs/h/h5422.md) by the [sōllâ](../../strongs/h/h5550.md), and by the [chereb](../../strongs/h/h2719.md);

<a name="jeremiah_33_5"></a>Jeremiah 33:5

They [bow'](../../strongs/h/h935.md) to [lāḥam](../../strongs/h/h3898.md) with the [Kaśdîmâ](../../strongs/h/h3778.md), but it is to [mālā'](../../strongs/h/h4390.md) them with the [peḡer](../../strongs/h/h6297.md) of ['āḏām](../../strongs/h/h120.md), whom I have [nakah](../../strongs/h/h5221.md) in mine ['aph](../../strongs/h/h639.md) and in my [chemah](../../strongs/h/h2534.md), and for all whose [ra'](../../strongs/h/h7451.md) I have [cathar](../../strongs/h/h5641.md) my [paniym](../../strongs/h/h6440.md) from this [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_33_6"></a>Jeremiah 33:6

Behold, I will [ʿālâ](../../strongs/h/h5927.md) it ['ărûḵâ](../../strongs/h/h724.md) and [marpē'](../../strongs/h/h4832.md), and I will [rapha'](../../strongs/h/h7495.md) them, and will [gālâ](../../strongs/h/h1540.md) unto them the [ʿăṯereṯ](../../strongs/h/h6283.md) of [shalowm](../../strongs/h/h7965.md) and ['emeth](../../strongs/h/h571.md).

<a name="jeremiah_33_7"></a>Jeremiah 33:7

And I will cause the [shebuwth](../../strongs/h/h7622.md) of [Yehuwdah](../../strongs/h/h3063.md) and the [shebuwth](../../strongs/h/h7622.md) of [Yisra'el](../../strongs/h/h3478.md) to [shuwb](../../strongs/h/h7725.md), and will [bānâ](../../strongs/h/h1129.md) them, as at the [ri'šôn](../../strongs/h/h7223.md).

<a name="jeremiah_33_8"></a>Jeremiah 33:8

And I will [ṭāhēr](../../strongs/h/h2891.md) them from all their ['avon](../../strongs/h/h5771.md), whereby they have [chata'](../../strongs/h/h2398.md) against me; and I will [sālaḥ](../../strongs/h/h5545.md) all their ['avon](../../strongs/h/h5771.md), whereby they have [chata'](../../strongs/h/h2398.md), and whereby they have [pāšaʿ](../../strongs/h/h6586.md) against me.

<a name="jeremiah_33_9"></a>Jeremiah 33:9

And it shall be to me a [shem](../../strongs/h/h8034.md) of [śāśôn](../../strongs/h/h8342.md), a [tehillah](../../strongs/h/h8416.md) and a [tip̄'ārâ](../../strongs/h/h8597.md) before all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md), which shall [shama'](../../strongs/h/h8085.md) all the [towb](../../strongs/h/h2896.md) that I ['asah](../../strongs/h/h6213.md) unto them: and they shall [pachad](../../strongs/h/h6342.md) and [ragaz](../../strongs/h/h7264.md) for all the [towb](../../strongs/h/h2896.md) and for all the [shalowm](../../strongs/h/h7965.md) that I ['asah](../../strongs/h/h6213.md) unto it.

<a name="jeremiah_33_10"></a>Jeremiah 33:10

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Again there shall be [shama'](../../strongs/h/h8085.md) in this [maqowm](../../strongs/h/h4725.md), which ye ['āmar](../../strongs/h/h559.md) shall be [ḥārēḇ](../../strongs/h/h2720.md) without ['āḏām](../../strongs/h/h120.md) and without [bĕhemah](../../strongs/h/h929.md), even in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that are [šāmēm](../../strongs/h/h8074.md), without ['āḏām](../../strongs/h/h120.md), and without [yashab](../../strongs/h/h3427.md), and without [bĕhemah](../../strongs/h/h929.md),

<a name="jeremiah_33_11"></a>Jeremiah 33:11

The [qowl](../../strongs/h/h6963.md) of [śāśôn](../../strongs/h/h8342.md), and the [qowl](../../strongs/h/h6963.md) of [simchah](../../strongs/h/h8057.md), the [qowl](../../strongs/h/h6963.md) of the [ḥāṯān](../../strongs/h/h2860.md), and the [qowl](../../strongs/h/h6963.md) of the [kallâ](../../strongs/h/h3618.md), the [qowl](../../strongs/h/h6963.md) of them that shall ['āmar](../../strongs/h/h559.md), [yadah](../../strongs/h/h3034.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md): for [Yĕhovah](../../strongs/h/h3068.md) is [towb](../../strongs/h/h2896.md); for his [checed](../../strongs/h/h2617.md) endureth ['owlam](../../strongs/h/h5769.md): and of them that shall [bow'](../../strongs/h/h935.md) the sacrifice of [tôḏâ](../../strongs/h/h8426.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md). For I will cause to [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of the ['erets](../../strongs/h/h776.md), as at the [ri'šôn](../../strongs/h/h7223.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_33_12"></a>Jeremiah 33:12

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Again in this [maqowm](../../strongs/h/h4725.md), which is [ḥārēḇ](../../strongs/h/h2720.md) without ['āḏām](../../strongs/h/h120.md) and without [bĕhemah](../../strongs/h/h929.md), and in all the [ʿîr](../../strongs/h/h5892.md) thereof, shall be a [nāvê](../../strongs/h/h5116.md) of [ra'ah](../../strongs/h/h7462.md) causing their [tso'n](../../strongs/h/h6629.md) to lie [rāḇaṣ](../../strongs/h/h7257.md).

<a name="jeremiah_33_13"></a>Jeremiah 33:13

In the [ʿîr](../../strongs/h/h5892.md) of the [har](../../strongs/h/h2022.md), in the [ʿîr](../../strongs/h/h5892.md) of the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [neḡeḇ](../../strongs/h/h5045.md), and in the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md), and in the [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md), and in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), shall the [tso'n](../../strongs/h/h6629.md) ['abar](../../strongs/h/h5674.md) under the [yad](../../strongs/h/h3027.md) of him that [mānâ](../../strongs/h/h4487.md) them, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_33_14"></a>Jeremiah 33:14

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [quwm](../../strongs/h/h6965.md) that [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) which I have [dabar](../../strongs/h/h1696.md) unto the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and to the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_33_15"></a>Jeremiah 33:15

In those [yowm](../../strongs/h/h3117.md), and at that [ʿēṯ](../../strongs/h/h6256.md), will I cause the [ṣemaḥ](../../strongs/h/h6780.md) of [tsedaqah](../../strongs/h/h6666.md) to grow [ṣāmaḥ](../../strongs/h/h6779.md) unto [Dāviḏ](../../strongs/h/h1732.md); and he shall ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md) in the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_33_16"></a>Jeremiah 33:16

In those [yowm](../../strongs/h/h3117.md) shall [Yehuwdah](../../strongs/h/h3063.md) be [yasha'](../../strongs/h/h3467.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) shall [shakan](../../strongs/h/h7931.md) [betach](../../strongs/h/h983.md): and this is the name wherewith she shall be [qara'](../../strongs/h/h7121.md), [Yᵊhōvâ Ṣiḏqēnû](../../strongs/h/h3072.md).

<a name="jeremiah_33_17"></a>Jeremiah 33:17

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [Dāviḏ](../../strongs/h/h1732.md) shall [lō'](../../strongs/h/h3808.md) [karath](../../strongs/h/h3772.md) an ['iysh](../../strongs/h/h376.md) to [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md);

<a name="jeremiah_33_18"></a>Jeremiah 33:18

Neither shall the [kōhēn](../../strongs/h/h3548.md) the [Lᵊvî](../../strongs/h/h3881.md) [karath](../../strongs/h/h3772.md) an ['iysh](../../strongs/h/h376.md) [paniym](../../strongs/h/h6440.md) me to [ʿālâ](../../strongs/h/h5927.md) [ʿōlâ](../../strongs/h/h5930.md), and to [qāṭar](../../strongs/h/h6999.md) meat [minchah](../../strongs/h/h4503.md), and to ['asah](../../strongs/h/h6213.md) [zebach](../../strongs/h/h2077.md) [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_33_19"></a>Jeremiah 33:19

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_33_20"></a>Jeremiah 33:20

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); If ye can [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) of the [yowm](../../strongs/h/h3117.md), and my [bĕriyth](../../strongs/h/h1285.md) of the [layil](../../strongs/h/h3915.md), and that there should not be [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md) in their [ʿēṯ](../../strongs/h/h6256.md);

<a name="jeremiah_33_21"></a>Jeremiah 33:21

Then may also my [bĕriyth](../../strongs/h/h1285.md) be [pārar](../../strongs/h/h6565.md) with [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md), that he should not have a [ben](../../strongs/h/h1121.md) to [mālaḵ](../../strongs/h/h4427.md) upon his [kicce'](../../strongs/h/h3678.md); and with the [Lᵊvî](../../strongs/h/h3881.md) the [kōhēn](../../strongs/h/h3548.md), my [sharath](../../strongs/h/h8334.md).

<a name="jeremiah_33_22"></a>Jeremiah 33:22

As the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md) cannot be [sāp̄ar](../../strongs/h/h5608.md), neither the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md) [māḏaḏ](../../strongs/h/h4058.md): so will I [rabah](../../strongs/h/h7235.md) the [zera'](../../strongs/h/h2233.md) of [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md), and the [Lᵊvî](../../strongs/h/h3881.md) that [sharath](../../strongs/h/h8334.md) unto me.

<a name="jeremiah_33_23"></a>Jeremiah 33:23

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_33_24"></a>Jeremiah 33:24

[ra'ah](../../strongs/h/h7200.md) thou not what this ['am](../../strongs/h/h5971.md) have [dabar](../../strongs/h/h1696.md), ['āmar](../../strongs/h/h559.md), The [šᵊnayim](../../strongs/h/h8147.md) [mišpāḥâ](../../strongs/h/h4940.md) which [Yĕhovah](../../strongs/h/h3068.md) hath [bāḥar](../../strongs/h/h977.md), he hath even [mā'as](../../strongs/h/h3988.md) them? thus they have [na'ats](../../strongs/h/h5006.md) my ['am](../../strongs/h/h5971.md), that they should be no more a [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) them.

<a name="jeremiah_33_25"></a>Jeremiah 33:25

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); If my [bĕriyth](../../strongs/h/h1285.md) be not with [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md), and if I have not [śûm](../../strongs/h/h7760.md) the [chuqqah](../../strongs/h/h2708.md) of [shamayim](../../strongs/h/h8064.md) and ['erets](../../strongs/h/h776.md);

<a name="jeremiah_33_26"></a>Jeremiah 33:26

[gam](../../strongs/h/h1571.md) will I [mā'as](../../strongs/h/h3988.md) the [zera'](../../strongs/h/h2233.md) of [Ya'aqob](../../strongs/h/h3290.md), and [Dāviḏ](../../strongs/h/h1732.md) my ['ebed](../../strongs/h/h5650.md), so that I will not [laqach](../../strongs/h/h3947.md) any of his [zera'](../../strongs/h/h2233.md) to be [mashal](../../strongs/h/h4910.md) over the [zera'](../../strongs/h/h2233.md) of ['Abraham](../../strongs/h/h85.md), [Yišḥāq](../../strongs/h/h3446.md), and [Ya'aqob](../../strongs/h/h3290.md): for I will cause their [shebuwth](../../strongs/h/h7622.md) to [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md), and have [racham](../../strongs/h/h7355.md) on them.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 32](jeremiah_32.md) - [Jeremiah 34](jeremiah_34.md)