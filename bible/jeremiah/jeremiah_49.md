# [Jeremiah 49](https://www.blueletterbible.org/kjv/jeremiah/49)

<a name="jeremiah_49_1"></a>Jeremiah 49:1

Concerning the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Hath [Yisra'el](../../strongs/h/h3478.md) no [ben](../../strongs/h/h1121.md)? hath he no [yarash](../../strongs/h/h3423.md)? why then doth their [melek](../../strongs/h/h4428.md) [yarash](../../strongs/h/h3423.md) [Gāḏ](../../strongs/h/h1410.md), and his ['am](../../strongs/h/h5971.md) [yashab](../../strongs/h/h3427.md) in his [ʿîr](../../strongs/h/h5892.md)?

<a name="jeremiah_49_2"></a>Jeremiah 49:2

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), that I will cause a [tᵊrûʿâ](../../strongs/h/h8643.md) of [milḥāmâ](../../strongs/h/h4421.md) to be [shama'](../../strongs/h/h8085.md) in [Rabâ](../../strongs/h/h7237.md) of the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md); and it shall be a [šᵊmāmâ](../../strongs/h/h8077.md) [tēl](../../strongs/h/h8510.md), and her [bath](../../strongs/h/h1323.md) shall be [yāṣaṯ](../../strongs/h/h3341.md) with ['esh](../../strongs/h/h784.md): then shall [Yisra'el](../../strongs/h/h3478.md) be [yarash](../../strongs/h/h3423.md) unto them that were his [yarash](../../strongs/h/h3423.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_49_3"></a>Jeremiah 49:3

[yālal](../../strongs/h/h3213.md), O [Hešbôn](../../strongs/h/h2809.md), for [ʿAy](../../strongs/h/h5857.md) is [shadad](../../strongs/h/h7703.md): [ṣāʿaq](../../strongs/h/h6817.md), ye [bath](../../strongs/h/h1323.md) of [Rabâ](../../strongs/h/h7237.md), [ḥāḡar](../../strongs/h/h2296.md) you with [śaq](../../strongs/h/h8242.md); [sāp̄aḏ](../../strongs/h/h5594.md), and [šûṭ](../../strongs/h/h7751.md) by the [gᵊḏērâ](../../strongs/h/h1448.md); for their [melek](../../strongs/h/h4428.md) shall [yālaḵ](../../strongs/h/h3212.md) into [gôlâ](../../strongs/h/h1473.md), and his [kōhēn](../../strongs/h/h3548.md) and his [śar](../../strongs/h/h8269.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="jeremiah_49_4"></a>Jeremiah 49:4

Wherefore [halal](../../strongs/h/h1984.md) thou in the [ʿēmeq](../../strongs/h/h6010.md), thy [zûḇ](../../strongs/h/h2100.md) [ʿēmeq](../../strongs/h/h6010.md), [šôḇēḇ](../../strongs/h/h7728.md) [bath](../../strongs/h/h1323.md)? that [batach](../../strongs/h/h982.md) in her ['ôṣār](../../strongs/h/h214.md), saying, Who shall [bow'](../../strongs/h/h935.md) unto me?

<a name="jeremiah_49_5"></a>Jeremiah 49:5

Behold, I will [bow'](../../strongs/h/h935.md) a [paḥaḏ](../../strongs/h/h6343.md) upon thee, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md), from all those that be [cabiyb](../../strongs/h/h5439.md) thee; and ye shall be [nāḏaḥ](../../strongs/h/h5080.md) every ['iysh](../../strongs/h/h376.md) right [paniym](../../strongs/h/h6440.md); and none shall [qāḇaṣ](../../strongs/h/h6908.md) him that [nāḏaḏ](../../strongs/h/h5074.md).

<a name="jeremiah_49_6"></a>Jeremiah 49:6

And ['aḥar](../../strongs/h/h310.md) I will [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_49_7"></a>Jeremiah 49:7

Concerning ['Ĕḏōm](../../strongs/h/h123.md), thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Is [ḥāḵmâ](../../strongs/h/h2451.md) no more in [Têmān](../../strongs/h/h8487.md)? is ['etsah](../../strongs/h/h6098.md) ['abad](../../strongs/h/h6.md) from the [bîn](../../strongs/h/h995.md)? is their [ḥāḵmâ](../../strongs/h/h2451.md) [sāraḥ](../../strongs/h/h5628.md)?

<a name="jeremiah_49_8"></a>Jeremiah 49:8

[nûs](../../strongs/h/h5127.md) ye, [panah](../../strongs/h/h6437.md), [yashab](../../strongs/h/h3427.md) [ʿāmaq](../../strongs/h/h6009.md), [yashab](../../strongs/h/h3427.md) of [Dᵊḏān](../../strongs/h/h1719.md); for I will [bow'](../../strongs/h/h935.md) the ['êḏ](../../strongs/h/h343.md) of [ʿĒśāv](../../strongs/h/h6215.md) upon him, the [ʿēṯ](../../strongs/h/h6256.md) that I will [paqad](../../strongs/h/h6485.md) him.

<a name="jeremiah_49_9"></a>Jeremiah 49:9

If [bāṣar](../../strongs/h/h1219.md) [bow'](../../strongs/h/h935.md) to thee, would they not [šā'ar](../../strongs/h/h7604.md) some [ʿōlēlôṯ](../../strongs/h/h5955.md)? if [gannāḇ](../../strongs/h/h1590.md) by [layil](../../strongs/h/h3915.md), they will [shachath](../../strongs/h/h7843.md) till they have [day](../../strongs/h/h1767.md).

<a name="jeremiah_49_10"></a>Jeremiah 49:10

But I have made [ʿĒśāv](../../strongs/h/h6215.md) [ḥāśap̄](../../strongs/h/h2834.md), I have [gālâ](../../strongs/h/h1540.md) his [mictar](../../strongs/h/h4565.md), and he shall not be [yakol](../../strongs/h/h3201.md) to [ḥāḇâ](../../strongs/h/h2247.md) himself: his [zera'](../../strongs/h/h2233.md) is [shadad](../../strongs/h/h7703.md), and his ['ach](../../strongs/h/h251.md), and his [šāḵēn](../../strongs/h/h7934.md), and he is not.

<a name="jeremiah_49_11"></a>Jeremiah 49:11

['azab](../../strongs/h/h5800.md) thy [yathowm](../../strongs/h/h3490.md), I will [ḥāyâ](../../strongs/h/h2421.md) them; and let thy ['almānâ](../../strongs/h/h490.md) [batach](../../strongs/h/h982.md) in me.

<a name="jeremiah_49_12"></a>Jeremiah 49:12

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, they whose [mishpat](../../strongs/h/h4941.md) was not to [šāṯâ](../../strongs/h/h8354.md) of the [kowc](../../strongs/h/h3563.md) have [šāṯâ](../../strongs/h/h8354.md) [šāṯâ](../../strongs/h/h8354.md); and art thou he that shall [naqah](../../strongs/h/h5352.md) go [naqah](../../strongs/h/h5352.md)? thou shalt not go [naqah](../../strongs/h/h5352.md), but thou shalt [šāṯâ](../../strongs/h/h8354.md) [šāṯâ](../../strongs/h/h8354.md) of it.

<a name="jeremiah_49_13"></a>Jeremiah 49:13

For I have [shaba'](../../strongs/h/h7650.md) by myself, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that [Bāṣrâ](../../strongs/h/h1224.md) shall become a [šammâ](../../strongs/h/h8047.md), a [cherpah](../../strongs/h/h2781.md), a [ḥōreḇ](../../strongs/h/h2721.md), and a [qᵊlālâ](../../strongs/h/h7045.md); and all the [ʿîr](../../strongs/h/h5892.md) thereof shall be ['owlam](../../strongs/h/h5769.md) [chorbah](../../strongs/h/h2723.md).

<a name="jeremiah_49_14"></a>Jeremiah 49:14

I have [shama'](../../strongs/h/h8085.md) a [šᵊmûʿâ](../../strongs/h/h8052.md) from [Yĕhovah](../../strongs/h/h3068.md), and an [ṣîr](../../strongs/h/h6735.md) is [shalach](../../strongs/h/h7971.md) unto the [gowy](../../strongs/h/h1471.md), saying, [qāḇaṣ](../../strongs/h/h6908.md) ye, and [bow'](../../strongs/h/h935.md) against her, and [quwm](../../strongs/h/h6965.md) to the [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_49_15"></a>Jeremiah 49:15

For, lo, I will [nathan](../../strongs/h/h5414.md) thee [qāṭān](../../strongs/h/h6996.md) among the [gowy](../../strongs/h/h1471.md), and [bazah](../../strongs/h/h959.md) among ['āḏām](../../strongs/h/h120.md).

<a name="jeremiah_49_16"></a>Jeremiah 49:16

Thy [tip̄leṣeṯ](../../strongs/h/h8606.md) hath [nasha'](../../strongs/h/h5377.md) thee, and the [zāḏôn](../../strongs/h/h2087.md) of thine [leb](../../strongs/h/h3820.md), O thou that [shakan](../../strongs/h/h7931.md) in the [ḥăḡāv](../../strongs/h/h2288.md) of the [cela'](../../strongs/h/h5553.md), that [tāp̄aś](../../strongs/h/h8610.md) the [marowm](../../strongs/h/h4791.md) of the [giḇʿâ](../../strongs/h/h1389.md): though thou shouldest make thy [qēn](../../strongs/h/h7064.md) as [gāḇah](../../strongs/h/h1361.md) as the [nesheׁr](../../strongs/h/h5404.md), I will [yarad](../../strongs/h/h3381.md) thee from thence, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_49_17"></a>Jeremiah 49:17

Also ['Ĕḏōm](../../strongs/h/h123.md) shall be a [šammâ](../../strongs/h/h8047.md): every one that ['abar](../../strongs/h/h5674.md) by it shall be [šāmēm](../../strongs/h/h8074.md), and shall [šāraq](../../strongs/h/h8319.md) at all the [makâ](../../strongs/h/h4347.md) thereof.

<a name="jeremiah_49_18"></a>Jeremiah 49:18

As in the [mahpēḵâ](../../strongs/h/h4114.md) of [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md) and the [šāḵēn](../../strongs/h/h7934.md) thereof, ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), no ['iysh](../../strongs/h/h376.md) shall [yashab](../../strongs/h/h3427.md) there, neither shall a [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [guwr](../../strongs/h/h1481.md) in it.

<a name="jeremiah_49_19"></a>Jeremiah 49:19

Behold, he shall [ʿālâ](../../strongs/h/h5927.md) like an ['ariy](../../strongs/h/h738.md) from the [gā'ôn](../../strongs/h/h1347.md) of [Yardēn](../../strongs/h/h3383.md) against the [nāvê](../../strongs/h/h5116.md) of the ['êṯān](../../strongs/h/h386.md): but I will [rāḡaʿ](../../strongs/h/h7280.md) make him run [rûṣ](../../strongs/h/h7323.md) from her: and who is a [bāḥar](../../strongs/h/h977.md) man, that I may [paqad](../../strongs/h/h6485.md) over her? for who is like me? and who will appoint me the [yāʿaḏ](../../strongs/h/h3259.md)? and who is that [ra'ah](../../strongs/h/h7462.md) that will ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me?

<a name="jeremiah_49_20"></a>Jeremiah 49:20

Therefore [shama'](../../strongs/h/h8085.md) the ['etsah](../../strongs/h/h6098.md) of [Yĕhovah](../../strongs/h/h3068.md), that he hath [ya'ats](../../strongs/h/h3289.md) against ['Ĕḏōm](../../strongs/h/h123.md); and his [maḥăšāḇâ](../../strongs/h/h4284.md), that he hath [chashab](../../strongs/h/h2803.md) against the [yashab](../../strongs/h/h3427.md) of [Têmān](../../strongs/h/h8487.md): Surely the [ṣāʿîr](../../strongs/h/h6810.md) of the [tso'n](../../strongs/h/h6629.md) shall [sāḥaḇ](../../strongs/h/h5498.md) them: surely he shall make their [nāvê](../../strongs/h/h5116.md) [šāmēm](../../strongs/h/h8074.md) with them.

<a name="jeremiah_49_21"></a>Jeremiah 49:21

The ['erets](../../strongs/h/h776.md) is [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of their [naphal](../../strongs/h/h5307.md), at the [tsa'aqah](../../strongs/h/h6818.md) the [qowl](../../strongs/h/h6963.md) thereof was [shama'](../../strongs/h/h8085.md) in the [sûp̄](../../strongs/h/h5488.md) [yam](../../strongs/h/h3220.md).

<a name="jeremiah_49_22"></a>Jeremiah 49:22

Behold, he shall [ʿālâ](../../strongs/h/h5927.md) and [da'ah](../../strongs/h/h1675.md) as the [nesheׁr](../../strongs/h/h5404.md), and [pāraś](../../strongs/h/h6566.md) his [kanaph](../../strongs/h/h3671.md) over [Bāṣrâ](../../strongs/h/h1224.md): and at that [yowm](../../strongs/h/h3117.md) shall the [leb](../../strongs/h/h3820.md) of the [gibôr](../../strongs/h/h1368.md) of ['Ĕḏōm](../../strongs/h/h123.md) be as the [leb](../../strongs/h/h3820.md) of an ['ishshah](../../strongs/h/h802.md) in her [tsarar](../../strongs/h/h6887.md).

<a name="jeremiah_49_23"></a>Jeremiah 49:23

Concerning [Dammeśeq](../../strongs/h/h1834.md). [Ḥămāṯ](../../strongs/h/h2574.md) is [buwsh](../../strongs/h/h954.md), and ['Arpāḏ](../../strongs/h/h774.md): for they have [shama'](../../strongs/h/h8085.md) [ra'](../../strongs/h/h7451.md) [šᵊmûʿâ](../../strongs/h/h8052.md): they are [mûḡ](../../strongs/h/h4127.md); there is [dᵊ'āḡâ](../../strongs/h/h1674.md) on the [yam](../../strongs/h/h3220.md); it [yakol](../../strongs/h/h3201.md) be [šāqaṭ](../../strongs/h/h8252.md).

<a name="jeremiah_49_24"></a>Jeremiah 49:24

[Dammeśeq](../../strongs/h/h1834.md) is [rāp̄â](../../strongs/h/h7503.md), and [panah](../../strongs/h/h6437.md) herself to [nûs](../../strongs/h/h5127.md), and [reṭeṭ](../../strongs/h/h7374.md) hath [ḥāzaq](../../strongs/h/h2388.md) on her: [tsarah](../../strongs/h/h6869.md) and [chebel](../../strongs/h/h2256.md) have ['āḥaz](../../strongs/h/h270.md) her, as a [yalad](../../strongs/h/h3205.md).

<a name="jeremiah_49_25"></a>Jeremiah 49:25

How is the [ʿîr](../../strongs/h/h5892.md) of [tehillah](../../strongs/h/h8416.md) not ['azab](../../strongs/h/h5800.md), the [qiryâ](../../strongs/h/h7151.md) of my [māśôś](../../strongs/h/h4885.md)!

<a name="jeremiah_49_26"></a>Jeremiah 49:26

Therefore her [bāḥûr](../../strongs/h/h970.md) shall [naphal](../../strongs/h/h5307.md) in her [rᵊḥōḇ](../../strongs/h/h7339.md), and all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) shall be [damam](../../strongs/h/h1826.md) in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_49_27"></a>Jeremiah 49:27

And I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in the [ḥômâ](../../strongs/h/h2346.md) of [Dammeśeq](../../strongs/h/h1834.md), and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Ben-Hăḏaḏ](../../strongs/h/h1130.md).

<a name="jeremiah_49_28"></a>Jeremiah 49:28

Concerning [Qēḏār](../../strongs/h/h6938.md), and concerning the [mamlāḵâ](../../strongs/h/h4467.md) of [Ḥāṣôr](../../strongs/h/h2674.md), which [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) shall [nakah](../../strongs/h/h5221.md), thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [quwm](../../strongs/h/h6965.md) ye, [ʿālâ](../../strongs/h/h5927.md) to [Qēḏār](../../strongs/h/h6938.md), and [shadad](../../strongs/h/h7703.md) the [ben](../../strongs/h/h1121.md) of the [qeḏem](../../strongs/h/h6924.md).

<a name="jeremiah_49_29"></a>Jeremiah 49:29

Their ['ohel](../../strongs/h/h168.md) and their [tso'n](../../strongs/h/h6629.md) shall they [laqach](../../strongs/h/h3947.md): they shall [nasa'](../../strongs/h/h5375.md) to themselves their [yᵊrîʿâ](../../strongs/h/h3407.md), and all their [kĕliy](../../strongs/h/h3627.md), and their [gāmāl](../../strongs/h/h1581.md); and they shall [qara'](../../strongs/h/h7121.md) unto them, [māḡôr](../../strongs/h/h4032.md) is on every [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_49_30"></a>Jeremiah 49:30

[nûs](../../strongs/h/h5127.md), [nuwd](../../strongs/h/h5110.md) you [me'od](../../strongs/h/h3966.md), [yashab](../../strongs/h/h3427.md) [ʿāmaq](../../strongs/h/h6009.md), O ye [yashab](../../strongs/h/h3427.md) of [Ḥāṣôr](../../strongs/h/h2674.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); for [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) hath taken ['etsah](../../strongs/h/h6098.md) [ya'ats](../../strongs/h/h3289.md) against you, and hath [chashab](../../strongs/h/h2803.md) a [maḥăšāḇâ](../../strongs/h/h4284.md) against you.

<a name="jeremiah_49_31"></a>Jeremiah 49:31

[quwm](../../strongs/h/h6965.md), get you [ʿālâ](../../strongs/h/h5927.md) unto the [šālēv](../../strongs/h/h7961.md) [gowy](../../strongs/h/h1471.md), that [yashab](../../strongs/h/h3427.md) without [betach](../../strongs/h/h983.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), which have neither [deleṯ](../../strongs/h/h1817.md) nor [bᵊrîaḥ](../../strongs/h/h1280.md), which [shakan](../../strongs/h/h7931.md) [bāḏāḏ](../../strongs/h/h910.md).

<a name="jeremiah_49_32"></a>Jeremiah 49:32

And their [gāmāl](../../strongs/h/h1581.md) shall be a [baz](../../strongs/h/h957.md), and the [hāmôn](../../strongs/h/h1995.md) of their [miqnê](../../strongs/h/h4735.md) a [šālāl](../../strongs/h/h7998.md): and I will [zārâ](../../strongs/h/h2219.md) into all [ruwach](../../strongs/h/h7307.md) them that are in the [qāṣaṣ](../../strongs/h/h7112.md) [pē'â](../../strongs/h/h6285.md); and I will [bow'](../../strongs/h/h935.md) their ['êḏ](../../strongs/h/h343.md) from all [ʿēḇer](../../strongs/h/h5676.md) thereof, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_49_33"></a>Jeremiah 49:33

And [Ḥāṣôr](../../strongs/h/h2674.md) shall be a [māʿôn](../../strongs/h/h4583.md) for [tannîn](../../strongs/h/h8577.md), and a [šᵊmāmâ](../../strongs/h/h8077.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md): there shall no ['iysh](../../strongs/h/h376.md) [yashab](../../strongs/h/h3427.md) there, nor any [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [guwr](../../strongs/h/h1481.md) in it.

<a name="jeremiah_49_34"></a>Jeremiah 49:34

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) against [ʿÊlām](../../strongs/h/h5867.md) in the [re'shiyth](../../strongs/h/h7225.md) of the [malkuwth](../../strongs/h/h4438.md) of [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_49_35"></a>Jeremiah 49:35

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Behold, I will [shabar](../../strongs/h/h7665.md) the [qesheth](../../strongs/h/h7198.md) of [ʿÊlām](../../strongs/h/h5867.md), the [re'shiyth](../../strongs/h/h7225.md) of their [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="jeremiah_49_36"></a>Jeremiah 49:36

And upon [ʿÊlām](../../strongs/h/h5867.md) will I [bow'](../../strongs/h/h935.md) the ['arbaʿ](../../strongs/h/h702.md) [ruwach](../../strongs/h/h7307.md) from the ['arbaʿ](../../strongs/h/h702.md) [qāṣâ](../../strongs/h/h7098.md) of [shamayim](../../strongs/h/h8064.md), and will [zārâ](../../strongs/h/h2219.md) them toward all those [ruwach](../../strongs/h/h7307.md); and there shall be no [gowy](../../strongs/h/h1471.md) whither the [nāḏaḥ](../../strongs/h/h5080.md) of [ʿÊlām](../../strongs/h/h5867.md) ['owlam](../../strongs/h/h5769.md) shall not [bow'](../../strongs/h/h935.md).

<a name="jeremiah_49_37"></a>Jeremiah 49:37

For I will cause [ʿÊlām](../../strongs/h/h5867.md) to be [ḥāṯaṯ](../../strongs/h/h2865.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), and [paniym](../../strongs/h/h6440.md) them that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md): and I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon them, even my [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and I will [shalach](../../strongs/h/h7971.md) the [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) them, till I have [kalah](../../strongs/h/h3615.md) them:

<a name="jeremiah_49_38"></a>Jeremiah 49:38

And I will [śûm](../../strongs/h/h7760.md) my [kicce'](../../strongs/h/h3678.md) in [ʿÊlām](../../strongs/h/h5867.md), and will ['abad](../../strongs/h/h6.md) from thence the [melek](../../strongs/h/h4428.md) and the [śar](../../strongs/h/h8269.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_49_39"></a>Jeremiah 49:39

But it shall come to pass in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md), that I will [shuwb](../../strongs/h/h7725.md) [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md) of [ʿÊlām](../../strongs/h/h5867.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 48](jeremiah_48.md) - [Jeremiah 50](jeremiah_50.md)