# [Jeremiah 6](https://www.blueletterbible.org/kjv/jeremiah/6)

<a name="jeremiah_6_1"></a>Jeremiah 6:1

O ye [ben](../../strongs/h/h1121.md) of [Binyāmîn](../../strongs/h/h1144.md), gather yourselves to [ʿûz](../../strongs/h/h5756.md) out of the [qereḇ](../../strongs/h/h7130.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and [tāqaʿ](../../strongs/h/h8628.md) the [šôp̄ār](../../strongs/h/h7782.md) in [Tᵊqôaʿ](../../strongs/h/h8620.md), and [nasa'](../../strongs/h/h5375.md) a [maśśᵊ'ēṯ](../../strongs/h/h4864.md) of fire in [Bêṯ-Hakerem](../../strongs/h/h1021.md): for [ra'](../../strongs/h/h7451.md) [šāqap̄](../../strongs/h/h8259.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md), and [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md).

<a name="jeremiah_6_2"></a>Jeremiah 6:2

I have [damah](../../strongs/h/h1820.md) the [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md) to a [nāvê](../../strongs/h/h5116.md) and [ʿānaḡ](../../strongs/h/h6026.md) woman.

<a name="jeremiah_6_3"></a>Jeremiah 6:3

The [ra'ah](../../strongs/h/h7462.md) with their [ʿēḏer](../../strongs/h/h5739.md) shall [bow'](../../strongs/h/h935.md) unto her; they shall [tāqaʿ](../../strongs/h/h8628.md) their ['ohel](../../strongs/h/h168.md) against her [cabiyb](../../strongs/h/h5439.md); they shall [ra'ah](../../strongs/h/h7462.md) every ['iysh](../../strongs/h/h376.md) in his [yad](../../strongs/h/h3027.md).

<a name="jeremiah_6_4"></a>Jeremiah 6:4

[qadash](../../strongs/h/h6942.md) ye [milḥāmâ](../../strongs/h/h4421.md) against her; [quwm](../../strongs/h/h6965.md), and let us [ʿālâ](../../strongs/h/h5927.md) at [ṣōhar](../../strongs/h/h6672.md). ['owy](../../strongs/h/h188.md) unto us! for the [yowm](../../strongs/h/h3117.md) goeth [panah](../../strongs/h/h6437.md), for the [ṣl](../../strongs/h/h6752.md) of the ['ereb](../../strongs/h/h6153.md) are stretched [natah](../../strongs/h/h5186.md).

<a name="jeremiah_6_5"></a>Jeremiah 6:5

[quwm](../../strongs/h/h6965.md), and let us [ʿālâ](../../strongs/h/h5927.md) by [layil](../../strongs/h/h3915.md), and let us [shachath](../../strongs/h/h7843.md) her ['armôn](../../strongs/h/h759.md).

<a name="jeremiah_6_6"></a>Jeremiah 6:6

For thus hath [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) ['āmar](../../strongs/h/h559.md), Hew ye [karath](../../strongs/h/h3772.md) [ʿēṣâ](../../strongs/h/h6097.md), and [šāp̄aḵ](../../strongs/h/h8210.md) a [sōllâ](../../strongs/h/h5550.md) against [Yĕruwshalaim](../../strongs/h/h3389.md): this is the [ʿîr](../../strongs/h/h5892.md) to be [paqad](../../strongs/h/h6485.md); she is [ʿōšeq](../../strongs/h/h6233.md) in the [qereḇ](../../strongs/h/h7130.md) of her.

<a name="jeremiah_6_7"></a>Jeremiah 6:7

As a [bowr](../../strongs/h/h953.md) casteth [qûr](../../strongs/h/h6979.md) her [mayim](../../strongs/h/h4325.md), so she [qûr](../../strongs/h/h6979.md) her [ra'](../../strongs/h/h7451.md): [chamac](../../strongs/h/h2555.md) and [shod](../../strongs/h/h7701.md) is [shama'](../../strongs/h/h8085.md) in her; before [paniym](../../strongs/h/h6440.md) [tāmîḏ](../../strongs/h/h8548.md) is [ḥŏlî](../../strongs/h/h2483.md) and [makâ](../../strongs/h/h4347.md).

<a name="jeremiah_6_8"></a>Jeremiah 6:8

Be thou [yacar](../../strongs/h/h3256.md), [Yĕruwshalaim](../../strongs/h/h3389.md), lest my [nephesh](../../strongs/h/h5315.md) [yāqaʿ](../../strongs/h/h3363.md) from thee; lest I [śûm](../../strongs/h/h7760.md) thee [šᵊmāmâ](../../strongs/h/h8077.md), an ['erets](../../strongs/h/h776.md) not [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_6_9"></a>Jeremiah 6:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), They shall [ʿālal](../../strongs/h/h5953.md) [ʿālal](../../strongs/h/h5953.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md) as a [gep̄en](../../strongs/h/h1612.md): [shuwb](../../strongs/h/h7725.md) thine [yad](../../strongs/h/h3027.md) as a [bāṣar](../../strongs/h/h1219.md) into the [salsillâ](../../strongs/h/h5552.md).

<a name="jeremiah_6_10"></a>Jeremiah 6:10

To whom shall I [dabar](../../strongs/h/h1696.md), and [ʿûḏ](../../strongs/h/h5749.md), that they may [shama'](../../strongs/h/h8085.md)? behold, their ['ozen](../../strongs/h/h241.md) is [ʿārēl](../../strongs/h/h6189.md), and they [yakol](../../strongs/h/h3201.md) [qashab](../../strongs/h/h7181.md): behold, the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) is unto them a [cherpah](../../strongs/h/h2781.md); they have no [ḥāp̄ēṣ](../../strongs/h/h2654.md) in it.

<a name="jeremiah_6_11"></a>Jeremiah 6:11

Therefore I am [mālē'](../../strongs/h/h4392.md) of the [chemah](../../strongs/h/h2534.md) of [Yĕhovah](../../strongs/h/h3068.md); I am [lā'â](../../strongs/h/h3811.md) with [kûl](../../strongs/h/h3557.md): I will pour it [šāp̄aḵ](../../strongs/h/h8210.md) upon the ['owlel](../../strongs/h/h5768.md) [ḥûṣ](../../strongs/h/h2351.md), and upon the [sôḏ](../../strongs/h/h5475.md) of young [bāḥûr](../../strongs/h/h970.md) [yaḥaḏ](../../strongs/h/h3162.md): for even the ['iysh](../../strongs/h/h376.md) with the ['ishshah](../../strongs/h/h802.md) shall be [lāḵaḏ](../../strongs/h/h3920.md), the [zāqēn](../../strongs/h/h2205.md) with him that is [mālā'](../../strongs/h/h4390.md) of [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_6_12"></a>Jeremiah 6:12

And their [bayith](../../strongs/h/h1004.md) shall be [cabab](../../strongs/h/h5437.md) unto ['aḥēr](../../strongs/h/h312.md), with their [sadeh](../../strongs/h/h7704.md) and ['ishshah](../../strongs/h/h802.md) [yaḥaḏ](../../strongs/h/h3162.md): for I will [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md) upon the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_6_13"></a>Jeremiah 6:13

For from the [qāṭān](../../strongs/h/h6996.md) of them even unto the [gadowl](../../strongs/h/h1419.md) of them every one is [batsa'](../../strongs/h/h1214.md) to [beṣaʿ](../../strongs/h/h1215.md); and from the [nāḇî'](../../strongs/h/h5030.md) even unto the [kōhēn](../../strongs/h/h3548.md) every one ['asah](../../strongs/h/h6213.md) [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_6_14"></a>Jeremiah 6:14

They have [rapha'](../../strongs/h/h7495.md) also the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) [qālal](../../strongs/h/h7043.md), ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md), [shalowm](../../strongs/h/h7965.md); when there is no [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_6_15"></a>Jeremiah 6:15

Were they [yāḇēš](../../strongs/h/h3001.md) when they had ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md)? [gam](../../strongs/h/h1571.md), they were not at [buwsh](../../strongs/h/h954.md) [buwsh](../../strongs/h/h954.md), [gam](../../strongs/h/h1571.md) [yada'](../../strongs/h/h3045.md) they [kālam](../../strongs/h/h3637.md): therefore they shall [naphal](../../strongs/h/h5307.md) among them that [naphal](../../strongs/h/h5307.md): at the [ʿēṯ](../../strongs/h/h6256.md) that I [paqad](../../strongs/h/h6485.md) them they shall be [kashal](../../strongs/h/h3782.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_6_16"></a>Jeremiah 6:16

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), ['amad](../../strongs/h/h5975.md) ye in the [derek](../../strongs/h/h1870.md), and [ra'ah](../../strongs/h/h7200.md), and [sha'al](../../strongs/h/h7592.md) for the ['owlam](../../strongs/h/h5769.md) [nāṯîḇ](../../strongs/h/h5410.md), where is the [towb](../../strongs/h/h2896.md) [derek](../../strongs/h/h1870.md), and [yālaḵ](../../strongs/h/h3212.md) therein, and ye shall [māṣā'](../../strongs/h/h4672.md) [margôaʿ](../../strongs/h/h4771.md) for your [nephesh](../../strongs/h/h5315.md). But they ['āmar](../../strongs/h/h559.md), We will not [yālaḵ](../../strongs/h/h3212.md) therein.

<a name="jeremiah_6_17"></a>Jeremiah 6:17

Also I [quwm](../../strongs/h/h6965.md) [tsaphah](../../strongs/h/h6822.md) over you, saying, [qashab](../../strongs/h/h7181.md) to the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md). But they ['āmar](../../strongs/h/h559.md), We will not [qashab](../../strongs/h/h7181.md).

<a name="jeremiah_6_18"></a>Jeremiah 6:18

Therefore [shama'](../../strongs/h/h8085.md), ye [gowy](../../strongs/h/h1471.md), and [yada'](../../strongs/h/h3045.md), O ['edah](../../strongs/h/h5712.md), what is among them.

<a name="jeremiah_6_19"></a>Jeremiah 6:19

[shama'](../../strongs/h/h8085.md), O ['erets](../../strongs/h/h776.md): behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon this ['am](../../strongs/h/h5971.md), even the [pĕriy](../../strongs/h/h6529.md) of their [maḥăšāḇâ](../../strongs/h/h4284.md), because they have not [qashab](../../strongs/h/h7181.md) unto my [dabar](../../strongs/h/h1697.md), nor to my [towrah](../../strongs/h/h8451.md), but [mā'as](../../strongs/h/h3988.md) it.

<a name="jeremiah_6_20"></a>Jeremiah 6:20

To what purpose [bow'](../../strongs/h/h935.md) there to me [lᵊḇônâ](../../strongs/h/h3828.md) from [Šᵊḇā'](../../strongs/h/h7614.md), and the [towb](../../strongs/h/h2896.md) [qānê](../../strongs/h/h7070.md) from a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md)? your [ʿōlâ](../../strongs/h/h5930.md) are not [ratsown](../../strongs/h/h7522.md), nor your [zebach](../../strongs/h/h2077.md) [ʿāraḇ](../../strongs/h/h6149.md) unto me.

<a name="jeremiah_6_21"></a>Jeremiah 6:21

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [nathan](../../strongs/h/h5414.md) [miḵšôl](../../strongs/h/h4383.md) before this ['am](../../strongs/h/h5971.md), and the ['ab](../../strongs/h/h1.md) and the [ben](../../strongs/h/h1121.md) [yaḥaḏ](../../strongs/h/h3162.md) shall [kashal](../../strongs/h/h3782.md) upon them; the [šāḵēn](../../strongs/h/h7934.md) and his [rea'](../../strongs/h/h7453.md) shall ['abad](../../strongs/h/h6.md).

<a name="jeremiah_6_22"></a>Jeremiah 6:22

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, an ['am](../../strongs/h/h5971.md) [bow'](../../strongs/h/h935.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md), and a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) shall be [ʿûr](../../strongs/h/h5782.md) from the [yᵊrēḵâ](../../strongs/h/h3411.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_6_23"></a>Jeremiah 6:23

They shall lay [ḥāzaq](../../strongs/h/h2388.md) on [qesheth](../../strongs/h/h7198.md) and [kîḏôn](../../strongs/h/h3591.md); they are ['aḵzārî](../../strongs/h/h394.md), and have no [racham](../../strongs/h/h7355.md); their [qowl](../../strongs/h/h6963.md) [hāmâ](../../strongs/h/h1993.md) like the [yam](../../strongs/h/h3220.md); and they [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md), set in ['arak](../../strongs/h/h6186.md) as ['iysh](../../strongs/h/h376.md) for [milḥāmâ](../../strongs/h/h4421.md) against thee, O [bath](../../strongs/h/h1323.md) of [Tsiyown](../../strongs/h/h6726.md).

<a name="jeremiah_6_24"></a>Jeremiah 6:24

We have [shama'](../../strongs/h/h8085.md) the [šōmaʿ](../../strongs/h/h8089.md) thereof: our [yad](../../strongs/h/h3027.md) wax [rāp̄â](../../strongs/h/h7503.md): [tsarah](../../strongs/h/h6869.md) hath taken [ḥāzaq](../../strongs/h/h2388.md) of us, and [ḥîl](../../strongs/h/h2427.md), as of a woman in [yalad](../../strongs/h/h3205.md).

<a name="jeremiah_6_25"></a>Jeremiah 6:25

Go not [yāṣā'](../../strongs/h/h3318.md) into the [sadeh](../../strongs/h/h7704.md), nor [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md); for the [chereb](../../strongs/h/h2719.md) of the ['oyeb](../../strongs/h/h341.md) and [māḡôr](../../strongs/h/h4032.md) is on every [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_6_26"></a>Jeremiah 6:26

O [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md), [ḥāḡar](../../strongs/h/h2296.md) thee with [śaq](../../strongs/h/h8242.md), and [pālaš](../../strongs/h/h6428.md) thyself in ['ēp̄er](../../strongs/h/h665.md): ['asah](../../strongs/h/h6213.md) thee ['ēḇel](../../strongs/h/h60.md), as for an only [yāḥîḏ](../../strongs/h/h3173.md), most [tamrûrîm](../../strongs/h/h8563.md) [mispēḏ](../../strongs/h/h4553.md): for the [shadad](../../strongs/h/h7703.md) shall [piṯ'ōm](../../strongs/h/h6597.md) [bow'](../../strongs/h/h935.md) upon us.

<a name="jeremiah_6_27"></a>Jeremiah 6:27

I have [nathan](../../strongs/h/h5414.md) thee for a [bāḥôn](../../strongs/h/h969.md) and a [miḇṣār](../../strongs/h/h4013.md) among my ['am](../../strongs/h/h5971.md), that thou mayest [yada'](../../strongs/h/h3045.md) and [bachan](../../strongs/h/h974.md) their [derek](../../strongs/h/h1870.md).

<a name="jeremiah_6_28"></a>Jeremiah 6:28

They are all [cuwr](../../strongs/h/h5493.md) [sārar](../../strongs/h/h5637.md), [halak](../../strongs/h/h1980.md) with [rāḵîl](../../strongs/h/h7400.md): they are [nᵊḥšeṯ](../../strongs/h/h5178.md) and [barzel](../../strongs/h/h1270.md); they are all [shachath](../../strongs/h/h7843.md).

<a name="jeremiah_6_29"></a>Jeremiah 6:29

The [mapuaḥ](../../strongs/h/h4647.md) are [ḥārar](../../strongs/h/h2787.md), the [ʿōp̄ereṯ](../../strongs/h/h5777.md) is [tamam](../../strongs/h/h8552.md) of the ['eššâ](../../strongs/h/h800.md) ['esh](../../strongs/h/h784.md); the [tsaraph](../../strongs/h/h6884.md) [tsaraph](../../strongs/h/h6884.md) in [shav'](../../strongs/h/h7723.md): for the [ra'](../../strongs/h/h7451.md) are not plucked [nathaq](../../strongs/h/h5423.md).

<a name="jeremiah_6_30"></a>Jeremiah 6:30

[mā'as](../../strongs/h/h3988.md) [keceph](../../strongs/h/h3701.md) shall men [qara'](../../strongs/h/h7121.md) them, because [Yĕhovah](../../strongs/h/h3068.md) hath [mā'as](../../strongs/h/h3988.md) them.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 5](jeremiah_5.md) - [Jeremiah 7](jeremiah_7.md)