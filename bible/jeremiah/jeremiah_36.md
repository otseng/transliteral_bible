# [Jeremiah 36](https://www.blueletterbible.org/kjv/jeremiah/36)

<a name="jeremiah_36_1"></a>Jeremiah 36:1

And it came to pass in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), that this [dabar](../../strongs/h/h1697.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_36_2"></a>Jeremiah 36:2

[laqach](../../strongs/h/h3947.md) thee a [mᵊḡillâ](../../strongs/h/h4039.md) of a [sēp̄er](../../strongs/h/h5612.md), and [kāṯaḇ](../../strongs/h/h3789.md) therein all the [dabar](../../strongs/h/h1697.md) that I have [dabar](../../strongs/h/h1696.md) unto thee against [Yisra'el](../../strongs/h/h3478.md), and against [Yehuwdah](../../strongs/h/h3063.md), and against all the [gowy](../../strongs/h/h1471.md), from the [yowm](../../strongs/h/h3117.md) I [dabar](../../strongs/h/h1696.md) unto thee, from the [yowm](../../strongs/h/h3117.md) of [Yō'Šîyâ](../../strongs/h/h2977.md), even unto this [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_36_3"></a>Jeremiah 36:3

It may be that the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) will [shama'](../../strongs/h/h8085.md) all the [ra'](../../strongs/h/h7451.md) which I [chashab](../../strongs/h/h2803.md) to ['asah](../../strongs/h/h6213.md) unto them; that they may [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md); that I may [sālaḥ](../../strongs/h/h5545.md) their ['avon](../../strongs/h/h5771.md) and their [chatta'ath](../../strongs/h/h2403.md).

<a name="jeremiah_36_4"></a>Jeremiah 36:4

Then [Yirmᵊyâ](../../strongs/h/h3414.md) [qara'](../../strongs/h/h7121.md) [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md): and [Bārûḵ](../../strongs/h/h1263.md) [kāṯaḇ](../../strongs/h/h3789.md) from the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), which he had [dabar](../../strongs/h/h1696.md) unto him, upon a [mᵊḡillâ](../../strongs/h/h4039.md) of a [sēp̄er](../../strongs/h/h5612.md).

<a name="jeremiah_36_5"></a>Jeremiah 36:5

And [Yirmᵊyâ](../../strongs/h/h3414.md) [tsavah](../../strongs/h/h6680.md) [Bārûḵ](../../strongs/h/h1263.md), ['āmar](../../strongs/h/h559.md), I am [ʿāṣar](../../strongs/h/h6113.md); I [yakol](../../strongs/h/h3201.md) [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md):

<a name="jeremiah_36_6"></a>Jeremiah 36:6

Therefore [bow'](../../strongs/h/h935.md) thou, and [qara'](../../strongs/h/h7121.md) in the [mᵊḡillâ](../../strongs/h/h4039.md), which thou hast [kāṯaḇ](../../strongs/h/h3789.md) from my [peh](../../strongs/h/h6310.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md) in [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) upon the [ṣôm](../../strongs/h/h6685.md) [yowm](../../strongs/h/h3117.md): and also thou shalt [qara'](../../strongs/h/h7121.md) them in the ['ozen](../../strongs/h/h241.md) of all [Yehuwdah](../../strongs/h/h3063.md) that [bow'](../../strongs/h/h935.md) of their [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_36_7"></a>Jeremiah 36:7

It may be they will [naphal](../../strongs/h/h5307.md) their [tĕchinnah](../../strongs/h/h8467.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md), and will [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md): for [gadowl](../../strongs/h/h1419.md) is the ['aph](../../strongs/h/h639.md) and the [chemah](../../strongs/h/h2534.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) against this ['am](../../strongs/h/h5971.md).

<a name="jeremiah_36_8"></a>Jeremiah 36:8

And [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md) ['asah](../../strongs/h/h6213.md) according to all that [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [tsavah](../../strongs/h/h6680.md) him, [qara'](../../strongs/h/h7121.md) in the [sēp̄er](../../strongs/h/h5612.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) in [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md).

<a name="jeremiah_36_9"></a>Jeremiah 36:9

And it came to pass in the [ḥămîšî](../../strongs/h/h2549.md) [šānâ](../../strongs/h/h8141.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the [tᵊšîʿî](../../strongs/h/h8671.md) [ḥōḏeš](../../strongs/h/h2320.md), that they [qara'](../../strongs/h/h7121.md) a [ṣôm](../../strongs/h/h6685.md) [paniym](../../strongs/h/h6440.md) [Yĕhovah](../../strongs/h/h3068.md) to all the ['am](../../strongs/h/h5971.md) in [Yĕruwshalaim](../../strongs/h/h3389.md), and to all the ['am](../../strongs/h/h5971.md) that [bow'](../../strongs/h/h935.md) from the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) unto [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_36_10"></a>Jeremiah 36:10

Then [qara'](../../strongs/h/h7121.md) [Bārûḵ](../../strongs/h/h1263.md) in the [sēp̄er](../../strongs/h/h5612.md) the [dabar](../../strongs/h/h1697.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), in the [liškâ](../../strongs/h/h3957.md) of [Gᵊmaryâ](../../strongs/h/h1587.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md) the [sāp̄ar](../../strongs/h/h5608.md), in the ['elyown](../../strongs/h/h5945.md) [ḥāṣēr](../../strongs/h/h2691.md), at the [peṯaḥ](../../strongs/h/h6607.md) of the [ḥāḏāš](../../strongs/h/h2319.md) [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), in the ['ozen](../../strongs/h/h241.md) of all the ['am](../../strongs/h/h5971.md).

<a name="jeremiah_36_11"></a>Jeremiah 36:11

When [Mîḵāyhû](../../strongs/h/h4321.md) the [ben](../../strongs/h/h1121.md) of [Gᵊmaryâ](../../strongs/h/h1587.md), the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), had [shama'](../../strongs/h/h8085.md) out of the [sēp̄er](../../strongs/h/h5612.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md),

<a name="jeremiah_36_12"></a>Jeremiah 36:12

Then he [yarad](../../strongs/h/h3381.md) into the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), into the [sāp̄ar](../../strongs/h/h5608.md) [liškâ](../../strongs/h/h3957.md): and, lo, all the [śar](../../strongs/h/h8269.md) [yashab](../../strongs/h/h3427.md) there, even ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [sāp̄ar](../../strongs/h/h5608.md), and [Dᵊlāyâ](../../strongs/h/h1806.md) the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md), and ['Elnāṯān](../../strongs/h/h494.md) the [ben](../../strongs/h/h1121.md) of [ʿAḵbôr](../../strongs/h/h5907.md), and [Gᵊmaryâ](../../strongs/h/h1587.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), and [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [Ḥănanyâ](../../strongs/h/h2608.md), and all the [śar](../../strongs/h/h8269.md).

<a name="jeremiah_36_13"></a>Jeremiah 36:13

Then [Mîḵāyhû](../../strongs/h/h4321.md) [nāḡaḏ](../../strongs/h/h5046.md) unto them all the [dabar](../../strongs/h/h1697.md) that he had [shama'](../../strongs/h/h8085.md), when [Bārûḵ](../../strongs/h/h1263.md) [qara'](../../strongs/h/h7121.md) the [sēp̄er](../../strongs/h/h5612.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md).

<a name="jeremiah_36_14"></a>Jeremiah 36:14

Therefore all the [śar](../../strongs/h/h8269.md) [shalach](../../strongs/h/h7971.md) [Yᵊhûḏî](../../strongs/h/h3065.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), the [ben](../../strongs/h/h1121.md) of [Šelemyâ](../../strongs/h/h8018.md), the [ben](../../strongs/h/h1121.md) of [Kûšî](../../strongs/h/h3570.md), unto [Bārûḵ](../../strongs/h/h1263.md), ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) in thine [yad](../../strongs/h/h3027.md) the [mᵊḡillâ](../../strongs/h/h4039.md) wherein thou hast [qara'](../../strongs/h/h7121.md) in the ['ozen](../../strongs/h/h241.md) of the ['am](../../strongs/h/h5971.md), and [yālaḵ](../../strongs/h/h3212.md). So [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md) [laqach](../../strongs/h/h3947.md) the [mᵊḡillâ](../../strongs/h/h4039.md) in his [yad](../../strongs/h/h3027.md), and [bow'](../../strongs/h/h935.md) unto them.

<a name="jeremiah_36_15"></a>Jeremiah 36:15

And they ['āmar](../../strongs/h/h559.md) unto him, [yashab](../../strongs/h/h3427.md) now, and [qara'](../../strongs/h/h7121.md) it in our ['ozen](../../strongs/h/h241.md). So [Bārûḵ](../../strongs/h/h1263.md) [qara'](../../strongs/h/h7121.md) it in their ['ozen](../../strongs/h/h241.md).

<a name="jeremiah_36_16"></a>Jeremiah 36:16

Now it came to pass, when they had [shama'](../../strongs/h/h8085.md) all the [dabar](../../strongs/h/h1697.md), they were [pachad](../../strongs/h/h6342.md) ['ēl](../../strongs/h/h413.md) ['iysh](../../strongs/h/h376.md) and [rea'](../../strongs/h/h7453.md), and ['āmar](../../strongs/h/h559.md) unto [Bārûḵ](../../strongs/h/h1263.md), We will [nāḡaḏ](../../strongs/h/h5046.md) [nāḡaḏ](../../strongs/h/h5046.md) the [melek](../../strongs/h/h4428.md) of all these [dabar](../../strongs/h/h1697.md).

<a name="jeremiah_36_17"></a>Jeremiah 36:17

And they [sha'al](../../strongs/h/h7592.md) [Bārûḵ](../../strongs/h/h1263.md), ['āmar](../../strongs/h/h559.md), [nāḡaḏ](../../strongs/h/h5046.md) us now, How didst thou [kāṯaḇ](../../strongs/h/h3789.md) all these [dabar](../../strongs/h/h1697.md) at his [peh](../../strongs/h/h6310.md)?

<a name="jeremiah_36_18"></a>Jeremiah 36:18

Then [Bārûḵ](../../strongs/h/h1263.md) ['āmar](../../strongs/h/h559.md) them, He [qara'](../../strongs/h/h7121.md) all these [dabar](../../strongs/h/h1697.md) unto me with his [peh](../../strongs/h/h6310.md), and I [kāṯaḇ](../../strongs/h/h3789.md) them with [dᵊyô](../../strongs/h/h1773.md) in the [sēp̄er](../../strongs/h/h5612.md).

<a name="jeremiah_36_19"></a>Jeremiah 36:19

Then ['āmar](../../strongs/h/h559.md) the [śar](../../strongs/h/h8269.md) unto [Bārûḵ](../../strongs/h/h1263.md), [yālaḵ](../../strongs/h/h3212.md), [cathar](../../strongs/h/h5641.md) thee, thou and [Yirmᵊyâ](../../strongs/h/h3414.md); and let no ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) ['êp̄ô](../../strongs/h/h375.md) ye be.

<a name="jeremiah_36_20"></a>Jeremiah 36:20

And they [bow'](../../strongs/h/h935.md) to the [melek](../../strongs/h/h4428.md) into the [ḥāṣēr](../../strongs/h/h2691.md), but they laid [paqad](../../strongs/h/h6485.md) the [mᵊḡillâ](../../strongs/h/h4039.md) in the [liškâ](../../strongs/h/h3957.md) of ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [sāp̄ar](../../strongs/h/h5608.md), and [nāḡaḏ](../../strongs/h/h5046.md) all the [dabar](../../strongs/h/h1697.md) in the ['ozen](../../strongs/h/h241.md) of the [melek](../../strongs/h/h4428.md).

<a name="jeremiah_36_21"></a>Jeremiah 36:21

So the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) [Yᵊhûḏî](../../strongs/h/h3065.md) to [laqach](../../strongs/h/h3947.md) the [mᵊḡillâ](../../strongs/h/h4039.md): and he [laqach](../../strongs/h/h3947.md) it out of ['Ĕlîšāmāʿ](../../strongs/h/h476.md) the [sāp̄ar](../../strongs/h/h5608.md) [liškâ](../../strongs/h/h3957.md). And [Yᵊhûḏî](../../strongs/h/h3065.md) [qara'](../../strongs/h/h7121.md) it in the ['ozen](../../strongs/h/h241.md) of the [melek](../../strongs/h/h4428.md), and in the ['ozen](../../strongs/h/h241.md) of all the [śar](../../strongs/h/h8269.md) which ['amad](../../strongs/h/h5975.md) [ʿal](../../strongs/h/h5921.md) the [melek](../../strongs/h/h4428.md).

<a name="jeremiah_36_22"></a>Jeremiah 36:22

Now the [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) in the [bayith](../../strongs/h/h1004.md) [ḥōrep̄](../../strongs/h/h2779.md) in the [tᵊšîʿî](../../strongs/h/h8671.md) [ḥōḏeš](../../strongs/h/h2320.md): and there was a fire on the ['āḥ](../../strongs/h/h254.md) [bāʿar](../../strongs/h/h1197.md) [paniym](../../strongs/h/h6440.md) him.

<a name="jeremiah_36_23"></a>Jeremiah 36:23

And it came to pass, that when [Yᵊhûḏî](../../strongs/h/h3065.md) had [qara'](../../strongs/h/h7121.md) [šālôš](../../strongs/h/h7969.md) or ['arbaʿ](../../strongs/h/h702.md) [deleṯ](../../strongs/h/h1817.md), he [qāraʿ](../../strongs/h/h7167.md) it with the [taʿar](../../strongs/h/h8593.md) [sāp̄ar](../../strongs/h/h5608.md), and [shalak](../../strongs/h/h7993.md) it into the ['esh](../../strongs/h/h784.md) that was on the ['āḥ](../../strongs/h/h254.md), until all the [mᵊḡillâ](../../strongs/h/h4039.md) was [tamam](../../strongs/h/h8552.md) in the ['esh](../../strongs/h/h784.md) that was on the ['āḥ](../../strongs/h/h254.md).

<a name="jeremiah_36_24"></a>Jeremiah 36:24

Yet they were not [pachad](../../strongs/h/h6342.md), nor [qāraʿ](../../strongs/h/h7167.md) their [beḡeḏ](../../strongs/h/h899.md), neither the [melek](../../strongs/h/h4428.md), nor any of his ['ebed](../../strongs/h/h5650.md) that [shama'](../../strongs/h/h8085.md) all these [dabar](../../strongs/h/h1697.md).

<a name="jeremiah_36_25"></a>Jeremiah 36:25

Nevertheless ['Elnāṯān](../../strongs/h/h494.md) and [Dᵊlāyâ](../../strongs/h/h1806.md) and [Gᵊmaryâ](../../strongs/h/h1587.md) had made [pāḡaʿ](../../strongs/h/h6293.md) to the [melek](../../strongs/h/h4428.md) that he would not [śārap̄](../../strongs/h/h8313.md) the [mᵊḡillâ](../../strongs/h/h4039.md): but he would not [shama'](../../strongs/h/h8085.md) them.

<a name="jeremiah_36_26"></a>Jeremiah 36:26

But the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [Yᵊraḥmᵊ'Ēl](../../strongs/h/h3396.md) the [ben](../../strongs/h/h1121.md) of [Meleḵ](../../strongs/h/h4429.md), and [Śᵊrāyâ](../../strongs/h/h8304.md) the [ben](../../strongs/h/h1121.md) of [ʿAzrî'Ēl](../../strongs/h/h5837.md), and [Šelemyâ](../../strongs/h/h8018.md) the [ben](../../strongs/h/h1121.md) of [ʿAḇdᵊ'Ēl](../../strongs/h/h5655.md), to [laqach](../../strongs/h/h3947.md) [Bārûḵ](../../strongs/h/h1263.md) the [sāp̄ar](../../strongs/h/h5608.md) and [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md): but [Yĕhovah](../../strongs/h/h3068.md) [cathar](../../strongs/h/h5641.md) them.

<a name="jeremiah_36_27"></a>Jeremiah 36:27

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Yirmᵊyâ](../../strongs/h/h3414.md), ['aḥar](../../strongs/h/h310.md) that the [melek](../../strongs/h/h4428.md) had [śārap̄](../../strongs/h/h8313.md) the [mᵊḡillâ](../../strongs/h/h4039.md), and the [dabar](../../strongs/h/h1697.md) which [Bārûḵ](../../strongs/h/h1263.md) [kāṯaḇ](../../strongs/h/h3789.md) at the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_36_28"></a>Jeremiah 36:28

[laqach](../../strongs/h/h3947.md) thee [shuwb](../../strongs/h/h7725.md) ['aḥēr](../../strongs/h/h312.md) [mᵊḡillâ](../../strongs/h/h4039.md), and [kāṯaḇ](../../strongs/h/h3789.md) in it all the [ri'šôn](../../strongs/h/h7223.md) [dabar](../../strongs/h/h1697.md) that were in the [ri'šôn](../../strongs/h/h7223.md) [mᵊḡillâ](../../strongs/h/h4039.md), which [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) hath [śārap̄](../../strongs/h/h8313.md).

<a name="jeremiah_36_29"></a>Jeremiah 36:29

And thou shalt ['āmar](../../strongs/h/h559.md) to [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Thou hast [śārap̄](../../strongs/h/h8313.md) this [mᵊḡillâ](../../strongs/h/h4039.md), ['āmar](../../strongs/h/h559.md), Why hast thou [kāṯaḇ](../../strongs/h/h3789.md) therein, ['āmar](../../strongs/h/h559.md), The [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) shall [bow'](../../strongs/h/h935.md) [bow'](../../strongs/h/h935.md) and [shachath](../../strongs/h/h7843.md) this ['erets](../../strongs/h/h776.md), and shall cause to [shabath](../../strongs/h/h7673.md) from thence ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md)?

<a name="jeremiah_36_30"></a>Jeremiah 36:30

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md); He shall have none to [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md): and his [nᵊḇēlâ](../../strongs/h/h5038.md) shall be [shalak](../../strongs/h/h7993.md) in the [yowm](../../strongs/h/h3117.md) to the [ḥōreḇ](../../strongs/h/h2721.md), and in the [layil](../../strongs/h/h3915.md) to the [qeraḥ](../../strongs/h/h7140.md).

<a name="jeremiah_36_31"></a>Jeremiah 36:31

And I will [paqad](../../strongs/h/h6485.md) him and his [zera'](../../strongs/h/h2233.md) and his ['ebed](../../strongs/h/h5650.md) for their ['avon](../../strongs/h/h5771.md); and I will [bow'](../../strongs/h/h935.md) upon them, and upon the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and upon the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), all the [ra'](../../strongs/h/h7451.md) that I have [dabar](../../strongs/h/h1696.md) against them; but they [shama'](../../strongs/h/h8085.md) not.

<a name="jeremiah_36_32"></a>Jeremiah 36:32

Then [laqach](../../strongs/h/h3947.md) [Yirmᵊyâ](../../strongs/h/h3414.md) ['aḥēr](../../strongs/h/h312.md) [mᵊḡillâ](../../strongs/h/h4039.md), and [nathan](../../strongs/h/h5414.md) it to [Bārûḵ](../../strongs/h/h1263.md) the [sāp̄ar](../../strongs/h/h5608.md), the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md); who [kāṯaḇ](../../strongs/h/h3789.md) therein from the [peh](../../strongs/h/h6310.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) all the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) which [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [śārap̄](../../strongs/h/h8313.md) in the ['esh](../../strongs/h/h784.md): and there were [yāsap̄](../../strongs/h/h3254.md) besides unto them [rab](../../strongs/h/h7227.md) [hēm](../../strongs/h/h1992.md) [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 35](jeremiah_35.md) - [Jeremiah 37](jeremiah_37.md)