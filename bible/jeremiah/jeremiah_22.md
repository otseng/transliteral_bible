# [Jeremiah 22](https://www.blueletterbible.org/kjv/jeremiah/22)

<a name="jeremiah_22_1"></a>Jeremiah 22:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Go [yarad](../../strongs/h/h3381.md) to the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [dabar](../../strongs/h/h1696.md) there this [dabar](../../strongs/h/h1697.md),

<a name="jeremiah_22_2"></a>Jeremiah 22:2

And ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), that [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), thou, and thy ['ebed](../../strongs/h/h5650.md), and thy ['am](../../strongs/h/h5971.md) that [bow'](../../strongs/h/h935.md) in by these [sha'ar](../../strongs/h/h8179.md):

<a name="jeremiah_22_3"></a>Jeremiah 22:3

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); ['asah](../../strongs/h/h6213.md) ye [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), and [natsal](../../strongs/h/h5337.md) the [gāzal](../../strongs/h/h1497.md) out of the [yad](../../strongs/h/h3027.md) of the [ʿāšôq](../../strongs/h/h6216.md): and do no [yānâ](../../strongs/h/h3238.md), do no [ḥāmas](../../strongs/h/h2554.md) to the [ger](../../strongs/h/h1616.md), the [yathowm](../../strongs/h/h3490.md), nor the ['almānâ](../../strongs/h/h490.md), neither [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) in this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_22_4"></a>Jeremiah 22:4

For if ye ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md) ['asah](../../strongs/h/h6213.md), then shall there [bow'](../../strongs/h/h935.md) by the [sha'ar](../../strongs/h/h8179.md) of this [bayith](../../strongs/h/h1004.md) [melek](../../strongs/h/h4428.md) [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), [rāḵaḇ](../../strongs/h/h7392.md) in [reḵeḇ](../../strongs/h/h7393.md) and on [sûs](../../strongs/h/h5483.md), he, and his ['ebed](../../strongs/h/h5650.md), and his ['am](../../strongs/h/h5971.md).

<a name="jeremiah_22_5"></a>Jeremiah 22:5

But if ye will not [shama'](../../strongs/h/h8085.md) these [dabar](../../strongs/h/h1697.md), I [shaba'](../../strongs/h/h7650.md) by myself, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that this [bayith](../../strongs/h/h1004.md) shall become a [chorbah](../../strongs/h/h2723.md).

<a name="jeremiah_22_6"></a>Jeremiah 22:6

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md); Thou art [Gilʿāḏ](../../strongs/h/h1568.md) unto me, and the [ro'sh](../../strongs/h/h7218.md) of [Lᵊḇānôn](../../strongs/h/h3844.md): yet surely I will [shiyth](../../strongs/h/h7896.md) thee a [midbar](../../strongs/h/h4057.md), and [ʿîr](../../strongs/h/h5892.md) which are not [yashab](../../strongs/h/h3427.md) [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_22_7"></a>Jeremiah 22:7

And I will [qadash](../../strongs/h/h6942.md) [shachath](../../strongs/h/h7843.md) against thee, every ['iysh](../../strongs/h/h376.md) with his [kĕliy](../../strongs/h/h3627.md): and they shall [karath](../../strongs/h/h3772.md) thy [miḇḥār](../../strongs/h/h4005.md) ['erez](../../strongs/h/h730.md), and [naphal](../../strongs/h/h5307.md) them into the ['esh](../../strongs/h/h784.md).

<a name="jeremiah_22_8"></a>Jeremiah 22:8

And [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) shall ['abar](../../strongs/h/h5674.md) by this [ʿîr](../../strongs/h/h5892.md), and they shall ['āmar](../../strongs/h/h559.md) every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md), Wherefore hath [Yĕhovah](../../strongs/h/h3068.md) ['asah](../../strongs/h/h6213.md) thus unto this [gadowl](../../strongs/h/h1419.md) [ʿîr](../../strongs/h/h5892.md)?

<a name="jeremiah_22_9"></a>Jeremiah 22:9

Then they shall ['āmar](../../strongs/h/h559.md), Because they have ['azab](../../strongs/h/h5800.md) the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and [shachah](../../strongs/h/h7812.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and ['abad](../../strongs/h/h5647.md) them.

<a name="jeremiah_22_10"></a>Jeremiah 22:10

[bāḵâ](../../strongs/h/h1058.md) ye not for the [muwth](../../strongs/h/h4191.md), neither [nuwd](../../strongs/h/h5110.md) him: but [bāḵâ](../../strongs/h/h1058.md) [bāḵâ](../../strongs/h/h1058.md) for him that goeth [halak](../../strongs/h/h1980.md): for he shall [shuwb](../../strongs/h/h7725.md) no more, nor [ra'ah](../../strongs/h/h7200.md) his [môleḏeṯ](../../strongs/h/h4138.md) ['erets](../../strongs/h/h776.md).

<a name="jeremiah_22_11"></a>Jeremiah 22:11

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['ēl](../../strongs/h/h413.md) [Šallûm](../../strongs/h/h7967.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), which [mālaḵ](../../strongs/h/h4427.md) instead of [Yō'Šîyâ](../../strongs/h/h2977.md) his ['ab](../../strongs/h/h1.md), which [yāṣā'](../../strongs/h/h3318.md) out of this [maqowm](../../strongs/h/h4725.md); He shall not [shuwb](../../strongs/h/h7725.md) thither any more:

<a name="jeremiah_22_12"></a>Jeremiah 22:12

But he shall [muwth](../../strongs/h/h4191.md) in the [maqowm](../../strongs/h/h4725.md) whither they have [gālâ](../../strongs/h/h1540.md) him, and shall [ra'ah](../../strongs/h/h7200.md) this ['erets](../../strongs/h/h776.md) no more.

<a name="jeremiah_22_13"></a>Jeremiah 22:13

[hôy](../../strongs/h/h1945.md) unto him that [bānâ](../../strongs/h/h1129.md) his [bayith](../../strongs/h/h1004.md) [lō'](../../strongs/h/h3808.md) [tsedeq](../../strongs/h/h6664.md), and his [ʿălîyâ](../../strongs/h/h5944.md) by [mishpat](../../strongs/h/h4941.md); that useth his [rea'](../../strongs/h/h7453.md) ['abad](../../strongs/h/h5647.md) without [ḥinnām](../../strongs/h/h2600.md), and [nathan](../../strongs/h/h5414.md) him not for his [pōʿal](../../strongs/h/h6467.md);

<a name="jeremiah_22_14"></a>Jeremiah 22:14

That ['āmar](../../strongs/h/h559.md), I will [bānâ](../../strongs/h/h1129.md) me a [midâ](../../strongs/h/h4060.md) [bayith](../../strongs/h/h1004.md) and [rāvaḥ](../../strongs/h/h7304.md) [ʿălîyâ](../../strongs/h/h5944.md), and [qāraʿ](../../strongs/h/h7167.md) him [ḥallôn](../../strongs/h/h2474.md); and it is [sāp̄an](../../strongs/h/h5603.md) with ['erez](../../strongs/h/h730.md), and [māšaḥ](../../strongs/h/h4886.md) with [šāšēr](../../strongs/h/h8350.md).

<a name="jeremiah_22_15"></a>Jeremiah 22:15

Shalt thou [mālaḵ](../../strongs/h/h4427.md), because thou [taḥărâ](../../strongs/h/h8474.md) thyself in ['erez](../../strongs/h/h730.md)? did not thy ['ab](../../strongs/h/h1.md) ['akal](../../strongs/h/h398.md) and [šāṯâ](../../strongs/h/h8354.md), and ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) and [tsedaqah](../../strongs/h/h6666.md), and then it was [towb](../../strongs/h/h2896.md) with him?

<a name="jeremiah_22_16"></a>Jeremiah 22:16

He [diyn](../../strongs/h/h1777.md) the [diyn](../../strongs/h/h1779.md) of the ['aniy](../../strongs/h/h6041.md) and ['ebyown](../../strongs/h/h34.md); then it was [towb](../../strongs/h/h2896.md) with him: was not this to [da'ath](../../strongs/h/h1847.md) me? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_22_17"></a>Jeremiah 22:17

But thine ['ayin](../../strongs/h/h5869.md) and thine [leb](../../strongs/h/h3820.md) are not but for thy [beṣaʿ](../../strongs/h/h1215.md), and for to [šāp̄aḵ](../../strongs/h/h8210.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md), and for [ʿōšeq](../../strongs/h/h6233.md), and for [mᵊruṣâ](../../strongs/h/h4835.md), to ['asah](../../strongs/h/h6213.md) it.

<a name="jeremiah_22_18"></a>Jeremiah 22:18

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md); They shall not [sāp̄aḏ](../../strongs/h/h5594.md) for him, saying, [hôy](../../strongs/h/h1945.md) my ['ach](../../strongs/h/h251.md)! or, [hôy](../../strongs/h/h1945.md) ['āḥôṯ](../../strongs/h/h269.md)! they shall not [sāp̄aḏ](../../strongs/h/h5594.md) for him, saying, [hôy](../../strongs/h/h1945.md) ['adown](../../strongs/h/h113.md)! or, [hôy](../../strongs/h/h1945.md) his [howd](../../strongs/h/h1935.md)!

<a name="jeremiah_22_19"></a>Jeremiah 22:19

He shall be [qāḇar](../../strongs/h/h6912.md) with the [qᵊḇûrâ](../../strongs/h/h6900.md) of a [chamowr](../../strongs/h/h2543.md), [sāḥaḇ](../../strongs/h/h5498.md) and [shalak](../../strongs/h/h7993.md) [hāl'â](../../strongs/h/h1973.md) the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_22_20"></a>Jeremiah 22:20

[ʿālâ](../../strongs/h/h5927.md) to [Lᵊḇānôn](../../strongs/h/h3844.md), and [ṣāʿaq](../../strongs/h/h6817.md); and lift [nathan](../../strongs/h/h5414.md) thy [qowl](../../strongs/h/h6963.md) in [Bāšān](../../strongs/h/h1316.md), and [ṣāʿaq](../../strongs/h/h6817.md) from the [ʿēḇer](../../strongs/h/h5676.md): for all thy ['ahab](../../strongs/h/h157.md) are [shabar](../../strongs/h/h7665.md).

<a name="jeremiah_22_21"></a>Jeremiah 22:21

I [dabar](../../strongs/h/h1696.md) unto thee in thy [šalvâ](../../strongs/h/h7962.md); but thou ['āmar](../../strongs/h/h559.md), I will not [shama'](../../strongs/h/h8085.md). This hath been thy [derek](../../strongs/h/h1870.md) from thy [nāʿur](../../strongs/h/h5271.md), that thou [shama'](../../strongs/h/h8085.md) not my [qowl](../../strongs/h/h6963.md).

<a name="jeremiah_22_22"></a>Jeremiah 22:22

The [ruwach](../../strongs/h/h7307.md) shall [ra'ah](../../strongs/h/h7462.md) all thy [ra'ah](../../strongs/h/h7462.md), and thy ['ahab](../../strongs/h/h157.md) shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md): surely then shalt thou be [buwsh](../../strongs/h/h954.md) and [kālam](../../strongs/h/h3637.md) for all thy [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_22_23"></a>Jeremiah 22:23

O [yashab](../../strongs/h/h3427.md) of [Lᵊḇānôn](../../strongs/h/h3844.md), that makest thy [qānan](../../strongs/h/h7077.md) in the ['erez](../../strongs/h/h730.md), how [chanan](../../strongs/h/h2603.md) shalt thou be when [chebel](../../strongs/h/h2256.md) [bow'](../../strongs/h/h935.md) upon thee, the [ḥîl](../../strongs/h/h2427.md) as of a [yalad](../../strongs/h/h3205.md)!

<a name="jeremiah_22_24"></a>Jeremiah 22:24

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), though [Kānyâû](../../strongs/h/h3659.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) were the [ḥôṯām](../../strongs/h/h2368.md) upon my [yamiyn](../../strongs/h/h3225.md) [yad](../../strongs/h/h3027.md), yet would I [nathaq](../../strongs/h/h5423.md) thee thence;

<a name="jeremiah_22_25"></a>Jeremiah 22:25

And I will [nathan](../../strongs/h/h5414.md) thee into the [yad](../../strongs/h/h3027.md) of them that [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md), and into the [yad](../../strongs/h/h3027.md) of them whose [paniym](../../strongs/h/h6440.md) thou [yāḡôr](../../strongs/h/h3016.md), even into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="jeremiah_22_26"></a>Jeremiah 22:26

And I will [ṭûl](../../strongs/h/h2904.md) thee, and thy ['em](../../strongs/h/h517.md) that [yalad](../../strongs/h/h3205.md) thee, into ['aḥēr](../../strongs/h/h312.md) ['erets](../../strongs/h/h776.md), where ye were not [yalad](../../strongs/h/h3205.md); and there shall ye [muwth](../../strongs/h/h4191.md).

<a name="jeremiah_22_27"></a>Jeremiah 22:27

But to the ['erets](../../strongs/h/h776.md) whereunto they [nasa'](../../strongs/h/h5375.md) [nephesh](../../strongs/h/h5315.md) to [shuwb](../../strongs/h/h7725.md), thither shall they not [shuwb](../../strongs/h/h7725.md).

<a name="jeremiah_22_28"></a>Jeremiah 22:28

Is this ['iysh](../../strongs/h/h376.md) [Kānyâû](../../strongs/h/h3659.md) a [bazah](../../strongs/h/h959.md) [naphats](../../strongs/h/h5310.md) ['etseb](../../strongs/h/h6089.md)? is he a [kĕliy](../../strongs/h/h3627.md) wherein is no [chephets](../../strongs/h/h2656.md)? wherefore are they [shalak](../../strongs/h/h7993.md), he and his [zera'](../../strongs/h/h2233.md), and are [ṭûl](../../strongs/h/h2904.md) into an ['erets](../../strongs/h/h776.md) which they [yada'](../../strongs/h/h3045.md) not?

<a name="jeremiah_22_29"></a>Jeremiah 22:29

O ['erets](../../strongs/h/h776.md), ['erets](../../strongs/h/h776.md), ['erets](../../strongs/h/h776.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_22_30"></a>Jeremiah 22:30

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [kāṯaḇ](../../strongs/h/h3789.md) ye this ['iysh](../../strongs/h/h376.md) [ʿărîrî](../../strongs/h/h6185.md), a [geḇer](../../strongs/h/h1397.md) that shall not [tsalach](../../strongs/h/h6743.md) in his [yowm](../../strongs/h/h3117.md): for no ['iysh](../../strongs/h/h376.md) of his [zera'](../../strongs/h/h2233.md) shall [tsalach](../../strongs/h/h6743.md), [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), and [mashal](../../strongs/h/h4910.md) any more in [Yehuwdah](../../strongs/h/h3063.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 21](jeremiah_21.md) - [Jeremiah 23](jeremiah_23.md)