# [Jeremiah 26](https://www.blueletterbible.org/kjv/jeremiah/26)

<a name="jeremiah_26_1"></a>Jeremiah 26:1

In the [re'shiyth](../../strongs/h/h7225.md) of the [mamlāḵûṯ](../../strongs/h/h4468.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) came this [dabar](../../strongs/h/h1697.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_26_2"></a>Jeremiah 26:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); ['amad](../../strongs/h/h5975.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), and [dabar](../../strongs/h/h1696.md) unto all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), which [bow'](../../strongs/h/h935.md) to [shachah](../../strongs/h/h7812.md) in [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), all the [dabar](../../strongs/h/h1697.md) that I [tsavah](../../strongs/h/h6680.md) thee to [dabar](../../strongs/h/h1696.md) unto them; [gāraʿ](../../strongs/h/h1639.md) not a [dabar](../../strongs/h/h1697.md):

<a name="jeremiah_26_3"></a>Jeremiah 26:3

If so be they will [shama'](../../strongs/h/h8085.md), and [shuwb](../../strongs/h/h7725.md) every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), that I may [nacham](../../strongs/h/h5162.md) me of the [ra'](../../strongs/h/h7451.md), which I [chashab](../../strongs/h/h2803.md) to ['asah](../../strongs/h/h6213.md) unto them [paniym](../../strongs/h/h6440.md) of the [rōaʿ](../../strongs/h/h7455.md) of their [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_26_4"></a>Jeremiah 26:4

And thou shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); If ye will not [shama'](../../strongs/h/h8085.md) to me, to [yālaḵ](../../strongs/h/h3212.md) in my [towrah](../../strongs/h/h8451.md), which I have [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you,

<a name="jeremiah_26_5"></a>Jeremiah 26:5

To [shama'](../../strongs/h/h8085.md) to the [dabar](../../strongs/h/h1697.md) of my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), whom I [shalach](../../strongs/h/h7971.md) unto you, both [šāḵam](../../strongs/h/h7925.md), and [shalach](../../strongs/h/h7971.md) them, but ye have not [shama'](../../strongs/h/h8085.md);

<a name="jeremiah_26_6"></a>Jeremiah 26:6

Then will I [nathan](../../strongs/h/h5414.md) this [bayith](../../strongs/h/h1004.md) like [Šîlô](../../strongs/h/h7887.md), and will [nathan](../../strongs/h/h5414.md) this [ʿîr](../../strongs/h/h5892.md) a [qᵊlālâ](../../strongs/h/h7045.md) to all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_26_7"></a>Jeremiah 26:7

So the [kōhēn](../../strongs/h/h3548.md) and the [nāḇî'](../../strongs/h/h5030.md) and all the ['am](../../strongs/h/h5971.md) [shama'](../../strongs/h/h8085.md) [Yirmᵊyâ](../../strongs/h/h3414.md) [dabar](../../strongs/h/h1696.md) these [dabar](../../strongs/h/h1697.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_26_8"></a>Jeremiah 26:8

Now it came to pass, when [Yirmᵊyâ](../../strongs/h/h3414.md) had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) all that [Yĕhovah](../../strongs/h/h3068.md) had [tsavah](../../strongs/h/h6680.md) him to [dabar](../../strongs/h/h1696.md) unto all the ['am](../../strongs/h/h5971.md), that the [kōhēn](../../strongs/h/h3548.md) and the [nāḇî'](../../strongs/h/h5030.md) and all the ['am](../../strongs/h/h5971.md) [tāp̄aś](../../strongs/h/h8610.md) him, ['āmar](../../strongs/h/h559.md), Thou shalt [muwth](../../strongs/h/h4191.md) [muwth](../../strongs/h/h4191.md).

<a name="jeremiah_26_9"></a>Jeremiah 26:9

Why hast thou [nāḇā'](../../strongs/h/h5012.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md), This [bayith](../../strongs/h/h1004.md) shall be like [Šîlô](../../strongs/h/h7887.md), and this [ʿîr](../../strongs/h/h5892.md) shall be [ḥāraḇ](../../strongs/h/h2717.md) without a [yashab](../../strongs/h/h3427.md)? And all the ['am](../../strongs/h/h5971.md) were [qāhal](../../strongs/h/h6950.md) against [Yirmᵊyâ](../../strongs/h/h3414.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_26_10"></a>Jeremiah 26:10

When the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) [shama'](../../strongs/h/h8085.md) these [dabar](../../strongs/h/h1697.md), then they [ʿālâ](../../strongs/h/h5927.md) from the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md) unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and [yashab](../../strongs/h/h3427.md) in the [peṯaḥ](../../strongs/h/h6607.md) of the [ḥāḏāš](../../strongs/h/h2319.md) [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) house.

<a name="jeremiah_26_11"></a>Jeremiah 26:11

Then ['āmar](../../strongs/h/h559.md) the [kōhēn](../../strongs/h/h3548.md) and the [nāḇî'](../../strongs/h/h5030.md) unto the [śar](../../strongs/h/h8269.md) and to all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), This ['iysh](../../strongs/h/h376.md) is [mishpat](../../strongs/h/h4941.md) to [maveth](../../strongs/h/h4194.md); for he hath [nāḇā'](../../strongs/h/h5012.md) against this [ʿîr](../../strongs/h/h5892.md), as ye have [shama'](../../strongs/h/h8085.md) with your ['ozen](../../strongs/h/h241.md).

<a name="jeremiah_26_12"></a>Jeremiah 26:12

Then ['āmar](../../strongs/h/h559.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto all the [śar](../../strongs/h/h8269.md) and to all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [shalach](../../strongs/h/h7971.md) me to [nāḇā'](../../strongs/h/h5012.md) against this [bayith](../../strongs/h/h1004.md) and against this [ʿîr](../../strongs/h/h5892.md) all the [dabar](../../strongs/h/h1697.md) that ye have [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_26_13"></a>Jeremiah 26:13

Therefore now [yatab](../../strongs/h/h3190.md) your [derek](../../strongs/h/h1870.md) and your [maʿălāl](../../strongs/h/h4611.md), and [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md); and [Yĕhovah](../../strongs/h/h3068.md) will [nacham](../../strongs/h/h5162.md) him of the [ra'](../../strongs/h/h7451.md) that he hath [dabar](../../strongs/h/h1696.md) against you.

<a name="jeremiah_26_14"></a>Jeremiah 26:14

As for me, behold, I am in your [yad](../../strongs/h/h3027.md): ['asah](../../strongs/h/h6213.md) with me as ['ayin](../../strongs/h/h5869.md) [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) unto you.

<a name="jeremiah_26_15"></a>Jeremiah 26:15

But [yada'](../../strongs/h/h3045.md) ye for [yada'](../../strongs/h/h3045.md), that if ye put me to [muwth](../../strongs/h/h4191.md), ye shall surely [nathan](../../strongs/h/h5414.md) [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) upon yourselves, and upon this [ʿîr](../../strongs/h/h5892.md), and upon the [yashab](../../strongs/h/h3427.md) thereof: for of an ['emeth](../../strongs/h/h571.md) [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) me unto you to [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md) in your ['ozen](../../strongs/h/h241.md).

<a name="jeremiah_26_16"></a>Jeremiah 26:16

Then ['āmar](../../strongs/h/h559.md) the [śar](../../strongs/h/h8269.md) and all the ['am](../../strongs/h/h5971.md) unto the [kōhēn](../../strongs/h/h3548.md) and to the [nāḇî'](../../strongs/h/h5030.md); This ['iysh](../../strongs/h/h376.md) is not [mishpat](../../strongs/h/h4941.md) to [maveth](../../strongs/h/h4194.md): for he hath [dabar](../../strongs/h/h1696.md) to us in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_26_17"></a>Jeremiah 26:17

Then [quwm](../../strongs/h/h6965.md) ['enowsh](../../strongs/h/h582.md) of the [zāqēn](../../strongs/h/h2205.md) of the ['erets](../../strongs/h/h776.md), and ['āmar](../../strongs/h/h559.md) to all the [qāhēl](../../strongs/h/h6951.md) of the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_26_18"></a>Jeremiah 26:18

[Mîḵāyâ](../../strongs/h/h4320.md) the [môraštî](../../strongs/h/h4183.md) [nāḇā'](../../strongs/h/h5012.md) in the [yowm](../../strongs/h/h3117.md) of [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md) to all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); [Tsiyown](../../strongs/h/h6726.md) shall be [ḥāraš](../../strongs/h/h2790.md) like a [sadeh](../../strongs/h/h7704.md), and [Yĕruwshalaim](../../strongs/h/h3389.md) shall become [ʿî](../../strongs/h/h5856.md), and the [har](../../strongs/h/h2022.md) of the [bayith](../../strongs/h/h1004.md) as the [bāmâ](../../strongs/h/h1116.md) of a [yaʿar](../../strongs/h/h3293.md).

<a name="jeremiah_26_19"></a>Jeremiah 26:19

Did [Ḥizqîyâ](../../strongs/h/h2396.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and all [Yehuwdah](../../strongs/h/h3063.md) put him at [muwth](../../strongs/h/h4191.md) to [muwth](../../strongs/h/h4191.md)? did he not [yārē'](../../strongs/h/h3373.md) [Yĕhovah](../../strongs/h/h3068.md), and [ḥālâ](../../strongs/h/h2470.md) [Yĕhovah](../../strongs/h/h3068.md) [paniym](../../strongs/h/h6440.md), and [Yĕhovah](../../strongs/h/h3068.md) [nacham](../../strongs/h/h5162.md) him of the [ra'](../../strongs/h/h7451.md) which he had [dabar](../../strongs/h/h1696.md) against them? ['ănaḥnû](../../strongs/h/h587.md) might we ['asah](../../strongs/h/h6213.md) [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md) against our [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_26_20"></a>Jeremiah 26:20

And there was also an ['iysh](../../strongs/h/h376.md) that [nāḇā'](../../strongs/h/h5012.md) in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), ['Ûrîyâ](../../strongs/h/h223.md) the [ben](../../strongs/h/h1121.md) of [ŠᵊmaʿYâ](../../strongs/h/h8098.md) of [Qiryaṯ YᵊʿĀrîm](../../strongs/h/h7157.md), who [nāḇā'](../../strongs/h/h5012.md) against this [ʿîr](../../strongs/h/h5892.md) and against this ['erets](../../strongs/h/h776.md) according to all the [dabar](../../strongs/h/h1697.md) of [Yirmᵊyâ](../../strongs/h/h3414.md):

<a name="jeremiah_26_21"></a>Jeremiah 26:21

And when [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [melek](../../strongs/h/h4428.md), with all his [gibôr](../../strongs/h/h1368.md), and all the [śar](../../strongs/h/h8269.md), [shama'](../../strongs/h/h8085.md) his [dabar](../../strongs/h/h1697.md), the [melek](../../strongs/h/h4428.md) [bāqaš](../../strongs/h/h1245.md) to put him to [muwth](../../strongs/h/h4191.md): but when ['Ûrîyâ](../../strongs/h/h223.md) [shama'](../../strongs/h/h8085.md) it, he was [yare'](../../strongs/h/h3372.md), and [bāraḥ](../../strongs/h/h1272.md), and [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md);

<a name="jeremiah_26_22"></a>Jeremiah 26:22

And [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md) ['enowsh](../../strongs/h/h582.md) into [Mitsrayim](../../strongs/h/h4714.md), namely, ['Elnāṯān](../../strongs/h/h494.md) the [ben](../../strongs/h/h1121.md) of [ʿAḵbôr](../../strongs/h/h5907.md), and certain ['enowsh](../../strongs/h/h582.md) with him into [Mitsrayim](../../strongs/h/h4714.md).

<a name="jeremiah_26_23"></a>Jeremiah 26:23

And they [yāṣā'](../../strongs/h/h3318.md) ['Ûrîyâ](../../strongs/h/h223.md) out of [Mitsrayim](../../strongs/h/h4714.md), and [bow'](../../strongs/h/h935.md) him unto [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [melek](../../strongs/h/h4428.md); who [nakah](../../strongs/h/h5221.md) him with the [chereb](../../strongs/h/h2719.md), and [shalak](../../strongs/h/h7993.md) his dead [nᵊḇēlâ](../../strongs/h/h5038.md) into the [qeber](../../strongs/h/h6913.md) of the [ben](../../strongs/h/h1121.md) ['am](../../strongs/h/h5971.md).

<a name="jeremiah_26_24"></a>Jeremiah 26:24

Nevertheless the [yad](../../strongs/h/h3027.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md) was with [Yirmᵊyâ](../../strongs/h/h3414.md), that they should not [nathan](../../strongs/h/h5414.md) him into the [yad](../../strongs/h/h3027.md) of the ['am](../../strongs/h/h5971.md) to put him to [muwth](../../strongs/h/h4191.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 25](jeremiah_25.md) - [Jeremiah 27](jeremiah_27.md)