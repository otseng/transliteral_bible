# [Jeremiah 9](https://www.blueletterbible.org/kjv/jeremiah/9)

<a name="jeremiah_9_1"></a>Jeremiah 9:1

Oh [nathan](../../strongs/h/h5414.md) my [ro'sh](../../strongs/h/h7218.md) were [mayim](../../strongs/h/h4325.md), and mine ['ayin](../../strongs/h/h5869.md) a [māqôr](../../strongs/h/h4726.md) of [dim'ah](../../strongs/h/h1832.md), that I might [bāḵâ](../../strongs/h/h1058.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md) for the [ḥālāl](../../strongs/h/h2491.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md)!

<a name="jeremiah_9_2"></a>Jeremiah 9:2

Oh [nathan](../../strongs/h/h5414.md) I had in the [midbar](../../strongs/h/h4057.md) a [mālôn](../../strongs/h/h4411.md) of wayfaring ['āraḥ](../../strongs/h/h732.md); that I might ['azab](../../strongs/h/h5800.md) my ['am](../../strongs/h/h5971.md), and [yālaḵ](../../strongs/h/h3212.md) from them! for they be all [na'aph](../../strongs/h/h5003.md), an [ʿăṣārâ](../../strongs/h/h6116.md) of [bāḡaḏ](../../strongs/h/h898.md).

<a name="jeremiah_9_3"></a>Jeremiah 9:3

And they [dāraḵ](../../strongs/h/h1869.md) their [lashown](../../strongs/h/h3956.md) like their [qesheth](../../strongs/h/h7198.md) for [sheqer](../../strongs/h/h8267.md): but they are not [gabar](../../strongs/h/h1396.md) for the ['ĕmûnâ](../../strongs/h/h530.md) upon the ['erets](../../strongs/h/h776.md); for they [yāṣā'](../../strongs/h/h3318.md) from [ra'](../../strongs/h/h7451.md) to [ra'](../../strongs/h/h7451.md), and they [yada'](../../strongs/h/h3045.md) not me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_9_4"></a>Jeremiah 9:4

Take ye [shamar](../../strongs/h/h8104.md) every ['iysh](../../strongs/h/h376.md) of his [rea'](../../strongs/h/h7453.md), and [batach](../../strongs/h/h982.md) ye not in any ['ach](../../strongs/h/h251.md): for every ['ach](../../strongs/h/h251.md) will [ʿāqaḇ](../../strongs/h/h6117.md) [ʿāqaḇ](../../strongs/h/h6117.md), and every [rea'](../../strongs/h/h7453.md) will [halak](../../strongs/h/h1980.md) with [rāḵîl](../../strongs/h/h7400.md).

<a name="jeremiah_9_5"></a>Jeremiah 9:5

And they will [hāṯal](../../strongs/h/h2048.md) every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md), and will not [dabar](../../strongs/h/h1696.md) the ['emeth](../../strongs/h/h571.md): they have [lamad](../../strongs/h/h3925.md) their [lashown](../../strongs/h/h3956.md) to [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md), and [lā'â](../../strongs/h/h3811.md) themselves to [ʿāvâ](../../strongs/h/h5753.md).

<a name="jeremiah_9_6"></a>Jeremiah 9:6

Thine [yashab](../../strongs/h/h3427.md) is in the [tavek](../../strongs/h/h8432.md) of [mirmah](../../strongs/h/h4820.md); through [mirmah](../../strongs/h/h4820.md) they [mā'ēn](../../strongs/h/h3985.md) to [yada'](../../strongs/h/h3045.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_9_7"></a>Jeremiah 9:7

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), Behold, I will [tsaraph](../../strongs/h/h6884.md) them, and [bachan](../../strongs/h/h974.md) them; for how shall I ['asah](../../strongs/h/h6213.md) [paniym](../../strongs/h/h6440.md) the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md)?

<a name="jeremiah_9_8"></a>Jeremiah 9:8

Their [lashown](../../strongs/h/h3956.md) is as a [chets](../../strongs/h/h2671.md) [šāḥaṭ](../../strongs/h/h7819.md); it [dabar](../../strongs/h/h1696.md) [mirmah](../../strongs/h/h4820.md): one [dabar](../../strongs/h/h1696.md) [shalowm](../../strongs/h/h7965.md) to his [rea'](../../strongs/h/h7453.md) with his [peh](../../strongs/h/h6310.md), but in [qereḇ](../../strongs/h/h7130.md) he [śûm](../../strongs/h/h7760.md) his ['ōreḇ](../../strongs/h/h696.md).

<a name="jeremiah_9_9"></a>Jeremiah 9:9

Shall I not [paqad](../../strongs/h/h6485.md) them for these things? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): shall not my [nephesh](../../strongs/h/h5315.md) be [naqam](../../strongs/h/h5358.md) on such a [gowy](../../strongs/h/h1471.md) as this?

<a name="jeremiah_9_10"></a>Jeremiah 9:10

For the [har](../../strongs/h/h2022.md) will I [nasa'](../../strongs/h/h5375.md) a [bĕkiy](../../strongs/h/h1065.md) and [nᵊhî](../../strongs/h/h5092.md), and for the [nā'â](../../strongs/h/h4999.md) of the [midbar](../../strongs/h/h4057.md) a [qînâ](../../strongs/h/h7015.md), because they are [yāṣaṯ](../../strongs/h/h3341.md), so that ['iysh](../../strongs/h/h376.md) can ['abar](../../strongs/h/h5674.md) through them; neither can men [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [miqnê](../../strongs/h/h4735.md); both the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md) and the [bĕhemah](../../strongs/h/h929.md) are [nāḏaḏ](../../strongs/h/h5074.md); they are [halak](../../strongs/h/h1980.md).

<a name="jeremiah_9_11"></a>Jeremiah 9:11

And I will [nathan](../../strongs/h/h5414.md) [Yĕruwshalaim](../../strongs/h/h3389.md) [gal](../../strongs/h/h1530.md), and a [māʿôn](../../strongs/h/h4583.md) of [tannîn](../../strongs/h/h8577.md); and I will [nathan](../../strongs/h/h5414.md) the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) [šᵊmāmâ](../../strongs/h/h8077.md), without a [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_9_12"></a>Jeremiah 9:12

Who is the [ḥāḵām](../../strongs/h/h2450.md) ['iysh](../../strongs/h/h376.md), that may [bîn](../../strongs/h/h995.md) this? and who is he to whom the [peh](../../strongs/h/h6310.md) of [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md), that he may [nāḡaḏ](../../strongs/h/h5046.md) it, for what the ['erets](../../strongs/h/h776.md) ['abad](../../strongs/h/h6.md) and is burned [yāṣaṯ](../../strongs/h/h3341.md) like a [midbar](../../strongs/h/h4057.md), that none ['abar](../../strongs/h/h5674.md)?

<a name="jeremiah_9_13"></a>Jeremiah 9:13

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), Because they have ['azab](../../strongs/h/h5800.md) my [towrah](../../strongs/h/h8451.md) which I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) them, and have not [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), neither [halak](../../strongs/h/h1980.md) therein;

<a name="jeremiah_9_14"></a>Jeremiah 9:14

But have [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) the [šᵊrîrûṯ](../../strongs/h/h8307.md) of their own [leb](../../strongs/h/h3820.md), and ['aḥar](../../strongs/h/h310.md) [BaʿAl](../../strongs/h/h1168.md), which their ['ab](../../strongs/h/h1.md) [lamad](../../strongs/h/h3925.md) them:

<a name="jeremiah_9_15"></a>Jeremiah 9:15

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will ['akal](../../strongs/h/h398.md) them, even this ['am](../../strongs/h/h5971.md), with [laʿănâ](../../strongs/h/h3939.md), and give them [mayim](../../strongs/h/h4325.md) of [rō'š](../../strongs/h/h7219.md) to [šāqâ](../../strongs/h/h8248.md).

<a name="jeremiah_9_16"></a>Jeremiah 9:16

I will [puwts](../../strongs/h/h6327.md) them also among the [gowy](../../strongs/h/h1471.md), whom neither they nor their ['ab](../../strongs/h/h1.md) have [yada'](../../strongs/h/h3045.md): and I will [shalach](../../strongs/h/h7971.md) a [chereb](../../strongs/h/h2719.md) ['aḥar](../../strongs/h/h310.md) them, till I have [kalah](../../strongs/h/h3615.md) them.

<a name="jeremiah_9_17"></a>Jeremiah 9:17

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), [bîn](../../strongs/h/h995.md) ye, and [qara'](../../strongs/h/h7121.md) for the [qônēn](../../strongs/h/h6969.md), that they may [bow'](../../strongs/h/h935.md); and [shalach](../../strongs/h/h7971.md) for [ḥāḵām](../../strongs/h/h2450.md) women, that they may [bow'](../../strongs/h/h935.md):

<a name="jeremiah_9_18"></a>Jeremiah 9:18

And let them make [māhar](../../strongs/h/h4116.md), and take [nasa'](../../strongs/h/h5375.md) a [nᵊhî](../../strongs/h/h5092.md) for us, that our ['ayin](../../strongs/h/h5869.md) may run [yarad](../../strongs/h/h3381.md) with [dim'ah](../../strongs/h/h1832.md), and our ['aph'aph](../../strongs/h/h6079.md) [nāzal](../../strongs/h/h5140.md) with [mayim](../../strongs/h/h4325.md).

<a name="jeremiah_9_19"></a>Jeremiah 9:19

For a [qowl](../../strongs/h/h6963.md) of [nᵊhî](../../strongs/h/h5092.md) is [shama'](../../strongs/h/h8085.md) out of [Tsiyown](../../strongs/h/h6726.md), How are we [shadad](../../strongs/h/h7703.md)! we are [me'od](../../strongs/h/h3966.md) [buwsh](../../strongs/h/h954.md), because we have ['azab](../../strongs/h/h5800.md) the ['erets](../../strongs/h/h776.md), because our [miškān](../../strongs/h/h4908.md) have [shalak](../../strongs/h/h7993.md) us.

<a name="jeremiah_9_20"></a>Jeremiah 9:20

Yet [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O ye ['ishshah](../../strongs/h/h802.md), and let your ['ozen](../../strongs/h/h241.md) [laqach](../../strongs/h/h3947.md) the [dabar](../../strongs/h/h1697.md) of his [peh](../../strongs/h/h6310.md), and [lamad](../../strongs/h/h3925.md) your [bath](../../strongs/h/h1323.md) [nᵊhî](../../strongs/h/h5092.md), and every ['ishshah](../../strongs/h/h802.md) her [rᵊʿûṯ](../../strongs/h/h7468.md) [qînâ](../../strongs/h/h7015.md).

<a name="jeremiah_9_21"></a>Jeremiah 9:21

For [maveth](../../strongs/h/h4194.md) is [ʿālâ](../../strongs/h/h5927.md) into our [ḥallôn](../../strongs/h/h2474.md), and is [bow'](../../strongs/h/h935.md) into our ['armôn](../../strongs/h/h759.md), to [karath](../../strongs/h/h3772.md) the ['owlel](../../strongs/h/h5768.md) from [ḥûṣ](../../strongs/h/h2351.md), and the young [bāḥûr](../../strongs/h/h970.md) from the [rᵊḥōḇ](../../strongs/h/h7339.md).

<a name="jeremiah_9_22"></a>Jeremiah 9:22

[dabar](../../strongs/h/h1696.md), Thus [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), Even the [nᵊḇēlâ](../../strongs/h/h5038.md) of ['āḏām](../../strongs/h/h120.md) shall [naphal](../../strongs/h/h5307.md) as [dōmen](../../strongs/h/h1828.md) upon the [paniym](../../strongs/h/h6440.md) [sadeh](../../strongs/h/h7704.md), and as the [ʿāmîr](../../strongs/h/h5995.md) ['aḥar](../../strongs/h/h310.md) the [qāṣar](../../strongs/h/h7114.md), and none shall ['āsap̄](../../strongs/h/h622.md) them.

<a name="jeremiah_9_23"></a>Jeremiah 9:23

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Let not the [ḥāḵām](../../strongs/h/h2450.md) man [halal](../../strongs/h/h1984.md) in his [ḥāḵmâ](../../strongs/h/h2451.md), neither let the [gibôr](../../strongs/h/h1368.md) man [halal](../../strongs/h/h1984.md) in his [gᵊḇûrâ](../../strongs/h/h1369.md), let not the [ʿāšîr](../../strongs/h/h6223.md) man [halal](../../strongs/h/h1984.md) in his [ʿōšer](../../strongs/h/h6239.md):

<a name="jeremiah_9_24"></a>Jeremiah 9:24

But let him that [halal](../../strongs/h/h1984.md) [halal](../../strongs/h/h1984.md) in this, that he [sakal](../../strongs/h/h7919.md) and [yada'](../../strongs/h/h3045.md) me, that I am [Yĕhovah](../../strongs/h/h3068.md) which ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md), [mishpat](../../strongs/h/h4941.md), and [tsedaqah](../../strongs/h/h6666.md), in the ['erets](../../strongs/h/h776.md): for in these things I [ḥāp̄ēṣ](../../strongs/h/h2654.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_9_25"></a>Jeremiah 9:25

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [paqad](../../strongs/h/h6485.md) all them which are [muwl](../../strongs/h/h4135.md) with the [ʿārlâ](../../strongs/h/h6190.md);

<a name="jeremiah_9_26"></a>Jeremiah 9:26

[Mitsrayim](../../strongs/h/h4714.md), and [Yehuwdah](../../strongs/h/h3063.md), and ['Ĕḏōm](../../strongs/h/h123.md), and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md), and [Mô'āḇ](../../strongs/h/h4124.md), and all that are in the [qāṣaṣ](../../strongs/h/h7112.md) [pē'â](../../strongs/h/h6285.md), that [yashab](../../strongs/h/h3427.md) in the [midbar](../../strongs/h/h4057.md): for all these [gowy](../../strongs/h/h1471.md) are [ʿārēl](../../strongs/h/h6189.md), and all the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) are [ʿārēl](../../strongs/h/h6189.md) in the [leb](../../strongs/h/h3820.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 8](jeremiah_8.md) - [Jeremiah 10](jeremiah_10.md)