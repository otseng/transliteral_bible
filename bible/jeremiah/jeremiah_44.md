# [Jeremiah 44](https://www.blueletterbible.org/kjv/jeremiah/44)

<a name="jeremiah_44_1"></a>Jeremiah 44:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) concerning all the [Yᵊhûḏî](../../strongs/h/h3064.md) which [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), which [yashab](../../strongs/h/h3427.md) at [Miḡdôl](../../strongs/h/h4024.md), and at [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md), and at [Nōp̄](../../strongs/h/h5297.md), and in the ['erets](../../strongs/h/h776.md) of [Paṯrôs](../../strongs/h/h6624.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_44_2"></a>Jeremiah 44:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Ye have [ra'ah](../../strongs/h/h7200.md) all the [ra'](../../strongs/h/h7451.md) that I have [bow'](../../strongs/h/h935.md) upon [Yĕruwshalaim](../../strongs/h/h3389.md), and upon all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md); and, behold, this [yowm](../../strongs/h/h3117.md) they are a [chorbah](../../strongs/h/h2723.md), and no man [yashab](../../strongs/h/h3427.md) therein,

<a name="jeremiah_44_3"></a>Jeremiah 44:3

[paniym](../../strongs/h/h6440.md) of their [ra'](../../strongs/h/h7451.md) which they have ['asah](../../strongs/h/h6213.md) to [kāʿas](../../strongs/h/h3707.md) me, in that they [yālaḵ](../../strongs/h/h3212.md) to [qāṭar](../../strongs/h/h6999.md), and to ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), whom they [yada'](../../strongs/h/h3045.md) not, neither they, ye, nor your ['ab](../../strongs/h/h1.md).

<a name="jeremiah_44_4"></a>Jeremiah 44:4

Howbeit I [shalach](../../strongs/h/h7971.md) unto you all my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), [šāḵam](../../strongs/h/h7925.md) and [shalach](../../strongs/h/h7971.md) them, ['āmar](../../strongs/h/h559.md), Oh, ['asah](../../strongs/h/h6213.md) not this [tôʿēḇâ](../../strongs/h/h8441.md) [dabar](../../strongs/h/h1697.md) that I [sane'](../../strongs/h/h8130.md).

<a name="jeremiah_44_5"></a>Jeremiah 44:5

But they [shama'](../../strongs/h/h8085.md) not, nor [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md) to [shuwb](../../strongs/h/h7725.md) from their [ra'](../../strongs/h/h7451.md), to not [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_44_6"></a>Jeremiah 44:6

Wherefore my [chemah](../../strongs/h/h2534.md) and mine ['aph](../../strongs/h/h639.md) was [nāṯaḵ](../../strongs/h/h5413.md), and was [bāʿar](../../strongs/h/h1197.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); and they are [chorbah](../../strongs/h/h2723.md) and [šᵊmāmâ](../../strongs/h/h8077.md), as at this [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_44_7"></a>Jeremiah 44:7

Therefore now thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Wherefore ['asah](../../strongs/h/h6213.md) ye this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md) against your [nephesh](../../strongs/h/h5315.md), to [karath](../../strongs/h/h3772.md) from you ['iysh](../../strongs/h/h376.md) and ['ishshah](../../strongs/h/h802.md), ['owlel](../../strongs/h/h5768.md) and [yānaq](../../strongs/h/h3243.md), [tavek](../../strongs/h/h8432.md) of [Yehuwdah](../../strongs/h/h3063.md), to [yāṯar](../../strongs/h/h3498.md) you none to [šᵊ'ērîṯ](../../strongs/h/h7611.md);

<a name="jeremiah_44_8"></a>Jeremiah 44:8

In that ye provoke me unto [kāʿas](../../strongs/h/h3707.md) with the [ma'aseh](../../strongs/h/h4639.md) of your [yad](../../strongs/h/h3027.md), [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), whither ye be [bow'](../../strongs/h/h935.md) to [guwr](../../strongs/h/h1481.md), that ye might [karath](../../strongs/h/h3772.md) yourselves, and that ye might be a [qᵊlālâ](../../strongs/h/h7045.md) and a [cherpah](../../strongs/h/h2781.md) among all the [gowy](../../strongs/h/h1471.md) of the ['erets](../../strongs/h/h776.md)?

<a name="jeremiah_44_9"></a>Jeremiah 44:9

Have ye [shakach](../../strongs/h/h7911.md) the [ra'](../../strongs/h/h7451.md) of your ['ab](../../strongs/h/h1.md), and the [ra'](../../strongs/h/h7451.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [ra'](../../strongs/h/h7451.md) of their ['ishshah](../../strongs/h/h802.md), and your own [ra'](../../strongs/h/h7451.md), and the [ra'](../../strongs/h/h7451.md) of your ['ishshah](../../strongs/h/h802.md), which they have ['asah](../../strongs/h/h6213.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="jeremiah_44_10"></a>Jeremiah 44:10

They are not [dāḵā'](../../strongs/h/h1792.md) even unto this [yowm](../../strongs/h/h3117.md), neither have they [yare'](../../strongs/h/h3372.md), nor [halak](../../strongs/h/h1980.md) in my [towrah](../../strongs/h/h8451.md), nor in my [chuqqah](../../strongs/h/h2708.md), that I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you and [paniym](../../strongs/h/h6440.md) your ['ab](../../strongs/h/h1.md).

<a name="jeremiah_44_11"></a>Jeremiah 44:11

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [śûm](../../strongs/h/h7760.md) my [paniym](../../strongs/h/h6440.md) against you for [ra'](../../strongs/h/h7451.md), and to [karath](../../strongs/h/h3772.md) all [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_44_12"></a>Jeremiah 44:12

And I will [laqach](../../strongs/h/h3947.md) the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md), that have [śûm](../../strongs/h/h7760.md) their [paniym](../../strongs/h/h6440.md) to [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there, and they shall all be [tamam](../../strongs/h/h8552.md), and [naphal](../../strongs/h/h5307.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); they shall even be [tamam](../../strongs/h/h8552.md) by the [chereb](../../strongs/h/h2719.md) and by the [rāʿāḇ](../../strongs/h/h7458.md): they shall [muwth](../../strongs/h/h4191.md), from the [qāṭān](../../strongs/h/h6996.md) even unto the [gadowl](../../strongs/h/h1419.md), by the [chereb](../../strongs/h/h2719.md) and by the [rāʿāḇ](../../strongs/h/h7458.md): and they shall be an ['alah](../../strongs/h/h423.md), and a [šammâ](../../strongs/h/h8047.md), and a [qᵊlālâ](../../strongs/h/h7045.md), and a [cherpah](../../strongs/h/h2781.md).

<a name="jeremiah_44_13"></a>Jeremiah 44:13

For I will [paqad](../../strongs/h/h6485.md) them that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), as I have [paqad](../../strongs/h/h6485.md) [Yĕruwshalaim](../../strongs/h/h3389.md), by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md):

<a name="jeremiah_44_14"></a>Jeremiah 44:14

So that none of the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md), which are [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there, shall [pālîṭ](../../strongs/h/h6412.md) or [śārîḏ](../../strongs/h/h8300.md), that they should [shuwb](../../strongs/h/h7725.md) into the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), to the which they [nasa'](../../strongs/h/h5375.md) a [nephesh](../../strongs/h/h5315.md) to [shuwb](../../strongs/h/h7725.md) to [yashab](../../strongs/h/h3427.md) there: for none shall [shuwb](../../strongs/h/h7725.md) but such as shall [pallēṭ](../../strongs/h/h6405.md).

<a name="jeremiah_44_15"></a>Jeremiah 44:15

Then all the ['enowsh](../../strongs/h/h582.md) which [yada'](../../strongs/h/h3045.md) that their ['ishshah](../../strongs/h/h802.md) had [qāṭar](../../strongs/h/h6999.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and all the ['ishshah](../../strongs/h/h802.md) that stood ['amad](../../strongs/h/h5975.md), a [gadowl](../../strongs/h/h1419.md) [qāhēl](../../strongs/h/h6951.md), even all the ['am](../../strongs/h/h5971.md) that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), in [Paṯrôs](../../strongs/h/h6624.md), ['anah](../../strongs/h/h6030.md) [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_44_16"></a>Jeremiah 44:16

As for the [dabar](../../strongs/h/h1697.md) that thou hast [dabar](../../strongs/h/h1696.md) unto us in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), we will not [shama'](../../strongs/h/h8085.md) unto thee.

<a name="jeremiah_44_17"></a>Jeremiah 44:17

But we will ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) whatsoever [dabar](../../strongs/h/h1697.md) [yāṣā'](../../strongs/h/h3318.md) out of our own [peh](../../strongs/h/h6310.md), to [qāṭar](../../strongs/h/h6999.md) unto the [milkōṯ](../../strongs/h/h4446.md) of [shamayim](../../strongs/h/h8064.md), and to [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto her, as we have ['asah](../../strongs/h/h6213.md), we, and our ['ab](../../strongs/h/h1.md), our [melek](../../strongs/h/h4428.md), and our [śar](../../strongs/h/h8269.md), in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): for then had we [sāׂbaʿ](../../strongs/h/h7646.md) of [lechem](../../strongs/h/h3899.md), and were [towb](../../strongs/h/h2896.md), and [ra'ah](../../strongs/h/h7200.md) no [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_44_18"></a>Jeremiah 44:18

But since we [ḥāḏal](../../strongs/h/h2308.md) to [qāṭar](../../strongs/h/h6999.md) to the [milkōṯ](../../strongs/h/h4446.md) of [shamayim](../../strongs/h/h8064.md), and to [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto her, we have [ḥāsēr](../../strongs/h/h2637.md) all things, and have been [tamam](../../strongs/h/h8552.md) by the [chereb](../../strongs/h/h2719.md) and by the [rāʿāḇ](../../strongs/h/h7458.md).

<a name="jeremiah_44_19"></a>Jeremiah 44:19

And when we [qāṭar](../../strongs/h/h6999.md) to the [milkōṯ](../../strongs/h/h4446.md) of [shamayim](../../strongs/h/h8064.md), and [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto her, did we ['asah](../../strongs/h/h6213.md) her [kaûān](../../strongs/h/h3561.md) to [ʿāṣaḇ](../../strongs/h/h6087.md) her, and pour [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto her, [bilʿăḏê](../../strongs/h/h1107.md) our ['enowsh](../../strongs/h/h582.md)?

<a name="jeremiah_44_20"></a>Jeremiah 44:20

Then [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), to the [geḇer](../../strongs/h/h1397.md), and to the ['ishshah](../../strongs/h/h802.md), and to all the ['am](../../strongs/h/h5971.md) which had given him that [dabar](../../strongs/h/h1697.md) ['anah](../../strongs/h/h6030.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_44_21"></a>Jeremiah 44:21

The [qiṭṭēr](../../strongs/h/h7002.md) that ye [qāṭar](../../strongs/h/h6999.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ye, and your ['ab](../../strongs/h/h1.md), your [melek](../../strongs/h/h4428.md), and your [śar](../../strongs/h/h8269.md), and the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), did not [Yĕhovah](../../strongs/h/h3068.md) [zakar](../../strongs/h/h2142.md) them, and [ʿālâ](../../strongs/h/h5927.md) it not into his [leb](../../strongs/h/h3820.md)?

<a name="jeremiah_44_22"></a>Jeremiah 44:22

So that [Yĕhovah](../../strongs/h/h3068.md) [yakol](../../strongs/h/h3201.md) no longer [nasa'](../../strongs/h/h5375.md), [paniym](../../strongs/h/h6440.md) of the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md), and [paniym](../../strongs/h/h6440.md) of the [tôʿēḇâ](../../strongs/h/h8441.md) which ye have ['asah](../../strongs/h/h6213.md); therefore is your ['erets](../../strongs/h/h776.md) a [chorbah](../../strongs/h/h2723.md), and a [šammâ](../../strongs/h/h8047.md), and a [qᵊlālâ](../../strongs/h/h7045.md), without a [yashab](../../strongs/h/h3427.md), as at this [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_44_23"></a>Jeremiah 44:23

[paniym](../../strongs/h/h6440.md) ['ăšer](../../strongs/h/h834.md) ye have [qāṭar](../../strongs/h/h6999.md), and because ye have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), and have not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), nor [halak](../../strongs/h/h1980.md) in his [towrah](../../strongs/h/h8451.md), nor in his [chuqqah](../../strongs/h/h2708.md), nor in his [ʿēḏûṯ](../../strongs/h/h5715.md); therefore this [ra'](../../strongs/h/h7451.md) is [qārā'](../../strongs/h/h7122.md) unto you, as at this [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_44_24"></a>Jeremiah 44:24

Moreover [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto all the ['am](../../strongs/h/h5971.md), and to all the ['ishshah](../../strongs/h/h802.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), all [Yehuwdah](../../strongs/h/h3063.md) that are in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="jeremiah_44_25"></a>Jeremiah 44:25

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md); Ye and your ['ishshah](../../strongs/h/h802.md) have both [dabar](../../strongs/h/h1696.md) with your [peh](../../strongs/h/h6310.md), and [mālā'](../../strongs/h/h4390.md) with your [yad](../../strongs/h/h3027.md), ['āmar](../../strongs/h/h559.md), We will ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) our [neḏer](../../strongs/h/h5088.md) that we have [nāḏar](../../strongs/h/h5087.md), to burn [qāṭar](../../strongs/h/h6999.md) to the [milkōṯ](../../strongs/h/h4446.md) of [shamayim](../../strongs/h/h8064.md), and to [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto her: ye will [quwm](../../strongs/h/h6965.md) [quwm](../../strongs/h/h6965.md) your [neḏer](../../strongs/h/h5088.md), and ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) your [neḏer](../../strongs/h/h5088.md).

<a name="jeremiah_44_26"></a>Jeremiah 44:26

Therefore [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), all [Yehuwdah](../../strongs/h/h3063.md) that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); Behold, I have [shaba'](../../strongs/h/h7650.md) by my [gadowl](../../strongs/h/h1419.md) [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), that my [shem](../../strongs/h/h8034.md) shall no more be [qara'](../../strongs/h/h7121.md) in the [peh](../../strongs/h/h6310.md) of any ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) in all the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), ['āmar](../../strongs/h/h559.md), The ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) [chay](../../strongs/h/h2416.md).

<a name="jeremiah_44_27"></a>Jeremiah 44:27

Behold, I will [šāqaḏ](../../strongs/h/h8245.md) over them for [ra'](../../strongs/h/h7451.md), and not for [towb](../../strongs/h/h2896.md): and all the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) that are in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) shall be [tamam](../../strongs/h/h8552.md) by the [chereb](../../strongs/h/h2719.md) and by the [rāʿāḇ](../../strongs/h/h7458.md), until there be a [kalah](../../strongs/h/h3615.md) of them.

<a name="jeremiah_44_28"></a>Jeremiah 44:28

Yet a [math](../../strongs/h/h4962.md) [mispār](../../strongs/h/h4557.md) that [pālîṭ](../../strongs/h/h6412.md) the [chereb](../../strongs/h/h2719.md) shall [shuwb](../../strongs/h/h7725.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) into the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md), that are [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there, shall [yada'](../../strongs/h/h3045.md) whose [dabar](../../strongs/h/h1697.md) shall [quwm](../../strongs/h/h6965.md), mine, or theirs.

<a name="jeremiah_44_29"></a>Jeremiah 44:29

And this shall be a ['ôṯ](../../strongs/h/h226.md) unto you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [paqad](../../strongs/h/h6485.md) you in this [maqowm](../../strongs/h/h4725.md), that ye may [yada'](../../strongs/h/h3045.md) that my [dabar](../../strongs/h/h1697.md) shall [quwm](../../strongs/h/h6965.md) [quwm](../../strongs/h/h6965.md) against you for [ra'](../../strongs/h/h7451.md):

<a name="jeremiah_44_30"></a>Jeremiah 44:30

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [nathan](../../strongs/h/h5414.md) [ParʿÔ Ḥāp̄Raʿ](../../strongs/h/h6548.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) into the [yad](../../strongs/h/h3027.md) of his ['oyeb](../../strongs/h/h341.md), and into the [yad](../../strongs/h/h3027.md) of them that [bāqaš](../../strongs/h/h1245.md) his [nephesh](../../strongs/h/h5315.md); as I [nathan](../../strongs/h/h5414.md) [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), his ['oyeb](../../strongs/h/h341.md), and that [bāqaš](../../strongs/h/h1245.md) his [nephesh](../../strongs/h/h5315.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 43](jeremiah_43.md) - [Jeremiah 45](jeremiah_45.md)