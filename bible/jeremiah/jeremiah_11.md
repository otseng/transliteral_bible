# [Jeremiah 11](https://www.blueletterbible.org/kjv/jeremiah/11)

<a name="jeremiah_11_1"></a>Jeremiah 11:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_11_2"></a>Jeremiah 11:2

[shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md), and [dabar](../../strongs/h/h1696.md) unto the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="jeremiah_11_3"></a>Jeremiah 11:3

And ['āmar](../../strongs/h/h559.md) thou unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); ['arar](../../strongs/h/h779.md) be the ['iysh](../../strongs/h/h376.md) that [shama'](../../strongs/h/h8085.md) not the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md),

<a name="jeremiah_11_4"></a>Jeremiah 11:4

Which I [tsavah](../../strongs/h/h6680.md) your ['ab](../../strongs/h/h1.md) in the [yowm](../../strongs/h/h3117.md) that I brought them [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), from the [barzel](../../strongs/h/h1270.md) [kûr](../../strongs/h/h3564.md), ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), and ['asah](../../strongs/h/h6213.md) them, according to all which I [tsavah](../../strongs/h/h6680.md) you: so shall ye be my ['am](../../strongs/h/h5971.md), and I will be your ['Elohiym](../../strongs/h/h430.md):

<a name="jeremiah_11_5"></a>Jeremiah 11:5

That I may [quwm](../../strongs/h/h6965.md) the [šᵊḇûʿâ](../../strongs/h/h7621.md) which I have [shaba'](../../strongs/h/h7650.md) unto your ['ab](../../strongs/h/h1.md), to [nathan](../../strongs/h/h5414.md) them an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md), as it is this [yowm](../../strongs/h/h3117.md). Then ['anah](../../strongs/h/h6030.md) I, and ['āmar](../../strongs/h/h559.md), So be ['amen](../../strongs/h/h543.md), [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_11_6"></a>Jeremiah 11:6

Then [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [qara'](../../strongs/h/h7121.md) all these [dabar](../../strongs/h/h1697.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md), and ['asah](../../strongs/h/h6213.md) them.

<a name="jeremiah_11_7"></a>Jeremiah 11:7

For I [ʿûḏ](../../strongs/h/h5749.md) [ʿûḏ](../../strongs/h/h5749.md) unto your ['ab](../../strongs/h/h1.md) in the [yowm](../../strongs/h/h3117.md) that I [ʿālâ](../../strongs/h/h5927.md) them out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), even unto this [yowm](../../strongs/h/h3117.md), rising [šāḵam](../../strongs/h/h7925.md) and [ʿûḏ](../../strongs/h/h5749.md), ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md).

<a name="jeremiah_11_8"></a>Jeremiah 11:8

Yet they [shama'](../../strongs/h/h8085.md) not, nor [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md), but [yālaḵ](../../strongs/h/h3212.md) every ['iysh](../../strongs/h/h376.md) in the [šᵊrîrûṯ](../../strongs/h/h8307.md) of their [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md): therefore I will [bow'](../../strongs/h/h935.md) upon them all the [dabar](../../strongs/h/h1697.md) of this [bĕriyth](../../strongs/h/h1285.md), which I [tsavah](../../strongs/h/h6680.md) them to ['asah](../../strongs/h/h6213.md); but they ['asah](../../strongs/h/h6213.md) them not.

<a name="jeremiah_11_9"></a>Jeremiah 11:9

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, A [qešer](../../strongs/h/h7195.md) is [māṣā'](../../strongs/h/h4672.md) among the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and among the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_11_10"></a>Jeremiah 11:10

They are [shuwb](../../strongs/h/h7725.md) to the ['avon](../../strongs/h/h5771.md) of their [ri'šôn](../../strongs/h/h7223.md) ['ab](../../strongs/h/h1.md), which [mā'ēn](../../strongs/h/h3985.md) to [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md); and they [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to ['abad](../../strongs/h/h5647.md) them: the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) have [pārar](../../strongs/h/h6565.md) my [bĕriyth](../../strongs/h/h1285.md) which I [karath](../../strongs/h/h3772.md) with their ['ab](../../strongs/h/h1.md).

<a name="jeremiah_11_11"></a>Jeremiah 11:11

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon them, which they shall not be [yakol](../../strongs/h/h3201.md) to [yāṣā'](../../strongs/h/h3318.md); and though they shall [zāʿaq](../../strongs/h/h2199.md) unto me, I will not [shama'](../../strongs/h/h8085.md) unto them.

<a name="jeremiah_11_12"></a>Jeremiah 11:12

Then shall the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) and [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [halak](../../strongs/h/h1980.md), and [zāʿaq](../../strongs/h/h2199.md) unto the ['Elohiym](../../strongs/h/h430.md) unto whom they offer [qāṭar](../../strongs/h/h6999.md): but they shall not [yasha'](../../strongs/h/h3467.md) them at [yasha'](../../strongs/h/h3467.md) in the [ʿēṯ](../../strongs/h/h6256.md) of their [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_11_13"></a>Jeremiah 11:13

For according to the [mispār](../../strongs/h/h4557.md) of thy [ʿîr](../../strongs/h/h5892.md) were thy ['Elohiym](../../strongs/h/h430.md), O [Yehuwdah](../../strongs/h/h3063.md); and according to the [mispār](../../strongs/h/h4557.md) of the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) have ye set [śûm](../../strongs/h/h7760.md) [mizbeach](../../strongs/h/h4196.md) to that [bšeṯ](../../strongs/h/h1322.md), even [mizbeach](../../strongs/h/h4196.md) to [qāṭar](../../strongs/h/h6999.md) unto [BaʿAl](../../strongs/h/h1168.md).

<a name="jeremiah_11_14"></a>Jeremiah 11:14

Therefore [palal](../../strongs/h/h6419.md) not thou for this ['am](../../strongs/h/h5971.md), neither [nasa'](../../strongs/h/h5375.md) a [rinnah](../../strongs/h/h7440.md) or [tĕphillah](../../strongs/h/h8605.md) for them: for I will not [shama'](../../strongs/h/h8085.md) them in the [ʿēṯ](../../strongs/h/h6256.md) that they [qara'](../../strongs/h/h7121.md) unto me for their [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_11_15"></a>Jeremiah 11:15

What hath my [yāḏîḏ](../../strongs/h/h3039.md) to do in mine [bayith](../../strongs/h/h1004.md), seeing she hath ['asah](../../strongs/h/h6213.md) [mezimmah](../../strongs/h/h4209.md) with [rab](../../strongs/h/h7227.md), and the [qodesh](../../strongs/h/h6944.md) [basar](../../strongs/h/h1320.md) is ['abar](../../strongs/h/h5674.md) from thee? when thou doest [ra'](../../strongs/h/h7451.md), then thou [ʿālaz](../../strongs/h/h5937.md).

<a name="jeremiah_11_16"></a>Jeremiah 11:16

[Yĕhovah](../../strongs/h/h3068.md) [qara'](../../strongs/h/h7121.md) thy [shem](../../strongs/h/h8034.md), A [raʿănān](../../strongs/h/h7488.md) olive [zayiṯ](../../strongs/h/h2132.md), [yāp̄ê](../../strongs/h/h3303.md), and of [tō'ar](../../strongs/h/h8389.md) [pĕriy](../../strongs/h/h6529.md): with the [qowl](../../strongs/h/h6963.md) of a [gadowl](../../strongs/h/h1419.md) [hămullâ](../../strongs/h/h1999.md) he hath [yāṣaṯ](../../strongs/h/h3341.md) ['esh](../../strongs/h/h784.md) upon it, and the [dālîṯ](../../strongs/h/h1808.md) of it are [ra'a'](../../strongs/h/h7489.md).

<a name="jeremiah_11_17"></a>Jeremiah 11:17

For [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that [nāṭaʿ](../../strongs/h/h5193.md) thee, hath [dabar](../../strongs/h/h1696.md) [ra'](../../strongs/h/h7451.md) against thee, [gālāl](../../strongs/h/h1558.md) the [ra'](../../strongs/h/h7451.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and of the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), which they have ['asah](../../strongs/h/h6213.md) against themselves to provoke me to [kāʿas](../../strongs/h/h3707.md) in [qāṭar](../../strongs/h/h6999.md) unto [BaʿAl](../../strongs/h/h1168.md).

<a name="jeremiah_11_18"></a>Jeremiah 11:18

And [Yĕhovah](../../strongs/h/h3068.md) hath given me [yada'](../../strongs/h/h3045.md) of it, and I [yada'](../../strongs/h/h3045.md) it: then thou [ra'ah](../../strongs/h/h7200.md) me their [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_11_19"></a>Jeremiah 11:19

But I was like a [keḇeś](../../strongs/h/h3532.md) or an ['allûp̄](../../strongs/h/h441.md) that is [yāḇal](../../strongs/h/h2986.md) to the [ṭāḇaḥ](../../strongs/h/h2873.md); and I [yada'](../../strongs/h/h3045.md) not that they had [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md) against me, saying, Let us [shachath](../../strongs/h/h7843.md) the ['ets](../../strongs/h/h6086.md) with the [lechem](../../strongs/h/h3899.md) thereof, and let us [karath](../../strongs/h/h3772.md) him from the ['erets](../../strongs/h/h776.md) of the [chay](../../strongs/h/h2416.md), that his [shem](../../strongs/h/h8034.md) may be no more [zakar](../../strongs/h/h2142.md).

<a name="jeremiah_11_20"></a>Jeremiah 11:20

But, [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that [shaphat](../../strongs/h/h8199.md) [tsedeq](../../strongs/h/h6664.md), that [bachan](../../strongs/h/h974.md) the [kilyah](../../strongs/h/h3629.md) and the [leb](../../strongs/h/h3820.md), let me [ra'ah](../../strongs/h/h7200.md) thy [nᵊqāmâ](../../strongs/h/h5360.md) on them: for unto thee have I [gālâ](../../strongs/h/h1540.md) my [rîḇ](../../strongs/h/h7379.md).

<a name="jeremiah_11_21"></a>Jeremiah 11:21

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of the ['enowsh](../../strongs/h/h582.md) of [ʿĂnāṯôṯ](../../strongs/h/h6068.md), that [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md), ['āmar](../../strongs/h/h559.md), [nāḇā'](../../strongs/h/h5012.md) not in the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), that thou [muwth](../../strongs/h/h4191.md) not by our [yad](../../strongs/h/h3027.md):

<a name="jeremiah_11_22"></a>Jeremiah 11:22

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), Behold, I will [paqad](../../strongs/h/h6485.md) them: the [bāḥûr](../../strongs/h/h970.md) shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md); their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) shall [muwth](../../strongs/h/h4191.md) by [rāʿāḇ](../../strongs/h/h7458.md):

<a name="jeremiah_11_23"></a>Jeremiah 11:23

And there shall be no [šᵊ'ērîṯ](../../strongs/h/h7611.md) of them: for I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon the ['enowsh](../../strongs/h/h582.md) of [ʿĂnāṯôṯ](../../strongs/h/h6068.md), even the [šānâ](../../strongs/h/h8141.md) of their [pᵊqudâ](../../strongs/h/h6486.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 10](jeremiah_10.md) - [Jeremiah 12](jeremiah_12.md)