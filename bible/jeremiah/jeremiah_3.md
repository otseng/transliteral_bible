# [Jeremiah 3](https://www.blueletterbible.org/kjv/jeremiah/3)

<a name="jeremiah_3_1"></a>Jeremiah 3:1

They ['āmar](../../strongs/h/h559.md), If an ['iysh](../../strongs/h/h376.md) [shalach](../../strongs/h/h7971.md) his ['ishshah](../../strongs/h/h802.md), and she [halak](../../strongs/h/h1980.md) from him, and become ['aḥēr](../../strongs/h/h312.md) ['iysh](../../strongs/h/h376.md), shall he return unto her [shuwb](../../strongs/h/h7725.md)? shall not that ['erets](../../strongs/h/h776.md) be [ḥānēp̄](../../strongs/h/h2610.md) [ḥānēp̄](../../strongs/h/h2610.md)? but thou hast [zānâ](../../strongs/h/h2181.md) with [rab](../../strongs/h/h7227.md) [rea'](../../strongs/h/h7453.md); yet [shuwb](../../strongs/h/h7725.md) to me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_3_2"></a>Jeremiah 3:2

[nasa'](../../strongs/h/h5375.md) thine ['ayin](../../strongs/h/h5869.md) unto the high [šᵊp̄î](../../strongs/h/h8205.md), and [ra'ah](../../strongs/h/h7200.md) ['êp̄ô](../../strongs/h/h375.md) thou hast not been [šāḡal](../../strongs/h/h7693.md) [shakab](../../strongs/h/h7901.md) with. In the [derek](../../strongs/h/h1870.md) hast thou [yashab](../../strongs/h/h3427.md) for them, as the [ʿĂrāḇî](../../strongs/h/h6163.md) in the [midbar](../../strongs/h/h4057.md); and thou hast [ḥānēp̄](../../strongs/h/h2610.md) the ['erets](../../strongs/h/h776.md) with thy [zᵊnûṯ](../../strongs/h/h2184.md) and with thy [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_3_3"></a>Jeremiah 3:3

Therefore the [rᵊḇîḇîm](../../strongs/h/h7241.md) have been [mānaʿ](../../strongs/h/h4513.md), and there hath been no [malqôš](../../strongs/h/h4456.md); and thou hadst a [zānâ](../../strongs/h/h2181.md) ['ishshah](../../strongs/h/h802.md) [mēṣaḥ](../../strongs/h/h4696.md), thou [mā'ēn](../../strongs/h/h3985.md) to be [kālam](../../strongs/h/h3637.md).

<a name="jeremiah_3_4"></a>Jeremiah 3:4

Wilt thou not from this time [qara'](../../strongs/h/h7121.md) unto me, My ['ab](../../strongs/h/h1.md), thou art the ['allûp̄](../../strongs/h/h441.md) of my [nāʿur](../../strongs/h/h5271.md)?

<a name="jeremiah_3_5"></a>Jeremiah 3:5

Will he [nāṭar](../../strongs/h/h5201.md) his anger ['owlam](../../strongs/h/h5769.md)? will he [shamar](../../strongs/h/h8104.md) it to the [netsach](../../strongs/h/h5331.md)? Behold, thou hast [dabar](../../strongs/h/h1696.md) and ['asah](../../strongs/h/h6213.md) evil [ra'](../../strongs/h/h7451.md) as thou [yakol](../../strongs/h/h3201.md).

<a name="jeremiah_3_6"></a>Jeremiah 3:6

[Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) also unto me in the [yowm](../../strongs/h/h3117.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) the [melek](../../strongs/h/h4428.md), Hast thou [ra'ah](../../strongs/h/h7200.md) that which [mᵊšûḇâ](../../strongs/h/h4878.md) [Yisra'el](../../strongs/h/h3478.md) hath ['asah](../../strongs/h/h6213.md)? she is [halak](../../strongs/h/h1980.md) upon every [gāḇōha](../../strongs/h/h1364.md) [har](../../strongs/h/h2022.md) and [taḥaṯ](../../strongs/h/h8478.md) every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md), and there hath [zānâ](../../strongs/h/h2181.md).

<a name="jeremiah_3_7"></a>Jeremiah 3:7

And I ['āmar](../../strongs/h/h559.md) ['aḥar](../../strongs/h/h310.md) she had ['asah](../../strongs/h/h6213.md) all these things, [shuwb](../../strongs/h/h7725.md) thou unto me. But she [shuwb](../../strongs/h/h7725.md) not. And her [bāḡôḏ](../../strongs/h/h901.md) ['āḥôṯ](../../strongs/h/h269.md) [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) it.

<a name="jeremiah_3_8"></a>Jeremiah 3:8

And I [ra'ah](../../strongs/h/h7200.md), when for all the ['ôḏôṯ](../../strongs/h/h182.md) whereby [mᵊšûḇâ](../../strongs/h/h4878.md) [Yisra'el](../../strongs/h/h3478.md) [na'aph](../../strongs/h/h5003.md) I had put her [shalach](../../strongs/h/h7971.md), and [nathan](../../strongs/h/h5414.md) her a [sēp̄er](../../strongs/h/h5612.md) of [kᵊrîṯûṯ](../../strongs/h/h3748.md); yet her [bāḡaḏ](../../strongs/h/h898.md) ['āḥôṯ](../../strongs/h/h269.md) [Yehuwdah](../../strongs/h/h3063.md) [yare'](../../strongs/h/h3372.md) not, but [yālaḵ](../../strongs/h/h3212.md) and [zānâ](../../strongs/h/h2181.md) also.

<a name="jeremiah_3_9"></a>Jeremiah 3:9

And it came to pass through the [qowl](../../strongs/h/h6963.md) of her [zᵊnûṯ](../../strongs/h/h2184.md), that she [ḥānēp̄](../../strongs/h/h2610.md) the ['erets](../../strongs/h/h776.md), and committed [na'aph](../../strongs/h/h5003.md) with ['eben](../../strongs/h/h68.md) and with ['ets](../../strongs/h/h6086.md).

<a name="jeremiah_3_10"></a>Jeremiah 3:10

And yet for all this her [bāḡôḏ](../../strongs/h/h901.md) ['āḥôṯ](../../strongs/h/h269.md) [Yehuwdah](../../strongs/h/h3063.md) hath not [shuwb](../../strongs/h/h7725.md) unto me with her whole [leb](../../strongs/h/h3820.md), but [sheqer](../../strongs/h/h8267.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_3_11"></a>Jeremiah 3:11

And [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, The [mᵊšûḇâ](../../strongs/h/h4878.md) [Yisra'el](../../strongs/h/h3478.md) hath [ṣāḏaq](../../strongs/h/h6663.md) [nephesh](../../strongs/h/h5315.md) more than [bāḡaḏ](../../strongs/h/h898.md) [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_3_12"></a>Jeremiah 3:12

[halak](../../strongs/h/h1980.md) and [qara'](../../strongs/h/h7121.md) these [dabar](../../strongs/h/h1697.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md), and ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md), thou [mᵊšûḇâ](../../strongs/h/h4878.md) [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and I will not cause mine [paniym](../../strongs/h/h6440.md) to [naphal](../../strongs/h/h5307.md) upon you: for I am [chaciyd](../../strongs/h/h2623.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and I will not [nāṭar](../../strongs/h/h5201.md) anger ['owlam](../../strongs/h/h5769.md).

<a name="jeremiah_3_13"></a>Jeremiah 3:13

Only [yada'](../../strongs/h/h3045.md) thine ['avon](../../strongs/h/h5771.md), that thou hast [pāšaʿ](../../strongs/h/h6586.md) against [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and hast [p̄zr](../../strongs/h/h6340.md) thy [derek](../../strongs/h/h1870.md) to the [zûr](../../strongs/h/h2114.md) under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md), and ye have not [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_3_14"></a>Jeremiah 3:14

[shuwb](../../strongs/h/h7725.md), O [šôḇāḇ](../../strongs/h/h7726.md) [ben](../../strongs/h/h1121.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); for I am [bāʿal](../../strongs/h/h1166.md) unto you: and I will [laqach](../../strongs/h/h3947.md) you ['echad](../../strongs/h/h259.md) of a [ʿîr](../../strongs/h/h5892.md), and [šᵊnayim](../../strongs/h/h8147.md) of a [mišpāḥâ](../../strongs/h/h4940.md), and I will [bow'](../../strongs/h/h935.md) you to [Tsiyown](../../strongs/h/h6726.md):

<a name="jeremiah_3_15"></a>Jeremiah 3:15

And I will [nathan](../../strongs/h/h5414.md) you [ra'ah](../../strongs/h/h7462.md) according to mine [leb](../../strongs/h/h3820.md), which shall [ra'ah](../../strongs/h/h7462.md) you with [dēʿâ](../../strongs/h/h1844.md) and [sakal](../../strongs/h/h7919.md).

<a name="jeremiah_3_16"></a>Jeremiah 3:16

And it shall come to pass, when ye be [rabah](../../strongs/h/h7235.md) and [parah](../../strongs/h/h6509.md) in the ['erets](../../strongs/h/h776.md), in those [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), they shall ['āmar](../../strongs/h/h559.md) no more, The ['ārôn](../../strongs/h/h727.md) of the [bĕriyth](../../strongs/h/h1285.md) of [Yĕhovah](../../strongs/h/h3068.md): neither shall it [ʿālâ](../../strongs/h/h5927.md) to [leb](../../strongs/h/h3820.md): neither shall they [zakar](../../strongs/h/h2142.md) it; neither shall they [paqad](../../strongs/h/h6485.md) it; neither shall that be ['asah](../../strongs/h/h6213.md) any more.

<a name="jeremiah_3_17"></a>Jeremiah 3:17

At that [ʿēṯ](../../strongs/h/h6256.md) they shall [qara'](../../strongs/h/h7121.md) [Yĕruwshalaim](../../strongs/h/h3389.md) the [kicce'](../../strongs/h/h3678.md) of [Yĕhovah](../../strongs/h/h3068.md); and all the [gowy](../../strongs/h/h1471.md) shall be [qāvâ](../../strongs/h/h6960.md) unto it, to the [shem](../../strongs/h/h8034.md) of [Yĕhovah](../../strongs/h/h3068.md), to [Yĕruwshalaim](../../strongs/h/h3389.md): neither shall they [yālaḵ](../../strongs/h/h3212.md) any more ['aḥar](../../strongs/h/h310.md) the [šᵊrîrûṯ](../../strongs/h/h8307.md) of their [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md).

<a name="jeremiah_3_18"></a>Jeremiah 3:18

In those [yowm](../../strongs/h/h3117.md) the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) shall [yālaḵ](../../strongs/h/h3212.md) with the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and they shall [bow'](../../strongs/h/h935.md) [yaḥaḏ](../../strongs/h/h3162.md) out of the ['erets](../../strongs/h/h776.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md) to the ['erets](../../strongs/h/h776.md) that I have given for a [nāḥal](../../strongs/h/h5157.md) unto your ['ab](../../strongs/h/h1.md).

<a name="jeremiah_3_19"></a>Jeremiah 3:19

But I ['āmar](../../strongs/h/h559.md), How shall I [shiyth](../../strongs/h/h7896.md) thee among the [ben](../../strongs/h/h1121.md), and [nathan](../../strongs/h/h5414.md) thee a [ḥemdâ](../../strongs/h/h2532.md) ['erets](../../strongs/h/h776.md), a [ṣᵊḇî](../../strongs/h/h6643.md) [nachalah](../../strongs/h/h5159.md) of the [tsaba'](../../strongs/h/h6635.md) of [gowy](../../strongs/h/h1471.md)? and I ['āmar](../../strongs/h/h559.md), Thou shalt [qara'](../../strongs/h/h7121.md) me, My ['ab](../../strongs/h/h1.md); and shalt not [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md).

<a name="jeremiah_3_20"></a>Jeremiah 3:20

['āḵēn](../../strongs/h/h403.md) as an ['ishshah](../../strongs/h/h802.md) [bāḡaḏ](../../strongs/h/h898.md) departeth from her [rea'](../../strongs/h/h7453.md), so have ye dealt [bāḡaḏ](../../strongs/h/h898.md) with me, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_3_21"></a>Jeremiah 3:21

A [qowl](../../strongs/h/h6963.md) was [shama'](../../strongs/h/h8085.md) upon the [šᵊp̄î](../../strongs/h/h8205.md), [bĕkiy](../../strongs/h/h1065.md) and [taḥănûn](../../strongs/h/h8469.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md): for they have [ʿāvâ](../../strongs/h/h5753.md) their [derek](../../strongs/h/h1870.md), and they have [shakach](../../strongs/h/h7911.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_3_22"></a>Jeremiah 3:22

[shuwb](../../strongs/h/h7725.md), ye [šôḇāḇ](../../strongs/h/h7726.md) [ben](../../strongs/h/h1121.md), and I will [rapha'](../../strongs/h/h7495.md) your [mᵊšûḇâ](../../strongs/h/h4878.md). Behold, we ['āṯâ](../../strongs/h/h857.md) unto thee; for thou art [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_3_23"></a>Jeremiah 3:23

['āḵēn](../../strongs/h/h403.md) in [sheqer](../../strongs/h/h8267.md) the [giḇʿâ](../../strongs/h/h1389.md), and from the [hāmôn](../../strongs/h/h1995.md) of [har](../../strongs/h/h2022.md): ['āḵēn](../../strongs/h/h403.md) in [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) is the [tᵊšûʿâ](../../strongs/h/h8668.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_3_24"></a>Jeremiah 3:24

For [bšeṯ](../../strongs/h/h1322.md) hath ['akal](../../strongs/h/h398.md) the [yᵊḡîaʿ](../../strongs/h/h3018.md) of our ['ab](../../strongs/h/h1.md) from our [nāʿur](../../strongs/h/h5271.md); their [tso'n](../../strongs/h/h6629.md) and their [bāqār](../../strongs/h/h1241.md), their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md).

<a name="jeremiah_3_25"></a>Jeremiah 3:25

We [shakab](../../strongs/h/h7901.md) in our [bšeṯ](../../strongs/h/h1322.md), and our [kĕlimmah](../../strongs/h/h3639.md) [kāsâ](../../strongs/h/h3680.md) us: for we have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), we and our ['ab](../../strongs/h/h1.md), from our [nāʿur](../../strongs/h/h5271.md) even unto this [yowm](../../strongs/h/h3117.md), and have not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 2](jeremiah_2.md) - [Jeremiah 4](jeremiah_4.md)