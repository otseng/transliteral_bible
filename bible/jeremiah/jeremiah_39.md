# [Jeremiah 39](https://www.blueletterbible.org/kjv/jeremiah/39)

<a name="jeremiah_39_1"></a>Jeremiah 39:1

In the [tᵊšîʿî](../../strongs/h/h8671.md) [šānâ](../../strongs/h/h8141.md) of [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the [ʿăśîrî](../../strongs/h/h6224.md) [ḥōḏeš](../../strongs/h/h2320.md), [bow'](../../strongs/h/h935.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) and all his [ḥayil](../../strongs/h/h2428.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and they [ṣûr](../../strongs/h/h6696.md) it.

<a name="jeremiah_39_2"></a>Jeremiah 39:2

And in the [ʿaštê](../../strongs/h/h6249.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of [Ṣḏqyh](../../strongs/h/h6667.md), in the [rᵊḇîʿî](../../strongs/h/h7243.md) [ḥōḏeš](../../strongs/h/h2320.md), the [tēšaʿ](../../strongs/h/h8672.md) day of the [ḥōḏeš](../../strongs/h/h2320.md), the [ʿîr](../../strongs/h/h5892.md) was [bāqaʿ](../../strongs/h/h1234.md).

<a name="jeremiah_39_3"></a>Jeremiah 39:3

And all the [śar](../../strongs/h/h8269.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md), and [yashab](../../strongs/h/h3427.md) in the [tavek](../../strongs/h/h8432.md) [sha'ar](../../strongs/h/h8179.md), even [Nērḡal Šar'Eṣer](../../strongs/h/h5371.md), [Samgar-Nᵊḇû](../../strongs/h/h5562.md), [Śarsᵊḵîm](../../strongs/h/h8310.md), [Rḇ-Srys](../../strongs/h/h7249.md), [Nērḡal Šar'Eṣer](../../strongs/h/h5371.md), [rḇ mḡ](../../strongs/h/h7248.md), with all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the [śar](../../strongs/h/h8269.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_39_4"></a>Jeremiah 39:4

And it came to pass, that when [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [ra'ah](../../strongs/h/h7200.md) them, and all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), then they [bāraḥ](../../strongs/h/h1272.md), and [yāṣā'](../../strongs/h/h3318.md) out of the [ʿîr](../../strongs/h/h5892.md) by [layil](../../strongs/h/h3915.md), by the [derek](../../strongs/h/h1870.md) of the [melek](../../strongs/h/h4428.md) [gan](../../strongs/h/h1588.md), by the [sha'ar](../../strongs/h/h8179.md) betwixt the two [ḥômâ](../../strongs/h/h2346.md): and he [yāṣā'](../../strongs/h/h3318.md) the [derek](../../strongs/h/h1870.md) of the ['arabah](../../strongs/h/h6160.md).

<a name="jeremiah_39_5"></a>Jeremiah 39:5

But the [Kaśdîmâ](../../strongs/h/h3778.md) [ḥayil](../../strongs/h/h2428.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) them, and [nāśaḡ](../../strongs/h/h5381.md) [Ṣḏqyh](../../strongs/h/h6667.md) in the ['arabah](../../strongs/h/h6160.md) of [Yᵊrēḥô](../../strongs/h/h3405.md): and when they had [laqach](../../strongs/h/h3947.md) him, they [ʿālâ](../../strongs/h/h5927.md) him to [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) to [Riḇlâ](../../strongs/h/h7247.md) in the ['erets](../../strongs/h/h776.md) of [Ḥămāṯ](../../strongs/h/h2574.md), where he [dabar](../../strongs/h/h1696.md) [mishpat](../../strongs/h/h4941.md) upon him.

<a name="jeremiah_39_6"></a>Jeremiah 39:6

Then the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [šāḥaṭ](../../strongs/h/h7819.md) the [ben](../../strongs/h/h1121.md) of [Ṣḏqyh](../../strongs/h/h6667.md) in [Riḇlâ](../../strongs/h/h7247.md) before his ['ayin](../../strongs/h/h5869.md): also the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [šāḥaṭ](../../strongs/h/h7819.md) all the [ḥōr](../../strongs/h/h2715.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_39_7"></a>Jeremiah 39:7

Moreover he put [ʿāvar](../../strongs/h/h5786.md) [Ṣḏqyh](../../strongs/h/h6667.md) ['ayin](../../strongs/h/h5869.md), and ['āsar](../../strongs/h/h631.md) him with [nᵊḥšeṯ](../../strongs/h/h5178.md), to [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_39_8"></a>Jeremiah 39:8

And the [Kaśdîmâ](../../strongs/h/h3778.md) [śārap̄](../../strongs/h/h8313.md) the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and the [bayith](../../strongs/h/h1004.md) of the ['am](../../strongs/h/h5971.md), with ['esh](../../strongs/h/h784.md), and [nāṯaṣ](../../strongs/h/h5422.md) the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_39_9"></a>Jeremiah 39:9

Then [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [gālâ](../../strongs/h/h1540.md) into [Bāḇel](../../strongs/h/h894.md) the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) that [šā'ar](../../strongs/h/h7604.md) in the [ʿîr](../../strongs/h/h5892.md), and those that fell [naphal](../../strongs/h/h5307.md), that [naphal](../../strongs/h/h5307.md) to him, with the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) that [šā'ar](../../strongs/h/h7604.md).

<a name="jeremiah_39_10"></a>Jeremiah 39:10

But [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [šā'ar](../../strongs/h/h7604.md) of the [dal](../../strongs/h/h1800.md) of the ['am](../../strongs/h/h5971.md), which had [mᵊ'ûmâ](../../strongs/h/h3972.md), in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), and [nathan](../../strongs/h/h5414.md) them [kerem](../../strongs/h/h3754.md) and [yāḡēḇ](../../strongs/h/h3010.md) at the same [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_39_11"></a>Jeremiah 39:11

Now [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) gave [tsavah](../../strongs/h/h6680.md) concerning [Yirmᵊyâ](../../strongs/h/h3414.md) [yad](../../strongs/h/h3027.md) [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_39_12"></a>Jeremiah 39:12

[laqach](../../strongs/h/h3947.md) him, and look ['ayin](../../strongs/h/h5869.md) [śûm](../../strongs/h/h7760.md) to him, and ['asah](../../strongs/h/h6213.md) him no [mᵊ'ûmâ](../../strongs/h/h3972.md) [ra'](../../strongs/h/h7451.md); but ['asah](../../strongs/h/h6213.md) unto him even as he shall [dabar](../../strongs/h/h1696.md) unto thee.

<a name="jeremiah_39_13"></a>Jeremiah 39:13

So [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [shalach](../../strongs/h/h7971.md), and [Nᵊḇûšazbān](../../strongs/h/h5021.md), [Rḇ-Srys](../../strongs/h/h7249.md), and [Nērḡal Šar'Eṣer](../../strongs/h/h5371.md), [rḇ mḡ](../../strongs/h/h7248.md), and all the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [rab](../../strongs/h/h7227.md);

<a name="jeremiah_39_14"></a>Jeremiah 39:14

Even they [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) [Yirmᵊyâ](../../strongs/h/h3414.md) out of the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md), and [nathan](../../strongs/h/h5414.md) him unto [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), that he should [yāṣā'](../../strongs/h/h3318.md) him [bayith](../../strongs/h/h1004.md): so he [yashab](../../strongs/h/h3427.md) [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md).

<a name="jeremiah_39_15"></a>Jeremiah 39:15

Now the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md), while he was [ʿāṣar](../../strongs/h/h6113.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_39_16"></a>Jeremiah 39:16

[halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md) to [ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) the [Kûšî](../../strongs/h/h3569.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [bow'](../../strongs/h/h935.md) my [dabar](../../strongs/h/h1697.md) upon this [ʿîr](../../strongs/h/h5892.md) for [ra'](../../strongs/h/h7451.md), and not for [towb](../../strongs/h/h2896.md); and they shall be accomplished in that [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) thee.

<a name="jeremiah_39_17"></a>Jeremiah 39:17

But I will [natsal](../../strongs/h/h5337.md) thee in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): and thou shalt not be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the ['enowsh](../../strongs/h/h582.md) of [paniym](../../strongs/h/h6440.md) thou art [yāḡôr](../../strongs/h/h3016.md).

<a name="jeremiah_39_18"></a>Jeremiah 39:18

For I will [mālaṭ](../../strongs/h/h4422.md) [mālaṭ](../../strongs/h/h4422.md) thee, and thou shalt not [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md), but thy [nephesh](../../strongs/h/h5315.md) shall be for a [šālāl](../../strongs/h/h7998.md) unto thee: because thou hast put thy [batach](../../strongs/h/h982.md) in me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 38](jeremiah_38.md) - [Jeremiah 40](jeremiah_40.md)