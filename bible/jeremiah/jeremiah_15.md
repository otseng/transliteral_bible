# [Jeremiah 15](https://www.blueletterbible.org/kjv/jeremiah/15)

<a name="jeremiah_15_1"></a>Jeremiah 15:1

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, Though [Mōshe](../../strongs/h/h4872.md) and [Šᵊmû'Ēl](../../strongs/h/h8050.md) ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me, yet my [nephesh](../../strongs/h/h5315.md) could not be toward this ['am](../../strongs/h/h5971.md): [shalach](../../strongs/h/h7971.md) them of my [paniym](../../strongs/h/h6440.md), and let them [yāṣā'](../../strongs/h/h3318.md).

<a name="jeremiah_15_2"></a>Jeremiah 15:2

And it shall come to pass, if they ['āmar](../../strongs/h/h559.md) unto thee, Whither shall we [yāṣā'](../../strongs/h/h3318.md)? then thou shalt ['āmar](../../strongs/h/h559.md) them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Such as are for [maveth](../../strongs/h/h4194.md), to [maveth](../../strongs/h/h4194.md); and such as are for the [chereb](../../strongs/h/h2719.md), to the [chereb](../../strongs/h/h2719.md); and such as are for the [rāʿāḇ](../../strongs/h/h7458.md), to the [rāʿāḇ](../../strongs/h/h7458.md); and such as are for the [šᵊḇî](../../strongs/h/h7628.md), to the [šᵊḇî](../../strongs/h/h7628.md).

<a name="jeremiah_15_3"></a>Jeremiah 15:3

And I will [paqad](../../strongs/h/h6485.md) over them ['arbaʿ](../../strongs/h/h702.md) [mišpāḥâ](../../strongs/h/h4940.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): the [chereb](../../strongs/h/h2719.md) to [harag](../../strongs/h/h2026.md), and the [keleḇ](../../strongs/h/h3611.md) to [sāḥaḇ](../../strongs/h/h5498.md), and the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md), to ['akal](../../strongs/h/h398.md) and [shachath](../../strongs/h/h7843.md).

<a name="jeremiah_15_4"></a>Jeremiah 15:4

And I will [nathan](../../strongs/h/h5414.md) them to be [zaʿăvâ](../../strongs/h/h2189.md) [zᵊvāʿâ](../../strongs/h/h2113.md) into all [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md), [gālāl](../../strongs/h/h1558.md) of [Mᵊnaššê](../../strongs/h/h4519.md) the [ben](../../strongs/h/h1121.md) of [Yᵊḥizqîyâ](../../strongs/h/h3169.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), for that which he ['asah](../../strongs/h/h6213.md) in [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_15_5"></a>Jeremiah 15:5

For who shall have [ḥāmal](../../strongs/h/h2550.md) upon thee, [Yĕruwshalaim](../../strongs/h/h3389.md)? or who shall [nuwd](../../strongs/h/h5110.md) thee? or who shall [cuwr](../../strongs/h/h5493.md) to [sha'al](../../strongs/h/h7592.md) how thou [shalowm](../../strongs/h/h7965.md)?

<a name="jeremiah_15_6"></a>Jeremiah 15:6

Thou hast [nāṭaš](../../strongs/h/h5203.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), thou art [yālaḵ](../../strongs/h/h3212.md) ['āḥôr](../../strongs/h/h268.md): therefore will I [natah](../../strongs/h/h5186.md) my [yad](../../strongs/h/h3027.md) against thee, and [shachath](../../strongs/h/h7843.md) thee; I am [lā'â](../../strongs/h/h3811.md) with [nacham](../../strongs/h/h5162.md).

<a name="jeremiah_15_7"></a>Jeremiah 15:7

And I will [zārâ](../../strongs/h/h2219.md) them with a [mizrê](../../strongs/h/h4214.md) in the [sha'ar](../../strongs/h/h8179.md) of the ['erets](../../strongs/h/h776.md); I will [šāḵōl](../../strongs/h/h7921.md) them of children, I will ['abad](../../strongs/h/h6.md) my ['am](../../strongs/h/h5971.md), since they [shuwb](../../strongs/h/h7725.md) not from their [derek](../../strongs/h/h1870.md).

<a name="jeremiah_15_8"></a>Jeremiah 15:8

Their ['almānâ](../../strongs/h/h490.md) are [ʿāṣam](../../strongs/h/h6105.md) to me above the [ḥôl](../../strongs/h/h2344.md) of the [yam](../../strongs/h/h3220.md): I have [bow'](../../strongs/h/h935.md) upon them against the ['em](../../strongs/h/h517.md) of the [bāḥûr](../../strongs/h/h970.md) a [shadad](../../strongs/h/h7703.md) at [ṣōhar](../../strongs/h/h6672.md): I have caused him to [naphal](../../strongs/h/h5307.md) upon it [piṯ'ōm](../../strongs/h/h6597.md), and [behālâ](../../strongs/h/h928.md) upon the [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_15_9"></a>Jeremiah 15:9

She that hath [yalad](../../strongs/h/h3205.md) [šeḇaʿ](../../strongs/h/h7651.md) ['āmal](../../strongs/h/h535.md): she hath given [nāp̄aḥ](../../strongs/h/h5301.md) the [nephesh](../../strongs/h/h5315.md); her [šemeš](../../strongs/h/h8121.md) is [bow'](../../strongs/h/h935.md) down while it was yet [yômām](../../strongs/h/h3119.md) [yowm](../../strongs/h/h3117.md): she hath been [buwsh](../../strongs/h/h954.md) and [ḥāp̄ēr](../../strongs/h/h2659.md): and the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of them will I [nathan](../../strongs/h/h5414.md) to the [chereb](../../strongs/h/h2719.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_15_10"></a>Jeremiah 15:10

['owy](../../strongs/h/h188.md) is me, my ['em](../../strongs/h/h517.md), that thou hast [yalad](../../strongs/h/h3205.md) me an ['iysh](../../strongs/h/h376.md) of [rîḇ](../../strongs/h/h7379.md) and an ['iysh](../../strongs/h/h376.md) of [māḏôn](../../strongs/h/h4066.md) to the ['erets](../../strongs/h/h776.md)! I have neither lent on [nāšâ](../../strongs/h/h5383.md), nor men have lent to me on [nāšâ](../../strongs/h/h5383.md); yet every one of them doth [qālal](../../strongs/h/h7043.md) me.

<a name="jeremiah_15_11"></a>Jeremiah 15:11

[Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md), [lō'](../../strongs/h/h3808.md) ['im](../../strongs/h/h518.md) it shall be [towb](../../strongs/h/h2896.md) with thy [šārâ](../../strongs/h/h8281.md) H8293; ['im](../../strongs/h/h518.md) I will cause the ['oyeb](../../strongs/h/h341.md) to [pāḡaʿ](../../strongs/h/h6293.md) thee well in the [ʿēṯ](../../strongs/h/h6256.md) of [ra'](../../strongs/h/h7451.md) and in the [ʿēṯ](../../strongs/h/h6256.md) of [tsarah](../../strongs/h/h6869.md).

<a name="jeremiah_15_12"></a>Jeremiah 15:12

Shall [barzel](../../strongs/h/h1270.md) [ra'a'](../../strongs/h/h7489.md) the [ṣāp̄ôn](../../strongs/h/h6828.md) [barzel](../../strongs/h/h1270.md) and the [nᵊḥšeṯ](../../strongs/h/h5178.md)?

<a name="jeremiah_15_13"></a>Jeremiah 15:13

Thy [ḥayil](../../strongs/h/h2428.md) and thy ['ôṣār](../../strongs/h/h214.md) will I [nathan](../../strongs/h/h5414.md) to the [baz](../../strongs/h/h957.md) without [mᵊḥîr](../../strongs/h/h4242.md), and that for all thy [chatta'ath](../../strongs/h/h2403.md), even in all thy [gᵊḇûl](../../strongs/h/h1366.md).

<a name="jeremiah_15_14"></a>Jeremiah 15:14

And I will make thee to ['abar](../../strongs/h/h5674.md) with thine ['oyeb](../../strongs/h/h341.md) into an ['erets](../../strongs/h/h776.md) which thou [yada'](../../strongs/h/h3045.md) not: for an ['esh](../../strongs/h/h784.md) is [qāḏaḥ](../../strongs/h/h6919.md) in mine ['aph](../../strongs/h/h639.md), which shall [yāqaḏ](../../strongs/h/h3344.md) upon you.

<a name="jeremiah_15_15"></a>Jeremiah 15:15

[Yĕhovah](../../strongs/h/h3068.md), thou [yada'](../../strongs/h/h3045.md): [zakar](../../strongs/h/h2142.md) me, and [paqad](../../strongs/h/h6485.md) me, and [naqam](../../strongs/h/h5358.md) me of my [radaph](../../strongs/h/h7291.md); take me not [laqach](../../strongs/h/h3947.md) in thy ['aph](../../strongs/h/h639.md) ['ārēḵ](../../strongs/h/h750.md): [yada'](../../strongs/h/h3045.md) that for thy sake I have [nasa'](../../strongs/h/h5375.md) [cherpah](../../strongs/h/h2781.md).

<a name="jeremiah_15_16"></a>Jeremiah 15:16

Thy [dabar](../../strongs/h/h1697.md) were [māṣā'](../../strongs/h/h4672.md), and I did ['akal](../../strongs/h/h398.md) them; and thy [dabar](../../strongs/h/h1697.md) was unto me the [śāśôn](../../strongs/h/h8342.md) and [simchah](../../strongs/h/h8057.md) of mine [lebab](../../strongs/h/h3824.md): for I am [qara'](../../strongs/h/h7121.md) by thy [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_15_17"></a>Jeremiah 15:17

I [yashab](../../strongs/h/h3427.md) not in the [sôḏ](../../strongs/h/h5475.md) of the [śāḥaq](../../strongs/h/h7832.md), nor [ʿālaz](../../strongs/h/h5937.md); I [yashab](../../strongs/h/h3427.md) [bāḏāḏ](../../strongs/h/h910.md) [paniym](../../strongs/h/h6440.md) of thy [yad](../../strongs/h/h3027.md): for thou hast [mālā'](../../strongs/h/h4390.md) me with [zaʿam](../../strongs/h/h2195.md).

<a name="jeremiah_15_18"></a>Jeremiah 15:18

Why is my [kᵊ'ēḇ](../../strongs/h/h3511.md) [netsach](../../strongs/h/h5331.md), and my [makâ](../../strongs/h/h4347.md) ['anash](../../strongs/h/h605.md), which [mā'ēn](../../strongs/h/h3985.md) to be [rapha'](../../strongs/h/h7495.md)? wilt thou be altogether unto me as an ['aḵzāḇ](../../strongs/h/h391.md), and as [mayim](../../strongs/h/h4325.md) that ['aman](../../strongs/h/h539.md)?

<a name="jeremiah_15_19"></a>Jeremiah 15:19

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), If thou [shuwb](../../strongs/h/h7725.md), then will I bring thee [shuwb](../../strongs/h/h7725.md), and thou shalt ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me: and if thou take [yāṣā'](../../strongs/h/h3318.md) the [yāqār](../../strongs/h/h3368.md) from the [zālal](../../strongs/h/h2151.md), thou shalt be as my [peh](../../strongs/h/h6310.md): let them [shuwb](../../strongs/h/h7725.md) unto thee; but [shuwb](../../strongs/h/h7725.md) not thou unto them.

<a name="jeremiah_15_20"></a>Jeremiah 15:20

And I will [nathan](../../strongs/h/h5414.md) thee unto this ['am](../../strongs/h/h5971.md) a [bāṣar](../../strongs/h/h1219.md) [nᵊḥšeṯ](../../strongs/h/h5178.md) [ḥômâ](../../strongs/h/h2346.md): and they shall [lāḥam](../../strongs/h/h3898.md) against thee, but they shall not [yakol](../../strongs/h/h3201.md) against thee: for I am with thee to [yasha'](../../strongs/h/h3467.md) thee and to [natsal](../../strongs/h/h5337.md) thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_15_21"></a>Jeremiah 15:21

And I will [natsal](../../strongs/h/h5337.md) thee out of the [yad](../../strongs/h/h3027.md) of the [ra'](../../strongs/h/h7451.md), and I will [pāḏâ](../../strongs/h/h6299.md) thee out of the [kaph](../../strongs/h/h3709.md) of the [ʿārîṣ](../../strongs/h/h6184.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 14](jeremiah_14.md) - [Jeremiah 16](jeremiah_16.md)