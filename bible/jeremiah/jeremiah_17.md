# [Jeremiah 17](https://www.blueletterbible.org/kjv/jeremiah/17)

<a name="jeremiah_17_1"></a>Jeremiah 17:1

The [chatta'ath](../../strongs/h/h2403.md) of [Yehuwdah](../../strongs/h/h3063.md) is [kāṯaḇ](../../strongs/h/h3789.md) with a [ʿēṭ](../../strongs/h/h5842.md) of [barzel](../../strongs/h/h1270.md), and with the [ṣipōren](../../strongs/h/h6856.md) of a [šāmîr](../../strongs/h/h8068.md): it is [ḥāraš](../../strongs/h/h2790.md) upon the [lûaḥ](../../strongs/h/h3871.md) of their [leb](../../strongs/h/h3820.md), and upon the [qeren](../../strongs/h/h7161.md) of your [mizbeach](../../strongs/h/h4196.md);

<a name="jeremiah_17_2"></a>Jeremiah 17:2

Whilst their [ben](../../strongs/h/h1121.md) [zakar](../../strongs/h/h2142.md) their [mizbeach](../../strongs/h/h4196.md) and their ['ăšērâ](../../strongs/h/h842.md) by the [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md) upon the [gāḇōha](../../strongs/h/h1364.md) [giḇʿâ](../../strongs/h/h1389.md).

<a name="jeremiah_17_3"></a>Jeremiah 17:3

O my [har](../../strongs/h/h2042.md) in the [sadeh](../../strongs/h/h7704.md), I will [nathan](../../strongs/h/h5414.md) thy [ḥayil](../../strongs/h/h2428.md) and all thy ['ôṣār](../../strongs/h/h214.md) to the [baz](../../strongs/h/h957.md), and thy [bāmâ](../../strongs/h/h1116.md) for [chatta'ath](../../strongs/h/h2403.md), throughout all thy [gᵊḇûl](../../strongs/h/h1366.md).

<a name="jeremiah_17_4"></a>Jeremiah 17:4

And thou, even thyself, shalt [šāmaṭ](../../strongs/h/h8058.md) from thine [nachalah](../../strongs/h/h5159.md) that I [nathan](../../strongs/h/h5414.md) thee; and I will cause thee to ['abad](../../strongs/h/h5647.md) thine ['oyeb](../../strongs/h/h341.md) in the ['erets](../../strongs/h/h776.md) which thou [yada'](../../strongs/h/h3045.md) not: for ye have [qāḏaḥ](../../strongs/h/h6919.md) an ['esh](../../strongs/h/h784.md) in mine ['aph](../../strongs/h/h639.md), which shall [yāqaḏ](../../strongs/h/h3344.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md).

<a name="jeremiah_17_5"></a>Jeremiah 17:5

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); ['arar](../../strongs/h/h779.md) be the [geḇer](../../strongs/h/h1397.md) that [batach](../../strongs/h/h982.md) in ['āḏām](../../strongs/h/h120.md), and [śûm](../../strongs/h/h7760.md) [basar](../../strongs/h/h1320.md) his [zerowa'](../../strongs/h/h2220.md), and whose [leb](../../strongs/h/h3820.md) [cuwr](../../strongs/h/h5493.md) from [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_17_6"></a>Jeremiah 17:6

For he shall be like the [ʿarʿōr](../../strongs/h/h6199.md) in the ['arabah](../../strongs/h/h6160.md), and shall not [ra'ah](../../strongs/h/h7200.md) when [towb](../../strongs/h/h2896.md) [bow'](../../strongs/h/h935.md); but shall [shakan](../../strongs/h/h7931.md) the [ḥărērîm](../../strongs/h/h2788.md) in the [midbar](../../strongs/h/h4057.md), in a [mᵊlēḥâ](../../strongs/h/h4420.md) ['erets](../../strongs/h/h776.md) and not [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_17_7"></a>Jeremiah 17:7

[barak](../../strongs/h/h1288.md) is the [geḇer](../../strongs/h/h1397.md) that [batach](../../strongs/h/h982.md) in [Yĕhovah](../../strongs/h/h3068.md), and whose [miḇṭāḥ](../../strongs/h/h4009.md) [Yĕhovah](../../strongs/h/h3068.md) is.

<a name="jeremiah_17_8"></a>Jeremiah 17:8

For he shall be as an ['ets](../../strongs/h/h6086.md) [šāṯal](../../strongs/h/h8362.md) by the [mayim](../../strongs/h/h4325.md), and that spreadeth [shalach](../../strongs/h/h7971.md) her [šereš](../../strongs/h/h8328.md) by the [yûḇāl](../../strongs/h/h3105.md), and shall not [ra'ah](../../strongs/h/h7200.md) when [ḥōm](../../strongs/h/h2527.md) [bow'](../../strongs/h/h935.md), but her ['aleh](../../strongs/h/h5929.md) shall be [raʿănān](../../strongs/h/h7488.md); and shall not be [dā'aḡ](../../strongs/h/h1672.md) in the [šānâ](../../strongs/h/h8141.md) of [baṣṣōreṯ](../../strongs/h/h1226.md), neither shall [mûš](../../strongs/h/h4185.md) from ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md).

<a name="jeremiah_17_9"></a>Jeremiah 17:9

The [leb](../../strongs/h/h3820.md) is ['aqob](../../strongs/h/h6121.md) above all things, and ['anash](../../strongs/h/h605.md): who can [yada'](../../strongs/h/h3045.md) it?

<a name="jeremiah_17_10"></a>Jeremiah 17:10

I [Yĕhovah](../../strongs/h/h3068.md) [chaqar](../../strongs/h/h2713.md) the [leb](../../strongs/h/h3820.md), I [bachan](../../strongs/h/h974.md) the [kilyah](../../strongs/h/h3629.md), even to [nathan](../../strongs/h/h5414.md) every ['iysh](../../strongs/h/h376.md) according to his [derek](../../strongs/h/h1870.md), and according to the [pĕriy](../../strongs/h/h6529.md) of his [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_17_11"></a>Jeremiah 17:11

As the [qōrē'](../../strongs/h/h7124.md) [dāḡar](../../strongs/h/h1716.md) on eggs, and [yalad](../../strongs/h/h3205.md) them not; so he that ['asah](../../strongs/h/h6213.md) [ʿōšer](../../strongs/h/h6239.md), and not by [mishpat](../../strongs/h/h4941.md), shall ['azab](../../strongs/h/h5800.md) them in the [ḥēṣî](../../strongs/h/h2677.md) of his [yowm](../../strongs/h/h3117.md), and at his ['aḥărîṯ](../../strongs/h/h319.md) shall be a [nabal](../../strongs/h/h5036.md).

<a name="jeremiah_17_12"></a>Jeremiah 17:12

A [kabowd](../../strongs/h/h3519.md) [marowm](../../strongs/h/h4791.md) [kicce'](../../strongs/h/h3678.md) from the [ri'šôn](../../strongs/h/h7223.md) is the [maqowm](../../strongs/h/h4725.md) of our [miqdash](../../strongs/h/h4720.md).

<a name="jeremiah_17_13"></a>Jeremiah 17:13

[Yĕhovah](../../strongs/h/h3068.md), the [miqvê](../../strongs/h/h4723.md) of [Yisra'el](../../strongs/h/h3478.md), all that ['azab](../../strongs/h/h5800.md) thee shall be [buwsh](../../strongs/h/h954.md), and they that [yāsûr](../../strongs/h/h3249.md) from [cuwr](../../strongs/h/h5493.md) shall be [kāṯaḇ](../../strongs/h/h3789.md) in the ['erets](../../strongs/h/h776.md), because they have ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md), the [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md).

<a name="jeremiah_17_14"></a>Jeremiah 17:14

[rapha'](../../strongs/h/h7495.md) me, [Yĕhovah](../../strongs/h/h3068.md), and I shall be [rapha'](../../strongs/h/h7495.md); [yasha'](../../strongs/h/h3467.md) me, and I shall be [yasha'](../../strongs/h/h3467.md): for thou art my [tehillah](../../strongs/h/h8416.md).

<a name="jeremiah_17_15"></a>Jeremiah 17:15

Behold, they ['āmar](../../strongs/h/h559.md) unto me, Where is the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md)? let it [bow'](../../strongs/h/h935.md) now.

<a name="jeremiah_17_16"></a>Jeremiah 17:16

As for me, I have not ['ûṣ](../../strongs/h/h213.md) from being a [ra'ah](../../strongs/h/h7462.md) to ['aḥar](../../strongs/h/h310.md) thee: neither have I ['āvâ](../../strongs/h/h183.md) the ['anash](../../strongs/h/h605.md) [yowm](../../strongs/h/h3117.md); thou [yada'](../../strongs/h/h3045.md): that which came [môṣā'](../../strongs/h/h4161.md) of my [saphah](../../strongs/h/h8193.md) was right [nōḵaḥ](../../strongs/h/h5227.md) [paniym](../../strongs/h/h6440.md).

<a name="jeremiah_17_17"></a>Jeremiah 17:17

Be not a [mᵊḥitâ](../../strongs/h/h4288.md) unto me: thou art my [machaceh](../../strongs/h/h4268.md) in the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_17_18"></a>Jeremiah 17:18

Let them be [buwsh](../../strongs/h/h954.md) that [radaph](../../strongs/h/h7291.md) me, but let not me be [buwsh](../../strongs/h/h954.md): let them be [ḥāṯaṯ](../../strongs/h/h2865.md), but let not me be [ḥāṯaṯ](../../strongs/h/h2865.md): [bow'](../../strongs/h/h935.md) upon them the [yowm](../../strongs/h/h3117.md) of [ra'](../../strongs/h/h7451.md), and [shabar](../../strongs/h/h7665.md) them with [mišnê](../../strongs/h/h4932.md) [šibārôn](../../strongs/h/h7670.md).

<a name="jeremiah_17_19"></a>Jeremiah 17:19

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me; [halak](../../strongs/h/h1980.md) and ['amad](../../strongs/h/h5975.md) in the [sha'ar](../../strongs/h/h8179.md) of the [ben](../../strongs/h/h1121.md) of the ['am](../../strongs/h/h5971.md), whereby the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [bow'](../../strongs/h/h935.md), and by the which they [yāṣā'](../../strongs/h/h3318.md), and in all the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="jeremiah_17_20"></a>Jeremiah 17:20

And ['āmar](../../strongs/h/h559.md) unto them, [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and all [Yehuwdah](../../strongs/h/h3063.md), and all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that enter [bow'](../../strongs/h/h935.md) by these [sha'ar](../../strongs/h/h8179.md):

<a name="jeremiah_17_21"></a>Jeremiah 17:21

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Take [shamar](../../strongs/h/h8104.md) to [nephesh](../../strongs/h/h5315.md), and [nasa'](../../strongs/h/h5375.md) no [maśśā'](../../strongs/h/h4853.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), nor [bow'](../../strongs/h/h935.md) it in by the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="jeremiah_17_22"></a>Jeremiah 17:22

Neither [yāṣā'](../../strongs/h/h3318.md) a [maśśā'](../../strongs/h/h4853.md) out of your [bayith](../../strongs/h/h1004.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), neither ['asah](../../strongs/h/h6213.md) ye any [mĕla'kah](../../strongs/h/h4399.md), but [qadash](../../strongs/h/h6942.md) ye the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), as I [tsavah](../../strongs/h/h6680.md) your ['ab](../../strongs/h/h1.md).

<a name="jeremiah_17_23"></a>Jeremiah 17:23

But they [shama'](../../strongs/h/h8085.md) not, neither [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md), but made their [ʿōrep̄](../../strongs/h/h6203.md) [qāšâ](../../strongs/h/h7185.md), that they might not [shama'](../../strongs/h/h8085.md), nor [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md).

<a name="jeremiah_17_24"></a>Jeremiah 17:24

And it shall come to pass, if ye [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), to [bow'](../../strongs/h/h935.md) in no [maśśā'](../../strongs/h/h4853.md) through the [sha'ar](../../strongs/h/h8179.md) of this [ʿîr](../../strongs/h/h5892.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), but [qadash](../../strongs/h/h6942.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), to ['asah](../../strongs/h/h6213.md) [biltî](../../strongs/h/h1115.md) [mĕla'kah](../../strongs/h/h4399.md) therein;

<a name="jeremiah_17_25"></a>Jeremiah 17:25

Then shall there [bow'](../../strongs/h/h935.md) into the [sha'ar](../../strongs/h/h8179.md) of this [ʿîr](../../strongs/h/h5892.md) [melek](../../strongs/h/h4428.md) and [śar](../../strongs/h/h8269.md) [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), [rāḵaḇ](../../strongs/h/h7392.md) in [reḵeḇ](../../strongs/h/h7393.md) and on [sûs](../../strongs/h/h5483.md), they, and their [śar](../../strongs/h/h8269.md), the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md): and this [ʿîr](../../strongs/h/h5892.md) shall [yashab](../../strongs/h/h3427.md) ['owlam](../../strongs/h/h5769.md).

<a name="jeremiah_17_26"></a>Jeremiah 17:26

And they shall [bow'](../../strongs/h/h935.md) from the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and from the places [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md), and from the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md), and from the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and from the [har](../../strongs/h/h2022.md), and from the [neḡeḇ](../../strongs/h/h5045.md), [bow'](../../strongs/h/h935.md) [ʿōlâ](../../strongs/h/h5930.md), and [zebach](../../strongs/h/h2077.md), and meat [minchah](../../strongs/h/h4503.md), and [lᵊḇônâ](../../strongs/h/h3828.md), and [bow'](../../strongs/h/h935.md) sacrifices of [tôḏâ](../../strongs/h/h8426.md), unto the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_17_27"></a>Jeremiah 17:27

But if ye will not [shama'](../../strongs/h/h8085.md) unto me to [qadash](../../strongs/h/h6942.md) the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md), and not to [nasa'](../../strongs/h/h5375.md) a [maśśā'](../../strongs/h/h4853.md), even entering [bow'](../../strongs/h/h935.md) at the [sha'ar](../../strongs/h/h8179.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) on the [shabbath](../../strongs/h/h7676.md) [yowm](../../strongs/h/h3117.md); then will I [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in the [sha'ar](../../strongs/h/h8179.md) thereof, and it shall ['akal](../../strongs/h/h398.md) the ['armôn](../../strongs/h/h759.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and it shall not be [kāḇâ](../../strongs/h/h3518.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 16](jeremiah_16.md) - [Jeremiah 18](jeremiah_18.md)