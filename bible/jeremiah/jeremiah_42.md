# [Jeremiah 42](https://www.blueletterbible.org/kjv/jeremiah/42)

<a name="jeremiah_42_1"></a>Jeremiah 42:1

Then all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), and [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and [Yᵊzanyâ](../../strongs/h/h3153.md) the [ben](../../strongs/h/h1121.md) of [HôšaʿYâ](../../strongs/h/h1955.md), and all the ['am](../../strongs/h/h5971.md) from the [qāṭān](../../strongs/h/h6996.md) even unto the [gadowl](../../strongs/h/h1419.md), came [nāḡaš](../../strongs/h/h5066.md),

<a name="jeremiah_42_2"></a>Jeremiah 42:2

And ['āmar](../../strongs/h/h559.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), Let, we beseech thee, our [tĕchinnah](../../strongs/h/h8467.md) be [naphal](../../strongs/h/h5307.md) [paniym](../../strongs/h/h6440.md) thee, and [palal](../../strongs/h/h6419.md) for us unto [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), even for all this [šᵊ'ērîṯ](../../strongs/h/h7611.md); (for we are [šā'ar](../../strongs/h/h7604.md) but a [mᵊʿaṭ](../../strongs/h/h4592.md) of [rabah](../../strongs/h/h7235.md), as thine ['ayin](../../strongs/h/h5869.md) do [ra'ah](../../strongs/h/h7200.md) us:)

<a name="jeremiah_42_3"></a>Jeremiah 42:3

That [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) may [nāḡaḏ](../../strongs/h/h5046.md) us the [derek](../../strongs/h/h1870.md) wherein we may [yālaḵ](../../strongs/h/h3212.md), and the [dabar](../../strongs/h/h1697.md) that we may ['asah](../../strongs/h/h6213.md).

<a name="jeremiah_42_4"></a>Jeremiah 42:4

Then [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) ['āmar](../../strongs/h/h559.md) unto them, I have [shama'](../../strongs/h/h8085.md) you; behold, I will [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md) according to your [dabar](../../strongs/h/h1697.md); and it shall come to pass, that whatsoever [dabar](../../strongs/h/h1697.md) [Yĕhovah](../../strongs/h/h3068.md) shall ['anah](../../strongs/h/h6030.md) you, I will [nāḡaḏ](../../strongs/h/h5046.md) it unto you; I will [mānaʿ](../../strongs/h/h4513.md) [dabar](../../strongs/h/h1697.md) [mānaʿ](../../strongs/h/h4513.md) from you.

<a name="jeremiah_42_5"></a>Jeremiah 42:5

Then they ['āmar](../../strongs/h/h559.md) to [Yirmᵊyâ](../../strongs/h/h3414.md), [Yĕhovah](../../strongs/h/h3068.md) be an ['emeth](../../strongs/h/h571.md) and ['aman](../../strongs/h/h539.md) ['ed](../../strongs/h/h5707.md) between us, if we ['asah](../../strongs/h/h6213.md) not even according to all [dabar](../../strongs/h/h1697.md) for the which [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) shall [shalach](../../strongs/h/h7971.md) thee to us.

<a name="jeremiah_42_6"></a>Jeremiah 42:6

Whether it be [towb](../../strongs/h/h2896.md), or whether it be [ra'](../../strongs/h/h7451.md), we will [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), to whom ['ănû](../../strongs/h/h580.md) [shalach](../../strongs/h/h7971.md) thee; that it may be [yatab](../../strongs/h/h3190.md) with us, when we [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_42_7"></a>Jeremiah 42:7

And it came to pass [qēṣ](../../strongs/h/h7093.md) [ʿeśer](../../strongs/h/h6235.md) [yowm](../../strongs/h/h3117.md), that the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto [Yirmᵊyâ](../../strongs/h/h3414.md).

<a name="jeremiah_42_8"></a>Jeremiah 42:8

Then [qara'](../../strongs/h/h7121.md) he [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) which were with him, and all the ['am](../../strongs/h/h5971.md) from the [qāṭān](../../strongs/h/h6996.md) even to the [gadowl](../../strongs/h/h1419.md),

<a name="jeremiah_42_9"></a>Jeremiah 42:9

And ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), unto whom ye [shalach](../../strongs/h/h7971.md) me to [naphal](../../strongs/h/h5307.md) your [tĕchinnah](../../strongs/h/h8467.md) [paniym](../../strongs/h/h6440.md) him;

<a name="jeremiah_42_10"></a>Jeremiah 42:10

If ye will [shuwb](../../strongs/h/h7725.md) [yashab](../../strongs/h/h3427.md) in this ['erets](../../strongs/h/h776.md), then will I [bānâ](../../strongs/h/h1129.md) you, and not pull you [harac](../../strongs/h/h2040.md), and I will [nāṭaʿ](../../strongs/h/h5193.md) you, and not [nathash](../../strongs/h/h5428.md) you: for I [nacham](../../strongs/h/h5162.md) me of the [ra'](../../strongs/h/h7451.md) that I have ['asah](../../strongs/h/h6213.md) unto you.

<a name="jeremiah_42_11"></a>Jeremiah 42:11

Be not [yare'](../../strongs/h/h3372.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), of [paniym](../../strongs/h/h6440.md) ye are [yārē'](../../strongs/h/h3373.md); be not [yare'](../../strongs/h/h3372.md) of him, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): for I am with you to [yasha'](../../strongs/h/h3467.md) you, and to [natsal](../../strongs/h/h5337.md) you from his [yad](../../strongs/h/h3027.md).

<a name="jeremiah_42_12"></a>Jeremiah 42:12

And I will [nathan](../../strongs/h/h5414.md) [raḥam](../../strongs/h/h7356.md) unto you, that he may have [racham](../../strongs/h/h7355.md) upon you, and cause you to [shuwb](../../strongs/h/h7725.md) to your own ['ăḏāmâ](../../strongs/h/h127.md).

<a name="jeremiah_42_13"></a>Jeremiah 42:13

But if ye ['āmar](../../strongs/h/h559.md), We will not [yashab](../../strongs/h/h3427.md) in this ['erets](../../strongs/h/h776.md), neither [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md),

<a name="jeremiah_42_14"></a>Jeremiah 42:14

['āmar](../../strongs/h/h559.md), No; but we will [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), where we shall [ra'ah](../../strongs/h/h7200.md) no [milḥāmâ](../../strongs/h/h4421.md), nor [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of the [šôp̄ār](../../strongs/h/h7782.md), nor have [rāʿēḇ](../../strongs/h/h7456.md) of [lechem](../../strongs/h/h3899.md); and there will we [yashab](../../strongs/h/h3427.md):

<a name="jeremiah_42_15"></a>Jeremiah 42:15

And now therefore [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), ye [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); If ye [śûm](../../strongs/h/h7760.md) [śûm](../../strongs/h/h7760.md) your [paniym](../../strongs/h/h6440.md) to [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md), and [bow'](../../strongs/h/h935.md) to [guwr](../../strongs/h/h1481.md) there;

<a name="jeremiah_42_16"></a>Jeremiah 42:16

Then it shall come to pass, that the [chereb](../../strongs/h/h2719.md), which ye [yārē'](../../strongs/h/h3373.md), shall [nāśaḡ](../../strongs/h/h5381.md) you there in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and the [rāʿāḇ](../../strongs/h/h7458.md), whereof ye were [dā'aḡ](../../strongs/h/h1672.md), shall [dāḇaq](../../strongs/h/h1692.md) ['aḥar](../../strongs/h/h310.md) you there in [Mitsrayim](../../strongs/h/h4714.md); and there ye shall [muwth](../../strongs/h/h4191.md).

<a name="jeremiah_42_17"></a>Jeremiah 42:17

So shall it be with all the ['enowsh](../../strongs/h/h582.md) that [śûm](../../strongs/h/h7760.md) their [paniym](../../strongs/h/h6440.md) to [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there; they shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md): and none of them shall [śārîḏ](../../strongs/h/h8300.md) or [pālîṭ](../../strongs/h/h6412.md) [paniym](../../strongs/h/h6440.md) the [ra'](../../strongs/h/h7451.md) that I will [bow'](../../strongs/h/h935.md) upon them.

<a name="jeremiah_42_18"></a>Jeremiah 42:18

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); As mine ['aph](../../strongs/h/h639.md) and my [chemah](../../strongs/h/h2534.md) hath been [nāṯaḵ](../../strongs/h/h5413.md) upon the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); so shall my [chemah](../../strongs/h/h2534.md) be [nāṯaḵ](../../strongs/h/h5413.md) upon you, when ye shall [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md): and ye shall be an ['alah](../../strongs/h/h423.md), and a [šammâ](../../strongs/h/h8047.md), and a [qᵊlālâ](../../strongs/h/h7045.md), and a [cherpah](../../strongs/h/h2781.md); and ye shall [ra'ah](../../strongs/h/h7200.md) this [maqowm](../../strongs/h/h4725.md) no more.

<a name="jeremiah_42_19"></a>Jeremiah 42:19

[Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) concerning you, O ye [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md); [bow'](../../strongs/h/h935.md) ye not into [Mitsrayim](../../strongs/h/h4714.md): [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that I have [ʿûḏ](../../strongs/h/h5749.md) you this [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_42_20"></a>Jeremiah 42:20

For ye [tāʿâ](../../strongs/h/h8582.md) in your [nephesh](../../strongs/h/h5315.md), when ye [shalach](../../strongs/h/h7971.md) me unto [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), ['āmar](../../strongs/h/h559.md), [palal](../../strongs/h/h6419.md) for us unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md); and according unto all that [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) shall ['āmar](../../strongs/h/h559.md), so [nāḡaḏ](../../strongs/h/h5046.md) unto us, and we will ['asah](../../strongs/h/h6213.md) it.

<a name="jeremiah_42_21"></a>Jeremiah 42:21

And now I have this [yowm](../../strongs/h/h3117.md) [nāḡaḏ](../../strongs/h/h5046.md) it to you; but ye have not [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), nor any thing for the which he hath [shalach](../../strongs/h/h7971.md) me unto you.

<a name="jeremiah_42_22"></a>Jeremiah 42:22

Now therefore [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that ye shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md), in the [maqowm](../../strongs/h/h4725.md) whither ye [ḥāp̄ēṣ](../../strongs/h/h2654.md) to [bow'](../../strongs/h/h935.md) and to [guwr](../../strongs/h/h1481.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 41](jeremiah_41.md) - [Jeremiah 43](jeremiah_43.md)