# [Jeremiah 31](https://www.blueletterbible.org/kjv/jeremiah/31)

<a name="jeremiah_31_1"></a>Jeremiah 31:1

At the same [ʿēṯ](../../strongs/h/h6256.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), will I be the ['Elohiym](../../strongs/h/h430.md) of all the [mišpāḥâ](../../strongs/h/h4940.md) of [Yisra'el](../../strongs/h/h3478.md), and they shall be my ['am](../../strongs/h/h5971.md).

<a name="jeremiah_31_2"></a>Jeremiah 31:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), The ['am](../../strongs/h/h5971.md) which were [śārîḏ](../../strongs/h/h8300.md) of the [chereb](../../strongs/h/h2719.md) [māṣā'](../../strongs/h/h4672.md) [ḥēn](../../strongs/h/h2580.md) in the [midbar](../../strongs/h/h4057.md); even [Yisra'el](../../strongs/h/h3478.md), when I [halak](../../strongs/h/h1980.md) to cause him to [rāḡaʿ](../../strongs/h/h7280.md).

<a name="jeremiah_31_3"></a>Jeremiah 31:3

[Yĕhovah](../../strongs/h/h3068.md) hath [ra'ah](../../strongs/h/h7200.md) of [rachowq](../../strongs/h/h7350.md) unto me, saying, Yea, I have ['ahab](../../strongs/h/h157.md) thee with an ['owlam](../../strongs/h/h5769.md) ['ahăḇâ](../../strongs/h/h160.md): therefore with [checed](../../strongs/h/h2617.md) have I [mashak](../../strongs/h/h4900.md) thee.

<a name="jeremiah_31_4"></a>Jeremiah 31:4

Again I will [bānâ](../../strongs/h/h1129.md) thee, and thou shalt be [bānâ](../../strongs/h/h1129.md), O [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yisra'el](../../strongs/h/h3478.md): thou shalt again be [ʿāḏâ](../../strongs/h/h5710.md) with thy [tōp̄](../../strongs/h/h8596.md), and shalt [yāṣā'](../../strongs/h/h3318.md) in the [māḥôl](../../strongs/h/h4234.md) of them that make [śāḥaq](../../strongs/h/h7832.md).

<a name="jeremiah_31_5"></a>Jeremiah 31:5

Thou shalt yet [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md) upon the [har](../../strongs/h/h2022.md) of [Šōmrôn](../../strongs/h/h8111.md): the [nāṭaʿ](../../strongs/h/h5193.md) shall [nāṭaʿ](../../strongs/h/h5193.md), and shall eat them as common [ḥālal](../../strongs/h/h2490.md).

<a name="jeremiah_31_6"></a>Jeremiah 31:6

For there shall [yēš](../../strongs/h/h3426.md) a [yowm](../../strongs/h/h3117.md), that the [nāṣar](../../strongs/h/h5341.md) upon the [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) shall [qara'](../../strongs/h/h7121.md), [quwm](../../strongs/h/h6965.md) ye, and let us [ʿālâ](../../strongs/h/h5927.md) to [Tsiyown](../../strongs/h/h6726.md) unto [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_31_7"></a>Jeremiah 31:7

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [ranan](../../strongs/h/h7442.md) with [simchah](../../strongs/h/h8057.md) for [Ya'aqob](../../strongs/h/h3290.md), and [ṣāhal](../../strongs/h/h6670.md) among the [ro'sh](../../strongs/h/h7218.md) of the [gowy](../../strongs/h/h1471.md): [shama'](../../strongs/h/h8085.md) ye, [halal](../../strongs/h/h1984.md) ye, and ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md), [yasha'](../../strongs/h/h3467.md) thy ['am](../../strongs/h/h5971.md), the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_31_8"></a>Jeremiah 31:8

Behold, I will [bow'](../../strongs/h/h935.md) them from the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md), and [qāḇaṣ](../../strongs/h/h6908.md) them from the [yᵊrēḵâ](../../strongs/h/h3411.md) of the ['erets](../../strongs/h/h776.md), and with them the [ʿiûēr](../../strongs/h/h5787.md) and the [pissēaḥ](../../strongs/h/h6455.md), the woman with [hārê](../../strongs/h/h2030.md) and her that [yalad](../../strongs/h/h3205.md) [yaḥaḏ](../../strongs/h/h3162.md): a [gadowl](../../strongs/h/h1419.md) [qāhēl](../../strongs/h/h6951.md) shall [shuwb](../../strongs/h/h7725.md) thither.

<a name="jeremiah_31_9"></a>Jeremiah 31:9

They shall [bow'](../../strongs/h/h935.md) with [bĕkiy](../../strongs/h/h1065.md), and with [taḥănûn](../../strongs/h/h8469.md) will I [yāḇal](../../strongs/h/h2986.md) them: I will cause them to [yālaḵ](../../strongs/h/h3212.md) by the [nachal](../../strongs/h/h5158.md) of [mayim](../../strongs/h/h4325.md) in a [yashar](../../strongs/h/h3477.md) [derek](../../strongs/h/h1870.md), wherein they shall not [kashal](../../strongs/h/h3782.md): for I am an ['ab](../../strongs/h/h1.md) to [Yisra'el](../../strongs/h/h3478.md), and ['Ep̄rayim](../../strongs/h/h669.md) is my [bᵊḵôr](../../strongs/h/h1060.md).

<a name="jeremiah_31_10"></a>Jeremiah 31:10

[shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O ye [gowy](../../strongs/h/h1471.md), and [nāḡaḏ](../../strongs/h/h5046.md) it in the ['î](../../strongs/h/h339.md) afar [merḥāq](../../strongs/h/h4801.md), and ['āmar](../../strongs/h/h559.md), He that [zārâ](../../strongs/h/h2219.md) [Yisra'el](../../strongs/h/h3478.md) will [qāḇaṣ](../../strongs/h/h6908.md) him, and [shamar](../../strongs/h/h8104.md) him, as a [ra'ah](../../strongs/h/h7462.md) doth his [ʿēḏer](../../strongs/h/h5739.md).

<a name="jeremiah_31_11"></a>Jeremiah 31:11

For [Yĕhovah](../../strongs/h/h3068.md) hath [pāḏâ](../../strongs/h/h6299.md) [Ya'aqob](../../strongs/h/h3290.md), and [gā'al](../../strongs/h/h1350.md) him from the [yad](../../strongs/h/h3027.md) of him that was [ḥāzāq](../../strongs/h/h2389.md) than he.

<a name="jeremiah_31_12"></a>Jeremiah 31:12

Therefore they shall [bow'](../../strongs/h/h935.md) and [ranan](../../strongs/h/h7442.md) in the [marowm](../../strongs/h/h4791.md) of [Tsiyown](../../strongs/h/h6726.md), and shall flow [nāhar](../../strongs/h/h5102.md) to the [ṭûḇ](../../strongs/h/h2898.md) of [Yĕhovah](../../strongs/h/h3068.md), for [dagan](../../strongs/h/h1715.md), and for [tiyrowsh](../../strongs/h/h8492.md), and for [yiṣhār](../../strongs/h/h3323.md), and for the [ben](../../strongs/h/h1121.md) of the [tso'n](../../strongs/h/h6629.md) and of the [bāqār](../../strongs/h/h1241.md): and their [nephesh](../../strongs/h/h5315.md) shall be as a [rāvê](../../strongs/h/h7302.md) [gan](../../strongs/h/h1588.md); and they shall not [dā'aḇ](../../strongs/h/h1669.md) any [yāsap̄](../../strongs/h/h3254.md) at all.

<a name="jeremiah_31_13"></a>Jeremiah 31:13

Then shall the [bᵊṯûlâ](../../strongs/h/h1330.md) [samach](../../strongs/h/h8055.md) in the [māḥôl](../../strongs/h/h4234.md), both young [bāḥûr](../../strongs/h/h970.md) and [zāqēn](../../strongs/h/h2205.md) [yaḥaḏ](../../strongs/h/h3162.md): for I will [hāp̄aḵ](../../strongs/h/h2015.md) their ['ēḇel](../../strongs/h/h60.md) into [śāśôn](../../strongs/h/h8342.md), and will [nacham](../../strongs/h/h5162.md) them, and make them [samach](../../strongs/h/h8055.md) from their [yagown](../../strongs/h/h3015.md).

<a name="jeremiah_31_14"></a>Jeremiah 31:14

And I will [rāvâ](../../strongs/h/h7301.md) the [nephesh](../../strongs/h/h5315.md) of the [kōhēn](../../strongs/h/h3548.md) with [dešen](../../strongs/h/h1880.md), and my ['am](../../strongs/h/h5971.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) with my [ṭûḇ](../../strongs/h/h2898.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_31_15"></a>Jeremiah 31:15

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); A [qowl](../../strongs/h/h6963.md) was [shama'](../../strongs/h/h8085.md) in [rāmâ](../../strongs/h/h7414.md), [nᵊhî](../../strongs/h/h5092.md), and [tamrûrîm](../../strongs/h/h8563.md) [bĕkiy](../../strongs/h/h1065.md); [Rāḥēl](../../strongs/h/h7354.md) [bāḵâ](../../strongs/h/h1058.md) for her [ben](../../strongs/h/h1121.md) [mā'ēn](../../strongs/h/h3985.md) to be [nacham](../../strongs/h/h5162.md) for her [ben](../../strongs/h/h1121.md), because they were not.

<a name="jeremiah_31_16"></a>Jeremiah 31:16

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [mānaʿ](../../strongs/h/h4513.md) thy [qowl](../../strongs/h/h6963.md) from [bĕkiy](../../strongs/h/h1065.md), and thine ['ayin](../../strongs/h/h5869.md) from [dim'ah](../../strongs/h/h1832.md): for thy [pe'ullah](../../strongs/h/h6468.md) shall [yēš](../../strongs/h/h3426.md) [śāḵār](../../strongs/h/h7939.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and they shall [shuwb](../../strongs/h/h7725.md) from the ['erets](../../strongs/h/h776.md) of the ['oyeb](../../strongs/h/h341.md).

<a name="jeremiah_31_17"></a>Jeremiah 31:17

And there [yēš](../../strongs/h/h3426.md) [tiqvâ](../../strongs/h/h8615.md) in thine ['aḥărîṯ](../../strongs/h/h319.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that thy [ben](../../strongs/h/h1121.md) shall [shuwb](../../strongs/h/h7725.md) to their own [gᵊḇûl](../../strongs/h/h1366.md).

<a name="jeremiah_31_18"></a>Jeremiah 31:18

I have [shama'](../../strongs/h/h8085.md) [shama'](../../strongs/h/h8085.md) ['Ep̄rayim](../../strongs/h/h669.md) [nuwd](../../strongs/h/h5110.md) himself thus; Thou hast [yacar](../../strongs/h/h3256.md) me, and I was [yacar](../../strongs/h/h3256.md), as a [ʿēḡel](../../strongs/h/h5695.md) [lō'](../../strongs/h/h3808.md) [lamad](../../strongs/h/h3925.md) to the yoke: [shuwb](../../strongs/h/h7725.md) thou me, and I shall be [shuwb](../../strongs/h/h7725.md); for thou art [Yĕhovah](../../strongs/h/h3068.md) my ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_31_19"></a>Jeremiah 31:19

Surely ['aḥar](../../strongs/h/h310.md) that I was [shuwb](../../strongs/h/h7725.md), I [nacham](../../strongs/h/h5162.md); and ['aḥar](../../strongs/h/h310.md) that I was [yada'](../../strongs/h/h3045.md), I [sāp̄aq](../../strongs/h/h5606.md) upon my [yārēḵ](../../strongs/h/h3409.md): I was [buwsh](../../strongs/h/h954.md), yea, even [kālam](../../strongs/h/h3637.md), because I did [nasa'](../../strongs/h/h5375.md) the [cherpah](../../strongs/h/h2781.md) of my [nāʿur](../../strongs/h/h5271.md).

<a name="jeremiah_31_20"></a>Jeremiah 31:20

Is ['Ep̄rayim](../../strongs/h/h669.md) my [yaqqîr](../../strongs/h/h3357.md) [ben](../../strongs/h/h1121.md)? is he a [šaʿăšûʿîm](../../strongs/h/h8191.md) [yeleḏ](../../strongs/h/h3206.md)? for [day](../../strongs/h/h1767.md) I [dabar](../../strongs/h/h1696.md) against him, I do [zakar](../../strongs/h/h2142.md) [zakar](../../strongs/h/h2142.md) him still: therefore my [me'ah](../../strongs/h/h4578.md) are [hāmâ](../../strongs/h/h1993.md) for him; I will [racham](../../strongs/h/h7355.md) have [racham](../../strongs/h/h7355.md) upon him, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_31_21"></a>Jeremiah 31:21

Set thee [nāṣaḇ](../../strongs/h/h5324.md) [ṣîyûn](../../strongs/h/h6725.md), [śûm](../../strongs/h/h7760.md) thee high [tamrûrîm](../../strongs/h/h8564.md): [shiyth](../../strongs/h/h7896.md) thine [leb](../../strongs/h/h3820.md) toward the [mĕcillah](../../strongs/h/h4546.md), even the [derek](../../strongs/h/h1870.md) which thou [halak](../../strongs/h/h1980.md): [shuwb](../../strongs/h/h7725.md), O [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yisra'el](../../strongs/h/h3478.md), [shuwb](../../strongs/h/h7725.md) to these thy [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_31_22"></a>Jeremiah 31:22

How long wilt thou [ḥāmaq](../../strongs/h/h2559.md), O thou [šôḇēḇ](../../strongs/h/h7728.md) [bath](../../strongs/h/h1323.md)? for [Yĕhovah](../../strongs/h/h3068.md) hath [bara'](../../strongs/h/h1254.md) a new [ḥāḏāš](../../strongs/h/h2319.md) in the ['erets](../../strongs/h/h776.md), A [nᵊqēḇâ](../../strongs/h/h5347.md) shall [cabab](../../strongs/h/h5437.md) a [geḇer](../../strongs/h/h1397.md).

<a name="jeremiah_31_23"></a>Jeremiah 31:23

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); As yet they shall ['āmar](../../strongs/h/h559.md) this [dabar](../../strongs/h/h1697.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md) and in the [ʿîr](../../strongs/h/h5892.md) thereof, when I shall [shuwb](../../strongs/h/h7725.md) their [shebuwth](../../strongs/h/h7622.md); [Yĕhovah](../../strongs/h/h3068.md) [barak](../../strongs/h/h1288.md) thee, O [nāvê](../../strongs/h/h5116.md) of [tsedeq](../../strongs/h/h6664.md), and [har](../../strongs/h/h2022.md) of [qodesh](../../strongs/h/h6944.md).

<a name="jeremiah_31_24"></a>Jeremiah 31:24

And there shall [yashab](../../strongs/h/h3427.md) in [Yehuwdah](../../strongs/h/h3063.md) itself, and in all the [ʿîr](../../strongs/h/h5892.md) thereof [yaḥaḏ](../../strongs/h/h3162.md), ['ikār](../../strongs/h/h406.md), and they that [nāsaʿ](../../strongs/h/h5265.md) with [ʿēḏer](../../strongs/h/h5739.md).

<a name="jeremiah_31_25"></a>Jeremiah 31:25

For I have [rāvâ](../../strongs/h/h7301.md) the [ʿāyēp̄](../../strongs/h/h5889.md) [nephesh](../../strongs/h/h5315.md), and I have [mālā'](../../strongs/h/h4390.md) every [dā'aḇ](../../strongs/h/h1669.md) [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_31_26"></a>Jeremiah 31:26

Upon this I [quwts](../../strongs/h/h6974.md), and [ra'ah](../../strongs/h/h7200.md); and my [šēnā'](../../strongs/h/h8142.md) was [ʿāraḇ](../../strongs/h/h6149.md) unto me.

<a name="jeremiah_31_27"></a>Jeremiah 31:27

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [zāraʿ](../../strongs/h/h2232.md) the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) with the [zera'](../../strongs/h/h2233.md) of ['āḏām](../../strongs/h/h120.md), and with the [zera'](../../strongs/h/h2233.md) of [bĕhemah](../../strongs/h/h929.md).

<a name="jeremiah_31_28"></a>Jeremiah 31:28

And it shall come to pass, that like as I have [šāqaḏ](../../strongs/h/h8245.md) them, to [nathash](../../strongs/h/h5428.md), and to [nāṯaṣ](../../strongs/h/h5422.md), and to [harac](../../strongs/h/h2040.md), and to ['abad](../../strongs/h/h6.md), and to [ra'a'](../../strongs/h/h7489.md); so will I [šāqaḏ](../../strongs/h/h8245.md) them, to [bānâ](../../strongs/h/h1129.md), and to [nāṭaʿ](../../strongs/h/h5193.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_31_29"></a>Jeremiah 31:29

In those [yowm](../../strongs/h/h3117.md) they shall ['āmar](../../strongs/h/h559.md) no more, The ['ab](../../strongs/h/h1.md) have ['akal](../../strongs/h/h398.md) a [bōser](../../strongs/h/h1155.md), and the [ben](../../strongs/h/h1121.md) [šēn](../../strongs/h/h8127.md) are set on [qāhâ](../../strongs/h/h6949.md).

<a name="jeremiah_31_30"></a>Jeremiah 31:30

But every ['iysh](../../strongs/h/h376.md) shall [muwth](../../strongs/h/h4191.md) for his own ['avon](../../strongs/h/h5771.md): every ['āḏām](../../strongs/h/h120.md) that ['akal](../../strongs/h/h398.md) the [bōser](../../strongs/h/h1155.md), his [šēn](../../strongs/h/h8127.md) shall be [qāhâ](../../strongs/h/h6949.md).

<a name="jeremiah_31_31"></a>Jeremiah 31:31

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [karath](../../strongs/h/h3772.md) a [ḥāḏāš](../../strongs/h/h2319.md) [bĕriyth](../../strongs/h/h1285.md) with the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), and with the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md):

<a name="jeremiah_31_32"></a>Jeremiah 31:32

Not according to the [bĕriyth](../../strongs/h/h1285.md) that I [karath](../../strongs/h/h3772.md) with their ['ab](../../strongs/h/h1.md) in the [yowm](../../strongs/h/h3117.md) that I [ḥāzaq](../../strongs/h/h2388.md) them by the [yad](../../strongs/h/h3027.md) to [yāṣā'](../../strongs/h/h3318.md) them of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); which my [bĕriyth](../../strongs/h/h1285.md) they [pārar](../../strongs/h/h6565.md), although I was a [bāʿal](../../strongs/h/h1166.md) unto them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md):

<a name="jeremiah_31_33"></a>Jeremiah 31:33

But this shall be the [bĕriyth](../../strongs/h/h1285.md) that I will [karath](../../strongs/h/h3772.md) with the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md); ['aḥar](../../strongs/h/h310.md) those [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), I will [nathan](../../strongs/h/h5414.md) my [towrah](../../strongs/h/h8451.md) in their [qereḇ](../../strongs/h/h7130.md), and [kāṯaḇ](../../strongs/h/h3789.md) it in their [leb](../../strongs/h/h3820.md); and will be their ['Elohiym](../../strongs/h/h430.md), and they shall be my ['am](../../strongs/h/h5971.md).

<a name="jeremiah_31_34"></a>Jeremiah 31:34

And they shall [lamad](../../strongs/h/h3925.md) no more every ['iysh](../../strongs/h/h376.md) his [rea'](../../strongs/h/h7453.md), and every ['iysh](../../strongs/h/h376.md) his ['ach](../../strongs/h/h251.md), ['āmar](../../strongs/h/h559.md), [yada'](../../strongs/h/h3045.md) [Yĕhovah](../../strongs/h/h3068.md): for they shall all [yada'](../../strongs/h/h3045.md) me, from the [qāṭān](../../strongs/h/h6996.md) of them unto the [gadowl](../../strongs/h/h1419.md) of them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): for I will [sālaḥ](../../strongs/h/h5545.md) their ['avon](../../strongs/h/h5771.md), and I will [zakar](../../strongs/h/h2142.md) their [chatta'ath](../../strongs/h/h2403.md) no more.

<a name="jeremiah_31_35"></a>Jeremiah 31:35

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), which [nathan](../../strongs/h/h5414.md) the [šemeš](../../strongs/h/h8121.md) for a ['owr](../../strongs/h/h216.md) by [yômām](../../strongs/h/h3119.md), and the [chuqqah](../../strongs/h/h2708.md) of the [yareach](../../strongs/h/h3394.md) and of the [kowkab](../../strongs/h/h3556.md) for a ['owr](../../strongs/h/h216.md) by [layil](../../strongs/h/h3915.md), which [rāḡaʿ](../../strongs/h/h7280.md) the [yam](../../strongs/h/h3220.md) when the [gal](../../strongs/h/h1530.md) thereof [hāmâ](../../strongs/h/h1993.md); [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md):

<a name="jeremiah_31_36"></a>Jeremiah 31:36

If those [choq](../../strongs/h/h2706.md) [mûš](../../strongs/h/h4185.md) from [paniym](../../strongs/h/h6440.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), then the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md) also shall [shabath](../../strongs/h/h7673.md) from being a [gowy](../../strongs/h/h1471.md) [paniym](../../strongs/h/h6440.md) me for [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_31_37"></a>Jeremiah 31:37

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); If [shamayim](../../strongs/h/h8064.md) [maʿal](../../strongs/h/h4605.md) can be [māḏaḏ](../../strongs/h/h4058.md), and the [mowcadah](../../strongs/h/h4146.md) of the ['erets](../../strongs/h/h776.md) [chaqar](../../strongs/h/h2713.md) [maṭṭâ](../../strongs/h/h4295.md), I will also [mā'as](../../strongs/h/h3988.md) all the [zera'](../../strongs/h/h2233.md) of [Yisra'el](../../strongs/h/h3478.md) for all that they have ['asah](../../strongs/h/h6213.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_31_38"></a>Jeremiah 31:38

Behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that the [ʿîr](../../strongs/h/h5892.md) shall be [bānâ](../../strongs/h/h1129.md) to [Yĕhovah](../../strongs/h/h3068.md) from the [miḡdāl](../../strongs/h/h4026.md) of [Ḥănan'Ēl](../../strongs/h/h2606.md) unto the [sha'ar](../../strongs/h/h8179.md) of the [pinnâ](../../strongs/h/h6438.md).

<a name="jeremiah_31_39"></a>Jeremiah 31:39

And the [midâ](../../strongs/h/h4060.md) [qāv](../../strongs/h/h6957.md) H6961 shall yet [yāṣā'](../../strongs/h/h3318.md) over against it upon the [giḇʿâ](../../strongs/h/h1389.md) [Gārēḇ](../../strongs/h/h1619.md), and shall [cabab](../../strongs/h/h5437.md) to [GōʿÂ](../../strongs/h/h1601.md).

<a name="jeremiah_31_40"></a>Jeremiah 31:40

And the whole [ʿēmeq](../../strongs/h/h6010.md) of the [peḡer](../../strongs/h/h6297.md), and of the [dešen](../../strongs/h/h1880.md), and all the [šᵊḏēmâ](../../strongs/h/h7709.md) H8309 unto the [nachal](../../strongs/h/h5158.md) of [Qiḏrôn](../../strongs/h/h6939.md), unto the [pinnâ](../../strongs/h/h6438.md) of the [sûs](../../strongs/h/h5483.md) [sha'ar](../../strongs/h/h8179.md) toward the [mizrach](../../strongs/h/h4217.md), shall be [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md); it shall not be [nathash](../../strongs/h/h5428.md), nor [harac](../../strongs/h/h2040.md) any more ['owlam](../../strongs/h/h5769.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 30](jeremiah_30.md) - [Jeremiah 32](jeremiah_32.md)