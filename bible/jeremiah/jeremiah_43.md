# [Jeremiah 43](https://www.blueletterbible.org/kjv/jeremiah/43)

<a name="jeremiah_43_1"></a>Jeremiah 43:1

And it came to pass, that when [Yirmᵊyâ](../../strongs/h/h3414.md) had made a [kalah](../../strongs/h/h3615.md) of [dabar](../../strongs/h/h1696.md) unto all the ['am](../../strongs/h/h5971.md) all the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), for which [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md) had [shalach](../../strongs/h/h7971.md) him to them, even all these [dabar](../../strongs/h/h1697.md),

<a name="jeremiah_43_2"></a>Jeremiah 43:2

Then ['āmar](../../strongs/h/h559.md) [ʿĂzaryâ](../../strongs/h/h5838.md) the [ben](../../strongs/h/h1121.md) of [HôšaʿYâ](../../strongs/h/h1955.md), and [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [zed](../../strongs/h/h2086.md) ['enowsh](../../strongs/h/h582.md), ['āmar](../../strongs/h/h559.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), Thou [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md): [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) hath not [shalach](../../strongs/h/h7971.md) thee to ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) not into [Mitsrayim](../../strongs/h/h4714.md) to [guwr](../../strongs/h/h1481.md) there:

<a name="jeremiah_43_3"></a>Jeremiah 43:3

But [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md) setteth thee [sûṯ](../../strongs/h/h5496.md) against us, for [maʿan](../../strongs/h/h4616.md) [nathan](../../strongs/h/h5414.md) us into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), that they might put us to [muwth](../../strongs/h/h4191.md), and [gālâ](../../strongs/h/h1540.md) us into [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_43_4"></a>Jeremiah 43:4

So [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), and all the ['am](../../strongs/h/h5971.md), [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), to [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_43_5"></a>Jeremiah 43:5

But [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md), [laqach](../../strongs/h/h3947.md) all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md), that were [shuwb](../../strongs/h/h7725.md) from all [gowy](../../strongs/h/h1471.md), whither they had been [nāḏaḥ](../../strongs/h/h5080.md), to [guwr](../../strongs/h/h1481.md) in the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md);

<a name="jeremiah_43_6"></a>Jeremiah 43:6

Even [geḇer](../../strongs/h/h1397.md), and ['ishshah](../../strongs/h/h802.md), and [ṭap̄](../../strongs/h/h2945.md), and the [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md), and every [nephesh](../../strongs/h/h5315.md) that [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) had [yānaḥ](../../strongs/h/h3240.md) with [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), and [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), and [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md).

<a name="jeremiah_43_7"></a>Jeremiah 43:7

So they [bow'](../../strongs/h/h935.md) into the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md): for they [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md): thus [bow'](../../strongs/h/h935.md) they even to [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md).

<a name="jeremiah_43_8"></a>Jeremiah 43:8

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md) in [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_43_9"></a>Jeremiah 43:9

[laqach](../../strongs/h/h3947.md) [gadowl](../../strongs/h/h1419.md) ['eben](../../strongs/h/h68.md) in thine [yad](../../strongs/h/h3027.md), and [taman](../../strongs/h/h2934.md) them in the [meleṭ](../../strongs/h/h4423.md) in the [malbēn](../../strongs/h/h4404.md), which is at the [peṯaḥ](../../strongs/h/h6607.md) of [Parʿô](../../strongs/h/h6547.md) [bayith](../../strongs/h/h1004.md) in [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md), in the ['ayin](../../strongs/h/h5869.md) of the ['enowsh](../../strongs/h/h582.md) of [Yᵊhûḏî](../../strongs/h/h3064.md);

<a name="jeremiah_43_10"></a>Jeremiah 43:10

And ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), my ['ebed](../../strongs/h/h5650.md), and will [śûm](../../strongs/h/h7760.md) his [kicce'](../../strongs/h/h3678.md) [maʿal](../../strongs/h/h4605.md) these ['eben](../../strongs/h/h68.md) that I have [taman](../../strongs/h/h2934.md); and he shall [natah](../../strongs/h/h5186.md) his royal [šap̄rûr](../../strongs/h/h8237.md) over them.

<a name="jeremiah_43_11"></a>Jeremiah 43:11

And when he [bow'](../../strongs/h/h935.md), he shall [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), and deliver such as are for [maveth](../../strongs/h/h4194.md) to [maveth](../../strongs/h/h4194.md); and such as are for [šᵊḇî](../../strongs/h/h7628.md) to [šᵊḇî](../../strongs/h/h7628.md); and such as are for the [chereb](../../strongs/h/h2719.md) to the [chereb](../../strongs/h/h2719.md).

<a name="jeremiah_43_12"></a>Jeremiah 43:12

And I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in the [bayith](../../strongs/h/h1004.md) of the ['Elohiym](../../strongs/h/h430.md) of [Mitsrayim](../../strongs/h/h4714.md); and he shall [śārap̄](../../strongs/h/h8313.md) them, and [šāḇâ](../../strongs/h/h7617.md) them: and he shall [ʿāṭâ](../../strongs/h/h5844.md) himself with the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), as a [ra'ah](../../strongs/h/h7462.md) putteth [ʿāṭâ](../../strongs/h/h5844.md) his [beḡeḏ](../../strongs/h/h899.md); and he shall [yāṣā'](../../strongs/h/h3318.md) from thence in [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_43_13"></a>Jeremiah 43:13

He shall [shabar](../../strongs/h/h7665.md) also the [maṣṣēḇâ](../../strongs/h/h4676.md) of [Bêṯ Šemeš](../../strongs/h/h1053.md), that is in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md); and the [bayith](../../strongs/h/h1004.md) of the ['Elohiym](../../strongs/h/h430.md) of the [Mitsrayim](../../strongs/h/h4714.md) shall he [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 42](jeremiah_42.md) - [Jeremiah 44](jeremiah_44.md)