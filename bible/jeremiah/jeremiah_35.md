# [Jeremiah 35](https://www.blueletterbible.org/kjv/jeremiah/35)

<a name="jeremiah_35_1"></a>Jeremiah 35:1

The [dabar](../../strongs/h/h1697.md) which came unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md) in the [yowm](../../strongs/h/h3117.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_35_2"></a>Jeremiah 35:2

[halak](../../strongs/h/h1980.md) unto the [bayith](../../strongs/h/h1004.md) of the [Rēḵâ](../../strongs/h/h7397.md), and [dabar](../../strongs/h/h1696.md) unto them, and [bow'](../../strongs/h/h935.md) them into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), into ['echad](../../strongs/h/h259.md) of the [liškâ](../../strongs/h/h3957.md), and give them [yayin](../../strongs/h/h3196.md) to [šāqâ](../../strongs/h/h8248.md).

<a name="jeremiah_35_3"></a>Jeremiah 35:3

Then I [laqach](../../strongs/h/h3947.md) [Ya'Ăzanyâ](../../strongs/h/h2970.md) the [ben](../../strongs/h/h1121.md) of [Yirmᵊyâ](../../strongs/h/h3414.md), the [ben](../../strongs/h/h1121.md) of [Ḥăḇaṣṣinyâ](../../strongs/h/h2262.md), and his ['ach](../../strongs/h/h251.md), and all his [ben](../../strongs/h/h1121.md), and the whole [bayith](../../strongs/h/h1004.md) of the [Rēḵâ](../../strongs/h/h7397.md);

<a name="jeremiah_35_4"></a>Jeremiah 35:4

And I [bow'](../../strongs/h/h935.md) them into the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), into the [liškâ](../../strongs/h/h3957.md) of the [ben](../../strongs/h/h1121.md) of [Ḥānān](../../strongs/h/h2605.md), the [ben](../../strongs/h/h1121.md) of [Yiḡdalyâû](../../strongs/h/h3012.md), an ['iysh](../../strongs/h/h376.md) of ['Elohiym](../../strongs/h/h430.md), which was ['ēṣel](../../strongs/h/h681.md) the [liškâ](../../strongs/h/h3957.md) of the [śar](../../strongs/h/h8269.md), which was [maʿal](../../strongs/h/h4605.md) the [liškâ](../../strongs/h/h3957.md) of [MaʿĂśêâ](../../strongs/h/h4641.md) the [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md), the [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md):

<a name="jeremiah_35_5"></a>Jeremiah 35:5

And I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) the [ben](../../strongs/h/h1121.md) of the [bayith](../../strongs/h/h1004.md) of the [Rēḵâ](../../strongs/h/h7397.md) [gāḇîaʿ](../../strongs/h/h1375.md) [mālē'](../../strongs/h/h4392.md) of [yayin](../../strongs/h/h3196.md), and [kowc](../../strongs/h/h3563.md), and I ['āmar](../../strongs/h/h559.md) unto them, [šāṯâ](../../strongs/h/h8354.md) ye [yayin](../../strongs/h/h3196.md).

<a name="jeremiah_35_6"></a>Jeremiah 35:6

But they ['āmar](../../strongs/h/h559.md), We will [šāṯâ](../../strongs/h/h8354.md) no [yayin](../../strongs/h/h3196.md): for [Yônāḏāḇ](../../strongs/h/h3122.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md) our ['ab](../../strongs/h/h1.md) [tsavah](../../strongs/h/h6680.md) us, ['āmar](../../strongs/h/h559.md), Ye shall [šāṯâ](../../strongs/h/h8354.md) no [yayin](../../strongs/h/h3196.md), neither ye, nor your [ben](../../strongs/h/h1121.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md):

<a name="jeremiah_35_7"></a>Jeremiah 35:7

Neither shall ye [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md), nor [zāraʿ](../../strongs/h/h2232.md) [zera'](../../strongs/h/h2233.md), nor [nāṭaʿ](../../strongs/h/h5193.md) [kerem](../../strongs/h/h3754.md), nor have any: but all your [yowm](../../strongs/h/h3117.md) ye shall [yashab](../../strongs/h/h3427.md) in ['ohel](../../strongs/h/h168.md); that ye may [ḥāyâ](../../strongs/h/h2421.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md) where ye be [guwr](../../strongs/h/h1481.md).

<a name="jeremiah_35_8"></a>Jeremiah 35:8

Thus have we [shama'](../../strongs/h/h8085.md) the [qowl](../../strongs/h/h6963.md) of [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md) our ['ab](../../strongs/h/h1.md) in all that he hath [tsavah](../../strongs/h/h6680.md) us, to [šāṯâ](../../strongs/h/h8354.md) no [yayin](../../strongs/h/h3196.md) all our [yowm](../../strongs/h/h3117.md), we, our ['ishshah](../../strongs/h/h802.md), our [ben](../../strongs/h/h1121.md), nor our [bath](../../strongs/h/h1323.md);

<a name="jeremiah_35_9"></a>Jeremiah 35:9

Nor to [bānâ](../../strongs/h/h1129.md) [bayith](../../strongs/h/h1004.md) for us to [yashab](../../strongs/h/h3427.md): neither have we [kerem](../../strongs/h/h3754.md), nor [sadeh](../../strongs/h/h7704.md), nor [zera'](../../strongs/h/h2233.md):

<a name="jeremiah_35_10"></a>Jeremiah 35:10

But we have [yashab](../../strongs/h/h3427.md) ['ohel](../../strongs/h/h168.md), and have [shama'](../../strongs/h/h8085.md), and ['asah](../../strongs/h/h6213.md) according to all that [Yônāḏāḇ](../../strongs/h/h3122.md) our ['ab](../../strongs/h/h1.md) [tsavah](../../strongs/h/h6680.md) us.

<a name="jeremiah_35_11"></a>Jeremiah 35:11

But it came to pass, when [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ʿālâ](../../strongs/h/h5927.md) into the ['erets](../../strongs/h/h776.md), that we ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md), and let us [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) for [paniym](../../strongs/h/h6440.md) of the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and for [paniym](../../strongs/h/h6440.md) of the [ḥayil](../../strongs/h/h2428.md) of the ['Ărām](../../strongs/h/h758.md): so we [yashab](../../strongs/h/h3427.md) at [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_35_12"></a>Jeremiah 35:12

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_35_13"></a>Jeremiah 35:13

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md) the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md) and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), Will ye not [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md) to [shama'](../../strongs/h/h8085.md) to my [dabar](../../strongs/h/h1697.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_35_14"></a>Jeremiah 35:14

The [dabar](../../strongs/h/h1697.md) of [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md), that he [tsavah](../../strongs/h/h6680.md) his [ben](../../strongs/h/h1121.md) not to [šāṯâ](../../strongs/h/h8354.md) [yayin](../../strongs/h/h3196.md), are [quwm](../../strongs/h/h6965.md); for unto this [yowm](../../strongs/h/h3117.md) they [šāṯâ](../../strongs/h/h8354.md) none, but [shama'](../../strongs/h/h8085.md) their ['ab](../../strongs/h/h1.md) [mitsvah](../../strongs/h/h4687.md): notwithstanding I have [dabar](../../strongs/h/h1696.md) unto you, rising [šāḵam](../../strongs/h/h7925.md) and [dabar](../../strongs/h/h1696.md); but ye [shama'](../../strongs/h/h8085.md) not unto me.

<a name="jeremiah_35_15"></a>Jeremiah 35:15

I have [shalach](../../strongs/h/h7971.md) also unto you all my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), [šāḵam](../../strongs/h/h7925.md) and [shalach](../../strongs/h/h7971.md) them, ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) ye now every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and [yatab](../../strongs/h/h3190.md) your [maʿălāl](../../strongs/h/h4611.md), and [yālaḵ](../../strongs/h/h3212.md) not ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to ['abad](../../strongs/h/h5647.md) them, and ye shall dwell [yashab](../../strongs/h/h3427.md) the ['ăḏāmâ](../../strongs/h/h127.md) which I have [nathan](../../strongs/h/h5414.md) to you and to your ['ab](../../strongs/h/h1.md): but ye have not [natah](../../strongs/h/h5186.md) your ['ozen](../../strongs/h/h241.md), nor [shama'](../../strongs/h/h8085.md) unto me.

<a name="jeremiah_35_16"></a>Jeremiah 35:16

Because the [ben](../../strongs/h/h1121.md) of [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md) have [quwm](../../strongs/h/h6965.md) the [mitsvah](../../strongs/h/h4687.md) of their ['ab](../../strongs/h/h1.md), which he [tsavah](../../strongs/h/h6680.md) them; but this ['am](../../strongs/h/h5971.md) hath not [shama'](../../strongs/h/h8085.md) unto me:

<a name="jeremiah_35_17"></a>Jeremiah 35:17

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [bow'](../../strongs/h/h935.md) upon [Yehuwdah](../../strongs/h/h3063.md) and upon all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) all the [ra'](../../strongs/h/h7451.md) that I have [dabar](../../strongs/h/h1696.md) against them: because I have [dabar](../../strongs/h/h1696.md) unto them, but they have not [shama'](../../strongs/h/h8085.md); and I have [qara'](../../strongs/h/h7121.md) unto them, but they have not ['anah](../../strongs/h/h6030.md).

<a name="jeremiah_35_18"></a>Jeremiah 35:18

And [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto the [bayith](../../strongs/h/h1004.md) of the [Rēḵâ](../../strongs/h/h7397.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Because ye have [shama'](../../strongs/h/h8085.md) the [mitsvah](../../strongs/h/h4687.md) of [Yᵊhônāḏāḇ](../../strongs/h/h3082.md) your ['ab](../../strongs/h/h1.md), and [shamar](../../strongs/h/h8104.md) all his [mitsvah](../../strongs/h/h4687.md), and ['asah](../../strongs/h/h6213.md) according unto all that he hath [tsavah](../../strongs/h/h6680.md) you:

<a name="jeremiah_35_19"></a>Jeremiah 35:19

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [Yônāḏāḇ](../../strongs/h/h3122.md) the [ben](../../strongs/h/h1121.md) of [Rēḵāḇ](../../strongs/h/h7394.md) shall not [karath](../../strongs/h/h3772.md) an ['iysh](../../strongs/h/h376.md) to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me for [yowm](../../strongs/h/h3117.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 34](jeremiah_34.md) - [Jeremiah 36](jeremiah_36.md)