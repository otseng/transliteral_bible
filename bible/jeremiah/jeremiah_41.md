# [Jeremiah 41](https://www.blueletterbible.org/kjv/jeremiah/41)

<a name="jeremiah_41_1"></a>Jeremiah 41:1

Now it came to pass in the [šᵊḇîʿî](../../strongs/h/h7637.md) [ḥōḏeš](../../strongs/h/h2320.md), that [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) the [ben](../../strongs/h/h1121.md) of ['Ĕlîšāmāʿ](../../strongs/h/h476.md), of the [zera'](../../strongs/h/h2233.md) [mᵊlûḵâ](../../strongs/h/h4410.md), and the [rab](../../strongs/h/h7227.md) of the [melek](../../strongs/h/h4428.md), even [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) with him, [bow'](../../strongs/h/h935.md) unto [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) to [Miṣpâ](../../strongs/h/h4709.md); and there they did ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) [yaḥaḏ](../../strongs/h/h3162.md) in [Miṣpê](../../strongs/h/h4708.md).

<a name="jeremiah_41_2"></a>Jeremiah 41:2

Then [quwm](../../strongs/h/h6965.md) [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), and the [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) that were with him, and [nakah](../../strongs/h/h5221.md) [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md) with the [chereb](../../strongs/h/h2719.md), and [muwth](../../strongs/h/h4191.md) him, whom the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had made [paqad](../../strongs/h/h6485.md) over the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_41_3"></a>Jeremiah 41:3

[Yišmāʿē'l](../../strongs/h/h3458.md) also [nakah](../../strongs/h/h5221.md) all the [Yᵊhûḏî](../../strongs/h/h3064.md) that were with him, even with [Gᵊḏalyâ](../../strongs/h/h1436.md), at [Miṣpâ](../../strongs/h/h4709.md), and the [Kaśdîmâ](../../strongs/h/h3778.md) that were [māṣā'](../../strongs/h/h4672.md) there, and the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_41_4"></a>Jeremiah 41:4

And it came to pass the [šēnî](../../strongs/h/h8145.md) [yowm](../../strongs/h/h3117.md) after he had [muwth](../../strongs/h/h4191.md) [Gᵊḏalyâ](../../strongs/h/h1436.md), and no ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) it,

<a name="jeremiah_41_5"></a>Jeremiah 41:5

That there [bow'](../../strongs/h/h935.md) ['enowsh](../../strongs/h/h582.md) from [Šᵊḵem](../../strongs/h/h7927.md), from [Šîlô](../../strongs/h/h7887.md), and from [Šōmrôn](../../strongs/h/h8111.md), even [šᵊmōnîm](../../strongs/h/h8084.md) ['iysh](../../strongs/h/h376.md), having their [zāqān](../../strongs/h/h2206.md) [gālaḥ](../../strongs/h/h1548.md), and their [beḡeḏ](../../strongs/h/h899.md) [qāraʿ](../../strongs/h/h7167.md), and having [gāḏaḏ](../../strongs/h/h1413.md) themselves, with [minchah](../../strongs/h/h4503.md) and [lᵊḇônâ](../../strongs/h/h3828.md) in their [yad](../../strongs/h/h3027.md), to [bow'](../../strongs/h/h935.md) them to the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_41_6"></a>Jeremiah 41:6

And [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) [yāṣā'](../../strongs/h/h3318.md) from [Miṣpâ](../../strongs/h/h4709.md) to [qārā'](../../strongs/h/h7125.md) them, [bāḵâ](../../strongs/h/h1058.md) all [halak](../../strongs/h/h1980.md) as he [halak](../../strongs/h/h1980.md): and it came to pass, as he [pāḡaš](../../strongs/h/h6298.md) them, he ['āmar](../../strongs/h/h559.md) unto them, [bow'](../../strongs/h/h935.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md).

<a name="jeremiah_41_7"></a>Jeremiah 41:7

And it was so, when they [bow'](../../strongs/h/h935.md) into the [tavek](../../strongs/h/h8432.md) of the [ʿîr](../../strongs/h/h5892.md), that [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) [šāḥaṭ](../../strongs/h/h7819.md) them, and cast them into the [tavek](../../strongs/h/h8432.md) of the [bowr](../../strongs/h/h953.md), he, and the ['enowsh](../../strongs/h/h582.md) that were with him.

<a name="jeremiah_41_8"></a>Jeremiah 41:8

But [ʿeśer](../../strongs/h/h6235.md) ['enowsh](../../strongs/h/h582.md) were [māṣā'](../../strongs/h/h4672.md) among them that ['āmar](../../strongs/h/h559.md) unto [Yišmāʿē'l](../../strongs/h/h3458.md), [muwth](../../strongs/h/h4191.md) us not: for we [yēš](../../strongs/h/h3426.md) [maṭmôn](../../strongs/h/h4301.md) in the [sadeh](../../strongs/h/h7704.md), of [ḥiṭṭâ](../../strongs/h/h2406.md), and of [śᵊʿōrâ](../../strongs/h/h8184.md), and of [šemen](../../strongs/h/h8081.md), and of [dĕbash](../../strongs/h/h1706.md). So he [ḥāḏal](../../strongs/h/h2308.md), and [muwth](../../strongs/h/h4191.md) them not [tavek](../../strongs/h/h8432.md) their ['ach](../../strongs/h/h251.md).

<a name="jeremiah_41_9"></a>Jeremiah 41:9

Now the [bowr](../../strongs/h/h953.md) wherein [Yišmāʿē'l](../../strongs/h/h3458.md) had [shalak](../../strongs/h/h7993.md) all the dead [peḡer](../../strongs/h/h6297.md) of the ['enowsh](../../strongs/h/h582.md), whom he had [nakah](../../strongs/h/h5221.md) [yad](../../strongs/h/h3027.md) of [Gᵊḏalyâ](../../strongs/h/h1436.md), was it which ['Āsā'](../../strongs/h/h609.md) the [melek](../../strongs/h/h4428.md) had ['asah](../../strongs/h/h6213.md) for [paniym](../../strongs/h/h6440.md) of [BaʿŠā'](../../strongs/h/h1201.md) [melek](../../strongs/h/h4428.md) of [Yisra'el](../../strongs/h/h3478.md): and [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) [mālā'](../../strongs/h/h4390.md) it with them that were [ḥālāl](../../strongs/h/h2491.md).

<a name="jeremiah_41_10"></a>Jeremiah 41:10

Then [Yišmāʿē'l](../../strongs/h/h3458.md) [šāḇâ](../../strongs/h/h7617.md) all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md) that were in [Miṣpâ](../../strongs/h/h4709.md), even the [melek](../../strongs/h/h4428.md) [bath](../../strongs/h/h1323.md), and all the ['am](../../strongs/h/h5971.md) that [šā'ar](../../strongs/h/h7604.md) in [Miṣpâ](../../strongs/h/h4709.md), whom [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) had [paqad](../../strongs/h/h6485.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md): and [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) [šāḇâ](../../strongs/h/h7617.md), and [yālaḵ](../../strongs/h/h3212.md) to go ['abar](../../strongs/h/h5674.md) to the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md).

<a name="jeremiah_41_11"></a>Jeremiah 41:11

But when [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) that were with him, [shama'](../../strongs/h/h8085.md) of all the [ra'](../../strongs/h/h7451.md) that [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) had ['asah](../../strongs/h/h6213.md),

<a name="jeremiah_41_12"></a>Jeremiah 41:12

Then they [laqach](../../strongs/h/h3947.md) all the ['enowsh](../../strongs/h/h582.md), and [yālaḵ](../../strongs/h/h3212.md) to [lāḥam](../../strongs/h/h3898.md) with [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), and [māṣā'](../../strongs/h/h4672.md) him by the [rab](../../strongs/h/h7227.md) [mayim](../../strongs/h/h4325.md) that are in [Giḇʿôn](../../strongs/h/h1391.md).

<a name="jeremiah_41_13"></a>Jeremiah 41:13

Now it came to pass, that when all the ['am](../../strongs/h/h5971.md) which were with [Yišmāʿē'l](../../strongs/h/h3458.md) [ra'ah](../../strongs/h/h7200.md) [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) that were with him, then they were [samach](../../strongs/h/h8055.md).

<a name="jeremiah_41_14"></a>Jeremiah 41:14

So all the ['am](../../strongs/h/h5971.md) that [Yišmāʿē'l](../../strongs/h/h3458.md) had [šāḇâ](../../strongs/h/h7617.md) from [Miṣpâ](../../strongs/h/h4709.md) [cabab](../../strongs/h/h5437.md) and [shuwb](../../strongs/h/h7725.md), and [yālaḵ](../../strongs/h/h3212.md) unto [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md).

<a name="jeremiah_41_15"></a>Jeremiah 41:15

But [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) [mālaṭ](../../strongs/h/h4422.md) [paniym](../../strongs/h/h6440.md) [Yôḥānān](../../strongs/h/h3110.md) with [šᵊmōnê](../../strongs/h/h8083.md) ['enowsh](../../strongs/h/h582.md), and [yālaḵ](../../strongs/h/h3212.md) to the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md).

<a name="jeremiah_41_16"></a>Jeremiah 41:16

Then [laqach](../../strongs/h/h3947.md) [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) that were with him, all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['am](../../strongs/h/h5971.md) whom he had [shuwb](../../strongs/h/h7725.md) from [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), from [Miṣpâ](../../strongs/h/h4709.md), ['aḥar](../../strongs/h/h310.md) that he had [nakah](../../strongs/h/h5221.md) [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md), even [geḇer](../../strongs/h/h1397.md) ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md), and the ['ishshah](../../strongs/h/h802.md), and the [ṭap̄](../../strongs/h/h2945.md), and the [sārîs](../../strongs/h/h5631.md), whom he had brought [shuwb](../../strongs/h/h7725.md) from [Giḇʿôn](../../strongs/h/h1391.md):

<a name="jeremiah_41_17"></a>Jeremiah 41:17

And they [yālaḵ](../../strongs/h/h3212.md), and [yashab](../../strongs/h/h3427.md) the [gērûṯ](../../strongs/h/h1628.md) of [Kimhām](../../strongs/h/h3643.md), which is ['ēṣel](../../strongs/h/h681.md) [Bêṯ leḥem](../../strongs/h/h1035.md), to [yālaḵ](../../strongs/h/h3212.md) to [bow'](../../strongs/h/h935.md) into [Mitsrayim](../../strongs/h/h4714.md),

<a name="jeremiah_41_18"></a>Jeremiah 41:18

[paniym](../../strongs/h/h6440.md) of the [Kaśdîmâ](../../strongs/h/h3778.md): for they were [yare'](../../strongs/h/h3372.md) of them, because [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) had [nakah](../../strongs/h/h5221.md) [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md), whom the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) made [paqad](../../strongs/h/h6485.md) in the ['erets](../../strongs/h/h776.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 40](jeremiah_40.md) - [Jeremiah 42](jeremiah_42.md)