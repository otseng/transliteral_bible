# [Jeremiah 5](https://www.blueletterbible.org/kjv/jeremiah/5)

<a name="jeremiah_5_1"></a>Jeremiah 5:1

Run ye to and [šûṭ](../../strongs/h/h7751.md) through the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and [ra'ah](../../strongs/h/h7200.md) now, and [yada'](../../strongs/h/h3045.md), and [bāqaš](../../strongs/h/h1245.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md) thereof, if ye can [māṣā'](../../strongs/h/h4672.md) an ['iysh](../../strongs/h/h376.md), if there [yēš](../../strongs/h/h3426.md) any that ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md), that [bāqaš](../../strongs/h/h1245.md) the ['ĕmûnâ](../../strongs/h/h530.md); and I will [sālaḥ](../../strongs/h/h5545.md) it.

<a name="jeremiah_5_2"></a>Jeremiah 5:2

And though they ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md); surely they [shaba'](../../strongs/h/h7650.md) [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_5_3"></a>Jeremiah 5:3

[Yĕhovah](../../strongs/h/h3068.md), are not thine ['ayin](../../strongs/h/h5869.md) upon the ['ĕmûnâ](../../strongs/h/h530.md)? thou hast [nakah](../../strongs/h/h5221.md) them, but they have not [chuwl](../../strongs/h/h2342.md); thou hast [kalah](../../strongs/h/h3615.md) them, but they have [mā'ēn](../../strongs/h/h3985.md) to [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md): they have made their [paniym](../../strongs/h/h6440.md) [ḥāzaq](../../strongs/h/h2388.md) than a [cela'](../../strongs/h/h5553.md); they have [mā'ēn](../../strongs/h/h3985.md) to [shuwb](../../strongs/h/h7725.md).

<a name="jeremiah_5_4"></a>Jeremiah 5:4

Therefore I ['āmar](../../strongs/h/h559.md), Surely these are [dal](../../strongs/h/h1800.md); they are [yā'al](../../strongs/h/h2973.md): for they [yada'](../../strongs/h/h3045.md) not the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), nor the [mishpat](../../strongs/h/h4941.md) of their ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_5_5"></a>Jeremiah 5:5

I will [yālaḵ](../../strongs/h/h3212.md) me unto the [gadowl](../../strongs/h/h1419.md), and will [dabar](../../strongs/h/h1696.md) unto them; for they have [yada'](../../strongs/h/h3045.md) the [derek](../../strongs/h/h1870.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [mishpat](../../strongs/h/h4941.md) of their ['Elohiym](../../strongs/h/h430.md): but these have [yaḥaḏ](../../strongs/h/h3162.md) [shabar](../../strongs/h/h7665.md) the [ʿōl](../../strongs/h/h5923.md), and [nathaq](../../strongs/h/h5423.md) the [mowcer](../../strongs/h/h4147.md).

<a name="jeremiah_5_6"></a>Jeremiah 5:6

Wherefore an ['ariy](../../strongs/h/h738.md) out of the [yaʿar](../../strongs/h/h3293.md) shall [nakah](../../strongs/h/h5221.md) them, and a [zᵊ'ēḇ](../../strongs/h/h2061.md) of the ['arabah](../../strongs/h/h6160.md) shall [shadad](../../strongs/h/h7703.md) them, a [nāmēr](../../strongs/h/h5246.md) shall [šāqaḏ](../../strongs/h/h8245.md) over their [ʿîr](../../strongs/h/h5892.md): every one that goeth [yāṣā'](../../strongs/h/h3318.md) thence [hēnnâ](../../strongs/h/h2007.md) be torn in [taraph](../../strongs/h/h2963.md): because their [pesha'](../../strongs/h/h6588.md) are [rabab](../../strongs/h/h7231.md), and their [mᵊšûḇâ](../../strongs/h/h4878.md) are [ʿāṣam](../../strongs/h/h6105.md).

<a name="jeremiah_5_7"></a>Jeremiah 5:7

['ay](../../strongs/h/h335.md) shall I [sālaḥ](../../strongs/h/h5545.md) thee for [zō'ṯ](../../strongs/h/h2063.md)? thy [ben](../../strongs/h/h1121.md) have ['azab](../../strongs/h/h5800.md) me, and [shaba'](../../strongs/h/h7650.md) by them that are [lō'](../../strongs/h/h3808.md) ['Elohiym](../../strongs/h/h430.md): when I had [shaba'](../../strongs/h/h7650.md), they then [na'aph](../../strongs/h/h5003.md), and [gāḏaḏ](../../strongs/h/h1413.md) in the [zānâ](../../strongs/h/h2181.md) [bayith](../../strongs/h/h1004.md).

<a name="jeremiah_5_8"></a>Jeremiah 5:8

They were as [zûn](../../strongs/h/h2109.md) [sûs](../../strongs/h/h5483.md) in the [šāḵâ](../../strongs/h/h7904.md): every ['iysh](../../strongs/h/h376.md) [ṣāhal](../../strongs/h/h6670.md) after his [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md).

<a name="jeremiah_5_9"></a>Jeremiah 5:9

Shall I not [paqad](../../strongs/h/h6485.md) for these things? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): and shall not my [nephesh](../../strongs/h/h5315.md) be [naqam](../../strongs/h/h5358.md) on such a [gowy](../../strongs/h/h1471.md) as this?

<a name="jeremiah_5_10"></a>Jeremiah 5:10

Go ye [ʿālâ](../../strongs/h/h5927.md) upon her [šārâ](../../strongs/h/h8284.md), and [shachath](../../strongs/h/h7843.md); but ['asah](../../strongs/h/h6213.md) not a [kālâ](../../strongs/h/h3617.md): [cuwr](../../strongs/h/h5493.md) her [nᵊṭîšâ](../../strongs/h/h5189.md); for they are not [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_5_11"></a>Jeremiah 5:11

For the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) have [bāḡaḏ](../../strongs/h/h898.md) [bāḡaḏ](../../strongs/h/h898.md) against me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_5_12"></a>Jeremiah 5:12

They have [kāḥaš](../../strongs/h/h3584.md) [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md), It is not he; neither shall [ra'](../../strongs/h/h7451.md) [bow'](../../strongs/h/h935.md) upon us; neither shall we [ra'ah](../../strongs/h/h7200.md) [chereb](../../strongs/h/h2719.md) nor [rāʿāḇ](../../strongs/h/h7458.md):

<a name="jeremiah_5_13"></a>Jeremiah 5:13

And the [nāḇî'](../../strongs/h/h5030.md) shall become [ruwach](../../strongs/h/h7307.md), and the [dabar](../../strongs/h/h1696.md) is not in them: thus shall it be ['asah](../../strongs/h/h6213.md) unto them.

<a name="jeremiah_5_14"></a>Jeremiah 5:14

Wherefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), Because ye [dabar](../../strongs/h/h1696.md) this [dabar](../../strongs/h/h1697.md), behold, I will [nathan](../../strongs/h/h5414.md) my [dabar](../../strongs/h/h1697.md) in thy [peh](../../strongs/h/h6310.md) ['esh](../../strongs/h/h784.md), and this ['am](../../strongs/h/h5971.md) ['ets](../../strongs/h/h6086.md), and it shall ['akal](../../strongs/h/h398.md) them.

<a name="jeremiah_5_15"></a>Jeremiah 5:15

Lo, I will [bow'](../../strongs/h/h935.md) a [gowy](../../strongs/h/h1471.md) upon you from [merḥāq](../../strongs/h/h4801.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): it is a ['êṯān](../../strongs/h/h386.md) [gowy](../../strongs/h/h1471.md), it is an ['owlam](../../strongs/h/h5769.md) [gowy](../../strongs/h/h1471.md), a [gowy](../../strongs/h/h1471.md) whose [lashown](../../strongs/h/h3956.md) thou [yada'](../../strongs/h/h3045.md) not, neither [shama'](../../strongs/h/h8085.md) what they [dabar](../../strongs/h/h1696.md).

<a name="jeremiah_5_16"></a>Jeremiah 5:16

Their ['ašpâ](../../strongs/h/h827.md) is as a [pāṯaḥ](../../strongs/h/h6605.md) [qeber](../../strongs/h/h6913.md), they are all [gibôr](../../strongs/h/h1368.md).

<a name="jeremiah_5_17"></a>Jeremiah 5:17

And they shall ['akal](../../strongs/h/h398.md) thine [qāṣîr](../../strongs/h/h7105.md), and thy [lechem](../../strongs/h/h3899.md), which thy [ben](../../strongs/h/h1121.md) and thy [bath](../../strongs/h/h1323.md) should ['akal](../../strongs/h/h398.md): they shall ['akal](../../strongs/h/h398.md) thy [tso'n](../../strongs/h/h6629.md) and thine [bāqār](../../strongs/h/h1241.md): they shall eat ['akal](../../strongs/h/h398.md) thy [gep̄en](../../strongs/h/h1612.md) and thy [tĕ'en](../../strongs/h/h8384.md): they shall [rāšaš](../../strongs/h/h7567.md) thy [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md), wherein [hēnnâ](../../strongs/h/h2007.md) [batach](../../strongs/h/h982.md), with the [chereb](../../strongs/h/h2719.md).

<a name="jeremiah_5_18"></a>Jeremiah 5:18

Nevertheless in those [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), I will not ['asah](../../strongs/h/h6213.md) a [kālâ](../../strongs/h/h3617.md) with you.

<a name="jeremiah_5_19"></a>Jeremiah 5:19

And it shall come to pass, when ye shall ['āmar](../../strongs/h/h559.md), [taḥaṯ](../../strongs/h/h8478.md) [mah](../../strongs/h/h4100.md) ['asah](../../strongs/h/h6213.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) all these things unto us? then shalt thou ['āmar](../../strongs/h/h559.md) them, Like as ye have ['azab](../../strongs/h/h5800.md) me, and ['abad](../../strongs/h/h5647.md) [nēḵār](../../strongs/h/h5236.md) ['Elohiym](../../strongs/h/h430.md) in your ['erets](../../strongs/h/h776.md), so shall ye ['abad](../../strongs/h/h5647.md) [zûr](../../strongs/h/h2114.md) in an ['erets](../../strongs/h/h776.md) that is not yours.

<a name="jeremiah_5_20"></a>Jeremiah 5:20

[nāḡaḏ](../../strongs/h/h5046.md) this in the [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and [shama'](../../strongs/h/h8085.md) it in [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_5_21"></a>Jeremiah 5:21

[shama'](../../strongs/h/h8085.md) now this, O [sāḵāl](../../strongs/h/h5530.md) ['am](../../strongs/h/h5971.md), and without [leb](../../strongs/h/h3820.md); which have ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) not; which have ['ozen](../../strongs/h/h241.md), and [shama'](../../strongs/h/h8085.md) not:

<a name="jeremiah_5_22"></a>Jeremiah 5:22

[yare'](../../strongs/h/h3372.md) ye not me? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): will ye not [chuwl](../../strongs/h/h2342.md) at my [paniym](../../strongs/h/h6440.md), which have [śûm](../../strongs/h/h7760.md) the [ḥôl](../../strongs/h/h2344.md) for the [gᵊḇûl](../../strongs/h/h1366.md) of the [yam](../../strongs/h/h3220.md) by a ['owlam](../../strongs/h/h5769.md) [choq](../../strongs/h/h2706.md), that it cannot ['abar](../../strongs/h/h5674.md) it: and though the [gal](../../strongs/h/h1530.md) thereof [gāʿaš](../../strongs/h/h1607.md) themselves, yet can they not [yakol](../../strongs/h/h3201.md); though they [hāmâ](../../strongs/h/h1993.md), yet can they not ['abar](../../strongs/h/h5674.md) it?

<a name="jeremiah_5_23"></a>Jeremiah 5:23

But this ['am](../../strongs/h/h5971.md) hath a [sārar](../../strongs/h/h5637.md) and a [marah](../../strongs/h/h4784.md) [leb](../../strongs/h/h3820.md); they are [cuwr](../../strongs/h/h5493.md) and [yālaḵ](../../strongs/h/h3212.md).

<a name="jeremiah_5_24"></a>Jeremiah 5:24

Neither ['āmar](../../strongs/h/h559.md) they in their [lebab](../../strongs/h/h3824.md), Let us now [yare'](../../strongs/h/h3372.md) [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), that [nathan](../../strongs/h/h5414.md) [gešem](../../strongs/h/h1653.md), both the [yôrâ](../../strongs/h/h3138.md) and the [malqôš](../../strongs/h/h4456.md), in his [ʿēṯ](../../strongs/h/h6256.md): he [shamar](../../strongs/h/h8104.md) unto us the [chuqqah](../../strongs/h/h2708.md) [šāḇûaʿ](../../strongs/h/h7620.md) of the [qāṣîr](../../strongs/h/h7105.md).

<a name="jeremiah_5_25"></a>Jeremiah 5:25

Your ['avon](../../strongs/h/h5771.md) have [natah](../../strongs/h/h5186.md) these things, and your [chatta'ath](../../strongs/h/h2403.md) have [mānaʿ](../../strongs/h/h4513.md) [towb](../../strongs/h/h2896.md) things from you.

<a name="jeremiah_5_26"></a>Jeremiah 5:26

For among my ['am](../../strongs/h/h5971.md) are [māṣā'](../../strongs/h/h4672.md) [rasha'](../../strongs/h/h7563.md) men: they lay [šûr](../../strongs/h/h7789.md), as he that [šāḵaḵ](../../strongs/h/h7918.md) [yāqôš](../../strongs/h/h3353.md); they [nāṣaḇ](../../strongs/h/h5324.md) a [mašḥîṯ](../../strongs/h/h4889.md), they [lāḵaḏ](../../strongs/h/h3920.md) ['enowsh](../../strongs/h/h582.md).

<a name="jeremiah_5_27"></a>Jeremiah 5:27

As a [kᵊlûḇ](../../strongs/h/h3619.md) is [mālē'](../../strongs/h/h4392.md) of [ʿôp̄](../../strongs/h/h5775.md), so are their [bayith](../../strongs/h/h1004.md) [mālē'](../../strongs/h/h4392.md) of [mirmah](../../strongs/h/h4820.md): therefore they are become [gāḏal](../../strongs/h/h1431.md), and [ʿāšar](../../strongs/h/h6238.md).

<a name="jeremiah_5_28"></a>Jeremiah 5:28

They are [šāman](../../strongs/h/h8080.md), they [ʿāšaṯ](../../strongs/h/h6245.md): yea, they ['abar](../../strongs/h/h5674.md) the [dabar](../../strongs/h/h1697.md) of the [ra'](../../strongs/h/h7451.md): they [diyn](../../strongs/h/h1777.md) not the [diyn](../../strongs/h/h1779.md), the cause of the [yathowm](../../strongs/h/h3490.md), yet they [tsalach](../../strongs/h/h6743.md); and the [mishpat](../../strongs/h/h4941.md) of the ['ebyown](../../strongs/h/h34.md) do they not [shaphat](../../strongs/h/h8199.md).

<a name="jeremiah_5_29"></a>Jeremiah 5:29

Shall I not [paqad](../../strongs/h/h6485.md) for these things? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): shall not my [nephesh](../../strongs/h/h5315.md) be [naqam](../../strongs/h/h5358.md) on such a [gowy](../../strongs/h/h1471.md) as this?

<a name="jeremiah_5_30"></a>Jeremiah 5:30

A [šammâ](../../strongs/h/h8047.md) and [šaʿărûr](../../strongs/h/h8186.md) is [hayah](../../strongs/h/h1961.md) in the ['erets](../../strongs/h/h776.md);

<a name="jeremiah_5_31"></a>Jeremiah 5:31

The [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md), and the [kōhēn](../../strongs/h/h3548.md) bear [radah](../../strongs/h/h7287.md) by their [yad](../../strongs/h/h3027.md); and my ['am](../../strongs/h/h5971.md) ['ahab](../../strongs/h/h157.md) to have it so: and what will ye ['asah](../../strongs/h/h6213.md) in the ['aḥărîṯ](../../strongs/h/h319.md) thereof?

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 4](jeremiah_4.md) - [Jeremiah 6](jeremiah_6.md)