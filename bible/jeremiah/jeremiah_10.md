# [Jeremiah 10](https://www.blueletterbible.org/kjv/jeremiah/10)

<a name="jeremiah_10_1"></a>Jeremiah 10:1

[shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) which [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) unto you, O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="jeremiah_10_2"></a>Jeremiah 10:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [lamad](../../strongs/h/h3925.md) not the [derek](../../strongs/h/h1870.md) of the [gowy](../../strongs/h/h1471.md), and be not [ḥāṯaṯ](../../strongs/h/h2865.md) at the ['ôṯ](../../strongs/h/h226.md) of [shamayim](../../strongs/h/h8064.md); for the [gowy](../../strongs/h/h1471.md) are [ḥāṯaṯ](../../strongs/h/h2865.md) at [hēm](../../strongs/h/h1992.md).

<a name="jeremiah_10_3"></a>Jeremiah 10:3

For the [chuqqah](../../strongs/h/h2708.md) of the ['am](../../strongs/h/h5971.md) are [heḇel](../../strongs/h/h1892.md): for one [karath](../../strongs/h/h3772.md) an ['ets](../../strongs/h/h6086.md) out of the [yaʿar](../../strongs/h/h3293.md), the [ma'aseh](../../strongs/h/h4639.md) of the [yad](../../strongs/h/h3027.md) of the [ḥārāš](../../strongs/h/h2796.md), with the [maʿăṣāḏ](../../strongs/h/h4621.md).

<a name="jeremiah_10_4"></a>Jeremiah 10:4

They [yāp̄â](../../strongs/h/h3302.md) it with [keceph](../../strongs/h/h3701.md) and with [zāhāḇ](../../strongs/h/h2091.md); they [ḥāzaq](../../strongs/h/h2388.md) it with [masmēr](../../strongs/h/h4548.md) and with [maqqāḇâ](../../strongs/h/h4717.md), that it [pûq](../../strongs/h/h6328.md) not.

<a name="jeremiah_10_5"></a>Jeremiah 10:5

They are [miqšâ](../../strongs/h/h4749.md) as the [tōmer](../../strongs/h/h8560.md), but [dabar](../../strongs/h/h1696.md) not: they must [nasa'](../../strongs/h/h5375.md) be [nasa'](../../strongs/h/h5375.md), because they cannot [ṣāʿaḏ](../../strongs/h/h6805.md). Be not [yare'](../../strongs/h/h3372.md) of them; for they cannot do [ra'a'](../../strongs/h/h7489.md), neither also is it in them to do [yatab](../../strongs/h/h3190.md).

<a name="jeremiah_10_6"></a>Jeremiah 10:6

Forasmuch as there is none like unto thee, [Yĕhovah](../../strongs/h/h3068.md); thou art [gadowl](../../strongs/h/h1419.md), and thy [shem](../../strongs/h/h8034.md) is [gadowl](../../strongs/h/h1419.md) in [gᵊḇûrâ](../../strongs/h/h1369.md).

<a name="jeremiah_10_7"></a>Jeremiah 10:7

Who would not [yare'](../../strongs/h/h3372.md) thee, O [melek](../../strongs/h/h4428.md) of [gowy](../../strongs/h/h1471.md)? for to thee doth it [yā'â](../../strongs/h/h2969.md): forasmuch as among all the [ḥāḵām](../../strongs/h/h2450.md) men of the [gowy](../../strongs/h/h1471.md), and in all their [malkuwth](../../strongs/h/h4438.md), there is none like unto thee.

<a name="jeremiah_10_8"></a>Jeremiah 10:8

But they are ['echad](../../strongs/h/h259.md) [bāʿar](../../strongs/h/h1197.md) and [kāsal](../../strongs/h/h3688.md): the ['ets](../../strongs/h/h6086.md) is a [mûsār](../../strongs/h/h4148.md) of [heḇel](../../strongs/h/h1892.md).

<a name="jeremiah_10_9"></a>Jeremiah 10:9

[keceph](../../strongs/h/h3701.md) spread into [rāqaʿ](../../strongs/h/h7554.md) is [bow'](../../strongs/h/h935.md) from [Taršîš](../../strongs/h/h8659.md), and [zāhāḇ](../../strongs/h/h2091.md) from ['Ûp̄Āz](../../strongs/h/h210.md), the [ma'aseh](../../strongs/h/h4639.md) of the [ḥārāš](../../strongs/h/h2796.md), and of the [yad](../../strongs/h/h3027.md) of the [tsaraph](../../strongs/h/h6884.md): [tᵊḵēleṯ](../../strongs/h/h8504.md) and ['argāmān](../../strongs/h/h713.md) is their [lᵊḇûš](../../strongs/h/h3830.md): they are all the [ma'aseh](../../strongs/h/h4639.md) of [ḥāḵām](../../strongs/h/h2450.md) men.

<a name="jeremiah_10_10"></a>Jeremiah 10:10

But [Yĕhovah](../../strongs/h/h3068.md) is the ['emeth](../../strongs/h/h571.md) ['Elohiym](../../strongs/h/h430.md), he is the [chay](../../strongs/h/h2416.md) ['Elohiym](../../strongs/h/h430.md), and an ['owlam](../../strongs/h/h5769.md) [melek](../../strongs/h/h4428.md): at his [qeṣep̄](../../strongs/h/h7110.md) the ['erets](../../strongs/h/h776.md) shall [rāʿaš](../../strongs/h/h7493.md), and the [gowy](../../strongs/h/h1471.md) shall not be able to [kûl](../../strongs/h/h3557.md) his [zaʿam](../../strongs/h/h2195.md).

<a name="jeremiah_10_11"></a>Jeremiah 10:11

[dēn](../../strongs/h/h1836.md) shall ye ['ămar](../../strongs/h/h560.md) unto them, The ['ĕlâ](../../strongs/h/h426.md) that have [lā'](../../strongs/h/h3809.md) [ʿăḇaḏ](../../strongs/h/h5648.md) the [šᵊmayin](../../strongs/h/h8065.md) and the ['ăraq](../../strongs/h/h778.md), even they shall ['ăḇaḏ](../../strongs/h/h7.md) from the ['ăraʿ](../../strongs/h/h772.md), and from [tᵊḥôṯ](../../strongs/h/h8460.md) ['ēllê](../../strongs/h/h429.md) [šᵊmayin](../../strongs/h/h8065.md).

<a name="jeremiah_10_12"></a>Jeremiah 10:12

He hath ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md) by his [koach](../../strongs/h/h3581.md), he hath [kuwn](../../strongs/h/h3559.md) the [tebel](../../strongs/h/h8398.md) by his [ḥāḵmâ](../../strongs/h/h2451.md), and hath [natah](../../strongs/h/h5186.md) the [shamayim](../../strongs/h/h8064.md) by his [tāḇûn](../../strongs/h/h8394.md).

<a name="jeremiah_10_13"></a>Jeremiah 10:13

When he [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md), there is a [hāmôn](../../strongs/h/h1995.md) of [mayim](../../strongs/h/h4325.md) in the [shamayim](../../strongs/h/h8064.md), and he causeth the [nāśî'](../../strongs/h/h5387.md) to [ʿālâ](../../strongs/h/h5927.md) from the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md); he ['asah](../../strongs/h/h6213.md) [baraq](../../strongs/h/h1300.md) with [māṭār](../../strongs/h/h4306.md), and [yāṣā'](../../strongs/h/h3318.md) the [ruwach](../../strongs/h/h7307.md) out of his ['ôṣār](../../strongs/h/h214.md).

<a name="jeremiah_10_14"></a>Jeremiah 10:14

Every ['āḏām](../../strongs/h/h120.md) is [bāʿar](../../strongs/h/h1197.md) in his [da'ath](../../strongs/h/h1847.md): every [tsaraph](../../strongs/h/h6884.md) is [yāḇēš](../../strongs/h/h3001.md) by the [pecel](../../strongs/h/h6459.md): for his [necek](../../strongs/h/h5262.md) is [sheqer](../../strongs/h/h8267.md), and there is no [ruwach](../../strongs/h/h7307.md) in them.

<a name="jeremiah_10_15"></a>Jeremiah 10:15

They are [heḇel](../../strongs/h/h1892.md), and the [ma'aseh](../../strongs/h/h4639.md) of [taʿtuʿîm](../../strongs/h/h8595.md): in the [ʿēṯ](../../strongs/h/h6256.md) of their [pᵊqudâ](../../strongs/h/h6486.md) they shall ['abad](../../strongs/h/h6.md).

<a name="jeremiah_10_16"></a>Jeremiah 10:16

The [cheleq](../../strongs/h/h2506.md) of [Ya'aqob](../../strongs/h/h3290.md) is not like them: for he is the [yāṣar](../../strongs/h/h3335.md) of all things; and [Yisra'el](../../strongs/h/h3478.md) is the [shebet](../../strongs/h/h7626.md) of his [nachalah](../../strongs/h/h5159.md): [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md).

<a name="jeremiah_10_17"></a>Jeremiah 10:17

['āsap̄](../../strongs/h/h622.md) thy [kᵊnāʿâ](../../strongs/h/h3666.md) out of the ['erets](../../strongs/h/h776.md), O [yashab](../../strongs/h/h3427.md) of the [māṣôr](../../strongs/h/h4692.md).

<a name="jeremiah_10_18"></a>Jeremiah 10:18

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [qālaʿ](../../strongs/h/h7049.md) out the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) at this [pa'am](../../strongs/h/h6471.md), and will [tsarar](../../strongs/h/h6887.md) them, that they may [māṣā'](../../strongs/h/h4672.md) it so.

<a name="jeremiah_10_19"></a>Jeremiah 10:19

['owy](../../strongs/h/h188.md) is me for my [šeḇar](../../strongs/h/h7667.md)! my [makâ](../../strongs/h/h4347.md) is [ḥālâ](../../strongs/h/h2470.md): but I ['āmar](../../strongs/h/h559.md), ['aḵ](../../strongs/h/h389.md) this is a [ḥŏlî](../../strongs/h/h2483.md), and I must [nasa'](../../strongs/h/h5375.md) it.

<a name="jeremiah_10_20"></a>Jeremiah 10:20

My ['ohel](../../strongs/h/h168.md) is [shadad](../../strongs/h/h7703.md), and all my [mêṯār](../../strongs/h/h4340.md) are [nathaq](../../strongs/h/h5423.md): my [ben](../../strongs/h/h1121.md) are [yāṣā'](../../strongs/h/h3318.md) of me, and they are not: there is none to stretch [natah](../../strongs/h/h5186.md) my ['ohel](../../strongs/h/h168.md) any more, and to set [quwm](../../strongs/h/h6965.md) my [yᵊrîʿâ](../../strongs/h/h3407.md).

<a name="jeremiah_10_21"></a>Jeremiah 10:21

For the [ra'ah](../../strongs/h/h7462.md) are become [bāʿar](../../strongs/h/h1197.md), and have not [darash](../../strongs/h/h1875.md) [Yĕhovah](../../strongs/h/h3068.md): therefore they shall not [sakal](../../strongs/h/h7919.md), and all their [marʿîṯ](../../strongs/h/h4830.md) shall be [puwts](../../strongs/h/h6327.md).

<a name="jeremiah_10_22"></a>Jeremiah 10:22

Behold, the [qowl](../../strongs/h/h6963.md) of the [šᵊmûʿâ](../../strongs/h/h8052.md) is [bow'](../../strongs/h/h935.md), and a [gadowl](../../strongs/h/h1419.md) [raʿaš](../../strongs/h/h7494.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md), to [śûm](../../strongs/h/h7760.md) the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) [šᵊmāmâ](../../strongs/h/h8077.md), and a [māʿôn](../../strongs/h/h4583.md) of [tannîn](../../strongs/h/h8577.md).

<a name="jeremiah_10_23"></a>Jeremiah 10:23

[Yĕhovah](../../strongs/h/h3068.md), I [yada'](../../strongs/h/h3045.md) that the [derek](../../strongs/h/h1870.md) of ['āḏām](../../strongs/h/h120.md) is not in himself: it is not in ['iysh](../../strongs/h/h376.md) that [halak](../../strongs/h/h1980.md) to [kuwn](../../strongs/h/h3559.md) his [ṣaʿaḏ](../../strongs/h/h6806.md).

<a name="jeremiah_10_24"></a>Jeremiah 10:24

[Yĕhovah](../../strongs/h/h3068.md), [yacar](../../strongs/h/h3256.md) me, but with [mishpat](../../strongs/h/h4941.md); not in thine ['aph](../../strongs/h/h639.md), lest thou bring me to [māʿaṭ](../../strongs/h/h4591.md).

<a name="jeremiah_10_25"></a>Jeremiah 10:25

[šāp̄aḵ](../../strongs/h/h8210.md) thy [chemah](../../strongs/h/h2534.md) upon the [gowy](../../strongs/h/h1471.md) that [yada'](../../strongs/h/h3045.md) thee not, and upon the [mišpāḥâ](../../strongs/h/h4940.md) that [qara'](../../strongs/h/h7121.md) not on thy [shem](../../strongs/h/h8034.md): for they have eaten ['akal](../../strongs/h/h398.md) [Ya'aqob](../../strongs/h/h3290.md), and ['akal](../../strongs/h/h398.md) him, and [kalah](../../strongs/h/h3615.md) him, and have made his [nāvê](../../strongs/h/h5116.md) [šāmēm](../../strongs/h/h8074.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 9](jeremiah_9.md) - [Jeremiah 11](jeremiah_11.md)