# [Jeremiah 13](https://www.blueletterbible.org/kjv/jeremiah/13)

<a name="jeremiah_13_1"></a>Jeremiah 13:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, [halak](../../strongs/h/h1980.md) and [qānâ](../../strongs/h/h7069.md) thee a [pēšeṯ](../../strongs/h/h6593.md) ['ēzôr](../../strongs/h/h232.md), and [śûm](../../strongs/h/h7760.md) it upon thy [māṯnayim](../../strongs/h/h4975.md), and [bow'](../../strongs/h/h935.md) it not in [mayim](../../strongs/h/h4325.md).

<a name="jeremiah_13_2"></a>Jeremiah 13:2

So I [qānâ](../../strongs/h/h7069.md) an ['ēzôr](../../strongs/h/h232.md) according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and [śûm](../../strongs/h/h7760.md) it on my [māṯnayim](../../strongs/h/h4975.md).

<a name="jeremiah_13_3"></a>Jeremiah 13:3

And the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me the second [šēnî](../../strongs/h/h8145.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_13_4"></a>Jeremiah 13:4

[laqach](../../strongs/h/h3947.md) the ['ēzôr](../../strongs/h/h232.md) that thou hast [qānâ](../../strongs/h/h7069.md), which is upon thy [māṯnayim](../../strongs/h/h4975.md), and [quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) to [Pᵊrāṯ](../../strongs/h/h6578.md), and [taman](../../strongs/h/h2934.md) it there in a [nāqîq](../../strongs/h/h5357.md) of the [cela'](../../strongs/h/h5553.md).

<a name="jeremiah_13_5"></a>Jeremiah 13:5

So I [yālaḵ](../../strongs/h/h3212.md), and [taman](../../strongs/h/h2934.md) it by [Pᵊrāṯ](../../strongs/h/h6578.md), as [Yĕhovah](../../strongs/h/h3068.md) [tsavah](../../strongs/h/h6680.md) me.

<a name="jeremiah_13_6"></a>Jeremiah 13:6

And it came to pass [qēṣ](../../strongs/h/h7093.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md), that [Yĕhovah](../../strongs/h/h3068.md) ['āmar](../../strongs/h/h559.md) unto me, [quwm](../../strongs/h/h6965.md), [yālaḵ](../../strongs/h/h3212.md) to [Pᵊrāṯ](../../strongs/h/h6578.md), and [laqach](../../strongs/h/h3947.md) the ['ēzôr](../../strongs/h/h232.md) from thence, which I [tsavah](../../strongs/h/h6680.md) thee to [taman](../../strongs/h/h2934.md) there.

<a name="jeremiah_13_7"></a>Jeremiah 13:7

Then I [yālaḵ](../../strongs/h/h3212.md) to [Pᵊrāṯ](../../strongs/h/h6578.md), and [chaphar](../../strongs/h/h2658.md), and [laqach](../../strongs/h/h3947.md) the ['ēzôr](../../strongs/h/h232.md) from the [maqowm](../../strongs/h/h4725.md) where I had [taman](../../strongs/h/h2934.md) it: and, behold, the ['ēzôr](../../strongs/h/h232.md) was [shachath](../../strongs/h/h7843.md), it was [tsalach](../../strongs/h/h6743.md) for nothing.

<a name="jeremiah_13_8"></a>Jeremiah 13:8

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_13_9"></a>Jeremiah 13:9

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), After this manner will I [shachath](../../strongs/h/h7843.md) the [gā'ôn](../../strongs/h/h1347.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [rab](../../strongs/h/h7227.md) [gā'ôn](../../strongs/h/h1347.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_13_10"></a>Jeremiah 13:10

This [ra'](../../strongs/h/h7451.md) ['am](../../strongs/h/h5971.md), which [mē'ēn](../../strongs/h/h3987.md) to [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md), which [halak](../../strongs/h/h1980.md) in the [šᵊrîrûṯ](../../strongs/h/h8307.md) of their [leb](../../strongs/h/h3820.md), and [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), to ['abad](../../strongs/h/h5647.md) them, and to [shachah](../../strongs/h/h7812.md) them, shall even be as this ['ēzôr](../../strongs/h/h232.md), which is [tsalach](../../strongs/h/h6743.md) for nothing.

<a name="jeremiah_13_11"></a>Jeremiah 13:11

For as the ['ēzôr](../../strongs/h/h232.md) [dāḇaq](../../strongs/h/h1692.md) to the [māṯnayim](../../strongs/h/h4975.md) of an ['iysh](../../strongs/h/h376.md), so have I caused to [dāḇaq](../../strongs/h/h1692.md) unto me the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) and the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); that they might be unto me for an ['am](../../strongs/h/h5971.md), and for a [shem](../../strongs/h/h8034.md), and for a [tehillah](../../strongs/h/h8416.md), and for a [tip̄'ārâ](../../strongs/h/h8597.md): but they would not [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_13_12"></a>Jeremiah 13:12

Therefore thou shalt ['āmar](../../strongs/h/h559.md) unto them this [dabar](../../strongs/h/h1697.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), Every [neḇel](../../strongs/h/h5035.md) shall be [mālā'](../../strongs/h/h4390.md) with [yayin](../../strongs/h/h3196.md): and they shall ['āmar](../../strongs/h/h559.md) unto thee, Do we not [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that every [neḇel](../../strongs/h/h5035.md) shall be [mālā'](../../strongs/h/h4390.md) with [yayin](../../strongs/h/h3196.md)?

<a name="jeremiah_13_13"></a>Jeremiah 13:13

Then shalt thou ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [mālā'](../../strongs/h/h4390.md) all the [yashab](../../strongs/h/h3427.md) of this ['erets](../../strongs/h/h776.md), even the [melek](../../strongs/h/h4428.md) that [yashab](../../strongs/h/h3427.md) upon [Dāviḏ](../../strongs/h/h1732.md) [kicce'](../../strongs/h/h3678.md), and the [kōhēn](../../strongs/h/h3548.md), and the [nāḇî'](../../strongs/h/h5030.md), and all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), with [šikārôn](../../strongs/h/h7943.md).

<a name="jeremiah_13_14"></a>Jeremiah 13:14

And I will [naphats](../../strongs/h/h5310.md) them ['iysh](../../strongs/h/h376.md) against ['ach](../../strongs/h/h251.md), even the ['ab](../../strongs/h/h1.md) and the [ben](../../strongs/h/h1121.md) [yaḥaḏ](../../strongs/h/h3162.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): I will not [ḥāmal](../../strongs/h/h2550.md), nor [ḥûs](../../strongs/h/h2347.md), nor have [racham](../../strongs/h/h7355.md), but [shachath](../../strongs/h/h7843.md) them.

<a name="jeremiah_13_15"></a>Jeremiah 13:15

[shama'](../../strongs/h/h8085.md) ye, and ['azan](../../strongs/h/h238.md); be not [gāḇah](../../strongs/h/h1361.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md).

<a name="jeremiah_13_16"></a>Jeremiah 13:16

[nathan](../../strongs/h/h5414.md) [kabowd](../../strongs/h/h3519.md) to [Yĕhovah](../../strongs/h/h3068.md) your ['Elohiym](../../strongs/h/h430.md), before he cause [ḥāšaḵ](../../strongs/h/h2821.md), and before your [regel](../../strongs/h/h7272.md) [nāḡap̄](../../strongs/h/h5062.md) upon the [nešep̄](../../strongs/h/h5399.md) [har](../../strongs/h/h2022.md), and, while ye [qāvâ](../../strongs/h/h6960.md) for ['owr](../../strongs/h/h216.md), he [śûm](../../strongs/h/h7760.md) it into the [ṣalmāveṯ](../../strongs/h/h6757.md), and [shiyth](../../strongs/h/h7896.md) [shiyth](../../strongs/h/h7896.md) it ['araphel](../../strongs/h/h6205.md).

<a name="jeremiah_13_17"></a>Jeremiah 13:17

But if ye will not [shama'](../../strongs/h/h8085.md) it, my [nephesh](../../strongs/h/h5315.md) shall [bāḵâ](../../strongs/h/h1058.md) in [mictar](../../strongs/h/h4565.md) [paniym](../../strongs/h/h6440.md) your [gēvâ](../../strongs/h/h1466.md); and mine ['ayin](../../strongs/h/h5869.md) shall [dāmaʿ](../../strongs/h/h1830.md) [dāmaʿ](../../strongs/h/h1830.md), and [yarad](../../strongs/h/h3381.md) with [dim'ah](../../strongs/h/h1832.md), because [Yĕhovah](../../strongs/h/h3068.md) [ʿēḏer](../../strongs/h/h5739.md) is [šāḇâ](../../strongs/h/h7617.md).

<a name="jeremiah_13_18"></a>Jeremiah 13:18

['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md) and to the [ḡᵊḇîrâ](../../strongs/h/h1377.md), [šāp̄ēl](../../strongs/h/h8213.md) yourselves, sit [yashab](../../strongs/h/h3427.md): for your [mar'āšôṯ](../../strongs/h/h4761.md) shall [yarad](../../strongs/h/h3381.md), even the [ʿăṭārâ](../../strongs/h/h5850.md) of your [tip̄'ārâ](../../strongs/h/h8597.md).

<a name="jeremiah_13_19"></a>Jeremiah 13:19

The [ʿîr](../../strongs/h/h5892.md) of the [neḡeḇ](../../strongs/h/h5045.md) shall be shut [cagar](../../strongs/h/h5462.md), and none shall [pāṯaḥ](../../strongs/h/h6605.md) them: [Yehuwdah](../../strongs/h/h3063.md) shall be [gālâ](../../strongs/h/h1540.md) all of it, it shall be [shalowm](../../strongs/h/h7965.md) [gālâ](../../strongs/h/h1540.md).

<a name="jeremiah_13_20"></a>Jeremiah 13:20

[nasa'](../../strongs/h/h5375.md) your ['ayin](../../strongs/h/h5869.md), and [ra'ah](../../strongs/h/h7200.md) them that [bow'](../../strongs/h/h935.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md): where is the [ʿēḏer](../../strongs/h/h5739.md) that was [nathan](../../strongs/h/h5414.md) thee, thy [tip̄'ārâ](../../strongs/h/h8597.md) [tso'n](../../strongs/h/h6629.md)?

<a name="jeremiah_13_21"></a>Jeremiah 13:21

What wilt thou ['āmar](../../strongs/h/h559.md) when he shall [paqad](../../strongs/h/h6485.md) thee? for thou hast [lamad](../../strongs/h/h3925.md) them to be ['allûp̄](../../strongs/h/h441.md), and as [ro'sh](../../strongs/h/h7218.md) over thee: shall not [chebel](../../strongs/h/h2256.md) ['āḥaz](../../strongs/h/h270.md) thee, as an ['ishshah](../../strongs/h/h802.md) in [yalad](../../strongs/h/h3205.md)?

<a name="jeremiah_13_22"></a>Jeremiah 13:22

And if thou ['āmar](../../strongs/h/h559.md) in thine [lebab](../../strongs/h/h3824.md), Wherefore [qārā'](../../strongs/h/h7122.md) these things upon me? For the [rōḇ](../../strongs/h/h7230.md) of thine ['avon](../../strongs/h/h5771.md) are thy [shuwl](../../strongs/h/h7757.md) [gālâ](../../strongs/h/h1540.md), and thy ['aqeb](../../strongs/h/h6119.md) made [ḥāmas](../../strongs/h/h2554.md).

<a name="jeremiah_13_23"></a>Jeremiah 13:23

Can the [Kûšî](../../strongs/h/h3569.md) [hāp̄aḵ](../../strongs/h/h2015.md) his ['owr](../../strongs/h/h5785.md), or the [nāmēr](../../strongs/h/h5246.md) his [ḥăḇarburôṯ](../../strongs/h/h2272.md)? then [yakol](../../strongs/h/h3201.md) ye also do [yatab](../../strongs/h/h3190.md), that are [limmûḏ](../../strongs/h/h3928.md) to do [ra'a'](../../strongs/h/h7489.md).

<a name="jeremiah_13_24"></a>Jeremiah 13:24

Therefore will I [puwts](../../strongs/h/h6327.md) them as the [qaš](../../strongs/h/h7179.md) that ['abar](../../strongs/h/h5674.md) by the [ruwach](../../strongs/h/h7307.md) of the [midbar](../../strongs/h/h4057.md).

<a name="jeremiah_13_25"></a>Jeremiah 13:25

This is thy [gôrāl](../../strongs/h/h1486.md), the [mānâ](../../strongs/h/h4490.md) of thy [maḏ](../../strongs/h/h4055.md) from me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); because thou hast [shakach](../../strongs/h/h7911.md) me, and [batach](../../strongs/h/h982.md) in [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_13_26"></a>Jeremiah 13:26

Therefore will I [ḥāśap̄](../../strongs/h/h2834.md) thy [shuwl](../../strongs/h/h7757.md) upon thy [paniym](../../strongs/h/h6440.md), that thy [qālôn](../../strongs/h/h7036.md) may [ra'ah](../../strongs/h/h7200.md).

<a name="jeremiah_13_27"></a>Jeremiah 13:27

I have [ra'ah](../../strongs/h/h7200.md) thine [ni'up̄îm](../../strongs/h/h5004.md), and thy [miṣhālâ](../../strongs/h/h4684.md), the [zimmâ](../../strongs/h/h2154.md) of thy [zᵊnûṯ](../../strongs/h/h2184.md), and thine [šiqqûṣ](../../strongs/h/h8251.md) on the [giḇʿâ](../../strongs/h/h1389.md) in the [sadeh](../../strongs/h/h7704.md). ['owy](../../strongs/h/h188.md) unto thee, [Yĕruwshalaim](../../strongs/h/h3389.md)! wilt thou not be made [ṭāhēr](../../strongs/h/h2891.md)? when shall it [ʿôḏ](../../strongs/h/h5750.md) ['aḥar](../../strongs/h/h310.md) be?

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 12](jeremiah_12.md) - [Jeremiah 14](jeremiah_14.md)