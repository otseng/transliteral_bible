# [Jeremiah 48](https://www.blueletterbible.org/kjv/jeremiah/48)

<a name="jeremiah_48_1"></a>Jeremiah 48:1

Against [Mô'āḇ](../../strongs/h/h4124.md) thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [hôy](../../strongs/h/h1945.md) unto [Nᵊḇô](../../strongs/h/h5015.md)! for it is [shadad](../../strongs/h/h7703.md): [Qiryāṯayim](../../strongs/h/h7156.md) is [yāḇēš](../../strongs/h/h3001.md) and [lāḵaḏ](../../strongs/h/h3920.md): [misgab](../../strongs/h/h4869.md) is [yāḇēš](../../strongs/h/h3001.md) and [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="jeremiah_48_2"></a>Jeremiah 48:2

There shall be no more [tehillah](../../strongs/h/h8416.md) of [Mô'āḇ](../../strongs/h/h4124.md): in [Hešbôn](../../strongs/h/h2809.md) they have [chashab](../../strongs/h/h2803.md) [ra'](../../strongs/h/h7451.md) against it; [yālaḵ](../../strongs/h/h3212.md), and let us [karath](../../strongs/h/h3772.md) it from being a [gowy](../../strongs/h/h1471.md). Also thou shalt be [damam](../../strongs/h/h1826.md), [Maḏmēn](../../strongs/h/h4086.md); the [chereb](../../strongs/h/h2719.md) shall [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md).

<a name="jeremiah_48_3"></a>Jeremiah 48:3

A [qowl](../../strongs/h/h6963.md) of [tsa'aqah](../../strongs/h/h6818.md) shall be from [ḥōrōnayim](../../strongs/h/h2773.md), [shod](../../strongs/h/h7701.md) and [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md).

<a name="jeremiah_48_4"></a>Jeremiah 48:4

[Mô'āḇ](../../strongs/h/h4124.md) is [shabar](../../strongs/h/h7665.md); her [ṣāʿîr](../../strongs/h/h6810.md) have caused a [zaʿaq](../../strongs/h/h2201.md) to be [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_48_5"></a>Jeremiah 48:5

For in the [maʿălê](../../strongs/h/h4608.md) of [lûḥîṯ](../../strongs/h/h3872.md) [bĕkiy](../../strongs/h/h1065.md) [bĕkiy](../../strongs/h/h1065.md) shall [ʿālâ](../../strongs/h/h5927.md); for in the [môrāḏ](../../strongs/h/h4174.md) of [ḥōrōnayim](../../strongs/h/h2773.md) the [tsar](../../strongs/h/h6862.md) have [shama'](../../strongs/h/h8085.md) a [tsa'aqah](../../strongs/h/h6818.md) of [šeḇar](../../strongs/h/h7667.md).

<a name="jeremiah_48_6"></a>Jeremiah 48:6

[nûs](../../strongs/h/h5127.md), [mālaṭ](../../strongs/h/h4422.md) your [nephesh](../../strongs/h/h5315.md), and [hayah](../../strongs/h/h1961.md) like the [ʿărôʿēr](../../strongs/h/h6176.md) in the [midbar](../../strongs/h/h4057.md).

<a name="jeremiah_48_7"></a>Jeremiah 48:7

For because thou hast [batach](../../strongs/h/h982.md) in thy [ma'aseh](../../strongs/h/h4639.md) and in thy ['ôṣār](../../strongs/h/h214.md), thou shalt also be [lāḵaḏ](../../strongs/h/h3920.md): and [kᵊmôš](../../strongs/h/h3645.md) shall [yāṣā'](../../strongs/h/h3318.md) into [gôlâ](../../strongs/h/h1473.md) with his [kōhēn](../../strongs/h/h3548.md) and his [śar](../../strongs/h/h8269.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="jeremiah_48_8"></a>Jeremiah 48:8

And the [shadad](../../strongs/h/h7703.md) shall [bow'](../../strongs/h/h935.md) upon every [ʿîr](../../strongs/h/h5892.md), and no [ʿîr](../../strongs/h/h5892.md) shall [mālaṭ](../../strongs/h/h4422.md): the [ʿēmeq](../../strongs/h/h6010.md) also shall ['abad](../../strongs/h/h6.md), and the [mîšôr](../../strongs/h/h4334.md) shall be [šāmaḏ](../../strongs/h/h8045.md), as [Yĕhovah](../../strongs/h/h3068.md) hath ['āmar](../../strongs/h/h559.md).

<a name="jeremiah_48_9"></a>Jeremiah 48:9

[nathan](../../strongs/h/h5414.md) [tsiyts](../../strongs/h/h6731.md) unto [Mô'āḇ](../../strongs/h/h4124.md), that it may [nāṣā'](../../strongs/h/h5323.md) and [yāṣā'](../../strongs/h/h3318.md): for the [ʿîr](../../strongs/h/h5892.md) thereof shall be [šammâ](../../strongs/h/h8047.md), without any to [yashab](../../strongs/h/h3427.md) [hēn](../../strongs/h/h2004.md).

<a name="jeremiah_48_10"></a>Jeremiah 48:10

['arar](../../strongs/h/h779.md) be he that ['asah](../../strongs/h/h6213.md) the [mĕla'kah](../../strongs/h/h4399.md) of [Yĕhovah](../../strongs/h/h3068.md) [rᵊmîyâ](../../strongs/h/h7423.md), and ['arar](../../strongs/h/h779.md) be he that [mānaʿ](../../strongs/h/h4513.md) his [chereb](../../strongs/h/h2719.md) from [dam](../../strongs/h/h1818.md).

<a name="jeremiah_48_11"></a>Jeremiah 48:11

[Mô'āḇ](../../strongs/h/h4124.md) hath been at [šā'an](../../strongs/h/h7599.md) from his [nāʿur](../../strongs/h/h5271.md), and he hath [šāqaṭ](../../strongs/h/h8252.md) on his [šemer](../../strongs/h/h8105.md), and hath not been [rîq](../../strongs/h/h7324.md) from [kĕliy](../../strongs/h/h3627.md) to [kĕliy](../../strongs/h/h3627.md), neither hath he [halak](../../strongs/h/h1980.md) into [gôlâ](../../strongs/h/h1473.md): therefore his [ṭaʿam](../../strongs/h/h2940.md) ['amad](../../strongs/h/h5975.md) in him, and his [rêaḥ](../../strongs/h/h7381.md) is not [mûr](../../strongs/h/h4171.md).

<a name="jeremiah_48_12"></a>Jeremiah 48:12

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [shalach](../../strongs/h/h7971.md) unto him [ṣāʿâ](../../strongs/h/h6808.md), that shall cause him to [ṣāʿâ](../../strongs/h/h6808.md), and shall [rîq](../../strongs/h/h7324.md) his [kĕliy](../../strongs/h/h3627.md), and [naphats](../../strongs/h/h5310.md) their [neḇel](../../strongs/h/h5035.md).

<a name="jeremiah_48_13"></a>Jeremiah 48:13

And [Mô'āḇ](../../strongs/h/h4124.md) shall be [buwsh](../../strongs/h/h954.md) of [kᵊmôš](../../strongs/h/h3645.md), as the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) was [buwsh](../../strongs/h/h954.md) of [Bêṯ-'ēl](../../strongs/h/h1008.md) their [miḇṭāḥ](../../strongs/h/h4009.md).

<a name="jeremiah_48_14"></a>Jeremiah 48:14

How ['āmar](../../strongs/h/h559.md) ye, We are [gibôr](../../strongs/h/h1368.md) and [ḥayil](../../strongs/h/h2428.md) ['enowsh](../../strongs/h/h582.md) for the [milḥāmâ](../../strongs/h/h4421.md)?

<a name="jeremiah_48_15"></a>Jeremiah 48:15

[Mô'āḇ](../../strongs/h/h4124.md) is [shadad](../../strongs/h/h7703.md), and [ʿālâ](../../strongs/h/h5927.md) out of her [ʿîr](../../strongs/h/h5892.md), and his [miḇḥār](../../strongs/h/h4005.md) young [bāḥûr](../../strongs/h/h970.md) are [yarad](../../strongs/h/h3381.md) to the [ṭeḇaḥ](../../strongs/h/h2874.md), [nᵊ'um](../../strongs/h/h5002.md) the [melek](../../strongs/h/h4428.md), whose [shem](../../strongs/h/h8034.md) is [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_48_16"></a>Jeremiah 48:16

The ['êḏ](../../strongs/h/h343.md) of [Mô'āḇ](../../strongs/h/h4124.md) is [qarowb](../../strongs/h/h7138.md) to [bow'](../../strongs/h/h935.md), and his [ra'](../../strongs/h/h7451.md) [māhar](../../strongs/h/h4116.md) [me'od](../../strongs/h/h3966.md).

<a name="jeremiah_48_17"></a>Jeremiah 48:17

All ye that are [cabiyb](../../strongs/h/h5439.md) him, [nuwd](../../strongs/h/h5110.md) him; and all ye that [yada'](../../strongs/h/h3045.md) his [shem](../../strongs/h/h8034.md), ['āmar](../../strongs/h/h559.md), How is the ['oz](../../strongs/h/h5797.md) [maṭṭê](../../strongs/h/h4294.md) [shabar](../../strongs/h/h7665.md), and the [tip̄'ārâ](../../strongs/h/h8597.md) [maqqēl](../../strongs/h/h4731.md)!

<a name="jeremiah_48_18"></a>Jeremiah 48:18

Thou [bath](../../strongs/h/h1323.md) that dost [yashab](../../strongs/h/h3427.md) [Dîḇōvn](../../strongs/h/h1769.md), [yarad](../../strongs/h/h3381.md) from thy [kabowd](../../strongs/h/h3519.md), and [yashab](../../strongs/h/h3427.md) in [ṣāmā'](../../strongs/h/h6772.md); for the [shadad](../../strongs/h/h7703.md) of [Mô'āḇ](../../strongs/h/h4124.md) shall [ʿālâ](../../strongs/h/h5927.md) upon thee, and he shall [shachath](../../strongs/h/h7843.md) thy [miḇṣār](../../strongs/h/h4013.md).

<a name="jeremiah_48_19"></a>Jeremiah 48:19

[yashab](../../strongs/h/h3427.md) of [ʿĂrôʿēr](../../strongs/h/h6177.md), ['amad](../../strongs/h/h5975.md) by the [derek](../../strongs/h/h1870.md), and [tsaphah](../../strongs/h/h6822.md); [sha'al](../../strongs/h/h7592.md) him that [nûs](../../strongs/h/h5127.md), and her that [mālaṭ](../../strongs/h/h4422.md), and ['āmar](../../strongs/h/h559.md), What is [hayah](../../strongs/h/h1961.md)?

<a name="jeremiah_48_20"></a>Jeremiah 48:20

[Mô'āḇ](../../strongs/h/h4124.md) is [yāḇēš](../../strongs/h/h3001.md); for it is [ḥāṯaṯ](../../strongs/h/h2865.md): [yālal](../../strongs/h/h3213.md) and [zāʿaq](../../strongs/h/h2199.md); [nāḡaḏ](../../strongs/h/h5046.md) ye it in ['arnôn](../../strongs/h/h769.md), that [Mô'āḇ](../../strongs/h/h4124.md) is [shadad](../../strongs/h/h7703.md),

<a name="jeremiah_48_21"></a>Jeremiah 48:21

And [mishpat](../../strongs/h/h4941.md) is [bow'](../../strongs/h/h935.md) upon the [mîšôr](../../strongs/h/h4334.md) ['erets](../../strongs/h/h776.md); upon [Ḥōlôn](../../strongs/h/h2473.md), and upon [Yahaṣ](../../strongs/h/h3096.md), and upon [Mēvp̄AʿAṯ](../../strongs/h/h4158.md),

<a name="jeremiah_48_22"></a>Jeremiah 48:22

And upon [Dîḇōvn](../../strongs/h/h1769.md), and upon [Nᵊḇô](../../strongs/h/h5015.md), and upon [Bêṯ Diḇlāṯayim](../../strongs/h/h1015.md),

<a name="jeremiah_48_23"></a>Jeremiah 48:23

And upon [Qiryāṯayim](../../strongs/h/h7156.md), and upon [Bêṯ Gāmûl](../../strongs/h/h1014.md), and upon [Bêṯ BaʿAl MᵊʿVn](../../strongs/h/h1010.md),

<a name="jeremiah_48_24"></a>Jeremiah 48:24

And upon [Qᵊrîyôṯ](../../strongs/h/h7152.md), and upon [Bāṣrâ](../../strongs/h/h1224.md), and upon all the [ʿîr](../../strongs/h/h5892.md) of the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md), [rachowq](../../strongs/h/h7350.md) or [qarowb](../../strongs/h/h7138.md).

<a name="jeremiah_48_25"></a>Jeremiah 48:25

The [qeren](../../strongs/h/h7161.md) of [Mô'āḇ](../../strongs/h/h4124.md) is cut [gāḏaʿ](../../strongs/h/h1438.md), and his [zerowa'](../../strongs/h/h2220.md) is [shabar](../../strongs/h/h7665.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_48_26"></a>Jeremiah 48:26

Make ye him [šāḵar](../../strongs/h/h7937.md): for he [gāḏal](../../strongs/h/h1431.md) himself against [Yĕhovah](../../strongs/h/h3068.md): [Mô'āḇ](../../strongs/h/h4124.md) also shall [sāp̄aq](../../strongs/h/h5606.md) in his [qē'](../../strongs/h/h6892.md), and he also shall be in [śᵊḥôq](../../strongs/h/h7814.md).

<a name="jeremiah_48_27"></a>Jeremiah 48:27

For was not [Yisra'el](../../strongs/h/h3478.md) a [śᵊḥôq](../../strongs/h/h7814.md) unto thee? was he [māṣā'](../../strongs/h/h4672.md) among [gannāḇ](../../strongs/h/h1590.md)? for [day](../../strongs/h/h1767.md) thou [dabar](../../strongs/h/h1697.md) of him, thou [nuwd](../../strongs/h/h5110.md) for joy.

<a name="jeremiah_48_28"></a>Jeremiah 48:28

O ye that [yashab](../../strongs/h/h3427.md) in [Mô'āḇ](../../strongs/h/h4124.md), ['azab](../../strongs/h/h5800.md) the [ʿîr](../../strongs/h/h5892.md), and [shakan](../../strongs/h/h7931.md) in the [cela'](../../strongs/h/h5553.md), and be like the [yônâ](../../strongs/h/h3123.md) that maketh her [qānan](../../strongs/h/h7077.md) in the [ʿēḇer](../../strongs/h/h5676.md) of the [paḥaṯ](../../strongs/h/h6354.md) [peh](../../strongs/h/h6310.md).

<a name="jeremiah_48_29"></a>Jeremiah 48:29

We have [shama'](../../strongs/h/h8085.md) the [gā'ôn](../../strongs/h/h1347.md) of [Mô'āḇ](../../strongs/h/h4124.md), (he is [me'od](../../strongs/h/h3966.md) [gē'ê](../../strongs/h/h1343.md) ) his [gobahh](../../strongs/h/h1363.md), and his [ga'avah](../../strongs/h/h1346.md), and his [gā'ôn](../../strongs/h/h1347.md), and the [rûm](../../strongs/h/h7312.md) of his [leb](../../strongs/h/h3820.md).

<a name="jeremiah_48_30"></a>Jeremiah 48:30

I [yada'](../../strongs/h/h3045.md) his ['ebrah](../../strongs/h/h5678.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); but it shall not be so; his [baḏ](../../strongs/h/h907.md) shall not so ['asah](../../strongs/h/h6213.md) it.

<a name="jeremiah_48_31"></a>Jeremiah 48:31

Therefore will I [yālal](../../strongs/h/h3213.md) for [Mô'āḇ](../../strongs/h/h4124.md), and I will cry [zāʿaq](../../strongs/h/h2199.md) for all [Mô'āḇ](../../strongs/h/h4124.md); mine heart shall [hagah](../../strongs/h/h1897.md) for the ['enowsh](../../strongs/h/h582.md) of [Qîr-ḥereś](../../strongs/h/h7025.md).

<a name="jeremiah_48_32"></a>Jeremiah 48:32

[gep̄en](../../strongs/h/h1612.md) of [Śᵊḇām](../../strongs/h/h7643.md), I will [bāḵâ](../../strongs/h/h1058.md) for thee with the [bĕkiy](../../strongs/h/h1065.md) of [Yaʿzêr](../../strongs/h/h3270.md): thy [nᵊṭîšâ](../../strongs/h/h5189.md) are ['abar](../../strongs/h/h5674.md) the [yam](../../strongs/h/h3220.md), they [naga'](../../strongs/h/h5060.md) even to the [yam](../../strongs/h/h3220.md) of [Yaʿzêr](../../strongs/h/h3270.md): the [shadad](../../strongs/h/h7703.md) is [naphal](../../strongs/h/h5307.md) upon thy [qayiṣ](../../strongs/h/h7019.md) and upon thy [bāṣîr](../../strongs/h/h1210.md).

<a name="jeremiah_48_33"></a>Jeremiah 48:33

And [simchah](../../strongs/h/h8057.md) and [gîl](../../strongs/h/h1524.md) is ['āsap̄](../../strongs/h/h622.md) from the [karmel](../../strongs/h/h3759.md), and from the ['erets](../../strongs/h/h776.md) of [Mô'āḇ](../../strongs/h/h4124.md); and I have caused [yayin](../../strongs/h/h3196.md) to [shabath](../../strongs/h/h7673.md) from the [yeqeḇ](../../strongs/h/h3342.md): none shall [dāraḵ](../../strongs/h/h1869.md) with [hêḏāḏ](../../strongs/h/h1959.md); their [hêḏāḏ](../../strongs/h/h1959.md) shall be no [hêḏāḏ](../../strongs/h/h1959.md).

<a name="jeremiah_48_34"></a>Jeremiah 48:34

From the [zaʿaq](../../strongs/h/h2201.md) of [Hešbôn](../../strongs/h/h2809.md) even unto ['Elʿālē'](../../strongs/h/h500.md), and even unto [Yahaṣ](../../strongs/h/h3096.md), have they [nathan](../../strongs/h/h5414.md) their [qowl](../../strongs/h/h6963.md), from [Ṣōʿar](../../strongs/h/h6820.md) even unto [ḥōrōnayim](../../strongs/h/h2773.md), as an [ʿeḡlâ](../../strongs/h/h5697.md) of [šᵊlîšî](../../strongs/h/h7992.md): for the [mayim](../../strongs/h/h4325.md) also of [nimrîm](../../strongs/h/h5249.md) shall be [mᵊšammâ](../../strongs/h/h4923.md).

<a name="jeremiah_48_35"></a>Jeremiah 48:35

Moreover I will cause to [shabath](../../strongs/h/h7673.md) in [Mô'āḇ](../../strongs/h/h4124.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), him that [ʿālâ](../../strongs/h/h5927.md) in the [bāmâ](../../strongs/h/h1116.md), and him that [qāṭar](../../strongs/h/h6999.md) to his ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_48_36"></a>Jeremiah 48:36

Therefore mine [leb](../../strongs/h/h3820.md) shall [hāmâ](../../strongs/h/h1993.md) for [Mô'āḇ](../../strongs/h/h4124.md) like [ḥālîl](../../strongs/h/h2485.md), and mine [leb](../../strongs/h/h3820.md) shall [hāmâ](../../strongs/h/h1993.md) like [ḥālîl](../../strongs/h/h2485.md) for the ['enowsh](../../strongs/h/h582.md) of [Qîr-ḥereś](../../strongs/h/h7025.md): because the [yiṯrâ](../../strongs/h/h3502.md) that he hath ['asah](../../strongs/h/h6213.md) are ['abad](../../strongs/h/h6.md).

<a name="jeremiah_48_37"></a>Jeremiah 48:37

For every [ro'sh](../../strongs/h/h7218.md) shall be [qrḥ](../../strongs/h/h7144.md), and every [zāqān](../../strongs/h/h2206.md) [gāraʿ](../../strongs/h/h1639.md): upon all the [yad](../../strongs/h/h3027.md) shall be [gᵊḏûḏ](../../strongs/h/h1417.md), and upon the [māṯnayim](../../strongs/h/h4975.md) [śaq](../../strongs/h/h8242.md).

<a name="jeremiah_48_38"></a>Jeremiah 48:38

There shall be [mispēḏ](../../strongs/h/h4553.md) generally upon all the [gāḡ](../../strongs/h/h1406.md) of [Mô'āḇ](../../strongs/h/h4124.md), and in the [rᵊḥōḇ](../../strongs/h/h7339.md) thereof: for I have [shabar](../../strongs/h/h7665.md) [Mô'āḇ](../../strongs/h/h4124.md) like a [kĕliy](../../strongs/h/h3627.md) wherein is no [chephets](../../strongs/h/h2656.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_48_39"></a>Jeremiah 48:39

They shall [yālal](../../strongs/h/h3213.md), saying, How is it [ḥāṯaṯ](../../strongs/h/h2865.md)! how hath [Mô'āḇ](../../strongs/h/h4124.md) [panah](../../strongs/h/h6437.md) the [ʿōrep̄](../../strongs/h/h6203.md) with [buwsh](../../strongs/h/h954.md)! so shall [Mô'āḇ](../../strongs/h/h4124.md) be a [śᵊḥôq](../../strongs/h/h7814.md) and a [mᵊḥitâ](../../strongs/h/h4288.md) to all them [cabiyb](../../strongs/h/h5439.md) him.

<a name="jeremiah_48_40"></a>Jeremiah 48:40

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, he shall [da'ah](../../strongs/h/h1675.md) as a [nesheׁr](../../strongs/h/h5404.md), and shall [pāraś](../../strongs/h/h6566.md) his [kanaph](../../strongs/h/h3671.md) over [Mô'āḇ](../../strongs/h/h4124.md).

<a name="jeremiah_48_41"></a>Jeremiah 48:41

[Qᵊrîyôṯ](../../strongs/h/h7152.md) is [lāḵaḏ](../../strongs/h/h3920.md), and the [mᵊṣāḏ](../../strongs/h/h4679.md) are [tāp̄aś](../../strongs/h/h8610.md), and the [gibôr](../../strongs/h/h1368.md) [leb](../../strongs/h/h3820.md) in [Mô'āḇ](../../strongs/h/h4124.md) at that [yowm](../../strongs/h/h3117.md) shall be as the [leb](../../strongs/h/h3820.md) of an ['ishshah](../../strongs/h/h802.md) in her [tsarar](../../strongs/h/h6887.md).

<a name="jeremiah_48_42"></a>Jeremiah 48:42

And [Mô'āḇ](../../strongs/h/h4124.md) shall be [šāmaḏ](../../strongs/h/h8045.md) from being an ['am](../../strongs/h/h5971.md), because he hath [gāḏal](../../strongs/h/h1431.md) himself against [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_48_43"></a>Jeremiah 48:43

[paḥaḏ](../../strongs/h/h6343.md), and the [paḥaṯ](../../strongs/h/h6354.md), and the [paḥ](../../strongs/h/h6341.md), shall be upon thee, [yashab](../../strongs/h/h3427.md) of [Mô'āḇ](../../strongs/h/h4124.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_48_44"></a>Jeremiah 48:44

He that [nûs](../../strongs/h/h5127.md) [nîs](../../strongs/h/h5211.md) [paniym](../../strongs/h/h6440.md) the [paḥaḏ](../../strongs/h/h6343.md) shall [naphal](../../strongs/h/h5307.md) into the [paḥaṯ](../../strongs/h/h6354.md); and he that [ʿālâ](../../strongs/h/h5927.md) out of the [paḥaṯ](../../strongs/h/h6354.md) shall be [lāḵaḏ](../../strongs/h/h3920.md) in the [paḥ](../../strongs/h/h6341.md): for I will [bow'](../../strongs/h/h935.md) upon it, even upon [Mô'āḇ](../../strongs/h/h4124.md), the [šānâ](../../strongs/h/h8141.md) of their [pᵊqudâ](../../strongs/h/h6486.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_48_45"></a>Jeremiah 48:45

They that [nûs](../../strongs/h/h5127.md) ['amad](../../strongs/h/h5975.md) under the [ṣēl](../../strongs/h/h6738.md) of [Hešbôn](../../strongs/h/h2809.md) because of the [koach](../../strongs/h/h3581.md): but an ['esh](../../strongs/h/h784.md) shall [yāṣā'](../../strongs/h/h3318.md) out of [Hešbôn](../../strongs/h/h2809.md), and a [lehāḇâ](../../strongs/h/h3852.md) from the [bayin](../../strongs/h/h996.md) of [Sîḥôn](../../strongs/h/h5511.md), and shall ['akal](../../strongs/h/h398.md) the [pē'â](../../strongs/h/h6285.md) of [Mô'āḇ](../../strongs/h/h4124.md), and the [qodqod](../../strongs/h/h6936.md) of the [shā'ôn](../../strongs/h/h7588.md) [ben](../../strongs/h/h1121.md).

<a name="jeremiah_48_46"></a>Jeremiah 48:46

['owy](../../strongs/h/h188.md) be unto thee, O [Mô'āḇ](../../strongs/h/h4124.md)! the ['am](../../strongs/h/h5971.md) of [kᵊmôš](../../strongs/h/h3645.md) ['abad](../../strongs/h/h6.md): for thy [ben](../../strongs/h/h1121.md) are [laqach](../../strongs/h/h3947.md) [šᵊḇî](../../strongs/h/h7628.md), and thy [bath](../../strongs/h/h1323.md) [šiḇyâ](../../strongs/h/h7633.md).

<a name="jeremiah_48_47"></a>Jeremiah 48:47

Yet will I [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of [Mô'āḇ](../../strongs/h/h4124.md) in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Thus [hēnnâ](../../strongs/h/h2008.md) is the [mishpat](../../strongs/h/h4941.md) of [Mô'āḇ](../../strongs/h/h4124.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 47](jeremiah_47.md) - [Jeremiah 49](jeremiah_49.md)