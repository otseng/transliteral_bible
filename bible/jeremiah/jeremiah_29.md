# [Jeremiah 29](https://www.blueletterbible.org/kjv/jeremiah/29)

<a name="jeremiah_29_1"></a>Jeremiah 29:1

Now these are the [dabar](../../strongs/h/h1697.md) of the [sēp̄er](../../strongs/h/h5612.md) that [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [shalach](../../strongs/h/h7971.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) unto the [yeṯer](../../strongs/h/h3499.md) of the [zāqēn](../../strongs/h/h2205.md) which were [gôlâ](../../strongs/h/h1473.md), and to the [kōhēn](../../strongs/h/h3548.md), and to the [nāḇî'](../../strongs/h/h5030.md), and to all the ['am](../../strongs/h/h5971.md) whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) had [gālâ](../../strongs/h/h1540.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) to [Bāḇel](../../strongs/h/h894.md);

<a name="jeremiah_29_2"></a>Jeremiah 29:2

(After ['aḥar](../../strongs/h/h310.md) that [Yᵊḵānyâ](../../strongs/h/h3204.md) the [melek](../../strongs/h/h4428.md), and the [ḡᵊḇîrâ](../../strongs/h/h1377.md), and the [sārîs](../../strongs/h/h5631.md), the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md), and the [ḥārāš](../../strongs/h/h2796.md), and the [masgēr](../../strongs/h/h4525.md), were [yāṣā'](../../strongs/h/h3318.md) from [Yĕruwshalaim](../../strongs/h/h3389.md);)

<a name="jeremiah_29_3"></a>Jeremiah 29:3

By the [yad](../../strongs/h/h3027.md) of ['ElʿĀśâ](../../strongs/h/h501.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), and [Gᵊmaryâ](../../strongs/h/h1587.md) the [ben](../../strongs/h/h1121.md) of [Ḥilqîyâ](../../strongs/h/h2518.md), (whom [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [shalach](../../strongs/h/h7971.md) unto [Bāḇel](../../strongs/h/h894.md) to [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) ) ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_29_4"></a>Jeremiah 29:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), unto all that are [gôlâ](../../strongs/h/h1473.md), whom I have caused to be [gālâ](../../strongs/h/h1540.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) unto [Bāḇel](../../strongs/h/h894.md);

<a name="jeremiah_29_5"></a>Jeremiah 29:5

[bānâ](../../strongs/h/h1129.md) ye [bayith](../../strongs/h/h1004.md), and [yashab](../../strongs/h/h3427.md) in them; and [nāṭaʿ](../../strongs/h/h5193.md) [gannâ](../../strongs/h/h1593.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of them;

<a name="jeremiah_29_6"></a>Jeremiah 29:6

[laqach](../../strongs/h/h3947.md) ye ['ishshah](../../strongs/h/h802.md), and [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [bath](../../strongs/h/h1323.md); and [laqach](../../strongs/h/h3947.md) ['ishshah](../../strongs/h/h802.md) for your [ben](../../strongs/h/h1121.md), and [nathan](../../strongs/h/h5414.md) your [bath](../../strongs/h/h1323.md) to ['enowsh](../../strongs/h/h582.md), that they may [yalad](../../strongs/h/h3205.md) [ben](../../strongs/h/h1121.md) and [ben](../../strongs/h/h1121.md); that ye may be [rabah](../../strongs/h/h7235.md) there, and not [māʿaṭ](../../strongs/h/h4591.md).

<a name="jeremiah_29_7"></a>Jeremiah 29:7

And [darash](../../strongs/h/h1875.md) the [shalowm](../../strongs/h/h7965.md) of the [ʿîr](../../strongs/h/h5892.md) whither I have caused you to be [gālâ](../../strongs/h/h1540.md), and [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md) for it: for in the [shalowm](../../strongs/h/h7965.md) thereof shall ye have [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_29_8"></a>Jeremiah 29:8

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Let not your [nāḇî'](../../strongs/h/h5030.md) and your [qāsam](../../strongs/h/h7080.md), that be in the [qereḇ](../../strongs/h/h7130.md) of you, [nasha'](../../strongs/h/h5377.md) you, neither [shama'](../../strongs/h/h8085.md) to your [ḥălôm](../../strongs/h/h2472.md) which ye cause to be [ḥālam](../../strongs/h/h2492.md).

<a name="jeremiah_29_9"></a>Jeremiah 29:9

For they [nāḇā'](../../strongs/h/h5012.md) [sheqer](../../strongs/h/h8267.md) unto you in my [shem](../../strongs/h/h8034.md): I have not [shalach](../../strongs/h/h7971.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_29_10"></a>Jeremiah 29:10

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), That [peh](../../strongs/h/h6310.md) [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md) be [mālā'](../../strongs/h/h4390.md) at [Bāḇel](../../strongs/h/h894.md) I will [paqad](../../strongs/h/h6485.md) you, and [quwm](../../strongs/h/h6965.md) my [towb](../../strongs/h/h2896.md) [dabar](../../strongs/h/h1697.md) toward you, in causing you to [shuwb](../../strongs/h/h7725.md) to this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_29_11"></a>Jeremiah 29:11

For I [yada'](../../strongs/h/h3045.md) the [maḥăšāḇâ](../../strongs/h/h4284.md) that I [chashab](../../strongs/h/h2803.md) toward you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), [maḥăšāḇâ](../../strongs/h/h4284.md) of [shalowm](../../strongs/h/h7965.md), and not of [ra'](../../strongs/h/h7451.md), to [nathan](../../strongs/h/h5414.md) you a [tiqvâ](../../strongs/h/h8615.md) ['aḥărîṯ](../../strongs/h/h319.md).

<a name="jeremiah_29_12"></a>Jeremiah 29:12

Then shall ye [qara'](../../strongs/h/h7121.md) upon me, and ye shall [halak](../../strongs/h/h1980.md) and [palal](../../strongs/h/h6419.md) unto me, and I will [shama'](../../strongs/h/h8085.md) unto you.

<a name="jeremiah_29_13"></a>Jeremiah 29:13

And ye shall [bāqaš](../../strongs/h/h1245.md) me, and [māṣā'](../../strongs/h/h4672.md) me, when ye shall [darash](../../strongs/h/h1875.md) for me with all your [lebab](../../strongs/h/h3824.md).

<a name="jeremiah_29_14"></a>Jeremiah 29:14

And I will be [māṣā'](../../strongs/h/h4672.md) of you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): and I will [shuwb](../../strongs/h/h7725.md) your [shebuwth](../../strongs/h/h7622.md) [shebuwth](../../strongs/h/h7622.md), and I will [qāḇaṣ](../../strongs/h/h6908.md) you from all the [gowy](../../strongs/h/h1471.md), and from all the [maqowm](../../strongs/h/h4725.md) whither I have [nāḏaḥ](../../strongs/h/h5080.md) you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and I will bring you [shuwb](../../strongs/h/h7725.md) into the [maqowm](../../strongs/h/h4725.md) whence I caused you to be [gālâ](../../strongs/h/h1540.md).

<a name="jeremiah_29_15"></a>Jeremiah 29:15

Because ye have ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) hath [quwm](../../strongs/h/h6965.md) us [nāḇî'](../../strongs/h/h5030.md) in [Bāḇel](../../strongs/h/h894.md);

<a name="jeremiah_29_16"></a>Jeremiah 29:16

Know that thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of the [melek](../../strongs/h/h4428.md) that [yashab](../../strongs/h/h3427.md) upon the [kicce'](../../strongs/h/h3678.md) of [Dāviḏ](../../strongs/h/h1732.md), and of all the ['am](../../strongs/h/h5971.md) that [yashab](../../strongs/h/h3427.md) in this [ʿîr](../../strongs/h/h5892.md), and of your ['ach](../../strongs/h/h251.md) that are not [yāṣā'](../../strongs/h/h3318.md) with you into [gôlâ](../../strongs/h/h1473.md);

<a name="jeremiah_29_17"></a>Jeremiah 29:17

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Behold, I will [shalach](../../strongs/h/h7971.md) upon them the [chereb](../../strongs/h/h2719.md), the [rāʿāḇ](../../strongs/h/h7458.md), and the [deḇer](../../strongs/h/h1698.md), and will [nathan](../../strongs/h/h5414.md) them like [šōʿār](../../strongs/h/h8182.md) [tĕ'en](../../strongs/h/h8384.md), that cannot be ['akal](../../strongs/h/h398.md), they are so [rōaʿ](../../strongs/h/h7455.md).

<a name="jeremiah_29_18"></a>Jeremiah 29:18

And I will [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) with the [chereb](../../strongs/h/h2719.md), with the [rāʿāḇ](../../strongs/h/h7458.md), and with the [deḇer](../../strongs/h/h1698.md), and will [nathan](../../strongs/h/h5414.md) them to be [zaʿăvâ](../../strongs/h/h2189.md) [zᵊvāʿâ](../../strongs/h/h2113.md) to all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md), to be an ['alah](../../strongs/h/h423.md), and a [šammâ](../../strongs/h/h8047.md), and a [šᵊrēqâ](../../strongs/h/h8322.md), and a [cherpah](../../strongs/h/h2781.md), among all the [gowy](../../strongs/h/h1471.md) whither I have [nāḏaḥ](../../strongs/h/h5080.md) them:

<a name="jeremiah_29_19"></a>Jeremiah 29:19

Because they have not [shama'](../../strongs/h/h8085.md) to my [dabar](../../strongs/h/h1697.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), which I [shalach](../../strongs/h/h7971.md) unto them by my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), rising up [šāḵam](../../strongs/h/h7925.md) and [shalach](../../strongs/h/h7971.md) them; but ye would not [shama'](../../strongs/h/h8085.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_29_20"></a>Jeremiah 29:20

[shama'](../../strongs/h/h8085.md) ye therefore the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), all ye of the [gôlâ](../../strongs/h/h1473.md), whom I have [shalach](../../strongs/h/h7971.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) to [Bāḇel](../../strongs/h/h894.md):

<a name="jeremiah_29_21"></a>Jeremiah 29:21

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), of ['Aḥ'Āḇ](../../strongs/h/h256.md) the [ben](../../strongs/h/h1121.md) of [Qôlāyâ](../../strongs/h/h6964.md), and of [Ṣḏqyh](../../strongs/h/h6667.md) the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md), which [nāḇā'](../../strongs/h/h5012.md) a [sheqer](../../strongs/h/h8267.md) unto you in my [shem](../../strongs/h/h8034.md); Behold, I will [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md); and he shall [nakah](../../strongs/h/h5221.md) them before your ['ayin](../../strongs/h/h5869.md);

<a name="jeremiah_29_22"></a>Jeremiah 29:22

And of them shall be taken [laqach](../../strongs/h/h3947.md) a [qᵊlālâ](../../strongs/h/h7045.md) by all the [gālûṯ](../../strongs/h/h1546.md) of [Yehuwdah](../../strongs/h/h3063.md) which are in [Bāḇel](../../strongs/h/h894.md), ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [śûm](../../strongs/h/h7760.md) thee like [Ṣḏqyh](../../strongs/h/h6667.md) and like ['Aḥ'Āḇ](../../strongs/h/h256.md), whom the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [qālâ](../../strongs/h/h7033.md) in the ['esh](../../strongs/h/h784.md);

<a name="jeremiah_29_23"></a>Jeremiah 29:23

Because they have ['asah](../../strongs/h/h6213.md) [nᵊḇālâ](../../strongs/h/h5039.md) in [Yisra'el](../../strongs/h/h3478.md), and have ['asah](../../strongs/h/h6213.md) [na'aph](../../strongs/h/h5003.md) with their [rea'](../../strongs/h/h7453.md) ['ishshah](../../strongs/h/h802.md), and have [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md) [dabar](../../strongs/h/h1697.md) in my [shem](../../strongs/h/h8034.md), which I have not [tsavah](../../strongs/h/h6680.md) them; even I [yada'](../../strongs/h/h3045.md), and am an ['ed](../../strongs/h/h5707.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_29_24"></a>Jeremiah 29:24

Thus shalt thou also ['āmar](../../strongs/h/h559.md) to [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [neḥĕlāmî](../../strongs/h/h5161.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_29_25"></a>Jeremiah 29:25

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), Because thou hast [shalach](../../strongs/h/h7971.md) [sēp̄er](../../strongs/h/h5612.md) in thy [shem](../../strongs/h/h8034.md) unto all the ['am](../../strongs/h/h5971.md) that are at [Yĕruwshalaim](../../strongs/h/h3389.md), and to [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md) the [kōhēn](../../strongs/h/h3548.md), and to all the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_29_26"></a>Jeremiah 29:26

[Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) thee [kōhēn](../../strongs/h/h3548.md) in the stead of [Yᵊhôyāḏāʿ](../../strongs/h/h3077.md) the [kōhēn](../../strongs/h/h3548.md), that ye should be [pāqîḏ](../../strongs/h/h6496.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), for every ['iysh](../../strongs/h/h376.md) that is [šāḡaʿ](../../strongs/h/h7696.md), and maketh himself a [nāḇā'](../../strongs/h/h5012.md), that thou shouldest [nathan](../../strongs/h/h5414.md) him in [mahpeḵeṯ](../../strongs/h/h4115.md), and in the [ṣînōq](../../strongs/h/h6729.md).

<a name="jeremiah_29_27"></a>Jeremiah 29:27

Now therefore why hast thou not [gāʿar](../../strongs/h/h1605.md) [Yirmᵊyâ](../../strongs/h/h3414.md) of [ʿAnnᵊṯōṯî](../../strongs/h/h6069.md), which maketh himself a [nāḇā'](../../strongs/h/h5012.md) to you?

<a name="jeremiah_29_28"></a>Jeremiah 29:28

For therefore he [shalach](../../strongs/h/h7971.md) unto us in [Bāḇel](../../strongs/h/h894.md), ['āmar](../../strongs/h/h559.md), [hû'](../../strongs/h/h1931.md) captivity is ['ārēḵ](../../strongs/h/h752.md): [bānâ](../../strongs/h/h1129.md) ye [bayith](../../strongs/h/h1004.md), and [yashab](../../strongs/h/h3427.md) in them; and [nāṭaʿ](../../strongs/h/h5193.md) [gannâ](../../strongs/h/h1593.md), and ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) of them.

<a name="jeremiah_29_29"></a>Jeremiah 29:29

And [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [kōhēn](../../strongs/h/h3548.md) [qara'](../../strongs/h/h7121.md) this [sēp̄er](../../strongs/h/h5612.md) in the ['ozen](../../strongs/h/h241.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="jeremiah_29_30"></a>Jeremiah 29:30

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_29_31"></a>Jeremiah 29:31

[shalach](../../strongs/h/h7971.md) to all them of the [gôlâ](../../strongs/h/h1473.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [neḥĕlāmî](../../strongs/h/h5161.md); Because that [ŠᵊmaʿYâ](../../strongs/h/h8098.md) hath [nāḇā'](../../strongs/h/h5012.md) unto you, and I [shalach](../../strongs/h/h7971.md) him not, and he caused you to [batach](../../strongs/h/h982.md) in a [sheqer](../../strongs/h/h8267.md):

<a name="jeremiah_29_32"></a>Jeremiah 29:32

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [paqad](../../strongs/h/h6485.md) [ŠᵊmaʿYâ](../../strongs/h/h8098.md) the [neḥĕlāmî](../../strongs/h/h5161.md), and his [zera'](../../strongs/h/h2233.md): he shall not have an ['iysh](../../strongs/h/h376.md) to [yashab](../../strongs/h/h3427.md) [tavek](../../strongs/h/h8432.md) this ['am](../../strongs/h/h5971.md); neither shall he [ra'ah](../../strongs/h/h7200.md) the [towb](../../strongs/h/h2896.md) that I will ['asah](../../strongs/h/h6213.md) for my ['am](../../strongs/h/h5971.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); because he hath [dabar](../../strongs/h/h1696.md) [sārâ](../../strongs/h/h5627.md) against [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 28](jeremiah_28.md) - [Jeremiah 30](jeremiah_30.md)