# [Jeremiah 40](https://www.blueletterbible.org/kjv/jeremiah/40)

<a name="jeremiah_40_1"></a>Jeremiah 40:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['aḥar](../../strongs/h/h310.md) that [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) had let him [shalach](../../strongs/h/h7971.md) from [rāmâ](../../strongs/h/h7414.md), when he had [laqach](../../strongs/h/h3947.md) him being ['āsar](../../strongs/h/h631.md) in ['ăziqqîm](../../strongs/h/h246.md) [tavek](../../strongs/h/h8432.md) all that were [gālûṯ](../../strongs/h/h1546.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) and [Yehuwdah](../../strongs/h/h3063.md), which were [gālâ](../../strongs/h/h1540.md) unto [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_40_2"></a>Jeremiah 40:2

And the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md) [Yirmᵊyâ](../../strongs/h/h3414.md), and ['āmar](../../strongs/h/h559.md) unto him, [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md) hath [dabar](../../strongs/h/h1696.md) this [ra'](../../strongs/h/h7451.md) upon this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_40_3"></a>Jeremiah 40:3

Now [Yĕhovah](../../strongs/h/h3068.md) hath [bow'](../../strongs/h/h935.md) it, and ['asah](../../strongs/h/h6213.md) according as he hath [dabar](../../strongs/h/h1696.md): because ye have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), and have not [shama'](../../strongs/h/h8085.md) his [qowl](../../strongs/h/h6963.md), therefore this [dabar](../../strongs/h/h1697.md) is come upon you.

<a name="jeremiah_40_4"></a>Jeremiah 40:4

And now, behold, I [pāṯaḥ](../../strongs/h/h6605.md) thee this [yowm](../../strongs/h/h3117.md) from the ['ăziqqîm](../../strongs/h/h246.md) which were upon thine [yad](../../strongs/h/h3027.md). If it seem [towb](../../strongs/h/h2896.md) ['ayin](../../strongs/h/h5869.md) unto thee to [bow'](../../strongs/h/h935.md) with me into [Bāḇel](../../strongs/h/h894.md), [bow'](../../strongs/h/h935.md); and I will [śûm](../../strongs/h/h7760.md) ['ayin](../../strongs/h/h5869.md) unto thee: but if it seem [ra'a'](../../strongs/h/h7489.md) ['ayin](../../strongs/h/h5869.md) unto thee to [bow'](../../strongs/h/h935.md) with me into [Bāḇel](../../strongs/h/h894.md), [ḥāḏal](../../strongs/h/h2308.md): [ra'ah](../../strongs/h/h7200.md), all the ['erets](../../strongs/h/h776.md) is [paniym](../../strongs/h/h6440.md) thee: whither it ['ēl](../../strongs/h/h413.md) [towb](../../strongs/h/h2896.md) and [yashar](../../strongs/h/h3477.md) ['ayin](../../strongs/h/h5869.md) for thee to [yālaḵ](../../strongs/h/h3212.md), thither [yālaḵ](../../strongs/h/h3212.md).

<a name="jeremiah_40_5"></a>Jeremiah 40:5

Now while he was not yet [shuwb](../../strongs/h/h7725.md), he said, [shuwb](../../strongs/h/h7725.md) also to [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md), whom the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) hath made [paqad](../../strongs/h/h6485.md) over the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and [yashab](../../strongs/h/h3427.md) with him [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md): or [yālaḵ](../../strongs/h/h3212.md) wheresoever it seemeth [yashar](../../strongs/h/h3477.md) ['ayin](../../strongs/h/h5869.md) unto thee to [yālaḵ](../../strongs/h/h3212.md). So the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [nathan](../../strongs/h/h5414.md) him ['ăruḥâ](../../strongs/h/h737.md) and a [maśśᵊ'ēṯ](../../strongs/h/h4864.md), and let him [shalach](../../strongs/h/h7971.md).

<a name="jeremiah_40_6"></a>Jeremiah 40:6

Then [bow'](../../strongs/h/h935.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) to [Miṣpê](../../strongs/h/h4708.md); and [yashab](../../strongs/h/h3427.md) with him [tavek](../../strongs/h/h8432.md) the ['am](../../strongs/h/h5971.md) that were [šā'ar](../../strongs/h/h7604.md) in the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_40_7"></a>Jeremiah 40:7

Now when all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) which were in the [sadeh](../../strongs/h/h7704.md), even they and their ['enowsh](../../strongs/h/h582.md), [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had made [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) [paqad](../../strongs/h/h6485.md) in the ['erets](../../strongs/h/h776.md), and had [paqad](../../strongs/h/h6485.md) unto him ['enowsh](../../strongs/h/h582.md), and ['ishshah](../../strongs/h/h802.md), and [ṭap̄](../../strongs/h/h2945.md), and of the [dallâ](../../strongs/h/h1803.md) of the ['erets](../../strongs/h/h776.md), of them that were not [gālâ](../../strongs/h/h1540.md) to [Bāḇel](../../strongs/h/h894.md);

<a name="jeremiah_40_8"></a>Jeremiah 40:8

Then they [bow'](../../strongs/h/h935.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) to [Miṣpê](../../strongs/h/h4708.md), even [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), and [Yôḥānān](../../strongs/h/h3110.md) and [Yônāṯān](../../strongs/h/h3129.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and [Śᵊrāyâ](../../strongs/h/h8304.md) the [ben](../../strongs/h/h1121.md) of [Tanḥumeṯ](../../strongs/h/h8576.md), and the [ben](../../strongs/h/h1121.md) of [ʿÔp̄Ay](../../strongs/h/h5778.md) the [Nᵊṭōp̄āṯî](../../strongs/h/h5200.md), and [Yᵊzanyâ](../../strongs/h/h3153.md) the [ben](../../strongs/h/h1121.md) of a [Maʿăḵāṯî](../../strongs/h/h4602.md), they and their ['enowsh](../../strongs/h/h582.md).

<a name="jeremiah_40_9"></a>Jeremiah 40:9

And [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md) [shaba'](../../strongs/h/h7650.md) unto them and to their ['enowsh](../../strongs/h/h582.md), ['āmar](../../strongs/h/h559.md), [yare'](../../strongs/h/h3372.md) not to ['abad](../../strongs/h/h5647.md) the [Kaśdîmâ](../../strongs/h/h3778.md): [yashab](../../strongs/h/h3427.md) the ['erets](../../strongs/h/h776.md), and ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and it shall be [yatab](../../strongs/h/h3190.md) with you.

<a name="jeremiah_40_10"></a>Jeremiah 40:10

As for me, behold, I will [yashab](../../strongs/h/h3427.md) at [Miṣpâ](../../strongs/h/h4709.md) to ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [Kaśdîmâ](../../strongs/h/h3778.md), which will [bow'](../../strongs/h/h935.md) unto us: but ye, ['āsap̄](../../strongs/h/h622.md) ye [yayin](../../strongs/h/h3196.md), and [qayiṣ](../../strongs/h/h7019.md), and [šemen](../../strongs/h/h8081.md), and [śûm](../../strongs/h/h7760.md) them in your [kĕliy](../../strongs/h/h3627.md), and [yashab](../../strongs/h/h3427.md) in your [ʿîr](../../strongs/h/h5892.md) that ye have [tāp̄aś](../../strongs/h/h8610.md).

<a name="jeremiah_40_11"></a>Jeremiah 40:11

Likewise when all the [Yᵊhûḏî](../../strongs/h/h3064.md) that were in [Mô'āḇ](../../strongs/h/h4124.md), and among the [ʿAmmôn](../../strongs/h/h5983.md), and in ['Ĕḏōm](../../strongs/h/h123.md), and that were in all the ['erets](../../strongs/h/h776.md), [shama'](../../strongs/h/h8085.md) that the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had [nathan](../../strongs/h/h5414.md) a [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yehuwdah](../../strongs/h/h3063.md), and that he had [paqad](../../strongs/h/h6485.md) over them [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) the [ben](../../strongs/h/h1121.md) of [Šāp̄ān](../../strongs/h/h8227.md);

<a name="jeremiah_40_12"></a>Jeremiah 40:12

Even all the [Yᵊhûḏî](../../strongs/h/h3064.md) re[shuwb](../../strongs/h/h7725.md) of all [maqowm](../../strongs/h/h4725.md) whither they were [nāḏaḥ](../../strongs/h/h5080.md), and [bow'](../../strongs/h/h935.md) to the ['erets](../../strongs/h/h776.md) of [Yehuwdah](../../strongs/h/h3063.md), to [Gᵊḏalyâ](../../strongs/h/h1436.md), unto [Miṣpê](../../strongs/h/h4708.md), and ['āsap̄](../../strongs/h/h622.md) [yayin](../../strongs/h/h3196.md) and [qayiṣ](../../strongs/h/h7019.md) [me'od](../../strongs/h/h3966.md) [rabah](../../strongs/h/h7235.md).

<a name="jeremiah_40_13"></a>Jeremiah 40:13

Moreover [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), and all the [śar](../../strongs/h/h8269.md) of the [ḥayil](../../strongs/h/h2428.md) that were in the [sadeh](../../strongs/h/h7704.md), [bow'](../../strongs/h/h935.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) to [Miṣpê](../../strongs/h/h4708.md),

<a name="jeremiah_40_14"></a>Jeremiah 40:14

And ['āmar](../../strongs/h/h559.md) unto him, Dost thou [yada'](../../strongs/h/h3045.md) [yada'](../../strongs/h/h3045.md) that [BaʿĂlîs](../../strongs/h/h1185.md) the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md) hath [shalach](../../strongs/h/h7971.md) [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md) to [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md) thee? But [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) ['aman](../../strongs/h/h539.md) them not.

<a name="jeremiah_40_15"></a>Jeremiah 40:15

Then [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md) ['āmar](../../strongs/h/h559.md) to [Gᵊḏalyâ](../../strongs/h/h1436.md) in [Miṣpâ](../../strongs/h/h4709.md) [cether](../../strongs/h/h5643.md), ['āmar](../../strongs/h/h559.md), Let me [yālaḵ](../../strongs/h/h3212.md), I pray thee, and I will [nakah](../../strongs/h/h5221.md) [Yišmāʿē'l](../../strongs/h/h3458.md) the [ben](../../strongs/h/h1121.md) of [Nᵊṯanyâ](../../strongs/h/h5418.md), and no ['iysh](../../strongs/h/h376.md) shall [yada'](../../strongs/h/h3045.md) it: wherefore should he [nakah](../../strongs/h/h5221.md) [nephesh](../../strongs/h/h5315.md) thee, that all the [Yᵊhûḏî](../../strongs/h/h3064.md) which are [qāḇaṣ](../../strongs/h/h6908.md) unto thee should be [puwts](../../strongs/h/h6327.md), and the [šᵊ'ērîṯ](../../strongs/h/h7611.md) in [Yehuwdah](../../strongs/h/h3063.md) ['abad](../../strongs/h/h6.md)?

<a name="jeremiah_40_16"></a>Jeremiah 40:16

But [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of ['Ăḥîqām](../../strongs/h/h296.md) ['āmar](../../strongs/h/h559.md) unto [Yôḥānān](../../strongs/h/h3110.md) the [ben](../../strongs/h/h1121.md) of [Qārēaḥ](../../strongs/h/h7143.md), Thou shalt not ['asah](../../strongs/h/h6213.md) this [dabar](../../strongs/h/h1697.md): for thou [dabar](../../strongs/h/h1696.md) [sheqer](../../strongs/h/h8267.md) of [Yišmāʿē'l](../../strongs/h/h3458.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 39](jeremiah_39.md) - [Jeremiah 41](jeremiah_41.md)