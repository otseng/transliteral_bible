# [Jeremiah 46](https://www.blueletterbible.org/kjv/jeremiah/46)

<a name="jeremiah_46_1"></a>Jeremiah 46:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) which came to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) against the [gowy](../../strongs/h/h1471.md);

<a name="jeremiah_46_2"></a>Jeremiah 46:2

Against [Mitsrayim](../../strongs/h/h4714.md), against the [ḥayil](../../strongs/h/h2428.md) of [ParʿÔ Nᵊḵô](../../strongs/h/h6549.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), which was by the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md) in [karkᵊmîš](../../strongs/h/h3751.md), which [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [nakah](../../strongs/h/h5221.md) in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_46_3"></a>Jeremiah 46:3

['arak](../../strongs/h/h6186.md) ye the [magen](../../strongs/h/h4043.md) and [tsinnah](../../strongs/h/h6793.md), and [nāḡaš](../../strongs/h/h5066.md) to [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_46_4"></a>Jeremiah 46:4

['āsar](../../strongs/h/h631.md) the [sûs](../../strongs/h/h5483.md); and [ʿālâ](../../strongs/h/h5927.md), ye [pārāš](../../strongs/h/h6571.md), and [yatsab](../../strongs/h/h3320.md) with your [kôḇaʿ](../../strongs/h/h3553.md); [māraq](../../strongs/h/h4838.md) the [rōmaḥ](../../strongs/h/h7420.md), and put [labash](../../strongs/h/h3847.md) the [siryōn](../../strongs/h/h5630.md).

<a name="jeremiah_46_5"></a>Jeremiah 46:5

Wherefore have I [ra'ah](../../strongs/h/h7200.md) them [ḥaṯ](../../strongs/h/h2844.md) and [sûḡ](../../strongs/h/h5472.md) away ['āḥôr](../../strongs/h/h268.md)? and their [gibôr](../../strongs/h/h1368.md) are [kāṯaṯ](../../strongs/h/h3807.md), and are [nûs](../../strongs/h/h5127.md) [mānôs](../../strongs/h/h4498.md), and look not [panah](../../strongs/h/h6437.md): for [māḡôr](../../strongs/h/h4032.md) was [cabiyb](../../strongs/h/h5439.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_46_6"></a>Jeremiah 46:6

Let not the [qal](../../strongs/h/h7031.md) flee [nûs](../../strongs/h/h5127.md), nor the [gibôr](../../strongs/h/h1368.md) [mālaṭ](../../strongs/h/h4422.md); they shall [kashal](../../strongs/h/h3782.md), and [naphal](../../strongs/h/h5307.md) toward the [ṣāp̄ôn](../../strongs/h/h6828.md) [yad](../../strongs/h/h3027.md) the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="jeremiah_46_7"></a>Jeremiah 46:7

Who is this that [ʿālâ](../../strongs/h/h5927.md) as a [yᵊ'ōr](../../strongs/h/h2975.md), whose [mayim](../../strongs/h/h4325.md) are [gāʿaš](../../strongs/h/h1607.md) as the [nāhār](../../strongs/h/h5104.md)?

<a name="jeremiah_46_8"></a>Jeremiah 46:8

[Mitsrayim](../../strongs/h/h4714.md) [ʿālâ](../../strongs/h/h5927.md) like a [yᵊ'ōr](../../strongs/h/h2975.md), and his [mayim](../../strongs/h/h4325.md) are [gāʿaš](../../strongs/h/h1607.md) like the [nāhār](../../strongs/h/h5104.md); and he ['āmar](../../strongs/h/h559.md), I will [ʿālâ](../../strongs/h/h5927.md), and will [kāsâ](../../strongs/h/h3680.md) the ['erets](../../strongs/h/h776.md); I will ['abad](../../strongs/h/h6.md) the [ʿîr](../../strongs/h/h5892.md) and the [yashab](../../strongs/h/h3427.md) thereof.

<a name="jeremiah_46_9"></a>Jeremiah 46:9

[ʿālâ](../../strongs/h/h5927.md), ye [sûs](../../strongs/h/h5483.md); and [halal](../../strongs/h/h1984.md), ye [reḵeḇ](../../strongs/h/h7393.md); and let the [gibôr](../../strongs/h/h1368.md) [yāṣā'](../../strongs/h/h3318.md); the [Kûš](../../strongs/h/h3568.md) and the [Pûṭ](../../strongs/h/h6316.md), that [tāp̄aś](../../strongs/h/h8610.md) the [magen](../../strongs/h/h4043.md); and the [Lûḏîy](../../strongs/h/h3866.md), that [tāp̄aś](../../strongs/h/h8610.md) and [dāraḵ](../../strongs/h/h1869.md) the [qesheth](../../strongs/h/h7198.md).

<a name="jeremiah_46_10"></a>Jeremiah 46:10

For this is the [yowm](../../strongs/h/h3117.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md), a [yowm](../../strongs/h/h3117.md) of [nᵊqāmâ](../../strongs/h/h5360.md), that he may [naqam](../../strongs/h/h5358.md) him of his [tsar](../../strongs/h/h6862.md): and the [chereb](../../strongs/h/h2719.md) shall ['akal](../../strongs/h/h398.md), and it shall be [sāׂbaʿ](../../strongs/h/h7646.md) and made [rāvâ](../../strongs/h/h7301.md) with their [dam](../../strongs/h/h1818.md): for the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) hath a [zebach](../../strongs/h/h2077.md) in the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md) by the [nāhār](../../strongs/h/h5104.md) [Pᵊrāṯ](../../strongs/h/h6578.md).

<a name="jeremiah_46_11"></a>Jeremiah 46:11

[ʿālâ](../../strongs/h/h5927.md) into [Gilʿāḏ](../../strongs/h/h1568.md), and [laqach](../../strongs/h/h3947.md) [ṣŏrî](../../strongs/h/h6875.md), O [bᵊṯûlâ](../../strongs/h/h1330.md), the [bath](../../strongs/h/h1323.md) of [Mitsrayim](../../strongs/h/h4714.md): in [shav'](../../strongs/h/h7723.md) shalt thou use [rabah](../../strongs/h/h7235.md) [rp̄v'h](../../strongs/h/h7499.md); for thou shalt not be [tᵊʿālâ](../../strongs/h/h8585.md).

<a name="jeremiah_46_12"></a>Jeremiah 46:12

The [gowy](../../strongs/h/h1471.md) have [shama'](../../strongs/h/h8085.md) of thy [qālôn](../../strongs/h/h7036.md), and thy [ṣᵊvāḥâ](../../strongs/h/h6682.md) hath [mālā'](../../strongs/h/h4390.md) the ['erets](../../strongs/h/h776.md): for the [gibôr](../../strongs/h/h1368.md) hath [kashal](../../strongs/h/h3782.md) against the [gibôr](../../strongs/h/h1368.md), and they are [naphal](../../strongs/h/h5307.md) [šᵊnayim](../../strongs/h/h8147.md) [yaḥaḏ](../../strongs/h/h3162.md).

<a name="jeremiah_46_13"></a>Jeremiah 46:13

The [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), how [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) should [bow'](../../strongs/h/h935.md) and [nakah](../../strongs/h/h5221.md) the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md).

<a name="jeremiah_46_14"></a>Jeremiah 46:14

[nāḡaḏ](../../strongs/h/h5046.md) ye in [Mitsrayim](../../strongs/h/h4714.md), and [shama'](../../strongs/h/h8085.md) in [Miḡdôl](../../strongs/h/h4024.md), and [shama'](../../strongs/h/h8085.md) in [Nōp̄](../../strongs/h/h5297.md) and in [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md): ['āmar](../../strongs/h/h559.md) ye, Stand [yatsab](../../strongs/h/h3320.md), and [kuwn](../../strongs/h/h3559.md) thee; for the [chereb](../../strongs/h/h2719.md) shall ['akal](../../strongs/h/h398.md) [cabiyb](../../strongs/h/h5439.md) thee.

<a name="jeremiah_46_15"></a>Jeremiah 46:15

Why are thy ['abîr](../../strongs/h/h47.md) men [sāḥap̄](../../strongs/h/h5502.md)? they ['amad](../../strongs/h/h5975.md) not, because [Yĕhovah](../../strongs/h/h3068.md) did [hāḏap̄](../../strongs/h/h1920.md) them.

<a name="jeremiah_46_16"></a>Jeremiah 46:16

He [rabah](../../strongs/h/h7235.md) to [kashal](../../strongs/h/h3782.md), yea, ['iysh](../../strongs/h/h376.md) [naphal](../../strongs/h/h5307.md) upon [rea'](../../strongs/h/h7453.md): and they ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), and let us [shuwb](../../strongs/h/h7725.md) to our own ['am](../../strongs/h/h5971.md), and to the ['erets](../../strongs/h/h776.md) of our [môleḏeṯ](../../strongs/h/h4138.md), [paniym](../../strongs/h/h6440.md) the [yānâ](../../strongs/h/h3238.md) [chereb](../../strongs/h/h2719.md).

<a name="jeremiah_46_17"></a>Jeremiah 46:17

They did [qara'](../../strongs/h/h7121.md) there, [Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md) is but a [shā'ôn](../../strongs/h/h7588.md); he hath ['abar](../../strongs/h/h5674.md) the [môʿēḏ](../../strongs/h/h4150.md).

<a name="jeremiah_46_18"></a>Jeremiah 46:18

As I [chay](../../strongs/h/h2416.md), [nᵊ'um](../../strongs/h/h5002.md) the [melek](../../strongs/h/h4428.md), whose [shem](../../strongs/h/h8034.md) is [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), Surely as [Tāḇôr](../../strongs/h/h8396.md) is among the [har](../../strongs/h/h2022.md), and as [karmel](../../strongs/h/h3760.md) by the [yam](../../strongs/h/h3220.md), so shall he [bow'](../../strongs/h/h935.md).

<a name="jeremiah_46_19"></a>Jeremiah 46:19

O thou [bath](../../strongs/h/h1323.md) [yashab](../../strongs/h/h3427.md) in [Mitsrayim](../../strongs/h/h4714.md), ['asah](../../strongs/h/h6213.md) thyself to go into [kĕliy](../../strongs/h/h3627.md) [gôlâ](../../strongs/h/h1473.md): for [Nōp̄](../../strongs/h/h5297.md) shall be [šammâ](../../strongs/h/h8047.md) and [yāṣaṯ](../../strongs/h/h3341.md) without a [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_46_20"></a>Jeremiah 46:20

[Mitsrayim](../../strongs/h/h4714.md) is like a very [yᵊp̄ê-p̄îyâ](../../strongs/h/h3304.md) [ʿeḡlâ](../../strongs/h/h5697.md), but [qereṣ](../../strongs/h/h7171.md) [bow'](../../strongs/h/h935.md); it [bow'](../../strongs/h/h935.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="jeremiah_46_21"></a>Jeremiah 46:21

Also her [śāḵîr](../../strongs/h/h7916.md) are in the [qereḇ](../../strongs/h/h7130.md) of her like [marbēq](../../strongs/h/h4770.md) [ʿēḡel](../../strongs/h/h5695.md); for they also are turned [panah](../../strongs/h/h6437.md), and are [nûs](../../strongs/h/h5127.md) [yaḥaḏ](../../strongs/h/h3162.md): they did not ['amad](../../strongs/h/h5975.md), because the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md) was [bow'](../../strongs/h/h935.md) upon them, and the [ʿēṯ](../../strongs/h/h6256.md) of their [pᵊqudâ](../../strongs/h/h6486.md).

<a name="jeremiah_46_22"></a>Jeremiah 46:22

The [qowl](../../strongs/h/h6963.md) thereof shall [yālaḵ](../../strongs/h/h3212.md) like a [nachash](../../strongs/h/h5175.md); for they shall [yālaḵ](../../strongs/h/h3212.md) with an [ḥayil](../../strongs/h/h2428.md), and [bow'](../../strongs/h/h935.md) against her with [qardōm](../../strongs/h/h7134.md), as [ḥāṭaḇ](../../strongs/h/h2404.md) of ['ets](../../strongs/h/h6086.md).

<a name="jeremiah_46_23"></a>Jeremiah 46:23

They shall [karath](../../strongs/h/h3772.md) her [yaʿar](../../strongs/h/h3293.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), though it cannot be [chaqar](../../strongs/h/h2713.md); because they are [rabab](../../strongs/h/h7231.md) than the ['arbê](../../strongs/h/h697.md), and are ['în](../../strongs/h/h369.md) [mispār](../../strongs/h/h4557.md).

<a name="jeremiah_46_24"></a>Jeremiah 46:24

The [bath](../../strongs/h/h1323.md) of [Mitsrayim](../../strongs/h/h4714.md) shall be [yāḇēš](../../strongs/h/h3001.md); she shall be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the ['am](../../strongs/h/h5971.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md).

<a name="jeremiah_46_25"></a>Jeremiah 46:25

[Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md); Behold, I will [paqad](../../strongs/h/h6485.md) the ['āmôn](../../strongs/h/h527.md) H528 of [Nō'](../../strongs/h/h4996.md), and [Parʿô](../../strongs/h/h6547.md), and [Mitsrayim](../../strongs/h/h4714.md), with their ['Elohiym](../../strongs/h/h430.md), and their [melek](../../strongs/h/h4428.md); even [Parʿô](../../strongs/h/h6547.md), and all them that [batach](../../strongs/h/h982.md) in him:

<a name="jeremiah_46_26"></a>Jeremiah 46:26

And I will [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of those that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md), and into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and into the [yad](../../strongs/h/h3027.md) of his ['ebed](../../strongs/h/h5650.md): and ['aḥar](../../strongs/h/h310.md) it shall be [shakan](../../strongs/h/h7931.md), as in the [yowm](../../strongs/h/h3117.md) of [qeḏem](../../strongs/h/h6924.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_46_27"></a>Jeremiah 46:27

But [yare'](../../strongs/h/h3372.md) not thou, O my ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md), and be not [ḥāṯaṯ](../../strongs/h/h2865.md), O [Yisra'el](../../strongs/h/h3478.md): for, behold, I will [yasha'](../../strongs/h/h3467.md) thee from afar [rachowq](../../strongs/h/h7350.md), and thy [zera'](../../strongs/h/h2233.md) from the ['erets](../../strongs/h/h776.md) of their [šᵊḇî](../../strongs/h/h7628.md); and [Ya'aqob](../../strongs/h/h3290.md) shall [shuwb](../../strongs/h/h7725.md), and be in [šāqaṭ](../../strongs/h/h8252.md) and at [šā'an](../../strongs/h/h7599.md), and none shall make him [ḥārēḏ](../../strongs/h/h2729.md).

<a name="jeremiah_46_28"></a>Jeremiah 46:28

[yare'](../../strongs/h/h3372.md) thou not, O [Ya'aqob](../../strongs/h/h3290.md) my ['ebed](../../strongs/h/h5650.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): for I am with thee; for I will ['asah](../../strongs/h/h6213.md) a full [kālâ](../../strongs/h/h3617.md) of all the [gowy](../../strongs/h/h1471.md) whither I have [nāḏaḥ](../../strongs/h/h5080.md) thee: but I will not ['asah](../../strongs/h/h6213.md) a full [kālâ](../../strongs/h/h3617.md) of thee, but [yacar](../../strongs/h/h3256.md) thee in [mishpat](../../strongs/h/h4941.md); yet will I not leave thee [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 45](jeremiah_45.md) - [Jeremiah 47](jeremiah_47.md)