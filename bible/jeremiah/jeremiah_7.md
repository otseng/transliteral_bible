# [Jeremiah 7](https://www.blueletterbible.org/kjv/jeremiah/7)

<a name="jeremiah_7_1"></a>Jeremiah 7:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_7_2"></a>Jeremiah 7:2

['amad](../../strongs/h/h5975.md) in the [sha'ar](../../strongs/h/h8179.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md), and [qara'](../../strongs/h/h7121.md) there this [dabar](../../strongs/h/h1697.md), and ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), all ye of [Yehuwdah](../../strongs/h/h3063.md), that enter [bow'](../../strongs/h/h935.md) at these [sha'ar](../../strongs/h/h8179.md) to [shachah](../../strongs/h/h7812.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_7_3"></a>Jeremiah 7:3

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), [yatab](../../strongs/h/h3190.md) your [derek](../../strongs/h/h1870.md) and your [maʿălāl](../../strongs/h/h4611.md), and I will cause you to [shakan](../../strongs/h/h7931.md) in this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_7_4"></a>Jeremiah 7:4

[batach](../../strongs/h/h982.md) ye not in [sheqer](../../strongs/h/h8267.md) [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), The [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), The [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), The [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), are these.

<a name="jeremiah_7_5"></a>Jeremiah 7:5

For if ye [yatab](../../strongs/h/h3190.md) [yatab](../../strongs/h/h3190.md) your [derek](../../strongs/h/h1870.md) and your [maʿălāl](../../strongs/h/h4611.md); if ye ['asah](../../strongs/h/h6213.md) ['asah](../../strongs/h/h6213.md) [mishpat](../../strongs/h/h4941.md) between an ['iysh](../../strongs/h/h376.md) and his [rea'](../../strongs/h/h7453.md);

<a name="jeremiah_7_6"></a>Jeremiah 7:6

If ye [ʿāšaq](../../strongs/h/h6231.md) not the [ger](../../strongs/h/h1616.md), the [yathowm](../../strongs/h/h3490.md), and the ['almānâ](../../strongs/h/h490.md), and [šāp̄aḵ](../../strongs/h/h8210.md) not [naqiy](../../strongs/h/h5355.md) [dam](../../strongs/h/h1818.md) in this [maqowm](../../strongs/h/h4725.md), neither [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to your [ra'](../../strongs/h/h7451.md):

<a name="jeremiah_7_7"></a>Jeremiah 7:7

Then will I cause you to [shakan](../../strongs/h/h7931.md) in this [maqowm](../../strongs/h/h4725.md), in the ['erets](../../strongs/h/h776.md) that I [nathan](../../strongs/h/h5414.md) to your ['ab](../../strongs/h/h1.md), [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md) and ['owlam](../../strongs/h/h5769.md).

<a name="jeremiah_7_8"></a>Jeremiah 7:8

Behold, ye [batach](../../strongs/h/h982.md) in [sheqer](../../strongs/h/h8267.md) [dabar](../../strongs/h/h1697.md), that cannot [yāʿal](../../strongs/h/h3276.md).

<a name="jeremiah_7_9"></a>Jeremiah 7:9

Will ye [ganab](../../strongs/h/h1589.md), [ratsach](../../strongs/h/h7523.md), and [na'aph](../../strongs/h/h5003.md), and [shaba'](../../strongs/h/h7650.md) [sheqer](../../strongs/h/h8267.md), and [qāṭar](../../strongs/h/h6999.md) unto [BaʿAl](../../strongs/h/h1168.md), and [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) whom ye [yada'](../../strongs/h/h3045.md) not;

<a name="jeremiah_7_10"></a>Jeremiah 7:10

And [bow'](../../strongs/h/h935.md) and ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me in this [bayith](../../strongs/h/h1004.md), which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), and ['āmar](../../strongs/h/h559.md), We are [natsal](../../strongs/h/h5337.md) to ['asah](../../strongs/h/h6213.md) all these [tôʿēḇâ](../../strongs/h/h8441.md)?

<a name="jeremiah_7_11"></a>Jeremiah 7:11

Is this [bayith](../../strongs/h/h1004.md), which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), become a [mᵊʿārâ](../../strongs/h/h4631.md) of [periyts](../../strongs/h/h6530.md) in your ['ayin](../../strongs/h/h5869.md)? Behold, even I have [ra'ah](../../strongs/h/h7200.md) it, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_7_12"></a>Jeremiah 7:12

But [yālaḵ](../../strongs/h/h3212.md) ye now unto my [maqowm](../../strongs/h/h4725.md) which was in [Šîlô](../../strongs/h/h7887.md), where I [shakan](../../strongs/h/h7931.md) my [shem](../../strongs/h/h8034.md) at the [ri'šôn](../../strongs/h/h7223.md), and [ra'ah](../../strongs/h/h7200.md) what I ['asah](../../strongs/h/h6213.md) to it [paniym](../../strongs/h/h6440.md) the [ra'](../../strongs/h/h7451.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_7_13"></a>Jeremiah 7:13

And now, because ye have ['asah](../../strongs/h/h6213.md) all these [ma'aseh](../../strongs/h/h4639.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and I [dabar](../../strongs/h/h1696.md) unto you, [šāḵam](../../strongs/h/h7925.md) and [dabar](../../strongs/h/h1696.md), but ye [shama'](../../strongs/h/h8085.md) not; and I [qara'](../../strongs/h/h7121.md) you, but ye ['anah](../../strongs/h/h6030.md) not;

<a name="jeremiah_7_14"></a>Jeremiah 7:14

Therefore will I ['asah](../../strongs/h/h6213.md) unto this [bayith](../../strongs/h/h1004.md), which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), wherein ye [batach](../../strongs/h/h982.md), and unto the [maqowm](../../strongs/h/h4725.md) which I [nathan](../../strongs/h/h5414.md) to you and to your ['ab](../../strongs/h/h1.md), as I have ['asah](../../strongs/h/h6213.md) to [Šîlô](../../strongs/h/h7887.md).

<a name="jeremiah_7_15"></a>Jeremiah 7:15

And I will [shalak](../../strongs/h/h7993.md) you of my [paniym](../../strongs/h/h6440.md), as I have [shalak](../../strongs/h/h7993.md) all your ['ach](../../strongs/h/h251.md), even the whole [zera'](../../strongs/h/h2233.md) of ['Ep̄rayim](../../strongs/h/h669.md).

<a name="jeremiah_7_16"></a>Jeremiah 7:16

Therefore [palal](../../strongs/h/h6419.md) not thou for this ['am](../../strongs/h/h5971.md), neither [nasa'](../../strongs/h/h5375.md) [rinnah](../../strongs/h/h7440.md) nor [tĕphillah](../../strongs/h/h8605.md) for [bᵊʿaḏ](../../strongs/h/h1157.md), neither [pāḡaʿ](../../strongs/h/h6293.md) to me: for I will not [shama'](../../strongs/h/h8085.md) thee.

<a name="jeremiah_7_17"></a>Jeremiah 7:17

[ra'ah](../../strongs/h/h7200.md) thou not what they ['asah](../../strongs/h/h6213.md) in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) and in the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md)?

<a name="jeremiah_7_18"></a>Jeremiah 7:18

The [ben](../../strongs/h/h1121.md) [lāqaṭ](../../strongs/h/h3950.md) ['ets](../../strongs/h/h6086.md), and the ['ab](../../strongs/h/h1.md) [bāʿar](../../strongs/h/h1197.md) the ['esh](../../strongs/h/h784.md), and the ['ishshah](../../strongs/h/h802.md) [lûš](../../strongs/h/h3888.md) their [bāṣēq](../../strongs/h/h1217.md), to ['asah](../../strongs/h/h6213.md) [kaûān](../../strongs/h/h3561.md) to the [milkōṯ](../../strongs/h/h4446.md) of [shamayim](../../strongs/h/h8064.md), and to [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), that they may provoke me to [kāʿas](../../strongs/h/h3707.md).

<a name="jeremiah_7_19"></a>Jeremiah 7:19

Do they provoke me to [kāʿas](../../strongs/h/h3707.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): do they not provoke themselves to the [bšeṯ](../../strongs/h/h1322.md) of their own [paniym](../../strongs/h/h6440.md)?

<a name="jeremiah_7_20"></a>Jeremiah 7:20

Therefore thus ['āmar](../../strongs/h/h559.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md); Behold, mine ['aph](../../strongs/h/h639.md) and my [chemah](../../strongs/h/h2534.md) shall be [nāṯaḵ](../../strongs/h/h5413.md) upon this [maqowm](../../strongs/h/h4725.md), upon ['āḏām](../../strongs/h/h120.md), and upon [bĕhemah](../../strongs/h/h929.md), and upon the ['ets](../../strongs/h/h6086.md) of the [sadeh](../../strongs/h/h7704.md), and upon the [pĕriy](../../strongs/h/h6529.md) of the ['ăḏāmâ](../../strongs/h/h127.md); and it shall [bāʿar](../../strongs/h/h1197.md), and shall not be [kāḇâ](../../strongs/h/h3518.md).

<a name="jeremiah_7_21"></a>Jeremiah 7:21

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [sāp̄â](../../strongs/h/h5595.md) your [ʿōlâ](../../strongs/h/h5930.md) unto your [zebach](../../strongs/h/h2077.md), and ['akal](../../strongs/h/h398.md) [basar](../../strongs/h/h1320.md).

<a name="jeremiah_7_22"></a>Jeremiah 7:22

For I [dabar](../../strongs/h/h1696.md) not unto your ['ab](../../strongs/h/h1.md), nor [tsavah](../../strongs/h/h6680.md) them in the [yowm](../../strongs/h/h3117.md) that I brought them [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), [dabar](../../strongs/h/h1697.md) [ʿōlâ](../../strongs/h/h5930.md) or [zebach](../../strongs/h/h2077.md):

<a name="jeremiah_7_23"></a>Jeremiah 7:23

But this [dabar](../../strongs/h/h1697.md) [tsavah](../../strongs/h/h6680.md) I them, ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) my [qowl](../../strongs/h/h6963.md), and I will be your ['Elohiym](../../strongs/h/h430.md), and ye shall be my ['am](../../strongs/h/h5971.md): and [halak](../../strongs/h/h1980.md) ye in all the [derek](../../strongs/h/h1870.md) that I have [tsavah](../../strongs/h/h6680.md) you, that it may be [yatab](../../strongs/h/h3190.md) unto you.

<a name="jeremiah_7_24"></a>Jeremiah 7:24

But they [shama'](../../strongs/h/h8085.md) not, nor [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md), but [yālaḵ](../../strongs/h/h3212.md) in the [mow'etsah](../../strongs/h/h4156.md) and in the [šᵊrîrûṯ](../../strongs/h/h8307.md) of their [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md), and went ['āḥôr](../../strongs/h/h268.md), and not [paniym](../../strongs/h/h6440.md).

<a name="jeremiah_7_25"></a>Jeremiah 7:25

Since the [yowm](../../strongs/h/h3117.md) that your ['ab](../../strongs/h/h1.md) [yāṣā'](../../strongs/h/h3318.md) of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) unto this [yowm](../../strongs/h/h3117.md) I have even [shalach](../../strongs/h/h7971.md) unto you all my ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), [yowm](../../strongs/h/h3117.md) [šāḵam](../../strongs/h/h7925.md) and [shalach](../../strongs/h/h7971.md) them:

<a name="jeremiah_7_26"></a>Jeremiah 7:26

Yet they [shama'](../../strongs/h/h8085.md) not unto me, nor [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md), but [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md): they did [ra'a'](../../strongs/h/h7489.md) than their ['ab](../../strongs/h/h1.md).

<a name="jeremiah_7_27"></a>Jeremiah 7:27

Therefore thou shalt [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md) unto them; but they will not [shama'](../../strongs/h/h8085.md) to thee: thou shalt also [qara'](../../strongs/h/h7121.md) unto them; but they will not ['anah](../../strongs/h/h6030.md) thee.

<a name="jeremiah_7_28"></a>Jeremiah 7:28

But thou shalt ['āmar](../../strongs/h/h559.md) unto them, This is a [gowy](../../strongs/h/h1471.md) that [shama'](../../strongs/h/h8085.md) not the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), nor [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md): ['ĕmûnâ](../../strongs/h/h530.md) is ['abad](../../strongs/h/h6.md), and is [karath](../../strongs/h/h3772.md) from their [peh](../../strongs/h/h6310.md).

<a name="jeremiah_7_29"></a>Jeremiah 7:29

Cut [gāzaz](../../strongs/h/h1494.md) thine [nēzer](../../strongs/h/h5145.md), and [shalak](../../strongs/h/h7993.md) it, and [nasa'](../../strongs/h/h5375.md) a [qînâ](../../strongs/h/h7015.md) on [šᵊp̄î](../../strongs/h/h8205.md); for [Yĕhovah](../../strongs/h/h3068.md) hath [mā'as](../../strongs/h/h3988.md) and [nāṭaš](../../strongs/h/h5203.md) the [dôr](../../strongs/h/h1755.md) of his ['ebrah](../../strongs/h/h5678.md).

<a name="jeremiah_7_30"></a>Jeremiah 7:30

For the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) have ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in my ['ayin](../../strongs/h/h5869.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): they have [śûm](../../strongs/h/h7760.md) their [šiqqûṣ](../../strongs/h/h8251.md) in the [bayith](../../strongs/h/h1004.md) which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), to [ṭāmē'](../../strongs/h/h2930.md) it.

<a name="jeremiah_7_31"></a>Jeremiah 7:31

And they have [bānâ](../../strongs/h/h1129.md) the [bāmâ](../../strongs/h/h1116.md) of [Tōp̄Eṯ](../../strongs/h/h8612.md), which is in the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), to [śārap̄](../../strongs/h/h8313.md) their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) in the ['esh](../../strongs/h/h784.md); which I [tsavah](../../strongs/h/h6680.md) them not, neither [ʿālâ](../../strongs/h/h5927.md) it into my [leb](../../strongs/h/h3820.md).

<a name="jeremiah_7_32"></a>Jeremiah 7:32

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that it shall no more be ['āmar](../../strongs/h/h559.md) [Tōp̄Eṯ](../../strongs/h/h8612.md), nor the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), but the [gay'](../../strongs/h/h1516.md) of [hărēḡâ](../../strongs/h/h2028.md): for they shall [qāḇar](../../strongs/h/h6912.md) in [Tōp̄Eṯ](../../strongs/h/h8612.md), till there be ['în](../../strongs/h/h369.md) [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_7_33"></a>Jeremiah 7:33

And the [nᵊḇēlâ](../../strongs/h/h5038.md) of this ['am](../../strongs/h/h5971.md) shall be [ma'akal](../../strongs/h/h3978.md) for the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and for the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md); and none shall fray them [ḥārēḏ](../../strongs/h/h2729.md).

<a name="jeremiah_7_34"></a>Jeremiah 7:34

Then will I cause to [shabath](../../strongs/h/h7673.md) from the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and from the [ḥûṣ](../../strongs/h/h2351.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [qowl](../../strongs/h/h6963.md) of [śāśôn](../../strongs/h/h8342.md), and the [qowl](../../strongs/h/h6963.md) of [simchah](../../strongs/h/h8057.md), the [qowl](../../strongs/h/h6963.md) of the [ḥāṯān](../../strongs/h/h2860.md), and the [qowl](../../strongs/h/h6963.md) of the [kallâ](../../strongs/h/h3618.md): for the ['erets](../../strongs/h/h776.md) shall be [chorbah](../../strongs/h/h2723.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 6](jeremiah_6.md) - [Jeremiah 8](jeremiah_8.md)