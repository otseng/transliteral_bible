# [Jeremiah 47](https://www.blueletterbible.org/kjv/jeremiah/47)

<a name="jeremiah_47_1"></a>Jeremiah 47:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) against the [Pᵊlištî](../../strongs/h/h6430.md), before that [Parʿô](../../strongs/h/h6547.md) [nakah](../../strongs/h/h5221.md) [ʿAzzâ](../../strongs/h/h5804.md).

<a name="jeremiah_47_2"></a>Jeremiah 47:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, [mayim](../../strongs/h/h4325.md) [ʿālâ](../../strongs/h/h5927.md) out of the [ṣāp̄ôn](../../strongs/h/h6828.md), and shall be a [šāṭap̄](../../strongs/h/h7857.md) [nachal](../../strongs/h/h5158.md), and shall [šāṭap̄](../../strongs/h/h7857.md) the ['erets](../../strongs/h/h776.md), and all that is [mᵊlō'](../../strongs/h/h4393.md); the [ʿîr](../../strongs/h/h5892.md), and them that [yashab](../../strongs/h/h3427.md) therein: then the ['āḏām](../../strongs/h/h120.md) shall [zāʿaq](../../strongs/h/h2199.md), and all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md) shall [yālal](../../strongs/h/h3213.md).

<a name="jeremiah_47_3"></a>Jeremiah 47:3

At the [qowl](../../strongs/h/h6963.md) of the [šᵊʿāṭâ](../../strongs/h/h8161.md) of the [parsâ](../../strongs/h/h6541.md) of his ['abîr](../../strongs/h/h47.md), at the [raʿaš](../../strongs/h/h7494.md) of his [reḵeḇ](../../strongs/h/h7393.md), and at the [hāmôn](../../strongs/h/h1995.md) of his [galgal](../../strongs/h/h1534.md), the ['ab](../../strongs/h/h1.md) shall not [panah](../../strongs/h/h6437.md) to their [ben](../../strongs/h/h1121.md) for [rip̄yôn](../../strongs/h/h7510.md) of [yad](../../strongs/h/h3027.md);

<a name="jeremiah_47_4"></a>Jeremiah 47:4

Because of the [yowm](../../strongs/h/h3117.md) that [bow'](../../strongs/h/h935.md) to [shadad](../../strongs/h/h7703.md) all the [Pᵊlištî](../../strongs/h/h6430.md), and to [karath](../../strongs/h/h3772.md) from [Ṣōr](../../strongs/h/h6865.md) and [Ṣîḏôn](../../strongs/h/h6721.md) every [ʿāzar](../../strongs/h/h5826.md) that [śārîḏ](../../strongs/h/h8300.md): for [Yĕhovah](../../strongs/h/h3068.md) will [shadad](../../strongs/h/h7703.md) the [Pᵊlištî](../../strongs/h/h6430.md), the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of the ['î](../../strongs/h/h339.md) of [Kap̄Tôr](../../strongs/h/h3731.md).

<a name="jeremiah_47_5"></a>Jeremiah 47:5

[qrḥ](../../strongs/h/h7144.md) is [bow'](../../strongs/h/h935.md) upon [ʿAzzâ](../../strongs/h/h5804.md); ['Ašqᵊlôn](../../strongs/h/h831.md) is cut [damah](../../strongs/h/h1820.md) with the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of their [ʿēmeq](../../strongs/h/h6010.md): how long wilt thou [gāḏaḏ](../../strongs/h/h1413.md) thyself?

<a name="jeremiah_47_6"></a>Jeremiah 47:6

[hôy](../../strongs/h/h1945.md) thou [chereb](../../strongs/h/h2719.md) of [Yĕhovah](../../strongs/h/h3068.md), how long will it be [lō'](../../strongs/h/h3808.md) thou be [šāqaṭ](../../strongs/h/h8252.md)? put ['āsap̄](../../strongs/h/h622.md) thyself into thy [taʿar](../../strongs/h/h8593.md), [rāḡaʿ](../../strongs/h/h7280.md), and be [damam](../../strongs/h/h1826.md).

<a name="jeremiah_47_7"></a>Jeremiah 47:7

How can it be [šāqaṭ](../../strongs/h/h8252.md), seeing [Yĕhovah](../../strongs/h/h3068.md) hath given it a [tsavah](../../strongs/h/h6680.md) against ['Ašqᵊlôn](../../strongs/h/h831.md), and against the [yam](../../strongs/h/h3220.md) [ḥôp̄](../../strongs/h/h2348.md)? there hath he [yāʿaḏ](../../strongs/h/h3259.md) it.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 46](jeremiah_46.md) - [Jeremiah 48](jeremiah_48.md)