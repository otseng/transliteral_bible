# [Jeremiah 30](https://www.blueletterbible.org/kjv/jeremiah/30)

<a name="jeremiah_30_1"></a>Jeremiah 30:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_30_2"></a>Jeremiah 30:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), ['āmar](../../strongs/h/h559.md), [kāṯaḇ](../../strongs/h/h3789.md) thee all the [dabar](../../strongs/h/h1697.md) that I have [dabar](../../strongs/h/h1696.md) unto thee in a [sēp̄er](../../strongs/h/h5612.md).

<a name="jeremiah_30_3"></a>Jeremiah 30:3

For, lo, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that I will [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) and [Yehuwdah](../../strongs/h/h3063.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md): and I will cause them to [shuwb](../../strongs/h/h7725.md) to the ['erets](../../strongs/h/h776.md) that I [nathan](../../strongs/h/h5414.md) to their ['ab](../../strongs/h/h1.md), and they shall [yarash](../../strongs/h/h3423.md) it.

<a name="jeremiah_30_4"></a>Jeremiah 30:4

And these are the [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) concerning [Yisra'el](../../strongs/h/h3478.md) and concerning [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_30_5"></a>Jeremiah 30:5

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); We have [shama'](../../strongs/h/h8085.md) a [qowl](../../strongs/h/h6963.md) of [ḥărāḏâ](../../strongs/h/h2731.md), of [paḥaḏ](../../strongs/h/h6343.md), and not of [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_30_6"></a>Jeremiah 30:6

[sha'al](../../strongs/h/h7592.md) ye now, and [ra'ah](../../strongs/h/h7200.md) whether a [zāḵār](../../strongs/h/h2145.md) doth travail with [yalad](../../strongs/h/h3205.md)? wherefore do I [ra'ah](../../strongs/h/h7200.md) every [geḇer](../../strongs/h/h1397.md) with his [yad](../../strongs/h/h3027.md) on his [ḥālāṣ](../../strongs/h/h2504.md), as a [yalad](../../strongs/h/h3205.md), and all [paniym](../../strongs/h/h6440.md) are [hāp̄aḵ](../../strongs/h/h2015.md) into [yērāqôn](../../strongs/h/h3420.md)?

<a name="jeremiah_30_7"></a>Jeremiah 30:7

[hôy](../../strongs/h/h1945.md)! for that [yowm](../../strongs/h/h3117.md) is [gadowl](../../strongs/h/h1419.md), so ['în](../../strongs/h/h369.md) none is like it: it is even the [ʿēṯ](../../strongs/h/h6256.md) of [Ya'aqob](../../strongs/h/h3290.md) [tsarah](../../strongs/h/h6869.md); but he shall be [yasha'](../../strongs/h/h3467.md) out of it.

<a name="jeremiah_30_8"></a>Jeremiah 30:8

For it shall come to pass in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that I will [shabar](../../strongs/h/h7665.md) his [ʿōl](../../strongs/h/h5923.md) from off thy [ṣaûā'r](../../strongs/h/h6677.md), and will [nathaq](../../strongs/h/h5423.md) thy [mowcer](../../strongs/h/h4147.md), and [zûr](../../strongs/h/h2114.md) shall no more ['abad](../../strongs/h/h5647.md) themselves of him:

<a name="jeremiah_30_9"></a>Jeremiah 30:9

But they shall ['abad](../../strongs/h/h5647.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md), and [Dāviḏ](../../strongs/h/h1732.md) their [melek](../../strongs/h/h4428.md), whom I will raise [quwm](../../strongs/h/h6965.md) unto them.

<a name="jeremiah_30_10"></a>Jeremiah 30:10

Therefore [yare'](../../strongs/h/h3372.md) thou not, O my ['ebed](../../strongs/h/h5650.md) [Ya'aqob](../../strongs/h/h3290.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); neither be [ḥāṯaṯ](../../strongs/h/h2865.md), O [Yisra'el](../../strongs/h/h3478.md): for, lo, I will [yasha'](../../strongs/h/h3467.md) thee from [rachowq](../../strongs/h/h7350.md), and thy [zera'](../../strongs/h/h2233.md) from the ['erets](../../strongs/h/h776.md) of their [šᵊḇî](../../strongs/h/h7628.md); and [Ya'aqob](../../strongs/h/h3290.md) shall [shuwb](../../strongs/h/h7725.md), and shall be in [šāqaṭ](../../strongs/h/h8252.md), and be [šā'an](../../strongs/h/h7599.md), and none shall make him [ḥārēḏ](../../strongs/h/h2729.md).

<a name="jeremiah_30_11"></a>Jeremiah 30:11

For I am with thee, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), to [yasha'](../../strongs/h/h3467.md) thee: though I ['asah](../../strongs/h/h6213.md) a full [kālâ](../../strongs/h/h3617.md) of all [gowy](../../strongs/h/h1471.md) whither I have [puwts](../../strongs/h/h6327.md) thee, yet will I not ['asah](../../strongs/h/h6213.md) a full [kālâ](../../strongs/h/h3617.md) of thee: but I will [yacar](../../strongs/h/h3256.md) thee in [mishpat](../../strongs/h/h4941.md), and will not leave thee [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md).

<a name="jeremiah_30_12"></a>Jeremiah 30:12

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Thy [šeḇar](../../strongs/h/h7667.md) is ['anash](../../strongs/h/h605.md), and thy [makâ](../../strongs/h/h4347.md) is [ḥālâ](../../strongs/h/h2470.md).

<a name="jeremiah_30_13"></a>Jeremiah 30:13

There is none to [diyn](../../strongs/h/h1777.md) thy [diyn](../../strongs/h/h1779.md), that thou mayest be [māzôr](../../strongs/h/h4205.md): thou hast no [tᵊʿālâ](../../strongs/h/h8585.md) [rp̄v'h](../../strongs/h/h7499.md).

<a name="jeremiah_30_14"></a>Jeremiah 30:14

All thy ['ahab](../../strongs/h/h157.md) have [shakach](../../strongs/h/h7911.md) thee; they [darash](../../strongs/h/h1875.md) thee not; for I have [nakah](../../strongs/h/h5221.md) thee with the [makâ](../../strongs/h/h4347.md) of an ['oyeb](../../strongs/h/h341.md), with the [mûsār](../../strongs/h/h4148.md) of an ['aḵzārî](../../strongs/h/h394.md), for the [rōḇ](../../strongs/h/h7230.md) of thine ['avon](../../strongs/h/h5771.md); because thy [chatta'ath](../../strongs/h/h2403.md) were [ʿāṣam](../../strongs/h/h6105.md).

<a name="jeremiah_30_15"></a>Jeremiah 30:15

Why [zāʿaq](../../strongs/h/h2199.md) thou for thine [šeḇar](../../strongs/h/h7667.md)? thy [maḵ'ōḇ](../../strongs/h/h4341.md) is ['anash](../../strongs/h/h605.md) for the [rōḇ](../../strongs/h/h7230.md) of thine ['avon](../../strongs/h/h5771.md): because thy [chatta'ath](../../strongs/h/h2403.md) were [ʿāṣam](../../strongs/h/h6105.md), I have ['asah](../../strongs/h/h6213.md) these things unto thee.

<a name="jeremiah_30_16"></a>Jeremiah 30:16

Therefore all they that ['akal](../../strongs/h/h398.md) thee shall be ['akal](../../strongs/h/h398.md); and all thine [tsar](../../strongs/h/h6862.md), every one of them, shall [yālaḵ](../../strongs/h/h3212.md) into [šᵊḇî](../../strongs/h/h7628.md); and they that [šō'ēs](../../strongs/h/h7601.md) [šāsâ](../../strongs/h/h8154.md) thee shall be a [mᵊšissâ](../../strongs/h/h4933.md), and all that [bāzaz](../../strongs/h/h962.md) upon thee will I [nathan](../../strongs/h/h5414.md) for a [baz](../../strongs/h/h957.md).

<a name="jeremiah_30_17"></a>Jeremiah 30:17

For I will [ʿālâ](../../strongs/h/h5927.md) ['ărûḵâ](../../strongs/h/h724.md) unto thee, and I will [rapha'](../../strongs/h/h7495.md) thee of thy [makâ](../../strongs/h/h4347.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); because they [qara'](../../strongs/h/h7121.md) thee a [nāḏaḥ](../../strongs/h/h5080.md), saying, This is [Tsiyown](../../strongs/h/h6726.md), whom no man [darash](../../strongs/h/h1875.md).

<a name="jeremiah_30_18"></a>Jeremiah 30:18

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [shuwb](../../strongs/h/h7725.md) the [shebuwth](../../strongs/h/h7622.md) of [Ya'aqob](../../strongs/h/h3290.md) ['ohel](../../strongs/h/h168.md), and have [racham](../../strongs/h/h7355.md) on his [miškān](../../strongs/h/h4908.md); and the [ʿîr](../../strongs/h/h5892.md) shall be [bānâ](../../strongs/h/h1129.md) upon her own [tēl](../../strongs/h/h8510.md), and the ['armôn](../../strongs/h/h759.md) shall [yashab](../../strongs/h/h3427.md) after the [mishpat](../../strongs/h/h4941.md) thereof.

<a name="jeremiah_30_19"></a>Jeremiah 30:19

And out of them shall [yāṣā'](../../strongs/h/h3318.md) [tôḏâ](../../strongs/h/h8426.md) and the [qowl](../../strongs/h/h6963.md) of them that make [śāḥaq](../../strongs/h/h7832.md): and I will [rabah](../../strongs/h/h7235.md) them, and they shall not be [māʿaṭ](../../strongs/h/h4591.md); I will also [kabad](../../strongs/h/h3513.md) them, and they shall not be [ṣāʿar](../../strongs/h/h6819.md).

<a name="jeremiah_30_20"></a>Jeremiah 30:20

Their [ben](../../strongs/h/h1121.md) also shall be as [qeḏem](../../strongs/h/h6924.md), and their ['edah](../../strongs/h/h5712.md) shall be [kuwn](../../strongs/h/h3559.md) [paniym](../../strongs/h/h6440.md) me, and I will [paqad](../../strongs/h/h6485.md) all that [lāḥaṣ](../../strongs/h/h3905.md) them.

<a name="jeremiah_30_21"></a>Jeremiah 30:21

And their ['addiyr](../../strongs/h/h117.md) shall be of themselves, and their [mashal](../../strongs/h/h4910.md) shall [yāṣā'](../../strongs/h/h3318.md) from the [qereḇ](../../strongs/h/h7130.md) of them; and I will cause him to [qāraḇ](../../strongs/h/h7126.md), and he shall [nāḡaš](../../strongs/h/h5066.md) unto me: for who is this that [ʿāraḇ](../../strongs/h/h6148.md) his [leb](../../strongs/h/h3820.md) to [nāḡaš](../../strongs/h/h5066.md) unto me? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_30_22"></a>Jeremiah 30:22

And ye shall be my ['am](../../strongs/h/h5971.md), and I will be your ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_30_23"></a>Jeremiah 30:23

Behold, the [saʿar](../../strongs/h/h5591.md) of [Yĕhovah](../../strongs/h/h3068.md) [yāṣā'](../../strongs/h/h3318.md) with [chemah](../../strongs/h/h2534.md), a [gārar](../../strongs/h/h1641.md) [saʿar](../../strongs/h/h5591.md): it shall fall with [chuwl](../../strongs/h/h2342.md) upon the [ro'sh](../../strongs/h/h7218.md) of the [rasha'](../../strongs/h/h7563.md).

<a name="jeremiah_30_24"></a>Jeremiah 30:24

The [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) shall not [shuwb](../../strongs/h/h7725.md), until he have ['asah](../../strongs/h/h6213.md) it, and until he have [quwm](../../strongs/h/h6965.md) the [mezimmah](../../strongs/h/h4209.md) of his [leb](../../strongs/h/h3820.md): in the ['aḥărîṯ](../../strongs/h/h319.md) [yowm](../../strongs/h/h3117.md) ye shall [bîn](../../strongs/h/h995.md) it.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 29](jeremiah_29.md) - [Jeremiah 31](jeremiah_31.md)