# [Jeremiah 32](https://www.blueletterbible.org/kjv/jeremiah/32)

<a name="jeremiah_32_1"></a>Jeremiah 32:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md) in the [ʿăśîrî](../../strongs/h/h6224.md) [šānâ](../../strongs/h/h8141.md) of [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), which was the [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md).

<a name="jeremiah_32_2"></a>Jeremiah 32:2

For then the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ḥayil](../../strongs/h/h2428.md) [ṣûr](../../strongs/h/h6696.md) [Yĕruwshalaim](../../strongs/h/h3389.md): and [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) was shut [kālā'](../../strongs/h/h3607.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md), which was in the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [bayith](../../strongs/h/h1004.md).

<a name="jeremiah_32_3"></a>Jeremiah 32:3

For [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) had [kālā'](../../strongs/h/h3607.md) him, ['āmar](../../strongs/h/h559.md), Wherefore dost thou [nāḇā'](../../strongs/h/h5012.md), and ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), Behold, I will [nathan](../../strongs/h/h5414.md) this [ʿîr](../../strongs/h/h5892.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [lāḵaḏ](../../strongs/h/h3920.md) it;

<a name="jeremiah_32_4"></a>Jeremiah 32:4

And [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) shall not [mālaṭ](../../strongs/h/h4422.md) out of the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), but shall [nathan](../../strongs/h/h5414.md) be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and shall [dabar](../../strongs/h/h1696.md) with him [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md), and his ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) his ['ayin](../../strongs/h/h5869.md);

<a name="jeremiah_32_5"></a>Jeremiah 32:5

And he shall [yālaḵ](../../strongs/h/h3212.md) [Ṣḏqyh](../../strongs/h/h6667.md) to [Bāḇel](../../strongs/h/h894.md), and there shall he be until I [paqad](../../strongs/h/h6485.md) him, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): though ye [lāḥam](../../strongs/h/h3898.md) with the [Kaśdîmâ](../../strongs/h/h3778.md), ye shall not [tsalach](../../strongs/h/h6743.md).

<a name="jeremiah_32_6"></a>Jeremiah 32:6

And [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md), The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_32_7"></a>Jeremiah 32:7

Behold, [Ḥănam'Ēl](../../strongs/h/h2601.md) the [ben](../../strongs/h/h1121.md) of [Šallûm](../../strongs/h/h7967.md) thine [dôḏ](../../strongs/h/h1730.md) shall [bow'](../../strongs/h/h935.md) unto thee, ['āmar](../../strongs/h/h559.md), [qānâ](../../strongs/h/h7069.md) thee my [sadeh](../../strongs/h/h7704.md) that is in [ʿĂnāṯôṯ](../../strongs/h/h6068.md): for the [mishpat](../../strongs/h/h4941.md) of [gᵊ'ullâ](../../strongs/h/h1353.md) is thine to [qānâ](../../strongs/h/h7069.md) it.

<a name="jeremiah_32_8"></a>Jeremiah 32:8

So [Ḥănam'Ēl](../../strongs/h/h2601.md) mine [dôḏ](../../strongs/h/h1730.md) [ben](../../strongs/h/h1121.md) [bow'](../../strongs/h/h935.md) to me in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md) according to the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), and ['āmar](../../strongs/h/h559.md) unto me, [qānâ](../../strongs/h/h7069.md) my [sadeh](../../strongs/h/h7704.md), I pray thee, that is in [ʿĂnāṯôṯ](../../strongs/h/h6068.md), which is in the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md): for the [mishpat](../../strongs/h/h4941.md) of [yᵊruššâ](../../strongs/h/h3425.md) is thine, and the [gᵊ'ullâ](../../strongs/h/h1353.md) is thine; [qānâ](../../strongs/h/h7069.md) it for thyself. Then I [yada'](../../strongs/h/h3045.md) that this was the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_32_9"></a>Jeremiah 32:9

And I [qānâ](../../strongs/h/h7069.md) the [sadeh](../../strongs/h/h7704.md) of [Ḥănam'Ēl](../../strongs/h/h2601.md) my [dôḏ](../../strongs/h/h1730.md) [ben](../../strongs/h/h1121.md), that was in [ʿĂnāṯôṯ](../../strongs/h/h6068.md), and [šāqal](../../strongs/h/h8254.md) him the [keceph](../../strongs/h/h3701.md), even [šeḇaʿ](../../strongs/h/h7651.md) [ʿeśer](../../strongs/h/h6235.md) [šeqel](../../strongs/h/h8255.md) of [keceph](../../strongs/h/h3701.md).

<a name="jeremiah_32_10"></a>Jeremiah 32:10

And I [kāṯaḇ](../../strongs/h/h3789.md) the [sēp̄er](../../strongs/h/h5612.md), and [ḥāṯam](../../strongs/h/h2856.md) it, and [ʿûḏ](../../strongs/h/h5749.md) ['ed](../../strongs/h/h5707.md), and [šāqal](../../strongs/h/h8254.md) him the [keceph](../../strongs/h/h3701.md) in the [mō'znayim](../../strongs/h/h3976.md).

<a name="jeremiah_32_11"></a>Jeremiah 32:11

So I [laqach](../../strongs/h/h3947.md) the [sēp̄er](../../strongs/h/h5612.md) of the [miqnâ](../../strongs/h/h4736.md), both that which was [ḥāṯam](../../strongs/h/h2856.md) according to the [mitsvah](../../strongs/h/h4687.md) and [choq](../../strongs/h/h2706.md), and that which was [gālâ](../../strongs/h/h1540.md):

<a name="jeremiah_32_12"></a>Jeremiah 32:12

And I [nathan](../../strongs/h/h5414.md) the [sēp̄er](../../strongs/h/h5612.md) of the [miqnâ](../../strongs/h/h4736.md) unto [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md), the [ben](../../strongs/h/h1121.md) of [Maḥsêâ](../../strongs/h/h4271.md), in the ['ayin](../../strongs/h/h5869.md) of [Ḥănam'Ēl](../../strongs/h/h2601.md) mine [dôḏ](../../strongs/h/h1730.md) son, and in the ['ayin](../../strongs/h/h5869.md) of the ['ed](../../strongs/h/h5707.md) that [kāṯaḇ](../../strongs/h/h3789.md) the [sēp̄er](../../strongs/h/h5612.md) of the [miqnâ](../../strongs/h/h4736.md), ['ayin](../../strongs/h/h5869.md) all the [Yᵊhûḏî](../../strongs/h/h3064.md) that [yashab](../../strongs/h/h3427.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md).

<a name="jeremiah_32_13"></a>Jeremiah 32:13

And I [tsavah](../../strongs/h/h6680.md) [Bārûḵ](../../strongs/h/h1263.md) ['ayin](../../strongs/h/h5869.md) them, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_32_14"></a>Jeremiah 32:14

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [laqach](../../strongs/h/h3947.md) these [sēp̄er](../../strongs/h/h5612.md), this [sēp̄er](../../strongs/h/h5612.md) of the [miqnâ](../../strongs/h/h4736.md), both which is [ḥāṯam](../../strongs/h/h2856.md), and this [sēp̄er](../../strongs/h/h5612.md) which is [gālâ](../../strongs/h/h1540.md); and [nathan](../../strongs/h/h5414.md) them in an [ḥereś](../../strongs/h/h2789.md) [kĕliy](../../strongs/h/h3627.md), that they may ['amad](../../strongs/h/h5975.md) [rab](../../strongs/h/h7227.md) [yowm](../../strongs/h/h3117.md).

<a name="jeremiah_32_15"></a>Jeremiah 32:15

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [bayith](../../strongs/h/h1004.md) and [sadeh](../../strongs/h/h7704.md) and [kerem](../../strongs/h/h3754.md) shall be [qānâ](../../strongs/h/h7069.md) in this ['erets](../../strongs/h/h776.md).

<a name="jeremiah_32_16"></a>Jeremiah 32:16

Now ['aḥar](../../strongs/h/h310.md) I had [nathan](../../strongs/h/h5414.md) the [sēp̄er](../../strongs/h/h5612.md) of the [miqnâ](../../strongs/h/h4736.md) unto [Bārûḵ](../../strongs/h/h1263.md) the [ben](../../strongs/h/h1121.md) of [Nērîyâ](../../strongs/h/h5374.md), I [palal](../../strongs/h/h6419.md) unto [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_32_17"></a>Jeremiah 32:17

['ăhâ](../../strongs/h/h162.md) ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md)! behold, thou hast ['asah](../../strongs/h/h6213.md) the [shamayim](../../strongs/h/h8064.md) and the ['erets](../../strongs/h/h776.md) by thy [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) and stretched [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and there is [dabar](../../strongs/h/h1697.md) too [pala'](../../strongs/h/h6381.md) for thee:

<a name="jeremiah_32_18"></a>Jeremiah 32:18

Thou ['asah](../../strongs/h/h6213.md) [checed](../../strongs/h/h2617.md) unto ['elep̄](../../strongs/h/h505.md), and [shalam](../../strongs/h/h7999.md) the ['avon](../../strongs/h/h5771.md) of the ['ab](../../strongs/h/h1.md) into the [ḥêq](../../strongs/h/h2436.md) of their [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) them: the [gadowl](../../strongs/h/h1419.md), the [gibôr](../../strongs/h/h1368.md) ['el](../../strongs/h/h410.md), [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), is his [shem](../../strongs/h/h8034.md),

<a name="jeremiah_32_19"></a>Jeremiah 32:19

[gadowl](../../strongs/h/h1419.md) in ['etsah](../../strongs/h/h6098.md), and [rab](../../strongs/h/h7227.md) in [ʿălîlîyâ](../../strongs/h/h5950.md): for thine ['ayin](../../strongs/h/h5869.md) are [paqach](../../strongs/h/h6491.md) upon all the [derek](../../strongs/h/h1870.md) of the [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md): to [nathan](../../strongs/h/h5414.md) every ['iysh](../../strongs/h/h376.md) according to his [derek](../../strongs/h/h1870.md), and according to the [pĕriy](../../strongs/h/h6529.md) of his [maʿălāl](../../strongs/h/h4611.md):

<a name="jeremiah_32_20"></a>Jeremiah 32:20

Which hast [śûm](../../strongs/h/h7760.md) ['ôṯ](../../strongs/h/h226.md) and [môp̄ēṯ](../../strongs/h/h4159.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), even unto this [yowm](../../strongs/h/h3117.md), and in [Yisra'el](../../strongs/h/h3478.md), and among other ['āḏām](../../strongs/h/h120.md); and hast ['asah](../../strongs/h/h6213.md) thee a [shem](../../strongs/h/h8034.md), as at this [yowm](../../strongs/h/h3117.md);

<a name="jeremiah_32_21"></a>Jeremiah 32:21

And hast [yāṣā'](../../strongs/h/h3318.md) thy ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md) with ['ôṯ](../../strongs/h/h226.md), and with [môp̄ēṯ](../../strongs/h/h4159.md), and with a [ḥāzāq](../../strongs/h/h2389.md) [yad](../../strongs/h/h3027.md), and with a stretched [natah](../../strongs/h/h5186.md) ['ezrôaʿ](../../strongs/h/h248.md), and with [gadowl](../../strongs/h/h1419.md) [mowra'](../../strongs/h/h4172.md);

<a name="jeremiah_32_22"></a>Jeremiah 32:22

And hast [nathan](../../strongs/h/h5414.md) them this ['erets](../../strongs/h/h776.md), which thou didst [shaba'](../../strongs/h/h7650.md) to their ['ab](../../strongs/h/h1.md) to [nathan](../../strongs/h/h5414.md) them, an ['erets](../../strongs/h/h776.md) [zûḇ](../../strongs/h/h2100.md) with [chalab](../../strongs/h/h2461.md) and [dĕbash](../../strongs/h/h1706.md);

<a name="jeremiah_32_23"></a>Jeremiah 32:23

And they [bow'](../../strongs/h/h935.md), and [yarash](../../strongs/h/h3423.md) it; but they [shama'](../../strongs/h/h8085.md) not thy [qowl](../../strongs/h/h6963.md), neither [halak](../../strongs/h/h1980.md) in thy [towrah](../../strongs/h/h8451.md); they have ['asah](../../strongs/h/h6213.md) nothing of all that thou [tsavah](../../strongs/h/h6680.md) them to ['asah](../../strongs/h/h6213.md): therefore thou hast caused all this [ra'](../../strongs/h/h7451.md) to [qārā'](../../strongs/h/h7122.md) upon them:

<a name="jeremiah_32_24"></a>Jeremiah 32:24

Behold the [sōllâ](../../strongs/h/h5550.md), they are [bow'](../../strongs/h/h935.md) unto the [ʿîr](../../strongs/h/h5892.md) to [lāḵaḏ](../../strongs/h/h3920.md) it; and the [ʿîr](../../strongs/h/h5892.md) is [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), that [lāḥam](../../strongs/h/h3898.md) against it, [paniym](../../strongs/h/h6440.md) of the [chereb](../../strongs/h/h2719.md), and of the [rāʿāḇ](../../strongs/h/h7458.md), and of the [deḇer](../../strongs/h/h1698.md): and what thou hast [dabar](../../strongs/h/h1696.md) is come to pass; and, behold, thou [ra'ah](../../strongs/h/h7200.md) it.

<a name="jeremiah_32_25"></a>Jeremiah 32:25

And thou hast ['āmar](../../strongs/h/h559.md) unto me, ['Adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md), [qānâ](../../strongs/h/h7069.md) thee the [sadeh](../../strongs/h/h7704.md) for [keceph](../../strongs/h/h3701.md), and [ʿûḏ](../../strongs/h/h5749.md) ['ed](../../strongs/h/h5707.md); for the [ʿîr](../../strongs/h/h5892.md) is [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="jeremiah_32_26"></a>Jeremiah 32:26

Then came the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_32_27"></a>Jeremiah 32:27

Behold, I am [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of all [basar](../../strongs/h/h1320.md): is there any [dabar](../../strongs/h/h1697.md) too [pala'](../../strongs/h/h6381.md) for me?

<a name="jeremiah_32_28"></a>Jeremiah 32:28

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [nathan](../../strongs/h/h5414.md) this [ʿîr](../../strongs/h/h5892.md) into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [lāḵaḏ](../../strongs/h/h3920.md) it:

<a name="jeremiah_32_29"></a>Jeremiah 32:29

And the [Kaśdîmâ](../../strongs/h/h3778.md), that [lāḥam](../../strongs/h/h3898.md) against this [ʿîr](../../strongs/h/h5892.md), shall [bow'](../../strongs/h/h935.md) and [yāṣaṯ](../../strongs/h/h3341.md) ['esh](../../strongs/h/h784.md) on this [ʿîr](../../strongs/h/h5892.md), and [śārap̄](../../strongs/h/h8313.md) it with the [bayith](../../strongs/h/h1004.md), upon whose [gāḡ](../../strongs/h/h1406.md) they have offered [qāṭar](../../strongs/h/h6999.md) unto [BaʿAl](../../strongs/h/h1168.md), and [nacak](../../strongs/h/h5258.md) [necek](../../strongs/h/h5262.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), to provoke me to [kāʿas](../../strongs/h/h3707.md).

<a name="jeremiah_32_30"></a>Jeremiah 32:30

For the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) have only ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) ['ayin](../../strongs/h/h5869.md) me from their [nāʿur](../../strongs/h/h5271.md): for the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) have only provoked me to [kāʿas](../../strongs/h/h3707.md) with the [ma'aseh](../../strongs/h/h4639.md) of their [yad](../../strongs/h/h3027.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_32_31"></a>Jeremiah 32:31

For this [ʿîr](../../strongs/h/h5892.md) hath been to me as a provocation of mine ['aph](../../strongs/h/h639.md) and of my [chemah](../../strongs/h/h2534.md) from the [yowm](../../strongs/h/h3117.md) that they [bānâ](../../strongs/h/h1129.md) it even unto this [yowm](../../strongs/h/h3117.md); that I should [cuwr](../../strongs/h/h5493.md) it from before my [paniym](../../strongs/h/h6440.md),

<a name="jeremiah_32_32"></a>Jeremiah 32:32

Because of all the [ra'](../../strongs/h/h7451.md) of the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and of the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md), which they have ['asah](../../strongs/h/h6213.md) to provoke me to [kāʿas](../../strongs/h/h3707.md), they, their [melek](../../strongs/h/h4428.md), their [śar](../../strongs/h/h8269.md), their [kōhēn](../../strongs/h/h3548.md), and their [nāḇî'](../../strongs/h/h5030.md), and the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md).

<a name="jeremiah_32_33"></a>Jeremiah 32:33

And they have [panah](../../strongs/h/h6437.md) unto me the [ʿōrep̄](../../strongs/h/h6203.md), and not the [paniym](../../strongs/h/h6440.md): though I [lamad](../../strongs/h/h3925.md) them, rising up [šāḵam](../../strongs/h/h7925.md) and [lamad](../../strongs/h/h3925.md) them, yet they have not [shama'](../../strongs/h/h8085.md) to [laqach](../../strongs/h/h3947.md) [mûsār](../../strongs/h/h4148.md).

<a name="jeremiah_32_34"></a>Jeremiah 32:34

But they [śûm](../../strongs/h/h7760.md) their [šiqqûṣ](../../strongs/h/h8251.md) in the [bayith](../../strongs/h/h1004.md), which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), to [ṭāmē'](../../strongs/h/h2930.md) it.

<a name="jeremiah_32_35"></a>Jeremiah 32:35

And they [bānâ](../../strongs/h/h1129.md) the [bāmâ](../../strongs/h/h1116.md) of [BaʿAl](../../strongs/h/h1168.md), which are in the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), to cause their [ben](../../strongs/h/h1121.md) and their [bath](../../strongs/h/h1323.md) to ['abar](../../strongs/h/h5674.md) through the fire unto [mōleḵ](../../strongs/h/h4432.md); which I [tsavah](../../strongs/h/h6680.md) them not, neither [ʿālâ](../../strongs/h/h5927.md) it into my [leb](../../strongs/h/h3820.md), that they should ['asah](../../strongs/h/h6213.md) this [tôʿēḇâ](../../strongs/h/h8441.md), to cause [Yehuwdah](../../strongs/h/h3063.md) to [chata'](../../strongs/h/h2398.md).

<a name="jeremiah_32_36"></a>Jeremiah 32:36

And now therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), concerning this [ʿîr](../../strongs/h/h5892.md), whereof ye ['āmar](../../strongs/h/h559.md), It shall be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) by the [chereb](../../strongs/h/h2719.md), and by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md);

<a name="jeremiah_32_37"></a>Jeremiah 32:37

Behold, I will [qāḇaṣ](../../strongs/h/h6908.md) them of all ['erets](../../strongs/h/h776.md), whither I have [nāḏaḥ](../../strongs/h/h5080.md) them in mine ['aph](../../strongs/h/h639.md), and in my [chemah](../../strongs/h/h2534.md), and in [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md); and I will [shuwb](../../strongs/h/h7725.md) them unto this [maqowm](../../strongs/h/h4725.md), and I will cause them to [yashab](../../strongs/h/h3427.md) [betach](../../strongs/h/h983.md):

<a name="jeremiah_32_38"></a>Jeremiah 32:38

And they shall be my ['am](../../strongs/h/h5971.md), and I will be their ['Elohiym](../../strongs/h/h430.md):

<a name="jeremiah_32_39"></a>Jeremiah 32:39

And I will [nathan](../../strongs/h/h5414.md) them ['echad](../../strongs/h/h259.md) [leb](../../strongs/h/h3820.md), and ['echad](../../strongs/h/h259.md) [derek](../../strongs/h/h1870.md), that they may [yare'](../../strongs/h/h3372.md) me for [yowm](../../strongs/h/h3117.md), for the [towb](../../strongs/h/h2896.md) of them, and of their [ben](../../strongs/h/h1121.md) ['aḥar](../../strongs/h/h310.md) them:

<a name="jeremiah_32_40"></a>Jeremiah 32:40

And I will [karath](../../strongs/h/h3772.md) an ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) with them, that I will not [shuwb](../../strongs/h/h7725.md) from ['aḥar](../../strongs/h/h310.md), to do them [yatab](../../strongs/h/h3190.md); but I will [nathan](../../strongs/h/h5414.md) my [yir'ah](../../strongs/h/h3374.md) in their [lebab](../../strongs/h/h3824.md), that they shall not [cuwr](../../strongs/h/h5493.md) from me.

<a name="jeremiah_32_41"></a>Jeremiah 32:41

Yea, I will [śûś](../../strongs/h/h7797.md) over them to do them [ṭôḇ](../../strongs/h/h2895.md), and I will [nāṭaʿ](../../strongs/h/h5193.md) them in this ['erets](../../strongs/h/h776.md) ['emeth](../../strongs/h/h571.md) with my whole [leb](../../strongs/h/h3820.md) and with my whole [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_32_42"></a>Jeremiah 32:42

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Like as I have [bow'](../../strongs/h/h935.md) all this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md) upon this ['am](../../strongs/h/h5971.md), so will I [bow'](../../strongs/h/h935.md) upon them all the [towb](../../strongs/h/h2896.md) that I have [dabar](../../strongs/h/h1696.md) them.

<a name="jeremiah_32_43"></a>Jeremiah 32:43

And [sadeh](../../strongs/h/h7704.md) shall be [qānâ](../../strongs/h/h7069.md) in this ['erets](../../strongs/h/h776.md), whereof ye ['āmar](../../strongs/h/h559.md), It is [šᵊmāmâ](../../strongs/h/h8077.md) without ['āḏām](../../strongs/h/h120.md) or [bĕhemah](../../strongs/h/h929.md); it is [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="jeremiah_32_44"></a>Jeremiah 32:44

Men shall [qānâ](../../strongs/h/h7069.md) [sadeh](../../strongs/h/h7704.md) for [keceph](../../strongs/h/h3701.md), and [kāṯaḇ](../../strongs/h/h3789.md) [sēp̄er](../../strongs/h/h5612.md), and [ḥāṯam](../../strongs/h/h2856.md) them, and [ʿûḏ](../../strongs/h/h5749.md) ['ed](../../strongs/h/h5707.md) in the ['erets](../../strongs/h/h776.md) of [Binyāmîn](../../strongs/h/h1144.md), and in the places [cabiyb](../../strongs/h/h5439.md) [Yĕruwshalaim](../../strongs/h/h3389.md), and in the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [har](../../strongs/h/h2022.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [šᵊp̄ēlâ](../../strongs/h/h8219.md), and in the [ʿîr](../../strongs/h/h5892.md) of the [neḡeḇ](../../strongs/h/h5045.md): for I will cause their [shebuwth](../../strongs/h/h7622.md) to [shuwb](../../strongs/h/h7725.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 31](jeremiah_31.md) - [Jeremiah 33](jeremiah_33.md)