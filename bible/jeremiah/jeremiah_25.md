# [Jeremiah 25](https://www.blueletterbible.org/kjv/jeremiah/25)

<a name="jeremiah_25_1"></a>Jeremiah 25:1

The [dabar](../../strongs/h/h1697.md) that came to [Yirmᵊyâ](../../strongs/h/h3414.md) concerning all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md) in the [rᵊḇîʿî](../../strongs/h/h7243.md) [šānâ](../../strongs/h/h8141.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), that was the [ri'šōnî](../../strongs/h/h7224.md) [šānâ](../../strongs/h/h8141.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md);

<a name="jeremiah_25_2"></a>Jeremiah 25:2

The which [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [dabar](../../strongs/h/h1696.md) unto all the ['am](../../strongs/h/h5971.md) of [Yehuwdah](../../strongs/h/h3063.md), and to all the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_25_3"></a>Jeremiah 25:3

From the [šālôš](../../strongs/h/h7969.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) the [ben](../../strongs/h/h1121.md) of ['Āmôn](../../strongs/h/h526.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), even unto this [yowm](../../strongs/h/h3117.md), that is the [šālôš](../../strongs/h/h7969.md) and [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md), the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) hath come unto me, and I have [dabar](../../strongs/h/h1696.md) unto you, [šāḵam](../../strongs/h/h7925.md) and [dabar](../../strongs/h/h1696.md); but ye have not [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_25_4"></a>Jeremiah 25:4

And [Yĕhovah](../../strongs/h/h3068.md) hath [shalach](../../strongs/h/h7971.md) unto you all his ['ebed](../../strongs/h/h5650.md) the [nāḇî'](../../strongs/h/h5030.md), rising [šāḵam](../../strongs/h/h7925.md) and [shalach](../../strongs/h/h7971.md) them; but ye have not [shama'](../../strongs/h/h8085.md), nor [natah](../../strongs/h/h5186.md) your ['ozen](../../strongs/h/h241.md) to [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_25_5"></a>Jeremiah 25:5

They ['āmar](../../strongs/h/h559.md), [shuwb](../../strongs/h/h7725.md) ye now every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and from the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md), and [yashab](../../strongs/h/h3427.md) in the ['ăḏāmâ](../../strongs/h/h127.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [nathan](../../strongs/h/h5414.md) unto you and to your ['ab](../../strongs/h/h1.md) [ʿaḏ](../../strongs/h/h5704.md) ['owlam](../../strongs/h/h5769.md) and ['owlam](../../strongs/h/h5769.md):

<a name="jeremiah_25_6"></a>Jeremiah 25:6

And [yālaḵ](../../strongs/h/h3212.md) not ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) to ['abad](../../strongs/h/h5647.md) them, and to [shachah](../../strongs/h/h7812.md) them, and provoke me not to [kāʿas](../../strongs/h/h3707.md) with the [ma'aseh](../../strongs/h/h4639.md) of your [yad](../../strongs/h/h3027.md); and I will do you no [ra'a'](../../strongs/h/h7489.md).

<a name="jeremiah_25_7"></a>Jeremiah 25:7

Yet ye have not [shama'](../../strongs/h/h8085.md) unto me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); that ye might provoke me to [kāʿas](../../strongs/h/h3707.md) with the [ma'aseh](../../strongs/h/h4639.md) of your [yad](../../strongs/h/h3027.md) to your own [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_25_8"></a>Jeremiah 25:8

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Because ye have not [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md),

<a name="jeremiah_25_9"></a>Jeremiah 25:9

Behold, I will [shalach](../../strongs/h/h7971.md) and [laqach](../../strongs/h/h3947.md) all the [mišpāḥâ](../../strongs/h/h4940.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), my ['ebed](../../strongs/h/h5650.md), and will [bow'](../../strongs/h/h935.md) them against this ['erets](../../strongs/h/h776.md), and against the [yashab](../../strongs/h/h3427.md) thereof, and against all these [gowy](../../strongs/h/h1471.md) [cabiyb](../../strongs/h/h5439.md), and will [ḥāram](../../strongs/h/h2763.md) them, and [śûm](../../strongs/h/h7760.md) them a [šammâ](../../strongs/h/h8047.md), and a [šᵊrēqâ](../../strongs/h/h8322.md), and ['owlam](../../strongs/h/h5769.md) [chorbah](../../strongs/h/h2723.md).

<a name="jeremiah_25_10"></a>Jeremiah 25:10

Moreover I will ['abad](../../strongs/h/h6.md) from them the [qowl](../../strongs/h/h6963.md) of [śāśôn](../../strongs/h/h8342.md), and the [qowl](../../strongs/h/h6963.md) of [simchah](../../strongs/h/h8057.md), the [qowl](../../strongs/h/h6963.md) of the [ḥāṯān](../../strongs/h/h2860.md), and the [qowl](../../strongs/h/h6963.md) of the [kallâ](../../strongs/h/h3618.md), the [qowl](../../strongs/h/h6963.md) of the [rēḥayim](../../strongs/h/h7347.md), and the ['owr](../../strongs/h/h216.md) of the [nîr](../../strongs/h/h5216.md).

<a name="jeremiah_25_11"></a>Jeremiah 25:11

And this ['erets](../../strongs/h/h776.md) shall be a [chorbah](../../strongs/h/h2723.md), and a [šammâ](../../strongs/h/h8047.md); and these [gowy](../../strongs/h/h1471.md) shall ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md).

<a name="jeremiah_25_12"></a>Jeremiah 25:12

And it shall come to pass, when [šiḇʿîm](../../strongs/h/h7657.md) [šānâ](../../strongs/h/h8141.md) are [mālā'](../../strongs/h/h4390.md), that I will [paqad](../../strongs/h/h6485.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and that [gowy](../../strongs/h/h1471.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), for their ['avon](../../strongs/h/h5771.md), and the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and will [śûm](../../strongs/h/h7760.md) it ['owlam](../../strongs/h/h5769.md) [šᵊmāmâ](../../strongs/h/h8077.md).

<a name="jeremiah_25_13"></a>Jeremiah 25:13

And I will [bow'](../../strongs/h/h935.md) upon that ['erets](../../strongs/h/h776.md) all my [dabar](../../strongs/h/h1697.md) which I have [dabar](../../strongs/h/h1696.md) against it, even all that is [kāṯaḇ](../../strongs/h/h3789.md) in this [sēp̄er](../../strongs/h/h5612.md), which [Yirmᵊyâ](../../strongs/h/h3414.md) hath [nāḇā'](../../strongs/h/h5012.md) against all the [gowy](../../strongs/h/h1471.md).

<a name="jeremiah_25_14"></a>Jeremiah 25:14

For [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) and [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md) shall ['abad](../../strongs/h/h5647.md) themselves of them also: and I will [shalam](../../strongs/h/h7999.md) them according to their [pōʿal](../../strongs/h/h6467.md), and according to the [ma'aseh](../../strongs/h/h4639.md) of their own [yad](../../strongs/h/h3027.md).

<a name="jeremiah_25_15"></a>Jeremiah 25:15

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md) unto me; [laqach](../../strongs/h/h3947.md) the [yayin](../../strongs/h/h3196.md) [kowc](../../strongs/h/h3563.md) of this [chemah](../../strongs/h/h2534.md) at my [yad](../../strongs/h/h3027.md), and cause all the [gowy](../../strongs/h/h1471.md), to whom I [shalach](../../strongs/h/h7971.md) thee, to [šāqâ](../../strongs/h/h8248.md) it.

<a name="jeremiah_25_16"></a>Jeremiah 25:16

And they shall [šāṯâ](../../strongs/h/h8354.md), and be [gāʿaš](../../strongs/h/h1607.md), and be [halal](../../strongs/h/h1984.md), [paniym](../../strongs/h/h6440.md) of the [chereb](../../strongs/h/h2719.md) that I will [shalach](../../strongs/h/h7971.md) among them.

<a name="jeremiah_25_17"></a>Jeremiah 25:17

Then [laqach](../../strongs/h/h3947.md) I the [kowc](../../strongs/h/h3563.md) at [Yĕhovah](../../strongs/h/h3068.md) [yad](../../strongs/h/h3027.md), and made all the [gowy](../../strongs/h/h1471.md) to [šāqâ](../../strongs/h/h8248.md), unto whom [Yĕhovah](../../strongs/h/h3068.md) had [shalach](../../strongs/h/h7971.md) me:

<a name="jeremiah_25_18"></a>Jeremiah 25:18

To wit, [Yĕruwshalaim](../../strongs/h/h3389.md), and the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [melek](../../strongs/h/h4428.md) thereof, and the [śar](../../strongs/h/h8269.md) thereof, to [nathan](../../strongs/h/h5414.md) them a [chorbah](../../strongs/h/h2723.md), a [šammâ](../../strongs/h/h8047.md), a [šᵊrēqâ](../../strongs/h/h8322.md), and a [qᵊlālâ](../../strongs/h/h7045.md); as it is this [yowm](../../strongs/h/h3117.md);

<a name="jeremiah_25_19"></a>Jeremiah 25:19

[Parʿô](../../strongs/h/h6547.md) [melek](../../strongs/h/h4428.md) of [Mitsrayim](../../strongs/h/h4714.md), and his ['ebed](../../strongs/h/h5650.md), and his [śar](../../strongs/h/h8269.md), and all his ['am](../../strongs/h/h5971.md);

<a name="jeremiah_25_20"></a>Jeremiah 25:20

And all the [ʿēreḇ](../../strongs/h/h6154.md) people, and all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) of [ʿÛṣ](../../strongs/h/h5780.md), and all the [melek](../../strongs/h/h4428.md) of the ['erets](../../strongs/h/h776.md) of the [Pᵊlištî](../../strongs/h/h6430.md), and ['Ašqᵊlôn](../../strongs/h/h831.md), and [ʿAzzâ](../../strongs/h/h5804.md), and [ʿEqrôn](../../strongs/h/h6138.md), and the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of ['Ašdôḏ](../../strongs/h/h795.md),

<a name="jeremiah_25_21"></a>Jeremiah 25:21

['Ĕḏōm](../../strongs/h/h123.md), and [Mô'āḇ](../../strongs/h/h4124.md), and the [ben](../../strongs/h/h1121.md) of [ʿAmmôn](../../strongs/h/h5983.md),

<a name="jeremiah_25_22"></a>Jeremiah 25:22

And all the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md), and all the [melek](../../strongs/h/h4428.md) of [Ṣîḏôn](../../strongs/h/h6721.md), and the [melek](../../strongs/h/h4428.md) of the ['î](../../strongs/h/h339.md) which are [ʿēḇer](../../strongs/h/h5676.md) the [yam](../../strongs/h/h3220.md),

<a name="jeremiah_25_23"></a>Jeremiah 25:23

[Dᵊḏān](../../strongs/h/h1719.md), and [Têmā'](../../strongs/h/h8485.md), and [Bûz](../../strongs/h/h938.md), and all that are in the [qāṣaṣ](../../strongs/h/h7112.md) [pē'â](../../strongs/h/h6285.md),

<a name="jeremiah_25_24"></a>Jeremiah 25:24

And all the [melek](../../strongs/h/h4428.md) of [ʿĂrāḇ](../../strongs/h/h6152.md), and all the [melek](../../strongs/h/h4428.md) of the [ʿēreḇ](../../strongs/h/h6154.md) that [shakan](../../strongs/h/h7931.md) in the [midbar](../../strongs/h/h4057.md),

<a name="jeremiah_25_25"></a>Jeremiah 25:25

And all the [melek](../../strongs/h/h4428.md) of [Zimrî](../../strongs/h/h2174.md), and all the [melek](../../strongs/h/h4428.md) of [ʿÊlām](../../strongs/h/h5867.md), and all the [melek](../../strongs/h/h4428.md) of the [Māḏay](../../strongs/h/h4074.md),

<a name="jeremiah_25_26"></a>Jeremiah 25:26

And all the [melek](../../strongs/h/h4428.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), [rachowq](../../strongs/h/h7350.md) and [qarowb](../../strongs/h/h7138.md), ['iysh](../../strongs/h/h376.md) with ['ach](../../strongs/h/h251.md), and all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md), which are upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md): and the [melek](../../strongs/h/h4428.md) of [Šēšaḵ](../../strongs/h/h8347.md) shall [šāṯâ](../../strongs/h/h8354.md) ['aḥar](../../strongs/h/h310.md) them.

<a name="jeremiah_25_27"></a>Jeremiah 25:27

Therefore thou shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [šāṯâ](../../strongs/h/h8354.md) ye, and be [šāḵar](../../strongs/h/h7937.md), and [qāyā'](../../strongs/h/h7006.md), and [naphal](../../strongs/h/h5307.md), and [quwm](../../strongs/h/h6965.md) no more, [paniym](../../strongs/h/h6440.md) of the [chereb](../../strongs/h/h2719.md) which I will [shalach](../../strongs/h/h7971.md) among you.

<a name="jeremiah_25_28"></a>Jeremiah 25:28

And it shall be, if they [mā'ēn](../../strongs/h/h3985.md) to [laqach](../../strongs/h/h3947.md) the [kowc](../../strongs/h/h3563.md) at thine [yad](../../strongs/h/h3027.md) to [šāṯâ](../../strongs/h/h8354.md), then shalt thou ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Ye shall [šāṯâ](../../strongs/h/h8354.md) [šāṯâ](../../strongs/h/h8354.md).

<a name="jeremiah_25_29"></a>Jeremiah 25:29

For, lo, I [ḥālal](../../strongs/h/h2490.md) to bring [ra'a'](../../strongs/h/h7489.md) on the [ʿîr](../../strongs/h/h5892.md) which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md), and should ye be [naqah](../../strongs/h/h5352.md) [naqah](../../strongs/h/h5352.md)? Ye shall not be [naqah](../../strongs/h/h5352.md): for I will [qara'](../../strongs/h/h7121.md) for a [chereb](../../strongs/h/h2719.md) upon all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_25_30"></a>Jeremiah 25:30

Therefore [nāḇā'](../../strongs/h/h5012.md) thou against them all these [dabar](../../strongs/h/h1697.md), and ['āmar](../../strongs/h/h559.md) unto them, [Yĕhovah](../../strongs/h/h3068.md) shall [šā'aḡ](../../strongs/h/h7580.md) from on [marowm](../../strongs/h/h4791.md), and [nathan](../../strongs/h/h5414.md) his [qowl](../../strongs/h/h6963.md) from his [qodesh](../../strongs/h/h6944.md) [māʿôn](../../strongs/h/h4583.md); he shall [šā'aḡ](../../strongs/h/h7580.md) [šā'aḡ](../../strongs/h/h7580.md) upon his [nāvê](../../strongs/h/h5116.md); he shall ['anah](../../strongs/h/h6030.md) a [hêḏāḏ](../../strongs/h/h1959.md), as they that [dāraḵ](../../strongs/h/h1869.md) the grapes, against all the [yashab](../../strongs/h/h3427.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_25_31"></a>Jeremiah 25:31

A [shā'ôn](../../strongs/h/h7588.md) shall [bow'](../../strongs/h/h935.md) even to the [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md); for [Yĕhovah](../../strongs/h/h3068.md) hath a [rîḇ](../../strongs/h/h7379.md) with the [gowy](../../strongs/h/h1471.md), he will [shaphat](../../strongs/h/h8199.md) with all [basar](../../strongs/h/h1320.md); he will [nathan](../../strongs/h/h5414.md) them that are [rasha'](../../strongs/h/h7563.md) to the [chereb](../../strongs/h/h2719.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_25_32"></a>Jeremiah 25:32

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), Behold, [ra'](../../strongs/h/h7451.md) shall [yāṣā'](../../strongs/h/h3318.md) from [gowy](../../strongs/h/h1471.md) to [gowy](../../strongs/h/h1471.md), and a [gadowl](../../strongs/h/h1419.md) [saʿar](../../strongs/h/h5591.md) shall be [ʿûr](../../strongs/h/h5782.md) from the [yᵊrēḵâ](../../strongs/h/h3411.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_25_33"></a>Jeremiah 25:33

And the [ḥālāl](../../strongs/h/h2491.md) of [Yĕhovah](../../strongs/h/h3068.md) shall be at that [yowm](../../strongs/h/h3117.md) from one [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) even unto the other [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md): they shall not be [sāp̄aḏ](../../strongs/h/h5594.md), neither ['āsap̄](../../strongs/h/h622.md), nor [qāḇar](../../strongs/h/h6912.md); they shall be [dōmen](../../strongs/h/h1828.md) [paniym](../../strongs/h/h6440.md) the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="jeremiah_25_34"></a>Jeremiah 25:34

[yālal](../../strongs/h/h3213.md), ye [ra'ah](../../strongs/h/h7462.md), and [zāʿaq](../../strongs/h/h2199.md); and [pālaš](../../strongs/h/h6428.md) yourselves in the ashes, ye ['addiyr](../../strongs/h/h117.md) of the [tso'n](../../strongs/h/h6629.md): for the [yowm](../../strongs/h/h3117.md) of your [ṭāḇaḥ](../../strongs/h/h2873.md) and of your [tᵊp̄ôṣâ](../../strongs/h/h8600.md) are [mālā'](../../strongs/h/h4390.md); and ye shall [naphal](../../strongs/h/h5307.md) like a [ḥemdâ](../../strongs/h/h2532.md) [kĕliy](../../strongs/h/h3627.md).

<a name="jeremiah_25_35"></a>Jeremiah 25:35

And the [ra'ah](../../strongs/h/h7462.md) shall have no [mānôs](../../strongs/h/h4498.md) to ['abad](../../strongs/h/h6.md), nor the ['addiyr](../../strongs/h/h117.md) of the [tso'n](../../strongs/h/h6629.md) to [pᵊlêṭâ](../../strongs/h/h6413.md).

<a name="jeremiah_25_36"></a>Jeremiah 25:36

A [qowl](../../strongs/h/h6963.md) of the [tsa'aqah](../../strongs/h/h6818.md) of the [ra'ah](../../strongs/h/h7462.md), and a [yᵊlālâ](../../strongs/h/h3215.md) of the ['addiyr](../../strongs/h/h117.md) of the [tso'n](../../strongs/h/h6629.md), shall be heard: for [Yĕhovah](../../strongs/h/h3068.md) hath [shadad](../../strongs/h/h7703.md) their [marʿîṯ](../../strongs/h/h4830.md).

<a name="jeremiah_25_37"></a>Jeremiah 25:37

And the [shalowm](../../strongs/h/h7965.md) [nā'â](../../strongs/h/h4999.md) are cut [damam](../../strongs/h/h1826.md) [paniym](../../strongs/h/h6440.md) of the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_25_38"></a>Jeremiah 25:38

He hath ['azab](../../strongs/h/h5800.md) his [sōḵ](../../strongs/h/h5520.md), as the [kephiyr](../../strongs/h/h3715.md): for their ['erets](../../strongs/h/h776.md) is [šammâ](../../strongs/h/h8047.md) [paniym](../../strongs/h/h6440.md) of the [charown](../../strongs/h/h2740.md) of the [yānâ](../../strongs/h/h3238.md), and [paniym](../../strongs/h/h6440.md) of his [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 24](jeremiah_24.md) - [Jeremiah 26](jeremiah_26.md)