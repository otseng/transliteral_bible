# [Jeremiah 34](https://www.blueletterbible.org/kjv/jeremiah/34)

<a name="jeremiah_34_1"></a>Jeremiah 34:1

The [dabar](../../strongs/h/h1697.md) which came unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), when [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and all his [ḥayil](../../strongs/h/h2428.md), and all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) of his [yad](../../strongs/h/h3027.md) [memshalah](../../strongs/h/h4475.md), and all the ['am](../../strongs/h/h5971.md), [lāḥam](../../strongs/h/h3898.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and against all the [ʿîr](../../strongs/h/h5892.md) thereof, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_34_2"></a>Jeremiah 34:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); [halak](../../strongs/h/h1980.md) and ['āmar](../../strongs/h/h559.md) to [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and ['āmar](../../strongs/h/h559.md) him, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I will [nathan](../../strongs/h/h5414.md) this [ʿîr](../../strongs/h/h5892.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md):

<a name="jeremiah_34_3"></a>Jeremiah 34:3

And thou shalt not [mālaṭ](../../strongs/h/h4422.md) of his [yad](../../strongs/h/h3027.md), but shalt [tāp̄aś](../../strongs/h/h8610.md) be [tāp̄aś](../../strongs/h/h8610.md), and [nathan](../../strongs/h/h5414.md) into his [yad](../../strongs/h/h3027.md); and thine ['ayin](../../strongs/h/h5869.md) shall [ra'ah](../../strongs/h/h7200.md) the ['ayin](../../strongs/h/h5869.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [dabar](../../strongs/h/h1696.md) with thee [peh](../../strongs/h/h6310.md) to [peh](../../strongs/h/h6310.md), and thou shalt [bow'](../../strongs/h/h935.md) to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_34_4"></a>Jeremiah 34:4

Yet [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of thee, Thou shalt not [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md):

<a name="jeremiah_34_5"></a>Jeremiah 34:5

But thou shalt [muwth](../../strongs/h/h4191.md) in [shalowm](../../strongs/h/h7965.md): and with the [miśrāp̄ôṯ](../../strongs/h/h4955.md) of thy ['ab](../../strongs/h/h1.md), the [ri'šôn](../../strongs/h/h7223.md) [melek](../../strongs/h/h4428.md) which were [paniym](../../strongs/h/h6440.md) thee, so shall they [śārap̄](../../strongs/h/h8313.md) odours for thee; and they will [sāp̄aḏ](../../strongs/h/h5594.md) thee, saying, [hôy](../../strongs/h/h1945.md) ['adown](../../strongs/h/h113.md)! for I have [dabar](../../strongs/h/h1696.md) the [dabar](../../strongs/h/h1697.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_34_6"></a>Jeremiah 34:6

Then [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) [dabar](../../strongs/h/h1696.md) all these [dabar](../../strongs/h/h1697.md) unto [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) in [Yĕruwshalaim](../../strongs/h/h3389.md),

<a name="jeremiah_34_7"></a>Jeremiah 34:7

When the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ḥayil](../../strongs/h/h2428.md) [lāḥam](../../strongs/h/h3898.md) against [Yĕruwshalaim](../../strongs/h/h3389.md), and against all the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) that were [yāṯar](../../strongs/h/h3498.md), against [Lāḵîš](../../strongs/h/h3923.md), and against [ʿĂzēqâ](../../strongs/h/h5825.md): for these [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md) [šā'ar](../../strongs/h/h7604.md) of the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_34_8"></a>Jeremiah 34:8

This is the [dabar](../../strongs/h/h1697.md) that came unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['aḥar](../../strongs/h/h310.md) that the [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md) had [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with all the ['am](../../strongs/h/h5971.md) which were at [Yĕruwshalaim](../../strongs/h/h3389.md), to [qara'](../../strongs/h/h7121.md) [dᵊrôr](../../strongs/h/h1865.md) unto them;

<a name="jeremiah_34_9"></a>Jeremiah 34:9

That every ['iysh](../../strongs/h/h376.md) should let his ['ebed](../../strongs/h/h5650.md), and every ['iysh](../../strongs/h/h376.md) his [šip̄ḥâ](../../strongs/h/h8198.md), being an [ʿiḇrî](../../strongs/h/h5680.md) or an [ʿiḇrî](../../strongs/h/h5680.md), [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md); that none should ['abad](../../strongs/h/h5647.md) himself of them, to wit, of a [Yᵊhûḏî](../../strongs/h/h3064.md) his ['ach](../../strongs/h/h251.md).

<a name="jeremiah_34_10"></a>Jeremiah 34:10

Now when all the [śar](../../strongs/h/h8269.md), and all the ['am](../../strongs/h/h5971.md), which had [bow'](../../strongs/h/h935.md) into the [bĕriyth](../../strongs/h/h1285.md), [shama'](../../strongs/h/h8085.md) that every ['iysh](../../strongs/h/h376.md) should let his ['ebed](../../strongs/h/h5650.md), and every ['iysh](../../strongs/h/h376.md) his [šip̄ḥâ](../../strongs/h/h8198.md), [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md), that none should ['abad](../../strongs/h/h5647.md) themselves of them any more, then they [shama'](../../strongs/h/h8085.md), and let them [shalach](../../strongs/h/h7971.md).

<a name="jeremiah_34_11"></a>Jeremiah 34:11

But ['aḥar](../../strongs/h/h310.md) they [shuwb](../../strongs/h/h7725.md), and caused the ['ebed](../../strongs/h/h5650.md) and the [šip̄ḥâ](../../strongs/h/h8198.md), whom they had let [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md), to [shuwb](../../strongs/h/h7725.md), and brought them into [kāḇaš](../../strongs/h/h3533.md) [kāḇaš](../../strongs/h/h3533.md) for ['ebed](../../strongs/h/h5650.md) and for [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="jeremiah_34_12"></a>Jeremiah 34:12

Therefore the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_34_13"></a>Jeremiah 34:13

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); I [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) with your ['ab](../../strongs/h/h1.md) in the [yowm](../../strongs/h/h3117.md) that I brought them [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), out of the [bayith](../../strongs/h/h1004.md) of ['ebed](../../strongs/h/h5650.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_34_14"></a>Jeremiah 34:14

At the [qēṣ](../../strongs/h/h7093.md) of [šeḇaʿ](../../strongs/h/h7651.md) [šānâ](../../strongs/h/h8141.md) let ye [shalach](../../strongs/h/h7971.md) every ['iysh](../../strongs/h/h376.md) his ['ach](../../strongs/h/h251.md) an [ʿiḇrî](../../strongs/h/h5680.md), which hath been [māḵar](../../strongs/h/h4376.md) unto thee; and when he hath ['abad](../../strongs/h/h5647.md) thee [šēš](../../strongs/h/h8337.md) [šānâ](../../strongs/h/h8141.md), thou shalt let him [shalach](../../strongs/h/h7971.md) [ḥāp̄šî](../../strongs/h/h2670.md) from thee: but your ['ab](../../strongs/h/h1.md) [shama'](../../strongs/h/h8085.md) not unto me, neither [natah](../../strongs/h/h5186.md) their ['ozen](../../strongs/h/h241.md).

<a name="jeremiah_34_15"></a>Jeremiah 34:15

And ye were [yowm](../../strongs/h/h3117.md) [shuwb](../../strongs/h/h7725.md), and had ['asah](../../strongs/h/h6213.md) [yashar](../../strongs/h/h3477.md) in my ['ayin](../../strongs/h/h5869.md), in [qara'](../../strongs/h/h7121.md) [dᵊrôr](../../strongs/h/h1865.md) every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md); and ye had [karath](../../strongs/h/h3772.md) a [bĕriyth](../../strongs/h/h1285.md) [paniym](../../strongs/h/h6440.md) me in the [bayith](../../strongs/h/h1004.md) which is [qara'](../../strongs/h/h7121.md) by my [shem](../../strongs/h/h8034.md):

<a name="jeremiah_34_16"></a>Jeremiah 34:16

But ye [shuwb](../../strongs/h/h7725.md) and [ḥālal](../../strongs/h/h2490.md) my [shem](../../strongs/h/h8034.md), and caused every ['iysh](../../strongs/h/h376.md) his ['ebed](../../strongs/h/h5650.md), and every ['iysh](../../strongs/h/h376.md) his [šip̄ḥâ](../../strongs/h/h8198.md), whom ye had [shalach](../../strongs/h/h7971.md) at [ḥāp̄šî](../../strongs/h/h2670.md) at their [nephesh](../../strongs/h/h5315.md), to [shuwb](../../strongs/h/h7725.md), and brought them into [kāḇaš](../../strongs/h/h3533.md), to be unto you for ['ebed](../../strongs/h/h5650.md) and for [šip̄ḥâ](../../strongs/h/h8198.md).

<a name="jeremiah_34_17"></a>Jeremiah 34:17

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Ye have not [shama'](../../strongs/h/h8085.md) unto me, in [qara'](../../strongs/h/h7121.md) [dᵊrôr](../../strongs/h/h1865.md), every ['iysh](../../strongs/h/h376.md) to his ['ach](../../strongs/h/h251.md), and every ['iysh](../../strongs/h/h376.md) to his [rea'](../../strongs/h/h7453.md): behold, I [qara'](../../strongs/h/h7121.md) a [dᵊrôr](../../strongs/h/h1865.md) for you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), to the [chereb](../../strongs/h/h2719.md), to the [deḇer](../../strongs/h/h1698.md), and to the [rāʿāḇ](../../strongs/h/h7458.md); and I will [nathan](../../strongs/h/h5414.md) you to be [zaʿăvâ](../../strongs/h/h2189.md) [zᵊvāʿâ](../../strongs/h/h2113.md) into all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_34_18"></a>Jeremiah 34:18

And I will [nathan](../../strongs/h/h5414.md) the ['enowsh](../../strongs/h/h582.md) that have ['abar](../../strongs/h/h5674.md) my [bĕriyth](../../strongs/h/h1285.md), which have not [quwm](../../strongs/h/h6965.md) the [dabar](../../strongs/h/h1697.md) of the [bĕriyth](../../strongs/h/h1285.md) which they had [karath](../../strongs/h/h3772.md) [paniym](../../strongs/h/h6440.md) me, when they [karath](../../strongs/h/h3772.md) the [ʿēḡel](../../strongs/h/h5695.md) in [šᵊnayim](../../strongs/h/h8147.md), and ['abar](../../strongs/h/h5674.md) between the [beṯer](../../strongs/h/h1335.md) thereof,

<a name="jeremiah_34_19"></a>Jeremiah 34:19

The [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [śar](../../strongs/h/h8269.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), the [sārîs](../../strongs/h/h5631.md), and the [kōhēn](../../strongs/h/h3548.md), and all the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), which ['abar](../../strongs/h/h5674.md) between the [beṯer](../../strongs/h/h1335.md) of the [ʿēḡel](../../strongs/h/h5695.md);

<a name="jeremiah_34_20"></a>Jeremiah 34:20

I will even [nathan](../../strongs/h/h5414.md) them into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md), and into the [yad](../../strongs/h/h3027.md) of them that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md): and their [nᵊḇēlâ](../../strongs/h/h5038.md) shall be for [ma'akal](../../strongs/h/h3978.md) unto the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and to the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_34_21"></a>Jeremiah 34:21

And [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and his [śar](../../strongs/h/h8269.md) will I [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md), and into the [yad](../../strongs/h/h3027.md) of them that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md), and into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ḥayil](../../strongs/h/h2428.md), which are [ʿālâ](../../strongs/h/h5927.md) from you.

<a name="jeremiah_34_22"></a>Jeremiah 34:22

Behold, I will [tsavah](../../strongs/h/h6680.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and cause them to [shuwb](../../strongs/h/h7725.md) to this [ʿîr](../../strongs/h/h5892.md); and they shall [lāḥam](../../strongs/h/h3898.md) against it, and [lāḵaḏ](../../strongs/h/h3920.md) it, and [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md): and I will [nathan](../../strongs/h/h5414.md) the [ʿîr](../../strongs/h/h5892.md) of [Yehuwdah](../../strongs/h/h3063.md) a [šᵊmāmâ](../../strongs/h/h8077.md) without a [yashab](../../strongs/h/h3427.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 33](jeremiah_33.md) - [Jeremiah 35](jeremiah_35.md)