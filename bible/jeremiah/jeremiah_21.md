# [Jeremiah 21](https://www.blueletterbible.org/kjv/jeremiah/21)

<a name="jeremiah_21_1"></a>Jeremiah 21:1

The [dabar](../../strongs/h/h1697.md) which came unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), when [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md) [shalach](../../strongs/h/h7971.md) unto him [Pašḥûr](../../strongs/h/h6583.md) the [ben](../../strongs/h/h1121.md) of [Malkîyâ](../../strongs/h/h4441.md), and [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [ben](../../strongs/h/h1121.md) of [MaʿĂśêâ](../../strongs/h/h4641.md) the [kōhēn](../../strongs/h/h3548.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_21_2"></a>Jeremiah 21:2

[darash](../../strongs/h/h1875.md), I pray thee, of [Yĕhovah](../../strongs/h/h3068.md) for [bᵊʿaḏ](../../strongs/h/h1157.md); for [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [lāḥam](../../strongs/h/h3898.md) against us; if so be that [Yĕhovah](../../strongs/h/h3068.md) will ['asah](../../strongs/h/h6213.md) with us according to all his [pala'](../../strongs/h/h6381.md), that he may [ʿālâ](../../strongs/h/h5927.md) from us.

<a name="jeremiah_21_3"></a>Jeremiah 21:3

Then ['āmar](../../strongs/h/h559.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto them, Thus shall ye ['āmar](../../strongs/h/h559.md) to [Ṣḏqyh](../../strongs/h/h6667.md):

<a name="jeremiah_21_4"></a>Jeremiah 21:4

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [cabab](../../strongs/h/h5437.md) the [kĕliy](../../strongs/h/h3627.md) of [milḥāmâ](../../strongs/h/h4421.md) that are in your [yad](../../strongs/h/h3027.md), wherewith ye [lāḥam](../../strongs/h/h3898.md) against the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and against the [Kaśdîmâ](../../strongs/h/h3778.md), which [ṣûr](../../strongs/h/h6696.md) you [ḥûṣ](../../strongs/h/h2351.md) the [ḥômâ](../../strongs/h/h2346.md), and I will ['āsap̄](../../strongs/h/h622.md) them into the [tavek](../../strongs/h/h8432.md) of this [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_21_5"></a>Jeremiah 21:5

And I myself will [lāḥam](../../strongs/h/h3898.md) against you with a [natah](../../strongs/h/h5186.md) [yad](../../strongs/h/h3027.md) and with a [ḥāzāq](../../strongs/h/h2389.md) [zerowa'](../../strongs/h/h2220.md), even in ['aph](../../strongs/h/h639.md), and in [chemah](../../strongs/h/h2534.md), and in [gadowl](../../strongs/h/h1419.md) [qeṣep̄](../../strongs/h/h7110.md).

<a name="jeremiah_21_6"></a>Jeremiah 21:6

And I will [nakah](../../strongs/h/h5221.md) the [yashab](../../strongs/h/h3427.md) of this [ʿîr](../../strongs/h/h5892.md), both ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md): they shall [muwth](../../strongs/h/h4191.md) of a [gadowl](../../strongs/h/h1419.md) [deḇer](../../strongs/h/h1698.md).

<a name="jeremiah_21_7"></a>Jeremiah 21:7

And ['aḥar](../../strongs/h/h310.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), I will [nathan](../../strongs/h/h5414.md) [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and his ['ebed](../../strongs/h/h5650.md), and the ['am](../../strongs/h/h5971.md), and such as are [šā'ar](../../strongs/h/h7604.md) in this [ʿîr](../../strongs/h/h5892.md) from the [deḇer](../../strongs/h/h1698.md), from the [chereb](../../strongs/h/h2719.md), and from the [rāʿāḇ](../../strongs/h/h7458.md), into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and into the [yad](../../strongs/h/h3027.md) of their ['oyeb](../../strongs/h/h341.md), and into the [yad](../../strongs/h/h3027.md) of those that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md): and he shall [nakah](../../strongs/h/h5221.md) them with the [peh](../../strongs/h/h6310.md) of the [chereb](../../strongs/h/h2719.md); he shall not [ḥûs](../../strongs/h/h2347.md) them, neither have [ḥāmal](../../strongs/h/h2550.md), nor have [racham](../../strongs/h/h7355.md).

<a name="jeremiah_21_8"></a>Jeremiah 21:8

And unto this ['am](../../strongs/h/h5971.md) thou shalt ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I [nathan](../../strongs/h/h5414.md) [paniym](../../strongs/h/h6440.md) you the [derek](../../strongs/h/h1870.md) of [chay](../../strongs/h/h2416.md), and the [derek](../../strongs/h/h1870.md) of [maveth](../../strongs/h/h4194.md).

<a name="jeremiah_21_9"></a>Jeremiah 21:9

He that [yashab](../../strongs/h/h3427.md) in this [ʿîr](../../strongs/h/h5892.md) shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), and by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md): but he that [yāṣā'](../../strongs/h/h3318.md), and [naphal](../../strongs/h/h5307.md) to the [Kaśdîmâ](../../strongs/h/h3778.md) that [ṣûr](../../strongs/h/h6696.md) you, he shall [ḥāyâ](../../strongs/h/h2421.md) [ḥāyâ](../../strongs/h/h2421.md), and his [nephesh](../../strongs/h/h5315.md) shall be unto him for a [šālāl](../../strongs/h/h7998.md).

<a name="jeremiah_21_10"></a>Jeremiah 21:10

For I have [śûm](../../strongs/h/h7760.md) my [paniym](../../strongs/h/h6440.md) against this [ʿîr](../../strongs/h/h5892.md) for [ra'](../../strongs/h/h7451.md), and not for [towb](../../strongs/h/h2896.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): it shall be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and he shall [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md).

<a name="jeremiah_21_11"></a>Jeremiah 21:11

And touching the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), say, [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md);

<a name="jeremiah_21_12"></a>Jeremiah 21:12

O [bayith](../../strongs/h/h1004.md) of [Dāviḏ](../../strongs/h/h1732.md), thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [diyn](../../strongs/h/h1777.md) [mishpat](../../strongs/h/h4941.md) in the [boqer](../../strongs/h/h1242.md), and [natsal](../../strongs/h/h5337.md) him that is [gāzal](../../strongs/h/h1497.md) out of the [yad](../../strongs/h/h3027.md) of the [ʿāšaq](../../strongs/h/h6231.md), lest my [chemah](../../strongs/h/h2534.md) [yāṣā'](../../strongs/h/h3318.md) like ['esh](../../strongs/h/h784.md), and [bāʿar](../../strongs/h/h1197.md) that none can [kāḇâ](../../strongs/h/h3518.md) it, [paniym](../../strongs/h/h6440.md) of the [rōaʿ](../../strongs/h/h7455.md) of your [maʿălāl](../../strongs/h/h4611.md).

<a name="jeremiah_21_13"></a>Jeremiah 21:13

Behold, I am against thee, O [yashab](../../strongs/h/h3427.md) of the [ʿēmeq](../../strongs/h/h6010.md), and [tsuwr](../../strongs/h/h6697.md) of the [mîšôr](../../strongs/h/h4334.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); which ['āmar](../../strongs/h/h559.md), Who shall [nāḥaṯ](../../strongs/h/h5181.md) against us? or who shall [bow'](../../strongs/h/h935.md) into our [mᵊʿônâ](../../strongs/h/h4585.md)?

<a name="jeremiah_21_14"></a>Jeremiah 21:14

But I will [paqad](../../strongs/h/h6485.md) you according to the [pĕriy](../../strongs/h/h6529.md) of your [maʿălāl](../../strongs/h/h4611.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): and I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in the [yaʿar](../../strongs/h/h3293.md) thereof, and it shall ['akal](../../strongs/h/h398.md) all things [cabiyb](../../strongs/h/h5439.md) it.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 20](jeremiah_20.md) - [Jeremiah 22](jeremiah_22.md)