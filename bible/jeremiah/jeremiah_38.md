# [Jeremiah 38](https://www.blueletterbible.org/kjv/jeremiah/38)

<a name="jeremiah_38_1"></a>Jeremiah 38:1

Then [Šᵊp̄Aṭyâ](../../strongs/h/h8203.md) the [ben](../../strongs/h/h1121.md) of [Matān](../../strongs/h/h4977.md), and [Gᵊḏalyâ](../../strongs/h/h1436.md) the [ben](../../strongs/h/h1121.md) of [Pašḥûr](../../strongs/h/h6583.md), and [Yûḵal](../../strongs/h/h3116.md) the [ben](../../strongs/h/h1121.md) of [Šelemyâ](../../strongs/h/h8018.md), and [Pašḥûr](../../strongs/h/h6583.md) the [ben](../../strongs/h/h1121.md) of [Malkîyâ](../../strongs/h/h4441.md), [shama'](../../strongs/h/h8085.md) the [dabar](../../strongs/h/h1697.md) that [Yirmᵊyâ](../../strongs/h/h3414.md) had [dabar](../../strongs/h/h1696.md) unto all the ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_38_2"></a>Jeremiah 38:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), He that [yashab](../../strongs/h/h3427.md) in this [ʿîr](../../strongs/h/h5892.md) shall [muwth](../../strongs/h/h4191.md) by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md): but he that [yāṣā'](../../strongs/h/h3318.md) to the [Kaśdîmâ](../../strongs/h/h3778.md) shall [ḥāyâ](../../strongs/h/h2421.md); for he shall have his [nephesh](../../strongs/h/h5315.md) for a [šālāl](../../strongs/h/h7998.md), and shall [ḥāyâ](../../strongs/h/h2421.md) [chayay](../../strongs/h/h2425.md).

<a name="jeremiah_38_3"></a>Jeremiah 38:3

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), This [ʿîr](../../strongs/h/h5892.md) shall [nathan](../../strongs/h/h5414.md) be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [ḥayil](../../strongs/h/h2428.md), which shall [lāḵaḏ](../../strongs/h/h3920.md) it.

<a name="jeremiah_38_4"></a>Jeremiah 38:4

Therefore the [śar](../../strongs/h/h8269.md) ['āmar](../../strongs/h/h559.md) unto the [melek](../../strongs/h/h4428.md), We beseech thee, let this ['iysh](../../strongs/h/h376.md) be put to [muwth](../../strongs/h/h4191.md): for [kēn](../../strongs/h/h3651.md) he [rāp̄â](../../strongs/h/h7503.md) the [yad](../../strongs/h/h3027.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) that [šā'ar](../../strongs/h/h7604.md) in this [ʿîr](../../strongs/h/h5892.md), and the [yad](../../strongs/h/h3027.md) of all the ['am](../../strongs/h/h5971.md), in [dabar](../../strongs/h/h1696.md) such [dabar](../../strongs/h/h1697.md) unto them: for this ['iysh](../../strongs/h/h376.md) [darash](../../strongs/h/h1875.md) not the [shalowm](../../strongs/h/h7965.md) of this ['am](../../strongs/h/h5971.md), but the [ra'](../../strongs/h/h7451.md).

<a name="jeremiah_38_5"></a>Jeremiah 38:5

Then [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md), Behold, he is in your [yad](../../strongs/h/h3027.md): for the [melek](../../strongs/h/h4428.md) is not he that [yakol](../../strongs/h/h3201.md) do any [dabar](../../strongs/h/h1697.md) against you.

<a name="jeremiah_38_6"></a>Jeremiah 38:6

Then [laqach](../../strongs/h/h3947.md) they [Yirmᵊyâ](../../strongs/h/h3414.md), and [shalak](../../strongs/h/h7993.md) him into the [bowr](../../strongs/h/h953.md) of [Malkîyâ](../../strongs/h/h4441.md) the [ben](../../strongs/h/h1121.md) of [melek](../../strongs/h/h4428.md), that was in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md): and they let [shalach](../../strongs/h/h7971.md) [Yirmᵊyâ](../../strongs/h/h3414.md) with [chebel](../../strongs/h/h2256.md). And in the [bowr](../../strongs/h/h953.md) there was no [mayim](../../strongs/h/h4325.md), but [ṭîṭ](../../strongs/h/h2916.md): so [Yirmᵊyâ](../../strongs/h/h3414.md) [ṭāḇaʿ](../../strongs/h/h2883.md) in the [ṭîṭ](../../strongs/h/h2916.md).

<a name="jeremiah_38_7"></a>Jeremiah 38:7

Now when [ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) the [Kûšî](../../strongs/h/h3569.md), ['iysh](../../strongs/h/h376.md) of the [sārîs](../../strongs/h/h5631.md) which was in the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), [shama'](../../strongs/h/h8085.md) that they had [nathan](../../strongs/h/h5414.md) [Yirmᵊyâ](../../strongs/h/h3414.md) in the [bowr](../../strongs/h/h953.md); the [melek](../../strongs/h/h4428.md) then [yashab](../../strongs/h/h3427.md) in the [sha'ar](../../strongs/h/h8179.md) of [Binyāmîn](../../strongs/h/h1144.md);

<a name="jeremiah_38_8"></a>Jeremiah 38:8

[ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) [yāṣā'](../../strongs/h/h3318.md) out of the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md), and [dabar](../../strongs/h/h1696.md) to the [melek](../../strongs/h/h4428.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_38_9"></a>Jeremiah 38:9

My ['adown](../../strongs/h/h113.md) the [melek](../../strongs/h/h4428.md), these ['enowsh](../../strongs/h/h582.md) have done [ra'a'](../../strongs/h/h7489.md) in all that they have ['asah](../../strongs/h/h6213.md) to [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md), whom they have [shalak](../../strongs/h/h7993.md) into the [bowr](../../strongs/h/h953.md); and he is like to [muwth](../../strongs/h/h4191.md) [paniym](../../strongs/h/h6440.md) [rāʿāḇ](../../strongs/h/h7458.md) in the [taḥaṯ](../../strongs/h/h8478.md) where he is: for there is no more [lechem](../../strongs/h/h3899.md) in the [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_38_10"></a>Jeremiah 38:10

Then the [melek](../../strongs/h/h4428.md) [tsavah](../../strongs/h/h6680.md) [ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) the [Kûšî](../../strongs/h/h3569.md), ['āmar](../../strongs/h/h559.md), [laqach](../../strongs/h/h3947.md) from hence [šᵊlōšîm](../../strongs/h/h7970.md) ['enowsh](../../strongs/h/h582.md) with [yad](../../strongs/h/h3027.md), and take [ʿālâ](../../strongs/h/h5927.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) out of the [bowr](../../strongs/h/h953.md), before he [muwth](../../strongs/h/h4191.md).

<a name="jeremiah_38_11"></a>Jeremiah 38:11

So [ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) [laqach](../../strongs/h/h3947.md) the ['enowsh](../../strongs/h/h582.md) with [yad](../../strongs/h/h3027.md), and [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) under the ['ôṣār](../../strongs/h/h214.md), and [laqach](../../strongs/h/h3947.md) thence [bᵊlô'](../../strongs/h/h1094.md) [sᵊḥāḇâ](../../strongs/h/h5499.md) and [bᵊlô'](../../strongs/h/h1094.md) rotten [melaḥ](../../strongs/h/h4418.md), and let them [shalach](../../strongs/h/h7971.md) by [chebel](../../strongs/h/h2256.md) into the [bowr](../../strongs/h/h953.md) to [Yirmᵊyâ](../../strongs/h/h3414.md).

<a name="jeremiah_38_12"></a>Jeremiah 38:12

And [ʿEḇeḏ Meleḵ](../../strongs/h/h5663.md) the [Kûšî](../../strongs/h/h3569.md) ['āmar](../../strongs/h/h559.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), [śûm](../../strongs/h/h7760.md) now these [bᵊlô'](../../strongs/h/h1094.md) cast [sᵊḥāḇâ](../../strongs/h/h5499.md) and [melaḥ](../../strongs/h/h4418.md) under thine ['aṣṣîl](../../strongs/h/h679.md) [yad](../../strongs/h/h3027.md) under the [chebel](../../strongs/h/h2256.md). And [Yirmᵊyâ](../../strongs/h/h3414.md) ['asah](../../strongs/h/h6213.md) so.

<a name="jeremiah_38_13"></a>Jeremiah 38:13

So they [mashak](../../strongs/h/h4900.md) [Yirmᵊyâ](../../strongs/h/h3414.md) with [chebel](../../strongs/h/h2256.md), and took him [ʿālâ](../../strongs/h/h5927.md) out of the [bowr](../../strongs/h/h953.md): and [Yirmᵊyâ](../../strongs/h/h3414.md) [yashab](../../strongs/h/h3427.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md).

<a name="jeremiah_38_14"></a>Jeremiah 38:14

Then [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) [shalach](../../strongs/h/h7971.md), and [laqach](../../strongs/h/h3947.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md) unto him into the [šᵊlîšî](../../strongs/h/h7992.md) [māḇô'](../../strongs/h/h3996.md) that is in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): and the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), I will [sha'al](../../strongs/h/h7592.md) thee a [dabar](../../strongs/h/h1697.md); [kāḥaḏ](../../strongs/h/h3582.md) nothing from me.

<a name="jeremiah_38_15"></a>Jeremiah 38:15

Then [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md) unto [Ṣḏqyh](../../strongs/h/h6667.md), If I [nāḡaḏ](../../strongs/h/h5046.md) it unto thee, wilt thou not [muwth](../../strongs/h/h4191.md) put me to [muwth](../../strongs/h/h4191.md)? and if I give thee [ya'ats](../../strongs/h/h3289.md), wilt thou not [shama'](../../strongs/h/h8085.md) unto me?

<a name="jeremiah_38_16"></a>Jeremiah 38:16

So [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) [shaba'](../../strongs/h/h7650.md) [cether](../../strongs/h/h5643.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), ['āmar](../../strongs/h/h559.md), As [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), that ['asah](../../strongs/h/h6213.md) us this [nephesh](../../strongs/h/h5315.md), I will not put thee to [muwth](../../strongs/h/h4191.md), neither will I [nathan](../../strongs/h/h5414.md) thee into the [yad](../../strongs/h/h3027.md) of these ['enowsh](../../strongs/h/h582.md) that [bāqaš](../../strongs/h/h1245.md) thy [nephesh](../../strongs/h/h5315.md).

<a name="jeremiah_38_17"></a>Jeremiah 38:17

Then ['āmar](../../strongs/h/h559.md) [Yirmᵊyâ](../../strongs/h/h3414.md) unto [Ṣḏqyh](../../strongs/h/h6667.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); If thou wilt [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) unto the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [śar](../../strongs/h/h8269.md), then thy [nephesh](../../strongs/h/h5315.md) shall [ḥāyâ](../../strongs/h/h2421.md), and this [ʿîr](../../strongs/h/h5892.md) shall not be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md); and thou shalt [ḥāyâ](../../strongs/h/h2421.md), and thine [bayith](../../strongs/h/h1004.md):

<a name="jeremiah_38_18"></a>Jeremiah 38:18

But if thou wilt not [yāṣā'](../../strongs/h/h3318.md) to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [śar](../../strongs/h/h8269.md), then shall this [ʿîr](../../strongs/h/h5892.md) be [nathan](../../strongs/h/h5414.md) into the [yad](../../strongs/h/h3027.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and they shall [śārap̄](../../strongs/h/h8313.md) it with ['esh](../../strongs/h/h784.md), and thou shalt not [mālaṭ](../../strongs/h/h4422.md) of their [yad](../../strongs/h/h3027.md).

<a name="jeremiah_38_19"></a>Jeremiah 38:19

And [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) ['āmar](../../strongs/h/h559.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), I am [dā'aḡ](../../strongs/h/h1672.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md) that are [naphal](../../strongs/h/h5307.md) to the [Kaśdîmâ](../../strongs/h/h3778.md), lest they [nathan](../../strongs/h/h5414.md) me into their [yad](../../strongs/h/h3027.md), and they [ʿālal](../../strongs/h/h5953.md) me.

<a name="jeremiah_38_20"></a>Jeremiah 38:20

But [Yirmᵊyâ](../../strongs/h/h3414.md) ['āmar](../../strongs/h/h559.md), They shall not [nathan](../../strongs/h/h5414.md) thee. [shama'](../../strongs/h/h8085.md), I beseech thee, the [qowl](../../strongs/h/h6963.md) of [Yĕhovah](../../strongs/h/h3068.md), which I [dabar](../../strongs/h/h1696.md) unto thee: so it shall be [yatab](../../strongs/h/h3190.md) unto thee, and thy [nephesh](../../strongs/h/h5315.md) shall [ḥāyâ](../../strongs/h/h2421.md).

<a name="jeremiah_38_21"></a>Jeremiah 38:21

But if thou [mā'ēn](../../strongs/h/h3986.md) to [yāṣā'](../../strongs/h/h3318.md), this is the [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) hath [ra'ah](../../strongs/h/h7200.md) me:

<a name="jeremiah_38_22"></a>Jeremiah 38:22

And, behold, all the ['ishshah](../../strongs/h/h802.md) that are [šā'ar](../../strongs/h/h7604.md) in the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) [bayith](../../strongs/h/h1004.md) shall be [yāṣā'](../../strongs/h/h3318.md) to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [śar](../../strongs/h/h8269.md), and those women shall ['āmar](../../strongs/h/h559.md), Thy ['enowsh](../../strongs/h/h582.md) [shalowm](../../strongs/h/h7965.md) have set thee [sûṯ](../../strongs/h/h5496.md), and have [yakol](../../strongs/h/h3201.md) against thee: thy [regel](../../strongs/h/h7272.md) are [ṭāḇaʿ](../../strongs/h/h2883.md) in the [bōṣ](../../strongs/h/h1206.md), and they are [sûḡ](../../strongs/h/h5472.md) ['āḥôr](../../strongs/h/h268.md).

<a name="jeremiah_38_23"></a>Jeremiah 38:23

So they shall [yāṣā'](../../strongs/h/h3318.md) all thy ['ishshah](../../strongs/h/h802.md) and thy [ben](../../strongs/h/h1121.md) to the [Kaśdîmâ](../../strongs/h/h3778.md): and thou shalt not [mālaṭ](../../strongs/h/h4422.md) of their [yad](../../strongs/h/h3027.md), but shalt be [tāp̄aś](../../strongs/h/h8610.md) by the [yad](../../strongs/h/h3027.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md): and thou shalt cause this [ʿîr](../../strongs/h/h5892.md) to be [śārap̄](../../strongs/h/h8313.md) with ['esh](../../strongs/h/h784.md).

<a name="jeremiah_38_24"></a>Jeremiah 38:24

Then ['āmar](../../strongs/h/h559.md) [Ṣḏqyh](../../strongs/h/h6667.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), Let no ['iysh](../../strongs/h/h376.md) [yada'](../../strongs/h/h3045.md) of these [dabar](../../strongs/h/h1697.md), and thou shalt not [muwth](../../strongs/h/h4191.md).

<a name="jeremiah_38_25"></a>Jeremiah 38:25

But if the [śar](../../strongs/h/h8269.md) [shama'](../../strongs/h/h8085.md) that I have [dabar](../../strongs/h/h1696.md) with thee, and they [bow'](../../strongs/h/h935.md) unto thee, and ['āmar](../../strongs/h/h559.md) unto thee, [nāḡaḏ](../../strongs/h/h5046.md) unto us now what thou hast [dabar](../../strongs/h/h1696.md) unto the [melek](../../strongs/h/h4428.md), [kāḥaḏ](../../strongs/h/h3582.md) it not from us, and we will not put thee to [muwth](../../strongs/h/h4191.md); also what the [melek](../../strongs/h/h4428.md) [dabar](../../strongs/h/h1696.md) unto thee:

<a name="jeremiah_38_26"></a>Jeremiah 38:26

Then thou shalt ['āmar](../../strongs/h/h559.md) unto them, I [naphal](../../strongs/h/h5307.md) my [tĕchinnah](../../strongs/h/h8467.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md), that he would not cause me to [shuwb](../../strongs/h/h7725.md) to [Yᵊhônāṯān](../../strongs/h/h3083.md) [bayith](../../strongs/h/h1004.md), to [muwth](../../strongs/h/h4191.md) there.

<a name="jeremiah_38_27"></a>Jeremiah 38:27

Then [bow'](../../strongs/h/h935.md) all the [śar](../../strongs/h/h8269.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md), and [sha'al](../../strongs/h/h7592.md) him: and he [nāḡaḏ](../../strongs/h/h5046.md) them according to all these [dabar](../../strongs/h/h1697.md) that the [melek](../../strongs/h/h4428.md) had [tsavah](../../strongs/h/h6680.md). So they left off [ḥāraš](../../strongs/h/h2790.md) with him; for the [dabar](../../strongs/h/h1697.md) was not [shama'](../../strongs/h/h8085.md).

<a name="jeremiah_38_28"></a>Jeremiah 38:28

So [Yirmᵊyâ](../../strongs/h/h3414.md) [yashab](../../strongs/h/h3427.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of the [maṭṭārâ](../../strongs/h/h4307.md) until the [yowm](../../strongs/h/h3117.md) that [Yĕruwshalaim](../../strongs/h/h3389.md) was [lāḵaḏ](../../strongs/h/h3920.md): and he was there when [Yĕruwshalaim](../../strongs/h/h3389.md) was [lāḵaḏ](../../strongs/h/h3920.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 37](jeremiah_37.md) - [Jeremiah 39](jeremiah_39.md)