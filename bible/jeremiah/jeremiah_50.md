# [Jeremiah 50](https://www.blueletterbible.org/kjv/jeremiah/50)

<a name="jeremiah_50_1"></a>Jeremiah 50:1

The [dabar](../../strongs/h/h1697.md) that [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) against [Bāḇel](../../strongs/h/h894.md) and against the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) [yad](../../strongs/h/h3027.md) [Yirmᵊyâ](../../strongs/h/h3414.md) the [nāḇî'](../../strongs/h/h5030.md).

<a name="jeremiah_50_2"></a>Jeremiah 50:2

[nāḡaḏ](../../strongs/h/h5046.md) ye among the [gowy](../../strongs/h/h1471.md), and [shama'](../../strongs/h/h8085.md), and set [nasa'](../../strongs/h/h5375.md) a [nēs](../../strongs/h/h5251.md); [shama'](../../strongs/h/h8085.md), and [kāḥaḏ](../../strongs/h/h3582.md) not: ['āmar](../../strongs/h/h559.md), [Bāḇel](../../strongs/h/h894.md) is [lāḵaḏ](../../strongs/h/h3920.md), [bēl](../../strongs/h/h1078.md) is [yāḇēš](../../strongs/h/h3001.md), [Mᵊrōḏaḵ](../../strongs/h/h4781.md) is broken in [ḥāṯaṯ](../../strongs/h/h2865.md); her [ʿāṣāḇ](../../strongs/h/h6091.md) are [yāḇēš](../../strongs/h/h3001.md), her [gillûl](../../strongs/h/h1544.md) are broken in [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="jeremiah_50_3"></a>Jeremiah 50:3

For out of the [ṣāp̄ôn](../../strongs/h/h6828.md) there [ʿālâ](../../strongs/h/h5927.md) a [gowy](../../strongs/h/h1471.md) against her, which shall [shiyth](../../strongs/h/h7896.md) her ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md), and none shall [yashab](../../strongs/h/h3427.md) therein: they shall [nuwd](../../strongs/h/h5110.md), they shall [halak](../../strongs/h/h1980.md), both ['āḏām](../../strongs/h/h120.md) and [bĕhemah](../../strongs/h/h929.md).

<a name="jeremiah_50_4"></a>Jeremiah 50:4

In those [yowm](../../strongs/h/h3117.md), and in that [ʿēṯ](../../strongs/h/h6256.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) shall [bow'](../../strongs/h/h935.md), they and the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) [yaḥaḏ](../../strongs/h/h3162.md), [halak](../../strongs/h/h1980.md) and [bāḵâ](../../strongs/h/h1058.md): they shall [yālaḵ](../../strongs/h/h3212.md), and [bāqaš](../../strongs/h/h1245.md) [Yĕhovah](../../strongs/h/h3068.md) their ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_50_5"></a>Jeremiah 50:5

They shall [sha'al](../../strongs/h/h7592.md) the [derek](../../strongs/h/h1870.md) to [Tsiyown](../../strongs/h/h6726.md) with their [paniym](../../strongs/h/h6440.md) [hēnnâ](../../strongs/h/h2008.md), saying, [bow'](../../strongs/h/h935.md), and let us [lāvâ](../../strongs/h/h3867.md) ourselves to [Yĕhovah](../../strongs/h/h3068.md) in a ['owlam](../../strongs/h/h5769.md) [bĕriyth](../../strongs/h/h1285.md) that shall not be [shakach](../../strongs/h/h7911.md).

<a name="jeremiah_50_6"></a>Jeremiah 50:6

My ['am](../../strongs/h/h5971.md) hath been ['abad](../../strongs/h/h6.md) [tso'n](../../strongs/h/h6629.md): their [ra'ah](../../strongs/h/h7462.md) have caused them to [tāʿâ](../../strongs/h/h8582.md), they have [shuwb](../../strongs/h/h7725.md) [šôḇāḇ](../../strongs/h/h7726.md) them on the [har](../../strongs/h/h2022.md): they have [halak](../../strongs/h/h1980.md) from [har](../../strongs/h/h2022.md) to [giḇʿâ](../../strongs/h/h1389.md), they have [shakach](../../strongs/h/h7911.md) their [rēḇeṣ](../../strongs/h/h7258.md).

<a name="jeremiah_50_7"></a>Jeremiah 50:7

All that [māṣā'](../../strongs/h/h4672.md) them have ['akal](../../strongs/h/h398.md) them: and their [tsar](../../strongs/h/h6862.md) ['āmar](../../strongs/h/h559.md), We ['asham](../../strongs/h/h816.md) not, because they have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md), the [nāvê](../../strongs/h/h5116.md) of [tsedeq](../../strongs/h/h6664.md), even [Yĕhovah](../../strongs/h/h3068.md), the [miqvê](../../strongs/h/h4723.md) of their ['ab](../../strongs/h/h1.md).

<a name="jeremiah_50_8"></a>Jeremiah 50:8

[nuwd](../../strongs/h/h5110.md) out of the [tavek](../../strongs/h/h8432.md) of [Bāḇel](../../strongs/h/h894.md), and [yāṣā'](../../strongs/h/h3318.md) [yāṣā'](../../strongs/h/h3318.md) out of the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), and be as the he [ʿatûḏ](../../strongs/h/h6260.md) [paniym](../../strongs/h/h6440.md) the [tso'n](../../strongs/h/h6629.md).

<a name="jeremiah_50_9"></a>Jeremiah 50:9

For, lo, I will [ʿûr](../../strongs/h/h5782.md) and cause to [ʿālâ](../../strongs/h/h5927.md) against [Bāḇel](../../strongs/h/h894.md) a [qāhēl](../../strongs/h/h6951.md) of [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md) ['erets](../../strongs/h/h776.md): and they shall set themselves in ['arak](../../strongs/h/h6186.md) against her; from thence she shall be [lāḵaḏ](../../strongs/h/h3920.md): their [chets](../../strongs/h/h2671.md) shall be as of a [gibôr](../../strongs/h/h1368.md) expert [sakal](../../strongs/h/h7919.md) [šāḵōl](../../strongs/h/h7921.md); none shall [shuwb](../../strongs/h/h7725.md) in [rêqām](../../strongs/h/h7387.md).

<a name="jeremiah_50_10"></a>Jeremiah 50:10

And [Kaśdîmâ](../../strongs/h/h3778.md) shall be a [šālāl](../../strongs/h/h7998.md): all that [šālal](../../strongs/h/h7997.md) her shall be [sāׂbaʿ](../../strongs/h/h7646.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_50_11"></a>Jeremiah 50:11

Because ye were [samach](../../strongs/h/h8055.md), because ye [ʿālaz](../../strongs/h/h5937.md), O ye [šāsâ](../../strongs/h/h8154.md) of mine [nachalah](../../strongs/h/h5159.md), because ye are [pûš](../../strongs/h/h6335.md) as the [ʿeḡlâ](../../strongs/h/h5697.md) at [deše'](../../strongs/h/h1877.md) [dûš](../../strongs/h/h1758.md), and [ṣāhal](../../strongs/h/h6670.md) as ['abîr](../../strongs/h/h47.md);

<a name="jeremiah_50_12"></a>Jeremiah 50:12

Your ['em](../../strongs/h/h517.md) shall be [me'od](../../strongs/h/h3966.md) [buwsh](../../strongs/h/h954.md); she that [yalad](../../strongs/h/h3205.md) you shall be [ḥāp̄ēr](../../strongs/h/h2659.md): behold, the ['aḥărîṯ](../../strongs/h/h319.md) of the [gowy](../../strongs/h/h1471.md) shall be a [midbar](../../strongs/h/h4057.md), a [ṣîyâ](../../strongs/h/h6723.md), and an ['arabah](../../strongs/h/h6160.md).

<a name="jeremiah_50_13"></a>Jeremiah 50:13

Because of the [qeṣep̄](../../strongs/h/h7110.md) of [Yĕhovah](../../strongs/h/h3068.md) it shall not be [yashab](../../strongs/h/h3427.md), but it shall be [šᵊmāmâ](../../strongs/h/h8077.md): every one that ['abar](../../strongs/h/h5674.md) by [Bāḇel](../../strongs/h/h894.md) shall be [šāmēm](../../strongs/h/h8074.md), and [šāraq](../../strongs/h/h8319.md) at all her [makâ](../../strongs/h/h4347.md).

<a name="jeremiah_50_14"></a>Jeremiah 50:14

Put yourselves in ['arak](../../strongs/h/h6186.md) against [Bāḇel](../../strongs/h/h894.md) [cabiyb](../../strongs/h/h5439.md): all ye that [dāraḵ](../../strongs/h/h1869.md) the [qesheth](../../strongs/h/h7198.md), [yadah](../../strongs/h/h3034.md) at her, [ḥāmal](../../strongs/h/h2550.md) no [chets](../../strongs/h/h2671.md): for she hath [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_50_15"></a>Jeremiah 50:15

[rûaʿ](../../strongs/h/h7321.md) against her [cabiyb](../../strongs/h/h5439.md): she hath [nathan](../../strongs/h/h5414.md) her [yad](../../strongs/h/h3027.md): her ['āšyâ](../../strongs/h/h803.md) are [naphal](../../strongs/h/h5307.md), her [ḥômâ](../../strongs/h/h2346.md) are [harac](../../strongs/h/h2040.md): for it is the [nᵊqāmâ](../../strongs/h/h5360.md) of [Yĕhovah](../../strongs/h/h3068.md): take [naqam](../../strongs/h/h5358.md) upon her; as she hath ['asah](../../strongs/h/h6213.md), ['asah](../../strongs/h/h6213.md) unto her.

<a name="jeremiah_50_16"></a>Jeremiah 50:16

[karath](../../strongs/h/h3772.md) the [zāraʿ](../../strongs/h/h2232.md) from [Bāḇel](../../strongs/h/h894.md), and him that [tāp̄aś](../../strongs/h/h8610.md) the [magāl](../../strongs/h/h4038.md) in the [ʿēṯ](../../strongs/h/h6256.md) of [qāṣîr](../../strongs/h/h7105.md): for [paniym](../../strongs/h/h6440.md) of the [yānâ](../../strongs/h/h3238.md) [chereb](../../strongs/h/h2719.md) they shall [panah](../../strongs/h/h6437.md) every ['iysh](../../strongs/h/h376.md) to his ['am](../../strongs/h/h5971.md), and they shall [nûs](../../strongs/h/h5127.md) every ['iysh](../../strongs/h/h376.md) to his own ['erets](../../strongs/h/h776.md).

<a name="jeremiah_50_17"></a>Jeremiah 50:17

[Yisra'el](../../strongs/h/h3478.md) is a [p̄zr](../../strongs/h/h6340.md) [śê](../../strongs/h/h7716.md); the ['ariy](../../strongs/h/h738.md) have [nāḏaḥ](../../strongs/h/h5080.md) him: [ri'šôn](../../strongs/h/h7223.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md) hath ['akal](../../strongs/h/h398.md) him; and ['aḥărôn](../../strongs/h/h314.md) this [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) hath [ʿāṣam](../../strongs/h/h6105.md).

<a name="jeremiah_50_18"></a>Jeremiah 50:18

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [paqad](../../strongs/h/h6485.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) and his ['erets](../../strongs/h/h776.md), as I have [paqad](../../strongs/h/h6485.md) the [melek](../../strongs/h/h4428.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="jeremiah_50_19"></a>Jeremiah 50:19

And I will [shuwb](../../strongs/h/h7725.md) [Yisra'el](../../strongs/h/h3478.md) [shuwb](../../strongs/h/h7725.md) to his [nāvê](../../strongs/h/h5116.md), and he shall [ra'ah](../../strongs/h/h7462.md) on [Karmel](../../strongs/h/h3760.md) and [Bāšān](../../strongs/h/h1316.md), and his [nephesh](../../strongs/h/h5315.md) shall be [sāׂbaʿ](../../strongs/h/h7646.md) upon [har](../../strongs/h/h2022.md) ['Ep̄rayim](../../strongs/h/h669.md) and [Gilʿāḏ](../../strongs/h/h1568.md).

<a name="jeremiah_50_20"></a>Jeremiah 50:20

In those [yowm](../../strongs/h/h3117.md), and in that [ʿēṯ](../../strongs/h/h6256.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), the ['avon](../../strongs/h/h5771.md) of [Yisra'el](../../strongs/h/h3478.md) shall be sought [bāqaš](../../strongs/h/h1245.md), and there shall be none; and the [chatta'ath](../../strongs/h/h2403.md) of [Yehuwdah](../../strongs/h/h3063.md), and they shall not be [māṣā'](../../strongs/h/h4672.md): for I will [sālaḥ](../../strongs/h/h5545.md) them whom I [šā'ar](../../strongs/h/h7604.md).

<a name="jeremiah_50_21"></a>Jeremiah 50:21

[ʿālâ](../../strongs/h/h5927.md) against the ['erets](../../strongs/h/h776.md) of [Mᵊrāṯayim](../../strongs/h/h4850.md), even against it, and against the [yashab](../../strongs/h/h3427.md) of [pᵊqôḏ](../../strongs/h/h6489.md): [ḥāraḇ](../../strongs/h/h2717.md) and [ḥāram](../../strongs/h/h2763.md) ['aḥar](../../strongs/h/h310.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and ['asah](../../strongs/h/h6213.md) according to all that I have [tsavah](../../strongs/h/h6680.md) thee.

<a name="jeremiah_50_22"></a>Jeremiah 50:22

A [qowl](../../strongs/h/h6963.md) of [milḥāmâ](../../strongs/h/h4421.md) is in the ['erets](../../strongs/h/h776.md), and of [gadowl](../../strongs/h/h1419.md) [šeḇar](../../strongs/h/h7667.md).

<a name="jeremiah_50_23"></a>Jeremiah 50:23

How is the [paṭṭîš](../../strongs/h/h6360.md) of the ['erets](../../strongs/h/h776.md) [gāḏaʿ](../../strongs/h/h1438.md) and [shabar](../../strongs/h/h7665.md)! how is [Bāḇel](../../strongs/h/h894.md) become a [šammâ](../../strongs/h/h8047.md) among the [gowy](../../strongs/h/h1471.md)!

<a name="jeremiah_50_24"></a>Jeremiah 50:24

I have laid a [yāqōš](../../strongs/h/h3369.md) for thee, and thou art also [lāḵaḏ](../../strongs/h/h3920.md), O [Bāḇel](../../strongs/h/h894.md), and thou wast not [yada'](../../strongs/h/h3045.md): thou art [māṣā'](../../strongs/h/h4672.md), and also [tāp̄aś](../../strongs/h/h8610.md), because thou hast [gārâ](../../strongs/h/h1624.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_50_25"></a>Jeremiah 50:25

[Yĕhovah](../../strongs/h/h3068.md) hath [pāṯaḥ](../../strongs/h/h6605.md) his ['ôṣār](../../strongs/h/h214.md), and hath [yāṣā'](../../strongs/h/h3318.md) the [kĕliy](../../strongs/h/h3627.md) of his [zaʿam](../../strongs/h/h2195.md): for this is the [mĕla'kah](../../strongs/h/h4399.md) of the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md) in the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md).

<a name="jeremiah_50_26"></a>Jeremiah 50:26

[bow'](../../strongs/h/h935.md) against her from the utmost [qēṣ](../../strongs/h/h7093.md), [pāṯaḥ](../../strongs/h/h6605.md) her [ma'ăḇûs](../../strongs/h/h3965.md): [sālal](../../strongs/h/h5549.md) her as [ʿărēmâ](../../strongs/h/h6194.md), and [ḥāram](../../strongs/h/h2763.md) her: let nothing of her be [šᵊ'ērîṯ](../../strongs/h/h7611.md).

<a name="jeremiah_50_27"></a>Jeremiah 50:27

[ḥāraḇ](../../strongs/h/h2717.md) all her [par](../../strongs/h/h6499.md); let them [yarad](../../strongs/h/h3381.md) to the [ṭeḇaḥ](../../strongs/h/h2874.md): [hôy](../../strongs/h/h1945.md) unto them! for their [yowm](../../strongs/h/h3117.md) is [bow'](../../strongs/h/h935.md), the [ʿēṯ](../../strongs/h/h6256.md) of their [pᵊqudâ](../../strongs/h/h6486.md).

<a name="jeremiah_50_28"></a>Jeremiah 50:28

The [qowl](../../strongs/h/h6963.md) of them that [nûs](../../strongs/h/h5127.md) and escape [pallēṭ](../../strongs/h/h6405.md) of the ['erets](../../strongs/h/h776.md) of [Bāḇel](../../strongs/h/h894.md), to [nāḡaḏ](../../strongs/h/h5046.md) in [Tsiyown](../../strongs/h/h6726.md) the [nᵊqāmâ](../../strongs/h/h5360.md) of [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md), the [nᵊqāmâ](../../strongs/h/h5360.md) of his [heykal](../../strongs/h/h1964.md).

<a name="jeremiah_50_29"></a>Jeremiah 50:29

[shama'](../../strongs/h/h8085.md) the [raḇ](../../strongs/h/h7228.md) against [Bāḇel](../../strongs/h/h894.md): all ye that [dāraḵ](../../strongs/h/h1869.md) the [qesheth](../../strongs/h/h7198.md), [ḥānâ](../../strongs/h/h2583.md) against it [cabiyb](../../strongs/h/h5439.md); let none thereof [pᵊlêṭâ](../../strongs/h/h6413.md): [shalam](../../strongs/h/h7999.md) her according to her [pōʿal](../../strongs/h/h6467.md); according to all that she hath ['asah](../../strongs/h/h6213.md), ['asah](../../strongs/h/h6213.md) unto her: for she hath been [zûḏ](../../strongs/h/h2102.md) against [Yĕhovah](../../strongs/h/h3068.md), against the [qadowsh](../../strongs/h/h6918.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_50_30"></a>Jeremiah 50:30

Therefore shall her [bāḥûr](../../strongs/h/h970.md) [naphal](../../strongs/h/h5307.md) in the [rᵊḥōḇ](../../strongs/h/h7339.md), and all her ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) shall be [damam](../../strongs/h/h1826.md) in that [yowm](../../strongs/h/h3117.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_50_31"></a>Jeremiah 50:31

Behold, I am against thee, O thou most [zāḏôn](../../strongs/h/h2087.md), [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md): for thy [yowm](../../strongs/h/h3117.md) is [bow'](../../strongs/h/h935.md), the [ʿēṯ](../../strongs/h/h6256.md) that I will [paqad](../../strongs/h/h6485.md) thee.

<a name="jeremiah_50_32"></a>Jeremiah 50:32

And the [zāḏôn](../../strongs/h/h2087.md) shall [kashal](../../strongs/h/h3782.md) and [naphal](../../strongs/h/h5307.md), and none shall [quwm](../../strongs/h/h6965.md) him: and I will [yāṣaṯ](../../strongs/h/h3341.md) an ['esh](../../strongs/h/h784.md) in his [ʿîr](../../strongs/h/h5892.md), and it shall ['akal](../../strongs/h/h398.md) all [cabiyb](../../strongs/h/h5439.md) him.

<a name="jeremiah_50_33"></a>Jeremiah 50:33

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); The [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) and the [ben](../../strongs/h/h1121.md) of [Yehuwdah](../../strongs/h/h3063.md) were [ʿāšaq](../../strongs/h/h6231.md) [yaḥaḏ](../../strongs/h/h3162.md): and all that took them [šāḇâ](../../strongs/h/h7617.md) held them [ḥāzaq](../../strongs/h/h2388.md); they [mā'ēn](../../strongs/h/h3985.md) to let them [shalach](../../strongs/h/h7971.md).

<a name="jeremiah_50_34"></a>Jeremiah 50:34

Their [gā'al](../../strongs/h/h1350.md) is [ḥāzāq](../../strongs/h/h2389.md); [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) is his [shem](../../strongs/h/h8034.md): he shall [riyb](../../strongs/h/h7378.md) [riyb](../../strongs/h/h7378.md) their [rîḇ](../../strongs/h/h7379.md), that he may [rāḡaʿ](../../strongs/h/h7280.md) to the ['erets](../../strongs/h/h776.md), and [ragaz](../../strongs/h/h7264.md) the [yashab](../../strongs/h/h3427.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_50_35"></a>Jeremiah 50:35

A [chereb](../../strongs/h/h2719.md) is upon the [Kaśdîmâ](../../strongs/h/h3778.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and upon the [yashab](../../strongs/h/h3427.md) of [Bāḇel](../../strongs/h/h894.md), and upon her [śar](../../strongs/h/h8269.md), and upon her [ḥāḵām](../../strongs/h/h2450.md) men.

<a name="jeremiah_50_36"></a>Jeremiah 50:36

A [chereb](../../strongs/h/h2719.md) is upon the [baḏ](../../strongs/h/h907.md); and they shall [yā'al](../../strongs/h/h2973.md): a [chereb](../../strongs/h/h2719.md) is upon her [gibôr](../../strongs/h/h1368.md); and they shall be [ḥāṯaṯ](../../strongs/h/h2865.md).

<a name="jeremiah_50_37"></a>Jeremiah 50:37

A [chereb](../../strongs/h/h2719.md) is upon their [sûs](../../strongs/h/h5483.md), and upon their [reḵeḇ](../../strongs/h/h7393.md), and upon all the [ʿēreḇ](../../strongs/h/h6154.md) that are in the [tavek](../../strongs/h/h8432.md) of her; and they shall become as ['ishshah](../../strongs/h/h802.md): a [chereb](../../strongs/h/h2719.md) is upon her ['ôṣār](../../strongs/h/h214.md); and they shall be [bāzaz](../../strongs/h/h962.md).

<a name="jeremiah_50_38"></a>Jeremiah 50:38

A [ḥōreḇ](../../strongs/h/h2721.md) is upon her [mayim](../../strongs/h/h4325.md); and they shall be [yāḇēš](../../strongs/h/h3001.md): for it is the ['erets](../../strongs/h/h776.md) of [pāsîl](../../strongs/h/h6456.md), and they are [halal](../../strongs/h/h1984.md) upon their ['êmâ](../../strongs/h/h367.md).

<a name="jeremiah_50_39"></a>Jeremiah 50:39

Therefore the [ṣîyî](../../strongs/h/h6728.md) with the ['î](../../strongs/h/h338.md) shall [yashab](../../strongs/h/h3427.md) there, and the [bath](../../strongs/h/h1323.md) [yaʿănâ](../../strongs/h/h3284.md) shall [yashab](../../strongs/h/h3427.md) therein: and it shall be no more [yashab](../../strongs/h/h3427.md) for [netsach](../../strongs/h/h5331.md); neither shall it be [shakan](../../strongs/h/h7931.md) in from [dôr](../../strongs/h/h1755.md) to [dôr](../../strongs/h/h1755.md).

<a name="jeremiah_50_40"></a>Jeremiah 50:40

As ['Elohiym](../../strongs/h/h430.md) [mahpēḵâ](../../strongs/h/h4114.md) [Sᵊḏōm](../../strongs/h/h5467.md) and [ʿĂmōrâ](../../strongs/h/h6017.md) and the [šāḵēn](../../strongs/h/h7934.md) cities thereof, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); so shall no ['iysh](../../strongs/h/h376.md) [yashab](../../strongs/h/h3427.md) there, neither shall any [ben](../../strongs/h/h1121.md) of ['āḏām](../../strongs/h/h120.md) [guwr](../../strongs/h/h1481.md) therein.

<a name="jeremiah_50_41"></a>Jeremiah 50:41

Behold, an ['am](../../strongs/h/h5971.md) shall [bow'](../../strongs/h/h935.md) from the [ṣāp̄ôn](../../strongs/h/h6828.md), and a [gadowl](../../strongs/h/h1419.md) [gowy](../../strongs/h/h1471.md), and [rab](../../strongs/h/h7227.md) [melek](../../strongs/h/h4428.md) shall be [ʿûr](../../strongs/h/h5782.md) from the [yᵊrēḵâ](../../strongs/h/h3411.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_50_42"></a>Jeremiah 50:42

They shall [ḥāzaq](../../strongs/h/h2388.md) the [qesheth](../../strongs/h/h7198.md) and the [kîḏôn](../../strongs/h/h3591.md): they are ['aḵzārî](../../strongs/h/h394.md), and will not shew [racham](../../strongs/h/h7355.md): their [qowl](../../strongs/h/h6963.md) shall [hāmâ](../../strongs/h/h1993.md) like the [yam](../../strongs/h/h3220.md), and they shall [rāḵaḇ](../../strongs/h/h7392.md) upon [sûs](../../strongs/h/h5483.md), every one put in ['arak](../../strongs/h/h6186.md), like an ['iysh](../../strongs/h/h376.md) to the [milḥāmâ](../../strongs/h/h4421.md), against thee, O [bath](../../strongs/h/h1323.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_50_43"></a>Jeremiah 50:43

The [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) hath [shama'](../../strongs/h/h8085.md) the [šēmaʿ](../../strongs/h/h8088.md) of them, and his [yad](../../strongs/h/h3027.md) [rāp̄â](../../strongs/h/h7503.md): [tsarah](../../strongs/h/h6869.md) took [ḥāzaq](../../strongs/h/h2388.md) of him, and [ḥîl](../../strongs/h/h2427.md) as of a [yalad](../../strongs/h/h3205.md).

<a name="jeremiah_50_44"></a>Jeremiah 50:44

Behold, he shall [ʿālâ](../../strongs/h/h5927.md) like an ['ariy](../../strongs/h/h738.md) from the [gā'ôn](../../strongs/h/h1347.md) of [Yardēn](../../strongs/h/h3383.md) unto the [nāvê](../../strongs/h/h5116.md) of the ['êṯān](../../strongs/h/h386.md): but I will [rûṣ](../../strongs/h/h7323.md) them [rāḡaʿ](../../strongs/h/h7280.md) run [rûṣ](../../strongs/h/h7323.md) [rûṣ](../../strongs/h/h7323.md) from her: and who is a [bāḥar](../../strongs/h/h977.md) man, that I may [paqad](../../strongs/h/h6485.md) over her? for who is like me? and who will [yāʿaḏ](../../strongs/h/h3259.md) me? and who is that [ra'ah](../../strongs/h/h7462.md) that will ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) me?

<a name="jeremiah_50_45"></a>Jeremiah 50:45

Therefore [shama'](../../strongs/h/h8085.md) ye the ['etsah](../../strongs/h/h6098.md) of [Yĕhovah](../../strongs/h/h3068.md), that he hath [ya'ats](../../strongs/h/h3289.md) against [Bāḇel](../../strongs/h/h894.md); and his [maḥăšāḇâ](../../strongs/h/h4284.md), that he hath [chashab](../../strongs/h/h2803.md) against the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md): Surely the [ṣāʿîr](../../strongs/h/h6810.md) of the [tso'n](../../strongs/h/h6629.md) shall [sāḥaḇ](../../strongs/h/h5498.md) them: surely he shall make their [nāvê](../../strongs/h/h5116.md) [šāmēm](../../strongs/h/h8074.md) with them.

<a name="jeremiah_50_46"></a>Jeremiah 50:46

At the [qowl](../../strongs/h/h6963.md) of the [tāp̄aś](../../strongs/h/h8610.md) of [Bāḇel](../../strongs/h/h894.md) the ['erets](../../strongs/h/h776.md) is [rāʿaš](../../strongs/h/h7493.md), and the [zaʿaq](../../strongs/h/h2201.md) is [shama'](../../strongs/h/h8085.md) among the [gowy](../../strongs/h/h1471.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 49](jeremiah_49.md) - [Jeremiah 51](jeremiah_51.md)