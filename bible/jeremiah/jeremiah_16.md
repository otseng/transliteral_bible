# [Jeremiah 16](https://www.blueletterbible.org/kjv/jeremiah/16)

<a name="jeremiah_16_1"></a>Jeremiah 16:1

The [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came also unto me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_16_2"></a>Jeremiah 16:2

Thou shalt not [laqach](../../strongs/h/h3947.md) thee an ['ishshah](../../strongs/h/h802.md), neither shalt thou have [ben](../../strongs/h/h1121.md) or [bath](../../strongs/h/h1323.md) in this [maqowm](../../strongs/h/h4725.md).

<a name="jeremiah_16_3"></a>Jeremiah 16:3

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) concerning the [ben](../../strongs/h/h1121.md) and concerning the [bath](../../strongs/h/h1323.md) that are [yalad](../../strongs/h/h3205.md) in this [maqowm](../../strongs/h/h4725.md), and concerning their ['em](../../strongs/h/h517.md) that [yalad](../../strongs/h/h3205.md) them, and concerning their ['ab](../../strongs/h/h1.md) that [yillôḏ](../../strongs/h/h3209.md) them in this ['erets](../../strongs/h/h776.md);

<a name="jeremiah_16_4"></a>Jeremiah 16:4

They shall [muwth](../../strongs/h/h4191.md) of [taḥălu'iym](../../strongs/h/h8463.md) [māmôṯ](../../strongs/h/h4463.md); they shall not be [sāp̄aḏ](../../strongs/h/h5594.md); neither shall they be [qāḇar](../../strongs/h/h6912.md); but they shall be as [dōmen](../../strongs/h/h1828.md) upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md): and they shall be [kalah](../../strongs/h/h3615.md) by the [chereb](../../strongs/h/h2719.md), and by [rāʿāḇ](../../strongs/h/h7458.md); and their [nᵊḇēlâ](../../strongs/h/h5038.md) shall be [ma'akal](../../strongs/h/h3978.md) for the [ʿôp̄](../../strongs/h/h5775.md) of [shamayim](../../strongs/h/h8064.md), and for the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_16_5"></a>Jeremiah 16:5

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [bow'](../../strongs/h/h935.md) not into the [bayith](../../strongs/h/h1004.md) of [marzēaḥ](../../strongs/h/h4798.md), neither [yālaḵ](../../strongs/h/h3212.md) to [sāp̄aḏ](../../strongs/h/h5594.md) nor [nuwd](../../strongs/h/h5110.md) them: for I have taken ['āsap̄](../../strongs/h/h622.md) my [shalowm](../../strongs/h/h7965.md) from this ['am](../../strongs/h/h5971.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), even [checed](../../strongs/h/h2617.md) and [raḥam](../../strongs/h/h7356.md).

<a name="jeremiah_16_6"></a>Jeremiah 16:6

Both the [gadowl](../../strongs/h/h1419.md) and the [qāṭān](../../strongs/h/h6996.md) shall [muwth](../../strongs/h/h4191.md) in this ['erets](../../strongs/h/h776.md): they shall not be [qāḇar](../../strongs/h/h6912.md), neither shall men [sāp̄aḏ](../../strongs/h/h5594.md) for them, nor [gāḏaḏ](../../strongs/h/h1413.md) themselves, nor make themselves [qāraḥ](../../strongs/h/h7139.md) for them:

<a name="jeremiah_16_7"></a>Jeremiah 16:7

Neither shall men [Pāras](../../strongs/h/h6536.md) themselves for them in ['ēḇel](../../strongs/h/h60.md), to [nacham](../../strongs/h/h5162.md) them for the [muwth](../../strongs/h/h4191.md); neither shall men give them the [kowc](../../strongs/h/h3563.md) of [tanḥûmôṯ](../../strongs/h/h8575.md) to [šāqâ](../../strongs/h/h8248.md) for their ['ab](../../strongs/h/h1.md) or for their ['em](../../strongs/h/h517.md).

<a name="jeremiah_16_8"></a>Jeremiah 16:8

Thou shalt not also [bow'](../../strongs/h/h935.md) into the [bayith](../../strongs/h/h1004.md) of [mištê](../../strongs/h/h4960.md), to [yashab](../../strongs/h/h3427.md) with them to ['akal](../../strongs/h/h398.md) and to [šāṯâ](../../strongs/h/h8354.md).

<a name="jeremiah_16_9"></a>Jeremiah 16:9

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will cause to [shabath](../../strongs/h/h7673.md) out of this [maqowm](../../strongs/h/h4725.md) in your ['ayin](../../strongs/h/h5869.md), and in your [yowm](../../strongs/h/h3117.md), the [qowl](../../strongs/h/h6963.md) of [śāśôn](../../strongs/h/h8342.md), and the [qowl](../../strongs/h/h6963.md) of [simchah](../../strongs/h/h8057.md), the [qowl](../../strongs/h/h6963.md) of the [ḥāṯān](../../strongs/h/h2860.md), and the [qowl](../../strongs/h/h6963.md) of the [kallâ](../../strongs/h/h3618.md).

<a name="jeremiah_16_10"></a>Jeremiah 16:10

And it shall come to pass, when thou shalt [nāḡaḏ](../../strongs/h/h5046.md) this ['am](../../strongs/h/h5971.md) all these [dabar](../../strongs/h/h1697.md), and they shall ['āmar](../../strongs/h/h559.md) unto thee, Wherefore hath [Yĕhovah](../../strongs/h/h3068.md) [dabar](../../strongs/h/h1696.md) all this [gadowl](../../strongs/h/h1419.md) [ra'](../../strongs/h/h7451.md) against us? or what is our ['avon](../../strongs/h/h5771.md)? or what is our [chatta'ath](../../strongs/h/h2403.md) that we have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md)?

<a name="jeremiah_16_11"></a>Jeremiah 16:11

Then shalt thou ['āmar](../../strongs/h/h559.md) unto them, Because your ['ab](../../strongs/h/h1.md) have ['azab](../../strongs/h/h5800.md) me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and have [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), and have ['abad](../../strongs/h/h5647.md) them, and have [shachah](../../strongs/h/h7812.md) them, and have ['azab](../../strongs/h/h5800.md) me, and have not [shamar](../../strongs/h/h8104.md) my [towrah](../../strongs/h/h8451.md);

<a name="jeremiah_16_12"></a>Jeremiah 16:12

And ye have ['asah](../../strongs/h/h6213.md) [ra'a'](../../strongs/h/h7489.md) than your ['ab](../../strongs/h/h1.md); for, [hinneh](../../strongs/h/h2009.md), ye [halak](../../strongs/h/h1980.md) every ['iysh](../../strongs/h/h376.md) ['aḥar](../../strongs/h/h310.md) the [šᵊrîrûṯ](../../strongs/h/h8307.md) of his [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md), that they may not [shama'](../../strongs/h/h8085.md) unto me:

<a name="jeremiah_16_13"></a>Jeremiah 16:13

Therefore will I [ṭûl](../../strongs/h/h2904.md) you out of this ['erets](../../strongs/h/h776.md) into an ['erets](../../strongs/h/h776.md) that ye [yada'](../../strongs/h/h3045.md) not, neither ye nor your ['ab](../../strongs/h/h1.md); and there shall ye ['abad](../../strongs/h/h5647.md) ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md) [yômām](../../strongs/h/h3119.md) and [layil](../../strongs/h/h3915.md); where I will not [nathan](../../strongs/h/h5414.md) you [ḥănînâ](../../strongs/h/h2594.md).

<a name="jeremiah_16_14"></a>Jeremiah 16:14

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that it shall no more be ['āmar](../../strongs/h/h559.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), that brought [ʿālâ](../../strongs/h/h5927.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md);

<a name="jeremiah_16_15"></a>Jeremiah 16:15

But, [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md), that brought [ʿālâ](../../strongs/h/h5927.md) the [ben](../../strongs/h/h1121.md) of [Yisra'el](../../strongs/h/h3478.md) from the ['erets](../../strongs/h/h776.md) of the [ṣāp̄ôn](../../strongs/h/h6828.md), and from all the ['erets](../../strongs/h/h776.md) whither he had [nāḏaḥ](../../strongs/h/h5080.md) them: and I will bring them [shuwb](../../strongs/h/h7725.md) into their ['ăḏāmâ](../../strongs/h/h127.md) that I [nathan](../../strongs/h/h5414.md) unto their ['ab](../../strongs/h/h1.md).

<a name="jeremiah_16_16"></a>Jeremiah 16:16

Behold, I will [shalach](../../strongs/h/h7971.md) for [rab](../../strongs/h/h7227.md) [dayyāḡ](../../strongs/h/h1771.md) [dûḡ](../../strongs/h/h1728.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and they shall [dîḡ](../../strongs/h/h1770.md) them; and ['aḥar](../../strongs/h/h310.md) will I [shalach](../../strongs/h/h7971.md) for [rab](../../strongs/h/h7227.md) [ṣayyāḏ](../../strongs/h/h6719.md), and they shall [ṣûḏ](../../strongs/h/h6679.md) them from every [har](../../strongs/h/h2022.md), and from every [giḇʿâ](../../strongs/h/h1389.md), and out of the [nāqîq](../../strongs/h/h5357.md) of the [cela'](../../strongs/h/h5553.md).

<a name="jeremiah_16_17"></a>Jeremiah 16:17

For mine ['ayin](../../strongs/h/h5869.md) are upon all their [derek](../../strongs/h/h1870.md): they are not [cathar](../../strongs/h/h5641.md) from my [paniym](../../strongs/h/h6440.md), neither is their ['avon](../../strongs/h/h5771.md) [tsaphan](../../strongs/h/h6845.md) [neḡeḏ](../../strongs/h/h5048.md) mine ['ayin](../../strongs/h/h5869.md).

<a name="jeremiah_16_18"></a>Jeremiah 16:18

And [ri'šôn](../../strongs/h/h7223.md) I will [shalam](../../strongs/h/h7999.md) their ['avon](../../strongs/h/h5771.md) and their [chatta'ath](../../strongs/h/h2403.md) [mišnê](../../strongs/h/h4932.md); because they have [ḥālal](../../strongs/h/h2490.md) my ['erets](../../strongs/h/h776.md), they have [mālā'](../../strongs/h/h4390.md) mine [nachalah](../../strongs/h/h5159.md) with the [nᵊḇēlâ](../../strongs/h/h5038.md) of their [šiqqûṣ](../../strongs/h/h8251.md) and abominable [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="jeremiah_16_19"></a>Jeremiah 16:19

[Yĕhovah](../../strongs/h/h3068.md), my ['oz](../../strongs/h/h5797.md), and my [māʿôz](../../strongs/h/h4581.md), and my [mānôs](../../strongs/h/h4498.md) in the [yowm](../../strongs/h/h3117.md) of [tsarah](../../strongs/h/h6869.md), the [gowy](../../strongs/h/h1471.md) shall [bow'](../../strongs/h/h935.md) unto thee from the ['ep̄es](../../strongs/h/h657.md) of the ['erets](../../strongs/h/h776.md), and shall ['āmar](../../strongs/h/h559.md), Surely our ['ab](../../strongs/h/h1.md) have [nāḥal](../../strongs/h/h5157.md) [sheqer](../../strongs/h/h8267.md), [heḇel](../../strongs/h/h1892.md), and things wherein there is no [yāʿal](../../strongs/h/h3276.md).

<a name="jeremiah_16_20"></a>Jeremiah 16:20

Shall an ['āḏām](../../strongs/h/h120.md) ['asah](../../strongs/h/h6213.md) ['Elohiym](../../strongs/h/h430.md) unto himself, and they are no ['Elohiym](../../strongs/h/h430.md)?

<a name="jeremiah_16_21"></a>Jeremiah 16:21

Therefore, behold, I will this [pa'am](../../strongs/h/h6471.md) cause them to [yada'](../../strongs/h/h3045.md), I will cause them to [yada'](../../strongs/h/h3045.md) mine [yad](../../strongs/h/h3027.md) and my [gᵊḇûrâ](../../strongs/h/h1369.md); and they shall [yada'](../../strongs/h/h3045.md) that my [shem](../../strongs/h/h8034.md) is [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 15](jeremiah_15.md) - [Jeremiah 17](jeremiah_17.md)