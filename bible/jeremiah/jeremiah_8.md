# [Jeremiah 8](https://www.blueletterbible.org/kjv/jeremiah/8)

<a name="jeremiah_8_1"></a>Jeremiah 8:1

At that [ʿēṯ](../../strongs/h/h6256.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), they shall [yāṣā'](../../strongs/h/h3318.md) the ['etsem](../../strongs/h/h6106.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and the ['etsem](../../strongs/h/h6106.md) of his [śar](../../strongs/h/h8269.md), and the ['etsem](../../strongs/h/h6106.md) of the [kōhēn](../../strongs/h/h3548.md), and the ['etsem](../../strongs/h/h6106.md) of the [nāḇî'](../../strongs/h/h5030.md), and the ['etsem](../../strongs/h/h6106.md) of the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), out of their [qeber](../../strongs/h/h6913.md):

<a name="jeremiah_8_2"></a>Jeremiah 8:2

And they shall [šāṭaḥ](../../strongs/h/h7849.md) them before the [šemeš](../../strongs/h/h8121.md), and the [yareach](../../strongs/h/h3394.md), and all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), whom they have ['ahab](../../strongs/h/h157.md), and whom they have ['abad](../../strongs/h/h5647.md), and ['aḥar](../../strongs/h/h310.md) whom they have [halak](../../strongs/h/h1980.md), and whom they have [darash](../../strongs/h/h1875.md), and whom they have [shachah](../../strongs/h/h7812.md): they shall not be ['āsap̄](../../strongs/h/h622.md), nor be [qāḇar](../../strongs/h/h6912.md); they shall be for [dōmen](../../strongs/h/h1828.md) upon the [paniym](../../strongs/h/h6440.md) of the ['ăḏāmâ](../../strongs/h/h127.md).

<a name="jeremiah_8_3"></a>Jeremiah 8:3

And [maveth](../../strongs/h/h4194.md) shall be [bāḥar](../../strongs/h/h977.md) rather than [chay](../../strongs/h/h2416.md) by all the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of them that [šā'ar](../../strongs/h/h7604.md) of this [ra'](../../strongs/h/h7451.md) [mišpāḥâ](../../strongs/h/h4940.md), which [šā'ar](../../strongs/h/h7604.md) in all the [maqowm](../../strongs/h/h4725.md) whither I have [nāḏaḥ](../../strongs/h/h5080.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_8_4"></a>Jeremiah 8:4

Moreover thou shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Shall they [naphal](../../strongs/h/h5307.md), and not [quwm](../../strongs/h/h6965.md)? shall he [shuwb](../../strongs/h/h7725.md), and not [shuwb](../../strongs/h/h7725.md)?

<a name="jeremiah_8_5"></a>Jeremiah 8:5

Why then is this ['am](../../strongs/h/h5971.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [shuwb](../../strongs/h/h7725.md) by a [nāṣaḥ](../../strongs/h/h5329.md) [mᵊšûḇâ](../../strongs/h/h4878.md)? they [ḥāzaq](../../strongs/h/h2388.md) fast [tārmâ](../../strongs/h/h8649.md), they [mā'ēn](../../strongs/h/h3985.md) to [shuwb](../../strongs/h/h7725.md).

<a name="jeremiah_8_6"></a>Jeremiah 8:6

I [qashab](../../strongs/h/h7181.md) and [shama'](../../strongs/h/h8085.md), but they spake not [dabar](../../strongs/h/h1696.md): no ['iysh](../../strongs/h/h376.md) [nacham](../../strongs/h/h5162.md) him [ʿal](../../strongs/h/h5921.md) his [ra'](../../strongs/h/h7451.md), ['āmar](../../strongs/h/h559.md), What have I ['asah](../../strongs/h/h6213.md)? every one [shuwb](../../strongs/h/h7725.md) to his [mᵊrûṣâ](../../strongs/h/h4794.md), as the [sûs](../../strongs/h/h5483.md) [šāṭap̄](../../strongs/h/h7857.md) into the [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_8_7"></a>Jeremiah 8:7

Yea, the [ḥăsîḏâ](../../strongs/h/h2624.md) in the [shamayim](../../strongs/h/h8064.md) [yada'](../../strongs/h/h3045.md) her appointed [môʿēḏ](../../strongs/h/h4150.md); and the [tôr](../../strongs/h/h8449.md) and the [sûs](../../strongs/h/h5483.md) and the [ʿāḡûr](../../strongs/h/h5693.md) [shamar](../../strongs/h/h8104.md) the [ʿēṯ](../../strongs/h/h6256.md) of their [bow'](../../strongs/h/h935.md); but my ['am](../../strongs/h/h5971.md) [yada'](../../strongs/h/h3045.md) not the [mishpat](../../strongs/h/h4941.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_8_8"></a>Jeremiah 8:8

How do ye ['āmar](../../strongs/h/h559.md), We are [ḥāḵām](../../strongs/h/h2450.md), and the [towrah](../../strongs/h/h8451.md) of [Yĕhovah](../../strongs/h/h3068.md) is with us? Lo, ['āḵēn](../../strongs/h/h403.md) in [sheqer](../../strongs/h/h8267.md) ['asah](../../strongs/h/h6213.md) he it; the [ʿēṭ](../../strongs/h/h5842.md) of the [sāp̄ar](../../strongs/h/h5608.md) is in [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_8_9"></a>Jeremiah 8:9

The [ḥāḵām](../../strongs/h/h2450.md) men are [yāḇēš](../../strongs/h/h3001.md), they are [ḥāṯaṯ](../../strongs/h/h2865.md) and [lāḵaḏ](../../strongs/h/h3920.md): lo, they have [mā'as](../../strongs/h/h3988.md) the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md); and what [ḥāḵmâ](../../strongs/h/h2451.md) is in them?

<a name="jeremiah_8_10"></a>Jeremiah 8:10

Therefore will I [nathan](../../strongs/h/h5414.md) their ['ishshah](../../strongs/h/h802.md) unto ['aḥēr](../../strongs/h/h312.md), and their [sadeh](../../strongs/h/h7704.md) to them that shall [yarash](../../strongs/h/h3423.md) them: for every one from the [qāṭān](../../strongs/h/h6996.md) even unto the [gadowl](../../strongs/h/h1419.md) is [batsa'](../../strongs/h/h1214.md) to [beṣaʿ](../../strongs/h/h1215.md), from the [nāḇî'](../../strongs/h/h5030.md) even unto the [kōhēn](../../strongs/h/h3548.md) every one ['asah](../../strongs/h/h6213.md) [sheqer](../../strongs/h/h8267.md).

<a name="jeremiah_8_11"></a>Jeremiah 8:11

For they have [rapha'](../../strongs/h/h7495.md) the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) [qālal](../../strongs/h/h7043.md), ['āmar](../../strongs/h/h559.md), [shalowm](../../strongs/h/h7965.md), [shalowm](../../strongs/h/h7965.md); when there is no [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_8_12"></a>Jeremiah 8:12

Were they [yāḇēš](../../strongs/h/h3001.md) when they had ['asah](../../strongs/h/h6213.md) [tôʿēḇâ](../../strongs/h/h8441.md)? nay, they were not at [buwsh](../../strongs/h/h954.md) [buwsh](../../strongs/h/h954.md), neither [yada'](../../strongs/h/h3045.md) they [kālam](../../strongs/h/h3637.md): therefore shall they [naphal](../../strongs/h/h5307.md) among them that [naphal](../../strongs/h/h5307.md): in the [ʿēṯ](../../strongs/h/h6256.md) of their [pᵊqudâ](../../strongs/h/h6486.md) they shall be cast [kashal](../../strongs/h/h3782.md), ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_8_13"></a>Jeremiah 8:13

I will ['āsap̄](../../strongs/h/h622.md) [sûp̄](../../strongs/h/h5486.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md): there shall be no [ʿēnāḇ](../../strongs/h/h6025.md) on the [gep̄en](../../strongs/h/h1612.md), nor [tĕ'en](../../strongs/h/h8384.md) on the [tĕ'en](../../strongs/h/h8384.md), and the ['aleh](../../strongs/h/h5929.md) shall [nabel](../../strongs/h/h5034.md); and the things that I have [nathan](../../strongs/h/h5414.md) them shall ['abar](../../strongs/h/h5674.md) from them.

<a name="jeremiah_8_14"></a>Jeremiah 8:14

Why do we [yashab](../../strongs/h/h3427.md)? ['āsap̄](../../strongs/h/h622.md) yourselves, and let us [bow'](../../strongs/h/h935.md) into the [miḇṣār](../../strongs/h/h4013.md) [ʿîr](../../strongs/h/h5892.md), and let us be [damam](../../strongs/h/h1826.md) there: for [Yĕhovah](../../strongs/h/h3068.md) our ['Elohiym](../../strongs/h/h430.md) hath put us to [damam](../../strongs/h/h1826.md), and given us [mayim](../../strongs/h/h4325.md) of [rō'š](../../strongs/h/h7219.md) to [šāqâ](../../strongs/h/h8248.md), because we have [chata'](../../strongs/h/h2398.md) against [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_8_15"></a>Jeremiah 8:15

We [qāvâ](../../strongs/h/h6960.md) for [shalowm](../../strongs/h/h7965.md), but no [towb](../../strongs/h/h2896.md) came; and for a [ʿēṯ](../../strongs/h/h6256.md) of [marpē'](../../strongs/h/h4832.md), and behold [bᵊʿāṯâ](../../strongs/h/h1205.md)!

<a name="jeremiah_8_16"></a>Jeremiah 8:16

The [naḥar](../../strongs/h/h5170.md) of his [sûs](../../strongs/h/h5483.md) was [shama'](../../strongs/h/h8085.md) from [Dān](../../strongs/h/h1835.md): the ['erets](../../strongs/h/h776.md) [rāʿaš](../../strongs/h/h7493.md) at the [qowl](../../strongs/h/h6963.md) of the [miṣhālâ](../../strongs/h/h4684.md) of his strong ['abîr](../../strongs/h/h47.md); for they are [bow'](../../strongs/h/h935.md), and have ['akal](../../strongs/h/h398.md) the ['erets](../../strongs/h/h776.md), and [mᵊlō'](../../strongs/h/h4393.md) that is in it; the [ʿîr](../../strongs/h/h5892.md), and those that [yashab](../../strongs/h/h3427.md) therein.

<a name="jeremiah_8_17"></a>Jeremiah 8:17

For, behold, I will [shalach](../../strongs/h/h7971.md) [nachash](../../strongs/h/h5175.md), [ṣep̄aʿ](../../strongs/h/h6848.md), among you, which will not be [laḥaš](../../strongs/h/h3908.md), and they shall [nāšaḵ](../../strongs/h/h5391.md) you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_8_18"></a>Jeremiah 8:18

When I would [maḇlîḡîṯ](../../strongs/h/h4010.md) myself against [yagown](../../strongs/h/h3015.md), my [leb](../../strongs/h/h3820.md) is [daûāy](../../strongs/h/h1742.md) in me.

<a name="jeremiah_8_19"></a>Jeremiah 8:19

Behold the [qowl](../../strongs/h/h6963.md) of the [shav'ah](../../strongs/h/h7775.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) because of them that dwell in a [merḥāq](../../strongs/h/h4801.md) ['erets](../../strongs/h/h776.md): Is not [Yĕhovah](../../strongs/h/h3068.md) in [Tsiyown](../../strongs/h/h6726.md)? is not her [melek](../../strongs/h/h4428.md) in her? Why have they provoked me to [kāʿas](../../strongs/h/h3707.md) with their [pāsîl](../../strongs/h/h6456.md), and with [nēḵār](../../strongs/h/h5236.md) [heḇel](../../strongs/h/h1892.md)?

<a name="jeremiah_8_20"></a>Jeremiah 8:20

The [qāṣîr](../../strongs/h/h7105.md) is ['abar](../../strongs/h/h5674.md), the [qayiṣ](../../strongs/h/h7019.md) is [kalah](../../strongs/h/h3615.md), and we are not [yasha'](../../strongs/h/h3467.md).

<a name="jeremiah_8_21"></a>Jeremiah 8:21

For the [šeḇar](../../strongs/h/h7667.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) am I [shabar](../../strongs/h/h7665.md); I am [qāḏar](../../strongs/h/h6937.md); [šammâ](../../strongs/h/h8047.md) hath taken [ḥāzaq](../../strongs/h/h2388.md) on me.

<a name="jeremiah_8_22"></a>Jeremiah 8:22

Is there no [ṣŏrî](../../strongs/h/h6875.md) in [Gilʿāḏ](../../strongs/h/h1568.md); is there no [rapha'](../../strongs/h/h7495.md) there? why [kî](../../strongs/h/h3588.md) is not the ['ărûḵâ](../../strongs/h/h724.md) of the [bath](../../strongs/h/h1323.md) of my ['am](../../strongs/h/h5971.md) [ʿālâ](../../strongs/h/h5927.md)?

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 7](jeremiah_7.md) - [Jeremiah 9](jeremiah_9.md)