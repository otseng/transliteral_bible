# [Jeremiah 27](https://www.blueletterbible.org/kjv/jeremiah/27)

<a name="jeremiah_27_1"></a>Jeremiah 27:1

In the [re'shiyth](../../strongs/h/h7225.md) of the [mamlāḵâ](../../strongs/h/h4467.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) the [ben](../../strongs/h/h1121.md) of [Yō'Šîyâ](../../strongs/h/h2977.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) came this [dabar](../../strongs/h/h1697.md) unto [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_27_2"></a>Jeremiah 27:2

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) to me; ['asah](../../strongs/h/h6213.md) thee [mowcer](../../strongs/h/h4147.md) and [môṭâ](../../strongs/h/h4133.md), and [nathan](../../strongs/h/h5414.md) them upon thy [ṣaûā'r](../../strongs/h/h6677.md),

<a name="jeremiah_27_3"></a>Jeremiah 27:3

And [shalach](../../strongs/h/h7971.md) them to the [melek](../../strongs/h/h4428.md) of ['Ĕḏōm](../../strongs/h/h123.md), and to the [melek](../../strongs/h/h4428.md) of [Mô'āḇ](../../strongs/h/h4124.md), and to the [melek](../../strongs/h/h4428.md) of the [ben](../../strongs/h/h1121.md) [ʿAmmôn](../../strongs/h/h5983.md), and to the [melek](../../strongs/h/h4428.md) of [Ṣōr](../../strongs/h/h6865.md), and to the [melek](../../strongs/h/h4428.md) of [Ṣîḏôn](../../strongs/h/h6721.md), by the [yad](../../strongs/h/h3027.md) of the [mal'ak](../../strongs/h/h4397.md) which [bow'](../../strongs/h/h935.md) to [Yĕruwshalaim](../../strongs/h/h3389.md) unto [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md);

<a name="jeremiah_27_4"></a>Jeremiah 27:4

And [tsavah](../../strongs/h/h6680.md) them to ['āmar](../../strongs/h/h559.md) unto their ['adown](../../strongs/h/h113.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Thus shall ye ['āmar](../../strongs/h/h559.md) unto your ['adown](../../strongs/h/h113.md);

<a name="jeremiah_27_5"></a>Jeremiah 27:5

I have ['asah](../../strongs/h/h6213.md) the ['erets](../../strongs/h/h776.md), the ['āḏām](../../strongs/h/h120.md) and the [bĕhemah](../../strongs/h/h929.md) that are [paniym](../../strongs/h/h6440.md) the ['erets](../../strongs/h/h776.md), by my [gadowl](../../strongs/h/h1419.md) [koach](../../strongs/h/h3581.md) and by my [natah](../../strongs/h/h5186.md) [zerowa'](../../strongs/h/h2220.md), and have [nathan](../../strongs/h/h5414.md) it unto whom it ['ayin](../../strongs/h/h5869.md) [yashar](../../strongs/h/h3474.md) unto me.

<a name="jeremiah_27_6"></a>Jeremiah 27:6

And now have I [nathan](../../strongs/h/h5414.md) all these ['erets](../../strongs/h/h776.md) into the [yad](../../strongs/h/h3027.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), my ['ebed](../../strongs/h/h5650.md); and the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md) have I [nathan](../../strongs/h/h5414.md) him also to ['abad](../../strongs/h/h5647.md) him.

<a name="jeremiah_27_7"></a>Jeremiah 27:7

And all [gowy](../../strongs/h/h1471.md) shall ['abad](../../strongs/h/h5647.md) him, and his [ben](../../strongs/h/h1121.md), and his [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md), until the very [ʿēṯ](../../strongs/h/h6256.md) of his ['erets](../../strongs/h/h776.md) [bow'](../../strongs/h/h935.md): and then [rab](../../strongs/h/h7227.md) [gowy](../../strongs/h/h1471.md) and [gadowl](../../strongs/h/h1419.md) [melek](../../strongs/h/h4428.md) shall ['abad](../../strongs/h/h5647.md) themselves of him.

<a name="jeremiah_27_8"></a>Jeremiah 27:8

And it shall come to pass, that the [gowy](../../strongs/h/h1471.md) and [mamlāḵâ](../../strongs/h/h4467.md) which will not ['abad](../../strongs/h/h5647.md) the same [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and that will not [nathan](../../strongs/h/h5414.md) their [ṣaûā'r](../../strongs/h/h6677.md) under the [ʿōl](../../strongs/h/h5923.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), that [gowy](../../strongs/h/h1471.md) will I [paqad](../../strongs/h/h6485.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), with the [chereb](../../strongs/h/h2719.md), and with the [rāʿāḇ](../../strongs/h/h7458.md), and with the [deḇer](../../strongs/h/h1698.md), until I have [tamam](../../strongs/h/h8552.md) them by his [yad](../../strongs/h/h3027.md).

<a name="jeremiah_27_9"></a>Jeremiah 27:9

Therefore [shama'](../../strongs/h/h8085.md) not ye to your [nāḇî'](../../strongs/h/h5030.md), nor to your [qāsam](../../strongs/h/h7080.md), nor to your [ḥălôm](../../strongs/h/h2472.md), nor to your [ʿānan](../../strongs/h/h6049.md), nor to your [kaššāp̄](../../strongs/h/h3786.md), which ['āmar](../../strongs/h/h559.md) unto you, ['āmar](../../strongs/h/h559.md), Ye shall not ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md):

<a name="jeremiah_27_10"></a>Jeremiah 27:10

For they [nāḇā'](../../strongs/h/h5012.md) a [sheqer](../../strongs/h/h8267.md) unto you, to [rachaq](../../strongs/h/h7368.md) you from your ['ăḏāmâ](../../strongs/h/h127.md); and that I should [nāḏaḥ](../../strongs/h/h5080.md) you, and ye should ['abad](../../strongs/h/h6.md).

<a name="jeremiah_27_11"></a>Jeremiah 27:11

But the [gowy](../../strongs/h/h1471.md) that [bow'](../../strongs/h/h935.md) their [ṣaûā'r](../../strongs/h/h6677.md) under the [ʿōl](../../strongs/h/h5923.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and ['abad](../../strongs/h/h5647.md) him, those will I let [yānaḥ](../../strongs/h/h3240.md) in their own ['ăḏāmâ](../../strongs/h/h127.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); and they shall ['abad](../../strongs/h/h5647.md) it, and [yashab](../../strongs/h/h3427.md) therein.

<a name="jeremiah_27_12"></a>Jeremiah 27:12

I [dabar](../../strongs/h/h1696.md) also to [Ṣḏqyh](../../strongs/h/h6667.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) according to all these [dabar](../../strongs/h/h1697.md), ['āmar](../../strongs/h/h559.md), [bow'](../../strongs/h/h935.md) your [ṣaûā'r](../../strongs/h/h6677.md) under the [ʿōl](../../strongs/h/h5923.md) of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and ['abad](../../strongs/h/h5647.md) him and his ['am](../../strongs/h/h5971.md), and [ḥāyâ](../../strongs/h/h2421.md).

<a name="jeremiah_27_13"></a>Jeremiah 27:13

Why will ye [muwth](../../strongs/h/h4191.md), thou and thy ['am](../../strongs/h/h5971.md), by the [chereb](../../strongs/h/h2719.md), by the [rāʿāḇ](../../strongs/h/h7458.md), and by the [deḇer](../../strongs/h/h1698.md), as [Yĕhovah](../../strongs/h/h3068.md) hath [dabar](../../strongs/h/h1696.md) against the [gowy](../../strongs/h/h1471.md) that will not ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md)?

<a name="jeremiah_27_14"></a>Jeremiah 27:14

Therefore [shama'](../../strongs/h/h8085.md) not unto the [dabar](../../strongs/h/h1697.md) of the [nāḇî'](../../strongs/h/h5030.md) that ['āmar](../../strongs/h/h559.md) unto you, ['āmar](../../strongs/h/h559.md), Ye shall not ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md): for they [nāḇā'](../../strongs/h/h5012.md) a [sheqer](../../strongs/h/h8267.md) unto you.

<a name="jeremiah_27_15"></a>Jeremiah 27:15

For I have not [shalach](../../strongs/h/h7971.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), yet they [nāḇā'](../../strongs/h/h5012.md) a [sheqer](../../strongs/h/h8267.md) in my [shem](../../strongs/h/h8034.md); that I might [nāḏaḥ](../../strongs/h/h5080.md) you, and that ye might ['abad](../../strongs/h/h6.md), ye, and the [nāḇî'](../../strongs/h/h5030.md) that [nāḇā'](../../strongs/h/h5012.md) unto you.

<a name="jeremiah_27_16"></a>Jeremiah 27:16

Also I [dabar](../../strongs/h/h1696.md) to the [kōhēn](../../strongs/h/h3548.md) and to all this ['am](../../strongs/h/h5971.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [shama'](../../strongs/h/h8085.md) not to the [dabar](../../strongs/h/h1697.md) of your [nāḇî'](../../strongs/h/h5030.md) that [nāḇā'](../../strongs/h/h5012.md) unto you, ['āmar](../../strongs/h/h559.md), Behold, the [kĕliy](../../strongs/h/h3627.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md) shall now [mᵊhērâ](../../strongs/h/h4120.md) be [shuwb](../../strongs/h/h7725.md) from [Bāḇel](../../strongs/h/h894.md): for they [nāḇā'](../../strongs/h/h5012.md) a [sheqer](../../strongs/h/h8267.md) unto you.

<a name="jeremiah_27_17"></a>Jeremiah 27:17

[shama'](../../strongs/h/h8085.md) not unto them; ['abad](../../strongs/h/h5647.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and [ḥāyâ](../../strongs/h/h2421.md): wherefore should this [ʿîr](../../strongs/h/h5892.md) be [chorbah](../../strongs/h/h2723.md)?

<a name="jeremiah_27_18"></a>Jeremiah 27:18

But if they be [nāḇî'](../../strongs/h/h5030.md), and if the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) [yēš](../../strongs/h/h3426.md) with them, let them now make [pāḡaʿ](../../strongs/h/h6293.md) to [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), that the [kĕliy](../../strongs/h/h3627.md) which are [yāṯar](../../strongs/h/h3498.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and at [Yĕruwshalaim](../../strongs/h/h3389.md), [bow'](../../strongs/h/h935.md) not to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_27_19"></a>Jeremiah 27:19

For thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md) concerning the [ʿammûḏ](../../strongs/h/h5982.md), and concerning the [yam](../../strongs/h/h3220.md), and concerning the [mᵊḵônâ](../../strongs/h/h4350.md), and concerning the [yeṯer](../../strongs/h/h3499.md) of the [kĕliy](../../strongs/h/h3627.md) that [yāṯar](../../strongs/h/h3498.md) in this [ʿîr](../../strongs/h/h5892.md),

<a name="jeremiah_27_20"></a>Jeremiah 27:20

Which [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [laqach](../../strongs/h/h3947.md) not, when he [gālâ](../../strongs/h/h1540.md) [Yᵊḵānyâ](../../strongs/h/h3204.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) to [Bāḇel](../../strongs/h/h894.md), and all the [ḥōr](../../strongs/h/h2715.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="jeremiah_27_21"></a>Jeremiah 27:21

Yea, thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md), concerning the [kĕliy](../../strongs/h/h3627.md) that [yāṯar](../../strongs/h/h3498.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and in the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md) and of [Yĕruwshalaim](../../strongs/h/h3389.md);

<a name="jeremiah_27_22"></a>Jeremiah 27:22

They shall be [bow'](../../strongs/h/h935.md) to [Bāḇel](../../strongs/h/h894.md), and there shall they be until the [yowm](../../strongs/h/h3117.md) that I [paqad](../../strongs/h/h6485.md) them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md); then will I [ʿālâ](../../strongs/h/h5927.md) them up, and [shuwb](../../strongs/h/h7725.md) them to this [maqowm](../../strongs/h/h4725.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 26](jeremiah_26.md) - [Jeremiah 28](jeremiah_28.md)