# [Jeremiah 12](https://www.blueletterbible.org/kjv/jeremiah/12)

<a name="jeremiah_12_1"></a>Jeremiah 12:1

[tsaddiyq](../../strongs/h/h6662.md) art thou, [Yĕhovah](../../strongs/h/h3068.md), when I [riyb](../../strongs/h/h7378.md) with thee: yet let me [dabar](../../strongs/h/h1696.md) with thee of thy [mishpat](../../strongs/h/h4941.md): Wherefore doth the [derek](../../strongs/h/h1870.md) of the [rasha'](../../strongs/h/h7563.md) [tsalach](../../strongs/h/h6743.md)? wherefore are all they [šālâ](../../strongs/h/h7951.md) that [beḡeḏ](../../strongs/h/h899.md) [bāḡaḏ](../../strongs/h/h898.md)?

<a name="jeremiah_12_2"></a>Jeremiah 12:2

Thou hast [nāṭaʿ](../../strongs/h/h5193.md) them, yea, they have [šērēš](../../strongs/h/h8327.md): they [yālaḵ](../../strongs/h/h3212.md), yea, they ['asah](../../strongs/h/h6213.md) [pĕriy](../../strongs/h/h6529.md): thou art [qarowb](../../strongs/h/h7138.md) in their [peh](../../strongs/h/h6310.md), and far [rachowq](../../strongs/h/h7350.md) their [kilyah](../../strongs/h/h3629.md).

<a name="jeremiah_12_3"></a>Jeremiah 12:3

But thou, [Yĕhovah](../../strongs/h/h3068.md), [yada'](../../strongs/h/h3045.md) me: thou hast [ra'ah](../../strongs/h/h7200.md) me, and [bachan](../../strongs/h/h974.md) mine [leb](../../strongs/h/h3820.md) toward thee: [nathaq](../../strongs/h/h5423.md) them like [tso'n](../../strongs/h/h6629.md) for the [ṭiḇḥâ](../../strongs/h/h2878.md), and [qadash](../../strongs/h/h6942.md) them for the [yowm](../../strongs/h/h3117.md) of [hărēḡâ](../../strongs/h/h2028.md).

<a name="jeremiah_12_4"></a>Jeremiah 12:4

How long shall the ['erets](../../strongs/h/h776.md) ['āḇal](../../strongs/h/h56.md), and the ['eseb](../../strongs/h/h6212.md) of every [sadeh](../../strongs/h/h7704.md) [yāḇēš](../../strongs/h/h3001.md), for the [ra'](../../strongs/h/h7451.md) of them that [yashab](../../strongs/h/h3427.md) therein? the [bĕhemah](../../strongs/h/h929.md) are [sāp̄â](../../strongs/h/h5595.md), and the [ʿôp̄](../../strongs/h/h5775.md); because they ['āmar](../../strongs/h/h559.md), He shall not [ra'ah](../../strongs/h/h7200.md) our last ['aḥărîṯ](../../strongs/h/h319.md).

<a name="jeremiah_12_5"></a>Jeremiah 12:5

If thou hast [rûṣ](../../strongs/h/h7323.md) with the [raḡlî](../../strongs/h/h7273.md), and they have [lā'â](../../strongs/h/h3811.md) thee, then how canst thou [taḥărâ](../../strongs/h/h8474.md) with [sûs](../../strongs/h/h5483.md)? and if in the ['erets](../../strongs/h/h776.md) of [shalowm](../../strongs/h/h7965.md), wherein thou [batach](../../strongs/h/h982.md), they wearied thee, then how wilt thou ['asah](../../strongs/h/h6213.md) in the [gā'ôn](../../strongs/h/h1347.md) of [Yardēn](../../strongs/h/h3383.md)?

<a name="jeremiah_12_6"></a>Jeremiah 12:6

For even thy ['ach](../../strongs/h/h251.md), and the [bayith](../../strongs/h/h1004.md) of thy ['ab](../../strongs/h/h1.md), even they have [bāḡaḏ](../../strongs/h/h898.md) with thee; yea, they have [qara'](../../strongs/h/h7121.md) a [mālē'](../../strongs/h/h4392.md) ['aḥar](../../strongs/h/h310.md) thee: ['aman](../../strongs/h/h539.md) them not, though they [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) unto thee.

<a name="jeremiah_12_7"></a>Jeremiah 12:7

I have ['azab](../../strongs/h/h5800.md) mine [bayith](../../strongs/h/h1004.md), I have [nāṭaš](../../strongs/h/h5203.md) mine [nachalah](../../strongs/h/h5159.md); I have [nathan](../../strongs/h/h5414.md) the [yᵊḏiḏûṯ](../../strongs/h/h3033.md) of my [nephesh](../../strongs/h/h5315.md) into the [kaph](../../strongs/h/h3709.md) of her ['oyeb](../../strongs/h/h341.md).

<a name="jeremiah_12_8"></a>Jeremiah 12:8

Mine [nachalah](../../strongs/h/h5159.md) is unto me as an ['ariy](../../strongs/h/h738.md) in the [yaʿar](../../strongs/h/h3293.md); it [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md) against me: therefore have I [sane'](../../strongs/h/h8130.md) it.

<a name="jeremiah_12_9"></a>Jeremiah 12:9

Mine [nachalah](../../strongs/h/h5159.md) is unto me as a [ṣāḇûaʿ](../../strongs/h/h6641.md) [ʿayiṭ](../../strongs/h/h5861.md), the [ʿayiṭ](../../strongs/h/h5861.md) [cabiyb](../../strongs/h/h5439.md) are against her; [yālaḵ](../../strongs/h/h3212.md) ye, ['āsap̄](../../strongs/h/h622.md) all the [chay](../../strongs/h/h2416.md) of the [sadeh](../../strongs/h/h7704.md), ['āṯâ](../../strongs/h/h857.md) to ['oklah](../../strongs/h/h402.md).

<a name="jeremiah_12_10"></a>Jeremiah 12:10

[rab](../../strongs/h/h7227.md) [ra'ah](../../strongs/h/h7462.md) have [shachath](../../strongs/h/h7843.md) my [kerem](../../strongs/h/h3754.md), they have [bûs](../../strongs/h/h947.md) my [ḥelqâ](../../strongs/h/h2513.md) under [bûs](../../strongs/h/h947.md), they have [nathan](../../strongs/h/h5414.md) my [ḥemdâ](../../strongs/h/h2532.md) [ḥelqâ](../../strongs/h/h2513.md) a [šᵊmāmâ](../../strongs/h/h8077.md) [midbar](../../strongs/h/h4057.md).

<a name="jeremiah_12_11"></a>Jeremiah 12:11

They have [śûm](../../strongs/h/h7760.md) it [šāmēm](../../strongs/h/h8076.md), and being [šᵊmāmâ](../../strongs/h/h8077.md) it ['āḇal](../../strongs/h/h56.md) unto me; the ['erets](../../strongs/h/h776.md) is [šāmēm](../../strongs/h/h8074.md), because no ['iysh](../../strongs/h/h376.md) [śûm](../../strongs/h/h7760.md) it to [leb](../../strongs/h/h3820.md).

<a name="jeremiah_12_12"></a>Jeremiah 12:12

The [shadad](../../strongs/h/h7703.md) are [bow'](../../strongs/h/h935.md) upon all high [šᵊp̄î](../../strongs/h/h8205.md) through the [midbar](../../strongs/h/h4057.md): for the [chereb](../../strongs/h/h2719.md) of [Yĕhovah](../../strongs/h/h3068.md) shall ['akal](../../strongs/h/h398.md) from the one [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md) even to the other [qāṣê](../../strongs/h/h7097.md) of the ['erets](../../strongs/h/h776.md): no [basar](../../strongs/h/h1320.md) shall have [shalowm](../../strongs/h/h7965.md).

<a name="jeremiah_12_13"></a>Jeremiah 12:13

They have [zāraʿ](../../strongs/h/h2232.md) [ḥiṭṭâ](../../strongs/h/h2406.md), but shall [qāṣar](../../strongs/h/h7114.md) [qowts](../../strongs/h/h6975.md): they have put themselves to [ḥālâ](../../strongs/h/h2470.md), but shall not [yāʿal](../../strongs/h/h3276.md): and they shall be [buwsh](../../strongs/h/h954.md) of your [tᵊḇû'â](../../strongs/h/h8393.md) because of the [charown](../../strongs/h/h2740.md) ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_12_14"></a>Jeremiah 12:14

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) against all mine [ra'](../../strongs/h/h7451.md) [šāḵēn](../../strongs/h/h7934.md), that [naga'](../../strongs/h/h5060.md) the [nachalah](../../strongs/h/h5159.md) which I have caused my ['am](../../strongs/h/h5971.md) [Yisra'el](../../strongs/h/h3478.md) to [nāḥal](../../strongs/h/h5157.md); Behold, I will [nathash](../../strongs/h/h5428.md) them of their ['ăḏāmâ](../../strongs/h/h127.md), and [nathash](../../strongs/h/h5428.md) the [bayith](../../strongs/h/h1004.md) of [Yehuwdah](../../strongs/h/h3063.md) from [tavek](../../strongs/h/h8432.md) them.

<a name="jeremiah_12_15"></a>Jeremiah 12:15

And it shall come to pass, ['aḥar](../../strongs/h/h310.md) that I have [nathash](../../strongs/h/h5428.md) them I will [shuwb](../../strongs/h/h7725.md), and have [racham](../../strongs/h/h7355.md) on them, and will bring them [shuwb](../../strongs/h/h7725.md), every ['iysh](../../strongs/h/h376.md) to his [nachalah](../../strongs/h/h5159.md), and every ['iysh](../../strongs/h/h376.md) to his ['erets](../../strongs/h/h776.md).

<a name="jeremiah_12_16"></a>Jeremiah 12:16

And it shall come to pass, if they will [lamad](../../strongs/h/h3925.md) [lamad](../../strongs/h/h3925.md) the [derek](../../strongs/h/h1870.md) of my ['am](../../strongs/h/h5971.md), to [shaba'](../../strongs/h/h7650.md) by my [shem](../../strongs/h/h8034.md), [Yĕhovah](../../strongs/h/h3068.md) [chay](../../strongs/h/h2416.md); as they [lamad](../../strongs/h/h3925.md) my ['am](../../strongs/h/h5971.md) to [shaba'](../../strongs/h/h7650.md) by [BaʿAl](../../strongs/h/h1168.md); then shall they be [bānâ](../../strongs/h/h1129.md) in the [tavek](../../strongs/h/h8432.md) of my ['am](../../strongs/h/h5971.md).

<a name="jeremiah_12_17"></a>Jeremiah 12:17

But if they will not [shama'](../../strongs/h/h8085.md), I will [nathash](../../strongs/h/h5428.md) [nathash](../../strongs/h/h5428.md) and ['abad](../../strongs/h/h6.md) that [gowy](../../strongs/h/h1471.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 11](jeremiah_11.md) - [Jeremiah 13](jeremiah_13.md)