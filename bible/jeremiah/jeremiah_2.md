# [Jeremiah 2](https://www.blueletterbible.org/kjv/jeremiah/2)

<a name="jeremiah_2_1"></a>Jeremiah 2:1

Moreover the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_2_2"></a>Jeremiah 2:2

[halak](../../strongs/h/h1980.md) and [qara'](../../strongs/h/h7121.md) in the ['ozen](../../strongs/h/h241.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); I [zakar](../../strongs/h/h2142.md) thee, the [checed](../../strongs/h/h2617.md) of thy [nāʿur](../../strongs/h/h5271.md), the ['ahăḇâ](../../strongs/h/h160.md) of thine [kᵊlûlôṯ](../../strongs/h/h3623.md), when thou [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) me in the [midbar](../../strongs/h/h4057.md), in an ['erets](../../strongs/h/h776.md) that was not [zāraʿ](../../strongs/h/h2232.md).

<a name="jeremiah_2_3"></a>Jeremiah 2:3

[Yisra'el](../../strongs/h/h3478.md) was [qodesh](../../strongs/h/h6944.md) unto [Yĕhovah](../../strongs/h/h3068.md), and the [re'shiyth](../../strongs/h/h7225.md) of his [tᵊḇû'â](../../strongs/h/h8393.md): all that ['akal](../../strongs/h/h398.md) him shall ['asham](../../strongs/h/h816.md); [ra'](../../strongs/h/h7451.md) shall [bow'](../../strongs/h/h935.md) upon them, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_2_4"></a>Jeremiah 2:4

[shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O [bayith](../../strongs/h/h1004.md) of [Ya'aqob](../../strongs/h/h3290.md), and all the [mišpāḥâ](../../strongs/h/h4940.md) of the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md):

<a name="jeremiah_2_5"></a>Jeremiah 2:5

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), What ['evel](../../strongs/h/h5766.md) have your ['ab](../../strongs/h/h1.md) [māṣā'](../../strongs/h/h4672.md) in me, that they are [rachaq](../../strongs/h/h7368.md) from me, and have [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) [heḇel](../../strongs/h/h1892.md), and are become [hāḇal](../../strongs/h/h1891.md)?

<a name="jeremiah_2_6"></a>Jeremiah 2:6

Neither ['āmar](../../strongs/h/h559.md) they, Where is [Yĕhovah](../../strongs/h/h3068.md) that [ʿālâ](../../strongs/h/h5927.md) us out of the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md), that [yālaḵ](../../strongs/h/h3212.md) us through the [midbar](../../strongs/h/h4057.md), through an ['erets](../../strongs/h/h776.md) of ['arabah](../../strongs/h/h6160.md) and of [šûḥâ](../../strongs/h/h7745.md), through an ['erets](../../strongs/h/h776.md) of [ṣîyâ](../../strongs/h/h6723.md), and of the [ṣalmāveṯ](../../strongs/h/h6757.md), through an ['erets](../../strongs/h/h776.md) that no ['iysh](../../strongs/h/h376.md) ['abar](../../strongs/h/h5674.md), and where no ['āḏām](../../strongs/h/h120.md) [yashab](../../strongs/h/h3427.md)?

<a name="jeremiah_2_7"></a>Jeremiah 2:7

And I [bow'](../../strongs/h/h935.md) you into a [karmel](../../strongs/h/h3759.md) ['erets](../../strongs/h/h776.md), to ['akal](../../strongs/h/h398.md) the [pĕriy](../../strongs/h/h6529.md) thereof and the [ṭûḇ](../../strongs/h/h2898.md) thereof; but when ye [bow'](../../strongs/h/h935.md), ye [ṭāmē'](../../strongs/h/h2930.md) my ['erets](../../strongs/h/h776.md), and [śûm](../../strongs/h/h7760.md) mine [nachalah](../../strongs/h/h5159.md) a [tôʿēḇâ](../../strongs/h/h8441.md).

<a name="jeremiah_2_8"></a>Jeremiah 2:8

The [kōhēn](../../strongs/h/h3548.md) ['āmar](../../strongs/h/h559.md) not, Where is [Yĕhovah](../../strongs/h/h3068.md)? and they that [tāp̄aś](../../strongs/h/h8610.md) the [towrah](../../strongs/h/h8451.md) [yada'](../../strongs/h/h3045.md) me not: the [ra'ah](../../strongs/h/h7462.md) also [pāšaʿ](../../strongs/h/h6586.md) against me, and the [nāḇî'](../../strongs/h/h5030.md) [nāḇā'](../../strongs/h/h5012.md) by [BaʿAl](../../strongs/h/h1168.md), and [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) things that do not [yāʿal](../../strongs/h/h3276.md).

<a name="jeremiah_2_9"></a>Jeremiah 2:9

Wherefore I will yet [riyb](../../strongs/h/h7378.md) with you, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and with your [ben](../../strongs/h/h1121.md) [ben](../../strongs/h/h1121.md) will I [riyb](../../strongs/h/h7378.md).

<a name="jeremiah_2_10"></a>Jeremiah 2:10

For ['abar](../../strongs/h/h5674.md) the ['î](../../strongs/h/h339.md) of [Kitîm](../../strongs/h/h3794.md), and [ra'ah](../../strongs/h/h7200.md); and [shalach](../../strongs/h/h7971.md) unto [Qēḏār](../../strongs/h/h6938.md), and [bîn](../../strongs/h/h995.md) [me'od](../../strongs/h/h3966.md), and [ra'ah](../../strongs/h/h7200.md) if there be such a thing.

<a name="jeremiah_2_11"></a>Jeremiah 2:11

Hath a [gowy](../../strongs/h/h1471.md) [yāmar](../../strongs/h/h3235.md) their ['Elohiym](../../strongs/h/h430.md), which are yet no ['Elohiym](../../strongs/h/h430.md)? but my ['am](../../strongs/h/h5971.md) have [mûr](../../strongs/h/h4171.md) their [kabowd](../../strongs/h/h3519.md) for that which doth not [yāʿal](../../strongs/h/h3276.md).

<a name="jeremiah_2_12"></a>Jeremiah 2:12

Be [šāmēm](../../strongs/h/h8074.md), O ye [shamayim](../../strongs/h/h8064.md), at this, and be [śāʿar](../../strongs/h/h8175.md), be ye [me'od](../../strongs/h/h3966.md) [ḥāraḇ](../../strongs/h/h2717.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_2_13"></a>Jeremiah 2:13

For my ['am](../../strongs/h/h5971.md) have ['asah](../../strongs/h/h6213.md) [šᵊnayim](../../strongs/h/h8147.md) [ra'](../../strongs/h/h7451.md); they have ['azab](../../strongs/h/h5800.md) me the [māqôr](../../strongs/h/h4726.md) of [chay](../../strongs/h/h2416.md) [mayim](../../strongs/h/h4325.md), and hewed them [ḥāṣaḇ](../../strongs/h/h2672.md) [bō'r](../../strongs/h/h877.md), [shabar](../../strongs/h/h7665.md) [bō'r](../../strongs/h/h877.md), that can [kûl](../../strongs/h/h3557.md) no [mayim](../../strongs/h/h4325.md).

<a name="jeremiah_2_14"></a>Jeremiah 2:14

Is [Yisra'el](../../strongs/h/h3478.md) an ['ebed](../../strongs/h/h5650.md)? is he a [bayith](../../strongs/h/h1004.md) [yālîḏ](../../strongs/h/h3211.md) slave? [madûaʿ](../../strongs/h/h4069.md) is he [baz](../../strongs/h/h957.md)?

<a name="jeremiah_2_15"></a>Jeremiah 2:15

The [kephiyr](../../strongs/h/h3715.md) [šā'aḡ](../../strongs/h/h7580.md) upon him, and [nathan](../../strongs/h/h5414.md) [qowl](../../strongs/h/h6963.md), and they [shiyth](../../strongs/h/h7896.md) his ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md): his [ʿîr](../../strongs/h/h5892.md) are [yāṣaṯ](../../strongs/h/h3341.md) without [yashab](../../strongs/h/h3427.md).

<a name="jeremiah_2_16"></a>Jeremiah 2:16

Also the [ben](../../strongs/h/h1121.md) of [Nōp̄](../../strongs/h/h5297.md) and [Tᵊḥap̄Nᵊḥēs](../../strongs/h/h8471.md) have [ra'ah](../../strongs/h/h7462.md) the crown of thy [qodqod](../../strongs/h/h6936.md).

<a name="jeremiah_2_17"></a>Jeremiah 2:17

Hast thou not ['asah](../../strongs/h/h6213.md) this unto thyself, in that thou hast ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), [ʿēṯ](../../strongs/h/h6256.md) he [yālaḵ](../../strongs/h/h3212.md) thee by the [derek](../../strongs/h/h1870.md)?

<a name="jeremiah_2_18"></a>Jeremiah 2:18

And now what hast thou to do in the [derek](../../strongs/h/h1870.md) of [Mitsrayim](../../strongs/h/h4714.md), to [šāṯâ](../../strongs/h/h8354.md) the [mayim](../../strongs/h/h4325.md) of [šîḥôr](../../strongs/h/h7883.md)? or what hast thou to do in the [derek](../../strongs/h/h1870.md) of ['Aššûr](../../strongs/h/h804.md), to [šāṯâ](../../strongs/h/h8354.md) the [mayim](../../strongs/h/h4325.md) of the [nāhār](../../strongs/h/h5104.md)?

<a name="jeremiah_2_19"></a>Jeremiah 2:19

Thine own [ra'](../../strongs/h/h7451.md) shall [yacar](../../strongs/h/h3256.md) thee, and thy [mᵊšûḇâ](../../strongs/h/h4878.md) shall [yakach](../../strongs/h/h3198.md) thee: [yada'](../../strongs/h/h3045.md) therefore and [ra'ah](../../strongs/h/h7200.md) that it is a [ra'](../../strongs/h/h7451.md) thing and [mar](../../strongs/h/h4751.md), that thou hast ['azab](../../strongs/h/h5800.md) [Yĕhovah](../../strongs/h/h3068.md) thy ['Elohiym](../../strongs/h/h430.md), and that my [paḥdâ](../../strongs/h/h6345.md) is not in thee, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md) of [tsaba'](../../strongs/h/h6635.md).

<a name="jeremiah_2_20"></a>Jeremiah 2:20

For of ['owlam](../../strongs/h/h5769.md) I have [shabar](../../strongs/h/h7665.md) thy [ʿōl](../../strongs/h/h5923.md), and [nathaq](../../strongs/h/h5423.md) thy [mowcer](../../strongs/h/h4147.md); and thou ['āmar](../../strongs/h/h559.md), I will not ['abar](../../strongs/h/h5674.md) ['abad](../../strongs/h/h5647.md); when upon every [gāḇōha](../../strongs/h/h1364.md) [giḇʿâ](../../strongs/h/h1389.md) and under every [raʿănān](../../strongs/h/h7488.md) ['ets](../../strongs/h/h6086.md) thou [ṣāʿâ](../../strongs/h/h6808.md), playing the [zānâ](../../strongs/h/h2181.md).

<a name="jeremiah_2_21"></a>Jeremiah 2:21

Yet I had [nāṭaʿ](../../strongs/h/h5193.md) thee a [śrēq](../../strongs/h/h8321.md), wholly an ['emeth](../../strongs/h/h571.md) [zera'](../../strongs/h/h2233.md): how then art thou [hāp̄aḵ](../../strongs/h/h2015.md) into the [sûr](../../strongs/h/h5494.md) of a [nāḵrî](../../strongs/h/h5237.md) [gep̄en](../../strongs/h/h1612.md) unto me?

<a name="jeremiah_2_22"></a>Jeremiah 2:22

For though thou [kāḇas](../../strongs/h/h3526.md) thee with [neṯer](../../strongs/h/h5427.md), and take thee [rabah](../../strongs/h/h7235.md) [bōrîṯ](../../strongs/h/h1287.md), yet thine ['avon](../../strongs/h/h5771.md) is [kāṯam](../../strongs/h/h3799.md) [paniym](../../strongs/h/h6440.md) me, [nᵊ'um](../../strongs/h/h5002.md) the ['adonay](../../strongs/h/h136.md) [Yᵊhōvâ](../../strongs/h/h3069.md).

<a name="jeremiah_2_23"></a>Jeremiah 2:23

How canst thou ['āmar](../../strongs/h/h559.md), I am not [ṭāmē'](../../strongs/h/h2930.md), I have not [halak](../../strongs/h/h1980.md) ['aḥar](../../strongs/h/h310.md) [BaʿAl](../../strongs/h/h1168.md)? [ra'ah](../../strongs/h/h7200.md) thy [derek](../../strongs/h/h1870.md) in the [gay'](../../strongs/h/h1516.md), [yada'](../../strongs/h/h3045.md) what thou hast ['asah](../../strongs/h/h6213.md): thou art a [qal](../../strongs/h/h7031.md) [biḵrâ](../../strongs/h/h1072.md) [śāraḵ](../../strongs/h/h8308.md) her [derek](../../strongs/h/h1870.md);

<a name="jeremiah_2_24"></a>Jeremiah 2:24

A [pere'](../../strongs/h/h6501.md) [limmûḏ](../../strongs/h/h3928.md) to the [midbar](../../strongs/h/h4057.md), that snuffeth [šā'ap̄](../../strongs/h/h7602.md) the [ruwach](../../strongs/h/h7307.md) at her ['aûâ](../../strongs/h/h185.md) [nephesh](../../strongs/h/h5315.md); in her [ta'ănâ](../../strongs/h/h8385.md) who can turn her [shuwb](../../strongs/h/h7725.md)? all they that [bāqaš](../../strongs/h/h1245.md) her will not [yāʿap̄](../../strongs/h/h3286.md) themselves; in her [ḥōḏeš](../../strongs/h/h2320.md) they shall [māṣā'](../../strongs/h/h4672.md) her.

<a name="jeremiah_2_25"></a>Jeremiah 2:25

[mānaʿ](../../strongs/h/h4513.md) thy [regel](../../strongs/h/h7272.md) from being [yāḥēp̄](../../strongs/h/h3182.md), and thy [garown](../../strongs/h/h1627.md) from [ṣim'â](../../strongs/h/h6773.md): but thou ['āmar](../../strongs/h/h559.md), There is no [yā'aš](../../strongs/h/h2976.md): no; for I have ['ahab](../../strongs/h/h157.md) [zûr](../../strongs/h/h2114.md), and ['aḥar](../../strongs/h/h310.md) them will I [yālaḵ](../../strongs/h/h3212.md).

<a name="jeremiah_2_26"></a>Jeremiah 2:26

As the [gannāḇ](../../strongs/h/h1590.md) is [bšeṯ](../../strongs/h/h1322.md) when he is [māṣā'](../../strongs/h/h4672.md), so is the [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md) [yāḇēš](../../strongs/h/h3001.md); they, their [melek](../../strongs/h/h4428.md), their [śar](../../strongs/h/h8269.md), and their [kōhēn](../../strongs/h/h3548.md), and their [nāḇî'](../../strongs/h/h5030.md),

<a name="jeremiah_2_27"></a>Jeremiah 2:27

['āmar](../../strongs/h/h559.md) to an ['ets](../../strongs/h/h6086.md), Thou art my ['ab](../../strongs/h/h1.md); and to an ['eben](../../strongs/h/h68.md), Thou hast brought me [yalad](../../strongs/h/h3205.md): for they have [panah](../../strongs/h/h6437.md) their [ʿōrep̄](../../strongs/h/h6203.md) unto me, and not their [paniym](../../strongs/h/h6440.md): but in the [ʿēṯ](../../strongs/h/h6256.md) of their [ra'](../../strongs/h/h7451.md) they will ['āmar](../../strongs/h/h559.md), [quwm](../../strongs/h/h6965.md), and [yasha'](../../strongs/h/h3467.md) us.

<a name="jeremiah_2_28"></a>Jeremiah 2:28

But where are thy ['Elohiym](../../strongs/h/h430.md) that thou hast ['asah](../../strongs/h/h6213.md) thee? let them [quwm](../../strongs/h/h6965.md), if they can [yasha'](../../strongs/h/h3467.md) thee in the [ʿēṯ](../../strongs/h/h6256.md) of thy [ra'](../../strongs/h/h7451.md): for according to the [mispār](../../strongs/h/h4557.md) of thy [ʿîr](../../strongs/h/h5892.md) are thy ['Elohiym](../../strongs/h/h430.md), O [Yehuwdah](../../strongs/h/h3063.md).

<a name="jeremiah_2_29"></a>Jeremiah 2:29

Wherefore will ye [riyb](../../strongs/h/h7378.md) with me? ye all have [pāšaʿ](../../strongs/h/h6586.md) against me, [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md).

<a name="jeremiah_2_30"></a>Jeremiah 2:30

In [shav'](../../strongs/h/h7723.md) have I [nakah](../../strongs/h/h5221.md) your [ben](../../strongs/h/h1121.md); they [laqach](../../strongs/h/h3947.md) no [mûsār](../../strongs/h/h4148.md): your own [chereb](../../strongs/h/h2719.md) hath ['akal](../../strongs/h/h398.md) your [nāḇî'](../../strongs/h/h5030.md), like a [shachath](../../strongs/h/h7843.md) ['ariy](../../strongs/h/h738.md).

<a name="jeremiah_2_31"></a>Jeremiah 2:31

O [dôr](../../strongs/h/h1755.md), [ra'ah](../../strongs/h/h7200.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md). Have I been a [midbar](../../strongs/h/h4057.md) unto [Yisra'el](../../strongs/h/h3478.md)? an ['erets](../../strongs/h/h776.md) of [ma'pēlyâ](../../strongs/h/h3991.md)? wherefore ['āmar](../../strongs/h/h559.md) my ['am](../../strongs/h/h5971.md), We are [rûḏ](../../strongs/h/h7300.md); we will [bow'](../../strongs/h/h935.md) no more unto thee?

<a name="jeremiah_2_32"></a>Jeremiah 2:32

Can a [bᵊṯûlâ](../../strongs/h/h1330.md) [shakach](../../strongs/h/h7911.md) her [ʿădiy](../../strongs/h/h5716.md), or a [kallâ](../../strongs/h/h3618.md) her [qiššurîm](../../strongs/h/h7196.md)? yet my ['am](../../strongs/h/h5971.md) have [shakach](../../strongs/h/h7911.md) me [yowm](../../strongs/h/h3117.md) without [mispār](../../strongs/h/h4557.md).

<a name="jeremiah_2_33"></a>Jeremiah 2:33

Why [yatab](../../strongs/h/h3190.md) thou thy [derek](../../strongs/h/h1870.md) to [bāqaš](../../strongs/h/h1245.md) ['ahăḇâ](../../strongs/h/h160.md)? therefore hast thou also [lamad](../../strongs/h/h3925.md) the [ra'](../../strongs/h/h7451.md) thy [derek](../../strongs/h/h1870.md).

<a name="jeremiah_2_34"></a>Jeremiah 2:34

Also in thy [kanaph](../../strongs/h/h3671.md) is [māṣā'](../../strongs/h/h4672.md) the [dam](../../strongs/h/h1818.md) of the [nephesh](../../strongs/h/h5315.md) of the ['ebyown](../../strongs/h/h34.md) [naqiy](../../strongs/h/h5355.md): I have not [māṣā'](../../strongs/h/h4672.md) it by [maḥtereṯ](../../strongs/h/h4290.md), but upon all these.

<a name="jeremiah_2_35"></a>Jeremiah 2:35

Yet thou ['āmar](../../strongs/h/h559.md), Because I am [naqah](../../strongs/h/h5352.md), surely his ['aph](../../strongs/h/h639.md) shall [shuwb](../../strongs/h/h7725.md) from me. Behold, I will [shaphat](../../strongs/h/h8199.md) with thee, because thou ['āmar](../../strongs/h/h559.md), I have not [chata'](../../strongs/h/h2398.md).

<a name="jeremiah_2_36"></a>Jeremiah 2:36

Why ['āzal](../../strongs/h/h235.md) thou so [me'od](../../strongs/h/h3966.md) to [šānâ](../../strongs/h/h8138.md) thy [derek](../../strongs/h/h1870.md)? thou also shalt be [buwsh](../../strongs/h/h954.md) of [Mitsrayim](../../strongs/h/h4714.md), as thou wast [buwsh](../../strongs/h/h954.md) of ['Aššûr](../../strongs/h/h804.md).

<a name="jeremiah_2_37"></a>Jeremiah 2:37

Yea, thou shalt [yāṣā'](../../strongs/h/h3318.md) from him, and thine [yad](../../strongs/h/h3027.md) upon thine [ro'sh](../../strongs/h/h7218.md): for [Yĕhovah](../../strongs/h/h3068.md) hath [mā'as](../../strongs/h/h3988.md) thy [miḇṭāḥ](../../strongs/h/h4009.md), and thou shalt not [tsalach](../../strongs/h/h6743.md) in them.

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 1](jeremiah_1.md) - [Jeremiah 3](jeremiah_3.md)