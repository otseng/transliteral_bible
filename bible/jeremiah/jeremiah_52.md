# [Jeremiah 52](https://www.blueletterbible.org/kjv/jeremiah/52)

<a name="jeremiah_52_1"></a>Jeremiah 52:1

[Ṣḏqyh](../../strongs/h/h6667.md) was ['echad](../../strongs/h/h259.md) and [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md) [ben](../../strongs/h/h1121.md) when he began to [mālaḵ](../../strongs/h/h4427.md), and he [mālaḵ](../../strongs/h/h4427.md) ['echad](../../strongs/h/h259.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) in [Yĕruwshalaim](../../strongs/h/h3389.md). And his ['em](../../strongs/h/h517.md) [shem](../../strongs/h/h8034.md) was [Ḥămûṭal](../../strongs/h/h2537.md) the [bath](../../strongs/h/h1323.md) of [Yirmᵊyâ](../../strongs/h/h3414.md) of [Liḇnâ](../../strongs/h/h3841.md).

<a name="jeremiah_52_2"></a>Jeremiah 52:2

And he ['asah](../../strongs/h/h6213.md) that which was [ra'](../../strongs/h/h7451.md) in the ['ayin](../../strongs/h/h5869.md) of [Yĕhovah](../../strongs/h/h3068.md), according to all that [Yᵊhôyāqîm](../../strongs/h/h3079.md) had ['asah](../../strongs/h/h6213.md).

<a name="jeremiah_52_3"></a>Jeremiah 52:3

For [ʿal](../../strongs/h/h5921.md) the ['aph](../../strongs/h/h639.md) of [Yĕhovah](../../strongs/h/h3068.md) it came to pass in [Yĕruwshalaim](../../strongs/h/h3389.md) and [Yehuwdah](../../strongs/h/h3063.md), till he had [shalak](../../strongs/h/h7993.md) them from his [paniym](../../strongs/h/h6440.md), that [Ṣḏqyh](../../strongs/h/h6667.md) [māraḏ](../../strongs/h/h4775.md) against the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_52_4"></a>Jeremiah 52:4

And it came to pass in the [tᵊšîʿî](../../strongs/h/h8671.md) [šānâ](../../strongs/h/h8141.md) of his [mālaḵ](../../strongs/h/h4427.md), in the [ʿăśîrî](../../strongs/h/h6224.md) [ḥōḏeš](../../strongs/h/h2320.md), in the [ʿāśôr](../../strongs/h/h6218.md) day of the [ḥōḏeš](../../strongs/h/h2320.md), that [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [bow'](../../strongs/h/h935.md), he and all his [ḥayil](../../strongs/h/h2428.md), against [Yĕruwshalaim](../../strongs/h/h3389.md), and [ḥānâ](../../strongs/h/h2583.md) against it, and [bānâ](../../strongs/h/h1129.md) [dāyēq](../../strongs/h/h1785.md) against it [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_52_5"></a>Jeremiah 52:5

[bow'](../../strongs/h/h935.md) the [ʿîr](../../strongs/h/h5892.md) was [māṣôr](../../strongs/h/h4692.md) unto the [ʿaštê](../../strongs/h/h6249.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of [melek](../../strongs/h/h4428.md) [Ṣḏqyh](../../strongs/h/h6667.md).

<a name="jeremiah_52_6"></a>Jeremiah 52:6

And in the [rᵊḇîʿî](../../strongs/h/h7243.md) [ḥōḏeš](../../strongs/h/h2320.md), in the [tēšaʿ](../../strongs/h/h8672.md) day of the [ḥōḏeš](../../strongs/h/h2320.md), the [rāʿāḇ](../../strongs/h/h7458.md) was [ḥāzaq](../../strongs/h/h2388.md) in the [ʿîr](../../strongs/h/h5892.md), so that there was no [lechem](../../strongs/h/h3899.md) for the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_52_7"></a>Jeremiah 52:7

Then the [ʿîr](../../strongs/h/h5892.md) was [bāqaʿ](../../strongs/h/h1234.md), and all the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md) [bāraḥ](../../strongs/h/h1272.md), and [yāṣā'](../../strongs/h/h3318.md) out of the [ʿîr](../../strongs/h/h5892.md) by [layil](../../strongs/h/h3915.md) by the [derek](../../strongs/h/h1870.md) of the [sha'ar](../../strongs/h/h8179.md) between the [ḥômâ](../../strongs/h/h2346.md), which was by the [melek](../../strongs/h/h4428.md) [gan](../../strongs/h/h1588.md); (now the [Kaśdîmâ](../../strongs/h/h3778.md) were by the [ʿîr](../../strongs/h/h5892.md) [cabiyb](../../strongs/h/h5439.md):) and they [yālaḵ](../../strongs/h/h3212.md) by the [derek](../../strongs/h/h1870.md) of the ['arabah](../../strongs/h/h6160.md).

<a name="jeremiah_52_8"></a>Jeremiah 52:8

But the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) [radaph](../../strongs/h/h7291.md) ['aḥar](../../strongs/h/h310.md) the [melek](../../strongs/h/h4428.md), and [nāśaḡ](../../strongs/h/h5381.md) [Ṣḏqyh](../../strongs/h/h6667.md) in the ['arabah](../../strongs/h/h6160.md) of [Yᵊrēḥô](../../strongs/h/h3405.md); and all his [ḥayil](../../strongs/h/h2428.md) was [puwts](../../strongs/h/h6327.md) from him.

<a name="jeremiah_52_9"></a>Jeremiah 52:9

Then they [tāp̄aś](../../strongs/h/h8610.md) the [melek](../../strongs/h/h4428.md), and [ʿālâ](../../strongs/h/h5927.md) him unto the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) to [Riḇlâ](../../strongs/h/h7247.md) in the ['erets](../../strongs/h/h776.md) of [Ḥămāṯ](../../strongs/h/h2574.md); where he [dabar](../../strongs/h/h1696.md) [mishpat](../../strongs/h/h4941.md) upon him.

<a name="jeremiah_52_10"></a>Jeremiah 52:10

And the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [šāḥaṭ](../../strongs/h/h7819.md) the [ben](../../strongs/h/h1121.md) of [Ṣḏqyh](../../strongs/h/h6667.md) before his ['ayin](../../strongs/h/h5869.md): he [šāḥaṭ](../../strongs/h/h7819.md) also all the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md) in [Riḇlâ](../../strongs/h/h7247.md).

<a name="jeremiah_52_11"></a>Jeremiah 52:11

Then he [ʿāvar](../../strongs/h/h5786.md) the ['ayin](../../strongs/h/h5869.md) of [Ṣḏqyh](../../strongs/h/h6667.md); and the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) ['āsar](../../strongs/h/h631.md) him in [nᵊḥšeṯ](../../strongs/h/h5178.md), and [bow'](../../strongs/h/h935.md) him to [Bāḇel](../../strongs/h/h894.md), and [nathan](../../strongs/h/h5414.md) him in [bayith](../../strongs/h/h1004.md) [pᵊqudâ](../../strongs/h/h6486.md) till the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md).

<a name="jeremiah_52_12"></a>Jeremiah 52:12

Now in the [ḥămîšî](../../strongs/h/h2549.md) [ḥōḏeš](../../strongs/h/h2320.md), in the [ʿāśôr](../../strongs/h/h6218.md) day of the [ḥōḏeš](../../strongs/h/h2320.md), which was the [tēšaʿ](../../strongs/h/h8672.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) [šānâ](../../strongs/h/h8141.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), [bow'](../../strongs/h/h935.md) [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md), [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md), which ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), into [Yĕruwshalaim](../../strongs/h/h3389.md),

<a name="jeremiah_52_13"></a>Jeremiah 52:13

And [śārap̄](../../strongs/h/h8313.md) the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [melek](../../strongs/h/h4428.md) [bayith](../../strongs/h/h1004.md); and all the [bayith](../../strongs/h/h1004.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and all the [bayith](../../strongs/h/h1004.md) of the [gadowl](../../strongs/h/h1419.md) men, [śārap̄](../../strongs/h/h8313.md) he with ['esh](../../strongs/h/h784.md):

<a name="jeremiah_52_14"></a>Jeremiah 52:14

And all the [ḥayil](../../strongs/h/h2428.md) of the [Kaśdîmâ](../../strongs/h/h3778.md), that were with the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md), brake [nāṯaṣ](../../strongs/h/h5422.md) all the [ḥômâ](../../strongs/h/h2346.md) of [Yĕruwshalaim](../../strongs/h/h3389.md) [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_52_15"></a>Jeremiah 52:15

Then [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [gālâ](../../strongs/h/h1540.md) certain of the [dallâ](../../strongs/h/h1803.md) of the ['am](../../strongs/h/h5971.md), and the [yeṯer](../../strongs/h/h3499.md) of the ['am](../../strongs/h/h5971.md) that [šā'ar](../../strongs/h/h7604.md) in the [ʿîr](../../strongs/h/h5892.md), and those that fell [naphal](../../strongs/h/h5307.md), that [naphal](../../strongs/h/h5307.md) to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), and the [yeṯer](../../strongs/h/h3499.md) of the ['āmôn](../../strongs/h/h527.md).

<a name="jeremiah_52_16"></a>Jeremiah 52:16

But [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [šā'ar](../../strongs/h/h7604.md) certain of the [dallâ](../../strongs/h/h1803.md) of the ['erets](../../strongs/h/h776.md) for [kōrēm](../../strongs/h/h3755.md) and for husbandmen H3009.

<a name="jeremiah_52_17"></a>Jeremiah 52:17

Also the [ʿammûḏ](../../strongs/h/h5982.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) that were in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), and the [mᵊḵônâ](../../strongs/h/h4350.md), and the [nᵊḥšeṯ](../../strongs/h/h5178.md) [yam](../../strongs/h/h3220.md) that was in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md), the [Kaśdîmâ](../../strongs/h/h3778.md) [shabar](../../strongs/h/h7665.md), and [nasa'](../../strongs/h/h5375.md) all the [nᵊḥšeṯ](../../strongs/h/h5178.md) of them to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_52_18"></a>Jeremiah 52:18

The [sîr](../../strongs/h/h5518.md) also, and the [yāʿ](../../strongs/h/h3257.md), and the [mᵊzammerê](../../strongs/h/h4212.md), and the [mizrāq](../../strongs/h/h4219.md), and the [kaph](../../strongs/h/h3709.md), and all the [kĕliy](../../strongs/h/h3627.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) wherewith they [sharath](../../strongs/h/h8334.md), took they [laqach](../../strongs/h/h3947.md).

<a name="jeremiah_52_19"></a>Jeremiah 52:19

And the [caph](../../strongs/h/h5592.md), and the [maḥtâ](../../strongs/h/h4289.md), and the [mizrāq](../../strongs/h/h4219.md), and the [sîr](../../strongs/h/h5518.md), and the [mᵊnôrâ](../../strongs/h/h4501.md), and the [kaph](../../strongs/h/h3709.md), and the [mᵊnaqqîṯ](../../strongs/h/h4518.md); that which was of [zāhāḇ](../../strongs/h/h2091.md) in [zāhāḇ](../../strongs/h/h2091.md), and that which was of [keceph](../../strongs/h/h3701.md) in [keceph](../../strongs/h/h3701.md), [laqach](../../strongs/h/h3947.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md).

<a name="jeremiah_52_20"></a>Jeremiah 52:20

The [šᵊnayim](../../strongs/h/h8147.md) [ʿammûḏ](../../strongs/h/h5982.md), ['echad](../../strongs/h/h259.md) [yam](../../strongs/h/h3220.md), and [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [nᵊḥšeṯ](../../strongs/h/h5178.md) [bāqār](../../strongs/h/h1241.md) that were under the [mᵊḵônâ](../../strongs/h/h4350.md), which [melek](../../strongs/h/h4428.md) [Šᵊlōmô](../../strongs/h/h8010.md) had ['asah](../../strongs/h/h6213.md) in the [bayith](../../strongs/h/h1004.md) of [Yĕhovah](../../strongs/h/h3068.md): the [nᵊḥšeṯ](../../strongs/h/h5178.md) of all these [kĕliy](../../strongs/h/h3627.md) was without [mišqāl](../../strongs/h/h4948.md).

<a name="jeremiah_52_21"></a>Jeremiah 52:21

And concerning the [ʿammûḏ](../../strongs/h/h5982.md), the [qômâ](../../strongs/h/h6967.md) of ['echad](../../strongs/h/h259.md) [ʿammûḏ](../../strongs/h/h5982.md) was [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) ['ammâ](../../strongs/h/h520.md); and a [ḥûṭ](../../strongs/h/h2339.md) of [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) ['ammâ](../../strongs/h/h520.md) did [cabab](../../strongs/h/h5437.md) it; and the [ʿăḇî](../../strongs/h/h5672.md) thereof was ['arbaʿ](../../strongs/h/h702.md) ['etsba'](../../strongs/h/h676.md): it was [nāḇaḇ](../../strongs/h/h5014.md).

<a name="jeremiah_52_22"></a>Jeremiah 52:22

And a [kōṯereṯ](../../strongs/h/h3805.md) of [nᵊḥšeṯ](../../strongs/h/h5178.md) was upon it; and the [qômâ](../../strongs/h/h6967.md) of ['echad](../../strongs/h/h259.md) [kōṯereṯ](../../strongs/h/h3805.md) was [ḥāmēš](../../strongs/h/h2568.md) ['ammâ](../../strongs/h/h520.md), with [śᵊḇāḵâ](../../strongs/h/h7639.md) and [rimmôn](../../strongs/h/h7416.md) upon the [kōṯereṯ](../../strongs/h/h3805.md) [cabiyb](../../strongs/h/h5439.md), all of [nᵊḥšeṯ](../../strongs/h/h5178.md). The [šēnî](../../strongs/h/h8145.md) [ʿammûḏ](../../strongs/h/h5982.md) also and the [rimmôn](../../strongs/h/h7416.md) were like unto these.

<a name="jeremiah_52_23"></a>Jeremiah 52:23

And there were [tišʿîm](../../strongs/h/h8673.md) and [šēš](../../strongs/h/h8337.md) [rimmôn](../../strongs/h/h7416.md) on a [ruwach](../../strongs/h/h7307.md); and all the [rimmôn](../../strongs/h/h7416.md) upon the [śᵊḇāḵâ](../../strongs/h/h7639.md) were a [mē'â](../../strongs/h/h3967.md) [cabiyb](../../strongs/h/h5439.md).

<a name="jeremiah_52_24"></a>Jeremiah 52:24

And the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md) [Śᵊrāyâ](../../strongs/h/h8304.md) the [ro'sh](../../strongs/h/h7218.md) [kōhēn](../../strongs/h/h3548.md), and [Ṣᵊp̄Anyâ](../../strongs/h/h6846.md) the [mišnê](../../strongs/h/h4932.md) [kōhēn](../../strongs/h/h3548.md), and the [šālôš](../../strongs/h/h7969.md) [shamar](../../strongs/h/h8104.md) of the [caph](../../strongs/h/h5592.md):

<a name="jeremiah_52_25"></a>Jeremiah 52:25

He [laqach](../../strongs/h/h3947.md) also out of the [ʿîr](../../strongs/h/h5892.md) ['echad](../../strongs/h/h259.md) [sārîs](../../strongs/h/h5631.md), which had the [pāqîḏ](../../strongs/h/h6496.md) of the ['enowsh](../../strongs/h/h582.md) of [milḥāmâ](../../strongs/h/h4421.md); and [šeḇaʿ](../../strongs/h/h7651.md) ['enowsh](../../strongs/h/h582.md) of them that were [ra'ah](../../strongs/h/h7200.md) the [melek](../../strongs/h/h4428.md) [paniym](../../strongs/h/h6440.md), which were [māṣā'](../../strongs/h/h4672.md) in the [ʿîr](../../strongs/h/h5892.md); and the [śar](../../strongs/h/h8269.md) [sāp̄ar](../../strongs/h/h5608.md) of the [tsaba'](../../strongs/h/h6635.md), who [ṣᵊḇā'](../../strongs/h/h6633.md) the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md); and [šiššîm](../../strongs/h/h8346.md) ['iysh](../../strongs/h/h376.md) of the ['am](../../strongs/h/h5971.md) of the ['erets](../../strongs/h/h776.md), that were [māṣā'](../../strongs/h/h4672.md) in the [tavek](../../strongs/h/h8432.md) of the [ʿîr](../../strongs/h/h5892.md).

<a name="jeremiah_52_26"></a>Jeremiah 52:26

So [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [laqach](../../strongs/h/h3947.md) them, and [yālaḵ](../../strongs/h/h3212.md) them to the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) to [Riḇlâ](../../strongs/h/h7247.md).

<a name="jeremiah_52_27"></a>Jeremiah 52:27

And the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) [nakah](../../strongs/h/h5221.md) them, and put them to [muwth](../../strongs/h/h4191.md) in [Riḇlâ](../../strongs/h/h7247.md) in the ['ăḏāmâ](../../strongs/h/h127.md) of [Ḥămāṯ](../../strongs/h/h2574.md). Thus [Yehuwdah](../../strongs/h/h3063.md) was [gālâ](../../strongs/h/h1540.md) out of his own ['erets](../../strongs/h/h776.md).

<a name="jeremiah_52_28"></a>Jeremiah 52:28

This is the ['am](../../strongs/h/h5971.md) whom [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [gālâ](../../strongs/h/h1540.md): in the [šeḇaʿ](../../strongs/h/h7651.md) [šānâ](../../strongs/h/h8141.md) [šālôš](../../strongs/h/h7969.md) ['elep̄](../../strongs/h/h505.md) [Yᵊhûḏî](../../strongs/h/h3064.md) and [šālôš](../../strongs/h/h7969.md) and [ʿeśrîm](../../strongs/h/h6242.md):

<a name="jeremiah_52_29"></a>Jeremiah 52:29

In the [šᵊmōnê](../../strongs/h/h8083.md) [ʿeśer](../../strongs/h/h6240.md) [šānâ](../../strongs/h/h8141.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) he [gālâ](../../strongs/h/h1540.md) from [Yĕruwshalaim](../../strongs/h/h3389.md) [šᵊmōnê](../../strongs/h/h8083.md) [mē'â](../../strongs/h/h3967.md) [šᵊlōšîm](../../strongs/h/h7970.md) and [šᵊnayim](../../strongs/h/h8147.md) [nephesh](../../strongs/h/h5315.md):

<a name="jeremiah_52_30"></a>Jeremiah 52:30

In the [šālôš](../../strongs/h/h7969.md) and [ʿeśrîm](../../strongs/h/h6242.md) [šānâ](../../strongs/h/h8141.md) of [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [Nᵊḇûzar'Ăḏān](../../strongs/h/h5018.md) the [rab](../../strongs/h/h7227.md) of the [ṭabāḥ](../../strongs/h/h2876.md) [gālâ](../../strongs/h/h1540.md) of the [Yᵊhûḏî](../../strongs/h/h3064.md) [šeḇaʿ](../../strongs/h/h7651.md) [mē'â](../../strongs/h/h3967.md) ['arbāʿîm](../../strongs/h/h705.md) and [ḥāmēš](../../strongs/h/h2568.md) [nephesh](../../strongs/h/h5315.md): all the [nephesh](../../strongs/h/h5315.md) were ['arbaʿ](../../strongs/h/h702.md) ['elep̄](../../strongs/h/h505.md) and [šēš](../../strongs/h/h8337.md) [mē'â](../../strongs/h/h3967.md).

<a name="jeremiah_52_31"></a>Jeremiah 52:31

And it came to pass in the [šeḇaʿ](../../strongs/h/h7651.md) and [šᵊlōšîm](../../strongs/h/h7970.md) [šānâ](../../strongs/h/h8141.md) of the [gālûṯ](../../strongs/h/h1546.md) of [Yᵊhôyāḵîn](../../strongs/h/h3078.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), in the [šᵊnayim](../../strongs/h/h8147.md) [ʿeśer](../../strongs/h/h6240.md) [ḥōḏeš](../../strongs/h/h2320.md), in the [ḥāmēš](../../strongs/h/h2568.md) and [ʿeśrîm](../../strongs/h/h6242.md) day of the [ḥōḏeš](../../strongs/h/h2320.md), that ['Ĕvîl Mᵊrōḏaḵ](../../strongs/h/h192.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) in the first [šānâ](../../strongs/h/h8141.md) of his [malkuwth](../../strongs/h/h4438.md) [nasa'](../../strongs/h/h5375.md) the [ro'sh](../../strongs/h/h7218.md) of [Yᵊhôyāḵîn](../../strongs/h/h3078.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [yāṣā'](../../strongs/h/h3318.md) him out of [bayith](../../strongs/h/h1004.md) H3628,

<a name="jeremiah_52_32"></a>Jeremiah 52:32

And [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) unto him, and [nathan](../../strongs/h/h5414.md) his [kicce'](../../strongs/h/h3678.md) [maʿal](../../strongs/h/h4605.md) the [kicce'](../../strongs/h/h3678.md) of the [melek](../../strongs/h/h4428.md) that were with him in [Bāḇel](../../strongs/h/h894.md),

<a name="jeremiah_52_33"></a>Jeremiah 52:33

And [šānâ](../../strongs/h/h8138.md) his [kele'](../../strongs/h/h3608.md) [beḡeḏ](../../strongs/h/h899.md): and he did [tāmîḏ](../../strongs/h/h8548.md) ['akal](../../strongs/h/h398.md) [lechem](../../strongs/h/h3899.md) [paniym](../../strongs/h/h6440.md) him all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

<a name="jeremiah_52_34"></a>Jeremiah 52:34

And for his ['ăruḥâ](../../strongs/h/h737.md), there was a [tāmîḏ](../../strongs/h/h8548.md) ['ăruḥâ](../../strongs/h/h737.md) [nathan](../../strongs/h/h5414.md) him of the [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md), [yowm](../../strongs/h/h3117.md) [yowm](../../strongs/h/h3117.md) a [dabar](../../strongs/h/h1697.md) until the [yowm](../../strongs/h/h3117.md) of his [maveth](../../strongs/h/h4194.md), all the [yowm](../../strongs/h/h3117.md) of his [chay](../../strongs/h/h2416.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 51](jeremiah_51.md)