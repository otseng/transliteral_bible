# [Jeremiah 19](https://www.blueletterbible.org/kjv/jeremiah/19)

<a name="jeremiah_19_1"></a>Jeremiah 19:1

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), [halak](../../strongs/h/h1980.md) and [qānâ](../../strongs/h/h7069.md) a [yāṣar](../../strongs/h/h3335.md) [ḥereś](../../strongs/h/h2789.md) [baqbûq](../../strongs/h/h1228.md), and take of the [zāqēn](../../strongs/h/h2205.md) of the ['am](../../strongs/h/h5971.md), and of the [zāqēn](../../strongs/h/h2205.md) of the [kōhēn](../../strongs/h/h3548.md);

<a name="jeremiah_19_2"></a>Jeremiah 19:2

And [yāṣā'](../../strongs/h/h3318.md) unto the [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), which is by the [peṯaḥ](../../strongs/h/h6607.md) of the [ḥarsûṯ](../../strongs/h/h2777.md) [sha'ar](../../strongs/h/h8179.md), and [qara'](../../strongs/h/h7121.md) there the [dabar](../../strongs/h/h1697.md) that I shall [dabar](../../strongs/h/h1696.md) thee,

<a name="jeremiah_19_3"></a>Jeremiah 19:3

And ['āmar](../../strongs/h/h559.md), [shama'](../../strongs/h/h8085.md) ye the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md), O [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md); Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [bow'](../../strongs/h/h935.md) [ra'](../../strongs/h/h7451.md) upon this [maqowm](../../strongs/h/h4725.md), the which whosoever [shama'](../../strongs/h/h8085.md), his ['ozen](../../strongs/h/h241.md) shall [ṣālal](../../strongs/h/h6750.md).

<a name="jeremiah_19_4"></a>Jeremiah 19:4

Because they have ['azab](../../strongs/h/h5800.md) me, and have [nāḵar](../../strongs/h/h5234.md) this [maqowm](../../strongs/h/h4725.md), and have [qāṭar](../../strongs/h/h6999.md) in it unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md), whom neither they nor their ['ab](../../strongs/h/h1.md) have [yada'](../../strongs/h/h3045.md), nor the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and have [mālā'](../../strongs/h/h4390.md) this [maqowm](../../strongs/h/h4725.md) with the [dam](../../strongs/h/h1818.md) of [naqiy](../../strongs/h/h5355.md);

<a name="jeremiah_19_5"></a>Jeremiah 19:5

They have [bānâ](../../strongs/h/h1129.md) also the [bāmâ](../../strongs/h/h1116.md) of [BaʿAl](../../strongs/h/h1168.md), to [śārap̄](../../strongs/h/h8313.md) their [ben](../../strongs/h/h1121.md) with ['esh](../../strongs/h/h784.md) for [ʿōlâ](../../strongs/h/h5930.md) unto [BaʿAl](../../strongs/h/h1168.md), which I [tsavah](../../strongs/h/h6680.md) not, nor [dabar](../../strongs/h/h1696.md) it, neither [ʿālâ](../../strongs/h/h5927.md) it into my [leb](../../strongs/h/h3820.md):

<a name="jeremiah_19_6"></a>Jeremiah 19:6

Therefore, behold, the [yowm](../../strongs/h/h3117.md) [bow'](../../strongs/h/h935.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), that this [maqowm](../../strongs/h/h4725.md) shall no more be [qara'](../../strongs/h/h7121.md) [Tōp̄Eṯ](../../strongs/h/h8612.md), nor The [gay'](../../strongs/h/h1516.md) of the [ben](../../strongs/h/h1121.md) of [Hinnōm](../../strongs/h/h2011.md), but The [gay'](../../strongs/h/h1516.md) of [hărēḡâ](../../strongs/h/h2028.md).

<a name="jeremiah_19_7"></a>Jeremiah 19:7

And I will make [bāqaq](../../strongs/h/h1238.md) the ['etsah](../../strongs/h/h6098.md) of [Yehuwdah](../../strongs/h/h3063.md) and [Yĕruwshalaim](../../strongs/h/h3389.md) in this [maqowm](../../strongs/h/h4725.md); and I will cause them to [naphal](../../strongs/h/h5307.md) by the [chereb](../../strongs/h/h2719.md) [paniym](../../strongs/h/h6440.md) their ['oyeb](../../strongs/h/h341.md), and by the [yad](../../strongs/h/h3027.md) of them that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md): and their [nᵊḇēlâ](../../strongs/h/h5038.md) will I [nathan](../../strongs/h/h5414.md) to be [ma'akal](../../strongs/h/h3978.md) for the [ʿôp̄](../../strongs/h/h5775.md) of the [shamayim](../../strongs/h/h8064.md), and for the [bĕhemah](../../strongs/h/h929.md) of the ['erets](../../strongs/h/h776.md).

<a name="jeremiah_19_8"></a>Jeremiah 19:8

And I will [śûm](../../strongs/h/h7760.md) this [ʿîr](../../strongs/h/h5892.md) [šammâ](../../strongs/h/h8047.md), and a [šᵊrēqâ](../../strongs/h/h8322.md); every one that ['abar](../../strongs/h/h5674.md) thereby shall be [šāmēm](../../strongs/h/h8074.md) and [šāraq](../../strongs/h/h8319.md) because of all the [makâ](../../strongs/h/h4347.md) thereof.

<a name="jeremiah_19_9"></a>Jeremiah 19:9

And I will cause them to ['akal](../../strongs/h/h398.md) the [basar](../../strongs/h/h1320.md) of their [ben](../../strongs/h/h1121.md) and the [basar](../../strongs/h/h1320.md) of their [bath](../../strongs/h/h1323.md), and they shall ['akal](../../strongs/h/h398.md) every ['iysh](../../strongs/h/h376.md) the [basar](../../strongs/h/h1320.md) of his [rea'](../../strongs/h/h7453.md) in the [māṣôr](../../strongs/h/h4692.md) and [māṣôq](../../strongs/h/h4689.md), wherewith their ['oyeb](../../strongs/h/h341.md), and they that [bāqaš](../../strongs/h/h1245.md) their [nephesh](../../strongs/h/h5315.md), shall [ṣûq](../../strongs/h/h6693.md) them.

<a name="jeremiah_19_10"></a>Jeremiah 19:10

Then shalt thou [shabar](../../strongs/h/h7665.md) the [baqbûq](../../strongs/h/h1228.md) in the ['ayin](../../strongs/h/h5869.md) of the ['enowsh](../../strongs/h/h582.md) that [halak](../../strongs/h/h1980.md) with thee,

<a name="jeremiah_19_11"></a>Jeremiah 19:11

And shalt ['āmar](../../strongs/h/h559.md) unto them, Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md); Even [kāḵâ](../../strongs/h/h3602.md) will I [shabar](../../strongs/h/h7665.md) this ['am](../../strongs/h/h5971.md) and this [ʿîr](../../strongs/h/h5892.md), as one [shabar](../../strongs/h/h7665.md) a [yāṣar](../../strongs/h/h3335.md) [kĕliy](../../strongs/h/h3627.md), that [yakol](../../strongs/h/h3201.md) be made whole [rapha'](../../strongs/h/h7495.md): and they shall [qāḇar](../../strongs/h/h6912.md) them in [Tōp̄Eṯ](../../strongs/h/h8612.md), till there be no [maqowm](../../strongs/h/h4725.md) to [qāḇar](../../strongs/h/h6912.md).

<a name="jeremiah_19_12"></a>Jeremiah 19:12

Thus will I ['asah](../../strongs/h/h6213.md) unto this [maqowm](../../strongs/h/h4725.md), [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md), and to the [yashab](../../strongs/h/h3427.md) thereof, and even [nathan](../../strongs/h/h5414.md) this [ʿîr](../../strongs/h/h5892.md) as [Tōp̄Eṯ](../../strongs/h/h8612.md):

<a name="jeremiah_19_13"></a>Jeremiah 19:13

And the [bayith](../../strongs/h/h1004.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), and the [bayith](../../strongs/h/h1004.md) of the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), shall be [tame'](../../strongs/h/h2931.md) as the [maqowm](../../strongs/h/h4725.md) of [Tōp̄Eṯ](../../strongs/h/h8612.md), because of all the [bayith](../../strongs/h/h1004.md) upon whose [gāḡ](../../strongs/h/h1406.md) they have burned [qāṭar](../../strongs/h/h6999.md) unto all the [tsaba'](../../strongs/h/h6635.md) of [shamayim](../../strongs/h/h8064.md), and have poured [nacak](../../strongs/h/h5258.md) drink [necek](../../strongs/h/h5262.md) unto ['aḥēr](../../strongs/h/h312.md) ['Elohiym](../../strongs/h/h430.md).

<a name="jeremiah_19_14"></a>Jeremiah 19:14

Then [bow'](../../strongs/h/h935.md) [Yirmᵊyâ](../../strongs/h/h3414.md) from [Tōp̄Eṯ](../../strongs/h/h8612.md), whither [Yĕhovah](../../strongs/h/h3068.md) had [shalach](../../strongs/h/h7971.md) him to [nāḇā'](../../strongs/h/h5012.md); and he ['amad](../../strongs/h/h5975.md) in the [ḥāṣēr](../../strongs/h/h2691.md) of [Yĕhovah](../../strongs/h/h3068.md) [bayith](../../strongs/h/h1004.md); and ['āmar](../../strongs/h/h559.md) to all the ['am](../../strongs/h/h5971.md),

<a name="jeremiah_19_15"></a>Jeremiah 19:15

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) of [tsaba'](../../strongs/h/h6635.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Behold, I will [bow'](../../strongs/h/h935.md) upon this [ʿîr](../../strongs/h/h5892.md) and upon all her [ʿîr](../../strongs/h/h5892.md) all the [ra'](../../strongs/h/h7451.md) that I have [dabar](../../strongs/h/h1696.md) against it, because they have [qāšâ](../../strongs/h/h7185.md) their [ʿōrep̄](../../strongs/h/h6203.md), that they might not [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 18](jeremiah_18.md) - [Jeremiah 20](jeremiah_20.md)