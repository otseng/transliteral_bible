# [Jeremiah 24](https://www.blueletterbible.org/kjv/jeremiah/24)

<a name="jeremiah_24_1"></a>Jeremiah 24:1

[Yĕhovah](../../strongs/h/h3068.md) [ra'ah](../../strongs/h/h7200.md) me, and, behold, [šᵊnayim](../../strongs/h/h8147.md) [dûḏay](../../strongs/h/h1736.md) of [tĕ'en](../../strongs/h/h8384.md) were [yāʿaḏ](../../strongs/h/h3259.md) [paniym](../../strongs/h/h6440.md) the [heykal](../../strongs/h/h1964.md) of [Yĕhovah](../../strongs/h/h3068.md), ['aḥar](../../strongs/h/h310.md) that [Nᵊḇûḵaḏne'Ṣṣar](../../strongs/h/h5019.md) [melek](../../strongs/h/h4428.md) of [Bāḇel](../../strongs/h/h894.md) had carried away [gālâ](../../strongs/h/h1540.md) [Yᵊḵānyâ](../../strongs/h/h3204.md) the [ben](../../strongs/h/h1121.md) of [Yᵊhôyāqîm](../../strongs/h/h3079.md) [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and the [śar](../../strongs/h/h8269.md) of [Yehuwdah](../../strongs/h/h3063.md), with the [ḥārāš](../../strongs/h/h2796.md) and [masgēr](../../strongs/h/h4525.md), from [Yĕruwshalaim](../../strongs/h/h3389.md), and had [bow'](../../strongs/h/h935.md) them to [Bāḇel](../../strongs/h/h894.md).

<a name="jeremiah_24_2"></a>Jeremiah 24:2

['echad](../../strongs/h/h259.md) [dûḏ](../../strongs/h/h1731.md) had [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md) [tĕ'en](../../strongs/h/h8384.md), even like the [tĕ'en](../../strongs/h/h8384.md) that are [bikûrâ](../../strongs/h/h1073.md): and the ['echad](../../strongs/h/h259.md) [dûḏ](../../strongs/h/h1731.md) had [me'od](../../strongs/h/h3966.md) [ra'](../../strongs/h/h7451.md) [tĕ'en](../../strongs/h/h8384.md), which could not be ['akal](../../strongs/h/h398.md), they were so [rōaʿ](../../strongs/h/h7455.md).

<a name="jeremiah_24_3"></a>Jeremiah 24:3

Then ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md) unto me, What [ra'ah](../../strongs/h/h7200.md) thou, [Yirmᵊyâ](../../strongs/h/h3414.md)? And I ['āmar](../../strongs/h/h559.md), [tĕ'en](../../strongs/h/h8384.md); the [towb](../../strongs/h/h2896.md) [tĕ'en](../../strongs/h/h8384.md), [me'od](../../strongs/h/h3966.md) [towb](../../strongs/h/h2896.md); and the [ra'](../../strongs/h/h7451.md), [me'od](../../strongs/h/h3966.md) [ra'](../../strongs/h/h7451.md), that cannot be ['akal](../../strongs/h/h398.md), they are so [rōaʿ](../../strongs/h/h7455.md).

<a name="jeremiah_24_4"></a>Jeremiah 24:4

Again the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came unto me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_24_5"></a>Jeremiah 24:5

Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), the ['Elohiym](../../strongs/h/h430.md) of [Yisra'el](../../strongs/h/h3478.md); Like these [towb](../../strongs/h/h2896.md) [tĕ'en](../../strongs/h/h8384.md), so will I [nāḵar](../../strongs/h/h5234.md) them that are [gālûṯ](../../strongs/h/h1546.md) of [Yehuwdah](../../strongs/h/h3063.md), whom I have [shalach](../../strongs/h/h7971.md) out of this [maqowm](../../strongs/h/h4725.md) into the ['erets](../../strongs/h/h776.md) of the [Kaśdîmâ](../../strongs/h/h3778.md) for their [towb](../../strongs/h/h2896.md).

<a name="jeremiah_24_6"></a>Jeremiah 24:6

For I will [śûm](../../strongs/h/h7760.md) mine ['ayin](../../strongs/h/h5869.md) upon them for [towb](../../strongs/h/h2896.md), and I will bring them [shuwb](../../strongs/h/h7725.md) to this ['erets](../../strongs/h/h776.md): and I will [bānâ](../../strongs/h/h1129.md) them, and not pull them [harac](../../strongs/h/h2040.md); and I will [nāṭaʿ](../../strongs/h/h5193.md) them, and not pluck them [nathash](../../strongs/h/h5428.md).

<a name="jeremiah_24_7"></a>Jeremiah 24:7

And I will [nathan](../../strongs/h/h5414.md) them a [leb](../../strongs/h/h3820.md) to [yada'](../../strongs/h/h3045.md) me, that I am [Yĕhovah](../../strongs/h/h3068.md): and they shall be my ['am](../../strongs/h/h5971.md), and I will be their ['Elohiym](../../strongs/h/h430.md): for they shall [shuwb](../../strongs/h/h7725.md) unto me with their [leb](../../strongs/h/h3820.md).

<a name="jeremiah_24_8"></a>Jeremiah 24:8

And as the [ra'](../../strongs/h/h7451.md) [tĕ'en](../../strongs/h/h8384.md), which cannot be ['akal](../../strongs/h/h398.md), they are so [rōaʿ](../../strongs/h/h7455.md); surely thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md), So will I [nathan](../../strongs/h/h5414.md) [Ṣḏqyh](../../strongs/h/h6667.md) the [melek](../../strongs/h/h4428.md) of [Yehuwdah](../../strongs/h/h3063.md), and his [śar](../../strongs/h/h8269.md), and the [šᵊ'ērîṯ](../../strongs/h/h7611.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), that [šā'ar](../../strongs/h/h7604.md) in this ['erets](../../strongs/h/h776.md), and them that [yashab](../../strongs/h/h3427.md) in the ['erets](../../strongs/h/h776.md) of [Mitsrayim](../../strongs/h/h4714.md):

<a name="jeremiah_24_9"></a>Jeremiah 24:9

And I will [nathan](../../strongs/h/h5414.md) them to be [zaʿăvâ](../../strongs/h/h2189.md) [zᵊvāʿâ](../../strongs/h/h2113.md) into all the [mamlāḵâ](../../strongs/h/h4467.md) of the ['erets](../../strongs/h/h776.md) for their [ra'](../../strongs/h/h7451.md), to be a [cherpah](../../strongs/h/h2781.md) and a [māšāl](../../strongs/h/h4912.md), a [šᵊnînâ](../../strongs/h/h8148.md) and a [qᵊlālâ](../../strongs/h/h7045.md), in all [maqowm](../../strongs/h/h4725.md) whither I shall [nāḏaḥ](../../strongs/h/h5080.md) them.

<a name="jeremiah_24_10"></a>Jeremiah 24:10

And I will [shalach](../../strongs/h/h7971.md) the [chereb](../../strongs/h/h2719.md), the [rāʿāḇ](../../strongs/h/h7458.md), and the [deḇer](../../strongs/h/h1698.md), among them, till they be [tamam](../../strongs/h/h8552.md) from off the ['ăḏāmâ](../../strongs/h/h127.md) that I [nathan](../../strongs/h/h5414.md) unto them and to their ['ab](../../strongs/h/h1.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 23](jeremiah_23.md) - [Jeremiah 25](jeremiah_25.md)