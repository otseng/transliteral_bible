# [Jeremiah 18](https://www.blueletterbible.org/kjv/jeremiah/18)

<a name="jeremiah_18_1"></a>Jeremiah 18:1

The [dabar](../../strongs/h/h1697.md) which came to [Yirmᵊyâ](../../strongs/h/h3414.md) from [Yĕhovah](../../strongs/h/h3068.md), ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_18_2"></a>Jeremiah 18:2

[quwm](../../strongs/h/h6965.md), and [yarad](../../strongs/h/h3381.md) to the [yāṣar](../../strongs/h/h3335.md) [bayith](../../strongs/h/h1004.md), and there I will cause thee to [shama'](../../strongs/h/h8085.md) my [dabar](../../strongs/h/h1697.md).

<a name="jeremiah_18_3"></a>Jeremiah 18:3

Then I [yarad](../../strongs/h/h3381.md) to the [yāṣar](../../strongs/h/h3335.md) [bayith](../../strongs/h/h1004.md), and, [hinneh](../../strongs/h/h2009.md), he ['asah](../../strongs/h/h6213.md) a [mĕla'kah](../../strongs/h/h4399.md) on the ['ōḇen](../../strongs/h/h70.md).

<a name="jeremiah_18_4"></a>Jeremiah 18:4

And the [kĕliy](../../strongs/h/h3627.md) that he ['asah](../../strongs/h/h6213.md) of [ḥōmer](../../strongs/h/h2563.md) was [shachath](../../strongs/h/h7843.md) in the [yad](../../strongs/h/h3027.md) of the [yāṣar](../../strongs/h/h3335.md): so he ['asah](../../strongs/h/h6213.md) it [shuwb](../../strongs/h/h7725.md) ['aḥēr](../../strongs/h/h312.md) [kĕliy](../../strongs/h/h3627.md), as ['ayin](../../strongs/h/h5869.md) [yashar](../../strongs/h/h3474.md) to the [yāṣar](../../strongs/h/h3335.md) to ['asah](../../strongs/h/h6213.md) it.

<a name="jeremiah_18_5"></a>Jeremiah 18:5

Then the [dabar](../../strongs/h/h1697.md) of [Yĕhovah](../../strongs/h/h3068.md) came to me, ['āmar](../../strongs/h/h559.md),

<a name="jeremiah_18_6"></a>Jeremiah 18:6

O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md), [yakol](../../strongs/h/h3201.md) I ['asah](../../strongs/h/h6213.md) with you as this [yāṣar](../../strongs/h/h3335.md)? [nᵊ'um](../../strongs/h/h5002.md) [Yĕhovah](../../strongs/h/h3068.md). Behold, as the [ḥōmer](../../strongs/h/h2563.md) is in the [yāṣar](../../strongs/h/h3335.md) [yad](../../strongs/h/h3027.md), so are ye in mine [yad](../../strongs/h/h3027.md), O [bayith](../../strongs/h/h1004.md) of [Yisra'el](../../strongs/h/h3478.md).

<a name="jeremiah_18_7"></a>Jeremiah 18:7

At what [reḡaʿ](../../strongs/h/h7281.md) I shall [dabar](../../strongs/h/h1696.md) concerning a [gowy](../../strongs/h/h1471.md), and concerning a [mamlāḵâ](../../strongs/h/h4467.md), to [nathash](../../strongs/h/h5428.md), and to [nāṯaṣ](../../strongs/h/h5422.md), and to ['abad](../../strongs/h/h6.md) it;

<a name="jeremiah_18_8"></a>Jeremiah 18:8

If that [gowy](../../strongs/h/h1471.md), against whom I have [dabar](../../strongs/h/h1696.md), [shuwb](../../strongs/h/h7725.md) from their [ra'](../../strongs/h/h7451.md), I will [nacham](../../strongs/h/h5162.md) of the [ra'](../../strongs/h/h7451.md) that I [chashab](../../strongs/h/h2803.md) to ['asah](../../strongs/h/h6213.md) unto them.

<a name="jeremiah_18_9"></a>Jeremiah 18:9

And at what [reḡaʿ](../../strongs/h/h7281.md) I shall [dabar](../../strongs/h/h1696.md) concerning a [gowy](../../strongs/h/h1471.md), and concerning a [mamlāḵâ](../../strongs/h/h4467.md), to [bānâ](../../strongs/h/h1129.md) and to [nāṭaʿ](../../strongs/h/h5193.md) it;

<a name="jeremiah_18_10"></a>Jeremiah 18:10

If it ['asah](../../strongs/h/h6213.md) [ra'](../../strongs/h/h7451.md) in my ['ayin](../../strongs/h/h5869.md), that it [shama'](../../strongs/h/h8085.md) not my [qowl](../../strongs/h/h6963.md), then I will [nacham](../../strongs/h/h5162.md) of the [towb](../../strongs/h/h2896.md), wherewith I ['āmar](../../strongs/h/h559.md) I would [yatab](../../strongs/h/h3190.md) them.

<a name="jeremiah_18_11"></a>Jeremiah 18:11

Now therefore go to, ['āmar](../../strongs/h/h559.md) to the ['iysh](../../strongs/h/h376.md) of [Yehuwdah](../../strongs/h/h3063.md), and to the [yashab](../../strongs/h/h3427.md) of [Yĕruwshalaim](../../strongs/h/h3389.md), ['āmar](../../strongs/h/h559.md), Thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); Behold, I [yāṣar](../../strongs/h/h3335.md) [ra'](../../strongs/h/h7451.md) against you, and [chashab](../../strongs/h/h2803.md) a [maḥăšāḇâ](../../strongs/h/h4284.md) against you: [shuwb](../../strongs/h/h7725.md) ye now every ['iysh](../../strongs/h/h376.md) from his [ra'](../../strongs/h/h7451.md) [derek](../../strongs/h/h1870.md), and make your [derek](../../strongs/h/h1870.md) and your [maʿălāl](../../strongs/h/h4611.md) [yatab](../../strongs/h/h3190.md).

<a name="jeremiah_18_12"></a>Jeremiah 18:12

And they ['āmar](../../strongs/h/h559.md), There is no [yā'aš](../../strongs/h/h2976.md): but we will [yālaḵ](../../strongs/h/h3212.md) ['aḥar](../../strongs/h/h310.md) our own [maḥăšāḇâ](../../strongs/h/h4284.md), and we will every ['iysh](../../strongs/h/h376.md) ['asah](../../strongs/h/h6213.md) the [šᵊrîrûṯ](../../strongs/h/h8307.md) of his [ra'](../../strongs/h/h7451.md) [leb](../../strongs/h/h3820.md).

<a name="jeremiah_18_13"></a>Jeremiah 18:13

Therefore thus ['āmar](../../strongs/h/h559.md) [Yĕhovah](../../strongs/h/h3068.md); [sha'al](../../strongs/h/h7592.md) ye now among the [gowy](../../strongs/h/h1471.md), who hath [shama'](../../strongs/h/h8085.md) such things: the [bᵊṯûlâ](../../strongs/h/h1330.md) of [Yisra'el](../../strongs/h/h3478.md) hath ['asah](../../strongs/h/h6213.md) a [me'od](../../strongs/h/h3966.md) horrible [šaʿărûr](../../strongs/h/h8186.md).

<a name="jeremiah_18_14"></a>Jeremiah 18:14

Will a man ['azab](../../strongs/h/h5800.md) the [šeleḡ](../../strongs/h/h7950.md) of [Lᵊḇānôn](../../strongs/h/h3844.md) which cometh from the [tsuwr](../../strongs/h/h6697.md) of the [sadeh](../../strongs/h/h7704.md)? or shall the [qar](../../strongs/h/h7119.md) [nāzal](../../strongs/h/h5140.md) [mayim](../../strongs/h/h4325.md) that come from another [zûr](../../strongs/h/h2114.md) be [nathash](../../strongs/h/h5428.md)?

<a name="jeremiah_18_15"></a>Jeremiah 18:15

Because my ['am](../../strongs/h/h5971.md) hath [shakach](../../strongs/h/h7911.md) me, they have burned [qāṭar](../../strongs/h/h6999.md) to [shav'](../../strongs/h/h7723.md), and they have caused them to [kashal](../../strongs/h/h3782.md) in their [derek](../../strongs/h/h1870.md) from the ['owlam](../../strongs/h/h5769.md) [šᵊḇîl](../../strongs/h/h7635.md) [šᵊḇîl](../../strongs/h/h7635.md), to [yālaḵ](../../strongs/h/h3212.md) in [nāṯîḇ](../../strongs/h/h5410.md), in a [derek](../../strongs/h/h1870.md) not cast [sālal](../../strongs/h/h5549.md);

<a name="jeremiah_18_16"></a>Jeremiah 18:16

To [śûm](../../strongs/h/h7760.md) their ['erets](../../strongs/h/h776.md) [šammâ](../../strongs/h/h8047.md), and a ['owlam](../../strongs/h/h5769.md) [šᵊrûqâ](../../strongs/h/h8292.md) [šᵊrûqâ](../../strongs/h/h8292.md); every one that ['abar](../../strongs/h/h5674.md) thereby shall be [šāmēm](../../strongs/h/h8074.md), and [nuwd](../../strongs/h/h5110.md) his [ro'sh](../../strongs/h/h7218.md).

<a name="jeremiah_18_17"></a>Jeremiah 18:17

I will [puwts](../../strongs/h/h6327.md) them as with a [qāḏîm](../../strongs/h/h6921.md) [ruwach](../../strongs/h/h7307.md) [paniym](../../strongs/h/h6440.md) the ['oyeb](../../strongs/h/h341.md); I will [ra'ah](../../strongs/h/h7200.md) them the [ʿōrep̄](../../strongs/h/h6203.md), and not the [paniym](../../strongs/h/h6440.md), in the [yowm](../../strongs/h/h3117.md) of their ['êḏ](../../strongs/h/h343.md).

<a name="jeremiah_18_18"></a>Jeremiah 18:18

Then ['āmar](../../strongs/h/h559.md) they, [yālaḵ](../../strongs/h/h3212.md), and let us [chashab](../../strongs/h/h2803.md) [maḥăšāḇâ](../../strongs/h/h4284.md) against [Yirmᵊyâ](../../strongs/h/h3414.md); for the [towrah](../../strongs/h/h8451.md) shall not ['abad](../../strongs/h/h6.md) from the [kōhēn](../../strongs/h/h3548.md), nor ['etsah](../../strongs/h/h6098.md) from the [ḥāḵām](../../strongs/h/h2450.md), nor the [dabar](../../strongs/h/h1697.md) from the [nāḇî'](../../strongs/h/h5030.md). [yālaḵ](../../strongs/h/h3212.md), and let us [nakah](../../strongs/h/h5221.md) him with the [lashown](../../strongs/h/h3956.md), and let us not [qashab](../../strongs/h/h7181.md) to any of his [dabar](../../strongs/h/h1697.md).

<a name="jeremiah_18_19"></a>Jeremiah 18:19

[qashab](../../strongs/h/h7181.md) to me, [Yĕhovah](../../strongs/h/h3068.md), and [shama'](../../strongs/h/h8085.md) to the [qowl](../../strongs/h/h6963.md) of them that [yārîḇ](../../strongs/h/h3401.md) with me.

<a name="jeremiah_18_20"></a>Jeremiah 18:20

Shall [ra'](../../strongs/h/h7451.md) be [shalam](../../strongs/h/h7999.md) for [towb](../../strongs/h/h2896.md)? for they have [karah](../../strongs/h/h3738.md) a [šûḥâ](../../strongs/h/h7745.md) for my [nephesh](../../strongs/h/h5315.md). [zakar](../../strongs/h/h2142.md) that I ['amad](../../strongs/h/h5975.md) [paniym](../../strongs/h/h6440.md) thee to [dabar](../../strongs/h/h1696.md) [towb](../../strongs/h/h2896.md) for them, and to [shuwb](../../strongs/h/h7725.md) thy [chemah](../../strongs/h/h2534.md) from them.

<a name="jeremiah_18_21"></a>Jeremiah 18:21

Therefore [nathan](../../strongs/h/h5414.md) their [ben](../../strongs/h/h1121.md) to the [rāʿāḇ](../../strongs/h/h7458.md), and pour [nāḡar](../../strongs/h/h5064.md) their blood by the [yad](../../strongs/h/h3027.md) of the [chereb](../../strongs/h/h2719.md); and let their ['ishshah](../../strongs/h/h802.md) be [šakûl](../../strongs/h/h7909.md) of their children, and be ['almānâ](../../strongs/h/h490.md); and let their ['enowsh](../../strongs/h/h582.md) be [harag](../../strongs/h/h2026.md) to [maveth](../../strongs/h/h4194.md); let their [bāḥûr](../../strongs/h/h970.md) be [nakah](../../strongs/h/h5221.md) by the [chereb](../../strongs/h/h2719.md) in [milḥāmâ](../../strongs/h/h4421.md).

<a name="jeremiah_18_22"></a>Jeremiah 18:22

Let a [zaʿaq](../../strongs/h/h2201.md) be [shama'](../../strongs/h/h8085.md) from their [bayith](../../strongs/h/h1004.md), when thou shalt [bow'](../../strongs/h/h935.md) a [gᵊḏûḏ](../../strongs/h/h1416.md) [piṯ'ōm](../../strongs/h/h6597.md) upon them: for they have [karah](../../strongs/h/h3738.md) a [šûḥâ](../../strongs/h/h7745.md) [šîḥâ](../../strongs/h/h7882.md) to [lāḵaḏ](../../strongs/h/h3920.md) me, and [taman](../../strongs/h/h2934.md) [paḥ](../../strongs/h/h6341.md) for my [regel](../../strongs/h/h7272.md).

<a name="jeremiah_18_23"></a>Jeremiah 18:23

Yet, [Yĕhovah](../../strongs/h/h3068.md), thou [yada'](../../strongs/h/h3045.md) all their ['etsah](../../strongs/h/h6098.md) against me to [maveth](../../strongs/h/h4194.md) me: [kāp̄ar](../../strongs/h/h3722.md) not their ['avon](../../strongs/h/h5771.md), neither blot [māḥâ](../../strongs/h/h4229.md) their [chatta'ath](../../strongs/h/h2403.md) from thy [paniym](../../strongs/h/h6440.md), but let them be [kashal](../../strongs/h/h3782.md) [paniym](../../strongs/h/h6440.md) thee; ['asah](../../strongs/h/h6213.md) thus with them in the [ʿēṯ](../../strongs/h/h6256.md) of thine ['aph](../../strongs/h/h639.md).

---

[Transliteral Bible](../bible.md)

[Jeremiah](jeremiah.md)

[Jeremiah 17](jeremiah_17.md) - [Jeremiah 19](jeremiah_19.md)