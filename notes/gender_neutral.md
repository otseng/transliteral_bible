# Gender neutral

| Bible | Gender neutral | Notes
|---|---|---|
|NIV 1984| No |
|NRSV 1989| Yes | First major translation to be gender neutral
|REB 1989| Partial |
|HCSB 2004| Partial |
|TNIV 2005| Yes |
|NIV 2011| Yes |
|CEB 2011| Yes |

## Links

[Gender in Bible translation](http://en.wikipedia.org/wiki/Gender_in_Bible_translation)
