# Online manuscripts

### Complete Bible Codex

[Codex Sinaiticus](https://codexsinaiticus.org/en/manuscript.aspx?__VIEWSTATEGENERATOR=01FB804F&book=36&lid=en&side=r&zoomSlider=0)

[Codex Vaticanus](https://digi.vatlib.it/view/MSS_Vat.gr.1209)

[Codex Alexandrinus](https://www.bl.uk/manuscripts/FullDisplay.aspx?ref=Royal_MS_1_d_viii&index=5)

### Tanakh

[Aleppo Codex](https://barhama.com/ajaxzoom/viewer/viewer.php?zoomDir=/pic/AleppoWM/&example=viewer5)

[Leningrad Codex](https://archive.org/details/Leningrad_Codex/mode/2up)

### Partial manuscripts

[CSNTM](https://manuscripts.csntm.org/)

[Ancient Biblical Manuscripts Online: Papyri](https://bmats.libguides.com/c.php?g=362544&p=2448767)

[Goodspeed](https://goodspeed.lib.uchicago.edu/list/index.php?list=listscanned)

[Early Bible](http://www.earlybible.com/manuscripts)

### Books

[Erasmus - Novum Instrumentum](https://printedbooks.csntm.org/PrintedBook/Group/ErasmusNovumInstrumentum)

[Stephanus - Novum Testamentum](https://printedbooks.csntm.org/PrintedBook/Group/RobertusStephanusNovumTestamentum1550)
