# Number eight

* Beginnings
* Renewal

## Passages

* Genesis 17:12 - And he that is eight days old shall be circumcised among you, every man child in your generations, he that is born in the house, or bought with money of any stranger, which [is] not of thy seed.

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/8.html)

[Bible Zero](https://www.biblezero.com/number-8-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-8/)

[Mary Jane Humes](https://maryjanehumes.com/number-8-meaning-in-the-bible/)
