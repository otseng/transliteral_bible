# Number seventeen

* Overcoming the enemy
* Complete victory
* Divine intervention

## Notes

* Union of 10 (completeness) and 7 (perfection)
* Ark rested on the Nissan 17

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/17.html)

[Bible Zero](https://www.biblezero.com/number-17-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-17/)

[Servant of Messiah - Nissan 17](https://servantofmessiah.org/nissan-17th/)

[Never Thirsty](https://www.neverthirsty.org/bible-qa/qa-archives/question/was-jesus-resurrected-on-the-same-day-noahs-ark-rested-on-mt-ararat/)