# NIV Bible

## History

[Bible Gateway NIV Intro](https://www.biblegateway.com/versions/New-International-Version-NIV-Bible/)

[The Inspiring Story of How the NIV Bible Became a Reality](https://www.biblica.com/articles/history-of-the-niv-bible-translation/)

## NIV Version differences

[Differences between NIV1984, tNIV (2005), and NIV2011](http://www.biblewebapp.com/niv2011-changes/)

[NIV2011 comparison with NIV1984 and TNIV](http://www.slowley.com/niv2011_comparison/index.html)

[The 2011 Revision of the NIV](http://www.bible-researcher.com/niv.2011.html)

## NIV 1984 Bible

[NIV 1984 PDF](https://www.clcpeople.net/niv-bible-1984-version.html)
