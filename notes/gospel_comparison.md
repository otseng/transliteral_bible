|          | Matthew  | Mark | Luke | John |
|----------|----------|----------|----------|----------|
| Other author name | Levi  | John, Colobodactolos |  | Beloved disciple |
| Other book name |  | Gospel of Peter | Gospel of Paul |  |
| Synoptic gospel | yes | yes | yes | no |
| Author ethnicity | Jew | Jew | Gentile | Jew |
| Relationship to Jesus | Disciple | n/a | n/a | Disciple |
| Relationship to others | n/a | Assistant to Barnabas, Paul, Luke, and Peter | Travelled with Paul | n/a |
| Occupation | Tax collector | Stenographer? | Doctor | Fisher man |
| Location of writing | Antioch? | Rome | Rome? Caesarea? | Ephesus? |
| Other possible author | Isador, disciple of Matthew |  |  |  Lazarus |
| Target audience   | Young believers   | Gentiles   | Gentiles  | Mature believers   |
| Writing style | Teaching | Narrative | History | Theology |
| Date written  | 75-85 AD | 60-70 AD | 75-85 AD | 95-110 AD |
| Purpose        |  Jesus is the Messiah  | Encouragement during persecution |  Theophilus to release Paul  |   |
| View of Jesus   | King of Jews   | Son of man   | Savior of world   | Son of God  |
| Who is Christ? | Christ the King | Christ the servant | Christ the Man | Christ is God |
| If it were a body part | mouth | hands, feet | mind | heart |
| Geneology    | To Abraham through Joseph |  | To Adam through Mary |  |
| Early life of Jesus  | n/a | n/a | Birth, 12 years old | n/a |
| View of God  | Never used the word God |    |     |    |
| Old Testament quotations | 53 | 36  | 25  | 20  |
| Old Testament allusions | 76 | 27  | 42  | 105  |
| Topic arrangemnet | Chronological | Chronological | Topical | Topical |
| Theme  | What Jesus said  | What Jesus did  | What Jesus said  | Who Jesus was  |
| Emphasis | Sermons | Miracles | Jesus' humanity | Jesus' principles |
| Related books  | n/a  | n/a  | Luke/Acts was one book  | n/a  |
| Greek style | Semitic influenced Greek | Poor Greek | Good Greek | Simple Greek |
| Geographic focus | Galilee | Galilee | Galilee | Jerusalem |
| Key words | 5 major discourses | "immediately" | parables | "amen, amen" |
| Ending | Resurrection | Ascension | Promise of Holy Spirit | Promise of return |
| % unique material | 42% | 7% | 59% | 92% |
| Other notes | | Last supper held in his father's home.  |  |  |


## Links

* [Brainscape](https://www.brainscape.com/flashcards/life-and-teachings-of-christ-part-3-14658489/packs/21623934)
* [Owlcation](https://owlcation.com/humanities/Comparing-the-Gospels-Matthew-Mark-Luke-and-John)
* [Catholic Resources](https://catholic-resources.org/Bible/Four_Gospel_Chart.htm)
* [SO4J](https://www.so4j.com/harmony-of-the-gospels-of-jesus-in-the-bible/)
* [Native Marriage](https://nativemarriage.com/biblical/topical/comparison_chart_of_the_four_gospels/)
* [Redeeming God](https://redeeminggod.com/sermons/miscellaneous/a-chart-on-the-four-gospels/)
* [Discover Jesus](https://discoverjesus.com/topic/the-four-gospels)
* [Comparison of the major gospels](https://en.wikipedia.org/wiki/Gospel_of_Thomas#Comparison_of_the_major_gospels)
