# Forms of you

ye
you
your
yours
thee
thou
thy
thine
thyself

[AV611 - Thee, Thou, and Ye](https://av1611.com/kjbp/articles/bacon-theethou.html)

Yinz
Yous
Y'all

|||NOM|OBJ|POSS|
|---|---|---|---|---|
|1st|singular|I     |Me   |My (or mine)   |
|   |plural  |We    |Us   |Our (or ours)  |
|2nd|singular|Thou  |Thee |Thy (or thine) |
|   |plural  |Ye    |You  |Your (or yours)|
|3rd|singular|He/She/It|Him/Her/It|His/Hers/Its|
|   |plural  |They  |Them |Their (or theirs)|
