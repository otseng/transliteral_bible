# Hebrew Grammar

[Wikipedia](https://en.wikipedia.org/wiki/Hebrew_language#Grammar)

[unfoldingWord Hebrew Grammar (UHG)](https://uhg.readthedocs.io/en/latest/front.html)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/grammar.html)

## Prefixes

* [Wikipedia](https://en.wikipedia.org/wiki/Prefixes_in_Hebrew)
