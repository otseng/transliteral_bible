# Hebrew synonyms

## Abomination

- [pigûl](../strongs/h/h6292.md)
- [šiqqûṣ](../strongs/h/h8251.md)
- [tôʿēḇâ](../strongs/h/h8441.md)

## Afraid

(See fear)

## Ark

- ['ārôn](../strongs/h/h727.md)
- [tēḇâ](../strongs/h/h8392.md)

## Army

- [chalats](../strongs/h/h2502.md)
- [gᵊḏûḏ](../strongs/h/h1416.md)
- [ḥayil](../strongs/h/h2428.md)
- [maʿărāḵâ](../strongs/h/h4634.md)
- [maṣṣāḇâ](../strongs/h/h4675.md)
- [tsaba'](../strongs/h/h6635.md)

## Blessed

- ['āšar](../strongs/h/h833.md)
- [bĕrakah](../strongs/h/h1293.md)
- ['esher](../strongs/h/h835.md)

## Bitter

- [memer](../strongs/h/h4470.md)
- [marah](../strongs/h/h4784.md)
- [mārâ](../strongs/h/h4787.md)
- [mᵊrî](../strongs/h/h4805.md)
- [mᵊrîrûṯ](../strongs/h/h4814.md)
- [mᵊrîrî](../strongs/h/h4815.md)
- [mārar](../strongs/h/h4843.md)
- [tamrûrîm](../strongs/h/h8563.md)

## Captivity

- [gālâ](../strongs/h/h1540.md)
- [gālûṯ](../strongs/h/h1546.md)
- [gālûṯ](../strongs/h/h1547.md)
- [gôlâ](../strongs/h/h1473.md)
- [šᵊḇî](../strongs/h/h7628.md)
- [shebuwth](../strongs/h/h7622.md)
- [šiḇyâ](../strongs/h/h7633.md)
- [šîḇâ](../strongs/h/h7870.md)
- [ṭalṭēlâ](../strongs/h/h2925.md)

## City

- [ʿîr](../strongs/h/h5892.md)
- [qiryâ](../strongs/h/h7151.md)

## Clean

- [bar](../strongs/h/h1249.md)
- [bārar](../strongs/h/h1305.md)
- [zāḵ](../strongs/h/h2134.md)
- [zāḵâ](../strongs/h/h2135.md)
- [zāḵû](../strongs/h/h2136.md)
- [zāḵaḵ](../strongs/h/h2141.md)
- [ḥāmîṣ](../strongs/h/h2548.md)
- [tahowr](../strongs/h/h2889.md)
- [ṭᵊhôr](../strongs/h/h2890.md)
- [ṭāhēr](../strongs/h/h2891.md)
- [naqiy](../strongs/h/h5355.md)
- [tamam](../strongs/h/h8552.md)

## Comfort

- [bālaḡ](../strongs/h/h1082.md)
- [maḇlîḡîṯ](../strongs/h/h4010.md)
- [niḥum](../strongs/h/h5150.md)
- [nacham](../strongs/h/h5162.md)
- [neḥāmâ](../strongs/h/h5165.md)
- [sāʿaḏ](../strongs/h/h5582.md)
- [rāp̄aḏ](../strongs/h/h7502.md)
- [tanḥûmôṯ](../strongs/h/h8575.md)

## Cover

- [ʿāṭâ](../strongs/h/h5844.md)
- [cakak](../strongs/h/h5526.md)
- [ḥāp̄ap̄](../strongs/h/h2653.md)
- [kāp̄aš](../strongs/h/h3728.md)
- [kāsâ](../strongs/h/h3680.md)
- [mᵊḵassê](../strongs/h/h4374.md)
- [nacak](../strongs/h/h5258.md)
- [qāram](../strongs/h/h7159.md)
- [qaśvâ](../strongs/h/h7184.md)
- [shuwph](../strongs/h/h7779.md)
- [ṭālal](../strongs/h/h2926.md)

## Cry

- [hāmâ](../strongs/h/h1993.md)
- [zāʿaq](../strongs/h/h2199.md)
- [zaʿaq](../strongs/h/h2201.md)
- [pāʿâ](../strongs/h/h6463.md)
- [ṣᵊvāḥâ](../strongs/h/h6682.md)
- [ṣāʿaq](../strongs/h/h6817.md)
- [tsa'aqah](../strongs/h/h6818.md)
- [ṣāraḥ](../strongs/h/h6873.md)
- [qara'](../strongs/h/h7121.md)
- [rûaʿ](../strongs/h/h7321.md)
- [rinnah](../strongs/h/h7440.md)
- [ranan](../strongs/h/h7442.md)
- [šāvaʿ](../strongs/h/h7768.md)
- [šôaʿ](../strongs/h/h7771.md)
- [shav'ah](../strongs/h/h7775.md)
- [tᵊšu'â](../strongs/h/h8663.md)

## Curse

- ['ālâ](../strongs/h/h422.md)
- ['alah](../strongs/h/h423.md)
- ['arar](../strongs/h/h779.md)
- [barak](../strongs/h/h1288.md)
- [ḥērem](../strongs/h/h2764.md)
- [mᵊ'ērâ](../strongs/h/h3994.md)
- [nāqaḇ](../strongs/h/h5344.md)
- [qāḇaḇ](../strongs/h/h6895.md)
- [qālal](../strongs/h/h7043.md)
- [qᵊlālâ](../strongs/h/h7045.md)
- [šᵊḇûʿâ](../strongs/h/h7621.md)
- [ta'ălâ](../strongs/h/h8381.md)

## Deliver

- [chalats](../strongs/h/h2502.md)
- [palat](../strongs/h/h6403.md)
- [natsal](../strongs/h/h5337.md)
- [nathan](../strongs/h/h5414.md)

## Desert

- [midbar](../strongs/h/h4057.md)
- [`arabah](../strongs/h/h6160.md)
- [ṣîyî](../strongs/h/h6728.md)

## Destroy

- ['abad](../strongs/h/h6.md)
- ['ăḇaḏ](../strongs/h/h7.md)
- ['asham](../strongs/h/h816.md)
- [bālaʿ](../strongs/h/h1104.md)
- [chabal](../strongs/h/h2254.md)
- [dāḵā'](../strongs/h/h1792.md)
- [damah](../strongs/h/h1820.md)
- [gārar](../strongs/h/h1641.md)
- [ḥăḇal](../strongs/h/h2255.md)
- [hāmam](../strongs/h/h2000.md)
- [harac](../strongs/h/h2040.md)
- [ḥāram](../strongs/h/h2763.md)
- [huwm](../strongs/h/h1949.md)
- [kalah](../strongs/h/h3615.md)
- [karath](../strongs/h/h3772.md)
- [māḥâ](../strongs/h/h4229.md)
- [mᵊḡar](../strongs/h/h4049.md)
- [mašḥîṯ](../strongs/h/h4889.md)
- [muwl](../strongs/h/h4135.md)
- [naqaph](../strongs/h/h5362.md)
- [nāsaḥ](../strongs/h/h5255.md)
- [nāšam](../strongs/h/h5395.md)
- [nāṯaṣ](../strongs/h/h5422.md)
- [qûr](../strongs/h/h6979.md)
- [šāmaḏ](../strongs/h/h8045.md)
- [ṣāmaṯ](../strongs/h/h6789.md)
- [šāmēm](../strongs/h/h8074.md)
- [sāp̄â](../strongs/h/h5595.md)
- [sᵊṯar](../strongs/h/h5642.md)
- [shabar](../strongs/h/h7665.md)
- [shadad](../strongs/h/h7703.md)
- [shachath](../strongs/h/h7843.md)
- [šô'](../strongs/h/h7722.md)
- [yānâ](../strongs/h/h3238.md)

## Dwell

- [guwr](../strongs/h/h1481.md)
- [shakan](../strongs/h/h7931.md)
- [yashab](../strongs/h/h3427.md)

## Eat

- ['akal](../strongs/h/h398.md)
- ['ăḵal](../strongs/h/h399.md)
- ['oklah](../strongs/h/h402.md)
- [bārâ](../strongs/h/h1262.md)
- [lāḥam](../strongs/h/h3898.md)
- [lechem](../strongs/h/h3899.md)
- [ra`ah](../strongs/h/h7462.md)


## Evil

- [beliya'al](../strongs/h/h1100.md)
- ['aven](../strongs/h/h205.md)
- [yāraʿ](../strongs/h/h3415.md)
- [ra'](../strongs/h/h7451.md)
- [rōaʿ](../strongs/h/h7455.md)
- [ra'a'](../strongs/h/h7489.md)
- [resha'](../strongs/h/h7562.md)

## Face

- ['aph](../strongs/h/h639.md)
- [paniym](../strongs/h/h6440.md)

## Fear

- [ʿāraṣ](../strongs/h/h6206.md)
- [bahal](../strongs/h/h926.md)
- [dᵊḥal](../strongs/h/h1763.md)
- [guwr](../strongs/h/h1481.md)
- [ḥāraḡ](../strongs/h/h2727.md)
- [ḥārēḏ](../strongs/h/h2729.md)
- [ḥārēḏ](../strongs/h/h2730.md)
- [ḥāṯaṯ](../strongs/h/h2865.md)
- [māḡôr](../strongs/h/h4032.md)
- [mowra'](../strongs/h/h4172.md)
- [pachad](../strongs/h/h6342.md)
- [rāʿaš](../strongs/h/h7493.md)
- [rāhâ](../strongs/h/h7297.md)
- [ragaz](../strongs/h/h7264.md)
- [śāʿar](../strongs/h/h8175.md)
- [yare'](../strongs/h/h3372.md)
- [yārē'](../strongs/h/h3373.md)
- [yāḡôr](../strongs/h/h3016.md)
- [yāḡōr](../strongs/h/h3025.md)
- [yir'ah](../strongs/h/h3374.md)

## Fight

- [lāḥam](../strongs/h/h3898.md)
- [milḥāmâ](../strongs/h/h4421.md)
- [maʿărāḵâ](../strongs/h/h4634.md)
- [ṣᵊḇā'](../strongs/h/h6633.md)
- [ṣāḇâ](../strongs/h/h6638.md)
- [rîḇ](../strongs/h/h7379.md)

## Forgive

- [kāp̄ar](../strongs/h/h3722.md)
- [nasa'](../strongs/h/h5375.md)
- [sālaḥ](../strongs/h/h5545.md)
- [sallāḥ](../strongs/h/h5546.md)

## Glory

- [hadar](../strongs/h/h1926.md)
- [kabowd](../strongs/h/h3519.md)
- [howd](../strongs/h/h1935.md)
- [tip̄'ārâ](../strongs/h/h8597.md)
- [ṣᵊḇî](../strongs/h/h6643.md)

> [Glory](http://www.dabhand.org/Word%20Studies/Glory.htm)

## God

- ['abiyr](../strongs/h/h46.md)
- ['adonay](../strongs/h/h136.md)
- ['el](../strongs/h/h410.md)
- ['ĕlâ](../strongs/h/h426.md)
- ['Elohiym](../strongs/h/h430.md)
- ['ĕlvôha](../strongs/h/h433.md)
- [Yahh](../strongs/h/h3050.md)
- [Yĕhovah](../strongs/h/h3068.md)
- [Yᵊhōvâ](../strongs/h/h3069.md)
- [Yᵊhōvâ yir'ê](../strongs/h/h3070.md)
- [Yᵊhōvâ nissî](../strongs/h/h3071.md)
- [Yᵊhōvâ Ṣiḏqēnû](../strongs/h/h3072.md)
- [Yᵊhōvâ Šālôm](../strongs/h/h3073.md)
- [Šaday](../strongs/h/h7706.md)

## Good

- [yatab](../strongs/h/h3190.md)
- [towb](../strongs/h/h2896.md)
- [ṭûḇ](../strongs/h/h2898.md)

## Hate

- [shâʼṭ](../strongs/h/h7590.md)
- [śāṭam](../strongs/h/h7852.md)
- [sane'](../strongs/h/h8130.md)

## Hear

- ['azan](../strongs/h/h238.md)
- ['anah](../strongs/h/h6030.md)
- [qashab](../strongs/h/h7181.md)
- [shama'](../strongs/h/h8085.md)
- [šᵊmaʿ](../strongs/h/h8086.md)
- [šēmaʿ](../strongs/h/h8088.md)

## Holy

- [qadowsh](../strongs/h/h6918.md)
- [qadash](../strongs/h/h6942.md)
- [qodesh](../strongs/h/h6944.md)

## Idol

- [ʿāṣāḇ](../strongs/h/h6091.md)
- ['aven](../strongs/h/h205.md)
- ['ĕlîl](../strongs/h/h457.md)
- ['etseb](../strongs/h/h6089.md)
- [gillûl](../strongs/h/h1544.md)
- [ḥammān](../strongs/h/h2553.md)
- [mip̄leṣeṯ](../strongs/h/h4656.md)
- [ʿōṣeḇ](../strongs/h/h6090.md)
- [pecel](../strongs/h/h6459.md)
- [semel](../strongs/h/h5566.md)
- [ṣîr](../strongs/h/h6736.md)
- [tᵊrāp̄îm](../strongs/h/h8655.md)

## Joy

- [gîl](../strongs/h/h1524.md)
- [gîlâ](../strongs/h/h1525.md)
- [giyl](../strongs/h/h1523.md)
- [ḥeḏvâ](../strongs/h/h2304.md)
- [ḥeḏvâ](../strongs/h/h2305.md)
- [māśôś](../strongs/h/h4885.md)
- [ranan](../strongs/h/h7442.md)
- [rannēn](../strongs/h/h7444.md)
- [rinnah](../strongs/h/h7440.md)
- [samach](../strongs/h/h8055.md)
- [śāśôn](../strongs/h/h8342.md)
- [simchah](../strongs/h/h8057.md)
- [śûś](../strongs/h/h7797.md)
- [tᵊrûʿâ](../strongs/h/h8643.md)
- [ṭûḇ](../strongs/h/h2898.md)

## Judgment

- [shaphat](../strongs/h/h8199.md)
- [šep̄eṭ](../strongs/h/h8201.md)

## Keep

- [nāṣar](../strongs/h/h5341.md)
- [nāṭar](../strongs/h/h5201.md)
- [shamar](../strongs/h/h8104.md)

## Kill

- [ḥālāl](../strongs/h/h2491.md)
- [harag](../strongs/h/h2026.md)
- [ḥāram](../strongs/h/h2763.md)
- [muwth](../strongs/h/h4191.md)
- [qāṭal](../strongs/h/h6991.md)
- [qᵊṭal](../strongs/h/h6992.md)
- [šāḥaṭ](../strongs/h/h7819.md)
- [ṭāḇaḥ](../strongs/h/h2873.md)
- [ṭeḇaḥ](../strongs/h/h2874.md)
- [zabach](../strongs/h/h2076.md)

## Know

- [da'ath](../strongs/h/h1847.md)
- [nāḵar](../strongs/h/h5234.md)
- [yada'](../strongs/h/h3045.md)

## Lamb

- [ṭᵊlî](../strongs/h/h2922.md)
- [ṭālê](../strongs/h/h2924.md)
- [keḇeś](../strongs/h/h3532.md)
- [kiḇśâ](../strongs/h/h3535.md)
- [kar](../strongs/h/h3733.md)
- [keśeḇ](../strongs/h/h3775.md)
- [kiśbâ](../strongs/h/h3776.md)
- ['immar](../strongs/h/h563.md)
- [tso'n](../strongs/h/h6629.md)
- [śê](../strongs/h/h7716.md)

## Land

- ['ăḏāmâ](../strongs/h/h127.md)
- ['erets](../strongs/h/h776.md)

## Law

- [dāṯ](../strongs/h/h1881.md)
- [diyn](../strongs/h/h1779.md)
- [choq](../strongs/h/h2706.md)
- [ḥāqaq](../strongs/h/h2710.md)
- [ḥāṯan](../strongs/h/h2859.md)
- [ḥāṯān](../strongs/h/h2860.md)
- [mitsvah](../strongs/h/h4687.md)
- [mishpat](../strongs/h/h4941.md)
- [towrah](../strongs/h/h8451.md)

## Light

- [ma'owr](../strongs/h/h3974.md)
- ['owr](../strongs/h/h215.md)
- ['owr](../strongs/h/h216.md)

## Live

- [chay](../strongs/h/h2416.md)
- [chayay](../strongs/h/h2425.md)
- [ḥālāṣ](../strongs/h/h2504.md)
- [ḥay](../strongs/h/h2417.md)
- [ḥăyā'](../strongs/h/h2418.md)
- [ḥāyâ](../strongs/h/h2421.md)

## Loins

- [ḥăraṣ](../strongs/h/h2783.md)
- [kesel](../strongs/h/h3689.md)
- [māṯnayim](../strongs/h/h4975.md)
- [yārēḵ](../strongs/h/h3409.md)

## Lust

- ['āvâ](../strongs/h/h183.md)
- ['aûâ](../strongs/h/h185.md)
- [kacaph](../strongs/h/h3700.md)
- [chamad](../strongs/h/h2530.md)
- [šᵊrîrûṯ](../strongs/h/h8307.md)
- [ta'avah](../strongs/h/h8378.md)

## Man

- ['adam](../strongs/h/h120.md)
- ['iysh](../strongs/h/h376.md)

## Mercy

- [chanan](../strongs/h/h2603.md)
- [checed](../strongs/h/h2617.md)
- [racham](../strongs/h/h7355.md)
- [raḥam](../strongs/h/h7356.md)

## Mighty

- ['abîr](../strongs/h/h47.md)
- ['abiyr](../strongs/h/h46.md)
- ['addiyr](../strongs/h/h117.md)
- ['ammîṣ](../strongs/h/h533.md)
- ['āp̄îq](../strongs/h/h650.md)
- [ʿāṣam](../strongs/h/h6105.md)
- ['atsuwm](../strongs/h/h6099.md)
- [ʿārîṣ](../strongs/h/h6184.md)
- [ʿăyām](../strongs/h/h5868.md)
- ['az](../strongs/h/h5794.md)
- ['el](../strongs/h/h410.md)
- ['Elohiym](../strongs/h/h430.md)
- ['êṯān](../strongs/h/h386.md)
- [gabar](../strongs/h/h1396.md)
- [gadowl](../strongs/h/h1419.md)
- [geḇer](../strongs/h/h1397.md)
- [gibār](../strongs/h/h1401.md)
- [gibôr](../strongs/h/h1368.md)
- [gᵊḇûrâ](../strongs/h/h1369.md)
- [ḥāzāq](../strongs/h/h2389.md)
- [kabîr](../strongs/h/h3524.md)
- ['oz](../strongs/h/h5797.md)
- [rab](../strongs/h/h7227.md)
- [šallîṭ](../strongs/h/h7989.md)
- [taqqîp̄](../strongs/h/h8624.md)
- ['ûl](../strongs/h/h193.md)
- [zerowa'](../strongs/h/h2220.md)

## Not

- ['în](../strongs/h/h369.md)
- ['în](../strongs/h/h371.md)
- ['al](../strongs/h/h408.md)
- ['al](../strongs/h/h409.md)
- ['im](../strongs/h/h518.md)
- ['ep̄es](../strongs/h/h657.md)
- [lō'](../strongs/h/h3808.md)
- [lā'](../strongs/h/h3809.md)

## Peace

- [shalowm](../strongs/h/h7965.md)
- [shalam](../strongs/h/h7999.md)
- [šālēm](../strongs/h/h8003.md)

## People

- [ben](../strongs/h/h1121.md)
- ['ummah](../strongs/h/h523.md)
- [gowy](../strongs/h/h1471.md)
- [lĕom](../strongs/h/h3816.md)
- [`edah](../strongs/h/h5712.md)
- [`am](../strongs/h/h5971.md)
- [ʿam](../strongs/h/h5972.md)

## Perfect

- [gāmar](../strongs/h/h1584.md)
- [gᵊmar](../strongs/h/h1585.md)
- [kālal](../strongs/h/h3634.md)
- [miḵlâ](../strongs/h/h4357.md)
- [šālēm](../strongs/h/h8003.md)
- [taḵlîṯ](../strongs/h/h8503.md)
- [tām](../strongs/h/h8535.md)
- [tamam](../strongs/h/h8552.md)
- [tamiym](../strongs/h/h8549.md)
- [tom](../strongs/h/h8537.md)

## Plague

- [magēp̄â](../strongs/h/h4046.md)
- [makâ](../strongs/h/h4347.md)
- [neḡaʿ](../strongs/h/h5061.md)
- [nāḡap̄](../strongs/h/h5062.md)
- [neḡep̄](../strongs/h/h5063.md)

## Praise

- [halal](../strongs/h/h1984.md)
- [hillûl](../strongs/h/h1974.md)
- [shabach](../strongs/h/h7623.md)
- [tehillah](../strongs/h/h8416.md)
- [tôḏâ](../strongs/h/h8426.md)
- [yadah](../strongs/h/h3034.md)
- [zamar](../strongs/h/h2167.md)

## Pray

- [ʿāṯar](../strongs/h/h6279.md)
- [bᵊʿā'](../strongs/h/h1156.md)
- [ḥālâ](../strongs/h/h2470.md)
- [pāḡaʿ](../strongs/h/h6293.md)
- [palal](../strongs/h/h6419.md)
- [ṣᵊlâ](../strongs/h/h6739.md)
- [śîaḥ](../strongs/h/h7878.md)

## Pride

- [gē'â](../strongs/h/h1344.md)
- [ga'avah](../strongs/h/h1346.md)
- [gā'ôn](../strongs/h/h1347.md)
- [ge'uwth](../strongs/h/h1348.md)
- [gobahh](../strongs/h/h1363.md)
- [gēvâ](../strongs/h/h1466.md)
- [gēvâ](../strongs/h/h1467.md)
- [zāḏôn](../strongs/h/h2087.md)
- [zûḏ](../strongs/h/h2103.md)
- [rōḵes](../strongs/h/h7407.md)
- [šaḥaṣ](../strongs/h/h7830.md)

## Redeem

- [gā'al](../strongs/h/h1350.md)
- [gᵊ'ullâ](../strongs/h/h1353.md)
- [pāḏâ](../strongs/h/h6299.md)
- [pᵊḏûṯ](../strongs/h/h6304.md)
- [paraq](../strongs/h/h6561.md)

## Rejoice

- [giyl](../strongs/h/h1523.md)
- [samach](../strongs/h/h8055.md)

## Rest

- [dᵊmî](../strongs/h/h1824.md)
- [damam](../strongs/h/h1826.md)
- [ḥāḏal](../strongs/h/h2308.md)
- [mānôaḥ](../strongs/h/h4494.md)
- [mᵊnûḥâ](../strongs/h/h4496.md)
- [margôaʿ](../strongs/h/h4771.md)
- [naḥaṯ](../strongs/h/h5183.md)
- [nûaḥ](../strongs/h/h5118.md)
- [nuwach](../strongs/h/h5117.md)
- [pûḡâ](../strongs/h/h6314.md)
- [rāḡaʿ](../strongs/h/h7280.md)
- [šᵊ'ār](../strongs/h/h7605.md)
- [šᵊ'ār](../strongs/h/h7606.md)
- [šᵊ'ērîṯ](../strongs/h/h7611.md)
- [šᵊlâ](../strongs/h/h7954.md)
- [šā'an](../strongs/h/h7599.md)
- [šāʿan](../strongs/h/h8172.md)
- [šā'ar](../strongs/h/h7604.md)
- [šāmaṭ](../strongs/h/h8058.md)
- [šāqaṭ](../strongs/h/h8252.md)
- [śārîḏ](../strongs/h/h8300.md)
- [shabath](../strongs/h/h7673.md)
- [šabāṯôn](../strongs/h/h7677.md)
- [shakab](../strongs/h/h7901.md)
- [shakan](../strongs/h/h7931.md)
- [yāṯar](../strongs/h/h3498.md)
- [yeṯer](../strongs/h/h3499.md)

## River

- ['ûḇāl](../strongs/h/h180.md)
- ['āp̄îq](../strongs/h/h650.md)
- [yᵊ'ōr](../strongs/h/h29.md)
- [yûḇāl](../strongs/h/h3105.md)
- [nᵊhar](../strongs/h/h5103.md)
- [nachal](../strongs/h/h5158.md)
- [peleḡ](../strongs/h/h6388.md)
- [pᵊlagâ](../strongs/h/h6390.md)
- [tᵊʿālâ](../strongs/h/h8585.md)

## Seek

- [bᵊʿā'](../strongs/h/h1156.md)
- [bāqar](../strongs/h/h1239.md)
- [bāqaš](../strongs/h/h1245.md)
- [baqqārâ](../strongs/h/h1243.md)
- [chaqar](../strongs/h/h2713.md)
- [darash](../strongs/h/h1875.md)
- [qārā'](../strongs/h/h7125.md)
- [šāḥar](../strongs/h/h7836.md)
- [tûr](../strongs/h/h8446.md)

## Sin

- [chata'](../strongs/h/h2398.md)
- [ḥăṭā'â](../strongs/h/h2401.md)
- [chatta'ath](../strongs/h/h2403.md)
- [`avon](../strongs/h/h5771.md)

## Sign

- ['ôṯ](../strongs/h/h226.md)
- [môp̄ēṯ](../strongs/h/h4159.md)

## Sing

- [ranan](../strongs/h/h7442.md)
- [shiyr](../strongs/h/h7891.md)
- [zamar](../strongs/h/h2167.md)

## Slay

(See kill)

## Sleep

- [yashen](../strongs/h/h3462.md)
- [yāšēn](../strongs/h/h3463.md)
- [rāḏam](../strongs/h/h7290.md)
- [shakab](../strongs/h/h7901.md)
- [šᵊnâ](../strongs/h/h8139.md)
- [šēnā'](../strongs/h/h8142.md)
- [šᵊnāṯ](../strongs/h/h8153.md)
- [tardēmâ](../strongs/h/h8639.md)

## Smite

- [dāḵā'](../strongs/h/h1792.md)
- [hālam](../strongs/h/h1986.md)
- [kāṯaṯ](../strongs/h/h3807.md)
- [māḥaṣ](../strongs/h/h4272.md)
- [naga`](../strongs/h/h5060.md)
- [nāḡap̄](../strongs/h/h5062.md)
- [nakah](../strongs/h/h5221.md)
- [sāp̄aq](../strongs/h/h5606.md)

## Statutes

- [choq](../strongs/h/h2706.md)
- [chuqqah](../strongs/h/h2708.md)

## Stranger

- [ger](../strongs/h/h1616.md)
- [nāḵrî](../strongs/h/h5237.md)
- [zûr](../strongs/h/h2114.md)

## Strength

- [gᵊḇûrâ](../strongs/h/h1369.md)
- [ḥayil](../strongs/h/h2428.md)
- [koach](../strongs/h/h3581.md)
- [māʿôz](../strongs/h/h4581.md)
- ['ôn](../strongs/h/h202.md)
- ['oz](../strongs/h/h5797.md)

## Tabernacle

- ['ohel](../strongs/h/h168.md)
- [miškān](../strongs/h/h4908.md)
- [sōḵ](../strongs/h/h5520.md)
- [cukkah](../strongs/h/h5521.md)
- [Sikûṯ](../strongs/h/h5522.md)
- [śōḵ](../strongs/h/h7900.md)

## Temple

- [bayith](../strongs/h/h1004.md)
- [heykal](../strongs/h/h1964.md)
- [hêḵal](../strongs/h/h1965.md)
- [raqqâ](../strongs/h/h7541.md)

## Trespass

- [māʿal](../strongs/h/h4603.md)
- [maʿal](../strongs/h/h4604.md)
- [pesha'](../strongs/h/h6588.md)
- ['asham](../strongs/h/h816.md)
- ['āšām](../strongs/h/h817.md)
- ['ašmâ](../strongs/h/h819.md)

## Trust

- [batach](../strongs/h/h982.md)
- [chacah](../strongs/h/h2620.md)

## Wall
- ['uššarnā'](../strongs/h/h846.md)
- [geḏer](../strongs/h/h1444.md)
- [gāḏēr](../strongs/h/h1447.md)
- [gᵊḏērâ](../strongs/h/h1448.md)
- [ḥômâ](../strongs/h/h2346.md)
- [cheyl](../strongs/h/h2426.md)
- [ḥayiṣ](../strongs/h/h2434.md)
- [kōṯel](../strongs/h/h3796.md)
- [kᵊṯal](../strongs/h/h3797.md)
- [qîr](../strongs/h/h7023.md)
- [šûrâ](../strongs/h/h7791.md)
- [šûr](../strongs/h/h7792.md)
- [šārâ](../strongs/h/h8284.md)

## Well

- [bᵊ'ēr](../strongs/h/h875.md)
- ['ayin](../strongs/h/h5869.md)
- [kēn](../strongs/h/h3651.md)
- [shalowm](../strongs/h/h7965.md)
- [towb](../strongs/h/h2896.md)
- [yatab](../strongs/h/h3190.md)

## Wickedness

- ['amal](../strongs/h/h5999.md)
- ['aven](../strongs/h/h205.md)
- ['evel](../strongs/h/h5766.md)
- [havvah](../strongs/h/h1942.md)
- [ra'](../strongs/h/h7451.md)
- [rōaʿ](../strongs/h/h7455.md)
- [rāšaʿ](../strongs/h/h7561.md)
- [resha'](../strongs/h/h7562.md)
- [rišʿâ](../strongs/h/h7564.md)
- [zimmâ](../strongs/h/h2154.md)

## Wisdom
- [ḥakîm](../strongs/h/h2445.md)
- [ḥāḵam](../strongs/h/h2449.md)
- [ḥāḵām](../strongs/h/h2450.md)
- [ḥāḵmâ](../strongs/h/h2451.md)
- [ḥāḵmâ](../strongs/h/h2452.md)
- [ḥāḵmôṯ](../strongs/h/h2454.md)

## Word

- ['emer](../strongs/h/h561.md)
- [dabar](../strongs/h/h1697.md)
- [millâ](../strongs/h/h4405.md)

## Work

- ['abad](../strongs/h/h5647.md)
- [mĕla'kah](../strongs/h/h4399.md)
- [ma'aseh](../strongs/h/h4639.md)
- [pa'al](../strongs/h/h6466.md)
- [pe'ullah](../strongs/h/h6468.md)
- [pōʿal](../strongs/h/h6467.md)
- [yad](../strongs/h/h3027.md)

## Wrath

- ['aph](../strongs/h/h639.md)
- [chemah](../strongs/h/h2534.md)
- [charown](../strongs/h/h2740.md)
- [`ebrah](../strongs/h/h5678.md)
- [kāʿas](../strongs/h/h3707.md)
- [ka`ac](../strongs/h/h3708.md)
- [qāṣap̄](../strongs/h/h7107.md)
- [qᵊṣap̄](../strongs/h/h7109.md)
- [qeṣep̄](../strongs/h/h7110.md)
- [rᵊḡaz](../strongs/h/h7265.md)
- [rōḡez](../strongs/h/h7267.md)
- [zaʿap̄](../strongs/h/h2197.md)
