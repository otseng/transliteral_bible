# Hebrew and Greek differences

| | Hebrew | Greek |
|  ---  |  ---  |  ---  |
| Writing direction | Right to left | Left to right |
| Writing relative to lines | On top of line | Below line |
| Vowels | No vowel letters | Has vowel letters |
| Lowercase | No lowercase | Has lowercase |
| Letters | Pictorial | Phonetic |
| Looking forward | Past | Future |
| Thought | Concrete | Abstract |
| Descriptions | Functional | Descriptive |
| Word emphasis  | Verb | Noun |
| Concepts       | Concrete | Abstract |

## Articles

[Ancient Hebrew](https://www.ancient-hebrew.org/language/philosophy-of-the-hebrew-language.htm)

## Videos

* [Think Hebrew, Not Greek](https://www.youtube.com/watch?v=LV8I6jiB-Lk&list=PLrJBb0vHkKaAoW8PABY-GM1HX5ilX4VPX&index=5&t=0s)
* [Western vs. Eastern Thinking / Gentile vs. Hebrew Mindsets](https://www.youtube.com/watch?v=Oxag2b2BsQU&t=159s)
* [The Hebrew Vs the Greek Mindset](https://www.youtube.com/watch?v=OjOi5s1ZQZo)
