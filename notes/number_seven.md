# Number seven

* Perfection
* Completeness
* Order
* Oath
* Guidance

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/7.html)

[Bible Zero](https://www.biblezero.com/number-7-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-7/)