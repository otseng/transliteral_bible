# Examples of adding and subtracting from the Bible

## Adding

Words in italics are added to the Bible that does not exist in the original text.

Original text does not have book names, chapter and verse references.

## Subtracting

Words that are difficult to translate are sometimes left untranslated.  

In [Matt 1:23 NIV](https://www.blueletterbible.org/niv/mat/1/9/s_930023), it does not translate [idou](../strongs/g/g2400.md).