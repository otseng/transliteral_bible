# Extrabiblical quotations

Jude/Enoch

https://blog.world-mysteries.com/science/the-forbidden-book-of-enoch/

The quote in (Jude 14-15) & (1 Enoch 1:9) is as follows: “In the seventh (generation) from Adam Enoch also prophesied these things, saying: ‘Behold, the Lord came with his holy myriads, to execute judgment on all, and to convict all the ungodly of all their ungodly deeds which they have committed in such an ungodly way, and of all the harsh things which ungodly sinners spoke against him’.”
