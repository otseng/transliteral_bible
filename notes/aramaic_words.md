# Aramaic words in Bible

https://en.wikipedia.org/wiki/Language_of_Jesus
http://www.learnthebible.org/aramaic-in-the-new-testament.html
https://bustedhalo.com/ministry-resources/what-verses-in-the-new-testament-are-aramaic

[Akeldama](../../strongs/g/g184.md)

[Act 1:19](../bible/acts/acts_1.md#acts_1_19) And it was known unto all the dwellers at Jerusalem; insomuch as that field is called in their proper tongue, Aceldama, that is to say, The field of blood.

[ephphatha](../../strongs/g/g2188.md)

[Mar 7:34](../bible/mark/mark_7.md#mark_7_34) And looking up to heaven, he sighed, and saith unto him, [ephphatha](../../strongs/g/g2188.md), that is, Be opened.

[elōï](../../strongs/g/g1682.md), [elōï](../../strongs/g/g1682.md), [lema](../../strongs/g/g2982.md) [sabachthani](../../strongs/g/g4518.md)

[Mar 15:34](../bible/mark/mark_15.md#mark_15_34) And at the ninth hour Jesus cried with a loud voice, saying, [elōï](../../strongs/g/g1682.md), [elōï](../../strongs/g/g1682.md), [lema](../../strongs/g/g2982.md) [sabachthani](../../strongs/g/g4518.md)? which is, being interpreted, My God, my God, why hast thou forsaken me?