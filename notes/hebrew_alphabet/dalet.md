# Dalet

## Hebrew

ד

## Script

![dalet](https://www.etz-hayim.com/hebrew/media/dalet5.jpg)

![dalet](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Dalet/dalet-forms.gif)

## Sound

d

## Frequency

[2.59%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Δ δ Delta](https://en.wikipedia.org/wiki/Delta_(letter))

## Meaning

Entrance, hang, move

## Pictograph

Door

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Dalet)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/dalet.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137076/jewish/Dalet.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Dalet/dalet.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/dalet.php)

[Free Hebrew and Greek Bible](https://freehebrewandgreekbible.wordpress.com/tag/dalet-%d7%93/)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-dalet-%d7%93/)
