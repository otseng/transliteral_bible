# Tav, Taw

## Hebrew

ת

## Script

![tav](https://www.etz-hayim.com/hebrew/media/tav5.jpg)

![tav](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tav/tav-forms.gif)

## Sound

t

## Frequency

[5.01%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Τ τ - Tau](https://en.wikipedia.org/wiki/Tau)

## Meaning

Mark, cross, sign, truth, life

## Pictograph

Crossed sticks

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Taw)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/tav.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137287/jewish/Tav.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tav/tav.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/tav.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-tav/)
