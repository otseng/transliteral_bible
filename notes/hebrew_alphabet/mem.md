# Mem

## Hebrew

מ ם 

## Script

![mem](https://www.etz-hayim.com/hebrew/media/mem5.jpg)

![mem](https://www.etz-hayim.com/hebrew/media/mem-sofit5.jpg)

## Sound

m

## Frequency

מ
[4.59](https://www.sttmedia.com/characterfrequency-hebrew)

ם
[3.03%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Μ μ - Mu](https://en.wikipedia.org/wiki/Mu_(letter))

## Meaning

Mighty, chaos

## Pictograph

Water, blood

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Mem)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/mem.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137085/jewish/Mem.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Mem/mem.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/mem.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-mem-%d7%9e/)

## Videos

[Living Word](https://www.thelivingword.org.au/grand-design/session31.php)