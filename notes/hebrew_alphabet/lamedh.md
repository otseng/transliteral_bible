# Lamedh, Lamed

## Hebrew

ל 

## Script

![lamed](https://www.etz-hayim.com/hebrew/media/lamed5.jpg)

![lamed](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Lamed/lamed-forms.gif)

## Sound 

l

## Frequency

[7.39%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Λ λ - Lambda](https://en.wikipedia.org/wiki/Lambda)

## Meaning

Learn, teach, study, direct, yoke, bind

## Pictograph

Shepherd's staff

## Notes

* Tallest letter, only letter extends above top line
* Center of alephbet

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Lamedh)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/lamed.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137084/jewish/Lamed.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Lamed/lamed.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/lamed.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-lamed-%d7%9c/)
