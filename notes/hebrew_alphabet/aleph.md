# Aleph, Alef

## Hebrew

א

## Script

![aleph](https://www.etz-hayim.com/hebrew/media/aleph5.jpg)

![aleph](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Aleph/aleph-write.gif)

## Sound

(silent)

## Frequency

[6.34%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Α α - Alpha](https://en.wikipedia.org/wiki/Alpha)

## Meaning

Power, Authority, Strength

## Pictograph

Ox head

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Aleph)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/aleph.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137073/jewish/Aleph.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Aleph/aleph.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/aleph.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-alef-%d7%90/)

[Free Hebrew and Greek Bible](https://freehebrewandgreekbible.wordpress.com/2013/01/01/aleph/)

[Haaretz](https://www.haaretz.com/israel-news/culture/.premium-the-one-and-only-alphabet-and-the-elephant-1.5312275)

[Study Light](https://www.studylight.org/language-studies/hebrew-thoughts.html?article=535)

## Videos

[Living Word](https://www.thelivingword.org.au/grand-design/session19.php)
