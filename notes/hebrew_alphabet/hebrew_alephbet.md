# Hebrew Alephbet

| #   | Name | Letter | Meaning | Value | Greek |
| --- | ---  | ---    | ---     | ---   | ---   |
| 1  | [Aleph](aleph.md) | א  | authority | 1, 1000 | Α α - Alpha |
| 2  | [Bet](bet.md) | ב | house | 2 | Β β - Beta |
| 3  | [Gimel](gimel.md) | ג | reward | 3 | Γ γ Gamma |
| 4  | [Dalet](dalet.md) | ד | door | 4 | Δ δ Delta |
| 5  | [He](he.md) | ה | divine revelation | 5 | Ε ε - Epsilon |
| 6  | [Vav](vav.md) | ו | hook | 6 | Υ υ - Upsilon |
| 7  | [Zayin](zayin.md) | ז | sword | 7 | Ζ ζ - Zeta |
| 8  | [Heth](heth.md) | ח | life | 8 | Η η - Eta |
| 9  | [Teth](teth.md) | ט | basket | 9 | Θ θ - Theta |
| 10 | [Yodh](yodh.md) | י | power | 10 | Ι ι - Iota |
| 11 | [Kaph](kaph.md) | כ ך | hand | 20 | Κ κ - Kappa |
| 12 | [Lamedh](lamedh.md) | ל | learn, teach | 30 | Λ λ - Lambda |
| 13 | [Mem](mem.md) | מ ם | water | 40 | Μ μ - Mu |
| 14 | [Nun](nun.md) | נ ן | increase | 50 | Ν ν - Nu |
| 15 | [Samech](samech.md) | ס | support, Jewish star | 60 | Ξ ξ - Xi |
| 16 | [Ayin](ayin.md) | ע | watch | 70 | Ω ω - Omega |
| 17 | [Pay](pe.md) | פ ף | mouth | 80 | Π π - Pi |
| 18 | [Tsade](tsade.md) | צ ץ | trail, righteous | 90 | (none) |
| 19 | [Qof](qof.md) | ק | cycle | 100 | (none) |
| 20 | [Resh](resh.md) | ר | head | 200 | Ρ ρ - Rho |
| 21 | [Shin](shin.md) | ש | fire, tooth | 300 | Σ σ ς - Sigma |
| 22 | [Tav](tav.md) | ת | seal | 400 | Τ τ - Tau |
|    | Kaf   | ך  | | 500 | 
|    | Mem   | מ  | | 600 |
|    | Nun   | ן  | | 700 |
|    | Pay   | ף | | 800 |
|    | Tsade | ץ | | 900 |

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Hebrew_alphabet)

[Study Light](https://www.studylight.org/lexicons/hebrew/ahl_alphabet.html)

[Aleph-Bet Trainer](https://www.chabad.org/library/howto/trainer_cdo/aid/138150/jewish/Aleph-Bet-Trainer.htm#!1228/v27)

[The Hebrew Alphabet](https://www.chabad.org/library/article_cdo/aid/4069287/jewish/The-Hebrew-Alphabet.htm)

[The Meaning of the Hebrew Alphabet](http://www.abarim-publications.com/Hebrew_Alphabet_Meaning.html)

[Spiritual Meanings of the Hebrew Alphabet Letters](http://www.walkingkabbalah.com/hebrew-alphabet-letter-meanings/)

[Hebrew Alphabet Chart](https://www.ancient-hebrew.org/alphabet/hebrew-alphabet-chart.htm)

[Free Hebrew and Greek Bible](https://freehebrewandgreekbible.wordpress.com/aleph-bet-chart/)

[Gnostic Teachings](https://gnosticteachings.org/courses/alphabet-of-kabbalah.html)

[Jewish Virtual Library](https://www.jewishvirtuallibrary.org/the-hebrew-alphabet-aleph-bet)

[Isopsephy](https://en.wikipedia.org/wiki/Isopsephy)

[Chaim Bentorah](https://www.chaimbentorah.com/background/)

## Videos

[Hebrew Aleph Bet](https://www.youtube.com/playlist?list=PLrJBb0vHkKaAoW8PABY-GM1HX5ilX4VPX)
