# He, Hey, Heh

## Hebrew
	
ה

## Script

![he](https://www.etz-hayim.com/hebrew/media/hey5.jpg)

![he](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Hey/hey-forms.gif)

## Sound

h, eh

## Frequency

[10.87%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ε ε - Epsilon](https://en.wikipedia.org/wiki/Epsilon)

## Meaning

[see, behold](https://www.blueletterbible.org/lexicon/h2009)

## Pictograph

Arms raised

## Links

[Wikipedia](https://en.wikipedia.org/wiki/He_(letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/hey.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137077/jewish/Heh.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Hey/hey.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/hey.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-hei-%d7%94/)
