# Ayin, ayen

## Hebrew

ע 

## Script

![ayin](https://www.etz-hayim.com/hebrew/media/ayin5.jpg)

![ayin](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Ayin/ayin-forms.gif)

## Sound

(silent)

## Frequency

[3.23%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ω ω - Omega](https://en.wikipedia.org/wiki/Omega)

[Ο ο - Omicron](https://en.wikipedia.org/wiki/Omicron)

## Meaning

Watch, Know, Shade

## Pictograph

Eye

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Ayin)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/ayin.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137088/jewish/Ayen.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Ayin/ayin.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/ayin.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-ain-%d7%a2/)
