# Vav, Waw

## Hebrew

ו

## Script

![vav](https://www.etz-hayim.com/hebrew/media/vav5.jpg)

![vav](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Vav/vav-forms.gif)

## Sound

v, w, ow, uw

## Frequency

[10.38%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Υ υ - Upsilon](https://en.wikipedia.org/wiki/Upsilon)

## Meaning

Secure, attach

## Pictograph

Hook, peg, tent peg

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Waw_(letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/vav.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137078/jewish/Vav.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Vav/vav.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/vav.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-vav-%D7%95/)
