# Gimel

## Hebrew

ג 

## Script

![gimel](https://www.etz-hayim.com/hebrew/media/gimmel5.jpg)

![gimel](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Gimmel/gimmel-forms.gif)

## Sound

g

## Frequency

[1.30%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Γ γ Gamma](https://en.wikipedia.org/wiki/Gamma)

## Meaning

Reward, walk, gather

## Pictograph

Foot

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Gimel)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/gimel.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137075/jewish/Gimmel.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Gimmel/gimmel.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/gimmel.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-hebrew-letter-gimel-%d7%92/)
