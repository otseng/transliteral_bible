# Pe, Pey

## Hebrew

פ

## Script

![pey](https://www.etz-hayim.com/hebrew/media/peystm1.jpg)

![pey](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Pey/pey-forms.gif)

## Sound

p, ph (f)

## Frequency

פ
[1.69%](https://www.sttmedia.com/characterfrequency-hebrew)

ף
[0.27%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Π π - Pi](https://en.wikipedia.org/wiki/Pi_(letter))

## Meaning

[ox](https://www.blueletterbible.org/lexicon/h504)

## Pictograph

mouth

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Pe_(Semitic_letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/pey.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137089/jewish/Pey-Fey.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Pey/pey.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/pey.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-peh-%d7%a4/)
