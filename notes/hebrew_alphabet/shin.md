# Shin, sheen, sin

## Hebrew

ש 

## Script

![shin](https://www.etz-hayim.com/hebrew/media/shin5.jpg)

![shin](https://www.etz-hayim.com/hebrew/media/sin5.jpg)

![shin](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Shin/shin-forms.gif)

## Frequency

[4.41%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Σ σ ς - Sigma](https://en.wikipedia.org/wiki/Sigma)

## Meaning

Sharp, eat, press

## Pictograph

Tooth, water

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Shin_(letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/shin.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137093/jewish/Shin-Sin.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Shin/shin.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/shin.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-shin-%d7%a9/)
