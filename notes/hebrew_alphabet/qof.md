# Qof, Kof, Quph, Qoph

## Hebrew

ק

## Script

![kof](https://www.etz-hayim.com/hebrew/media/kof5.jpg)

![kof](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Qof/qof-forms.gif)

## Sound

q, k

## Frequency

[2.14%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ϙ](https://en.wikipedia.org/wiki/Koppa_(letter)) - (archaic letter)

## Meaning

cycle, circle, time, revolve

## Pictograph

Sun at horizon

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Qoph)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/quph.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137091/jewish/Kuf.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Qof/qof.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/kof.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-kuf/)
