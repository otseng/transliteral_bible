# Yodh, Yod, Yud

## Hebrew

י

## Script

![yud](https://www.etz-hayim.com/hebrew/media/yud5.jpg)

![yud](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Yod/yod-forms.gif)

## Sound

y, id

## Frequency

[11.06%](https://www.sttmedia.com/characterfrequency-hebrew) - most frequent letter

## Greek

[Ι ι - Iota](https://en.wikipedia.org/wiki/Iota)

## Meaning

Power, work, praise, throw, make

## Pictograph

Hand, arm

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Yodh)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/yud.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137082/jewish/Yod.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Yod/yod.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/yud.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-yud-%d7%99/)
