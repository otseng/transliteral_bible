# Resh, Rosh

## Hebrew

ר

## Script

![resh](https://www.etz-hayim.com/hebrew/media/resh5.jpg)

![resh](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Resh/resh-forms.gif)

## Sound

r

## Frequency

[5.61%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ρ ρ - Rho](https://en.wikipedia.org/wiki/Rho)

## Meaning

head, first, chief, beginning

## Pictograph

head of a man

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Resh)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/resh.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137092/jewish/Resh.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Resh/resh.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/resh.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-resh-%d7%a8/)
