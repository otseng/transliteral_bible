# Bet

## Hebrew

ב

## Script

![bet](https://www.etz-hayim.com/hebrew/media/bet5.jpg)

![bet](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Bet/betstyles.gif)

## Sound

b

## Frequency

[4.74%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Β β - beta](https://en.wikipedia.org/wiki/Beta)

## Meaning

[house](https://www.blueletterbible.org/lexicon/h1004)

## Pictograph

Floorplan of a tent

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Bet_(letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/beyt.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137074/jewish/Bet-Vet.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Bet/bet.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/bet.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-hebrew-letter-bet/)

[Free Hebrew and Greek Bible](https://freehebrewandgreekbible.wordpress.com/2013/01/01/beyt/)

## Videos

[Living Word](https://www.thelivingword.org.au/grand-design/session20.php)
