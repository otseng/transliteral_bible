# Nun

## Hebrew

נ ן

## Script

![nun](https://www.etz-hayim.com/hebrew/media/nun5.jpg)

![nun sofit](https://www.etz-hayim.com/hebrew/media/nun-sofit5.jpg)

![nun](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Nun/nun-forms.gif)

## Sound

n

## Frequency

נ
[2.86%](https://www.sttmedia.com/characterfrequency-hebrew)

ן
[1.10%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ν ν - Nu](https://en.wikipedia.org/wiki/Nu_(letter))

## Meaning

Continue, Heir, Son

## Pictograph

Sprouting seed, snake, fish

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Nun_(letter))

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/nun.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137086/jewish/Nun.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Nun/nun.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/nun.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-nun-%d7%a0/)
