# Kaph, Kaf, Khaf, Chaf

## Hebrew

כ ך 

## Script

![kaf](https://www.etz-hayim.com/hebrew/media/kaf5.jpg)

![kaf](https://www.etz-hayim.com/hebrew/media/khaf-sofit5.jpg)

## Sound

k, kh

## Frequency

כ
[2.70%](https://www.sttmedia.com/characterfrequency-hebrew)

ך
[0.81%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Κ κ - Kappa](https://en.wikipedia.org/wiki/Kappa)

## Meaning

Bend, allow, spoon

## Pictograph

Open palm, hand

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Kaph)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/kaph.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137083/jewish/Kaf-Chaf.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Kaf/kaf.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/kaf.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-kaf-%d7%9b/)
