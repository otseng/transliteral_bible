# Teth, Tet, Tes, Thet

## Hebrew

ט

## Script

![tet](https://www.etz-hayim.com/hebrew/media/tet5.jpg)

![tet](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tet/tet-forms.gif)

## Sound

th

## Frequency

[1.24%](https://www.sttmedia.com/characterfrequency-hebrew) - least used

## Greek

[Θ θ - Theta](https://en.wikipedia.org/wiki/Theta)

## Meaning

Contain, good, surround, mud

## Pictograph

Basket, wheel

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Teth)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/tet.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137081/jewish/Tet.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tet/tet.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/tet.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-tel-%d7%98/)
