# Zayin

## Hebrew

ז

## Script

![zayin](https://www.etz-hayim.com/hebrew/media/zayin5.jpg)

![zayin](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Zayin/zayin-forms.gif)

## Sound

z

## Frequency

[1.33%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ζ ζ - Zeta](https://en.wikipedia.org/wiki/Zeta)

## Meaning

Food, cut, nourish

## Pictograph

Plow, sword

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Zayin)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/zayin.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137079/jewish/Zayin.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Zayin/zayin.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/zayin.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-zine-%d7%96/)
