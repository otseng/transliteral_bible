# Samech

## Hebrew

ס 

## Script

![samekh](https://www.etz-hayim.com/hebrew/media/samekh5.jpg)

![samekh](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Samekh/samekh-forms.gif)

## Sound

s

## Frequency

[1.48%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ξ ξ - Xi](https://en.wikipedia.org/wiki/Xi_(letter))

## Meaning

Grab, Protect, Pierce, Support

## Pictograph

Thorn, Shield

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Samekh)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/samehh.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137087/jewish/Samech.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Samekh/samekh.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/samekh.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-samech-%d7%a1/)
