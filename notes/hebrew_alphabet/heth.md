# Heth, Het, Chet

## Hebrew

ח 

## Script

![het](https://www.etz-hayim.com/hebrew/media/het5.jpg)

![het](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Chet/chet-forms.gif)

## Sound

guttural h

## Frequency

[2.48%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Η η - Eta](https://en.wikipedia.org/wiki/Eta)

## Meaning

Life, outside

## Pictograph

Wall

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Heth)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/hhet.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137080/jewish/Chet.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Chet/chet.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/het.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-het-%d7%97/)
