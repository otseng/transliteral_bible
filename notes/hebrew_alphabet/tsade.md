# Tsade, Tzadik, Sade 

## Hebrew

צ

## Script

![tsade](https://www.etz-hayim.com/hebrew/media/tsadestm1.jpg)

![tsade](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tsade/tsade-forms.gif)

## Sound

ts

## Frequency

צ
[1.24%](https://www.sttmedia.com/characterfrequency-hebrew)

ץ
[0.12%](https://www.sttmedia.com/characterfrequency-hebrew)

## Greek

[Ϻ](https://en.wikipedia.org/wiki/San_(letter)) - (archaic letter)

## Meaning

journey, hunt, chase, righteous, side

## Pictograph

trail, stronghold

## Links

[Wikipedia](https://en.wikipedia.org/wiki/Tsade)

[Ancient Hebrew](https://www.ancient-hebrew.org/ancient-alphabet/tsade.htm)

[Chabad](https://www.chabad.org/library/article_cdo/aid/137090/jewish/Tzadik.htm)

[Hebrew4Christians](https://www.hebrew4christians.com/Grammar/Unit_One/Aleph-Bet/Tsade/tsade.html)

[Etz Hayim](https://www.etz-hayim.com/hebrew/letters_and_vowels/tsade.php)

[Hebrew Today](https://hebrewtoday.com/alphabet/the-letter-tzadi-%d7%a6/)
