# Learning Greek

[12 Greek Words You Should Know](https://www.dailywritingtips.com/greek-words/)

[Greek prefixes](https://msu.edu/~defores1/gre/roots/gre_rts_afx_tab1.htm)

[Little Greek](http://www.ibiblio.org/koine/)

## Typing Greek

[TypeGreek](http://www.typegreek.com/)
## Bible in original language

[Biblical Text](https://biblicaltext.com/read/)