# Bible Commentaries

## Translations

[New English Translation](https://netbible.org/bible/Matthew+1)

[Revised English Version](https://www.revisedenglishversion.com/John/chapter3/16)

## Collections

[Precept Austin](https://www.preceptaustin.org/genesis_commentaries)

## Personal

[Enduring Word](https://enduringword.com/bible-commentary/matthew-1/)

## Commentaries on textual variations

* ASGTNT - A Student's Guide to the New Testament
* Berean Interlinear
* GNTvar - Greek New Testament variants

## Commentaries on word statistics

* BCW - Bonnell's Combined Word Book 

## Commentaries book, chapter summaries

* Brooks Complete Summary