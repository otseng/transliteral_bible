# [Hebrew acrostics](pdf/hebrew_acrostics.pdf)

Psalm 9-10 Each Hebrew consonant covers two verses. These two psalms Van der Spuy: Hebrew Alphabetic Acrostics OTE 21/2 (2008), 513-532 515 form one acrostic unit. Because of text-critical problems and the fact that they are presented as two separate Psalms, they are not always included in the list of acrostics. In the Septuagint they constitute one psalm.

Psalm 25 Each Hebrew consonant covers 1 verse.

Psalm 34 Each Hebrew consonant covers 1 verse.

Psalm 37 Each Hebrew consonant covers 2 verses.

Psalm 111 Each Hebrew consonant covers ½ verse.

Psalm 112 Each Hebrew consonant covers ½ verse.

Psalm 119 Each Hebrew consonant covers 8 verses.

Psalm 145 Each Hebrew consonant covers 1 verse.

Lam 1- 4 In chapter 1 and 2 each Hebrew consonant covers 1 verse which consists of 3 stanzas. In chapter 3 each consonant covers 3 stanzas/verses, therefore it has 66 verses. Chapter 4 has 22 verses, each consonant consists of 2 stanzas beginning with that letter of the alphabet. Chapter 5 has 22 verses, but is not an alphabetic acrostic.

Prov 31: 10 –31 Each Hebrew consonant covers 1 verse.

Nahum 1: 1- 9 The Aleph covers three lines. There seems to be an interjection of 2 lines before the rest of the consonants, which covers only one verse each. The letter zayin appears in the second position of the
line.
