# Acrostic Psalms

Psalm 9 - 2 verses for each of the 22 Hebrew consonants
Psalm 10 - 2 verses each
Psalm 25 - 1 verse each
Psalm 34 - 1 verse each
Psalm 37 - 2 verses each
Psalm 111 - ½ verse each
Psalm 112 - ½ verse each
Psalm 119 - 8 verses each
Psalm 145 - 1 verse each
