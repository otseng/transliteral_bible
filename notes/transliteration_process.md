# Transliteration process

## New Testamant

Use text from KJV

- [KJV with Strong's](https://www.blueletterbible.org/kjv/jhn/1/1/p0/rl1/t_conc_998001)

## Old Testament

### Hebrew (Masoretic)

Use text from KJV

- [KJV with Strong's](https://www.blueletterbible.org/kjv/gen/1/1/t_conc_1001)

### Greek (Septuagint)

Use text from Brenton translation

- [Brenton translation](http://ecmarsh.com/lxx-kjv/genesis/gen_001.htm)

Lookup Strong's number

- [Apostolic Bible translation](https://www.apostolicbible.com/text.htm)
