# Online Bible Translations

[Blue Letter Bible](https://www.blueletterbible.org)

[Marvel.bible](https://marvel.bible/)

[Conservative Bible](https://www.conservapedia.com/Conservative_Bible)

[Literal Idiomatic Translation](http://www.believershomepage.com/)

[Mechon Mamre](https://www.mechon-mamre.org)

[NET Bible](https://netbible.org/bible/Matthew+1)

[Katabiblion](https://en.katabiblon.com/us/index.php?text=LXX&book=Gn&ch=1&interlin=on&diacritics=off)

* Septuagint

[Bible Web App](http://www.biblewebapp.com/study/)

[Kalvesmaki](http://www.kalvesmaki.com/LXX/Index.htm)

[QBible](http://qbible.com/brenton-septuagint/)

* Hebrew

[BibleBento](https://biblebento.com/index.html?bhs&10.1.1)

[Hebrew Faith Bible](https://hebraicfaithbible.com/)

* Septuagint

[BibleBento](https://biblebento.com/index.html?lxx1&10.1.1)

* Greek

[BibleBento](https://biblebento.com/index.html?nestle1904i&470.1.1)

[OpenGNT](https://opengnt.com/)

* Commentaries

[Bible Go](https://www.biblego.org/read)

* Links

https://www.logosapostolic.org/bible_study_tools_index.htm
