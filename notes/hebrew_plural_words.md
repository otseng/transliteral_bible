# Hebrew Plural Words

Elohim

Kipurrim

The word kippurim 'atonement' is one of many Biblical Hebrew words which, while using a grammatical plural form, refers to a singular abstract concept.
https://en.wikipedia.org/wiki/Yom_Kippur

