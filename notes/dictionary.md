# Dictionaries

## Hebrew

[Ancient Hebrew](https://www.ancient-hebrew.org/dictionary/ancient-aleph.html)

## Etymology

[Etymonline](https://www.etymonline.com)

## General

[Memidex](http://www.memidex.com/)

## Theology

[Baker's Evangelical Dictionary of Biblical Theology](https://www.studylight.org/dictionaries/bed.html)]