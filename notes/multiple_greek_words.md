# Greek synonyms

## Links

- [Christ Words](http://christswords.com/node/1289)
- [NT Words](http://ntwords.com/resurrect.htm)
- [Sermon Index](http://www.sermonindex.net/modules/articles/index.php?view=article&aid=34210)

## Abide

- [aulizomai](../strongs/g/g835.md)
- [anastrephō](../strongs/g/g390.md)
- [diamenō](../strongs/g/g1265.md)
- [diatribō](../strongs/g/g1304.md)
- [epimenō](../strongs/g/g1961.md)
- [menō](../strongs/g/g3306.md)
- [paramenō](../strongs/g/g3887.md)
- [prosmenō](../strongs/g/g4357.md)
- [hypomenō](../strongs/g/g5278.md)

## Beware

- [blepō](../strongs/g/g991.md)
- [prosechō](../strongs/g/g4337.md)

## Blessed

- [eulogeō](../strongs/g/g2127.md)
- [eulogētos](../strongs/g/g2128.md)
- [makarios](../strongs/g/g3107.md)

## Child

- [huios](../strongs/g/g5207.md)
- [paidion](../strongs/g/g3813.md)
- [pais](../strongs/g/g3816.md)
- [teknon](../strongs/g/g5043.md)

## Choose

- [haireō](../strongs/g/g138.md)
- [hairetizō](../strongs/g/g140.md)
- [epilegō](../strongs/g/g1951.md)
- [procheirizō](../strongs/g/g4400.md)
- [procheirotoneō](../strongs/g/g4401.md)
- [eklegomai](../strongs/g/g1586.md)
- [cheirotoneō](../strongs/g/g5500.md)

## Clean

- [katharizō](../strongs/g/g2511.md)
- [katharos](../strongs/g/g2513.md)

## Clothe

- [amphiennymi](../strongs/g/g294.md)
- [endidyskō](../strongs/g/g1737.md)
- [endyō](../strongs/g/g1746.md)
- [himatizō](../strongs/g/g2439.md)
- [periballō](../strongs/g/g4016.md)

## Come

- [deuro](../strongs/g/g1204.md)
- [eiserchomai](../strongs/g/g1525.md)
- [ephistēmi](../strongs/g/g2186.md)
- [eperchomai](../strongs/g/g1904.md)
- [erchomai](../strongs/g/g2064.md)
- [ginomai](../strongs/g/g1096.md)
- [hēkō](../strongs/g/g2240.md)
- [katabainō](../strongs/g/g2597.md)
- [katantaō](../strongs/g/g2658.md)
- [katerchomai](../strongs/g/g2718.md)
- [paraginomai](../strongs/g/g3854.md)
- [phthanō](../strongs/g/g5348.md)

## Command

- [eipon](../strongs/g/g2036.md)
- [entellō](../strongs/g/g1781.md)
- [epitassō](../strongs/g/g2004.md)
- [paraggellō](../strongs/g/g3853.md)

## Commandment

- [entolē](../strongs/g/g1785.md)
- [epitagē](../strongs/g/g2003.md)
- [entalma](../strongs/g/g1778.md)
- [diatagma](../strongs/g/g1297.md)

## Cry

- [alalazō](../strongs/g/g214.md)
- [anaboaō](../strongs/g/g310.md)
- [anakrazō](../strongs/g/g349.md)
- [boaō](../strongs/g/g994.md)
- [boē](../strongs/g/g995.md)
- [epiboaō](../strongs/g/g1916.md)
- [epiphōneō](../strongs/g/g2019.md)
- [keleusma](../strongs/g/g2752.md)
- [klaiō](../strongs/g/g2799.md)
- [krazō](../strongs/g/g2896.md)
- [kraugazō](../strongs/g/g2905.md)
- [kraugē](../strongs/g/g2906.md)
- [phōneō](../strongs/g/g5455.md)

## Curse

- [anathema](../strongs/g/g331.md)
- [anathematizō](../strongs/g/g332.md)
- [kakologeō](../strongs/g/g2551.md)
- [katathema](../strongs/g/g2652.md)
- [katathematizō](../strongs/g/g2653.md)
- [katara](../strongs/g/g2671.md)
- [kataraomai](../strongs/g/g2672.md)

## Deceive

- [apataō](../strongs/g/g538.md)
- [exapataō](../strongs/g/g1818.md)
- [paralogizomai](../strongs/g/g3884.md)
- [phrenapataō](../strongs/g/g5422.md)
- [planaō](../strongs/g/g4105.md)
- [pseudomai](../strongs/g/g5574.md)

## Despise

- [atheteō](../strongs/g/g114.md)
- [exoudeneō](../strongs/g/g1847.md)
- [exoutheneō](../strongs/g/g1848.md)
- [kataphroneō](../strongs/g/g2706.md)
- [oligōreō](../strongs/g/g3643.md)
- [periphroneō](../strongs/g/g4065.md)
- [atimazō](../strongs/g/g818.md)

## Destroy

- [apokteinō](../strongs/g/g615.md)
- [apollymi](../strongs/g/g622.md)
- [diaphtheirō](../strongs/g/g1311.md)
- [exolethreuō](../strongs/g/g1842.md)
- [katalyō](../strongs/g/g2647.md)
- [katargeō](../strongs/g/g2673.md)
- [kathaireō](../strongs/g/g2507.md)
- [lyō](../strongs/g/g3089.md)
- [olothreuō](../strongs/g/g3645.md)
- [phtheirō](../strongs/g/g5351.md)
- [portheō](../strongs/g/g4199.md)
- [phthora](../strongs/g/g5356.md)

## Disciple

- [mathētēs](../strongs/g/g3101.md)
- Akoloutheō
- Mimeomai
- Opisō

## Doubt

- [diakrinō](../strongs/g/g1252.md)
- [diaporeō](../strongs/g/g1280.md)
- [distazō](../strongs/g/g1365.md)

## Dwell

- [skēnoō](../strongs/g/g4637.md)
- [oikeō](../strongs/g/g3611.md)
- [enoikeō](../strongs/g/g1774.md)
- [menō](../strongs/g/g3306.md)
- [skēnoō](../strongs/g/g4637.md)
- katoikeo

## Eat

- [bibrōskō](../strongs/g/g977.md)
- [esthiō](../strongs/g/g2068.md)
- [trōgō](../strongs/g/g5176.md)
- [phago](../strongs/g/g5315.md)

## End

- [synteleia](../strongs/g/g4930.md)
- [telos](../strongs/g/g5056.md)

## Eternal

- [aiōnios](../strongs/g/g166.md)
- [aïdios](../strongs/g/g126.md)

## Evil

- [kakos](../strongs/g/g2556.md)
- [ponēros](../strongs/g/g4190.md)

## Fast

- [asitos](../strongs/g/g777.md)
- [nēsteuō](../strongs/g/g3522.md)

## Fight

- [agōn](../strongs/g/g73.md)
- [agōnizomai](../strongs/g/g75.md)
- [athlēsis](../strongs/g/g119.md)
- [machomai](../strongs/g/g3164.md)
- [palē](../strongs/g/g3823.md)
- [polemeō](../strongs/g/g4170.md)
- [polemos](../strongs/g/g4171.md)
- [pykteuō](../strongs/g/g4438.md)

## Finish

- [apartismos](../strongs/g/g535.md)
- [apoteleō](../strongs/g/g658.md)
- [dianyō](../strongs/g/g1274.md)
- [ekteleō](../strongs/g/g1615.md)
- [exartizō](../strongs/g/g1822.md)
- [epiteleō](../strongs/g/g2005.md)
- [synteleō](../strongs/g/g4931.md)
- [teleioō](../strongs/g/g5048.md)
- [teleō](../strongs/g/g5055.md)

## Fish

- [opsarion](../strongs/g/g3795.md)
- ichthys

## Forgive

- [aphiēmi](../strongs/g/g863.md)
- [apolyō](../strongs/g/g630.md)
- [charizomai](../strongs/g/g5483.md)

> [Christ's Words](https://christswords.com/content/forgive-leave-and-let)

## Gift

- [charis](../strongs/g/g5485.md)
- [charisma](../strongs/g/g5486.md)
- [dōrea](../strongs/g/g1431.md)
- [dōrēma](../strongs/g/g1434.md)
- [dōron](../strongs/g/g1435.md)
- [pneumatikos](../strongs/g/g4152.md)

## Glory

- [doxa](../strongs/g/g1391.md)
- [doxazō](../strongs/g/g1392.md)
- [kauchaomai](../strongs/g/g2744.md)
- [kenodoxos](../strongs/g/g2755.md)

> [Dabhand](http://www.dabhand.org/Word%20Studies/Glory.htm)

## Good

- [agathos](../strongs/g/g18.md)
- ethlos
- [eu](../strongs/g/g2095.md)
- [eudokia](../strongs/g/g2107.md)
- [chrēstos](../strongs/g/g5543.md)
- [kalos](../strongs/g/g2570.md)

## Heal

- [iaomai](../strongs/g/g2390.md)
- [sōzō](../strongs/g/g4982.md)
- [therapeuō](../strongs/g/g2323.md)

## Hear

- [akouō](../strongs/g/g191.md)
- [epakroaomai](../strongs/g/g1874.md)
- [eisakouō](../strongs/g/g1522.md)

## Holy

- [hagios](../strongs/g/g40.md)
- [hieros](../strongs/g/g2413.md)
- [hosios](../strongs/g/g3741.md)

## Jerusalem

- [Hierosolyma](../strongs/g/g2414.md)
- [Ierousalēm](../strongs/g/g2419.md)

## Joy

- [agalliasis](../strongs/g/g20.md)
- [chairō](../strongs/g/g5463.md)
- [chara](../strongs/g/g5479.md)
- [euphrosynē](../strongs/g/g2167.md)
- [kauchaomai](../strongs/g/g2744.md)

## Kill

- [anaireō](../strongs/g/g337.md)
- [apokteinō](../strongs/g/g615.md)
- [diacheirizō](../strongs/g/g1315.md)
- [katasphazō](../strongs/g/g2695.md)
- [nekroō](../strongs/g/g3499.md)
- [olothreuō](../strongs/g/g3645.md)
- [patassō](../strongs/g/g3960.md)
- [phoneuō](../strongs/g/g5407.md)
- [sphazō](../strongs/g/g4969.md)
- [thyō](../strongs/g/g2380.md)

## Kind

- [chrēsteuomai](../strongs/g/g5541.md)
- [chrēstos](../strongs/g/g5543.md)

## Know

- [eidō](../strongs/g/g1492.md)
- [epiginōskō](../strongs/g/g1921.md)
- [epistamai](../strongs/g/g1987.md)
- [ginōskō](../strongs/g/g1097.md)

## Laid

- [epiballō](../strongs/g/g1911.md)
- [epitithēmi](../strongs/g/g2007.md)
- [prostithēmi](../strongs/g/g4369.md)
- [tithēmi](../strongs/g/g5087.md)

## Lamb

- [amnos](../strongs/g/g286.md)
- [arēn](../strongs/g/g704.md)
- [arnion](../strongs/g/g721.md)

## Learn

- [manthanō](../strongs/g/g3129.md)

## Lord

- [despotēs](../strongs/g/g1203.md)
- [kyrios](../strongs/g/g2962.md)
- [rhabbouni](../strongs/g/g4462.md)

## Look

- [atenizō](../strongs/g/g816.md)
- [blepō](../strongs/g/g991.md)
- [eidō](../strongs/g/g1492.md)
- [emblepō](../strongs/g/g1689.md)
- [horasis](../strongs/g/g3706.md)
- [idou](../strongs/g/g2400.md)
- [episkeptomai](../strongs/g/g1980.md)
- [optanomai](../strongs/g/g3700.md)
- [prosdokaō](../strongs/g/g4328.md)
- [skopeō](../strongs/g/g4648.md)
- [theaomai](../strongs/g/g2300.md)
- [theōreō](../strongs/g/g2334.md)

## Love

- [agapaō](../strongs/g/g25.md)
- [phileō](../strongs/g/g5368.md)

> [Christ's Words](https://christswords.com/content/love-agape-phileo-and-eros)

## Lust

- [aselgeia](../strongs/g/g766.md)
- [epipotheo](../strongs/g/g1971.md)
- [epithymia](../strongs/g/g1939.md)
- [epithymētēs](../strongs/g/g1938.md)
- [epithymeō](../strongs/g/g1937.md)
- [hēdonē](../strongs/g/g2237.md)
- [orexis](../strongs/g/g3715.md)
- [pathos](../strongs/g/g3806.md)

## Manifest

- [apokalyptō](../strongs/g/g601.md)
- [dēlos](../strongs/g/g1212.md)
- [ekdēlos](../strongs/g/g1552.md)
- [emphanēs](../strongs/g/g1717.md)
- [emphanizō](../strongs/g/g1718.md)
- [endeiknymi](../strongs/g/g1731.md)
- [epiphanēs](../strongs/g/g2016.md)
- [phaneroō](../strongs/g/g5319.md)
- [phaneros](../strongs/g/g5318.md)
- [prodēlos](../strongs/g/g4271.md)

## Master

- [didaskalos](../strongs/g/g1320.md)
- [epistatēs](../strongs/g/g1988.md)
- [kathēgētēs](../strongs/g/g2519.md)
- [kyrios](../strongs/g/g2962.md)

## Meat

- [brōma](../strongs/g/g1033.md)
- [phago](../strongs/g/g5315.md)
- [trophē](../strongs/g/g5160.md)

## Men

- [anthrōpos](../strongs/g/g444.md)
- [anēr](../strongs/g/g435.md)

## Mighty

- [biaios](../strongs/g/g972.md)
- [dynamis](../strongs/g/g1411.md)
- [dynastēs](../strongs/g/g1413.md)
- [dynatos](../strongs/g/g1415.md)
- [ischyros](../strongs/g/g2478.md)
- [ischys](../strongs/g/g2479.md)
- [krataios](../strongs/g/g2900.md)
- [kratos](../strongs/g/g2904.md)

## Mind

- [dianoia](../strongs/g/g1271.md)
- [nous](../strongs/g/g3563.md)

## Money

- [argyrion](../strongs/g/g694.md)
- [chalkos](../strongs/g/g5475.md)
- [chrēma](../strongs/g/g5536.md)

## Neighbor

- [geitōn](../strongs/g/g1069.md)
- [plēsion](../strongs/g/g4139.md)
- [perioikos](../strongs/g/g4040.md)

## Net

- [amphiblēstron](../strongs/g/g293.md)
- [diktyon](../strongs/g/g1350.md)
- [sagēnē](../strongs/g/g4522.md)

## Obey

- [hypakouō](../strongs/g/g5219.md)
- [peitharcheō](../strongs/g/g3980.md)

## People

- [anthrōpos](../strongs/g/g444.md)
- [dēmos](../strongs/g/g1218.md)
- [ethnos](../strongs/g/g1484.md)
- [laos](../strongs/g/g2992.md)
- [ochlos](../strongs/g/g3793.md)

## Perfect

- [akribōs](../strongs/g/g199.md)
- [artios](../strongs/g/g739.md)
- [epiteleō](../strongs/g/g2005.md)
- [katartizō](../strongs/g/g2675.md)
- [teleios](../strongs/g/g5046.md)

## Plague

- [loimos](../strongs/g/g3061.md)
- [mastix](../strongs/g/g3148.md)
- [nosos](../strongs/g/g3554.md)
- [plēgē](../strongs/g/g4127.md)

## Please

- [areskō](../strongs/g/g700.md)
- [eudokeō](../strongs/g/g2106.md)

## Power
- [dynamis](../strongs/g/g1411.md)
- [exousia](../strongs/g/g1849.md)
- [kratos](../strongs/g/g2904.md)

## Pray

- [aitēma](../strongs/g/g0155.md)
- [deomai](../strongs/g/g1189.md)
- [erōtaō](../strongs/g/g2065.md)
- [euchomai](../strongs/g/g2172.md)
- [parakaleō](../strongs/g/g3870.md)
- [proseuchomai](../strongs/g/g4336.md)

## Prayer

- [deēsis](../strongs/g/g1162.md)
- [enteuxis](../strongs/g/g1783.md)
- [euchē](../strongs/g/g2171.md)

> [Precept Austin]((https://www.preceptaustin.org/prayer-greek_words_for_prayer))
> [NT Words](http://ntwords.com/prayer1.htm)
> [Garth D. Wiebe](http://www.wiebefamily.org/prayer.htm)

## Prison

- [desmōtērion](../strongs/g/g1201.md)
- [phylakē](../strongs/g/g5438.md)
- [tērēsis](../strongs/g/g5084.md)

## Poor

- [ptōchos](../strongs/g/g4434.md)
- [penichros](../strongs/g/g3998.md)
- [penēs](../strongs/g/g3993.md)

## Power

- [dynamis](../strongs/g/g1411.md)
- [exousia](../strongs/g/g1849.md)

> [DocRob](http://s-studies.0catch.com/DocRob/power.htm)

## Redeem

- [exagorazō](../strongs/g/g1805.md)
- [lytroō](../strongs/g/g3084.md)
- [lytrōsis](../strongs/g/g3085.md)
- [agorazō](../strongs/g/g59.md)

## Repent

- [metanoia](../strongs/g/g3341.md)
- [metamelomai](../strongs/g/g3338.md)

## Rest

- [anapauō](../strongs/g/g373.md)
- [anapausis](../strongs/g/g372.md)
- [anesis](../strongs/g/g425.md)
- [eirēnē](../strongs/g/g1515.md)
- [epanapauomai](../strongs/g/g1879.md)
- [episkēnoō](../strongs/g/g1981.md)
- [epiloipos](../strongs/g/g1954.md)
- [hēsychazō](../strongs/g/g2270.md)
- [katapausis](../strongs/g/g2663.md)
- [katapauō](../strongs/g/g2664.md)
- [katheudō](../strongs/g/g2518.md)
- [kataskēnoō](../strongs/g/g2681.md)
- [koimēsis](../strongs/g/g2838.md)
- [loipos](../strongs/g/g3062.md)
- [sabbatismos](../strongs/g/g4520.md)

## Resurrect

- [anastasis](../strongs/g/g386.md)
- [anistēmi](../strongs/g/g450.md)
- [egeirō](../strongs/g/g1453.md)
- [egersis](../strongs/g/g1454.md)

## Ruler

- [archōn](../strongs/g/g758.md)
- [basileus](../strongs/g/g935.md)
- [hēgemōn](../strongs/g/g2232.md)
- [kosmokratōr](../strongs/g/g2888.md)

## Save

- [sōzō](../strongs/g/g4982.md)

## Say

- [eipon](../strongs/g/g2036.md)
- [eipon](../strongs/g/g2046.md)
- [laleō](../strongs/g/g2980.md)
- [legō](../strongs/g/g3004.md)
- [phēmi](../strongs/g/g5346.md)

## See

- [blepō](../strongs/g/g991.md)
- [horaō](../strongs/g/g3708.md)
- [optanomai](../strongs/g/g3700.md)

> [Abarim Publications](http://www.abarim-publications.com/DictionaryG/e/e-i-d-om.html)

## Serve

- [diakoneō](../strongs/g/g1247.md)
- [douleuō](../strongs/g/g1398.md)
- [latreuō](../strongs/g/g3000.md)

## Servant

- [diakonos](../strongs/g/g1249.md)
- [doulos](../strongs/g/g1401.md)
- [pais](../strongs/g/g3816.md)

## Sin

- [hamartanō](../strongs/g/g264.md)
- [paraptōma](../strongs/g/g3900.md)

> [Bible Food](http://www.biblefood.com/7wrdsin.html)
> [Study Bible](https://studybible.info/trench/Sin)

## Slay

(See kill)

## Smite

- [derō](../strongs/g/g1194.md)
- [paiō](../strongs/g/g3817.md)
- [patassō](../strongs/g/g3960.md)
- [plēssō](../strongs/g/g4141.md)
- [rhapizō](../strongs/g/g4474.md)
- [typtō](../strongs/g/g5180.md)

## Some

- [allos](../strongs/g/g243.md)
- [ek](../strongs/g/g1537.md)
- [heis](../strongs/g/g1520.md)
- [hos](../strongs/g/g3739.md) [men](../strongs/g/g3303.md)
- [tis](../strongs/g/g5100.md)

## Tabernacle

- [skēnē](../strongs/g/g4633.md)
- [skēnos](../strongs/g/g4636.md)
- [skēnōma](../strongs/g/g4638.md)

## Take

- [airō](../strongs/g/g142.md)
- [dechomai](../strongs/g/g1209.md)
- [katalambanō](../strongs/g/g2638.md)
- [krateō](../strongs/g/g2902.md)
- [lambanō](../strongs/g/g2983.md)
- [paralambanō](../strongs/g/g3880.md)
- [piazō](../strongs/g/g4084.md)
- [proslambanō](../strongs/g/g4355.md)
- [syllambanō](../strongs/g/g4815.md)
- [hypolambanō](../strongs/g/g5274.md)

## Talk

- [homileō](../strongs/g/g3656.md)

## Temple

- [hieron](../strongs/g/g2411.md)
- [naos](../strongs/g/g3485.md)

## Think

- [enthymeomai](../strongs/g/g1760.md)
- [nomizō](../strongs/g/g3543.md)
- [phroneō](../strongs/g/g5426.md)

## Time

- [chronos](../strongs/g/g5550.md)
- [kairos](../strongs/g/g2540.md)

## Tree

- [dendron](../strongs/g/g1186.md)
- [xylon](../strongs/g/g3586.md)

## Truly

- [alēthōs](../strongs/g/g230.md)
- [amēn](../strongs/g/g281.md)

## Trust

- [elpizō](../strongs/g/g1679.md)
- [pepoithēsis](../strongs/g/g4006.md)
- [peithō](../strongs/g/g3982.md)

## Verily

- [amēn](../strongs/g/g281.md)
- [nai](../strongs/g/g3483.md)

## Will

- [boulē](../strongs/g/g1012.md)
- [thelēma](../strongs/g/g2307.md)
- [thelō](../strongs/g/g2309.md)

## Wise

- [phronimos](../strongs/g/g5429.md)
- [sophos](../strongs/g/g4680.md)

## Wisdom

- [phronēsis](../strongs/g/g5428.md)
- [sophia](../strongs/g/g4678.md)

## Word

- [logos](../strongs/g/g3056.md)
- [rhēma](../strongs/g/g4487.md)

## World

- [aiōn](../strongs/g/g165.md)
- [kosmos](../strongs/g/g2889.md)
- [oikoumenē](../strongs/g/g3625.md)

## Worship

- [eusebeō](../strongs/g/g2151.md)
- [proskyneō](../strongs/g/g4352.md)
- [therapeuō](../strongs/g/g2323.md)
- [latreia](../strongs/g/g2999.md)
- [sebō](../strongs/g/g4576.md)

## Wrath

- [cholē](../strongs/g/g5521.md)
- [orgē](../strongs/g/g3709.md)
- [parorgismos](../strongs/g/g3950.md)
- [orgizō](../strongs/g/g3710.md)
- [thymos](../strongs/g/g2372.md)

## Vision

- [horama](../strongs/g/g3705.md)
- [optasia](../strongs/g/g3701.md)