# Hebrew words transliterated into Greek

[hallēlouïa](../strongs/g/g239.md)

[amēn](../strongs/g/g281.md)

[korban](../strongs/g/g2878.md)

[rhabbi](../strongs/g/g4461.md)

[rhaka](../strongs/g/g4469.md)

[sabbaton](../strongs/g/g4521.md)

[Satanas](../strongs/g/g4567.md)

[Sabaōth](../strongs/g/g4519.md)

[hōsanna](../strongs/g/g5614.md)
