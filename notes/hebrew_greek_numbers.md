## Hebrew numbers

[One](number_one.md)

* ['echad](../strongs/h/h259.md)

    * Deuteronomy 6:4 - Hear, O Israel: The LORD our God is one ('echad) LORD:

[Two](number_two.md)

* [šᵊnayim](../strongs/h/h8147.md)

    * Genesis 1:16 - And God made two (šᵊnayim) great lights; the greater light to rule the day, and the lesser light to rule the night: [he made] the stars also.

[Three](number_three.md)

* [šālôš](../strongs/h/h7969.md)

    * Jonah 1:17 -  Now the LORD had prepared a great fish to swallow up Jonah. And Jonah was in the belly of the fish three (šālôš) days and three (šālôš) nights.

[Four](number_four.md)

* ['arbaʿ](../strongs/h/h702.md)
* [rᵊḇîʿî](../strongs/h/h7243.md)

    * Genesis 1:19 And the evening and the morning were the fourth (rᵊḇîʿî) day.
    * Gen 2:10 And a river went out of Eden to water the garden; and from thence it was parted, and became into four ('arbaʿ) heads.

[Five](number_five.md)

* [ḥāmēš](../strongs/h/h2568.md)
* [ḥămîšî](../strongs/h/h2549.md)

    * Exodus 27:18 The length of the court [shall be] a hundred cubits, and the breadth fifty every where, and the height five (ḥāmēš) cubits [of] fine twined linen, and their sockets [of] brass.

[Six](number_six.md)

* [šēš](../strongs/h/h8337.md)
* [šiššî](../strongs/h/h8345.md)

    *  Gen 1:31 And God saw every thing that he had made, and, behold, it was very good. And the evening and the morning were the sixth (šiššî) day.
    * Gen 7:6 And Noah was six (šēš) hundred years old when the flood of waters was upon the earth.

[Seven](number_seven.md)

* [šeḇaʿ](../strongs/h/h7651.md)

    * Gen 7:2 Of every clean beast thou shalt take to thee by sevens (šeḇaʿ), the male and his female: and of beasts that are not clean by two, the male and his female.

[Eight](number_eight.md)

* [šᵊmōnê](../strongs/h/h8083.md)

    * Genesis 17:12 - And he that is eight (šᵊmōnê) days old shall be circumcised among you, every man child in your generations, he that is born in the house, or bought with money of any stranger, which [is] not of thy seed.

[Nine](number_nine.md)

* [tēšaʿ](../strongs/h/h8672.md)

[Ten](number_ten.md)

* [ʿeśer](../strongs/h/h6235.md)
* [ʿāśôr](../strongs/h/h6218.md)

[Forty](number_forty.md)

* ['arbāʿîm](../strongs/h/h705.md)

    * Gen 7:4 - For yet seven days, and I will cause it to rain upon the earth forty ('arbāʿîm) days and forty nights; and every living substance that I have made will I destroy from off the face of the earth.

## Greek numbers

[One](number_one.md)

* [heis](../strongs/g/g1520.md)
* [mia](../strongs/g/g3391.md)

[Two](number_two.md)

* [dyo](../strongs/g/g1417.md)
* [dietēs](../strongs/g/g1332.md)

[Three](number_three.md)

* [treis](../strongs/g/g5140.md)

[Four](number_four.md)

* [tetartos](../strongs/g/g5067.md)
* [tessares](../strongs/g/g5064.md)

[Five](number_five.md)

* [pente](../strongs/g/g4002.md)

[Six](number_six.md)

* [ektos](../strongs/g/g1623.md)
* [ex](../strongs/g/g1803.md)

[Seven](number_seven.md)

* [hepta](../strongs/g/g2033.md)
* [heptakis](../strongs/g/g2034.md)

[Eight](number_eight.md)

* [ogdoos](../strongs/g/g3590.md)
* [oktō](../strongs/g/g3638.md)

[Nine](number_nine.md)

* [ennea](../strongs/g/g1767.md)

[Ten](number_ten.md)

* [deka](../strongs/g/g1176.md)

[Twelve](number_twelve.md)

* [dōdeka](../strongs/g/g1427.md)

[Forty](number_forty.md)

* [tessarakonta](../strongs/g/g5062.md)

## Links

[BibleStudy.org](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/1.html)

[Increasing Faith](https://increasingfaithintl.org/biblical-numerology/)

