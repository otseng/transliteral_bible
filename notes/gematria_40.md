## Gematria 40

Represents:
* transition or change
* concept of renewal
* new beginning

[Wikipedia](https://en.wikipedia.org/wiki/40_(number))

[Aish](https://aish.com/the-number-40/)

[Betemunah](https://www.betemunah.org/forty.html)

[Jewish Encyclopedia](https://www.jewishencyclopedia.com/articles/6248-forty-the-number)

[BibleStudy.org](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/40.html)

[Fathers of Mercy](https://fathersofmercy.com/the-significance-of-40-in-scripture/)
