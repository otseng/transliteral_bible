# Bible Translating Tools

[unfoldingWord](https://www.unfoldingword.org)

[unfoldingWord Translation Academy](https://door43.org/u/unfoldingWord/en_ta/master/)

[unfoldingWord Open Bible Stories](https://door43.org/u/Door43-Catalog/en_obs/77268476b8/)

[Door43 Forum](https://forum.door43.org/)