# Bible stats

There are 929 chapters in the Old Testament. There are 260 chapters in the New Testament. This gives a total of 1,189 chapters.

There are 23,145 verses in the Old Testament and 7,957 verses in the New Testament. This gives a total of 31,102 verses, which is an average of a little more than 26 verses per chapter.

Translate 22 verses per day can finish New Testament in one year.