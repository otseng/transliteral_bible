# Number forty

* testing
* trial
* probation

# Passages

* Gen 7:4 - For yet seven days, and I will cause it to rain upon the earth forty days and forty nights; and every living substance that I have made will I destroy from off the face of the earth.
* Exo 16:35 - And the children of Israel did eat manna forty years, until they came to a land inhabited; they did eat manna, until they came unto the borders of the land of Canaan.
* Exod 24:18 - And Moses went into the midst of the cloud, and gat him up into the mount: and Moses was in the mount forty days and forty nights.
* Exo 34:28 – Moses was there with the Lord forty days and forty nights without eating bread or drinking water. And he wrote on the tablets the words of the covenant—the Ten Commandments.”
* Jonah 3:4 – Jonah began by going a day’s journey into the city, proclaiming, ‘Forty more days and Nineveh will be overthrown.'

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/40.html)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-40/)