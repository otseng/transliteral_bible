# Cross References

[Open Bible](https://www.openbible.info/labs/cross-references/)

## Visualizations

[Chris Harrison](https://www.chrisharrison.net/index.php/Visualizations/BibleViz)

[Viz.Bible](https://viz.bible/remaking-influential-cross-reference-visualization/)

[Open Bible](https://www.openbible.info/labs/cross-references/visualization)