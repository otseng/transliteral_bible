# Christian Terms

Cathedral

* Chair - [kathedra](../strongs/g/g2515.md)

Doxology

* Glory, praise - [doxa](../strongs/g/g1391.md)

Eucharist

* Thanksgiving - [eucharisteō](../strongs/g/g2168.md)

Hymn

* Sing praise - [hymneō](../strongs/g/g5214.md)
