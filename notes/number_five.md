# Number five

* Grace
* Mercy
* Power
* Provision

## Passages

* Mat 14:17 - And they say unto him, We have here but five loaves, and two fishes.
* Ephesians 4:11 - And he gave some, apostles; and some, prophets; and some, evangelists; and some, pastors and teachers;

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/5.html)

[Bible Zero](https://www.biblezero.com/number-5-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-5/)

[Mary Jane Humes](https://maryjanehumes.com/what-does-5-mean-in-the-bible/)
