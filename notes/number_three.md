# Number three

* Trinity
* Perfection
* Completeness
* Order
* Wholeness

# Passages

* Jonah 1:17 -  Now the LORD had prepared a great fish to swallow up Jonah. And Jonah was in the belly of the fish three days and three nights.

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/3.html)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-3/)

[Mary Jane Humes](https://maryjanehumes.com/what-does-the-number-3-mean-in-the-bible/)