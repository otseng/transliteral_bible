# Dubious, wrong, bad translations

## Matthew

[Mat 28:17 KJV] 17 And when they saw him, they worshipped him: but some doubted.

There is no word behind "some".  But why is it not italicized?

## Hebrews

[Heb 4:13 KJV] 13 Neither is there any creature that is not manifest in his sight: but all things [are] naked and opened unto the eyes of him with whom we have to do.

"To do" is [logos](../../strongs/g/g3056.md)

## Kai

[kai](../strongs/g/g2532.md) not translated 350 times

## Job

[Job 22:30 KJV] 30 He shall deliver the island of the innocent: and it is delivered by the pureness of thine hands.

"island" should be "not".  Word is spelled the same in Hebrew.  It is also the root word of Ichabad.

