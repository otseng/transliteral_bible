# Textual variants

[List of major textual variants in the New Testament](https://en.wikipedia.org/wiki/List_of_major_textual_variants_in_the_New_Testament)

[Textual variants in the New Testament](https://en.wikipedia.org/wiki/Textual_variants_in_the_New_Testament)

## Variants found

[Romans 12:11](../commentary/romans/romans_12_commentary.md#romans_12_11)

