# Number two

* Unity
* Division
* Balance
* Partnership

## Passages

* Genesis 1:16 - And God made two great lights; the greater light to rule the day, and the lesser light to rule the night: [he made] the stars also.

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/2.html)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-2/)