# Books of the Bible by date of authorship

## [Bible Gateway](https://www.biblegateway.com/blog/2016/02/when-was-each-book-of-the-bible-written/)

### Old Testament

|Book|Date|
|---|---|
|Job|Date unknown
|Genesis|1445-1405 BC
|Exodus|1445-1405 BC
|Leviticus|1445-1405 BC
|Numbers|1445-1405 BC
|Deuteronomy|1445-1405 BC
|Psalms|1410-450 BC
|Joshua|1405-1385 BC
|Judges|1043 BC
|Ruth|1030-1010 BC
|Song of Songs|971-965 BC
|Proverbs|971-686 BC
|Ecclesiastes|940-931 BC
|1 Samuel|931-722 BC
|2 Samuel|931-722 BC
|Obadiah|850-840 BC
|Joel|835-796 BC
|Jonah|775 BC
|Amos|750 BC
|Hosea|750-710 BC
|Micah|735-710 BC
|Isaiah|700-681 BC
|Nahum|650 BC
|Zephaniah|635-625 BC
|Habakkuk|615-605 BC
|Ezekiel|590-570 BC
|Lamentations|586 BC
|Jeremiah|586-570 BC
|1 Kings|561-538 BC
|Judith*|Uncertain (538 BC-AD 70)
|Daniel|536-530 BC
|Haggai|520 BC
|Baruch*|500-100 BC
|Zechariah|480-470 BC
|Ezra|457-444 BC
|1 Chronicles|450-430 BC
|2 Chronicles|450-430 BC
|Esther|450-331 BC
|Malachi|433-424 BC
|Nehemiah|424-400 BC
|Susanna*|400 BC-AD 70
|Psalm 151*|400 BC-AD 100
|Letter of Jeremiah*|307-317 BC
|Tobit*|225-175 BC
|Ben Sira (Sirach)*|200-175 BC
|Bel and the Dragon*|200-100 BC
|Greek Esther*|200-1 BC
|1 Maccabees*|150-100 BC
|2 Maccabees*|150-100 BC
|1 Esdras*|100 BC-AD 100
|Prayer of Manasseh*|100-1 BC
|3 Maccabees*|100-1 BC
|4 Maccabees*|100-1 BC
|Wisdom*|50-20 BC
|2 Esdras**|AD 100-200

### New Testament

|Book|Date|
|---|---|
|James|AD 44-49
|Galatians|AD 49-50
|Mark|AD 50-60
|Matthew|AD 50-60
|1 Thessalonians|AD 51
|2 Thessalonians|AD 51-52
|1 Corinthians|AD 55
|2 Corinthians|AD 55-56
|Romans|AD 56
|Luke|AD 60-61
|Ephesians|AD 60-62
|Philippians|AD 60-62
|Philemon|AD 60-62
|Colossians|AD 60-62
|Acts|AD 62
|1 Timothy|AD 62-64
|Titus|AD 62-64
|1 Peter|AD 64-65
|2 Timothy|AD 66-67
|2 Peter|AD 67-68
|Hebrews|AD 67-69
|Jude|AD 68-70
|John|AD 80-90
|1 John|AD 90-95
|2 John|AD 90-95
|3 John|AD 90-95
|Revelation|AD 94-96

## [Wikipedia](https://en.wikipedia.org/wiki/Dating_the_Bible)

### New Testament

|Book|Date|
|---|---|
|Galatians| 48 or 55 CE
|1 Thessalonians| 51 CE
|2 Thessalonians| 51 CE or post-70 CE
|1 Corinthians| 53-57 CE
|Philippians| 54–55 CE
|Philemon| 54–55 CE
|2 Corinthians| 55-58 CE
|Romans| 57-58 CE
|Colossians| 62–70 CE
|Mark| 65–73 CE
|James| 65–85 CE
|1 Peter| 75–90 CE
|Matthew| 80-90 CE
|Luke| 80–90 CE.
|Ephesians| 80–90 CE
|Acts| 80-90 CE
|Hebrews| 80–90 CE
|John| 90–110 CE
|1 John| 90–110 CE
|2 John| 90–110 CE
|3 John| 90–110 CE
|Revelation| 95 CE
|1 Timothy| 100 CE
|Titus| 100 CE
|2 Timothy| 100 CE
|2 Peter| 110 CE
|Jude| Uncertain
