# Verb Voices

Indicates the relationship of the subject to the verbal action.

3 types of voices:

* Active - action of the verb is being performed by the subject
* Middle - subject of the verb does action unto itself, or for its own benefit
* Passive - action of the verb being done unto the subject but not by the subject

---

Back to [Greek grammar](greek_grammar.md)