# Greek Grammar

* [Ancient Greek for Everyone](https://ancientgreek.pressbooks.com/front-matter/introduction/)
* [BCBSR](http://www.bcbsr.com/greek/grklnk.html?SUBMIT=Greek+Grammar+Menu)
* [Blue Letter Bible](https://www.blueletterbible.org/help/lexicalDefinitions.cfm#greek)
* [Dana-Mantey](http://icotb.org/resources/Dana-ManteyGreekGrammar.pdf)
* [Free Bible Commentary](http://www.freebiblecommentary.org/special_topics/greek_grammar.html)
* [Precept Austin](https://www.preceptaustin.org/greek_quick_reference_guide)
* [Smyth](http://www.ccel.org/s/smyth/grammar/html/toc.htm)
* [Wikipedia](https://en.wikipedia.org/wiki/Ancient_Greek_grammar)
* [Wikibooks](https://en.m.wikibooks.org/wiki/Koine_Greek)
* [Ezra Project](https://ezraproject.com/understanding-grammar/)

## Quick Reference

* [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/quick-reference-guide.cfm)

## Alphabet

* [Wikipedia](https://en.wikipedia.org/wiki/Greek_alphabet)

## [Nouns](greek_nouns.md)

* Gender - male, female, neuter
* Number - how many
* [Case](greek_cases.md) - function in the sentence

## [Verbs](greek_verbs.md)

* [Tense](greek_tenses.md) - when it happened and its permanence
* [Voice](greek_voices.md) - relationship of subject to action
* [Mood](greek_moods.md) - actuality or potentiality of the action

## Notes

Word order is not important.  Case endings determine meaning.

Words sound just like how it is spelled, unlike English where a word can have multiple pronunciations.

## Grammar mnemonic

### Verb

Tense = time and start end

Aorist - sometime in the past, no aspect, default past tense - "Aristotle"

Perfect - happened in past and is still occurring - "Permanent"

Voice - "voice" acting on self or another person

Active - subject is "acting" and performing the action

Passive - subject is "passively" receiving the action

Mood - depending on my "mood" I might do it

Indicative - "indicator" expresses a fact

Imperative - "important" request or entreaty

Subjective - "subjective" hypothetical situation

Infinitive - "to infinity and beyond".  translated with "to" in front of it (eg to see)

### Noun

Dative - part of a prepositional phrase - "Datum"

Accusative - the direct object of the sentence, verb is being performed on - Receives "accusation"

