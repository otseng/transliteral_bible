# Noun Cases

* Nominative - naming case, word is the subject of the sentence, object performing the action
  * "The ***angels*** came and ministered unto him."
  * [Wiktionary](https://en.wiktionary.org/wiki/nominative_case)
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/nominative-case.cfm)
  * [Ezra Project](https://ezraproject.com/nominative-case-uses/)

* Accusative - direct object, object receiving action
  * "Then, therefore, Pilate took and scourged ***Jesus***"
  * [Wiktionary](https://en.wiktionary.org/wiki/accusative_case)
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/accusative-case.cfm)
  * [Ezra Project](https://ezraproject.com/accusative-case-uses/)

* Dative - indirect object to a verb, often found inside a prepositional phrase
  * "See with what large letters I am writing to ***you*** with my own hand"
  * [Wiktionary](https://en.wiktionary.org/wiki/dative_case)
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/dative-case.cfm)
  * [Ezra Project](https://ezraproject.com/dative-case-uses/)

* Genitive - defines, describes, qualifies, restricts, limits, ownership, "of" or "by" something
  * "And when Jesus had come to the home ***of Peter***"
  * "but His disciples took him ***by night***"
  * [Wiktionary](https://en.wiktionary.org/wiki/genitive_case)
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/genitive-case.cfm)
  * [Ezra Project](https://ezraproject.com/genitive-case-uses/)

* Ablative - separation, movement away, "from, concerning, by, greater than"
  * "For in it the righteousness of God is revealed ***from faith*** to faith"
  * "concerning what was told them ***by the shepherds***"
  * "there has not arisen anyone greater than ***John the Baptist***"
  * [Wiktionary](https://en.wiktionary.org/wiki/ablative_case)
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/ablative-case.cfm)

* Vocative - addressing someone
  * "***Our Father***, which art in heaven.”
  * [Wiktionary](https://en.wiktionary.org/wiki/vocative_case)

* Instrumental - association, how something is done
  * "because he had often been bound ***with shackles and chains***"
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/instrumental-case.cfm)

* Locative - position, location
  * "***in the city*** of David"
  * [Blue Letter Bible](https://www.blueletterbible.org/resources/grammars/greek/simplified-greek/locative-case.cfm)

---

Back to [Greek grammar](greek_grammar.md)