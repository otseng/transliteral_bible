# Verb Tenses

[BCBSR](http://www.bcbsr.com/greek/gtense.html)
[Ezra Project](https://www.ezraproject.com/greek-tenses-explained)

Tense involves two parts: time (when it happened) and aspect (permanence)

Time:
Primary (present, future)
Secondary (past) - imperfect, aorist, perfect, pluperfect

Aspect:
Ongoing - ongoing habitual action
Completed - permanent lasting results
Simple - not marked as ongoing or completed

## Aorist

Past verb tense, simple verb aspect

Aorist is the default tense, especially when a writer is describing the past.  If in doubt, Greeks would use aorist.  If you see any other tense, you should suspect that it was used deliberately to make a point.  

[Wikipedia](https://en.wikipedia.org/wiki/Aorist_(Ancient_Greek))
[NT Greek](http://www.ntgreek.net/lesson22.htm#aorist)
[Ancient Greek](https://ancientgreek.pressbooks.com/chapter/31/)
[Bill Mounce](https://www.billmounce.com/monday-with-mounce/the-aorist-so-much-more-past-tense)

## Imperfect

Past verb tense, ongoing verb aspect

The imperfect tense is the ideal way to describe an action that was in the process of happening at some time in the past. 

mnemonic: "in progress"

"the angels were ministering to him" (Mark 1:13)

## Perfect

Past verb tense, permanent verb aspect

The perfect has results existing up to the time of speaking.

“For by grace you have been saved through faith” (Eph 2:8)

"I am crucified with Christ" (Gal 2:20)

## Pluperfect

Past verb tense, permanent verb aspect

The pluperfect makes no comment about the results existing up to the time of speaking. Such results may exist at the time of speaking, or they may not; the pluperfect contributes nothing either way.

## Future

Future verb tense (Only one future tense)

## Aorist Imperative

Like an order from a commanding general which calls for full attention and immediate obedience.

---

Back to [Greek grammar](greek_grammar.md)
