# Verb Moods

Indicates the actuality or potentiality of the action

4 types of moods:

* Indicative - Simply states a thing as being a fact. There is no doubt that the action occurred. If an action really occurs or has occurred or will occur, it will be rendered in the indicative mood
* Imperative - Calls for the recipient to perform a certain action by the order and authority of one commanding. Imperative mood can also indicate a request or entreaty.
* Subjunctive - Expresses an action which may or should happen but which is not necessarily true at present. In other words subjunctive expresses some doubt that an action occurred (or will occur). It suggests that the action is dependent upon some condition being met.
* Optative - Expresses a wish or desire most often specifically indicates a prayer.
* Participle - Corresponds for the most part to the English participle, reflecting "-ing" or "-ed" being suffixed to the basic verb form. The participle can be used either like a verb or a noun, as in English, and thus is often termed a "verbal noun."

---

Back to [Greek grammar](greek_grammar.md)