# Nouns

* [Wikipedia](https://en.wikipedia.org/wiki/Ancient_Greek_nouns)

## Noun Forms

* [Case](greek_cases.md) - function in the sentence
* Gender - male, female, neuter
* Number - how many

Declension - inflectional change of a noun to indicate the form (number, case)

---

Back to [Greek grammar](greek_grammar.md)