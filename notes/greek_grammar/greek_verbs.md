# Verbs

* [Wikipedia](https://en.wikipedia.org/wiki/Ancient_Greek_verbs)
* [BLB](https://www.blueletterbible.org/help/greekverbs.cfm)
* [John Stevenson](http://www.angelfire.com/nt/theology/greekverbs.html)

## Verb Forms

* [Tense](greek_tenses.md) - when it happened and its permanence
* [Voice](greek_voices.md) - relationship of subject to action
* [Mood](greek_moods.md) - actuality or potentiality of the action

Conjugation - inflection change of a verb to indicate the form (tense, voice, mood)

---

Back to [Greek grammar](greek_grammar.md)