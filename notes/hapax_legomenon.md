## Hapax legomenon

[Wikipedia](https://en.wikipedia.org/wiki/Hapax_legomenon)

[Jewish encyclopedia](https://www.jewishencyclopedia.com/articles/7236-hapax-legomena)

[Cateclesia](https://cateclesia.com/2022/10/24/death-by-a-thousand-cuts-examining-biblical-hapax-legomena-one-word-at-a-time/)

[Logos forum](https://community.logos.com/forums/t/66705.aspx)

approximately 3,414 words which appear only once in the Bible, which account for nearly 25% of the total vocabulary. In the New Testament alone there are approximately 1,934 hapaxes of 5,436 lexemes,10 or 36% of the total vocabulary.

