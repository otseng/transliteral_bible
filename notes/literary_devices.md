# Literary devices

[Metaphor](https://en.wikipedia.org/wiki/Metaphor) - a figure of speech that, for rhetorical effect, directly refers to one thing by mentioning another

[Figure of speech](https://en.wikipedia.org/wiki/Figure_of_speech) - an intentional deviation from ordinary language, chosen to produce a rhetorical effect

[Parable](https://en.wikipedia.org/wiki/Parable) - a succinct, didactic story, in prose or verse, that illustrates one or more instructive lessons or principles

[Trope](https://en.wikipedia.org/wiki/Trope_(literature)) - the use of figurative language

* Allegory – A sustained metaphor continued through whole sentences or even through a whole discourse. For example: "The ship of state has sailed through rougher storms than the tempest of these lobbyists."
* Antanaclasis – The stylistic trope of repeating a single word, but with a different meaning each time; antanaclasis is a common type of pun, and like other kinds of pun, it is often found in slogans.
* Hyperbole - the use of exaggeration to create a strong impression.
* Irony – Creating a trope through implying the opposite of the standard meaning, such as describing a bad situation as "good times".
* Litotes
* Metaphor – An explanation of an object or idea through juxtaposition of disparate things with a similar characteristic, such as describing a courageous person as having a "heart of a lion".
* Metonymy – A trope through proximity or correspondence. For example, referring to actions of the U.S. President as "actions of the White House".
* Oxymoron
* Synecdoche – Related to metonymy and metaphor, creates a play on words by referring to something with a related concept: for example, referring to the whole with the name of a part, such as "hired hands" for workers; a part with the name of the whole, such as "the law" for police officers; the general with the specific, such as "bread" for food; the specific with the general, such as "cat" for a lion; or an object with its substance, such as "bricks and mortar" for a building.
* Catachresis – improper use of metaphor
