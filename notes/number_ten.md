# Number ten

* Completeness
* Perfection
* God’s divine order

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/10.html)

[Bible Zero](https://www.biblezero.com/number-10-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-10/)

[Mary Jane Humes](https://maryjanehumes.com/what-does-the-number-10-mean-in-the-bible/)
