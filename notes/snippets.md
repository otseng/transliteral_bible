# Snippets

## Bible

<a name=galatians_5_1></a>[Galatians 5:1](../../bible/galatians/galatians_5.md#galatians_5_1)

## Footnotes

[^1]

[^1]: [Luke 23:53 Commentary](../../commentary/luke/luke_23_commentary.md#luke_23_53)

[^1]: [Matthew 5:44 Commentary](../../commentary/matthew/matthew_5_commentary.md#matthew_5_44)

[^1]: [Matthew 28:27 Commentary](../../commentary/matthew/matthew_28_commentary.md#matthew_28_27)

[^1]: [Romans 12:11 Commentary](../../commentary/romans/romans_commentary.md#romans_12_11)

[^1]: [Acts 3:1 Commentary](../../commentary/acts/acts_3_commentary.md#acts_3_1)

[^1]: [Ephesians 1:3 Commentary](../../commentary/ephesians/ephesians_1_commentary.md#ephesians_1_3)

[^1]: [Numbers 6:24](../../commentary/numbers/numbers_6_commentary.md#numbers_6_24)
