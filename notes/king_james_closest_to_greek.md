# KJV closest translation to Greek

[Closer pronoun tenses](http://www.kjvtoday.com/home/language-of-the-kjv) (thee, ye)

[No quotation marks](http://www.kjvtoday.com/home/Features-of-the-KJV/quotation-marks)

[Hebraisms](pdf/hebraisms_in_authorized_version_roserich.pdf)