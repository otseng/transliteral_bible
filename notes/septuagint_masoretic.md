# Septuagint vs Masoretic

[Bible Beatdown! The Septuagint Text VS. The Masoretic Text](https://preachersinstitute.com/2016/09/02/bible-beatdown-the-septuagint-text-vs-the-masoretic-text/)

[Comparing the Septuagint and King James Bibles](http://ecmarsh.com/lxx-kjv/)

[Wikipedia - Masoretic Text](https://en.wikipedia.org/wiki/Masoretic_Text)

[Wikipedia - Septuagint](https://en.wikipedia.org/wiki/Septuagint)

[Why Use the Septuagint?](https://blog.logos.com/2007/12/why_use_the_septuagint/)

[Comparisons between the Bible and the Septuagint](https://www.ecclesia.org/truth/comparisons.html)

[The Role of the Septuagint in the Transmission of the Scriptures](http://www.biblearchaeology.org/post/2012/02/17/The-Role-of-the-Septuagint-in-the-Transmission-of-the-Scriptures.aspx)

[Mysteries of the Kingdom](http://www.matthewbryan.net/lxx3.html)

[A Comparison Between The Modern Text Of Genesis And The Septuagint](http://daatemet.org.il/en/torah-talmud/torah-text/a-comparison-between-the-modern-text-of-genesis-and-the-septuagint/)