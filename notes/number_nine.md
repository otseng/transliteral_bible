# Number nine

* Finality
* Judgment
* Divine wrath

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/9.html)

[Bible Zero](https://www.biblezero.com/number-9-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-9/)

[Mary Jane Humes](https://maryjanehumes.com/number-9-meaning-in-the-bible/)
