# Bible Translations

| Bible | Language | Abbrev | Published | Sources | Approach | Notes
|---|---|---|---|---|---|--
|[American Standard](https://en.wikipedia.org/wiki/American_Standard_Version)|English|ASV|1901|KJV, Masoretic Text, Westcott and Hort 1881 and Tregelles 1857|Formal equivalence
|[Amplified Bible](https://en.wikipedia.org/wiki/Amplified_Bible)|English|AMP|1965|ASV, Masoretic Text, Westcott and Hort 1881 and Tregelles 1857|Formal/Dynamic|
|[Berean Study Bible](https://berean.bible/)|Engjlish|BSB||||Freely distributable
|[Bible in Basic English](https://en.wikipedia.org/wiki/Bible_in_Basic_English)|English|BBE|1941|||[Hooke](https://en.wikipedia.org/wiki/S._H._Hooke), Public domain
|[Bible in Worldwide English](https://en.wikipedia.org/wiki/Bible_in_Worldwide_English)|English|BWE|1959|||Annie Cressman|
|[Common English](https://en.wikipedia.org/wiki/Common_English_Bible)|English|CEB|2011|Nestle-Aland, Masoretic|Formal/Dynamic|7th grade reading level
|[Complete Jewish Bible](https://en.wikipedia.org/wiki/Messianic_Bible_translations#Complete_Jewish_Bible_(CJB))|English|CJB|1998|Masoretic Text, Greek New Testament 3rd Edition UBS|Dynamic equivalence|[David Stern](https://en.wikipedia.org/wiki/David_H._Stern)
|[Contemporary English Version](https://en.wikipedia.org/wiki/Contemporary_English_Version)|English|CEV|1995||Dynamic equivalence - Messianic|
|[Coverdale](https://en.wikipedia.org/wiki/Coverdale_Bible)|English|TCB|1537|Tyndale, Luther, Vulgate
|[Douay-Rheims](https://en.wikipedia.org/wiki/Douay%E2%80%93Rheims_Bible)|English|DRC|1582, 1610, 1752|Latin Vulgate, KJV|Formal equivalence
|[Easy to Read](https://en.wikipedia.org/wiki/Easy-to-Read_Version)|English|ERV|1987|Biblia Hebraica Stuttgartensia, Septuagint
|[Emphasized Bible](https://en.wikipedia.org/wiki/Emphasized_Bible)|English|EBR|1902|Westcott and Hort||[Joseph Bryant Rotherham](https://en.wikipedia.org/wiki/Joseph_Bryant_Rotherham)
|[English Standard Version](https://en.wikipedia.org/wiki/English_Standard_Version)|English|ESV|2001|RSV, Westcott-Hort, Weiss, Tischendorf|Formal equivalance
|[Geneva Bible](https://en.wikipedia.org/wiki/Geneva_Bible)|English|GEN|1557/1561|Masoretic Text, Textus Receptus|Formal equivalence|First English Bible to have verse numbering
|[God's Word Translation](https://en.wikipedia.org/wiki/God's_Word_Translation)|English|GW|1995|Nestle-Aland Greek New Testament, Biblia Hebraica Stuttgartensia|Natural equivalence|Copyright
|[Good News](https://en.wikipedia.org/wiki/Good_News_Bible)|English|GNB, GNT|1976|Nestle-Aland |Dynamic equivalence|
|[Great Bible](https://en.wikipedia.org/wiki/Great_Bible)|English||1539|Tyndale, Vulgate||Coverdale, Cromwell. First authorized English Bible
|[Holman Christian Standard Bible](https://en.wikipedia.org/wiki/Holman_Christian_Standard_Bible)|English|HCSB|2004|Biblia Hebraica Stuttgartensia, Novum Testamentum Graece 27th Edition, United Bible Societies 4th Edition|Formal/Dynamic|Funded by LifeWay
|[International Standard Version](https://en.wikipedia.org/wiki/International_Standard_Version)|English|ISV|2011||Mixed|
|[Jubilee Bible](https://www.thejubileebible.com)|English|JUB|
|[King James Version](https://en.wikipedia.org/wiki/King_James_Version)|English|KJV, AV|1611|Masoretic Text, Textus Receptus, Tyndale 1526 NT, some Erasmus manuscripts, and Bezae 1598 TR.|Formal equivalence
|[Lamsa Bible](https://en.wikipedia.org/wiki/Lamsa_Bible)|English||1933|Syriac Peshitta|
|[Lexham English Bible](https://en.wikipedia.org/wiki/Lexham_English_Bible)|English|LEB|2011|SBL Greek New Testament.|Formal Equivalence|Logos Bible Software
|[The Living Bible](https://en.wikipedia.org/wiki/The_Living_Bible)|English|TLB, LB|1971|ASV|Paraphrase|[Kenneth Taylor](https://en.wikipedia.org/wiki/Kenneth_N._Taylor)
|[Matthew](https://en.wikipedia.org/wiki/Matthew_Bible)|English||1537|Tyndale, Coverdale||[John Rogers](https://en.wikipedia.org/wiki/John_Rogers_(Bible_editor_and_martyr))
|[The Message](https://en.wikipedia.org/wiki/The_Message_(Bible))|English|MSG|2002||Paraphrase|[Eugene Peterson](https://en.wikipedia.org/wiki/Eugene_H._Peterson)
|[New American Bible](https://en.wikipedia.org/wiki/New_American_Bible)|English|NAB|1970|Confraternity Bible|Formal equivalence
|[New American Bible Revised Edition](https://en.wikipedia.org/wiki/New_American_Bible_Revised_Edition)|English|NABRE|2011|Confraternity Bible, New American Bible|Formal|
|[New American Standard](https://en.wikipedia.org/wiki/New_American_Standard_Bible)|English|NASB|1971|ASV, Masoretic, Nestle-Aland |Formal equivalence
|[New Century Version](https://en.wikipedia.org/wiki/New_Century_Version)|English|NCV|1987||Free|5th grade level
|[New English Bible](https://en.wikipedia.org/wiki/New_English_Bible)|English|NEB|1970|MT, Greek manuscripts|Dynamic equivalence
|[New English Translation](https://en.wikipedia.org/wiki/New_English_Translation)|English|NET|2005|Nestle-Aland, BHS|Dynamic equivalence, Formal footnotes|Freely usable
|[New International Version](https://en.wikipedia.org/wiki/New_International_Version)|English|NIV|1978|Masoretic, Nestle-Aland|Formal/Dynamic|Initiated by Howard Long
|[New Heart English Bible](https://nheb.net/)|English|NHEB||||Publish domain
|[New King James Version](https://en.wikipedia.org/wiki/New_King_James_Version)|English|NKJV|1982|KJV, TR, Masoretic|Formal equivalence
|[New Living Translation](https://en.wikipedia.org/wiki/New_Living_Translation)|English|NLT|1996|The Living Bible, Masoretic, Nestle-Aland|Formal/Dynamic|
|[New Revised Standard Version](https://en.wikipedia.org/wiki/New_Revised_Standard_Version)|English|NRSV|1989|RSV (ASV)||
|[Orthodox Study Bible](https://en.wikipedia.org/wiki/Orthodox_Study_Bible)|English|OSB|2008|Septuagint, Masoretic, NKJV|
|[Phillips New Testament ](https://en.wikipedia.org/wiki/Phillips_New_Testament_in_Modern_English)|English|Phi|1958|Nestle Greek text|Dynamic|Copyright
|[Revised English Bible](https://en.wikipedia.org/wiki/Revised_English_Bible)|English|REB|1989|NEB|Dynamic equivalence|
|[Revised Version](https://en.wikipedia.org/wiki/Revised_Version)|English|RV|1885|KJV, Westcott and Hort 1881 and Tregelles 1857|Formal equivalence
|[Revised Standard Version](https://en.wikipedia.org/wiki/Revised_Standard_Version)|English|RSV|1946, 1952|ASV, Masoretic Text, Nestle-Aland Greek New Testament|Formal equivalence|Copyright
|[Textus Receptus](https://en.wikipedia.org/wiki/Textus_Receptus) (Received Text)|Greek||1516|||[Erasmus](https://en.wikipedia.org/wiki/Erasmus)
|[Tree of Life](https://en.wikipedia.org/wiki/Messianic_Bible_translations#Tree_of_Life_Version_of_the_Holy_Scriptures_(TLV))|English|TLV|2011|Masoretic Text, Nestle-Aland's Novum Testamentum Graece|Formal - Messianic|
|[Tyndale](https://en.wikipedia.org/wiki/Tyndale_Bible)|English||1526 (NT)|Greek, Hebrew||[William Tyndale](https://en.wikipedia.org/wiki/William_Tyndale) 

|[Vulgate](https://en.wikipedia.org/wiki/Vulgate)|Latin||Late 300|||[Jerome](https://en.wikipedia.org/wiki/Jerome)|
|[World English Bible](https://en.wikipedia.org/wiki/World_English_Bible)|English|[WEB](https://ebible.org/web/)|2000|ASV, Biblia Hebraica Stutgartensa, Greek Majority Text|Formal equivalence|Public Domain
|[Wycliffe's Bible](https://en.wikipedia.org/wiki/Wycliffe%27s_Bible)|Middle English|WYC|1388|Vulgate||[Wycliffe](https://en.wikipedia.org/wiki/John_Wycliffe)
|[Young's Literal Translation](https://en.wikipedia.org/wiki/Young%27s_Literal_Translation)|English|YLT|1862|TR, MT|Extreme Formal|[Robert Young](https://en.wikipedia.org/wiki/Robert_Young_(biblical_scholar))

## Lists of Bible

[List of English Bible translations](https://en.wikipedia.org/wiki/List_of_English_Bible_translations)

[Protestant Bible](https://en.wikipedia.org/wiki/Protestant_Bible)

[DeRose](http://www.derose.net/steve/Bible/EnglishBibleTranslations.html)

[Bible Study Tools](https://www.biblestudytools.com/bible-versions/)

[Wikidot](http://bibles.wikidot.com/indexpage)

## Translation Comparisons

[Choosing a Bible Translation](https://www.crosswalk.com/faith/spiritual-life/choosing-a-bible-translation-11631126.html)

[A Brief Description Of Popular Bible Translations](http://bibleresources.americanbible.org/resource/a-brief-description-of-popular-bible-translations)

## Other Bible Translations

[A Conservative Version](http://www.stillvoices.org/)

[Berean Study Bible](https://biblehub.com/bsb/genesis/1.htm)

[Disciples' Literal New Testament](http://literalnewtestament.com/)

[East English Bible](https://www.easyenglish.bible/bible/easy/)

[Hidden Testament](http://www.hiddentestament.org/)

[Jonathan Mitchell New Testament](https://www.jonathanmitchellnewtestament.com/)

[Lighthouse](https://sites.google.com/site/lighthouseversion/)

[Literal Standard Version](https://www.lsvbible.com/)

[Majority Text](http://majoritytext.com/)

[Open English Bible](https://openenglishbible.org/oeb/dev/read/)

[Open Scriptures](https://hb.openscriptures.org/read/)

[Unlocked Literal Bible](https://door43.org/u/Door43-Catalog/en_ulb/)
