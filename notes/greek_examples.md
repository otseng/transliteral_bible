# Examples of Greek transliteration

**Examples where the Greek transliteration illuminates passages.**

## amēn, amēn

English translations use "Verily, verily, I say to you" or "Truly, truly".  In Greek, it is "amēn, amēn".

John 1:51

And he saith unto him, **[amēn](../strongs/g/g281.md), [amēn](../strongs/g/g281.md), I say unto you

## anthrōpos

The gender neutral anthrōpos is often translated to man in English.

John 6:10

And Jesus said, Make the men sit down. Now there was much grass in the place. So the men sat down, in number about five thousand.

And Jesus said, **Make the [anthrōpos](../strongs/g/g444.md) sit down.** Now there was much grass in the place. So the [anēr](../strongs/g/g435.md) sat down, in number about five thousand.

## Luke 11:8

“I say to you, though he will not rise and give to him because he is his friend, yet because of his persistence he will rise and give him as many as he needs."

English translations of the word 'anaideia' make us believe the word applies to the man asking for bread.  But the Greek shows the word applies to the man in bed.  It should be translated "no shame". 

https://tomstuart.org/2010/11/26/one-parable-we-all-misinterpreted/

## Luke 15:22

But the father said to his servants, Bring forth the best robe, and put on him; and put a ring on his hand, and shoes on feet:

Robe is "stolē".  It is not a robe found in the back of the closet.  It is worn by kings, priests, and persons of rank.  