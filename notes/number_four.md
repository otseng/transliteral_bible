# Number four

* Completion
* Creation
* Stability

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/4.html)

[Digital Bible](https://digitalbible.ca/article-page/bible-study-symbols-the-symbolism-of-the-number-4-in-the-bible-delving-into-numerology-historical-and-biblical-significance-1700845095532x588318623419386200)

[Bible Zero](https://www.biblezero.com/number-4-meaning-in-the-bible/)

[Mary Jane Humes](https://maryjanehumes.com/meaning-of-number-4-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-4/)
