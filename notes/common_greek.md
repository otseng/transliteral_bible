# Commonly used Greek words

[adelphos](../../strongs/g/g80.md)

[agapaō](../../strongs/g/g25.md)

[agapē](../../strongs/g/g26.md)

[agathos](../../strongs/g/g18.md)

[aggelos](../../strongs/g/g32.md)

[airō](../../strongs/g/g142.md)

[aiōnios](../../strongs/g/g166.md) [zōē](../../strongs/g/g2222.md)

[akoloutheō](../../strongs/g/g190.md)

[akouō](../../strongs/g/g191.md)

[alētheia](../../strongs/g/g225.md)

[amēn](../../strongs/g/g281.md), [amēn](../../strongs/g/g281.md)

[amnos](../../strongs/g/g286.md)

[anēr](../../strongs/g/g435.md)

[anōthen](../../strongs/g/g509.md)

[anthrōpos](../../strongs/g/g444.md)

[apokteinō](../../strongs/g/g615.md)

[apollymi](../../strongs/g/g622.md)

[apostellō](../../strongs/g/g649.md)

[apothnēskō](../../strongs/g/g599.md)

[archōn](../../strongs/g/g758.md)

[artos](../../strongs/g/g740.md)

[astheneō](../../strongs/g/g770.md)

[baptizō](../../strongs/g/g907.md)

[basileia](../../strongs/g/g932.md)

[basileia](../../strongs/g/g932.md) of [theos](../../strongs/g/g2316.md)

[basileus](../../strongs/g/g935.md)

[brōsis](../../strongs/g/g1035.md)

[chairō](../../strongs/g/g5463.md)

[chara](../../strongs/g/g5479.md)

[charis](../../strongs/g/g5485.md)

[cheir](../../strongs/g/g5495.md)

[Christos](../../strongs/g/g5547.md)

[Christos](../../strongs/g/g5547.md), the [huios](../../strongs/g/g5207.md) of the [zaō](../../strongs/g/g2198.md) [theos](../../strongs/g/g2316.md)

[didaskalos](../../strongs/g/g1320.md)

[didōmi](../../strongs/g/g1325.md)

[dikaios](../../strongs/g/g1342.md)

[doxa](../../strongs/g/g1391.md)

[doxazō](../../strongs/g/g1392.md)

[eidō](../../strongs/g/g1492.md)

[eiserchomai](../../strongs/g/g1525.md)

[egeirō](../../strongs/g/g1453.md)

[egō](../../strongs/g/g1473.md) [eimi](../../strongs/g/g1510.md)

[entolē](../../strongs/g/g1785.md)

[erchomai](../../strongs/g/g2064.md)

[ergazomai](../../strongs/g/g2038.md)

[ergon](../../strongs/g/g2041.md)

[exousia](../../strongs/g/g1849.md)

[gennaō](../../strongs/g/g1080.md)

[ginomai](../../strongs/g/g1096.md)

[ginōskō](../../strongs/g/g1097.md)

[graphē](../../strongs/g/g1124.md)

[graphō](../../strongs/g/g1125.md)

[gynē](../../strongs/g/g1135.md)

[hagios](../../strongs/g/g40.md) [pneuma](../../strongs/g/g4151.md)

[hamartanō](../../strongs/g/g264.md)

[hamartia](../../strongs/g/g266.md)

[hieron](../../strongs/g/g2411.md)

[homou](../../strongs/g/g3674.md)

[hōra](../../strongs/g/g5610.md)

[horaō](../../strongs/g/g3708.md)

[huios](../../strongs/g/g5207.md)

[hydōr](../../strongs/g/g5204.md)

[hypagō](../../strongs/g/g5217.md)

[hypsoō](../../strongs/g/g5312.md)

[idios](../../strongs/g/g2398.md)

[Iēsous](../../strongs/g/g2424.md)

[Iēsous](../../strongs/g/g2424.md) [Christos](../../strongs/g/g5547.md)

[karpos](../../strongs/g/g2590.md)

[katharismos](../../strongs/g/g2512.md)

[kosmos](../../strongs/g/g2889.md)

[krinō](../../strongs/g/g2919.md)

[krisis](../../strongs/g/g2920.md)

[kyrios](../../strongs/g/g2962.md)

[laleō](../../strongs/g/g2980.md)

[logos](../../strongs/g/g3056.md)

[manna](../../strongs/g/g3131.md)

[mathētēs](../../strongs/g/g3101.md)

[martyreō](../../strongs/g/g3140.md)

[martyria](../../strongs/g/g3141.md)

[menō](../../strongs/g/g3306.md)

[Messias](../../strongs/g/g3323.md)

[mētēr](../../strongs/g/g3384.md)

[monogenēs](../../strongs/g/g3439.md)

[monogenēs](../../strongs/g/g3439.md) [huios](../../strongs/g/g5207.md) of [theos](../../strongs/g/g2316.md)

[naos](../../strongs/g/g3485.md)

[nekros](../../strongs/g/g3498.md)

[nomos](../../strongs/g/g3551.md)

[ochlos](../../strongs/g/g3793.md)

[oikia](../../strongs/g/g3614.md)

[onoma](../../strongs/g/g3686.md)

[ophthalmos](../../strongs/g/g3788.md)

[oros](../../strongs/g/g3735.md)

[oudeis](../../strongs/g/g3762.md)

[ouranos](../../strongs/g/g3772.md)

[pas](../../strongs/g/g3956.md)

[pascha](../../strongs/g/g3957.md)

[patēr](../../strongs/g/g3962.md)

[peripateō](../../strongs/g/g4043.md)

[phago](../../strongs/g/g5315.md)

[phaneroō](../../strongs/g/g5319.md)

[Pharisaios](../../strongs/g/g5330.md)

[phobeō](../../strongs/g/g5399.md)

[phōnē](../../strongs/g/g5456.md)

[phōs](../../strongs/g/g5457.md)

[pinō](../../strongs/g/g4095.md)

[pisteuō](../../strongs/g/g4100.md)

[plēroō](../../strongs/g/g4137.md)

[pneuma](../../strongs/g/g4151.md)

[poieō](../../strongs/g/g4160.md)

[polis](../../strongs/g/g4172.md)

[ponēros](../../strongs/g/g4190.md)

[prophētēs](../../strongs/g/g4396.md)

[proskyneō](../../strongs/g/g4352.md)

[prōtos](../../strongs/g/g4413.md)

[pyr](../../strongs/g/g4442.md)

[rhabbi](../../strongs/g/g4461.md)

[rhēma](../../strongs/g/g4487.md)

[sabbaton](../../strongs/g/g4521.md)

[sarx](../../strongs/g/g4561.md)

[sēmeion](../../strongs/g/g4592.md)

[sōma](../../strongs/g/g4983.md)

[sōtēria](../../strongs/g/g4991.md)

[stoma](../../strongs/g/g4750.md)

[thaumazō](../../strongs/g/g2296.md)

[thelēma](../../strongs/g/g2307.md)

[theos](../../strongs/g/g2316.md)

[tithēmi](../../strongs/g/g5087.md)

[zēteō](../../strongs/g/g2212.md)

[zaō](../../strongs/g/g2198.md)

[zōē](../../strongs/g/g2222.md) [aiōnios](../../strongs/g/g166.md)

[zōē](../../strongs/g/g2222.md) [hydōr](../../strongs/g/g5204.md)