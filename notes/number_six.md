# Number six

* Man
* Imperfection
* Incomplete
* Weakness

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/6.html)

[Bible Zero](https://www.biblezero.com/number-6-meaning-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-6/)

[Mary Jane Humes](https://maryjanehumes.com/number-6-meaning-in-the-bible/)
