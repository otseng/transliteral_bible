# Number one

* Unity
* Beginning
* Primacy

## Links

[Bible Study](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/1.html)

[May Jane Humes](https://maryjanehumes.com/what-does-the-number-1-mean-in-the-bible/)

[Increasing Faith](https://increasingfaithintl.org/biblical-meaning-of-number-1/)