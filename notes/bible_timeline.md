# Bible Timeline

[Great Site](https://www.greatsite.com/timeline-english-bible-history/#timeline)

200 BC - [Septuagint](https://en.wikipedia.org/wiki/Septuagint) - Greek

*272 - 337 [Constantine](https://en.wikipedia.org/wiki/Constantine_the_Great)*

384 - [Latin Vulgate](https://en.wikipedia.org/wiki/Vulgate) - Latin ([Jerome](https://en.wikipedia.org/wiki/Jerome))

*800s? - [Masoretic Text](https://en.wikipedia.org/wiki/Masoretic_Text) - Hebrew Old Testament which almost all modern Bibles use*

1382 - 1395 [Wycliffe's Bible
](https://en.wikipedia.org/wiki/Wycliffe%27s_Bible) - Middle English

*1439 - [Printing press](https://en.wikipedia.org/wiki/Printing_press)*

*1453 - [Fall of Constantinople](https://en.wikipedia.org/wiki/Fall_of_Constantinople)

1516 - [Textus Receptus](https://en.wikipedia.org/wiki/Textus_Receptus) - Greek ([Erasmus](https://en.wikipedia.org/wiki/Erasmus))

*1517 - [Protestant Reformation](https://en.wikipedia.org/wiki/Reformation)*

*1534 - [Church of England](https://en.wikipedia.org/wiki/History_of_the_Church_of_England)*

1534 - [Luther Bible](https://en.wikipedia.org/wiki/Luther_Bible) - German ([Luther](https://en.wikipedia.org/wiki/Martin_Luther))

1535 - [Tyndale Bible](https://en.wikipedia.org/wiki/Tyndale_Bible) - English ([Tyndale](https://en.wikipedia.org/wiki/William_Tyndale))

1535 - [Coverdale Bible](https://en.wikipedia.org/wiki/Coverdale_Bible) - English ([Coverdale](https://en.wikipedia.org/wiki/Myles_Coverdale))

1539 - [Great Bible](https://en.wikipedia.org/wiki/Great_Bible) - English ([Coverdale](https://en.wikipedia.org/wiki/Myles_Coverdale))

1557/1560 [Geneva Bible](https://en.wikipedia.org/wiki/Geneva_Bible) - English

1568 - [Bishops' Bible](https://en.wikipedia.org/wiki/Bishops%27_Bible) - English

1582/1610 - [Douay-Rheims](https://en.wikipedia.org/wiki/Douay%E2%80%93Rheims_Bible) - English (from Latin)

1611 - [King James Version](https://en.wikipedia.org/wiki/King_James_Version) - English

1885 - Apocrypha removed from KJV