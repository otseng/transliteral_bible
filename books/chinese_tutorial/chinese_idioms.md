# Idioms

| <div style="width:200px">Chinese</div> | Pinyin | Definition |
|---------|--------|------------|
| [拍案叫绝](https://www.purpleculture.net/dictionary-details/?word=拍案叫绝) | pāi àn jiào jué  |  slap the table and shout with praise |
| [刻苦耐劳](https://www.purpleculture.net/dictionary-details/?word=刻苦耐劳) | kè kǔ nài láo  | hard-working and capable of overcoming adversity |
| [小不忍则乱大谋](https://www.purpleculture.net/dictionary-details/?word=小不忍则乱大谋) | xiǎo bù rěn zé luàn dà móu  |  great plans can be ruined by just a touch of impatience  |
| [不经一事，不长一智](https://www.purpleculture.net/dictionary-details/?word=不经一事，不长一智) |  bù jīng yī shì , bù zhǎng yī zhì | wisdom only comes with experience   |
| [利令智昏](https://www.purpleculture.net/dictionary-details/?word=利令智昏) | lì lìng zhì hūn  |  to lose one's head through material greed |
| [吃着碗里，看着锅里](https://www.purpleculture.net/dictionary-details/?word=吃着碗里，看着锅里) | chī zhe wǎn lǐ , kàn zhe guō lǐ  | 1 lit. eyeing what's in the pot as one eats from one's bowl (idiom). 2 not content with what one already has   |
| [一不做，二不休](https://www.purpleculture.net/dictionary-details/?word=一不做，二不休) | yī bù zuò , èr bù xiū  | Don't do it, or don't rest. Since we started, we must carry it through whatever happens.  |
| [价廉物美](https://www.purpleculture.net/dictionary-details/?word=价廉物美) | jià lián wù měi  | inexpensive and of good quality  |
| [赏心悦目](https://www.purpleculture.net/dictionary-details/?word=赏心悦目) | shǎng xīn yuè mù  | warms the heart and delights the eye  |
| [可遇不可求](https://www.purpleculture.net/dictionary-details/?word=可遇不可求) | kě yù bù kě qiú  | can be discovered but not sought   |
| [出乎意外](https://www.purpleculture.net/dictionary-details/?word=出乎意外) | chū hū yì wài  | beyond expectation   |
| [一言难尽](https://www.purpleculture.net/dictionary-details/?word=一言难尽) | yī yán nán jìn  | hard to explain in a few words  |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
