# Vocabulary

| <div style="width:100px">Chinese</div> | Pinyin | Definition |
|---------|--------|------------|
| [聖經](https://www.purpleculture.net/dictionary-details/?word=聖經) | shèng jīng  | Bible  |
| [聖靈](https://www.purpleculture.net/dictionary-details/?word=聖靈) | Sheng4 ling2  | Holy Spirit  |
| [禱告](https://www.purpleculture.net/dictionary-details/?word=禱告) | dao3 gao4  | to pray  |
| [聖](https://www.purpleculture.net/dictionary-details/?word=聖) | sheng4  |  holy; sacred |
| [降臨](https://www.purpleculture.net/dictionary-details/?word=降臨) | jiang4 lin2  |  arrive, come |
| [旨意](https://www.purpleculture.net/dictionary-details/?word=旨意) | zhi3 yi4  | will, intent, decree  |
| [如同](https://www.purpleculture.net/dictionary-details/?word=如同) | ru2 tong2  | like, as  |
| [行](https://www.purpleculture.net/dictionary-details/?word=行) | xíng | go, walk, travel |
| [救](https://www.purpleculture.net/dictionary-details/?word=救) | jiu4  | save  |
| [榮耀](https://www.purpleculture.net/dictionary-details/?word=榮耀) | rong2 yao4  |  honor; glory |
| [阿們](https://www.purpleculture.net/dictionary-details/?word=阿們) |  a1 men5 | amen  |
| [饒恕](https://www.purpleculture.net/dictionary-details/?word=饒恕) |  rao2 shu4 | forgive, pardon  |
| [喜樂](https://www.purpleculture.net/dictionary-details/?word=喜樂) | xi3 le4  | joy  |
| [親愛](https://www.purpleculture.net/dictionary-details/?word=親愛) | qin1 ai4  | beloved  |
| [恩賜](https://www.purpleculture.net/dictionary-details/?word=恩賜) | en1 ci4  | bestow (favors, charity)  |
| [動怒](https://www.purpleculture.net/dictionary-details/?word=動怒) |  dong4 nu4 | to get angry  |
| [福音](https://www.purpleculture.net/dictionary-details/?word=福音) | fu2 yin1  | good news, gospel  |
| [施洗](https://www.purpleculture.net/dictionary-details/?word=施洗) | shi1 xi3  | baptize  |
| [世界](https://www.purpleculture.net/dictionary-details/?word=世界) |  shi4 jie4 | world  |
| [拉比](https://www.purpleculture.net/dictionary-details/?word=拉比) | la1 bi3  | rabbi  |
| [猶太人](https://www.purpleculture.net/dictionary-details/?word=猶太人) |  You2 tai4 ren2 |  Jew |
| [復活](https://www.purpleculture.net/dictionary-details/?word=復活) |  fu4 huo2 | resurrection  |
| [實實在在](https://www.purpleculture.net/dictionary-details/?word=實實在在) | shí shí zaì zaì  | verily, truly  |
| [重生](https://www.purpleculture.net/dictionary-details/?word=重生) | chong2 sheng1  |  rebirth, born |
| [必須](https://www.purpleculture.net/dictionary-details/?word=必須) | bi4 xu1  |  must, necessary |
| [希奇](https://www.purpleculture.net/dictionary-details/?word=希奇) | xi1 qi2  | strange, rare  |
| [試驗](https://www.purpleculture.net/dictionary-details/?word=試驗) | shi4 yan4  | experiment, test  |
| [眾人](https://www.purpleculture.net/dictionary-details/?word=眾人) |  zhong4 ren2 | everyone  |
| [靈魂](https://www.purpleculture.net/dictionary-details/?word=靈魂) | ling2 hun2  | soul, spirit  |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |

## Names

| <div style="width:100px">Chinese</div> | Pinyin | Definition |
|---------|--------|------------|
| [耶穌 基督](https://www.purpleculture.net/dictionary-details/?word=耶穌基督) | Yēsū jīdū  | Jesus Christ  |
| [馬太](https://www.purpleculture.net/dictionary-details/?word=馬太) |  Ma3 tai4 | Matthew  |
| [馬可](https://www.purpleculture.net/dictionary-details/?word=馬可) | Mǎ kè  |  Mark |
| [路加](https://www.purpleculture.net/dictionary-details/?word=路加) |  Lu4 jia1 |  Luke |
| [約翰](https://www.purpleculture.net/dictionary-details/?word=約翰) |  Yue1 han4 | John  |
| [保羅](https://www.purpleculture.net/dictionary-details/?word=保羅) | Bao3 luo2  |  Paul |
| [彼得](https://www.purpleculture.net/dictionary-details/?word=彼得) | Bi3 de2  |  Peter  |
| [雅各](https://www.purpleculture.net/dictionary-details/?word=雅各) | Ya3 ge4  |  Jacob, James |
| [多馬](https://www.purpleculture.net/dictionary-details/?word=多馬) | Duōmǎ  | Thomas  |
| [西門](https://www.purpleculture.net/dictionary-details/?word=西門) | Xi1 men2  | Simon |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [馬利亞](https://www.purpleculture.net/dictionary-details/?word=馬利亞) | Ma3 li4 ya4  |  Mary |
| [馬大](https://www.purpleculture.net/dictionary-details/?word=馬大) | Mǎdà  |  Martha |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |


## Places

| <div style="width:100px">Chinese</div> | Pinyin | Definition |
|---------|--------|------------|
| [耶路撒冷](https://www.purpleculture.net/dictionary-details/?word=耶路撒冷) |  Ye1 lu4 sa1 leng3 |  Jerusalem |
| [加利利](https://www.purpleculture.net/dictionary-details/?word=加利利) |  Jia1 li4 li4 | Galilee  |
| [撒馬利亞](https://www.purpleculture.net/dictionary-details/?word=撒馬利亞) | Sa1 ma3 li4 ya4  |  Samaria |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
| [](https://www.purpleculture.net/dictionary-details/?word=) |   |   |
