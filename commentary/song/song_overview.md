# Song of Solomon Overview

## Other names

* Song of Songs

## Title

* Hebrew: Shir HaShirim (Song of Songs)
* Greek: Ἆσμα Ἀσμάτων (Asma Asmatōn)
* Latin: Canticum Canticorum (Canticle of Canticles)

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Solomon

## Dating

* Between 971 and 931 BC

## Genre

* Poetry

## Literary form

* Song

## Audience

* Israel

## Statistics

* 8 chapters, 117 verses, 2661 words.

## Videos

### David Pawson

* [Song of Songs](https://www.youtube.com/watch?v=YYtZ-gkQtFI&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=36)

### Bible Project

* [Song of Songs](https://www.youtube.com/watch?v=4KC7xE4fgOw)

### Randall Smith

* [Song of Songs](https://www.youtube.com/watch?v=b5NJq8fiKHg&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=14)

## Audio

### Steve Kreloff

* [Marital Pleasures: Exposition of the Song of Solomon](https://www.lakesidechapel.com/mediaPlayer/#/series/54)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Notes

* Never quoted by NT
* God never explicitly mentioned
* No doctrine or teachings
* Read by Jews on the Sabbath during Passover

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/song-of-solomon/song-of-solomon-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/songofsongs-intro.cfm)

[Bible.org](https://bible.org/article/introduction-song-songs)

[ESV](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Song_of_Songs)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-song-of-solomon/)

[USCCB](https://bible.usccb.org/bible/songofsongs/0)

[Charles Swindoll](https://insight.org/resources/bible/the-wisdom-books/song-of-solomon)

[Wikipedia](https://en.wikipedia.org/wiki/Song_of_Songs)

[Precept Austin](https://www.preceptaustin.org/song_of_solomon_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-song-of-songs/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/song/song.htm)

[Bible Hub](https://biblehub.com/sum/song)
