# 1 Peter 4

<a name=1peter_4_14></a>[1 Peter 4:14](../../bible/1peter/1peter_4.md#1peter_4_14)

> MT/TR: If you are reproached for the name of Christ, blessed are you, for the Spirit of glory and of God rests upon you. On their part He is blasphemed, but on your part He is glorified.
> 
> CT: If you are reproached for the name of Christ, blessed are you, for the Spirit of glory and of God rests upon you.
