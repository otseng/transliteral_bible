# 1 Peter 2

<a name=1peter_2_2></a>[1 Peter 2:2](../../bible/1peter/1peter_2.md#1peter_2_2)

> MT/TR: as newborn babes, desire the pure milk of the word, that you may grow thereby
> 
> CT: as newborn babes, desire the pure milk of the word, that you may grow thereby unto salvation

# 1 Peter 2
<a name="1peter_2_9"></a>[1 Peter 2:9](../../bible/1peter/1peter_2.md#1peter_2_9)

royal is incorrectly numbered by Strong as G934. It should be G933.

