# 1 Peter Overview

## Title

* First Epistle of Peter

## Author

* Peter

## Dating

* 64 AD

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Churches in Asia Minor

## Themes

* Those who persevere in faith while suffering persecution should be full of hope

## Statistics

* 5 chapters, 105 verses, 2482 words

## Videos

### David Pawson

* [1 Peter](https://www.youtube.com/watch?v=d8dBDPa9uvQ&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=27&pp=iAQB)

### Bible Project

* [1 Peter](https://www.youtube.com/watch?v=WhP7AZQlzCg&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=27&pp=iAQB)

### Randall Smith

* [1 Peter](https://www.youtube.com/watch?v=FjUizG0uoTI&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=47&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Written in advanced Greek

## Links

[Blue Letter Bible](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/1peter-intro.cfm)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1peter/1peter-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1peter/1peter-outline.cfm)

[Bible.org](https://bible.org/seriespage/21-first-peter-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-1-peter/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-1-peter/)

[USCCB](https://bible.usccb.org/bible/1peter/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-1-peter/)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/first-peter)

[Wikipedia](https://en.wikipedia.org/wiki/First_Epistle_of_Peter)

[Precept Austin](https://www.preceptaustin.org/1_peter_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-1-peter/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1peter/1peter.htm)

[Bible Hub](https://biblehub.com/sum/1_peter)
