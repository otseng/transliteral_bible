# Hebrews 2

<a name=hebrews_2_7></a>[Hebrews 2:7](../../bible/hebrews/hebrews_2.md#hebrews_2_7)

TR: You have made him a little lower than the angels; You have crowned him with glory and honor, And set him over the works of Your hands.

MT/CT: You have made him a little lower than the angels; You have crowned him with glory and honor.
