# Hebrews Overview

## Title

* Epistle to the Hebrews

* "To the Hebrews"

## Author

* Written anonymously

* Early church admits ignorance of authorship

* Possibly written by Paul, Barnabas, Apollos, Luke, Silas, Clement, [Priscilla](https://en.wikipedia.org/wiki/Priscilla_and_Aquila)

* Possibly written by more than one person (extensive use of "we").

## Dating

* Makes no reference to destruction of temple (70 AD)

* Between 60 and 70 AD

* Middle of Nero's reign while Christians were persecuted, but before they were martyred.

## Genre

* Epistle, but has no greeting or authorship

* Sermon

## Audience

* Hebrew Christians in Rome

## Canon

* Not considered canonical by earliest sources

## Themes

* Supremacy of Christ

* Encourage Christians in a time of trial

* Warn Jewish Christians against apostasy to Judaism

## Statistics

* 13 chapters, 303 verses, 6913 words

## Videos

### David Pawson

* [Unlocking the New Testament Hebrews 1/2](https://www.youtube.com/watch?v=kmUXH0_rQuQ&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=23&pp=iAQB)
* [Unlocking the New Testament Hebrews 2/2](https://www.youtube.com/watch?v=NrW4pWjUzRQ&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=24&pp=iAQB)

### Bruce Gore

* [Hebrews series](https://www.youtube.com/watch?v=K6GHNpDI-G0&list=PLYFBLkHop2anWGTRJkQhzlPnD0ZIJIQge)

### Bible Project

* [Hebrews](https://www.youtube.com/watch?v=1fNWTZZwgbs&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=25&pp=iAQB)

### Randall Smith

* [Hebrews](https://www.youtube.com/watch?v=r961X6FXwpw&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=45&pp=iAQB)

### ThirdMill

* [Hebrews](https://www.youtube.com/watch?v=aUfBc2Hwczs)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* "No biblical document outside of the four Gospels focuses as totally and forcefully on the Person and redemptive achievement of Jesus." - Leonard S. Walmark

* Style of Greek among the best in New Testament and better than Paul's

* Quotes from Septuagint, not from Hebrew Bible

* Many unique Greek words in Hebrews

* During the time of Nero, Judaism was legal, but Christianity was illegel.  Jewish Christians were tempted to revert back to Judaism to avoid persecution.  The book of Hebrews attempts to persuade the Jewish Christians that Jesus is better than Judaism.

* Use of nautical language - Heb 2:1, Heb 6:19, Heb 11:12, Heb 11:29 

## Links

[Bible.org](https://bible.org/seriespage/19-hebrews-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-acts/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/hebrews-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-hebrews/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Hebrews&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-hebrews/)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/hebrews)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Hebrews)

[Precept Austin](https://www.preceptaustin.org/hebrews)

[Authorship of the Epistle to the Hebrews](https://en.wikipedia.org/wiki/Authorship_of_the_Epistle_to_the_Hebrews)

[Bible Project](https://bibleproject.com/guides/book-of-hebrews/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/hebrews/hebrews.htm)

[Bible Hub](https://biblehub.com/sum/hebrews)
