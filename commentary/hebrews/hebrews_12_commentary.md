# Hebrews 12

<a name=hebrews_12_20></a>[Hebrews 12:20](../../bible/hebrews/hebrews_12.md#hebrews_12_20)

TR: For they could not endure what was commanded: “And if so much as a beast touches the mountain, it shall be stoned or shot with an arrow.”

MT/CT: For they could not endure what was commanded: “And if so much as a beast touches the mountain, it shall be stoned.”
