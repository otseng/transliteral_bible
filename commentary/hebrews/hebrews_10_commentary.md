# Hebrews 10

<a name=hebrews_10_34></a>[Hebrews 10:34](../../bible/hebrews/hebrews_10.md#hebrews_10_34)

MT/TR: for you had compassion on me in my chains, and joyfully accepted the plundering of your goods, knowing that you have a better and an enduring possession for yourselves in heaven.

CT: for you had compassion on the prisoners, and joyfully accepted the plundering of your goods, knowing that you have a better and an enduring possession for yourselves in heaven.
