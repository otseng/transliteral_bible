# Nahum Overview

## Title

* Hebrew: Nachum (Comfort)
* Greek: Ναούμ Ζʹ (VII. Naoum)
* Latin: Nahum

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Nahum

## Dating

* Before 612 B.C., 630 B.C.

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Nineveh, capital of Assyria

## Statistics

* 3 chapters, 47 verses, 1285 words.

## Videos

### David Pawson

* [Nahum](https://www.youtube.com/watch?v=nPe0-YDFe1I&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=53)

### Bible Project

* [Nahum](https://www.youtube.com/watch?v=Y30DanA5EhU&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=25)

### Randall Smith

* [Nahum](https://www.youtube.com/watch?v=H1JRNZi-VhI&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=22)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Notable passages

* Nahum 1:15 - Behold upon the mountains the feet of him that bringeth good tidings, that publisheth peace! O Judah, keep thy solemn feasts, perform thy vows: for the wicked shall no more pass through thee; he is utterly cut off.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/nahum/nahum-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/nahum-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-nahum)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-nahum/)

[USCCB](https://bible.usccb.org/bible/nahum/0)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/nahum)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Nahum)

[Precept Austin](https://www.preceptaustin.org/nahum_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-nahum/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/nahum/nahum.htm)

[Bible Hub](https://biblehub.com/sum/nahum/)