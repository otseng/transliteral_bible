# 1 Timothy 3

<a name=1timothy_3_16></a>[1 Timothy 3:16](../../bible/1timothy/1timothy_3.md#1timothy_3_16)

MT/TR: And without controversy great is the mystery of godliness: God was manifested in the flesh, justified in the Spirit, seen by angels, preached among the Gentiles, believed on in the world, received up in glory.

CT: By common confession, great is the mystery of godliness: He who was revealed in the flesh, was vindicated in the Spirit, seen by angels, proclaimed among the nations, believed on in the world, taken up in glory.
