# 1 Timothy 6

<a name=1timothy_6_5></a>[1 Timothy 6:5](../../bible/1timothy/1timothy_6.md#1timothy_6_5)

MT/TR: useless wranglings of men of corrupt minds and destitute of the truth, who suppose that godliness is a means of gain. From such withdraw yourself.

CT: useless wranglings of men of corrupt minds and destitute of the truth, who suppose that godliness is a means of gain.
