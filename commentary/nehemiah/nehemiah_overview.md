# Nehemiah Overview

## Title

* Hebrew: Nechemya (Comfort of God)
* Greek: Ἔσδρας Βʹ - 2 Esdras
* Latin: 2 Esdras

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)

## Author

* Nehemiah and later scribes

## Dating

* ~ 430 BC

## Genre

* History

## Literary form

* Narrative

## Audience

* The people of Israel

## Canon

* Ezra/Nehemiah was one book in the MT 
* Placed before Chronicles in book order, but  is after Chronicles chronologically
* Early Syrian canon and Theodore of Mopsuestia (a leader of the Antiochean school of interpretation) omitted Chronicles, Ezra, and Nehemiah from their list of inspired books.

## Themes

* Rebuilding of the walls
* Restoration of community
* Governmental reform

## Statistics

* 13 chapters, 406 verses, 10,483 words.

## Videos

### David Pawson

* [Nehemiah](https://www.youtube.com/watch?v=9q0vg9-p-8E&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=26)

### Bible Project

* [Ezra-Nehemiah](https://www.youtube.com/watch?v=MkETkRv9tG8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=40)

## Timelines

* [470-350 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CEzra,%20Nehemiah,%20Malachi%3E470-350_BCE)

## Notes

* Ezra 2:2-16 and Neh 7:6-63 are identical
* Records last historical events of the Old Testament period

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/nehemiah/nehemiah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/nehemiah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-nehemiah)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-nehemiah/)

[USCCB](https://bible.usccb.org/bible/nehemiah/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/nehemiah)

[Wikipedia](https://en.wikipedia.org/wiki/Ezra%E2%80%93Nehemiah)

[Precept Austin](https://www.preceptaustin.org/nehemiah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ezra-nehemiah/)
[Thomas Constable](https://soniclight.com/tcon/notes/html/nehemiah/nehemiah.htm)

[Bible Hub](https://biblehub.com/sum/nehemiah)
