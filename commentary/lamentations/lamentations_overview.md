# Lamentations Overview

## Title

* Hebrew: Eicha (Alas, How, Oh)
* Greek: Θρῆνοι, Thrēnoi (lament)
* Latin: Lamentationes

## Other names

* Hebrew: qnôt̠ (dirges, laments)

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)

## Author

* Traditionally Jeremiah

## Dating

* After 586 BC (fall of Jerusalem)

## Genre

* Prophecy

## Literary form

* Poetry
* Chiastic acrostic

## Statistics

* 5 chapters, 154 verses, 3415 words.

## Videos

### David Pawson

* [Lamentations](https://www.youtube.com/watch?v=eD4ruTpZ0qk&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=41)

### Bible Project

* [Lamentations](https://www.youtube.com/watch?v=p8GDFPdaQZQ&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=36)

### Randall Smith

* [Lamentations](https://www.youtube.com/watch?v=eTSoO0ZoM_I&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=16)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Themes

* Mourning for Jerusalem shows there comes a time when prayers will not be answered, and whatsoever a man sows, he shall reap.
* Mourning and sorrow for the destruction of Jerusalem by the Babylonians in 586 BC

## Notable passages

* Lamentations 3:22-23: Because of the Lord’s great love we are not consumed, for his compassions never fail. They are new every morning; great is your faithfulness.

## Notes

* Read annually in Jewish tradition on Tisha B'Av, a day of mourning commemorating the destruction of both the First and Second Temples in Jerusalem.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/lamentations/lamentations-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/jeremiah-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-lamentations)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-lamentations/)

[USCCB](https://bible.usccb.org/bible/lamentations/0)

[Charles Swindoll](https://insight.org/resources/bible/the-major-prophets/lamentations)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Lamentations)

[Precept Austin](https://www.preceptaustin.org/lamentations_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-lamentations/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/lamentation/lamentation.htm)

[Bible Hub](https://biblehub.com/sum/lamentation)
