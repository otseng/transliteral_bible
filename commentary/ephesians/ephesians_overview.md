# Ephesians Overview

## Title

* Epistle to the Ephesians

## Author

* Paul

## Dating

* AD 60-62

## Genre

* Epistle

## Literary form

* Personal letter of exhortation and instruction

## Audience

* Church in the city of Ephesus, capital of the Roman province of Asia (now Turkey)

## Themes

* Christ has reconciled all creation to himself and to God
* Christ has united people from all nations to himself and to one another in his church
* Christians must live as new people

## Statistics

* 6 chapters, 155 verses, 3039 words.

### David Pawson

* [Ephesians](https://www.youtube.com/watch?v=lq7mT9Vo9Ls&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=15&pp=iAQB)

### Bible Project

* [Ephesians](https://www.youtube.com/watch?v=Y71r-T98E2Q&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=16&pp=iAQB)

### Randall Smith

* [Ephesians](https://www.youtube.com/watch?v=WGEHpTFLKJw&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=38&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Prison epistle (along with Philippians, Colossians, and Philemon)

## Maps

* [Google Maps](https://goo.gl/maps/Jjhao1rVSuNWEJL49)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Ephesians)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/ephesians/ephesians-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/ephesians-intro.cfm)

[Bible.org](https://bible.org/seriespage/10-ephesians-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-ephesians/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-ephesians/)

[USCCB](https://bible.usccb.org/bible/ephesians/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-ephesians/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/ephesians)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Ephesians)

[Precept Austin](https://www.preceptaustin.org/ephesians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ephesians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/ephesians/ephesians.htm)

[Bible Hub](https://biblehub.com/sum/ephesians)
