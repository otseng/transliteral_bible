# Ephesians 5

<a name=ephesians_5_30></a>[Ephesians 5:30](../../bible/ephesians/ephesians_5.md#ephesians_5_30)

MT/TR: For we are members of His body, of His flesh and of His bones.

CT: For we are members of His body.
