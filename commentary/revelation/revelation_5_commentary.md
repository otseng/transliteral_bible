# Revelation 5

<a name=revelation_5_14></a>[Revelation 5:14](../../bible/revelation/revelation_5.md#revelation_5_14)

TR: Then the four living creatures said, “Amen!” And the twenty-four elders fell down and worshiped Him who lives forever and ever.

MT/CT: Then the four living creatures said, “Amen!” And the elders fell down and worshiped Him.
