# Revelation 22

<a name=revelation_22_14></a>[Revelation 22:14](../../bible/revelation/revelation_22.md#revelation_22_14)

MT/TR: Blessed are those who do His commandments, that they may have the right to the tree of life, and may enter through the gates into the city.

CT: Blessed are those who wash their robes, that they may have the right to the tree of life, and may enter through the gates into the city.

<a name=revelation_22_19></a>[Revelation 22:19](../../bible/revelation/revelation_22.md#revelation_22_19)

TR: and if anyone takes away from the words of the book of this prophecy, God shall take away his part from the Book of Life, from the holy city, and from the things which are written in this book.

MT: and if anyone takes away from the words of the book of this prophecy, may God take away his part from the Tree of Life, from the holy city, and from the things which are written in this book.

CT: and if anyone takes away from the words of the book of this prophecy, God shall take away his part from the Tree of Life, from the holy city, and from the things which are written in this book.
