# Revelation 11

<a name=revelation_11_17></a>[Revelation 11:17](../../bible/revelation/revelation_11.md#revelation_11_17)

TR: saying: “We give You thanks, O Lord God Almighty, The One who is and who was and who is to come, Because You have taken Your great power and reigned."

MT/CT: saying: “We give You thanks, O Lord God Almighty, The One who is and who was, Because You have taken Your great power and reigned."
