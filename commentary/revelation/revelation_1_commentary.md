# Revelation 1

<a name=revelation_1_8></a>[Revelation 1:8](../../bible/revelation/revelation_1.md#revelation_1_8)

TR: “I am the Alpha and the Omega, the Beginning and the End,” says the Lord, “who is and who was and who is to come, the Almighty.”

MT/CT: “I am the Alpha and the Omega” says the Lord God, “who is and who was and who is to come, the Almighty.”

<a name=revelation_1_11></a>[Revelation 1:11](../../bible/revelation/revelation_1.md#revelation_1_11)

TR: saying, “I am the Alpha and the Omega, the First and the Last,” and, “What you see, write in a book and send it to the seven churches which are in Asia: to Ephesus, to Smyrna, to Pergamos, to Thyatira, to Sardis, to Philadelphia, and to Laodicea.”

MT/CT: saying, “What you see, write in a book and send it to the seven churches: to Ephesus, to Smyrna, to Pergamos, to Thyatira, to Sardis, to Philadelphia, and to Laodicea.”
