# Revelation 21

<a name=revelation_21_24></a>[Revelation 21:24](../../bible/revelation/revelation_21.md#revelation_21_24)

TR: And the nations of those who are saved shall walk in its light, and the kings of the earth bring their glory and honor into it.

MT: And the nations shall walk in its light, and the kings of the earth bring the glory and honor of the nations to Him.

CT: And the nations shall walk in its light, and the kings of the earth bring their glory and honor into it.
