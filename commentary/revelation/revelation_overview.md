# Revelation Overview

## Title

* Book of Revelation

## Other names

* Apocalypse of John
* Revelation to John 
* Revelation from Jesus Christ

## Author

* John of Patmos

## Dating

* 95 AD
* Written after [Fall of Jerusalem](https://en.wikipedia.org/wiki/Siege_of_Jerusalem_(70_CE))

## Genre

* Prophecy, Apocalyptic, Epistle

## Literary form

* Apocalyptic

## Audience

* 7 churches in Asia Minor

## Canon

* Martin Luther rejected Revelation as canonical

## Themes

* "Christ’s future triumph over the forces of evil and His re-creation of the world for the redeemed"

## Statistics

* 22 chapters, 404 verses, 12,000 words

## Videos

### David Pawson

* [1/6](https://www.youtube.com/watch?v=nX0eQgmoouc&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=34)
* [2/6](https://www.youtube.com/watch?v=C6OSefErEZo&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=35)
* [3/6](https://www.youtube.com/watch?v=1D7WoTS5vkU&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=35&pp=iAQB)
* [4/6](https://www.youtube.com/watch?v=Gt9Tjj4_Wok&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=37)
* [5/6](https://www.youtube.com/watch?v=2ygj_W2JDMo&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=38)
* [6/6](https://www.youtube.com/watch?v=lXVzL3bNoT8&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=39)

### Bruce Gore

* [Book of Revelation series](https://www.youtube.com/watch?v=xbESweN-xjE&list=PLYFBLkHop2akIGtKdFHrLRR1GXSPUnX4Q)

### Bible Project

* [Revelation](https://www.youtube.com/watch?v=5nvVVcYD-0w&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=31)

### Randall Smith

* [Revelation](https://www.youtube.com/watch?v=7EzaVMNdAPA&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=52)

### ThirdMill

* [Revelation](https://www.youtube.com/watch?v=YgFBI1VIPwk)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Jewish Apocalyptic literature was popular: Enoch, The Sibylline Oracles, The Testaments of the Twelve Patriarchs, The Ascension of Isaiah, The Assumption of Moses, The Apocalypse of Baruch, Fourth Ezra.

## Maps

* [Google Maps](https://goo.gl/maps/Jjhao1rVSuNWEJL49)

## Commentaries

* COMMENTARY:::Bridgeway:::Revelation
* COMMENTARY:::DSBNT:::Revelation
* COMMENTARY:::Hengstenberg:::Revelation
* COMMENTARY:::OBB:::Revelation
* COMMENTARY:::Peake:::Revelation
* COMMENTARY:::Sutcliffe:::Revelation

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Revelation)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/revelation/revelation-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/revelation-intro.cfm)

[Bible.org](https://bible.org/seriespage/revelation-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-revelation/)

[Bible Study Daily](biblestudydaily.org/introduction-to-revelation/)

[USCCB](https://bible.usccb.org/bible/revelation/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-revelation/)

[Charles Swindoll](https://insight.org/resources/bible/the-apocalypse/revelation)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Revelation)

[Precept Austin](https://www.preceptaustin.org/revelation_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-revelation/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/revelation/revelation.htm)

[Bible Hub](https://biblehub.com/sum/revelation)
