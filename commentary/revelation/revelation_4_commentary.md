# Revelation 4

<a name=revelation_4_2></a>[Revelation 4:2](../../bible/revelation/revelation_4.md#revelation_4_2)

TR/CT: Immediately I was in the Spirit; and behold, a throne set in heaven, and One sat on the throne. And He who sat there was like a jasper and a sardius stone in appearance; and there was a rainbow around the throne, in appearance like an emerald.

MT: Immediately I was in the Spirit; and behold, a throne set in heaven, and One sat on the throne like a jasper and a sardius stone in appearance; and there was a rainbow around the throne, in appearance like an emerald.
