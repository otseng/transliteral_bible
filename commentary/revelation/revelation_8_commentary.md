# Revelation 8

<a name=revelation_8_7></a>[Revelation 8:7](../../bible/revelation/revelation_8.md#revelation_8_7)

TR: The first angel sounded: And hail and fire followed, mingled with blood, and they were thrown to the earth. And a third of the trees were burned up, and all green grass was burned up.

MT/CT: The first angel sounded: And hail and fire followed, mingled with blood, and they were thrown to the earth and a third of the earth was burned up. And a third of the trees were burned up, and all green grass was burned up.
