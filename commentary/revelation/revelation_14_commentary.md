# Revelation 14

<a name=revelation_14_5></a>[Revelation 14:5](../../bible/revelation/revelation_14.md#revelation_14_5)

TR: And in their mouth was found no deceit, for they are without fault before the throne of God.

MT/CT: And in their mouth was found no falsehood, for they are without fault.
