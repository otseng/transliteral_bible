# Revelation 2

<a name=revelation_2_20></a>[Revelation 2:20](../../bible/revelation/revelation_2.md#revelation_2_20)

TR: Nevertheless I have a few things against you, because you allow that woman, Jezebel who calls herself a prophetess, to teach and seduce My servants to commit sexual immorality and eat things sacrificed to idols.

MT: Nevertheless I have against you that you tolerate your wife Jezebel who calls herself a prophetess, and teaches and seduces My servants to commit sexual immorality and eat things sacrificed to idols.

CT: Nevertheless I have against you that you tolerate that woman, Jezebel who calls herself a prophetess, and teaches and seduces My servants to commit sexual immorality and eat things sacrificed to idols.
