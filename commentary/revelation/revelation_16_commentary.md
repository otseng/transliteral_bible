# Revelation 16

<a name=revelation_16_5></a>[Revelation 16:5](../../bible/revelation/revelation_16.md#revelation_16_5)

TR: And I heard the angel of the waters saying: "You are righteous, O Lord, The One who is and who was and who is to be, Because You have judged these things."

MT/CT: And I heard the angel of the waters saying: "You are righteous, The One who is and who was, the Holy One, Because You have judged these things."
