# Malachi Overview

## Title

* Hebrew: Malachi (Messenger, minister)
* Greek: Μαλαχίας ΙΒʹ (XII. Malachias)
* Latin: Malachiæ

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Malachi

## Dating

* Unknown
* Perhaps 433 BC - 400 BC

## Genre

* Prophecy

## Literary form

* Lofty prose

## Audience

* Israel

## Themes

* Rebuke

## Statistics

* 4 chapters, 55 verses, 1782 words.

## Videos

### David Pawson

* [Malachi 1/2](https://www.youtube.com/watch?v=csz6N5ZByJQ&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=59&pp=iAQB)
* [Malachi 2/2](https://www.youtube.com/watch?v=bUJxoINZW2Q&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=60&pp=iAQB)

### Bible Project

* [Malachi](https://www.youtube.com/watch?v=HPGShWZ4Jvk&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=30&pp=iAQB)

### Randall Smith

* [Malachi](https://www.youtube.com/watch?v=jjamo9GAX8U&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=28&pp=iAQB)

## Timelines

* [470-350 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CEzra,%20Nehemiah,%20Malachi%3E470-350_BCE)

## Notable passages

* Mal 3:10 Bring ye all the tithes into the storehouse, that there may be meat in mine house, and prove me now herewith, saith the LORD of hosts, if I will not open you the windows of heaven, and pour you out a blessing, that [there shall] not [be room] enough [to receive it].

## Notes

* The "Hebrew Socrates". Questions and answers.
* Malachi lived about 100 years after the Israelites had returned from Babylonian exile

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/malachi/malachi-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/malachi-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-malachi)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-malachi/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Malachi&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/malachi)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Malachi)

[Precept Austin](https://www.preceptaustin.org/malachi_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-malachi/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/malachi/malachi.htm)

[Bible Hub](https://biblehub.com/sum/malachi/)