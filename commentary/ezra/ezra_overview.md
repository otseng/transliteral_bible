# Ezra Overview

## Title

* Hebrew: Ezra (help)
* Greek: Ἔσδρας Βʹ - 2 Esdras - Ezra-Nehemiah  * Latin: 1 Esdras

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Ezra

## Dating

* 538 BC - 458 BC

## Genre

* History

## Literary form

* Narrative

## Audience

* Returned remnant who were rebuilding Jerusalem following their seventy-year Babylonian captivity.

## Canon

* Hebrew Bible combines Ezra and Nehemiah into a single book - Ezra
* [Wikipedia - 1 Esdras](https://en.wikipedia.org/wiki/1_Esdras)
* 1 Esdras is similar to Ezra-Nehemiah, but has some additions
* Early Syrian canon and Theodore of Mopsuestia (a leader of the Antiochean school of interpretation) omitted Chronicles, Ezra, and Nehemiah from their list of inspired books.

## Themes

* Israelites return from exile
* Religious reform

## Statistics

* 10 chapters, 280 verses, 7,441 words.

## Videos

### David Pawson

* [Ezra](https://www.youtube.com/watch?v=JS-c9qS8uA8&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=25&pp=iAQB)

### Bible Project

* [Ezra - Nehemiah](https://www.youtube.com/watch?v=MkETkRv9tG8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=40)

## Timelines

* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)
* [470-350 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CEzra,%20Nehemiah,%20Malachi%3E470-350_BCE)

## Notes

* Not quoted by NT
* Ezra 2 and Nehemiah 7 are the same

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/ezra/ezra-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/ezra-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-ezra)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-ezra/)

[USCCB](https://bible.usccb.org/bible/ezra/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/ezra)

[Wikipedia](https://en.wikipedia.org/wiki/Ezra%E2%80%93Nehemiah)

[Precept Austin](https://www.preceptaustin.org/ezra_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ezra-nehemiah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/ezra/ezra.htm)

[Bible Hub](https://biblehub.com/sum/ezra)
