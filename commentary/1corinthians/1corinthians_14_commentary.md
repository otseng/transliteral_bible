# 1 Corinthians 14

<a name="1corinthians_14_35"></a>[1 Corinthians 14:35](../../bible/1corinthians/1corinthians_14.md#1corinthians_14_35)

> [Bible Ref](https://www.bibleref.com/1-Corinthians/14/1-Corinthians-14-35.html)
>
> [Got Questions](https://www.gotquestions.org/women-silent-church.html)
>
> [9 Marks](https://www.9marks.org/article/must-women-be-silent-in-churches/)
>
> [Marg Mowczko](https://margmowczko.com/interpretations-applications-1-cor-14_34-35/)

<a name=1corinthians_14_38></a>[1 Corinthians 14:38](../../bible/1corinthians/1corinthians_14.md#1corinthians_14_38)

> MT/TR: But if anyone is ignorant, let him be ignorant.
> 
> CT: But if anyone does not recognize this, he is not recognized.
