# 1 Corinthians Overview

## Title

* First Epistle to the Corinthians
* Ancient Greek: Α΄ ᾽Επιστολὴ πρὸς Κορινθίους

## Collection

* Pauline epistle

## Author

* [Paul](https://en.wikipedia.org/wiki/Paul_the_Apostle)
* Undisputed by scholars
* The earliest NT book in which an extra-biblical writer attaches a name

## Dating

* 53 - 55 AD

## Genre

* Epistle

## Audience

* Church that resided in Corinth of Achaia

## Canon

* Included in every ancient canon

## Themes

* Deals with abuses of liberty

## Corinth

* Was one of the dominant commercial centers of the Mediterranean world as early as the eighth century b.c. No city in Greece was more favorably situated for land and sea trade. A diolkos, or stone road for the overland transport of ships, linked the two seas. Crowning the Acrocorinth was the temple of Aphrodite, served, according to Strabo, by more than 1,000 pagan priestess-prostitutes.
* "to Corinthianize" came to mean “to practice sexual immorality.
* Paul founded church in 50-51 AD

## Statistics

* 16 chapters, 437 verses, 9489 words.

## Videos

### David Pawson

* [1 Corinthians](https://www.youtube.com/watch?v=EyVF6rrQAPE&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=11&pp=iAQB)

### Bible Project

* [1 Corinthians](https://www.youtube.com/watch?v=yiHf8klCCc4&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=13&pp=iAQB)

### Randall Smith

* [1 Corinthians](https://www.youtube.com/watch?v=L97h99iNods&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=35&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Paul wrote 4 letters to Corinth.  Only 2 survive.
* Provides us with a fuller insight into the life of an early Christian community of the first generation than any other book of the New Testament

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/corinthi.cfm)

[Bible.org](https://bible.org/seriespage/7-1-corinthians-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-1-corinthians/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/1corinthians-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-1-corinthians/)

[USCCB](http://usccb.org/bible/1corinthians/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-1-corinthians/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/first-corinthians)

[Wikipedia](https://en.wikipedia.org/wiki/First_Epistle_to_the_Corinthians)

[Precept Austin](https://www.preceptaustin.org/1_corinthians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-1-corinthians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1corinthians/1corinthians.htm)

[Bible Hub](https://biblehub.com/sum/1_corinthians)
