# 1 Corinthians 6

<a name=1corinthians_6_20></a>[1 Corinthians 6:20](../../bible/1corinthians/1corinthians_6.md#1corinthians_6_20)

> MT/TR: For you were bought at a price; therefore glorify God in your body and in your spirit, which are God’s.
> 
> CT: For you were bought at a price; therefore glorify God in your body.
