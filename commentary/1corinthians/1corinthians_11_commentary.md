# 1 Corinthians 11

<a name=1corinthians_11_24></a>[1 Corinthians 11:24](../../bible/1corinthians/1corinthians_11.md#1corinthians_11_24)

MT/TR: and when He had given thanks, He broke it and said, “Take, eat; this is My body which is broken for you; do this in remembrance of Me.”

CT: and when He had given thanks, He broke it and said, “This is My body which is for you; do this in remembrance of Me.”
