# 1 Corinthians 9

<a name="1corinthians_9_20"></a>[1 Corinthians 9:20](../../bible/1corinthians/1corinthians_9.md#1corinthians_9_20)

> MT/TR: and to the Jews I became as a Jew, that I might win Jews; to those who are under the law, as under the > law, that I might win those who are under the law
> 
> CT: and to the Jews I became as a Jew, that I might win Jews; to those who are under the law, as under the > law, though not being myself under the law that I might win those who are under the law
