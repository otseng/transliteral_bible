# Judges Overview

## Title

* Hebrew: [Shoftim](https://en.wikipedia.org/wiki/Shophet) (Deliverers)
* Greek: Κριταί - Kritai
* Latin: Judices

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Unknown (Samuel)

## Dating

* 1425 - 1120 BC

## Genre

* Narrative history

## Literary form

* Narrative

## Audience

* Israelites

## Themes

* Israel apostasy

## Statistics

* 21 chapters, 618 verses, 18,976 words.

## Videos

### David Pawson

* [Judges](https://www.youtube.com/watch?v=iF-UsFYX1XM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=18&pp=iAQB)

### Bible Project

* [Judges](https://www.youtube.com/watch?v=kOYy8iCfIJ4&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=10)

### Randall Smith

* [Judges](https://www.youtube.com/watch?v=8LBBr2k5rAc&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=7)

## Timeline

* [1410-1290 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CRuth,%20Judges,%20Joshua%3E1410-1290_BCE)
* [1290-1170 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges%3E1290-1170_BCE)
* [1170-1050 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel%3E1170-1050_BCE)
* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/judges/judges-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/judges-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-judges)

[ESV](https://www.blueletterbible.org/esv-study-bible/old-testament/introductions/introduction-to-judges.cfm)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-judges/)

[USCCB](https://bible.usccb.org/bible/judges/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/judges)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Judges)

[Precept Austin](https://www.preceptaustin.org/judges_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-judges/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/judges/judges.htm)

[Bible Hub](https://biblehub.com/sum/judges)
