# Job Overview

## Title

* Hebrew: Iyov (Job, "hated, oppressed")
* Greek: Ἰώβ (Iōb)
* Latin: Job

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Unknown

## Dating

* Setting around 1900 to 1700 BC.  
* Possibly written 400 years before book of Genesis.

## Genre

* Poetry, Wisdom, Drama

## Language

* Paleo-Hebrew
* Contains Syriac and Arabic expressions

## Audience

* Unknown

## Themes

* Problem of suffering and evil

## Statistics

* 42 chapters, 1070 verses, 10,102 words.

## Videos

### David Pawson

* [Job 1/2](https://www.youtube.com/watch?v=mBSKwuVUOfs&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=28&t=3s&pp=iAQB)
* [Job 2/2](https://www.youtube.com/watch?v=XWcWYdo7hXA&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=29&t=3s&pp=iAQB)

### Bible Project

* [Job](https://www.youtube.com/watch?v=xQwnH8th_fs&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=33)

### Randall Smith

* [Job](https://www.youtube.com/watch?v=NB4o8O5SnsU)

## Timelines

* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)

## Notable passages

* Job 19:25 - For I know that my Redeemer lives, and at the last he will stand upon the earth.

## Notes

* Traditionally considered the oldest book of the Bible. [Bible Gateway](https://www.biblegateway.com/blog/2016/02/when-was-each-book-of-the-bible-written/)

* Makes no reference to the law, people, events, or other books of the Bible.

* The lifestyle and longevity of Job are similar to that of the patriarchs.

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Job)

[Bible.org](https://bible.org/article/introduction-book-job)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-job/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/job-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-job/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Job&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-wisdom-books/job)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Job)

[Precept Austin](https://www.preceptaustin.org/job_commentaries)

[Belief Net](https://www.beliefnet.com/faiths/christianity/what-is-the-oldest-book-in-the-bible.aspx)

[Bible Project](https://bibleproject.com/guides/book-of-job/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/job/job.htm)

[Bible Hub](https://biblehub.com/sum/job)
