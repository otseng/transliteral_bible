# Habakkuk Overview

## Title

* Hebrew: Chavakkuk (Wrestle, embrace)
* Greek: Ἀμβακούμ Ηʹ (VIII. Ambakoum)
* Latin: Habacuc

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Dating

* Written shortly after the Fall of Nineveh (in 612 BC) and before the Babylonian capture of Jerusalem (in 586 BC). 

## Genre

* Prophecy

## Literary form

* Musical Poetry

## Audience

* Judah

## Themes

* The righteous shall live by faith. (Quoted 3 times in NT)

## Statistics

* 3 chapters, 56 verses, 1474 words.

## Videos

### David Pawson

* [Habakkuk](https://www.youtube.com/watch?v=lGYkZtGoXvs&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=54&pp=iAQB)

### Bible Project

* [Habakkuk](https://www.youtube.com/watch?v=OPMaRqGJPUU&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=26&pp=iAQB)

### Randall Smith

* [Habakkuk](https://www.youtube.com/watch?v=aK4hcJKncGE&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=25&pp=iAQB_)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Notable passages

* Hab 2:4 Behold, his soul [which] is lifted up is not upright in him: but the just shall live by his faith.

## Notes

* Only prophet to devote his entire work to the question of suffering and justice 
* Entirely addressed to God
* Mostly poems of lament

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/habakkuk/habakkuk-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/habakkuk-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-habakkuk)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-habakkuk/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Habakkuk&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/habakkuk)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Habakkuk)

[Precept Austin](https://www.preceptaustin.org/habakkuk_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-habakkuk/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/habakkuk/habakkuk.htm)

[Bible Hub](https://biblehub.com/sum/habakkuk/)