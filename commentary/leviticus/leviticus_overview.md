# Leviticus Overview

## Title

* Hebrew: Vayikra (And He Called)
* Greek: Λευϊτικόν (Leuitikon)
* Latin: Leviticus

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Pentateuch](https://en.wikipedia.org/wiki/Torah)

## Author

* Written anonymously
* Traditionally attributed to Moses

## Dating

* Between 1450 and 1406 B.C.

## Genre

* Primarily Law with some Historical Narrative

## Literary form

* Oration (speeches)

## Audience

* Israelites

## Themes

* Pertaining to the Levites
* Atonement and holy living
* Covenant obligations (ritual, ethical)
* Sanctification
* Get right, keep right

## Statistics

* 27 chapters, 859 verses, 24,546 words.

## Videos

### David Pawson

* [Unlocking the Old Testament Part 10 - Leviticus 1](https://www.youtube.com/watch?v=R4QFF-56NAY&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=10)

* [Unlocking the Old Testament Part 11 - Leviticus 2](https://www.youtube.com/watch?v=HhZcDGo0AJc&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=11)

### Bible Project

* [Leviticus](https://www.youtube.com/watch?v=IJ-FekWUZzE&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=6&pp=iAQB)

### Randall Smith

* [Leviticus](https://www.youtube.com/watch?v=4h_g5j6pQAI&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=4)

## Timelines

* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)

## Notable passages

* Lev 19:2 Give the following instructions to the entire community of Israel. You must be holy because I, the Lord your God, am holy. 
* Lev 19:18 Thou shalt not avenge, nor bear any grudge against the children of thy people, but thou shalt love thy neighbour as thyself: I [am] the LORD.

## Notes

* "The book of Leviticus was the first book studied by a Jewish child; yet is often among the last books of the Bible to be studied by a Christian."
* “It is perhaps the most neglected of the neglected biblical books.”

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/leviticus/leviticus-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/leviticus-intro.cfm)

[Bible.org](https://bible.org/article/introduction-leviticus)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-leviticus/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-leviticus/)

[USCCB](https://bible.usccb.org/bible/leviticus/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-leviticus/)

[Charles Swindoll](https://www.insight.org/resources/bible/the-pentateuch/leviticus)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Leviticus)

[Precept Austin](https://www.preceptaustin.org/leviticus_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-leviticus/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/leviticus/leviticus.htm)

[Bible Hub](https://biblehub.com/sum/leviticus)
