# Joshua Overview

## Title

* Hebrew: Yehoshua (Jehovah is salvation, Jesus)
* Greek: Ἰησοῦς Ναυῆ (Iēsous Nauē) 
* Latin: Josue

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)
* First of the 12 historical books
* Former prophets

## Author

* Author not explicitly mentioned
* Written in first person
* Joshua
* Last verses by Eleazar or Phinehas

## Dating

* 1405 - 1385 BC

## Genre

* History

## Literary form

* Narrative

## Audience

* Israelites

## Themes

* God's promises
* Conquering the Land

## Statistics

* 24 chapters, 685 verses, 18,858 words

## Videos

### David Pawson

* [Joshua 1/2](https://www.youtube.com/watch?v=OS7N6kQIOtI&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=17)
* [Joshua 2/2](https://www.youtube.com/watch?v=O2GlEjf6B1k&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=18)

### Bible Project

* [Joshua](https://www.youtube.com/watch?v=JqOqJlFF_eU&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=9&pp=iAQB)

### Randall Smith

* [Joshua](https://www.youtube.com/watch?v=hTtWyDrRIAI&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=6&pp=iAQB)

## Timeline

* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)
* [1410-1290 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CRuth,%20Judges,%20Joshua%3E1410-1290_BCE)

## Notable passages

* Joshua 1:9: "Have I not commanded you? Be strong and courageous. Do not be afraid; do not be discouraged, for the LORD your God will be with you wherever you go."
* Joshua 6:20: "When the trumpets sounded, the army shouted, and at the sound of the trumpet, when the men gave a loud shout, the wall collapsed; so everyone charged straight in, and they took the city."
* Joshua 24:15: "But if serving the LORD seems undesirable to you, then choose for yourselves this day whom you will serve, whether the gods your ancestors served beyond the Euphrates, or the gods of the Amorites, in whose land you are living. But as for me and my household, we will serve the LORD."

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/joshua/joshua-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/joshua-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-joshua)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-joshua/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-joshua/)

[USCCB](https://bible.usccb.org/bible/joshua/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/joshua)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Joshua)

[Precept Austin](https://www.preceptaustin.org/joshua_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-joshua/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/joshua/joshua.htm)

[Bible Hub](https://biblehub.com/sum/joshua)
