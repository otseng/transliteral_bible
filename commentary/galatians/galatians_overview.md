# Galatians Overview

## Title

* Epistle to the Galatians

## Author

* Paul
* No one has seriously questioned his authorship

## Dating

* Around 48 AD

## Genre

* Epistle

## Literary form

* Personal letter of exhortation and instruction

## Audience

* Galatia (Modern Turkey)

## Themes

* Justification by faith

## Statistics

* 6 chapters, 149 verses, 3098 words

## Videos

### David Pawson

* [Galatians 1/2](https://www.youtube.com/watch?v=dCPYlU_XAfI&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=13&pp=iAQB)
* [Galatians 2/2](https://www.youtube.com/watch?v=wlAcBE-3BlQ&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=14&pp=iAQB)

### Bible Project

* [Galatians](https://www.youtube.com/watch?v=vmx4UjRFp0M&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=15&pp=iAQB)

### Randall Smith

* [Galatians](https://www.youtube.com/watch?v=XQswUdzJKko&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=37&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notable passages

* Gal 2:20 (KJV) I am crucified with Christ: nevertheless I live; yet not I, but Christ liveth in me: and the life which I now live in the flesh I live by the faith of the Son of God, who loved me, and gave himself for me.
* Gal 3:28 (KJV) There is neither Jew nor Greek, there is neither bond nor free, there is neither male nor female: for ye are all one in Christ Jesus.
* Gal 5:14 (KJV) For all the law is fulfilled in one word, [even] in this; Thou shalt love thy neighbor as thyself.
* Gal 5:22 (KJV) But the fruit of the Spirit is love, joy, peace, longsuffering, gentleness, goodness, faith,
* Gal 6:9 (KJV) And let us not be weary in well doing: for in due season we shall reap, if we faint not.

## Notes

* Galatians is the only epistle Paul wrote that does not contain a commendation for its readers
* Martin Luther’s favorite epistle
* "the Magna Carta of the Reformation"

## Maps

* [Galatia](https://en.wikipedia.org/wiki/Galatia)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Galatians)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/galatians/galatians-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/galatians-intro.cfm)

[Bible.org](https://bible.org/seriespage/galatians-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-galatians/)

[Bible Study Daily](https://biblestudydaily.org/?p=2691)

[USCCB](https://bible.usccb.org/bible/galatians/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-galatians/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/galatians)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Galatians)

[Precept Austin](https://www.preceptaustin.org/galatians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-galatians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/galatians/galatians.htm)

[Bible Hub](https://biblehub.com/sum/galatians)
