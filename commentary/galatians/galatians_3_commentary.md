# Galatians 3

<a name=galatians_3_1></a>[Galatians 3:1](../../bible/galatians/galatians_3.md#galatians_3_1)

MT/TR: O foolish Galatians! Who has bewitched you that you should not obey the truth, before whose eyes Jesus Christ was clearly portrayed among you as crucified?

CT: O foolish Galatians! Who has bewitched you before whose eyes Jesus Christ was clearly portrayed among you as crucified?
