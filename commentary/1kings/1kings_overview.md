# 1 Kings Overview

## Title

* Hebrew: Melachim (Kings)
* Greek: Βασιλειῶν Γʹ (3 Basileiōn)
* Latin: 3 Regum (3 Kings)

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Unknown
* Possibly Jeremiah

## Dating

* 562 B.C.-536 B.C

## Genre

* History

## Literary form

* Narrative

## Audience

* Israel

## Themes

* A kingdom united, then divided.

## Statistics

* 22 chapters, 816 verses, 24,524 words.

## Videos

### David Pawson

* [Kings 1/2](https://www.youtube.com/watch?v=tjfEgekLD5s&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=22&pp=iAQB)
* [Kings 1/2](https://www.youtube.com/watch?v=kkj7q9vI3Zs&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=23&pp=iAQB)

### Bible Project

* [Kings](https://www.youtube.com/watch?v=bVFW3wbi9pk&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=13&pp=iAQB)

### Randall Smith

* [Kings](https://www.youtube.com/watch?v=W0PjEe21XM4&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=10&pp=iAQB)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)
* [930-810 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJoel,%20Obadiah,%202%20Chronicles,%201%20Kings,%202%20Kings%3E930-810_BCE)


## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1kings/1kings-2kings-overview.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/1kings-intro.cfm)

[Bible.org](https://bible.org/article/introduction-books-first-and-second-kings)

[Bible Study Daily](http://biblestudydaily.org/?p=923)

[USCCB](https://bible.usccb.org/bible/1kings/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/first-kings)

[Wikipedia](https://en.wikipedia.org/wiki/Books_of_Kings)

[Precept Austin](https://www.preceptaustin.org/1_kings_commentaries)

[Bible Project](https://bibleproject.com/guides/books-of-kings/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1kings/1kings.htm)

[Bible Hub](https://biblehub.com/sum/1_kings)
