# Ruth Overview

## Title

* Hebrew: Rut (friend)
* Greek: Routh
* Latin: Ruth

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Anonymous
* Traditionally attributed to Samuel

## Dating

* Took place around 1140 BC
* Likely written during the reign of King David (1011-971 BC)

## Genre

* Narrative History

## Literary form

* Historical Narrative

## Audience

* Israel

## Themes

* kinsman redeemer - only example in the Bible
* loyalty

### David Pawson

* [Ruth](https://www.youtube.com/watch?v=WHGbkXiN5Mk&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=19&t=10s&pp=iAQB)

### Bible Project

* [Ruth](https://www.youtube.com/watch?v=0h1eoBeR4Jk&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=35)

### Randall Smith

* [Ruth](https://www.youtube.com/watch?v=Vwqunk9LSoQ&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=8)

## Timeline

* [1410-1290 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CRuth,%20Judges,%20Joshua%3E1410-1290_BCE)

## Notes

* An addendum to the Book of Judges
* One of the [Five Megillot](https://en.wikipedia.org/wiki/Five_Megillot)
* One of two books (Ruth, Esther) named after a woman
* Book named after a Moabite
* Primary liturgical text in Judaism for the celebration of the feast of Weeks (Shabuot)
* "God through the eyes of a mother"
* Naomi has been compared to a female Job
* In Hebrew Bible, Ruth is after Proverbs (exemplification of Proverbs 31 woman)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Ruth)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/ruth/ruth-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/ruth-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-ruth)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-ruth/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-ruth/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Ruth&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-ruth/)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/ruth)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Ruth)

[Precept Austin](https://www.preceptaustin.org/ruth_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ruth/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/ruth/ruth.htm)

[Bible Hub](https://biblehub.com/sum/ruth)
