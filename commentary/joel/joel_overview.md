# Joel Overview

## Title

* Hebrew: Yoel (Jehovah is God)
* Greek: Ἰωήλ Δʹ (IV. Iōēl)
* Latin: Joel

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Joel

## Dating

* No clear dating
* 837 BC - 795 BC according to tradition
* ~ 400 BC according to scholars

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Southern Kingdom

## Themes

* Day of the Lord

## Statistics

* 3 chapters, 73 verses, 2034 words.

## Videos

### David Pawson

* [Joel](https://www.youtube.com/watch?v=sSD9K55gxkM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=48)

### Bible Project

* [Joel](https://www.youtube.com/watch?v=zQLazbgz90c&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=20)

### Randall Smith

* [Joel](https://www.youtube.com/watch?v=STvdYnzPjU8&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=19)

## Timelines

* [930-810 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJoel,%20Obadiah,%202%20Chronicles,%201%20Kings,%202%20Kings%3E930-810_BCE)

## Notable passages

* Joel 2:28 And it shall come to pass afterward, [that] I will pour out my spirit upon all flesh; and your sons and your daughters shall prophesy, your old men shall dream dreams, your young men shall see visions:
* Joel 3:10 Beat your plowshares into swords, and your pruning hooks into spears: let the weak say, I [am] strong.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/joel/joel-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/joel-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-joel)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-joel/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Joel&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/joel)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Joel)

[Precept Austin](https://www.preceptaustin.org/joel_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-joel/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/joel/joel.htm)

[Bible Hub](https://biblehub.com/sum/joel/)