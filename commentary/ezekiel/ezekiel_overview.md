# Ezekiel Overview

## Title

* Hebrew: Yechezkel (Ezekiel)
* Greek: Ἰεζεκιήλ (Iezekiēl)
* Latin: Ezechielis (Ezechiel)

## Collection

* Major prophet

## Author

* Ezekiel, "God will strengthen"
* Written in Babylon

## Dating

* After 597 BC
* Ezekiel was contemporary with Jeremiah and Daniel.

## Genre

* Prophecy

## Literary form

* Apocalyptic, prose, poetry, parable

## Audience

* Exiles in Babylon

## Statistics

* 48 chapters, 1273 verses, 39,407 words.

## Videos

### David Pawson

* [Ezekiel 1/3](https://www.youtube.com/watch?v=xTzc8FoA4i0&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=43)
* [Ezekiel 2/3](https://www.youtube.com/watch?v=kciYe1lIYvs&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=44)
* [Ezekiel 3/3](https://www.youtube.com/watch?v=Q7oE5xET8P8&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=45)

### Bible Project

* [Ezekiel 1/2](https://www.youtube.com/watch?v=R-CIPu1nko8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=18)
* [Ezekiel 2/2](https://www.youtube.com/watch?v=SDeCWW_Bnyw&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=19)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Notable passages

## Notes

* “Ezekiel is the prophet of the Spirit, as Isaiah is the prophet of the Son, and Jeremiah the prophet of the Father.”
* Ezekiel not mentioned anywhere else in the Bible.
* Taken captive to Babylon 
* Ezekiel's temple different than Mosaic temple

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/ezekiel/ezekiel-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/ezekiel-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-ezekiel)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-ezekiel/)

[USCCB](https://bible.usccb.org/bible/ezekiel/0)

[Charles Swindoll](https://insight.org/resources/bible/the-major-prophets/ezekiel)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Ezekiel)

[Precept Austin](https://www.preceptaustin.org/ezekiel_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ezekiel/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/ezekiel/ezekiel.htm)

[Bible Hub](https://biblehub.com/sum/ezekiel)
