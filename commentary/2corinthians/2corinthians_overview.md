# 2 Corinthians Overview

## Title

* Second Epistle to the Corinthians

## Collection

* Pauline epistle

## Author

* [Paul](https://en.wikipedia.org/wiki/Paul_the_Apostle)
* Undisputed by scholars
* The earliest NT book in which an extra-biblical writer attaches a name

## Dating

* 55-56 AD

## Genre

* Epistle

## Audience

* Church that resided in Corinth of Achaia

## Canon

* Included in every ancient canon

## Corinth

* Was one of the dominant commercial centers of the Mediterranean world as early as the eighth century b.c. No city in Greece was more favorably situated for land and sea trade. A diolkos, or stone road for the overland transport of ships, linked the two seas. Crowning the Acrocorinth was the temple of Aphrodite, served, according to Strabo, by more than 1,000 pagan priestess-prostitutes.
* "to Corinthianize" came to mean “to practice sexual immorality.
* Paul founded church in 50-51 AD

## Statistics

* 13 chapters, 257 verses, 6092 words. 	

## Videos

### David Pawson

* [2 Corinthians](https://www.youtube.com/watch?v=iis8b_nCYyE&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=12&pp=iAQB)

### Bible Project

* [2 Corinthians](https://www.youtube.com/watch?v=3lfPK2vfC54&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=14&pp=iAQB)

### Randall Smith

* [2 Corinthians](https://www.youtube.com/watch?v=k6MVvyEoLYI&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=36&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Paul wrote 4 letters to Corinth.  Only 2 survive.
* Provides us with a fuller insight into the life of an early Christian community of the first generation than any other book of the New Testament
* "Human weakness, spiritual strength, the deepest tenderness of affection, wounded feeling, sternness, irony, rebuke, impassioned self-vindication, humility, a just self-respect, zeal for the welfare of the weak and suffering, as well as for the progress of the church of Christ and for the spiritual advancement of its members, are all displayed in turn in the course of his appeal." —Lias

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/corinthi.cfm)

[Bible.org](https://bible.org/seriespage/7-1-corinthians-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-2-corinthians/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/2corinthians-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-2-corinthians/)

[USCCB](http://usccb.org/bible/2corinthians/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-2-corinthians/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/second-corinthians)

[Wikipedia](https://en.wikipedia.org/wiki/Second_Epistle_to_the_Corinthians)

[Precept Austin](https://www.preceptaustin.org/2_corinthians_commentaries)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2corinthians/2corinthians.htm)

[Bible Hub](https://biblehub.com/sum/2_corinthians)
