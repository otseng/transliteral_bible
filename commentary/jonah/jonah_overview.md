# Jonah Overview

## Title

* Hebrew: Yonah (Dove)
* Greek: Ἰωνᾶς Ϛ' (VI. Iōnas)
* Latin: Jonæ

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Jonah

## Dating

* 800 - 750 BC

## Genre

* History

## Literary form

* Narrative

## Audience

* Israel

## Themes

* Mercy of God

## Statistics

* 4 chapters, 48 verses, 1321 words.

## Videos

### David Pawson

* [Jonah](https://www.youtube.com/watch?v=g_UJOIgazU4&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=51)

### Bible Project

* [Jonah](https://www.youtube.com/watch?v=dLIabZc0O4c&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=23)

### Randall Smith

* [Jonah](https://www.youtube.com/watch?v=9MOW9VHhQPQ&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=23)

## Timelines

* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)

## Notable passages

* Jonah 4:2 - "I knew that you are a gracious and compassionate God, slow to anger and abounding in love, a God who relents from sending calamity."

## Notes

* Full of irony
* One of four prophets Jesus mentioned by name

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/jonah/jonah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/jonah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-jonah)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-jonah/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Jonah&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/jonah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Jonah)

[Precept Austin](https://www.preceptaustin.org/jonah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-jonah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/jonah/jonah.htm)

[Bible Hub](https://biblehub.com/sum/jonah/)