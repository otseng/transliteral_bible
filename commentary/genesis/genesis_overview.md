# Genesis Overview

## Title

* Hebrew: bereishit (in the beginning)
* Greek: geneseōs (Genesis)
* Latin: genesis
* Derived from the Greek verb gennao, which means "to beget" or "give birth to."

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Pentateuch](https://en.wikipedia.org/wiki/Torah)

## Author

* Written anonymously
* Traditionally attributed to Moses

## Dating

* Between 1450 and 1406 B.C.

## Genre

* Law

## Literary form

* Prose Narrative

## Themes

* Book of beginnings, sources, birth, generations, genealogy
* Creation, sin, and re-creation

## Statistics

* 50 chapters, 1533 verses, 38,267 words.

## Movies

* [Vision Video - Genesis](https://www.youtube.com/watch?v=YBbFG6Vq3rg)

## Videos

### David Pawson

* [Unlocking the Old Testament Part 1 - Overview of the Old Testament](https://www.youtube.com/watch?v=3y7nN4TVX5Y&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=1&t=114s&pp=iAQB)
* [Unlocking the Old Testament Part 2 - Genesis 1](https://www.youtube.com/watch?v=TdKe0NNHWmY&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=2&t=1956s&pp=iAQB)
* [Unlocking the Old Testament Part 3 - Genesis 2](https://www.youtube.com/watch?v=3FQhzYaVk6k&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=3&t=1132s&pp=iAQB)
* [Unlocking the Old Testament Part 4 - Genesis 3](https://www.youtube.com/watch?v=5DsSO1pM-fE&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=4&t=1026s&pp=iAQB)
* [Unlocking the Old Testament Part 5 - Genesis 4](https://www.youtube.com/watch?v=n2t1p7Zfm2s&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=6&t=8s)
* [Unlocking the Old Testament Part 6 - Genesis 5](https://www.youtube.com/watch?v=K26US7y8TyQ&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=6&t=5s&pp=iAQB)
* [Unlocking the Old Testament Part 7 - Genesis 6](https://www.youtube.com/watch?v=Xk7vBiRlzeA&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=7&t=802s&pp=iAQB)

### Bruce Gore

* [Genesis series](https://www.youtube.com/watch?v=6j37zBJ5jlY&list=PLYFBLkHop2alf7ItBiBn4dACvbLnBgqNI)

### Bible Project

* [Genesis 1/2](https://www.youtube.com/watch?v=GQI72THyO5I&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=3)
* [Genesis 2/2](https://www.youtube.com/watch?v=F4isSyennFo&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=3&pp=iAQB)

### Randall Smith

* [Genesis](https://www.youtube.com/watch?v=KM2GjMX5K-Q&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=1&pp=iAQB)

## Timelines

* [2210-2090 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis%3E2210-2090_BCE)
* [2090-1970 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis%3E2090-1970_BCE)
* [1970-1850 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1970-1850_BCE)
* [1850-1730 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1850-1730_BCE)
* [1750-1630 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1750-1630_BCE)
* [1630-1510 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1630-1510_BCE)

## Notable passages

* Gen 1:1 In the beginning God created the heaven and the earth.

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Genesis)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/genesis/genesis-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/genesis-intro.cfm)

[Bible.org](https://bible.org/article/introduction-genesis)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-genesis/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-genesis/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Genesis&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-genesis/)

[Charles Swindoll](https://insight.org/resources/bible/the-pentateuch/genesis)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Genesis)

[Precept Austin](https://www.preceptaustin.org/genesis_commentaries)

[New Advent](https://www.newadvent.org/cathen/11646c.htm)

[Bible Project](https://bibleproject.com/guides/book-of-genesis/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/genesis/genesis.htm)

[Bible Hub](https://biblehub.com/sum/genesis)
