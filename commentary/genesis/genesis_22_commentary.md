# Genesis 22

<a name="genesis_22_2"></a>[Genesis 22:2](../../bible/genesis/genesis_22.md#genesis_22_2)

"Please (na') take thy son"

Other translations (AFV+, KJV*, LEBx, NASBx, NETx, NIV2011x, NSRVx, Tanakhxx) include na' [h4994](../../strongs/h/h4994.md).  
