
# 1 and 2 Chronicles Overview

## Title

* Hebrew: Divrei HaYamim (Words of the days)
* The original title in the Hebrew Bible read "The annals (i.e., events or happenings) of the days." First and Second Chronicles were comprised of one book until later divided into separate books in the Greek OT translation, the Septuagint (LXX)1, ca. 200 B.C. The title also changed at that time to the inaccurate title, "the things omitted," i.e., reflecting material not in 1, 2 Samuel and 1, 2 Kings. The English title "Chronicles" originated with Jerome’s Latin Vulgate translation (ca. 400 A.D.), which used the fuller title "The Chronicles of the Entire Sacred History." 
* Greek: Paraleipomenwn [The Books] of Things Left Out
* Latin: 1 Paralipomenon

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)

## Author

* The "chronicler" 
* Written anonymously
* Traditionally attributed to Ezra

## Dating

* Written sometime after Judah began to return from the Babylonian exile in 538 
* 500 BC - 400 BC

## Genre

* Genealogy, history

## Literary form

* Sacred history

## Audience

* Returned remnant who were rebuilding Jerusalem following their seventy-year Babylonian captivity.

## Canon

* First and Second Chronicles were once one. First and Second Chronicles and Ezra perhaps one history.
* Chronicles is last book in Jewish Bible
* Early Syrian canon and Theodore of Mopsuestia (a leader of the Antiochean school of interpretation) omitted Chronicles, Ezra, and Nehemiah from their list of inspired books.

## Themes

* Genealogy and history of Israel.
* David is the subject of 1 Chronicles; the house of David is prominent in 2 Chronicles. 
* Chronicles gives the history of Judah while practically ignoring the northern kingdom.
* Written to reassure the returned exiles of God's faithfulness toward his people.

## Statistics

* 29 chapters, 942 verses, 20,369 words.

## Videos

### David Pawson

* [1 & 2 Chronicles](https://www.youtube.com/watch?v=N-Y6TBUWoc0&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=24&t=5s&pp=iAQB)

### Bible Project

* [Chronicles](https://www.youtube.com/watch?v=HR7xaHv3Ias)

### Randall Smith

* [Chronicles](https://www.youtube.com/watch?v=p8s2HFJ2Fqk&list=WL&index=6)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Notable passages

## Notes

* The NT does not directly quote either 1 or 2 Chronicles. 
* Kings gives us man’s viewpoint; Chronicles gives us God’s viewpoint.
* In the Septuagint and Vulgate, it is placed immediately after the Books of Kings. 
* In the Hebrew Bible, it is the last book and placed at the end of the writings (Ketuvim).

## Links

[Blue Letter Bible](https://www.blueletterbible.org/resources/intros.cfm)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1chronicles/1chronicles-2chronicles-overview.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/1chronicles-intro.cfm)

[Bible.org](https://bible.org/article/introduction-first-and-second-chronicles)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-1-2-chronicles/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-1-chronicles/)

[USCCB](https://bible.usccb.org/bible/1chronicles/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-1-chronicles/)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/first-chronicles)

[Wikipedia](https://en.wikipedia.org/wiki/Books_of_Chronicles)

[Precept Austin](https://www.preceptaustin.org/1_chronicles_commentaries)

[New Advent](https://www.newadvent.org/cathen/11472a.htm)

[Bible Project](https://bibleproject.com/guides/books-of-chronicles/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1chronicles/1chronicles.htm)

[Bible Hub](https://biblehub.com/sum/1_chronicles)
