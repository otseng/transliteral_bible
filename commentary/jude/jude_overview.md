# Jude Overview

## Title

* Epistle of Jude

## Author

* Jude, Judas, Judah, brother of James

## Dating

* 66 - 69 AD

## Genre

* Epistle

## Literary form

* Letter

## Audience

* General

## Themes

* Addresses apostacy

## Statistics

* 1 chapter, 25 verses, 613 words

## Videos

### David Pawson

* [Jude](https://www.youtube.com/watch?v=5iyll9XfNKA&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=32&pp=iAQB)

### Bible Project

* [Jude](https://www.youtube.com/watch?v=6UoCmakZmys&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=30&pp=iAQB)

### Randall Smith

* [Jude](https://www.youtube.com/watch?v=LBstdNindqk&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=51&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Similar with 2 Peter

## Links

[Blue Letter Bible](https://www.blueletterbible.org/resources/intros.cfm)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/jude/jude-outline.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/jude-intro.cfm)

[Bible.org](https://bible.org/seriespage/26-jude-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-jude/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-jude/)

[USCCB](https://bible.usccb.org/bible/jude/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-jude/)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/jude)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_of_Jude)

[Precept Austin](https://www.preceptaustin.org/jude_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-jude/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/jude/jude.htm)

[Bible Hub](https://biblehub.com/sum/jude)
