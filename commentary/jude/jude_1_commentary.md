# Jude 1

<a name=jude_1_22></a>[Jude 1:22](../../bible/jude/jude_1.md#jude_1_22)

MT/TR: And on some have compassion, making a distinction; but others save with fear, pulling them out of the fire, hating even the garment defiled by the flesh.

CT: And on some have compassion, who are doubting; but others save, pulling them out of the fire, and on some have mercy with fear hating even the garment defiled by the flesh.

<a name=jude_1_25></a>[Jude 1:25](../../bible/jude/jude_1.md#jude_1_25)

MT/TR: To God our Savior, Who alone is wise, Be glory and majesty, Dominion and power, Both now and forever. Amen.

CT: To the only God our Savior, through Jesus Christ our Lord, Be glory and majesty, Dominion and power, Before all time, now and forever. Amen.
