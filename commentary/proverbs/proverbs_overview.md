# Proverbs Overview

## Title

* Hebrew: Mishlei
* Greek: Paroimiai Παροιμίαι
* Latin: Proverbia

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Solomon and others

## Dating

* 950 BC - 539 BC

## Genre

* Poetry

## Literary form

* Wisdom literature

## Audience

* Young man

## Themes

* Wisdom

## Statistics

* 31 chapters, 915 verses, 15,043 words

## Videos

### David Pawson

* [Proverbs 1/2](https://www.youtube.com/watch?v=O3rct6R2AA8&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=33&pp=iAQB)
* [Proverbs 2/2](https://www.youtube.com/watch?v=cj7XMIatkgc&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=34)

### Bible Project

* [Proverbs](https://www.youtube.com/watch?v=AzmYV8GNAIM&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=32)

### Randall Smith

* [Proverbs](https://www.youtube.com/watch?v=7gk1EOwGO5k&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=12)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/proverbs/proverbs-outline.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/proverbs-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-proverbs)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-proverbs/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-proverbs/)

[USCCB](https://bible.usccb.org/bible/proverbs/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-acts/)

[Charles Swindoll](https://insight.org/resources/bible/the-wisdom-books/proverbs)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Proverbs)

[Precept Austin](https://www.preceptaustin.org/proverbs_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-proverbs/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/proverbs/proverbs.htm)

[Bible Hub](https://biblehub.com/sum/proverbs)
