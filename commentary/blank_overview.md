# Amos Overview

## Other names

## Title

* Hebrew: https://headcoverings-by-devorah.com/HebrewBibleNames.html
* Hebrew: Shemot (Names)
* Greek: https://en.wikipedia.org/wiki/Septuagint#Textual_history
* Greek: Ἔξοδος (Exodos)
* Latin: http://textus-receptus.com/wiki/Books_of_the_Latin_Vulgate

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)

* [Pentateuch](https://en.wikipedia.org/wiki/Torah)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)
* [Prophetic](https://en.wikipedia.org/wiki/Prophetic_books)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Amos

## Dating

*

## Genre

*

## Literary form

*

## Audience

*

## Canon

*

## Themes

*

## Statistics

* BOOK:::Bible Statistics:::18 - The Old Testament
* 150 chapters, 2461 verses, 43,743 words.

## Videos

### David Pawson

* [Amos](https://www.youtube.com/playlist?list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC)

### Bible Project

* [Amos](https://www.youtube.com/watch?v=ALsluAKBZ-c&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u)

### Randall Smith

* [Amos](https://www.youtube.com/watch?v=KM2GjMX5K-Q&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p)

## Timelines

* []()
* []()
* []()
* []()

## Notable passages

## Notes

* 

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/resources/intros.cfm)
[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/amos/amos-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/resources/intros.cfm)
[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/amos-intro.cfm)

[Bible.org](https://www.google.com/search?channel=fs&q=site%3A+https%3A%2F%2Fbible.org+introduction+amos)
[Bible.org](https://bible.org/seriespage/introduction-book-amos)

[Bible Study Daily](http://biblestudydaily.org/study-helps/ot-book-introductions/)
[Bible Study Daily](http://biblestudydaily.org/introduction-to-amos/)

http://www.usccb.org/bible/books-of-the-bible/index.cfm
[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Amos&ch=)

[Charles Swindoll](https://insight.org/resources/bible)
[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/amos)

[Wikipedia](https://en.wikipedia.org/wiki/Old_Testament)
[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Amos)

https://www.google.com/search?q=joel+commentaries+site%3A+preceptaustin.org
[Precept Austin](https://www.preceptaustin.org/amos_commentaries)

[Bible Project](https://bibleproject.com/guides/)
[Bible Project](https://bibleproject.com/guides/book-of-amos/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/amos/amos.htm)

[Bible Hub](https://biblehub.com/sum/amos/)