# Jeremiah Overview

## Title

* Hebrew: Yirmeyahu - God will raise up, God exalts
* Greek: Epistolē Ieremiou
* Latin: Jeremias

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Prophetic](https://en.wikipedia.org/wiki/Prophetic_books)
* Second of major prophets

## Author

* Jeremiah, son of Hilkiah.  Written by his scribe, Baruch.

## Dating

* 627 - 586 BC

### Timeline

* 627 BC Call of Jeremiah
* 612 BC Fall of Nineveh (Assyrian capital)
* 609 BC Death of King Josiah (at Megiddo)
* 605 BC Fall of the Assyrian Empire
* 605 BC First siege of Jerusalem by Nebuchadnezzar (Daniel exiled to Babylon)
* 597 BC Second siege of Jerusalem
* 588-586 BC Final siege of Jerusalem (and the Babylonian Captivity)

## Genre

* Prophecy, history, biography

## Literary form

* Poetry, prose

## Audience

* Judah, Jerusalem

## Themes

* Judgment is at hand

## Statistics

* 52 chapters, 1364 verses, 42,659 words

## Movies

* [Vision Video - Jeremiah](https://www.youtube.com/watch?v=pL4FYXj0qQQ&feature=youtu.be)

## Videos

### David Pawson

* [Jeremiah 1/2](https://www.youtube.com/watch?v=7ziUDeb9RWM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=39&pp=iAQB)

* [Jeremiah 2/2](https://www.youtube.com/watch?v=IezUHoqpRcY&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=41)

### Bible Project

* [Jeremiah](https://www.youtube.com/watch?v=RSK36cHbrk0&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=16)

### Randall Smith

* [Jeremiah](https://www.youtube.com/watch?v=5FeLBgkJJwY&pp=ygUWamVyZW1pYWggYm9vayBzeW5vcHNpcw%3D%3D)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Notable passages

* Jer 1:5 Before I formed thee in the belly I knew thee; and before thou camest forth out of the womb I sanctified thee, [and] I ordained thee a prophet unto the nations.

* Jer 29:11 For I know the thoughts that I think toward you, saith the LORD, thoughts of peace, and not of evil, to give you an expected end.

## Notes

* Two versions of Jeremiah (Septuagint and Masoretic)
* Septuagint version is shorter than the Masoretic version
* [Textual Variants in the Book of Jeremiah](https://www.bible.ca/manuscripts/Book-of-Jeremiah-Bible-Manuscript-Textual-Variants-Old-Testament-Tanakh-Septuagint-LXX-Masoretic-Text-MT-scribal-gloss-copying-error.htm)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/jeremiah/jeremiah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/jeremiah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-jeremiah)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-jeremiah/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-jeremiah/)

[USCCB](https://bible.usccb.org/bible/jeremiah/0)

[Charles Swindoll](https://insight.org/resources/bible/the-major-prophets/jeremiah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Jeremiah)

[Precept Austin](https://www.preceptaustin.org/jeremiah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-jeremiah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/jeremiah/jeremiah.htm)

[Bible Hub](https://biblehub.com/sum/jeremiah)
