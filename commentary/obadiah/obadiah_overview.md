# Obadiah Overview

## Title

* Hebrew: 'Ovadya (Servant of God)
* Greek: Ὀβδιού Εʹ (V. Obdiou)
* Latin: Abdiæ

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Obadiah, "Servant of Jehovah"

## Dating

* Unknown
* There are three prominent opinions: 1) during the reign of Jehoram (848-841 B.C., 2) during the reign of Ahaz (731-715 B.C.), and 3) in 585 B.C. shortly after the destruction of the temple in 586 B.C.

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Edom

## Statistics

* 1 chapter, 21 verses, 670 words.

## Videos

### David Pawson

* [Obadiah](https://www.youtube.com/watch?v=sSD9K55gxkM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=48&pp=iAQB)

### Bible Project

* [Obadiah](https://www.youtube.com/watch?v=i4ogCrEoG5s&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=22)

### Randall Smith

* [Obadiah](https://www.youtube.com/watch?v=8fI3xGwzb9o&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=21)

## Timelines

* [930-810 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJoel,%20Obadiah,%202%20Chronicles,%201%20Kings,%202%20Kings%3E930-810_BCE)
* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)
* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

### Notable passages

* Obadiah 1:4 - "Though you soar like the eagle and make your nest among the stars, from there I will bring you down," declares the Lord.

## Notes

* Shortest book in the Old Testament — only twenty-one verses
* Not quoted in New Testament
* Nothing is known about Obadiah
* Obad 1-9 and Jer 49:7-22 are very similar

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/obadiah/obadiah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/obadiah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-obadiah)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-obadiah/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Obadiah&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/obadiah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Obadiah)

[Precept Austin](https://www.preceptaustin.org/obadiah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-obadiah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/obadiah/obadiah.htm)

[Bible Hub](https://biblehub.com/sum/obadiah/)