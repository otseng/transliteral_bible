# Zephaniah Overview

## Other names

## Title

* Hebrew: Tzefanya (Hidden by God)
* Greek: Σοφονίας Θʹ (IX. Sophonias)
* Latin: Sophoniae

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Zephaniah
* Great-great-grandson of King Hezekiah

## Dating

* 627 BC - 607 BC

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Judah

## Themes

* Day of the Lord

## Statistics

* 3 chapters, 53 verses, 1617 words.

## Videos

### David Pawson

* [Zephaniah](https://www.youtube.com/watch?v=S_NEUA55I_U&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=55)

### Bible Project

* [Zephaniah](https://www.youtube.com/watch?v=oFZknKPNvz8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=27&pp=iAQB)

### Randall Smith

* [Zephaniah](https://www.youtube.com/watch?v=FAoww8N9Bl4&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=26)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)

## Notable passages

* Zephaniah 3:17 - The LORD thy God in the midst of thee [is] mighty; he will save, he will rejoice over thee with joy; he will rest in his love, he will joy over thee with singing.

## Notes

* Zephaniah 3:8 is the only Old Testament verse that includes every one of the 22 Hebrew alphabet letters.
* Zephaniah is the only Old Testament biblical book that records God singing (3:17).

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/zephaniah/zephaniah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/zephaniah-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-zephaniah)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-zephaniah/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Zephaniah&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/zephaniah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Zephaniah)

[Precept Austin](https://www.preceptaustin.org/zephaniah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-zephaniah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/zephaniah/zephaniah.htm)

[Thomas Constable](https://soniclight.com/tcon/notes/html/zephaniah/zephaniah.htm)

[Bible Hub](https://biblehub.com/sum/zephaniah)
