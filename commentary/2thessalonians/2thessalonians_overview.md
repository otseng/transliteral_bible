# 2 Thessalonians Overview

## Title

* Second Epistle to the Thessalonians

## Other names

“To the Thessalonians"

## Author

* Paul

## Dating

* 

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Church in Thessalonia

## Statistics

* 3 chapters, 47 verses, 1042 words

## Videos

### David Pawson

* [2 Thessalonians](https://www.youtube.com/watch?v=zcPZWRPdsrQ&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=20&pp=iAQB)

### Bible Project

* [2 Thessalonians](https://www.youtube.com/watch?v=kbPBDKOn1cc&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=20&pp=iAQB)

### Randall Smith

* [Thessalonians](https://www.youtube.com/watch?v=yl7tYikcahQ&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=40&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Capital of Macedonia

* Present-day Thessaloniki, Greece

* Due to its location, Thessalonica might well be called “the key to the whole of Macedonia.” The dictum of Meletius concerning it was, “So long as nature does not change, Thessalonica will remain wealthy and fortunate.” One of its native poets proudly called it the “mother of all Macedon.”

## Maps

* [Google Maps](https://goo.gl/maps/R5hfgRhq8BVLxvLD9)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/resources/intros.cfm)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/2thessalonians/2thessalonians-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/2thessalonians-intro.cfm)

[Bible.org](https://bible.org/seriespage/2-thessalonians-introduction-outline-and-argument)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-2-thessalonians/)

[Bible Study Daily](https://biblestudydaily.org/introduction-2-thessalonians/)

[USCCB](https://bible.usccb.org/bible/2thessalonians/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-2-thessalonians/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/second-thessalonians)

[Wikipedia](https://en.wikipedia.org/wiki/Second_Epistle_to_the_Thessalonians)

[Precept Austin](https://www.preceptaustin.org/2_thessalonians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-2-thessalonians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2thessalonians/2thessalonians.htm)

[Bible Hub](https://biblehub.com/sum/2_thessalonians)
