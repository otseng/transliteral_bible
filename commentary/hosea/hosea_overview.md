# Hosea Overview

## Title

* Hebrew: Hoshea (Salvation)
* Greek: Ὡσηέ Αʹ (I. Hōsēe)
* Latin: Osee

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Hosea

## Dating

* 760 - 720 BC

## Genre

* Prophecy

## Literary form

* Poetry, prose

## Audience

* Northern Israel

## Themes

* Unfaithfulness - Hosea married harlot Gomer
* Redeeming love

## Statistics

* 14 chapters, 197 verses, 5175 words.

## Videos

### David Pawson

* [Hosea](https://www.youtube.com/watch?v=S68kXnzI3xA&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=47)

### Bible Project

* [Hosea](https://www.youtube.com/watch?v=kE6SZ1ogOVU&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=19)

### Randall Smith

* [Hosea](https://www.youtube.com/watch?v=q2Mu2OjTnjY&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=18)

### Thirdmill

* [Hosea](https://www.youtube.com/watch?v=60k5ZpXF3T8&list=WL&index=12)

## Movies

[Amazing Love - The Story of Hosea](https://www.youtube.com/watch?v=A7wHoafxLUY)

## Timelines

* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)

## Notable passages

* Hosea 6:6 - For I desired mercy, and not sacrifice; and the knowledge of God more than burnt offerings.
* Hosea 8:7 - For they have sown the wind, and they shall reap the whirlwind: it hath no stalk: the bud shall yield no meal: if so be it yield, the strangers shall swallow it up.

## Notes

* First of minor prophets
* Addressed Northern Kingdom along with Amos
* Contemporary with Isaiah, Micah, Amos

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/hosea/hosea-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/hosea-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-hosea)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-hosea/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Hosea&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/hosea)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Hosea)

[Precept Austin](https://www.preceptaustin.org/hosea_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-hosea/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/hosea/hosea.htm)

[Bible Hub](https://biblehub.com/sum/hosea)
