# John 5

<a name="john_5_3"></a>[John 5:3](../../bible/john/john_5.md#john_5_3)

MT/TR: In these lay a great multitude of sick people, blind, lame, paralyzed, waiting for the moving of the water. For an angel went down at a certain time into the pool and stirred up the water; then whoever stepped in first, after the stirring of the water, was made well of whatever disease he had.

CT: In these lay a great multitude of sick people, blind, lame, paralyzed.
