# John Overview

## Title

* Gospel of John

## Author

* Written anonymously

* Traditionally attributed to John the Apostle, the son of Zebedee

* Written by "disciple whom Jesus loved"

## Collection

* Gospels

## Dating

* 70 - 100 AD

## Genre

* Gospel

## Audience

* Unknown

## Themes

* Jesus’s deity as the Son of God

## Statistics

* 21 chapters, 878 verses, 19,099 words

## Videos

### David Pawson

* [John 1/2](https://www.youtube.com/watch?v=XFXTBYMFrYI&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=6)

* [John 2/2](https://www.youtube.com/watch?v=WCyTlWUthw4&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=7)

### Bruce Gore

* [Gospel of John series](https://www.youtube.com/watch?v=gSx6gXRoMeI&list=PLYFBLkHop2altIzEj4zgC3XpfFQd3J1PV)

### Bible Project

* [John](https://www.youtube.com/watch?v=G-2e9mMf7E8&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=5&pp=iAQB)

### Randall Smith

* [John](https://www.youtube.com/watch?v=yJmUwFSqcNY&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=32&pp=iAQB)

## Movies

* [Gospel of John](https://www.youtube.com/watch?v=-k0D_qFPb4o)

## Timelines

* [John](https://simple.uniquebibleapp.com/book/Timelines/by_John)
* [Gospels](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John%3Eby_All_4_Gospels)
* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notable passages

* [John 3:16](https://www.blueletterbible.org/kjv/jhn/3/16/s_1000016)

## Notes

* 90% of the content is unique to John

## Reading

[The disciple whom Jesus loved](http://www.thedisciplewhomjesusloved.com/) - [pdf](../../notes/pdf/book-the-beloved-disciple-ebook.pdf)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/john.cfm)

[Bible.org](https://bible.org/seriespage/4-gospel-john-introduction-argument-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-john)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/john-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-john/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=John&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-john/)

[Charles Swindoll](https://insight.org/resources/bible/the-gospels/john)

[Wikipedia](https://en.wikipedia.org/wiki/Gospel_of_John)

[Precept Austin](https://www.preceptaustin.org/john_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-john/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/john/john.htm)

[Bible Hub](https://biblehub.com/sum/john)
