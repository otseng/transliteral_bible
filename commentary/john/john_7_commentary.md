# John 7

<a name="john_7_53"></a>[John 7:53](../../bible/john/john_7.md#john_7_53)

Jesus and the woman taken in adultery (pericope de adultera)

Passage omitted in the critical text.

[Wikipedia](https://en.wikipedia.org/wiki/Jesus_and_the_woman_taken_in_adultery)

[Science on the web](http://textualcriticism.scienceontheweb.net/pa-intro.html)

[Wieland Willker](https://web.archive.org/web/20110409164438/http://www-user.uni-bremen.de/~wie/TCG/TC-John-PA.pdf)

[Bible researcher](http://www.bible-researcher.com/adult.html)
