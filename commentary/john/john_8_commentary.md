# John 8

<a name="john_8_59"></a>[John 8:59](../../bible/john/john_8.md#john_8_59)

MT/TR: Then they took up stones to throw at Him; but Jesus hid Himself and went out of the temple, going through the midst of them, and so passed by.

CT: Then they took up stones to throw at Him; but Jesus hid Himself and went out of the temple.
