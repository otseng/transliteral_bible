# John 6

<a name="john_6_69"></a>[John 6:69](../../bible/john/john_6.md#john_6_69)

MT/TR: Also we have come to believe and know that You are the Christ, the Son of the living God.

CT: Also we have come to believe and know that You are the Holy One of God.
