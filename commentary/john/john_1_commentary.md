# John 1

<a name="john_1_18"></a>[John 1:18](../../bible/john/john_1.md#john_1_18)

MT/TR: No one has seen God at any time. The only begotten Son, who is in the bosom of the Father, He has declared Him.

CT: No one has seen God at any time. The only begotten God, who is in the bosom of the Father, He has declared Him.
