# Psalms Overview

## Title

* Book of Psalms
* The Book of Praises
* Psalter
* Hebrew: Tehillim (Praise songs)
* Greek: Ψαλμοί (Psalmoi)
* Latin: Psalmi

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Various
   * David
   * Asaph
   * Solomon
   * Moses
   * Ethan and Heman the Ezrahites
   * Others

* One of two works (Proverbs) with composite authors

## Dating

* Spans 1000 years. Psalm 90 (Moses) - Psalm 137 (Babylonian captivity)
* Compiled around 300 BC

## Genre

* Wisdom, Poetry

## Themes

* Book of prayer and praise
* Prayer book
* Liturgical worship
* Religious musical lyric poetry
* Ancient hymnal of God’s people

## Psalter divisions

Psalms was compiled into 5 Psalters.

* Book 1: 1 - 41
   * Promise of God's anointed
   * Most have no superscriptions
   * Psalm 1 and 2 gateway to Psalter
   * Most written by David in books 1 and 2 (only a few written by David in books 3-5)
   * David as exalted king and a suffering king
* Book 2: 42 - 72
   * Psalm 72 is last psalm of David
* Book 3: 73 - 89
   * Failures of Israel
* Book 4: 90 - 106
   * Not focused on the Davidic king
   * Praise, thanks to God
   * Psa 90 - Prayer of Moses
   * Focused on God
   * 101, 103 - Psalms of David
* Book 5: 107-150
   * Psa 110 - Most quoted Psalm in the NT

* Book 1: 1-41 Reflect Genesis.
* Book 2: 41-72 Israel’s cry for deliverance to Israel’s king.
* Book 3: 73-89 Sanctuary - dominant note, parallel Leviticus.
* Book 4: 90-106 Corresponds to Numbers, the fourth book of Moses.
* Book 5: 107-150 Linked with Deuteronomy, God’s Word Selah - meaning unknown, many opinions - pause in the music accompanying a psalm or prayer; suspension (of music) as a pause.

## Types of Psalms

* [Dennis Bratcher](http://www.crivoice.org/psalmtypes.html)

* Lament
   * [Imprecatory Psalms](https://en.wikipedia.org/wiki/Imprecatory_Psalms) -  judgment, calamity, or curses, upon one's enemies
   * [Penitential Psalms](https://en.wikipedia.org/wiki/Penitential_Psalms) - expressive of sorrow for sin
   * [Psalm of communal lament](https://en.wikipedia.org/wiki/Psalm_of_communal_lament)

* Confession, Penitential
   * [Penitential Psalms](https://en.wikipedia.org/wiki/Penitential_Psalms)

* Royalty
   * [Royal Psalms](https://en.wikipedia.org/wiki/Royal_psalms)

## Statistics

* 150 chapters, 2461 verses, 43,743 

### David Pawson

* [Hebrew Poetry](https://www.youtube.com/watch?v=v5y0G6WhDDA&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=31)
* [Psalms 1/2](https://www.youtube.com/watch?v=qB3QV713xm0&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=31&t=1445s&pp=iAQB)
* [Psalms 2/2](https://www.youtube.com/watch?v=hQJNMgIHVKw&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=33&t=94s)

### Bible Project

* [Psalms](https://www.youtube.com/watch?v=j9phNEaPrv8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=31)

### Randall Smith

* [Psalms](https://www.youtube.com/watch?v=DD1RsV-u3cg&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=11)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Notes

Psalms is a school of prayer.

Parallelism poetry.

Jewish and Christian content and ordering of Psalms is basically identical.

Most quoted book in New Testament.  Most quoted book by Jesus.

More Psalms of lament than of any other type (one third of Psalms).

## Trivia

Psalm 110 - Most quoted chapter in Psalms in the NT.

Psalm 117 - Shortest chapter in Bible with 2 verses.  Middle chapter of entire Bible.

Psalm 119 - Longest chapter in the Bible with 176 verses (22 * 8).

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/psalms/psalms-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/psalms-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-psalms)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-the-psalms/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-psalms/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Psalms&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-psalms/)

[Charles Swindoll](https://insight.org/resources/bible/the-wisdom-books/psalms)

[Wikipedia](https://en.wikipedia.org/wiki/Psalms)

[Precept Austin](https://www.preceptaustin.org/psalms_commentaries)

[Knowable Word Psalms Analysis](https://docs.google.com/spreadsheets/d/1L6cgtXUVX4kEpRwSwJDkJR-bM0qja_SRMtGZJX5JMeM/edit#gid=0)

[Bible Project](https://bibleproject.com/guides/book-of-psalms/)


[Thomas Constable](https://soniclight.com/tcon/notes/html/psalms/psalms.htm)

[Bible Hub](https://biblehub.com/sum/psalms)
