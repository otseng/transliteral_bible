# Psalm 119 Commentary

C.S. Lewis - Psalm 119 "is not, and does not pretend to be, a sudden outpouring of the heart...It is a pattern, a thing done like embroidery, stitch by stitch, through long, quiet hours, for love of the subject and for the delight in leisurely, disciplined craftsmanship."

## Commentaries

[Wikipedia](https://en.wikipedia.org/wiki/Psalm_119)

[Basics of the Bible](http://www.basicsofthebible.org/psalm119/)

[Enduring Word](https://enduringword.com/bible-commentary/psalm-119/)

[Semicolon Blog](https://www.semicolonblog.com/?p=22735)

## PDF

[I Love Your Law](pdf/Psalm_119_I_Love_Your_Law.pdf)
