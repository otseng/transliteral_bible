# Acts 13

<a name="acts_13_42"></a>[Acts 13:42](../../bible/acts/acts_13.md#acts_13_42)

MT/TR: So when the Jews went out of the synagogue, the Gentiles begged that these words might be preached to them the next Sabbath.

CT: And when they went out, they begged that these words might be preached to them the next Sabbath.
