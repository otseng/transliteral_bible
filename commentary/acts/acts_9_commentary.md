# Acts 9

<a name="acts_9_5"></a>[Acts 9:5](../../bible/acts/acts_9.md#acts_9_5)

TR: And he said, “Who are You, Lord?” Then the Lord said, “I am Jesus, whom you are persecuting. It is hard for you to kick against the goads.” So he, trembling and astonished, said, “Lord, what do You want me to do?” Then the Lord said to him, “Arise and go into the city, and you will be told what you must do.”

MT/CT: And he said, “Who are You, Lord?” Then the Lord said, “I am Jesus, whom you are persecuting. But arise and go into the city, and you will be told what you must do.”
