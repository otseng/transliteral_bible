# Acts 18

<a name="acts_18_21"></a>[Acts 18:21](../../bible/acts/acts_18.md#acts_18_21)

MT/TR: but took leave of them, saying, “I must by all means keep this coming feast in Jerusalem; but I will return again to you, God willing.” And he sailed from Ephesus.

CT: but took leave of them, saying, “I will return again to you, God willing.” And he sailed from Ephesus.
