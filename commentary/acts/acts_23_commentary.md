# Acts 23

<a name="acts_23_9"></a>[Acts 23:9](../../bible/acts/acts_23.md#acts_23_9)

MT/TR: Then there arose a loud outcry. And the scribes of the Pharisees’ party arose and protested, saying, “We find no evil in this man; but if a spirit or an angel has spoken to him, let us not fight against God.”

CT: Then there arose a loud outcry. And the scribes of the Pharisees’ party arose and protested, saying, “We find no evil in this man; what if a spirit or an angel has spoken to him?”
