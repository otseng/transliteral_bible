# Acts Overview

## Title

* Acts of the Apostles
* Greek: Πράξεις Ἀποστόλων (Práxeis Apostólōn)
* Latin: Actūs Apostolōrum

## Author

* Written anonymously

* Traditionally attributed to [Luke the Evangelist](https://en.wikipedia.org/wiki/Luke_the_Evangelist), physician and companion of Paul, Syrian from Antioch

## Title

* The Acts of the Apostles

## Dating

* 80 – 90 AD

## Genre

* Historical narrative

## Audience

* Addressed to Theophilus

* Gentile audience

## Canon

* Many other books of [Acts of the apostles](https://en.wikipedia.org/wiki/Acts_of_the_Apostles_(genre)) have been written, but only the Luke account has been considered canonical.

## Themes

* Founding of the Christian church and the spread of its message to the Roman Empire.

## Statistics

* 28 chapters, 1007 verses, 24,250 words

## Videos

### David Pawson

* [Unlocking the New Testament Acts 1/2](https://www.youtube.com/watch?v=2Z-Hwn_OEpo&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=8)
* [Unlocking the New Testament Acts 2/2](https://www.youtube.com/watch?v=HZm6pk8cGZA&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=9)

### Bruce Gore

* [Acts series](https://www.youtube.com/watch?v=rMPQ8GGqpaQ&list=PLYFBLkHop2an8rK0DTM1W7zvVqxkRwZvZ&pp=iAQB)

### Bible Project

* [Acts](https://www.youtube.com/watch?v=CGbNw855ksw&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=9&pp=iAQB)

### Randall Smith

* [Acts](https://www.youtube.com/watch?v=zJI-vjWcVMY&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=33&pp=iAQB)

### ThirdMill

* [Acts - Background](https://www.youtube.com/watch?v=cY4m36NyR3g)
* [Acts - Structure and Content](https://www.youtube.com/watch?v=CBhSEwB3ukA)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Part 2 of 2 volume work - Luke-Acts.  Luke-Acts comprise over 25% of the New Testament.

* Peter and Paul are the primary apostles mentioned in Acts.

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/acts.cfm)

[Bible.org](https://bible.org/seriespage/5-acts-introduction-outline-and-argument)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-acts/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/acts-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-acts/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Acts&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-acts/)

[Charles Swindoll](https://insight.org/resources/bible/the-history-of-the-early-church/acts)

[Wikipedia](https://en.wikipedia.org/wiki/Acts_of_the_Apostles)

[Precept Austin](https://www.preceptaustin.org/acts_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-acts/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/acts/acts.htm)

[Bible Hub](https://biblehub.com/sum/acts)
