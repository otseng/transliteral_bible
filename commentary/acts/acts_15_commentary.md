# Acts 15

<a name="acts_15_24"></a>[Acts 15:24](../../bible/acts/acts_15.md#acts_15_24)

MT/TR: Since we have heard that some who went out from us have troubled you with words, unsettling your souls, saying, “You must be circumcised and keep the law”—to whom we gave no such commandment

CT: Since we have heard that some who went out from us have troubled you with words, unsettling your souls, to whom we gave no such commandment

<a name="acts_15_34"></a>[Acts 15:34](../../bible/acts/acts_15.md#acts_15_34)

TR: However, it seemed good to Silas to remain there.

MT/CT: Verse omitted
