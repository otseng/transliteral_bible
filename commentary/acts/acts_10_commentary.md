# Acts 10

<a name="acts_10_6"></a>[Acts 10:6](../../bible/acts/acts_10.md#acts_10_6)

TR: “He is lodging with Simon, a tanner, whose house is by the sea. He will tell you what you must do.”

MT/CT: “He is lodging with Simon, a tanner, whose house is by the sea.”

<a name="acts_10_21"></a>[Acts 10:21](../../bible/acts/acts_10.md#acts_10_21)

TR: Then Peter went down to the men who had been sent to him from Cornelius, and said, “Yes, I am he whom you seek. For what reason have you come?”

MT/CT: Then Peter went down to the men, and said, “Yes, I am he whom you seek. For what reason have you come?”
