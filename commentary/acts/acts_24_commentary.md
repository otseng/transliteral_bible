# Acts 24

<a name="acts_24_6"></a>[Acts 24:6](../../bible/acts/acts_24.md#acts_24_6)

TR: "He even tried to profane the temple, and we seized him, and wanted to judge him according to our law. But the commander Lysias came by and with great violence took him out of our hands, commanding his accusers to come to you. By examining him yourself you may ascertain all these things of which we accuse him.”

MT/CT: He even tried to profane the temple, and we seized him. By examining him yourself you may ascertain all these things of which we accuse him.”
