# Acts 2

<a name="acts_2_30"></a>[Acts 2:30](../../bible/acts/acts_2.md#acts_2_30)

MT/TR: Therefore, being a prophet, and knowing that God had sworn with an oath to him that of the fruit of his body, according to the flesh, He would raise up the Christ to sit on his throne

CT: Therefore, being a prophet, and knowing that God had sworn with an oath to him that of the fruit of his body, He would seat one on his throne
