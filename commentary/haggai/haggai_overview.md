# Haggai Overview

## Title

* Hebrew: Chaggai (Feast)
* Greek: Ἀγγαῖος Ιʹ (X. Angaios)
* Latin: Aggæi

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Haggai

## Dating

* 520 BC (second year of King Darius). 70 years after exile.
* Contemporary with Zechariah

## Genre

* Prophecy

## Literary form

* Prose

## Audience

* Israel

## Themes

* Rebuilding the temple

## Statistics

* 2 chapters, 38 verses, 1131 words.

## Videos

### David Pawson

* [Haggai](https://www.youtube.com/watch?v=juPvv_xcX-U&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=28&pp=iAQB)

### Bible Project

* [Haggai](https://www.youtube.com/watch?v=juPvv_xcX-U&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=28&pp=iAQB)

### Randall Smith

* [Haggai](https://www.youtube.com/watch?v=T_NRw4qVU38&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=27&pp=iAQB)

## Timelines

* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

## Notable passages

* Hag 1:6 Ye have sown much, and bring in little; ye eat, but ye have not enough; ye drink, but ye are not filled with drink; ye clothe you, but there is none warm; and he that earneth wages earneth wages [to put it] into a bag with holes.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/haggai/haggai-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/haggai-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-haggai)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-haggai/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Haggai&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/haggai)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Haggai)

[Precept Austin](https://www.preceptaustin.org/haggai_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-haggai/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/haggai/haggai.htm)

[Bible Hub](https://biblehub.com/sum/haggai/)