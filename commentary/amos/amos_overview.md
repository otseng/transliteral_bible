# Amos Overview

## Title

* Hebrew: 'Amos (Burdened, Troubled)
* Greek: Ἀμώς Βʹ (II. Āmōs)
* Latin: Amos

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Amos

## Dating

* ~ 760 BC

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Northern Kingdom

## Statistics

* 9 chapters, 146 verses, 4217 words.

## Videos

### David Pawson

* [Amos](https://www.youtube.com/watch?v=9F_OBKkgj54&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=50&pp=iAQB)

### Bible Project

* [Amos](https://www.youtube.com/watch?v=mGgWaPGpGz4&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=21)

### Randall Smith

* [Amos](https://www.youtube.com/watch?v=r5e5eueK7PM&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=20)

## Timelines

* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)

## Notable passages

* Amos 5:24 - But let justice roll down like waters, and righteousness like an ever-flowing stream.
* Amos 8:11 - Behold, the days come, saith the Lord GOD, that I will send a famine in the land, not a famine of bread, nor a thirst for water, but of hearing the words of the LORD:

## Notes

* Amos was a shepherd and fig tree farmer from Tekoa, a small town in Judah, making him one of the few prophets with a humble, non-priestly background.
* Contemporary of Hosea, Isaiah, and Micah

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/amos/amos-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/amos-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-amos)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-amos/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Amos&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/amos)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Amos)

[Precept Austin](https://www.preceptaustin.org/amos_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-amos/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/amos/amos.htm)

[Bible Hub](https://biblehub.com/sum/amos/)