# 2 Kings Overview

## Title

* Hebrew: Melachim (Kings)
* Greek: Βασιλειῶν Δʹ (4 Basileiōn)
* Latin: 4 Regum (4 Kings)

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Unknown
* Possibly Jeremiah

## Dating

* 562 B.C.-536 B.C

## Genre

* History

## Literary form

* Narrative

## Audience

* Israel

## Statistics

* 25 chapters, 719 verses, 23,532 words.

## Videos

### David Pawson

* [Kings 1/2](https://www.youtube.com/watch?v=tjfEgekLD5s&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=22&pp=iAQB)
* [Kings 1/2](https://www.youtube.com/watch?v=kkj7q9vI3Zs&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=23&pp=iAQB)

### Bible Project

* [Kings](https://www.youtube.com/watch?v=bVFW3wbi9pk&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=13&pp=iAQB)

## Timelines

* [930-810 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJoel,%20Obadiah,%202%20Chronicles,%201%20Kings,%202%20Kings%3E930-810_BCE)
* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)
* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)
* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1kings/1kings-2kings-overview.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/2kings-intro.cfm)

[Bible.org](https://bible.org/article/introduction-books-first-and-second-kings)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-2-kings/)

[USCCB](https://bible.usccb.org/bible/1kings/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/second-kings)

[Wikipedia](https://en.wikipedia.org/wiki/Books_of_Kings)

[Precept Austin](https://www.preceptaustin.org/2_kings_commentaries)

[Bible Project](https://bibleproject.com/guides/books-of-kings/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2kings/2kings.htm)

[Bible Hub](https://biblehub.com/sum/2_kings)
