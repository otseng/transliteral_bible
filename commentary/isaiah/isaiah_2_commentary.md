# Isaiah 2

<a name="isaiah_2_4"></a>[Isaiah 2:4](../../bible/isaiah/isaiah_2.md#isaiah_2_4)

## Cross references

Verbatim quote in [Micah 4:3](../../bible/micah/micah_4.md#micah_4_3)

Referenced in [Joel 3:10](../../bible/joel/joel_3.md#joel_3_10))

## Wikipedia

[Swords to ploughshares](https://en.wikipedia.org/wiki/Swords_to_ploughshares)

## United Nations

Passage is on Isaiah Wall in [Ralph Bunche Park](https://en.wikipedia.org/wiki/Ralph_Bunche_Park), which is across the street from the United Nations building in New York.

Article 2(4) of the UN Charter:

“All Members shall refrain in their international relations from the threat or use of force against the territorial integrity or political independence of any state, or in any other manner inconsistent with the Purposes of the United Nations.”

```
From UN.org: “The United Nations garden contains several sculptures and statues that have been donated by different countries. This one is called "Let Us Beat Swords into Plowshares" and was a gift from the then Soviet Union presented in 1959. Made by Evgeniy Vuchetich, the bronze statue represents the figure of a man holding a hammer in one hand and, in the other, a sword which he is making into a plowshare, symbolizing man's desire to put an end to war and convert the means of destruction into creative tools for the benefit of all mankind.”
```
[United Nations garden sculpture](https://www.college.columbia.edu/core/content/%E2%80%9Clet-us-beat-our-swords-plowshares%E2%80%9D-evgeniy-vuchetich-united-nations-art-collection-1959)

[Isaiah’s Echo: Progress, Prophecy, and the UN Charter](https://www.ejiltalk.org/isaiahs-echo-progress-prophecy-and-the-un-charter/)

