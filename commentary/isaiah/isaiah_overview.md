# Isaiah Overview

## Title

* Hebrew: Yeshayah "God is Salvation"

## Collection

* Nevi'im
* First of major prophets

## Author

* Isaiah ben Amoz
* The prophet lived until at least 681 B.C. when he penned the account of Sennacherib’s death (cf. 37:38). Tradition has it that he met his death under King Manasseh (ca. 695–642 B.C.) by being cut in two with a wooden saw (cf. Heb. 11:37).
* There is debate as to whether there was one or two (three or four) author’s of the book because there does not seem to be any reason for eighth century Isaiah to discuss events lying 200 years in the future for Hezekiah’s generation. It is assumed that chapters 40-66 were written by a Second Isaiah at the end of the exile to deported and defeated fellow-countrymen. This broad generalization is not a necessary conclusion.
* Lived most of his life in Judah

## Dating

* Isaiah began his prophetic ministry in the last year of Uzziah, 742 B.C. His prophetic ministry lasted some 61 years until his death in 681 B.C.
* 740-700 B.C.
* Period of the divided kingdom
* Northern Kingdom collapsed under Assyria in 722/721 B.C
* Isaiah lived during the decline of Israel in the shadow of Assyria
* Contemporary of Amos, Hosea and Micah
* Prophesied Babylonian captivity

## Genre

* Prophecy

## Literary form

* Primarily poetry, with some transitional prose between chapters and a prose section in chapters 36-39.

## Audience

* Judah

## Themes

* role of Jerusalem in God's plan for the world
* "the Fifth Gospel"

## References

* Isaiah is quoted directly in the NT over 65 times, far more than any other OT prophet, and mentioned by name over 20 times.

## Parallels with Bible

| Bible  | Isaiah |
| ---  |  ---  |
| 66 Books | 66 Chapters |
| 39 Books in Old Testament | 39 Chapters on law, the Government of God  |
| 27 Books in New Testament	| 27 Chapters on grace, Salvation of God |

## Commentaries

* COMMENTARY:::Bridgeway:::Isaiah
* COMMENTARY:::OBB:::Isaiah
* COMMENTARY:::Peake:::Isaiah
* COMMENTARY:::Sutcliffe:::Isaiah

## Statistics

* 66 chapters, 1292 verses, 37,044 words.

## Videos

* [Justin Johnson](https://graceambassadors.com/books/isaiah-01)
* [Jacques Doukhan](https://www.youtube.com/watch?v=9t5W41WTv2E&list=PLlwzgHJ6-Ni9Vwob0j2VyoT49yDCebZbn)

### David Pawson

* [Isaiah 1/2](https://www.youtube.com/watch?v=XqNzGrYbFWg&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=37&t=18s&pp=iAQB)
* [Isaiah 2/2](https://www.youtube.com/watch?v=1VLLZ_6ZjiE&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=38&t=806s&pp=iAQB)

### Bible Project

* [Isaiah](https://www.youtube.com/watch?v=d0A6Uchb1F8&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=14)

### Randall Smith

* [Isaiah](https://www.youtube.com/watch?v=HbTSdnno4RA&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=15)

## Timelines

* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)

## Influence on culture

* Isaiah 2:4 - [Swords to ploughshares](https://en.wikipedia.org/wiki/Swords_to_ploughshares)
* [Handel's Messiah](https://en.wikipedia.org/wiki/Messiah_(Handel))

## Notable passages

* [Isaiah 6:8](../../bible/isaiah/isaiah_6.md) - Here am I; send me.
* [Isaiah 40](../../bible/isaiah/isaiah_40.md) 
    - A voice of one calling in the wilderness
    - Grass withers and the flowers fall, but God's word endures
    - Gives strength to the weary, soar on wings like eagles
* [Isaiah 53](../../bible/isaiah/isaiah_53.md) - Suffering servant

## Notes

* Most quoted prophet in NT
* Second most quoted OT book in NT after Psalms

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/isaiah/isaiah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/isaiah-intro.cfm)

[Bible.org](https://bible.org/article/introduction-isaiah)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-isaiah/)

[Jamieson, Fausset & Brown](https://www.blueletterbible.org/Comm/jfb/Isa/Isa_000.cfm?a=685003)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-isaiah/)

[USCCB](https://bible.usccb.org/bible/isaiah/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-isaiah/)

[Charles Swindoll](https://insight.org/resources/bible/the-major-prophets/isaiah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Isaiah)

[Precept Austin](https://www.preceptaustin.org/isaiah_commentaries)

[Isaiah Connections](https://isaiahminibible.com/)

[Bible Project](https://bibleproject.com/guides/book-of-isaiah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/isaiah/isaiah.htm)

[Bible Hub](https://biblehub.com/sum/isaiah)
