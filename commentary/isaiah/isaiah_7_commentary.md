# Isaiah 7

<a name="isaiah_7_14"></a>[Isaiah 7:14](../../bible/isaiah/isaiah_7.md#isaiah_7_14)

[Wikipedia](https://en.wikipedia.org/wiki/Isaiah_7:14)

[Michael Heiser](https://drmsh.com/the-almah-of-isaiah-714/)

[Michael Chiavone - Matthew's Use of Isaiah 7:14: A Valid Hermeneutic](https://digitalcommons.cedarville.edu/cgi/viewcontent.cgi?referer=&httpsredir=1&article=1128&context=biblical_and_ministry_studies_presentations)

[Jean Jones](https://www.jeanejones.net/2020/12/the-virgin-shall-conceive-why-isaiah-714-confuses-people/)

