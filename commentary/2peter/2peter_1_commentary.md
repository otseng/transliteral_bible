# 2 Peter 1

<a name=2peter_1_21></a>[2 Peter 1:21](../../bible/2peter/2peter_1.md#2peter_1_21)

MT/TR: for prophecy never came by the will of man, but holy men of God spoke as they were moved by the Holy Spirit.

CT: for prophecy never came by the will of man, but men spoke from God as they were moved by the Holy Spirit.
