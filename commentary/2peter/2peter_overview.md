# 2 Peter Overview

## Title

* Second Epistle of Peter

## Author

* Peter

* the most disputed book in the NT canon as to authenticity

* Dr. William Moorehead said, “The Second Epistle of Peter comes to us with less historical support of its genuineness than any other book of the New Testament.” 

* "Among modern scholars there is wide agreement that 2 Peter is a pseudonymous work, i.e., one written by a later author who attributed it to Peter according to a literary convention popular at the time. It gives the impression of being more remote in time from the apostolic period than 1 Peter; indeed, many think it is the latest work in the New Testament and assign it to the first or even the second quarter of the second century."

## Canon

* The Peshitta does not contain 2 Peter

## Dating

* 65-68 AD

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Churches in Asia Minor

* "the second letter I am writing to you.” (2 Peter 3:1)

## Themes

* Importance of learning and clinging to the proper knowledge of God

## Statistics

* 3 chapters, 61 verses, 1559 words

## Videos

### David Pawson

* [2 Peter](https://www.youtube.com/watch?v=qJyTe0NZNVY&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=29)

### Bible Project

* [2 Peter](https://www.youtube.com/watch?v=wWLv_ITyKYc&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=28&pp=iAQB)

### Randall Smith

* [2 Peter](https://www.youtube.com/watch?v=GAxXSx9kGYw&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=48&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Written in poor Greek (compared to 1 Peter in good Greek)

* Contains many [Hapax legomenon](https://en.wikipedia.org/wiki/Hapax_legomenon)

* Swan song of Peter

* 2 Peter was not as widely known and recognized in the early church as 1 Peter

* Similarities between 2 Peter and Jude (2Pet 2 with Jude 4–18)

* "knowledge" appears 15 times in the book

## Links

[Blue Letter Bible](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/2peter-intro.cfm)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/2peter/2peter-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/2peter/2peter-outline.cfm)

[Bible.org](https://bible.org/seriespage/22-second-peter-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-2-peter/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-2-peter/)

[USCCB](https://bible.usccb.org/bible/2peter/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-2-peter/)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/second-peter)

[Wikipedia](https://en.wikipedia.org/wiki/Second_Epistle_of_Peter)

[Precept Austin](https://www.preceptaustin.org/2_peter_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-2-peter/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2peter/2peter.htm)

[Bible Hub](https://biblehub.com/sum/2_peter)
