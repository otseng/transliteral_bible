# Micah Overview

## Title

* Hebrew: Micha (Who is like God)
* Greek: Μιχαίας Γʹ (III. Michaias)
* Latin: Michææ

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Micah, Micaiah

## Dating

* 740 BC - 687 BC, 8th century BC
* Contemporary of Isaiah

## Genre

* Prophecy

## Literary form

* Poetry

## Audience

* Northern Kingdom

## Theme

* Judgment and pardon

## Statistics

* 7 chapters, 105 verses, 3153 words.

## Videos

### David Pawson

* [Micah](https://www.youtube.com/watch?v=qhsA1DoYqBw&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=52)

### Bible Project

* [Micah](https://www.youtube.com/watch?v=MFEUEcylwLc&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=24)

### Randall Smith

* [Micah](https://www.youtube.com/watch?v=NHx6Xx1Q1n4&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=24)

## Timelines

* [810-690 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJonah,%20Micah,%20Isaiah,%20Amos,%20Hosea,%202%20Chronicles,%202%20Kings%3E810-690_BCE)

## Notable passages

* Micah 4:3 And he shall judge among many people, and rebuke strong nations afar off; and they shall beat their swords into plowshares, and their spears into pruning hooks: nation shall not lift up a sword against nation, neither shall they learn war any more.
* Micah 5:2 But thou, Bethlehem Ephratah, [though] thou be little among the thousands of Judah, [yet] out of thee shall he come forth unto me [that is] to be ruler in Israel; whose goings forth [have been] from of old, from everlasting.
* Micah 6:8 He hath shewed thee, O man, what [is] good; and what doth the LORD require of thee, but to do justly, and to love mercy, and to walk humbly with thy God?

## Notes

* Micah was final prophet to Northern Kingdom
* Only prophet sent to both the Southern and Northern Kingdoms
* First prophet to predict the Babylonian captivity
* Contemporary with Isaiah, Hosea, Amos
* Quoted by Jeremiah

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/micah/micah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/micah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-micah)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-micah/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Micah&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/micah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Micah)

[Precept Austin](https://www.preceptaustin.org/micah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-micah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/micah/micah.htm)

[Bible Hub](https://biblehub.com/sum/micah/)