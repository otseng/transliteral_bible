# Luke 2

<a name="luke_2_14"></a>[Luke 2:14](../../bible/luke/luke_2.md#luke_2_14)

MT/TR: “Glory to God in the highest, and on earth peace, goodwill toward men!”

CT: "Glory to God in the highest, and on earth peace to men on whom his favor rests."
