# Luke 9

<a name="luke_9_55"></a>[Luke 9:55](../../bible/luke/luke_9.md#luke_9_55)

MT/TR: But He turned and rebuked them, and said, “You do not know what manner of spirit you are of. For the Son of Man did not come to destroy men’s lives but to save them.” And they went to another village.

CT: But He turned and rebuked them. And they went to another village.
