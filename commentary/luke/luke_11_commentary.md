# Luke 11

<a name="luke_11_2"></a>[Luke 11:2](../../bible/luke/luke_11.md#luke_11_2)

MT/TR: So He said to them, “When you pray, say: Our Father in heaven, Hallowed be Your name. Your kingdom come. Your will be done On earth as it is in heaven. Give us day by day our daily bread. And forgive us our sins, For we also forgive everyone who is indebted to us. And do not lead us into temptation, But deliver us from the evil one.”

CT: So He said to them, “When you pray, say: Father, Hallowed be Your name. Your kingdom come. Give us day by day our daily bread. And forgive us our sins, For we also forgive everyone who is indebted to us. And do not lead us into temptation.”

<a name="luke_11_11"></a>[Luke 11:11](../../bible/luke/luke_11.md#luke_11_11)

MT/TR: If a son asks for bread from any father among you, will he give him a stone? Or if he asks for a fish, will he give him a serpent instead of a fish?

CT: If a son asks from any father among you for a fish, will he give him a serpent instead of a fish?
