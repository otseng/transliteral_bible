# Luke 22

<a name="luke_22_43"></a>[Luke 22:43](../../bible/luke/luke_22.md#luke_22_43)

MT/TR: Then an angel appeared to Him from heaven, strengthening Him. And being in agony, He prayed more earnestly. Then His sweat became like great drops of blood falling down to the ground.

CT: Verses marked as a later addition
