# Luke 1

<a name="luke_1_28"></a>[Luke 1:28](../../bible/luke/luke_1.md#luke_1_28)

MT/TR: And having come in, the angel said to her, “Rejoice, highly favored one, the Lord is with you; blessed are you among women!”

CT: And having come in, the angel said to her, “Rejoice, highly favored one, the Lord is with you!”
