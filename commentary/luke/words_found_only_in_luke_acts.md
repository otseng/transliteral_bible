# Words found only in Luke/Acts

[achlys](../../strongs/g/g887)

[Adramyttēnos](../../strongs/g/g98)

[agkalē](../../strongs/g/g43)

[agoraios](../../strongs/g/g60)

[agrammatos](../../strongs/g/g62.md)

[akatakritos](../../strongs/g/g178)

[akroatērion](../../strongs/g/g201)

[aichmalōtos](../../strongs/g/g164.md)

[Aithiops](../strongs/g/g128)

[aitiama](../../strongs/g/g157)

[akolytos](../../strongs/g/g209)

[akribēs](../../strongs/g/g196)

[Alexandreus](../../strongs/g/g221.md)

[Alexandrinos](../../strongs/g/g222)

[alisgēma](../../strongs/g/g234)

[allophylos](../strongs/g/g246)

[amartyros](../../strongs/g/g267)

[amynomai](../../strongs/g/g292.md)

[anabolē](../../strongs/g/g311)

[anaideia](../strongs/g/g335)

[anadeixis](../../strongs/g/g323.md)

[anairesis](../../strongs/g/g336.md)

[anagnōrizō](../../strongs/g/g319.md)

[anakrisis](../../strongs/g/g351)

[analēmpsis](../../strongs/g/g354)

[anapeithō](../../strongs/g/g374)

[anantirrētōs](../strongs/g/g369)

[anapsyxis](../../strongs/g/g403.md)

[anaskeuazō](../../strongs/g/g384)

[anaspaō](../../strongs/g/g385.md)

[anatrephō](../../strongs/g/g397.md)

[anetazō](../../strongs/g/g426)

[aneuthetos](../../strongs/g/g428)

[anoikodomeō](../../strongs/g/g456)

[anthomologeomai](../../strongs/g/g437.md)

[anthypatos](../../strongs/g/g446)

[Antipatris](../../strongs/g/g494)

[antipiptō](../../strongs/g/g496)

[antophthalmeō](../../strongs/g/g503)

[apartismos](../../strongs/g/g535)

[apelaunō](../../strongs/g/g556)

[apelegmos](../../strongs/g/g557.md)

[aperitmētos](../../strongs/g/g564)

[aphixis](../../strongs/g/g867.md)

[apokatastasis](../../strongs/g/g605.md)

[apophortizomai](../../strongs/g/g670)

[apoplynō](../../strongs/g/g637)

[aposkeuazō](../../strongs/g/g643)

[Appios](../../strongs/g/g675)

[Araps](../../strongs/g/g690)

[archieratikos](../../strongs/g/g748.md)

[Areopagitēs](../../strongs/g/g698)

[argyrokopos](../../strongs/g/g695)

[arotron](../../strongs/g/g723)

[artemōn](../../strongs/g/g736)

[Artemis](../../strongs/g/g735.md)

[asēmos](../../strongs/g/g767)

[Asianos](../../strongs/g/g774)

[Asiarchēs](../../strongs/g/g775)

[asitia](../../strongs/g/g776)

[asitos](../../strongs/g/g777)

[askeō](../../strongs/g/g778)

[asōtōs](../../strongs/g/g811)

[Assos](../../strongs/g/g789.md)

[asymphonos](../../strongs/g/g800)

[Athēnaios](../../strongs/g/g117)

[Attaleia](../../strongs/g/g825)

[augoustos](../../strongs/g/g828.md)

[autoptēs](../../strongs/g/g845)

[basis](../../strongs/g/g939.md)

[bathynō](../../strongs/g/g900)

[Beroia](../../strongs/g/g960)

[Beroiaios](../../strongs/g/g961)

[biōsis](../../strongs/g/g981)

[bolizō](../../strongs/g/g1001)

[bōmos](../../strongs/g/g1041)

[brychō](../../strongs/g/g1031)

[byrseus](../strongs/g/g1038)

[Chaldaios](../../strongs/g/g5466.md)

[cheimazō](../../strongs/g/g5492)

[cheiragōgos](../../strongs/g/g5497)

[Chios](../../strongs/g/g5508.md)

[chronotribeō](../../strongs/g/g5551.md)

[chortasma](../../strongs/g/g5527.md)

[chrōs](../../strongs/g/g5559)

[daktylios](../strongs/g/g1146)

[Damaris](../../strongs/g/g1152)

[daneistēs](../strongs/g/g1157)

[dapanē](../../strongs/g/g1160.md)

[deisidaimōn](../../strongs/g/g1174)

[deisidaimonia](../../strongs/g/g1175)

[dekadyo](../../strongs/g/g1177)

[dēmēgoreō](../../strongs/g/g1215)

[dēmosios](../../strongs/g/g1219)

[Derbaios](../../strongs/g/g1190)

[desmeō](../../strongs/g/g1196)

[desmophylax](../../strongs/g/g1200)

[diaginōskō](../../strongs/g/g1231)

[diagrēgoreō](../strongs/g/g1235)

[diaseiō](../strongs/g/g1286)

[diēgēsis](../strongs/g/g1335)

[dikastēs](../../strongs/g/g1348)

[diaballō](../strongs/g/g1225)

[diadechomai](../../strongs/g/g1237)

[dianemō](../../strongs/g/g1268.md)

[diacheirizō](../../strongs/g/g1315.md)

[diagnōrizō](../../strongs/g/g1232.md)

[diakatelegchomai](../../strongs/g/g1246)

[dialyō](../../strongs/g/g1262.md)

[dianyktereuō](../../strongs/g/g1273.md)

[dialaleō](../strongs/g/g1255)

[diapheugō](../../strongs/g/g1309)

[diaphthora](../../strongs/g/g1312)

[diaponeomai](../../strongs/g/g1278.md)

[diapriō](../../strongs/g/g1282)

[diastēma](../../strongs/g/g1292)

[diatarassō](../../strongs/g/g1298)

[diïschyrizomai](../../strongs/g/g1340)

[Dionysios](../../strongs/g/g1354)

[diorthōma](../../strongs/g/g2735)

[dithalassos](../../strongs/g/g1337)

[dōdekaphylon](../../strongs/g/g1429)

[Drousilla](../../strongs/g/g1409)

[dysenteria](../../strongs/g/g1420)

[echthes](../../strongs/g/g5504)

[edaphos](../../strongs/g/g1475.md)

[egkyos](../../strongs/g/g1471)

[eispēdaō](../../strongs/g/g1530)

[ekdiēgeomai](../../strongs/g/g1555)

[egklēma](../../strongs/g/g1462)

[ekplēroō](../../strongs/g/g1603)

[ekpsychō](../../strongs/g/g1634.md)

[ektarassō](../../strongs/g/g1613)

[ekthetos](../../strongs/g/g1570.md)

[ektithēmi](../../strongs/g/g1620.md)

[Elamitēs](../strongs/g/g1639)

[Eliezer](../../strongs/g/g1663)

[Elmōdam](../strongs/g/g1678)

[emballō](../../strongs/g/g1685)

[emmainomai](../../strongs/g/g1693)

[Emmor](../../strongs/g/g1697.md)

[endeēs](../../strongs/g/g1729.md)

[enedron](../../strongs/g/g1749.md)

[epaiteō](../../strongs/g/g1871)

[epakroaomai](../../strongs/g/g1874)

[epanagkes](../../strongs/g/g1876)

[ephallomai](../../strongs/g/g2177)

[epikouria](../../strongs/g/g1947.md)

[Epikoureios](../../strongs/g/g1946)

[epineuō](../../strongs/g/g1962)

[episphalēs](../../strongs/g/g2000.md)

[episitismos](../../strongs/g/g1979)

[epitropē](../../strongs/g/g2011)

[epistrophē](../../strongs/g/g1995)

[euergeteō](../strongs/g/g2109)

[eulabēs](../../strongs/g/g2126)

[euroklydōn](../../strongs/g/g2148)

[euthydromeō](../../strongs/g/g2113)

[exeimi](../../strongs/g/g1826.md)

[exochē](../../strongs/g/g1851)

[exolethreuō](../../strongs/g/g1842.md)

[exorkistēs](../../strongs/g/g1845)

[exōtheō](../../strongs/g/g1856)

[exypnos](../../strongs/g/g1853)

[Galliōn](../../strongs/g/g1058)

[gelaō](../../strongs/g/g1070)

[gēras](../../strongs/g/g1094)

[gerousia](../../strongs/g/g1087)

[gnōstēs](../../strongs/g/g1109)

[hagnismos](../../strongs/g/g49)

[Hebraïs](../../strongs/g/g1446)

[Hellas](../../strongs/g/g1671)

[Hesli](../strongs/g/g2069)

[hierateuō](../../strongs/g/g2407)

[hierosylos](../../strongs/g/g2417)

[hodeuō](../../strongs/g/g3593)

[hodoiporeō](../strongs/g/g3596)

[holoklēria](../../strongs/g/g3647.md)

[homotechnos](../../strongs/g/g3673)

[hyperekchyn(n)ō](../strongs/g/g5240)

[hypēreteō](../../strongs/g/g5256)

[hyperoraō](../../strongs/g/g5237)

[hypoballō](../../strongs/g/g5260.md)

[hypochōreō](../../strongs/g/g5298)

[hypopneō](../../strongs/g/g5285)

[iasis](../../strongs/g/g2392.md)

[kakōsis](../../strongs/g/g2561)

[Kandakē](../strongs/g/g2582)

[karpophoros](../../strongs/g/g2593)

[kataklēronomeō](../../strongs/g/g2624)

[katakrēmnizō](../../strongs/g/g2630.md)

[kataloipos](../../strongs/g/g2645)

[kataneuō](../strongs/g/g2656)

[katephistamai](../../strongs/g/g2721)

[kataphronētēs](../../strongs/g/g2707)

[katapiptō](../../strongs/g/g2667)

[kataponeō](../../strongs/g/g2669.md)

[kataschesis](../../strongs/g/g2697.md)

[katasophizomai](../../strongs/g/g2686.md)

[katasyrō](../strongs/g/g2694)

[kateidōlos](../../strongs/g/g2712)

[kathaptō](../../strongs/g/g2510)

[kathēmerinos](../../strongs/g/g2522.md)

[kathexēs](../../strongs/g/g2517.md)

[katoikia](../../strongs/g/g2733)

[keration](../../strongs/g/g2769)

[kichrēmi](../../strongs/g/g5531)

[Kis](../../strongs/g/g2797)

[Kleopas](../strongs/g/g2810)

[Knidos](../../strongs/g/g2834)

[kolōnia](../../strongs/g/g2862)

[kolymbaō](../../strongs/g/g2860)

[kopetos](../../strongs/g/g2870)

[korax](../../strongs/g/g2876)

[kouphizō](../../strongs/g/g2893)

[kratistos](../../strongs/g/g2903.md)

[ktētōr](../../strongs/g/g2935.md)

[Kyprios](../../strongs/g/g2953.md)

[lamprotēs](../../strongs/g/g2987)

[Lasaia](../../strongs/g/g2996.md)

[leios](../strongs/g/g3006)

[Libertinos](../../strongs/g/g3032.md)

[Libyē](../strongs/g/g3033)

[limēn](../../strongs/g/g3040.md)

[Lykaonia](../../strongs/g/g3071)

[Lykaonisti](../../strongs/g/g3072)

[Lykia](../../strongs/g/g3073)

[lymainō](../../strongs/g/g3075)

[lytrōtēs](../../strongs/g/g3086)

[Madiam](../../strongs/g/g3099)

[mageia](../strongs/g/g3095)

[makrothymōs](../../strongs/g/g3116)

[Manaēn](../../strongs/g/g3127)

[mania](../../strongs/g/g3130.md)

[manteuomai](../../strongs/g/g3132)

[mastizō](../../strongs/g/g3147)

[Mathousala](../strongs/g/g3103)

[mēdamōs](../strongs/g/g3365.md)

[Mēdos](../strongs/g/g3370)

[Melea](../strongs/g/g3190)

[Melitē](../../strongs/g/g3194)

[meristēs](../../strongs/g/g3312)

[mesēmbria](../strongs/g/g3314)

[Mesopotamia](../../strongs/g/g3318.md)

[metapempō](../strongs/g/g3343)

[metoikizō](../../strongs/g/g3351.md)

[misthoma](../../strongs/g/g3410)

[Mnasōn](../../strongs/g/g3416)

[Moloch](../../strongs/g/g3434)

[moschopoieō](../../strongs/g/g3447)

[Myra](../../strongs/g/g3460)

[nauklēros](../../strongs/g/g3490)

[naus](../../strongs/g/g3491)

[neanias](../../strongs/g/g3494)

[Nea Polis](../../strongs/g/g3496)

[nēsion](../../strongs/g/g3519)

[Niger](../../strongs/g/g3526)

[ochleō](../../strongs/g/g3791)

[ochlopoieō](../../strongs/g/g3792)

[oikēma](../../strongs/g/g3612)

[oneidos](../../strongs/g/g3681)

[ōneomai](../../strongs/g/g5608.md)

[ōon](../strongs/g/g5609)

[othonē](../strongs/g/g3607)

[ouranothen](../../strongs/g/g3771)

[pandocheion](../../strongs/g/g3829)

[pandocheus](../../strongs/g/g3830)

[Paphos](../../strongs/g/g3974)

[paracheimasia](../../strongs/g/g3915)

[paradoxos](../../strongs/g/g3861)

[paranomeō](../../strongs/g/g3891)

[paratygchanō](../../strongs/g/g3909)

[parenochleō](../../strongs/g/g3926)

[paroichomai](../../strongs/g/g3944)

[parotrynō](../../strongs/g/g3951.md)

[pathētos](../../strongs/g/g3805.md)

[patrōos](../../strongs/g/g3971)

[Pergē](../../strongs/g/g4011)

[periochē](../strongs/g/g4042)

[perirēgnymi](../../strongs/g/g4048)

[perispaō](../strongs/g/g4049)

[perioikeō](../../strongs/g/g4039)

[phantasia](../../strongs/g/g5325)

[pharagx](../../strongs/g/g5327)

[phasis](../../strongs/g/g5334.md)

[Phēstos](../../strongs/g/g5347)

[philophronōs](../../strongs/g/g5390)

[Phoinikē](../../strongs/g/g5403)

[Phoinix](../../strongs/g/g5405)

[phoron](../../strongs/g/g5410)

[phortos](../../strongs/g/g5414)

[phylakizō](../../strongs/g/g5439)

[phryassō](../../strongs/g/g5433.md)

[phryganon](../../strongs/g/g5434)

[pimprēmi](../../strongs/g/g4092)

[plēmmyra](../../strongs/g/g4132)

[ploos](../../strongs/g/g4144)

[pnoē](../../strongs/g/g4157)

[politarchēs](../../strongs/g/g4173)

[Pontikos](../../strongs/g/g4193)

[Poplios](../../strongs/g/g4196.md)

[Porkios](../../strongs/g/g4201)

[porphyropōlis](../../strongs/g/g4211)

[Potioloi](../../strongs/g/g4223)

[procheirotoneō](../strongs/g/g4401)

[prokēryssō](../../strongs/g/g4296.md)

[prōra](../../strongs/g/g4408)

[prosanabainō](../../strongs/g/g4320)

[prosapeileō](../../strongs/g/g4324.md)

[prosdapanaō](../../strongs/g/g4325)

[prosklēroō](../../strongs/g/g4345)

[prosōpolēmptēs](../strongs/g/g4381)

[prospeinos](../strongs/g/g4361)

[prōtostatēs](../../strongs/g/g4414)

[proteinō](../../strongs/g/g4385)

[protrepō](../../strongs/g/g4389)

[Ptolemaïs](../../strongs/g/g4424)

[pythōn](../../strongs/g/g4436)

[pyra](../../strongs/g/g4443.md)

[rhabdouchos](../../strongs/g/g4465)

[rhadiourgia](../../strongs/g/g4468)

[rhadiourgēma](../../strongs/g/g4467)

[Rhaiphan](../../strongs/g/g4481)

[Rhēgion](../../strongs/g/g4484)

[rhēgma](../../strongs/g/g4485.md)

[rhētōr](../../strongs/g/g4489.md)

[rhipteō](../../strongs/g/g4495)

[Rhodē](../../strongs/g/g4498.md)

[Rhodos](../../strongs/g/g4499)

[rhōnnymi](../../strongs/g/g4517)

[Salamis](../../strongs/g/g4529)

[Salmōnē](../../strongs/g/g4534)

[Samos](../../strongs/g/g4544.md)

[Samothrakē](../../strongs/g/g4543)

[sanis](../../strongs/g/g4548)

[Sapphira](../../strongs/g/g4551.md)

[Seleukeia](../../strongs/g/g4581)

[sikarios](../../strongs/g/g4607)

[Silas](../../strongs/g/g4609)

[sitometrion](../../strongs/g/g4620)

[skēnopoios](../../strongs/g/g4635)

[Skeuas](../../strongs/g/g4630)

[skeuē](../../strongs/g/g4631)

[sklērotrachēlos](../../strongs/g/g4644)

[skōlēkobrōtos](../../strongs/g/g4662)

[skylon](../../strongs/g/g4661)

[Sōpatros](../../strongs/g/g4986)

[sphagion](../../strongs/g/g4968)

[sphodrōs](../../strongs/g/g4971)

[sphydron](../../strongs/g/g4974.md)

[stigmē](../strongs/g/g4743)

[Stoïkos](../../strongs/g/g4770)

[stratia](../../strongs/g/g4756)

[stratopedarchēs](../../strongs/g/g4759)

[Sousanna](../strongs/g/g4677)

[Sychem](../../strongs/g/g4966.md)

[sygkalyptō](../strongs/g/g4780)

[sygkineō](../../strongs/g/g4787.md)

[sygkomizō](../../strongs/g/g4792)

[sygkyria](../strongs/g/g4795)

[symperilambanō](../../strongs/g/g4843)

[sympinō](../strongs/g/g4844)

[synallassō](../../strongs/g/g4900.md)

[synephistēmi](../../strongs/g/g4911)

[synodia](../../strongs/g/g4923)

[synomileō](../strongs/g/g4926)

[synomoreō](../../strongs/g/g4927)

[synarpazō](../../strongs/g/g4884.md)

[synōmosia](../../strongs/g/g4945)

[synthryptō](../../strongs/g/g4919)

[Syrtis](../../strongs/g/g4950)

[sysparassō](../../strongs/g/g4952)

[systrophē](../../strongs/g/g4963)

[syzētēsis](../../strongs/g/g4803)

[tabernai](../../strongs/g/g4999)

[telesphoreō](../../strongs/g/g5052)

[Tertyllos](../../strongs/g/g5061)

[tetradion](../../strongs/g/g5069)

[tharsos](../../strongs/g/g2294)

[theomacheō](../../strongs/g/g2313)

[theomachos](../../strongs/g/g2314.md)

[Theophilos](../../strongs/g/g2321)

[thēreuō](../../strongs/g/g2340)

[thermē](../../strongs/g/g2329)

[Theudas](../../strongs/g/g2333.md)

[thorybazō](../../strongs/g/g5182.md)

[thymomacheō](../../strongs/g/g2371)

[timōreō](../../strongs/g/g5097)

[toichos](../../strongs/g/g5109)

[trachys](../../strongs/g/g5138)

[trauma](../../strongs/g/g5134.md)

[tristegon](../../strongs/g/g5152)

[Trōgyllion](../../strongs/g/g5175.md)

[tropophoreō](../../strongs/g/g5159)

[typhōnikos](../../strongs/g/g5189)

[Tyrannos](../../strongs/g/g5181)

[Tyrios](../../strongs/g/g5183)

[zētēma](../../strongs/g/g2213)

[zeuktēria](../../strongs/g/g2202)
