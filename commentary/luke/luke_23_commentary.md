# Luke 23

<a name="luke_23_17"></a>[Luke 23:17](../../bible/luke/luke_23.md#luke_23_17)

MT/TR: (for it was necessary for him to release one to them at the feast)
CT: Verse omitted

<a name="luke_23_34"></a>[Luke 23:34](../../bible/luke/luke_23.md#luke_23_34)

MT/TR: Then Jesus said, “Father, forgive them, for they do not know what they do.” And they divided His garments and cast lots.

CT: And they divided His garments and cast lots. (The first sentence is marked as a later addition)

<a name="luke_23_53"></a>[Luke 23:53](../../bible/luke/luke_23.md#luke_23_53)

Triple negative
[ou](../../strongs/g/g3756.md) [oudepō](../../strongs/g/g3764.md) [oudeis](../../strongs/g/g3762.md)
