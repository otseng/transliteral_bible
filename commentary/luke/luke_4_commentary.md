# Luke 4

<a name="luke_4_4"></a>[Luke 4:4](../../bible/luke/luke_4.md#luke_4_4)

MT/TR: Jesus answered him, “It is written, ‘Man does not live by bread alone. But by every word of God.’”

CT: Jesus answered him, saying, “It is written, ‘Man does not live by bread alone.’”
