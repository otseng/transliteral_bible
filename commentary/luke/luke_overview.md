# Luke Overview

## Title

* Gospel of Luke

## Author

* Written anonymously

* Traditionally attributed to [Luke the Evangelist](https://en.wikipedia.org/wiki/Luke_the_Evangelist), Greek physician and companion of Paul, Syrian from Antioch

* Only Gentile author of the New Testament

## Collection

* Gospels

## Dating

* 80 – 90 AD

## Genre

* Gospel (One of the three synoptic gospels)

## Audience

* Addressed to Theophilus

* Gentile audience

## Themes

* Jesus as the Son of Man, rejected by Israel, offered to the Gentiles

* Jesus as servant

## Statistics

* 24 chapters, 1151 verses, 25,944 words

## Videos

### David Pawson

* [Unlocking the New Testament Luke](https://www.youtube.com/watch?v=miPvZcX811M&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=4&pp=iAQB)

### Bible Project

* [Luke](https://www.youtube.com/watch?v=XIb_dCIxzr0&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=8)

### Randall Smith

* [Luke](https://www.youtube.com/watch?v=p7MB2QEPY4s&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=31&pp=iAQB)

## Timelines

* [Luke](https://simple.uniquebibleapp.com/book/Timelines/by_Luke)
* [Gospels](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John%3Eby_All_4_Gospels)
* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Longest of the gospels

* Part 1 of 2 volume work - Luke-Acts.  Luke-Acts comprise over 25% of the New Testament.

* Many [words found only in Luke/Acts](words_found_only_in_luke_acts.md)

* [Jesus Film](https://www.jesusfilm.org/) is based on Luke

## Links

[Bible.org](https://bible.org/seriespage/3-luke-introduction-outline-and-argument)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-luke/)

[John MacArthur](https://www.blueletterbible.org/commentaries/macarthur_john/)

[Bible Study Daily](http://biblestudydaily.org/study-helps/nt-book-introductions/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Luke&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-gospels/luke)

[Wikipedia](https://en.wikipedia.org/wiki/Gospel_of_Luke)

[Precept Austin](https://www.preceptaustin.org/luke_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-luke/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/luke/luke.htm)

[Bible Hub](https://biblehub.com/sum/luke)
