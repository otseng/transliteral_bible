
# Ecclesiastes Overview

## Title

* Hebrew: Kohelet (Speaker)
* Greek: Ἐκκλησιαστής (Ekklēsiastēs)
* Latin: Ecclesiastes

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* [Wisdom](https://en.wikipedia.org/wiki/Poetic_Books)

## Author

* Solomon

## Dating

* 930 BC

## Genre

* Poetry

## Literary form

* Wisdom

## Audience

* Israel

## Themes

* All is vanity

## Statistics

* 12 chapters, 222 verses, 5,584 words.

## Videos

### David Pawson

* [Ecclesiastes](https://www.youtube.com/watch?v=mExfn1kAxLE&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=36&t=4s)

### Bible Project

* [Ecclesiastes](https://www.youtube.com/watch?v=lrsQ1tc-2wk&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=37)

### Randall Smith

* [Ecclesiastes](https://www.youtube.com/watch?v=N03i8_f4UAw&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=13)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Notable passages

* Ecc 3:1 - For everything there is a season
* Ecc 3:11 - he has put eternity into man's heart
* Ecc 3:20 - All are from dust, and to dust all return

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/ecclesiastes/ecclesiastes-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/ecclesiastes-intro.cfm)

[Bible.org](https://bible.org/seriespage/1-introduction-ecclesiastes)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-eccclesiastes/)

[USCCB](https://bible.usccb.org/bible/ecclesiastes/0)

[Charles Swindoll](https://insight.org/resources/bible/the-wisdom-books/ecclesiastes)

[Wikipedia](https://en.wikipedia.org/wiki/Ecclesiastes)

[Precept Austin](https://www.preceptaustin.org/ecclesiastes_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-ecclesiastes/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/ecclesiastes/ecclesiastes.htm)

[Bible Hub](https://biblehub.com/sum/ecclesiastes)
