# Colossians Overview

## Title

* Epistle to the Colossians

## Author

* Paul.  Written in Rome in prison.

## Dating

* Around 62 AD

## Genre

* Epistle

## Literary form

* Letter.  Some poetry/hymn (Col 1:15-20)

## Audience

* Church as Colosse

## Statistics

* 4 chapters, 95 verses, 1998 words.

## Videos

### David Pawson

* [Colossians](https://www.youtube.com/watch?v=2XCX__pCeOI&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=18)

### Bible Project

* [Colossians](https://www.youtube.com/watch?v=pXTXlDxQsvc&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=18&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/colossians/colossians-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/colossians-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-colossians)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-colossians/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-colossians/)

[USCCB](https://bible.usccb.org/bible/colossians/0)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/colossians)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Colossians)

[Precept Austin](https://www.preceptaustin.org/colossians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-colossians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/colossians/colossians.htm)

[Bible Hub](https://biblehub.com/sum/colossians)
