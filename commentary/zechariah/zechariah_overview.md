# Zechariah Overview

## Title

* Hebrew: Zecharya (God remembers)
* Greek: Ζαχαρίας ΙΑʹ (XI. Zacharias)
* Latin: Zachariæ

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)

## Author

* Zechariah

## Dating

* 520 B.C.-487 B.C.
* Contemporary with Haggai

## Genre

* Prophecy

## Literary form

* Apocalyptic Poetry

## Audience

* Israel

## Themes

* Rebuilding the temple
* Coming of Messiah

## Statistics

* 14 chapters, 211 verses, 6444 words.

## Videos

### David Pawson

* [Zechariah 1/2](https://www.youtube.com/watch?v=QjWIXNOXOrg&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=57&pp=iAQB)
* [Zechariah 2/2](https://www.youtube.com/watch?v=MVfylfuesZU&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=58&pp=iAQB)

### Bible Project

* [Zechariah](https://www.youtube.com/watch?v=_106IfO6Kc0&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=29&pp=iAQB)

## Timelines

* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

## Notable passages

* Zechariah 4:6 - Not by might nor by power, but by My Spirit,' says the Lord Almighty.
* Zechariah 7:9-10 - This is what the Lord Almighty said: ‘Administer true justice; show mercy and compassion to one another. Do not oppress the widow or the fatherless, the foreigner or the poor. Do not plot evil against each other.’
* Zechariah 9:9 - Rejoice greatly, O daughter of Zion; shout, O daughter of Jerusalem: behold, thy King cometh unto thee: he [is] just, and having salvation; lowly, and riding upon an ass, and upon a colt the foal of an ass.
* Zechariah 12:10 - And I will pour out on the house of David and the inhabitants of Jerusalem a spirit of grace and supplication. They will look on Me, the One they have pierced, and they will mourn for Him as one mourns for an only child, and grieve bitterly for Him as one grieves for a firstborn son.

## Notes

* The most messianic, apocalyptic, and eschatological in the OT.
* Zechariah is second only to Isaiah in the number of messianic passages it contains.
* Chapters 1-8 are alluded to extensively in the book of the Revelation, while chapters 9-14 are alluded to often in the Gospels.
* Longest of the minor prophets

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/zechariah/zechariah-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/zechariah-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-zechariah)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-zechariah/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Zechariah&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-minor-prophets/zechariah)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Zechariah)

[Precept Austin](https://www.preceptaustin.org/zechariah_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-zechariah/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/zechariah/zechariah.htm)

[Bible Hub](https://biblehub.com/sum/zechariah/)