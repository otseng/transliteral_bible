# James Overview

## Title

* Epistle of James

## Other names

* "Proverbs of the New Testament"

## Author

* Jesus' brother James

## Dating

* 40 - 50 AD

## Genre

* General Epistle

## Literary form

* Has opening of a letter, but does not have benediction or closing.
* Wisdom literature

## Audience

* Twelve tribes in the dispersion

## Themes

* Christians must live out their faith. They should be doers, not just hearers, of God’s Word.

## Statistics

* 5 chapters, 108 verses, 2309 words

## Videos

### David Pawson

* [Unlocking the New Testament James 1/2](https://www.youtube.com/watch?v=hQjB5nttZLw&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=25&pp=iAQB)
* [Unlocking the New Testament James 2/2](https://www.youtube.com/watch?v=AcMqdZ564rk&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=26&pp=iAQB)

### Bruce Gore

* [James series](https://www.youtube.com/watch?v=NiTbJ4Y3StE&list=PLYFBLkHop2amQ7gYZn2Ablv78bclySQ8H&pp=iAQB)

### Bible Project

* [James](https://www.youtube.com/watch?v=qn-hLHWwRYY&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=26&pp=iAQB)

### Randall Smith

* [James](https://www.youtube.com/watch?v=Kyo-uyYsGKk&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=46&pp=iAQB)

### ThirdMill

* [James](https://www.youtube.com/watch?v=JO7ZnbF6pcA)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* “there are more parallels in this Epistle than in any other New Testament book to the teaching of our Lord in the Gospels"

* "Greek grammarians generally recognize James‘ Greek as among the most refined in the New Testament"

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_James)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/james/james-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/james-intro.cfm)

[Bible.org](https://bible.org/seriespage/20-james-introduction-outline-and-argument)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-james/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-james/)

[USCCB](https://bible.usccb.org/bible/james/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-james)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/james)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_of_James)

[Precept Austin](https://www.preceptaustin.org/james_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-james/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/james/james.htm)

[Bible Hub](https://biblehub.com/sum/james)
