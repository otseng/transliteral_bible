# Daniel Overview

## Title

* Hebrew: Dani'el (God is judge)
* Greek: Δανιήλ (Daniel)
* Latin: Danielis

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)
* Categorized as apocolyptic writing in Hebrew Bible, Christian Bible as major prophet

## Author

* Daniel

## Dating

* ~ 530 BC

## Genre

* Narrative

## Literary form

* Apoclyptic Prose, Prophecy, History

## Audience

* Israelites (Hebrew) and Gentiles (Aramaic)

## Themes

* Rise and fall of kingdoms

## Statistics

* 12 chapters, 357 verses, 11,606 words.

## Videos

### David Pawson

* [Daniel 1/2](https://www.youtube.com/watch?v=Rln-zNby09U&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=45)
* [Daniel 2/2](https://www.youtube.com/watch?v=jMHt0qjS6OI&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=47)

### Bible Project

* [Daniel](https://www.youtube.com/watch?v=9cSC9uobtPM&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=39)

### Randall Smith

* [Daniel](https://www.youtube.com/watch?v=s74qFY6Edas)

## Timelines

* [690-570 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Nahum,%20Zephaniah,%20Jeremiah,%20Lamentations,%20Ezekiel,%20Habakkuk%3E690-570_BCE)
* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

## Notable passages

* Daniel 3:17-18: "If we are thrown into the blazing furnace, the God we serve is able to deliver us from it, and he will deliver us from Your Majesty’s hand. But even if he does not, we want you to know, Your Majesty, that we will not serve your gods or worship the image of gold you have set up."
* Daniel 7:13-14 I saw in the night visions, and, behold, [one] like the Son of man came with the clouds of heaven, and came to the Ancient of days, and they brought him near before him. And there was given him dominion, and glory, and a kingdom, that all people, nations, and languages, should serve him: his dominion [is] an everlasting dominion, which shall not pass away, and his kingdom [that] which shall not be destroyed.

## Notes

* What Revelation is to the NT prophetically and apocalyptically, Daniel is to the OT. 
* Daniel 1:1-2:3 is Hebrew,  Daniel 2:4-7:28 is Aramaic, and Daniel 8:1-12:13 is Hebrew.
* Daniel lived under 3 rulers: Nebuchadnezzar (Babylon), Darius (Median), Cyrus (Persian)
* Only Old Testament book translated מָשִׁיחַ as Messiah (9:25, 26) instead of anointed.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/daniel/daniel-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/daniel-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-daniel)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-daniel/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Daniel&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-major-prophets/daniel)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Daniel)

[Precept Austin](https://www.preceptaustin.org/daniel_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-daniel/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/daniel/daniel.htm)

[Bible Hub](https://biblehub.com/sum/daniel/)