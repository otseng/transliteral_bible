
# 1 Samuel Overview

## Title

* Hebrew: Shmuel (His name is God)
* Greek: 1 Basileiōn (Kings 1)
* Latin: 1 Samuelis, 1 Regum

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Samuel (up to 1 Sam 25)
* Others (after 1 Sam 25)

## Dating

* Around 1000 BC

## Genre

* History

## Literary form

* Historical Narrative

## Audience

* Israelites

## Themes

* Establishment of monarchy
* Samuel, Saul, David

## Statistics

* 31 chapters, 810 verses, 25,061 words.

## Videos

### David Pawson

* [Samuel 1/2](https://www.youtube.com/watch?v=V-gozmcy3PM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=21)

* [Samuel 2/2](https://www.youtube.com/watch?v=ULLioZwvdEU&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=22)

### Bible Project

* [1 Samuel](https://www.youtube.com/watch?v=QJOju5Dw0V0&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=11&pp=iAQB)

### Randall Smith

* [Samuel](https://www.youtube.com/watch?v=jlG3CyvzJqQ&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=9&pp=iAQB)

## Timeline

* [1170-1050 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel%3E1170-1050_BCE)
* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Notable passages

* 1 Samuel 15:22 - And Samuel said, "Has the LORD as great delight in burnt offerings and sacrifices, as in obeying the voice of the LORD? Behold, to obey is better than sacrifice, and to listen than the fat of rams.

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1samuel/1samuel-2samuel-overview.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/1samuel-intro.cfm)

[Bible.org](https://bible.org/seriespage/1-introduction-1-samuel)

[ESV](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_1_Samuel)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-1-samuel/)

[USCCB](https://bible.usccb.org/bible/1samuel/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/first-samuel)

[Wikipedia](https://en.wikipedia.org/wiki/Books_of_Samuel)

[Precept Austin](https://www.preceptaustin.org/1_samuel_commentaries)

[Bible Project](https://bibleproject.com/explore/video/1-samuel/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1samuel/1samuel.htm)

[Bible Hub](https://biblehub.com/sum/1_samuel)
