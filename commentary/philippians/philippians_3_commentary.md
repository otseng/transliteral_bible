# Philippians 3

<a name=philippians_3_16></a>[Philippians 3:16](../../bible/philippians/philippians_3.md#philippians_3_16)

MT/TR: Nevertheless, to the degree that we have already attained, let us walk by the same rule, let us be of the same mind.

CT: Nevertheless, to the degree that we have already attained, let us walk by the same.
