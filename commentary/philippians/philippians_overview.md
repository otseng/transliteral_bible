# Philippians Overview

## Title

* Epistle to the Philippians

## Author

* Paul. Generally accepted by scholars.

## Dating

* A.D. 54 to 62. Prison epistle.

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Church of Philippi.  First place in Europe Paul established a church.

## Themes

* Imitation of Jesus through humble service
* The hope for the resurrection life
* God’s gift of peace in difficulty

## Statistics

* 4 chapters, 104 verses, 2002 words.

## Videos

### David Pawson

* [Philippians](https://www.youtube.com/watch?v=OEMr3lNzMDs&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=16)

### Bible Project

* [Philippians](https://www.youtube.com/watch?v=oE9qqW1-BkU&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=17)

### Randall Smith

* [Philippians](https://www.youtube.com/watch?v=3zvSdr4bo60&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=39&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Key verses

* He who began a good work in you will perfect it until the day of Christ Jesus (1:6) 
* To live is Christ and to die is gain (1:21)
* I can do all things through Him who strengthens me (4:13) 

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/philippians/philippians-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/philippians-intro.cfm)

[Bible.org](https://bible.org/seriespage/philippians-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-philippians/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-philippians/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Philippians&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/philippians)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Philippians)

[Precept Austin](https://www.preceptaustin.org/philippians_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-philippians/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/philippians/philippians.htm)

[Bible Hub](https://biblehub.com/sum/philippians)
