# Titus Overview

## Title

* Epistle to Titus

## Author

* Paul

## Dating

* Mid 60s

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Titus

## Themes

* Link between faith and practice, belief and behavior

## Statistics

* 3 chapters, 46 verses, 921 words

## Videos

### David Pawson

* [Titus](https://www.youtube.com/watch?v=iljyisQNcVM&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=22&pp=iAQB)

### Bible Project

* [Titus](https://www.youtube.com/watch?v=PUEYCVXJM3k&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=23&pp=iAQB)

### Randall Smith

* [Titus](https://www.youtube.com/watch?v=dtfH8wl0Ls8&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=43&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Titus)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/titus/titus-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/titus-intro.cfm)

[Bible.org](https://bible.org/seriespage/17-titus-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-titus/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-titus/)

[USCCB](https://bible.usccb.org/bible/titus/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-titus/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/titus)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_Titus)

[Precept Austin](https://www.preceptaustin.org/titus_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-titus/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/titus/titus.htm)

[Bible Hub](https://biblehub.com/sum/titus)
