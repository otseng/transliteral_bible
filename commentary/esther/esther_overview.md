# Esther Overview

## Title

* Hebrew: Ester (Star)
* Greek: Ἐσθήρ (Esther)
* Latin: Esther

## Other names

* The scroll (Megillah)

## Collection

* [Ketuvim](https://en.wikipedia.org/wiki/Ketuvim)

## Author

* Unknown

## Dating

*  After King Xerxes, king of Persia 486-465 BC

## Genre

* History

## Literary form

* Narrative

## Statistics

* 10 chapters, 167 verses, 5637 words.

## Movies

* [Vision Video - Esther](https://www.youtube.com/watch?v=ccTHkycbYXo)

## Videos

### David Pawson

* [Esther](https://www.youtube.com/watch?v=_skPXjv4XZY&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=27)

### Bible Project

* [Esther](https://www.youtube.com/watch?v=JydNSlufRIs&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=38)

## Timelines

* [570-450 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CObadiah,%202%20Chronicles,%202%20Kings,%20Daniel,%20Ezra,%20Esther,%20Haggai,%20Zechariah%3E570-450_BCE)

## Notable passages

* Esther 4:14 For if thou altogether holdest thy peace at this time, [then] shall there enlargement and deliverance arise to the Jews from another place; but thou and thy father's house shall be destroyed: and who knoweth whether thou art come to the kingdom for [such] a time as this?

## Notes

* One of two books named for a woman (other is Ruth).
* One of two books that does not contain references to God (other is Song of Solomon)
* Read in [festival of Purim](https://en.wikipedia.org/wiki/Purim)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/esther/esther-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/esther-intro.cfm)

[Bible.org](https://bible.org/seriespage/introduction-esther)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-esther/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Esther&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/esther)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Esther)

[Precept Austin](https://www.preceptaustin.org/esther_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-esther/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/esther/esther.htm)

[Bible Hub](https://biblehub.com/sum/esther)
