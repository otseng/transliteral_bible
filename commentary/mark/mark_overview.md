# Mark Overview

## Title

* Gospel of Mark

## Author

* Written anonymously

* Traditionally attributed to John Mark, companion of both Peter and Paul, cousin of Barnabas

## Collection

* Gospels

## Dating

* 66 – 70 AD

## Genre

* Gospel (One of the three synoptic gospels)

## Audience

* Roman believers, Gentile audience

## Themes

* Ministry of Jesus

## Statistics

* 16 chapters, 678 verses, 15,171 words

## Videos

### David Pawson

* [Unlocking the New Testament Mark](https://www.youtube.com/watch?v=XJVVDqQx3w0&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=3&pp=iAQB)

### Bible Project

* [Mark](https://www.youtube.com/watch?v=HGHqu9-DtXk&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=4&pp=iAQB)

### Randall Smith

* [Mark](https://www.youtube.com/watch?v=h4Ys4cmvZac&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=30&pp=iAQB)

## Timelines

* [Mark](https://simple.uniquebibleapp.com/book/Timelines/by_Mark)
* [Gospels](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John%3Eby_All_4_Gospels)
* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)
## Notes

* Scholars consider Mark to be the source text of Matthew and Luke

* Shortest of all the gospels

* Portrays Jesus as constantly on the move

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/mark.cfm)

[Bible.org](https://bible.org/seriespage/2-mark-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-mark/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/mark-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-mark/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Mark&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-mark/)

[Charles Swindoll](https://insight.org/resources/bible/the-gospels/mark)

[Wikipedia](https://en.wikipedia.org/wiki/Gospel_of_Mark)

[Precept Austin](https://www.preceptaustin.org/mark_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-mark/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/mark/mark.htm)

[Bible Hub](https://biblehub.com/sum/mark)
