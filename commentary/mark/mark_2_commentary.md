# Mark 2

<a name="mark_2_16"></a>[Mark 2:16](../../bible/mark/mark_2.md#mark_2_16)

MT/TR: “Why is He eating and drinking with tax collectors and sinners?” (see Lk 5:30)

CT: “Why is He eating with tax collectors and sinners?”

<a name="mark_2_26"></a>[Mark 2:26](../../bible/mark/mark_2.md#mark_2_26)

"A very famous passage of which Bart Ehrman says in what caused him to reject the Bible as an errant and fallible text." Bill Mounce

Abiathar was not the high priest, but Ahimelech was according to 1 Sam 21:1.

<a href="https://carm.org/bible-difficulties/was-abiathar-or-ahimelech-the-high-priest-mark-226-and-1-samuel-211/">CARM</a>

<a href="https://amateurexegete.com/2018/02/04/musings-on-mark-who-was-the-high-priest/">Amateur Exegete</a>
