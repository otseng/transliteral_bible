# Mark 9

<a name="mark_9_38"></a>[Mark 9:38](../../bible/mark/mark_9.md#mark_9_38)

MT/TR: And John answered him, saying, "Teacher, we saw someone, who does not follow us, driving out demons in/by your name and we hindered him, because he does not follow us."

CT: John said to him, "Teacher, we saw someone driving out demons in your name and we tried to hinder him, because he was not following us."

<a name="mark_9_43"></a>[Mark 9:43](../../bible/mark/mark_9.md#mark_9_43)

MT/TR: If your hand causes you to sin, cut it off. It is better for you to enter into life maimed, rather than having two hands, to go to hell, into the fire that shall never be quenched—‘Their worm does not die And the fire is not quenched.’ And if your foot causes you to sin, cut it off. It is better for you to enter life lame, rather than having two feet, to be cast into hell, into the fire that shall never be quenched—where ‘Their worm does not die And the fire is not quenched.’

CT: If your hand causes you to sin, cut it off. It is better for you to enter into life maimed, rather than having two hands, to go to hell, into the fire that shall never be quenched. And if your foot causes you to sin, cut it off. It is better for you to enter life lame, rather than having two feet, to be cast into hell.

<a name="mark_9_49"></a>[Mark 9:49](../../bible/mark/mark_9.md#mark_9_49)

MT/TR: For everyone will be seasoned with fire, and every sacrifice will be seasoned with salt.

CT: For everyone will be seasoned with fire.
