# Mark 6

<a name="mark_6_11"></a>[Mark 6:11](../../bible/mark/mark_6.md#mark_6_11)

MT/TR: And whoever will not receive you nor hear you, when you depart from there, shake off the dust under your feet as a testimony against them. Assuredly, I say to you, it will be more tolerable for Sodom and Gomorrah in the day of judgment than for that city!

CT: And whoever will not receive you nor hear you, when you depart from there, shake off the dust under your feet as a testimony against them.
