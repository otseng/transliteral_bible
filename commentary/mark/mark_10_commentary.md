# Mark 10

<a name="mark_10_24"></a>[Mark 10:24](../../bible/mark/mark_10.md#mark_10_24)

MT/TR: And the disciples were astonished at His words. But Jesus answered again and said to them, “Children, how hard it is for those who trust in riches to enter the kingdom of God!

CT: And the disciples were astonished at His words. But Jesus answered again and said to them, “Children, how hard it is to enter the kingdom of God!
