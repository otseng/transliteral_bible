# Mark 7

<a name="mark_7_8"></a>[Mark 7:8](../../bible/mark/mark_7.md#mark_7_8)

MT/TR: For laying aside the commandment of God, you hold the tradition of men—the washing of pitchers and cups, and many other such things you do.

CT: For laying aside the commandment of God, you hold the tradition of men.

<a name="mark_7_16"></a>[Mark 7:16](../../bible/mark/mark_7.md#mark_7_16)

MT/TR: If anyone has ears to hear, let him hear!

CT: Verse omitted
