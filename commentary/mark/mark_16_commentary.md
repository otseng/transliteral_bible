# Mark 16

<a name="mark_16_9"></a>[Mark 16:9](../../bible/mark/mark_16.md#mark_16_9)

Verses 9-20 omitted in the critical text (long ending of Mark).

[Wikipedia](https://en.wikipedia.org/wiki/Mark_16)

[Got Questions](https://www.gotquestions.org/Mark-16-9-20.html)

[A Case for the Longer Ending of Mark](https://textandcanon.org/a-case-for-the-longer-ending-of-mark/)

[A Case against the Longer Ending of Mark](https://textandcanon.org/a-case-against-the-longer-ending-of-mark/)

[Early Church History](https://earlychurchhistory.org/beliefs-2/long-or-short-ending-in-mark/)
