# 2 Timothy Overview

## Title

* Second Epistle to Timothy

## Author

* Traditionally attributed to Paul

* Scholars consider a pseudepigraphical work and disputes Paul authorship

## Dating

* Last epistle written by Paul

* Traditionally dated 64 - 67 AD

* Scholars date 90 - 140 AD

## Genre

* Pastoral epistle

## Audience

* Letter to Timothy, leader of the church at Ephesus

## Statistics

* 4 chapters, 83 verses, 1703 words

## Videos

### David Pawson

* [2 Timothy](https://www.youtube.com/watch?v=iljyisQNcVM&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=22&pp=iAQB)

### Bible Project

* [2 Timothy](https://www.youtube.com/watch?v=urlvnxCaL00&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=22&pp=iAQB)

### Randall Smith

* [2 Timothy](https://www.youtube.com/watch?v=5mdWGCil4-0&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=42&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Swan song of Paul

## Links

[Bible.org](https://bible.org/seriespage/16-2-timothy-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-2-timothy/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/2timothy-intro.cfm?a=1126001)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-2-timothy/)

[USCCB](http://www.usccb.org/bible/1timothy/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-2-timothy/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/second-timothy)

[Wikipedia](https://en.wikipedia.org/wiki/Second_Epistle_to_Timothy)

[Precept Austin](https://www.preceptaustin.org/2_timothy_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-2-timothy/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2timothy/2timothy.htm)

[Bible Hub](https://biblehub.com/sum/2_timothy)
