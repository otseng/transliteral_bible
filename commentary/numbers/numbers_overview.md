# Numbers Overview

## Title

* Hebrew: Bemidbar, "In the Wilderness"
* Greek: Ἀριθμοί (Arithmoi)
* Latin: Numeri

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Pentateuch](https://en.wikipedia.org/wiki/Torah)

## Author

* Written anonymously
* Traditionally attributed to Moses

## Dating

* Between 1450 and 1406 B.C.

## Audience

* The Israelites

## Genre

* Law

## Literary form

* Historical Narrative

## Themes

*  Israel’s journey from Mount Sinai to the borders of the Promised Land

## Statistics

* 36 chapters, 1288 verses, 32,902 or 32,092 words.

## Videos

### David Pawson

* [Unlocking the Old Testament Part 12 - Numbers 1](https://www.youtube.com/watch?v=e3jOEfaSV1A&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=12)

* [Unlocking the Old Testament Part 13 - Numbers 2](https://www.youtube.com/watch?v=0L-qZWuTbR8&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=13)

### Bible Project

* [Numbers](https://www.youtube.com/watch?v=tp5MIrMZFqo&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=7&pp=iAQB)

### Randall Smith

* [Numbers](https://www.youtube.com/watch?v=uJiC-Gv9t50&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=4&pp=iAQB)

## Timelines

* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)

## Notable passages

* Priestly blessing - [Numbers 6:24](https://www.blueletterbible.org/kjv/num/6/24/s_123024)

## Notes

* Account from the first month of the second year, and brings it down to the eleventh month of the fortieth year

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Numbers)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/numbers/numbers-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/numbers-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-numbers)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-numbers/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-numbers/)

[USCCB](https://bible.usccb.org/bible/numbers/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-numbers/)

[Charles Swindoll](https://insight.org/resources/bible/the-pentateuch/numbers)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Numbers)

[Precept Austin](https://www.preceptaustin.org/numbers_commentaries)

[New Advent](https://www.newadvent.org/cathen/11646c.htm)

[Bible Project](https://bibleproject.com/guides/book-of-numbers/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/numbers/numbers.htm)

[Bible Hub](https://biblehub.com/sum/numbers)
