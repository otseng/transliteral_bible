# Numbers 6

<a name="numbers_6_24"></a>[Numbers 6:24](../../bible/numbers/numbers_6.md#numbers_6_24)

Priestly Blessing, Priestly Benediction, Birkat HaKohanim, Aaronic Blessing

Transliteration:

Yivarechecha Adonai viyishmirecha
Ya'er Adonai panav elecha veechuneka
Yeesa Adonai panav elecha viyasem lecha shalom

Hebrew:

יְבָרֶכְךָ יהוה, וְיִשְׁמְרֶךָ
יָאֵר יהוה פָּנָיו אֵלֶיךָ, וִיחֻנֶּךָּ
יִשָּׂא יהוה פָּנָיו אֵלֶיךָ, וְיָשֵׂם לְךָ שָׁלוֹם

"This is the oldest known Biblical text that has been found; amulets with these verses written on them have been found in graves at Ketef Hinnom, dating from the First Temple Period."

Links:

[Wikipedia](https://en.wikipedia.org/wiki/Priestly_Blessing)
[Olive Tree](https://www.olivetree.com/blog/significance-priestly-blessing/)
[Hebrew 4 Christians](https://www.hebrew4christians.com/Blessings/Synagogue_Blessings/Priestly_Blessing/priestly_blessing.html)
[My Jewish Learning](https://www.myjewishlearning.com/article/the-priestly-blessing/)
[Karaites](https://www.karaites.org/the-birkat-hakohanim-the-priestly-benediction.html)
[Chabad](https://www.chabad.org/library/article_cdo/aid/894583/jewish/The-Priestly-Blessing.htm)
[Reform Judaism](https://reformjudaism.org/learning/torah-study/naso/priestly-blessing)

Videos:

[BimBam](https://www.youtube.com/watch?v=T3tXSX_ltDI&feature=emb_logo)
[New Life ICF](https://www.youtube.com/watch?v=x-eHCxMM3PI)
[Aaronic Blessing in Hebrew](https://www.youtube.com/watch?v=FgwTyjF_0IA)