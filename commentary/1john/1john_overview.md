# 1 John Overview

## Title

* First Epistle of John

## Author

* Epistle does not identify the author

## Dating

* No later than the 90s

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Churches across Asia Minor

## Statistics

* 5 chapters, 105 verses, 2523 words

## Videos

### David Pawson

* [1 John](https://www.youtube.com/watch?v=5jPlZjNwZhM&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=30&pp=iAQB)

### Bible Project

* [1-3 John](https://www.youtube.com/watch?v=l3QkE6nKylM&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=29&pp=iAQB)

### Randall Smith

* [1 John](https://www.youtube.com/watch?v=cuO4KOFHezc&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=49&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_1_John)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1john/1john-outline.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/1john-intro.cfm)

[Bible.org](https://bible.org/seriespage/1-john-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-1-3-john/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-1-john/)

[USCCB](https://bible.usccb.org/bible/1john/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-1-john/)

[Charles Swindoll](https://insight.org/resources/bible/the-general-epistles/first-john)

[Wikipedia](https://en.wikipedia.org/wiki/First_Epistle_of_John)

[Precept Austin](https://www.preceptaustin.org/1_john_commentaries)

[Bible Project](https://bibleproject.com/guides/books-of-1-3-john/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/1john/1john.htm)

[Bible Hub](https://biblehub.com/sum/1_john)
