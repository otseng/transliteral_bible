# 1 John 5

<a name=1john_5_7></a>[1 John 5:7](../../bible/1john/1john_5.md#1john_5_7)

[Johannine Comma](https://en.wikipedia.org/wiki/Johannine_Comma)

TR: For there are three that bear witness in heaven: the Father, the Word, and the Holy Spirit; and these three are one. And there are three that bear witness on earth: the Spirit, the water, and the blood; and these three agree as one.

MT/CT: For there are three that bear witness: the Spirit, the water, and the blood; and these three agree as one.

<a name=1john_5_13></a>[1 John 5:13](../../bible/1john/1john_5.md#1john_5_13)

MT/TR: These things I have written to you who believe in the name of the Son of God, that you may know that you have eternal life, and that you may continue to believe in the name of the Son of God.

CT: These things I have written to you who believe in the name of the Son of God, that you may know that you have eternal life.
