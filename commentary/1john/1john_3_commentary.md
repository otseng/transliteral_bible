# 1 John 3

<a name=1john_3_1></a>[1 John 3:1](../../bible/1john/1john_3.md#1john_3_1)

MT/TR: Behold what manner of love the Father has bestowed on us, that we should be called children of God! Therefore the world does not know us, because it did not know Him.

CT: Behold what manner of love the Father has bestowed on us, that we should be called children of God! And we are. Therefore the world does not know us, because it did not know Him.
