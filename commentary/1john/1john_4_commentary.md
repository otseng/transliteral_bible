# 1 John 4

<a name=1john_4_3></a>[1 John 4:3](../../bible/1john/1john_4.md#1john_4_3)

MT/TR: and every spirit that does not confess that Jesus Christ has come in the flesh is not of God. And this is the spirit of the Antichrist, which you have heard was coming, and is now already in the world.

CT: and every spirit that does not confess Jesus is not of God. And this is the spirit of the Antichrist, which you have heard was coming, and is now already in the world.
