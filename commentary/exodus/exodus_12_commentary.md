# Exodus 12

<a name="exodus_12_38"></a>[Exodus 12:38](../../bible/exodus/exodus_12.md#exodus_12_38)

### Erev Rav

[Wikipedia - Erev Rav](https://en.wikipedia.org/wiki/Erev_Rav)

[TheTorah](https://www.thetorah.com/article/erev-rav-a-mixed-multitude-of-meanings)

[Torah.org](https://torah.org/torah-portion/perceptions-5781-bo/)
