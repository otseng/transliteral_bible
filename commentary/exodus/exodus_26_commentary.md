# Exodus 26

<a name="exodus_26_3"></a>[Exodus 26:3](../../bible/exodus/exodus_26.md#exodus_26_3)

joined "woman" to "sister"

KJV1963
The five curtains shall be coupled together, each to its sister. And five curtains shall be coupled together, each to its sister.
