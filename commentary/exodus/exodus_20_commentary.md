# Exodus 20

<a name="exodus_20_2"></a>[Exodus 20:2](../../bible/exodus/exodus_20.md#exodus_20_2)

[Wikipedia](https://en.wikipedia.org/wiki/I_am_the_Lord_thy_God)

<a name="exodus_20_3"></a>[Exodus 20:3](../../bible/exodus/exodus_20.md#exodus_20_3)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_have_no_other_gods_before_me)

<a name="exodus_20_6"></a>[Exodus 20:6](../../bible/exodus/exodus_20.md#exodus_20_6)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_make_unto_thee_any_graven_image)

<a name="exodus_20_7"></a>[Exodus 20:7](../../bible/exodus/exodus_20.md#exodus_20_7)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_take_the_name_of_the_Lord_thy_God_in_vain)

<a name="exodus_20_11"></a>[Exodus 20:11](../../bible/exodus/exodus_20.md#exodus_20_11)

[Wikipedia](https://en.wikipedia.org/wiki/Remember_the_sabbath_day,_to_keep_it_holy)

<a name="exodus_20_12"></a>[Exodus 20:12](../../bible/exodus/exodus_20.md#exodus_20_12)

[Wikipedia](https://en.wikipedia.org/wiki/Honour_thy_father_and_thy_mother)

<a name="exodus_20_13"></a>[Exodus 20:13](../../bible/exodus/exodus_20.md#exodus_20_13)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_kill)

<a name="exodus_20_14"></a>[Exodus 20:14](../../bible/exodus/exodus_20.md#exodus_20_14)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_commit_adultery)

<a name="exodus_20_15"></a>[Exodus 20:15](../../bible/exodus/exodus_20.md#exodus_20_15)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_steal)

<a name="exodus_20_16"></a>[Exodus 20:16](../../bible/exodus/exodus_20.md#exodus_20_16)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_bear_false_witness_against_thy_neighbour)

<a name="exodus_20_17"></a>[Exodus 20:17](../../bible/exodus/exodus_20.md#exodus_20_17)

[Wikipedia](https://en.wikipedia.org/wiki/Thou_shalt_not_covet)
