# Exodus Overview

## Title

* Hebrew: Shemot (Names)
* Greek: Ἔξοδος (Exodos)
* Latin: Exodus

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Pentateuch](https://en.wikipedia.org/wiki/Torah)

## Author

* Written anonymously
* Traditionally attributed to Moses

## Dating

* Between 1450 and 1406 B.C.

## Audience

* The Israelites

## Genre

* Narrative History with some Law

## Literary form

* Historical Narrative

## Themes

* Deliverance from Egypt
* Fulfillment of God’s promises to the patriarchs

## Statistics

* 40 chapters, 1213 verses, 32,692 words

## Videos

### David Pawson

* [Unlocking the Old Testament Part 8 - Exodus 1](https://www.youtube.com/watch?v=S5cEpDlOP9A&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=8&pp=iAQB)
* [Unlocking the Old Testament Part 9 - Exodus 2](https://www.youtube.com/watch?v=jiNKP3GlPi4&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=9&pp=iAQB)

### Bible Project

* [Exodus](https://www.youtube.com/watch?v=oNpTha80yyE&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=5&pp=iAQB)

### Bruce Gore

* [Exodus series](https://www.youtube.com/watch?v=qJXy7OOJMsQ&list=PLYFBLkHop2akHMcr-TJzd0dbfys2xqDdr&pp=iAQB)

### Randall Smith

* [Exodus](https://www.youtube.com/watch?v=7UfrSwM8trA&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=2&pp=iAQB)

## Timelines

* [1970-1850 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1970-1850_BCE)
* [1850-1730 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1850-1730_BCE)
* [1750-1630 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1750-1630_BCE)
* [1630-1510 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CGenesis,%20Exodus%3E1630-1510_BCE)
* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)

## Notable passages

* Exodus 20:2-3 I [am] the LORD thy God, which have brought thee out of the land of Egypt, out of the house of bondage. Thou shalt have no other gods before me.

## Notes

* 40 years thinking he was somebody. 40 years learning he was nobody. 40 years discovering what God can do with a nobody.

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Exodus)

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/exodus/exodus-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/exodus-intro.cfm)

[Bible.org](https://bible.org/article/introduction-book-exodus)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-exodus/)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-exodus/)

[USCCB](https://bible.usccb.org/bible/exodus/0)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-exodus/)

[Charles Swindoll](https://insight.org/resources/bible/the-pentateuch/exodus)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Exodus)

[Precept Austin](https://www.preceptaustin.org/exodus_commentaries)

[New Advent](https://www.newadvent.org/cathen/11646c.htm)

[Bible Project](https://bibleproject.com/guides/book-of-exodus/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/exodus/exodus.htm)

[Bible Hub](https://biblehub.com/sum/exodus)
