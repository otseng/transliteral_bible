# Exodus 34

<a name="exodus_34_19"></a>[Exodus 34:19](../../bible/exodus/exodus_34.md#exodus_34_19)

Error with male.  Male in KJVx is H2142, which is not male, but remember (zakar).  Male is H2145, [zāḵār](../../strongs/h/h2145.md).  

Blue Letter Bible has the phrase in italics:
https://www.blueletterbible.org/kjv/exo/34/19/t_conc_84019
