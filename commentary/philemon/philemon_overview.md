# Philemon Overview

## Title

* Epistle to Philemon

## Author

* Paul, undisputed among scholars

## Dating

* A.D. 55 to 63

## Genre

* Epistle

## Literary form

* Letter

## Audience

* Philemon, his wife Apphia, his son Archippus, and the members of his household.

## Statistics

* 1 chapter, 25 verses, 445 words.

## Videos

### David Pawson

* [Philemon](https://www.youtube.com/watch?v=uj6VvtIQBIU&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=17)

### Bible Project

* [Philemon](https://www.youtube.com/watch?v=aW9Q3Jt6Yvk&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=24&pp=iAQB)

### Randall Smith

* [Philemon](https://www.youtube.com/watch?v=CXvnOnlL-Lc&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=44&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

# Notes

* Shortest of Paul's letters

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/philemon/philemon-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/philemon-intro.cfm)

[Bible.org](https://bible.org/seriespage/philemon-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-philemon/)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-philemon/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Philemon&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/philemon)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_Philemon)

[Precept Austin](https://www.preceptaustin.org/philemon_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-philemon/)
