# Romans 16

<a name="romans_16_24"></a>[Romans 16:24](../../bible/romans/romans_16.md#romans_16_24)

MT/TR: The grace of our Lord Jesus Christ be with you all. Amen.

CT: Verse omitted

<a name="romans_16_25"></a>[Romans 16:25](../../bible/romans/romans_16.md#romans_16_25)

MT: Verses are placed after Rom 14:23

CT/TR: Verses are placed here
