# Romans 14

<a name="romans_14_6"></a>[Romans 14:6](../../bible/romans/romans_14.md#romans_14_6)

MT/TR: He who observes the day, observes it to the Lord; and he who does not observe the day, to the Lord he does not observe it. He who eats, eats to the Lord, for he gives God thanks; and he who does not eat, to the Lord he does not eat, and gives God thanks.

CT: He who observes the day, observes it to the Lord. He who eats, eats to the Lord, for he gives God thanks; and he who does not eat, to the Lord he does not eat, and gives God thanks.
