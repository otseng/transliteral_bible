# Romans 10

<a name="romans_10_15"></a>[Romans 10:15](../../bible/romans/romans_10.md#romans_10_15)

MT/TR: And how shall they preach unless they are sent? As it is written: “How beautiful are the feet of those who preach the gospel of peace, Who bring glad tidings of good things!”

CT: And how shall they preach unless they are sent? As it is written: “How beautiful are the feet of those who bring glad tidings of good things!”
