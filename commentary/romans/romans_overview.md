# Romans Overview

## Title

* Epistle to the Romans

## Collection

* Epistles

## Author

* [Paul](https://en.wikipedia.org/wiki/Paul_the_Apostle)

* Undisputed by scholars

## Dating

* 55 - 58 AD

## Genre

* Epistle

## Audience

* Christians in Rome

## Themes

* God’s plan of salvation and righteousness for all humankind

## Statistics

* 16 chapters, 433 verses, 9447 words.

## Videos

### David Pawson

* [Unlocking the New Testament Romans 1/2](https://www.youtube.com/watch?v=OpXQbCnMWgA&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=10)
* [Unlocking the New Testament Romans 2/2](https://www.youtube.com/watch?v=TiCXzATXLmo&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=11)

### Bruce Gore

* [Romans series](https://www.youtube.com/watch?v=mSK2eLF8obc&list=PLYFBLkHop2al4zSOvt6qvPRM_jvjG-awR&pp=iAQB)

### Bible Project

* [Romans](https://www.youtube.com/watch?v=ej_6dVdJSIU&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=11&pp=iAQB)

### Randall Smith

* [Romans](https://www.youtube.com/watch?v=wqkwoNfJ8Qk&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=34&pp=iAQB)

## Timelines

* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)

## Notes

* Longest of Paul's epistles

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/romans.cfm)

[Bible.org](https://bible.org/seriespage/6-romans-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-romans/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/romans-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-romans/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Romans&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-romans/)

[Charles Swindoll](https://insight.org/resources/bible/the-pauline-epistles/romans)

[Wikipedia](https://en.wikipedia.org/wiki/Epistle_to_the_Romans)

[Precept Austin](https://www.preceptaustin.org/romans_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-romans/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/romans/romans.htm)

[Bible Hub](https://biblehub.com/sum/romans)
