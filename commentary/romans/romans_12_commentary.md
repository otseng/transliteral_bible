# Romans 12

<a name="romans_12_11"></a>[Romans 12:11](../../bible/romans/romans_12.md#romans_12_11)

Textual variant: [kairos](../../strongs/g/g2540.md) should be [kyrios](../../strongs/g/g2962.md). In MGNT, uses "κυρίῳ".  In TR, uses "καιρῷ".
