# Romans 11

<a name="romans_11_6"></a>[Romans 11:6](../../bible/romans/romans_11.md#romans_11_6)

MT/TR: And if by grace, then it is no longer of works; otherwise grace is no longer grace. But if it is of works, it is no longer grace; otherwise work is no longer work.

CT: And if by grace, then it is no longer of works; otherwise grace is no longer grace.
