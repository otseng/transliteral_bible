# Romans 8

<a name="romans_8_1"></a>[Romans 8:1](../../bible/romans/romans_8.md#romans_8_1)

MT/TR: There is therefore now no condemnation to those who are in Christ Jesus, who do not walk according to the flesh, but according to the Spirit.

CT: There is therefore now no condemnation to those who are in Christ Jesus.
