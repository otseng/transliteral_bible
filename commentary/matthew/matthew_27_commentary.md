# Matthew 27

<a name="matthew_27_4"></a>[Matthew 27:4](../../bible/matthew/matthew_27.md#matthew_27_4)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_27:4)

<a name="matthew_27_9"></a>[Matthew 27:9](../../bible/matthew/matthew_27.md#matthew_27_9)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_27:9)

<a name="matthew_27_16"></a>[Matthew 27:16](../../bible/matthew/matthew_27.md#matthew_27_16)

MT/TR: At that time they had a notorious prisoner, called Barabbas.

CT: At that time they had a notorious prisoner, called [Jesus] Barabbas.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_27:16)

<a name="matthew_27_35"></a>[Matthew 27:35](../../bible/matthew/matthew_27.md#matthew_27_35)

TR: Then they crucified Him, and divided His garments, casting lots, that it might be fulfilled which was spoken by the prophet: “They divided My garments among them, And for My clothing they cast lots.”

MT/CT: Then they crucified Him, and divided His garments, casting lots.

<a name="matthew_27_49"></a>[Matthew 27:49](../../bible/matthew/matthew_27.md#matthew_27_49)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_27:49)
