# Matthew 20

<a name="matthew_20_16"></a>[Matthew 20:16](../../bible/matthew/matthew_20.md#matthew_20_16)

MT/TR: So the last will be first, and the first last. For many are called, but few chosen.

CT: So the last will be first, and the first last.

<a name="matthew_20_22"></a>[Matthew 20:22](../../bible/matthew/matthew_20.md#matthew_20_22)

MT/TR: But Jesus answered and said, “You do not know what you ask. Are you able to drink the cup that I am about to drink, and be baptized with the baptism that I am baptized with?” They said to Him, “We are able.” So He said to them, “You will indeed drink My cup, and be baptized with the baptism that I am baptized with; but to sit on My right hand and on My left is not Mine to give, but it is for those for whom it is prepared by My Father.

CT: But Jesus answered and said, “You do not know what you ask. Are you able to drink the cup that I am about to drink?” They said to Him, “We are able.” So He said to them, “You will indeed drink My cup; but to sit on My right hand and on My left is not Mine to give, but it is for those for whom it is prepared by My Father.

<a name="matthew_20_28"></a>[Matthew 20:28](../../bible/matthew/matthew_20.md#matthew_20_28)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_20:28)
