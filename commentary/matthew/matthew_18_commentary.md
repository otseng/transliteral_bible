# Matthew 18

<a name="matthew_18_11"></a>[Matthew 18:11](../../bible/matthew/matthew_18.md#matthew_18_11)

MT/TR: For the Son of Man has come to save that which was lost.

CT: Verse omitted

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_18:11)
