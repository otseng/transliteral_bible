# Matthew 26

<a name="matthew_26_28"></a>[Matthew 26:28](../../bible/matthew/matthew_26.md#matthew_26_28)

MT/TR: This is my blood of the new covenant, which is poured out for many for the forgiveness of sins.

CT: This is my blood of the covenant, which is poured out for many for the forgiveness of sins.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_26:28)
