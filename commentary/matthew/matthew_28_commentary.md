# Matthew 28

<a name="matthew_28_17"></a>[Matthew 28:17](../../bible/matthew/matthew_28.md#matthew_28_17)

> The word "some" does not have a Greek word behind it.  Almost all Bible translations have "some" doubted, only the NABRE does not have "some".  
> 
> New American Bible (Revised Edition) (NABRE)
> "When they saw him, they worshiped, but they doubted."
> https://www.biblegateway.com/passage/?search=matt+28%3A17&version=NABRE
> 
> [Dr. Larry Perkins](http://moments.nbseminary.com/archives/62-worshipping-but-uncertain-matthew-2817/)
>
> [Tim Chaffey](http://midwestapologetics.org/blog/?p=1014)

<a name="matthew_28_19"></a>[Matthew 28:19](../../bible/matthew/matthew_28.md#matthew_28_19)

> [Wikipedia](https://en.wikipedia.org/wiki/Matthew_28:19)
>
> [Berean Patriot](http://www.bereanpatriot.com/the-great-commission-in-matthew-2819-is-not-what-youve-been-taught/)
