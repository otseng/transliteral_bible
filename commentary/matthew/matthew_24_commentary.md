# Matthew 24

<a name="matthew_24_36"></a>[Matthew 24:36](../../bible/matthew/matthew_24.md#matthew_24_36)

MT/TR: But of that day and hour no one knows, not even the angels of heaven, but My Father only.
CT: But of that day and hour no one knows, not even the angels of heaven, nor the Son, but the Father only.
