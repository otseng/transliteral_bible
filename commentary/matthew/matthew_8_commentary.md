# Matthew 8

<a name="matthew_8_3"></a>[Matthew 8:3](../../bible/matthew/matthew_8.md#matthew_8_3)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:3)

<a name="matthew_8_5"></a>[Matthew 8:5](../../bible/matthew/matthew_8.md#matthew_8_5)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:5)

<a name="matthew_8_6"></a>[Matthew 8:6](../../bible/matthew/matthew_8.md#matthew_8_6)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:6)

<a name="matthew_8_7"></a>[Matthew 8:7](../../bible/matthew/matthew_8.md#matthew_8_7)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:7)

<a name="matthew_8_8"></a>[Matthew 8:8](../../bible/matthew/matthew_8.md#matthew_8_8)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:8)

<a name="matthew_8_9"></a>[Matthew 8:9](../../bible/matthew/matthew_8.md#matthew_8_9)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:9)

<a name="matthew_8_10"></a>[Matthew 8:10](../../bible/matthew/matthew_8.md#matthew_8_10)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:10)

<a name="matthew_8_11"></a>[Matthew 8:11](../../bible/matthew/matthew_8.md#matthew_8_11)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:11)

<a name="matthew_8_12"></a>[Matthew 8:12](../../bible/matthew/matthew_8.md#matthew_8_12)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:12)

<a name="matthew_8_13"></a>[Matthew 8:13](../../bible/matthew/matthew_8.md#matthew_8_13)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:13)

<a name="matthew_8_15"></a>[Matthew 8:15](../../bible/matthew/matthew_8.md#matthew_8_15)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:15)

<a name="matthew_8_18"></a>[Matthew 8:18](../../bible/matthew/matthew_8.md#matthew_8_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:18)

<a name="matthew_8_21"></a>[Matthew 8:21](../../bible/matthew/matthew_8.md#matthew_8_21)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:21)

<a name="matthew_8_22"></a>[Matthew 8:22](../../bible/matthew/matthew_8.md#matthew_8_22)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:22)

<a name="matthew_8_25"></a>[Matthew 8:25](../../bible/matthew/matthew_8.md#matthew_8_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:25)

<a name="matthew_8_26"></a>[Matthew 8:26](../../bible/matthew/matthew_8.md#matthew_8_26)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:26)

<a name="matthew_8_28"></a>[Matthew 8:28](../../bible/matthew/matthew_8.md#matthew_8_28)

MT/TR: And when he arrived at the other side in the region of the Gergesenes, two demon-possessed men coming from the tombs met him. They were so violent that no one could pass that way.

CT: And when he arrived at the other side in the region of the Gadarenes, two demon-possessed men coming from the tombs met him. They were so violent that no one could pass that way.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:28)

<a name="matthew_8_29"></a>[Matthew 8:29](../../bible/matthew/matthew_8.md#matthew_8_29)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:29)

<a name="matthew_8_30"></a>[Matthew 8:30](../../bible/matthew/matthew_8.md#matthew_8_30)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:30)

<a name="matthew_8_31"></a>[Matthew 8:31](../../bible/matthew/matthew_8.md#matthew_8_31)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:31)

<a name="matthew_8_32"></a>[Matthew 8:32](../../bible/matthew/matthew_8.md#matthew_8_32)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_8:32)
