# Matthew 9

<a name="matthew_9_1"></a>[Matthew 9:1](../../bible/matthew/matthew_9.md#matthew_9_1)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:1)

<a name="matthew_9_2"></a>[Matthew 9:2](../../bible/matthew/matthew_9.md#matthew_9_2)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:2)

<a name="matthew_9_4"></a>[Matthew 9:4](../../bible/matthew/matthew_9.md#matthew_9_4)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:4)

<a name="matthew_9_8"></a>[Matthew 9:8](../../bible/matthew/matthew_9.md#matthew_9_8)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:8)

<a name="matthew_9_9"></a>[Matthew 9:9](../../bible/matthew/matthew_9.md#matthew_9_9)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:9)

<a name="matthew_9_11"></a>[Matthew 9:11](../../bible/matthew/matthew_9.md#matthew_9_11)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:11)

<a name="matthew_9_12"></a>[Matthew 9:12](../../bible/matthew/matthew_9.md#matthew_9_12)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:12)

<a name="matthew_9_13"></a>[Matthew 9:13](../../bible/matthew/matthew_9.md#matthew_9_13)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:13)

<a name="matthew_9_14"></a>[Matthew 9:14](../../bible/matthew/matthew_9.md#matthew_9_14)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:14)

<a name="matthew_9_15"></a>[Matthew 9:15](../../bible/matthew/matthew_9.md#matthew_9_15)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:15)

<a name="matthew_9_18"></a>[Matthew 9:18](../../bible/matthew/matthew_9.md#matthew_9_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:18)

<a name="matthew_9_19"></a>[Matthew 9:19](../../bible/matthew/matthew_9.md#matthew_9_19)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:19)

<a name="matthew_9_21"></a>[Matthew 9:21](../../bible/matthew/matthew_9.md#matthew_9_21)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:21)

<a name="matthew_9_22"></a>[Matthew 9:22](../../bible/matthew/matthew_9.md#matthew_9_22)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:22)

<a name="matthew_9_24"></a>[Matthew 9:24](../../bible/matthew/matthew_9.md#matthew_9_24)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:24)

<a name="matthew_9_25"></a>[Matthew 9:25](../../bible/matthew/matthew_9.md#matthew_9_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:25)

<a name="matthew_9_26"></a>[Matthew 9:26](../../bible/matthew/matthew_9.md#matthew_9_26)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:26)

<a name="matthew_9_28"></a>[Matthew 9:28](../../bible/matthew/matthew_9.md#matthew_9_28)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:28)

<a name="matthew_9_32"></a>[Matthew 9:32](../../bible/matthew/matthew_9.md#matthew_9_32)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:32)

<a name="matthew_9_34"></a>[Matthew 9:34](../../bible/matthew/matthew_9.md#matthew_9_34)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:34)

<a name="matthew_9_35"></a>[Matthew 9:35](../../bible/matthew/matthew_9.md#matthew_9_35)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:35)

<a name="matthew_9_36"></a>[Matthew 9:36](../../bible/matthew/matthew_9.md#matthew_9_36)

TR: When he saw the crowds, he had compassion on them, because they were weary and cast out, like sheep without a shepherd.

MT/CT: When he saw the crowds, he had compassion on them, because they were mistreated and cast out, like sheep without a shepherd.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_9:36)
