# Matthew 23

<a name="matthew_23_14"></a>[Matthew 23:14](../../bible/matthew/matthew_23.md#matthew_23_14)

MT/TR: Woe to you, scribes and Pharisees, hypocrites! For you devour widows’ houses, and for a pretense make long prayers. Therefore you will receive greater condemnation.

CT: Verse omitted

<a name="matthew_23_26"></a>[Matthew 23:26](../../bible/matthew/matthew_23.md#matthew_23_26)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_23:26)

<a name="matthew_23_38"></a>[Matthew 23:38](../../bible/matthew/matthew_23.md#matthew_23_38)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_23:38)
