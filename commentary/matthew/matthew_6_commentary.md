# Matthew 6

<a name="matthew_6_1"></a>[Matthew 6:1](../../bible/matthew/matthew_6.md#matthew_6_1)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:1)

<a name="matthew_6_4"></a>[Matthew 6:4](../../bible/matthew/matthew_6.md#matthew_6_4)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:4)

<a name="matthew_6_5"></a>[Matthew 6:5](../../bible/matthew/matthew_6.md#matthew_6_5)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:5)

<a name="matthew_6_6"></a>[Matthew 6:6](../../bible/matthew/matthew_6.md#matthew_6_6)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:6)

<a name="matthew_6_7"></a>[Matthew 6:7](../../bible/matthew/matthew_6.md#matthew_6_7)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:7)

<a name="matthew_6_8"></a>[Matthew 6:8](../../bible/matthew/matthew_6.md#matthew_6_8)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:8)

<a name="matthew_6_9"></a>[Matthew 6:9](../../bible/matthew/matthew_6.md#matthew_6_9)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:9)

<a name="matthew_6_11"></a>[Matthew 6:11](../../bible/matthew/matthew_6.md#matthew_6_11)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:11)

<a name="matthew_6_12"></a>[Matthew 6:12](../../bible/matthew/matthew_6.md#matthew_6_12)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:12)

<a name="matthew_6_13"></a>[Matthew 6:13](../../bible/matthew/matthew_6.md#matthew_6_13)

MT/TR: And do not lead us into temptation, But deliver us from the evil one. For Yours is the kingdom and the power and the glory forever. Amen.

CT: And do not lead us into temptation, But deliver us from the evil one.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:13)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:13)

<a name="matthew_6_14"></a>[Matthew 6:14](../../bible/matthew/matthew_6.md#matthew_6_14)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:14)

<a name="matthew_6_15"></a>[Matthew 6:15](../../bible/matthew/matthew_6.md#matthew_6_15)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:15)

<a name="matthew_6_18"></a>[Matthew 6:18](../../bible/matthew/matthew_6.md#matthew_6_18)

TR: so that it will not be obvious to men that you are fasting, but only to your Father, who is unseen; and your Father, who sees what is done in secret, will reward you openly.

MT/CT: so that it will not be obvious to men that you are fasting, but only to your Father, who is unseen; and your Father, who sees what is done in secret, will reward you.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:18)

<a name="matthew_6_20"></a>[Matthew 6:20](../../bible/matthew/matthew_6.md#matthew_6_20)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:20)

<a name="matthew_6_24"></a>[Matthew 6:24](../../bible/matthew/matthew_6.md#matthew_6_24)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:24)

<a name="matthew_6_25"></a>[Matthew 6:25](../../bible/matthew/matthew_6.md#matthew_6_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:25)

<a name="matthew_6_27"></a>[Matthew 6:27](../../bible/matthew/matthew_6.md#matthew_6_27)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:27)

<a name="matthew_6_28"></a>[Matthew 6:28](../../bible/matthew/matthew_6.md#matthew_6_28)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:28)

<a name="matthew_6_33"></a>[Matthew 6:33](../../bible/matthew/matthew_6.md#matthew_6_33)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_6:33)
