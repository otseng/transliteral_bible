# Matthew Overview

## Title

* Gospel of Matthew

## Author

* Written anonymously

* Traditionally attributed to [Matthew the Apostle](https://en.wikipedia.org/wiki/Matthew_the_Apostle), referred to as Levi in the other gospels

## Collection

* Gospels

## Dating

* Scholars date between 70 - 110 AD

## Genre

* Gospel (One of the three synoptic gospels)

## Audience

* Predominantly Jewish

## Themes

* Jesus as King and Messiah of the people of God

* "the kingdom of heaven" occurs 32 times (and nowhere else in all of Scripture)

* Conflict between Christ and the religious leaders

## Statistics

* 28 chapters, 1071 verses, 23,684 words

## Movies

* [Gospel according to Matthew](https://www.youtube.com/watch?v=yf9jWonGL74)

## Videos

### David Pawson

* [Matthew 1/2](https://www.youtube.com/watch?v=F2KMYwBRngU&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=1&pp=iAQB)
* [Matthew 2/2](https://www.youtube.com/watch?v=DmY9AHaOMEI&list=PLfk5IZQHKovK_7RzISco4nVgMvt0bqFUZ&index=3)

### Bible Project

* [Matthew](https://www.youtube.com/watch?v=3Dv4-n6OYGI&list=PLH0Szn1yYNecanpQqdixWAm3zHdhY2kPR&index=2&pp=iAQB)

### Randall Smith

* [Matthew](https://www.youtube.com/watch?v=kLoQu1AhIAE&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=29&pp=iAQB)

## Timelines

* [Matthew](https://simple.uniquebibleapp.com/book/Timelines/by_Matthew)
* [Gospels](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John%3Eby_All_4_Gospels)
* [240-120 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E240-120_BCE)
* [120-1 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E120-1_BCE)
* [10-110 AD](https://simple.uniquebibleapp.com/book/Timelines/%3CMatthew,%20Mark,%20Luke,%20John,%20Acts,%20Romans,%201%20Corinthians,%202%20Corinthians,%20Galatians,%20Ephesians,%20Philippians,%20Colossians,%201%20Thessalonians,%202%20Thessalonians,%201%20Timothy,%202%20Timothy,%20Titus,%20Philemon,%20Hebrews,%20James,%201%20Peter,%202%20Peter,%201%20John,%202%20John,%203%20John,%20Jude,%20Revelation%3E10-110_CE)
## Notable passages

* Matthew 22:37 - Jesus said unto him, Thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy mind.

* Matthew 28:18 - And Jesus came and spake unto them, saying, All power is given unto me in heaven and in earth.

## Notes

* Historically considered the most important gospel.  The most quoted by early church fathers.

* Possibly first written in Hebrew or Aramaic and later translated into Greek.  Papias, "Matthew has written these words (Greek logia) in the Hebrew language, but everybody translated them as best they could."

## Links

[Blue Letter Bible](https://www.blueletterbible.org/study/intros/matthew.cfm)

[Bible.org](https://bible.org/seriespage/1-matthew-introduction-argument-and-outline)

[ESV](https://www.esv.org/resources/esv-global-study-bible/introduction-to-matthew/)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/matthew-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-matthew/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Matthew&ch=)

[Charles Swindoll](https://insight.org/resources/bible/the-gospels/matthew)

[Wikipedia](https://en.wikipedia.org/wiki/Gospel_of_Matthew)

[Precept Austin](https://www.preceptaustin.org/matthew_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-matthew/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/matthew/matthew.htm)

[Bible Hub](https://biblehub.com/sum/matthew)
