# Matthew 5

<a name="matthew_5_4"></a>[Matthew 5:4](../../bible/matthew/matthew_5.md#matthew_5_4)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:4)

<a name="matthew_5_11"></a>[Matthew 5:11](../../bible/matthew/matthew_5.md#matthew_5_11)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:11)

<a name="matthew_5_12"></a>[Matthew 5:12](../../bible/matthew/matthew_5.md#matthew_5_12)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:12)

<a name="matthew_5_18"></a>[Matthew 5:18](../../bible/matthew/matthew_5.md#matthew_5_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:18)

<a name="matthew_5_19"></a>[Matthew 5:19](../../bible/matthew/matthew_5.md#matthew_5_19)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:19)

<a name="matthew_5_20"></a>[Matthew 5:20](../../bible/matthew/matthew_5.md#matthew_5_20)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:20)

<a name="matthew_5_22"></a>[Matthew 5:22](../../bible/matthew/matthew_5.md#matthew_5_22)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:22)

<a name="matthew_5_25"></a>[Matthew 5:25](../../bible/matthew/matthew_5.md#matthew_5_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:25)

<a name="matthew_5_27"></a>[Matthew 5:27](../../bible/matthew/matthew_5.md#matthew_5_27)

TR: "You have heard that it was said by those of old, 'Do not commit adultery.'

MT/CT: "You have heard that it was said, 'Do not commit adultery.'

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:27)

<a name="matthew_5_29"></a>[Matthew 5:29](../../bible/matthew/matthew_5.md#matthew_5_29)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:29)

<a name="matthew_5_30"></a>[Matthew 5:30](../../bible/matthew/matthew_5.md#matthew_5_30)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:30)

<a name="matthew_5_32"></a>[Matthew 5:32](../../bible/matthew/matthew_5.md#matthew_5_32)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:32)

<a name="matthew_5_33"></a>[Matthew 5:33](../../bible/matthew/matthew_5.md#matthew_5_33)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:33)

<a name="matthew_5_39"></a>[Matthew 5:39](../../bible/matthew/matthew_5.md#matthew_5_39)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:39)

<a name="matthew_5_44"></a>[Matthew 5:44](../../bible/matthew/matthew_5.md#matthew_5_44)

MT/TR: But I say to you, love your enemies, bless those who curse you, do good to those who hate you, and pray for those who spitefully use you and persecute you.

CT: But I say to you, love your enemies, and pray for those who persecute you.

<a name="matthew_5_44"></a>[Matthew 5:44](../../bible/matthew/matthew_5.md#matthew_5_44)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:44)

<a name="matthew_5_47"></a>[Matthew 5:47](../../bible/matthew/matthew_5.md#matthew_5_47)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_5:47)
