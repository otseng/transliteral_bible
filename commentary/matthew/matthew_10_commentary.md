# Matthew 10

<a name="matthew_10_1"></a>[Matthew 10:1](../../bible/matthew/matthew_10.md#matthew_10_1)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:1)

<a name="matthew_10_8"></a>[Matthew 10:8](../../bible/matthew/matthew_10.md#matthew_10_8)

TR/CT: Heal the sick, cleanse those who have leprosy <> raise the dead, drive out demons. Freely you have received, freely give.

MT: Heal the sick, cleanse those who have leprosy, drive out demons. Freely you have received, freely give.

<a name="matthew_10_13"></a>[Matthew 10:13](../../bible/matthew/matthew_10.md#matthew_10_13)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:13)

<a name="matthew_10_14"></a>[Matthew 10:14](../../bible/matthew/matthew_10.md#matthew_10_14)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:14)

<a name="matthew_10_16"></a>[Matthew 10:16](../../bible/matthew/matthew_10.md#matthew_10_16)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:16)

<a name="matthew_10_18"></a>[Matthew 10:18](../../bible/matthew/matthew_10.md#matthew_10_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:18)

<a name="matthew_10_19"></a>[Matthew 10:19](../../bible/matthew/matthew_10.md#matthew_10_19)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:19)

<a name="matthew_10_23"></a>[Matthew 10:23](../../bible/matthew/matthew_10.md#matthew_10_23)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:23)

<a name="matthew_10_25"></a>[Matthew 10:25](../../bible/matthew/matthew_10.md#matthew_10_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:25)

<a name="matthew_10_35"></a>[Matthew 10:35](../../bible/matthew/matthew_10.md#matthew_10_35)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:35)

<a name="matthew_10_37"></a>[Matthew 10:37](../../bible/matthew/matthew_10.md#matthew_10_37)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:37)

<a name="matthew_10_38"></a>[Matthew 10:38](../../bible/matthew/matthew_10.md#matthew_10_38)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:38)

<a name="matthew_10_39"></a>[Matthew 10:39](../../bible/matthew/matthew_10.md#matthew_10_39)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:39)

<a name="matthew_10_41"></a>[Matthew 10:41](../../bible/matthew/matthew_10.md#matthew_10_41)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:41)

<a name="matthew_10_42"></a>[Matthew 10:42](../../bible/matthew/matthew_10.md#matthew_10_42)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_10:42)
