# Matthew 4

<a name="matthew_4_2"></a>[Matthew 4:2](../../bible/matthew/matthew_4.md#matthew_4_2)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:2)

<a name="matthew_4_4"></a>[Matthew 4:4](../../bible/matthew/matthew_4.md#matthew_4_4)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:4)

<a name="matthew_4_6"></a>[Matthew 4:6](../../bible/matthew/matthew_4.md#matthew_4_6)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:6)

<a name="matthew_4_10"></a>[Matthew 4:10](../../bible/matthew/matthew_4.md#matthew_4_10)

CT/TR: Then Jesus said to him, “Away with you, Satan! For it is written, ‘You shall worship the Lord your God, and Him only you shall serve.’”

MT: Then Jesus said to him, “Get behind me, Satan! For it is written, ‘You shall worship the Lord your God, and Him only you shall serve.’”

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:10)

<a name="matthew_4_12"></a>[Matthew 4:12](../../bible/matthew/matthew_4.md#matthew_4_12)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:12)

<a name="matthew_4_17"></a>[Matthew 4:17](../../bible/matthew/matthew_4.md#matthew_4_17)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:17)

<a name="matthew_4_18"></a>[Matthew 4:18](../../bible/matthew/matthew_4.md#matthew_4_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:18)

<a name="matthew_4_21"></a>[Matthew 4:21](../../bible/matthew/matthew_4.md#matthew_4_21)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:21)

<a name="matthew_4_23"></a>[Matthew 4:23](../../bible/matthew/matthew_4.md#matthew_4_23)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:23)

<a name="matthew_4_24"></a>[Matthew 4:24](../../bible/matthew/matthew_4.md#matthew_4_24)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_4:24)
