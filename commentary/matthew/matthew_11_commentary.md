# Matthew 11

<a name="matthew_11_1"></a>[Matthew 11:1](../../bible/matthew/matthew_11.md#matthew_11_1)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:1)

<a name="matthew_11_2"></a>[Matthew 11:2](../../bible/matthew/matthew_11.md#matthew_11_2)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:2)

<a name="matthew_11_3"></a>[Matthew 11:3](../../bible/matthew/matthew_11.md#matthew_11_3)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:3)

<a name="matthew_11_5"></a>[Matthew 11:5](../../bible/matthew/matthew_11.md#matthew_11_5)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:5)

<a name="matthew_11_7"></a>[Matthew 11:7](../../bible/matthew/matthew_11.md#matthew_11_7)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:7)

<a name="matthew_11_8"></a>[Matthew 11:8](../../bible/matthew/matthew_11.md#matthew_11_8)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:8)

<a name="matthew_11_9"></a>[Matthew 11:9](../../bible/matthew/matthew_11.md#matthew_11_9)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:9)

<a name="matthew_11_13"></a>[Matthew 11:13](../../bible/matthew/matthew_11.md#matthew_11_13)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:13)

<a name="matthew_11_15"></a>[Matthew 11:15](../../bible/matthew/matthew_11.md#matthew_11_15)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:15)

<a name="matthew_11_16"></a>[Matthew 11:16](../../bible/matthew/matthew_11.md#matthew_11_16)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:16)

<a name="matthew_11_17"></a>[Matthew 11:17](../../bible/matthew/matthew_11.md#matthew_11_17)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:17)

<a name="matthew_11_18"></a>[Matthew 11:18](../../bible/matthew/matthew_11.md#matthew_11_18)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:18)

<a name="matthew_11_19"></a>[Matthew 11:19](../../bible/matthew/matthew_11.md#matthew_11_19)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:19)

<a name="matthew_11_20"></a>[Matthew 11:20](../../bible/matthew/matthew_11.md#matthew_11_20)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:20)

<a name="matthew_11_21"></a>[Matthew 11:21](../../bible/matthew/matthew_11.md#matthew_11_21)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:21)

<a name="matthew_11_23"></a>[Matthew 11:23](../../bible/matthew/matthew_11.md#matthew_11_23)

MT/TR: And you, Capernaum, which is lifted up to heaven, shall go down to hades. If the miracles that were performed (mid.) in you had been performed in Sodom, it would have remained to this day.

CT: And you, Capernaum, will (you) not be lifted up to heaven(?) (No) you will go down to hades. If the miracles that were performed (pass.) in you had been performed in Sodom, it would have remained to this day.

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:23)

<a name="matthew_11_25"></a>[Matthew 11:25](../../bible/matthew/matthew_11.md#matthew_11_25)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:25)

<a name="matthew_11_27"></a>[Matthew 11:27](../../bible/matthew/matthew_11.md#matthew_11_27)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:27)

<a name="matthew_11_29"></a>[Matthew 11:29](../../bible/matthew/matthew_11.md#matthew_11_29)

[Wikipedia](https://en.wikipedia.org/wiki/Matthew_11:29)
