# Deuteronomy 6

<a name="deuteronomy_6_4"></a>[Deuteronomy 6:4](../../bible/deuteronomy/deuteronomy_6.md#deuteronomy_6_4)

Shema

* https://en.wikipedia.org/wiki/Shema_Yisrael

Learning

* https://www.youtube.com/watch?v=LCRIPntvZzs
* https://www.youtube.com/watch?v=5oCri6q7wJM

Songs

* https://www.youtube.com/watch?v=wxO09BUrkMk
* https://www.youtube.com/watch?v=W1X92zw60iQ
* https://www.youtube.com/watch?v=BUJ1qDGbelQ
s