# Deuteronomy Overview

## Title

* Hebrew: Devarim - "words"
* Greek: Δευτερονόμιον (Deuteronomion) deuteros + nomos, "second law", "copy of the law", "repetition of the law"
* Elleh haddebarim - "These are the words"
* Latin: Deuteronomium

## Collection

* [Torah](https://en.wikipedia.org/wiki/Torah)
* [Pentateuch](https://en.wikipedia.org/wiki/Torah)

## Author

* Moses

## Dating

* Tradition dating - around 1406 BC
* Scholarly dating - between the 7th and 5th centuries BC

## Genre

* Law

## Literary form

* Speeches, oration

## Audience

* Israel

## Themes

* The nature and character of God
* The covenant relationship between God and the people of Israel
* The people’s response in faith to God
* The consequences of sin

## Contents

* Recounts the history of the nation Israel’s 40 years of wandering through the desert and their arrival at the Promised Land
* A retelling by Moses of the teachings and events of Exodus, Leviticus, and Numbers
* An extended review of the Ten Commandments
* Blessings for faithfulness and curses for unfaithfulness
* Takes place entirely in one location over about one month of time
* Concentrates on events that took place in the final weeks of Moses’ life

## Videos

### David Pawson

* [Unlocking the Old Testament Part 14 - Deuteronomy 1](https://www.youtube.com/watch?v=B-VsUBdfgPE&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=14)

* [Unlocking the Old Testament Part 15 - Deuteronomy 2](https://www.youtube.com/watch?v=D1edhZfYIpY&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=15)

### Bible Project

* [Deuteronomy](https://www.youtube.com/watch?v=q5QEH9bH8AU&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=8&pp=iAQB)

### Randall Smith

* [Deuteronomy](https://www.youtube.com/watch?v=otF4Wr_zEzo&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=5&pp=iAQB)

## Timelines

* [1510-1390 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJob,%20Exodus,%20Leviticus,%20Numbers,%20Deuteronomy,%20Joshua%3E1510-1390_BCE)

## Notable passages

* Deu 6:4-5 Hear, O Israel: The LORD our God [is] one LORD: And thou shalt love the LORD thy God with all thine heart, and with all thy soul, and with all thy might.

## Notes

* Last book of the Pentateuch
* Quoted over 40 times in the NT (exceeded only by Psalms and Isaiah)

## Links

[Bible.org](https://bible.org/article/introduction-deuteronomy)

[ESV](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_Deuteronomy)

[J. Vernon McGee](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/deuteronomy/deuteronomy-notes.cfm)

[John MacArthur](https://www.blueletterbible.org/Comm/macarthur_john/bible-introductions/deuteronomy-intro.cfm)

[Bible Study Daily](http://biblestudydaily.org/introduction-to-deuteronomy/)

[USCCB](http://www.usccb.org/bible/scripture.cfm?bk=Deuteronomy&ch=)

[NIV Study Bible](https://www.biblica.com/resources/scholar-notes/niv-study-bible/intro-to-deuteronomy/)

[Charles Swindoll](https://insight.org/resources/bible/the-pentateuch/deuteronomy)

[Wikipedia](https://en.wikipedia.org/wiki/Book_of_Deuteronomy)

[Precept Austin](https://www.preceptaustin.org/acts_commentaries)

[Bible Project](https://bibleproject.com/guides/book-of-deuteronomy/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/deuteronomy/deuteronomy.htm)

[Bible Hub](https://biblehub.com/sum/deuteronomy)
