
# 2 Samuel Overview

## Title

* Hebrew: Shmuel (His name is God)
* Greek: 2 Basileiōn (Kings 2)
* Latin: 2 Samuelis, 2 Regum

## Collection

* [Nevi'im](https://en.wikipedia.org/wiki/Nevi%27im)
* [Historical](https://en.wikipedia.org/wiki/Historical_books)

## Author

* Unknown

## Dating

* Around 1000 BC

## Genre

* History

## Literary form

* Historical Narrative

## Audience

* Israelites

## Themes

* After Saul's reign

## Statistics

* 24 chapters, 695 verses, 20,612 words.

## Videos

### David Pawson

* [Samuel 1/2](https://www.youtube.com/watch?v=V-gozmcy3PM&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=21)

* [Samuel 2/2](https://www.youtube.com/watch?v=ULLioZwvdEU&list=PLfk5IZQHKovKhMVo9ekMLvZk8vFV9RJvC&index=22)

### Bible Project

* [2 Samuel](https://www.youtube.com/watch?v=YvoWDXNDJgs&list=PLH0Szn1yYNeeVFodkI9J_WEATHQCwRZ0u&index=12)

### Randall Smith

* [Samuel](https://www.youtube.com/watch?v=jlG3CyvzJqQ&list=PL7fPEPqhBL6kzcNHVK2q-6Ou9bitS2j2p&index=9&pp=iAQB)

## Timeline

* [1050-930 BC](https://simple.uniquebibleapp.com/book/Timelines/%3CJudges,%201%20Samuel,%202%20Samuel,%201%20Kings,%201%20Chronicles,%202%20Chronicles,%20Psalms,%20Proverbs,%20Ecclesiastes,%20Song%20of%20Songs%3E1050-930_BCE)

## Links

[J. Vernon McGee Notes](https://www.blueletterbible.org/comm/mcgee_j_vernon/notes-outlines/1samuel/1samuel-2samuel-overview.cfm)

[John MacArthur](https://www.blueletterbible.org/comm/macarthur_john/bible-introductions/2samuel-intro.cfm)

[Bible.org](https://bible.org/seriespage/1-introduction-2-samuel)

[ESV](https://www.blueletterbible.org/study/intros/esv_intros.cfm#at_2_Samuel)

[Bible Study Daily](https://biblestudydaily.org/introduction-to-2-samuel/)

[USCCB](https://bible.usccb.org/bible/2samuel/0)

[Charles Swindoll](https://insight.org/resources/bible/the-historical-books/second-samuel)

[Wikipedia](https://en.wikipedia.org/wiki/Books_of_Samuel)

[Precept Austin](https://www.preceptaustin.org/2_samuel_commentaries)

[Bible Project](https://bibleproject.com/explore/video/2-samuel/)

[Thomas Constable](https://soniclight.com/tcon/notes/html/2samuel/2samuel.htm)

[Bible Hub](https://biblehub.com/sum/2_samuel)
