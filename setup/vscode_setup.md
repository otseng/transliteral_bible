# VS Code Setup

## Markdown file

~/Library/Application\ Support/Code/User/snippets/markdown.json

## Keybinding Multicommand

Open settings file with "Open User settings (JSON)" in command palette and copy [settings.json](settings.json) into it.

Bind shortcut key to multicommand in:
Settings, Keyboard shortcuts, Show user keybindings

Open the keybindings.json file from the Command Palette (⇧⌘P) with "Preferences: Open Keyboard Shortcuts (JSON)"

``` json
    { "key": "cmd+k r", 
      "command": "multiCommand.redletter", "when": "editorTextFocus"
    },
    { "key": "cmd+k e", 
      "command": "multiCommand.openfile", "when": "editorTextFocus"
    },
    { "key": "cmd+k m", 
      "command": "multiCommand.markdown_snippet", "when": "editorTextFocus"
    },
```
