# Contributing to the Transliteral Bible

You can contribute by forking the Transliteral Bible and submitting a merge request to fix issues.

## Code

The Bible is hosted at [gitlab.com/otseng/transliteral_bible](https://gitlab.com/otseng/transliteral_bible)

## Base text

Use the KJV text (1769) as the base text.  

## Data file sources

The data files in the [Unique Bible App](https://github.com/eliranwong/UniqueBible) are used as the source for the KJV text and for the Strong's information.  The Strong's data is from the KJVx Bible.  You may also use the [Blue Letter Bible](https://www.blueletterbible.org) as a source.  If there is a discrepancy, the UBA data files will take priority.

## Issues

### Typos and formatting issues

Sometimes things are incorrectly typed.  Submit an MR to correct these.

### Missing transliterated words

Not all words are transliterated.  If you feel one should be transliterated, submit an MR and it will be reviewed to be added.

### Italicized words

Most italicized words are removed.  Submit an MR to remove words that are italicized.

### Grammar issues

Some word ordering has been changed to make it more understandable in English.  Some more might need to be changed.

Articles 'a' or 'an' have been corrected to match the transliterated word.  However, there could be cases where this was missed.

## Contributors list

All contributors that have their MR approved will be added to the [Contributors](CONTRIBUTORS.md).
