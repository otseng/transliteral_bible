# https://en.wikipedia.org/wiki/Isopsephy

import sys

if len(sys.argv) < 2:
    print("Please enter word to calculate")
    exit(1)

word = sys.argv[1]

numvalue = dict()
numvalue['Α'] = 1
numvalue['α'] = 1
numvalue['Β'] = 2 
numvalue['β'] = 2
numvalue['Γ'] = 3 
numvalue['γ'] = 3
numvalue['Δ'] = 4
numvalue['δ'] = 4
numvalue['Ε'] = 5
numvalue['ε'] = 5
numvalue['Ϝ'] = 6
numvalue['ϛ'] = 6
numvalue['Ζ'] = 7
numvalue['ζ'] = 7
numvalue['Η'] = 8
numvalue['η'] = 8
numvalue['Θ'] = 9
numvalue['θ'] = 9
numvalue['Ι'] = 10
numvalue['ι'] = 10
numvalue['Κ'] = 20
numvalue['κ'] = 20
numvalue['Λ'] = 30
numvalue['λ'] = 30
numvalue['Μ'] = 40
numvalue['μ'] = 40
numvalue['Ν'] = 50
numvalue['ν'] = 50
numvalue['Ξ'] = 60
numvalue['ξ'] = 60
numvalue['Ο'] = 70
numvalue['ο'] = 70
numvalue['Π'] = 80
numvalue['π'] = 80
numvalue['Ϙ'] = 90
numvalue['Ρ'] = 100
numvalue['ρ'] = 100
numvalue['Σ'] = 200
numvalue['σ'] = 200
numvalue['Τ'] = 300
numvalue['τ'] = 300
numvalue['Υ'] = 400
numvalue['υ'] = 400
numvalue['Φ'] = 500
numvalue['φ'] = 500
numvalue['Χ'] = 600
numvalue['χ'] = 600
numvalue['Ψ'] = 700
numvalue['ψ'] = 700
numvalue['Ω'] = 800
numvalue['ω'] = 800
numvalue['ϡ'] = 900

sum = 0

for c in word:
    if c in numvalue.keys():
        sum = sum + numvalue[c]

print('Sum: ' + str(sum))