import glob
import re
import sys

def find_lsj():
    filesearch = "../strongs/h/*.md"
    search_pattern = 'LSJ'
    for file in glob.glob(filesearch):
        filecontents = open(file).read()
        result = re.findall(r'' + search_pattern + '', filecontents)
        if result:
            print(file)

def find_definition_without_paren():
    filesearch = "../strongs/g/*.md"
    for file in glob.glob(filesearch):
        fp = open(file)
        line = fp.readline()
        while line:
            line = fp.readline()
            if (("Definition:" in line) and not ("(" in line)):
               print(file) 

find_definition_without_paren()