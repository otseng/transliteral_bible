import os, sqlite3, re, glob, platform
from pathlib import Path
import sys
from urllib.parse import quote

class TransformBible:

    def __init__(self):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'
        # self.bible = self.home + '/UniqueBible/marvelData/bibles/CUVx.bible'
        self.bible = self.home + '/UniqueBible/marvelData/bibles/CUVl.bible'
        self.connection = sqlite3.connect(self.bible)
        self.cursor = self.connection.cursor()

        self.lexicon = self.home + '/UniqueBible/marvelData/lexicons/CEDICT.lexicon'
        self.connectionLexicon = sqlite3.connect(self.lexicon)
        self.cursorLexicon = self.connectionLexicon.cursor()

        self.lexiconCache = {}

        self.START_BOOK = 1
        self.END_BOOK = 67  # 67
        self.MAX_VERSES = 10000000000

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def process_bible(self):
        count = 0
        for book in range(self.START_BOOK, self.END_BOOK):
            print("Processing " + str(book))
            query = "SELECT Book, Chapter, Scripture FROM Bible " + \
                "WHERE Book=" + str(book) + " ORDER BY Chapter"
            self.cursor.execute(query)
            lines = self.cursor.fetchall()
            for line in lines:
                chapter = line[1]
                verse = line[2]
                # extract verses from string
                verses = re.findall('<verse>(.*?)</verse>', verse)
                for i in range(len(verses)):
                    if count > self.MAX_VERSES:
                        exit(0)
                    count += 1
                    if count % 20 == 0:
                        print(".", end="", flush=True)

                    verse = i + 1
                    text = verses[i]
                    
                    # text = self.convert_text_with_strongs(text)
                    text = self.convert_text_with_individual_words(text)
                    text = self.break_text_into_words(text)
                    self.replace_verse(book, chapter, verse, text)

    def fix_bible(self):
        count = 0
        for book in range(self.START_BOOK, self.END_BOOK):
            print("Processing " + str(book))
            query = "SELECT Book, Chapter, Verse, Scripture FROM Verses " + \
                "WHERE Book=" + str(book) + " ORDER BY Chapter"
            self.cursor.execute(query)
            lines = self.cursor.fetchall()
            for line in lines:
                chapter = line[1]
                verse = line[2]
                scripture = line[3]
                scripture = scripture.replace("、", "、 ")
                self.replace_verse(book, chapter, verse, scripture)

    def break_text_into_words(self, line):
        words = line.split(" ")
        text = ""
        for word in words:
            word = word.strip()
            if word == "":
                continue
            if re.search("[、：；。「 」，？ ]", word):
                text += word + " "
            else:
                encode = self.url_encode_chinese(word)
                if not self.entry_exists(encode):
                    text += self.split_word(word) + " "
                else:
                    text += word + " "
        return text
    
    def split_word(self, word):
        for i in range(len(word)-1, 0, -1):
            prefix = word[:i]
            suffix = word[i:]
            if self.entry_exists(self.url_encode_chinese(prefix)):
                if not self.entry_exists(self.url_encode_chinese(suffix)):
                    suffix = self.split_word(suffix)
                return prefix + " " + suffix
        return word

    def url_encode_chinese(self, text):
        return quote(text, encoding='utf-8')
    
    def entry_exists(self, topic):
        if topic in self.lexiconCache:
            return self.lexiconCache[topic]
        self.cursorLexicon.execute("SELECT COUNT(*) from Lexicon where topic='" + topic + "'")
        count = self.cursorLexicon.fetchone()[0]
        found = True
        if count == 0:
            found = False
        self.lexiconCache[topic] = found
        return found

    def convert_text_with_individual_words(self, line):
        # remove <vid> tags
        line = re.sub("<vid.*?>(.*?)</vid>", "", line)
        # remove <sup> tags
        line = re.sub("<sup>(.*?)</sup>", " ", line)
        line = line.replace("<mbn>", "").replace("</mbn>", "")
        line = line.replace("<S>", "").replace("</S>", "")
        line = line.replace("†", "")
        line = re.sub("([：；。「 」，？])", " \\1 ", line)
        line = line.replace("  ", " ")
        line = line.replace("  ", " ")
        line = line.replace("  ", " ")
        
        return line

    def convert_text_with_strongs(self, line):
        # remove <vid> tags
        line = re.sub("<vid.*?>(.*?)</vid>", "", line)
        # convert <ref> tags
        line = re.sub("<ref onclick='lex.*?'>(.*?)</ref>", " \\1 ", line)
        # remove <sup> tags
        line = line.replace("<sup>", "").replace("</sup>", "")
        line = line.replace("<mbn>", "").replace("</mbn>", "")
        line = line.replace("†", "")
        line = line.replace("  ", " ")
        line = line.replace("  ", " ")
        return line

    def replace_verse(self, book, chapter, verse, text):
        self.cursor.execute("UPDATE Verses SET Scripture = ? WHERE Book = ? AND Chapter = ? AND Verse = ?", (text, book, chapter, verse))

db = TransformBible()
db.debug = False
db.overwrite = False
# db.process_bible()
db.fix_bible()