import glob
import re
import sys

if len(sys.argv) < 2:
    print("Search parameters: keyword (part of speech)")
    exit(1)

keyword = sys.argv[1]

if len(sys.argv) == 3:
    pos = sys.argv[2]
else:
    pos = ""

filesearch = "../strongs/h/*.md"

count = 0
for file in glob.glob(filesearch):
    fp = open(file)
    line = fp.readline()
    original_word = line
    original_word = original_word.replace("\n", "")
    original_word = original_word.replace("# ", "")
    found = False
    while line:
        line = fp.readline()
        if ("www.blueletterbible.org/lang/lexicon" in line):
            transliteral = re.search('\[(.+?)\]', line).group(1)
        if ("Part of speech:" in line):
            part_of_speech = line.replace("Part of speech: ", "").replace("\n", "")
        if ("Definition:" in line) and (re.search(r'\b' + keyword + r'\b', line)):
            found = True
            definition = line.replace("Definition: ", "").replace("\n", "")
    if (found and pos != ""):
        if not (pos in part_of_speech):
            found = False
    if found:
        print(original_word)
        print("[" + transliteral + "](" + file + ")")
        print(part_of_speech)
        print(definition)
        print()
