import glob
import re
import sys

saveToFile = True
limit = 10

if len(sys.argv) < 2:
    print("Please enter number of occurances")
    exit(1)

occurance = sys.argv[1]

# filesearch = "../strongs/h/*.md"
filesearch = "../strongs/g/*.md"

if saveToFile:
    fileSave = open("words.txt", "w+")

count = 0
for file in glob.glob(filesearch):
    fp = open(file)
    line = fp.readline()
    found = False
    search = "Occurs " + occurance + " times"
    while line:
        line = fp.readline()
        if ("www.blueletterbible.org/lang/lexicon" in line):
            transliteral = re.search('\[(.+?)\]', line).group(1)
        elif (search in line):
            found = True
        elif ("Definition:" in line):
            definition = line.replace("Definition: ", "").replace("\n", "")
    if found:
        count = count + 1
        if (count > limit):
            break
        print("[" + transliteral + "](" + file + ")")
        print(definition)
        print()
        if saveToFile:
            fileSave.write("[" + transliteral + "]\n")
