import glob
import re
import sys
import os
import subprocess

saveToFile = True
limit = 10

if len(sys.argv) < 2:
    print("Please enter book/books [luke,acts]")
    exit(1)

parameter = sys.argv[1]

books = parameter.split(",")

ntBooks = ["matthew", "mark", "luke", "john", "acts", "romans", "1corinthians", "2corinthians",
    "galatians", "ephesians", "philippians", "colossians",
    "1thessalonians", "2thessalonians", "1timothy", "2timothy", "titus", "philemon", "hebrews",
    "james", "1peter", "2peter", "1john", "2john", "3john", "jude", "revelation"]

testBooks = ntBooks

count = 0

with open("words.txt") as fp:
    word = fp.readline()
    while word:
        word = word.replace("\n","")

        # Check if not in any of the other books
        test1 = True
        for book in testBooks:
            if not(book in books):
                if os.path.isdir("../bible/" + book):
                    command = "fgrep -o '" + word + "' ../bible/" + book + "/*.md | wc -l"
                    bytes = subprocess.check_output(command, shell=True)
                    result = bytes.decode("utf-8") 
                    count = int(result)
                    if (count > 0):
                        test1 = False
                        break

        # Check exists in the books to search for
        test2 = False
        for book in books:
            command = "fgrep -o '" + word + "' ../bible/" + book + "/*.md | wc -l"
            bytes = subprocess.check_output(command, shell=True)
            result = bytes.decode("utf-8") 
            count = int(result)
            if (count > 0):
                test2 = True
                break

        if (test1 and test2):
            print(word)

        word = fp.readline()