# https://en.wikipedia.org/wiki/Gematria
# https://www.hebrew4christians.com/Grammar/Unit_Eight/Hebrew_Gematria/hebrew_gematria.html

import sys

if len(sys.argv) < 2:
    print("Please enter word to calculate")
    exit(1)

word = sys.argv[1]

standard = dict()
standard['א'] = 1
standard['ב'] = 2 
standard['ב'] = 3
standard['ד'] = 4
standard['ה'] = 5
standard['ו'] = 6
standard['ז'] = 7
standard['ח'] = 8
standard['ט'] = 9
standard['י'] = 10
standard['כ'] = 20
standard['ל'] = 30
standard['מ'] = 40
standard['נ'] = 50
standard['ס'] = 60
standard['ע'] = 70
standard['פ'] = 80
standard['צ'] = 90
standard['ק'] = 100
standard['ר'] = 200
standard['ש'] = 300
standard['ת'] = 400
standard['ך'] = 20
standard['ם'] = 40
standard['ן'] = 50
standard['ף'] = 80
standard['ץ'] = 90

mispar = dict()
mispar['א'] = 1
mispar['ב'] = 2 
mispar['ב'] = 3
mispar['ד'] = 4
mispar['ה'] = 5
mispar['ו'] = 6
mispar['ז'] = 7
mispar['ח'] = 8
mispar['ט'] = 9
mispar['י'] = 10
mispar['כ'] = 20
mispar['ל'] = 30
mispar['מ'] = 40
mispar['נ'] = 50
mispar['ס'] = 60
mispar['ע'] = 70
mispar['פ'] = 80
mispar['צ'] = 90
mispar['ק'] = 100
mispar['ר'] = 200
mispar['ש'] = 300
mispar['ת'] = 400
mispar['ך'] = 500
mispar['ם'] = 600
mispar['ן'] = 700
mispar['ף'] = 800
mispar['ץ'] = 900


ordinal = dict()
ordinal['א'] = 1
ordinal['ב'] = 2 
ordinal['ב'] = 3
ordinal['ד'] = 4
ordinal['ה'] = 5
ordinal['ו'] = 6
ordinal['ז'] = 7
ordinal['ח'] = 8
ordinal['ט'] = 9
ordinal['י'] = 10
ordinal['כ'] = 11
ordinal['ל'] = 12
ordinal['מ'] = 13
ordinal['נ'] = 14
ordinal['ס'] = 15
ordinal['ע'] = 16
ordinal['פ'] = 17
ordinal['צ'] = 18
ordinal['ק'] = 19
ordinal['ר'] = 20
ordinal['ש'] = 21
ordinal['ת'] = 22
ordinal['ך'] = 23
ordinal['ם'] = 24
ordinal['ן'] = 25
ordinal['ף'] = 26
ordinal['ץ'] = 27

sum_standard = 0
sum_mispar = 0
sum_ordinal = 0

for c in word:
    if c in standard.keys():
        sum_standard = sum_standard + standard[c]
    if c in mispar.keys():
        sum_mispar = sum_mispar + mispar[c]
    if c in ordinal.keys():
        sum_ordinal = sum_ordinal + ordinal[c]

print('Standard: ' + str(sum_standard))
print('Mispar Gadol: ' + str(sum_mispar))
print('Ordinal: ' + str(sum_ordinal))