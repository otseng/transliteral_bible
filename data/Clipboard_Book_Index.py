#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import os
import platform

if len(sys.argv) == 3:
    book = sys.argv[1]
    chapter = sys.argv[2]
else:
    print(len(sys.argv))
    print("Usage: <book> <chapters>")
    exit(0)

uBook = book.title()
uBook = uBook.replace("1", "1 ")
uBook = uBook.replace("2", "2 ")
lBook = book.lower()
chapter = int(chapter) + 1

output = "# " + uBook + "\n\n" 

output = output + "[" + uBook + " Overview](../../commentary/" + lBook + "/" + lBook + "_overview.md)\n\n"

for x in range(1, chapter):
    output = output + "[" + uBook + " " + str(x) + "](" + lBook + "_" + str(x) +".md)\n"
    output = output + "\n"

output = output + "---\n"
output = output + "\n"
output = output + "[Transliteral Bible](../index.md)"

if platform.system() == "Linux":
    os.system("/bin/echo -n \"" + output + "\" | xclip -selection clipboard")
else:
    os.system("/bin/echo -n \"" + output + "\" | pbcopy")

print("Copied to clipboard")