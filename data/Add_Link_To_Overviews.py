import glob
import sys

# break_count = 5
break_count = 100

if len(sys.argv) < 2:
    print("Specify site (soniclight, biblehub)")
    exit(1)

def convert_book(book):
    if book[0].isdigit():
        return book[0] + "_" + book[1:]
    return book

site = sys.argv[1].lower()

filesearch = "../commentary/*/*_overview.md"

count = 0
for file in glob.glob(filesearch):
    count += 1
    if count > break_count:
        exit(0)
    book = file.split("/")[-1].split("_")[0]
    data = "\n"
    if site == "soniclight":
        data += "[Thomas Constable](https://soniclight.com/tcon/notes/html/{0}/{0}.htm)\n".format(book)
    elif site == "biblehub":
        book = convert_book(book)
        data += "[Bible Hub](https://biblehub.com/sum/{0})\n".format(book)
        
    else:
        print("Unknown site: " + site)
        exit(1)
    print("Adding link to " + file + " " + str(count))
    with open(file, "a") as myfile:
        myfile.write(data)


