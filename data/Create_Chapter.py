import sys
import os
import re

OVERWRITE = False

if len(sys.argv) < 3:
    print("Usage: book chapter override")
    print("Books:")
    print("matthew, mark, luke, john, acts, romans, 1corinthians, 2corinthians")
    print("galatians, ephesians, philippians, colossians")
    print("1thessalonians, 2thessalonians, 1timothy, 2timothy, titus, philemon, hebrews")
    print("james, 1peter, 2peter, 1john, 2john, 3john, jude, revelation")
    print("genesis, exodus, leviticus, numbers, deuteronomy")
    print("joshua, judges, ruth, 1samuel, 2samuel, 1kings, 1kings, 1chronicles, 2chronicles")
    print("ezra, nehemiah, esther, job, psalms, proverbs, ecclesiastes, solomon")
    print("isaiah, jeremiah, lamentations, ezekiel, daniel, hosea")
    print("joel, amos, obadiah, jonah, micah, nahum")
    print("habakkuk, zephaniah, haggai, zechariah, malachi")
    exit(1)

if len(sys.argv) == 4:
    OVERWRITE = True

book = sys.argv[1]
chapter = sys.argv[2]
bookfile = book.lower()

def format(str):
    p = re.compile('(\d)(.*)')
    s = p.sub(r'\1 \2', str)
    return s

if (bookfile == "matthew"):
    bookname = bookfile.title()
    bookshort = "Mat"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "mark"):
    bookname = bookfile.title()
    bookshort = "Mar"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "luke"):
    bookname = bookfile.title()
    bookshort = "Luk"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "john"):
    bookname = bookfile.title()
    bookshort = "Joh"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "acts"):
    bookname = bookfile.title()
    bookshort = "Act"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "romans"):
    bookname = bookfile.title()
    bookshort = "Rom"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "1corinthians"):
    bookname = "1 Corinthians"
    bookshort = "Co1"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2corinthians"):
    bookname = "2 Corinthians"
    bookshort = "Co2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "galatians"):
    bookname = bookfile.title()
    bookshort = "Gal"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "ephesians"):
    bookname = bookfile.title()
    bookshort = "Eph"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "philippians"):
    bookname = bookfile.title()
    bookshort = "Phi"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "colossians"):
    bookname = bookfile.title()
    bookshort = "Col"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "1thessalonians"):
    bookname = "1 Thessalonians"
    bookshort = "Th1"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2thessalonians"):
    bookname = "2 Thessalonians"
    bookshort = "Th2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2thessalonians"):
    bookname = bookfile.title()
    bookshort = "Th2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "1timothy"):
    bookname = "1 Timothy"
    bookshort = "Ti1"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2timothy"):
    bookname = "2 Timothy"
    bookshort = "Ti2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "titus"):
    bookname = bookfile.title()
    bookshort = "Tit"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "philemon"):
    bookname = bookfile.title()
    bookshort = "Plm"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "hebrews"):
    bookname = bookfile.title()
    bookshort = "Heb"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "james"):
    bookname = bookfile.title()
    bookshort = "Jam"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "1peter"):
    bookname = "1 Peter"
    bookshort = "Pe1"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2peter"):
    bookname = "2 Peter"
    bookshort = "Pe2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "1john"):
    bookname = "1 John"
    bookshort = "Jo1"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "2john"):
    bookname = "2 John"
    bookshort = "Jo2"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "3john"):
    bookname = "3 John"
    bookshort = "Jo3"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "jude"):
    bookname = bookfile.title()
    bookshort = "Jde"
    infile = open("kjv_nt.txt", "r")
elif (bookfile == "revelation"):
    bookname = bookfile.title()
    bookshort = "Rev"
    infile = open("kjv_nt.txt", "r")

elif (bookfile == "genesis"):
    bookname = bookfile.title()
    bookshort = "Gen"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "exodus"):
    bookname = bookfile.title()
    bookshort = "Exo"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "leviticus"):
    bookname = bookfile.title()
    bookshort = "Lev"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "numbers"):
    bookname = bookfile.title()
    bookshort = "Num"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "deuteronomy"):
    bookname = bookfile.title()
    bookshort = "Deu"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "joshua"):
    bookname = bookfile.title()
    bookshort = "Jos"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "judges"):
    bookname = bookfile.title()
    bookshort = "Jdg"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "ruth"):
    bookname = bookfile.title()
    bookshort = "Rut"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "1samuel"):
    bookname = "1 Samuel"
    bookshort = "Sa1"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "1samue2"):
    bookname = "2 Samuel"
    bookshort = "Sa2"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "1kings"):
    bookname = "1 Kings"
    bookshort = "Kg1"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "2kings"):
    bookname = "2 Kings"
    bookshort = "Kg2"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "1chronicles"):
    bookname = "1 Chronicles"
    bookshort = "Ch1"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "2chronicles"):
    bookname = "2 Chronicles"
    bookshort = "Ch2"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "ezra"):
    bookname = bookfile.title()
    bookshort = "Ezr"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "nehemiah"):
    bookname = bookfile.title()
    bookshort = "Neh"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "esther"):
    bookname = bookfile.title()
    bookshort = "Est"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "job"):
    bookname = bookfile.title()
    bookshort = "Job"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "psalms"):
    bookname = bookfile.title()
    bookshort = "Psa"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "proverbs"):
    bookname = bookfile.title()
    bookshort = "Pro"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "ecclesiastes"):
    bookname = bookfile.title()
    bookshort = "Ecc"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "solomon"):
    bookname = bookfile.title()
    bookshort = "Sol"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "isaiah"):
    bookname = bookfile.title()
    bookshort = "Isa"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "jeremiah"):
    bookname = bookfile.title()
    bookshort = "Jer"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "lamentations"):
    bookname = bookfile.title()
    bookshort = "Lam"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "ezekiel"):
    bookname = bookfile.title()
    bookshort = "Eze"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "daniel"):
    bookname = bookfile.title()
    bookshort = "Dan"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "hosea"):
    bookname = bookfile.title()
    bookshort = "Hos"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "joel"):
    bookname = bookfile.title()
    bookshort = "Joe"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "amos"):
    bookname = bookfile.title()
    bookshort = "Amo"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "obadiah"):
    bookname = bookfile.title()
    bookshort = "Oba"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "jonah"):
    bookname = bookfile.title()
    bookshort = "Jon"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "micah"):
    bookname = bookfile.title()
    bookshort = "Mic"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "nahum"):
    bookname = bookfile.title()
    bookshort = "Nah"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "habakkuk"):
    bookname = bookfile.title()
    bookshort = "Hab"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "zephaniah"):
    bookname = bookfile.title()
    bookshort = "Zep"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "haggai"):
    bookname = bookfile.title()
    bookshort = "Hag"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "zechariah"):
    bookname = bookfile.title()
    bookshort = "Zac"
    infile = open("kjv_ot.txt", "r")
elif (bookfile == "malachai"):
    bookname = bookfile.title()
    bookshort = "Mal"
    infile = open("kjv_ot.txt", "r")
else:
    print(book + " not found")
    exit(1)

directory = "../bible/" + bookfile.lower()

if not os.path.exists(directory):
    os.mkdir(directory)

    outfile = open(directory + "/" + bookfile + ".md", "w")
    outfile.write("# " + bookname + "\n\n")
    outfile.write("[" + bookname + " 1](" + bookfile + "_1.md)\n\n")
    outfile.write("---\n\n")
    outfile.write("[Transliteral Bible](../bible.md)")
    outfile.close()

filename = directory + "/" + bookfile + "_" + chapter + ".md"
if OVERWRITE or not os.path.isfile(filename) :
    outfile = open(filename, "w")

    outfile.write("# [" + book + " " + chapter + "]( " +
        "https://www.blueletterbible.org/kjv/" + book + "/" + chapter + ")\\n\n")

    for line in infile.readlines():
        token = line.split("|")
        if (token[0] == bookshort) and (token[1] == chapter):
            chap = token[1]
            verse = token[2]
            text = token[3]
            link = "\n\n<a name=\"" + bookfile + "_" + chap + "_" + verse + "\"></a>" + bookname + " " + chap + ":" + verse
            outfile.write(link + "\n")
            outfile.write("\n")

            text = text.replace("~","")
            text = text.strip()
            outfile.write(text)

    outfile.write("\n\n---\n\n")
    outfile.write("[Transliteral Bible](../bible.md)\n\n")
    outfile.write("[" + bookname + "](" + bookfile.lower() + ".md)\n\n")
    if int(chapter) > 1:
        outfile.write("[" + bookname + " " + str(int(chapter) - 1) + "](" + bookfile.lower() + "_" + str(int(chapter) - 1) + ".md) - ")
    outfile.write("[" + bookname + " " + str(int(chapter) + 1) + "](" + bookfile.lower() + "_" + str(int(chapter) + 1) + ".md)")

    print("Created " + filename + "!")

    infile.close()
    outfile.close()
else:
    print(filename + " already exists")