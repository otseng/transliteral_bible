#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import os

if len(sys.argv) <= 2:
    print(len(sys.argv))
    print("Usage: <book> <chapter> (lxx)")
    exit(0)
elif len(sys.argv) > 2:
    book = sys.argv[1]
    chapter = sys.argv[2]

if (len(sys.argv) == 4):
    append = "_" + sys.argv[3]
else:
    append = ""

uBook = book.title()
lBook = book.lower()
chapter = int(chapter)

output = ""
output = output + "# [" + uBook + " " + str(chapter) + "]()\n" 

output = output + "\n"
output = output + "\n"
output = output + "---\n"
output = output + "\n"
output = output + "[Transliteral Bible](../bible.md)\n"
output = output + "\n"
output = output + "[" + uBook + "](" + lBook + append + ".md)\n"
output = output + "\n"
output = output + "[" + uBook + "](" + lBook + append + "_" + str(chapter - 1) + ".md) - [" + uBook + "](" + lBook + append + "_" + str(chapter + 1) + ".md)"

os.system("echo \"" + output + "\" | pbcopy")
print("Copied to clipboard")