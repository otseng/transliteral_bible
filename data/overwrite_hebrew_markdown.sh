FILE="/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json"
if [[ ! -f "$FILE" ]]; then
    FILE=~/.config/Code/User/snippets/markdown.json
fi
echo '< user/markdown'
echo '> snippets/markdown'
diff `dirname "$0"`/../setup/markdown_hebrew.json "$FILE"
read -p "Press enter to overwrite file"
cp `dirname "$0"`/../setup/markdown_hebrew.json "$FILE" 
echo "Overwrote markdown with Hebrew"
