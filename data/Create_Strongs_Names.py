#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import re
import sys
import os
import html
import webbrowser
from urllib import request

overwrite = False

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

while True:
    if len(sys.argv) == 1:
        strongs = input("Enter number: ")
    elif len(sys.argv) == 2:
        strongs = sys.argv[1]
    elif len(sys.argv) == 3:
        strongs = sys.argv[1]
        overwrite = True
        print("Overwriting file")

    if (strongs == "" or strongs == "q"):
        exit(0)

    strongs = strongs.lower()
    results = re.findall(r'\d+', strongs)
    if results:
        number = results[0]
    else:
        print("Not found")
        continue

    hebrew = False
    greek = False

    first = strongs[0]
    if (first == 'h'):
        hebrew = True
    elif (first == 'h'):
        greek = True
    else:
        first = 'g'
        greek = True

    file = "../strongs/" + first + "/" + first + number + ".md"

    if os.path.isfile(file) and not overwrite:
        with open(file, 'r') as myfile:
            filedata = myfile.read()
        re_transliteral = re.compile(r'\[(.*)\]\(https:\/\/www.blueletterbible.org')
        s_transliteral = html.unescape(re_transliteral.search(filedata).group(1))
        output = "[" + s_transliteral + "](../" + file + ")"
        os.system("echo \"" + output + "\" | pbcopy")
        print("Already exists: " + file)
        print(output)
    else:
        url = "https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=" + strongs
        response = request.urlopen(url)
        pagedata = response.read().decode("utf-8")
        re_transliteral = re.compile('<title>.* - (.*) - .*</title>')
        s_transliteral = html.unescape(re_transliteral.search(pagedata).group(1))

        print("Creating " + file)

        if first == 'g':
            re_original = re.compile('lexTitleGk.*>(.*)<')
            s_original = re_original.search(pagedata).group(1)
        elif first == 'h':
            re_original = re.compile('lexTitleHb.*>(.*)<')
            s_original = re_original.search(pagedata).group(1)

        re_count = re.compile('which occurs (.*) times in .*>(.*)<')
        g_count = re_count.search(pagedata)
        s_count1 = g_count.group(1)
        s_count2 = g_count.group(2)

        re_speech = re.compile('Part of Speech</div>[\s\S]*<div class="small-text-right">(.*)</div>')
        g_speech = re_speech.search(pagedata)
        s_speech = g_speech.group(1).strip()

        re_definition = re.compile('<div id="outlineBiblical".*>[\s\S]*<div class="row show-for-medium">')
        g_definition = re_definition.search(pagedata)
        s_definition = g_definition.group(0)
        s_definition = s_definition.replace("<p>", "<p>, ")
        s_definition = cleanhtml(s_definition)
        s_definition = s_definition.replace("&quot;", "")
        s_definition = s_definition.strip()

        outfile = open(file, "w")

        outfile.write("# " + s_original + "\n\n")
        outfile.write("[" + s_transliteral + "](" + url + ")\n\n")

        definition = "Definition: " + s_definition
        definition = definition.replace(": ,", ":")
        outfile.write(definition + "\n\n")
        outfile.write("Part of speech: " + s_speech + "\n\n")
        outfile.write("Occurs " + s_count1 + " times in " + s_count2 + " verses\n\n")
        outfile.write("## Articles\n\n")

        outfile.write("[Study Light](https://www.studylight.org/lexicons/greek/" + number + ".html)\n\n")
        outfile.write("[Bible Hub](https://biblehub.com/str/greek/" + number + ".htm)\n\n")
        outfile.write("[LSJ](https://lsj.gr/wiki/" + s_original + ")\n\n")
        outfile.write("[NET Bible](http://classic.net.bible.org/strong.php?id=" + number + ")\n")

        outfile.close()

        # https://www.url-encode-decode.com/
        encoded = s_transliteral.replace("ō", "%C5%8D")
        encoded = s_transliteral.replace("ē", "%C4%93")

        output = "[" + s_transliteral + "](../" + file + ")"
        os.system("echo \"" + output + "\" | pbcopy")
        print(output)

    if len(sys.argv) > 1:
        exit(0)