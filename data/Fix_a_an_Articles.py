import os
import re
import sys

# https://owl.purdue.edu/owl/general_writing/grammar/articles_a_versus_an.html
#
# Use "a":
# consonant sound
# "u" makes the same sound as "you" (unicorn)
# "o" makes the same sound as "w" (one)
#
# Use "an":
# vowel sound
# silent "h" (an hour)

chapters = []

if len(sys.argv) < 2:
    print("Usage: python fix_a_an_articles.py psalms 1")
    sys.exit(0)
elif len(sys.argv) == 3:
    chapter = sys.argv[2]
    chapters.append(chapter)
else:
    chapters = [str(i) for i in range(1, 151)]
book = sys.argv[1]

total_count = 0
for chapter in chapters:
    chapter_count = 0
    filename = "../bible/" + book + "/" + book + "_" + chapter + ".md"
    new_filename = "../bible/" + book + "/" + book + "_" + chapter + "_new.md"

    if not os.path.exists(filename):
        break

    input = open(filename, "r")
    output = open(new_filename, "w")

    conversions = [
        ("A \[('?[AaāâEeēĕIi])", "An [\\1"),
        (" a \[('?[AaāâEeēĕIi])", " an [\\1"),
        ("An \[('?[bcdfgjklmnpqrsštvwxyz])", "A [\\1"),
        (" an \[('?[bcdfgjklmnpqrsštvwxyz])", " a [\\1"),
    ]

    lines = input.readlines()
    for line in lines:
        for conversion in conversions:
            res = re.search(conversion[0], line)
            if res:
                chapter_count += 1
                total_count += 1
                line = re.sub(conversion[0], conversion[1], line)
        output.write(line)
    os.remove(filename)
    os.rename(new_filename, filename)
    if chapter_count > 0:
        print("Fixed " + filename)

if total_count == 0:
    print("All good")
