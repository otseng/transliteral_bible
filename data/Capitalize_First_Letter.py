import os
import platform
import sys

while True:
    text = input("Enter text: ")
    first_letter_location = 0
    for i in range(0, len(text)):
        first_letter = text[i]
        if first_letter in ('[', "'", 'ʿ', 'ʾ', 'ʹ', 'ʺ', 'ʻ', 'ʼ', 'ʽ', 'ʾ', 'ʿ'):
            continue
        else:
            first_letter_location = i
            break

    text = text[:first_letter_location] + text[first_letter_location].upper() + text[first_letter_location+1:]

    print(text + " copied to clipboard")
        
    if platform.system() == "Linux":
        os.system("/bin/echo -n \"" + text + "\" | xclip -selection clipboard")
    else:
        os.system("/bin/echo -n \"" + text + "\" | pbcopy")