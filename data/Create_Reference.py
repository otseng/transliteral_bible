#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import os
import re

if len(sys.argv) == 4:
    book = sys.argv[1]
    chapter = sys.argv[2]
    verse = sys.argv[3]
else:
    print(len(sys.argv))
    print("Usage: <book> <chapter> <verse>")
    exit(0)

def format(str):
    p = re.compile('(\d)(.*)')
    s = p.sub(r'\1 \2', str)
    return s
    
uBook = book.title()
fBook = format(uBook)
lBook = book.lower()

line = '[' + fBook + ' ' + chapter + ':' + verse + '](../../bible/' + lBook + '/' + lBook + '_' + chapter + '.md#' + lBook + '_' + chapter + '_' + verse + ')'

os.system("echo \"" + line + "\" | pbcopy")

print(line + " copied to clipboard")
