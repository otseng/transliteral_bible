FILE="/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json"
if [[ ! -f "$FILE" ]]; then
    FILE=~/.config/Code/User/snippets/markdown.json
fi

fgrep -o '[chay](../../strongs/h/h2416.md)' "$FILE" > /dev/null
if [[ $? == 0 ]] ; then
    echo "Saving Hebrew"
    cp "$FILE" `dirname "$0"`/../setup/markdown_hebrew.json
else
    echo "It's not Hebrew"
fi