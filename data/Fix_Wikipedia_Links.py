
import glob
import re
import sys

# replaces ( with %28
# replaces ) with %29

# break_count = 5
break_count = 1000

if len(sys.argv) < 2:
    print("Usage: python fix_wikipedia_links.py [greek|hebrew]")
    sys.exit(0)

if sys.argv[1] == 'greek':
    filesearch = "../strongs/g/*.md"
elif sys.argv[1] == 'hebrew':
    filesearch = "../strongs/h/*.md"
else:
    filesearch = "../strongs/g/*.md"

total_count = 0
fix_count = 0
for filename in glob.glob(filesearch):
    file = open(filename, "r")
    filecontents = ''
    lines = file.readlines()
    found = False
    test = "(.*?)(https:\/\/en.wikipedia.org\/wiki\/.*_)\((.*?)\)(.*?)"
    # https://en.wikipedia.org/wiki/Ab_(Semitic)
    for line in lines:
        if re.search(test, line):
            found = True
            fix_count = fix_count + 1
            line = re.sub(test, r"\1\2%28\3%29\4", line)
        filecontents = filecontents + line

    if found:
        print(filename)
        myfile = open(filename, "w")
        myfile.writelines(filecontents)
        myfile.close()
        if fix_count >= break_count:
            break

    total_count = total_count + 1

print("Total: " + str(total_count))
print("Fixed: " + str(fix_count))

