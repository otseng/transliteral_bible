import sys
import os

if len(sys.argv) < 3:
    print("Usage: book chapter <number to start incrementing from>")
    exit(1)

book = sys.argv[1]
chapter = sys.argv[2]
start = sys.argv[3]

def increment(line):
    for x in range(int(start), 100):
        old = '[^' + str(x) + ']'
        new = '[^' + str(int(x)+1) + ']'
        newline = line.replace(old, new)
        if (line != newline):
            return(newline)
    return line

bookname = book.lower()

directory = "../bible/" + bookname
filename = directory + "/" + bookname + "_" + chapter + ".md"
workfile = filename + ".work"

if os.path.isfile(filename) :
    print("Modifying " + filename)

    file = open(filename, 'r')
    work = open(workfile, 'w') 
    lines = file.readlines()
    
    count = 0
    for line in lines: 
        work.write(increment(line))
    
    file.close()
    work.close()

    os.remove(filename)
    os.rename(workfile, filename)
else:
    print(filename + " does not exist")
