import os, sqlite3, re, glob, platform
from pathlib import Path
import requests
from BibleData import BibleData
from urllib.request import urlopen
from bs4 import BeautifulSoup 
import time
import random

class Downloader_HEBT:

    MAX_CHAPTERS = 200
    MAX_VERSES = 200

    BIBLE_NAME = "HEBT"
    DESCRIPTION = "Hebrew Transliteration"
    BIBLE_SITE = "https://biblehub.com/interlinear/transliterated"

    books = {
        "genesis": 1,
        "exodus": 2,
        "leviticus": 3,
        "numbers": 4,
        "deuteronomy": 5,
        "joshua": 6,
        "judges": 7,
        "ruth": 8,
        "1samuel": 9,
        "2samuel": 10,
        "1kings": 11,
        "2kings": 12,
        "1chronicles": 13,
        "2chronicles": 14,
        "ezra": 15,
        "nehemiah": 16,
        "esther": 17,
        "job": 18,
        "psalms": 19,
        "proverbs": 20,
        "ecclesiastes": 21,
        "songs": 22,
        "isaiah": 23,
        "jeremiah": 24,
        "lamentations": 25,
        "ezekiel": 26,
        "daniel": 27,
        "hosea": 28,
        "joel": 29,
        "amos": 30,
        "obadiah": 31,
        "jonah": 32,
        "micah": 33,
        "nahum": 34,
        "habakkuk": 35,
        "zephaniah": 36,
        "haggai": 37,
        "zechariah": 38,
        "malachi": 39,

        }

    def __init__(self):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'
        self.file = self.home + '/UniqueBible/uniquebible/marvelData/bibles/' + self.BIBLE_NAME + '.bible'
        self.connection = sqlite3.connect(self.file)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def process_bible(self):
        for book in self.books.keys():
            print("Processing " + book)
            self.process_book(book)

    # https://biblehub.com/interlinear/transliterated/genesis/1.htm
    def process_book(self, book):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        request_count = 0
        for chapter in range(1, chapters + 1):
            if chapter > self.MAX_CHAPTERS:
                return
            if self.chapter_exists(self.cursor, book_num, chapter) and not self.overwrite:
                continue
            print(book + " " + str(chapter))
            self.delete_chapter(book_num, chapter)
            verses = BibleData.verses[book_num][chapter]
            bookname = book.replace("1", "1_")
            bookname = bookname.replace("2", "2_")
            bookname = bookname.replace("3", "3_")
            file = self.get_file(bookname, chapter)
            if not os.path.isfile(file):
                command = "curl --location --request GET " + self.BIBLE_SITE + f"/{bookname}/{chapter}.htm > html/{bookname}-{chapter}.html"
                os.system(command)
                if self.pause:
                    rand = random.randint(5, 20)
                    # print("Pausing " + str(rand))
                    time.sleep(rand)
            if os.path.isfile(file):
                with open(file, 'r') as file:
                    html = file.read()
            else:
                print("Could not load " + file)
                break

            soup = BeautifulSoup(html, "html.parser")

            chapterText = ""
            for verse in range(1, verses + 1):
                if verse > self.MAX_VERSES:
                    break
                verseMarker = f"{verse}.*"
                spanObject = soup.find("span", {'class': 'refheb'}, string=re.compile(verseMarker))
                vid = "v" + str(book_num) + str(chapter).zfill(3) + str(verse).zfill(3)
                verseText = ""
                chapterSubText = ""
                for sibling in spanObject.next_siblings:
                    wordData = sibling.find("a")
                    if (wordData == None):
                        break
                    elif (wordData != -1):
                        if wordData.contents == []:
                            continue
                        word = wordData.contents[0]
                        if word == "p̄":
                            continue
                        strongs = ""
                        href = wordData['href']
                        search = re.search('hebrew/(.*?).htm', href)
                        if (search):
                            strongs = search.group(1)
                        lastChar = word[-1]
                        if lastChar in ('.', ',', ';', ':', '?', '!', '"', "'"):
                            word = word[:-1]
                            verseText += word + lastChar + " "
                            chapterSubText += f"<ref onclick=\"lex('H{strongs}')\">{word}</ref>{lastChar} "
                        else:
                            verseText += word + " "
                            chapterSubText += f"<ref onclick=\"lex('H{strongs}')\">{word}</ref> "

                self.insert_verse(self.cursor, book_num, chapter, verse, verseText)
                
                key = str(book_num) + "." + str(chapter) + "." + str(verse)
                if key in BibleData.agbSubheadings.keys():
                    if verse > 1:
                        chapterText += "<br><br>"
                    chapterText += "<u><b>" + BibleData.agbSubheadings[key] + "</b></u><br><br>"
                elif (book_num, chapter, verse) in BibleData.asvParagraphs:
                    chapterText += "<br><br>"

                chapterText += "<verse>"
                chapterText += ("<vid id=\"v{0}.{1}.{2}\" onclick=\"luV({2})\">{2} "
                    "</vid>"
                    ).format(book_num, chapter, verse)
                chapterText += chapterSubText
                chapterText += "</verse>"
                # print(chapterText)

            try:
                self.insert_chapter(self.cursor, book_num, chapter, chapterText)
            except:
                pass
        self.connection.commit()

    def clean_text(self, contents):
        contents = re.sub("<a .*?>.*?<\/a>", "", str(contents))
        contents = re.sub('<[^>]+>', "", contents)
        contents = contents.replace(";", "; ")
                
        return contents
    
    def get_file(self, book, chapter):
        file = "./html/" + book + "-" + str(chapter) + ".html"
        return file
    
    def get_url(self, book, chapter):
        url = self.BIBLE_SITE + book + "-" + str(chapter) + "/"
        return url

    def create_tables(self):
        print('Create tables')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Details (Title NVARCHAR(100), 
                               Abbreviation NVARCHAR(50), Information TEXT, Version INT, OldTestament BOOL,
                               NewTestament BOOL, Apocrypha BOOL, Strongs BOOL);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Bible (Book INT, Chapter INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Verses (Book INT, Chapter INT, Verse INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Notes (Book INT, Chapter INT, Verse INT, ID TEXT, Note TEXT);
                            ''')

    def drop_tables(self):
        print('Drop tables')
        self.cursor.execute('DROP TABLE IF EXISTS Bible;')
        self.cursor.execute('DROP TABLE IF EXISTS Details;')
        self.cursor.execute('DROP TABLE IF EXISTS Verses;')
        self.cursor.execute('DROP TABLE IF EXISTS Notes;')

    def insert_details(self):
        if self.get_count(self.cursor, 'Details') == 0:
            sql = ("INSERT INTO Details VALUES ('" + self.DESCRIPTION + "', '" + self.BIBLE_NAME + "',"
                   "'" + self.BIBLE_SITE + "', 1, 1, 1, 0, 1);")
            self.cursor.execute(sql)
            sql = ("INSERT INTO Verses ( Scripture, Book, Chapter, Verse ) "
                "VALUES ('Transliteral Bible', 0, 0, 0)")
            self.cursor.execute(sql)

    def get_count(self, cursor, table):
        cursor.execute('SELECT COUNT(*) from ' + table)
        count = cursor.fetchone()[0]
        return count

    def delete_book(self, book_num):
        self.cursor.execute('DELETE from Bible where Book=' + str(book_num))
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num))

    def delete_chapter(self, book_num, chapter_num):
        self.cursor.execute('DELETE from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num))

    def chapter_exists(self, cursor, book_num, chapter_num):
        cursor.execute('SELECT COUNT(*) from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        count = cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def delete_verse(self, cursor, book_num, chapter_num, verse_num):
        cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))

    def verse_exists(self, cursor, book_num, chapter_num, verse_num):
        cursor.execute('SELECT COUNT(*) from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))
        count = cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_chapter(self, cursor, book_num, chapter_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        text = text.replace('\n', '')
        print("Inserting bible: book {0} chapter {1}".format(book_num, chapter_num))
        sql = ("INSERT INTO Bible (Book, Chapter, Scripture) VALUES ({0}, {1}, '{2}');".format(book_num, chapter_num, text))
        if self.debug:
            print(sql)
        else:
            cursor.execute(sql)

    def insert_verse(self, cursor, book_num, chapter_num, verse_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        print("Inserting verses: book {0} chapter {1} verse {2}".format(book_num, chapter_num, verse_num))
        sql = ("INSERT INTO Verses VALUES ({0}, {1}, {2}, '{3}');".format(book_num, chapter_num, verse_num, text))
        if self.debug:
            print(sql)
        else:
            cursor.execute(sql)

    def reset_chapter(self, cursor, book_name, chapter_num):
        book_num = self.get_book_num(book_name)
        if book_num:
            self.delete_chapter(cursor, book_num, chapter_num)
            filename = self.BASE_DIR + book_name + '/' + book_name + '_' + str(chapter_num) + '.md'
            if os.path.exists(filename):
                self.process_chapter(filename)
            else:
                print(filename + " does not exist")
        else:
            print("Could not find " + book_name)

    def get_book_num(self, book):
        if not book in self.books:
            return False 
        book_num = self.books[book]
        # print(book + ":" + str(book_num))
        return book_num

    def get_chapter_verse(self, line):
        # <a name="matthew_1_1"></a>Matthew 1:1
        res = re.search('_(\d+)_(\d+)', line).groups()
        return res[0], res[1]

    def test1(self):
        bookname = "genesis"
        chapter = 1
        command = "curl --location --request GET " + self.BIBLE_SITE + f"/{bookname}/{chapter}.htm > html/{bookname}-{chapter}.html"
        print(command)

db = Downloader_HEBT()
db.debug = False
db.overwrite = False
db.pause = False

# db.drop_tables()
# db.create_tables()
# db.insert_details()

db.delete_book(4) 

db.process_bible()

# db.test1()