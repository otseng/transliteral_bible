import os, sqlite3, re, glob, platform
from pathlib import Path
import requests
from BibleData import BibleData
from urllib.request import urlopen
from bs4 import BeautifulSoup 
import time
import random

class Downloader_KJV_Parallel:

    MAX_CHAPTERS = 28

    BIBLE_NAME_TR = "KJV-TR"
    BIBLE_NAME_CT = "KJV-CT"
    BIBLE_SITE = "https://kjvparallelbible.org/"

    books = {
        # "genesis": 1,
        # "exodus": 2,
        # "leviticus": 3,
        # "numbers": 4,
        # "deuteronomy": 5,
        # "joshua": 6,
        # "judges": 7,
        # "ruth": 8,
        # "1samuel": 9,
        # "2samuel": 10,
        # "1kings": 11,
        # "2kings": 12,
        # "1chronicles": 13,
        # "2chronicles": 14,
        # "ezra": 15,
        # "nehemiah": 16,
        # "esther": 17,
        # "job": 18,
        # "psalms": 19,
        # "proverbs": 20,
        # "ecclesiastes": 21,
        # "song": 22,
        # "isaiah": 23,
        # "jeremiah": 24,
        # "lamentations": 25,
        # "ezekiel": 26,
        # "daniel": 27,
        # "hosea": 28,
        # "joel": 29,
        # "amos": 30,
        # "obadiah": 31,
        # "jonah": 32,
        # "micah": 33,
        # "nahum": 34,
        # "habakkuk": 35,
        # "zephaniah": 36,
        # "haggai": 37,
        # "zechariah": 38,
        # "malachi": 39,

        # "matthew": 40,
        "mark": 41,
        "luke": 42,
        "john": 43,
        "acts": 44,
        "romans": 45,
        "1corinthians": 46,
        "2corinthians": 47,
        "galatians": 48,
        "ephesians": 49,
        "philippians": 50,
        "colossians": 51,
        "1thessalonians": 52,
        "2thessalonians": 53,
        "1timothy": 54,
        "2timothy": 55,
        "titus": 56,
        "philemon": 57,
        "hebrews": 58,
        "james": 59,
        "1peter": 60,
        "2peter": 61,
        # "1john": 62,
        # "2john": 63,
        # "3john": 64,
        # "jude": 65,
        # "revelation": 66
        }

    def __init__(self):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'
        self.fileTR = self.home + '/UniqueBible/marvelData/bibles/' + self.BIBLE_NAME_TR + '.bible'
        self.fileCT = self.home + '/UniqueBible/marvelData/bibles/' + self.BIBLE_NAME_CT + '.bible'
        self.connectionTR = sqlite3.connect(self.fileTR)
        self.connectionCT = sqlite3.connect(self.fileCT)
        self.cursorTR = self.connectionTR.cursor()
        self.cursorCT = self.connectionCT.cursor()

    def __del__(self):
        self.connectionTR.commit()
        self.connectionTR.close()
        self.connectionCT.commit()
        self.connectionCT.close()

    def process_bible(self):
        for book in self.books.keys():
            print("Processing " + book)
            self.process_book(book)

    # https://kjvparallelbible.org/matthew-1/
    def process_book(self, book):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        request_count = 0
        for chapter in range(1, chapters + 1):
            if chapter > self.MAX_CHAPTERS:
                return
            if self.chapter_exists(self.cursorTR, book_num, chapter) and not self.overwrite:
                continue
            print(book + " " + str(chapter))
            self.delete_tr_ct_chapter(book_num, chapter)
            verses = BibleData.verses[book_num][chapter]
            file = self.get_file(book, chapter)
            if not os.path.isfile(file):
                bookname = book.replace("1", "1-")
                bookname = bookname.replace("2", "2-")
                bookname = bookname.replace("3", "3-")
                if chapters > 1:
                    bookname = bookname + "-" + str(chapter)
                os.system(f"curl --location --request GET 'https://kjvparallelbible.org/{bookname}/' > html/{book}-{chapter}.html")
                rand = random.randint(5, 20)
                print("Pausing " + str(rand))
                time.sleep(rand)
            if os.path.isfile(file):
                with open(file, 'r') as file:
                    html = file.read()
            else:
                print("Could not load " + file)
                break;

            soup = BeautifulSoup(html, "html.parser")

            contentTR = ""
            contentCT = ""
            for verse in range(1, verses + 1):
                offset = verse + 4
                row = soup.find(class_="et_pb_row_" + str(offset))
                contents = row.find_all(class_="et_pb_text_inner")
                if contents:
                    verseTR = self.clean_text(str(contents[0].contents[0]), verse)
                    verseCT = self.clean_text(str(contents[1].contents[0]), verse)
                else:
                    break
                self.insert_verse(self.cursorTR, book_num, chapter, verse, verseTR)
                self.insert_verse(self.cursorCT, book_num, chapter, verse, verseCT)
                
                key = str(book_num) + "." + str(chapter) + "." + str(verse)
                if key in BibleData.agbSubheadings.keys():
                    if verse > 1:
                        contentTR += "<br><br>"
                        contentCT += "<br><br>"
                    contentTR += "<u><b>" + BibleData.agbSubheadings[key] + "</b></u><br><br>"
                    contentCT += "<u><b>" + BibleData.agbSubheadings[key] + "</b></u><br><br>"
                elif (book_num, chapter, verse) in BibleData.asvParagraphs:
                    contentTR += "<br><br>"
                    contentCT += "<br><br>"

                contentTR += "<verse>"
                contentTR += ("<vid id=\"v{0}.{1}.{2}\" onclick=\"luV({2})\">{2} "
                    "</vid>"
                    ).format(book_num, chapter, verse)
                contentTR += verseTR
                contentTR += "</verse>"

                contentCT += "<verse>"
                contentCT += ("<vid id=\"v{0}.{1}.{2}\" onclick=\"luV({2})\">{2} "
                    "</vid>"
                    ).format(book_num, chapter, verse)
                contentCT += verseCT
                contentCT += "</verse>"

            try:
                self.insert_chapter(self.cursorTR, book_num, chapter, contentTR)
                self.insert_chapter(self.cursorCT, book_num, chapter, contentCT)
            except:
                pass
        self.connectionTR.commit()
        self.connectionCT.commit()

    def clean_text(self, text, verse):
        text = text.replace("<p>", "")
        text = text.replace("</p>", "")
        text = re.sub("^" + str(verse) + " ", "", text)
        return text
    
    def get_file(self, book, chapter):
        file = "./html/" + book + "-" + str(chapter) + ".html"
        return file
    
    def get_url(self, book, chapter):
        url = self.BIBLE_SITE + book + "-" + str(chapter) + "/"
        return url

    def create_tr_ct_tables(self):
        self.create_tables(self.cursorTR)
        self.create_tables(self.cursorCT)

    def create_tables(self, cursor):
        print('Create tables')
        cursor.execute('''CREATE TABLE IF NOT EXISTS Details (Title NVARCHAR(100), 
                               Abbreviation NVARCHAR(50), Information TEXT, Version INT, OldTestament BOOL,
                               NewTestament BOOL, Apocrypha BOOL, Strongs BOOL);
                            ''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS Bible (Book INT, Chapter INT, Scripture TEXT);
                            ''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS Verses (Book INT, Chapter INT, Verse INT, Scripture TEXT);
                            ''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS Notes (Book INT, Chapter INT, Verse INT, ID TEXT, Note TEXT);
                            ''')

    def drop_tr_ct_tables(self):
        self.drop_tables(self.cursorTR)
        self.drop_tables(self.cursorCT)
        
    def drop_tables(self, cursor):
        print('Drop tables')
        cursor.execute('DROP TABLE IF EXISTS Bible;')
        cursor.execute('DROP TABLE IF EXISTS Details;')
        cursor.execute('DROP TABLE IF EXISTS Verses;')
        cursor.execute('DROP TABLE IF EXISTS Notes;')

    def insert_tr_ct_details(self):
        self.insert_details(self.cursorTR, self.BIBLE_NAME_TR, self.BIBLE_SITE)
        self.insert_details(self.cursorCT, self.BIBLE_NAME_CT, self.BIBLE_SITE)

    def insert_details(self, cursor, bible_name, BIBLE_SITE):
        if self.get_count(cursor, 'Details') == 0:
            description = "KJV Textus Receptus"
            if "CT" in bible_name:
                description = "KJV Critical Text"
            sql = ("INSERT INTO Details VALUES ('" + description + "', '" + bible_name + "',"
                   "'" + BIBLE_SITE + "', 1, 1, 1, 0, 1);")
            cursor.execute(sql)
            sql = ("INSERT INTO Verses ( Scripture, Book, Chapter, Verse ) "
                "VALUES ('Transliteral Bible', 0, 0, 0)")
            cursor.execute(sql)

    def get_count(self, cursor, table):
        cursor.execute('SELECT COUNT(*) from ' + table)
        count = cursor.fetchone()[0]
        return count

    def delete_book(self, cursor, book_num):
        cursor.execute('DELETE from Bible where Book=' + str(book_num))
        cursor.execute('DELETE from Verses where Book=' + str(book_num))

    def delete_tr_ct_chapter(self, book_num, chapter_num):
        self.delete_chapter(self.cursorTR, book_num, chapter_num)
        self.delete_chapter(self.cursorCT, book_num, chapter_num)

    def delete_chapter(self, cursor, book_num, chapter_num):
        cursor.execute('DELETE from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num))

    def chapter_exists(self, cursor, book_num, chapter_num):
        cursor.execute('SELECT COUNT(*) from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        count = cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def delete_verse(self, cursor, book_num, chapter_num, verse_num):
        cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))

    def verse_exists(self, cursor, book_num, chapter_num, verse_num):
        cursor.execute('SELECT COUNT(*) from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))
        count = cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_chapter(self, cursor, book_num, chapter_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        text = text.replace('\n', '')
        print("Inserting bible: book {0} chapter {1}".format(book_num, chapter_num))
        sql = ("INSERT INTO Bible (Book, Chapter, Scripture) VALUES ({0}, {1}, '{2}');".format(book_num, chapter_num, text))
        if self.debug:
            print(sql)
        else:
            cursor.execute(sql)

    def insert_verse(self, cursor, book_num, chapter_num, verse_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        print("Inserting verses: book {0} chapter {1} verse {2}".format(book_num, chapter_num, verse_num))
        sql = ("INSERT INTO Verses VALUES ({0}, {1}, {2}, '{3}');".format(book_num, chapter_num, verse_num, text))
        if self.debug:
            print(sql)
        else:
            cursor.execute(sql)

    def reset_chapter(self, cursor, book_name, chapter_num):
        book_num = self.get_book_num(book_name)
        if book_num:
            self.delete_chapter(cursor, book_num, chapter_num)
            filename = self.BASE_DIR + book_name + '/' + book_name + '_' + str(chapter_num) + '.md'
            if os.path.exists(filename):
                self.process_chapter(filename)
            else:
                print(filename + " does not exist")
        else:
            print("Could not find " + book_name)

    def get_book_num(self, book):
        if not book in self.books:
            return False 
        book_num = self.books[book]
        # print(book + ":" + str(book_num))
        return book_num

    def get_chapter_verse(self, line):
        # <a name="matthew_1_1"></a>Matthew 1:1
        res = re.search('_(\d+)_(\d+)', line).groups()
        return res[0], res[1]

    def test1(self):
        book = "matthew"
        chapter = 3
        print(f"curl --location --request GET 'https://kjvparallelbible.org/{book}-{chapter}/' > html/{book}-{chapter}.html")

db = Downloader_KJV_Parallel()
db.debug = False
db.overwrite = False

# db.drop_tr_ct_tables()
# db.create_tr_ct_tables()
# db.insert_tr_ct_details()

# db.delete_book(22) 

db.process_bible()

# db.test1()