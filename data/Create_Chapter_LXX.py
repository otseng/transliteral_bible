import sys
import os
import html
import re
from urllib import request

ENABLE_MASORETIC = False
OVERWRITE = False

if len(sys.argv) < 2:
    print("Usage: <url> override")
    print("URL: http://ecmarsh.com/lxx-kjv/genesis/gen_001.htm")
    exit(1)

url = sys.argv[1]   

if len(sys.argv) == 3:
    OVERWRITE = True

response = request.urlopen(url)
pagedata = response.read().decode("utf-8")

dataformat = 2

if '<td class="lxxnum">' in pagedata:
    dataformat = 1

re_bookname = re.compile('<span class="book">(.*)</span>')
bookname = re_bookname.search(pagedata).group(1)
re_chapter = re.compile('<span class="chapter">.* (.*)</span>')
re_result = re_chapter.search(pagedata)
chapter = re_result.group(1)

start = pagedata.find('<span class="chapter">')
end = pagedata.find('EndEditable', start)

bibledata = pagedata[start:end]
bibledata = bibledata.replace("\n", "")

filename = bookname.lower() + "_" + chapter + ".md"
if ENABLE_MASORETIC:
    outfile = open(filename, "w")
    outfile.write("# " + bookname.title() + " " + chapter + "\n\n")

if dataformat == 1:
    re_verse = re.compile('<td class="lxxnum">(\d*?)</td>\s*<td>(.*?)</td>\s*<td class="kjvnum">(\d*?)</td>\s*<td>(.*?)</td>')

    iterator = re_verse.finditer(bibledata)

    for match in iterator:
        verse = match.group(1)
        if (verse != ''):
            masoretic = match.group(4).strip()
            link = "<a name=\"" + bookname.lower() + "_" + chapter + "_" + verse + "\"></a>" + bookname.title() + " " + chapter + ":" + verse
            if ENABLE_MASORETIC:
                outfile.write(link + "\n")
                outfile.write("\n")
                outfile.write(masoretic)
                outfile.write("\n\n")
                print(filename + " created")
else:
    bibledata = bibledata[bibledata.find("<td style")+10:]
    bibledata = bibledata[bibledata.find("<td style")+10:]
    bibledata = bibledata[bibledata.find("<td style")+10:]
    bibledata = bibledata.replace("\t","")
    bibledata = re.sub(" +", " ", bibledata)
    bibledata = bibledata.replace("</p>", "<br />")

    masoreticdata = bibledata[0:bibledata.find("<td style")]
    lxxdata = bibledata[bibledata.find("<td style")+25:]

    masoreticdata = masoreticdata[masoreticdata.find("<p>")+3:]
    lxxdata = lxxdata[masoreticdata.find("<p>")+3:]

    re_verse = re.compile('\d*?:(\d*?) (.*?)<br />')

    iterator = re_verse.finditer(masoreticdata)

    for match in iterator:
        verse = match.group(1)
        if (verse != ''):
            masoretic = match.group(2).strip()
            link = "<a name=\"" + bookname.lower() + "_" + chapter + "_" + verse + "\"></a>" + bookname.title() + " " + chapter + ":" + verse
            if ENABLE_MASORETIC:    
                outfile.write(link + "\n")
                outfile.write("\n")
                outfile.write(masoretic)
                outfile.write("\n\n")
                print(filename + " created")
                outfile.close()

directory = "../bible/" + bookname.lower()

if not os.path.exists(directory):
    os.mkdir(directory)

filename = directory + "/" + bookname.lower() + "_lxx.md"
if not os.path.isfile(filename):
    outfile = open(filename, "w")
    outfile.write("# " + bookname.title() + "\n\n")
    outfile.write("[" + bookname.title() + " 1](" + bookname.lower() + "_lxx_1.md)\n\n")
    outfile.write("---\n\n")
    outfile.write("[Transliteral Bible](../bible.md)")
    outfile.close()

filename = directory + "/" + bookname.lower() + "_lxx_" + chapter + ".md"
if OVERWRITE or not os.path.isfile(filename) :
    outfile = open(filename, "w")

    outfile.write("# [" + bookname.title() + " " + chapter + "](" + url + ")\n\n")

    if dataformat == 1:
        iterator = re_verse.finditer(bibledata)

        for match in iterator:
            verse = match.group(1)
            if (verse != ''):
                septuagint = match.group(2).strip()
                link = "<a name=\"" + bookname.lower() + "_lxx_" + chapter + "_" + verse + "\"></a>" + bookname.title() + " " + chapter + ":" + verse
                outfile.write(link + "\n")
                outfile.write("\n")

                outfile.write(septuagint)
                outfile.write("\n\n")
        print(filename + " created")
    else:
        iterator = re_verse.finditer(lxxdata)

        for match in iterator:
            verse = match.group(1)
            if (verse != ''):
                septuagint = match.group(2).strip()
                link = "<a name=\"" + bookname.lower() + "_" + chapter + "_" + verse + "\"></a>" + bookname.title() + " " + chapter + ":" + verse
                outfile.write(link + "\n")
                outfile.write("\n")

                outfile.write(septuagint)
                outfile.write("\n\n")

        outfile.write("---\n\n")
        outfile.write("[Transliteral Bible](../bible.md)\n\n")
        outfile.write("[" + bookname.title() + "](" + bookname.lower() + "_lxx.md)\n\n")
        outfile.write("[" + bookname.title() + " " + str(int(chapter) - 1) + "](" + bookname.lower() + "_lxx.md) - " "[" + bookname.title() + " " + str(int(chapter) + 1) + "](" + bookname.lower() + "_lxx.md)")

        print(filename + " created")

    outfile.close()
else:
    print(filename + " exists")