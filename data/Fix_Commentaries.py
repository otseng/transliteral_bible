import glob
import re
import sys

# break_count = 1
break_count = 1000

filesearch = "../commentary/romans/*.md"

count = 0
for file in glob.glob(filesearch):
    print(file)
    filecontents = open(file).read()
    p = re.compile('<a name=([a-z_0-9]*)></a>')
    filecontents = p.sub(r'<a name="\1"></a>', filecontents)
    # print(filecontents)
    myfile = open(file, "w")
    myfile.writelines(filecontents)
    myfile.close()
    count = count + 1
    if count >= break_count:
        break
print(str(count) + " done")

