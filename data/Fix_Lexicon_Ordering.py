import glob
import re
import sys

# break_count = 5
break_count = 1000

if len(sys.argv) < 2:
    print("Usage: python fix_lexicon_ordering.py [greek|hebrew|edenics]")
    sys.exit(0)

if sys.argv[1] == 'greek':
    filesearch = "../strongs/g/*.md"
    # should_go_first = "^Occurs .* times in .*"
    should_go_first = "^Greek: "
    should_go_second = "^Hebrew: "
elif sys.argv[1] == 'hebrew':
    filesearch = "../strongs/h/*.md"
    # should_go_first = "^Occurs .* times in .*"
    should_go_first = "^Hebrew: "
    should_go_second = "^Greek: "
elif sys.argv[1] == 'edenics':
    filesearch = "../strongs/h/*.md"
    should_go_first = "Occurs .* times in .*"
    should_go_second = "Edenics:"

count = 0
for filename in glob.glob(filesearch):
    file = open(filename, "r")
    filecontents = ''
    found_first = False
    found_second = False
    correct_order = False
    skip_next_line = False
    save_line = ''
    lines = file.readlines()
    for line in lines: 
        if re.match(should_go_first, line):
            found_first = True
            if not found_second:
                correct_order = True
                break
            else:
               filecontents = filecontents + line + "\n"
               filecontents = filecontents + save_line
        elif re.match(should_go_second, line):
            found_second = True
            save_line = line
            skip_next_line = True
        elif skip_next_line:
            skip_next_line = False
        else:
            filecontents = filecontents + line

    if not correct_order and found_first and found_second:
        print(filename)
        myfile = open(filename, "w")
        myfile.writelines(filecontents)
        myfile.close()
        count = count + 1
        if count >= break_count:
            break
print(str(count) + " done")

