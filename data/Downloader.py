import os, sqlite3, re, glob, platform

from pathlib import Path

import requests

from BibleData import BibleData

class Downloader:

    MAX_CHAPTERS = 999
    MAX_VERSES = 999

    BIBLE_NAME = "Recovery"
    BIBLE_INFO = "https://www.recoveryversion.bible"

    books = {
        # "genesis": 1,
        # "exodus": 2,
        # "leviticus": 3,
        # "numbers": 4,
        # "deuteronomy": 5,
        # "joshua": 6,
        # "judges": 7,
        # "ruth": 8,
        # "1samuel": 9,
        # "2samuel": 10,
        # "1kings": 11,
        # "2kings": 12,
        # "1chronicles": 13,
        # "2chronicles": 14,
        # "ezra": 15,
        # "nehemiah": 16,
        # "esther": 17,
        # "job": 18,
        # "psalms": 19,
        # "proverbs": 20,
        # "ecclesiastes": 21,
        # "song": 22,
        # "isaiah": 23,
        # "jeremiah": 24,
        # "lamentations": 25,
        # "ezekiel": 26,
        # "daniel": 27,
        # "hosea": 28,
        # "joel": 29,
        # "amos": 30,
        # "obadiah": 31,
        # "jonah": 32,
        # "micah": 33,
        # "nahum": 34,
        # "habakkuk": 35,
        # "zephaniah": 36,
        # "haggai": 37,
        # "zechariah": 38,
        # "malachi": 39,

        # "matthew": 40,
        # "mark": 41,
        # "luke": 42,
        # "john": 43,
        # "acts": 44,
        # "romans": 45,
        # "1corinthians": 46,
        # "2corinthians": 47,
        # "galatians": 48,
        # "ephesians": 49,
        # "philippians": 50,
        # "colossians": 51,
        # "1thessalonians": 52,
        # "2thessalonians": 53,
        # "1timothy": 54,
        # "2timothy": 55,
        # "titus": 56,
        # "philemon": 57,
        # "hebrews": 58,
        # "james": 59,
        # "1peter": 60,
        # "2peter": 61,
        # "1john": 62,
        # "2john": 63,
        # "3john": 64,
        # "jude": 65,
        # "revelation": 66
        }

    def __init__(self):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'
        self.file = self.home + '/UniqueBible/marvelData/bibles/' + self.BIBLE_NAME + '.bible'
        self.connection = sqlite3.connect(self.file)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def process_bible(self):
        for book in Downloader.books.keys():
            print("Processing " + book)
            self.process_book(book)

    def process_book(self, book):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        request_count = 0
        for chapter in range(1, chapters + 1):
            content = ''
            if chapter > Downloader.MAX_CHAPTERS:
                return
            if self.chapter_exists(book_num, chapter):
                continue
            print(book + " " + str(chapter))
            self.delete_chapter(book_num, chapter)
            verses = BibleData.verses[book_num][chapter]
            for verse in range(1, verses + 1):
                if verse > Downloader.MAX_VERSES:
                    break
                passage = book + " " + str(chapter) + ":" + str(verse)
                try:
                    url = "https://api.lsm.org/recver.php?String='" + passage + "'&Out=json"
                    response = requests.get(url)
                    json = response.json()
                    text = json['verses'][0]['text']
                except Exception as ex:
                    print(json)
                    print(ex)

                try:
                    text = text.replace("\t", "")

                    self.insert_verse(book_num, chapter, verse, text)
                
                    key = str(book_num) + "." + str(chapter) + "." + str(verse)
                    if key in BibleData.agbSubheadings.keys():
                        if verse > 1:
                            content += "<br><br>"
                        content += "<u><b>" + BibleData.agbSubheadings[key] + "</b></u><br><br>"
                    elif (book_num, chapter, verse) in BibleData.asvParagraphs:
                        content += "<br><br>"

                    content += "<verse>"
                    content += ("<vid id=\"v{0}.{1}.{2}\" onclick=\"luV({2})\">{2} "
                        "</vid>"
                        ).format(book_num, chapter, verse)
                    content += text
                    content += "</verse>"
                except:
                    pass

            try:
                self.insert_chapter(book_num, chapter, content)
            except:
                pass
        self.connection.commit()


    def create_tables(self):
        print('Create tables')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Details (Title NVARCHAR(100), 
                               Abbreviation NVARCHAR(50), Information TEXT, Version INT, OldTestament BOOL,
                               NewTestament BOOL, Apocrypha BOOL, Strongs BOOL);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Bible (Book INT, Chapter INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Verses (Book INT, Chapter INT, Verse INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Notes (Book INT, Chapter INT, Verse INT, ID TEXT, Note TEXT);
                            ''')

    def drop_tables(self):
        print('Drop tables')
        self.cursor.execute('DROP TABLE IF EXISTS Bible;')
        self.cursor.execute('DROP TABLE IF EXISTS Details;')
        self.cursor.execute('DROP TABLE IF EXISTS Verses;')
        self.cursor.execute('DROP TABLE IF EXISTS Notes;')

    def insert_details(self):
        if self.get_count('Details') == 0:
            sql = ("INSERT INTO Details VALUES ('" + self.BIBLE_NAME + "', '" + self.BIBLE_NAME + "',"
                   "'" + self.BIBLE_INFO + "', 1, 1, 1, 0, 1);")
            self.cursor.execute(sql)
            sql = ("INSERT INTO Verses ( Scripture, Book, Chapter, Verse ) "
                "VALUES ('Transliteral Bible', 0, 0, 0)")
            self.cursor.execute(sql)

    def get_count(self, table):
        self.cursor.execute('SELECT COUNT(*) from ' + table)
        count = self.cursor.fetchone()[0]
        return count

    def delete_book(self, book_num):
        self.cursor.execute('DELETE from Bible where Book=' + str(book_num))
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num))

    def delete_chapter(self, book_num, chapter_num):
        self.cursor.execute('DELETE from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num))

    def chapter_exists(self, book_num, chapter_num):
        self.cursor.execute('SELECT COUNT(*) from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def delete_verse(self, book_num, chapter_num, verse_num):
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))

    def verse_exists(self, book_num, chapter_num, verse_num):
        self.cursor.execute('SELECT COUNT(*) from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_chapter(self, book_num, chapter_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        text = text.replace('\n', '')
        print("Inserting bible: book {0} chapter {1}".format(book_num, chapter_num))
        sql = ("INSERT INTO Bible (Book, Chapter, Scripture) VALUES ({0}, {1}, '{2}');".format(book_num, chapter_num, text))
        if self.debug:
            print(sql)
        else:
            self.cursor.execute(sql)

    def insert_verse(self, book_num, chapter_num, verse_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        print("Inserting verses: book {0} chapter {1} verse {2}".format(book_num, chapter_num, verse_num))
        sql = ("INSERT INTO Verses VALUES ({0}, {1}, {2}, '{3}');".format(book_num, chapter_num, verse_num, text))
        if self.debug:
            print(sql)
        else:
            self.cursor.execute(sql)

    def reset_chapter(self, book_name, chapter_num):
        book_num = self.get_book_num(book_name)
        if book_num:
            self.delete_chapter(book_num, chapter_num)
            filename = Downloader.BASE_DIR + book_name + '/' + book_name + '_' + str(chapter_num) + '.md'
            if os.path.exists(filename):
                self.process_chapter(filename)
            else:
                print(filename + " does not exist")
        else:
            print("Could not find " + book_name)

    def get_book_num(self, book):
        if not book in Downloader.books:
            return False 
        book_num = Downloader.books[book]
        # print(book + ":" + str(book_num))
        return book_num

    def get_chapter_verse(self, line):
        # <a name="matthew_1_1"></a>Matthew 1:1
        res = re.search('_(\d+)_(\d+)', line).groups()
        return res[0], res[1]
        
    def unformat(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def replace_greek_link(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<ref onclick=\"lex('G\\2')\">\\1</ref>", line)
        return res

    def replace_hebrew_link(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[h](\d+)\.md\)", "<ref onclick=\"lex('H\\2')\">\\1</ref>", line)
        return res

    def replace_greek_simple(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def replace_hebrew_simple(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[h](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def remove_footnote(self, line):
        res = re.sub("\[\^\d+\]", "", line)
        return res

    def red_letter(self, line):
        res = re.sub("\*\*(.+?)\*\*", "<red>\\1</red>", line)
        return res

    def remove_red_letter(self, line):
        res = line.replace("**", "")
        return res

    def test1(self):
        res = db.get_chapter_verse('<a name="matthew_2_3"></a>Matthew 2:3')
        print(res)

    def test3(self):
        line = "**This is in red.** This is not.  **Is this red?**"
        print(self.red_letter(line))

db = Downloader()
db.debug = False
db.overwrite = True

# db.drop_tables()
# db.create_tables()
# db.insert_details()

# db.delete_book(22)

db.process_bible()
