#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import platform
import sys
import os
import re
from urllib import request
import webbrowser

def checkExists(url):
    try:
        response = request.urlopen(url)
        webbrowser.open(url)
        return True
    except:
        return False

if len(sys.argv) == 2:
    name = sys.argv[1]
else:
    print("Creates links for biblicalcyclopedia and videobible")
    print("Usage: name") 
    exit(0)

uName = name.title()
lName = name.lower()
letter = uName[0]

lines = ""

index = f'https://www.biblicalcyclopedia.com/{letter}'
url = index + f'/{lName}.html'
line = f'[Biblical Cyclopedia]({url})'
if (checkExists(url)):
    lines += line + "\n\n"

index = 'https://www.videobible.com/bible-dictionary'
url = index + f'/{lName}'
line = f'[Video Bible]({url})'
if (checkExists(url)):
    lines += line + "\n"


if platform.system() == "Linux":
    os.system("/bin/echo -n \"" + lines + "\" | xclip -selection clipboard")
else:
    os.system("/bin/echo -n \"" + lines + "\" | pbcopy")

print(lines)
