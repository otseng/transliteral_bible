#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import sys
import os
import re
from pathlib import Path

if len(sys.argv) == 4:
    book = sys.argv[1]
    chapter = sys.argv[2]
    verse = sys.argv[3]
else:
    print(len(sys.argv))
    print("Usage: <book> <chapter> <verse>")
    exit()

def format(str):
    p = re.compile('(\d)(.*)')
    s = p.sub(r'\1 \2', str)
    return s

def determineFootnoteNumber(filecontents, index):
    for x in range(1, 100):
        search_string = '[^' + str(x) + ']'
        if not (search_string in filecontents[0:index]):
            return x
    return -1

def findPlaceToInsert(filecontents, search_string):
    index = filecontents.find(search_string)
    if index < 0:
        print('Could not find ' + search_string)
        exit()
    else:
        while (filecontents[index] != "\n"):
            index = index + 1
        index = index + 1
        while (filecontents[index] != "\n"):
            index = index + 1
        index = index + 1
        while (filecontents[index] != "\n"):
            index = index + 1
        return index

def incrementOtherFootnotes(filecontents, footnote, index):
    for x in range(100,int(footnote)-1,-1):
        old = '[^' + str(x) + ']'
        new = '[^' + str(int(x)+1) + ']'
        # print(old+":"+new)
        filecontents = filecontents.replace(old, new)
    return filecontents

def insertFootnote(filecontents, line1, footnote):
    search_string = '[^' + str(footnote+1) + ']:'
    index = filecontents.find(search_string)
    if index < 0:
        filecontents = filecontents + "\n" + line1 + "\n"
    else:
        filecontents = filecontents[:index] + line1 + '\n\n' + filecontents[index:] 
    return filecontents

uBook = book.title()
fBook = format(uBook)
lBook = book.lower()

line2 = '# ' + fBook + ' ' + chapter
line3 = '<a name="' + lBook + '_' + chapter + '_' + verse +'"></a>[' + fBook + ' ' + chapter + ':' + verse + '](../../bible/' + lBook + '/' + lBook + '_' + chapter + '.md#' + lBook + '_' + chapter + '_' + verse + ')'
line4 = '[Wikipedia](https://en.wikipedia.org/wiki/' + uBook + '_' + chapter + ':' + verse + ')'
commentary = '../../commentary/' + lBook + '/' + lBook + '_' + chapter + '_commentary.md'
has_footnotes = True

filename = '../bible/' + lBook + '/' + lBook + '_' + chapter + '.md'
if not os.path.isfile(filename):
    print(filename + ' does not exist')
    exit()
file = Path(filename)

filecontents = file.read_text(None, None)
testcommentary = '[' + fBook + ' ' + chapter + ':' + verse + ' Commentary]'
if testcommentary in filecontents:
    print(filename + " already has footnote")
    already_has_footnote = True
else:
    print('Modifying chapter: ' + filename)
    already_has_footnote = False
    has_footnotes = "[^1]" in filecontents
    search_string = '<a name="'+ lBook + '_' + chapter + '_' + verse + '">'
    index = findPlaceToInsert(filecontents, search_string)
    footnote = determineFootnoteNumber(filecontents, index)
    filecontents = incrementOtherFootnotes(filecontents, footnote, index)
    filecontents = filecontents[:index] + ' [^' + str(footnote) + ']' + filecontents[index:]
    filecontents = filecontents
    # if not (filecontents[len(filecontents)-2:len(filecontents)-1] == '\n\n'):
    #     filecontents = filecontents + '\n'
    if not has_footnotes:
        filecontents = filecontents + '\n\n---\n'
    line1 = '[^' + str(footnote) + ']: [' + fBook + ' ' + chapter + ':' + verse + ' Commentary](' + commentary + '#' + lBook + '_' + chapter + '_' + verse + ')'
    filecontents = insertFootnote(filecontents, line1, footnote)
    myfile = open(filename, "w")
    myfile.writelines(filecontents)
    myfile.close()

commentary = commentary.replace('../../', '../')
print('Modifying commentary: ' + commentary)
if not os.path.isfile(commentary):
    myfile = open(commentary, "w")
    myfile.write('# ' + fBook + ' ' + chapter)
    myfile.write('\n')
    myfile.write('\n')
    myfile.write(line3)
    myfile.write('\n\n')
    myfile.write(line4)
    myfile.write('\n')
    myfile.close()
    exit()
file = Path(commentary)
filecontents = file.read_text(None, None)
for x in range(int(verse)+1, 200):
    search_string = '<a name="'+ lBook + '_' + chapter + '_' + str(x) + '">'
    # print(search_string)
    index = filecontents.find(search_string)
    if already_has_footnote:
        replace = line4
    else:
        replace = line3 + '\n\n' + line4
    if index > 0:
        filecontents = filecontents[:index] + replace + '\n\n' + filecontents[index:] 
        break
    if x == 199:
        filecontents = filecontents + '\n' + replace + '\n'
myfile = open(commentary, "w")
myfile.writelines(filecontents)
myfile.close()
