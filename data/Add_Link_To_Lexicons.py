import glob
import re
import sys

# break_count = 5
break_count = 10000

ARTICLES = "## Articles"

def findPlaceToInsert(filecontents, language):
    if language == "hebrew":
        index = filecontents.find("[Study Light]")
        if index < 0:
            return len(filecontents) - 1
    else:
        index = filecontents.find("[Study Light]")
        if index < 0:
            return len(filecontents)
    # while (filecontents[index] != ")"):
    #     index = index + 1
    # index = index + 1
    return index

if len(sys.argv) < 3:
    print("Please specify language (greek, hebrew) and site (blueletterbible, biblehub, studylight.org, lsj, net.bible, newjerusalem, lexiconcordance, biblestudycompany)")
    exit(1)

language = sys.argv[1].lower()
site = sys.argv[2].lower()

if (language == "greek"):
    filesearch = "../strongs/g/*.md"
elif (language == "hebrew"):
    filesearch = "../strongs/h/*.md"
else:
    print("Unknown language: " + language)
    exit(1)

count = 0
for file in glob.glob(filesearch):
    number = re.findall(r'\d+', file)[0]
    while (len(number) < 4):
        number = "0" + number
    filecontents = open(file).read()
    word = re.findall(r'# (.*)', filecontents)[0]
    if site == "biblehub":
        url = "[Bible Hub](https://biblehub.com/str/" + language + "/" + number + ".htm)"
    elif site == "studylight.org":
        url = "[Study Light](https://www.studylight.org/lexicons/" + language + "/" + number + ".html)"
    elif site == "lsj":
        url = "[LSJ](https://lsj.gr/wiki/" + word + ")"
    elif site == "net.bible":
        if (language == "hebrew"):
            number = "0" + number
        url = "[NET Bible](http://classic.net.bible.org/strong.php?id=" + number + ")"
    elif site == "newjerusalem":
        url = "[New Jerusalem](http://www.newjerusalem.org/Strongs/" + language[0] + number + ")"
    elif site == "lexiconcordance":
        url = "[Lexicon Concordance](http://lexiconcordance.com/" + language + "/" + number + ".html)"
    elif site == "biblestudycompany":
        url = "[Bible Study Company](https://biblestudycompany.com/reader/strongs/" + number + "/" + language + ")"
    elif site == "biblebento":
        url = "[Bible Bento](https://biblebento.com/dictionary/" + language[0].upper() + number + ".html)"
    elif site == "blueletterbible":
        url = "[Blue Letter Bible](https://www.blueletterbible.org/lexicon/" + language[0] + number + ")"
    else:
        print("Unknown site: " + site)
        exit(1)
    if site == "blueletterbible" and "[Blue Letter Bible]" in filecontents:
        print(file + " already done")
    elif site != "blueletterbible" and site in filecontents:
        print(file + " already done")
    else:
        if ARTICLES in filecontents:
            print(file + " added to articles")
            index = findPlaceToInsert(filecontents, language)
            newfilecontents = filecontents[:index] + url + "\n\n" + filecontents[index:]
            myfile = open(file, "w")
            myfile.writelines(newfilecontents)
            myfile.close()
        else:
            print(file + " appended articles")
            with open(file, "a") as myfile:
                myfile.write("\n\n## Articles\n\n")
                myfile.write(url)
        count = count + 1
        if count >= break_count:
            break
print(str(count) + " done")

