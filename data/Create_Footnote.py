#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import platform
import sys
import os
import re

if len(sys.argv) >= 4:
    book = sys.argv[1]
    chapter = sys.argv[2]
    verse = sys.argv[3]
    footnote_number = '1'
if len(sys.argv) == 5:
    footnote_number = sys.argv[4]
else:
    print("Usage: <book> <chapter> <verse> [footnote number]")
    print("Example: mark 2 26 3")
    exit(0)

# print(len("argv: " + str(len(sys.argv))))

def format(str):
    p = re.compile('(\d)(.*)')
    s = p.sub(r'\1 \2', str)
    return s
    
uBook = book.title()
fBook = format(uBook)
lBook = book.lower()

line1 = '[^' + footnote_number + ']: [' + fBook + ' ' + chapter + ':' + verse + ' Commentary](../../commentary/' + lBook + '/' + lBook + '_' + chapter + '_commentary.md#' + lBook + '_' + chapter + '_' + verse + ')'
line2 = '# ' + fBook + ' ' + chapter
line3 = '<a name="' + lBook + '_' + chapter + '_' + verse +'"></a>[' + fBook + ' ' + chapter + ':' + verse + '](../../bible/' + lBook + '/' + lBook + '_' + chapter + '.md#' + lBook + '_' + chapter + '_' + verse + ')'
# line4 = '[Wikipedia](https://en.wikipedia.org/wiki/' + uBook + '_' + chapter + ':' + verse + ')'

output = line1 + "\n" + line2 + "\n\n" + line3

filename = '../commentary/' + lBook + '/' + lBook + '_' + chapter + '_commentary.md'
if os.path.isfile(filename):
    print('Copied to clipboard\n')
else:
    print("Creating " + filename)
    outfile = open(filename, "w")
    outfile.write(line2)
    outfile.write("\n\n")
    outfile.write(line3)
    outfile.close()

output = line1 + "\n" + "\n\n" + line3

output = output.replace('"', '\"')

if platform.system() == "Linux":
    os.system("/bin/echo -n '" + output + "' | xclip -selection clipboard")
else:
    os.system("/bin/echo -n '" + output + "' | pbcopy")

print(line1 + "\n")
print(line3 + "\n")
