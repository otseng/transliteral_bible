import os, sqlite3, re, glob, platform
from pathlib import Path
import sys
from LanguageData import LanguageData
from BibleData import BibleData

class HighlightMissing:

    def __init__(self):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'

        missing = self.home + '/UniqueBible/marvelData/bibles/Missing.bible'
        self.connectionMissing = sqlite3.connect(missing)
        self.cursorMissing = self.connectionMissing.cursor()

        self.TRLITx = self.home + '/UniqueBible/marvelData/bibles/TRLITx.bible'
        self.connectionTRLITx = sqlite3.connect(self.TRLITx)
        self.cursorTRLITx = self.connectionTRLITx.cursor()

        self.KJVx = self.home + '/UniqueBible/marvelData/bibles/KJVx.bible'
        self.connectionKJVx = sqlite3.connect(self.KJVx)
        self.cursorKJVx = self.connectionKJVx.cursor()

    def __del__(self):
        self.connectionMissing.commit()
        self.connectionMissing.close()

    def create_tables(self):
        self.cursorMissing.execute('''CREATE TABLE IF NOT EXISTS Details (Title NVARCHAR(100), 
                               Abbreviation NVARCHAR(50), Information TEXT, Version INT, OldTestament BOOL,
                               NewTestament BOOL, Apocrypha BOOL, Strongs BOOL);
                            ''')
        self.cursorMissing.execute('''CREATE TABLE IF NOT EXISTS Bible (Book INT, Chapter INT, Scripture TEXT);
                            ''')
        self.cursorMissing.execute('''CREATE TABLE IF NOT EXISTS Verses (Book INT, Chapter INT, Verse INT, Scripture TEXT);
                            ''')
        self.cursorMissing.execute('''CREATE TABLE IF NOT EXISTS Notes (Book INT, Chapter INT, Verse INT, ID TEXT, Note TEXT);
                            ''')

    def drop_tables(self):
        self.cursorMissing.execute('DROP TABLE IF EXISTS Bible;')
        self.cursorMissing.execute('DROP TABLE IF EXISTS Details;')
        self.cursorMissing.execute('DROP TABLE IF EXISTS Verses;')
        self.cursorMissing.execute('DROP TABLE IF EXISTS Notes;')

    def insert_details(self):
        if self.get_count('Details') == 0:
            sql = ("INSERT INTO Details VALUES ('Transliteral Bible', 'TRLITx',"
                   "'https://transliteralbible.com', 1, 1, 1, 0, 1);")
            self.cursorMissing.execute(sql)
            sql = ("INSERT INTO Verses ( Scripture, Book, Chapter, Verse ) "
                "VALUES ('Transliteral Bible', 0, 0, 0)")
            self.cursorMissing.execute(sql)

    def get_count(self, table):
        self.cursorMissing.execute('SELECT COUNT(*) from ' + table)
        count = self.cursorMissing.fetchone()[0]
        return count

    def delete_chapter(self, book_num, chapter_num):
        self.cursorMissing.execute('DELETE from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))

    def insert_chapter(self, book_num, chapter_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        text = text.replace('\n', '')
        if self.overwrite:
            self.delete_chapter(book_num, chapter_num)
        if not self.chapter_exists(book_num, chapter_num):
            print("Inserting book {0} chapter {1}".format(book_num, chapter_num))
            sql = ("INSERT INTO Bible (Book, Chapter, Scripture) VALUES ({0}, {1}, '{2}');".format(book_num, chapter_num, text))
            if self.debug:
                print(sql)
            else:
                self.cursorMissing.execute(sql)

    def insert_verse(self, book_num, chapter_num, verse_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        sql = ("INSERT INTO Verses VALUES ({0}, {1}, {2}, '{3}');".format(book_num, chapter_num, verse_num, text))
        self.cursorMissing.execute(sql)

    def delete_verse(self, book_num, chapter_num, verse_num):
        self.cursorMissing.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))

    def get_book_num(self, book):
        if not book in BibleData.books:
            print("Cannot find book: " + book)
            exit(1) 
        book_num = BibleData.books[book]
        return book_num

    def read_verse(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]

    def process_verse(self, book, chapter, verse):
        book_num = self.get_book_num(book)
        verseKJVx = self.read_verse(self.cursorKJVx, book_num, chapter, verse)
        verseTRLITx = self.read_verse(self.cursorTRLITx, book_num, chapter, verse)
        
        if book_num < 40:
            regex = r'H\d+' # Hebrew
        else:
            regex = r'G\d+' # Greek
        strongsKJVx = re.findall(regex, verseKJVx)
        strongsTRLITx = re.findall(regex, verseTRLITx)

        # Find items in TRLITx that are not in KJVx
        differences = self.get_list_differences(strongsKJVx, strongsTRLITx)
        self.remove_known_from_list(differences)

        line = verseKJVx
        for difference in differences:
            search = r"(\w+) (" + difference + ") "
            replace = "<span style='color: red'>\\1</span> \\2 "
            line = re.sub(search, replace, line)

        self.delete_verse(book_num, chapter, verse)
        self.insert_verse(book_num, chapter, verse, line)

        if differences:
            print(book + ' ' + str(chapter) + ':' + str(verse))
            print(differences)
            return True
        else:
            return False

    def get_list_differences(self, list1, list2):
        return list(set(list1) - set(list2))

    def remove_known_from_list(self, list):
        to_remove = LanguageData.commonGreek + LanguageData.commonHebrew
        for item in to_remove:
            if item[0] in list:
                list.remove(item[0])

    def process_chapter(self, book, chapter):
        book_num = self.get_book_num(book)
        verses = BibleData.verses[book_num][int(chapter)]
        for verse in range(1, verses + 1):
            self.process_verse(book, chapter, verse)

    def process_book(self, book, start=1):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        error_found = False
        for chapter in range(int(start), chapters + 1):
            print("Processing " + str(book) + " " + str(chapter))
            verses = BibleData.verses[book_num][int(chapter)]
            for verse in range(1, verses + 1):
                error_found = self.process_verse(book, chapter, verse) | error_found
            if error_found:
                return


if len(sys.argv) < 3:
    print("Usage: book chapter (verse)")
    print("Books:")
    print("all, matthew, mark, luke, john, acts, romans, 1corinthians, 2corinthians")
    print("galatians, ephesians, philippians, colossians")
    print("1thessalonians, 2thessalonians, 1timothy, 2timothy, titus, philemon, hebrews")
    print("james, 1peter, 2peter, 1john, 2john, 3john, jude, revelation")
    print("genesis, exodus, leviticus, numbers, deuteronomy")
    print("joshua, judges, ruth, 1samuel, 2samuel, 1kings, 1kings, 1chronicles, 2chronicles")
    print("ezra, nehemiah, esther, job, psalms, proverbs, ecclesiastes, solomon")
    print("isaiah, jeremiah, lamentations, ezekiel, daniel, hosea")
    print("joel, amos, obadiah, jonah, micah, nahum")
    print("habakkuk, zephaniah, haggai, zechariah, malachi")
    exit(1)

proofread = HighlightMissing()
book = sys.argv[1]
bookfile = book.lower()

proofread.create_tables()

if len(sys.argv) == 2:
    if book in ("nt", "all"):
        for book in BibleData.books:
            if BibleData.books[book] >= 40:
                proofread.process_book(book)
    else:
        proofread.process_book(bookfile)
elif len(sys.argv) == 3:
    chapter = sys.argv[2]
    proofread.process_book(bookfile, chapter)

print("Done")
