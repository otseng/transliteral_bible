FILE="/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json"
if [[ ! -f "$FILE" ]]; then
    FILE=~/.config/Code/User/snippets/markdown.json
fi

fgrep -o '[menō](../../strongs/g/g3306.md)' "$FILE" > /dev/null
if [[ $? == 0 ]] ; then
    echo "Saving Greek"
    cp "$FILE" `dirname "$0"`/../setup/markdown_greek.json
else
    echo "It's not Greek"
fi