#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import re
import sys
import os
import html
import webbrowser
from urllib import request
import glob

if len(sys.argv) < 2:
    print("Please specify language (greek, hebrew)")
    exit(1)

language = sys.argv[1]

if (language == "greek"):
    filesearch = "../strongs/g/*.md"
elif (language == "hebrew"):
    filesearch = "../strongs/h/*.md"
else:
    print("Unknown language: " + language)
    exit(1)

OCCURS = "Occurs "
PART_OF_SPEECH = "Part of speech: "

break_count = 5000

count = 0
for file in glob.glob(filesearch):
    strongs = re.findall(r'/strongs/.+/(.*)\.md', file)[0]
    filecontents = open(file).read()
    if PART_OF_SPEECH in filecontents:
        print(file + " already done")
    else:
            index = filecontents.find(OCCURS)

            url = "https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=" + strongs
            response = request.urlopen(url)
            pagedata = response.read().decode("utf-8")
            
            re_speech = re.compile('Part of Speech</div>[\s\S]*<div class="small-text-right">(.*)</div>')
            g_speech = re_speech.search(pagedata)
            s_speech = g_speech.group(1).strip()

            newfilecontents = filecontents[:index] + "Part of speech: " + s_speech + "\n\n" + filecontents[index:]

            myfile = open(file, "w")
            myfile.writelines(newfilecontents)
            myfile.close()

            print(file + " updated")

    count = count + 1
    if count >= break_count:
        exit()