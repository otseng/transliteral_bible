FILE='/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json'
if [[ -f $FILE ]]; then
    echo "Using Mac"
    FILE="/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json"
else
    echo "Using Linux"
    FILE=~/.config/Code/User/snippets/markdown.json
fi

fgrep -o '[chay](../../strongs/h/h2416.md)' "$FILE" > /dev/null
if [[ $? == 0 ]] ; then
    echo "Markdown is already Hebrew"
    `dirname "$0"`/save_hebrew_markdown_snippets.sh
else
    `dirname "$0"`/save_greek_markdown_snippets.sh
    cp `dirname "$0"`/../setup/markdown_hebrew.json "$FILE"
    echo "Using Hebrew"
fi