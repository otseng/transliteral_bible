import os, sqlite3, re, glob, platform
from pathlib import Path
import sys
from BibleData import BibleData

class CompareBibles:

    def __init__(self, bible1, bible2):
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'


        self.BibleFirst = self.home + '/UniqueBible/marvelData/bibles/' + bible1 + '.bible'
        self.BibleSecond = self.home + '/UniqueBible/marvelData/bibles/' + bible2 + '.bible'

        if not os.path.exists(self.BibleFirst):
            print("Cannot find " + self.BibleFirst)
            exit(1)
        if not os.path.exists(self.BibleSecond):
            print("Cannot find " + self.BibleSecond)
            exit(1)

        self.connectionBibleFirst = sqlite3.connect(self.BibleFirst)
        self.cursorBibleFirst = self.connectionBibleFirst.cursor()

        self.connectionBibleSecond = sqlite3.connect(self.BibleSecond)
        self.cursorBibleSecond = self.connectionBibleSecond.cursor()

    def __del__(self):
        self.connectionBibleFirst.close()
        self.connectionBibleSecond.close()

    def get_book_num(self, book):
        if not book in BibleData.books:
            print("Cannot find book: " + book)
            exit(1) 
        book_num = BibleData.books[book]
        return book_num

    def read_verse(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]

    def process_verse(self, book, chapter, verse):
        book_num = self.get_book_num(book)
        verseBibleSecond = self.read_verse(self.cursorBibleSecond, book_num, chapter, verse)
        verseBibleFirst = self.read_verse(self.cursorBibleFirst, book_num, chapter, verse)
        
        if book_num < 40:
            regex = r'H\d+' # Hebrew
        else:
            regex = r'G\d+' # Greek
        strongsBibleFirst = re.findall(regex, verseBibleFirst)
        strongsBibleSecond = re.findall(regex, verseBibleSecond)

        # Find items in BibleFirst that are not in BibleSecond
        i = 0
        error = []
        for strong in strongsBibleFirst:
            # print(strong + " " + strongsBibleSecond[i])
            while True:
                if i >= len(strongsBibleSecond):
                    error.append(strong)
                    break
                if strongsBibleSecond[i] == strong:
                    i = i + 1
                    break
                i = i + 1
        if error:
            print(book + ' ' + str(chapter) + ':' + str(verse))
            print(error)
            return True
        return False

    def process_chapter(self, book, chapter):
        book_num = self.get_book_num(book)
        verses = BibleData.verses[book_num][int(chapter)]
        for verse in range(1, verses + 1):
            self.process_verse(book, chapter, verse)

    def process_book(self, book, start=1):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        error_found = False
        for chapter in range(int(start), chapters + 1):
            print("Processing " + str(book) + " " + str(chapter))
            verses = BibleData.verses[book_num][int(chapter)]
            for verse in range(1, verses + 1):
                error_found = self.process_verse(book, chapter, verse) | error_found
            if error_found:
                return


if len(sys.argv) < 3:
    # print("Usage: KJVx MacKJVx book (chapter) (verse)")
    exit(1)

bible1 = sys.argv[1]
bible2 = sys.argv[2]
compare = CompareBibles(bible1, bible2)

bookfile = sys.argv[3].lower()

if len(sys.argv) == 4:
    compare.process_book(bookfile)
elif len(sys.argv) == 5:
    chapter = sys.argv[4]
    compare.process_book(bookfile, chapter)
elif len(sys.argv) == 5:
    chapter = sys.argv[4]
    verse = sys.argv[5]
    print("Processing " + bookfile + " " + chapter + ":" + verse)
    compare.process_verse(bookfile, chapter, verse)

print("Done")
