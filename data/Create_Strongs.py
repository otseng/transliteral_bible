#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import re
import sys
import os
import html
import traceback
import webbrowser
import platform
from urllib import request
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

class GenerateStrongs:

    @staticmethod
    def create_strongs(strongs, first, number):

        open_browser = False
        copy_clipboard = False
        pagedata = ""
        
        file = "../strongs/" + first + "/" + first + number + ".md"
        url = "https://www.blueletterbible.org/lexicon/" + strongs
        try:
            response = request.urlopen(url)
            pagedata = response.read().decode("utf-8")
            re_transliteral = re.compile('<title>.* - (.*) - .*</title>')
            s_transliteral = html.unescape(re_transliteral.search(pagedata).group(1))

            print("Creating: " + file)

            if first == 'g':
                re_original = re.compile('lexTitleGk.*>(.*)<')
                s_original = re_original.search(pagedata).group(1)
            elif first == 'h':
                re_original = re.compile('lexTitleHb.*>(.*)<')
                s_original = re_original.search(pagedata).group(1)

            re_count = re.compile('which occurs (.*) times in .*>(.*)<')
            g_count = re_count.search(pagedata)
            s_count1 = g_count.group(1)
            s_count2 = g_count.group(2)

            re_speech = re.compile('Part of Speech</div>[\s\S]*<div .* class="small-text-right">(.*)</div>')
            g_speech = re_speech.search(pagedata)
            s_speech = "?"
            if g_speech:
                s_speech = g_speech.group(1).strip()

            re_definition = re.compile('following manner:(.*)</div>')
            g_definition = re_definition.search(pagedata)
            s_definition = g_definition.group(1).strip()
            s_definition = remove_tags(s_definition)
            s_definition = s_definition.replace('&nbsp;', ' ')

            outfile = open(file, "w")

            checks = ["proper masculine noun", "proper feminine noun", "proper locative noun"]
            if any(check in pagedata for check in checks):
                s_transliteral = s_transliteral.title()

            outfile.write("# " + s_original + "\n\n")
            outfile.write("[" + s_transliteral + "](" + url + ")\n\n")
            outfile.write("Definition:" + s_definition + "\n\n")
            outfile.write("Part of speech: " + s_speech + "\n\n")
            outfile.write("Occurs " + s_count1 + " times in " + s_count2 + " verses\n\n")
            # if first == 'g':
            #     outfile.write("Derived words: \n\n")
            outfile.write("## Articles\n\n")

            if first == 'g':
                outfile.write("[Blue Letter Bible](https://www.blueletterbible.org/lexicon/g" + number + ")\n\n")
                outfile.write("[Study Light](https://www.studylight.org/lexicons/greek/" + number + ".html)\n\n")
                outfile.write("[Bible Hub](https://biblehub.com/str/greek/" + number + ".htm)\n\n")
                outfile.write("[LSJ](https://lsj.gr/wiki/" + s_original + ")\n\n")
                outfile.write("[NET Bible](http://classic.net.bible.org/strong.php?id=" + number + ")\n\n")
                outfile.write("[Bible Bento](https://biblebento.com/dictionary/G" + number + ".html)\n\n")
                outfile.write("[Bible Study Company](https://biblestudycompany.com/reader/strongs/" + number + "/greek)\n\n")
                outfile.write("[Lexicon Concordance](http://lexiconcordance.com/greek/" + number + ".html)\n\n")
                outfile.write("[New Jerusalem](http://www.newjerusalem.org/Strongs/g" + number + ")\n\n")
            if first == 'h':
                outfile.write("[Blue Letter Bible](https://www.blueletterbible.org/lexicon/h" + number + ")\n\n")
                outfile.write("[Study Light](https://www.studylight.org/lexicons/hebrew/" + number + ".html)\n\n")
                outfile.write("[Bible Hub](https://biblehub.com/str/hebrew/" + number + ".htm)\n\n")
                # outfile.write("[Morfix]()\n\n")
                outfile.write("[NET Bible](http://classic.net.bible.org/strong.php?id=0" + number + ")\n\n")
                outfile.write("[Bible Bento](https://biblebento.com/dictionary/H" + number + ".html)\n\n")
                outfile.write("[Bible Study Company](https://biblestudycompany.com/reader/strongs/" + number + "/hebrew)\n\n")
                outfile.write("[Lexicon Concordance](http://lexiconcordance.com/hebrew/" + number + ".html)\n\n")
                outfile.write("[New Jerusalem](http://www.newjerusalem.org/Strongs/h" + number + ")\n")
            # if first == 'g':
            #     outfile.write("[Wiktionary]()\n\n")
            #     outfile.write("[Wikipedia]()\n\n")
            #     outfile.write("[Sermon Index]()\n\n")
            #     outfile.write("[Precept Austin]()\n")

            outfile.close()

            # https://www.url-encode-decode.com/
            encoded = s_transliteral.replace("ō", "%C5%8D")
            encoded = s_transliteral.replace("ē", "%C4%93")

            # if first == 'g':
            #     webbrowser.open('https://www.google.com/search?q=' + encoded, new=2)
            #     webbrowser.open('https://www.google.com/search?q=' + encoded + '+site:+www.sermonindex.net', new=2)
            #     webbrowser.open('https://www.google.com/search?q=site:+www.preceptaustin.org+' + encoded, new=2)

            # if first == 'h':
            #     webbrowser.open('https://www.morfix.co.il/en/', new=2)

            # webbrowser.open('https://en.wiktionary.org/wiki/Special:Search?search=' + encoded, new=2)
            if open_browser:
                webbrowser.open(url, new=2)

            s_transliteral = s_transliteral.replace('`', "'")
            output = "[" + s_transliteral + "](../" + file + ")"
            if copy_clipboard:
                if platform.system() == "Linux":
                    os.system("/bin/echo -n \"" + output + "\" | xclip -selection clipboard")
                else:
                    os.system("/bin/echo -n \"" + output + "\" | pbcopy")
            print(output)
            return s_transliteral
        except Exception as ex:
            print(ex)
            print(traceback.format_exc())
            print("Could not open " + url)
            return False
    

def remove_tags(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

if __name__ == "__main__":

    overwrite = False

    type = 'bible'

    if len(sys.argv) == 2:
        # "lexicon", "l", "lex", "short", "s" to remove ../strongs/ from output
        type = sys.argv[1]

    while True:
        strongs = input(f"Number ({type}): ")

        strongs = strongs.replace("[", "")
        strongs = strongs.replace("]", "")
        
        if (strongs in ("q", "", "quit", "exit")):
            exit(0)

        strongs = strongs.lower()
        results = re.findall(r'\d+', strongs)
        if results:
            number = results[0]
        else:
            print("Number not found")
            continue

        hebrew = False
        greek = False

        first = strongs[0]
        if (first in ('h', '+')):
            hebrew = True
            first = 'h'
        elif (first == 'g'):
            greek = True

        if not hebrew and not greek:
            first = 'g'
            greek = True

        file = "../strongs/" + first + "/" + first + number + ".md"

        if os.path.isfile(file) and not overwrite:
            with open(file, 'r') as myfile:
                filedata = myfile.read()
            re_transliteral = re.compile(r'\[(.*)\]\(https:\/\/www.blueletterbible.org')
            s_transliteral = html.unescape(re_transliteral.search(filedata).group(1))
            s_transliteral = s_transliteral.replace('`', "'")
            output = "[" + s_transliteral + "](../" + file + ")"

            if type in ("lexicon", "l", "lex", "short", "s"):
                output = output.replace("../strongs/", "")
            
            if platform.system() == "Linux":
                os.system("/bin/echo -n \"" + output + "\" | xclip -selection clipboard")
            else:
                os.system("/bin/echo -n \"" + output + "\" | pbcopy")
            print("Exists: " + file)
            print(output)
        else:
            GenerateStrongs.create_strongs(strongs, first, number)

