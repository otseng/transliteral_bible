import html
import os, sqlite3, re, platform
import sys
from TextReplaceData import TextReplaceData
from LanguageData import LanguageData
from BibleData import BibleData
from Create_Strongs import GenerateStrongs

class GenerateTransliteration:

    def __init__(self):

        # TODO: implement correct coding for ABPenx.bible
        self.ABP = HOME + '/UniqueBible/marvelData/bibles/ABPenx.bible'
        self.connectionABP = sqlite3.connect(self.ABP)
        self.cursorABP = self.connectionABP.cursor()
        self.output = ""
        
    def __del__(self):
        self.connectionABP.close()

    def get_book_num(self, book):
        if not book in BibleData.books:
            print("Cannot find book: " + book)
            exit(1) 
        book_num = BibleData.books[book]
        return book_num

    def read_verse(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]

    def process_verse(self, book, chapter, verse):

        self.output = self.output + "<a name=\"" + book + "_" + str(chapter) + "_" + str(verse) + "\"></a>" + book.title() + " " + str(chapter) + ":" + str(verse) + "\n\n"

        book_num = self.get_book_num(book)
        verseABP = self.read_verse(self.cursorABP, book_num, chapter, verse)
        
        # Fix Strongs after punctuation
        regex = r'([.,;:!?])[ ]?([GH]\d+)'
        text = re.sub(regex, ' \\2 \\1 ', verseABP)

        # Remove superscript
        regex = r'[¹²³⁴⁵⁶⁷⁸⁹⁰]'
        text = re.sub(regex, '', text)

        regex = r'(^|\s)([a-zA-Z\']*) ([GH]\d+)'
        text = re.sub(regex, replace_words, text)

        regex = r'(^|\s)([GH]\d+)'
        text = re.sub(regex, replace_strongs, text)

        text = re.sub(r'^ ', '', text)

        replace_text = TextReplaceData.hebrew
        ignore_strongs = LanguageData.commonGreek + LanguageData.commonHebrew

        for key in replace_text.keys():
            text = text.replace(key, replace_text[key])

        text = text.replace("\n", " ")
        text = text.replace("  ", " ")
        text = text.replace("  ", " ")

        self.output = self.output + text + "\n\n"

    def process_chapter(self, book, chapter):

        self.output = self.output + "# [" + book.title() + " " + chapter + "](" + "https://www.blueletterbible.org/lxx/" + book + "/" + chapter + ")\n\n"

        book_num = self.get_book_num(book)
        verses = BibleData.verses[book_num][int(chapter)]
        for verse in range(1, verses + 1):
            self.process_verse(book, chapter, verse)

        self.output = self.output + "---\n\n"
        self.output = self.output + "[Transliteral Bible](../bible.md)\n\n"
        self.output = self.output + "[" + book.title() + "](" + bookfile.lower() + "_lxx.md)\n\n"
        if int(chapter) > 1:
            self.output = self.output + "[" + book.title() + " " + str(int(chapter) - 1) + "](" + bookfile.lower() + "_lxx_" + str(int(chapter) - 1) + ".md)"
        chapters = BibleData.chapters[book_num]
        if int(chapter) < chapters:
            if int(chapter) > 1:
                self.output = self.output + " - "
            self.output = self.output + "[" + book.title() + " " + str(int(chapter) + 1) + "](" + bookfile.lower() + "_lxx_" + str(int(chapter) + 1) + ".md)"

        directory = "../bible/" + bookfile.lower()

        filename = directory + "/" + bookfile + "_lxx_" + chapter + ".md"
        if not os.path.isfile(filename) or self.overwrite:
            outfile = open(filename, "w")
            outfile.write(self.output)
            outfile.close()

            # self.output = self.output.replace('"', '\\"')
                                            
            # if platform.system() == "Linux":
            #     os.system("/bin/echo -n \"" + self.output + "\" | xclip -selection clipboard")
            # else:
            #     os.system("/bin/echo -n \"" + self.output + "\" | pbcopy")
            # print(self.output)
            print("File created: " + filename)
        else:
            print("File already exists: " + filename)
    
    def create_index(self, bookfile):
        directory = "../bible/" + bookfile.lower()
        filename = directory + "/" + bookfile.lower() + "_lxx.md"
        if not os.path.isfile(filename):
            outfile = open(filename, "w")
            outfile.write("# " + bookfile.title() + "\n\n")
            outfile.write("[" + bookfile.title() + " Overview](../../commentary/" + bookfile.lower() + 
                        "/" + bookfile.lower() + "_overview.md)\n\n")
            book_num = self.get_book_num(bookfile)
            chapters = BibleData.chapters[book_num]
            for chapter in range(1, chapters + 1):
                outfile.write("[" + bookfile.title() + " " + str(chapter) + "](" + bookfile.lower() + "_lxx_" + str(chapter) + ".md)\n\n")
            outfile.write("---\n\n")
            outfile.write("[Transliteral Bible](../bible.md)")
            outfile.close()


HOME = '/Users/otseng/'
if platform.system() == "Linux":
    HOME = '/home/oliver/'
DEV = HOME + 'dev/'

ignore_strongs = []
for item in LanguageData.commonGreek + LanguageData.commonHebrew:
    ignore_strongs.append(item[0])

def replace_words(match):
        word = match.group(2)
        strongs = match.group(3)
        return replace(word, strongs)

def replace_strongs(match):
        word = ''
        strongs = match.group(2)
        return replace(word, strongs)

def replace(word, strongs):
    if strongs in ignore_strongs:
        # print("Ignoring common word: " + word + " " + strongs)
        return " " + word
    strongs = strongs.lower()
    first = strongs[0].lower()
    number = strongs[1:]
    TRLIT_DIR = DEV + "transliteral_bible/"
    file = TRLIT_DIR + "strongs/" + first + "/" + first + number + ".md"
    if os.path.isfile(file):
        with open(file, 'r') as myfile:
            filedata = myfile.read()
        re_transliteral = re.compile(r'\[(.*)\]\(https:\/\/www.blueletterbible.org')
        s_transliteral = html.unescape(re_transliteral.search(filedata).group(1))
        s_transliteral = s_transliteral.replace('`', "'")
        text = "[" + s_transliteral + "](../../" + file + ")"
        text = text.replace(TRLIT_DIR, "")
        text = text.replace("//", "/../")
        return " " + text
    else:
        word = GenerateStrongs.create_strongs(strongs, first, number)
        if word:
            return " [" + word + "](../../strongs/h/" + strongs + ".md) "
        else:
            print("Error creating strongs: " + strongs)
            return " " + strongs

if len(sys.argv) < 3:
    print("Usage: book chapter (verse)")
    print("Books:")
    print("all, matthew, mark, luke, john, acts, romans, 1corinthians, 2corinthians")
    print("galatians, ephesians, philippians, colossians")
    print("1thessalonians, 2thessalonians, 1timothy, 2timothy, titus, philemon, hebrews")
    print("james, 1peter, 2peter, 1john, 2john, 3john, jude, revelation")
    print("genesis, exodus, leviticus, numbers, deuteronomy")
    print("joshua, judges, ruth, 1samuel, 2samuel, 1kings, 1kings, 1chronicles, 2chronicles")
    print("ezra, nehemiah, esther, job, psalms, proverbs, ecclesiastes, song")
    print("isaiah, jeremiah, lamentations, ezekiel, daniel, hosea")
    print("joel, amos, obadiah, jonah, micah, nahum")
    print("habakkuk, zephaniah, haggai, zechariah, malachi")
    exit(1)

book = sys.argv[1]
bookfile = book.lower()

generate = GenerateTransliteration()
generate.overwrite = False

if len(sys.argv) == 3:
    chapter = sys.argv[2]
    generate.create_index(bookfile)
    generate.process_chapter(bookfile, chapter)

elif len(sys.argv) == 4:
    chapter = sys.argv[2]
    verse = sys.argv[3]
    generate.process_verse(bookfile, chapter, verse)

