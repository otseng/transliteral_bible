FILE='/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json'
if [[ -f $FILE ]]; then
    echo "Using Mac"
    FILE="/Users/otseng/Library/Application Support/Code/User/snippets/markdown.json"
else
    echo "Using Linux"
    FILE=~/.config/Code/User/snippets/markdown.json
fi

if [[ -f "$FILE" ]]; then
    fgrep -o '[menō](../../strongs/g/g3306.md)' "$FILE" > /dev/null
    # echo fgrep -o '[menō](../../strongs/g/g3306.md)' '$FILE'
    if [[ $? == 0 ]] ; then
        echo "Markdown is already Greek"
        `dirname "$0"`/save_greek_markdown_snippets.sh
    else
        `dirname "$0"`/save_hebrew_markdown_snippets.sh
        cp `dirname "$0"`/../setup/markdown_greek.json "$FILE"
        echo "Using Greek"
    fi
else
    echo "File does not exist - $FILE"
fi