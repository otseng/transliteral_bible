import html
import os, sqlite3, re, platform
import sys
from TextReplaceData import TextReplaceData
from LanguageData import LanguageData
from BibleData import BibleData
from Create_Strongs import GenerateStrongs

class GenerateTransliteration:

    def __init__(self):

        self.KJVx = HOME + '/UniqueBible/marvelData/bibles/KJVx.bible'
        self.connectionKJVx = sqlite3.connect(self.KJVx)
        self.cursorKJVx = self.connectionKJVx.cursor()
        self.output = ""
        
    def __del__(self):
        self.connectionKJVx.close()

    def get_book_num(self, book):
        if not book in BibleData.books:
            print("Cannot find book: " + book)
            exit(1) 
        book_num = BibleData.books[book]
        return book_num

    def read_verse(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]

    def process_verse(self, book, chapter, verse):

        self.output = self.output + "<a name=\"" + book + "_" + str(chapter) + "_" + str(verse) + "\"></a>" + book.title() + " " + str(chapter) + ":" + str(verse) + "\n\n"

        book_num = self.get_book_num(book)
        verseKJVx = self.read_verse(self.cursorKJVx, book_num, chapter, verse)
        
        regex = r'(^|\s)([a-zA-Z\']*) ([GH]\d+)'
        text = re.sub(regex, replace_words, verseKJVx)

        regex = r'(^|\s)([GH]\d+)'
        text = re.sub(regex, replace_strongs, text)

        text = re.sub(r'^ ', '', text)

        replace_text = TextReplaceData.hebrew
        ignore_strongs = LanguageData.commonGreek + LanguageData.commonHebrew

        for key in replace_text.keys():
            text = text.replace(key, replace_text[key])

        text = text.replace("  ", " ")

        self.output = self.output + text + "\n\n"

    def create_overview(self, bookfile):
        directory = "../bible/" + bookfile.lower()
        if not os.path.exists(directory):
            os.makedirs(directory)

        filename = directory + "/" + bookfile + ".md"
        uBook = bookfile.title()
        uBook = uBook.replace("1", "1 ")
        uBook = uBook.replace("2", "2 ")
        uBook = uBook.replace("3", "3 ")
        lBook = bookfile.lower()
        outfile = open(filename, "w")

        chapter = BibleData.chapters[BibleData.books[bookfile]]

        output = "# " + uBook + "\n\n" 

        output = output + "[" + uBook + " Overview](../../commentary/" + lBook + "/" + lBook + "_overview.md)\n\n"

        for x in range(1, chapter + 1):
            output = output + "[" + uBook + " " + str(x) + "](" + lBook + "_" + str(x) +".md)\n"
            output = output + "\n"

        output = output + "---\n"
        output = output + "\n"
        output = output + "[Transliteral Bible](../index.md)"

        outfile.write(output)


    def process_chapter(self, book, chapter):

        # [Psalms 80](https://www.blueletterbible.org/kjv/psa/80)
        self.output = self.output + "# [" + book.title() + " " + chapter + "](" + "https://www.blueletterbible.org/kjv/" + book + "/" + chapter + ")\n\n"

        book_num = self.get_book_num(book)
        verses = BibleData.verses[book_num][int(chapter)]
        for verse in range(1, verses + 1):
            self.process_verse(book, chapter, verse)

        self.output = self.output + "---\n\n"
        self.output = self.output + "[Transliteral Bible](../bible.md)\n\n"
        self.output = self.output + "[" + book.title() + "](" + bookfile.lower() + ".md)\n\n"
        if int(chapter) > 1:
            self.output = self.output + "[" + book.title() + " " + str(int(chapter) - 1) + "](" + bookfile.lower() + "_" + str(int(chapter) - 1) + ".md)"
        chapters = BibleData.chapters[book_num]
        if int(chapter) < chapters:
            if int(chapter) > 1:
                self.output = self.output + " - " 
            self.output = self.output + "[" + book.title() + " " + str(int(chapter) + 1) + "](" + bookfile.lower() + "_" + str(int(chapter) + 1) + ".md)"

        directory = "../bible/" + bookfile.lower()

        filename = directory + "/" + bookfile + "_" + chapter + ".md"
        if not os.path.isfile(filename) or self.overwrite:
            outfile = open(filename, "w")
            outfile.write(self.output)
            outfile.close()

            proofread.output = proofread.output.replace('"', '\\"')
                                            
            if platform.system() == "Linux":
                os.system("/bin/echo -n \"" + proofread.output + "\" | xclip -selection clipboard")
            else:
                os.system("/bin/echo -n \"" + proofread.output + "\" | pbcopy")
            # print(proofread.output)
            print("File created: " + filename)
        else:
            print("File already exists: " + filename)

HOME = '/Users/otseng/'
if platform.system() == "Linux":
    HOME = '/home/oliver/'
DEV = HOME + 'dev/'

ignore_strongs = []
for item in LanguageData.commonGreek + LanguageData.commonHebrew:
    ignore_strongs.append(item[0])

def replace_words(match):
        word = match.group(2)
        strongs = match.group(3)
        return replace(word, strongs)

def replace_strongs(match):
        word = ''
        strongs = match.group(2)
        return replace(word, strongs)

def replace(word, strongs):
    if strongs in ignore_strongs:
        # print("Ignoring common word: " + word + " " + strongs)
        return " " + word
    strongs = strongs.lower()
    first = strongs[0].lower()
    number = strongs[1:]
    TRLIT_DIR = DEV + "transliteral_bible/"
    file = TRLIT_DIR + "strongs/" + first + "/" + first + number + ".md"
    if os.path.isfile(file):
        with open(file, 'r') as myfile:
            filedata = myfile.read()
        re_transliteral = re.compile(r'\[(.*)\]\(https:\/\/www.blueletterbible.org')
        s_transliteral = html.unescape(re_transliteral.search(filedata).group(1))
        s_transliteral = s_transliteral.replace('`', "'")
        text = "[" + s_transliteral + "](../../" + file + ")"
        text = text.replace(TRLIT_DIR, "")
        text = text.replace("//", "/../")
        return " " + text
    else:
        word = GenerateStrongs.create_strongs(strongs, first, number)
        if word:
            return " [" + word + "](../../strongs/h/" + strongs + ".md) "
        else:
            print("Error creating strongs: " + strongs)
            return " " + strongs

if len(sys.argv) < 3:
    print("Usage: book chapter (verse)")
    print("Books:")
    print("all, matthew, mark, luke, john, acts, romans, 1corinthians, 2corinthians")
    print("galatians, ephesians, philippians, colossians")
    print("1thessalonians, 2thessalonians, 1timothy, 2timothy, titus, philemon, hebrews")
    print("james, 1peter, 2peter, 1john, 2john, 3john, jude, revelation")
    print("genesis, exodus, leviticus, numbers, deuteronomy")
    print("joshua, judges, ruth, 1samuel, 2samuel, 1kings, 1kings, 1chronicles, 2chronicles")
    print("ezra, nehemiah, esther, job, psalms, proverbs, ecclesiastes, song")
    print("isaiah, jeremiah, lamentations, ezekiel, daniel, hosea")
    print("joel, amos, obadiah, jonah, micah, nahum")
    print("habakkuk, zephaniah, haggai, zechariah, malachi")
    exit(1)

book = sys.argv[1]
bookfile = book.lower()

proofread = GenerateTransliteration()
proofread.overwrite = False

if len(sys.argv) == 3:
    chapter = sys.argv[2]
    proofread.create_overview(bookfile)
    proofread.process_chapter(bookfile, chapter)
elif len(sys.argv) == 4:
    chapter = sys.argv[2]
    verse = sys.argv[3]
    proofread.process_verse(bookfile, chapter, verse)

