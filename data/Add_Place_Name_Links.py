import glob
import re
import sys
from urllib import request

max_change = 3000
max_total_count = 20000

def getLineToAdd(site, name):
    uName = name.title()
    lName = name.lower()
    letter = uName[0]
    if site == "biblicalcyclopedia" or site == "b":
        index = f'https://www.biblicalcyclopedia.com/{letter}'
        url = index + f'/{lName}.html'
        line = f'[Biblical Cyclopedia]({url})'
    elif site == "videobible" or site == "v":
        index = 'https://www.videobible.com/bible-dictionary'
        url = index + f'/{lName}'
        line = f'[Video Bible]({url})'
    try:
        response = request.urlopen(url)
        return line
    except:
        return False

if len(sys.argv) < 3:
    print("Usage: [biblicalcyclopedia|b|videobible|v] [place|man}woman]")
    exit(1)

site = sys.argv[1]
type = sys.argv[2]

filesearch = "../strongs/*/*.md"

total_count = 0
change = 0
for file in glob.glob(filesearch):
    filecontents = open(file).read()
    search_string = "XXXXX"
    if type == "man":
        search_string = "proper masculine noun"
    elif type == "woman":
        search_string = "proper feminine noun"
    elif type == "place":
        search_string = "proper locative noun"
    else:
        print("Invalid type")
        exit(1)
    if search_string not in filecontents:
        continue
    if site == "biblicalcyclopedia" or site == "b":
        if "biblicalcyclopedia.com" in filecontents:
            continue
    elif site == "videobible" or site == "v":
        if "videobible.com" in filecontents:
            continue
    name = ""
    line = ""
    try:
        res = re.search('Definition: (.*?) ', filecontents).groups()
        name = res[0]
        line = getLineToAdd(site, name)
    except:
        print(f"Error: {file}")
        continue
    if line:
        newfilecontents = filecontents
        if newfilecontents[-3:] == "\n\n\n":
            newfilecontents = newfilecontents[:-1]
        elif newfilecontents[-2:] == "\n\n":
            newfilecontents = newfilecontents + ""
        elif newfilecontents[-1:] == "\n":
            newfilecontents = newfilecontents + "\n"
        else:
            newfilecontents = newfilecontents + "\n\n"
        newfilecontents = newfilecontents + line
        myfile = open(file, "w")
        myfile.writelines(newfilecontents)
        myfile.close()
        print(f"Added {name} to {file}")
        change = change + 1
        if change >= max_change:
            break
    total_count = total_count + 1
    if total_count >= max_total_count:
        break
