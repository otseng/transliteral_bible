
import os, sqlite3, re, platform
import sys
import markdown2
from common.BibleData import BibleData

class CreateBook:

    def __init__(self):
        self.home = '/Users/otseng'
        if platform.system() == "Linux":
            self.home = '/home/oliver'

    def set_book_name(self, book_name):
        file = self.home + '/UniqueBible/marvelData/books/' + book_name + '.book'
        self.book = file
        self.connection = sqlite3.connect(self.book)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def create_tables(self):
        print('Create table')
        self.cursor.execute('CREATE TABLE Reference (Chapter NVARCHAR(100), Content TEXT)')

    def drop_tables(self):
        print('Drop table')
        self.cursor.execute('DROP TABLE IF EXISTS Reference;')

    def delete_entry(self, chapter):
        self.cursor.execute("DELETE from Reference where Chapter='" + chapter + "'")

    def entry_exists(self, chapter):
        self.cursor.execute("SELECT COUNT(*) from Reference where Chapter='" + chapter + "'")
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_chapter(self, chapter, text):
        try:
            chapter = self.fix_chapter(chapter)
            text = text.replace("'", "''")
            sql = ("INSERT INTO Reference VALUES ('{0}', '{1}')".format(chapter, text))
            self.cursor.execute(sql)
        except Exception as e:
            print(e)
            print(text)

    def fix_chapter(self, chapter):
        if chapter == 'song':
            return "Song of Songs"
        chapter = chapter.replace('1', '1 ')
        chapter = chapter.replace('2', '2 ')
        chapter = chapter.replace('3', '3 ')
        chapter = chapter.title()
        return chapter

    def process_intros(self):
        for book in BibleData.books: 
            print(book, end=' ')
            filename = self.home + '/dev/transliteral_bible/commentary/' + book + '/' + book + '_overview.md'
            if os.path.exists(filename):
                print('!', end=' ')
                contents = open(filename).read()
                html = markdown2.markdown(contents)
                self.insert_chapter(book, html)
            else:
                print('missing', end=' ')
            print("")

    def process_bible_headings(self):
        data = ""
        for book in BibleData.books:
            bookNum = BibleData.books[book] 
            bookName = self.get_book_name(book)
            data += f"<h1>{bookName}</h1>\n"
            for chapter in range(1, BibleData.chapters[bookNum] + 1):
                if BibleData.agbSubheadings.get(".".join([str(bookNum), str(chapter), "1"])):
                    data += f"<b>Chapter {chapter}</b><br>\n<ul>\n"
                    for verse in range(1, BibleData.verses[bookNum][chapter] + 1):
                        heading = BibleData.agbSubheadings.get(".".join([str(bookNum), str(chapter), str(verse)]))
                        if heading:
                            url = f"https://simple.uniquebibleapp.com/bible/KJV-TRLITx/{bookName}/{chapter}#v{chapter}_{verse}"
                            heading = f"<li><a target='_blank' href='{url}'>{heading}</a>"
                            data += heading + "<br>\n"
                    data += "</ul>\n"
            self.insert_chapter(bookName, data)
            data = ""

    def process_multiple_words(self):
        print('multiple_words')
        filename = self.home + '/dev/transliteral_bible/notes/multiple_hebrew_words.md'
        contents = open(filename).read()
        contents = self.convert_link(contents)
        html = markdown2.markdown(contents)
        html = self.add_anchors(html)
        self.insert_chapter("Hebrew", html)
        print(html)
        filename = self.home + '/dev/transliteral_bible/notes/multiple_greek_words.md'
        contents = open(filename).read()
        contents = self.convert_link(contents)
        html = markdown2.markdown(contents)
        html = self.add_anchors(html)
        self.insert_chapter("Greek", html)
        print(html)

    def process_chinese_tutorial(self):
        filename = self.home + '/dev/transliteral_bible/books/chinese_tutorial/chinese_vocabulary.md'
        contents = open(filename).read()
        html = markdown2.markdown(contents, extras=["tables"])
        self.insert_chapter("Vocabulary", html)
        filename = self.home + '/dev/transliteral_bible/books/chinese_tutorial/chinese_idioms.md'
        contents = open(filename).read()
        html = markdown2.markdown(contents, extras=["tables"])
        self.insert_chapter("Idioms", html)

    def process_benedictions(self):
        filename = self.home + '/dev/transliteral_bible/notes/benedictions.md'
        contents = open(filename).read()
        contents = self.convert_link(contents)
        html = markdown2.markdown(contents)
        html = self.add_anchors(html)
        self.insert_chapter("Benedictions", html)

    def convert_link(self, html):
        # [chanan](../strongs/h/h2603.md)
        # <a href="https://www.blueletterbible.org/lexicon/h2603">chanan</a>
        html = re.sub("\[(.+?)\]\(.+?\/[gG](\d+)\.md\)", "<a target='_blank' href=\"https://www.blueletterbible.org/lexicon/G\\2\">\\1</a>", html)
        html = re.sub("\[(.+?)\]\(.+?\/[hH](\d+)\.md\)", "<a target='_blank' href=\"https://www.blueletterbible.org/lexicon/H\\2\">\\1</a>", html)
        # [chanan](../strongs/h/h2603.md)
        # <ref onclick=\"lex('H2603')\">chanan</ref>
        # html = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<ref onclick=\"lex('G\\2')\">\\1</ref>", html)
        # html = re.sub("\[(.+?)\]\(.+?\/[h](\d+)\.md\)", "<ref onclick=\"lex('H\\2')\">\\1</ref>", html)
        return html
    
    def get_book_name(self, book):
        if book[0] in ['1', '2', '3']:
            book_name = book[0] + " " + book[1:].title()
        else:
            book_name = book.title()
        return book_name

    def add_anchors(self, html):
        html = re.sub("<h2>(.*)</h2>", "<a id='\\1' name='\\1'></a>\n<h2>\\1</h2>", html)
        return html
    
    def create_book_overviews(self):
        self.set_book_name('Bible_Book_Overviews')
        self.drop_tables()
        self.create_tables()
        self.process_intros()

    def create_bible_headings(self):
        self.set_book_name('Bible_Headings')
        self.drop_tables()
        self.create_tables()
        self.process_bible_headings()

    def create_multiple_words(self):
        self.set_book_name('Synonyms')
        self.drop_tables()
        self.create_tables()
        self.process_multiple_words()

    def create_chinese_tutorial(self):
        self.set_book_name('Chinese_Tutorial')
        self.drop_tables()
        self.create_tables()
        self.process_chinese_tutorial()

    def create_benedictions(self):
        self.set_book_name('Benedictions')
        self.drop_tables()
        self.create_tables()
        self.process_benedictions()

if len(sys.argv) < 2:
    print("Specify book (bible_overviews, bible_headings, multiple_words, chinese_tutorial, benedictions)")
    exit(1)

book = sys.argv[1].lower()

createbook = CreateBook()
if book == 'bible_overviews':
    createbook.create_book_overviews()
elif book == 'bible_headings':
    createbook.create_bible_headings()
elif book == 'multiple_words':
    createbook.create_multiple_words()
elif book == 'chinese_tutorial':
    createbook.create_chinese_tutorial()
elif book == 'benedictions':
    createbook.create_benedictions()
else:
    print("Unknown book: " + book)
    exit(1)
print("Done")

