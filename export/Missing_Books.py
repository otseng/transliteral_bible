import os, sqlite3, re, glob, platform
from pathlib import Path
import sys

class MissingBooks:

    # BibleBooks.py
    books = {
        "genesis": 1,
        "exodus": 2,
        "leviticus": 3,
        "numbers": 4,
        "deuteronomy": 5,
        "joshua": 6,
        "judges": 7,
        "ruth": 8,
        "1samuel": 9,
        "2samuel": 10,
        "1kings": 11,
        "2kings": 12,
        "1chronicles": 13,
        "2chronicles": 14,
        "ezra": 15,
        "nehemiah": 16,
        "esther": 17,
        "job": 18,
        "psalms": 19,
        "proverbs": 20,
        "ecclesiastes": 21,
        "song": 22,
        "isaiah": 23,
        "jeremiah": 24,
        "lamentations": 25,
        "ezekiel": 26,
        "daniel": 27,
        "hosea": 28,
        "joel": 29,
        "amos": 30,
        "obadiah": 31,
        "jonah": 32,
        "micah": 33,
        "nahum": 34,
        "habakkuk": 35,
        "zephaniah": 36,
        "haggai": 37,
        "zechariah": 38,
        "malachi": 39,

        "matthew": 40,
        "mark": 41,
        "luke": 42,
        "john": 43,
        "acts": 44,
        "romans": 45,
        "1corinthians": 46,
        "2corinthians": 47,
        "galatians": 48,
        "ephesians": 49,
        "philippians": 50,
        "colossians": 51,
        "1thessalonians": 52,
        "2thessalonians": 53,
        "1timothy": 54,
        "2timothy": 55,
        "titus": 56,
        "philemon": 57,
        "hebrews": 58,
        "james": 59,
        "1peter": 60,
        "2peter": 61,
        "1john": 62,
        "2john": 63,
        "3john": 64,
        "jude": 65,
        "revelation": 66
        }

    def __init__(self):
        self.home = '/Users/otseng'
        if platform.system() == "Linux":
            self.home = '/home/oliver'
        self.TrlitBible = self.home + '/UniqueBible/marvelData/bibles/TRLIT.bible'
        self.TrlitxBible = self.home + '/UniqueBible/marvelData/bibles/TRLITx.bible'
        self.connectionTrlit = sqlite3.connect(self.TrlitBible)
        self.connectionTrlitx = sqlite3.connect(self.TrlitxBible)
        self.cursorTrlit = self.connectionTrlit.cursor()
        self.cursorTrlitx = self.connectionTrlitx.cursor()

    def __del__(self):
        self.connectionTrlit.commit()
        self.connectionTrlitx.close()

    def book_exists(self, cursor, book_num):
        cursor.execute('SELECT COUNT(*) from Bible where Book=' + str(book_num))
        count = cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def check_books(self):
        print("Missing books in TRLIT:")
        for book in self.books:
            book_num = self.books[book]
            if not self.book_exists(self.cursorTrlit, book_num):
                print(book)
        print("Missing books in TRLITx:")
        for book in self.books:
            book_num = self.books[book]
            if not self.book_exists(self.cursorTrlitx, book_num):
                print(book)

check = MissingBooks()
check.check_books()

