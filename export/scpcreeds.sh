if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  BASE_DIR="/home/oliver"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  BASE_DIR="/Users/otseng"
fi
scp -P $SERVER_PORT ${BASE_DIR}/UniqueBible/marvelData/books/Creeds.book $SERVER_USER@$SERVER_IP:/home/otseng/UniqueBible/marvelData_enx/books
