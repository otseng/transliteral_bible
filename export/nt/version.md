# [Version history](ch001.xhtml)

0.24.4 - 2/22/2024

* Parallel format with KJV and TRLIT
* Link verses and lexicon entries to Simple Unique Bible Viewer 

0.24.3 - 1/30/2024

* Fix broken lexicon entries

0.24.2 - 1/27/2024

* Fix books of the New Testament list
* Update lexicon format

0.24.1 - 1/25/2024

* Initial release