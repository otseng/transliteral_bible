echo "Creating TransliteralBibleNT.epub file..."

start_time=$(date +%s)

pandoc -o TransliteralBibleNT.epub metadata.yaml TransliteralBible_epub.md

end_time=$(date +%s)
elapsed_time=$((end_time - start_time))

echo "Generation time (secs): $elapsed_time"
