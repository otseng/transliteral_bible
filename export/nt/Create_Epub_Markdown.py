import platform
import re
import glob
import os
import math
from pathlib import Path
import sqlite3
import sys

sys.path.append('../common')

from LexicalData import LexicalData 
from BibleData import BibleData

class MarkdownBible:

    def __init__(self):
        self.remove_footnotes_flag = True
        self.red_letter_flag = False

        self.filename = "TransliteralBible_epub.md"
        self.outfile = open(self.filename, "w")
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'

        bibleKJV = self.home + '/UniqueBible/marvelData/bibles/KJV.bible'
        connectionKJV = sqlite3.connect(bibleKJV)
        self.cursorKJV = connectionKJV.cursor()

    def create_toc(self):
        print('Adding toc')
        self.outfile.write("# Transliteral Bible\n\n")
        self.outfile.write("## Contents\n\n")
        count = 2
        self.outfile.write("* [Preface](" + count_to_chapter(count) + ")\n")
        count = count + 1
        self.nt_books_page_parallel = count
        self.outfile.write("* [New Testament](" + count_to_chapter(count) + ")\n")
        count = count + len(nt_books) + 1
        self.nt_books_page_trlitx = count
        # self.outfile.write("* [New Testament with Strong's](" + count_to_chapter(count) + ")\n")
        # count = count + len(nt_books) + 1
        lexicon_page = count
        self.outfile.write("* [Greek lexicon](" + count_to_chapter(count) + ")\n")
        count = count + 57
        self.outfile.write("* [License](" + count_to_chapter(count) + ")\n")
        count = count + 1
        self.outfile.write("* [Version history](" + count_to_chapter(count) + ")\n")
        self.outfile.write("\n\n")

    def create_preface(self):
        print('Adding preface')
        filename = self.home + '/transliteral_bible/export/common/preface.md'
        file = open(filename, "r")
        lines = file.readlines()
        for line in lines:
            self.outfile.write(line)

    def create_version_info(self):
        print('Adding version info')
        filename = 'version.md'
        file = open(filename, "r")
        lines = file.readlines()
        for line in lines:
            self.outfile.write(line)

    def create_license(self):
        print('Adding license')
        self.outfile.write("\n\n# [License](ch001.xhtml)\n\n")
        filename = self.home + '/transliteral_bible/LICENSE'
        file = open(filename, "r")
        lines = file.readlines()
        for line in lines:
            self.outfile.write(line)
        self.outfile.write("\n\n")

    def create_nt_index(self, title, count):
        self.outfile.write("\n\n# [" + title + "](ch001.xhtml)\n\n")
        for book in nt_books:
            bookName = self.get_book_name(book)
            self.outfile.write("* [" + bookName + "](ch" + str(count).zfill(3) + ".xhtml)\n")
            count = count + 1

    # type = parallel, strongs
    def add_book(self, book, type):
        print('Adding book: ' + book)
        bookName = self.get_book_name(book)
        self.outfile.write("\n\n")
        self.outfile.write("# [" + bookName + "](" + count_to_chapter(self.nt_books_page_parallel) + ")\n\n")
        self.outfile.write("<a name='" + book + "'></a>\n\n")
        bookNum = BibleData.books[book]

        # Chapter index
        for chapter in range(1, BibleData.chapters[bookNum] + 1):
            if type == "strongs":
                count = self.nt_books_page_trlitx
            elif type == "parallel":
                count = self.nt_books_page_parallel

            count = count + bookNum - 40 + 1
            self.outfile.write("* [" + bookName + " " + str(chapter) + "](ch" + str(count).zfill(3) + ".xhtml#" + book + "_" + str(chapter) + ")" + "\n")

        self.outfile.write("\n\n")

        # Chapter text
        for chapter in range(1, BibleData.chapters[bookNum] + 1):
            self.add_chapter(book, bookNum, chapter, type)

    def add_chapter(self, book, bookNum, chapter, type):
        filename = self.home + '/transliteral_bible/bible/' + book + '/' + book + '_' + str(chapter) + '.md'
        bookName = self.get_book_name(book)
        if chapter > 1:
            self.outfile.write("\n\n")

        count = nt_books_chapter_parallel + nt_books.index(book)
        self.outfile.write("## [" + bookName + " " + str(chapter) + "](ch" + str(count).zfill(3) + ".xhtml)\n\n")
        self.outfile.write("<a name='" + book + "_" + str(chapter) + "'></a>")
        if ".".join([str(bookNum), str(chapter), str(1)]) not in BibleData.agbSubheadings:
            self.outfile.write("\n\n")
        file = open(filename, "r")
        lines = file.readlines()
        verse = 0
        for line in lines:
            if line.startswith('---'):
                break
            elif line.startswith('#'):
                pass
            elif line.strip() == '':
                pass
            elif line.startswith('<a name='):
                verse = self.get_verse_number(line)
            else:
                if self.remove_footnotes_flag:
                    line = self.remove_footnotes(line)
                if not self.red_letter_flag:
                    line = re.sub(r"\*\*", "", line)
                line = self.convert_strongs(line)

                if verse > 0:
                    heading = BibleData.agbSubheadings.get(".".join([str(bookNum), str(chapter), str(verse)]))
                    if heading:
                        self.outfile.write("\n\n**" + heading + "**\n\n")
                    elif verse > 1:
                        if type == "strongs" and (bookNum, chapter, verse) in BibleData.asvParagraphs:
                            self.outfile.write("\n\n")
                        elif type == "parallel":
                            self.outfile.write("\n\n")
                    if type == "strongs":
                        self.outfile.write("**" + str(verse) + "** " + line.strip() + " ")
                    elif type == "parallel":
                        ubaUrl = "https://simple.uniquebibleapp.com/bible/KJV-TRLITx/" + self.title_case(book) + "/" + str(chapter) + "#v" +  str(chapter) + "_" + str(verse)
                        ubaUrl = ubaUrl.replace(" ", "%20")
                        self.outfile.write("[" + self.title_case(book) + " " + str(chapter) + ":" + str(verse) + "](" + ubaUrl + ")\n\n")
                        if not quick_compile:
                            kjv_verse = self.read_verse_from_db(self.cursorKJV, bookNum, chapter, verse)
                            self.outfile.write(kjv_verse.strip() + "\n\n")
                        self.outfile.write(line.strip())

    def add_greek_lexicon(self):

        filesearch = "../../strongs/g/*.md"
        files = sorted(glob.glob(filesearch), key=get_order)
        print('Adding ' + str(len(files)) + " Greek lexicons")
        for filename in files:
            lexicon = os.path.basename(filename).split('.')[0]
            number = int(lexicon[1:])
            if number % 100 == 1 or number == 3303:
                self.outfile.write("\n<a name='greek_lexicon'></a>\n\n")
                self.outfile.write("# [Greek lexicon G" + str(number) + " - G" + str(number+99) + "](ch001.xhtml)\n\n")
            file = open(filename, "r")
            lines = file.readlines()
            self.outfile.write("\n<a name='" + lexicon + "'></a>\n")
            ubaUrl = "https://simple.uniquebibleapp.com/lexicon/" + lexicon.upper()
            self.outfile.write("**[" + lexicon.upper() + "](" + ubaUrl + ")**\n\n")
            for line in lines:
                if line.startswith('## Articles') or line.startswith('Hebrew:'):
                    break
                elif line.strip() == '':
                    pass
                elif line.startswith('#'):
                    original_word = line[2:].strip()
                elif "blueletterbible.org" in line and "lexicon" in line:
                    (transliteral, url) = re.search(r"\[(.*?)\]\((.*)\)", line).groups()
                    self.outfile.write("" + transliteral + " - " + original_word + "<br/>\n")
                    # self.outfile.write("[" + transliteral + " - " + original_word + "](" + url + ")<br/>\n")
                else:
                    line = re.sub("\[(.*?)\]\(.*?\)", "\\1", line)
                    line = line.replace("Occurs 1 times in 1 verses", "Occurs 1 time in 1 verse")
                    self.outfile.write(line.strip() + "<br/>\n")

    def title_case(self, line):
        line = line.title()
        line = line.replace("1", "1 ")
        line = line.replace("2", "2 ")
        line = line.replace("3", "3 ")
        return line

    def convert_strongs(self, line):
        # [biblos](../../strongs/g/g976.md)
        line = re.sub("\[(.*?)\]\(.*?/[gh]/(.*?)\.md\)", add_link_to_strongs, line)
        return line
    
    def remove_footnotes(self, line):
        line = re.sub("\[\^\d*?\]", "", line)
        return line
    
    def get_book_name(self, book):
        if book[0] in ['1', '2', '3']:
            book_name = book[0] + " " + book[1:].title()
        else:
            book_name = book.title()
        return book_name

    def get_verse_number(self, line):
        (verse) = re.search(r":(\d*)", line).groups()
        return int(verse[0])
    
    def get_chapter_verse(self, bookName, line):
        (chapter, verse) = re.search(r"</a>" + bookName + " (\d*):(\d*)", line).groups()
        return chapter, verse
    
    def read_verse_from_db(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]
    
    def test_remove_footnotes(self):
        line = "This is a test[^1] of the emergency[^2] broadcast[^3] system[^4]."
        line = self.remove_footnotes(line)
        print(line)
    
    def test_get_book_name(self):
        for book in BibleData.books:
            book_name = self.get_book_name(book)
            print(book_name)

    def test_get_verse_number(self):
        line = '<a name="1corinthians_2_14"></a>1 Corinthians 2:14'
        book = '1corinthians'
        bookName = self.get_book_name(book)
        verse = self.get_verse_number(line)
        print(line)
        print(verse)

    def create(self):

        self.create_toc()

        self.create_preface()

        self.create_nt_index("New Testament", nt_books_chapter_parallel)

        for book in nt_books:
            self.add_book(book, "parallel")

        self.add_greek_lexicon()

        self.create_license()

        self.create_version_info()

def get_order(file):
    file_pattern = re.compile(r'.*?(\d+).*?')
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])

def add_link_to_strongs(m):
    strongs = m.group(2)
    number = int(strongs[1:])
    count = nt_books_chapter_trlitx + len(nt_books) + math.floor((number-1) / 100)
    info = LexicalData.data[strongs.upper()]
    tooltip = strongs
    if info:
        tooltip = info[0] + " - " + info[2]
        tooltip = tooltip.replace("'", "&apos;")
    return m.group(1) + " <sub title='" + tooltip + "'>[" + strongs.upper() + "](" + count_to_chapter(count) + "#" + strongs + ")</sub>"

def count_to_chapter(count):
    return "ch" + str(count).zfill(3) + ".xhtml"

nt_books_chapter_parallel = 4
nt_books_chapter_trlitx = 4

## Change this to True to not include KJV verses
quick_compile = False
# quick_compile = True

nt_books = ['matthew', 'mark', 'luke', 'john', 'acts', 'romans', '1corinthians', '2corinthians', 'galatians',
        'ephesians', 'philippians', 'colossians', '1thessalonians', '2thessalonians', '1timothy', 
        '2timothy', 'titus', 'philemon', 'hebrews', 'james', '1peter', '2peter', 
        '1john', '2john', '3john', 'jude', 'revelation']

markdown_bible = MarkdownBible()
markdown_bible.create()

print("Finished creating " + markdown_bible.filename)
