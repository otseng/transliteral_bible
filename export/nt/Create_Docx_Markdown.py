import platform
import re
import glob
import os
import math
from pathlib import Path 

from LexicalData import LexicalData
from BibleData import BibleData

class MarkdownBible:

    def __init__(self):
        self.remove_footnotes_flag = True
        self.red_letter_flag = False

        self.filename = "TransliteralBible_docx.md"
        self.outfile = open(self.filename, "w")
        self.home = '/Users/otseng/dev'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'

        self.NEW_PAGE = "<div style='page-break-after: always'></div>\n"
        self.DROP_CAP1 = '<w:framePr w:dropCap="drop" w:lines="3" w:wrap="around" w:vAnchor="text" w:hAnchor="text"/>\n'
        self.DROP_CAP = '<w:pPr><w:keepNext/><w:framePr w:dropCap="drop" w:lines="3" w:wrap="around" w:vAnchor="text" w:hAnchor="text"/><w:spacing w:line="878" w:lineRule="exact"/><w:textAlignment w:val="baseline"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:position w:val="-11"/><w:sz w:val="120"/></w:rPr></w:pPr>'

    def create_toc(self):
        print('Adding toc')
        self.outfile.write("# Transliteral Bible\n\n")
        self.outfile.write("## Contents\n\n")
        count = 2
        self.outfile.write("* Preface\n")
        count = count + 1
        self.nt_books_page = count
        self.outfile.write("* Books of the New Testament\n")
        for book in nt_books:
            bookName = self.get_book_name(book)
            self.outfile.write("  * " + bookName + "\n")
        count = count + len(nt_books) + 1
        self.outfile.write("\n* Version\n")
        self.outfile.write("\n\n")

    def create_preface(self):
        print('Adding preface')
        self.outfile.write(self.NEW_PAGE)
        filename = self.home + '/transliteral_bible/export/data/preface.md'
        file = open(filename, "r")
        lines = file.readlines()
        for line in lines:
            line = self.remove_links(line)
            self.outfile.write(line)

    def create_version_info(self):
        print('Adding version info')
        self.outfile.write("\n\n")
        filename = self.home + '/transliteral_bible/export/data/version.md'
        file = open(filename, "r")
        lines = file.readlines()
        for line in lines:
            self.outfile.write(line)

    def add_book(self, book):
        print('Adding book: ' + book)
        bookName = self.get_book_name(book)
        self.outfile.write('\n' + self.NEW_PAGE)
        self.outfile.write("\n# " + bookName + "\n")
        bookNum = BibleData.books[book]

        # Chapter text
        for chapter in range(1, BibleData.chapters[bookNum] + 1):
            self.add_chapter(book, bookNum, chapter)

    def add_chapter(self, book, bookNum, chapter):
        filename = self.home + '/transliteral_bible/bible/' + book + '/' + book + '_' + str(chapter) + '.md'
        bookName = self.get_book_name(book)
        # if chapter > 1:
        #     self.outfile.write("\n\n")
        # self.outfile.write("<a name='" + book + "_" + str(chapter) + "'></a>\n\n")
        self.outfile.write("\n\n## " + bookName + " " + str(chapter) + "\n")

        file = open(filename, "r")
        lines = file.readlines()
        verse = 0
        for line in lines:
            if line.startswith('---'):
                break
            elif line.startswith('#'):
                pass
            elif line.strip() == '':
                pass
            elif line.startswith('<a name='):
                verse = self.get_verse_number(line)
            else:
                if self.remove_footnotes_flag:
                    line = self.remove_footnotes(line)
                if not self.red_letter_flag:
                    line = re.sub(r"\*\*", "", line)
                line = self.convert_strongs(line)

                if verse > 0:
                    if verse > 1 and (bookNum, chapter, verse) in BibleData.asvParagraphs:
                        self.outfile.write("\n\n")
                    heading = BibleData.agbSubheadings.get(".".join([str(bookNum), str(chapter), str(verse)]))
                    if heading:
                        self.outfile.write("\n<p style='text-align:center; vertical-align: top'><strong>" + heading + "</strong></p>\n")
                    # Add drop case for first verse with chapter number
                    if verse == 1:
                        self.outfile.write('<img style="float:left" src="file://' + os.getcwd() +'/images/' + str(chapter) + '.png"></img>')
                    else:
                        self.outfile.write("^**" + str(verse) + "**^ ")
                    self.outfile.write(line.strip() + " ")

    def convert_strongs(self, line):
        # [biblos](../../strongs/g/g976.md)
        line = re.sub("\[(.*?)\]\(.*?/[gh]/(.*?)\.md\)", add_link_to_strongs, line)
        return line
    
    def remove_footnotes(self, line):
        line = re.sub("\[\^\d*?\]", "", line)
        return line
    
    def remove_links(self, line):
        line = re.sub("\[(.*?)\]\(.*?\)", "\\1", line)
        return line
    
    def get_book_name(self, book):
        if book[0] in ['1', '2', '3']:
            book_name = book[0] + " " + book[1:].title()
        else:
            book_name = book.title()
        return book_name

    def get_verse_number(self, line):
        (verse) = re.search(r":(\d*)", line).groups()
        return int(verse[0])
    
    def get_chapter_verse(self, bookName, line):
        (chapter, verse) = re.search(r"</a>" + bookName + " (\d*):(\d*)", line).groups()
        return chapter, verse
    
    def create(self):

        self.create_toc()

        self.create_preface()

        for book in nt_books:
            self.add_book(book)

def get_order(file):
    file_pattern = re.compile(r'.*?(\d+).*?')
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])

def add_link_to_strongs(m):
    strongs = m.group(2)
    return m.group(1) + " ~" + strongs.upper() + "~"

## Change this to True to compile only the book of Matthew
# quick_compile = False
quick_compile = True

if quick_compile:
    nt_books = ['matthew']
else:
    nt_books = ['matthew', 'mark', 'luke', 'john', 'acts', 'romans', '1corinthians', '2corinthians', 'galatians',
            'ephesians', 'philippians', 'colossians', '1thessalonians', '2thessalonians', '1timothy', 
            '2timothy', 'titus', 'philemon', 'hebrews', 'james', '1peter', '2peter', 
            '1john', '2john', '3john', 'jude', 'revelation']

markdown_bible = MarkdownBible()
markdown_bible.create()

print("Finished creating " + markdown_bible.filename)
