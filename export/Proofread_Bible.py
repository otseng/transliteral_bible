import os, sqlite3, re, glob, platform
from pathlib import Path
import sys
from common.BibleData import BibleData
from common.LanguageData import LanguageData

# Proofreading methods:
# 1. diff - check all Strongs in TRLITx are in KJVx
# 2. order - check all Strongs in TRLITx are in the same order as KJVx
# 3. missing - check all Strongs in KJVx with low frequency are in TRLITx

class ProofreadBible:

    def __init__(self):
        self.home = '/Users/otseng'
        if platform.system() == "Linux":
            self.home = '/home/oliver/dev'

        self.TRLITx = self.home + '/UniqueBible/marvelData/bibles/TRLITx.bible'
        self.connectionTRLITx = sqlite3.connect(self.TRLITx)
        self.cursorTRLITx = self.connectionTRLITx.cursor()

        self.KJVx = self.home + '/UniqueBible/marvelData/bibles/KJVx.bible'
        self.connectionKJVx = sqlite3.connect(self.KJVx)
        self.cursorKJVx = self.connectionKJVx.cursor()

        self.method = "diff"

    def __del__(self):
        self.connectionTRLITx.close()
        self.connectionKJVx.close()

    def get_book_num(self, book):
        if not book in BibleData.books:
            print("Cannot find book: " + book)
            exit(1) 
        book_num = BibleData.books[book]
        return book_num

    def get_book_name(self, number):
        for book in BibleData.books:
            if (BibleData.books[book] == number):
                return book
        return "Unknown"

    def read_verse(self, cursor, book_num, chapter_num, verse_num):
        sql = 'SELECT * from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num)
        cursor.execute(sql)
        data = cursor.fetchone()
        return data[3]

    def process_verse(self, book, chapter, verse):
        book_num = self.get_book_num(book)
        verseKJVx = self.read_verse(self.cursorKJVx, book_num, chapter, verse)
        verseTRLITx = self.read_verse(self.cursorTRLITx, book_num, chapter, verse)
        
        if book_num < 40:
            regex = r'H\d+' # Hebrew
        else:
            regex = r'G\d+' # Greek
        strongsKJVx = re.findall(regex, verseKJVx)
        strongsTRLITx = re.findall(regex, verseTRLITx)

        # Find items in TRLITx that are not in KJVx
        if self.method == "diff":
            differences = self.get_list_differences(strongsTRLITx, strongsKJVx)
            self.remove_known_from_list(differences)

            if differences:
                print(book + ' ' + str(chapter) + ':' + str(verse))
                print(differences)
                return True
            else:
                return False
        elif self.method == "order":
            i = 0
            error = []
            for strong in strongsTRLITx:
                # print(strong + " " + strongsKJVx[i])
                while True:
                    if i >= len(strongsKJVx):
                        error.append(strong)
                        break
                    if strongsKJVx[i] == strong:
                        i = i + 1
                        break
                    i = i + 1
            if error:
                print(book + ' ' + str(chapter) + ':' + str(verse))
                print(error)
                return True
            return False
        elif self.method == "missing":
            differences = self.get_list_differences(strongsKJVx, strongsTRLITx)
            self.remove_known_from_list(differences)

            if differences:
                print(book + ' ' + str(chapter) + ':' + str(verse))
                print(differences)
                return True
            else:
                return False
        else:
            print(self.method + " method not implemented")
            exit(1)

    def get_list_differences(self, list1, list2):
        return list(set(list1) - set(list2))

    def remove_known_from_list(self, list):
        to_remove = LanguageData.commonHebrew + LanguageData.commonGreek
        for item in to_remove:
            if item[0] in list:
                list.remove(item[0])

    def process_chapter(self, book, chapter):
        book_num = self.get_book_num(book)
        verses = BibleData.verses[book_num][int(chapter)]
        for verse in range(1, verses + 1):
            self.process_verse(book, chapter, verse)

    def process_book(self, book, start=1):
        book_num = self.get_book_num(book)
        chapters = BibleData.chapters[book_num]
        error_found = False
        for chapter in range(int(start), chapters + 1):
            print("Processing " + str(book) + " " + str(chapter))
            verses = BibleData.verses[book_num][int(chapter)]
            for verse in range(1, verses + 1):
                error_found = self.process_verse(book, chapter, verse) | error_found
            # if error_found:
            #     return


if len(sys.argv) < 3:
    print("Usage: method book chapter (verse)")
    print("Method: diff, order, missing")
    print("Books:")
    print("all, ot, nt, matthew, mark, luke, john, acts, romans, 1corinthians, 2corinthians")
    print("galatians, ephesians, philippians, colossians")
    print("1thessalonians, 2thessalonians, 1timothy, 2timothy, titus, philemon, hebrews")
    print("james, 1peter, 2peter, 1john, 2john, 3john, jude, revelation")
    print("genesis, exodus, leviticus, numbers, deuteronomy")
    print("joshua, judges, ruth, 1samuel, 2samuel, 1kings, 1kings, 1chronicles, 2chronicles")
    print("ezra, nehemiah, esther, job, psalms, proverbs, ecclesiastes, solomon")
    print("isaiah, jeremiah, lamentations, ezekiel, daniel, hosea")
    print("joel, amos, obadiah, jonah, micah, nahum")
    print("habakkuk, zephaniah, haggai, zechariah, malachi")
    exit(1)

proofread = ProofreadBible()
proofread.method = sys.argv[1].lower()
book = sys.argv[2]
bookfile = book.lower()

# print(str(len(sys.argv)) + " arguments")

if len(sys.argv) == 3:
    if bookfile in ("nt", "ot", "all"):
        for book in BibleData.books:
            if (bookfile == "nt" and BibleData.books[book] >= 40) or \
            (bookfile == "ot" and BibleData.books[book] < 40) or \
            bookfile == "all":
                proofread.process_book(book)
    else:
        start = proofread.get_book_num(bookfile)
        if (start < 40):
            end = 40
        else:
            end = 67
        for bookNum in range(start, end):
            bookfile = proofread.get_book_name(bookNum)
            proofread.process_book(bookfile)
elif len(sys.argv) == 4:
    chapter = sys.argv[3]
    proofread.process_book(bookfile, chapter)
elif len(sys.argv) == 5:
    chapter = sys.argv[3]
    verse = sys.argv[4]
    print("Processing " + book + " " + chapter + ":" + verse)
    proofread.process_verse(bookfile, chapter, verse)

print("Done")
