import os, sqlite3, re, glob, platform
import sys
from urllib.parse import quote

# CEDict Lexicon
# Data from: https://cc-cedict.org/wiki/

class CEDictLexicon:

    start_count = 0
    end_count = 125000
    debug = False

    limit = 125000 # takes 15 minutes to process all entries
    status_offset = 1000
    cache = {}
    keys = []

    def __init__(self):
        self.home = '/Users/otseng'
        if platform.system() == "Linux":
            self.home = '/home/oliver'
        file = self.home + '/UniqueBible/marvelData/lexicons/CEDICT.lexicon'
        self.CEDictSource = self.home + '/Downloads/save/cedict_ts.u8'
        self.CEDictLexicon = file
        self.connection = sqlite3.connect(self.CEDictLexicon)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def create_tables(self):
        print('Create table')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS Lexicon (Topic NVARCHAR(100), Definition TEXT);')

    def drop_tables(self):
        print('Drop table')
        self.cursor.execute('DROP TABLE IF EXISTS Lexicon;')

    def insert_details(self):
        if not self.entry_exists('info'):
            sql = ("INSERT INTO Lexicon VALUES ('info', 'CEDICT Lexicon - https://cc-cedict.org')")
            self.cursor.execute(sql)

    def delete_entry(self, topic):
        self.cursor.execute("DELETE from Lexicon where topic='" + topic + "'")

    def entry_exists(self, topic):
        if topic in self.cache:
            # print("Cache hit: " + topic)
            return True
        else:
            # print("Cache miss: " + topic)
            return False

    def get_entry(self, topic):
        text = self.cache[topic]
        return text

    def url_encode_chinese(self, text):
        return quote(text, encoding='utf-8')

    def insert_topic(self, topic, text):
        if self.entry_exists(topic):
            self.update_entry(topic, text)
        else:
            self.insert_entry(topic, text)
        self.connection.commit()

    def update_entry(self, topic, text):
        original = self.get_entry(topic)
        split_marker = "<br/><b>Links:</b><br/>"
        split_text = original.split(split_marker, 1)
        if text in split_text[0]:
            return
        text = text.replace("''", "'")
        text = text.replace("'", "''")
        new_text = split_text[0] + text + split_marker + split_text[1]
        new_text = new_text.replace('"', '\"')

        sql = ("UPDATE Lexicon SET definition='{1}' WHERE topic='{0}'".format(topic, new_text))
        self.cursor.execute(sql)
        self.cache[topic] = new_text
        self.prune_cache(topic)

    def insert_entry(self, topic, text):
        topic = topic.strip()
        text += "<br/><b>Links:</b><br/>"
        text += "<a target='_blank' href='https://www.chinesepod.com/dictionary/" + topic + "'>Chinese Pod</a><br/>"
        text += "<a target='_blank' href='https://www.purpleculture.net/dictionary-details/?word=" + topic + "'>Purple Culture</a><br/>"
        text += "<a target='_blank' href='https://en.wiktionary.org/wiki/" + topic + "'>Wiktionary</a><br/>"
        text += "<a target='_blank' href='https://www.google.com/search?q=define%3A+" + topic + "'>Google</a><br/>"
        text = text.replace("'", "''")
        text = text.replace('"', '\"')

        sql = ("INSERT INTO Lexicon VALUES ('{0}', '{1}')".format(topic, text))
        # if self.debug:
        #     print(text + "\n\n")
        self.cursor.execute(sql)
        self.cache[topic] = text
        self.prune_cache(topic)

    def prune_cache(self, topic):
        if len(self.cache) > 25:
            key = self.keys.pop(0)
            if key in self.cache:
                self.cache.pop(key)
        self.keys.append(topic)

    def process_lexicon(self):
        file1 = open(self.CEDictSource, 'r')
        count = 0

        while True:

            count += 1
            if count > self.limit:
                break
            if count % self.status_offset == 0:
                print(str(count))

            if count < self.start_count:
                continue

            if count > self.end_count:
                return

            line = file1.readline()

            if not line:
                break

            (traditional, simplified, text) = self.parse_line(line)
            # if traditional != '好玩':
            #     continue
            if text:
                self.insert_topic(traditional, text)
                encoded = self.url_encode_chinese(traditional)
                if traditional != encoded:
                    self.insert_topic(encoded, text)

                if (traditional != simplified):
                    self.insert_topic(simplified, text)
                    encoded = self.url_encode_chinese(simplified)
                    if simplified != encoded:
                        self.insert_topic(encoded, text)

        print("Processed: " + str(count))
        file1.close()

    # 一步裙 一步裙 [yi1 bu4 qun2] /pencil skirt/
    def parse_line(self, line):
        if line[0] == '#':
            return (False, False, False)

        parts = line.split('/', 1)

        characters = parts[0]
        text = parts[1]

        words = characters.split(' [', 1)[0]
        pinyin = characters.split(' [', 1)[1]
        pinyin = pinyin[:-2]
        parts = words.split(' ', 1)
        traditional = parts[0]
        simplified = parts[1]

        text = text[:-1]
        text = text.replace("/", "<br/>")
        text = pinyin + "<br/><br/>" + text + "<br/>"

        return (traditional, simplified, text)
    
    def test1(self):
        data = [
            "一氣 一气 [yi1 qi4] /at one go/at a stretch/for a period of time/forming a gang/"
        ]
        for line in data:
            (traditional, simplified, text) = self.parse_line(line)
            print(traditional, simplified, text)

    def fix_issues(self):
        self.cursor.execute("SELECT topic, definition from Lexicon where definition like '%''''%'")
        data = self.cursor.fetchall()
        print("To fix: " + str(len(data)))
        for row in data:
            topic = row[0]
            definition = row[1]
            definition = definition.replace("''''", "''")
            # print(topic + "\n" + definition + "\n\n")
            try:
                sql = ("UPDATE Lexicon SET definition='{1}' WHERE topic='{0}'".format(topic, definition))
                self.cursor.execute(sql)
            except:
                print("Error: " + topic)

db = CEDictLexicon()

# db.test1()

db.drop_tables()
db.create_tables()
db.insert_details()
db.process_lexicon()

# db.fix_issues()