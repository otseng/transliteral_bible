# [Preface](ch001.xhtml)

The Transliteral Bible (TRLIT) is an English translation of the Bible that transliterates keywords in the King James Version text. It allows readers to be able to read the Bible in a format closer to the original languages.

### Purpose of the Transliteral Bible

* To read the Bible in the original languages without the need to learn the Hebrew and Greek alphabets and grammar.
* To assist in learning Hebrew and Greek by focusing first on vocabulary instead of grammar.
* To read in a closer format to how the Bible was originally written.
* To have a deeper and richer understanding of the Bible.

### Translation Methodology

The translation methodology takes the King James Version text and replaces English words that have a Strong's number through the process of transliteration. For example, the Greek word for God “θεός” is transliterated to “theos”. Most nouns, verbs, and adjectives are transliterated. Words in italics are also removed.

Since the methodology used is a mechanical process, no interpretation bias is introduced.  Anybody who uses this methodology would achieve practically the same result, even if they do not know the original languages.

Since the Transliteral Bible is not hampered by having to decide how to translate words into English, there is no dynamic equivalence versus formal equivalence problem involved. Instead of a translator interpreting the meaning of the original words, it pushes the responsibility of interpreting what the original words mean to the reader.

### Limitations of the Transliteral Bible

* The Transliteral Bible is intended to be a study Bible and is not meant to be a natural reading Bible.
* Only the base Greek/Hebrew words are used for transliterated words, so it strips the words of grammatical information (number, case, tense, voice, mood).
* This translation is not intended to replace any Hebrew or Greek critical text.  The ultimate way to read the Bible would be to read it in the original languages.

### Strong's and Lexicon

All transliterated words include the Strong's number.  In the Kindle version, Strong's numbers are linked to the lexicon at the back of the book.

### Bible study apps

The Transliteral Bible can be read online at [UniqueBibleApp.com](https://uniquebibleapp.com/).  This site is targeted for mobile devices with an internet connection.

The Transliteral Bible is also available in the [Unique Bible App software](https://github.com/eliranwong/UniqueBible) on GitHub at [github.com/eliranwong/UniqueBible](https://github.com/eliranwong/UniqueBible).  It is open source, completely free to use, and supports Windows, Mac, and Linux.  Python proficiency is required to install the program.

### Open Source

The Transliteral Bible text is released as open source under the Apache 2.0 license. Everyone is free to use this Bible in whatever way they want as long as they abide by the license agreement.  

### Contributing

Errors in the translation exist, so readers are encouraged to submit corrections.

The Transliteral Bible is available on GitLab at [gitlab.com/otseng/transliteral_bible](https://gitlab.com/otseng/transliteral_bible). 

### FAQ

For more information, see [transliteralbible.com/faq](https://transliteralbible.com/faq)

### Feedback

Issues, suggestions, or questions can be submitted at [gitlab.com/otseng/transliteral_bible/-/issues](https://gitlab.com/otseng/transliteral_bible/-/issues).
