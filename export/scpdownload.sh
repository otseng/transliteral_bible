if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  BASE_DIR="/home/oliver"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  BASE_DIR="/Users/otseng"
fi
scp -P $SERVER_PORT $SERVER_USER@$SERVER_IP:/home/otseng/UniqueBible/marvelData_enx/lexicons/TRLIT.lexicon  ${BASE_DIR}/UniqueBible/marvelData/lexicons 
scp -P $SERVER_PORT $SERVER_USER@$SERVER_IP:/home/otseng/UniqueBible/marvelData_enx/bibles/TRLXX.bible  ${BASE_DIR}/UniqueBible/marvelData/bibles 
scp -P $SERVER_PORT $SERVER_USER@$SERVER_IP:/home/otseng/UniqueBible/marvelData_enx/bibles/TRLIT.bible  ${BASE_DIR}/UniqueBible/marvelData/bibles 
scp -P $SERVER_PORT $SERVER_USER@$SERVER_IP:/home/otseng/UniqueBible/marvelData_enx/bibles/TRLITx.bible  ${BASE_DIR}/UniqueBible/marvelData/bibles 
