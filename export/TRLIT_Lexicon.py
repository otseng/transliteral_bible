from datetime import datetime, timedelta
import os, sqlite3, re, glob, platform
import sys

class TrlitLexicon:

    limit = 15000
    overwrite = True
    debug = False
    modified_days = 10

    def __init__(self):
        self.home = '/Users/otseng/'
        if platform.system() == "Linux":
            self.home = '/home/oliver/'
        file = self.home + '/UniqueBible/marvelData/lexicons/TRLIT.lexicon'
        self.TrlitLexicon = file
        self.connection = sqlite3.connect(self.TrlitLexicon)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def create_tables(self):
        print('Create table')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS Lexicon (Topic NVARCHAR(100), Definition TEXT);')

    def drop_tables(self):
        print('Drop table')
        self.cursor.execute('DROP TABLE IF EXISTS Lexicon;')

    def insert_details(self):
        if not self.entry_exists('info'):
            sql = ("INSERT INTO Lexicon VALUES ('info', 'Transliteral Bible Lexicon')")
            self.cursor.execute(sql)

    def delete_entry(self, topic):
        self.cursor.execute("DELETE from Lexicon where topic='" + topic + "'")

    def delete_hebrew(self):
        self.cursor.execute("DELETE from Lexicon where topic like 'H%'")

    def delete_greek(self):
        self.cursor.execute("DELETE from Lexicon where topic like 'G%'")

    def entry_exists(self, topic):
        self.cursor.execute("SELECT COUNT(*) from Lexicon where topic='" + topic + "'")
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_topic(self, topic, text):
        topic = topic.upper()
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        if self.overwrite:
            self.delete_entry(topic)
        if not self.entry_exists(topic):
            sql = ("INSERT INTO Lexicon VALUES ('{0}', '{1}')".format(topic, text))
            if self.debug:
                print(sql)
            else:
                self.cursor.execute(sql)

    def update_topic(self, topic, text):
        topic = topic.upper()
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        if self.overwrite:
            self.delete_entry(topic)
        if not self.entry_exists(topic):
            sql = ("UPDATE Lexicon SET Topic='{0}', Description='{1}' ".format(topic, text) + " where Topic='" + topic + "'")
            if self.debug:
                print(sql)
            else:
                self.cursor.execute(sql)

    def was_file_modified(self, file_path):
        if self.modified_days == 0:
            return True
        if not os.path.exists(file_path):
            return False
        last_modified_time = os.path.getmtime(file_path)
        days_ago = datetime.now() - timedelta(days=self.modified_days)
        last_modified_datetime = datetime.fromtimestamp(last_modified_time)
        return last_modified_datetime > days_ago

    def process_strongs(self, lang, pattern="*"):
        fileglob = self.home + '/dev/transliteral_bible/strongs/' + lang + '/' + lang + pattern + '.md'
        count = 0
        files = glob.glob(fileglob)
        size = len(files)
        for filename in files:
            if not self.was_file_modified(filename):
                continue
            fname = os.path.basename(filename).lower()
            count += 1
            if count > TrlitLexicon.limit:
                break
            topic = fname[:-3]
            print("Processing (" + str(count) + "/" + str(size) + "): " + topic)
            file = open(filename, "r")
            content = ''
            lines = file.readlines()
            found_text = False
            found_links = False
            for line in lines:
                if line.startswith('##'):
                    content += "<b>" + line[2:] + "</b>\n"
                    content = content.replace("Articles", "Links")
                    found_links = True
                elif line.startswith('#'):
                    content += "<wlexeme><mlexeme><b><grk>" + line[2:] + "</grk></b></grk></wlexeme>\n"
                elif len(line) < 2:
                    if found_text:
                        content += '<br>\n'
                        if not found_links:
                            content += '<br>\n'
                else:
                    if not found_text:
                        content += "Transliteration: "
                    found_text = True
                    content += self.replace_link(line)

            if not self.entry_exists(topic):
                self.insert_topic(topic, content)
            file.close()

    def process_lexicon(self):
        self.process_strongs('h')
        self.process_strongs('g')

    def replace_link(self, line):
        line = re.sub("\[(.+?)\]\(\.\./h/h(\d+?)\.md\)", "<ref onclick=\"lex('H\\2')\">\\1</ref>", line)
        line = re.sub("\[(.+?)\]\(\.\./g/g(\d+?)\.md\)", "<ref onclick=\"lex('G\\2')\">\\1</ref>", line)
        line = re.sub("\[(.+?)\]\(h(\d+?)\.md\)", "<ref onclick=\"lex('H\\2')\">\\1</ref>", line)
        line = re.sub("\[(.+?)\]\(g(\d+?)\.md\)", "<ref onclick=\"lex('G\\2')\">\\1</ref>", line)
        line = re.sub("\[(.+?)\]\((.+?)\)", "<a href=\"\\2\">\\1</a>", line)
        return line

    def test1(self):
        res = db.replace_link(("[Study Light](https://www.studylight.org/lexicons/greek/2.html)"
            "[Bible Hub](https://biblehub.com/str/greek/2.htm)"))
        print(res)

db = TrlitLexicon()

# python TRLIT_Lexicon.py
if len(sys.argv) < 2:
    # db.drop_tables()
    # db.create_tables()
    # db.insert_details()
    # db.process_lexicon()

    db.process_strongs('g', '1206')

    # db.delete_greek()
    # db.process_strongs('g')

    # db.delete_hebrew()
    # db.process_strongs('h')
# python TRLIT_Lexicon.py (b|h|g|full|g1206|h1206)
elif len(sys.argv) == 2:
    lang = sys.argv[1][0].lower()
    if sys.argv[1] == "full":
            db.modified_days = 0
            db.delete_greek()
            db.delete_hebrew()
            db.process_strongs('h')
            db.process_strongs('g')
    elif (len(sys.argv[1]) > 1):
        strongs = sys.argv[1][1:]
        if db.overwrite:
            db.delete_entry(lang + strongs)
        print("Inserting: " + lang.upper() + strongs)
        db.process_strongs(lang, strongs)
    else:
        if lang == "h" or lang == "b":
            db.process_strongs('h')
        if lang == "g" or lang == "b":
            db.process_strongs('g')
        
