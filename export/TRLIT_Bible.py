import os, sqlite3, re, glob, platform
from pathlib import Path
import sys

from common.BibleData import BibleData
from TRLIT_Bible_Books import TrlitBibleBooks

class TrlitBible:

    def __init__(self):
        self.home = '/Users/otseng'
        if platform.system() == "Linux":
            self.home = '/home/oliver'
        self.TrlitBible = self.home + '/UniqueBible/marvelData/bibles/TRLIT.bible'
        print(self.TrlitBible)
        self.connection = sqlite3.connect(self.TrlitBible)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def create_tables(self):
        print('Create tables')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Details (Title NVARCHAR(100), 
                               Abbreviation NVARCHAR(50), Information TEXT, Version INT, OldTestament BOOL,
                               NewTestament BOOL, Apocrypha BOOL, Strongs BOOL);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Bible (Book INT, Chapter INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Verses (Book INT, Chapter INT, Verse INT, Scripture TEXT);
                            ''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Notes (Book INT, Chapter INT, Verse INT, ID TEXT, Note TEXT);
                            ''')

    def drop_tables(self):
        print('Drop tables')
        self.cursor.execute('DROP TABLE IF EXISTS Bible;')
        self.cursor.execute('DROP TABLE IF EXISTS Details;')
        self.cursor.execute('DROP TABLE IF EXISTS Verses;')
        self.cursor.execute('DROP TABLE IF EXISTS Notes;')

    def insert_details(self):
        if self.get_count('Details') == 0:
            sql = ("INSERT INTO Details VALUES ('Transliteral Bible', 'TRLIT',"
                   "'https://transliteralbible.com', 1, 1, 1, 0, 1);")
            self.cursor.execute(sql)
            sql = ("INSERT INTO Verses ( Scripture, Book, Chapter, Verse ) "
                "VALUES ('Transliteral Bible', 0, 0, 0)")
            self.cursor.execute(sql)

    def get_count(self, table):
        self.cursor.execute('SELECT COUNT(*) from ' + table)
        count = self.cursor.fetchone()[0]
        return count

    def delete_chapter(self, book_num, chapter_num):
        self.cursor.execute('DELETE from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))

    def chapter_exists(self, book_num, chapter_num):
        self.cursor.execute('SELECT COUNT(*) from Bible where Book=' + str(book_num) + ' and Chapter=' + str(chapter_num))
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def delete_verse(self, book_num, chapter_num, verse_num):
        self.cursor.execute('DELETE from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))

    def verse_exists(self, book_num, chapter_num, verse_num):
        self.cursor.execute('SELECT COUNT(*) from Verses where Book=' + str(book_num) \
               + ' and Chapter=' + str(chapter_num) + ' and Verse=' + str(verse_num))
        count = self.cursor.fetchone()[0]
        if count == 0:
            return False
        else: 
            return True

    def insert_chapter(self, book_num, chapter_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        text = text.replace('\n', '')
        if self.overwrite:
            self.delete_chapter(book_num, chapter_num)
        if not self.chapter_exists(book_num, chapter_num):
            print("Inserting book {0} chapter {1}".format(book_num, chapter_num))
            sql = ("INSERT INTO Bible (Book, Chapter, Scripture) VALUES ({0}, {1}, '{2}');".format(book_num, chapter_num, text))
            if self.debug:
                print(sql)
            else:
                self.cursor.execute(sql)

    def insert_verse(self, book_num, chapter_num, verse_num, text):
        text = text.replace("'", "''")
        text = text.replace('"', '\"')
        if self.overwrite:
            self.delete_verse(book_num, chapter_num, verse_num)
        if not self.verse_exists(book_num, chapter_num, verse_num):
            print("Inserting book {0} chapter {1} verse {2}".format(book_num, chapter_num, verse_num))
            sql = ("INSERT INTO Verses VALUES ({0}, {1}, {2}, '{3}');".format(book_num, chapter_num, verse_num, text))
            if not self.debug:
                self.cursor.execute(sql)

    def process_book(self, book):
        fileglob = self.home + '/dev/transliteral_bible/bible/' + book + '/' + book + '_*.md'
        for filename in glob.glob(fileglob):
            if "lxx" in filename:
                continue
            self.process_chapter(filename)
          
    def process_chapter(self, filename):
        file = open(filename, "r")
        # TODO: need to handle LXX later
        if "lxx" in filename:
            return
        book, chapter_num = Path(filename).stem.split("_")
        book_num = self.get_book_num(book)
        self.delete_chapter(book_num, chapter_num)
        content = ''
        lines = file.readlines()
        found_text = False
        for line in lines:
            if line.startswith('---'):
                break
            if line.startswith('#'):
                pass
            elif len(line) < 2:
                if found_text:
                    content += '<br>\n'
            elif line.startswith('<a name'):
                x, verse_num = self.get_chapter_verse(line)
            else:
                found_text = True
                line = self.remove_footnote(line)

                unformatted = line
                unformatted = self.red_letter(unformatted)
                unformatted = self.replace_greek_simple(unformatted)
                unformatted = self.replace_hebrew_simple(unformatted)

                formatted = line
                formatted = self.red_letter(formatted)
                formatted = self.replace_greek_link(formatted)
                formatted = self.replace_hebrew_link(formatted)

                # print(str(chapter_num) + ":" + str(verse_num))
                try:
                    self.insert_verse(book_num, chapter_num, verse_num, unformatted)
                
                    content += "<verse>"
                    content += ("<vid id=\"v{0}.{1}.{2}\" onclick=\"luV({2})\">{2} "
                        "</vid>"
                        ).format(book_num, chapter_num, verse_num)
                    content += formatted
                    content += "</verse>"
                except:
                    pass

        try:
            self.insert_chapter(book_num, chapter_num, content)
        except:
            pass
        file.close()

    def reset_chapter(self, book_name, chapter_num):
        book_num = self.get_book_num(book_name)
        if book_num:
            self.delete_chapter(book_num, chapter_num)
            filename = TrlitBible.BASE_DIR + book_name + '/' + book_name + '_' + str(chapter_num) + '.md'
            if os.path.exists(filename):
                self.process_chapter(filename)
            else:
                print(filename + " does not exist")
        else:
            print("Could not find " + book_name)

    def process_bible(self):
        for book in TrlitBibleBooks.books.keys():
            print("Processing " + book)
            self.process_book(book)

    def get_book_num(self, book):
        if not book in BibleData.books:
            return False 
        book_num = BibleData.books[book]
        # print(book + ":" + str(book_num))
        return book_num

    def get_chapter_verse(self, line):
        # <a name="matthew_1_1"></a>Matthew 1:1
        res = re.search('_(\d+)_(\d+)', line).groups()
        return res[0], res[1]
        
    def unformat(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def replace_greek_link(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<ref onclick=\"lex('G\\2')\">\\1</ref>", line)
        return res

    def replace_hebrew_link(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[h](\d+)\.md\)", "<ref onclick=\"lex('H\\2')\">\\1</ref>", line)
        return res

    def replace_greek_simple(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[g](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def replace_hebrew_simple(self, line):
        res = re.sub("\[(.+?)\]\(.+?\/[h](\d+)\.md\)", "<i>\\1</i>", line)
        return res

    def remove_footnote(self, line):
        res = re.sub("\[\^\d+\]", "", line)
        return res

    def red_letter(self, line):
        res = re.sub("\*\*(.+?)\*\*", "<red>\\1</red>", line)
        return res

    def remove_red_letter(self, line):
        res = line.replace("**", "")
        return res

    def test1(self):
        res = db.get_chapter_verse('<a name="matthew_2_3"></a>Matthew 2:3')
        print(res)

    def test3(self):
        line = "**This is in red.** This is not.  **Is this red?**"
        print(self.red_letter(line))

db = TrlitBible()
db.debug = False
db.overwrite = True

if len(sys.argv) < 2:

    # db.drop_tables()
    # db.create_tables()
    # db.insert_details()

    db.process_bible()

else:
    book = sys.argv[1]
    bookfile = book.lower()
    if len(sys.argv) == 3:
        chapter = sys.argv[2]
        db.process_chapter(bookfile, chapter)
    elif len(sys.argv) == 2:
        db.process_book(bookfile)