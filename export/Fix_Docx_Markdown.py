file_name = "TransliteralBible.html"

with open(file_name, "r") as f:
    lines = f.readlines()

with open(file_name, "w") as f:
    for line in lines:
        if "<h1" in line or "<h2" in line:
            f.write(line + "\n")
        else:
            f.write(line[:-1] + " ")
