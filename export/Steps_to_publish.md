# Steps to publish

## Automated proofread

* Run Proofread_Bible.py

## Update versions

* Update version.md
* Update metadata.yaml

## Generate EPUB doc

* Set quick_compile = False in Create_Epub_markdown.py
* Run create_epub_markdown.sh
* Run create_epub_file.sh

## Proofread EPUB

* Read EPUB with Apple Books app

## Check lexicon entries

* 1 Tim 4:8 - ōphelimos G5624

## Proofread Kindle

* Preview in Kindle Previewer

## Publish EPUB files

* [Publish in KDP](https://kdp.amazon.com/en_US/bookshelf)
