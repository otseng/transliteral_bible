from PIL import Image, ImageDraw, ImageFont

fnt = ImageFont.truetype("/Users/otseng/Library/Fonts/Cambria.ttf", 25)
for number in range(1, 100):
    size = (25, 35)
    if number > 9:
        size = (35, 35)
    image = Image.new("RGBA", size, (255, 255, 255, 0))
    draw = ImageDraw.Draw(image)
    color = (0, 0, 0)
    draw.text((5,3), str(number), color, font=fnt)

    image.save("images/" + str(number) + ".png")
